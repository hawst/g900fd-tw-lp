.class Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$25;
.super Ljava/lang/Object;
.source "SlideShowActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private direction:F

.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

.field private x:F


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V
    .locals 0

    .prologue
    .line 2937
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$25;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 2941
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2962
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 2943
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$25;->x:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$25;->direction:F

    .line 2944
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$25;->direction:F

    const/high16 v1, 0x42200000    # 40.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 2947
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$25;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsUseGLES:Z
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$4200(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$25;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$600(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IIsWaitingFlag()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2948
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$25;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # invokes: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setSlideVisibility()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$4300(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    goto :goto_0

    .line 2950
    :cond_2
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$25;->direction:F

    const/high16 v1, -0x3de00000    # -40.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 2953
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$25;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsUseGLES:Z
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$4200(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$25;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$600(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IIsWaitingFlag()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2954
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$25;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onCloseSlideShow()V

    goto :goto_0

    .line 2958
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$25;->x:F

    goto :goto_0

    .line 2941
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
