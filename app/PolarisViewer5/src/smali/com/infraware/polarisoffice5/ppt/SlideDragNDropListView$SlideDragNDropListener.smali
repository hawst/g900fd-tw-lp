.class public interface abstract Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;
.super Ljava/lang/Object;
.source "SlideDragNDropListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SlideDragNDropListener"
.end annotation


# virtual methods
.method public abstract onDrop(II)V
.end method

.method public abstract onEndAnimation(I)V
.end method

.method public abstract onMoveNext()V
.end method

.method public abstract onMovePrevious()V
.end method

.method public abstract onSelect(Landroid/view/View;I)V
.end method

.method public abstract onStartAnimation()V
.end method
