.class public Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;
.super Ljava/lang/Object;
.source "SlideShowGestureProc.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_HID_ACTION;
.implements Lcom/infraware/office/evengine/E$EV_SLIDESHOW_PLAY_TYPE;
.implements Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$SlideShowGestureProcListener;,
        Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$GestureMode;
    }
.end annotation


# static fields
.field public static final VIDEO_NONE:I = 0x0

.field public static final VIDEO_PLAING:I = 0x2

.field public static final VIDEO_START:I = 0x1


# instance fields
.field private final LOG_CAT:Ljava/lang/String;

.field private hasAudioFocus:Z

.field public isPlay:I

.field private mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

.field private mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mEvInterface:Lcom/infraware/office/evengine/EvInterface;

.field private mGestureDetector:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

.field private mGestureMode:I

.field private mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$SlideShowGestureProcListener;

.field private mPenDragTime:[I

.field private mPenDragX:[I

.field private mPenDragY:[I

.field private mPenPressure:[I

.field private mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

.field private mPopup:Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;

.field private mSlideGLImage:Landroid/view/View;

.field private mSlideImage:Landroid/view/View;

.field private mSlideShowMode:I

.field private mVideo:Landroid/widget/VideoView;

.field private mVideobtn:Landroid/widget/ImageView;

.field private m_VideoFrame:Landroid/widget/RelativeLayout;

.field private m_bIsUseGLES:Z

.field private rcVideoPath:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/app/Activity;Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$SlideShowGestureProcListener;Landroid/view/View;Landroid/view/View;Lcom/infraware/polarisoffice5/common/PointerDrawView;Z)V
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "listener"    # Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$SlideShowGestureProcListener;
    .param p3, "slideGLImage"    # Landroid/view/View;
    .param p4, "slideImage"    # Landroid/view/View;
    .param p5, "pointerDraw"    # Lcom/infraware/polarisoffice5/common/PointerDrawView;
    .param p6, "isUseGLES"    # Z

    .prologue
    const/16 v3, 0x100

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const-string/jumbo v0, "SlideShowGestureProc"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->LOG_CAT:Ljava/lang/String;

    .line 37
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->m_bIsUseGLES:Z

    .line 40
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureDetector:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    .line 41
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$SlideShowGestureProcListener;

    .line 42
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideGLImage:Landroid/view/View;

    .line 44
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideImage:Landroid/view/View;

    .line 45
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    .line 46
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPopup:Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;

    .line 48
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideShowMode:I

    .line 49
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureMode:I

    .line 51
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragX:[I

    .line 52
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragY:[I

    .line 54
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragTime:[I

    .line 55
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenPressure:[I

    .line 57
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->m_VideoFrame:Landroid/widget/RelativeLayout;

    .line 58
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideobtn:Landroid/widget/ImageView;

    .line 60
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->rcVideoPath:Ljava/lang/String;

    .line 61
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideo:Landroid/widget/VideoView;

    .line 62
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->isPlay:I

    .line 69
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mAudioManager:Landroid/media/AudioManager;

    .line 70
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 71
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->hasAudioFocus:Z

    .line 87
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 88
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    invoke-direct {v0, p1, p0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;-><init>(Landroid/app/Activity;Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;Z)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureDetector:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    move-object v0, p1

    .line 89
    check-cast v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .line 90
    iput-object p2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$SlideShowGestureProcListener;

    .line 91
    iput-boolean p6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->m_bIsUseGLES:Z

    .line 92
    iput-object p3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideGLImage:Landroid/view/View;

    .line 93
    iput-object p4, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideImage:Landroid/view/View;

    .line 94
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->m_bIsUseGLES:Z

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideGLImage:Landroid/view/View;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureDetector:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 98
    :goto_0
    iput-object p5, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    .line 100
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;

    invoke-direct {v0, p1, p4}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;-><init>(Landroid/app/Activity;Landroid/view/View;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPopup:Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;

    .line 103
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    const-string/jumbo v1, "audio"

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mAudioManager:Landroid/media/AudioManager;

    .line 104
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$1;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 119
    return-void

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideImage:Landroid/view/View;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureDetector:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0
.end method

.method private BGMPause()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 123
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->hasAudioFocus:Z

    if-nez v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 125
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->hasAudioFocus:Z

    .line 127
    :cond_0
    return-void
.end method

.method private BGMResume()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->hasAudioFocus:Z

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 133
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->hasAudioFocus:Z

    .line 135
    :cond_0
    return-void
.end method

.method private StartPlayVideo(Landroid/graphics/Rect;Ljava/lang/String;)V
    .locals 5
    .param p1, "a_rcVideo"    # Landroid/graphics/Rect;
    .param p2, "rcPath"    # Ljava/lang/String;

    .prologue
    .line 467
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$2;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$2;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;)V

    const-wide/16 v3, 0x3e8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 474
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideobtn:Landroid/widget/ImageView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 475
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    const v2, 0x7f0b0202

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/VideoView;

    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideo:Landroid/widget/VideoView;

    .line 476
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideo:Landroid/widget/VideoView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setVisibility(I)V

    .line 477
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideo:Landroid/widget/VideoView;

    invoke-virtual {v1, p2}, Landroid/widget/VideoView;->setVideoPath(Ljava/lang/String;)V

    .line 478
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideo:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 479
    .local v0, "oParams":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 480
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 481
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->m_VideoFrame:Landroid/widget/RelativeLayout;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 482
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->m_VideoFrame:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->requestLayout()V

    .line 483
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideo:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->start()V

    .line 484
    const/4 v1, 0x2

    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->isPlay:I

    .line 486
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideo:Landroid/widget/VideoView;

    new-instance v2, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$3;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$3;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;)V

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 492
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->BGMPause()V

    return-void
.end method

.method private onHIDAction(IIIII)V
    .locals 7
    .param p1, "action"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "time"    # I
    .param p5, "pressLevel"    # I

    .prologue
    .line 440
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v4, 0x0

    move v1, p1

    move v2, p2

    move v3, p3

    move v5, p4

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 441
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setEnableEraser()V

    .line 442
    return-void
.end method

.method private onSetPenPosition([I[I[I[II)V
    .locals 6
    .param p1, "x"    # [I
    .param p2, "y"    # [I
    .param p3, "time"    # [I
    .param p4, "pressLevel"    # [I
    .param p5, "count"    # I

    .prologue
    .line 445
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvInterface;->ISetPenPosition([I[I[I[II)V

    .line 446
    return-void
.end method

.method private setVideobtn(Landroid/graphics/Rect;)V
    .locals 4
    .param p1, "oRect"    # Landroid/graphics/Rect;

    .prologue
    const/4 v3, 0x0

    .line 449
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideo:Landroid/widget/VideoView;

    if-eqz v1, :cond_0

    .line 450
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->removeVideoView()V

    .line 451
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    const v2, 0x7f0b0200

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->m_VideoFrame:Landroid/widget/RelativeLayout;

    .line 452
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    const v2, 0x7f0b0201

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideobtn:Landroid/widget/ImageView;

    .line 453
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->m_VideoFrame:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 454
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideobtn:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 455
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->m_VideoFrame:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 456
    .local v0, "oHolderParam":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 457
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 458
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->m_VideoFrame:Landroid/widget/RelativeLayout;

    iget v2, p1, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 459
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->m_VideoFrame:Landroid/widget/RelativeLayout;

    iget v2, p1, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 460
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->m_VideoFrame:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 461
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->m_VideoFrame:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->requestLayout()V

    .line 462
    const/4 v1, 0x1

    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->isPlay:I

    .line 463
    return-void
.end method


# virtual methods
.method public Gesturefinalize()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 138
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureDetector:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureDetector:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->GestureDetectorFinalize()V

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPopup:Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;

    if-eqz v0, :cond_1

    .line 142
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPopup:Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->PopupMenuFinalize()V

    .line 143
    :cond_1
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 144
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureDetector:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    .line 145
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$SlideShowGestureProcListener;

    .line 146
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->m_bIsUseGLES:Z

    if-eqz v0, :cond_2

    .line 148
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideGLImage:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 149
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideGLImage:Landroid/view/View;

    .line 156
    :goto_0
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    .line 157
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->m_VideoFrame:Landroid/widget/RelativeLayout;

    .line 158
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideobtn:Landroid/widget/ImageView;

    .line 159
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideo:Landroid/widget/VideoView;

    .line 160
    return-void

    .line 153
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideImage:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 154
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideImage:Landroid/view/View;

    goto :goto_0
.end method

.method public cancelGesture()V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureDetector:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureDetector:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->cancel()V

    .line 165
    :cond_0
    return-void
.end method

.method public checkExistVideoFile(Ljava/lang/String;)Z
    .locals 5
    .param p1, "mStrVideoPath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 516
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 518
    .local v0, "oVideoFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 519
    const/4 v1, 0x1

    .line 523
    :goto_0
    return v1

    .line 522
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    const v4, 0x7f0702e3

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public getGestureMode()I
    .locals 1

    .prologue
    .line 436
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureMode:I

    return v0
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 390
    const-string/jumbo v0, "SlideShowGestureProc"

    const-string/jumbo v1, "onDoubleTap"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    const/4 v0, 0x3

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureMode:I

    .line 392
    const/4 v0, 0x0

    return v0
.end method

.method public onDoubleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 397
    const-string/jumbo v0, "SlideShowGestureProc"

    const-string/jumbo v1, "onDoubleTapConfirmed"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$SlideShowGestureProcListener;

    invoke-interface {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$SlideShowGestureProcListener;->onDoubleTapConfirmed()V

    .line 399
    const/4 v0, 0x0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v2, 0x1

    .line 404
    const-string/jumbo v0, "SlideShowGestureProc"

    const-string/jumbo v1, "onFling"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideShowMode:I

    if-ne v0, v2, :cond_6

    .line 407
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->OnTouchTheScreen()Z

    move-result v0

    if-nez v0, :cond_1

    .line 432
    :cond_0
    :goto_0
    return v2

    .line 411
    :cond_1
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 412
    const/4 v0, 0x2

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureMode:I

    .line 413
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideo:Landroid/widget/VideoView;

    if-eqz v0, :cond_2

    .line 414
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->removeVideoView()V

    .line 415
    :cond_2
    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-lez v0, :cond_4

    .line 418
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->isUseGLES()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IIsWaitingFlag()Z

    move-result v0

    if-nez v0, :cond_0

    .line 419
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$SlideShowGestureProcListener;

    invoke-interface {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$SlideShowGestureProcListener;->onTouchPlayPrev()V

    goto :goto_0

    .line 423
    :cond_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->isUseGLES()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IIsWaitingFlag()Z

    move-result v0

    if-nez v0, :cond_0

    .line 424
    :cond_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$SlideShowGestureProcListener;

    invoke-interface {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$SlideShowGestureProcListener;->onTouchPlayNext()V

    goto :goto_0

    .line 429
    :cond_6
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideShowMode:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    .line 430
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    goto :goto_0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 307
    const-string/jumbo v3, "SlideShowGestureProc"

    const-string/jumbo v4, "onSingleTapConfirmed"

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureMode:I

    if-eq v3, v8, :cond_c

    .line 309
    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideShowMode:I

    if-ne v3, v7, :cond_d

    .line 311
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Lcom/infraware/office/evengine/EvInterface;->IGetHyperLinkInfoEx(II)Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-result-object v0

    .line 312
    .local v0, "hyperlinkInfo":Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;
    if-eqz v0, :cond_1

    .line 313
    iget v3, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->bUse:I

    if-ne v3, v7, :cond_1

    iget-object v3, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 317
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPopup:Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->setPosition(II)V

    .line 318
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPopup:Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;

    invoke-virtual {v3, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->setHyperlink(Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;)V

    .line 319
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPopup:Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->show()V

    .line 385
    .end local v0    # "hyperlinkInfo":Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;
    :cond_0
    :goto_0
    return v6

    .line 324
    .restart local v0    # "hyperlinkInfo":Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;
    :cond_1
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 325
    .local v2, "rcVideo":Landroid/graphics/Rect;
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v3, v4, v5, v2}, Lcom/infraware/office/evengine/EvInterface;->IGetSlideShowVideoInfo(IILandroid/graphics/Rect;)Ljava/lang/String;

    move-result-object v1

    .line 326
    .local v1, "mTempPath":Ljava/lang/String;
    if-eqz v1, :cond_9

    .line 328
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->checkExistVideoFile(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string/jumbo v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 332
    :cond_2
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->rcVideoPath:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 334
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->rcVideoPath:Ljava/lang/String;

    .line 345
    :cond_3
    :goto_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->rcVideoPath:Ljava/lang/String;

    if-eqz v3, :cond_9

    .line 347
    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->isPlay:I

    if-nez v3, :cond_5

    .line 349
    invoke-direct {p0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->setVideobtn(Landroid/graphics/Rect;)V

    goto :goto_0

    .line 338
    :cond_4
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->rcVideoPath:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 340
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->rcVideoPath:Ljava/lang/String;

    .line 341
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->removeVideoView()V

    goto :goto_1

    .line 352
    :cond_5
    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->isPlay:I

    if-ne v3, v7, :cond_6

    .line 354
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->rcVideoPath:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->StartPlayVideo(Landroid/graphics/Rect;Ljava/lang/String;)V

    goto :goto_0

    .line 357
    :cond_6
    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->isPlay:I

    if-ne v3, v8, :cond_9

    .line 359
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideo:Landroid/widget/VideoView;

    invoke-virtual {v3}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideo:Landroid/widget/VideoView;

    invoke-virtual {v3}, Landroid/widget/VideoView;->canPause()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 360
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideo:Landroid/widget/VideoView;

    invoke-virtual {v3}, Landroid/widget/VideoView;->pause()V

    goto :goto_0

    .line 362
    :cond_7
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideo:Landroid/widget/VideoView;

    invoke-virtual {v3}, Landroid/widget/VideoView;->canPause()Z

    move-result v3

    if-nez v3, :cond_8

    .line 363
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->removeVideoView()V

    goto :goto_0

    .line 365
    :cond_8
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideo:Landroid/widget/VideoView;

    invoke-virtual {v3}, Landroid/widget/VideoView;->start()V

    goto :goto_0

    .line 372
    :cond_9
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideo:Landroid/widget/VideoView;

    if-eqz v3, :cond_a

    .line 373
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->removeVideoView()V

    .line 376
    :cond_a
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->isUseGLES()Z

    move-result v3

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->IIsWaitingFlag()Z

    move-result v3

    if-nez v3, :cond_c

    :cond_b
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->OnTouchTheScreen()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 377
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$SlideShowGestureProcListener;

    invoke-interface {v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$SlideShowGestureProcListener;->onTouchPlayNext()V

    .line 384
    .end local v0    # "hyperlinkInfo":Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;
    .end local v1    # "mTempPath":Ljava/lang/String;
    .end local v2    # "rcVideo":Landroid/graphics/Rect;
    :cond_c
    :goto_2
    iput v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureMode:I

    goto/16 :goto_0

    .line 380
    :cond_d
    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideShowMode:I

    const/4 v4, 0x7

    if-ne v3, v4, :cond_c

    .line 381
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v3, v7}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    goto :goto_2
.end method

.method public onTouchCancel()V
    .locals 10

    .prologue
    .line 246
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->isPenDownNoUp()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideShowMode:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 248
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragX:[I

    array-length v0, v0

    add-int/lit8 v8, v0, -0x1

    .line 249
    .local v8, "x":I
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragY:[I

    array-length v0, v0

    add-int/lit8 v9, v0, -0x1

    .line 250
    .local v9, "y":I
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragTime:[I

    array-length v0, v0

    add-int/lit8 v7, v0, -0x1

    .line 251
    .local v7, "t":I
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenPressure:[I

    array-length v0, v0

    add-int/lit8 v6, v0, -0x1

    .line 253
    .local v6, "p":I
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setEnablePage(Z)V

    .line 254
    const/4 v1, 0x2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragX:[I

    aget v2, v0, v8

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragY:[I

    aget v3, v0, v9

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragTime:[I

    aget v4, v0, v7

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenPressure:[I

    aget v5, v0, v6

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->onHIDAction(IIIII)V

    .line 255
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureMode:I

    .line 257
    .end local v6    # "p":I
    .end local v7    # "t":I
    .end local v8    # "x":I
    .end local v9    # "y":I
    :cond_0
    return-void
.end method

.method public onTouchDown(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 177
    const-string/jumbo v0, "SlideShowGestureProc"

    const-string/jumbo v2, "onTouchDown"

    invoke-static {v0, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->isUseGLES()Z

    move-result v0

    if-eq v0, v6, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideGLImage:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 181
    :cond_0
    const-string/jumbo v0, "SlideShowGestureProc"

    const-string/jumbo v2, "onTouchDown No FreeDrawing "

    invoke-static {v0, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IStopSlideEffect()V

    .line 185
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->IsAutoSwitch()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 186
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getAutoSwitchTimer()Landroid/os/CountDownTimer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 214
    :cond_1
    :goto_0
    return v1

    .line 191
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->isTouchCancel()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 192
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideShowMode:I

    if-ne v0, v4, :cond_3

    .line 193
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setTouchCancel(Z)V

    .line 194
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureMode:I

    if-ne v0, v6, :cond_3

    .line 195
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureMode:I

    .line 198
    :cond_3
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureMode:I

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureMode:I

    if-ne v0, v3, :cond_5

    .line 199
    :cond_4
    iput v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureMode:I

    .line 200
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideShowMode:I

    if-ne v0, v3, :cond_6

    .line 201
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setEnablePage(Z)V

    .line 202
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->onTouch(Landroid/view/MotionEvent;)V

    :cond_5
    :goto_1
    move v1, v6

    .line 214
    goto :goto_0

    .line 204
    :cond_6
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideShowMode:I

    if-eq v0, v4, :cond_7

    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideShowMode:I

    const/4 v2, 0x6

    if-ne v0, v2, :cond_5

    .line 207
    :cond_7
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setEnablePage(Z)V

    .line 208
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v2, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v3, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    long-to-int v4, v4

    const/4 v0, -0x1

    invoke-static {p1, v0}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->onHIDAction(IIIII)V

    .line 210
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v0, v6}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setPenDownNoUp(Z)V

    goto :goto_1
.end method

.method public onTouchMove(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 13
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 261
    const-string/jumbo v0, "SlideShowGestureProc"

    const-string/jumbo v1, "onTouchMove"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 263
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideShowMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 264
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    invoke-virtual {v0, p2}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->onTouch(Landroid/view/MotionEvent;)V

    .line 302
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 266
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideShowMode:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    .line 267
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v5

    .line 268
    .local v5, "N":I
    if-lez v5, :cond_3

    const/16 v0, 0x64

    if-ge v5, v0, :cond_3

    .line 269
    const/4 v12, 0x0

    .local v12, "j":I
    :goto_1
    if-ge v12, v5, :cond_2

    .line 270
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragX:[I

    invoke-virtual {p2, v12}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v1

    float-to-int v1, v1

    aput v1, v0, v12

    .line 271
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragY:[I

    invoke-virtual {p2, v12}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v1

    float-to-int v1, v1

    aput v1, v0, v12

    .line 273
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragTime:[I

    invoke-virtual {p2, v12}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    move-result-wide v1

    long-to-int v1, v1

    aput v1, v0, v12

    .line 274
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenPressure:[I

    invoke-static {p2, v12}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v1

    aput v1, v0, v12

    .line 269
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 277
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragX:[I

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragY:[I

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragTime:[I

    iget-object v4, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenPressure:[I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->onSetPenPosition([I[I[I[II)V

    goto :goto_0

    .line 279
    .end local v12    # "j":I
    :cond_3
    if-nez v5, :cond_4

    .line 280
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragX:[I

    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    aput v2, v0, v1

    .line 281
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragY:[I

    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    aput v2, v0, v1

    .line 283
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragTime:[I

    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    long-to-int v2, v2

    aput v2, v0, v1

    .line 284
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenPressure:[I

    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-static {p2, v2}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v2

    aput v2, v0, v1

    .line 286
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragX:[I

    iget-object v8, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragY:[I

    iget-object v9, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragTime:[I

    iget-object v10, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenPressure:[I

    const/4 v11, 0x1

    move-object v6, p0

    invoke-direct/range {v6 .. v11}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->onSetPenPosition([I[I[I[II)V

    goto :goto_0

    .line 289
    :cond_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragX:[I

    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    aput v2, v0, v1

    .line 290
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragY:[I

    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    aput v2, v0, v1

    .line 292
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragTime:[I

    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    long-to-int v2, v2

    aput v2, v0, v1

    .line 293
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenPressure:[I

    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-static {p2, v2}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v2

    aput v2, v0, v1

    .line 295
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragX:[I

    iget-object v8, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragY:[I

    iget-object v9, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenDragTime:[I

    iget-object v10, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPenPressure:[I

    const/4 v11, 0x1

    move-object v6, p0

    invoke-direct/range {v6 .. v11}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->onSetPenPosition([I[I[I[II)V

    goto/16 :goto_0

    .line 298
    .end local v5    # "N":I
    :cond_5
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideShowMode:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 299
    const/4 v7, 0x1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v8, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v9, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    long-to-int v10, v0

    const/4 v0, -0x1

    invoke-static {p2, v0}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v11

    move-object v6, p0

    invoke-direct/range {v6 .. v11}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->onHIDAction(IIIII)V

    goto/16 :goto_0
.end method

.method public onTouchUp(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x6

    const/4 v3, 0x4

    const/4 v7, 0x0

    const/4 v1, 0x2

    const/4 v6, 0x1

    .line 219
    const-string/jumbo v0, "SlideShowGestureProc"

    const-string/jumbo v2, "onTouchUp"

    invoke-static {v0, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureMode:I

    if-ne v0, v6, :cond_2

    .line 221
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideShowMode:I

    if-eq v0, v3, :cond_0

    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideShowMode:I

    if-eq v0, v4, :cond_0

    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideShowMode:I

    if-ne v0, v1, :cond_1

    .line 223
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v0, v6}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setEnablePage(Z)V

    .line 225
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideShowMode:I

    if-ne v0, v1, :cond_4

    .line 226
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v0, v6}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setEnablePage(Z)V

    .line 227
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->onTouch(Landroid/view/MotionEvent;)V

    .line 238
    :cond_2
    :goto_0
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureMode:I

    if-eq v0, v1, :cond_3

    .line 239
    iput v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureMode:I

    .line 241
    :cond_3
    return v6

    .line 229
    :cond_4
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideShowMode:I

    if-eq v0, v3, :cond_5

    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideShowMode:I

    if-ne v0, v4, :cond_2

    .line 232
    :cond_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v0, v6}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setEnablePage(Z)V

    .line 233
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v2, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v3, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    long-to-int v4, v4

    const/4 v0, -0x1

    invoke-static {p1, v0}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->onHIDAction(IIIII)V

    .line 235
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mActivity:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v0, v7}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setPenDownNoUp(Z)V

    goto :goto_0
.end method

.method public removeVideoView()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/16 v3, 0x8

    .line 495
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->BGMResume()V

    .line 498
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideo:Landroid/widget/VideoView;

    if-nez v1, :cond_0

    .line 513
    :goto_0
    return-void

    .line 501
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideo:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 502
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideo:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->stopPlayback()V

    .line 504
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideo:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 505
    .local v0, "oParams":Landroid/view/ViewGroup$LayoutParams;
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 506
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 508
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideo:Landroid/widget/VideoView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 509
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->m_VideoFrame:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 510
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideobtn:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 511
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mVideo:Landroid/widget/VideoView;

    invoke-virtual {v1, v3}, Landroid/widget/VideoView;->setVisibility(I)V

    .line 512
    const/4 v1, 0x0

    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->isPlay:I

    goto :goto_0
.end method

.method public setSlideShowMode(I)V
    .locals 2
    .param p1, "slideShowMode"    # I

    .prologue
    .line 168
    iput p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideShowMode:I

    .line 169
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mSlideShowMode:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 170
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureDetector:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->SetAlwaysInMoveRegion(Z)V

    .line 173
    :goto_0
    return-void

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->mGestureDetector:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->SetAlwaysInMoveRegion(Z)V

    goto :goto_0
.end method
