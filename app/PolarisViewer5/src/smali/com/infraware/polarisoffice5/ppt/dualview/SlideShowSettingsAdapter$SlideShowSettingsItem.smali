.class public Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$SlideShowSettingsItem;
.super Ljava/lang/Object;
.source "SlideShowSettingsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SlideShowSettingsItem"
.end annotation


# instance fields
.field private mImage:Landroid/graphics/drawable/Drawable;

.field private mText:Ljava/lang/String;

.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter;


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter;Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "image"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/4 v0, 0x0

    .line 26
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$SlideShowSettingsItem;->this$0:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$SlideShowSettingsItem;->mText:Ljava/lang/String;

    .line 24
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$SlideShowSettingsItem;->mImage:Landroid/graphics/drawable/Drawable;

    .line 27
    iput-object p2, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$SlideShowSettingsItem;->mText:Ljava/lang/String;

    .line 28
    iput-object p3, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$SlideShowSettingsItem;->mImage:Landroid/graphics/drawable/Drawable;

    .line 29
    return-void
.end method


# virtual methods
.method public getImage()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$SlideShowSettingsItem;->mImage:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$SlideShowSettingsItem;->mText:Ljava/lang/String;

    return-object v0
.end method
