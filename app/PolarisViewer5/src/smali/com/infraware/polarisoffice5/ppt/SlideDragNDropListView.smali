.class public Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;
.super Landroid/widget/ListView;
.source "SlideDragNDropListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;,
        Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$GestureMode;
    }
.end annotation


# static fields
.field private static END_LONG_PRESS:I

.field private static FASTSPEED:I

.field private static RUN_DRAG:I

.field private static SLOWSPEED:I

.field private static START_DRAG:I

.field private static START_LONG_PRESS:I


# instance fields
.field private final LOG_CAT:Ljava/lang/String;

.field private mAnimation:Landroid/view/animation/AlphaAnimation;

.field private mDragView:Landroid/widget/ImageView;

.field private mEndPosition:I

.field private mGestureMode:I

.field private mHandler:Landroid/os/Handler;

.field private mIndicatorView:Landroid/view/View;

.field private mLocation:[I

.field private mOffsetX:I

.field private mOffsetY:I

.field private mPrevX:I

.field private mPrevY:I

.field private mSelectView:Landroid/view/View;

.field private mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

.field private mStartPosition:I

.field private mStartX:I

.field private mStartY:I

.field private mTouchSlop:I

.field private m_oActivity:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

.field mbSlideDragNDropEnable:Z

.field mbSlideMoveEnable:Z

.field mbSlideSelectEnable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    sput v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->START_LONG_PRESS:I

    .line 56
    const/4 v0, 0x2

    sput v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->END_LONG_PRESS:I

    .line 57
    const/4 v0, 0x3

    sput v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->START_DRAG:I

    .line 58
    const/4 v0, 0x4

    sput v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->RUN_DRAG:I

    .line 59
    const/16 v0, 0x8

    sput v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->SLOWSPEED:I

    .line 60
    const/16 v0, 0x20

    sput v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->FASTSPEED:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 82
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    const-string/jumbo v0, "SlideDragNDropListView"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->LOG_CAT:Ljava/lang/String;

    .line 34
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mbSlideSelectEnable:Z

    .line 35
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mbSlideMoveEnable:Z

    .line 36
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mbSlideDragNDropEnable:Z

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mAnimation:Landroid/view/animation/AlphaAnimation;

    .line 43
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mGestureMode:I

    .line 44
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mStartPosition:I

    .line 45
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mEndPosition:I

    .line 46
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mTouchSlop:I

    .line 47
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mOffsetX:I

    .line 48
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mOffsetY:I

    .line 49
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mPrevX:I

    .line 50
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mPrevY:I

    .line 51
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mStartX:I

    .line 52
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mStartY:I

    .line 53
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mLocation:[I

    .line 147
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$2;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mHandler:Landroid/os/Handler;

    .line 83
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mTouchSlop:I

    .line 84
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x41f55c29    # 30.67f

    invoke-static {v0, v1}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mOffsetX:I

    .line 85
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x415547ae    # 13.33f

    invoke-static {v0, v1}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mOffsetY:I

    .line 87
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const v2, 0x3e99999a    # 0.3f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mAnimation:Landroid/view/animation/AlphaAnimation;

    .line 88
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mAnimation:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 89
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mAnimation:Landroid/view/animation/AlphaAnimation;

    new-instance v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$1;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$1;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 108
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mSelectView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$002(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mSelectView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$100()I
    .locals 1

    .prologue
    .line 27
    sget v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->END_LONG_PRESS:I

    return v0
.end method

.method static synthetic access$1000(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    .prologue
    .line 27
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mPrevX:I

    return v0
.end method

.method static synthetic access$1100(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    .prologue
    .line 27
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mPrevY:I

    return v0
.end method

.method static synthetic access$1200(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;II)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->onRunDrag(II)V

    return-void
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;)Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->m_oActivity:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    return-object v0
.end method

.method static synthetic access$400()I
    .locals 1

    .prologue
    .line 27
    sget v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->START_LONG_PRESS:I

    return v0
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->onStartLongPress()V

    return-void
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->onEndLongPress()V

    return-void
.end method

.method static synthetic access$700()I
    .locals 1

    .prologue
    .line 27
    sget v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->START_DRAG:I

    return v0
.end method

.method static synthetic access$800(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->onStartDrag()V

    return-void
.end method

.method static synthetic access$900()I
    .locals 1

    .prologue
    .line 27
    sget v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->RUN_DRAG:I

    return v0
.end method

.method private moveIndicator(III)V
    .locals 11
    .param p1, "y"    # I
    .param p2, "dragViewY"    # I
    .param p3, "moveY"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v10, -0x1

    const/4 v9, 0x0

    .line 420
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mIndicatorView:Landroid/view/View;

    if-nez v7, :cond_1

    .line 446
    :cond_0
    :goto_0
    return-void

    .line 423
    :cond_1
    const/4 v7, 0x2

    new-array v4, v7, [I

    .line 424
    .local v4, "location":[I
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->getLocationOnScreen([I)V

    .line 425
    aget v7, v4, v8

    sub-int v7, p2, v7

    invoke-virtual {p0, v9, v7}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->pointToPosition(II)I

    move-result v5

    .line 426
    .local v5, "startPosition":I
    aget v7, v4, v8

    sub-int v7, p2, v7

    iget-object v8, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {p0, v9, v7}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->pointToPosition(II)I

    move-result v0

    .line 427
    .local v0, "endPosition":I
    if-ne v5, v10, :cond_2

    if-eq v0, v10, :cond_0

    .line 430
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->getFirstVisiblePosition()I

    move-result v7

    sub-int v7, v5, v7

    invoke-virtual {p0, v7}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 431
    .local v6, "startView":Landroid/view/View;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->getFirstVisiblePosition()I

    move-result v7

    sub-int v7, v0, v7

    invoke-virtual {p0, v7}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 432
    .local v1, "endView":Landroid/view/View;
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mIndicatorView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 433
    .local v2, "indicatorLayoutParams":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout$LayoutParams;

    .line 434
    .local v3, "listLayoutParams":Landroid/widget/FrameLayout$LayoutParams;
    if-nez v6, :cond_4

    .line 435
    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mEndPosition:I

    .line 436
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v7

    iget-object v8, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mIndicatorView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getHeight()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    sub-int/2addr v7, v8

    iget v8, v3, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    add-int/2addr v7, v8

    sub-int/2addr v7, p3

    iput v7, v2, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 442
    :goto_1
    iget v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mStartPosition:I

    iget v8, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mEndPosition:I

    if-ge v7, v8, :cond_3

    .line 443
    iget v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mEndPosition:I

    add-int/lit8 v7, v7, -0x1

    iput v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mEndPosition:I

    .line 444
    :cond_3
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mIndicatorView:Landroid/view/View;

    invoke-virtual {v7, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 445
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mIndicatorView:Landroid/view/View;

    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 439
    :cond_4
    add-int/lit8 v7, v5, 0x1

    iput v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mEndPosition:I

    .line 440
    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result v7

    iget-object v8, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mIndicatorView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getHeight()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    sub-int/2addr v7, v8

    iget v8, v3, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    add-int/2addr v7, v8

    sub-int/2addr v7, p3

    iput v7, v2, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    goto :goto_1
.end method

.method private onCancelLongPress()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 303
    const-string/jumbo v0, "SlideDragNDropListView"

    const-string/jumbo v1, "onCancelLongPress"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mGestureMode:I

    if-ne v0, v2, :cond_1

    .line 305
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mbSlideDragNDropEnable:Z

    if-ne v0, v2, :cond_0

    .line 306
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    const/4 v1, -0x1

    invoke-interface {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;->onEndAnimation(I)V

    .line 315
    :cond_0
    :goto_0
    return-void

    .line 308
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mGestureMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 309
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mGestureMode:I

    .line 310
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mSelectView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mSelectView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 312
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mSelectView:Landroid/view/View;

    goto :goto_0
.end method

.method private onEndLongPress()V
    .locals 3

    .prologue
    .line 289
    const-string/jumbo v1, "SlideDragNDropListView"

    const-string/jumbo v2, "onEndLongPress"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mStartPosition:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 291
    const/4 v0, -0x1

    .line 292
    .local v0, "position":I
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mGestureMode:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 293
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mStartPosition:I

    .line 294
    const/4 v1, 0x3

    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mGestureMode:I

    .line 295
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mHandler:Landroid/os/Handler;

    sget v2, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->START_DRAG:I

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 297
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mbSlideDragNDropEnable:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 298
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    invoke-interface {v1, v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;->onEndAnimation(I)V

    .line 300
    .end local v0    # "position":I
    :cond_1
    return-void
.end method

.method private onPrepareLongPress()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 267
    const-string/jumbo v0, "SlideDragNDropListView"

    const-string/jumbo v1, "onPrepareLongPress"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mGestureMode:I

    if-ne v0, v2, :cond_1

    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mStartPosition:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 269
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mbSlideDragNDropEnable:Z

    if-ne v0, v2, :cond_0

    .line 270
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    invoke-interface {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;->onStartAnimation()V

    .line 271
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mHandler:Landroid/os/Handler;

    sget v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->START_LONG_PRESS:I

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 273
    :cond_1
    return-void
.end method

.method private onRunDrag(II)V
    .locals 12
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 372
    const-string/jumbo v10, "SlideDragNDropListView"

    const-string/jumbo v11, "onRunDrag"

    invoke-static {v10, v11}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    iget-object v10, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mDragView:Landroid/widget/ImageView;

    if-nez v10, :cond_0

    .line 417
    :goto_0
    return-void

    .line 376
    :cond_0
    const/4 v5, 0x1

    .line 377
    .local v5, "scroll":Z
    iget v10, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mGestureMode:I

    const/4 v11, 0x3

    if-ne v10, v11, :cond_1

    .line 378
    const/4 v5, 0x0

    .line 380
    :cond_1
    const/4 v6, 0x0

    .line 381
    .local v6, "speed":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->getHeight()I

    move-result v2

    .line 382
    .local v2, "height":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->getFirstVisiblePosition()I

    move-result v1

    .line 383
    .local v1, "firstVisiblePosition":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->getLastVisiblePosition()I

    move-result v3

    .line 384
    .local v3, "lastVisiblePosition":I
    const/4 v10, 0x0

    invoke-static {p2, v10}, Ljava/lang/Math;->max(II)I

    move-result p2

    .line 385
    const/4 v10, 0x1

    if-ne v5, v10, :cond_3

    .line 386
    iget-object v10, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v10}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v7

    .line 387
    .local v7, "upperBound":I
    iget-object v10, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v10}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v10

    sub-int v4, v2, v10

    .line 388
    .local v4, "lowerBound":I
    if-lt p2, v4, :cond_5

    iget v10, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mPrevY:I

    if-lt p2, v10, :cond_5

    .line 389
    sub-int v10, v3, v1

    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 390
    .local v8, "view":Landroid/view/View;
    if-eqz v8, :cond_3

    .line 391
    add-int v10, v2, v4

    div-int/lit8 v10, v10, 0x2

    if-le p2, v10, :cond_4

    sget v6, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->FASTSPEED:I

    .line 392
    :goto_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->getCount()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    if-ne v3, v10, :cond_2

    .line 393
    invoke-virtual {v8}, Landroid/view/View;->getBottom()I

    move-result v10

    sub-int/2addr v10, v2

    invoke-static {v10, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 394
    :cond_2
    invoke-virtual {v8}, Landroid/view/View;->getTop()I

    move-result v10

    sub-int/2addr v10, v6

    invoke-virtual {p0, v3, v10}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->setSelectionFromTop(II)V

    .line 407
    .end local v4    # "lowerBound":I
    .end local v7    # "upperBound":I
    .end local v8    # "view":Landroid/view/View;
    :cond_3
    :goto_2
    iput p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mPrevX:I

    .line 408
    iput p2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mPrevY:I

    .line 410
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->getContext()Landroid/content/Context;

    move-result-object v10

    const-string/jumbo v11, "window"

    invoke-virtual {v10, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/WindowManager;

    .line 411
    .local v9, "windowManager":Landroid/view/WindowManager;
    iget-object v10, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v10}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager$LayoutParams;

    .line 412
    .local v0, "dragLayoutParams":Landroid/view/WindowManager$LayoutParams;
    iget-object v10, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mLocation:[I

    const/4 v11, 0x0

    aget v10, v10, v11

    iget v11, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mStartX:I

    sub-int v11, p1, v11

    add-int/2addr v10, v11

    iget v11, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mOffsetX:I

    add-int/2addr v10, v11

    iput v10, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 413
    iget-object v10, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mLocation:[I

    const/4 v11, 0x1

    aget v10, v10, v11

    iget v11, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mStartY:I

    sub-int v11, p2, v11

    add-int/2addr v10, v11

    iget v11, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mOffsetY:I

    sub-int/2addr v10, v11

    iput v10, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 414
    iget-object v10, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mDragView:Landroid/widget/ImageView;

    invoke-interface {v9, v10, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 416
    iget v10, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-direct {p0, p2, v10, v6}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->moveIndicator(III)V

    goto/16 :goto_0

    .line 391
    .end local v0    # "dragLayoutParams":Landroid/view/WindowManager$LayoutParams;
    .end local v9    # "windowManager":Landroid/view/WindowManager;
    .restart local v4    # "lowerBound":I
    .restart local v7    # "upperBound":I
    .restart local v8    # "view":Landroid/view/View;
    :cond_4
    sget v6, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->SLOWSPEED:I

    goto :goto_1

    .line 397
    .end local v8    # "view":Landroid/view/View;
    :cond_5
    if-gt p2, v7, :cond_3

    iget v10, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mPrevY:I

    if-gt p2, v10, :cond_3

    .line 398
    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 399
    .restart local v8    # "view":Landroid/view/View;
    if-eqz v8, :cond_3

    .line 400
    div-int/lit8 v10, v7, 0x2

    if-ge p2, v10, :cond_7

    sget v10, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->FASTSPEED:I

    neg-int v6, v10

    .line 401
    :goto_3
    if-nez v1, :cond_6

    .line 402
    invoke-virtual {v8}, Landroid/view/View;->getTop()I

    move-result v10

    invoke-static {v10, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 403
    :cond_6
    invoke-virtual {v8}, Landroid/view/View;->getTop()I

    move-result v10

    sub-int/2addr v10, v6

    invoke-virtual {p0, v1, v10}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->setSelectionFromTop(II)V

    goto :goto_2

    .line 400
    :cond_7
    sget v10, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->SLOWSPEED:I

    neg-int v6, v10

    goto :goto_3
.end method

.method private onStartDrag()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, -0x2

    const/4 v9, 0x0

    .line 318
    const-string/jumbo v7, "SlideDragNDropListView"

    const-string/jumbo v8, "onStartDrag"

    invoke-static {v7, v8}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    iget v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mStartPosition:I

    const/4 v8, -0x1

    if-eq v7, v8, :cond_0

    iget v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mGestureMode:I

    const/4 v8, 0x3

    if-ne v7, v8, :cond_0

    .line 320
    iget v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mStartPosition:I

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->getFirstVisiblePosition()I

    move-result v8

    sub-int v5, v7, v8

    .line 321
    .local v5, "visiblePosition":I
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 322
    .local v3, "item":Landroid/view/View;
    if-nez v3, :cond_1

    .line 355
    .end local v3    # "item":Landroid/view/View;
    .end local v5    # "visiblePosition":I
    :cond_0
    :goto_0
    return-void

    .line 325
    .restart local v3    # "item":Landroid/view/View;
    .restart local v5    # "visiblePosition":I
    :cond_1
    const v7, 0x7f0b01e6

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 326
    .local v4, "itemView":Landroid/view/View;
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mLocation:[I

    invoke-virtual {v4, v7}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 327
    invoke-virtual {v4, v11}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 329
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 330
    .local v1, "context":Landroid/content/Context;
    new-instance v7, Landroid/widget/ImageView;

    invoke-direct {v7, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mDragView:Landroid/widget/ImageView;

    .line 331
    invoke-virtual {v4, v11}, Landroid/view/View;->getDrawingCache(Z)Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-static {v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 332
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 333
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mDragView:Landroid/widget/ImageView;

    const/16 v8, 0x7f

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 334
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v7, v9, v9}, Landroid/widget/ImageView;->measure(II)V

    .line 336
    const-string/jumbo v7, "window"

    invoke-virtual {v1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/WindowManager;

    .line 337
    .local v6, "windowManager":Landroid/view/WindowManager;
    new-instance v2, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v2}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 338
    .local v2, "dragLayoutParams":Landroid/view/WindowManager$LayoutParams;
    const/16 v7, 0x33

    iput v7, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 339
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mLocation:[I

    aget v7, v7, v9

    iget v8, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mOffsetX:I

    add-int/2addr v7, v8

    iput v7, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 340
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mLocation:[I

    aget v7, v7, v11

    iget v8, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mOffsetY:I

    sub-int/2addr v7, v8

    iput v7, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 341
    iput v10, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 342
    iput v10, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 343
    const/16 v7, 0x398

    iput v7, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 348
    iput v10, v2, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 349
    iput v9, v2, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 350
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mDragView:Landroid/widget/ImageView;

    invoke-interface {v6, v7, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 352
    iget v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mStartX:I

    iget v8, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mStartY:I

    invoke-direct {p0, v7, v8}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->onRunDrag(II)V

    .line 353
    const/4 v7, 0x5

    iput v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mGestureMode:I

    goto :goto_0
.end method

.method private onStartLongPress()V
    .locals 4

    .prologue
    .line 276
    const-string/jumbo v2, "SlideDragNDropListView"

    const-string/jumbo v3, "onStartLongPress"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mGestureMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mStartPosition:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 278
    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mStartPosition:I

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->getFirstVisiblePosition()I

    move-result v3

    sub-int v1, v2, v3

    .line 279
    .local v1, "visiblePosition":I
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 280
    .local v0, "item":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 281
    const v2, 0x7f0b01e6

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mSelectView:Landroid/view/View;

    .line 282
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mSelectView:Landroid/view/View;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v2, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 283
    const/4 v2, 0x2

    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mGestureMode:I

    .line 286
    .end local v0    # "item":Landroid/view/View;
    .end local v1    # "visiblePosition":I
    :cond_0
    return-void
.end method

.method private removeAllMessages()V
    .locals 2

    .prologue
    .line 259
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mHandler:Landroid/os/Handler;

    sget v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->START_LONG_PRESS:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 260
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mHandler:Landroid/os/Handler;

    sget v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->END_LONG_PRESS:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 261
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mHandler:Landroid/os/Handler;

    sget v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->START_DRAG:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 262
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mHandler:Landroid/os/Handler;

    sget v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->RUN_DRAG:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 263
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->onCancelLongPress()V

    .line 264
    return-void
.end method


# virtual methods
.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 231
    sparse-switch p1, :sswitch_data_0

    .line 255
    :cond_0
    :goto_0
    :sswitch_0
    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :sswitch_1
    return v0

    .line 233
    :sswitch_2
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->removeAllMessages()V

    .line 234
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mbSlideMoveEnable:Z

    if-ne v1, v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    invoke-interface {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;->onMovePrevious()V

    goto :goto_0

    .line 239
    :sswitch_3
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->removeAllMessages()V

    .line 240
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mbSlideMoveEnable:Z

    if-ne v1, v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    invoke-interface {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;->onMoveNext()V

    goto :goto_0

    .line 245
    :sswitch_4
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->removeAllMessages()V

    goto :goto_0

    .line 231
    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_2
        0x14 -> :sswitch_3
        0x15 -> :sswitch_1
        0x16 -> :sswitch_1
        0x42 -> :sswitch_4
        0x6f -> :sswitch_0
    .end sparse-switch
.end method

.method public onStopDrag()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 358
    const-string/jumbo v1, "SlideDragNDropListView"

    const-string/jumbo v2, "onStopDrag"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    const/4 v1, 0x0

    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mGestureMode:I

    .line 361
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mDragView:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 362
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 363
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 364
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 365
    .local v0, "windowManager":Landroid/view/WindowManager;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mDragView:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 366
    iput-object v4, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mDragView:Landroid/widget/ImageView;

    .line 368
    .end local v0    # "windowManager":Landroid/view/WindowManager;
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mIndicatorView:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 369
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v9, 0x5

    const/4 v8, -0x1

    const/4 v5, 0x1

    .line 170
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v3, v6

    .line 171
    .local v3, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v4, v6

    .line 173
    .local v4, "y":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 207
    :pswitch_0
    const-string/jumbo v6, "SlideDragNDropListView"

    const-string/jumbo v7, "onTouchEvent ACTION_UP"

    invoke-static {v6, v7}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->removeAllMessages()V

    .line 209
    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mGestureMode:I

    if-ne v6, v5, :cond_5

    .line 210
    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    if-eqz v6, :cond_0

    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mStartPosition:I

    if-eq v6, v8, :cond_0

    .line 211
    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mStartPosition:I

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->getFirstVisiblePosition()I

    move-result v7

    sub-int v2, v6, v7

    .line 212
    .local v2, "visiblePosition":I
    iget-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mbSlideSelectEnable:Z

    if-ne v6, v5, :cond_0

    .line 213
    iget-object v5, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    iget v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mStartPosition:I

    invoke-interface {v5, v6, v7}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;->onSelect(Landroid/view/View;I)V

    .line 222
    .end local v2    # "visiblePosition":I
    :cond_0
    :goto_0
    const/4 v5, 0x0

    iput v5, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mGestureMode:I

    .line 225
    :goto_1
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    :cond_1
    :goto_2
    return v5

    .line 175
    :pswitch_1
    const-string/jumbo v6, "SlideDragNDropListView"

    const-string/jumbo v7, "onTouchEvent ACTION_DOWN"

    invoke-static {v6, v7}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->removeAllMessages()V

    .line 177
    iput v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mStartX:I

    .line 178
    iput v4, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mStartY:I

    .line 179
    iput v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mPrevX:I

    .line 180
    iput v4, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mPrevY:I

    .line 181
    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mStartX:I

    iget v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mStartY:I

    invoke-virtual {p0, v6, v7}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->pointToPosition(II)I

    move-result v6

    iput v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mEndPosition:I

    iput v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mStartPosition:I

    .line 182
    iput v5, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mGestureMode:I

    .line 183
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->onPrepareLongPress()V

    goto :goto_1

    .line 186
    :pswitch_2
    const-string/jumbo v6, "SlideDragNDropListView"

    const-string/jumbo v7, "onTouchEvent ACTION_MOVE"

    invoke-static {v6, v7}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mPrevX:I

    sub-int/2addr v6, v3

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 188
    .local v0, "deltaX":I
    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mPrevY:I

    sub-int/2addr v6, v4

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 189
    .local v1, "deltaY":I
    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mTouchSlop:I

    if-ge v0, v6, :cond_2

    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mTouchSlop:I

    if-lt v1, v6, :cond_1

    .line 192
    :cond_2
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->removeAllMessages()V

    .line 193
    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mGestureMode:I

    const/4 v7, 0x3

    if-ne v6, v7, :cond_3

    .line 194
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->onStartDrag()V

    goto :goto_2

    .line 197
    :cond_3
    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mGestureMode:I

    if-ne v6, v9, :cond_4

    .line 198
    invoke-direct {p0, v3, v4}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->onRunDrag(II)V

    .line 199
    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mHandler:Landroid/os/Handler;

    sget v7, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->RUN_DRAG:I

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_2

    .line 202
    :cond_4
    const/4 v5, 0x4

    iput v5, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mGestureMode:I

    goto :goto_1

    .line 217
    .end local v0    # "deltaX":I
    .end local v1    # "deltaY":I
    :cond_5
    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mGestureMode:I

    if-ne v6, v9, :cond_0

    .line 218
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->onStopDrag()V

    .line 219
    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    if-eqz v6, :cond_0

    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mStartPosition:I

    if-eq v6, v8, :cond_0

    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mEndPosition:I

    if-eq v6, v8, :cond_0

    iget-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mbSlideDragNDropEnable:Z

    if-ne v6, v5, :cond_0

    .line 220
    iget-object v5, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mStartPosition:I

    iget v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mEndPosition:I

    invoke-interface {v5, v6, v7}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;->onDrop(II)V

    goto/16 :goto_0

    .line 173
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method setDragNDropEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 127
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mbSlideDragNDropEnable:Z

    .line 128
    return-void
.end method

.method public setParent(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 0
    .param p1, "a_oActivity"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 111
    check-cast p1, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .end local p1    # "a_oActivity":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->m_oActivity:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .line 112
    return-void
.end method

.method public setSelectedItem(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 136
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->setSelectionFromTop(II)V

    .line 145
    return-void
.end method

.method public setSlideDragNDropListener(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;)V
    .locals 0
    .param p1, "l"    # Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    .line 116
    return-void
.end method

.method public setSlideIndicatorView(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mIndicatorView:Landroid/view/View;

    .line 132
    return-void
.end method

.method setSlideMoveEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 119
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mbSlideMoveEnable:Z

    .line 120
    return-void
.end method

.method setSlideSelectEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 123
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mbSlideSelectEnable:Z

    .line 124
    return-void
.end method
