.class Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$4;
.super Landroid/os/Handler;
.source "PPTMainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V
    .locals 0

    .prologue
    .line 540
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$4;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 543
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$4;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->isFinishing()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 558
    :cond_0
    :goto_0
    return-void

    .line 545
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 547
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$4;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # invokes: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->setSlideNoteString()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$600(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V

    .line 548
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$4;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # invokes: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getSlideNoteString()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$700(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V

    .line 549
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$4;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # invokes: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->decideSlideNoteEditable()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$800(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V

    goto :goto_0

    .line 552
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$4;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$900(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Landroid/widget/PopupWindow;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 553
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$4;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$900(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    goto :goto_0

    .line 545
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
