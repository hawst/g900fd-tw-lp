.class public Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;
.super Landroid/os/Handler;
.source "PPTMainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ThumbnailHandler"
.end annotation


# static fields
.field public static final MSG_ADD_THUMBNAIL:I = 0x0

.field public static final MSG_ADD_THUMBNAIL_DELAYED:I = 0x1

.field public static final MSG_UPDATE_SLIDE_LIST:I = 0x2

.field public static final mAddDelay:I = 0x12c

.field public static final mUpdateDelay:I = 0x3e8


# instance fields
.field private mbDrawAllThumbnail:Z

.field private mbMakingThumbnail:Z

.field private mbUpdateThumbnail:Z

.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 440
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 447
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->mbDrawAllThumbnail:Z

    .line 448
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->mbUpdateThumbnail:Z

    .line 450
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->mbMakingThumbnail:Z

    return-void
.end method


# virtual methods
.method public getDrawAllThumbnail()Z
    .locals 1

    .prologue
    .line 453
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->mbDrawAllThumbnail:Z

    return v0
.end method

.method public getUpdateThumbnail()Z
    .locals 1

    .prologue
    .line 456
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->mbUpdateThumbnail:Z

    return v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    .line 464
    iget v0, p1, Landroid/os/Message;->what:I

    if-eqz v0, :cond_1

    iget v0, p1, Landroid/os/Message;->what:I

    if-eq v0, v1, :cond_1

    iget v0, p1, Landroid/os/Message;->what:I

    if-eq v0, v2, :cond_1

    .line 512
    :cond_0
    :goto_0
    return-void

    .line 467
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->isFinishing()Z

    move-result v0

    if-eq v0, v2, :cond_0

    .line 470
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->mbMakingThumbnail:Z

    if-eq v0, v2, :cond_0

    .line 475
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->canRequestThumbnail()Z

    move-result v0

    if-nez v0, :cond_3

    .line 476
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v7

    .line 477
    .local v7, "nextTryMsg":Landroid/os/Message;
    iget v0, p1, Landroid/os/Message;->what:I

    iput v0, v7, Landroid/os/Message;->what:I

    .line 479
    const/16 v6, 0x12c

    .line 480
    .local v6, "nDelayed":I
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v1, :cond_2

    .line 481
    const/16 v6, 0x3e8

    .line 484
    :cond_2
    int-to-long v0, v6

    invoke-virtual {p0, v7, v0, v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 489
    .end local v6    # "nDelayed":I
    .end local v7    # "nextTryMsg":Landroid/os/Message;
    :cond_3
    const/4 v4, 0x0

    .line 490
    .local v4, "pageNum":I
    iget v0, p1, Landroid/os/Message;->what:I

    if-eqz v0, :cond_4

    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v1, :cond_5

    .line 491
    :cond_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getVisibleSlideImage()I

    move-result v4

    .line 494
    :cond_5
    iget v0, p1, Landroid/os/Message;->what:I

    if-eq v0, v2, :cond_6

    if-nez v4, :cond_7

    .line 495
    :cond_6
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getInvisibleSlideImage()I

    move-result v4

    .line 498
    :cond_7
    if-nez v4, :cond_8

    .line 499
    const-string/jumbo v0, "ThumbnailRequester"

    const-string/jumbo v1, "mbDrawAllThumbnail == true,  mbUpdateThumbnail == true"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->mbDrawAllThumbnail:Z

    .line 501
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->mbUpdateThumbnail:Z

    goto :goto_0

    .line 506
    :cond_8
    if-eqz v4, :cond_0

    .line 507
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->mbMakingThumbnail:Z

    .line 509
    const-string/jumbo v0, "ThumbnailRequester"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "ISlideObjStartEx() Call, page num:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideImageWidth:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$400(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)I

    move-result v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideImageHeight:I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$500(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)I

    move-result v2

    const v3, 0x3ecccccd    # 0.4f

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvInterface;->ISlideObjStartEx(IIFIZ)V

    goto/16 :goto_0
.end method

.method public removeAllMessage()V
    .locals 1

    .prologue
    .line 531
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->removeMessages(I)V

    .line 532
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->removeMessages(I)V

    .line 533
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->removeMessages(I)V

    .line 534
    return-void
.end method

.method public request()V
    .locals 3

    .prologue
    .line 515
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->removeAllMessage()V

    .line 517
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 518
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    .line 519
    const-wide/16 v1, 0x12c

    invoke-virtual {p0, v0, v1, v2}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 520
    return-void
.end method

.method public request(I)V
    .locals 3
    .param p1, "nDelayed"    # I

    .prologue
    .line 523
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->removeAllMessage()V

    .line 525
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 526
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    .line 527
    int-to-long v1, p1

    invoke-virtual {p0, v0, v1, v2}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 528
    return-void
.end method

.method public setDrawAllThumbnail(Z)V
    .locals 0
    .param p1, "bDrawAll"    # Z

    .prologue
    .line 452
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->mbDrawAllThumbnail:Z

    return-void
.end method

.method public setMakingThumbnail(Z)V
    .locals 0
    .param p1, "bMaking"    # Z

    .prologue
    .line 458
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->mbMakingThumbnail:Z

    return-void
.end method

.method public setUpdateThumbnail(Z)V
    .locals 0
    .param p1, "bUpdate"    # Z

    .prologue
    .line 455
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->mbUpdateThumbnail:Z

    return-void
.end method
