.class Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$1;
.super Landroid/os/Handler;
.source "SlideShowActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V
    .locals 0

    .prologue
    .line 247
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$1;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x0

    .line 250
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 293
    :goto_0
    return-void

    .line 252
    :pswitch_0
    const-string/jumbo v1, "SlideShowActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "handleMessage MSG_SHOW_NORMAL_VIEW m_bChangeScreenForSlideShow ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$1;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bChangeScreenForSlideShow:Z
    invoke-static {v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$000(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$1;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$100(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->setVisibility(I)V

    goto :goto_0

    .line 261
    :pswitch_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$1;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$200(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->setVisibility(I)V

    goto :goto_0

    .line 264
    :pswitch_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$1;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v1, v4}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setViewMode(Z)V

    goto :goto_0

    .line 267
    :pswitch_3
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$1;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setViewMode(Z)V

    goto :goto_0

    .line 270
    :pswitch_4
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$1;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setEnableEraser()V

    goto :goto_0

    .line 274
    :pswitch_5
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$1;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->AutoSwitchTimer:Landroid/os/CountDownTimer;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$300(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/os/CountDownTimer;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/CountDownTimer;->cancel()V

    .line 275
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$1;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->AutoSwitchTimer:Landroid/os/CountDownTimer;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$300(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/os/CountDownTimer;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    goto :goto_0

    .line 279
    :pswitch_6
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$1;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$400(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)I

    move-result v0

    .line 280
    .local v0, "slideShowMode":I
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$1;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # setter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I
    invoke-static {v1, v4}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$402(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;I)I

    .line 281
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$1;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v1, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    goto :goto_0

    .line 284
    .end local v0    # "slideShowMode":I
    :pswitch_7
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$1;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # invokes: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setLastSlideVisibility()V
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$500(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    goto :goto_0

    .line 288
    :pswitch_8
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$1;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$600(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IStopSlideEffect()V

    goto :goto_0

    .line 250
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
