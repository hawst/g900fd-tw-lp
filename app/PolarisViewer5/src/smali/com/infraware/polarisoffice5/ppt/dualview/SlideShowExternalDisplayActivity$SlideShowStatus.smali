.class public Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$SlideShowStatus;
.super Ljava/lang/Object;
.source "SlideShowExternalDisplayActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SlideShowStatus"
.end annotation


# static fields
.field public static final ERASER_STATUS:[I

.field public static final MARKER_OPTION_STATUS:[I

.field public static final MARKER_STATUS:[I

.field public static final POINTER_OPTION_STATUS:[I

.field public static final POINTER_STATUS:[I

.field public static final SELECT_STATUS:[I

.field public static final SLIDE_STATUS:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x7

    .line 110
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$SlideShowStatus;->SLIDE_STATUS:[I

    .line 111
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$SlideShowStatus;->POINTER_STATUS:[I

    .line 112
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$SlideShowStatus;->POINTER_OPTION_STATUS:[I

    .line 113
    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$SlideShowStatus;->MARKER_STATUS:[I

    .line 114
    new-array v0, v1, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$SlideShowStatus;->MARKER_OPTION_STATUS:[I

    .line 115
    new-array v0, v1, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$SlideShowStatus;->ERASER_STATUS:[I

    .line 116
    new-array v0, v1, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$SlideShowStatus;->SELECT_STATUS:[I

    return-void

    .line 110
    nop

    :array_0
    .array-data 4
        0x0
        0x8
        0x8
        0x8
        0x8
        0x8
        0x8
    .end array-data

    .line 111
    :array_1
    .array-data 4
        0x0
        0x0
        0x8
        0x0
        0x8
        0x8
        0x8
    .end array-data

    .line 112
    :array_2
    .array-data 4
        0x0
        0x0
        0x8
        0x0
        0x8
        0x0
        0x8
    .end array-data

    .line 113
    :array_3
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x8
        0x8
        0x8
    .end array-data

    .line 114
    :array_4
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x8
        0x8
        0x0
    .end array-data

    .line 115
    :array_5
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x8
        0x8
        0x8
    .end array-data

    .line 116
    :array_6
    .array-data 4
        0x8
        0x8
        0x8
        0x8
        0x0
        0x8
        0x8
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
