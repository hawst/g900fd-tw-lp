.class public Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;
.super Landroid/os/Handler;
.source "SlideShowActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "HandlerLoop"
.end annotation


# instance fields
.field isDie:Z

.field mLastProgress:I

.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V
    .locals 1

    .prologue
    .line 297
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 298
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;->mLastProgress:I

    .line 299
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;->isDie:Z

    return-void
.end method


# virtual methods
.method public isFinish()Z
    .locals 2

    .prologue
    .line 313
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;->mLastProgress:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$700(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getMax()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public killHandlerLoop()V
    .locals 1

    .prologue
    .line 310
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;->isDie:Z

    .line 311
    return-void
.end method

.method public startHandlerLoop()V
    .locals 3

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;->isDie:Z

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 303
    const/4 v0, 0x6

    const-wide/16 v1, 0x190

    invoke-virtual {p0, v0, v1, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;->sendEmptyMessageDelayed(IJ)Z

    .line 305
    :cond_0
    return-void
.end method

.method public stopHandlerLoop()V
    .locals 1

    .prologue
    .line 307
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;->removeMessages(I)V

    .line 308
    return-void
.end method
