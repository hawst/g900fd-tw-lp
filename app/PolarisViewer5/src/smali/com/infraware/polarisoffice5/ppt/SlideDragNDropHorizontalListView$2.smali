.class Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView$2;
.super Landroid/os/Handler;
.source "SlideDragNDropHorizontalListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 163
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->m_oActivity:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->access$300(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;)Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getIsEncryptDoc()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->m_oActivity:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->access$300(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;)Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->IsViewerMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 165
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->START_LONG_PRESS:I
    invoke-static {}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->access$400()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 166
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    # invokes: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->onStartLongPress()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->access$500(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;)V

    goto :goto_0

    .line 168
    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->END_LONG_PRESS:I
    invoke-static {}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->access$100()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 169
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    # invokes: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->onEndLongPress()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->access$600(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;)V

    goto :goto_0

    .line 171
    :cond_3
    iget v0, p1, Landroid/os/Message;->what:I

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->START_DRAG:I
    invoke-static {}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->access$700()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 172
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    # invokes: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->onStartDrag()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->access$800(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;)V

    goto :goto_0

    .line 174
    :cond_4
    iget v0, p1, Landroid/os/Message;->what:I

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->RUN_DRAG:I
    invoke-static {}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->access$900()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 175
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mPrevX:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->access$1000(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;)I

    move-result v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mPrevY:I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->access$1100(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;)I

    move-result v2

    # invokes: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->onRunDrag(II)V
    invoke-static {v0, v1, v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->access$1200(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;II)V

    .line 176
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->access$200(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;)Landroid/os/Handler;

    move-result-object v0

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->RUN_DRAG:I
    invoke-static {}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->access$900()I

    move-result v1

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
