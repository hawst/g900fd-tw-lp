.class public Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;
.super Lcom/infraware/polarisoffice5/ppt/HorizontalListView;
.source "SlideDragNDropHorizontalListView.java"


# static fields
.field private static END_LONG_PRESS:I

.field private static FASTSPEED:I

.field private static RUN_DRAG:I

.field private static SLOWSPEED:I

.field private static START_DRAG:I

.field private static START_LONG_PRESS:I


# instance fields
.field private final LOG_CAT:Ljava/lang/String;

.field private mAnimation:Landroid/view/animation/AlphaAnimation;

.field private mDragView:Landroid/widget/ImageView;

.field private mEndPosition:I

.field private mGestureMode:I

.field private mHandler:Landroid/os/Handler;

.field private mIndicatorView:Landroid/view/View;

.field private mLocation:[I

.field private mOffsetX:I

.field private mOffsetY:I

.field private mPrevX:I

.field private mPrevY:I

.field private mSelectView:Landroid/view/View;

.field private mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

.field private mStartPosition:I

.field private mStartX:I

.field private mStartY:I

.field private mTouchSlop:I

.field private m_oActivity:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

.field mbSlideDragNDropEnable:Z

.field mbSlideMoveEnable:Z

.field mbSlideSelectEnable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x1

    sput v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->START_LONG_PRESS:I

    .line 60
    const/4 v0, 0x2

    sput v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->END_LONG_PRESS:I

    .line 61
    const/4 v0, 0x3

    sput v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->START_DRAG:I

    .line 62
    const/4 v0, 0x4

    sput v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->RUN_DRAG:I

    .line 63
    const/16 v0, 0x10

    sput v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->SLOWSPEED:I

    .line 64
    const/16 v0, 0x30

    sput v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->FASTSPEED:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 68
    invoke-direct {p0, p1, p2}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    const-string/jumbo v0, "SlideDragNDropListView"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->LOG_CAT:Ljava/lang/String;

    .line 37
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mbSlideSelectEnable:Z

    .line 38
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mbSlideMoveEnable:Z

    .line 39
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mbSlideDragNDropEnable:Z

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mAnimation:Landroid/view/animation/AlphaAnimation;

    .line 47
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mGestureMode:I

    .line 48
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mStartPosition:I

    .line 49
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mEndPosition:I

    .line 50
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mTouchSlop:I

    .line 51
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mOffsetX:I

    .line 52
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mOffsetY:I

    .line 53
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mPrevX:I

    .line 54
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mPrevY:I

    .line 55
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mStartX:I

    .line 56
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mStartY:I

    .line 57
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mLocation:[I

    .line 160
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView$2;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mHandler:Landroid/os/Handler;

    .line 70
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mTouchSlop:I

    .line 71
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x41f55c29    # 30.67f

    invoke-static {v0, v1}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mOffsetX:I

    .line 72
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x415547ae    # 13.33f

    invoke-static {v0, v1}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mOffsetY:I

    .line 74
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const v2, 0x3e99999a    # 0.3f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mAnimation:Landroid/view/animation/AlphaAnimation;

    .line 75
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mAnimation:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 76
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mAnimation:Landroid/view/animation/AlphaAnimation;

    new-instance v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView$1;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView$1;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 96
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mSelectView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$002(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mSelectView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$100()I
    .locals 1

    .prologue
    .line 29
    sget v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->END_LONG_PRESS:I

    return v0
.end method

.method static synthetic access$1000(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    .prologue
    .line 29
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mPrevX:I

    return v0
.end method

.method static synthetic access$1100(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    .prologue
    .line 29
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mPrevY:I

    return v0
.end method

.method static synthetic access$1200(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;II)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->onRunDrag(II)V

    return-void
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;)Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->m_oActivity:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    return-object v0
.end method

.method static synthetic access$400()I
    .locals 1

    .prologue
    .line 29
    sget v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->START_LONG_PRESS:I

    return v0
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->onStartLongPress()V

    return-void
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->onEndLongPress()V

    return-void
.end method

.method static synthetic access$700()I
    .locals 1

    .prologue
    .line 29
    sget v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->START_DRAG:I

    return v0
.end method

.method static synthetic access$800(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->onStartDrag()V

    return-void
.end method

.method static synthetic access$900()I
    .locals 1

    .prologue
    .line 29
    sget v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->RUN_DRAG:I

    return v0
.end method

.method private moveIndicator(III)V
    .locals 11
    .param p1, "x"    # I
    .param p2, "dragViewX"    # I
    .param p3, "moveX"    # I

    .prologue
    const/4 v10, -0x1

    const/4 v9, 0x0

    .line 433
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mIndicatorView:Landroid/view/View;

    if-nez v7, :cond_1

    .line 468
    :cond_0
    :goto_0
    return-void

    .line 436
    :cond_1
    const/4 v7, 0x2

    new-array v4, v7, [I

    .line 437
    .local v4, "location":[I
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getLocationOnScreen([I)V

    .line 439
    aget v7, v4, v9

    sub-int v7, p2, v7

    invoke-virtual {p0, v7, v9}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->pointToPosition(II)I

    move-result v5

    .line 440
    .local v5, "startPosition":I
    aget v7, v4, v9

    sub-int v7, p2, v7

    iget-object v8, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {p0, v7, v9}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->pointToPosition(II)I

    move-result v0

    .line 442
    .local v0, "endPosition":I
    if-ne v5, v10, :cond_2

    if-eq v0, v10, :cond_0

    .line 445
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getFirstVisiblePosition()I

    move-result v7

    sub-int v7, v5, v7

    invoke-virtual {p0, v7}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 446
    .local v6, "startView":Landroid/view/View;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getFirstVisiblePosition()I

    move-result v7

    sub-int v7, v0, v7

    invoke-virtual {p0, v7}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 448
    .local v1, "endView":Landroid/view/View;
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mIndicatorView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 449
    .local v2, "indicatorLayoutParams":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout$LayoutParams;

    .line 450
    .local v3, "listLayoutParams":Landroid/widget/FrameLayout$LayoutParams;
    if-nez v6, :cond_4

    .line 452
    if-eqz v1, :cond_0

    .line 456
    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mEndPosition:I

    .line 457
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v7

    iget-object v8, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mIndicatorView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    sub-int/2addr v7, v8

    iget v8, v3, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    add-int/2addr v7, v8

    sub-int/2addr v7, p3

    iput v7, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 463
    :goto_1
    iget v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mStartPosition:I

    iget v8, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mEndPosition:I

    if-ge v7, v8, :cond_3

    .line 464
    iget v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mEndPosition:I

    add-int/lit8 v7, v7, -0x1

    iput v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mEndPosition:I

    .line 466
    :cond_3
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mIndicatorView:Landroid/view/View;

    invoke-virtual {v7, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 467
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mIndicatorView:Landroid/view/View;

    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 460
    :cond_4
    add-int/lit8 v7, v5, 0x1

    iput v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mEndPosition:I

    .line 461
    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    move-result v7

    iget-object v8, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mIndicatorView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    sub-int/2addr v7, v8

    iget v8, v3, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    add-int/2addr v7, v8

    sub-int/2addr v7, p3

    iput v7, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    goto :goto_1
.end method

.method private onCancelLongPress()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 316
    const-string/jumbo v0, "SlideDragNDropListView"

    const-string/jumbo v1, "onCancelLongPress"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mGestureMode:I

    if-ne v0, v2, :cond_1

    .line 318
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mbSlideDragNDropEnable:Z

    if-ne v0, v2, :cond_0

    .line 319
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    const/4 v1, -0x1

    invoke-interface {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;->onEndAnimation(I)V

    .line 328
    :cond_0
    :goto_0
    return-void

    .line 321
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mGestureMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 322
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mGestureMode:I

    .line 323
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mSelectView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mSelectView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 325
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mSelectView:Landroid/view/View;

    goto :goto_0
.end method

.method private onEndLongPress()V
    .locals 3

    .prologue
    .line 302
    const-string/jumbo v1, "SlideDragNDropListView"

    const-string/jumbo v2, "onEndLongPress"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mStartPosition:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 304
    const/4 v0, -0x1

    .line 305
    .local v0, "position":I
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mGestureMode:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 306
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mStartPosition:I

    .line 307
    const/4 v1, 0x3

    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mGestureMode:I

    .line 308
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mHandler:Landroid/os/Handler;

    sget v2, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->START_DRAG:I

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 310
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mbSlideDragNDropEnable:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 311
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    invoke-interface {v1, v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;->onEndAnimation(I)V

    .line 313
    .end local v0    # "position":I
    :cond_1
    return-void
.end method

.method private onPrepareLongPress()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 280
    const-string/jumbo v0, "SlideDragNDropListView"

    const-string/jumbo v1, "onPrepareLongPress"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mGestureMode:I

    if-ne v0, v2, :cond_1

    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mStartPosition:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 282
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mbSlideDragNDropEnable:Z

    if-ne v0, v2, :cond_0

    .line 283
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    invoke-interface {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;->onStartAnimation()V

    .line 284
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mHandler:Landroid/os/Handler;

    sget v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->START_LONG_PRESS:I

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 286
    :cond_1
    return-void
.end method

.method private onRunDrag(II)V
    .locals 12
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 385
    const-string/jumbo v10, "SlideDragNDropListView"

    const-string/jumbo v11, "onRunDrag"

    invoke-static {v10, v11}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    iget-object v10, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mDragView:Landroid/widget/ImageView;

    if-nez v10, :cond_0

    .line 430
    :goto_0
    return-void

    .line 389
    :cond_0
    const/4 v5, 0x1

    .line 390
    .local v5, "scroll":Z
    iget v10, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mGestureMode:I

    const/4 v11, 0x3

    if-ne v10, v11, :cond_1

    .line 391
    const/4 v5, 0x0

    .line 393
    :cond_1
    const/4 v6, 0x0

    .line 394
    .local v6, "speed":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getWidth()I

    move-result v8

    .line 395
    .local v8, "width":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getFirstVisiblePosition()I

    move-result v1

    .line 396
    .local v1, "firstVisiblePosition":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getLastVisiblePosition()I

    move-result v2

    .line 397
    .local v2, "lastVisiblePosition":I
    const/4 v10, 0x0

    invoke-static {p1, v10}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 398
    const/4 v10, 0x1

    if-ne v5, v10, :cond_3

    .line 399
    iget-object v10, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v10}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v3

    .line 400
    .local v3, "leftBound":I
    iget-object v10, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v10}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v10

    sub-int v4, v8, v10

    .line 401
    .local v4, "rightBound":I
    if-lt p1, v4, :cond_5

    iget v10, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mPrevX:I

    if-lt p1, v10, :cond_5

    .line 402
    sub-int v10, v2, v1

    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 403
    .local v7, "view":Landroid/view/View;
    if-eqz v7, :cond_3

    .line 404
    add-int v10, v8, v4

    div-int/lit8 v10, v10, 0x2

    if-le p1, v10, :cond_4

    sget v6, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->FASTSPEED:I

    .line 405
    :goto_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getCount()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    if-ne v2, v10, :cond_2

    .line 406
    invoke-virtual {v7}, Landroid/view/View;->getRight()I

    move-result v10

    sub-int/2addr v10, v8

    invoke-static {v10, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 407
    :cond_2
    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->onScroll(I)V

    .line 420
    .end local v3    # "leftBound":I
    .end local v4    # "rightBound":I
    .end local v7    # "view":Landroid/view/View;
    :cond_3
    :goto_2
    iput p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mPrevX:I

    .line 421
    iput p2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mPrevY:I

    .line 423
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v10

    const-string/jumbo v11, "window"

    invoke-virtual {v10, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/WindowManager;

    .line 424
    .local v9, "windowManager":Landroid/view/WindowManager;
    iget-object v10, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v10}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager$LayoutParams;

    .line 425
    .local v0, "dragLayoutParams":Landroid/view/WindowManager$LayoutParams;
    iget-object v10, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mLocation:[I

    const/4 v11, 0x0

    aget v10, v10, v11

    iget v11, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mStartX:I

    sub-int v11, p1, v11

    add-int/2addr v10, v11

    iget v11, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mOffsetX:I

    add-int/2addr v10, v11

    iput v10, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 426
    iget-object v10, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mLocation:[I

    const/4 v11, 0x1

    aget v10, v10, v11

    iget v11, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mStartY:I

    sub-int v11, p2, v11

    add-int/2addr v10, v11

    iget v11, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mOffsetY:I

    sub-int/2addr v10, v11

    iput v10, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 427
    iget-object v10, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mDragView:Landroid/widget/ImageView;

    invoke-interface {v9, v10, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 429
    iget v10, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    invoke-direct {p0, p1, v10, v6}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->moveIndicator(III)V

    goto/16 :goto_0

    .line 404
    .end local v0    # "dragLayoutParams":Landroid/view/WindowManager$LayoutParams;
    .end local v9    # "windowManager":Landroid/view/WindowManager;
    .restart local v3    # "leftBound":I
    .restart local v4    # "rightBound":I
    .restart local v7    # "view":Landroid/view/View;
    :cond_4
    sget v6, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->SLOWSPEED:I

    goto :goto_1

    .line 410
    .end local v7    # "view":Landroid/view/View;
    :cond_5
    if-gt p1, v3, :cond_3

    iget v10, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mPrevX:I

    if-gt p1, v10, :cond_3

    .line 411
    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 412
    .restart local v7    # "view":Landroid/view/View;
    if-eqz v7, :cond_3

    .line 413
    div-int/lit8 v10, v3, 0x2

    add-int/lit8 v10, v10, -0x32

    if-ge p1, v10, :cond_7

    sget v10, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->FASTSPEED:I

    neg-int v6, v10

    .line 414
    :goto_3
    if-nez v1, :cond_6

    .line 415
    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    move-result v10

    invoke-static {v10, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 416
    :cond_6
    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->onScroll(I)V

    goto :goto_2

    .line 413
    :cond_7
    sget v10, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->SLOWSPEED:I

    neg-int v6, v10

    goto :goto_3
.end method

.method private onStartDrag()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, -0x2

    const/4 v9, 0x0

    .line 331
    const-string/jumbo v7, "SlideDragNDropListView"

    const-string/jumbo v8, "onStartDrag"

    invoke-static {v7, v8}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    iget v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mStartPosition:I

    const/4 v8, -0x1

    if-eq v7, v8, :cond_0

    iget v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mGestureMode:I

    const/4 v8, 0x3

    if-ne v7, v8, :cond_0

    .line 333
    iget v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mStartPosition:I

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getFirstVisiblePosition()I

    move-result v8

    sub-int v5, v7, v8

    .line 334
    .local v5, "visiblePosition":I
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 335
    .local v3, "item":Landroid/view/View;
    if-nez v3, :cond_1

    .line 368
    .end local v3    # "item":Landroid/view/View;
    .end local v5    # "visiblePosition":I
    :cond_0
    :goto_0
    return-void

    .line 338
    .restart local v3    # "item":Landroid/view/View;
    .restart local v5    # "visiblePosition":I
    :cond_1
    const v7, 0x7f0b01e6

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 339
    .local v4, "itemView":Landroid/view/View;
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mLocation:[I

    invoke-virtual {v4, v7}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 340
    invoke-virtual {v4, v11}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 342
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 343
    .local v1, "context":Landroid/content/Context;
    new-instance v7, Landroid/widget/ImageView;

    invoke-direct {v7, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mDragView:Landroid/widget/ImageView;

    .line 344
    invoke-virtual {v4, v11}, Landroid/view/View;->getDrawingCache(Z)Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-static {v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 345
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 346
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mDragView:Landroid/widget/ImageView;

    const/16 v8, 0x7f

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 347
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v7, v9, v9}, Landroid/widget/ImageView;->measure(II)V

    .line 349
    const-string/jumbo v7, "window"

    invoke-virtual {v1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/WindowManager;

    .line 350
    .local v6, "windowManager":Landroid/view/WindowManager;
    new-instance v2, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v2}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 351
    .local v2, "dragLayoutParams":Landroid/view/WindowManager$LayoutParams;
    const/16 v7, 0x33

    iput v7, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 352
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mLocation:[I

    aget v7, v7, v9

    iget v8, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mOffsetX:I

    add-int/2addr v7, v8

    iput v7, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 353
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mLocation:[I

    aget v7, v7, v11

    iget v8, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mOffsetY:I

    sub-int/2addr v7, v8

    iput v7, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 354
    iput v10, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 355
    iput v10, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 356
    const/16 v7, 0x398

    iput v7, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 361
    iput v10, v2, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 362
    iput v9, v2, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 363
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mDragView:Landroid/widget/ImageView;

    invoke-interface {v6, v7, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 365
    iget v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mStartX:I

    iget v8, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mStartY:I

    invoke-direct {p0, v7, v8}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->onRunDrag(II)V

    .line 366
    const/4 v7, 0x5

    iput v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mGestureMode:I

    goto :goto_0
.end method

.method private onStartLongPress()V
    .locals 4

    .prologue
    .line 289
    const-string/jumbo v2, "SlideDragNDropListView"

    const-string/jumbo v3, "onStartLongPress"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mGestureMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mStartPosition:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 291
    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mStartPosition:I

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getFirstVisiblePosition()I

    move-result v3

    sub-int v1, v2, v3

    .line 292
    .local v1, "visiblePosition":I
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 293
    .local v0, "item":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 294
    const v2, 0x7f0b01e6

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mSelectView:Landroid/view/View;

    .line 295
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mSelectView:Landroid/view/View;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v2, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 296
    const/4 v2, 0x2

    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mGestureMode:I

    .line 299
    .end local v0    # "item":Landroid/view/View;
    .end local v1    # "visiblePosition":I
    :cond_0
    return-void
.end method

.method private removeAllMessages()V
    .locals 2

    .prologue
    .line 272
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mHandler:Landroid/os/Handler;

    sget v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->START_LONG_PRESS:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 273
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mHandler:Landroid/os/Handler;

    sget v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->END_LONG_PRESS:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 274
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mHandler:Landroid/os/Handler;

    sget v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->START_DRAG:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 275
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mHandler:Landroid/os/Handler;

    sget v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->RUN_DRAG:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 276
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->onCancelLongPress()V

    .line 277
    return-void
.end method


# virtual methods
.method public finalizeSlideDragNDropHorizontalListView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 98
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_CHECK_MEMORYLEAK()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 99
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mAnimation:Landroid/view/animation/AlphaAnimation;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 101
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mAnimation:Landroid/view/animation/AlphaAnimation;

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mDragView:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 105
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mDragView:Landroid/widget/ImageView;

    .line 107
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_2

    .line 108
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->removeAllMessages()V

    .line 109
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mHandler:Landroid/os/Handler;

    .line 112
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mLocation:[I

    if-eqz v0, :cond_3

    .line 113
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mLocation:[I

    .line 115
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    if-eqz v0, :cond_4

    .line 116
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    .line 118
    :cond_4
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 244
    sparse-switch p1, :sswitch_data_0

    .line 268
    :cond_0
    :goto_0
    :sswitch_0
    invoke-super {p0, p1, p2}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :sswitch_1
    return v0

    .line 246
    :sswitch_2
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->removeAllMessages()V

    .line 247
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mbSlideMoveEnable:Z

    if-ne v1, v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    invoke-interface {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;->onMovePrevious()V

    goto :goto_0

    .line 252
    :sswitch_3
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->removeAllMessages()V

    .line 253
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mbSlideMoveEnable:Z

    if-ne v1, v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    invoke-interface {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;->onMoveNext()V

    goto :goto_0

    .line 258
    :sswitch_4
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->removeAllMessages()V

    goto :goto_0

    .line 244
    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_2
        0x14 -> :sswitch_3
        0x15 -> :sswitch_1
        0x16 -> :sswitch_1
        0x42 -> :sswitch_4
        0x6f -> :sswitch_0
    .end sparse-switch
.end method

.method public onStopDrag()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 371
    const-string/jumbo v1, "SlideDragNDropListView"

    const-string/jumbo v2, "onStopDrag"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    const/4 v1, 0x0

    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mGestureMode:I

    .line 374
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mDragView:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 375
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 376
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 377
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 378
    .local v0, "windowManager":Landroid/view/WindowManager;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mDragView:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 379
    iput-object v4, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mDragView:Landroid/widget/ImageView;

    .line 381
    .end local v0    # "windowManager":Landroid/view/WindowManager;
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mIndicatorView:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 382
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v9, 0x5

    const/4 v8, -0x1

    const/4 v5, 0x1

    .line 183
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v3, v6

    .line 184
    .local v3, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v4, v6

    .line 186
    .local v4, "y":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 220
    :pswitch_0
    const-string/jumbo v6, "SlideDragNDropListView"

    const-string/jumbo v7, "onTouchEvent ACTION_UP"

    invoke-static {v6, v7}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->removeAllMessages()V

    .line 222
    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mGestureMode:I

    if-ne v6, v5, :cond_5

    .line 223
    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    if-eqz v6, :cond_0

    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mStartPosition:I

    if-eq v6, v8, :cond_0

    .line 224
    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mStartPosition:I

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getFirstVisiblePosition()I

    move-result v7

    sub-int v2, v6, v7

    .line 225
    .local v2, "visiblePosition":I
    iget-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mbSlideMoveEnable:Z

    if-ne v6, v5, :cond_0

    .line 226
    iget-object v5, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    iget v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mStartPosition:I

    invoke-interface {v5, v6, v7}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;->onSelect(Landroid/view/View;I)V

    .line 235
    .end local v2    # "visiblePosition":I
    :cond_0
    :goto_0
    const/4 v5, 0x0

    iput v5, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mGestureMode:I

    .line 238
    :goto_1
    invoke-super {p0, p1}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    :cond_1
    :goto_2
    return v5

    .line 188
    :pswitch_1
    const-string/jumbo v6, "SlideDragNDropListView"

    const-string/jumbo v7, "onTouchEvent ACTION_DOWN"

    invoke-static {v6, v7}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->removeAllMessages()V

    .line 190
    iput v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mStartX:I

    .line 191
    iput v4, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mStartY:I

    .line 192
    iput v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mPrevX:I

    .line 193
    iput v4, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mPrevY:I

    .line 194
    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mStartX:I

    iget v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mStartY:I

    invoke-virtual {p0, v6, v7}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->pointToPosition(II)I

    move-result v6

    iput v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mEndPosition:I

    iput v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mStartPosition:I

    .line 195
    iput v5, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mGestureMode:I

    .line 196
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->onPrepareLongPress()V

    goto :goto_1

    .line 199
    :pswitch_2
    const-string/jumbo v6, "SlideDragNDropListView"

    const-string/jumbo v7, "onTouchEvent ACTION_MOVE"

    invoke-static {v6, v7}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mPrevX:I

    sub-int/2addr v6, v3

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 201
    .local v0, "deltaX":I
    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mPrevY:I

    sub-int/2addr v6, v4

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 202
    .local v1, "deltaY":I
    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mTouchSlop:I

    if-ge v0, v6, :cond_2

    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mTouchSlop:I

    if-lt v1, v6, :cond_1

    .line 205
    :cond_2
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->removeAllMessages()V

    .line 206
    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mGestureMode:I

    const/4 v7, 0x3

    if-ne v6, v7, :cond_3

    .line 207
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->onStartDrag()V

    goto :goto_2

    .line 210
    :cond_3
    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mGestureMode:I

    if-ne v6, v9, :cond_4

    .line 211
    invoke-direct {p0, v3, v4}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->onRunDrag(II)V

    .line 212
    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mHandler:Landroid/os/Handler;

    sget v7, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->RUN_DRAG:I

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_2

    .line 215
    :cond_4
    const/4 v5, 0x4

    iput v5, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mGestureMode:I

    goto :goto_1

    .line 230
    .end local v0    # "deltaX":I
    .end local v1    # "deltaY":I
    :cond_5
    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mGestureMode:I

    if-ne v6, v9, :cond_0

    .line 231
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->onStopDrag()V

    .line 232
    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    if-eqz v6, :cond_0

    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mStartPosition:I

    if-eq v6, v8, :cond_0

    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mEndPosition:I

    if-eq v6, v8, :cond_0

    iget-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mbSlideDragNDropEnable:Z

    if-ne v6, v5, :cond_0

    .line 233
    iget-object v5, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mStartPosition:I

    iget v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mEndPosition:I

    invoke-interface {v5, v6, v7}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;->onDrop(II)V

    goto/16 :goto_0

    .line 186
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method setDragNDropEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 137
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mbSlideDragNDropEnable:Z

    .line 138
    return-void
.end method

.method public setParent(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 0
    .param p1, "a_oActivity"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 121
    check-cast p1, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .end local p1    # "a_oActivity":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->m_oActivity:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .line 122
    return-void
.end method

.method public setSelectedItem(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 145
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getFirstVisiblePosition()I

    move-result v1

    if-gt p1, v1, :cond_0

    .line 146
    invoke-virtual {p0, p1, v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->setSelectionFromLeft(II)V

    .line 158
    :goto_0
    return-void

    .line 148
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getLastVisiblePosition()I

    move-result v1

    if-eq v1, v3, :cond_1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getLastVisiblePosition()I

    move-result v1

    if-lt p1, v1, :cond_1

    .line 149
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 150
    .local v0, "view":Landroid/view/View;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->setSelectionFromLeft(II)V

    goto :goto_0

    .line 152
    .end local v0    # "view":Landroid/view/View;
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getFirstVisiblePosition()I

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->getLastVisiblePosition()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 153
    invoke-virtual {p0, p1, v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->setSelectionFromLeft(II)V

    goto :goto_0

    .line 156
    :cond_2
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->setSelection(I)V

    goto :goto_0
.end method

.method public setSlideDragNDropListener(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;)V
    .locals 0
    .param p1, "l"    # Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    .line 126
    return-void
.end method

.method public setSlideIndicatorView(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mIndicatorView:Landroid/view/View;

    .line 142
    return-void
.end method

.method setSlideMoveEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 129
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mbSlideMoveEnable:Z

    .line 130
    return-void
.end method

.method setSlideSelectEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 133
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->mbSlideSelectEnable:Z

    .line 134
    return-void
.end method
