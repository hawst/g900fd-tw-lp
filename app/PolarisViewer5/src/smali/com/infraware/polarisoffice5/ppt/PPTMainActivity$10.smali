.class Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;
.super Ljava/lang/Object;
.source "PPTMainActivity.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V
    .locals 0

    .prologue
    .line 1800
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrop(II)V
    .locals 5
    .param p1, "from"    # I
    .param p2, "to"    # I

    .prologue
    const/4 v4, 0x7

    .line 1824
    const-string/jumbo v1, "PPTMainActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "SlideDragNDropListener.onDrop from:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", to:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1825
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    move-result-object v1

    instance-of v1, v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    if-eqz v1, :cond_3

    .line 1826
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->onDrop(II)V

    .line 1827
    if-ge p2, p1, :cond_1

    .line 1828
    add-int/lit8 v0, p1, 0x1

    .line 1829
    .local v0, "i":I
    add-int/lit8 v0, p1, 0x1

    :goto_0
    add-int/lit8 v1, p2, 0x1

    if-le v0, v1, :cond_3

    .line 1830
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v1, :cond_0

    .line 1831
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x4

    invoke-virtual {v1, v2, v0, v4}, Lcom/infraware/office/evengine/EvInterface;->ISlideAddMove(III)V

    .line 1829
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1833
    .end local v0    # "i":I
    :cond_1
    if-le p2, p1, :cond_3

    .line 1834
    add-int/lit8 v0, p1, 0x1

    .line 1835
    .restart local v0    # "i":I
    add-int/lit8 v0, p1, 0x1

    :goto_1
    add-int/lit8 v1, p2, 0x1

    if-ge v0, v1, :cond_3

    .line 1836
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v1, :cond_2

    .line 1837
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, v0, v4}, Lcom/infraware/office/evengine/EvInterface;->ISlideAddMove(III)V

    .line 1835
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1841
    .end local v0    # "i":I
    :cond_3
    return-void
.end method

.method public onEndAnimation(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1807
    const-string/jumbo v0, "PPTMainActivity"

    const-string/jumbo v1, "SlideDragNDropListener.onEndAnimation"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1808
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    move-result-object v0

    instance-of v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    if-eqz v0, :cond_0

    .line 1809
    const/4 v0, -0x1

    if-eq p1, v0, :cond_1

    .line 1810
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    move-result-object v0

    invoke-virtual {v0, p1, v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->setSelectedPosition(IZ)V

    .line 1811
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    .line 1812
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x6

    add-int/lit8 v2, p1, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/infraware/office/evengine/EvInterface;->IMovePage(II)V

    .line 1819
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # setter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbStartAnimation:Z
    invoke-static {v0, v3}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2002(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;Z)Z

    .line 1820
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mThumbnailRequester:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;

    invoke-virtual {v0, v3}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->setUpdateThumbnail(Z)V

    .line 1821
    return-void

    .line 1814
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mThumbnailRequester:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->getUpdateThumbnail()Z

    move-result v0

    if-ne v0, v2, :cond_0

    .line 1815
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->notifyDataSetChanged()V

    .line 1816
    const-string/jumbo v0, "PPTMainActivity"

    const-string/jumbo v1, "notifyDataSetChanged"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onMoveNext()V
    .locals 3

    .prologue
    .line 1861
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    move-result-object v0

    instance-of v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    if-eqz v0, :cond_0

    .line 1862
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getSelectedPosition()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 1864
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getSelectedPosition()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->setSelectedPosition(IZ)V

    .line 1865
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    .line 1866
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getSelectedPosition()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/infraware/office/evengine/EvInterface;->IMovePage(II)V

    .line 1869
    :cond_0
    return-void
.end method

.method public onMovePrevious()V
    .locals 3

    .prologue
    .line 1872
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    move-result-object v0

    instance-of v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    if-eqz v0, :cond_0

    .line 1873
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getSelectedPosition()I

    move-result v0

    if-lez v0, :cond_0

    .line 1875
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getSelectedPosition()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->setSelectedPosition(IZ)V

    .line 1876
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    .line 1877
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getSelectedPosition()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/infraware/office/evengine/EvInterface;->IMovePage(II)V

    .line 1880
    :cond_0
    return-void
.end method

.method public onSelect(Landroid/view/View;I)V
    .locals 3
    .param p1, "itemView"    # Landroid/view/View;
    .param p2, "index"    # I

    .prologue
    .line 1844
    const-string/jumbo v0, "PPTMainActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "SlideDragNDropListener.onSelect index:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1845
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    move-result-object v0

    instance-of v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    if-eqz v0, :cond_0

    .line 1846
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getSelectedPosition()I

    move-result v0

    if-ne v0, p2, :cond_1

    .line 1858
    :cond_0
    :goto_0
    return-void

    .line 1852
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->playSoundEffect(I)V

    .line 1853
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p2, v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->setSelectedPosition(IZ)V

    .line 1854
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    .line 1855
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x6

    add-int/lit8 v2, p2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/infraware/office/evengine/EvInterface;->IMovePage(II)V

    goto :goto_0
.end method

.method public onStartAnimation()V
    .locals 2

    .prologue
    .line 1802
    const-string/jumbo v0, "PPTMainActivity"

    const-string/jumbo v1, "SlideDragNDropListener.onStartAnimation"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1803
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    const/4 v1, 0x1

    # setter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbStartAnimation:Z
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2002(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;Z)Z

    .line 1804
    return-void
.end method
