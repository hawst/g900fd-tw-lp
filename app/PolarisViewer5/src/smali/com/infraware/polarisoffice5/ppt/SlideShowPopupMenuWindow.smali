.class public Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;
.super Landroid/widget/PopupWindow;
.source "SlideShowPopupMenuWindow.java"


# static fields
.field static final POPUPMENU_AROW_UNDERCENTER:I = 0x5

.field static final POPUPMENU_AROW_UNDERLEFT:I = 0x4

.field static final POPUPMENU_AROW_UNDERRIGHT:I = 0x6

.field static final POPUPMENU_AROW_UPCENTER:I = 0x2

.field static final POPUPMENU_AROW_UPLEFT:I = 0x1

.field static final POPUPMENU_AROW_UPRIGHT:I = 0x3

.field static final POPUPMENU_ARROW_GAB:I = 0x21

.field static final POPUPMENU_MAX_ITEM_SIZE:I = 0xa

.field public static final POPUPMENU_MSG_ENDTIMER:I = 0x2

.field public static final POPUPMENU_MSG_NONE:I = 0x0

.field public static final POPUPMENU_MSG_STARTTIMER:I = 0x1

.field static final POPUPMENU_SHOWING_TIME:I = 0x9c4

.field static final POPUPMENU_TYPE_CARET:I = 0x1

.field static final POPUPMENU_TYPE_GENERAL:I = 0x0

.field static final POPUPMENU_TYPE_ROTATE:I = 0x2

.field static final POPUPMENU_VERTICAL_GAP:I = 0x19

.field static final POPUPMENU_WAIT_TIME:I = 0x12c

.field public static final POPUP_CMDID_RUNHYPERLINK:I = 0x1


# instance fields
.field hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

.field mActivity:Landroid/app/Activity;

.field mClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

.field mCmdArray:[I

.field mDocType:I

.field mHandler:Landroid/os/Handler;

.field mInterface:Lcom/infraware/office/evengine/EvInterface;

.field mItemSize:I

.field mLinearLayout:Landroid/widget/LinearLayout;

.field mParentRect:Landroid/graphics/Rect;

.field mParentView:Landroid/view/View;

.field mPopupType:I

.field mRootLinear:Landroid/widget/LinearLayout;

.field mScrollView:Landroid/widget/HorizontalScrollView;

.field mSelectRect:Landroid/graphics/Rect;

.field mShowRunnable:Ljava/lang/Runnable;

.field mWaitRunnable:Ljava/lang/Runnable;

.field positionHeight:I

.field positionWidth:I

.field positionX:I

.field positionY:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;)V
    .locals 6
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "parentView"    # Landroid/view/View;

    .prologue
    const/4 v5, -0x2

    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 97
    invoke-direct {p0, p1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    .line 62
    iput-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mActivity:Landroid/app/Activity;

    .line 63
    iput-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    .line 64
    iput-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 66
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v2, v2, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    .line 67
    iput-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mParentView:Landroid/view/View;

    .line 68
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v2, v2, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mParentRect:Landroid/graphics/Rect;

    .line 70
    iput-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    .line 71
    iput-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mScrollView:Landroid/widget/HorizontalScrollView;

    .line 72
    iput-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mRootLinear:Landroid/widget/LinearLayout;

    .line 74
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mItemSize:I

    .line 75
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mPopupType:I

    .line 77
    const/16 v0, 0xa

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mCmdArray:[I

    .line 83
    iput-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mHandler:Landroid/os/Handler;

    .line 84
    iput-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mShowRunnable:Ljava/lang/Runnable;

    .line 85
    iput-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mWaitRunnable:Ljava/lang/Runnable;

    .line 87
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mDocType:I

    .line 89
    iput-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    .line 91
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->positionX:I

    .line 92
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->positionY:I

    .line 93
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->positionWidth:I

    .line 94
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->positionHeight:I

    .line 100
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mHandler:Landroid/os/Handler;

    .line 102
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mActivity:Landroid/app/Activity;

    .line 104
    iput-object p2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mParentView:Landroid/view/View;

    .line 106
    new-instance v0, Landroid/widget/HorizontalScrollView;

    invoke-direct {v0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mScrollView:Landroid/widget/HorizontalScrollView;

    .line 107
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mScrollView:Landroid/widget/HorizontalScrollView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 110
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    .line 111
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 112
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 115
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 117
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mScrollView:Landroid/widget/HorizontalScrollView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->addView(Landroid/view/View;)V

    .line 119
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mRootLinear:Landroid/widget/LinearLayout;

    .line 120
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mRootLinear:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 123
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mRootLinear:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mScrollView:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 125
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mRootLinear:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->setContentView(Landroid/view/View;)V

    .line 127
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mScrollView:Landroid/widget/HorizontalScrollView;

    const v1, 0x7f0201a6

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setBackgroundResource(I)V

    .line 129
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 131
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mRootLinear:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow$1;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow$1;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 143
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->setOutsideTouchable(Z)V

    .line 144
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->setFocusable(Z)V

    .line 146
    return-void
.end method

.method private AddButton(I)V
    .locals 2
    .param p1, "cmd_id"    # I

    .prologue
    .line 639
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mItemSize:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    .line 648
    :cond_0
    :goto_0
    return-void

    .line 642
    :cond_1
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->isExistCmd(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 645
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mCmdArray:[I

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mItemSize:I

    aput p1, v0, v1

    .line 647
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mItemSize:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mItemSize:I

    goto :goto_0
.end method

.method private UpdateTimer()V
    .locals 4

    .prologue
    .line 604
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mShowRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 605
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mShowRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 606
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mShowRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x9c4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 608
    :cond_0
    return-void
.end method

.method private deleteButton(I)V
    .locals 5
    .param p1, "cmd"    # I

    .prologue
    .line 259
    const/4 v0, -0x1

    .line 260
    .local v0, "idx":I
    const/4 v1, 0x0

    .local v1, "k":I
    :goto_0
    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mItemSize:I

    if-ge v1, v2, :cond_0

    .line 262
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mCmdArray:[I

    aget v2, v2, v1

    if-ne v2, p1, :cond_1

    .line 264
    move v0, v1

    .line 269
    :cond_0
    const/4 v2, -0x1

    if-eq v0, v2, :cond_3

    .line 271
    move v1, v0

    :goto_1
    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mItemSize:I

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_2

    .line 273
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mCmdArray:[I

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mCmdArray:[I

    add-int/lit8 v4, v1, 0x1

    aget v3, v3, v4

    aput v3, v2, v1

    .line 271
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 260
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 276
    :cond_2
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mCmdArray:[I

    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mItemSize:I

    add-int/lit8 v3, v3, -0x1

    const/4 v4, 0x0

    aput v4, v2, v3

    .line 277
    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mItemSize:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mItemSize:I

    .line 279
    :cond_3
    return-void
.end method

.method private doAction()V
    .locals 49

    .prologue
    .line 284
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mItemSize:I

    move/from16 v45, v0

    if-nez v45, :cond_1

    .line 600
    :cond_0
    :goto_0
    return-void

    .line 287
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mParentView:Landroid/view/View;

    move-object/from16 v45, v0

    invoke-virtual/range {v45 .. v45}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v11

    .line 289
    .local v11, "context":Landroid/content/Context;
    const/high16 v45, 0x3f800000    # 1.0f

    move/from16 v0, v45

    invoke-static {v11, v0}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v14

    .line 292
    .local v14, "dip_len":F
    const/16 v16, 0x0

    .line 294
    .local v16, "i":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    invoke-virtual/range {v45 .. v45}, Landroid/graphics/Rect;->width()I

    move-result v28

    .line 295
    .local v28, "pw":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    invoke-virtual/range {v45 .. v45}, Landroid/graphics/Rect;->height()I

    move-result v25

    .line 296
    .local v25, "ph":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v24, v0

    .line 297
    .local v24, "pTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v23, v0

    .line 299
    .local v23, "pBottom":I
    if-nez v28, :cond_2

    if-eqz v25, :cond_0

    .line 303
    :cond_2
    const/high16 v45, 0x40e00000    # 7.0f

    mul-float v45, v45, v14

    move/from16 v0, v45

    float-to-int v13, v0

    .line 304
    .local v13, "def_padding":I
    const/high16 v45, 0x42890000    # 68.5f

    mul-float v45, v45, v14

    move/from16 v0, v45

    float-to-int v12, v0

    .line 305
    .local v12, "def_minwidth":I
    const/high16 v45, 0x3f000000    # 0.5f

    mul-float v45, v45, v14

    move/from16 v0, v45

    float-to-int v0, v0

    move/from16 v19, v0

    .line 308
    .local v19, "line_width":I
    new-instance v22, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow$3;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow$3;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;)V

    .line 319
    .local v22, "ocl":Landroid/view/View$OnClickListener;
    const/16 v16, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mItemSize:I

    move/from16 v45, v0

    move/from16 v0, v16

    move/from16 v1, v45

    if-ge v0, v1, :cond_4

    .line 321
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mCmdArray:[I

    move-object/from16 v45, v0

    aget v45, v45, v16

    move-object/from16 v0, p0

    move/from16 v1, v45

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->getResString(I)Ljava/lang/String;

    move-result-object v33

    .line 323
    .local v33, "str":Ljava/lang/String;
    if-lez v16, :cond_3

    .line 324
    new-instance v17, Landroid/view/View;

    move-object/from16 v0, v17

    invoke-direct {v0, v11}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 325
    .local v17, "iv":Landroid/view/View;
    new-instance v45, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v46, -0x1

    move-object/from16 v0, v45

    move/from16 v1, v19

    move/from16 v2, v46

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    move-object/from16 v0, v17

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 326
    const v45, -0xc8c3bc

    move-object/from16 v0, v17

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 328
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 331
    .end local v17    # "iv":Landroid/view/View;
    :cond_3
    new-instance v9, Landroid/widget/TextView;

    invoke-direct {v9, v11}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 334
    .local v9, "b":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mCmdArray:[I

    move-object/from16 v45, v0

    aget v45, v45, v16

    move/from16 v0, v45

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setId(I)V

    .line 335
    move-object/from16 v0, v33

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 336
    const/16 v45, 0x2

    const/high16 v46, 0x41600000    # 14.0f

    move/from16 v0, v45

    move/from16 v1, v46

    invoke-virtual {v9, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 337
    const/16 v45, -0x1

    move/from16 v0, v45

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 338
    const/16 v45, 0x1

    move/from16 v0, v45

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setClickable(Z)V

    .line 340
    const v45, 0x7f020010

    move/from16 v0, v45

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 342
    const/16 v45, 0x0

    const/16 v46, 0x0

    move/from16 v0, v45

    move/from16 v1, v46

    invoke-virtual {v9, v13, v0, v13, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 343
    invoke-virtual {v9, v12}, Landroid/widget/TextView;->setMinWidth(I)V

    .line 345
    new-instance v45, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v46, -0x2

    const/high16 v47, 0x421c0000    # 39.0f

    mul-float v47, v47, v14

    move/from16 v0, v47

    float-to-int v0, v0

    move/from16 v47, v0

    invoke-direct/range {v45 .. v47}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    move-object/from16 v0, v45

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 347
    const/16 v45, 0x11

    move/from16 v0, v45

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 349
    const/16 v45, 0x1

    move/from16 v0, v45

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setClickable(Z)V

    .line 350
    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 352
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 319
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_1

    .line 355
    .end local v9    # "b":Landroid/widget/TextView;
    .end local v33    # "str":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    move-object/from16 v45, v0

    const/16 v46, 0x0

    const/16 v47, 0x0

    invoke-virtual/range {v45 .. v47}, Landroid/widget/LinearLayout;->measure(II)V

    .line 357
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    move-object/from16 v45, v0

    invoke-virtual/range {v45 .. v45}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v21

    .line 358
    .local v21, "linear_width":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    move-object/from16 v45, v0

    invoke-virtual/range {v45 .. v45}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v20

    .line 360
    .local v20, "linear_height":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mScrollView:Landroid/widget/HorizontalScrollView;

    move-object/from16 v45, v0

    invoke-virtual/range {v45 .. v45}, Landroid/widget/HorizontalScrollView;->getPaddingLeft()I

    move-result v45

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mScrollView:Landroid/widget/HorizontalScrollView;

    move-object/from16 v46, v0

    invoke-virtual/range {v46 .. v46}, Landroid/widget/HorizontalScrollView;->getPaddingRight()I

    move-result v46

    add-int v31, v45, v46

    .line 362
    .local v31, "scroll_padding":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mScrollView:Landroid/widget/HorizontalScrollView;

    move-object/from16 v45, v0

    invoke-virtual/range {v45 .. v45}, Landroid/widget/HorizontalScrollView;->getPaddingTop()I

    move-result v45

    add-int v45, v45, v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mScrollView:Landroid/widget/HorizontalScrollView;

    move-object/from16 v46, v0

    invoke-virtual/range {v46 .. v46}, Landroid/widget/HorizontalScrollView;->getPaddingBottom()I

    move-result v46

    add-int v26, v45, v46

    .line 365
    .local v26, "popup_height":I
    add-int v27, v21, v31

    .line 367
    .local v27, "popup_width":I
    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->setHeight(I)V

    .line 368
    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->setWidth(I)V

    .line 370
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v45, v0

    move/from16 v0, v45

    move/from16 v1, v24

    if-ge v0, v1, :cond_5

    .line 371
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move/from16 v0, v24

    move-object/from16 v1, v45

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 372
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v45, v0

    if-gez v45, :cond_6

    .line 373
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    const/16 v46, 0x0

    move/from16 v0, v46

    move-object/from16 v1, v45

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 375
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v45, v0

    move/from16 v0, v45

    move/from16 v1, v23

    if-le v0, v1, :cond_7

    .line 376
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move/from16 v0, v23

    move-object/from16 v1, v45

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 377
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v45, v0

    move/from16 v0, v45

    move/from16 v1, v28

    if-le v0, v1, :cond_8

    .line 378
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move/from16 v0, v28

    move-object/from16 v1, v45

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 380
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->getWidth()I

    move-result v36

    .line 381
    .local v36, "w":I
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->getHeight()I

    move-result v15

    .line 382
    .local v15, "h":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v46, v0

    move-object/from16 v0, v46

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v46, v0

    sub-int v32, v45, v46

    .line 385
    .local v32, "sh":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v46, v0

    move-object/from16 v0, v46

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v46, v0

    add-int v45, v45, v46

    div-int/lit8 v45, v45, 0x2

    div-int/lit8 v46, v36, 0x2

    sub-int v37, v45, v46

    .line 389
    .local v37, "x":I
    if-gez v37, :cond_10

    .line 390
    const/16 v37, 0x0

    .line 394
    :cond_9
    :goto_2
    add-int/lit8 v18, v37, 0x21

    .line 395
    .local v18, "left_pos":I
    div-int/lit8 v45, v36, 0x2

    add-int v10, v37, v45

    .line 396
    .local v10, "center_pos":I
    add-int v45, v37, v36

    add-int/lit8 v29, v45, -0x21

    .line 398
    .local v29, "right_pos":I
    const/high16 v39, -0x40800000    # -1.0f

    .local v39, "xx_l":F
    const/high16 v38, -0x40800000    # -1.0f

    .local v38, "xx_c":F
    const/high16 v40, -0x40800000    # -1.0f

    .line 400
    .local v40, "xx_r":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v45, v0

    move/from16 v0, v18

    move/from16 v1, v45

    if-le v0, v1, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v45, v0

    move/from16 v0, v18

    move/from16 v1, v45

    if-ge v0, v1, :cond_a

    .line 401
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v45, v0

    sub-int v45, v18, v45

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v46, v0

    move-object/from16 v0, v46

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v46, v0

    sub-int v46, v46, v18

    move/from16 v0, v45

    move/from16 v1, v46

    if-le v0, v1, :cond_11

    .line 402
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v45, v0

    sub-int v45, v18, v45

    move/from16 v0, v45

    int-to-float v0, v0

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v46, v0

    move-object/from16 v0, v46

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v46, v0

    sub-int v46, v46, v18

    move/from16 v0, v46

    int-to-float v0, v0

    move/from16 v46, v0

    div-float v39, v45, v46

    .line 409
    :cond_a
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v45, v0

    move/from16 v0, v45

    if-le v10, v0, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v45, v0

    move/from16 v0, v45

    if-ge v10, v0, :cond_b

    .line 410
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v45, v0

    sub-int v45, v10, v45

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v46, v0

    move-object/from16 v0, v46

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v46, v0

    sub-int v46, v46, v10

    move/from16 v0, v45

    move/from16 v1, v46

    if-le v0, v1, :cond_12

    .line 411
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v45, v0

    sub-int v45, v10, v45

    move/from16 v0, v45

    int-to-float v0, v0

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v46, v0

    move-object/from16 v0, v46

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v46, v0

    sub-int v46, v46, v10

    move/from16 v0, v46

    int-to-float v0, v0

    move/from16 v46, v0

    div-float v38, v45, v46

    .line 418
    :cond_b
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v45, v0

    move/from16 v0, v29

    move/from16 v1, v45

    if-le v0, v1, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v45, v0

    move/from16 v0, v29

    move/from16 v1, v45

    if-ge v0, v1, :cond_c

    .line 419
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v45, v0

    sub-int v45, v29, v45

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v46, v0

    move-object/from16 v0, v46

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v46, v0

    sub-int v46, v46, v29

    move/from16 v0, v45

    move/from16 v1, v46

    if-le v0, v1, :cond_13

    .line 420
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v45, v0

    sub-int v45, v29, v45

    move/from16 v0, v45

    int-to-float v0, v0

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v46, v0

    move-object/from16 v0, v46

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v46, v0

    sub-int v46, v46, v29

    move/from16 v0, v46

    int-to-float v0, v0

    move/from16 v46, v0

    div-float v40, v45, v46

    .line 427
    :cond_c
    :goto_5
    const/16 v34, 0x0

    .line 429
    .local v34, "type":I
    const/16 v45, 0x0

    cmpl-float v45, v39, v45

    if-lez v45, :cond_1c

    .line 430
    const/16 v45, 0x0

    cmpl-float v45, v38, v45

    if-lez v45, :cond_19

    .line 431
    const/16 v45, 0x0

    cmpl-float v45, v40, v45

    if-lez v45, :cond_17

    .line 432
    cmpg-float v45, v39, v40

    if-gez v45, :cond_15

    .line 433
    cmpg-float v45, v39, v38

    if-gez v45, :cond_14

    .line 434
    const/16 v34, 0x1

    .line 478
    :goto_6
    const/16 v45, -0x1

    move/from16 v0, v34

    move/from16 v1, v45

    if-ne v0, v1, :cond_d

    .line 480
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v46, v0

    move-object/from16 v0, v46

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v46, v0

    add-int v45, v45, v46

    div-int/lit8 v30, v45, 0x2

    .line 482
    .local v30, "s_center":I
    sub-int v45, v18, v30

    invoke-static/range {v45 .. v45}, Ljava/lang/Math;->abs(I)I

    move-result v6

    .line 483
    .local v6, "abs_1":I
    sub-int v45, v10, v30

    invoke-static/range {v45 .. v45}, Ljava/lang/Math;->abs(I)I

    move-result v7

    .line 484
    .local v7, "abs_2":I
    sub-int v45, v29, v30

    invoke-static/range {v45 .. v45}, Ljava/lang/Math;->abs(I)I

    move-result v8

    .line 485
    .local v8, "abs_3":I
    if-ge v6, v7, :cond_22

    .line 486
    if-ge v6, v8, :cond_21

    .line 487
    const/16 v34, 0x1

    .line 498
    .end local v6    # "abs_1":I
    .end local v7    # "abs_2":I
    .end local v8    # "abs_3":I
    .end local v30    # "s_center":I
    :cond_d
    :goto_7
    const/16 v35, 0x19

    .line 499
    .local v35, "v_gap":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mPopupType:I

    move/from16 v45, v0

    const/16 v46, 0x1

    move/from16 v0, v45

    move/from16 v1, v46

    if-ne v0, v1, :cond_e

    .line 500
    const/16 v35, 0x6

    .line 502
    :cond_e
    const/16 v42, -0x1

    .line 503
    .local v42, "y1":I
    const/16 v43, -0x1

    .line 504
    .local v43, "y2":I
    const/16 v44, -0x1

    .line 506
    .local v44, "y3":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mPopupType:I

    move/from16 v45, v0

    const/16 v46, 0x2

    move/from16 v0, v45

    move/from16 v1, v46

    if-ne v0, v1, :cond_25

    .line 507
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v45, v0

    add-int v44, v45, v35

    .line 509
    move/from16 v0, v32

    int-to-float v0, v0

    move/from16 v45, v0

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v46, v0

    const/high16 v47, 0x40000000    # 2.0f

    mul-float v46, v46, v47

    const/high16 v47, 0x40400000    # 3.0f

    div-float v46, v46, v47

    cmpl-float v45, v45, v46

    if-lez v45, :cond_24

    .line 510
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v45, v0

    div-int/lit8 v46, v32, 0x2

    add-int v45, v45, v46

    div-int/lit8 v46, v15, 0x2

    sub-int v43, v45, v46

    .line 511
    const/16 v44, -0x1

    .line 551
    :cond_f
    :goto_8
    const/16 v41, 0x0

    .line 552
    .local v41, "y":I
    const/4 v5, 0x2

    .line 553
    .local v5, "Pos_Type":I
    const/16 v45, -0x1

    move/from16 v0, v43

    move/from16 v1, v45

    if-eq v0, v1, :cond_2a

    .line 555
    move/from16 v41, v43

    .line 556
    const/16 v45, 0x1

    move/from16 v0, v34

    move/from16 v1, v45

    if-ne v0, v1, :cond_28

    .line 557
    const/4 v5, 0x1

    .line 582
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mParentView:Landroid/view/View;

    move-object/from16 v45, v0

    const/16 v46, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v45

    move/from16 v2, v46

    move/from16 v3, v37

    move/from16 v4, v41

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->showAtLocation(Landroid/view/View;III)V

    .line 584
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mShowRunnable:Ljava/lang/Runnable;

    move-object/from16 v45, v0

    if-nez v45, :cond_30

    .line 586
    new-instance v45, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow$4;

    move-object/from16 v0, v45

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow$4;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;)V

    move-object/from16 v0, v45

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mShowRunnable:Ljava/lang/Runnable;

    .line 593
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mHandler:Landroid/os/Handler;

    move-object/from16 v45, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mShowRunnable:Ljava/lang/Runnable;

    move-object/from16 v46, v0

    const-wide/16 v47, 0x9c4

    invoke-virtual/range {v45 .. v48}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 391
    .end local v5    # "Pos_Type":I
    .end local v10    # "center_pos":I
    .end local v18    # "left_pos":I
    .end local v29    # "right_pos":I
    .end local v34    # "type":I
    .end local v35    # "v_gap":I
    .end local v38    # "xx_c":F
    .end local v39    # "xx_l":F
    .end local v40    # "xx_r":F
    .end local v41    # "y":I
    .end local v42    # "y1":I
    .end local v43    # "y2":I
    .end local v44    # "y3":I
    :cond_10
    add-int v45, v37, v36

    move/from16 v0, v45

    move/from16 v1, v28

    if-le v0, v1, :cond_9

    .line 392
    sub-int v37, v28, v36

    goto/16 :goto_2

    .line 405
    .restart local v10    # "center_pos":I
    .restart local v18    # "left_pos":I
    .restart local v29    # "right_pos":I
    .restart local v38    # "xx_c":F
    .restart local v39    # "xx_l":F
    .restart local v40    # "xx_r":F
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v45, v0

    sub-int v45, v45, v18

    move/from16 v0, v45

    int-to-float v0, v0

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v46, v0

    move-object/from16 v0, v46

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v46, v0

    sub-int v46, v18, v46

    move/from16 v0, v46

    int-to-float v0, v0

    move/from16 v46, v0

    div-float v39, v45, v46

    goto/16 :goto_3

    .line 414
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v45, v0

    sub-int v45, v45, v10

    move/from16 v0, v45

    int-to-float v0, v0

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v46, v0

    move-object/from16 v0, v46

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v46, v0

    sub-int v46, v10, v46

    move/from16 v0, v46

    int-to-float v0, v0

    move/from16 v46, v0

    div-float v38, v45, v46

    goto/16 :goto_4

    .line 423
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v45, v0

    sub-int v45, v45, v29

    move/from16 v0, v45

    int-to-float v0, v0

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v46, v0

    move-object/from16 v0, v46

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v46, v0

    sub-int v46, v29, v46

    move/from16 v0, v46

    int-to-float v0, v0

    move/from16 v46, v0

    div-float v40, v45, v46

    goto/16 :goto_5

    .line 436
    .restart local v34    # "type":I
    :cond_14
    const/16 v34, 0x2

    goto/16 :goto_6

    .line 438
    :cond_15
    cmpg-float v45, v40, v38

    if-gez v45, :cond_16

    .line 439
    const/16 v34, 0x3

    goto/16 :goto_6

    .line 441
    :cond_16
    const/16 v34, 0x2

    goto/16 :goto_6

    .line 444
    :cond_17
    cmpg-float v45, v39, v38

    if-gez v45, :cond_18

    .line 445
    const/16 v34, 0x1

    goto/16 :goto_6

    .line 447
    :cond_18
    const/16 v34, 0x2

    goto/16 :goto_6

    .line 450
    :cond_19
    const/16 v45, 0x0

    cmpl-float v45, v40, v45

    if-lez v45, :cond_1b

    .line 451
    cmpg-float v45, v39, v40

    if-gez v45, :cond_1a

    .line 452
    const/16 v34, 0x1

    goto/16 :goto_6

    .line 454
    :cond_1a
    const/16 v34, 0x3

    goto/16 :goto_6

    .line 456
    :cond_1b
    const/16 v34, 0x1

    goto/16 :goto_6

    .line 460
    :cond_1c
    const/16 v45, 0x0

    cmpl-float v45, v38, v45

    if-lez v45, :cond_1f

    .line 461
    const/16 v45, 0x0

    cmpl-float v45, v40, v45

    if-lez v45, :cond_1e

    .line 462
    cmpg-float v45, v40, v38

    if-gez v45, :cond_1d

    .line 463
    const/16 v34, 0x3

    goto/16 :goto_6

    .line 465
    :cond_1d
    const/16 v34, 0x2

    goto/16 :goto_6

    .line 467
    :cond_1e
    const/16 v34, 0x2

    goto/16 :goto_6

    .line 470
    :cond_1f
    const/16 v45, 0x0

    cmpl-float v45, v40, v45

    if-lez v45, :cond_20

    .line 471
    const/16 v34, 0x3

    goto/16 :goto_6

    .line 473
    :cond_20
    const/16 v34, -0x1

    goto/16 :goto_6

    .line 489
    .restart local v6    # "abs_1":I
    .restart local v7    # "abs_2":I
    .restart local v8    # "abs_3":I
    .restart local v30    # "s_center":I
    :cond_21
    const/16 v34, 0x3

    goto/16 :goto_7

    .line 491
    :cond_22
    if-ge v8, v7, :cond_23

    .line 492
    const/16 v34, 0x3

    goto/16 :goto_7

    .line 494
    :cond_23
    const/16 v34, 0x2

    goto/16 :goto_7

    .line 512
    .end local v6    # "abs_1":I
    .end local v7    # "abs_2":I
    .end local v8    # "abs_3":I
    .end local v30    # "s_center":I
    .restart local v35    # "v_gap":I
    .restart local v42    # "y1":I
    .restart local v43    # "y2":I
    .restart local v44    # "y3":I
    :cond_24
    add-int v45, v44, v15

    move/from16 v0, v45

    move/from16 v1, v23

    if-le v0, v1, :cond_f

    .line 513
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v45, v0

    div-int/lit8 v46, v32, 0x2

    add-int v45, v45, v46

    div-int/lit8 v46, v15, 0x2

    sub-int v43, v45, v46

    .line 514
    const/16 v44, -0x1

    .line 516
    add-int v45, v43, v15

    move/from16 v0, v45

    move/from16 v1, v23

    if-le v0, v1, :cond_f

    .line 517
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v45, v0

    add-int v43, v45, v35

    .line 519
    add-int v45, v43, v15

    move/from16 v0, v45

    move/from16 v1, v23

    if-le v0, v1, :cond_f

    .line 520
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v45, v0

    sub-int v45, v45, v35

    sub-int v42, v45, v15

    .line 521
    const/16 v43, -0x1

    .line 523
    move/from16 v0, v42

    move/from16 v1, v24

    if-ge v0, v1, :cond_f

    .line 524
    add-int v42, v24, v35

    goto/16 :goto_8

    .line 530
    :cond_25
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v45, v0

    sub-int v45, v45, v35

    sub-int v42, v45, v15

    .line 532
    move/from16 v0, v32

    int-to-float v0, v0

    move/from16 v45, v0

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v46, v0

    const/high16 v47, 0x40000000    # 2.0f

    mul-float v46, v46, v47

    const/high16 v47, 0x40400000    # 3.0f

    div-float v46, v46, v47

    cmpl-float v45, v45, v46

    if-lez v45, :cond_26

    .line 533
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v45, v0

    div-int/lit8 v46, v32, 0x2

    add-int v45, v45, v46

    div-int/lit8 v46, v15, 0x2

    sub-int v43, v45, v46

    .line 534
    const/16 v42, -0x1

    goto/16 :goto_8

    .line 535
    :cond_26
    move/from16 v0, v42

    move/from16 v1, v24

    if-ge v0, v1, :cond_f

    .line 536
    const/16 v42, -0x1

    .line 537
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v45, v0

    add-int/lit8 v44, v45, 0x1b

    .line 540
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mPopupType:I

    move/from16 v45, v0

    const/16 v46, 0x1

    move/from16 v0, v45

    move/from16 v1, v46

    if-ne v0, v1, :cond_27

    .line 541
    add-int/lit8 v44, v44, 0x4

    .line 543
    :cond_27
    add-int v45, v44, v15

    move/from16 v0, v45

    move/from16 v1, v23

    if-le v0, v1, :cond_f

    .line 544
    const/16 v44, -0x1

    .line 545
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v45, v0

    div-int/lit8 v46, v32, 0x2

    add-int v45, v45, v46

    div-int/lit8 v46, v15, 0x2

    sub-int v43, v45, v46

    goto/16 :goto_8

    .line 558
    .restart local v5    # "Pos_Type":I
    .restart local v41    # "y":I
    :cond_28
    const/16 v45, 0x2

    move/from16 v0, v34

    move/from16 v1, v45

    if-ne v0, v1, :cond_29

    .line 559
    const/4 v5, 0x2

    goto/16 :goto_9

    .line 561
    :cond_29
    const/4 v5, 0x3

    goto/16 :goto_9

    .line 562
    :cond_2a
    const/16 v45, -0x1

    move/from16 v0, v44

    move/from16 v1, v45

    if-eq v0, v1, :cond_2d

    .line 564
    move/from16 v41, v44

    .line 565
    const/16 v45, 0x1

    move/from16 v0, v34

    move/from16 v1, v45

    if-ne v0, v1, :cond_2b

    .line 566
    const/4 v5, 0x4

    goto/16 :goto_9

    .line 567
    :cond_2b
    const/16 v45, 0x2

    move/from16 v0, v34

    move/from16 v1, v45

    if-ne v0, v1, :cond_2c

    .line 568
    const/4 v5, 0x5

    goto/16 :goto_9

    .line 570
    :cond_2c
    const/4 v5, 0x6

    goto/16 :goto_9

    .line 573
    :cond_2d
    move/from16 v41, v42

    .line 574
    const/16 v45, 0x1

    move/from16 v0, v34

    move/from16 v1, v45

    if-ne v0, v1, :cond_2e

    .line 575
    const/4 v5, 0x1

    goto/16 :goto_9

    .line 576
    :cond_2e
    const/16 v45, 0x2

    move/from16 v0, v34

    move/from16 v1, v45

    if-ne v0, v1, :cond_2f

    .line 577
    const/4 v5, 0x2

    goto/16 :goto_9

    .line 579
    :cond_2f
    const/4 v5, 0x3

    goto/16 :goto_9

    .line 597
    :cond_30
    invoke-direct/range {p0 .. p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->UpdateTimer()V

    goto/16 :goto_0
.end method

.method private getResString(I)Ljava/lang/String;
    .locals 3
    .param p1, "cmd_id"    # I

    .prologue
    .line 682
    const-string/jumbo v1, ""

    .line 684
    .local v1, "str":Ljava/lang/String;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mParentView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 686
    .local v0, "r":Landroid/content/res/Resources;
    packed-switch p1, :pswitch_data_0

    .line 692
    :goto_0
    return-object v1

    .line 689
    :pswitch_0
    const v2, 0x7f0701e3

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 686
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private isExistCmd(I)Z
    .locals 2
    .param p1, "cmd"    # I

    .prologue
    .line 248
    const/4 v0, 0x0

    .local v0, "k":I
    :goto_0
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mItemSize:I

    if-ge v0, v1, :cond_1

    .line 250
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mCmdArray:[I

    aget v1, v1, v0

    if-ne v1, p1, :cond_0

    .line 251
    const/4 v1, 0x1

    .line 254
    :goto_1
    return v1

    .line 248
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 254
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private setRect(IIII)Z
    .locals 5
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 612
    const/4 v2, 0x0

    .line 613
    .local v2, "ret":Z
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 614
    .local v1, "r":Landroid/graphics/Rect;
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 615
    .local v0, "p":Landroid/graphics/Point;
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mParentView:Landroid/view/View;

    invoke-virtual {v3, v1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 617
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Point;->x:I

    add-int/2addr v4, p1

    iput v4, v3, Landroid/graphics/Rect;->left:I

    .line 618
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Point;->y:I

    add-int/2addr v4, p2

    iput v4, v3, Landroid/graphics/Rect;->top:I

    .line 619
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Point;->x:I

    add-int/2addr v4, p3

    iput v4, v3, Landroid/graphics/Rect;->right:I

    .line 620
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Point;->y:I

    add-int/2addr v4, p4

    iput v4, v3, Landroid/graphics/Rect;->bottom:I

    .line 622
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 624
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget v4, v1, Landroid/graphics/Rect;->left:I

    if-le v3, v4, :cond_0

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget v4, v1, Landroid/graphics/Rect;->right:I

    if-ge v3, v4, :cond_0

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget v4, v1, Landroid/graphics/Rect;->bottom:I

    if-ge v3, v4, :cond_0

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget v4, v1, Landroid/graphics/Rect;->top:I

    if-gt v3, v4, :cond_1

    .line 631
    :cond_0
    :goto_0
    const/4 v1, 0x0

    .line 632
    const/4 v0, 0x0

    .line 634
    return v2

    .line 627
    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected Excute(I)V
    .locals 4
    .param p1, "cmd_id"    # I

    .prologue
    .line 660
    packed-switch p1, :pswitch_data_0

    .line 667
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->dismiss()V

    .line 668
    return-void

    .line 663
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mActivity:Landroid/app/Activity;

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 660
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public InitItem()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 652
    iput v4, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mPopupType:I

    .line 653
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    iput v4, v3, Landroid/graphics/Rect;->bottom:I

    iput v4, v2, Landroid/graphics/Rect;->top:I

    iput v4, v1, Landroid/graphics/Rect;->right:I

    iput v4, v0, Landroid/graphics/Rect;->left:I

    .line 654
    iput v4, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mItemSize:I

    .line 655
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 656
    return-void
.end method

.method public PopupMenuFinalize()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 149
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mShowRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mShowRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 151
    :cond_0
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mShowRunnable:Ljava/lang/Runnable;

    .line 153
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mWaitRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 154
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mWaitRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 155
    :cond_1
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mWaitRunnable:Ljava/lang/Runnable;

    .line 157
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mHandler:Landroid/os/Handler;

    .line 159
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mScrollView:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->removeAllViews()V

    .line 160
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mRootLinear:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 162
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mActivity:Landroid/app/Activity;

    .line 163
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    .line 164
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 166
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mParentView:Landroid/view/View;

    .line 167
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    .line 168
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mScrollView:Landroid/widget/HorizontalScrollView;

    .line 169
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mRootLinear:Landroid/widget/LinearLayout;

    .line 170
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mParentRect:Landroid/graphics/Rect;

    .line 171
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mCmdArray:[I

    .line 173
    return-void
.end method

.method public dismiss()V
    .locals 0

    .prologue
    .line 675
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->InitItem()V

    .line 676
    invoke-super {p0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 677
    return-void
.end method

.method public setHyperlink(Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;)V
    .locals 0
    .param p1, "hyperlinkInfo"    # Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    .prologue
    .line 193
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    .line 194
    return-void
.end method

.method public setPosition(II)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 186
    iput p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->positionX:I

    .line 187
    iput p2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->positionY:I

    .line 188
    iput p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->positionWidth:I

    .line 189
    iput p2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->positionHeight:I

    .line 190
    return-void
.end method

.method public show()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x12c

    .line 199
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mParentView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mActivity:Landroid/app/Activity;

    if-nez v0, :cond_1

    .line 231
    :cond_0
    :goto_0
    return-void

    .line 203
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 205
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mWaitRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_2

    .line 206
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mWaitRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 207
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mShowRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_3

    .line 208
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mShowRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 209
    :cond_3
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->dismiss()V

    .line 212
    :cond_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mWaitRunnable:Ljava/lang/Runnable;

    if-nez v0, :cond_5

    .line 213
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow$2;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mWaitRunnable:Ljava/lang/Runnable;

    .line 221
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mWaitRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 225
    :cond_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mWaitRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 226
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mShowRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_6

    .line 227
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mShowRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 228
    :cond_6
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->mWaitRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public showAsDropDown(Landroid/view/View;II)V
    .locals 0
    .param p1, "anchor"    # Landroid/view/View;
    .param p2, "xoff"    # I
    .param p3, "yoff"    # I

    .prologue
    .line 182
    invoke-super {p0, p1, p2, p3}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    .line 183
    return-void
.end method

.method public showFromRunnable()V
    .locals 4

    .prologue
    .line 235
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->InitItem()V

    .line 237
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    if-eqz v0, :cond_0

    .line 238
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->AddButton(I)V

    .line 240
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->positionX:I

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->positionY:I

    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->positionWidth:I

    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->positionHeight:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->setRect(IIII)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowPopupMenuWindow;->doAction()V

    .line 244
    :cond_0
    return-void
.end method
