.class public Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsRelativeLayout;
.super Landroid/widget/RelativeLayout;
.source "SlideShowSettingsRelativeLayout.java"

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field private mCheckable:Landroid/widget/Checkable;

.field private mCheckableId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsRelativeLayout;->mCheckable:Landroid/widget/Checkable;

    .line 16
    const v0, 0x7f0b021f

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsRelativeLayout;->mCheckableId:I

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsRelativeLayout;->mCheckable:Landroid/widget/Checkable;

    .line 21
    const v0, 0x7f0b021f

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsRelativeLayout;->mCheckableId:I

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsRelativeLayout;->mCheckable:Landroid/widget/Checkable;

    .line 26
    const v0, 0x7f0b021f

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsRelativeLayout;->mCheckableId:I

    .line 27
    return-void
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsRelativeLayout;->mCheckableId:I

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Checkable;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsRelativeLayout;->mCheckable:Landroid/widget/Checkable;

    .line 32
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsRelativeLayout;->mCheckable:Landroid/widget/Checkable;

    if-nez v0, :cond_0

    .line 33
    const/4 v0, 0x0

    .line 34
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsRelativeLayout;->mCheckable:Landroid/widget/Checkable;

    invoke-interface {v0}, Landroid/widget/Checkable;->isChecked()Z

    move-result v0

    goto :goto_0
.end method

.method public setChecked(Z)V
    .locals 1
    .param p1, "checked"    # Z

    .prologue
    .line 39
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsRelativeLayout;->mCheckableId:I

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Checkable;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsRelativeLayout;->mCheckable:Landroid/widget/Checkable;

    .line 40
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsRelativeLayout;->mCheckable:Landroid/widget/Checkable;

    if-nez v0, :cond_0

    .line 43
    :goto_0
    return-void

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsRelativeLayout;->mCheckable:Landroid/widget/Checkable;

    invoke-interface {v0, p1}, Landroid/widget/Checkable;->setChecked(Z)V

    goto :goto_0
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsRelativeLayout;->mCheckableId:I

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Checkable;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsRelativeLayout;->mCheckable:Landroid/widget/Checkable;

    .line 48
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsRelativeLayout;->mCheckable:Landroid/widget/Checkable;

    if-nez v0, :cond_0

    .line 51
    :goto_0
    return-void

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsRelativeLayout;->mCheckable:Landroid/widget/Checkable;

    invoke-interface {v0}, Landroid/widget/Checkable;->toggle()V

    goto :goto_0
.end method
