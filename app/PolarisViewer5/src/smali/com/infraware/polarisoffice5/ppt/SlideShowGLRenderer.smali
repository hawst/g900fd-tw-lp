.class public Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;
.super Ljava/lang/Object;
.source "SlideShowGLRenderer.java"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer$SlideShowGLRendererListener;,
        Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer$SlideShowStatus;
    }
.end annotation


# static fields
.field public static mglsync:Z


# instance fields
.field private final LOG_CAT:Ljava/lang/String;

.field private mCurPage:I

.field private mEvInterface:Lcom/infraware/office/evengine/EvInterface;

.field private mHeight:I

.field private mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer$SlideShowGLRendererListener;

.field private mPage:I

.field public mStatus:I

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    sput-boolean v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mglsync:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-string/jumbo v0, "SlideShowGLRenderer"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->LOG_CAT:Ljava/lang/String;

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mStatus:I

    .line 23
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mPage:I

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 45
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 46
    return-void
.end method


# virtual methods
.method public getGlsync()Z
    .locals 1

    .prologue
    .line 78
    sget-boolean v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mglsync:Z

    return v0
.end method

.method public isEglCtxAvailable()Z
    .locals 3

    .prologue
    .line 53
    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    .line 54
    .local v0, "egl":Ljavax/microedition/khronos/egl/EGL10;
    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetCurrentContext()Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 55
    const/4 v1, 0x0

    .line 57
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 6
    .param p1, "glUnused"    # Ljavax/microedition/khronos/opengles/GL10;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x5

    .line 86
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->isEglCtxAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 123
    :goto_0
    return-void

    .line 89
    :cond_0
    const-string/jumbo v0, "SlideShowGLRenderer"

    const-string/jumbo v1, "onDrawFrame - start"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mStatus:I

    if-ne v0, v4, :cond_2

    .line 91
    const-string/jumbo v0, "SlideShowGLRenderer"

    const-string/jumbo v1, "onDrawFrame - Slide Show Start"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer$SlideShowGLRendererListener;

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mWidth:I

    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mHeight:I

    invoke-interface {v0, v1, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer$SlideShowGLRendererListener;->onStart(II)V

    .line 93
    iput v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mStatus:I

    .line 94
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mCurPage:I

    .line 122
    :cond_1
    :goto_1
    const-string/jumbo v0, "SlideShowGLRenderer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDrawFrame - endmStatus = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mStatus:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 96
    :cond_2
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mStatus:I

    if-ne v0, v5, :cond_3

    .line 97
    const-string/jumbo v0, "SlideShowGLRenderer"

    const-string/jumbo v1, "onDrawFrame - Slide Show Prev Play"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer$SlideShowGLRendererListener;

    invoke-interface {v0, v4, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer$SlideShowGLRendererListener;->onPlay(II)V

    .line 99
    iput v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mStatus:I

    .line 100
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mCurPage:I

    goto :goto_1

    .line 102
    :cond_3
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mStatus:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 103
    const-string/jumbo v0, "SlideShowGLRenderer"

    const-string/jumbo v1, "onDrawFrame - Slide Show Next Play"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer$SlideShowGLRendererListener;

    invoke-interface {v0, v5, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer$SlideShowGLRendererListener;->onPlay(II)V

    .line 105
    iput v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mStatus:I

    .line 106
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mCurPage:I

    goto :goto_1

    .line 109
    :cond_4
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mStatus:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    .line 110
    const-string/jumbo v0, "SlideShowGLRenderer"

    const-string/jumbo v1, "onDrawFrame - Slide Show Page Play"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer$SlideShowGLRendererListener;

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mPage:I

    invoke-interface {v0, v3, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer$SlideShowGLRendererListener;->onPlay(II)V

    .line 112
    iput v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mStatus:I

    .line 113
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mCurPage:I

    goto :goto_1

    .line 115
    :cond_5
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mStatus:I

    if-ne v0, v3, :cond_1

    .line 116
    const-string/jumbo v0, "SlideShowGLRenderer"

    const-string/jumbo v1, "onDrawFrame - Slide Show Continue"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer$SlideShowGLRendererListener;

    invoke-interface {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer$SlideShowGLRendererListener;->onContinue()V

    .line 119
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mCurPage:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v1

    iget v1, v1, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    if-eq v0, v1, :cond_1

    .line 120
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer$SlideShowGLRendererListener;

    invoke-interface {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer$SlideShowGLRendererListener;->onUpdateSeekBar()V

    goto/16 :goto_1
.end method

.method public onPlaySlideShow(II)V
    .locals 2
    .param p1, "nPlayType"    # I
    .param p2, "nPage"    # I

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 133
    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mPage:I

    .line 135
    if-ne p1, v0, :cond_1

    .line 136
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mStatus:I

    .line 143
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    if-ne p1, v1, :cond_2

    .line 138
    const/4 v0, 0x3

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mStatus:I

    goto :goto_0

    .line 139
    :cond_2
    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    .line 140
    const/4 v0, 0x4

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mStatus:I

    .line 141
    iput p2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mPage:I

    goto :goto_0
.end method

.method public onStartSlideShow(II)V
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 126
    const-string/jumbo v0, "SlideShowGLRenderer"

    const-string/jumbo v1, "onStartSlideShow"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mStatus:I

    .line 128
    iput p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mWidth:I

    .line 129
    iput p2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mHeight:I

    .line 130
    return-void
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 1
    .param p1, "glUnused"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 68
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mWidth:I

    if-ne v0, p2, :cond_0

    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mHeight:I

    if-eq v0, p3, :cond_1

    .line 69
    :cond_0
    iput p2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mWidth:I

    .line 70
    iput p3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mHeight:I

    .line 72
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->isEglCtxAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 73
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISetPPTSlideGLSync()V

    .line 75
    :cond_1
    return-void
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 2
    .param p1, "glUnused"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "config"    # Ljavax/microedition/khronos/egl/EGLConfig;

    .prologue
    const/4 v1, 0x0

    .line 62
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->isEglCtxAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v1, v1, v1, v0}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 64
    :cond_0
    return-void
.end method

.method public setGlsync(Z)V
    .locals 0
    .param p1, "setSync"    # Z

    .prologue
    .line 81
    sput-boolean p1, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mglsync:Z

    .line 82
    return-void
.end method

.method public setSlideShowGLRendererListener(Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer$SlideShowGLRendererListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer$SlideShowGLRendererListener;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer$SlideShowGLRendererListener;

    .line 50
    return-void
.end method
