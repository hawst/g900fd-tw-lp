.class Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$1;
.super Landroid/os/Handler;
.source "SlideShowGestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$1;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 47
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 53
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unknown message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$1;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->access$000(Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;)Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$1;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mStillDown:Z
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->access$100(Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$1;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->access$000(Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;)Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$1;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->access$200(Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;)Landroid/view/MotionEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    .line 55
    :cond_0
    return-void

    .line 47
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
