.class Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$NfcCallback;
.super Ljava/lang/Object;
.source "PPTMainActivity.java"

# interfaces
.implements Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;
.implements Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NfcCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;


# direct methods
.method private constructor <init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V
    .locals 0

    .prologue
    .line 2270
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
    .param p2, "x1"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$1;

    .prologue
    .line 2270
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$NfcCallback;-><init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V

    return-void
.end method


# virtual methods
.method public createNdefMessage(Landroid/nfc/NfcEvent;)Landroid/nfc/NdefMessage;
    .locals 7
    .param p1, "event"    # Landroid/nfc/NfcEvent;

    .prologue
    const/4 v3, 0x0

    .line 2288
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v4

    if-nez v4, :cond_1

    .line 2305
    :cond_0
    :goto_0
    return-object v3

    .line 2290
    :cond_1
    iget-object v4, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-static {v4}, Lcom/infraware/common/util/SBeamUtils;->isSBeamSupportedDevice(Landroid/content/Context;)Z

    move-result v2

    .line 2291
    .local v2, "sbeamSupport":Z
    if-eqz v2, :cond_0

    .line 2292
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-static {v4}, Lcom/infraware/common/util/SBeamUtils;->isSBeamEnabled(Landroid/content/Context;)Z

    move-result v4

    # setter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSBeamEnabled:Z
    invoke-static {v3, v4}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2702(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;Z)Z

    .line 2294
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mstrOpenFilePath:Ljava/lang/String;
    invoke-static {v6}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2800(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/infraware/common/util/SBeamUtils;->createSBeamNdefRecord(Landroid/content/Context;[Ljava/lang/String;)Landroid/nfc/NdefRecord;

    move-result-object v1

    .line 2295
    .local v1, "record":Landroid/nfc/NdefRecord;
    new-instance v0, Ljava/lang/String;

    invoke-virtual {v1}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>([B)V

    .line 2297
    .local v0, "payload":Ljava/lang/String;
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSBeamEnabled:Z
    invoke-static {v3}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2700(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2298
    sget-object v3, Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;->ERROR_SBEAM_OFF:Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

    invoke-static {v3}, Lcom/infraware/common/util/SBeamUtils;->createSBeamNdefErrorRecord(Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;)Landroid/nfc/NdefRecord;

    move-result-object v1

    .line 2303
    :cond_2
    :goto_1
    invoke-static {v1}, Lcom/infraware/common/util/SBeamUtils;->createSBeamNdefMessage(Landroid/nfc/NdefRecord;)Landroid/nfc/NdefMessage;

    move-result-object v3

    goto :goto_0

    .line 2300
    :cond_3
    const-string/jumbo v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2301
    sget-object v3, Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;->ERROR_FILE_NOT_SELECTED:Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

    invoke-static {v3}, Lcom/infraware/common/util/SBeamUtils;->createSBeamNdefErrorRecord(Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;)Landroid/nfc/NdefRecord;

    move-result-object v1

    goto :goto_1
.end method

.method public onNdefPushComplete(Landroid/nfc/NfcEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/nfc/NfcEvent;

    .prologue
    .line 2273
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2284
    :cond_0
    :goto_0
    return-void

    .line 2275
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-static {v1}, Lcom/infraware/common/util/SBeamUtils;->isSBeamSupportedDevice(Landroid/content/Context;)Z

    move-result v0

    .line 2276
    .local v0, "sbeamSupport":Z
    if-eqz v0, :cond_0

    .line 2277
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSBeamEnabled:Z
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2700(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2278
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-static {v1}, Lcom/infraware/common/util/SBeamUtils;->startSBeamDirectShareService(Landroid/content/Context;)V

    goto :goto_0

    .line 2281
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    sget-object v2, Lcom/infraware/common/util/SBeamUtils$SBeamPopupType;->POPUP_SBEAM_DISABLED:Lcom/infraware/common/util/SBeamUtils$SBeamPopupType;

    invoke-static {v1, v2}, Lcom/infraware/common/util/SBeamUtils;->startSBeamPopupActivity(Landroid/content/Context;Lcom/infraware/common/util/SBeamUtils$SBeamPopupType;)V

    goto :goto_0
.end method
