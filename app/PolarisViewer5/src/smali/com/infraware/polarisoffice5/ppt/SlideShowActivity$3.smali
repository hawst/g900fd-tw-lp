.class Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$3;
.super Ljava/lang/Object;
.source "SlideShowActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V
    .locals 0

    .prologue
    .line 399
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$3;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 402
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    .line 404
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 425
    :goto_0
    :pswitch_0
    return v0

    .line 406
    :pswitch_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$3;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNextPage:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$2000(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 407
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$3;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPrevPage:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$2100(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_0

    .line 410
    :pswitch_2
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$3;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPrevPage:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$2100(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 411
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$3;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNextPage:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$2000(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_0

    .line 417
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_1

    .line 419
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$3;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPrevPage:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$2100(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 420
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$3;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNextPage:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$2000(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 421
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$3;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v1, p1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onClickPage(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 425
    goto :goto_0

    .line 404
    :pswitch_data_0
    .packed-switch 0x7f0b0203
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
