.class Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$9;
.super Landroid/os/Handler;
.source "PPTMainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V
    .locals 0

    .prologue
    .line 1566
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$9;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1569
    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    .line 1596
    :cond_0
    :goto_0
    return-void

    .line 1573
    :pswitch_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 1575
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v5, "SINGLE_SLIDE_VIEW"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    .line 1577
    .local v2, "isSingleSlideViewChecked":Ljava/lang/Boolean;
    const/4 v1, 0x0

    .line 1578
    .local v1, "isContinueModeChecked":Z
    if-eqz v2, :cond_1

    .line 1579
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-nez v5, :cond_3

    const/4 v1, 0x1

    .line 1581
    :cond_1
    :goto_1
    const-string/jumbo v5, "SMART_GUIDE"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    .line 1582
    .local v3, "isSmartGuideChecked":Ljava/lang/Boolean;
    const-string/jumbo v5, "SPELL_CHECK"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    .line 1585
    .local v4, "isSpellChecked":Ljava/lang/Boolean;
    if-eqz v2, :cond_2

    iget-object v5, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$9;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbContinueMode:Z
    invoke-static {v5}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$1600(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Z

    move-result v5

    if-eq v5, v1, :cond_2

    .line 1586
    iget-object v5, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$9;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # invokes: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onChangeScreenMode()V
    invoke-static {v5}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$1700(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V

    .line 1589
    :cond_2
    if-eqz v3, :cond_0

    iget-object v5, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$9;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbSmartGuides:Z
    invoke-static {v5}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$1800(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Z

    move-result v5

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eq v5, v6, :cond_0

    .line 1590
    iget-object v5, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$9;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # invokes: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onSmartGuides()V
    invoke-static {v5}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$1900(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V

    goto :goto_0

    .line 1579
    .end local v3    # "isSmartGuideChecked":Ljava/lang/Boolean;
    .end local v4    # "isSpellChecked":Ljava/lang/Boolean;
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 1569
    :pswitch_data_0
    .packed-switch 0x2f
        :pswitch_0
    .end packed-switch
.end method
