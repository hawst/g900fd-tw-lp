.class public final Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;
.super Landroid/widget/BaseAdapter;
.source "SlideDragNDropAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter$ViewHolder;
    }
.end annotation


# static fields
.field private static mContext:Landroid/content/Context;

.field private static mInflater:Landroid/view/LayoutInflater;

.field private static uniqueInstance:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;


# instance fields
.field private final LOG_CAT:Ljava/lang/String;

.field private mAdapterID:I

.field private mIds:[I

.field private mLayout:I

.field private mSelectedPosition:I

.field private mSlideDragNDropList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;",
            ">;"
        }
    .end annotation
.end field

.field private mbChangedDataSet:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24
    sput-object v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->uniqueInstance:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    .line 25
    sput-object v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mContext:Landroid/content/Context;

    .line 26
    sput-object v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 22
    const-string/jumbo v0, "SlideDragNDropAdapter"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->LOG_CAT:Ljava/lang/String;

    .line 28
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mAdapterID:I

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSlideDragNDropList:Ljava/util/List;

    .line 33
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSelectedPosition:I

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mbChangedDataSet:Z

    .line 43
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    sget-object v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->uniqueInstance:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    if-nez v0, :cond_0

    .line 47
    const-class v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    monitor-enter v1

    .line 48
    :try_start_0
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    invoke-direct {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;-><init>()V

    sput-object v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->uniqueInstance:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    .line 49
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    :cond_0
    sget-object v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mContext:Landroid/content/Context;

    if-nez v0, :cond_2

    .line 53
    sput-object p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mContext:Landroid/content/Context;

    .line 54
    invoke-static {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->initContext(Landroid/content/Context;)V

    .line 63
    :cond_1
    :goto_0
    sget-object v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->uniqueInstance:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    return-object v0

    .line 49
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 57
    :cond_2
    sget-object v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 58
    sput-object p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mContext:Landroid/content/Context;

    .line 59
    invoke-static {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->initContext(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private static initContext(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 67
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sput-object v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 68
    return-void
.end method


# virtual methods
.method public changeListItems(ILjava/lang/Boolean;Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "position"    # I
    .param p2, "bImage"    # Ljava/lang/Boolean;
    .param p3, "image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 118
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getItem(I)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;

    move-result-object v0

    .line 119
    .local v0, "listItem":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;
    if-nez v0, :cond_0

    .line 123
    :goto_0
    return-void

    .line 121
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->setExistImage(Z)V

    .line 122
    invoke-virtual {v0, p3}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->setImage(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public changeListItems(ILjava/lang/Boolean;Landroid/graphics/Bitmap;Z)V
    .locals 2
    .param p1, "position"    # I
    .param p2, "bImage"    # Ljava/lang/Boolean;
    .param p3, "image"    # Landroid/graphics/Bitmap;
    .param p4, "isHideSlide"    # Z

    .prologue
    .line 126
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getItem(I)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;

    move-result-object v0

    .line 127
    .local v0, "listItem":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;
    if-nez v0, :cond_0

    .line 132
    :goto_0
    return-void

    .line 129
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->setExistImage(Z)V

    .line 130
    invoke-virtual {v0, p3}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->setImage(Landroid/graphics/Bitmap;)V

    .line 131
    invoke-virtual {v0, p4}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->setSlideHideInfo(Z)V

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSlideDragNDropList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 101
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSlideDragNDropList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 102
    :cond_0
    const/4 v0, 0x0

    .line 103
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSlideDragNDropList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getItem(I)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 107
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemImage(I)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 111
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getItem(I)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;

    move-result-object v0

    .line 112
    .local v0, "listItem":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;
    if-nez v0, :cond_0

    .line 113
    const/4 v1, 0x0

    .line 114
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->getImage()Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0
.end method

.method public getSelectedPosition()I
    .locals 1

    .prologue
    .line 135
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSelectedPosition:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v9, 0x0

    const/4 v3, 0x0

    const/4 v8, 0x1

    const/high16 v7, 0x3f800000    # 1.0f

    .line 156
    if-nez p2, :cond_3

    .line 157
    sget-object v4, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mInflater:Landroid/view/LayoutInflater;

    if-nez v4, :cond_0

    .line 216
    :goto_0
    return-object v3

    .line 161
    :cond_0
    :try_start_0
    sget-object v4, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mInflater:Landroid/view/LayoutInflater;

    iget v5, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mLayout:I

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p2

    .line 166
    new-instance v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter$ViewHolder;

    invoke-direct {v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter$ViewHolder;-><init>()V

    .line 167
    .local v1, "holder":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter$ViewHolder;
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mIds:[I

    aget v3, v3, v9

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout;

    iput-object v3, v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter$ViewHolder;->mSlide:Landroid/widget/FrameLayout;

    .line 168
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mIds:[I

    aget v3, v3, v8

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter$ViewHolder;->mImage:Landroid/widget/ImageView;

    .line 169
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mIds:[I

    const/4 v4, 0x2

    aget v3, v3, v4

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter$ViewHolder;->mPageNum:Landroid/widget/TextView;

    .line 170
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mIds:[I

    const/4 v4, 0x3

    aget v3, v3, v4

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter$ViewHolder;->mHidePage:Landroid/widget/ImageView;

    .line 171
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 177
    :goto_1
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getItem(I)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;

    move-result-object v2

    .line 178
    .local v2, "listItem":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;
    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSelectedPosition:I

    if-ne p1, v3, :cond_4

    .line 179
    iget-object v3, v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter$ViewHolder;->mSlide:Landroid/widget/FrameLayout;

    const v4, 0x7f02024f

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 180
    iget-object v3, v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter$ViewHolder;->mPageNum:Landroid/widget/TextView;

    const-string/jumbo v4, "#FF007c93"

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 181
    iput-boolean v8, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mbChangedDataSet:Z

    .line 187
    :goto_2
    if-eqz v2, :cond_1

    .line 188
    iget-object v3, v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter$ViewHolder;->mImage:Landroid/widget/ImageView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->getImage()Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 190
    :cond_1
    iget-object v3, v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter$ViewHolder;->mPageNum:Landroid/widget/TextView;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 192
    if-eqz v2, :cond_2

    .line 193
    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->isHideSlide()Z

    move-result v3

    if-ne v3, v8, :cond_6

    .line 194
    iget-object v3, v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter$ViewHolder;->mHidePage:Landroid/widget/ImageView;

    invoke-virtual {v3, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 200
    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSelectedPosition:I

    if-ne p1, v3, :cond_5

    .line 201
    iget-object v3, v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter$ViewHolder;->mSlide:Landroid/widget/FrameLayout;

    const v4, 0x7f02024d

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    :cond_2
    :goto_3
    move-object v3, p2

    .line 216
    goto/16 :goto_0

    .line 163
    .end local v1    # "holder":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter$ViewHolder;
    .end local v2    # "listItem":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;
    :catch_0
    move-exception v0

    .line 164
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    goto/16 :goto_0

    .line 174
    .end local v0    # "e":Landroid/content/res/Resources$NotFoundException;
    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter$ViewHolder;

    .restart local v1    # "holder":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter$ViewHolder;
    goto :goto_1

    .line 184
    .restart local v2    # "listItem":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;
    :cond_4
    iget-object v3, v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter$ViewHolder;->mSlide:Landroid/widget/FrameLayout;

    const v4, 0x7f02024e

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 185
    iget-object v3, v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter$ViewHolder;->mPageNum:Landroid/widget/TextView;

    const-string/jumbo v4, "#FF646464"

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2

    .line 204
    :cond_5
    iget-object v3, v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter$ViewHolder;->mSlide:Landroid/widget/FrameLayout;

    const v4, 0x7f02024c

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    goto :goto_3

    .line 208
    :cond_6
    iget-object v3, v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter$ViewHolder;->mHidePage:Landroid/widget/ImageView;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 210
    iget-object v3, v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter$ViewHolder;->mHidePage:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 211
    iget-object v3, v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter$ViewHolder;->mImage:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 212
    iget-object v3, v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter$ViewHolder;->mPageNum:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setAlpha(F)V

    goto :goto_3
.end method

.method public hasValidData(I)Z
    .locals 1
    .param p1, "interfaceHandleValue"    # I

    .prologue
    .line 89
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mAdapterID:I

    if-ne v0, p1, :cond_0

    .line 90
    const/4 v0, 0x1

    .line 93
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initResources(II[I)V
    .locals 0
    .param p1, "adpaterID"    # I
    .param p2, "layout"    # I
    .param p3, "ids"    # [I

    .prologue
    .line 83
    iput p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mAdapterID:I

    .line 84
    iput-object p3, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mIds:[I

    .line 85
    iput p2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mLayout:I

    .line 86
    return-void
.end method

.method public isChangedDataSet()Z
    .locals 1

    .prologue
    .line 151
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mbChangedDataSet:Z

    return v0
.end method

.method public onAdd(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;)V
    .locals 1
    .param p1, "item"    # Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;

    .prologue
    .line 220
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSlideDragNDropList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    return-void
.end method

.method public onClear()V
    .locals 2

    .prologue
    .line 318
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSlideDragNDropList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 319
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->notifyDataSetChanged()V

    .line 320
    const-string/jumbo v0, "SlideDragNDropAdapter"

    const-string/jumbo v1, "onClear - notifyDataSetChanged"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    return-void
.end method

.method public onDelete()I
    .locals 3

    .prologue
    .line 251
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSelectedPosition:I

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSelectedPosition:I

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSlideDragNDropList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 252
    :cond_0
    const/4 v0, -0x1

    .line 260
    :goto_0
    return v0

    .line 254
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSelectedPosition:I

    .line 255
    .local v0, "deletedPosition":I
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSlideDragNDropList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 256
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSlideDragNDropList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 257
    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSelectedPosition:I

    .line 258
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->notifyDataSetChanged()V

    .line 259
    const-string/jumbo v1, "SlideDragNDropAdapter"

    const-string/jumbo v2, "onDelete - notifyDataSetChanged"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDeleteAtLocation(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 264
    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSlideDragNDropList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt p1, v1, :cond_1

    .line 265
    :cond_0
    const/4 v0, -0x1

    .line 273
    :goto_0
    return v0

    .line 267
    :cond_1
    move v0, p1

    .line 268
    .local v0, "deletedPosition":I
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSlideDragNDropList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 269
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSlideDragNDropList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 270
    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSelectedPosition:I

    .line 271
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->notifyDataSetChanged()V

    .line 272
    const-string/jumbo v1, "SlideDragNDropAdapter"

    const-string/jumbo v2, "onDeleteAtLocation - notifyDataSetChanged"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDrop(II)V
    .locals 3
    .param p1, "from"    # I
    .param p2, "to"    # I

    .prologue
    .line 277
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getItem(I)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;

    move-result-object v0

    .line 278
    .local v0, "listItem":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSlideDragNDropList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 279
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSlideDragNDropList:Ljava/util/List;

    invoke-interface {v1, p2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 280
    iput p2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSelectedPosition:I

    .line 281
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->notifyDataSetChanged()V

    .line 282
    const-string/jumbo v1, "SlideDragNDropAdapter"

    const-string/jumbo v2, "onDrop - notifyDataSetChanged"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    return-void
.end method

.method public onDuplicate()I
    .locals 5

    .prologue
    .line 244
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSlideDragNDropList:Ljava/util/List;

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSelectedPosition:I

    add-int/lit8 v1, v1, 0x1

    new-instance v2, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;-><init>(ZLandroid/graphics/Bitmap;)V

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 245
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->notifyDataSetChanged()V

    .line 246
    const-string/jumbo v0, "SlideDragNDropAdapter"

    const-string/jumbo v1, "onDuplicate - notifyDataSetChanged"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSelectedPosition:I

    return v0
.end method

.method public onHide()I
    .locals 3

    .prologue
    .line 300
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSelectedPosition:I

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getItem(I)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;

    move-result-object v0

    .line 301
    .local v0, "listItem":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;
    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->isHideSlide()Z

    move-result v1

    if-nez v1, :cond_0

    .line 302
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->setSlideHideInfo(Z)V

    .line 307
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->notifyDataSetChanged()V

    .line 308
    const-string/jumbo v1, "SlideDragNDropAdapter"

    const-string/jumbo v2, "onHide - notifyDataSetChanged"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSelectedPosition:I

    return v1

    .line 305
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->setSlideHideInfo(Z)V

    goto :goto_0
.end method

.method public onInsert()V
    .locals 5

    .prologue
    .line 231
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSlideDragNDropList:Ljava/util/List;

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSelectedPosition:I

    add-int/lit8 v1, v1, 0x1

    new-instance v2, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;-><init>(ZLandroid/graphics/Bitmap;)V

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 232
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->notifyDataSetChanged()V

    .line 233
    const-string/jumbo v0, "SlideDragNDropAdapter"

    const-string/jumbo v1, "onInsert - notifyDataSetChanged"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    return-void
.end method

.method public onInsertAtLocation(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 237
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSlideDragNDropList:Ljava/util/List;

    new-instance v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;-><init>(ZLandroid/graphics/Bitmap;)V

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 238
    iput p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSelectedPosition:I

    .line 239
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->notifyDataSetChanged()V

    .line 240
    const-string/jumbo v0, "SlideDragNDropAdapter"

    const-string/jumbo v1, "onInsertAtLocation - notifyDataSetChanged"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    return-void
.end method

.method public onListClear()V
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSlideDragNDropList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 314
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->notifyDataSetChanged()V

    .line 315
    return-void
.end method

.method public onUndoHide(I)V
    .locals 3
    .param p1, "nPageNum"    # I

    .prologue
    .line 286
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getItem(I)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;

    move-result-object v0

    .line 287
    .local v0, "listItem":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;
    if-eqz v0, :cond_0

    .line 288
    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->isHideSlide()Z

    move-result v1

    if-nez v1, :cond_1

    .line 289
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->setSlideHideInfo(Z)V

    .line 294
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->notifyDataSetChanged()V

    .line 295
    const-string/jumbo v1, "SlideDragNDropAdapter"

    const-string/jumbo v2, "onHide - notifyDataSetChanged"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    :cond_0
    return-void

    .line 292
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->setSlideHideInfo(Z)V

    goto :goto_0
.end method

.method public setChangedDataSet(Z)V
    .locals 0
    .param p1, "bChangedDataSet"    # Z

    .prologue
    .line 147
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mbChangedDataSet:Z

    .line 148
    return-void
.end method

.method public setSelectedPosition(IZ)V
    .locals 2
    .param p1, "position"    # I
    .param p2, "update"    # Z

    .prologue
    .line 139
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSelectedPosition:I

    if-eq v0, p1, :cond_0

    .line 140
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->notifyDataSetChanged()V

    .line 141
    const-string/jumbo v0, "SlideDragNDropAdapter"

    const-string/jumbo v1, "setSelectedPosition - notifyDataSetChanged"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    :cond_0
    iput p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->mSelectedPosition:I

    .line 144
    return-void
.end method
