.class public Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;
.super Lcom/infraware/common/baseactivity/BaseActivity;
.source "SlideShowExternalDisplayActivity.java"

# interfaces
.implements Lcom/infraware/office/baseframe/EvBaseE$BaseActivityEventType;
.implements Lcom/infraware/office/baseframe/EvBaseE$PopupDialogEventType;
.implements Lcom/infraware/office/evengine/E$EV_MOVE_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_PEN_MODE;
.implements Lcom/infraware/office/evengine/E$EV_SLIDESHOW_PLAY_TYPE;
.implements Lcom/infraware/office/evengine/EvListener$PptEditorListener;
.implements Lcom/infraware/office/evengine/EvListener$ViewerListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$CloseActionReceiver;,
        Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$SlideShowStatus;,
        Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$SlideShowMode;
    }
.end annotation


# static fields
.field private static final ACTION_HDMI_PLUGGED:Ljava/lang/String; = "android.intent.action.HDMI_PLUGGED"

.field private static final ACTION_WIFI_DISPLAY:Ljava/lang/String; = "android.intent.action.WIFI_DISPLAY"

.field public static final MSG_START_SLIDE_SHOW:I = 0x1

.field public static final MSG_UPDATE_SLIDE_NOTE:I = 0x2


# instance fields
.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mConformMsgPopup:Landroid/app/AlertDialog;

.field private mControllerFirst:Landroid/widget/ImageView;

.field private mControllerLast:Landroid/widget/ImageView;

.field private mControllerTVOut:Landroid/widget/ImageView;

.field private mEraserAllMode:Landroid/widget/ImageView;

.field private mEraserMode:Landroid/widget/ImageView;

.field private mEvInterface:Lcom/infraware/office/evengine/EvInterface;

.field private mFilePath:Ljava/lang/String;

.field private mGestureProc:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;

.field private mHandler:Landroid/os/Handler;

.field private mMarkerMode:Landroid/widget/ImageView;

.field private mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

.field private mModeOption:Landroid/widget/LinearLayout;

.field private mNextPage:Landroid/widget/ImageView;

.field private mNote:Landroid/widget/LinearLayout;

.field private mNoteEditText:Landroid/widget/EditText;

.field private mNoteMode:Landroid/widget/ImageView;

.field private mNoteString:Ljava/lang/String;

.field private mPageNum:I

.field private mPointerColor:I

.field private mPointerColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mPointerColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mPointerColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mPointerColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mPointerColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

.field private mPointerMode:Landroid/widget/ImageView;

.field private mPointerOption:Landroid/widget/LinearLayout;

.field private mPrevPage:Landroid/widget/ImageView;

.field private mSettings:Landroid/widget/ImageView;

.field private mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

.field private mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

.field private mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

.field private mSlideMode:Landroid/widget/ImageView;

.field private mSlideShowGestureProcListener:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc$SlideShowGestureProcListener;

.field private mSlideShowMode:I

.field private mSlideShowStartPage:I

.field private mSlideShowSurfaceViewListener:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView$SlideShowSurfaceViewListener;

.field private m_oCloseFilter:Landroid/content/IntentFilter;

.field private m_oCloseReceiver:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$CloseActionReceiver;

.field private mbPPSFile:Z

.field private mbPauseTVOut:Z

.field private mbSlideShow:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Lcom/infraware/common/baseactivity/BaseActivity;-><init>()V

    .line 48
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 49
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mGestureProc:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;

    .line 51
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    .line 52
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    .line 53
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    .line 54
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPrevPage:Landroid/widget/ImageView;

    .line 55
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mNextPage:Landroid/widget/ImageView;

    .line 57
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mModeOption:Landroid/widget/LinearLayout;

    .line 58
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideMode:Landroid/widget/ImageView;

    .line 59
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerMode:Landroid/widget/ImageView;

    .line 60
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mMarkerMode:Landroid/widget/ImageView;

    .line 62
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerOption:Landroid/widget/LinearLayout;

    .line 63
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 64
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 65
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 66
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 67
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 68
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    .line 70
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    .line 71
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSettings:Landroid/widget/ImageView;

    .line 72
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEraserAllMode:Landroid/widget/ImageView;

    .line 73
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEraserMode:Landroid/widget/ImageView;

    .line 75
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mControllerFirst:Landroid/widget/ImageView;

    .line 76
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mControllerLast:Landroid/widget/ImageView;

    .line 77
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mControllerTVOut:Landroid/widget/ImageView;

    .line 78
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mNoteMode:Landroid/widget/ImageView;

    .line 80
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mNote:Landroid/widget/LinearLayout;

    .line 81
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mNoteEditText:Landroid/widget/EditText;

    .line 82
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mConformMsgPopup:Landroid/app/AlertDialog;

    .line 84
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor:I

    .line 85
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowMode:I

    .line 86
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPageNum:I

    .line 87
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowStartPage:I

    .line 88
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mNoteString:Ljava/lang/String;

    .line 89
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mFilePath:Ljava/lang/String;

    .line 90
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mbPPSFile:Z

    .line 91
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mbSlideShow:Z

    .line 92
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mbPauseTVOut:Z

    .line 95
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$CloseActionReceiver;

    .line 96
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    .line 121
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$1;-><init>(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mHandler:Landroid/os/Handler;

    .line 142
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$2;-><init>(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 157
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$3;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$3;-><init>(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowSurfaceViewListener:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView$SlideShowSurfaceViewListener;

    .line 170
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$4;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$4;-><init>(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowGestureProcListener:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc$SlideShowGestureProcListener;

    .line 852
    return-void
.end method

.method static synthetic access$002(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mbSlideShow:Z

    return p1
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    .prologue
    .line 45
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowStartPage:I

    return v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)Lcom/infraware/office/evengine/EvInterface;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    return-object v0
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->getSlideNoteString()V

    return-void
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onFinish()V

    return-void
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mbPPSFile:Z

    return v0
.end method

.method private getSlideNoteString()V
    .locals 2

    .prologue
    .line 549
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPageNum:I

    .line 550
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPageNum:I

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->IGetSlideNoteString_Editor(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mNoteString:Ljava/lang/String;

    .line 551
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mNoteEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mNoteString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 552
    return-void
.end method

.method private onChangeMarker()V
    .locals 4

    .prologue
    .line 373
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getThickness()I

    move-result v1

    mul-int/lit8 v1, v1, 0x48

    div-int/lit8 v0, v1, 0xa

    .line 376
    .local v0, "thickness":I
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-nez v1, :cond_0

    .line 377
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 379
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v1, :cond_1

    .line 380
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->ISetPenMode(IZ)V

    .line 381
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1, v0}, Lcom/infraware/office/evengine/EvInterface;->ISetPenSize(I)V

    .line 382
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getColor()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetPenColor(I)V

    .line 383
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getTransparency()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetPenTransparency(I)V

    .line 385
    :cond_1
    return-void
.end method

.method private onChangeMode(I)V
    .locals 8
    .param p1, "mode"    # I

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 388
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowMode:I

    if-ne v1, p1, :cond_1

    .line 451
    :cond_0
    :goto_0
    return-void

    .line 390
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSettings:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 391
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEraserMode:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 392
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onChangeShowMode(I)V

    .line 394
    sget-object v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$SlideShowStatus;->SLIDE_STATUS:[I

    .line 395
    .local v0, "status":[I
    packed-switch p1, :pswitch_data_0

    .line 432
    :cond_2
    :goto_1
    iput p1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowMode:I

    .line 433
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mGestureProc:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;

    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowMode:I

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->setSlideShowMode(I)V

    .line 434
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    aget v2, v0, v3

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->setVisibility(I)V

    .line 435
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSettings:Landroid/widget/ImageView;

    aget v2, v0, v4

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 436
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEraserAllMode:Landroid/widget/ImageView;

    aget v2, v0, v5

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 437
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEraserMode:Landroid/widget/ImageView;

    aget v2, v0, v5

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 438
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPrevPage:Landroid/widget/ImageView;

    aget v2, v0, v6

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 439
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mNextPage:Landroid/widget/ImageView;

    aget v2, v0, v6

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 440
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mModeOption:Landroid/widget/LinearLayout;

    aget v2, v0, v7

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 441
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerOption:Landroid/widget/LinearLayout;

    const/4 v2, 0x5

    aget v2, v0, v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 442
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    const/4 v2, 0x6

    aget v2, v0, v2

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->setVisibility(I)V

    .line 443
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowMode:I

    if-eq v1, v5, :cond_3

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowMode:I

    if-ne v1, v6, :cond_7

    .line 444
    :cond_3
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    invoke-virtual {v1, v3}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->setVisibility(I)V

    .line 445
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    invoke-virtual {v1, v4}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->setPointerMode(Z)V

    goto :goto_0

    .line 397
    :pswitch_0
    sget-object v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$SlideShowStatus;->POINTER_STATUS:[I

    .line 398
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor:I

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->setColor(I)V

    .line 399
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor:I

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->setColor(I)V

    goto :goto_1

    .line 402
    :pswitch_1
    sget-object v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$SlideShowStatus;->POINTER_OPTION_STATUS:[I

    .line 403
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSettings:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_1

    .line 406
    :pswitch_2
    sget-object v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$SlideShowStatus;->MARKER_STATUS:[I

    .line 407
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onChangeMarker()V

    goto :goto_1

    .line 410
    :pswitch_3
    sget-object v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$SlideShowStatus;->MARKER_OPTION_STATUS:[I

    .line 411
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSettings:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setSelected(Z)V

    goto/16 :goto_1

    .line 414
    :pswitch_4
    sget-object v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$SlideShowStatus;->ERASER_STATUS:[I

    .line 415
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEraserMode:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 416
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/16 v2, 0x9

    invoke-virtual {v1, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->ISetPenMode(IZ)V

    goto/16 :goto_1

    .line 419
    :pswitch_5
    sget-object v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$SlideShowStatus;->SELECT_STATUS:[I

    .line 420
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideMode:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 421
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerMode:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 422
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mMarkerMode:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 423
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowMode:I

    if-ne v1, v4, :cond_4

    .line 424
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideMode:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setSelected(Z)V

    goto/16 :goto_1

    .line 425
    :cond_4
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowMode:I

    if-ne v1, v5, :cond_5

    .line 426
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerMode:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setSelected(Z)V

    goto/16 :goto_1

    .line 427
    :cond_5
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowMode:I

    if-eq v1, v7, :cond_6

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowMode:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_2

    .line 428
    :cond_6
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mMarkerMode:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setSelected(Z)V

    goto/16 :goto_1

    .line 447
    :cond_7
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowMode:I

    const/4 v2, 0x7

    if-eq v1, v2, :cond_0

    .line 448
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->setVisibility(I)V

    .line 449
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    invoke-virtual {v1, v3}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->setPointerMode(Z)V

    goto/16 :goto_0

    .line 395
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private onChangeShowMode(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    const/4 v1, 0x0

    .line 337
    packed-switch p1, :pswitch_data_0

    .line 368
    :cond_0
    :goto_0
    return-void

    .line 339
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->setMarker(Z)V

    .line 340
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    const v1, 0x7f02023c

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 344
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->setMarker(Z)V

    .line 345
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 346
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    const v1, 0x7f020265

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 347
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 348
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    const v1, 0x7f020266

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 349
    :cond_2
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 350
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    const v1, 0x7f020263

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 351
    :cond_3
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 352
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    const v1, 0x7f020262

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 353
    :cond_4
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 354
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    const v1, 0x7f020264

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 359
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->setMarker(Z)V

    .line 360
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->setColor(I)V

    .line 361
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 362
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getType()I

    move-result v0

    if-nez v0, :cond_5

    .line 363
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    const v1, 0x7f020261

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 365
    :cond_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    const v1, 0x7f020260

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 337
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private onFinish()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 659
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1, v2, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetPenMode(IZ)V

    .line 660
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 661
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "POINTER_COLOR"

    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 662
    const-string/jumbo v1, "MARKER_TYPE"

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getType()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 663
    const-string/jumbo v1, "MARKER_THICKNESS"

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getThickness()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 664
    const-string/jumbo v1, "MARKER_COLOR"

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getColor()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 665
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->setResult(ILandroid/content/Intent;)V

    .line 666
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->finish()V

    .line 667
    return-void
.end method

.method private onGetPointerColor()Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;
    .locals 2

    .prologue
    .line 323
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 324
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 333
    :goto_0
    return-object v0

    .line 325
    :cond_0
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 326
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto :goto_0

    .line 327
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 328
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto :goto_0

    .line 329
    :cond_2
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 330
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto :goto_0

    .line 331
    :cond_3
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 332
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto :goto_0

    .line 333
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onInitMarkerOption()V
    .locals 7

    .prologue
    .line 313
    const v4, 0x7f0b0212

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    iput-object v4, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    .line 315
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 316
    .local v0, "extras":Landroid/os/Bundle;
    const-string/jumbo v4, "MARKER_TYPE"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 317
    .local v3, "markerType":I
    const-string/jumbo v4, "MARKER_THICKNESS"

    const/16 v5, 0xa

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 318
    .local v2, "markerThickness":I
    const-string/jumbo v4, "MARKER_COLOR"

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f05005a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 319
    .local v1, "markerColor":I
    iget-object v4, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v4, v3, v2, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->initOptions(III)V

    .line 320
    return-void
.end method

.method private onInitPointerOption()V
    .locals 5

    .prologue
    const v4, 0x7f05006c

    .line 294
    const v1, 0x7f0b020c

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerOption:Landroid/widget/LinearLayout;

    .line 295
    const v1, 0x7f0b020d

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 296
    const v1, 0x7f0b020e

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 297
    const v1, 0x7f0b020f

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 298
    const v1, 0x7f0b0210

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 299
    const v1, 0x7f0b0211

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 301
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 302
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05006d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 303
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05006e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 304
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05006f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 305
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050070

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 307
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 308
    .local v0, "extras":Landroid/os/Bundle;
    const-string/jumbo v1, "POINTER_COLOR"

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor:I

    .line 309
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onGetPointerColor()Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setSelected(Z)V

    .line 310
    return-void
.end method


# virtual methods
.method public GetBitmap(II)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 784
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mbSlideShow:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 785
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->getBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 786
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public GetChartThumbnailBitmap(III)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "index"    # I
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    .line 842
    const/4 v0, 0x0

    return-object v0
.end method

.method public GetPageThumbnailBitmap(III)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "nPageNum"    # I
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    .line 771
    const/4 v0, 0x0

    return-object v0
.end method

.method public GetPrintBitmap(II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 816
    const/4 v0, 0x0

    return-object v0
.end method

.method public GetThumbnailBitmap(III)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "nPageNum"    # I
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    .line 765
    const/4 v0, 0x0

    return-object v0
.end method

.method public OnBFinalDrawBitmap(I)V
    .locals 0
    .param p1, "nReserved"    # I

    .prologue
    .line 822
    return-void
.end method

.method public OnCloseDoc()V
    .locals 0

    .prologue
    .line 747
    return-void
.end method

.method public OnDrawBitmap(II)V
    .locals 2
    .param p1, "nCallId"    # I
    .param p2, "bShowAutomap"    # I

    .prologue
    .line 791
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mbSlideShow:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 792
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->drawAllContents()V

    .line 793
    :cond_0
    return-void
.end method

.method public OnDrawGetChartThumbnail(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 849
    return-void
.end method

.method public OnDrawGetPageThumbnail(I)V
    .locals 0
    .param p1, "nPageNum"    # I

    .prologue
    .line 774
    return-void
.end method

.method public OnDrawThumbnailBitmap(I)V
    .locals 0
    .param p1, "nPageNum"    # I

    .prologue
    .line 768
    return-void
.end method

.method public OnEditCopyCut(IIILjava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "caller"    # I
    .param p2, "nType"    # I
    .param p3, "result"    # I
    .param p4, "text"    # Ljava/lang/String;
    .param p5, "data"    # Ljava/lang/String;
    .param p6, "nMode"    # I

    .prologue
    .line 825
    return-void
.end method

.method public OnGetResID(I)Ljava/lang/String;
    .locals 1
    .param p1, "nStrID"    # I

    .prologue
    .line 806
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public OnLoadComplete(I)V
    .locals 0
    .param p1, "bBookmarkExist"    # I

    .prologue
    .line 735
    return-void
.end method

.method public OnLoadFail(I)V
    .locals 0
    .param p1, "EEV_ERROR_CODE"    # I

    .prologue
    .line 744
    return-void
.end method

.method public OnObjectPoints(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V
    .locals 0
    .param p1, "setValue"    # Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;

    .prologue
    .line 810
    return-void
.end method

.method public OnPageMove(III)V
    .locals 0
    .param p1, "nCurrentPage"    # I
    .param p2, "nTotalPage"    # I
    .param p3, "nErrorCode"    # I

    .prologue
    .line 750
    return-void
.end method

.method public OnPptDrawSlidesBitmap(I)V
    .locals 0
    .param p1, "pageNum"    # I

    .prologue
    .line 837
    return-void
.end method

.method public OnPptGetSlidenoteBitmap(II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 718
    const/4 v0, 0x0

    return-object v0
.end method

.method public OnPptGetSlidesBitmap(IIIIZLjava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "bBitmapIamage"    # I
    .param p2, "nPageNum"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I
    .param p5, "isHideSlide"    # Z
    .param p6, "strSlideTitle"    # Ljava/lang/String;

    .prologue
    .line 715
    const/4 v0, 0x0

    return-object v0
.end method

.method public OnPptOnDrawSlidenote(I)V
    .locals 0
    .param p1, "pageNum"    # I

    .prologue
    .line 732
    return-void
.end method

.method public OnPptSlideDelete()V
    .locals 0

    .prologue
    .line 712
    return-void
.end method

.method public OnPptSlideMoveNext()V
    .locals 0

    .prologue
    .line 709
    return-void
.end method

.method public OnPptSlideMovePrev()V
    .locals 0

    .prologue
    .line 706
    return-void
.end method

.method public OnPptSlideShowDrawBitmap()V
    .locals 2

    .prologue
    .line 727
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getConnected()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 728
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->drawAllContents()V

    .line 729
    :cond_0
    return-void
.end method

.method public OnPptSlideShowEffectEnd(I)V
    .locals 0
    .param p1, "nSubType"    # I

    .prologue
    .line 831
    return-void
.end method

.method public OnPptSlideShowGetBitmap(II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 722
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public OnPptSlideexInsert()V
    .locals 0

    .prologue
    .line 703
    return-void
.end method

.method public OnPptSlidenoteStart()V
    .locals 0

    .prologue
    .line 700
    return-void
.end method

.method public OnPrintCompleted(I)V
    .locals 0
    .param p1, "zoomScale"    # I

    .prologue
    .line 819
    return-void
.end method

.method public OnPrintMode(Ljava/lang/String;)V
    .locals 0
    .param p1, "strPrintFile"    # Ljava/lang/String;

    .prologue
    .line 796
    return-void
.end method

.method public OnPrintedCount(I)V
    .locals 0
    .param p1, "nTotalCount"    # I

    .prologue
    .line 799
    return-void
.end method

.method public OnProgress(II)V
    .locals 0
    .param p1, "EV_PROGRESS_TYPE"    # I
    .param p2, "nProgressValue"    # I

    .prologue
    .line 756
    return-void
.end method

.method public OnProgressStart(I)V
    .locals 0
    .param p1, "EV_PROGRESS_TYPE"    # I

    .prologue
    .line 753
    return-void
.end method

.method public OnSearchMode(IIII)V
    .locals 0
    .param p1, "EEV_SEARCH_TYPE"    # I
    .param p2, "nCurrentPage"    # I
    .param p3, "nTotalPage"    # I
    .param p4, "nReplaceAllCount"    # I

    .prologue
    .line 762
    return-void
.end method

.method public OnTerminate()V
    .locals 0

    .prologue
    .line 759
    return-void
.end method

.method public OnTextToSpeachString(Ljava/lang/String;)V
    .locals 0
    .param p1, "strTTS"    # Ljava/lang/String;

    .prologue
    .line 802
    return-void
.end method

.method public OnTotalLoadComplete()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 739
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mbPPSFile:Z

    if-ne v0, v1, :cond_0

    .line 740
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 741
    :cond_0
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 567
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_4

    .line 568
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 569
    .local v0, "rect":Landroid/graphics/Rect;
    const/4 v1, 0x0

    .line 570
    .local v1, "view":Landroid/view/View;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerOption:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v2

    if-nez v2, :cond_2

    .line 571
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerOption:Landroid/widget/LinearLayout;

    .line 575
    :cond_0
    :goto_0
    if-eqz v1, :cond_4

    .line 576
    invoke-virtual {v1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 577
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-nez v2, :cond_4

    .line 578
    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowMode:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_3

    .line 579
    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onChangeMode(I)V

    .line 582
    :cond_1
    :goto_1
    const/4 v2, 0x1

    .line 586
    .end local v0    # "rect":Landroid/graphics/Rect;
    .end local v1    # "view":Landroid/view/View;
    :goto_2
    return v2

    .line 572
    .restart local v0    # "rect":Landroid/graphics/Rect;
    .restart local v1    # "view":Landroid/view/View;
    :cond_2
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 573
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    goto :goto_0

    .line 580
    :cond_3
    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowMode:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_1

    .line 581
    const/4 v2, 0x4

    invoke-direct {p0, v2}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onChangeMode(I)V

    goto :goto_1

    .line 586
    .end local v0    # "rect":Landroid/graphics/Rect;
    .end local v1    # "view":Landroid/view/View;
    :cond_4
    invoke-super {p0, p1}, Lcom/infraware/common/baseactivity/BaseActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    goto :goto_2
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 556
    invoke-super {p0, p1, p2, p3}, Lcom/infraware/common/baseactivity/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 557
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    .line 558
    const/16 v1, 0x15

    if-ne p1, v1, :cond_0

    .line 559
    const-string/jumbo v1, "color_value"

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05005a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 560
    .local v0, "color":I
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v1, v0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->setOtherColor(I)V

    .line 563
    .end local v0    # "color":I
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 629
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mNote:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 630
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mNoteMode:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onClickNote(Landroid/view/View;)V

    .line 656
    :cond_0
    :goto_0
    return-void

    .line 633
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowMode:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_5

    .line 634
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEraserMode:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isSelected()Z

    move-result v0

    if-ne v0, v2, :cond_2

    .line 635
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onChangeMode(I)V

    goto :goto_0

    .line 636
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideMode:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isSelected()Z

    move-result v0

    if-ne v0, v2, :cond_3

    .line 637
    invoke-direct {p0, v2}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onChangeMode(I)V

    goto :goto_0

    .line 638
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerMode:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isSelected()Z

    move-result v0

    if-ne v0, v2, :cond_4

    .line 639
    invoke-direct {p0, v3}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onChangeMode(I)V

    goto :goto_0

    .line 640
    :cond_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mMarkerMode:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isSelected()Z

    move-result v0

    if-ne v0, v2, :cond_0

    .line 641
    invoke-direct {p0, v4}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onChangeMode(I)V

    goto :goto_0

    .line 643
    :cond_5
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowMode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_6

    .line 644
    invoke-direct {p0, v3}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onChangeMode(I)V

    goto :goto_0

    .line 646
    :cond_6
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowMode:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_7

    .line 647
    invoke-direct {p0, v4}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onChangeMode(I)V

    goto :goto_0

    .line 650
    :cond_7
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IIsPenDataForSlideShow()I

    move-result v0

    if-nez v0, :cond_8

    .line 651
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onFinish()V

    goto :goto_0

    .line 653
    :cond_8
    const/16 v0, 0x25

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->showDialog(I)V

    goto :goto_0
.end method

.method public onClickColor(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 491
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onGetPointerColor()Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    move-result-object v0

    .local v0, "view":Landroid/view/View;
    move-object v1, p1

    .line 492
    check-cast v1, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerColor:I

    .line 493
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onChangeMode(I)V

    .line 494
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 495
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/view/View;->setSelected(Z)V

    .line 496
    return-void
.end method

.method public onClickColorPicker(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 499
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 500
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "color_value"

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getColor()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 501
    const/16 v1, 0x15

    invoke-virtual {p0, v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 502
    return-void
.end method

.method public onClickController(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 507
    :try_start_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mControllerFirst:Landroid/widget/ImageView;

    if-ne p1, v1, :cond_1

    .line 508
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->slideShowPlay(I)V

    .line 527
    :cond_0
    :goto_0
    return-void

    .line 509
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mControllerLast:Landroid/widget/ImageView;

    if-ne p1, v1, :cond_2

    .line 510
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->slideShowPlay(I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 524
    :catch_0
    move-exception v0

    .line 525
    .local v0, "npe":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 511
    .end local v0    # "npe":Ljava/lang/NullPointerException;
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mControllerTVOut:Landroid/widget/ImageView;

    if-ne p1, v1, :cond_0

    .line 512
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mbPauseTVOut:Z

    if-ne v1, v2, :cond_3

    .line 513
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mbPauseTVOut:Z

    .line 514
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mControllerTVOut:Landroid/widget/ImageView;

    const v2, 0x7f020258

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 515
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->setConnected(Z)V

    .line 516
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->drawAllContents()V

    goto :goto_0

    .line 519
    :cond_3
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mbPauseTVOut:Z

    .line 520
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mControllerTVOut:Landroid/widget/ImageView;

    const v2, 0x7f020259

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 521
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->setConnected(Z)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public onClickEraserAll(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 475
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISetClearAllPen()V

    .line 476
    return-void
.end method

.method public onClickMode(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x6

    const/4 v2, 0x2

    const/4 v1, 0x4

    .line 454
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    if-ne p1, v0, :cond_1

    .line 455
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onChangeMode(I)V

    .line 472
    :cond_0
    :goto_0
    return-void

    .line 456
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSettings:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_4

    .line 457
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowMode:I

    if-ne v0, v2, :cond_2

    .line 458
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onChangeMode(I)V

    goto :goto_0

    .line 459
    :cond_2
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowMode:I

    if-ne v0, v1, :cond_3

    .line 460
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onChangeMode(I)V

    goto :goto_0

    .line 461
    :cond_3
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowMode:I

    if-ne v0, v3, :cond_0

    .line 462
    invoke-direct {p0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onChangeMode(I)V

    goto :goto_0

    .line 464
    :cond_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEraserMode:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_5

    .line 465
    invoke-direct {p0, v3}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onChangeMode(I)V

    goto :goto_0

    .line 466
    :cond_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideMode:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_6

    .line 467
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onChangeMode(I)V

    goto :goto_0

    .line 468
    :cond_6
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerMode:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_7

    .line 469
    invoke-direct {p0, v2}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onChangeMode(I)V

    goto :goto_0

    .line 470
    :cond_7
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mMarkerMode:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_0

    .line 471
    invoke-direct {p0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onChangeMode(I)V

    goto :goto_0
.end method

.method public onClickNote(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 530
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mNote:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 531
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mNote:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 532
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mNoteMode:Landroid/widget/ImageView;

    const v1, 0x7f02026b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 539
    :goto_0
    return-void

    .line 535
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mNote:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 536
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mNoteMode:Landroid/widget/ImageView;

    const v1, 0x7f02026a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 537
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->getSlideNoteString()V

    goto :goto_0
.end method

.method public onClickPage(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x2

    .line 479
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPrevPage:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_2

    .line 480
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->slideShowPlay(I)V

    .line 484
    :cond_0
    :goto_0
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowMode:I

    if-ne v0, v1, :cond_1

    .line 485
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->onClear()V

    .line 486
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->onClear()V

    .line 488
    :cond_1
    return-void

    .line 481
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mNextPage:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_0

    .line 482
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->slideShowPlay(I)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 17
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 178
    invoke-super/range {p0 .. p1}, Lcom/infraware/common/baseactivity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 180
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 181
    const v1, 0x7f03004d

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->setContentView(I)V

    .line 182
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 183
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v1, :cond_0

    .line 184
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p0

    move-object/from16 v5, p0

    invoke-virtual/range {v1 .. v7}, Lcom/infraware/office/evengine/EvInterface;->ISetListener(Lcom/infraware/office/evengine/EvListener$ViewerListener;Lcom/infraware/office/evengine/EvListener$EditorListener;Lcom/infraware/office/evengine/EvListener$WordEditorListener;Lcom/infraware/office/evengine/EvListener$PptEditorListener;Lcom/infraware/office/evengine/EvListener$SheetEditorListener;Lcom/infraware/office/evengine/EvListener$PdfViewerListener;)V

    .line 187
    :cond_0
    const v1, 0x7f0b0213

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    .line 188
    const v1, 0x7f0b01f6

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    .line 189
    const v1, 0x7f0b01fb

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/PointerDrawView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    .line 190
    const v1, 0x7f0b0203

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPrevPage:Landroid/widget/ImageView;

    .line 191
    const v1, 0x7f0b0205

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mNextPage:Landroid/widget/ImageView;

    .line 193
    const v1, 0x7f0b0208

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mModeOption:Landroid/widget/LinearLayout;

    .line 194
    const v1, 0x7f0b0209

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideMode:Landroid/widget/ImageView;

    .line 195
    const v1, 0x7f0b020a

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerMode:Landroid/widget/ImageView;

    .line 196
    const v1, 0x7f0b020b

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mMarkerMode:Landroid/widget/ImageView;

    .line 198
    const v1, 0x7f0b0217

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mControllerFirst:Landroid/widget/ImageView;

    .line 199
    const v1, 0x7f0b0218

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mControllerLast:Landroid/widget/ImageView;

    .line 200
    const v1, 0x7f0b0219

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mControllerTVOut:Landroid/widget/ImageView;

    .line 201
    const v1, 0x7f0b021a

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mNoteMode:Landroid/widget/ImageView;

    .line 203
    const v1, 0x7f0b01fd

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSettings:Landroid/widget/ImageView;

    .line 204
    const v1, 0x7f0b01fc

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    .line 205
    const v1, 0x7f0b01ff

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEraserAllMode:Landroid/widget/ImageView;

    .line 206
    const v1, 0x7f0b01fe

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEraserMode:Landroid/widget/ImageView;

    .line 208
    const v1, 0x7f0b021b

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mNote:Landroid/widget/LinearLayout;

    .line 209
    const v1, 0x7f0b021c

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mNoteEditText:Landroid/widget/EditText;

    .line 211
    new-instance v1, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowGestureProcListener:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc$SlideShowGestureProcListener;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;-><init>(Landroid/app/Activity;Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc$SlideShowGestureProcListener;Landroid/view/View;Lcom/infraware/polarisoffice5/common/PointerDrawView;Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mGestureProc:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;

    .line 212
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    if-eqz v1, :cond_1

    .line 213
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->setEvInterface(Lcom/infraware/office/evengine/EvInterface;)V

    .line 214
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowSurfaceViewListener:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView$SlideShowSurfaceViewListener;

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->setSlideShowSurfaceViewListener(Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView$SlideShowSurfaceViewListener;)V

    .line 216
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->getIntent()Landroid/content/Intent;

    move-result-object v13

    .line 217
    .local v13, "intent":Landroid/content/Intent;
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v11

    .line 218
    .local v11, "extras":Landroid/os/Bundle;
    const-string/jumbo v1, "START_PAGE_NUM"

    const/4 v2, 0x0

    invoke-virtual {v11, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowStartPage:I

    .line 221
    new-instance v1, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$CloseActionReceiver;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$CloseActionReceiver;-><init>(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$CloseActionReceiver;

    .line 222
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    .line 223
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    const-string/jumbo v2, "com.infraware.polarisviewer5.CLOSE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 224
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$CloseActionReceiver;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 226
    invoke-direct/range {p0 .. p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onInitPointerOption()V

    .line 227
    invoke-direct/range {p0 .. p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onInitMarkerOption()V

    .line 228
    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onChangeMode(I)V

    .line 231
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    .line 232
    .local v14, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    const-string/jumbo v1, "WIDTH"

    const/4 v2, 0x0

    invoke-virtual {v11, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, v14, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 233
    const-string/jumbo v1, "HEIGHT"

    const/4 v2, 0x0

    invoke-virtual {v11, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, v14, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 235
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    if-eqz v1, :cond_2

    .line 236
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    invoke-virtual {v1, v14}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 237
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->setEvInterface(Lcom/infraware/office/evengine/EvInterface;)V

    .line 238
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->setConnected(Z)V

    .line 241
    :cond_2
    new-instance v12, Landroid/content/IntentFilter;

    invoke-direct {v12}, Landroid/content/IntentFilter;-><init>()V

    .line 242
    .local v12, "filter":Landroid/content/IntentFilter;
    const-string/jumbo v1, "HDMI_RESOLUTION"

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_a

    .line 243
    const-string/jumbo v1, "android.intent.action.HDMI_PLUGGED"

    invoke-virtual {v12, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 246
    :cond_3
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v12}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 248
    const-string/jumbo v1, "window"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v10

    .line 249
    .local v10, "display":Landroid/view/Display;
    const-string/jumbo v1, "key_pps_file"

    const/4 v2, 0x0

    invoke-virtual {v13, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mbPPSFile:Z

    .line 250
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mbPPSFile:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_9

    .line 251
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerMode:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v16

    .line 252
    .local v16, "pointerParams":Landroid/view/ViewGroup$LayoutParams;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mMarkerMode:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v15

    .line 253
    .local v15, "markerParams":Landroid/view/ViewGroup$LayoutParams;
    iget v1, v15, Landroid/view/ViewGroup$LayoutParams;->width:I

    move-object/from16 v0, v16

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 254
    iget v1, v15, Landroid/view/ViewGroup$LayoutParams;->height:I

    move-object/from16 v0, v16

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 255
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerMode:Landroid/widget/ImageView;

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 256
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mPointerMode:Landroid/widget/ImageView;

    const v2, 0x7f020269

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 257
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mMarkerMode:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 259
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/infraware/common/util/Utils;->getCurrentLocaleType(Landroid/content/res/Resources;)I

    move-result v6

    .line 260
    .local v6, "locale":I
    const-string/jumbo v1, "key_pps_file_path"

    invoke-virtual {v13, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mFilePath:Ljava/lang/String;

    .line 261
    const/4 v8, 0x0

    .line 262
    .local v8, "tempPath":Ljava/lang/String;
    sget-object v1, Lcom/infraware/common/define/CMModelDefine;->CUSTOM_TEMP_PATH:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_4

    .line 263
    sget-object v1, Lcom/infraware/common/define/CMModelDefine;->CUSTOM_TEMP_PATH:Ljava/lang/String;

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-static {v1, v2, v0}, Lcom/infraware/common/util/FileUtils;->makeDirectory(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 264
    :cond_4
    if-nez v8, :cond_5

    .line 265
    const-string/jumbo v1, "polarisTemp"

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-static {v1, v2, v0}, Lcom/infraware/common/util/FileUtils;->makeDirectory(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_5

    .line 266
    sget-object v1, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->TEMP_PATH:Ljava/lang/String;

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-static {v1, v2, v0}, Lcom/infraware/common/util/FileUtils;->makeDirectory(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_5

    .line 267
    sget-object v8, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->SDROOT_PATH:Ljava/lang/String;

    .line 268
    :cond_5
    invoke-static {v8}, Lcom/infraware/common/util/FileUtils;->addPathDelemeter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 270
    const/4 v9, 0x0

    .line 271
    .local v9, "bookmarkPath":Ljava/lang/String;
    const-string/jumbo v1, ""

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_6

    .line 272
    const-string/jumbo v1, ""

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-static {v1, v2, v0}, Lcom/infraware/common/util/FileUtils;->makeDirectory(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    .line 273
    :cond_6
    if-nez v9, :cond_7

    .line 274
    const-string/jumbo v1, "polarisBookmark"

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-static {v1, v2, v0}, Lcom/infraware/common/util/FileUtils;->makeDirectory(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_7

    .line 275
    sget-object v1, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->BOOKMARK_PATH:Ljava/lang/String;

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-static {v1, v2, v0}, Lcom/infraware/common/util/FileUtils;->makeDirectory(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_7

    .line 276
    sget-object v9, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->SDROOT_PATH:Ljava/lang/String;

    .line 277
    :cond_7
    invoke-static {v9}, Lcom/infraware/common/util/FileUtils;->addPathDelemeter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 279
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v1, :cond_9

    .line 280
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->getNativeInterfaceHandle()I

    move-result v1

    if-nez v1, :cond_8

    .line 281
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IInitInterfaceHandleAddress()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/office/evengine/EvInterface;->setNativeInterfaceHandle(I)V

    .line 282
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->getNativeInterfaceHandle()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/office/evengine/EvInterface;->SetInterfaceHandleAddress(I)V

    .line 284
    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v10}, Landroid/view/Display;->getWidth()I

    move-result v2

    invoke-virtual {v10}, Landroid/view/Display;->getHeight()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IInitialize(II)V

    .line 285
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mFilePath:Ljava/lang/String;

    invoke-virtual {v10}, Landroid/view/Display;->getWidth()I

    move-result v3

    invoke-virtual {v10}, Landroid/view/Display;->getHeight()I

    move-result v4

    const/16 v5, 0x20

    const/4 v7, 0x1

    invoke-virtual/range {v1 .. v9}, Lcom/infraware/office/evengine/EvInterface;->IOpen(Ljava/lang/String;IIIIILjava/lang/String;Ljava/lang/String;)V

    .line 290
    .end local v6    # "locale":I
    .end local v8    # "tempPath":Ljava/lang/String;
    .end local v9    # "bookmarkPath":Ljava/lang/String;
    .end local v15    # "markerParams":Landroid/view/ViewGroup$LayoutParams;
    .end local v16    # "pointerParams":Landroid/view/ViewGroup$LayoutParams;
    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 291
    return-void

    .line 244
    .end local v10    # "display":Landroid/view/Display;
    :cond_a
    const-string/jumbo v1, "WIFIDISPLAY_RESOLUTION"

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 245
    const-string/jumbo v1, "android.intent.action.WIFI_DISPLAY"

    invoke-virtual {v12, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;

    .prologue
    .line 678
    const/16 v0, 0x25

    if-ne p1, v0, :cond_0

    .line 679
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v1

    invoke-direct {v0, p0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f070219

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07006b

    new-instance v2, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$6;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$6;-><init>(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070062

    new-instance v2, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$5;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$5;-><init>(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mConformMsgPopup:Landroid/app/AlertDialog;

    .line 693
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mConformMsgPopup:Landroid/app/AlertDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 694
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mConformMsgPopup:Landroid/app/AlertDialog;

    .line 696
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/infraware/common/baseactivity/BaseActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 614
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 615
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mbPPSFile:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 616
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->DeleteOpenedFileList(Ljava/lang/String;)Z

    .line 617
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IClose()V

    .line 619
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 622
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$CloseActionReceiver;

    if-eqz v0, :cond_1

    .line 623
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$CloseActionReceiver;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 624
    :cond_1
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onDestroy()V

    .line 625
    return-void
.end method

.method public onLocaleChange(I)V
    .locals 0
    .param p1, "nLocale"    # I

    .prologue
    .line 813
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 591
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onPause()V

    .line 592
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    if-eqz v0, :cond_0

    .line 593
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->setConnected(Z)V

    .line 594
    :cond_0
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 671
    invoke-super {p0, p1, p2}, Lcom/infraware/common/baseactivity/BaseActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 672
    const/16 v0, 0x25

    if-ne p1, v0, :cond_0

    .line 673
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->removeDialog(I)V

    .line 674
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 598
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onResume()V

    .line 601
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_1

    .line 602
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mbPPSFile:Z

    if-ne v0, v7, :cond_0

    .line 603
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->getNativeInterfaceHandle()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->SetInterfaceHandleAddress(I)V

    .line 605
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object v1, p0

    move-object v3, v2

    move-object v4, p0

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->ISetListener(Lcom/infraware/office/evengine/EvListener$ViewerListener;Lcom/infraware/office/evengine/EvListener$EditorListener;Lcom/infraware/office/evengine/EvListener$WordEditorListener;Lcom/infraware/office/evengine/EvListener$PptEditorListener;Lcom/infraware/office/evengine/EvListener$SheetEditorListener;Lcom/infraware/office/evengine/EvListener$PdfViewerListener;)V

    .line 608
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mbPauseTVOut:Z

    if-nez v0, :cond_2

    .line 609
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    invoke-virtual {v0, v7}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->setConnected(Z)V

    .line 610
    :cond_2
    return-void
.end method

.method public slideShowPlay(I)V
    .locals 2
    .param p1, "a_PlayType"    # I

    .prologue
    .line 543
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    .line 544
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvInterface;->ISlideShowPlay(I)V

    .line 545
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 546
    return-void
.end method
