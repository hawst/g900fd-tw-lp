.class public interface abstract Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer$SlideShowGLRendererListener;
.super Ljava/lang/Object;
.source "SlideShowGLRenderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SlideShowGLRendererListener"
.end annotation


# virtual methods
.method public abstract onContinue()V
.end method

.method public abstract onPlay(II)V
.end method

.method public abstract onStart(II)V
.end method

.method public abstract onUpdateSeekBar()V
.end method
