.class Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$6;
.super Ljava/lang/Object;
.source "SlideShowActivity.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView$SlideShowGLSurfaceViewListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V
    .locals 0

    .prologue
    .line 645
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$6;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSurfaceChanged(II)V
    .locals 0
    .param p1, "oldHeight"    # I
    .param p2, "newHeight"    # I

    .prologue
    .line 652
    return-void
.end method

.method public onSurfaceCreated(II)V
    .locals 3
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 647
    const-string/jumbo v0, "SlideShowActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "SlideShowGLSurfaceViewListener width = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "height = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$6;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$600(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1, p2}, Lcom/infraware/office/evengine/EvInterface;->IChangeScreen(III)V

    .line 649
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$6;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$200(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->myRenderer:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->onStartSlideShow(II)V

    .line 650
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$6;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$200(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->requestRender()V

    .line 651
    return-void
.end method

.method public onSurfaceDestroyed()V
    .locals 0

    .prologue
    .line 655
    return-void
.end method
