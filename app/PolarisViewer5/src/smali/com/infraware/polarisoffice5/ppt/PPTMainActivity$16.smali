.class Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$16;
.super Ljava/lang/Object;
.source "PPTMainActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onClickSlideShow(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V
    .locals 0

    .prologue
    .line 2228
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$16;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 2231
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$16;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$900(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 2232
    invoke-static {p0}, Lcom/infraware/common/define/CMModelDefine$B;->USE_EXTERNAL_DISPLAY(Landroid/view/View$OnClickListener;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$16;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mHDMIResolution:I
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$1000(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$16;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mWIFIDisplayResolution:I
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$1100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 2233
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$16;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$16;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v1

    iget v1, v1, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    # invokes: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->showSlideShowSettingsDialog(I)V
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2500(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;I)V

    .line 2236
    :goto_0
    return-void

    .line 2235
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$16;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$16;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v1

    iget v1, v1, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    # invokes: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onSlideShowActivity(I)V
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$1500(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;I)V

    goto :goto_0
.end method
