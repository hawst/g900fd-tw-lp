.class Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$7;
.super Ljava/lang/Object;
.source "SlideShowActivity.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView$SlideShowSurfaceViewListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V
    .locals 0

    .prologue
    .line 658
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$7;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSurfaceChanged(II)V
    .locals 3
    .param p1, "oldHeight"    # I
    .param p2, "newHeight"    # I

    .prologue
    const/4 v2, 0x1

    .line 668
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$7;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$600(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IIsNextEffect()Z

    move-result v0

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$7;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->IsAutoSwitch()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$7;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$400(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$7;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$400(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 671
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$7;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$2400(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 672
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$7;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # setter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bChangeScreenForSlideShow:Z
    invoke-static {v0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$002(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;Z)Z

    .line 673
    return-void
.end method

.method public onSurfaceCreated(II)V
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 660
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$7;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$2400(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 661
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$7;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbFinishCalled:Z
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$2300(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$7;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$600(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IIsNextEffect()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$7;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->IsAutoSwitch()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$7;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$400(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$7;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$400(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 665
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$7;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$2400(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 666
    :cond_0
    return-void
.end method

.method public onWindowFocusChanged()V
    .locals 0

    .prologue
    .line 681
    return-void
.end method
