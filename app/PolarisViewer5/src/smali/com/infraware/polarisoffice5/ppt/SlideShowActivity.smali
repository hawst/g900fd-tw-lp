.class public Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;
.super Lcom/infraware/common/baseactivity/BaseActivity;
.source "SlideShowActivity.java"

# interfaces
.implements Lcom/infraware/office/baseframe/EvBaseE$BaseActivityEventType;
.implements Lcom/infraware/office/baseframe/EvBaseE$PopupDialogEventType;
.implements Lcom/infraware/office/evengine/E$EV_DOCEXTENSION_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_ERROR_CODE;
.implements Lcom/infraware/office/evengine/E$EV_MOVE_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_PEN_MODE;
.implements Lcom/infraware/office/evengine/E$EV_SLIDESHOW_PLAY_TYPE;
.implements Lcom/infraware/office/evengine/EvListener$EditorListener;
.implements Lcom/infraware/office/evengine/EvListener$PptEditorListener;
.implements Lcom/infraware/office/evengine/EvListener$ViewerListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$CloseActionReceiver;,
        Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$NfcCallback;,
        Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;,
        Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$SlideShowStatus;,
        Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$SlideShowMode;
    }
.end annotation


# static fields
.field public static final MSG_CHANGE_MODE:I = 0x7

.field public static final MSG_GET_THUMBNAIL_FROM_ENGINE:I = 0x6

.field public static final MSG_GL_VIEW_MODE:I = 0x4

.field public static final MSG_NEXT_PLAY:I = 0x6

.field public static final MSG_NORMAL_VIEW_MODE:I = 0x3

.field public static final MSG_SET_ERASE:I = 0x5

.field public static final MSG_SHOW_GL_VIEW:I = 0x2

.field public static final MSG_SHOW_NORMAL_VIEW:I = 0x1

.field public static final MSG_SLIDE_END:I = 0x8

.field public static final MSG_STOP_AUTO_SWITCH:I = 0x9

.field private static mClickLock:Ljava/lang/Object;

.field private static mIsClicking:Z


# instance fields
.field private AutoSwitchTimer:Landroid/os/CountDownTimer;

.field private final LOG_CAT:Ljava/lang/String;

.field private ModeSwitchTimer:Landroid/os/CountDownTimer;

.field private isPenDownNoUp:Z

.field private isTouchCancel:Z

.field private mClose:Landroid/widget/ImageView;

.field private mCmdType:I

.field private mConformMsgPopup:Landroid/app/AlertDialog;

.field private mDlgId:I

.field private mEraserAllMode:Landroid/widget/ImageView;

.field private mEraserMode:Landroid/widget/ImageView;

.field private mErrorAndClosePopup:Landroid/app/AlertDialog;

.field private mEvInterface:Lcom/infraware/office/evengine/EvInterface;

.field private mFilePath:Ljava/lang/String;

.field private mGLGestureProc:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;

.field private mGestureProc:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;

.field private mHandler:Landroid/os/Handler;

.field private mHandlerLoop:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;

.field private mImageEraseAll:Landroid/widget/ImageView;

.field private mImageEraser:Landroid/widget/ImageView;

.field private mImageMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

.field private mImageSettings:Landroid/widget/ImageView;

.field private mInterfaceHandleAddress:I

.field private mLastOrientation:I

.field private mLastSlideImage:Landroid/view/SurfaceView;

.field private mLayoutImageMode:Landroid/widget/LinearLayout;

.field private mMarkerMode:Landroid/widget/ImageView;

.field private mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

.field private mModeOption:Landroid/widget/LinearLayout;

.field private mNextPage:Landroid/widget/ImageView;

.field private mNfcAdapter:Landroid/nfc/NfcAdapter;

.field private mNfcCallback:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$NfcCallback;

.field private mOnClickListener:Landroid/view/View$OnTouchListener;

.field private mOnLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

.field private mPointerColor:I

.field private mPointerColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mPointerColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mPointerColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mPointerColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mPointerColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

.field private mPointerMode:Landroid/widget/ImageView;

.field private mPointerOption:Landroid/widget/LinearLayout;

.field private mPrevPage:Landroid/widget/ImageView;

.field private mPreviewImageFrameLayout:Landroid/widget/FrameLayout;

.field private mPreviewSeekBar:Landroid/widget/SeekBar;

.field private mPreviewSeekbarListener:Landroid/view/View$OnTouchListener;

.field private mRemovedViewQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mSBeamEnabled:Z

.field private mSaveFilePath:Ljava/lang/String;

.field private mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

.field private mSeekBarMoveHistory:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSettings:Landroid/widget/ImageView;

.field private mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

.field private mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

.field private mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

.field private mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

.field private mSlideImageHeight:I

.field private mSlideImageWidth:I

.field private mSlideMode:Landroid/widget/ImageView;

.field private mSlideShowFinished:Landroid/widget/TextView;

.field private mSlideShowGLRendererListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer$SlideShowGLRendererListener;

.field private mSlideShowGLSurfaceViewListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView$SlideShowGLSurfaceViewListener;

.field private mSlideShowGestureProcListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$SlideShowGestureProcListener;

.field private mSlideShowMain:Landroid/widget/RelativeLayout;

.field private mSlideShowMode:I

.field private mSlideShowStartPage:I

.field private mSlideShowSurfaceViewListener:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView$SlideShowSurfaceViewListener;

.field private mTempPath:Ljava/lang/String;

.field private mTotalPageNum:I

.field private mVideo:Landroid/widget/VideoView;

.field private mVideobtn:Landroid/widget/ImageView;

.field private m_OrientationPortrait:Z

.field private m_VideoFrame:Landroid/widget/RelativeLayout;

.field private m_bByMultipleLauncher:Z

.field private volatile m_bChangeScreenForSlideShow:Z

.field m_bCheckAutoSwitchAndMode:Z

.field m_bIsAutoModeSwitch:Z

.field private m_bIsMakerDrawing:Z

.field private m_bIsSeekbarDragging:Z

.field private m_bIsUseGLES:Z

.field private m_bRestart:Z

.field m_bSlideModeState:I

.field m_bStateLastSlide:Z

.field private m_bSupportSlideShow:Z

.field private m_oCloseFilter:Landroid/content/IntentFilter;

.field private m_oCloseReceiver:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$CloseActionReceiver;

.field private m_orientation:I

.field private mbChangeDocument:Z

.field private mbDeletePenData:Z

.field private mbFinish:Z

.field private mbFinishCalled:Z

.field private mbPPSFile:Z

.field private mbSaveAndFinish:Z

.field private mbSaving:Z

.field private mbSlideShow:Z

.field private mnPageJump:I

.field private onLastSlideTouchListener:Landroid/view/View$OnTouchListener;

.field private onModeTouchupListener:Landroid/view/View$OnTouchListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mClickLock:Ljava/lang/Object;

    .line 97
    const/4 v0, 0x0

    sput-boolean v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mIsClicking:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const-wide/16 v4, 0x3e8

    const/4 v2, 0x1

    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 92
    invoke-direct {p0}, Lcom/infraware/common/baseactivity/BaseActivity;-><init>()V

    .line 98
    const-string/jumbo v0, "SlideShowActivity"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->LOG_CAT:Ljava/lang/String;

    .line 99
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMain:Landroid/widget/RelativeLayout;

    .line 100
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 101
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mGestureProc:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;

    .line 102
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mGLGestureProc:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;

    .line 104
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsUseGLES:Z

    .line 105
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    .line 106
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    .line 107
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    .line 108
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    .line 109
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSettings:Landroid/widget/ImageView;

    .line 110
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserAllMode:Landroid/widget/ImageView;

    .line 111
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserMode:Landroid/widget/ImageView;

    .line 113
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    .line 114
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewImageFrameLayout:Landroid/widget/FrameLayout;

    .line 115
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    .line 116
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mRemovedViewQueue:Ljava/util/Queue;

    .line 117
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSeekBarMoveHistory:Ljava/util/Stack;

    .line 119
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNextPage:Landroid/widget/ImageView;

    .line 120
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPrevPage:Landroid/widget/ImageView;

    .line 121
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mClose:Landroid/widget/ImageView;

    .line 123
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mModeOption:Landroid/widget/LinearLayout;

    .line 124
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideMode:Landroid/widget/ImageView;

    .line 125
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerMode:Landroid/widget/ImageView;

    .line 126
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerMode:Landroid/widget/ImageView;

    .line 128
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerOption:Landroid/widget/LinearLayout;

    .line 129
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 130
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 131
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 132
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 133
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 134
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    .line 136
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mErrorAndClosePopup:Landroid/app/AlertDialog;

    .line 137
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mConformMsgPopup:Landroid/app/AlertDialog;

    .line 138
    iput v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor:I

    .line 139
    iput v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    .line 140
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mFilePath:Ljava/lang/String;

    .line 141
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTempPath:Ljava/lang/String;

    .line 142
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbPPSFile:Z

    .line 143
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbSlideShow:Z

    .line 144
    iput v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mInterfaceHandleAddress:I

    .line 145
    iput v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mCmdType:I

    .line 147
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 148
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$NfcCallback;

    .line 149
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSBeamEnabled:Z

    .line 151
    iput v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTotalPageNum:I

    .line 152
    iput v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowStartPage:I

    .line 154
    iput v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImageWidth:I

    .line 155
    iput v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImageHeight:I

    .line 157
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mImageMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    .line 158
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mLayoutImageMode:Landroid/widget/LinearLayout;

    .line 159
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mImageSettings:Landroid/widget/ImageView;

    .line 160
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mImageEraser:Landroid/widget/ImageView;

    .line 161
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mImageEraseAll:Landroid/widget/ImageView;

    .line 163
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSaveFilePath:Ljava/lang/String;

    .line 165
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbSaving:Z

    .line 166
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbSaveAndFinish:Z

    .line 167
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbChangeDocument:Z

    .line 168
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbDeletePenData:Z

    .line 169
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbFinish:Z

    .line 171
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mLastOrientation:I

    .line 173
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsSeekbarDragging:Z

    .line 175
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_VideoFrame:Landroid/widget/RelativeLayout;

    .line 176
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mVideobtn:Landroid/widget/ImageView;

    .line 177
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mVideo:Landroid/widget/VideoView;

    .line 179
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bSupportSlideShow:Z

    .line 181
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_OrientationPortrait:Z

    .line 182
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bChangeScreenForSlideShow:Z

    .line 184
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bRestart:Z

    .line 186
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    .line 187
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 188
    iput v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mnPageJump:I

    .line 190
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mLastSlideImage:Landroid/view/SurfaceView;

    .line 191
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowFinished:Landroid/widget/TextView;

    .line 192
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bStateLastSlide:Z

    .line 193
    iput v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bSlideModeState:I

    .line 195
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bCheckAutoSwitchAndMode:Z

    .line 197
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsAutoModeSwitch:Z

    .line 200
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbFinishCalled:Z

    .line 201
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bByMultipleLauncher:Z

    .line 202
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$CloseActionReceiver;

    .line 203
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    .line 205
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsMakerDrawing:Z

    .line 209
    iput v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_orientation:I

    .line 211
    iput v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mDlgId:I

    .line 247
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$1;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mHandler:Landroid/os/Handler;

    .line 318
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mHandlerLoop:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;

    .line 399
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$3;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$3;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mOnClickListener:Landroid/view/View$OnTouchListener;

    .line 431
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$4;

    const-wide/16 v2, 0xbb8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$4;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;JJ)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->ModeSwitchTimer:Landroid/os/CountDownTimer;

    .line 455
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$5;

    const-wide/16 v2, 0x1388

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$5;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;JJ)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->AutoSwitchTimer:Landroid/os/CountDownTimer;

    .line 645
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$6;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$6;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowGLSurfaceViewListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView$SlideShowGLSurfaceViewListener;

    .line 658
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$7;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$7;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowSurfaceViewListener:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView$SlideShowSurfaceViewListener;

    .line 684
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$8;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$8;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowGLRendererListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer$SlideShowGLRendererListener;

    .line 726
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$9;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$9;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowGestureProcListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$SlideShowGestureProcListener;

    .line 738
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$10;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$10;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekbarListener:Landroid/view/View$OnTouchListener;

    .line 1114
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->isTouchCancel:Z

    .line 1115
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->isPenDownNoUp:Z

    .line 2937
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$25;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$25;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onLastSlideTouchListener:Landroid/view/View$OnTouchListener;

    .line 3041
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$26;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$26;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onModeTouchupListener:Landroid/view/View$OnTouchListener;

    .line 3072
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$27;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$27;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mOnLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bChangeScreenForSlideShow:Z

    return v0
.end method

.method static synthetic access$002(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bChangeScreenForSlideShow:Z

    return p1
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerMode:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserMode:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserAllMode:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mGestureProc:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mGLGestureProc:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Ljava/util/Stack;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSeekBarMoveHistory:Ljava/util/Stack;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImageWidth:I

    return v0
.end method

.method static synthetic access$1900(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImageHeight:I

    return v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNextPage:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPrevPage:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/os/CountDownTimer;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->ModeSwitchTimer:Landroid/os/CountDownTimer;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbFinishCalled:Z

    return v0
.end method

.method static synthetic access$2400(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2502(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbSlideShow:Z

    return p1
.end method

.method static synthetic access$2600(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowStartPage:I

    return v0
.end method

.method static synthetic access$2700(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->changeScreenForSlideShow()V

    return-void
.end method

.method static synthetic access$2800(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->cancelModeSwitchTimer()V

    return-void
.end method

.method static synthetic access$2900(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsSeekbarDragging:Z

    return v0
.end method

.method static synthetic access$2902(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsSeekbarDragging:Z

    return p1
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/os/CountDownTimer;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->AutoSwitchTimer:Landroid/os/CountDownTimer;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMain:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_OrientationPortrait:Z

    return v0
.end method

.method static synthetic access$3300(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSettings:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/common/util/SbeamHelper;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideMode:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerMode:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$3802(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbDeletePenData:Z

    return p1
.end method

.method static synthetic access$3900(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onFinish()V

    return-void
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    return v0
.end method

.method static synthetic access$402(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;
    .param p1, "x1"    # I

    .prologue
    .line 92
    iput p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    return p1
.end method

.method static synthetic access$4100(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSBeamEnabled:Z

    return v0
.end method

.method static synthetic access$4102(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSBeamEnabled:Z

    return p1
.end method

.method static synthetic access$4200(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsUseGLES:Z

    return v0
.end method

.method static synthetic access$4300(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setSlideVisibility()V

    return-void
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setLastSlideVisibility()V

    return-void
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/office/evengine/EvInterface;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    return-object v0
.end method

.method static synthetic access$700(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$800(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewImageFrameLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$900(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/polarisoffice5/common/PointerDrawView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    return-object v0
.end method

.method private cancelModeSwitchTimer()V
    .locals 1

    .prologue
    .line 450
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsAutoModeSwitch:Z

    .line 451
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->ModeSwitchTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 452
    return-void
.end method

.method private changeScreenForSlideShow()V
    .locals 2

    .prologue
    .line 2391
    const-string/jumbo v0, "SlideShowActivity"

    const-string/jumbo v1, "changeScreenForSlideShow()"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2392
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bChangeScreenForSlideShow:Z

    if-eqz v0, :cond_0

    .line 2395
    sget-object v1, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mClickLock:Ljava/lang/Object;

    monitor-enter v1

    .line 2396
    :try_start_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISetPPTSlideGLSync()V

    .line 2397
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bChangeScreenForSlideShow:Z

    .line 2398
    monitor-exit v1

    .line 2401
    :cond_0
    return-void

    .line 2398
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getInterfaceHandle()I
    .locals 2

    .prologue
    .line 1099
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mCmdType:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1100
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mInterfaceHandleAddress:I

    .line 1102
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->getNativeInterfaceHandle()I

    move-result v0

    goto :goto_0
.end method

.method private onChangeMarker()V
    .locals 4

    .prologue
    .line 1416
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getThickness()I

    move-result v1

    mul-int/lit8 v1, v1, 0x48

    div-int/lit8 v0, v1, 0xa

    .line 1417
    .local v0, "thickness":I
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->ISetPenMode(IZ)V

    .line 1418
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1, v0}, Lcom/infraware/office/evengine/EvInterface;->ISetPenSize(I)V

    .line 1419
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getColor()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetPenColor(I)V

    .line 1420
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getTransparency()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetPenTransparency(I)V

    .line 1422
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IEditPageRedrawBitmap()V

    .line 1423
    return-void
.end method

.method private onChangeShowMode(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 1392
    packed-switch p1, :pswitch_data_0

    .line 1411
    :goto_0
    return-void

    .line 1395
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    const v1, 0x7f02023b

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->setImageResource(I)V

    goto :goto_0

    .line 1400
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    const v1, 0x7f020227

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->setImageResource(I)V

    .line 1401
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->showModeButtonUpdate()V

    goto :goto_0

    .line 1407
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->setColor(I)V

    .line 1408
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    const v1, 0x7f020223

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->setImageResource(I)V

    goto :goto_0

    .line 1392
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private onFinish()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 2136
    const-string/jumbo v1, "SlideShowActivity"

    const-string/jumbo v2, "onFinish"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2138
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IStopSlideEffect()V

    .line 2140
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mVideo:Landroid/widget/VideoView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mVideo:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2141
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mVideo:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->stopPlayback()V

    .line 2143
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mVideo:Landroid/widget/VideoView;

    .line 2145
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1, v3, v3}, Lcom/infraware/office/evengine/EvInterface;->ISetPenMode(IZ)V

    .line 2146
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbPPSFile:Z

    if-ne v1, v5, :cond_2

    .line 2148
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTempPath:Ljava/lang/String;

    sget-object v2, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->OFFICE_ROOT_PATH:Ljava/lang/String;

    if-eq v1, v2, :cond_1

    .line 2149
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTempPath:Ljava/lang/String;

    invoke-static {v1, v5}, Lcom/infraware/common/util/FileUtils;->deleteDirectory(Ljava/lang/String;Z)V

    .line 2151
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/infraware/office/evengine/EvInterface;->DeleteOpenedFileList(Ljava/lang/String;)Z

    .line 2152
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IClose()V

    .line 2155
    :cond_2
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mCmdType:I

    if-ne v1, v4, :cond_3

    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbPPSFile:Z

    if-eqz v1, :cond_3

    .line 2157
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mInterfaceHandleAddress:I

    invoke-virtual {v1, v2}, Lcom/infraware/office/evengine/EvInterface;->deleteInterfaceHandle(I)V

    .line 2158
    iput v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mInterfaceHandleAddress:I

    .line 2161
    :cond_3
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbPPSFile:Z

    if-nez v1, :cond_4

    .line 2163
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2164
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "POINTER_COLOR"

    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2165
    const-string/jumbo v1, "MARKER_TYPE"

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getType()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2166
    const-string/jumbo v1, "MARKER_THICKNESS"

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getThickness()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2167
    const-string/jumbo v1, "MARKER_COLOR"

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getColor()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2168
    invoke-virtual {p0, v4, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setResult(ILandroid/content/Intent;)V

    .line 2171
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_4
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 2172
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->finish()V

    .line 2173
    return-void
.end method

.method private onGetPointerColor()Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;
    .locals 2

    .prologue
    .line 1365
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 1366
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 1375
    :goto_0
    return-object v0

    .line 1367
    :cond_0
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 1368
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto :goto_0

    .line 1369
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 1370
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto :goto_0

    .line 1371
    :cond_2
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 1372
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto :goto_0

    .line 1373
    :cond_3
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 1374
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto :goto_0

    .line 1375
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onInitMarkerOption()V
    .locals 7

    .prologue
    .line 1306
    const v4, 0x7f0b0212

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    iput-object v4, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    .line 1308
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1309
    .local v0, "extras":Landroid/os/Bundle;
    const-string/jumbo v4, "MARKER_TYPE"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 1310
    .local v3, "markerType":I
    const-string/jumbo v4, "MARKER_THICKNESS"

    const/16 v5, 0xa

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 1311
    .local v2, "markerThickness":I
    const-string/jumbo v4, "MARKER_COLOR"

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f05005a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 1312
    .local v1, "markerColor":I
    iget-object v4, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v4, v3, v2, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->initOptions(III)V

    .line 1314
    iget-object v4, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-direct {p0, v4}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setOptionPosition(Ljava/lang/Object;)V

    .line 1315
    return-void
.end method

.method private onInitModeOption()V
    .locals 2

    .prologue
    .line 1235
    const v0, 0x7f0b0208

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mModeOption:Landroid/widget/LinearLayout;

    .line 1236
    const v0, 0x7f0b0209

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideMode:Landroid/widget/ImageView;

    .line 1237
    const v0, 0x7f0b020a

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerMode:Landroid/widget/ImageView;

    .line 1238
    const v0, 0x7f0b020b

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerMode:Landroid/widget/ImageView;

    .line 1241
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideMode:Landroid/widget/ImageView;

    new-instance v1, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$16;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$16;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1252
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerMode:Landroid/widget/ImageView;

    new-instance v1, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$17;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$17;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1263
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerMode:Landroid/widget/ImageView;

    new-instance v1, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$18;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$18;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1276
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideMode:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onModeTouchupListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1277
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerMode:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onModeTouchupListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1278
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerMode:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onModeTouchupListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1280
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mModeOption:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setOptionPosition(Ljava/lang/Object;)V

    .line 1281
    return-void
.end method

.method private onInitPointerOption()V
    .locals 5

    .prologue
    const v4, 0x7f05006c

    .line 1284
    const v1, 0x7f0b020c

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerOption:Landroid/widget/LinearLayout;

    .line 1286
    const v1, 0x7f0b020d

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 1287
    const v1, 0x7f0b020e

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 1288
    const v1, 0x7f0b020f

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 1289
    const v1, 0x7f0b0210

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 1290
    const v1, 0x7f0b0211

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 1292
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 1293
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05006d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 1294
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05006e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 1295
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05006f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 1296
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050070

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 1298
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1299
    .local v0, "extras":Landroid/os/Bundle;
    const-string/jumbo v1, "POINTER_COLOR"

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor:I

    .line 1300
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onGetPointerColor()Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setSelected(Z)V

    .line 1302
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerOption:Landroid/widget/LinearLayout;

    invoke-direct {p0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setOptionPosition(Ljava/lang/Object;)V

    .line 1303
    return-void
.end method

.method private onResultPassWordActivity(Landroid/content/Intent;)V
    .locals 4
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 1751
    const-string/jumbo v1, "key_new_file"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1752
    .local v0, "pass":Ljava/lang/String;
    const-string/jumbo v1, "exit"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1753
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->finish()V

    .line 1757
    :goto_0
    return-void

    .line 1756
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mFilePath:Ljava/lang/String;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTempPath:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Lcom/infraware/office/evengine/EvInterface;->IOpenEx(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setChangescreenbtn()V
    .locals 9

    .prologue
    const v8, 0x7f0b01fe

    const v7, 0x7f0b01fd

    const v6, 0x7f0b01fc

    const/4 v5, 0x1

    const/4 v4, -0x2

    .line 1050
    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_OrientationPortrait:Z

    if-ne v3, v5, :cond_0

    .line 1052
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1053
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1054
    .local v1, "layoutParams_1":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1056
    .local v2, "layoutParams_2":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1057
    const/16 v3, 0xa

    invoke-virtual {v0, v3, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1059
    invoke-virtual {v1, v5, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1060
    const/16 v3, 0xa

    invoke-virtual {v1, v3, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1062
    invoke-virtual {v2, v5, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1063
    const/16 v3, 0xa

    invoke-virtual {v2, v3, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1066
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSettings:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1067
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserMode:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1068
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserAllMode:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1093
    :goto_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSettings:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->requestLayout()V

    .line 1094
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserMode:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->requestLayout()V

    .line 1095
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserAllMode:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->requestLayout()V

    .line 1096
    return-void

    .line 1074
    .end local v0    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v1    # "layoutParams_1":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v2    # "layoutParams_2":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1075
    .restart local v0    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1076
    .restart local v1    # "layoutParams_1":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1078
    .restart local v2    # "layoutParams_2":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v3, 0x3

    invoke-virtual {v0, v3, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1079
    const/16 v3, 0x9

    invoke-virtual {v0, v3, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1081
    const/4 v3, 0x3

    invoke-virtual {v1, v3, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1082
    const/16 v3, 0x9

    invoke-virtual {v1, v3, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1084
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1085
    const/16 v3, 0x9

    invoke-virtual {v2, v3, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1087
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSettings:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1088
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserMode:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1089
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserAllMode:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private setLastSlideVisibility()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2984
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->setVisibility(I)V

    .line 2987
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowFinished:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2988
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mLastSlideImage:Landroid/view/SurfaceView;

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setVisibility(I)V

    .line 2990
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bSlideModeState:I

    .line 2991
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    .line 2993
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bStateLastSlide:Z

    .line 2995
    return-void
.end method

.method private setNfcCallback()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2747
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2755
    :cond_0
    :goto_0
    return-void

    .line 2749
    :cond_1
    invoke-static {p0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 2750
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    if-eqz v0, :cond_0

    .line 2751
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$NfcCallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$NfcCallback;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$1;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$NfcCallback;

    .line 2752
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$NfcCallback;

    new-array v2, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p0, v2}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 2753
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$NfcCallback;

    new-array v2, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p0, v2}, Landroid/nfc/NfcAdapter;->setOnNdefPushCompleteCallback(Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    goto :goto_0
.end method

.method private setOptionPosition(Ljava/lang/Object;)V
    .locals 8
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x1

    const v5, 0x43808000    # 257.0f

    const/4 v4, -0x2

    const v3, 0x7f0b01fd

    .line 1319
    instance-of v1, p1, Landroid/widget/LinearLayout;

    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerOption:Landroid/widget/LinearLayout;

    move-object v1, p1

    check-cast v1, Landroid/widget/LinearLayout;

    if-ne v2, v1, :cond_2

    .line 1321
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_OrientationPortrait:Z

    if-nez v1, :cond_1

    .line 1323
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1324
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v6, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1326
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->getHeight()I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1327
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1328
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerOption:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1362
    .end local v0    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local p1    # "object":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-void

    .line 1332
    .restart local p1    # "object":Ljava/lang/Object;
    :cond_1
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1333
    .restart local v0    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v7, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1335
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->getWidth()I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1336
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1337
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerOption:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 1340
    .end local v0    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    instance-of v1, p1, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    check-cast p1, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    .end local p1    # "object":Ljava/lang/Object;
    if-ne v1, p1, :cond_0

    .line 1343
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_OrientationPortrait:Z

    if-nez v1, :cond_3

    .line 1345
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-static {p0, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v1

    invoke-direct {v0, v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1346
    .restart local v0    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v6, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1348
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->getHeight()I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1349
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1350
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v1, v0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 1354
    .end local v0    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-static {p0, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v1

    invoke-direct {v0, v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1356
    .restart local v0    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->getWidth()I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1357
    invoke-virtual {v0, v7, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1358
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1359
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v1, v0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private setOrientation(I)V
    .locals 1
    .param p1, "orientation"    # I

    .prologue
    .line 474
    if-nez p1, :cond_0

    .line 475
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setRequestedOrientation(I)V

    .line 480
    :goto_0
    return-void

    .line 476
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 477
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 479
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setRequestedOrientation(I)V

    goto :goto_0
.end method

.method private setSlideVisibility()V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 2969
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->setVisibility(I)V

    .line 2970
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowFinished:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2971
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mLastSlideImage:Landroid/view/SurfaceView;

    invoke-virtual {v0, v2}, Landroid/view/SurfaceView;->setVisibility(I)V

    .line 2973
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bSlideModeState:I

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    .line 2975
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bStateLastSlide:Z

    .line 2977
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v4, v1}, Lcom/infraware/office/evengine/EvInterface;->IIsNoneEffect(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2978
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->changeScreenForSlideShow()V

    .line 2979
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v2

    iget v2, v2, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/infraware/office/evengine/EvInterface;->ISlideShowPlay(IIIZ)V

    .line 2981
    :cond_0
    return-void
.end method

.method private showModeButtonUpdate()V
    .locals 2

    .prologue
    .line 1379
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 1380
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    const v1, 0x7f020233

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->setImageResource(I)V

    .line 1389
    :goto_0
    return-void

    .line 1381
    :cond_0
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 1382
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    const v1, 0x7f02022b

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->setImageResource(I)V

    goto :goto_0

    .line 1383
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 1384
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    const v1, 0x7f020228

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->setImageResource(I)V

    goto :goto_0

    .line 1385
    :cond_2
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 1386
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    const v1, 0x7f020230

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->setImageResource(I)V

    goto :goto_0

    .line 1388
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    const v1, 0x7f020227

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->setImageResource(I)V

    goto :goto_0
.end method


# virtual methods
.method protected AlertErrorAndCloseDlg(I)V
    .locals 4
    .param p1, "contentStringId"    # I

    .prologue
    .line 2560
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2562
    .local v1, "content":Ljava/lang/String;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v2

    invoke-direct {v0, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 2563
    .local v0, "ab":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 2564
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 2565
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070063

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$22;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$22;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2572
    new-instance v2, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$23;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$23;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 2579
    new-instance v2, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$24;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$24;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 2590
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mErrorAndClosePopup:Landroid/app/AlertDialog;

    .line 2591
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mErrorAndClosePopup:Landroid/app/AlertDialog;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 2592
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mErrorAndClosePopup:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 2593
    return-void
.end method

.method public CheckSaveDocType(I)I
    .locals 2
    .param p1, "docExtensionType"    # I

    .prologue
    .line 2097
    const/4 v0, 0x0

    .line 2098
    .local v0, "contentType":I
    sparse-switch p1, :sswitch_data_0

    .line 2132
    :goto_0
    return v0

    .line 2100
    :sswitch_0
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->SAVE_2007()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2101
    const/4 v0, 0x5

    .line 2102
    goto :goto_0

    .line 2105
    :cond_0
    :sswitch_1
    const/4 v0, 0x2

    .line 2106
    goto :goto_0

    .line 2108
    :sswitch_2
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->SAVE_2007()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2109
    const/16 v0, 0xd

    .line 2110
    goto :goto_0

    .line 2113
    :cond_1
    :sswitch_3
    const/16 v0, 0xc

    .line 2114
    goto :goto_0

    .line 2116
    :sswitch_4
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->SAVE_2007()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2117
    const/4 v0, 0x4

    .line 2118
    goto :goto_0

    .line 2121
    :cond_2
    :sswitch_5
    const/4 v0, 0x1

    .line 2122
    goto :goto_0

    .line 2124
    :sswitch_6
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->SAVE_2007()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2125
    const/4 v0, 0x6

    .line 2126
    goto :goto_0

    .line 2129
    :cond_3
    :sswitch_7
    const/4 v0, 0x3

    goto :goto_0

    .line 2098
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_5
        0x5 -> :sswitch_7
        0x12 -> :sswitch_4
        0x13 -> :sswitch_0
        0x14 -> :sswitch_6
        0x27 -> :sswitch_3
        0x28 -> :sswitch_2
    .end sparse-switch
.end method

.method public GetBitmap(II)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 2632
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbSlideShow:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2633
    const-string/jumbo v0, "SlideShowActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "GetBitmap =  width = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " height= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2634
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->getBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2636
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public GetChartThumbnailBitmap(III)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "index"    # I
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    .line 2873
    const/4 v0, 0x0

    return-object v0
.end method

.method public GetPageThumbnailBitmap(III)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "nPageNum"    # I
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    .line 2625
    const/4 v0, 0x0

    return-object v0
.end method

.method public GetPrintBitmap(II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 2735
    const/4 v0, 0x0

    return-object v0
.end method

.method public GetThumbnailBitmap(III)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "nPageNum"    # I
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    .line 2619
    const/4 v0, 0x0

    return-object v0
.end method

.method public IsAutoSwitch()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2910
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v3

    iget v3, v3, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    invoke-virtual {v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IGetSlideShowEffect(I)Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;

    move-result-object v0

    .line 2911
    .local v0, "data":Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;
    iget v2, v0, Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;->bAdvTime:I

    if-ne v2, v1, :cond_0

    .line 2916
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public OnBFinalDrawBitmap(I)V
    .locals 0
    .param p1, "nReserved"    # I

    .prologue
    .line 2741
    return-void
.end method

.method public OnBookMarkEditorMode()V
    .locals 0

    .prologue
    .line 2812
    return-void
.end method

.method public OnCloseDoc()V
    .locals 0

    .prologue
    .line 2601
    return-void
.end method

.method public OnCoreNotify(I)V
    .locals 2
    .param p1, "nNotifyCode"    # I

    .prologue
    const/16 v0, 0xe

    .line 2835
    if-ne p1, v0, :cond_0

    .line 2836
    if-ne p1, v0, :cond_0

    .line 2837
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IStopSlideEffect()V

    .line 2838
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2841
    :cond_0
    return-void
.end method

.method public OnCoreNotify2(II)V
    .locals 0
    .param p1, "nNotifyCode2"    # I
    .param p2, "nData"    # I

    .prologue
    .line 2846
    return-void
.end method

.method public OnDrawBitmap(II)V
    .locals 4
    .param p1, "nCallId"    # I
    .param p2, "bShowAutomap"    # I

    .prologue
    .line 2646
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->updateSeekBarPos()V

    .line 2647
    const-string/jumbo v0, "SlideShowActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "OnDrawBitmap = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " mbSlideShow = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbSlideShow:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " m_bIsUseGLES = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsUseGLES:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2648
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbSlideShow:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 2649
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsUseGLES:Z

    if-eqz v0, :cond_2

    .line 2650
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->drawAllContents()V

    .line 2656
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->IsAutoSwitch()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bCheckAutoSwitchAndMode:Z

    if-eqz v0, :cond_0

    .line 2662
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x9

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2663
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bCheckAutoSwitchAndMode:Z

    .line 2673
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2675
    :cond_1
    return-void

    .line 2667
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->drawAllContents()V

    .line 2669
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mHandlerLoop:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;->startHandlerLoop()V

    goto :goto_0
.end method

.method public OnDrawGetChartThumbnail(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 2877
    return-void
.end method

.method public OnDrawGetPageThumbnail(I)V
    .locals 0
    .param p1, "nPageNum"    # I

    .prologue
    .line 2628
    return-void
.end method

.method public OnDrawThumbnailBitmap(I)V
    .locals 0
    .param p1, "nPageNum"    # I

    .prologue
    .line 2622
    return-void
.end method

.method public OnEditCopyCut(IIILjava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "caller"    # I
    .param p2, "nType"    # I
    .param p3, "result"    # I
    .param p4, "text"    # Ljava/lang/String;
    .param p5, "data"    # Ljava/lang/String;
    .param p6, "nMode"    # I

    .prologue
    .line 2744
    return-void
.end method

.method public OnEditOrViewMode(II)V
    .locals 0
    .param p1, "bEditMode"    # I
    .param p2, "EV_EDITMODETYPE"    # I

    .prologue
    .line 2815
    return-void
.end method

.method public OnFlick(I)V
    .locals 0
    .param p1, "nRetData"    # I

    .prologue
    .line 2884
    return-void
.end method

.method public OnGetResID(I)Ljava/lang/String;
    .locals 1
    .param p1, "nStrID"    # I

    .prologue
    .line 2702
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public OnHidAction(I)V
    .locals 0
    .param p1, "EEV_HID_ACTION"    # I

    .prologue
    .line 2821
    return-void
.end method

.method public OnIMEInsertMode()V
    .locals 0

    .prologue
    .line 2827
    return-void
.end method

.method public OnInsertFreeformShapes()V
    .locals 0

    .prologue
    .line 2881
    return-void
.end method

.method public OnInsertTableMode()V
    .locals 0

    .prologue
    .line 2824
    return-void
.end method

.method public OnLoadComplete(I)V
    .locals 6
    .param p1, "bBookmarkExsit"    # I

    .prologue
    .line 2488
    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbPPSFile:Z

    if-eqz v2, :cond_2

    .line 2489
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->onClear()V

    .line 2491
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v2, :cond_1

    .line 2492
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v2

    iget v2, v2, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nTotalPages:I

    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTotalPageNum:I

    .line 2498
    :goto_0
    const/4 v1, 0x0

    .line 2499
    .local v1, "interfaceHandleValue":I
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v2, :cond_0

    .line 2500
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->getJNIInterfaceHandleValue()I

    move-result v1

    .line 2503
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    const v3, 0x7f030049

    const/4 v4, 0x4

    new-array v4, v4, [I

    fill-array-data v4, :array_0

    invoke-virtual {v2, v1, v3, v4}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->initResources(II[I)V

    .line 2505
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTotalPageNum:I

    if-ge v0, v2, :cond_2

    .line 2506
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    new-instance v3, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;-><init>(ZLandroid/graphics/Bitmap;)V

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->onAdd(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;)V

    .line 2505
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2494
    .end local v0    # "i":I
    .end local v1    # "interfaceHandleValue":I
    :cond_1
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 2495
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v2

    iget v2, v2, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nTotalPages:I

    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTotalPageNum:I

    goto :goto_0

    .line 2508
    :cond_2
    return-void

    .line 2503
    :array_0
    .array-data 4
        0x7f0b01e6
        0x7f0b01e7
        0x7f0b01e8
        0x7f0b01e9
    .end array-data
.end method

.method public OnLoadFail(I)V
    .locals 3
    .param p1, "nErrorCode"    # I

    .prologue
    const/16 v2, -0x16

    .line 2520
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->cancelModeSwitchTimer()V

    .line 2523
    const/4 v1, -0x5

    if-eq p1, v1, :cond_0

    if-ne v2, p1, :cond_2

    .line 2525
    :cond_0
    if-ne v2, p1, :cond_1

    .line 2526
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->OnPassWordActivity(Z)V

    .line 2557
    :goto_0
    return-void

    .line 2528
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->OnPassWordActivity(Z)V

    goto :goto_0

    .line 2532
    :cond_2
    const v0, 0x7f070132

    .line 2533
    .local v0, "nStrID":I
    sparse-switch p1, :sswitch_data_0

    .line 2556
    :goto_1
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->AlertErrorAndCloseDlg(I)V

    goto :goto_0

    .line 2537
    :sswitch_0
    const v0, 0x7f070132

    .line 2538
    goto :goto_1

    .line 2541
    :sswitch_1
    const v0, 0x7f070133

    .line 2542
    goto :goto_1

    .line 2544
    :sswitch_2
    const v0, 0x7f07019f

    .line 2545
    goto :goto_1

    .line 2547
    :sswitch_3
    const v0, 0x7f070120

    .line 2548
    goto :goto_1

    .line 2550
    :sswitch_4
    const v0, 0x7f07029c

    .line 2551
    goto :goto_1

    .line 2533
    :sswitch_data_0
    .sparse-switch
        -0x21 -> :sswitch_4
        -0x7 -> :sswitch_3
        -0x5 -> :sswitch_2
        -0x4 -> :sswitch_0
        -0x3 -> :sswitch_1
        -0x2 -> :sswitch_1
        -0x1 -> :sswitch_0
        0x0 -> :sswitch_0
    .end sparse-switch
.end method

.method public OnNewDoc(I)V
    .locals 0
    .param p1, "bOk"    # I

    .prologue
    .line 2818
    return-void
.end method

.method public OnOLEFormatInfo(Ljava/lang/String;)V
    .locals 0
    .param p1, "nWideString"    # Ljava/lang/String;

    .prologue
    .line 3081
    return-void
.end method

.method public OnObjectPoints(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V
    .locals 0
    .param p1, "setValue"    # Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;

    .prologue
    .line 2706
    return-void
.end method

.method public OnPageMove(III)V
    .locals 0
    .param p1, "nCurrentPage"    # I
    .param p2, "nTotalPage"    # I
    .param p3, "nErrorCode"    # I

    .prologue
    .line 2604
    return-void
.end method

.method protected OnPassWordActivity(Z)V
    .locals 2
    .param p1, "bReFail"    # Z

    .prologue
    .line 2595
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/infraware/polarisoffice5/common/PasswordActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2596
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "REFAIL"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2597
    const/16 v1, 0x1e

    invoke-virtual {p0, v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2598
    return-void
.end method

.method public OnPptDrawSlidesBitmap(I)V
    .locals 3
    .param p1, "pageNum"    # I

    .prologue
    .line 2860
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_THUMBNAILOFSLIDESHOW()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2862
    const-string/jumbo v1, "SlideShowActivity"

    const-string/jumbo v2, "notifyDataSetChanged"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2863
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->notifyDataSetChanged()V

    .line 2865
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewImageFrameLayout:Landroid/widget/FrameLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2866
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 2867
    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 2869
    .end local v0    # "child":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public OnPptGetSlidenoteBitmap(II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 2474
    const/4 v0, 0x0

    return-object v0
.end method

.method public OnPptGetSlidesBitmap(IIIIZLjava/lang/String;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "bBitmapImage"    # I
    .param p2, "nPageNum"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I
    .param p5, "isHideSlide"    # Z
    .param p6, "strSlideTitle"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 2439
    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    .line 2470
    :cond_0
    :goto_0
    return-object v1

    .line 2442
    :cond_1
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_THUMBNAILOFSLIDESHOW()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2444
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    instance-of v2, v2, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    if-eqz v2, :cond_0

    .line 2445
    const-string/jumbo v2, "SlideShowActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "OnPptGetSlidesBitmap pageNum:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2447
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    add-int/lit8 v3, p2, -0x1

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getItemImage(I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2450
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_3

    .line 2451
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-ne v2, p3, :cond_2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-eq v2, p4, :cond_3

    .line 2452
    :cond_2
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 2453
    const/4 v1, 0x0

    .line 2457
    :cond_3
    if-nez v1, :cond_4

    .line 2458
    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {p3, p4, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2461
    :cond_4
    if-nez p1, :cond_5

    .line 2462
    const/4 v0, 0x0

    .line 2466
    .local v0, "bHaveBitmap":Z
    :goto_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    add-int/lit8 v3, p2, -0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v1, p5}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->changeListItems(ILjava/lang/Boolean;Landroid/graphics/Bitmap;Z)V

    goto :goto_0

    .line 2464
    .end local v0    # "bHaveBitmap":Z
    :cond_5
    const/4 v0, 0x1

    .restart local v0    # "bHaveBitmap":Z
    goto :goto_1
.end method

.method public OnPptOnDrawSlidenote(I)V
    .locals 0
    .param p1, "pageNum"    # I

    .prologue
    .line 2483
    return-void
.end method

.method public OnPptSlideDelete()V
    .locals 0

    .prologue
    .line 2435
    return-void
.end method

.method public OnPptSlideMoveNext()V
    .locals 0

    .prologue
    .line 2432
    return-void
.end method

.method public OnPptSlideMovePrev()V
    .locals 0

    .prologue
    .line 2429
    return-void
.end method

.method public OnPptSlideShowDrawBitmap()V
    .locals 0

    .prologue
    .line 2480
    return-void
.end method

.method public OnPptSlideShowEffectEnd(I)V
    .locals 4
    .param p1, "nSubType"    # I

    .prologue
    .line 2679
    const-string/jumbo v0, "SlideShowActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "OnPptSlideShowEffectEnd  = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->getVisibility()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2681
    if-nez p1, :cond_0

    .line 2682
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 2683
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2687
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2689
    :cond_0
    return-void

    .line 2685
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public OnPptSlideShowGetBitmap(II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 2477
    const/4 v0, 0x0

    return-object v0
.end method

.method public OnPptSlideexInsert()V
    .locals 0

    .prologue
    .line 2426
    return-void
.end method

.method public OnPptSlidenoteStart()V
    .locals 0

    .prologue
    .line 2423
    return-void
.end method

.method public OnPrintCompleted(I)V
    .locals 0
    .param p1, "zoomScale"    # I

    .prologue
    .line 2738
    return-void
.end method

.method public OnPrintMode(Ljava/lang/String;)V
    .locals 0
    .param p1, "strPrintFile"    # Ljava/lang/String;

    .prologue
    .line 2692
    return-void
.end method

.method public OnPrintedCount(I)V
    .locals 0
    .param p1, "nTotalCount"    # I

    .prologue
    .line 2695
    return-void
.end method

.method public OnProgress(II)V
    .locals 0
    .param p1, "EV_PROGRESS_TYPE"    # I
    .param p2, "nProgressValue"    # I

    .prologue
    .line 2610
    return-void
.end method

.method public OnProgressStart(I)V
    .locals 0
    .param p1, "EV_PROGRESS_TYPE"    # I

    .prologue
    .line 2607
    return-void
.end method

.method public OnSaveDoc(I)V
    .locals 3
    .param p1, "bOk"    # I

    .prologue
    const/4 v2, 0x0

    .line 2798
    const-string/jumbo v0, "SlideShowActivity"

    const-string/jumbo v1, "OnSaveDoc - POPUP_DIALOG_SAVE_PROGRESS"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2799
    invoke-static {}, Lcom/infraware/polarisoffice5/common/DlgFactory;->getInstance()Lcom/infraware/polarisoffice5/common/DlgFactory;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, p0, v1}, Lcom/infraware/polarisoffice5/common/DlgFactory;->dismisProgress(Landroid/app/Activity;I)V

    .line 2801
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbSaving:Z

    .line 2802
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbChangeDocument:Z

    .line 2804
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSaveFilePath:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSaveFilePath:Ljava/lang/String;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2805
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSaveFilePath:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mFilePath:Ljava/lang/String;

    .line 2807
    :cond_0
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbSaveAndFinish:Z

    if-eqz v0, :cond_1

    .line 2808
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onFinish()V

    .line 2809
    :cond_1
    return-void
.end method

.method public OnSearchMode(IIII)V
    .locals 0
    .param p1, "EEV_SEARCH_TYPE"    # I
    .param p2, "nCurrentPage"    # I
    .param p3, "nTotalPage"    # I
    .param p4, "nReplaceAllCount"    # I

    .prologue
    .line 2616
    return-void
.end method

.method public OnSpellCheck(Ljava/lang/String;IIIIIII)V
    .locals 0
    .param p1, "pWordStr"    # Ljava/lang/String;
    .param p2, "nLen"    # I
    .param p3, "bClass"    # I
    .param p4, "nPageNum"    # I
    .param p5, "nObjectID"    # I
    .param p6, "nNoteNum"    # I
    .param p7, "nParaIndex"    # I
    .param p8, "nColIndex"    # I

    .prologue
    .line 2851
    return-void
.end method

.method public OnTerminate()V
    .locals 0

    .prologue
    .line 2613
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->finish()V

    return-void
.end method

.method public OnTextToSpeachString(Ljava/lang/String;)V
    .locals 0
    .param p1, "strTTS"    # Ljava/lang/String;

    .prologue
    .line 2698
    return-void
.end method

.method public OnTotalLoadComplete()V
    .locals 4

    .prologue
    .line 2512
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbPPSFile:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2514
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2515
    :cond_0
    return-void
.end method

.method public OnTouchTheScreen()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2898
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v3

    iget v3, v3, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    invoke-virtual {v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IGetSlideShowEffect(I)Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;

    move-result-object v0

    .line 2899
    .local v0, "data":Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;
    iget v2, v0, Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;->bAdvClick:I

    if-ne v2, v1, :cond_0

    .line 2904
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public OnUndoOrRedo(ZIII)V
    .locals 0
    .param p1, "bUndo"    # Z
    .param p2, "nAction"    # I
    .param p3, "nPage1"    # I
    .param p4, "nPage2"    # I

    .prologue
    .line 2830
    return-void
.end method

.method public addViewToPreviewIamgeLayout(Landroid/view/MotionEvent;)V
    .locals 10
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 2290
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_THUMBNAILOFSLIDESHOW()Z

    move-result v7

    if-nez v7, :cond_1

    .line 2329
    :cond_0
    :goto_0
    return-void

    .line 2292
    :cond_1
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewImageFrameLayout:Landroid/widget/FrameLayout;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2293
    .local v1, "child":Landroid/view/View;
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v7, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 2294
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewImageFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v7}, Landroid/widget/FrameLayout;->removeAllViewsInLayout()V

    .line 2297
    const/4 v3, 0x1

    .line 2298
    .local v3, "pageNum":I
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v7}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v7

    iget v6, v7, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nTotalPages:I

    .line 2299
    .local v6, "totalPageNum":I
    const/4 v4, 0x0

    .line 2301
    .local v4, "progress":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    if-nez v7, :cond_3

    .line 2303
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v7}, Landroid/widget/SeekBar;->getWidth()I

    move-result v7

    iget-object v8, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v8}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v8

    sub-int/2addr v7, v8

    iget-object v8, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v8}, Landroid/widget/SeekBar;->getPaddingRight()I

    move-result v8

    sub-int v0, v7, v8

    .line 2305
    .local v0, "available":I
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v7}, Landroid/widget/SeekBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2306
    .local v2, "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    div-int/lit8 v5, v7, 0x2

    .line 2308
    .local v5, "thumbWidth":I
    sub-int/2addr v0, v5

    .line 2309
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v7}, Landroid/widget/SeekBar;->getThumbOffset()I

    move-result v7

    mul-int/lit8 v7, v7, 0x2

    add-int/2addr v0, v7

    .line 2311
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    int-to-float v8, v0

    div-float/2addr v7, v8

    iget-object v8, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v8}, Landroid/widget/SeekBar;->getScaleX()F

    move-result v8

    mul-float/2addr v7, v8

    const/high16 v8, 0x42c80000    # 100.0f

    mul-float/2addr v7, v8

    float-to-int v4, v7

    .line 2317
    .end local v0    # "available":I
    .end local v2    # "d":Landroid/graphics/drawable/Drawable;
    .end local v5    # "thumbWidth":I
    :goto_1
    mul-int v7, v6, v4

    iget-object v8, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v8}, Landroid/widget/SeekBar;->getMax()I

    move-result v8

    div-int v3, v7, v8

    .line 2319
    if-lt v3, v6, :cond_4

    .line 2320
    add-int/lit8 v3, v6, -0x1

    .line 2326
    :cond_2
    :goto_2
    iget-object v8, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v7}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/View;

    iget-object v9, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewImageFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v8, v3, v7, v9}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2327
    if-eqz v1, :cond_0

    .line 2328
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewImageFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v7, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 2314
    :cond_3
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v7}, Landroid/widget/SeekBar;->getProgress()I

    move-result v4

    goto :goto_1

    .line 2322
    :cond_4
    if-gez v3, :cond_2

    .line 2323
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 1761
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_4

    .line 1762
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1763
    .local v0, "rect":Landroid/graphics/Rect;
    const/4 v1, 0x0

    .line 1764
    .local v1, "view":Landroid/view/View;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerOption:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v2

    if-nez v2, :cond_2

    .line 1765
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerOption:Landroid/widget/LinearLayout;

    .line 1769
    :cond_0
    :goto_0
    if-eqz v1, :cond_4

    .line 1770
    invoke-virtual {v1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1771
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1772
    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_3

    .line 1773
    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    .line 1776
    :cond_1
    :goto_1
    const/4 v2, 0x1

    .line 1780
    .end local v0    # "rect":Landroid/graphics/Rect;
    .end local v1    # "view":Landroid/view/View;
    :goto_2
    return v2

    .line 1766
    .restart local v0    # "rect":Landroid/graphics/Rect;
    .restart local v1    # "view":Landroid/view/View;
    :cond_2
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 1767
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    goto :goto_0

    .line 1774
    :cond_3
    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_1

    .line 1775
    const/4 v2, 0x4

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    goto :goto_1

    .line 1780
    .end local v0    # "rect":Landroid/graphics/Rect;
    .end local v1    # "view":Landroid/view/View;
    :cond_4
    invoke-super {p0, p1}, Lcom/infraware/common/baseactivity/BaseActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    goto :goto_2
.end method

.method public finish()V
    .locals 2

    .prologue
    .line 1941
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbFinishCalled:Z

    .line 1942
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bByMultipleLauncher:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbPPSFile:Z

    if-eqz v0, :cond_0

    .line 1943
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getFilesDir()Ljava/io/File;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/infraware/common/util/FileUtils;->deleteDirectory(Ljava/io/File;Z)V

    .line 1946
    :cond_0
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbPPSFile:Z

    if-eqz v0, :cond_1

    .line 1947
    invoke-static {}, Lcom/infraware/polarisoffice5/MyApplication;->getInstance()Lcom/infraware/polarisoffice5/MyApplication;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/MyApplication;->setCurrentSlideShow(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    .line 1951
    :cond_1
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->finish()V

    .line 1952
    return-void
.end method

.method public fisnishbyMultipleLauncher()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1958
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbPPSFile:Z

    if-nez v1, :cond_0

    .line 1959
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1960
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.infraware.polarisviewer5.CLOSE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1961
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1964
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bByMultipleLauncher:Z

    .line 1965
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbSaveAndFinish:Z

    .line 1966
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onFinish()V

    .line 1967
    return-void
.end method

.method public getAutoSwitchTimer()Landroid/os/CountDownTimer;
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->AutoSwitchTimer:Landroid/os/CountDownTimer;

    return-object v0
.end method

.method public getHandlerLoop()Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mHandlerLoop:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;

    return-object v0
.end method

.method public getIsMakerDrawing()Z
    .locals 1

    .prologue
    .line 206
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsMakerDrawing:Z

    return v0
.end method

.method public getOpenFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3056
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public getPageFromCurrentProgress()I
    .locals 4

    .prologue
    .line 2341
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x42c80000    # 100.0f

    div-float v1, v2, v3

    .line 2342
    .local v1, "progress":F
    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTotalPageNum:I

    int-to-float v2, v2

    mul-float/2addr v2, v1

    float-to-int v2, v2

    add-int/lit8 v0, v2, 0x1

    .line 2344
    .local v0, "page":I
    const/4 v2, 0x1

    if-ge v0, v2, :cond_1

    .line 2345
    const/4 v0, 0x1

    .line 2349
    :cond_0
    :goto_0
    return v0

    .line 2346
    :cond_1
    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTotalPageNum:I

    if-le v0, v2, :cond_0

    .line 2347
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTotalPageNum:I

    goto :goto_0
.end method

.method public getPageFromProgress(I)I
    .locals 4
    .param p1, "progress"    # I

    .prologue
    .line 2354
    int-to-float v2, p1

    const/high16 v3, 0x42c80000    # 100.0f

    div-float v0, v2, v3

    .line 2355
    .local v0, "_progress":F
    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTotalPageNum:I

    int-to-float v2, v2

    mul-float/2addr v2, v0

    float-to-int v2, v2

    add-int/lit8 v1, v2, 0x1

    .line 2357
    .local v1, "page":I
    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    .line 2358
    const/4 v1, 0x1

    .line 2362
    :cond_0
    :goto_0
    return v1

    .line 2359
    :cond_1
    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTotalPageNum:I

    if-le v1, v2, :cond_0

    .line 2360
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTotalPageNum:I

    goto :goto_0
.end method

.method public getProgressFromPage(I)I
    .locals 3
    .param p1, "page"    # I

    .prologue
    .line 2333
    const/high16 v1, 0x42c80000    # 100.0f

    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTotalPageNum:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    div-float v0, v1, v2

    .line 2334
    .local v0, "progress":F
    add-int/lit8 v1, p1, -0x1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    .line 2336
    float-to-int v1, v0

    return v1
.end method

.method public getTranstionInfo()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 2921
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowStartPage:I

    invoke-virtual {v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IGetSlideShowEffect(I)Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;

    move-result-object v0

    .line 2922
    .local v0, "data":Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;
    const-string/jumbo v2, "SlideShowActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "getTranstionInfo Start : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;->nAdvTime:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2923
    iget v2, v0, Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;->bAdvTime:I

    if-ne v2, v1, :cond_0

    .line 2928
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public initSeekBarPos()V
    .locals 3

    .prologue
    .line 2224
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_THUMBNAILOFSLIDESHOW()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2237
    :goto_0
    return-void

    .line 2226
    :cond_0
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowStartPage:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 2227
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 2230
    :cond_1
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowStartPage:I

    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTotalPageNum:I

    if-ne v1, v2, :cond_2

    .line 2231
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 2234
    :cond_2
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowStartPage:I

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getProgressFromPage(I)I

    move-result v0

    .line 2235
    .local v0, "progress":I
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0
.end method

.method public isPenDownNoUp()Z
    .locals 1

    .prologue
    .line 1126
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->isPenDownNoUp:Z

    return v0
.end method

.method public isSupportSlideShow()Z
    .locals 1

    .prologue
    .line 1106
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bSupportSlideShow:Z

    return v0
.end method

.method public isTouchCancel()Z
    .locals 1

    .prologue
    .line 1118
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->isTouchCancel:Z

    return v0
.end method

.method public isUseGLES()Z
    .locals 1

    .prologue
    .line 2933
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsUseGLES:Z

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v2, -0x1

    .line 1733
    invoke-super {p0, p1, p2, p3}, Lcom/infraware/common/baseactivity/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1734
    const/16 v1, 0x15

    if-ne p1, v1, :cond_1

    .line 1735
    if-ne p2, v2, :cond_0

    .line 1736
    const-string/jumbo v1, "color_value"

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05005a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 1737
    .local v0, "color":I
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v1, v0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->setOtherColor(I)V

    .line 1748
    .end local v0    # "color":I
    :cond_0
    :goto_0
    return-void

    .line 1740
    :cond_1
    const/16 v1, 0x1e

    if-ne p1, v1, :cond_0

    .line 1741
    if-ne p2, v2, :cond_0

    .line 1743
    invoke-direct {p0, p3}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onResultPassWordActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2054
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ICancel()V

    .line 2057
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IIsSlideShow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2059
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IStopSlideEffect()V

    .line 2062
    :cond_0
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_5

    .line 2063
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserMode:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isSelected()Z

    move-result v0

    if-ne v0, v2, :cond_1

    .line 2064
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    .line 2086
    :goto_0
    return-void

    .line 2065
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideMode:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isSelected()Z

    move-result v0

    if-ne v0, v2, :cond_2

    .line 2066
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    goto :goto_0

    .line 2067
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerMode:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isSelected()Z

    move-result v0

    if-ne v0, v2, :cond_3

    .line 2068
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    goto :goto_0

    .line 2069
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerMode:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isSelected()Z

    move-result v0

    if-ne v0, v2, :cond_4

    .line 2070
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    goto :goto_0

    .line 2072
    :cond_4
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onCloseSlideShow()V

    goto :goto_0

    .line 2076
    :cond_5
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_6

    .line 2077
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    goto :goto_0

    .line 2080
    :cond_6
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_7

    .line 2081
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    goto :goto_0

    .line 2084
    :cond_7
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbSaveAndFinish:Z

    .line 2085
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onCloseSlideShow()V

    goto :goto_0
.end method

.method public onButtonClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x7

    const/4 v1, 0x1

    const/4 v3, 0x6

    const/4 v2, 0x4

    .line 1200
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mLayoutImageMode:Landroid/widget/LinearLayout;

    if-ne p1, v0, :cond_1

    .line 1202
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    .line 1232
    :cond_0
    :goto_0
    return-void

    .line 1204
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mImageSettings:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_5

    .line 1206
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    if-ne v0, v1, :cond_2

    .line 1207
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    goto :goto_0

    .line 1208
    :cond_2
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 1209
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    goto :goto_0

    .line 1210
    :cond_3
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    if-ne v0, v2, :cond_4

    .line 1211
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    goto :goto_0

    .line 1212
    :cond_4
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    if-ne v0, v3, :cond_0

    .line 1213
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    goto :goto_0

    .line 1215
    :cond_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mImageEraser:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_7

    .line 1217
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mImageEraser:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isSelected()Z

    move-result v0

    if-ne v0, v1, :cond_6

    .line 1218
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    goto :goto_0

    .line 1220
    :cond_6
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    goto :goto_0

    .line 1222
    :cond_7
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mImageEraseAll:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_0

    .line 1224
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mImageEraser:Landroid/widget/ImageView;

    if-eqz v0, :cond_8

    .line 1225
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mImageEraser:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1227
    :cond_8
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISetClearAllPen()V

    .line 1229
    iput v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    .line 1230
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    goto :goto_0
.end method

.method public onChangeMode(I)V
    .locals 10
    .param p1, "mode"    # I

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1426
    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    if-ne v3, p1, :cond_1

    .line 1567
    :cond_0
    :goto_0
    return-void

    .line 1429
    :cond_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSettings:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1430
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserMode:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1431
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeShowMode(I)V

    .line 1433
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->IIsSlideShow()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsAutoModeSwitch:Z

    if-nez v3, :cond_2

    .line 1438
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->IStopSlideEffect()V

    .line 1441
    :cond_2
    sget-object v2, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$SlideShowStatus;->SLIDE_STATUS:[I

    .line 1442
    .local v2, "status":[I
    packed-switch p1, :pswitch_data_0

    .line 1526
    :cond_3
    :goto_1
    iput p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    .line 1528
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mGestureProc:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;

    iget v4, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->setSlideShowMode(I)V

    .line 1529
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mGLGestureProc:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;

    iget v4, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->setSlideShowMode(I)V

    .line 1531
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    aget v4, v2, v5

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->setVisibility(I)V

    .line 1532
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mClose:Landroid/widget/ImageView;

    aget v4, v2, v5

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1534
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSettings:Landroid/widget/ImageView;

    aget v4, v2, v6

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1535
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserAllMode:Landroid/widget/ImageView;

    aget v4, v2, v7

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1536
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserMode:Landroid/widget/ImageView;

    aget v4, v2, v7

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1537
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPrevPage:Landroid/widget/ImageView;

    aget v4, v2, v9

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1538
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNextPage:Landroid/widget/ImageView;

    aget v4, v2, v9

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1539
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mModeOption:Landroid/widget/LinearLayout;

    aget v4, v2, v8

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1540
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerOption:Landroid/widget/LinearLayout;

    const/4 v4, 0x5

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1541
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    const/4 v4, 0x6

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->setVisibility(I)V

    .line 1543
    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    if-eq v3, v7, :cond_4

    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    if-ne v3, v9, :cond_d

    .line 1544
    :cond_4
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    invoke-virtual {v3, v5}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->setVisibility(I)V

    .line 1549
    :cond_5
    :goto_2
    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    const/16 v4, 0x8

    if-ne v3, v4, :cond_e

    .line 1550
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mClose:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1553
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPrevPage:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1554
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNextPage:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1556
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3, v8}, Landroid/widget/SeekBar;->setVisibility(I)V

    goto/16 :goto_0

    .line 1445
    :pswitch_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->IsAutoSwitch()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1447
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->AutoSwitchTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v3}, Landroid/os/CountDownTimer;->cancel()V

    .line 1449
    :cond_6
    sget-object v2, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$SlideShowStatus;->POINTER_STATUS:[I

    .line 1450
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSettings:Landroid/widget/ImageView;

    const v4, 0x7f0702d5

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1451
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    iget v4, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor:I

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->setColor(I)V

    goto/16 :goto_1

    .line 1454
    :pswitch_1
    sget-object v2, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$SlideShowStatus;->POINTER_OPTION_STATUS:[I

    .line 1455
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSettings:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setSelected(Z)V

    goto/16 :goto_1

    .line 1459
    :pswitch_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->IsAutoSwitch()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1461
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->AutoSwitchTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v3}, Landroid/os/CountDownTimer;->cancel()V

    .line 1463
    :cond_7
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v3

    iget v1, v3, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    .line 1464
    .local v1, "nPage":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3, v1}, Lcom/infraware/office/evengine/EvInterface;->IIsPenDataForSlideShow(I)I

    move-result v0

    .line 1465
    .local v0, "hasMarkerData":I
    if-nez v0, :cond_8

    .line 1467
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserMode:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1468
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserAllMode:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1474
    :goto_3
    sget-object v2, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$SlideShowStatus;->MARKER_STATUS:[I

    .line 1475
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSettings:Landroid/widget/ImageView;

    const v4, 0x7f0702d4

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1476
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMarker()V

    goto/16 :goto_1

    .line 1471
    :cond_8
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserMode:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1472
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserAllMode:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_3

    .line 1479
    .end local v0    # "hasMarkerData":I
    .end local v1    # "nPage":I
    :pswitch_3
    sget-object v2, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$SlideShowStatus;->MARKER_OPTION_STATUS:[I

    .line 1480
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSettings:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setSelected(Z)V

    goto/16 :goto_1

    .line 1484
    :pswitch_4
    sget-object v2, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$SlideShowStatus;->ERASER_STATUS:[I

    .line 1486
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserMode:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1487
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/16 v4, 0x9

    invoke-virtual {v3, v4, v5}, Lcom/infraware/office/evengine/EvInterface;->ISetPenMode(IZ)V

    goto/16 :goto_1

    .line 1491
    :pswitch_5
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->ModeSwitchTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v3}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 1493
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->IsAutoSwitch()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1495
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->AutoSwitchTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v3}, Landroid/os/CountDownTimer;->cancel()V

    .line 1497
    :cond_9
    sget-object v2, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$SlideShowStatus;->SELECT_STATUS:[I

    .line 1498
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideMode:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1499
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerMode:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1500
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerMode:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1503
    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    if-ne v3, v6, :cond_a

    .line 1504
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideMode:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setSelected(Z)V

    goto/16 :goto_1

    .line 1505
    :cond_a
    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    if-ne v3, v7, :cond_b

    .line 1506
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerMode:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setSelected(Z)V

    goto/16 :goto_1

    .line 1507
    :cond_b
    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    if-eq v3, v8, :cond_c

    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    const/4 v4, 0x6

    if-ne v3, v4, :cond_3

    .line 1508
    :cond_c
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerMode:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setSelected(Z)V

    goto/16 :goto_1

    .line 1513
    :pswitch_6
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->cancelModeSwitchTimer()V

    .line 1515
    sget-object v2, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$SlideShowStatus;->SHOW_END_STATUS:[I

    .line 1516
    goto/16 :goto_1

    .line 1519
    :pswitch_7
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->IsAutoSwitch()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1521
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->AutoSwitchTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v3}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    goto/16 :goto_1

    .line 1545
    :cond_d
    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    const/4 v4, 0x7

    if-eq v3, v4, :cond_5

    .line 1546
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->setVisibility(I)V

    goto/16 :goto_2

    .line 1558
    :cond_e
    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    if-ne v3, v6, :cond_f

    .line 1559
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3, v8}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 1560
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mClose:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1563
    :cond_f
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_THUMBNAILOFSLIDESHOW()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1564
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3, v5}, Landroid/widget/SeekBar;->setVisibility(I)V

    goto/16 :goto_0

    .line 1442
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public onChangeScreen(I)V
    .locals 2
    .param p1, "nType"    # I

    .prologue
    const/16 v1, 0x400

    .line 1190
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1192
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 1193
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 1198
    :cond_0
    :goto_0
    return-void

    .line 1195
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    goto :goto_0
.end method

.method protected onChangedFromMultiwindowToNormalWindow()V
    .locals 0

    .prologue
    .line 3036
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->videoStop()V

    .line 3037
    return-void
.end method

.method protected onChangedFromNormalWindowToMultiWindow()V
    .locals 0

    .prologue
    .line 3031
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->videoStop()V

    .line 3032
    return-void
.end method

.method public onClickClose(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1646
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ICancel()V

    .line 1649
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->cancelModeSwitchTimer()V

    .line 1651
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->AutoSwitchTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 1654
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IIsSlideShow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1656
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IStopSlideEffect()V

    .line 1659
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onCloseSlideShow()V

    .line 1660
    return-void
.end method

.method public onClickColor(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1663
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onGetPointerColor()Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    move-result-object v0

    .local v0, "view":Landroid/view/View;
    move-object v1, p1

    .line 1664
    check-cast v1, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerColor:I

    .line 1666
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->showModeButtonUpdate()V

    .line 1668
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    .line 1669
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 1670
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/view/View;->setSelected(Z)V

    .line 1671
    return-void
.end method

.method public onClickColorPicker(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1674
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1675
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "color_value"

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getColor()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1676
    const/16 v1, 0x15

    invoke-virtual {p0, v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1677
    return-void
.end method

.method public onClickEraserAll(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1610
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserMode:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 1611
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserMode:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1613
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISetClearAllPen()V

    .line 1615
    const/4 v0, 0x6

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    .line 1616
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    .line 1617
    return-void
.end method

.method public onClickMode(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x7

    const/4 v4, 0x6

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x4

    .line 1570
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->videoStop()V

    .line 1573
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->cancelModeSwitchTimer()V

    .line 1576
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    if-ne p1, v0, :cond_1

    .line 1577
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    .line 1607
    :cond_0
    :goto_0
    return-void

    .line 1578
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSettings:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_6

    .line 1580
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    if-ne v0, v2, :cond_3

    .line 1581
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    .line 1589
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mModeOption:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setOptionPosition(Ljava/lang/Object;)V

    .line 1590
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerOption:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setOptionPosition(Ljava/lang/Object;)V

    .line 1591
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setOptionPosition(Ljava/lang/Object;)V

    goto :goto_0

    .line 1582
    :cond_3
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    if-ne v0, v3, :cond_4

    .line 1583
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    goto :goto_1

    .line 1584
    :cond_4
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    if-ne v0, v1, :cond_5

    .line 1585
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    goto :goto_1

    .line 1586
    :cond_5
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    if-ne v0, v4, :cond_2

    .line 1587
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    goto :goto_1

    .line 1593
    :cond_6
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserMode:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_8

    .line 1594
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserMode:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isSelected()Z

    move-result v0

    if-ne v0, v2, :cond_7

    .line 1595
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    goto :goto_0

    .line 1598
    :cond_7
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    goto :goto_0

    .line 1601
    :cond_8
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideMode:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_9

    .line 1602
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    goto :goto_0

    .line 1603
    :cond_9
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerMode:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_a

    .line 1604
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    goto :goto_0

    .line 1605
    :cond_a
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerMode:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_0

    .line 1606
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    goto :goto_0
.end method

.method public onClickPage(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 1622
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->cancelModeSwitchTimer()V

    .line 1625
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    .line 1627
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->ModeSwitchTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 1629
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPrevPage:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_3

    .line 1630
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onClickPlayPrev()V

    .line 1634
    :cond_1
    :goto_0
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 1635
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->onClear()V

    .line 1637
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->playSoundEffect(I)V

    .line 1639
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPrevPage:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1640
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNextPage:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1642
    return-void

    .line 1631
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNextPage:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_1

    .line 1632
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onClickPlayNext()V

    goto :goto_0
.end method

.method public declared-synchronized onClickPlayNext()V
    .locals 4

    .prologue
    .line 483
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mClickLock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 484
    :try_start_1
    sget-boolean v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mIsClicking:Z

    if-eqz v0, :cond_0

    .line 485
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 548
    :goto_0
    monitor-exit p0

    return-void

    .line 494
    :cond_0
    const/4 v0, 0x1

    :try_start_2
    sput-boolean v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mIsClicking:Z

    .line 495
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IStopSlideEffect()V

    .line 496
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mHandlerLoop:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;->stopHandlerLoop()V

    .line 497
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->AutoSwitchTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 498
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->videoStop()V

    .line 501
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    const/16 v2, 0x8

    if-ne v0, v2, :cond_3

    .line 502
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsUseGLES:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IIsWaitingFlag()Z

    move-result v0

    if-nez v0, :cond_2

    .line 503
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onCloseSlideShow()V

    .line 505
    :cond_2
    const/4 v0, 0x0

    sput-boolean v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mIsClicking:Z

    .line 506
    monitor-exit v1

    goto :goto_0

    .line 547
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 483
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 509
    :cond_3
    :try_start_4
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->isSupportSlideShow()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 527
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IIsNoneEffect(II)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 529
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->changeScreenForSlideShow()V

    .line 530
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/infraware/office/evengine/EvInterface;->ISlideShowPlay(I)V

    .line 531
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->updateSeekBarPos()V

    .line 546
    :goto_1
    const/4 v0, 0x0

    sput-boolean v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mIsClicking:Z

    .line 547
    monitor-exit v1

    goto :goto_0

    .line 535
    :cond_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->myRenderer:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->onPlaySlideShow(II)V

    .line 536
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setViewMode(Z)V

    .line 537
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->requestRender()V

    goto :goto_1

    .line 541
    :cond_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->ISlideShowPlay(IZ)V

    .line 542
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IStopSlideEffect()V

    .line 543
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->updateSeekBarPos()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method public onClickPlayPage(I)V
    .locals 5
    .param p1, "page"    # I

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x5

    .line 609
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mHandlerLoop:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;->stopHandlerLoop()V

    .line 611
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->AutoSwitchTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 612
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->videoStop()V

    .line 614
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->isSupportSlideShow()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 616
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nTotalPages:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v1

    iget v1, v1, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    if-ne v0, v1, :cond_0

    .line 617
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, Lcom/infraware/office/evengine/EvInterface;->IIsNoneEffect(II)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->getVisibility()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 618
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    invoke-virtual {v0, v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->setVisibility(I)V

    .line 643
    :goto_0
    return-void

    .line 623
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v2, p1}, Lcom/infraware/office/evengine/EvInterface;->IIsNoneEffect(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 625
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->changeScreenForSlideShow()V

    .line 626
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v2, p1, v4}, Lcom/infraware/office/evengine/EvInterface;->ISlideShowPlay(III)V

    .line 627
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->updateSeekBarPos()V

    goto :goto_0

    .line 631
    :cond_1
    const-string/jumbo v0, "SlideShowActivity"

    const-string/jumbo v1, "onClickPlayPage 1 "

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->myRenderer:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;

    invoke-virtual {v0, v2, p1}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->onPlaySlideShow(II)V

    .line 633
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setViewMode(Z)V

    .line 634
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->requestRender()V

    goto :goto_0

    .line 638
    :cond_2
    const-string/jumbo v0, "SlideShowActivity"

    const-string/jumbo v1, "onClickPlayPage no Support 1 "

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v2, p1, v4, v3}, Lcom/infraware/office/evengine/EvInterface;->ISlideShowPlay(IIIZ)V

    .line 640
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IStopSlideEffect()V

    .line 641
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->updateSeekBarPos()V

    goto :goto_0
.end method

.method public declared-synchronized onClickPlayPrev()V
    .locals 4

    .prologue
    .line 551
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mClickLock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 552
    :try_start_1
    sget-boolean v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mIsClicking:Z

    if-eqz v0, :cond_0

    .line 553
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 606
    :goto_0
    monitor-exit p0

    return-void

    .line 555
    :cond_0
    const/4 v0, 0x1

    :try_start_2
    sput-boolean v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mIsClicking:Z

    .line 556
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mHandlerLoop:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;->stopHandlerLoop()V

    .line 557
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->AutoSwitchTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 558
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->videoStop()V

    .line 561
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    const/16 v2, 0x8

    if-ne v0, v2, :cond_2

    .line 562
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsUseGLES:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IIsWaitingFlag()Z

    move-result v0

    if-nez v0, :cond_2

    .line 563
    :cond_1
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setSlideVisibility()V

    .line 564
    const/4 v0, 0x0

    sput-boolean v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mIsClicking:Z

    .line 565
    monitor-exit v1

    goto :goto_0

    .line 605
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 551
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 569
    :cond_2
    :try_start_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    const/4 v2, 0x2

    if-ge v0, v2, :cond_3

    .line 570
    const/4 v0, 0x0

    sput-boolean v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mIsClicking:Z

    .line 571
    monitor-exit v1

    goto :goto_0

    .line 581
    :cond_3
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->isSupportSlideShow()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 587
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IIsNoneEffect(II)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 589
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->changeScreenForSlideShow()V

    .line 590
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/infraware/office/evengine/EvInterface;->ISlideShowPlay(I)V

    .line 591
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->updateSeekBarPos()V

    .line 604
    :goto_1
    const/4 v0, 0x0

    sput-boolean v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mIsClicking:Z

    .line 605
    monitor-exit v1

    goto :goto_0

    .line 595
    :cond_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->myRenderer:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->onPlaySlideShow(II)V

    .line 596
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setViewMode(Z)V

    .line 597
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->requestRender()V

    goto :goto_1

    .line 600
    :cond_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->ISlideShowPlay(IZ)V

    .line 601
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IStopSlideEffect()V

    .line 602
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->updateSeekBarPos()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method public onCloseSlideShow()V
    .locals 1

    .prologue
    .line 2090
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IIsPenDataForSlideShow()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2091
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IDeletePenDataForSlideShow()V

    .line 2093
    :cond_0
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onFinish()V

    .line 2094
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1136
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_orientation:I

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_0

    .line 1137
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_orientation:I

    .line 1138
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mGestureProc:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->onTouchCancel()V

    .line 1139
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->isTouchCancel:Z

    .line 1142
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IStopSlideEffect()V

    .line 1143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowFinished:Landroid/widget/TextView;

    const v1, 0x7f070222

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1145
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->OnPptSlideShowEffectEnd(I)V

    .line 1146
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNextPage:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1147
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPrevPage:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1148
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewImageFrameLayout:Landroid/widget/FrameLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1149
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->videoStop()V

    .line 1150
    invoke-super {p0, p1}, Lcom/infraware/common/baseactivity/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1163
    const-string/jumbo v0, "SlideShowActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onConfigurationChanged m_OrientationPortrait = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_OrientationPortrait:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1164
    const-string/jumbo v0, "SlideShowActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onConfigurationChanged getResources().getConfiguration().orientation = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1165
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v3, :cond_2

    .line 1167
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_OrientationPortrait:Z

    if-nez v0, :cond_1

    .line 1168
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bChangeScreenForSlideShow:Z

    .line 1169
    const-string/jumbo v0, "SlideShowActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Configuration m_bChangeScreenForSlideShow = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bChangeScreenForSlideShow:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1172
    :cond_1
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_OrientationPortrait:Z

    .line 1182
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mModeOption:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setOptionPosition(Ljava/lang/Object;)V

    .line 1183
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerOption:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setOptionPosition(Ljava/lang/Object;)V

    .line 1184
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setOptionPosition(Ljava/lang/Object;)V

    .line 1186
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setChangescreenbtn()V

    .line 1187
    return-void

    .line 1175
    :cond_2
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_OrientationPortrait:Z

    if-ne v0, v3, :cond_3

    .line 1176
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bChangeScreenForSlideShow:Z

    .line 1179
    :cond_3
    iput-boolean v4, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_OrientationPortrait:Z

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 804
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_orientation:I

    .line 805
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->requestWindowFeature(I)Z

    .line 806
    invoke-super {p0, p1}, Lcom/infraware/common/baseactivity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 808
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 810
    const v0, 0x7f03004c

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setContentView(I)V

    .line 812
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 814
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-nez v0, :cond_0

    .line 815
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->finish()V

    .line 1046
    :goto_0
    return-void

    .line 820
    :cond_0
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$CloseActionReceiver;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$CloseActionReceiver;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$CloseActionReceiver;

    .line 821
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    .line 822
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    const-string/jumbo v1, "com.infraware.polarisviewer5.CLOSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 823
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$CloseActionReceiver;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 825
    const-string/jumbo v0, "SlideShowActivity"

    const-string/jumbo v1, "onCreate"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 826
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v11

    .line 827
    .local v11, "intent":Landroid/content/Intent;
    const-string/jumbo v0, "key_pps_file"

    const/4 v1, 0x0

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbPPSFile:Z

    .line 828
    const-string/jumbo v0, "PPT_FILEPATH"

    invoke-virtual {v11, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mFilePath:Ljava/lang/String;

    .line 829
    const-string/jumbo v0, "InterfaceHandleAddress"

    const/4 v1, 0x0

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mInterfaceHandleAddress:I

    .line 830
    const-string/jumbo v0, "START_PAGE_NUM"

    const/4 v1, 0x0

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowStartPage:I

    .line 831
    const-string/jumbo v0, "SLIDE_THUMBNAIL_IMAGE_WIDTH"

    const/high16 v1, 0x42f00000    # 120.0f

    invoke-static {p0, v1}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v1

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImageWidth:I

    .line 832
    const-string/jumbo v0, "SLIDE_THUMBNAIL_IMAGE_HEIGHT"

    const/high16 v1, 0x42b40000    # 90.0f

    invoke-static {p0, v1}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v1

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImageHeight:I

    .line 834
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 835
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_OrientationPortrait:Z

    .line 840
    :goto_1
    const-string/jumbo v0, "SlideShowActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onCreate m_OrientationPortrait = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_OrientationPortrait:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 842
    const-string/jumbo v0, "INTCMD"

    const/4 v1, -0x1

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mCmdType:I

    .line 843
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mInterfaceHandleAddress:I

    if-nez v0, :cond_1

    .line 844
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IInitInterfaceHandleAddress()I

    move-result v13

    .line 845
    .local v13, "tmpHandle":I
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mCmdType:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_4

    .line 846
    iput v13, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mInterfaceHandleAddress:I

    .line 850
    .end local v13    # "tmpHandle":I
    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getInterfaceHandle()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->SetInterfaceHandleAddress(I)V

    .line 852
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nTotalPages:I

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTotalPageNum:I

    .line 855
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    if-gtz v0, :cond_2

    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbPPSFile:Z

    if-nez v0, :cond_2

    .line 856
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->finish()V

    .line 859
    :cond_2
    const-string/jumbo v0, "window"

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v9

    .line 860
    .local v9, "display":Landroid/view/Display;
    invoke-static {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getInstance(Landroid/content/Context;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    .line 862
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p0

    move-object v4, p0

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->ISetListener(Lcom/infraware/office/evengine/EvListener$ViewerListener;Lcom/infraware/office/evengine/EvListener$EditorListener;Lcom/infraware/office/evengine/EvListener$WordEditorListener;Lcom/infraware/office/evengine/EvListener$PptEditorListener;Lcom/infraware/office/evengine/EvListener$SheetEditorListener;Lcom/infraware/office/evengine/EvListener$PdfViewerListener;)V

    .line 864
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowStartPage:I

    if-nez v0, :cond_6

    .line 865
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowStartPage:I

    .line 867
    const/4 v10, 0x1

    .line 868
    .local v10, "i":I
    const/4 v10, 0x1

    :goto_3
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTotalPageNum:I

    if-gt v10, v0, :cond_5

    .line 869
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v10}, Lcom/infraware/office/evengine/EvInterface;->IGetIsSlideHide(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 870
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowStartPage:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowStartPage:I

    .line 868
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 837
    .end local v9    # "display":Landroid/view/Display;
    .end local v10    # "i":I
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_OrientationPortrait:Z

    .line 838
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    const/16 v2, 0x400

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFlags(II)V

    goto/16 :goto_1

    .line 848
    .restart local v13    # "tmpHandle":I
    :cond_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v13}, Lcom/infraware/office/evengine/EvInterface;->setNativeInterfaceHandle(I)V

    goto :goto_2

    .line 875
    .end local v13    # "tmpHandle":I
    .restart local v9    # "display":Landroid/view/Display;
    .restart local v10    # "i":I
    :cond_5
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowStartPage:I

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTotalPageNum:I

    if-le v0, v1, :cond_6

    .line 876
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowStartPage:I

    .line 879
    .end local v10    # "i":I
    :cond_6
    const v0, 0x7f0b01f3

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMain:Landroid/widget/RelativeLayout;

    .line 880
    const v0, 0x7f0b01f7

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    .line 881
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->setZOrderMediaOverlay(Z)V

    .line 882
    const v0, 0x7f0b01f6

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    .line 884
    const v0, 0x7f0b01fb

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/PointerDrawView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    .line 885
    const v0, 0x7f0b01fc

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    .line 886
    const v0, 0x7f0b0203

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPrevPage:Landroid/widget/ImageView;

    .line 887
    const v0, 0x7f0b0204

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    .line 888
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekbarListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 889
    const v0, 0x7f0b0206

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewImageFrameLayout:Landroid/widget/FrameLayout;

    .line 890
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_THUMBNAILOFSLIDESHOW()Z

    move-result v0

    if-nez v0, :cond_7

    .line 892
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 894
    :cond_7
    const v0, 0x7f0b0205

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNextPage:Landroid/widget/ImageView;

    .line 895
    const v0, 0x7f0b0207

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mClose:Landroid/widget/ImageView;

    .line 897
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mShowMode:Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;

    new-instance v1, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$11;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$11;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 907
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPrevPage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mOnClickListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 908
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNextPage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mOnClickListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 909
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPrevPage:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 910
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNextPage:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 912
    const v0, 0x7f0b01fd

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSettings:Landroid/widget/ImageView;

    .line 913
    const v0, 0x7f0b01fe

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserMode:Landroid/widget/ImageView;

    .line 914
    const v0, 0x7f0b01ff

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserAllMode:Landroid/widget/ImageView;

    .line 916
    const v0, 0x7f0b0200

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_VideoFrame:Landroid/widget/RelativeLayout;

    .line 917
    const v0, 0x7f0b0201

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mVideobtn:Landroid/widget/ImageView;

    .line 919
    const v0, 0x7f0b0202

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/VideoView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mVideo:Landroid/widget/VideoView;

    .line 920
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mVideo:Landroid/widget/VideoView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setZOrderOnTop(Z)V

    .line 921
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowGestureProcListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$SlideShowGestureProcListener;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    const/4 v6, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;-><init>(Landroid/app/Activity;Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$SlideShowGestureProcListener;Landroid/view/View;Landroid/view/View;Lcom/infraware/polarisoffice5/common/PointerDrawView;Z)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mGestureProc:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;

    .line 922
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->setEvInterface(Lcom/infraware/office/evengine/EvInterface;)V

    .line 923
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowGLSurfaceViewListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView$SlideShowGLSurfaceViewListener;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->setSlideShowSurfaceViewListener(Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView$SlideShowGLSurfaceViewListener;)V

    .line 924
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowGestureProcListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$SlideShowGestureProcListener;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;-><init>(Landroid/app/Activity;Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc$SlideShowGestureProcListener;Landroid/view/View;Landroid/view/View;Lcom/infraware/polarisoffice5/common/PointerDrawView;Z)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mGLGestureProc:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;

    .line 925
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->setEvInterface(Lcom/infraware/office/evengine/EvInterface;)V

    .line 926
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowSurfaceViewListener:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView$SlideShowSurfaceViewListener;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->setSlideShowSurfaceViewListener(Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView$SlideShowSurfaceViewListener;)V

    .line 927
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->myRenderer:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowGLRendererListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer$SlideShowGLRendererListener;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->setSlideShowGLRendererListener(Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer$SlideShowGLRendererListener;)V

    .line 929
    const v0, 0x7f0b01f8

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mLastSlideImage:Landroid/view/SurfaceView;

    .line 930
    const v0, 0x7f0b01f9

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowFinished:Landroid/widget/TextView;

    .line 932
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mLastSlideImage:Landroid/view/SurfaceView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onLastSlideTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 933
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowFinished:Landroid/widget/TextView;

    const v1, 0x7f070222

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 935
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->setVisibility(I)V

    .line 937
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mRemovedViewQueue:Ljava/util/Queue;

    .line 938
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSeekBarMoveHistory:Ljava/util/Stack;

    .line 940
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSettings:Landroid/widget/ImageView;

    new-instance v1, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$12;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$12;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 950
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserMode:Landroid/widget/ImageView;

    new-instance v1, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$13;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$13;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 957
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserAllMode:Landroid/widget/ImageView;

    new-instance v1, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$14;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$14;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 966
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setChangescreenbtn()V

    .line 967
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onInitModeOption()V

    .line 968
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onInitPointerOption()V

    .line 969
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onInitMarkerOption()V

    .line 970
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    .line 971
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->initSeekBarPos()V

    .line 973
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbPPSFile:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_a

    .line 983
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->setVisibility(I)V

    .line 985
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/infraware/common/util/Utils;->getCurrentLocaleType(Landroid/content/res/Resources;)I

    move-result v5

    .line 986
    .local v5, "locale":I
    const-string/jumbo v0, "key_pps_file_path"

    invoke-virtual {v11, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mFilePath:Ljava/lang/String;

    .line 988
    sget-object v0, Lcom/infraware/common/define/CMModelDefine;->CUSTOM_TEMP_PATH:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_8

    .line 989
    sget-object v0, Lcom/infraware/common/define/CMModelDefine;->CUSTOM_TEMP_PATH:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {v0, v1, p0}, Lcom/infraware/common/util/FileUtils;->makeDirectory(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTempPath:Ljava/lang/String;

    .line 991
    :cond_8
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTempPath:Ljava/lang/String;

    if-nez v0, :cond_9

    .line 992
    const-string/jumbo v0, "polarisTemp"

    const/4 v1, 0x0

    invoke-static {v0, v1, p0}, Lcom/infraware/common/util/FileUtils;->makeDirectory(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTempPath:Ljava/lang/String;

    if-nez v0, :cond_9

    .line 993
    sget-object v0, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->TEMP_PATH:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {v0, v1, p0}, Lcom/infraware/common/util/FileUtils;->makeDirectory(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTempPath:Ljava/lang/String;

    if-nez v0, :cond_9

    .line 994
    sget-object v0, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->SDROOT_PATH:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTempPath:Ljava/lang/String;

    .line 996
    :cond_9
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTempPath:Ljava/lang/String;

    invoke-static {v0}, Lcom/infraware/common/util/FileUtils;->addPathDelemeter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTempPath:Ljava/lang/String;

    .line 997
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTempPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getInterfaceHandle()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTempPath:Ljava/lang/String;

    .line 998
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTempPath:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {v0, v1, p0}, Lcom/infraware/common/util/FileUtils;->makeDirectory(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;

    .line 1000
    const/4 v8, 0x0

    .line 1012
    .local v8, "bookmarkPath":Ljava/lang/String;
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v9}, Landroid/view/Display;->getWidth()I

    move-result v1

    invoke-virtual {v9}, Landroid/view/Display;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/infraware/office/evengine/EvInterface;->IInitialize(II)V

    .line 1013
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mFilePath:Ljava/lang/String;

    invoke-virtual {v9}, Landroid/view/Display;->getWidth()I

    move-result v2

    invoke-virtual {v9}, Landroid/view/Display;->getHeight()I

    move-result v3

    const/16 v4, 0x20

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTempPath:Ljava/lang/String;

    invoke-virtual/range {v0 .. v8}, Lcom/infraware/office/evengine/EvInterface;->IOpen(Ljava/lang/String;IIIIILjava/lang/String;Ljava/lang/String;)V

    .line 1016
    .end local v5    # "locale":I
    .end local v8    # "bookmarkPath":Ljava/lang/String;
    :cond_a
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1017
    invoke-static {}, Lcom/infraware/common/util/Utils;->isJB()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1018
    new-instance v0, Lcom/infraware/common/util/SbeamHelper;

    const-string/jumbo v1, "text/DirectSharePolarisEditor"

    invoke-direct {v0, p0, v1}, Lcom/infraware/common/util/SbeamHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    .line 1019
    new-instance v12, Landroid/content/IntentFilter;

    invoke-direct {v12}, Landroid/content/IntentFilter;-><init>()V

    .line 1020
    .local v12, "intentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v0, "com.android.systemui.statusbar.COLLAPSED"

    invoke-virtual {v12, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1021
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$15;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$15;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 1033
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0, v12}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1039
    .end local v12    # "intentFilter":Landroid/content/IntentFilter;
    :cond_b
    :goto_4
    invoke-static {}, Lcom/infraware/common/util/Utils;->isICS()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-static {p0}, Lcom/infraware/polarisoffice5/common/HoveringManager;->setHoveringButton(Landroid/app/Activity;)Z

    .line 1042
    :cond_c
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbPPSFile:Z

    if-eqz v0, :cond_d

    .line 1043
    invoke-static {}, Lcom/infraware/polarisoffice5/MyApplication;->getInstance()Lcom/infraware/polarisoffice5/MyApplication;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/infraware/polarisoffice5/MyApplication;->setCurrentSlideShow(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    .line 1045
    :cond_d
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMain:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mOnLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    goto/16 :goto_0

    .line 1036
    :cond_e
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setNfcCallback()V

    goto :goto_4
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;

    .prologue
    .line 2189
    iput p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mDlgId:I

    .line 2190
    sparse-switch p1, :sswitch_data_0

    .line 2219
    invoke-super {p0, p1, p2}, Lcom/infraware/common/baseactivity/BaseActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2192
    :sswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v1

    invoke-direct {v0, p0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f070219

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07006b

    new-instance v2, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$21;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$21;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070062

    new-instance v2, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$20;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$20;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$19;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$19;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07021a

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mConformMsgPopup:Landroid/app/AlertDialog;

    .line 2213
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mConformMsgPopup:Landroid/app/AlertDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 2214
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mConformMsgPopup:Landroid/app/AlertDialog;

    goto :goto_0

    .line 2217
    :sswitch_1
    invoke-static {}, Lcom/infraware/polarisoffice5/common/DlgFactory;->getInstance()Lcom/infraware/polarisoffice5/common/DlgFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/infraware/polarisoffice5/common/DlgFactory;->createSaveProgressDlg(Landroid/app/Activity;)Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;

    move-result-object v0

    goto :goto_0

    .line 2190
    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_1
        0x25 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1971
    const-string/jumbo v0, "SlideShowActivity"

    const-string/jumbo v1, "onDestory"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1974
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->cancelModeSwitchTimer()V

    .line 1976
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->AutoSwitchTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 1978
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMain:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 1979
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMain:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mOnLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1982
    :cond_0
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsUseGLES:Z

    if-eqz v0, :cond_7

    .line 1983
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    if-eqz v0, :cond_1

    .line 1984
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1996
    :cond_1
    :goto_0
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mVideo:Landroid/widget/VideoView;

    .line 1997
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mHandlerLoop:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;->removeMessages(I)V

    .line 1998
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mHandlerLoop:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;->killHandlerLoop()V

    .line 2000
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbPPSFile:Z

    if-eqz v0, :cond_2

    .line 2003
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IClose()V

    .line 2005
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mInterfaceHandleAddress:I

    if-eqz v0, :cond_2

    .line 2007
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mInterfaceHandleAddress:I

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->deleteInterfaceHandle(I)V

    .line 2008
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mInterfaceHandleAddress:I

    .line 2011
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->CheckOpenedFileList(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2012
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->DeleteOpenedFileList(Ljava/lang/String;)Z

    .line 2017
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    if-eqz v0, :cond_3

    .line 2018
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/infraware/common/util/Utils;->unbindDrawables(Landroid/view/View;)V

    .line 2021
    :cond_3
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbPPSFile:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbFinishCalled:Z

    if-nez v0, :cond_4

    .line 2022
    invoke-static {}, Lcom/infraware/polarisoffice5/MyApplication;->getInstance()Lcom/infraware/polarisoffice5/MyApplication;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/MyApplication;->setCurrentSlideShow(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    .line 2023
    :cond_4
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onDestroy()V

    .line 2026
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_5

    .line 2027
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2028
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 2032
    :cond_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$CloseActionReceiver;

    if-eqz v0, :cond_6

    .line 2033
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$CloseActionReceiver;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2034
    :cond_6
    return-void

    .line 1987
    :cond_7
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    if-eqz v0, :cond_1

    .line 1988
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v3, 0x5

    .line 1816
    sparse-switch p1, :sswitch_data_0

    .line 1867
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/infraware/common/baseactivity/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 1819
    :sswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPrevPage:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onClickPage(Landroid/view/View;)V

    goto :goto_0

    .line 1823
    :sswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNextPage:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onClickPage(Landroid/view/View;)V

    goto :goto_0

    .line 1827
    :sswitch_2
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onFinish()V

    goto :goto_0

    .line 1830
    :sswitch_3
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mnPageJump:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNextPage:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onClickPage(Landroid/view/View;)V

    .line 1833
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowStartPage:I

    invoke-virtual {v0, v3, v1}, Lcom/infraware/office/evengine/EvInterface;->IIsNoneEffect(II)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1835
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->changeScreenForSlideShow()V

    .line 1836
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mnPageJump:I

    const/4 v2, -0x1

    invoke-virtual {v0, v3, v1, v2}, Lcom/infraware/office/evengine/EvInterface;->ISlideShowPlay(III)V

    .line 1837
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->updateSeekBarPos()V

    .line 1846
    :goto_2
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mnPageJump:I

    goto :goto_0

    .line 1831
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mnPageJump:I

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTotalPageNum:I

    if-le v0, v1, :cond_0

    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTotalPageNum:I

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mnPageJump:I

    goto :goto_1

    .line 1841
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->myRenderer:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mnPageJump:I

    invoke-virtual {v0, v3, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->onPlaySlideShow(II)V

    .line 1842
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->setViewMode(Z)V

    .line 1843
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->requestRender()V

    goto :goto_2

    .line 1858
    :sswitch_4
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mnPageJump:I

    mul-int/lit8 v0, v0, 0xa

    add-int/2addr v0, p1

    add-int/lit8 v0, v0, -0x7

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mnPageJump:I

    goto :goto_0

    .line 1861
    :sswitch_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPrevPage:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onClickPage(Landroid/view/View;)V

    goto :goto_0

    .line 1864
    :sswitch_6
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNextPage:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onClickPage(Landroid/view/View;)V

    goto :goto_0

    .line 1816
    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_4
        0x8 -> :sswitch_4
        0x9 -> :sswitch_4
        0xa -> :sswitch_4
        0xb -> :sswitch_4
        0xc -> :sswitch_4
        0xd -> :sswitch_4
        0xe -> :sswitch_4
        0xf -> :sswitch_4
        0x10 -> :sswitch_4
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0x42 -> :sswitch_3
        0x5c -> :sswitch_5
        0x5d -> :sswitch_6
        0x6f -> :sswitch_2
    .end sparse-switch
.end method

.method public onLocaleChange(I)V
    .locals 4
    .param p1, "nLocale"    # I

    .prologue
    .line 2710
    invoke-static {}, Lcom/infraware/common/util/Utils;->isICS()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p0}, Lcom/infraware/polarisoffice5/common/HoveringManager;->setHoveringButton(Landroid/app/Activity;)Z

    .line 2714
    :cond_0
    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mDlgId:I

    packed-switch v2, :pswitch_data_0

    .line 2732
    :goto_0
    return-void

    .line 2716
    :pswitch_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mConformMsgPopup:Landroid/app/AlertDialog;

    const v3, 0x7f07021a

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 2717
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mConformMsgPopup:Landroid/app/AlertDialog;

    const v3, 0x7f070219

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 2720
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mConformMsgPopup:Landroid/app/AlertDialog;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    .line 2722
    .local v1, "positiveBtn":Landroid/widget/Button;
    const v2, 0x7f07006b

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 2724
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mConformMsgPopup:Landroid/app/AlertDialog;

    const/4 v3, -0x2

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 2726
    .local v0, "negativeBtn":Landroid/widget/Button;
    const v2, 0x7f070062

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    .line 2714
    :pswitch_data_0
    .packed-switch 0x25
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 1903
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->AutoSwitchTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 1906
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbFinishCalled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IIsSlideShow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1909
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IStopSlideEffect()V

    .line 1912
    :cond_0
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onPause()V

    .line 1913
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 2177
    invoke-super {p0, p1, p2}, Lcom/infraware/common/baseactivity/BaseActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 2179
    packed-switch p1, :pswitch_data_0

    .line 2185
    :goto_0
    return-void

    .line 2182
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->removeDialog(I)V

    goto :goto_0

    .line 2179
    nop

    :pswitch_data_0
    .packed-switch 0x25
        :pswitch_0
    .end packed-switch
.end method

.method protected onRestart()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2039
    const-string/jumbo v1, "SlideShowActivity"

    const-string/jumbo v2, "onRestart"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2040
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->getWidth()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->getHeight()I

    move-result v1

    if-lez v1, :cond_0

    .line 2041
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->getHeight()I

    move-result v3

    invoke-virtual {v1, v4, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IChangeScreen(III)V

    .line 2047
    :goto_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bRestart:Z

    .line 2048
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onRestart()V

    .line 2049
    return-void

    .line 2044
    :cond_0
    const-string/jumbo v1, "window"

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 2045
    .local v0, "display":Landroid/view/Display;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v3

    invoke-virtual {v1, v4, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IChangeScreen(III)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v3, 0x0

    const/4 v8, 0x1

    .line 1872
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onResume()V

    .line 1874
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getInterfaceHandle()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->SetInterfaceHandleAddress(I)V

    .line 1875
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object v1, p0

    move-object v2, p0

    move-object v4, p0

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->ISetListener(Lcom/infraware/office/evengine/EvListener$ViewerListener;Lcom/infraware/office/evengine/EvListener$EditorListener;Lcom/infraware/office/evengine/EvListener$WordEditorListener;Lcom/infraware/office/evengine/EvListener$PptEditorListener;Lcom/infraware/office/evengine/EvListener$SheetEditorListener;Lcom/infraware/office/evengine/EvListener$PdfViewerListener;)V

    .line 1877
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v8, :cond_2

    .line 1878
    iput-boolean v8, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_OrientationPortrait:Z

    .line 1884
    :goto_0
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsUseGLES:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IIsNextEffect()Z

    move-result v0

    if-ne v0, v8, :cond_0

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->IsAutoSwitch()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1888
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->AutoSwitchTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 1891
    :cond_0
    const-string/jumbo v0, "SlideShowActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onResume m_OrientationPortrait = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_OrientationPortrait:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1893
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1894
    invoke-static {}, Lcom/infraware/common/util/Utils;->isJB()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1895
    new-array v7, v8, [Landroid/net/Uri;

    .line 1896
    .local v7, "filePath":[Landroid/net/Uri;
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mFilePath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    aput-object v0, v7, v9

    .line 1897
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v7, p0, v1}, Lcom/infraware/common/util/SbeamHelper;->setBeamUris([Landroid/net/Uri;Landroid/app/Activity;Landroid/content/Context;)V

    .line 1900
    .end local v7    # "filePath":[Landroid/net/Uri;
    :cond_1
    return-void

    .line 1880
    :cond_2
    iput-boolean v9, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_OrientationPortrait:Z

    goto :goto_0
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 1917
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onStart()V

    .line 1918
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 1922
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mHandlerLoop:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;

    if-eqz v0, :cond_0

    .line 1923
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mHandlerLoop:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;->stopHandlerLoop()V

    .line 1924
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->AutoSwitchTimer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_1

    .line 1925
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->AutoSwitchTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 1926
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->myRenderer:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;

    if-eqz v0, :cond_2

    .line 1927
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->myRenderer:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->setGlsync(Z)V

    .line 1931
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_3

    .line 1932
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IStopSlideEffect()V

    .line 1933
    :cond_3
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onStop()V

    .line 1934
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 7
    .param p1, "hasFocus"    # Z

    .prologue
    const/4 v3, 0x0

    .line 2888
    if-eqz p1, :cond_0

    .line 2889
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getInterfaceHandle()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->SetInterfaceHandleAddress(I)V

    .line 2890
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object v1, p0

    move-object v2, p0

    move-object v4, p0

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->ISetListener(Lcom/infraware/office/evengine/EvListener$ViewerListener;Lcom/infraware/office/evengine/EvListener$EditorListener;Lcom/infraware/office/evengine/EvListener$WordEditorListener;Lcom/infraware/office/evengine/EvListener$PptEditorListener;Lcom/infraware/office/evengine/EvListener$SheetEditorListener;Lcom/infraware/office/evengine/EvListener$PdfViewerListener;)V

    .line 2893
    :cond_0
    invoke-super {p0, p1}, Lcom/infraware/common/baseactivity/BaseActivity;->onWindowFocusChanged(Z)V

    .line 2894
    return-void
.end method

.method public positionPreviewImage(Landroid/view/MotionEvent;)V
    .locals 8
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x0

    .line 2258
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_THUMBNAILOFSLIDESHOW()Z

    move-result v5

    if-nez v5, :cond_0

    .line 2287
    :goto_0
    return-void

    .line 2260
    :cond_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v5}, Landroid/widget/SeekBar;->getWidth()I

    move-result v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v6}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v6

    sub-int/2addr v5, v6

    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v6}, Landroid/widget/SeekBar;->getPaddingRight()I

    move-result v6

    sub-int v0, v5, v6

    .line 2262
    .local v0, "available":I
    iget-object v5, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v5}, Landroid/widget/SeekBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2263
    .local v1, "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    div-int/lit8 v3, v5, 0x2

    .line 2265
    .local v3, "thumbWidth":I
    sub-int/2addr v0, v3

    .line 2266
    iget-object v5, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v5}, Landroid/widget/SeekBar;->getThumbOffset()I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v0, v5

    .line 2269
    int-to-float v5, v0

    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v6}, Landroid/widget/SeekBar;->getScaleX()F

    move-result v6

    mul-float/2addr v5, v6

    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v6}, Landroid/widget/SeekBar;->getProgress()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v5, v6

    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v6}, Landroid/widget/SeekBar;->getMax()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v6}, Landroid/widget/SeekBar;->getLeft()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v6}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    div-int/lit8 v6, v3, 0x2

    int-to-float v6, v6

    add-float/2addr v5, v6

    float-to-int v4, v5

    .line 2272
    .local v4, "xPos":I
    iget-object v5, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewImageFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 2274
    .local v2, "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    cmpl-float v5, v5, v7

    if-ltz v5, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v6}, Landroid/widget/SeekBar;->getWidth()I

    move-result v6

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_1

    .line 2275
    iget-object v5, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewImageFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int v5, v4, v5

    iput v5, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 2286
    :goto_1
    iget-object v5, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewImageFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 2277
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    cmpg-float v5, v5, v7

    if-gez v5, :cond_2

    .line 2278
    iget-object v5, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v5}, Landroid/widget/SeekBar;->getLeft()I

    move-result v4

    .line 2279
    iget-object v5, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewImageFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int v5, v4, v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v6}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v6

    add-int/2addr v5, v6

    div-int/lit8 v6, v3, 0x2

    add-int/2addr v5, v6

    iput v5, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    goto :goto_1

    .line 2282
    :cond_2
    iget-object v5, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v5}, Landroid/widget/SeekBar;->getRight()I

    move-result v4

    .line 2283
    iget-object v5, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewImageFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int v5, v4, v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v6}, Landroid/widget/SeekBar;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    div-int/lit8 v6, v3, 0x2

    sub-int/2addr v5, v6

    iput v5, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    goto :goto_1
.end method

.method public saveSeekBarHistroy()V
    .locals 3

    .prologue
    .line 2367
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_THUMBNAILOFSLIDESHOW()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2388
    :cond_0
    :goto_0
    return-void

    .line 2369
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mHandlerLoop:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;->isFinish()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2372
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getPageFromCurrentProgress()I

    move-result v0

    .line 2373
    .local v0, "page":I
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSeekBarMoveHistory:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2374
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSeekBarMoveHistory:Ljava/util/Stack;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2385
    :goto_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSeekBarMoveHistory:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v1

    const/16 v2, 0xa

    if-le v1, v2, :cond_0

    .line 2386
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSeekBarMoveHistory:Ljava/util/Stack;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/Stack;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 2376
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSeekBarMoveHistory:Ljava/util/Stack;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Stack;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2377
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSeekBarMoveHistory:Ljava/util/Stack;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Stack;->remove(Ljava/lang/Object;)Z

    .line 2378
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSeekBarMoveHistory:Ljava/util/Stack;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2381
    :cond_3
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSeekBarMoveHistory:Ljava/util/Stack;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public setEnableEraser()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1685
    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    const/4 v3, 0x4

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_1

    .line 1688
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v2

    iget v1, v2, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    .line 1689
    .local v1, "nPage":I
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2, v1}, Lcom/infraware/office/evengine/EvInterface;->IIsPenDataForSlideShow(I)I

    move-result v0

    .line 1691
    .local v0, "hasMarkerData":I
    if-nez v0, :cond_2

    .line 1692
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserMode:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1693
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserAllMode:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1694
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerMode:Landroid/widget/ImageView;

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onClickMode(Landroid/view/View;)V

    .line 1701
    .end local v0    # "hasMarkerData":I
    .end local v1    # "nPage":I
    :cond_1
    :goto_0
    return-void

    .line 1697
    .restart local v0    # "hasMarkerData":I
    .restart local v1    # "nPage":I
    :cond_2
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserMode:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1698
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserAllMode:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_0
.end method

.method public setEnablePage(Z)V
    .locals 1
    .param p1, "set"    # Z

    .prologue
    .line 1680
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPrevPage:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1681
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNextPage:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1682
    return-void
.end method

.method public setIsMarkerDrawing(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 207
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsMakerDrawing:Z

    return-void
.end method

.method public setPenDownNoUp(Z)V
    .locals 0
    .param p1, "isPenDownNoUp"    # Z

    .prologue
    .line 1130
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->isPenDownNoUp:Z

    .line 1131
    return-void
.end method

.method public setSupportSlideShow(Z)V
    .locals 0
    .param p1, "bSupportSlideShow"    # Z

    .prologue
    .line 1110
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bSupportSlideShow:Z

    .line 1111
    return-void
.end method

.method public setTouchCancel(Z)V
    .locals 0
    .param p1, "isTouchCancel"    # Z

    .prologue
    .line 1122
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->isTouchCancel:Z

    .line 1123
    return-void
.end method

.method protected setViewMode(Z)V
    .locals 6
    .param p1, "isUseGLES"    # Z

    .prologue
    const/16 v5, 0x9

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1784
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsUseGLES:Z

    if-ne v0, p1, :cond_1

    .line 1812
    :cond_0
    :goto_0
    return-void

    .line 1786
    :cond_1
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsUseGLES:Z

    .line 1787
    const-string/jumbo v0, "SlideShowActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "m_bIsUseGLES = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsUseGLES:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1788
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsUseGLES:Z

    if-eqz v0, :cond_4

    .line 1789
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    invoke-virtual {v0, v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->setVisibility(I)V

    .line 1793
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->IsAutoSwitch()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1795
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1796
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 1799
    :cond_2
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMode:I

    if-eq v0, v4, :cond_3

    .line 1800
    iput-boolean v4, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bCheckAutoSwitchAndMode:Z

    goto :goto_0

    .line 1802
    :cond_3
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bCheckAutoSwitchAndMode:Z

    goto :goto_0

    .line 1806
    :cond_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    invoke-virtual {v0, v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->setVisibility(I)V

    .line 1807
    const-string/jumbo v0, "SlideShowActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mSlideImage.getWidth() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "mSlideImage.getHeight() ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1808
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImage:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->drawAllContents()V

    .line 1809
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public updateSeekBarPos()V
    .locals 4

    .prologue
    .line 2240
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_THUMBNAILOFSLIDESHOW()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2255
    :goto_0
    return-void

    .line 2242
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v2

    iget v0, v2, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    .line 2244
    .local v0, "page":I
    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 2245
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 2248
    :cond_1
    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mTotalPageNum:I

    if-ne v0, v2, :cond_2

    .line 2249
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 2252
    :cond_2
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getProgressFromPage(I)I

    move-result v1

    .line 2253
    .local v1, "progress":I
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0
.end method

.method public videoStop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 2404
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mVideo:Landroid/widget/VideoView;

    if-eqz v0, :cond_2

    .line 2405
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mVideo:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2406
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mVideo:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->stopPlayback()V

    .line 2407
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mVideobtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 2408
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mGLGestureProc:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;

    iput v2, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->isPlay:I

    .line 2409
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_VideoFrame:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2410
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mVideobtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2411
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mVideo:Landroid/widget/VideoView;

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setVisibility(I)V

    .line 2413
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mVideo:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 2414
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mGLGestureProc:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;

    iput v2, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->isPlay:I

    .line 2415
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_VideoFrame:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2416
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mVideobtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2417
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mVideo:Landroid/widget/VideoView;

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setVisibility(I)V

    .line 2420
    :cond_2
    return-void
.end method
