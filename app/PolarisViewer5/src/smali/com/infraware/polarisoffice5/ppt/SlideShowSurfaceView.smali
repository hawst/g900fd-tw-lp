.class public Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;
.super Landroid/view/SurfaceView;
.source "SlideShowSurfaceView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView$SlideShowSurfaceViewListener;
    }
.end annotation


# static fields
.field protected static final SCREEN_SIZE_MISMATCH:I


# instance fields
.field private final LOG_CAT:Ljava/lang/String;

.field private bSurfaceCreated:Z

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mEvInterface:Lcom/infraware/office/evengine/EvInterface;

.field protected final mHandler:Landroid/os/Handler;

.field private mHeight:I

.field private mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView$SlideShowSurfaceViewListener;

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 62
    invoke-direct {p0, p1, p2, p3}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    const-string/jumbo v0, "SlideShowSurfaceView"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->LOG_CAT:Ljava/lang/String;

    .line 22
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 25
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->bSurfaceCreated:Z

    .line 28
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mBitmap:Landroid/graphics/Bitmap;

    .line 29
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mWidth:I

    .line 30
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mHeight:I

    .line 39
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView$1;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mHandler:Landroid/os/Handler;

    .line 63
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 64
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 65
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->bSurfaceCreated:Z

    .line 66
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    .prologue
    .line 18
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mWidth:I

    return v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;

    .prologue
    .line 18
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mHeight:I

    return v0
.end method


# virtual methods
.method public destory()V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mBitmap:Landroid/graphics/Bitmap;

    .line 81
    :cond_0
    return-void
.end method

.method public drawAllContents()V
    .locals 6

    .prologue
    .line 113
    const/4 v0, 0x0

    .line 115
    .local v0, "canvas":Landroid/graphics/Canvas;
    :try_start_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 116
    :try_start_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    .line 117
    if-eqz v0, :cond_0

    .line 119
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 120
    :cond_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 125
    if-eqz v0, :cond_1

    .line 126
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 128
    :cond_1
    :goto_0
    return-void

    .line 120
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v1
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 121
    :catch_0
    move-exception v1

    .line 125
    if-eqz v0, :cond_1

    .line 126
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 125
    :catchall_1
    move-exception v1

    if-eqz v0, :cond_2

    .line 126
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v2, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    :cond_2
    throw v1
.end method

.method public getBitmap(II)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "aScreenWidth"    # I
    .param p2, "aScreenHeight"    # I

    .prologue
    const/4 v3, 0x0

    .line 84
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-ne v1, p1, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-eq v1, p2, :cond_4

    .line 88
    :cond_0
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mWidth:I

    if-ne v1, p1, :cond_1

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mHeight:I

    if-eq v1, p2, :cond_2

    .line 89
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 91
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_3

    .line 92
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 93
    iput-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mBitmap:Landroid/graphics/Bitmap;

    .line 97
    :cond_3
    :try_start_0
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109
    :cond_4
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mBitmap:Landroid/graphics/Bitmap;

    return-object v1

    .line 99
    :catch_0
    move-exception v0

    .line 100
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    const/4 v1, 0x0

    :try_start_1
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 102
    .end local v0    # "e":Ljava/lang/OutOfMemoryError;
    :catchall_0
    move-exception v1

    throw v1
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->destory()V

    .line 176
    invoke-super {p0}, Landroid/view/SurfaceView;->onDetachedFromWindow()V

    .line 177
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 133
    :try_start_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    :goto_0
    return-void

    .line 134
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 181
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView$SlideShowSurfaceViewListener;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView$SlideShowSurfaceViewListener;

    invoke-interface {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView$SlideShowSurfaceViewListener;->onWindowFocusChanged()V

    .line 183
    :cond_0
    invoke-super {p0, p1}, Landroid/view/SurfaceView;->onWindowFocusChanged(Z)V

    .line 184
    return-void
.end method

.method public setEvInterface(Lcom/infraware/office/evengine/EvInterface;)V
    .locals 0
    .param p1, "evInterface"    # Lcom/infraware/office/evengine/EvInterface;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 70
    return-void
.end method

.method public setSlideShowSurfaceViewListener(Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView$SlideShowSurfaceViewListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView$SlideShowSurfaceViewListener;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView$SlideShowSurfaceViewListener;

    .line 74
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    const/4 v3, 0x1

    .line 141
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->bSurfaceCreated:Z

    if-nez v0, :cond_2

    .line 142
    const-string/jumbo v0, "SlideShowSurfaceView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "surfaceChanged_create width="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", height="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView$SlideShowSurfaceViewListener;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView$SlideShowSurfaceViewListener;

    invoke-interface {v0, p3, p4}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView$SlideShowSurfaceViewListener;->onSurfaceCreated(II)V

    .line 145
    :cond_0
    iput p3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mWidth:I

    .line 146
    iput p4, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mHeight:I

    .line 147
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->bSurfaceCreated:Z

    .line 160
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->drawAllContents()V

    .line 161
    return-void

    .line 150
    :cond_2
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mWidth:I

    if-ne v0, p3, :cond_3

    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mHeight:I

    if-eq v0, p4, :cond_1

    .line 151
    :cond_3
    const-string/jumbo v0, "SlideShowSurfaceView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "surfaceChanged_resize width="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", height="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    iput p3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mWidth:I

    .line 153
    iput p4, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mHeight:I

    .line 154
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v3, p3, p4}, Lcom/infraware/office/evengine/EvInterface;->IChangeScreen(III)V

    .line 155
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView$SlideShowSurfaceViewListener;

    if-eqz v0, :cond_1

    .line 156
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView$SlideShowSurfaceViewListener;

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView;->mHeight:I

    invoke-interface {v0, v1, p4}, Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView$SlideShowSurfaceViewListener;->onSurfaceChanged(II)V

    goto :goto_0
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 165
    const-string/jumbo v0, "SlideShowSurfaceView"

    const-string/jumbo v1, "surfaceCreated"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 170
    const-string/jumbo v0, "SlideShowSurfaceView"

    const-string/jumbo v1, "surfaceDestroyed"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    return-void
.end method
