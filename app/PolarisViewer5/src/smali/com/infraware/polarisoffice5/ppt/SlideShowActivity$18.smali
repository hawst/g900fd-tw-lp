.class Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$18;
.super Ljava/lang/Object;
.source "SlideShowActivity.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onInitModeOption()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V
    .locals 0

    .prologue
    .line 1263
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$18;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 1267
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$18;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # invokes: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->cancelModeSwitchTimer()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$2800(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    .line 1270
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$18;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$18;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowMain:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$3000(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/RelativeLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$18;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerMode:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$3700(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/ImageView;

    move-result-object v2

    const v3, 0x7f0702d0

    iget-object v4, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$18;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_OrientationPortrait:Z
    invoke-static {v4}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$3200(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/infraware/common/util/Utils;->slideshowToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;ILjava/lang/Boolean;)V

    .line 1271
    const/4 v0, 0x1

    return v0
.end method
