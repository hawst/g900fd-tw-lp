.class public Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;
.super Landroid/app/Activity;
.source "OfficeDownloadFromSamsungApps.java"


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mCheckBox:Landroid/widget/CheckBox;

.field private m_btnDownload:Landroid/widget/Button;

.field private m_nDownKey:I

.field private m_nLocaleCode:I

.field private m_nOrientation:I

.field private m_tvProductDownload:Landroid/widget/TextView;

.field private m_tvProductInfo1:Landroid/widget/TextView;

.field private m_tvProductInfo2:Landroid/widget/TextView;

.field onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field onNetWorkUseAlarmClickListener:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 37
    iput v0, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->m_nOrientation:I

    .line 38
    iput v0, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->m_nLocaleCode:I

    .line 39
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->m_nDownKey:I

    .line 183
    new-instance v0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps$2;-><init>(Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 189
    new-instance v0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps$3;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps$3;-><init>(Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->onNetWorkUseAlarmClickListener:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->onOkSamsungApps()V

    return-void
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->mCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->connectSamsungApps()V

    return-void
.end method

.method private connectSamsungApps()V
    .locals 3

    .prologue
    .line 237
    const-string/jumbo v1, "samsungapps://ProductDetail/com.infraware.polarisoffice5"

    .line 239
    .local v1, "string_of_uri":Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 240
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 241
    const v2, 0x14000020

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 244
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->startActivity(Landroid/content/Intent;)V

    .line 245
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->finish()V

    .line 246
    return-void
.end method

.method private onOkSamsungApps()V
    .locals 11

    .prologue
    const/4 v6, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 206
    iget-object v3, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->mActivity:Landroid/app/Activity;

    const/16 v4, 0x133

    invoke-static {v3, v4}, Lcom/infraware/common/define/CMModelDefine;->isShowConnetNetWorkPopup(Landroid/content/Context;I)Z

    move-result v3

    if-ne v3, v8, :cond_2

    .line 209
    iget-object v3, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->mActivity:Landroid/app/Activity;

    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 210
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030006

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 212
    .local v1, "networkView":Landroid/view/View;
    const v3, 0x7f0b003b

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 213
    .local v2, "tv":Landroid/widget/TextView;
    const v3, 0x7f0b003c

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->mCheckBox:Landroid/widget/CheckBox;

    .line 214
    iget-object v3, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/infraware/common/util/Utils;->getIsConnectWlan(Landroid/app/Activity;)Z

    move-result v3

    if-ne v3, v8, :cond_0

    .line 215
    const v3, 0x7f07005d

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 216
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_WLAN_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->mActivity:Landroid/app/Activity;

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->onNetWorkUseAlarmClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v7, v6, v8

    iget-object v7, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v7, v6, v10

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    .line 233
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    .end local v1    # "networkView":Landroid/view/View;
    .end local v2    # "tv":Landroid/widget/TextView;
    :goto_0
    return-void

    .line 221
    .restart local v0    # "inflater":Landroid/view/LayoutInflater;
    .restart local v1    # "networkView":Landroid/view/View;
    .restart local v2    # "tv":Landroid/widget/TextView;
    :cond_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/infraware/common/util/Utils;->getIsConnectMobileNetwork(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 222
    const v3, 0x7f07005b

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 223
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_MOBILE_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->mActivity:Landroid/app/Activity;

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->onNetWorkUseAlarmClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v7, v6, v8

    iget-object v7, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v7, v6, v10

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto :goto_0

    .line 227
    :cond_1
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_NETWORK_NOT_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto :goto_0

    .line 231
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    .end local v1    # "networkView":Landroid/view/View;
    .end local v2    # "tv":Landroid/widget/TextView;
    :cond_2
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->connectSamsungApps()V

    goto :goto_0
.end method


# virtual methods
.method public onApplyThemeResource(Landroid/content/res/Resources$Theme;IZ)V
    .locals 2
    .param p1, "theme"    # Landroid/content/res/Resources$Theme;
    .param p2, "resid"    # I
    .param p3, "first"    # Z

    .prologue
    .line 100
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onApplyThemeResource(Landroid/content/res/Resources$Theme;IZ)V

    .line 101
    const v0, 0x1030059

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 102
    return-void
.end method

.method public onCheckboxClicked(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 173
    move-object v1, p1

    check-cast v1, Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 176
    .local v0, "checked":Z
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 181
    :goto_0
    return-void

    .line 178
    :pswitch_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 176
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b003c
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "_newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 139
    iget v1, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->m_nOrientation:I

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v1, v2, :cond_0

    .line 140
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->onOrientationChanged()V

    .line 142
    :cond_0
    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v1}, Lcom/infraware/common/util/Utils;->getLocaleType(Ljava/util/Locale;)I

    move-result v0

    .line 143
    .local v0, "nLocale":I
    iget v1, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->m_nLocaleCode:I

    if-eq v1, v0, :cond_1

    .line 145
    iput v0, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->m_nLocaleCode:I

    .line 146
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->onLocaleChanged()V

    .line 149
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 150
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 52
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    const-string/jumbo v5, "window"

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 55
    .local v0, "display":Landroid/view/Display;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v6

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/view/Window;->setLayout(II)V

    .line 56
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->requestWindowFeature(I)Z

    .line 58
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 60
    invoke-static {p0}, Lcom/infraware/common/util/Utils;->fullScreenActivity(Landroid/app/Activity;)V

    .line 64
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->getCurrentLocaleType(Landroid/content/res/Resources;)I

    move-result v5

    iput v5, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->m_nLocaleCode:I

    .line 66
    const v5, 0x7f030039

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->setContentView(I)V

    .line 68
    const v5, 0x7f0b01ac

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->m_tvProductInfo1:Landroid/widget/TextView;

    .line 69
    const v5, 0x7f0b01ad

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->m_tvProductInfo2:Landroid/widget/TextView;

    .line 71
    const v5, 0x7f0b01ae

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->m_tvProductDownload:Landroid/widget/TextView;

    .line 73
    const v5, 0x7f0b01b2

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->m_btnDownload:Landroid/widget/Button;

    .line 74
    iget-object v5, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->m_btnDownload:Landroid/widget/Button;

    new-instance v6, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps$1;

    invoke-direct {v6, p0}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps$1;-><init>(Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;)V

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->onOrientationChanged()V

    .line 84
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->m_tvProductDownload:Landroid/widget/TextView;

    invoke-static {v5, v6}, Lcom/infraware/common/util/Utils;->changeDownloadMarketName(Landroid/content/Context;Landroid/view/View;)V

    .line 87
    :try_start_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 88
    .local v2, "pi":Landroid/content/pm/PackageInfo;
    iget-object v4, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 89
    .local v4, "versionName":Ljava/lang/String;
    const v5, 0x7f0b01b1

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 90
    .local v3, "tv_version_text":Landroid/widget/TextView;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "version "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    .end local v2    # "pi":Landroid/content/pm/PackageInfo;
    .end local v3    # "tv_version_text":Landroid/widget/TextView;
    .end local v4    # "versionName":Ljava/lang/String;
    :goto_0
    iput-object p0, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->mActivity:Landroid/app/Activity;

    .line 95
    return-void

    .line 91
    :catch_0
    move-exception v1

    .line 92
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 106
    iput p1, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->m_nDownKey:I

    .line 108
    const/16 v0, 0x6f

    if-ne p1, v0, :cond_0

    .line 109
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->finish()V

    .line 111
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v2, -0x1

    .line 117
    iget v1, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->m_nDownKey:I

    if-eq v1, p1, :cond_0

    .line 118
    iput v2, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->m_nDownKey:I

    .line 133
    :goto_0
    return v0

    .line 122
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 132
    :sswitch_0
    iput v2, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->m_nDownKey:I

    .line 133
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 128
    :sswitch_1
    iput v2, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->m_nDownKey:I

    goto :goto_0

    .line 122
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x52 -> :sswitch_1
        0x54 -> :sswitch_1
    .end sparse-switch
.end method

.method public onLocaleChanged()V
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->m_tvProductInfo1:Landroid/widget/TextView;

    const v1, 0x7f0702b7

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 155
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->m_tvProductInfo2:Landroid/widget/TextView;

    const v1, 0x7f0702b8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 156
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->m_tvProductDownload:Landroid/widget/TextView;

    const v1, 0x7f0702b2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 157
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->m_btnDownload:Landroid/widget/Button;

    const v1, 0x7f070063

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 159
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->m_tvProductDownload:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/infraware/common/util/Utils;->changeDownloadMarketName(Landroid/content/Context;Landroid/view/View;)V

    .line 160
    return-void
.end method

.method public onOrientationChanged()V
    .locals 2

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->m_nOrientation:I

    .line 165
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v0

    if-nez v0, :cond_0

    .line 167
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->getWindow()Landroid/view/Window;

    move-result-object v0

    iget v1, p0, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;->m_nOrientation:I

    invoke-static {v0, v1}, Lcom/infraware/common/util/Utils;->setScreenMode(Landroid/view/Window;I)V

    .line 169
    :cond_0
    return-void
.end method
