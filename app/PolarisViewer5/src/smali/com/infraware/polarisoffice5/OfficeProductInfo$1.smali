.class Lcom/infraware/polarisoffice5/OfficeProductInfo$1;
.super Ljava/lang/Object;
.source "OfficeProductInfo.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/OfficeProductInfo;->onOrientationChanged()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/OfficeProductInfo;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/OfficeProductInfo;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo$1;->this$0:Lcom/infraware/polarisoffice5/OfficeProductInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 183
    iget-object v3, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo$1;->this$0:Lcom/infraware/polarisoffice5/OfficeProductInfo;

    # getter for: Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_isPause:Z
    invoke-static {v3}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->access$000(Lcom/infraware/polarisoffice5/OfficeProductInfo;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 184
    iget-object v3, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo$1;->this$0:Lcom/infraware/polarisoffice5/OfficeProductInfo;

    # getter for: Lcom/infraware/polarisoffice5/OfficeProductInfo;->mActivity:Landroid/app/Activity;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->access$100(Lcom/infraware/polarisoffice5/OfficeProductInfo;)Landroid/app/Activity;

    move-result-object v3

    const/16 v4, 0x136

    invoke-static {v3, v4}, Lcom/infraware/common/define/CMModelDefine;->isShowConnetNetWorkPopup(Landroid/content/Context;I)Z

    move-result v3

    if-ne v3, v8, :cond_3

    .line 187
    iget-object v3, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo$1;->this$0:Lcom/infraware/polarisoffice5/OfficeProductInfo;

    # getter for: Lcom/infraware/polarisoffice5/OfficeProductInfo;->mActivity:Landroid/app/Activity;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->access$100(Lcom/infraware/polarisoffice5/OfficeProductInfo;)Landroid/app/Activity;

    move-result-object v3

    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 188
    .local v0, "inflater1":Landroid/view/LayoutInflater;
    const v3, 0x7f030006

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 190
    .local v1, "networkView":Landroid/view/View;
    const v3, 0x7f0b003b

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 191
    .local v2, "tv":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo$1;->this$0:Lcom/infraware/polarisoffice5/OfficeProductInfo;

    const v3, 0x7f0b003c

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    # setter for: Lcom/infraware/polarisoffice5/OfficeProductInfo;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v4, v3}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->access$202(Lcom/infraware/polarisoffice5/OfficeProductInfo;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 192
    iget-object v3, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo$1;->this$0:Lcom/infraware/polarisoffice5/OfficeProductInfo;

    # getter for: Lcom/infraware/polarisoffice5/OfficeProductInfo;->mActivity:Landroid/app/Activity;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->access$100(Lcom/infraware/polarisoffice5/OfficeProductInfo;)Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/infraware/common/util/Utils;->getIsConnectWlan(Landroid/app/Activity;)Z

    move-result v3

    if-ne v3, v8, :cond_1

    .line 193
    const v3, 0x7f07005d

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 194
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo$1;->this$0:Lcom/infraware/polarisoffice5/OfficeProductInfo;

    # getter for: Lcom/infraware/polarisoffice5/OfficeProductInfo;->mActivity:Landroid/app/Activity;
    invoke-static {v4}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->access$100(Lcom/infraware/polarisoffice5/OfficeProductInfo;)Landroid/app/Activity;

    move-result-object v4

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_WLAN_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo$1;->this$0:Lcom/infraware/polarisoffice5/OfficeProductInfo;

    # getter for: Lcom/infraware/polarisoffice5/OfficeProductInfo;->mActivity:Landroid/app/Activity;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->access$100(Lcom/infraware/polarisoffice5/OfficeProductInfo;)Landroid/app/Activity;

    move-result-object v7

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo$1;->this$0:Lcom/infraware/polarisoffice5/OfficeProductInfo;

    iget-object v7, v7, Lcom/infraware/polarisoffice5/OfficeProductInfo;->onNetWorkUseAlarmClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v7, v6, v8

    iget-object v7, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo$1;->this$0:Lcom/infraware/polarisoffice5/OfficeProductInfo;

    iget-object v7, v7, Lcom/infraware/polarisoffice5/OfficeProductInfo;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v7, v6, v10

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    .line 212
    .end local v0    # "inflater1":Landroid/view/LayoutInflater;
    .end local v1    # "networkView":Landroid/view/View;
    .end local v2    # "tv":Landroid/widget/TextView;
    :cond_0
    :goto_0
    return-void

    .line 199
    .restart local v0    # "inflater1":Landroid/view/LayoutInflater;
    .restart local v1    # "networkView":Landroid/view/View;
    .restart local v2    # "tv":Landroid/widget/TextView;
    :cond_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo$1;->this$0:Lcom/infraware/polarisoffice5/OfficeProductInfo;

    # getter for: Lcom/infraware/polarisoffice5/OfficeProductInfo;->mActivity:Landroid/app/Activity;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->access$100(Lcom/infraware/polarisoffice5/OfficeProductInfo;)Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/infraware/common/util/Utils;->getIsConnectMobileNetwork(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 200
    const v3, 0x7f07005b

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 201
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo$1;->this$0:Lcom/infraware/polarisoffice5/OfficeProductInfo;

    # getter for: Lcom/infraware/polarisoffice5/OfficeProductInfo;->mActivity:Landroid/app/Activity;
    invoke-static {v4}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->access$100(Lcom/infraware/polarisoffice5/OfficeProductInfo;)Landroid/app/Activity;

    move-result-object v4

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_MOBILE_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo$1;->this$0:Lcom/infraware/polarisoffice5/OfficeProductInfo;

    # getter for: Lcom/infraware/polarisoffice5/OfficeProductInfo;->mActivity:Landroid/app/Activity;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->access$100(Lcom/infraware/polarisoffice5/OfficeProductInfo;)Landroid/app/Activity;

    move-result-object v7

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo$1;->this$0:Lcom/infraware/polarisoffice5/OfficeProductInfo;

    iget-object v7, v7, Lcom/infraware/polarisoffice5/OfficeProductInfo;->onNetWorkUseAlarmClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v7, v6, v8

    iget-object v7, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo$1;->this$0:Lcom/infraware/polarisoffice5/OfficeProductInfo;

    iget-object v7, v7, Lcom/infraware/polarisoffice5/OfficeProductInfo;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v7, v6, v10

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto :goto_0

    .line 205
    :cond_2
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo$1;->this$0:Lcom/infraware/polarisoffice5/OfficeProductInfo;

    # getter for: Lcom/infraware/polarisoffice5/OfficeProductInfo;->mActivity:Landroid/app/Activity;
    invoke-static {v4}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->access$100(Lcom/infraware/polarisoffice5/OfficeProductInfo;)Landroid/app/Activity;

    move-result-object v4

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_NETWORK_NOT_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto :goto_0

    .line 209
    .end local v0    # "inflater1":Landroid/view/LayoutInflater;
    .end local v1    # "networkView":Landroid/view/View;
    .end local v2    # "tv":Landroid/widget/TextView;
    :cond_3
    iget-object v3, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo$1;->this$0:Lcom/infraware/polarisoffice5/OfficeProductInfo;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->connectInfraHome()V

    goto :goto_0
.end method
