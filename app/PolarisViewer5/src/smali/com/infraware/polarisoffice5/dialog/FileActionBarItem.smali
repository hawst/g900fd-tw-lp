.class public Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;
.super Ljava/lang/Object;
.source "FileActionBarItem.java"


# instance fields
.field public m_bEnable:Z

.field public m_nIconId:I

.field public m_nMenuId:I

.field public m_nServiceType:I

.field public m_strPath:Ljava/lang/String;

.field public m_strText:Ljava/lang/String;

.field public m_strUserId:Ljava/lang/String;


# direct methods
.method public constructor <init>(IILjava/lang/String;)V
    .locals 1
    .param p1, "menuId"    # I
    .param p2, "iconId"    # I
    .param p3, "strText"    # Ljava/lang/String;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput p1, p0, Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;->m_nMenuId:I

    .line 17
    iput p2, p0, Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;->m_nIconId:I

    .line 18
    iput-object p3, p0, Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;->m_strText:Ljava/lang/String;

    .line 19
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;->m_strUserId:Ljava/lang/String;

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;->m_bEnable:Z

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;->m_nServiceType:I

    .line 23
    return-void
.end method
