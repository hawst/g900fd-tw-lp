.class public Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;
.super Ljava/lang/Object;
.source "FileManagerDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;
    }
.end annotation


# instance fields
.field private listener:Lcom/infraware/polarisoffice5/dialog/FileManagerDialogListener;

.field private m_bEditFolder:Z

.field private m_bLocal:Z

.field private m_nDialogType:I

.field private m_nItemCount:I

.field private m_nMaxLength:I

.field private m_nMessage:I

.field private m_nTitleId:I

.field private m_oDialog:Landroid/app/AlertDialog;

.field private m_oEditText:Landroid/widget/EditText;

.field private m_oParent:Landroid/app/Activity;

.field private m_oTextView:Landroid/widget/TextView;

.field private m_strAccount:Ljava/lang/String;

.field private m_strExt:Ljava/lang/String;

.field private m_strFileName:Ljava/lang/String;

.field private m_strName:Ljava/lang/String;

.field private m_strParentPath:Ljava/lang/String;

.field private m_strPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/infraware/filemanager/file/FileListItem;)V
    .locals 3
    .param p1, "parent"    # Landroid/app/Activity;
    .param p2, "item"    # Lcom/infraware/filemanager/file/FileListItem;

    .prologue
    const/4 v2, 0x0

    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oParent:Landroid/app/Activity;

    .line 35
    iput-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    .line 37
    iput-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oEditText:Landroid/widget/EditText;

    .line 38
    iput-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oTextView:Landroid/widget/TextView;

    .line 40
    iput v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nDialogType:I

    .line 42
    iput v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nTitleId:I

    .line 43
    iput v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nMessage:I

    .line 45
    iput v2, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nItemCount:I

    .line 47
    sget v0, Lcom/infraware/filemanager/define/FMDefine;->MAX_FILENAME_LENGTH:I

    iput v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nMaxLength:I

    .line 49
    iput-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strName:Ljava/lang/String;

    .line 50
    iput-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strFileName:Ljava/lang/String;

    .line 51
    iput-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strParentPath:Ljava/lang/String;

    .line 52
    iput-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strExt:Ljava/lang/String;

    .line 53
    iput-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strPath:Ljava/lang/String;

    .line 54
    iput-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strAccount:Ljava/lang/String;

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_bLocal:Z

    .line 56
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_bEditFolder:Z

    .line 60
    iput-object p1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oParent:Landroid/app/Activity;

    .line 61
    if-eqz p2, :cond_0

    .line 62
    invoke-virtual {p2}, Lcom/infraware/filemanager/file/FileListItem;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strPath:Ljava/lang/String;

    .line 63
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    .prologue
    .line 32
    iget v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nDialogType:I

    return v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->isExist(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oParent:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;)Lcom/infraware/polarisoffice5/dialog/FileManagerDialogListener;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->listener:Lcom/infraware/polarisoffice5/dialog/FileManagerDialogListener;

    return-object v0
.end method

.method private decideFolderName(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 10
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "isLocal"    # Z

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 503
    new-instance v3, Ljava/lang/String;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oParent:Landroid/app/Activity;

    const v5, 0x7f0702ab

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 505
    .local v3, "prefix":Ljava/lang/String;
    const-string/jumbo v4, "/"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 506
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 508
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/infraware/common/util/FileUtils;->isExist(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 516
    .end local v3    # "prefix":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 511
    .restart local v3    # "prefix":Ljava/lang/String;
    :cond_1
    const/4 v0, 0x1

    .line 512
    .local v0, "index":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "%d"

    new-array v6, v9, [Ljava/lang/Object;

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "index":I
    .local v1, "index":I
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .local v2, "newFolder":Ljava/lang/String;
    move v0, v1

    .line 513
    .end local v1    # "index":I
    .restart local v0    # "index":I
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/infraware/common/util/FileUtils;->isExist(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 514
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "%d"

    new-array v6, v9, [Ljava/lang/Object;

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "index":I
    .restart local v1    # "index":I
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move v0, v1

    .end local v1    # "index":I
    .restart local v0    # "index":I
    goto :goto_1

    :cond_2
    move-object v3, v2

    .line 516
    goto :goto_0
.end method

.method private isExist(Ljava/lang/String;)Z
    .locals 8
    .param p1, "inputName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 472
    :try_start_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strParentPath:Ljava/lang/String;

    .line 473
    .local v1, "strFilePath":Ljava/lang/String;
    const-string/jumbo v4, "/"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 474
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 478
    :goto_0
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v4

    iget-object v5, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oParent:Landroid/app/Activity;

    const/16 v6, 0xd

    const/4 v7, 0x1

    invoke-virtual {v4, v5, v6, v7}, Lcom/infraware/common/config/RuntimeConfig;->getBooleanPreference(Landroid/content/Context;IZ)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_bLocal:Z

    if-nez v4, :cond_1

    .line 480
    :cond_0
    iget-object v4, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strPath:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strExt:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_1

    .line 481
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strExt:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 484
    :cond_1
    invoke-static {v1}, Lcom/infraware/common/util/FileUtils;->isExist(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 489
    .end local v1    # "strFilePath":Ljava/lang/String;
    :goto_1
    return v2

    .line 476
    .restart local v1    # "strFilePath":Ljava/lang/String;
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :cond_3
    move v2, v3

    .line 487
    goto :goto_1

    .line 488
    .end local v1    # "strFilePath":Ljava/lang/String;
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/NullPointerException;
    move v2, v3

    .line 489
    goto :goto_1
.end method

.method private showPromptFileName()V
    .locals 14

    .prologue
    const v13, 0x7f070063

    const v12, 0x7f07005f

    const/16 v11, 0x8

    const/4 v10, -0x1

    const/4 v9, 0x0

    .line 250
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oParent:Landroid/app/Activity;

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 251
    .local v2, "layoutInflater":Landroid/view/LayoutInflater;
    const v6, 0x7f030036

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 253
    .local v3, "oView":Landroid/view/View;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oParent:Landroid/app/Activity;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v7

    invoke-direct {v0, v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 255
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nTitleId:I

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 258
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 260
    new-instance v6, Lcom/infraware/polarisoffice5/dialog/FileManagerDialogListener;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oParent:Landroid/app/Activity;

    iget v8, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nDialogType:I

    invoke-direct {v6, v7, v8}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialogListener;-><init>(Landroid/app/Activity;I)V

    iput-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->listener:Lcom/infraware/polarisoffice5/dialog/FileManagerDialogListener;

    .line 261
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->listener:Lcom/infraware/polarisoffice5/dialog/FileManagerDialogListener;

    invoke-virtual {v0, v13, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 262
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->listener:Lcom/infraware/polarisoffice5/dialog/FileManagerDialogListener;

    invoke-virtual {v0, v12, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 264
    iget v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nDialogType:I

    packed-switch v6, :pswitch_data_0

    .line 274
    :pswitch_0
    iget v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nMessage:I

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 278
    :goto_0
    :pswitch_1
    iget v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nDialogType:I

    packed-switch v6, :pswitch_data_1

    .line 369
    :goto_1
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    if-eqz v6, :cond_0

    .line 371
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    new-instance v7, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$3;

    invoke-direct {v7, p0}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$3;-><init>(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 378
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    new-instance v7, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$4;

    invoke-direct {v7, p0}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$4;-><init>(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 394
    :cond_0
    return-void

    .line 267
    :pswitch_2
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oParent:Landroid/app/Activity;

    iget v7, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nMessage:I

    invoke-virtual {v6, v7}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    iget v8, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nItemCount:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 285
    :pswitch_3
    const v6, 0x7f0b01a4

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    iput-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oEditText:Landroid/widget/EditText;

    .line 286
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oEditText:Landroid/widget/EditText;

    const-string/jumbo v7, "inputType=filename"

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 287
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oEditText:Landroid/widget/EditText;

    invoke-virtual {v6, v11}, Landroid/widget/EditText;->setVisibility(I)V

    .line 289
    const v6, 0x7f0b01a5

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oTextView:Landroid/widget/TextView;

    .line 290
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 292
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    .line 293
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 294
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    new-instance v7, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$1;

    invoke-direct {v7, p0}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$1;-><init>(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 302
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6}, Landroid/app/AlertDialog;->show()V

    .line 304
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6, v10}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v6

    invoke-virtual {v6, v13}, Landroid/widget/Button;->setText(I)V

    .line 305
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    const/4 v7, -0x2

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v6

    invoke-virtual {v6, v12}, Landroid/widget/Button;->setText(I)V

    goto/16 :goto_1

    .line 311
    :pswitch_4
    const v6, 0x7f0b01a4

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    iput-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oEditText:Landroid/widget/EditText;

    .line 312
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oEditText:Landroid/widget/EditText;

    const-string/jumbo v7, "inputType=filename"

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 313
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oEditText:Landroid/widget/EditText;

    invoke-virtual {v6, v9}, Landroid/widget/EditText;->setVisibility(I)V

    .line 314
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oEditText:Landroid/widget/EditText;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strFileName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 316
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strFileName:Ljava/lang/String;

    if-eqz v6, :cond_1

    .line 318
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strFileName:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    iget v7, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nMaxLength:I

    if-le v6, v7, :cond_4

    .line 319
    iget-object v4, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strFileName:Ljava/lang/String;

    .line 320
    .local v4, "orgFilename":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 321
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oEditText:Landroid/widget/EditText;

    iget v7, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nMaxLength:I

    invoke-virtual {v4, v9, v7}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 322
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oEditText:Landroid/widget/EditText;

    iget v7, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nMaxLength:I

    invoke-virtual {v6, v9, v7}, Landroid/widget/EditText;->setSelection(II)V

    .line 334
    .end local v4    # "orgFilename":Ljava/lang/String;
    :cond_1
    :goto_2
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->listener:Lcom/infraware/polarisoffice5/dialog/FileManagerDialogListener;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strFileName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialogListener;->setInputName(Ljava/lang/String;)V

    .line 336
    new-instance v1, Lcom/infraware/filemanager/file/FileInputFilter;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oParent:Landroid/app/Activity;

    invoke-direct {v1, v6}, Lcom/infraware/filemanager/file/FileInputFilter;-><init>(Landroid/content/Context;)V

    .line 337
    .local v1, "inputFilter":Lcom/infraware/filemanager/file/FileInputFilter;
    iget v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nMaxLength:I

    invoke-virtual {v1, v6}, Lcom/infraware/filemanager/file/FileInputFilter;->setMaxLength(I)V

    .line 338
    iget-boolean v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_bEditFolder:Z

    invoke-virtual {v1, v6}, Lcom/infraware/filemanager/file/FileInputFilter;->setEditFolder(Z)V

    .line 339
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Lcom/infraware/filemanager/file/FileInputFilter;->getFilters()[Landroid/text/InputFilter;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 341
    new-instance v5, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;

    invoke-direct {v5, p0}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;-><init>(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;)V

    .line 342
    .local v5, "watcher":Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oEditText:Landroid/widget/EditText;

    invoke-virtual {v6, v5}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 344
    const v6, 0x7f0b01a5

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oTextView:Landroid/widget/TextView;

    .line 345
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 347
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    .line 348
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 349
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    new-instance v7, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$2;

    invoke-direct {v7, p0}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$2;-><init>(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 357
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6}, Landroid/app/AlertDialog;->show()V

    .line 359
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6, v10}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v6

    invoke-virtual {v6, v13}, Landroid/widget/Button;->setText(I)V

    .line 360
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    const/4 v7, -0x2

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v6

    invoke-virtual {v6, v12}, Landroid/widget/Button;->setText(I)V

    .line 362
    iget v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nDialogType:I

    const/4 v7, 0x5

    if-eq v6, v7, :cond_2

    iget v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nDialogType:I

    const/4 v7, 0x6

    if-ne v6, v7, :cond_3

    .line 363
    :cond_2
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6, v10}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/widget/Button;->setEnabled(Z)V

    .line 365
    :cond_3
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v6

    const/16 v7, 0x25

    invoke-virtual {v6, v7}, Landroid/view/Window;->setSoftInputMode(I)V

    goto/16 :goto_1

    .line 327
    .end local v1    # "inputFilter":Lcom/infraware/filemanager/file/FileInputFilter;
    .end local v5    # "watcher":Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;
    :cond_4
    iget v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nDialogType:I

    const/4 v7, 0x5

    if-ne v6, v7, :cond_5

    .line 328
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oEditText:Landroid/widget/EditText;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strName:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v9, v7}, Landroid/widget/EditText;->setSelection(II)V

    goto/16 :goto_2

    .line 330
    :cond_5
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oEditText:Landroid/widget/EditText;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strFileName:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v9, v7}, Landroid/widget/EditText;->setSelection(II)V

    goto/16 :goto_2

    .line 264
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 278
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 83
    :cond_0
    return-void
.end method

.method public inputNameDlg(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;
    .param p4, "ext"    # Ljava/lang/String;
    .param p5, "isLocal"    # Z

    .prologue
    .line 156
    iput p1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nDialogType:I

    .line 157
    iput-object p2, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strFileName:Ljava/lang/String;

    .line 158
    iput-object p3, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strParentPath:Ljava/lang/String;

    .line 159
    iput-object p4, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strExt:Ljava/lang/String;

    .line 160
    iput-boolean p5, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_bLocal:Z

    .line 162
    iget v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nDialogType:I

    packed-switch v0, :pswitch_data_0

    .line 193
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->showPromptFileName()V

    .line 194
    return-void

    .line 181
    :pswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_bEditFolder:Z

    .line 182
    sget v0, Lcom/infraware/filemanager/define/FMDefine;->MAX_FILENAME_LENGTH:I

    add-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nMaxLength:I

    .line 183
    const v0, 0x7f0702a2

    iput v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nTitleId:I

    .line 184
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strFileName:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strFileName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 185
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strParentPath:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_bLocal:Z

    invoke-direct {p0, v0, v1}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->decideFolderName(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strFileName:Ljava/lang/String;

    goto :goto_0

    .line 162
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
    .end packed-switch
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    .line 74
    const/4 v0, 0x0

    .line 76
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 87
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 91
    return-void
.end method

.method public onSDCardRemoved()V
    .locals 2

    .prologue
    .line 137
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strPath:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strPath:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_1

    .line 139
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 140
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 141
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->dismiss()V

    .line 152
    .end local v0    # "file":Ljava/io/File;
    :cond_0
    :goto_0
    return-void

    .line 146
    :cond_1
    iget v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nDialogType:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 150
    :pswitch_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->dismiss()V

    goto :goto_0

    .line 146
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onShow()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 95
    iget v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nDialogType:I

    if-ne v0, v5, :cond_0

    .line 133
    :goto_0
    return-void

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    iget v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nTitleId:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 100
    iget v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nDialogType:I

    packed-switch v0, :pswitch_data_0

    .line 116
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oParent:Landroid/app/Activity;

    iget v2, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nMessage:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 120
    :goto_1
    :pswitch_1
    iget v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nDialogType:I

    packed-switch v0, :pswitch_data_1

    :pswitch_2
    goto :goto_0

    .line 129
    :pswitch_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f070063

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 130
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f07005f

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    .line 103
    :pswitch_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oParent:Landroid/app/Activity;

    iget v2, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nMessage:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    iget v3, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nItemCount:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 106
    :pswitch_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oParent:Landroid/app/Activity;

    iget v2, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nMessage:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strAccount:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 109
    :pswitch_6
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oParent:Landroid/app/Activity;

    iget v2, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nMessage:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_strAccount:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 100
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 120
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method
