.class Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$1;
.super Ljava/lang/Object;
.source "FileSelectActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->isSdCardAction(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;

.field final synthetic val$currentPath:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1278
    iput-object p1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$1;->this$0:Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;

    iput-object p2, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$1;->val$currentPath:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1281
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$1;->val$currentPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1282
    .local v0, "file":Ljava/io/File;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$1;->val$currentPath:Ljava/lang/String;

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->canExecute()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1283
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$1;->this$0:Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->initDeviceStoargeState()V

    .line 1285
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$1;->this$0:Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->onUpdateList()V

    .line 1286
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$1;->this$0:Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;

    # invokes: Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->makeSDCardList()V
    invoke-static {v1}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->access$000(Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;)V

    .line 1287
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$1;->this$0:Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;

    # invokes: Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->setTitle()V
    invoke-static {v1}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->access$100(Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;)V

    .line 1289
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$1;->this$0:Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oMenuPopup:Landroid/widget/PopupWindow;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$1;->this$0:Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oMenuPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1290
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$1;->this$0:Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oMenuPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    .line 1291
    :cond_2
    return-void
.end method
