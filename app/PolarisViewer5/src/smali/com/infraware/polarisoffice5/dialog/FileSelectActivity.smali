.class public Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;
.super Lcom/infraware/filemanager/file/FileBaseActivity;
.source "FileSelectActivity.java"

# interfaces
.implements Lcom/infraware/common/event/SdCardListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$CloseActionReceiver;
    }
.end annotation


# instance fields
.field public final MOVE_POPUP_MAX_WIDTH_LAND:F

.field public final MOVE_POPUP_MAX_WIDTH_PORT:F

.field public final MOVE_POPUP_MIN_WIDTH:F

.field private m_layoutTextTitle:Landroid/widget/LinearLayout;

.field public m_nMaxItem:I

.field public m_nMoveWidthMax:I

.field public m_nMoveWidthMin:I

.field public m_oActionList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;",
            ">;"
        }
    .end annotation
.end field

.field private m_oCloseFilter:Landroid/content/IntentFilter;

.field private m_oCloseReceiver:Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$CloseActionReceiver;

.field private m_oDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

.field public m_oFileDialog:Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

.field private m_oItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field public m_oMenuPopup:Landroid/widget/PopupWindow;

.field public m_oReceiver:Lcom/infraware/common/event/SdCardEvent;

.field private m_oTitleClickListener:Landroid/view/View$OnClickListener;

.field private m_strNewName:Ljava/lang/String;

.field private m_strOriginFile:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;-><init>()V

    .line 54
    const/high16 v0, 0x43290000    # 169.0f

    iput v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->MOVE_POPUP_MIN_WIDTH:F

    .line 55
    const/high16 v0, 0x43700000    # 240.0f

    iput v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->MOVE_POPUP_MAX_WIDTH_PORT:F

    .line 56
    const/high16 v0, 0x43b50000    # 362.0f

    iput v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->MOVE_POPUP_MAX_WIDTH_LAND:F

    .line 58
    iput-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oMenuPopup:Landroid/widget/PopupWindow;

    .line 59
    iput v2, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nMoveWidthMin:I

    .line 60
    iput v2, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nMoveWidthMax:I

    .line 61
    iput v2, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nMaxItem:I

    .line 63
    iput-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oReceiver:Lcom/infraware/common/event/SdCardEvent;

    .line 69
    iput-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_strOriginFile:Ljava/lang/String;

    .line 75
    iput-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oFileDialog:Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    .line 77
    iput-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_strNewName:Ljava/lang/String;

    .line 81
    iput-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$CloseActionReceiver;

    .line 82
    iput-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    .line 1331
    new-instance v0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$2;-><init>(Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oTitleClickListener:Landroid/view/View$OnClickListener;

    .line 1491
    new-instance v0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$3;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$3;-><init>(Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

    .line 1501
    new-instance v0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$4;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$4;-><init>(Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1678
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->makeSDCardList()V

    return-void
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->setTitle()V

    return-void
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->showActionBarMovePopup()V

    return-void
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_layoutTextTitle:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_tvTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->dismissPopupWindow()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;ILjava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->onMoveStorage(ILjava/lang/String;I)V

    return-void
.end method

.method private dismissPopupWindow()Z
    .locals 1

    .prologue
    .line 1610
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oMenuPopup:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 1612
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oMenuPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 1613
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oMenuPopup:Landroid/widget/PopupWindow;

    .line 1615
    const/4 v0, 0x1

    .line 1617
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getIcon(Ljava/lang/String;)I
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1691
    const v0, 0x7f020069

    .line 1692
    .local v0, "icon":I
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1693
    const v0, 0x7f020067

    .line 1701
    :cond_0
    :goto_0
    return v0

    .line 1694
    :cond_1
    const-string/jumbo v1, "/storage/extSdCard"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1695
    const v0, 0x7f020069

    goto :goto_0

    .line 1696
    :cond_2
    const-string/jumbo v1, "/storage/UsbDrive"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1697
    const v0, 0x7f02006a

    goto :goto_0

    .line 1698
    :cond_3
    const-string/jumbo v1, "/storage/PersonalPage"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1699
    const v0, 0x7f020068

    goto :goto_0
.end method

.method private onMoveStorage(ILjava/lang/String;I)V
    .locals 9
    .param p1, "menuId"    # I
    .param p2, "accountId"    # Ljava/lang/String;
    .param p3, "serviceType"    # I

    .prologue
    const/4 v1, 0x1

    .line 1521
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 1522
    .local v8, "result":Landroid/content/Intent;
    packed-switch p1, :pswitch_data_0

    .line 1562
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->onUpdateList()V

    .line 1563
    return-void

    .line 1525
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_tvTitle:Landroid/widget/TextView;

    const/high16 v2, 0x41900000    # 18.0f

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1526
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_tvTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1528
    const/4 v7, 0x0

    .line 1529
    .local v7, "path":Ljava/lang/String;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_1

    .line 1531
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/filemanager/file/FileListItem;

    iget-object v0, v0, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1532
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/filemanager/file/FileListItem;

    iget-object v7, v0, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    .line 1529
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 1534
    :cond_1
    const-string/jumbo v0, "key_list_type"

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1535
    const-string/jumbo v2, ""

    const-string/jumbo v3, ""

    const-string/jumbo v4, ""

    const/4 v5, -0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->setStorageType(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1536
    invoke-virtual {p0, v7, v1}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->setCurPath(Ljava/lang/String;Z)V

    .line 1537
    const-string/jumbo v0, ""

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->setCurFileId(Ljava/lang/String;)V

    goto :goto_0

    .line 1522
    :pswitch_data_0
    .packed-switch 0x3d
        :pswitch_0
    .end packed-switch
.end method

.method private onNewFolder()V
    .locals 6

    .prologue
    .line 1000
    const/4 v5, 0x1

    .line 1003
    .local v5, "isLocal":Z
    new-instance v0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;-><init>(Landroid/app/Activity;Lcom/infraware/filemanager/file/FileListItem;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oFileDialog:Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    .line 1008
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oFileDialog:Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    const/4 v1, 0x7

    const-string/jumbo v2, ""

    iget-object v3, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_strCurPath:Ljava/lang/String;

    const-string/jumbo v4, ""

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->inputNameDlg(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1009
    return-void
.end method

.method private setActionBarList(I)V
    .locals 6
    .param p1, "menuId"    # I

    .prologue
    .line 1415
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oActionList:Ljava/util/ArrayList;

    .line 1417
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1419
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/filemanager/file/FileListItem;

    iget-object v1, v1, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_tvTitle:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1420
    iget-object v2, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oActionList:Ljava/util/ArrayList;

    new-instance v3, Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;

    const/16 v4, 0x3d

    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/filemanager/file/FileListItem;

    iget-object v1, v1, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->getIcon(Ljava/lang/String;)I

    move-result v5

    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/filemanager/file/FileListItem;

    iget-object v1, v1, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    invoke-direct {v3, v4, v5, v1}, Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;-><init>(IILjava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1417
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1489
    :cond_1
    return-void
.end method

.method private setInfoText()V
    .locals 13

    .prologue
    const v12, 0x7f05007a

    const/16 v11, 0x21

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, -0x1

    .line 1137
    iget v6, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nInfoId:I

    if-nez v6, :cond_0

    .line 1139
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_tvInfo:Landroid/widget/TextView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1224
    :goto_0
    return-void

    .line 1157
    :cond_0
    invoke-static {}, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->useExternalSD()Z

    move-result v6

    if-eqz v6, :cond_3

    iget v6, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nServiceType:I

    if-ne v6, v8, :cond_3

    .line 1159
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_strCurPath:Ljava/lang/String;

    iget v7, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nStorageType:I

    invoke-virtual {p0, v7}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->getRootPath(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1161
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_tvInfo:Landroid/widget/TextView;

    const v7, 0x7f070033

    invoke-virtual {p0, v7}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1165
    :cond_1
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_strCurPath:Ljava/lang/String;

    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->getSDCardName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1166
    .local v4, "sdCardName":Ljava/lang/String;
    if-eqz v4, :cond_3

    .line 1168
    iget v6, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nInfoId:I

    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v4, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1169
    .local v3, "outputText":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 1171
    .local v1, "index":I
    if-ne v1, v8, :cond_2

    .line 1173
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_tvInfo:Landroid/widget/TextView;

    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1177
    :cond_2
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1178
    .local v5, "span":Landroid/text/SpannableStringBuilder;
    invoke-virtual {v5, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1179
    new-instance v6, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-direct {v6, v7}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v7, v1

    add-int/lit8 v7, v7, 0x2

    invoke-virtual {v5, v6, v1, v7, v11}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1180
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_tvInfo:Landroid/widget/TextView;

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1186
    .end local v1    # "index":I
    .end local v3    # "outputText":Ljava/lang/String;
    .end local v4    # "sdCardName":Ljava/lang/String;
    .end local v5    # "span":Landroid/text/SpannableStringBuilder;
    :cond_3
    const-string/jumbo v0, ""

    .line 1188
    .local v0, "currentFolder":Ljava/lang/String;
    iget v6, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nServiceType:I

    if-ne v6, v8, :cond_6

    .line 1190
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_strCurPath:Ljava/lang/String;

    const-string/jumbo v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 1191
    .restart local v1    # "index":I
    if-gtz v1, :cond_5

    .line 1192
    const-string/jumbo v0, "/"

    .line 1205
    .end local v1    # "index":I
    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    .line 1206
    .local v2, "nameLength":I
    const/16 v6, 0xf

    if-le v2, v6, :cond_4

    .line 1207
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v7, 0x7

    invoke-virtual {v0, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "..."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    add-int/lit8 v7, v2, -0x7

    invoke-virtual {v0, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1211
    :cond_4
    iget v6, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nInfoId:I

    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v0, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1212
    .restart local v3    # "outputText":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 1215
    .restart local v1    # "index":I
    if-gez v1, :cond_8

    .line 1216
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_tvInfo:Landroid/widget/TextView;

    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1194
    .end local v2    # "nameLength":I
    .end local v3    # "outputText":Ljava/lang/String;
    :cond_5
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_strCurPath:Ljava/lang/String;

    add-int/lit8 v7, v1, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1198
    .end local v1    # "index":I
    :cond_6
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_strCurPath:Ljava/lang/String;

    const-string/jumbo v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1199
    const-string/jumbo v0, "/"

    goto :goto_1

    .line 1201
    :cond_7
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_strCurPath:Ljava/lang/String;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_strCurPath:Ljava/lang/String;

    const-string/jumbo v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 1219
    .restart local v1    # "index":I
    .restart local v2    # "nameLength":I
    .restart local v3    # "outputText":Ljava/lang/String;
    :cond_8
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1220
    .restart local v5    # "span":Landroid/text/SpannableStringBuilder;
    invoke-virtual {v5, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1221
    new-instance v6, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-direct {v6, v7}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v7, v1

    add-int/lit8 v7, v7, 0x2

    invoke-virtual {v5, v6, v1, v7, v11}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1222
    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_tvInfo:Landroid/widget/TextView;

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method private setTitle()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1312
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 1313
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_tvTitle:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/filemanager/file/FileListItem;

    iget-object v0, v0, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1314
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_tvTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1315
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_layoutTextTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1316
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_layoutTextTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 1317
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_layoutTextTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 1329
    :goto_0
    return-void

    .line 1319
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_tvTitle:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/filemanager/file/FileListItem;

    iget-object v0, v0, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1320
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_tvTitle:Landroid/widget/TextView;

    const v1, 0x7f02003e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 1321
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_layoutTextTitle:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oTitleClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1322
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_layoutTextTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 1323
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_layoutTextTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setClickable(Z)V

    goto :goto_0
.end method

.method private showActionBarMovePopup()V
    .locals 18

    .prologue
    .line 1343
    const-string/jumbo v15, "layout_inflater"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 1345
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const/16 v15, 0x3c

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->setActionBarList(I)V

    .line 1347
    new-instance v1, Lcom/infraware/polarisoffice5/dialog/FileActionBarAdapter;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oActionList:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v15}, Lcom/infraware/polarisoffice5/dialog/FileActionBarAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 1349
    .local v1, "adapter":Lcom/infraware/polarisoffice5/dialog/FileActionBarAdapter;
    const v15, 0x7f030016

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v3, v15, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    .line 1351
    .local v11, "popupView":Landroid/view/View;
    const v15, 0x7f0b0077

    invoke-virtual {v11, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ListView;

    .line 1352
    .local v7, "listView":Landroid/widget/ListView;
    invoke-virtual {v7, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1353
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v7, v15}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1355
    new-instance v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1356
    .local v13, "tvWidthTest":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget v15, v0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nMoveWidthMax:I

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->setWidth(I)V

    .line 1358
    move-object/from16 v0, p0

    iget v15, v0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nStorageType:I

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_0

    .line 1359
    const/4 v15, 0x1

    const/high16 v16, 0x41900000    # 18.0f

    move/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1363
    :goto_0
    move-object/from16 v0, p0

    iget v9, v0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nMoveWidthMin:I

    .line 1364
    .local v9, "nPopupWidth":I
    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/dialog/FileActionBarAdapter;->getCount()I

    move-result v6

    .line 1365
    .local v6, "listCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v6, :cond_3

    .line 1367
    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/dialog/FileActionBarAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;

    .line 1369
    .local v4, "item":Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;
    iget-object v15, v4, Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;->m_strText:Ljava/lang/String;

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    new-array v14, v15, [F

    .line 1370
    .local v14, "widths":[F
    const/4 v10, 0x0

    .line 1371
    .local v10, "nWidth":I
    invoke-virtual {v13}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v15

    iget-object v0, v4, Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;->m_strText:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v14}, Landroid/text/TextPaint;->getTextWidths(Ljava/lang/String;[F)I

    move-result v8

    .line 1372
    .local v8, "nCount":I
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_2
    if-ge v5, v8, :cond_1

    .line 1373
    int-to-float v15, v10

    aget v16, v14, v5

    const/high16 v17, 0x3f800000    # 1.0f

    add-float v16, v16, v17

    add-float v15, v15, v16

    float-to-int v10, v15

    .line 1372
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1361
    .end local v2    # "i":I
    .end local v4    # "item":Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;
    .end local v5    # "j":I
    .end local v6    # "listCount":I
    .end local v8    # "nCount":I
    .end local v9    # "nPopupWidth":I
    .end local v10    # "nWidth":I
    .end local v14    # "widths":[F
    :cond_0
    const/4 v15, 0x1

    const/high16 v16, 0x41700000    # 15.0f

    move/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0

    .line 1375
    .restart local v2    # "i":I
    .restart local v4    # "item":Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;
    .restart local v5    # "j":I
    .restart local v6    # "listCount":I
    .restart local v8    # "nCount":I
    .restart local v9    # "nPopupWidth":I
    .restart local v10    # "nWidth":I
    .restart local v14    # "widths":[F
    :cond_1
    const/high16 v15, 0x42540000    # 53.0f

    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v15

    add-int/2addr v15, v10

    if-le v15, v9, :cond_2

    .line 1376
    const/high16 v15, 0x42540000    # 53.0f

    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v15

    add-int v9, v10, v15

    .line 1365
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1379
    .end local v4    # "item":Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;
    .end local v5    # "j":I
    .end local v8    # "nCount":I
    .end local v10    # "nWidth":I
    .end local v14    # "widths":[F
    :cond_3
    move-object/from16 v0, p0

    iget v15, v0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nMoveWidthMax:I

    if-le v9, v15, :cond_4

    .line 1380
    move-object/from16 v0, p0

    iget v9, v0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nMoveWidthMax:I

    .line 1393
    :cond_4
    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v7, v15, v0}, Landroid/widget/ListView;->measure(II)V

    .line 1394
    const/4 v12, 0x0

    .line 1395
    .local v12, "popupWindowHeight":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oActionList:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v15

    if-eqz v15, :cond_5

    .line 1396
    invoke-virtual {v7}, Landroid/widget/ListView;->getMeasuredHeight()I

    move-result v15

    invoke-virtual {v7}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v16

    add-int v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oActionList:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v16

    mul-int v15, v15, v16

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v16

    const/high16 v17, 0x40000000    # 2.0f

    invoke-static/range {v16 .. v17}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v16

    add-int v12, v15, v16

    .line 1400
    :cond_5
    new-instance v15, Landroid/widget/PopupWindow;

    invoke-direct {v15, v11, v9, v12}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oMenuPopup:Landroid/widget/PopupWindow;

    .line 1401
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oMenuPopup:Landroid/widget/PopupWindow;

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0201a6

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1402
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oMenuPopup:Landroid/widget/PopupWindow;

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 1403
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oMenuPopup:Landroid/widget/PopupWindow;

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    .line 1404
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oMenuPopup:Landroid/widget/PopupWindow;

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 1405
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oMenuPopup:Landroid/widget/PopupWindow;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 1406
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oMenuPopup:Landroid/widget/PopupWindow;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_layoutTextTitle:Landroid/widget/LinearLayout;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;)V

    .line 1407
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oMenuPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v15}, Landroid/widget/PopupWindow;->update()V

    .line 1408
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_layoutTextTitle:Landroid/widget/LinearLayout;

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 1409
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_tvTitle:Landroid/widget/TextView;

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1410
    return-void
.end method


# virtual methods
.method public getSdcardIntentFilter()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 1252
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 1253
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1254
    const-string/jumbo v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 1256
    return-object v0
.end method

.method public isSdCardAction(Ljava/lang/String;)V
    .locals 7
    .param p1, "nowAction"    # Ljava/lang/String;

    .prologue
    .line 1261
    iget v3, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nStorageType:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    .line 1309
    :cond_0
    :goto_0
    return-void

    .line 1265
    :cond_1
    const/4 v2, 0x0

    .line 1267
    .local v2, "isUnmount":Z
    const-string/jumbo v3, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1268
    iget-object v3, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_strOriginFile:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_strOriginFile:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    .line 1269
    new-instance v1, Ljava/io/File;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_strOriginFile:Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1270
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1271
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->finish()V

    goto :goto_0

    .line 1276
    .end local v1    # "file":Ljava/io/File;
    :cond_2
    const/4 v2, 0x1

    .line 1277
    new-instance v0, Ljava/lang/String;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_strCurPath:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 1278
    .local v0, "currentPath":Ljava/lang/String;
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$1;

    invoke-direct {v4, p0, v0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$1;-><init>(Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;Ljava/lang/String;)V

    const-wide/16 v5, 0x5dc

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 1293
    .end local v0    # "currentPath":Ljava/lang/String;
    :cond_3
    const-string/jumbo v3, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1294
    if-eqz v2, :cond_4

    .line 1295
    const/4 v2, 0x0

    .line 1296
    goto :goto_0

    .line 1298
    :cond_4
    iget v3, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nInternalMode:I

    const/16 v4, 0xe

    if-ne v3, v4, :cond_5

    .line 1299
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->setSelectMode(I)V

    .line 1302
    :cond_5
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->onUpdateList()V

    .line 1303
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->makeSDCardList()V

    .line 1304
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->setTitle()V

    .line 1306
    iget-object v3, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oMenuPopup:Landroid/widget/PopupWindow;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oMenuPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1307
    iget-object v3, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oMenuPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->dismiss()V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 659
    packed-switch p1, :pswitch_data_0

    .line 671
    :cond_0
    :goto_0
    return-void

    .line 662
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 663
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->onUpdateList()V

    goto :goto_0

    .line 659
    nop

    :pswitch_data_0
    .packed-switch 0x1028
        :pswitch_0
    .end packed-switch
.end method

.method public onButtonClick(Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Landroid/view/View;

    .prologue
    .line 675
    invoke-static {}, Lcom/infraware/common/util/Utils;->isButtonEnable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 736
    :goto_0
    return-void

    .line 678
    :cond_0
    invoke-static {}, Lcom/infraware/common/util/Utils;->setButtonDisable()V

    .line 680
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 681
    .local v0, "result":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 733
    :pswitch_0
    invoke-super {p0, p1}, Lcom/infraware/filemanager/file/FileBaseActivity;->onButtonClick(Landroid/view/View;)V

    goto :goto_0

    .line 711
    :pswitch_1
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->onNewFolder()V

    goto :goto_0

    .line 728
    :pswitch_2
    const-string/jumbo v1, "key_new_folder"

    iget-object v2, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_strCurPath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 729
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->setResult(ILandroid/content/Intent;)V

    .line 730
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->finish()V

    goto :goto_0

    .line 681
    :pswitch_data_0
    .packed-switch 0x7f0b0037
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onConfirmCreateFolder(Ljava/lang/String;)V
    .locals 13
    .param p1, "newName"    # Ljava/lang/String;

    .prologue
    const/16 v12, -0x1d

    const/4 v11, -0x1

    const/4 v10, 0x0

    .line 1013
    iget-object v8, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_strCurPath:Ljava/lang/String;

    const-string/jumbo v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_1

    iget-object v6, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_strCurPath:Ljava/lang/String;

    .line 1014
    .local v6, "path":Ljava/lang/String;
    :goto_0
    move-object v3, p1

    .line 1016
    .local v3, "name":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_2

    .line 1057
    :cond_0
    :goto_1
    return-void

    .line 1013
    .end local v3    # "name":Ljava/lang/String;
    .end local v6    # "path":Ljava/lang/String;
    :cond_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_strCurPath:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 1019
    .restart local v3    # "name":Ljava/lang/String;
    .restart local v6    # "path":Ljava/lang/String;
    :cond_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1020
    .local v5, "newFolderName":Ljava/lang/String;
    const/4 v2, 0x1

    .line 1022
    .local v2, "m_bLocal":Z
    iget v8, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nServiceType:I

    if-eq v8, v11, :cond_3

    .line 1023
    const/4 v2, 0x0

    .line 1025
    :cond_3
    invoke-static {v5}, Lcom/infraware/common/util/FileUtils;->isExist(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1027
    const/4 v8, -0x3

    invoke-virtual {p0, v8, v10}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->onErrorMessage(IZ)V

    goto :goto_1

    .line 1031
    :cond_4
    iget v8, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nServiceType:I

    if-ne v8, v11, :cond_0

    .line 1033
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1034
    .local v4, "newFolder":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->mkdir()Z

    move-result v8

    if-nez v8, :cond_6

    .line 1036
    const/4 v1, 0x0

    .line 1038
    .local v1, "freeBlock":I
    :try_start_0
    new-instance v7, Landroid/os/StatFs;

    invoke-virtual {v4}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1039
    .local v7, "sf":Landroid/os/StatFs;
    invoke-virtual {v7}, Landroid/os/StatFs;->getAvailableBlocks()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1043
    .end local v7    # "sf":Landroid/os/StatFs;
    :goto_2
    if-nez v1, :cond_5

    .line 1044
    invoke-virtual {p0, v12, v10}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->onErrorMessage(IZ)V

    goto :goto_1

    .line 1040
    :catch_0
    move-exception v0

    .line 1041
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {p0, v12, v10}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->onErrorMessage(IZ)V

    goto :goto_2

    .line 1046
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_5
    invoke-virtual {p0, v11, v10}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->onErrorMessage(IZ)V

    goto :goto_1

    .line 1049
    .end local v1    # "freeBlock":I
    :cond_6
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->onUpdateList()V

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x1

    const/4 v13, 0x0

    .line 110
    invoke-super {p0, p1}, Lcom/infraware/filemanager/file/FileBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 112
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    invoke-static {p0}, Lcom/infraware/common/util/Utils;->fullScreenActivity(Landroid/app/Activity;)V

    .line 120
    :cond_0
    new-instance v0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$CloseActionReceiver;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$CloseActionReceiver;-><init>(Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$CloseActionReceiver;

    .line 121
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    .line 122
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    const-string/jumbo v2, "com.infraware.polarisviewer5.CLOSE"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$CloseActionReceiver;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v2}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 125
    new-instance v0, Lcom/infraware/common/event/SdCardEvent;

    invoke-direct {v0}, Lcom/infraware/common/event/SdCardEvent;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oReceiver:Lcom/infraware/common/event/SdCardEvent;

    .line 126
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oReceiver:Lcom/infraware/common/event/SdCardEvent;

    invoke-virtual {v0, p0}, Lcom/infraware/common/event/SdCardEvent;->setSdListener(Lcom/infraware/common/event/SdCardListener;)V

    .line 128
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oReceiver:Lcom/infraware/common/event/SdCardEvent;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->getSdcardIntentFilter()Landroid/content/IntentFilter;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 130
    const v0, 0x7f0b0033

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_layoutTextTitle:Landroid/widget/LinearLayout;

    .line 132
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    .line 133
    .local v9, "intent":Landroid/content/Intent;
    invoke-virtual {v9}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    .line 134
    .local v7, "extras":Landroid/os/Bundle;
    if-eqz v7, :cond_1

    .line 136
    const-string/jumbo v0, "key_interanl_mode"

    invoke-virtual {v7, v0, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nInternalMode:I

    .line 151
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nInternalMode:I

    if-eqz v0, :cond_2

    .line 152
    const-string/jumbo v0, "key_current_file"

    invoke-virtual {v9, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_strOriginFile:Ljava/lang/String;

    .line 159
    :cond_2
    const-string/jumbo v2, ""

    const-string/jumbo v3, ""

    const-string/jumbo v4, ""

    const/4 v5, -0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->setStorageType(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 184
    const-string/jumbo v0, "key_current_folder"

    invoke-virtual {v9, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 187
    .local v12, "strPath":Ljava/lang/String;
    const-string/jumbo v0, "key_storage_type"

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    .line 188
    .local v11, "nStorageType":I
    const/4 v10, 0x1

    .line 190
    .local v10, "isLocal":Z
    const/4 v0, 0x2

    if-ne v11, v0, :cond_3

    .line 191
    const/4 v10, 0x0

    .line 193
    :cond_3
    if-eqz v12, :cond_4

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_5

    .line 195
    :cond_4
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v6

    .line 196
    .local v6, "config":Lcom/infraware/common/config/RuntimeConfig;
    const/16 v0, 0x5e

    const/4 v2, 0x0

    invoke-virtual {v6, p0, v0, v2}, Lcom/infraware/common/config/RuntimeConfig;->getStringPreference(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 197
    if-nez v12, :cond_5

    .line 198
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->getRootPath(I)Ljava/lang/String;

    move-result-object v12

    .line 201
    .end local v6    # "config":Lcom/infraware/common/config/RuntimeConfig;
    :cond_5
    invoke-virtual {p0, v12, v10}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->setCurPath(Ljava/lang/String;Z)V

    .line 203
    iget v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nInternalMode:I

    packed-switch v0, :pswitch_data_0

    .line 223
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->finish()V

    .line 380
    :goto_0
    return-void

    .line 210
    :pswitch_0
    iput v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nFilterType:I

    .line 227
    iget v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nInternalMode:I

    packed-switch v0, :pswitch_data_1

    .line 258
    :goto_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_tvTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 262
    if-eqz v12, :cond_a

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_a

    const-string/jumbo v0, "/"

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 264
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v8, v0, :cond_6

    .line 266
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/filemanager/file/FileListItem;

    iget-object v0, v0, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    invoke-virtual {v12, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 268
    iget-object v2, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_tvTitle:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/filemanager/file/FileListItem;

    iget-object v0, v0, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 282
    .end local v8    # "i":I
    :cond_6
    :goto_3
    iget v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nInternalMode:I

    packed-switch v0, :pswitch_data_2

    .line 353
    :cond_7
    :goto_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_layoutTitleBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v13}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 369
    iget v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nInfoId:I

    if-lez v0, :cond_8

    .line 371
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_layoutInfo:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v13}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 372
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->setInfoText()V

    .line 375
    :cond_8
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/infraware/common/util/Utils;->getCurrentLocaleType(Landroid/content/res/Resources;)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nLocaleCode:I

    .line 376
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->onOrientationChanged()V

    goto :goto_0

    .line 238
    :pswitch_1
    const v0, 0x7f070066

    iput v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nButtonId:I

    .line 239
    const v0, 0x7f0702a8

    iput v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nInfoId:I

    goto :goto_1

    .line 264
    .restart local v8    # "i":I
    :cond_9
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 274
    .end local v8    # "i":I
    :cond_a
    iget-object v2, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_tvTitle:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/filemanager/file/FileListItem;

    iget-object v0, v0, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 306
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v1, :cond_7

    .line 308
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_tvTitle:Landroid/widget/TextView;

    const v1, 0x7f02003e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 309
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_layoutTextTitle:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oTitleClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_4

    .line 203
    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_0
    .end packed-switch

    .line 227
    :pswitch_data_1
    .packed-switch 0xd
        :pswitch_1
    .end packed-switch

    .line 282
    :pswitch_data_2
    .packed-switch 0xd
        :pswitch_2
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oReceiver:Lcom/infraware/common/event/SdCardEvent;

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oReceiver:Lcom/infraware/common/event/SdCardEvent;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 388
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$CloseActionReceiver;

    if-eqz v0, :cond_1

    .line 389
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$CloseActionReceiver;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 390
    :cond_1
    invoke-super {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->onDestroy()V

    .line 391
    return-void
.end method

.method public onErrorMessage(IZ)V
    .locals 1
    .param p1, "error"    # I
    .param p2, "isWebError"    # Z

    .prologue
    .line 1060
    const/4 v0, 0x0

    .line 1065
    .local v0, "message":Ljava/lang/String;
    invoke-static {p0, p1}, Lcom/infraware/filemanager/porting/FileError;->getErrorMessage(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 1067
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->onToastMessage(Ljava/lang/String;)V

    .line 1068
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 410
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->isUpdatable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 422
    :goto_0
    :sswitch_0
    return v0

    .line 413
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 422
    invoke-super {p0, p1, p2}, Lcom/infraware/filemanager/file/FileBaseActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 416
    :sswitch_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->finish()V

    goto :goto_0

    .line 413
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x54 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 5
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 482
    iget-object v2, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oMapItemIdx:Ljava/util/HashMap;

    iget v3, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nCurDepth:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 483
    iget-object v2, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oMapItemTop:Ljava/util/HashMap;

    iget v3, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nCurDepth:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 485
    invoke-virtual {p0, p3}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->getFileItem(I)Lcom/infraware/filemanager/file/FileListItem;

    move-result-object v1

    .line 486
    .local v1, "item":Lcom/infraware/filemanager/file/FileListItem;
    if-nez v1, :cond_0

    .line 488
    new-instance v1, Lcom/infraware/filemanager/file/FileListItem;

    .end local v1    # "item":Lcom/infraware/filemanager/file/FileListItem;
    invoke-direct {v1}, Lcom/infraware/filemanager/file/FileListItem;-><init>()V

    .line 489
    .restart local v1    # "item":Lcom/infraware/filemanager/file/FileListItem;
    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/infraware/filemanager/file/FileListItem;->isFolder:Z

    .line 490
    iget v2, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nStorageType:I

    iput v2, v1, Lcom/infraware/filemanager/file/FileListItem;->type:I

    .line 491
    const-string/jumbo v2, ""

    iput-object v2, v1, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    .line 548
    :cond_0
    iget-boolean v2, v1, Lcom/infraware/filemanager/file/FileListItem;->isSDCard:Z

    if-eqz v2, :cond_1

    .line 550
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 552
    iget-object v3, v1, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/filemanager/file/FileListItem;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 554
    iget-object v3, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_tvTitle:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/filemanager/file/FileListItem;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 570
    .end local v0    # "i":I
    :cond_1
    invoke-super/range {p0 .. p5}, Lcom/infraware/filemanager/file/FileBaseActivity;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 571
    return-void

    .line 550
    .restart local v0    # "i":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onLocaleChanged(I)V
    .locals 1
    .param p1, "nLocale"    # I

    .prologue
    .line 741
    iput p1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nLocaleCode:I

    .line 749
    iget v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nInfoId:I

    if-lez v0, :cond_0

    .line 750
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->setInfoText()V

    .line 752
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oFileDialog:Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oFileDialog:Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 753
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oFileDialog:Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->onShow()V

    .line 754
    :cond_1
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 639
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 651
    invoke-super {p0, p1}, Lcom/infraware/filemanager/file/FileBaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 654
    :goto_0
    return v0

    .line 642
    :pswitch_0
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->onNewFolder()V

    .line 654
    const/4 v0, 0x1

    goto :goto_0

    .line 639
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onOrientationChanged()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/high16 v7, 0x43290000    # 169.0f

    const/4 v6, 0x1

    .line 759
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    iput v4, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nOrientation:I

    .line 760
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v4

    if-nez v4, :cond_0

    .line 762
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    iget v5, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nOrientation:I

    invoke-static {v4, v5}, Lcom/infraware/common/util/Utils;->setScreenMode(Landroid/view/Window;I)V

    .line 765
    :cond_0
    iget v4, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nStorageType:I

    if-ne v4, v6, :cond_1

    .line 766
    iget-object v4, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_tvTitle:Landroid/widget/TextView;

    const/high16 v5, 0x41900000    # 18.0f

    invoke-virtual {v4, v6, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 770
    :goto_0
    iget-object v4, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_tvTitle:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 771
    .local v1, "layoutParamTitleText":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v4, 0x41500000    # 13.0f

    invoke-static {p0, v4}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v4

    float-to-int v4, v4

    iput v4, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 772
    const/high16 v4, 0x40a00000    # 5.0f

    invoke-static {p0, v4}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v4

    float-to-int v4, v4

    iput v4, v1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 773
    iget-object v4, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_tvTitle:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 775
    iget-object v4, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_layoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 776
    .local v0, "layoutParam":Landroid/widget/LinearLayout$LayoutParams;
    iget v4, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nOrientation:I

    if-ne v4, v6, :cond_2

    .line 778
    const/high16 v4, 0x42400000    # 48.0f

    invoke-static {p0, v4}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v4

    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 779
    iget-object v4, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_layoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 781
    iget-object v4, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_btnTitle:Landroid/widget/Button;

    const v5, 0x418aa3d7    # 17.33f

    invoke-virtual {v4, v8, v5}, Landroid/widget/Button;->setTextSize(IF)V

    .line 783
    invoke-static {p0, v7}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v4

    iput v4, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nMoveWidthMin:I

    .line 784
    const/high16 v4, 0x43700000    # 240.0f

    invoke-static {p0, v4}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v4

    iput v4, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nMoveWidthMax:I

    .line 786
    const/16 v4, 0xa

    iput v4, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nMaxItem:I

    .line 811
    :goto_1
    return-void

    .line 768
    .end local v0    # "layoutParam":Landroid/widget/LinearLayout$LayoutParams;
    .end local v1    # "layoutParamTitleText":Landroid/widget/LinearLayout$LayoutParams;
    :cond_1
    iget-object v4, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_tvTitle:Landroid/widget/TextView;

    const/high16 v5, 0x41700000    # 15.0f

    invoke-virtual {v4, v6, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0

    .line 790
    .restart local v0    # "layoutParam":Landroid/widget/LinearLayout$LayoutParams;
    .restart local v1    # "layoutParamTitleText":Landroid/widget/LinearLayout$LayoutParams;
    :cond_2
    const/high16 v4, 0x42200000    # 40.0f

    invoke-static {p0, v4}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v4

    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 791
    iget-object v4, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_layoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 793
    iget-object v4, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_btnTitle:Landroid/widget/Button;

    const/high16 v5, 0x41800000    # 16.0f

    invoke-virtual {v4, v8, v5}, Landroid/widget/Button;->setTextSize(IF)V

    .line 795
    invoke-static {p0, v7}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v4

    iput v4, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nMoveWidthMin:I

    .line 796
    const/high16 v4, 0x43b50000    # 362.0f

    invoke-static {p0, v4}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v4

    iput v4, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nMoveWidthMax:I

    .line 798
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 799
    .local v2, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 800
    iget v3, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 801
    .local v3, "windowY":I
    iget v4, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    if-ge v4, v3, :cond_3

    .line 802
    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 804
    :cond_3
    const/16 v4, 0x1f4

    if-le v3, v4, :cond_4

    .line 805
    const/4 v4, 0x7

    iput v4, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nMaxItem:I

    goto :goto_1

    .line 807
    :cond_4
    const/4 v4, 0x6

    iput v4, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nMaxItem:I

    goto :goto_1
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 400
    invoke-super {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->onPause()V

    .line 401
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 605
    iget-object v2, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_lvFileList:Landroid/widget/ListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 606
    .local v1, "itemView":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 608
    const/high16 v2, 0x7f080000

    invoke-virtual {v1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/filemanager/file/FileListItem;

    move-object v0, v2

    check-cast v0, Lcom/infraware/filemanager/file/FileListItem;

    .line 610
    .local v0, "item":Lcom/infraware/filemanager/file/FileListItem;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oMapItemIdx:Ljava/util/HashMap;

    iget v3, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nCurDepth:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v4, v0}, Lcom/infraware/filemanager/file/FileListAdapter;->getPosition(Lcom/infraware/filemanager/file/FileListItem;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 611
    iget-object v2, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oMapItemTop:Ljava/util/HashMap;

    iget v3, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nCurDepth:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 614
    .end local v0    # "item":Lcom/infraware/filemanager/file/FileListItem;
    :cond_0
    invoke-super {p0, p1}, Lcom/infraware/filemanager/file/FileBaseActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 616
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 633
    const/4 v2, 0x1

    return v2
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 394
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->onUpdateList()V

    .line 395
    invoke-super {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->onResume()V

    .line 396
    return-void
.end method

.method public setButtonState()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 834
    invoke-static {}, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->useExternalSD()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_strCurPath:Ljava/lang/String;

    iget v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nStorageType:I

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->getRootPath(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nServiceType:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 836
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_btnTitleMenuNewFolder:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 840
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_btnTitleMenuSave:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 842
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_btnTitle:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 843
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_btnTitle:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 890
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->setButtonState()V

    .line 891
    return-void

    .line 847
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_nInternalMode:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 877
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_btnTitleMenuNewFolder:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 881
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_btnTitleMenuSave:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 847
    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_0
    .end packed-switch
.end method

.method public updateScreen()V
    .locals 0

    .prologue
    .line 897
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->setInfoText()V

    .line 898
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->setButtonState()V

    .line 900
    invoke-super {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->updateScreen()V

    .line 901
    return-void
.end method
