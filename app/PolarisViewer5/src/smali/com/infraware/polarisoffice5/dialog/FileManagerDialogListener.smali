.class public Lcom/infraware/polarisoffice5/dialog/FileManagerDialogListener;
.super Ljava/lang/Object;
.source "FileManagerDialogListener.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private inputName:Ljava/lang/String;

.field private m_oParent:Landroid/app/Activity;

.field private type:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;I)V
    .locals 1
    .param p1, "parent"    # Landroid/app/Activity;
    .param p2, "type"    # I

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialogListener;->type:I

    .line 17
    iput-object p1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialogListener;->m_oParent:Landroid/app/Activity;

    .line 18
    iput p2, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialogListener;->type:I

    .line 19
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "v"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 27
    invoke-static {}, Lcom/infraware/common/util/Utils;->setButtonDisable()V

    .line 29
    iget v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialogListener;->type:I

    packed-switch v1, :pswitch_data_0

    .line 107
    :cond_0
    :goto_0
    return-void

    .line 92
    :pswitch_0
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    .line 99
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialogListener;->m_oParent:Landroid/app/Activity;

    instance-of v1, v1, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;

    if-eqz v1, :cond_0

    .line 101
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialogListener;->m_oParent:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;

    .line 102
    .local v0, "parent":Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialogListener;->inputName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->onConfirmCreateFolder(Ljava/lang/String;)V

    goto :goto_0

    .line 29
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
    .end packed-switch
.end method

.method public setInputName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialogListener;->inputName:Ljava/lang/String;

    .line 24
    return-void
.end method
