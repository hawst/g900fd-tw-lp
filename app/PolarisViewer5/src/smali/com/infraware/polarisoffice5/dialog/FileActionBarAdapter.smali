.class public Lcom/infraware/polarisoffice5/dialog/FileActionBarAdapter;
.super Landroid/widget/BaseAdapter;
.source "FileActionBarAdapter.java"


# instance fields
.field private context:Landroid/content/Context;

.field private isNavigation:Z

.field private m_oActionBarList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;",
            ">;"
        }
    .end annotation
.end field

.field private m_oInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileActionBarAdapter;->m_oActionBarList:Ljava/util/ArrayList;

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/dialog/FileActionBarAdapter;->isNavigation:Z

    .line 30
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileActionBarAdapter;->m_oInflater:Landroid/view/LayoutInflater;

    .line 31
    iput-object p2, p0, Lcom/infraware/polarisoffice5/dialog/FileActionBarAdapter;->m_oActionBarList:Ljava/util/ArrayList;

    .line 32
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileActionBarAdapter;->m_oActionBarList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileActionBarAdapter;->m_oActionBarList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45
    :cond_0
    const/4 v0, 0x0

    .line 47
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileActionBarAdapter;->m_oActionBarList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 52
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileActionBarAdapter;->m_oActionBarList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileActionBarAdapter;->m_oActionBarList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileActionBarAdapter;->m_oActionBarList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 53
    :cond_0
    const/4 v0, 0x0

    .line 55
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/dialog/FileActionBarAdapter;->m_oActionBarList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 60
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v9, 0x7f0b0074

    const/4 v8, 0x0

    .line 66
    if-nez p2, :cond_0

    .line 67
    iget-object v5, p0, Lcom/infraware/polarisoffice5/dialog/FileActionBarAdapter;->m_oInflater:Landroid/view/LayoutInflater;

    const v6, 0x7f030015

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 69
    :cond_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/dialog/FileActionBarAdapter;->m_oActionBarList:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;

    .line 75
    .local v1, "item":Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;
    iget v5, v1, Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;->m_nIconId:I

    if-nez v5, :cond_2

    iget v5, v1, Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;->m_nServiceType:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_2

    .line 77
    const v5, 0x7f0b0073

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 79
    .local v3, "layout":Landroid/widget/LinearLayout;
    const/4 v2, 0x0

    .line 80
    .local v2, "ivIcon":Landroid/widget/ImageView;
    invoke-virtual {v3, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 117
    .local v4, "tvText":Landroid/widget/TextView;
    :cond_1
    :goto_0
    iget-object v5, v1, Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;->m_strText:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    const v5, 0x7f0b0072

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 121
    .local v0, "ibEnable":Landroid/widget/ImageButton;
    iget-boolean v5, v1, Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;->m_bEnable:Z

    if-eqz v5, :cond_3

    .line 122
    const/16 v5, 0x8

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 129
    :goto_1
    invoke-virtual {v3, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 130
    iget-boolean v5, v1, Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;->m_bEnable:Z

    invoke-virtual {p2, v5}, Landroid/view/View;->setEnabled(Z)V

    .line 131
    return-object p2

    .line 84
    .end local v0    # "ibEnable":Landroid/widget/ImageButton;
    .end local v2    # "ivIcon":Landroid/widget/ImageView;
    .end local v3    # "layout":Landroid/widget/LinearLayout;
    .end local v4    # "tvText":Landroid/widget/TextView;
    :cond_2
    const v5, 0x7f0b0075

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 86
    .restart local v3    # "layout":Landroid/widget/LinearLayout;
    const v5, 0x7f0b0076

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 87
    .restart local v2    # "ivIcon":Landroid/widget/ImageView;
    invoke-virtual {v3, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 89
    .restart local v4    # "tvText":Landroid/widget/TextView;
    iget v5, v1, Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;->m_nIconId:I

    if-eqz v5, :cond_1

    .line 90
    iget v5, v1, Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;->m_nIconId:I

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 125
    .restart local v0    # "ibEnable":Landroid/widget/ImageButton;
    :cond_3
    invoke-virtual {v0}, Landroid/widget/ImageButton;->bringToFront()V

    .line 126
    invoke-virtual {v0, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_1
.end method
