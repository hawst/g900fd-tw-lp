.class Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$4;
.super Ljava/lang/Object;
.source "FileSelectActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;)V
    .locals 0

    .prologue
    .line 1502
    iput-object p1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$4;->this$0:Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1506
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$4;->this$0:Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;

    # invokes: Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->dismissPopupWindow()Z
    invoke-static {v1}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->access$500(Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;)Z

    .line 1507
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$4;->this$0:Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->m_oActionList:Ljava/util/ArrayList;

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;

    .line 1509
    .local v0, "actionBar":Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;
    iget v1, v0, Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;->m_nMenuId:I

    packed-switch v1, :pswitch_data_0

    .line 1516
    :goto_0
    return-void

    .line 1513
    :pswitch_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity$4;->this$0:Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;

    iget v2, v0, Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;->m_nMenuId:I

    iget-object v3, v0, Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;->m_strText:Ljava/lang/String;

    iget v4, v0, Lcom/infraware/polarisoffice5/dialog/FileActionBarItem;->m_nServiceType:I

    # invokes: Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->onMoveStorage(ILjava/lang/String;I)V
    invoke-static {v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;->access$600(Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;ILjava/lang/String;I)V

    goto :goto_0

    .line 1509
    nop

    :pswitch_data_0
    .packed-switch 0x3d
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
