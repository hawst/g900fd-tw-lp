.class Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;
.super Ljava/lang/Object;
.source "FileManagerDialog.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EditWatcher"
.end annotation


# instance fields
.field m_nErrorMessageId:I

.field final synthetic this$0:Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;)V
    .locals 1

    .prologue
    .line 398
    iput-object p1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->this$0:Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 397
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->m_nErrorMessageId:I

    .line 399
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 7
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/16 v3, 0x8

    const/4 v6, 0x1

    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 403
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 404
    .local v0, "name":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 405
    iput v4, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->m_nErrorMessageId:I

    .line 406
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->this$0:Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    # getter for: Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oTextView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->access$000(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 407
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->this$0:Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    # getter for: Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->access$100(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 457
    :goto_0
    return-void

    .line 411
    :cond_0
    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x2e

    if-ne v1, v2, :cond_1

    .line 413
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->this$0:Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    # getter for: Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nDialogType:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->access$200(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 424
    :goto_1
    :pswitch_0
    iget v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->m_nErrorMessageId:I

    if-lez v1, :cond_1

    .line 425
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->this$0:Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    # getter for: Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oTextView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->access$000(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;)Landroid/widget/TextView;

    move-result-object v1

    iget v2, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->m_nErrorMessageId:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 426
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->this$0:Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    # getter for: Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oTextView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->access$000(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 427
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->this$0:Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    # getter for: Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->access$100(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 418
    :pswitch_1
    const v1, 0x7f07025e

    iput v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->m_nErrorMessageId:I

    goto :goto_1

    .line 421
    :pswitch_2
    iput v4, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->m_nErrorMessageId:I

    goto :goto_1

    .line 432
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->this$0:Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    # invokes: Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->isExist(Ljava/lang/String;)Z
    invoke-static {v1, v0}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->access$300(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 434
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->this$0:Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    # getter for: Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_nDialogType:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->access$200(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;)I

    move-result v1

    packed-switch v1, :pswitch_data_1

    .line 447
    :goto_2
    :pswitch_3
    iget v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->m_nErrorMessageId:I

    if-lez v1, :cond_2

    .line 448
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->this$0:Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    # getter for: Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oParent:Landroid/app/Activity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->access$400(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;)Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->this$0:Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    # getter for: Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oParent:Landroid/app/Activity;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->access$400(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;)Landroid/app/Activity;

    move-result-object v2

    iget v3, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->m_nErrorMessageId:I

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 449
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->this$0:Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    # getter for: Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->listener:Lcom/infraware/polarisoffice5/dialog/FileManagerDialogListener;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->access$500(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;)Lcom/infraware/polarisoffice5/dialog/FileManagerDialogListener;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialogListener;->setInputName(Ljava/lang/String;)V

    .line 450
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->this$0:Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    # getter for: Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->access$100(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_0

    .line 438
    :pswitch_4
    const v1, 0x7f0702a7

    iput v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->m_nErrorMessageId:I

    goto :goto_2

    .line 441
    :pswitch_5
    const v1, 0x7f0702a6

    iput v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->m_nErrorMessageId:I

    goto :goto_2

    .line 444
    :pswitch_6
    iput v4, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->m_nErrorMessageId:I

    goto :goto_2

    .line 454
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->this$0:Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    # getter for: Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->listener:Lcom/infraware/polarisoffice5/dialog/FileManagerDialogListener;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->access$500(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;)Lcom/infraware/polarisoffice5/dialog/FileManagerDialogListener;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialogListener;->setInputName(Ljava/lang/String;)V

    .line 455
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->this$0:Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    # getter for: Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oTextView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->access$000(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 456
    iget-object v1, p0, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog$EditWatcher;->this$0:Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;

    # getter for: Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->m_oDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;->access$100(Lcom/infraware/polarisoffice5/dialog/FileManagerDialog;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_0

    .line 413
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 434
    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_6
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 461
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 466
    return-void
.end method
