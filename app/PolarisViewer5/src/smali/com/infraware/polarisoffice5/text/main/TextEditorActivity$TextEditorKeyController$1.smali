.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$1;
.super Ljava/lang/Object;
.source "TextEditorActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;)V
    .locals 0

    .prologue
    .line 5264
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private onKeyDown(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "a_view"    # Landroid/view/View;
    .param p2, "a_keyCode"    # I
    .param p3, "a_keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v6, 0xe

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 5279
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v4

    and-int/lit16 v4, v4, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_2

    move v0, v2

    .line 5281
    .local v0, "isCtrl":Z
    :goto_0
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    # invokes: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->setKeyEventOwner(Landroid/view/View;)V
    invoke-static {v4, p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->access$8600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;Landroid/view/View;)V

    .line 5283
    const/4 v1, 0x0

    .line 5284
    .local v1, "isSuccess":Z
    sparse-switch p2, :sswitch_data_0

    :cond_0
    :goto_1
    move v2, v1

    .line 5339
    :cond_1
    :goto_2
    return v2

    .end local v0    # "isCtrl":Z
    .end local v1    # "isSuccess":Z
    :cond_2
    move v0, v3

    .line 5279
    goto :goto_0

    .line 5287
    .restart local v0    # "isCtrl":Z
    .restart local v1    # "isSuccess":Z
    :sswitch_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mKeyEventable:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->access$8700(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;->onKeyLeft()Z

    move-result v1

    .line 5288
    goto :goto_1

    .line 5291
    :sswitch_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mKeyEventable:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->access$8700(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;->onKeyRight()Z

    move-result v1

    .line 5292
    goto :goto_1

    .line 5300
    :sswitch_2
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    iget-object v4, v4, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nShowMode:I
    invoke-static {v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$8800(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    .line 5301
    if-eqz v0, :cond_1

    .line 5302
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    invoke-virtual {v4, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->setActionBarFocus(Z)V

    .line 5303
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->requestFocus()Z

    goto :goto_2

    .line 5311
    :cond_3
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->isActionBarFocused()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 5312
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    invoke-virtual {v4, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->setActionBarFocus(Z)V

    goto :goto_2

    .line 5316
    :cond_4
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mKeyEventable:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->access$8700(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;->onKeyDown()Z

    move-result v1

    .line 5317
    goto :goto_1

    .line 5320
    :sswitch_3
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mKeyEventable:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->access$8700(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;->onKeyUp()Z

    move-result v1

    .line 5321
    goto :goto_1

    .line 5325
    :sswitch_4
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mKeyEventable:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->access$8700(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;

    move-result-object v2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    invoke-virtual {v2, p2, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;->onKeyCode(II)Z

    move-result v1

    .line 5326
    goto :goto_1

    .line 5330
    :sswitch_5
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v2, v6, :cond_0

    .line 5331
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->setActionBarFocus(Z)V

    goto :goto_1

    .line 5335
    :sswitch_6
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v2, v6, :cond_0

    .line 5336
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->setActionBarFocus(Z)V

    goto :goto_1

    .line 5284
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_6
        0x13 -> :sswitch_3
        0x14 -> :sswitch_2
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0x17 -> :sswitch_4
        0x39 -> :sswitch_5
        0x3a -> :sswitch_5
        0x42 -> :sswitch_4
    .end sparse-switch
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "a_view"    # Landroid/view/View;
    .param p2, "a_keyCode"    # I
    .param p3, "a_event"    # Landroid/view/KeyEvent;

    .prologue
    .line 5268
    const/4 v0, 0x0

    .line 5269
    .local v0, "result":Z
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    .line 5270
    invoke-direct {p0, p1, p2, p3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$1;->onKeyDown(Landroid/view/View;ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 5274
    :cond_0
    :goto_0
    return v0

    .line 5271
    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 5272
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    # invokes: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->onKeyUp(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    invoke-static {v1, p1, p2, p3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->access$8500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;Landroid/view/View;ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
