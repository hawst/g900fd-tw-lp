.class public Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;
.super Ljava/lang/Object;
.source "EditOptionMenu.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Flag;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$StringReference;


# instance fields
.field private isClicked:Z

.field private mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

.field private mPopup:Landroid/widget/PopupWindow;

.field mTouchListener:Landroid/view/View$OnTouchListener;

.field private m_adapter:Lcom/infraware/polarisoffice5/common/MultiAdapter;

.field private m_bCheckMatchCase:Z

.field private m_bCheckWholeWord:Z

.field private m_bNeedMove:Z

.field private m_flag:I

.field private m_listView:Landroid/widget/ListView;

.field private popupitem:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/common/MultiListItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;I)V
    .locals 2
    .param p1, "mParent"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    .param p2, "type"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mPopup:Landroid/widget/PopupWindow;

    .line 44
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .line 45
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->isClicked:Z

    .line 47
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_listView:Landroid/widget/ListView;

    .line 48
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_adapter:Lcom/infraware/polarisoffice5/common/MultiAdapter;

    .line 50
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_bNeedMove:Z

    .line 52
    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_flag:I

    .line 54
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_bCheckMatchCase:Z

    .line 55
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_bCheckWholeWord:Z

    .line 83
    new-instance v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$1;-><init>(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 58
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .line 59
    iput p2, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_flag:I

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_listView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->getEnabledTopPosition()I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->getEnabledBottomPosition()I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->isClicked:Z

    return v0
.end method

.method static synthetic access$302(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->isClicked:Z

    return p1
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)Lcom/infraware/polarisoffice5/common/MultiAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_adapter:Lcom/infraware/polarisoffice5/common/MultiAdapter;

    return-object v0
.end method

.method private addItem(I)V
    .locals 23
    .param p1, "itemCase"    # I

    .prologue
    .line 321
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    if-eqz v5, :cond_0

    .line 322
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    .line 324
    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    .line 327
    new-instance v17, Lcom/infraware/polarisoffice5/common/MultiListItem;

    const/4 v5, 0x2

    const v12, 0x7f020146

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v13}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0700df

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/16 v14, 0x10

    move-object/from16 v0, v17

    invoke-direct {v0, v5, v12, v13, v14}, Lcom/infraware/polarisoffice5/common/MultiListItem;-><init>(IILjava/lang/String;I)V

    .line 329
    .local v17, "bookmarkItem":Lcom/infraware/polarisoffice5/common/MultiListItem;
    new-instance v1, Lcom/infraware/polarisoffice5/common/MultiListItem;

    const/4 v2, 0x2

    const v3, 0x7f020164

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v12, 0x7f07023f

    invoke-virtual {v5, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    const/4 v12, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->checkEnable(I)Z

    move-result v6

    invoke-direct/range {v1 .. v6}, Lcom/infraware/polarisoffice5/common/MultiListItem;-><init>(IILjava/lang/String;IZ)V

    .line 332
    .local v1, "ttsItem":Lcom/infraware/polarisoffice5/common/MultiListItem;
    new-instance v22, Lcom/infraware/polarisoffice5/common/MultiListItem;

    const/4 v5, 0x2

    const v12, 0x7f0201ba

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v13}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f070309

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x4

    move-object/from16 v0, v22

    invoke-direct {v0, v5, v12, v13, v14}, Lcom/infraware/polarisoffice5/common/MultiListItem;-><init>(IILjava/lang/String;I)V

    .line 333
    .local v22, "sendfileItem":Lcom/infraware/polarisoffice5/common/MultiListItem;
    new-instance v21, Lcom/infraware/polarisoffice5/common/MultiListItem;

    const/4 v5, 0x2

    const v12, 0x7f0201cc

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v13}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0701ad

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x3

    move-object/from16 v0, v21

    invoke-direct {v0, v5, v12, v13, v14}, Lcom/infraware/polarisoffice5/common/MultiListItem;-><init>(IILjava/lang/String;I)V

    .line 336
    .local v21, "printItem":Lcom/infraware/polarisoffice5/common/MultiListItem;
    new-instance v18, Lcom/infraware/polarisoffice5/common/MultiListItem;

    const/4 v5, 0x2

    const v12, 0x7f0201b9

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v13}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f070126

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/16 v14, 0x12

    move-object/from16 v0, v18

    invoke-direct {v0, v5, v12, v13, v14}, Lcom/infraware/polarisoffice5/common/MultiListItem;-><init>(IILjava/lang/String;I)V

    .line 338
    .local v18, "editItem":Lcom/infraware/polarisoffice5/common/MultiListItem;
    new-instance v2, Lcom/infraware/polarisoffice5/common/MultiListItem;

    const/4 v3, 0x2

    const v4, 0x7f0201b5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v12, 0x7f0702f8

    invoke-virtual {v5, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x8

    const/16 v12, 0x8

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->checkEnable(I)Z

    move-result v7

    invoke-direct/range {v2 .. v7}, Lcom/infraware/polarisoffice5/common/MultiListItem;-><init>(IILjava/lang/String;IZ)V

    .line 339
    .local v2, "infoItem":Lcom/infraware/polarisoffice5/common/MultiListItem;
    new-instance v19, Lcom/infraware/polarisoffice5/common/MultiListItem;

    const/4 v5, 0x2

    const v12, 0x7f0201c8

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v13}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0702f7

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x7

    move-object/from16 v0, v19

    invoke-direct {v0, v5, v12, v13, v14}, Lcom/infraware/polarisoffice5/common/MultiListItem;-><init>(IILjava/lang/String;I)V

    .line 340
    .local v19, "helpItem":Lcom/infraware/polarisoffice5/common/MultiListItem;
    new-instance v20, Lcom/infraware/polarisoffice5/common/MultiListItem;

    const/4 v5, 0x2

    const v12, 0x7f020159

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v13}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0700bf

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/16 v14, 0x9

    move-object/from16 v0, v20

    invoke-direct {v0, v5, v12, v13, v14}, Lcom/infraware/polarisoffice5/common/MultiListItem;-><init>(IILjava/lang/String;I)V

    .line 343
    .local v20, "officesettingItem":Lcom/infraware/polarisoffice5/common/MultiListItem;
    new-instance v3, Lcom/infraware/polarisoffice5/common/MultiListItem;

    const/4 v4, 0x2

    const v5, 0x7f020151

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v12}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f070064

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x11

    const/16 v12, 0x11

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->checkEnable(I)Z

    move-result v8

    invoke-direct/range {v3 .. v8}, Lcom/infraware/polarisoffice5/common/MultiListItem;-><init>(IILjava/lang/String;IZ)V

    .line 346
    .local v3, "productinfoItem":Lcom/infraware/polarisoffice5/common/MultiListItem;
    const v6, 0x7f020016

    .line 349
    .local v6, "nCheckBoxDrawable":I
    new-instance v4, Lcom/infraware/polarisoffice5/common/MultiListItem;

    const/16 v5, 0x8

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v12}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0702f3

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0xa

    const/4 v9, 0x1

    invoke-direct/range {v4 .. v9}, Lcom/infraware/polarisoffice5/common/MultiListItem;-><init>(IILjava/lang/String;IZ)V

    .line 350
    .local v4, "findmatchcaseItem":Lcom/infraware/polarisoffice5/common/MultiListItem;
    new-instance v7, Lcom/infraware/polarisoffice5/common/MultiListItem;

    const/16 v8, 0x8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v12, 0x7f0702f4

    invoke-virtual {v5, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/16 v11, 0xc

    const/4 v12, 0x1

    move v9, v6

    invoke-direct/range {v7 .. v12}, Lcom/infraware/polarisoffice5/common/MultiListItem;-><init>(IILjava/lang/String;IZ)V

    .line 351
    .local v7, "findwholewordItem":Lcom/infraware/polarisoffice5/common/MultiListItem;
    new-instance v8, Lcom/infraware/polarisoffice5/common/MultiListItem;

    const/16 v9, 0x8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v12, 0x7f0702f5

    invoke-virtual {v5, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/16 v12, 0xe

    const/4 v13, 0x1

    move v10, v6

    invoke-direct/range {v8 .. v13}, Lcom/infraware/polarisoffice5/common/MultiListItem;-><init>(IILjava/lang/String;IZ)V

    .line 354
    .local v8, "findchangemodeItem":Lcom/infraware/polarisoffice5/common/MultiListItem;
    new-instance v9, Lcom/infraware/polarisoffice5/common/MultiListItem;

    const/16 v10, 0x8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v12, 0x7f0702f3

    invoke-virtual {v5, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/16 v13, 0xb

    const/4 v14, 0x1

    move v11, v6

    invoke-direct/range {v9 .. v14}, Lcom/infraware/polarisoffice5/common/MultiListItem;-><init>(IILjava/lang/String;IZ)V

    .line 355
    .local v9, "replacematchcaseItem":Lcom/infraware/polarisoffice5/common/MultiListItem;
    new-instance v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    const/16 v11, 0x8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v12, 0x7f0702f4

    invoke-virtual {v5, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/16 v14, 0xd

    const/4 v15, 0x1

    move v12, v6

    invoke-direct/range {v10 .. v15}, Lcom/infraware/polarisoffice5/common/MultiListItem;-><init>(IILjava/lang/String;IZ)V

    .line 356
    .local v10, "replacewholewordItem":Lcom/infraware/polarisoffice5/common/MultiListItem;
    new-instance v11, Lcom/infraware/polarisoffice5/common/MultiListItem;

    const/16 v12, 0x8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v13, 0x7f0702f2

    invoke-virtual {v5, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/16 v15, 0xf

    const/16 v16, 0x1

    move v13, v6

    invoke-direct/range {v11 .. v16}, Lcom/infraware/polarisoffice5/common/MultiListItem;-><init>(IILjava/lang/String;IZ)V

    .line 358
    .local v11, "replacechangemodeItem":Lcom/infraware/polarisoffice5/common/MultiListItem;
    packed-switch p1, :pswitch_data_0

    .line 458
    :cond_1
    :goto_0
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v12, "FT03"

    invoke-virtual {v5, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 459
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->isSamsungEmailOpen()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 460
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 462
    :cond_2
    return-void

    .line 360
    :pswitch_0
    move-object/from16 v0, p0

    iget v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_flag:I

    packed-switch v5, :pswitch_data_1

    goto :goto_0

    .line 363
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->isKnoxMode(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->isActiveTTS(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 364
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 365
    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 366
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 369
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 370
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-static {v5}, Lcom/infraware/office/baseframe/porting/EvPrintHelper;->isSupportPrint(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->isKnoxMode(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 371
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 374
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 375
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 376
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 379
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 380
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 381
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 386
    :pswitch_5
    move-object/from16 v0, p0

    iget v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_flag:I

    packed-switch v5, :pswitch_data_2

    goto/16 :goto_0

    .line 388
    :pswitch_6
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 389
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 392
    :cond_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->isKnoxMode(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->isActiveTTS(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 393
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 394
    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 395
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 396
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 398
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-static {v5}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_DOWNLOAD_FROM_SAUMSUNGAPPS(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 399
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 403
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-static {v5}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_DOWNLOAD_FROM_SAUMSUNGAPPS(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 405
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->isInstalledPolarisOffice(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->isSupportSamsungApps(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 406
    :cond_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 409
    :cond_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 410
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-static {v5}, Lcom/infraware/office/baseframe/porting/EvPrintHelper;->isSupportPrint(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->isKnoxMode(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 411
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 414
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 415
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 418
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 419
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 424
    :pswitch_a
    move-object/from16 v0, p0

    iget v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_flag:I

    packed-switch v5, :pswitch_data_3

    goto/16 :goto_0

    .line 427
    :pswitch_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->isKnoxMode(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->isActiveTTS(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 428
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 429
    :cond_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 430
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 432
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-static {v5}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_DOWNLOAD_FROM_SAUMSUNGAPPS(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 433
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 437
    :pswitch_c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-static {v5}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_DOWNLOAD_FROM_SAUMSUNGAPPS(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 439
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->isInstalledPolarisOffice(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_9

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->isSupportSamsungApps(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 440
    :cond_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 442
    :cond_a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 443
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-static {v5}, Lcom/infraware/office/baseframe/porting/EvPrintHelper;->isSupportPrint(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->isKnoxMode(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 444
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 447
    :pswitch_d
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 448
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 451
    :pswitch_e
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 452
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 358
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_5
        :pswitch_a
    .end packed-switch

    .line 360
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 386
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 424
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method private checkEnable(I)Z
    .locals 3
    .param p1, "event"    # I

    .prologue
    .line 465
    const/4 v0, 0x1

    .line 467
    .local v0, "menuEnable":Z
    sparse-switch p1, :sswitch_data_0

    .line 483
    :cond_0
    :goto_0
    return v0

    .line 469
    :sswitch_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isChanged()Z

    move-result v1

    if-nez v1, :cond_0

    .line 470
    const/4 v0, 0x0

    goto :goto_0

    .line 474
    :sswitch_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 475
    const/4 v0, 0x0

    goto :goto_0

    .line 478
    :sswitch_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isNewFile()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 479
    const/4 v0, 0x0

    goto :goto_0

    .line 467
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_1
        0x8 -> :sswitch_2
    .end sparse-switch
.end method

.method private getEnabledBottomPosition()I
    .locals 3

    .prologue
    .line 310
    const/4 v1, -0x1

    .line 311
    .local v1, "result":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_adapter:Lcom/infraware/polarisoffice5/common/MultiAdapter;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/MultiAdapter;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 312
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_adapter:Lcom/infraware/polarisoffice5/common/MultiAdapter;

    invoke-virtual {v2, v0}, Lcom/infraware/polarisoffice5/common/MultiAdapter;->isEnabled(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 313
    move v1, v0

    .line 311
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 316
    :cond_1
    return v1
.end method

.method private getEnabledTopPosition()I
    .locals 2

    .prologue
    .line 301
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_adapter:Lcom/infraware/polarisoffice5/common/MultiAdapter;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/MultiAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 302
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_adapter:Lcom/infraware/polarisoffice5/common/MultiAdapter;

    invoke-virtual {v1, v0}, Lcom/infraware/polarisoffice5/common/MultiAdapter;->isEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 306
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 301
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 306
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method


# virtual methods
.method public dismissPopupWindow()Z
    .locals 2

    .prologue
    .line 71
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getAlertDialog()Landroid/app/AlertDialog;

    move-result-object v0

    .line 72
    .local v0, "ad":Landroid/app/AlertDialog;
    if-eqz v0, :cond_0

    .line 73
    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 74
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mPopup:Landroid/widget/PopupWindow;

    if-eqz v1, :cond_1

    .line 75
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    .line 76
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mPopup:Landroid/widget/PopupWindow;

    .line 77
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 78
    const/4 v1, 0x1

    .line 80
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isCheckMatchCase()Z
    .locals 1

    .prologue
    .line 487
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_bCheckMatchCase:Z

    return v0
.end method

.method public isCheckWholeWord()Z
    .locals 1

    .prologue
    .line 510
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_bCheckWholeWord:Z

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mPopup:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    const/4 v0, 0x1

    .line 68
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public optionPopupwindow()V
    .locals 28

    .prologue
    .line 92
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->dismissPopupWindow()Z

    .line 95
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-object/from16 v23, v0

    const-string/jumbo v24, "layout_inflater"

    invoke-virtual/range {v23 .. v24}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/LayoutInflater;

    .line 97
    .local v10, "inflater":Landroid/view/LayoutInflater;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getViewMode()I

    move-result v23

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->addItem(I)V

    .line 98
    new-instance v23, Lcom/infraware/polarisoffice5/common/MultiAdapter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-direct/range {v23 .. v25}, Lcom/infraware/polarisoffice5/common/MultiAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_adapter:Lcom/infraware/polarisoffice5/common/MultiAdapter;

    .line 100
    const v23, 0x7f030050

    const/16 v24, 0x0

    move/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v10, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v13

    .line 101
    .local v13, "popupView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mTouchListener:Landroid/view/View$OnTouchListener;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v13, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 103
    const v23, 0x7f0b0222

    move/from16 v0, v23

    invoke-virtual {v13, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/ListView;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_listView:Landroid/widget/ListView;

    .line 104
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_listView:Landroid/widget/ListView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_adapter:Lcom/infraware/polarisoffice5/common/MultiAdapter;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 105
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f020039

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v18

    .line 106
    .local v18, "selector":Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_listView:Landroid/widget/ListView;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 108
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_listView:Landroid/widget/ListView;

    move-object/from16 v23, v0

    new-instance v24, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$2;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$2;-><init>(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)V

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 139
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_listView:Landroid/widget/ListView;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/16 v25, 0x0

    invoke-virtual/range {v23 .. v25}, Landroid/widget/ListView;->measure(II)V

    .line 140
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_listView:Landroid/widget/ListView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/ListView;->getMeasuredWidth()I

    move-result v8

    .line 142
    .local v8, "exceptTextSize":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v11

    .line 143
    .local v11, "itemTotalNumber":I
    const/4 v15, 0x0

    .line 145
    .local v15, "popupWindowWidth":I
    new-instance v20, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-object/from16 v23, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 146
    .local v20, "tempText":Landroid/widget/TextView;
    const/16 v23, 0x1

    const/high16 v24, 0x41880000    # 17.0f

    move-object/from16 v0, v20

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 149
    const/16 v19, 0x0

    .local v19, "start":I
    :goto_0
    move/from16 v0, v19

    if-ge v0, v11, :cond_2

    .line 151
    invoke-virtual/range {v20 .. v20}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/infraware/polarisoffice5/common/MultiListItem;

    invoke-virtual/range {v23 .. v23}, Lcom/infraware/polarisoffice5/common/MultiListItem;->getText1()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v21, v0

    .line 153
    .local v21, "x_wid":I
    if-nez v19, :cond_0

    .line 154
    sub-int v8, v8, v21

    .line 156
    :cond_0
    move/from16 v0, v21

    if-le v0, v15, :cond_1

    .line 157
    move/from16 v15, v21

    .line 149
    :cond_1
    add-int/lit8 v19, v19, 0x1

    goto :goto_0

    .line 159
    .end local v21    # "x_wid":I
    :cond_2
    const/16 v20, 0x0

    .line 160
    int-to-float v0, v15

    move/from16 v23, v0

    int-to-float v0, v8

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-object/from16 v25, v0

    const/high16 v26, 0x41200000    # 10.0f

    invoke-static/range {v25 .. v26}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v25

    add-float v24, v24, v25

    add-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v15, v0

    .line 162
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/infraware/common/util/Utils;->getScreenWidthPixels(Landroid/app/Activity;)I

    move-result v17

    .line 164
    .local v17, "screenWidth":I
    add-int/lit8 v23, v17, -0xa

    move/from16 v0, v23

    if-ge v0, v15, :cond_3

    .line 165
    add-int/lit8 v15, v17, -0xa

    .line 169
    :cond_3
    const/4 v14, 0x0

    .line 170
    .local v14, "popupWindowHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v23

    if-eqz v23, :cond_4

    .line 171
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_listView:Landroid/widget/ListView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/ListView;->getMeasuredHeight()I

    move-result v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_listView:Landroid/widget/ListView;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v24

    add-int v23, v23, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v24

    mul-int v23, v23, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-object/from16 v24, v0

    const/high16 v25, 0x40000000    # 2.0f

    invoke-static/range {v24 .. v25}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v24

    add-int v14, v23, v24

    .line 175
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-object/from16 v23, v0

    const v24, 0x7f0b024e

    invoke-virtual/range {v23 .. v24}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 176
    .local v6, "btnMenuView":Landroid/view/View;
    new-instance v16, Landroid/graphics/Rect;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Rect;-><init>()V

    .line 177
    .local v16, "rect":Landroid/graphics/Rect;
    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 178
    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v22, v0

    .line 179
    .local v22, "y":I
    const/16 v16, 0x0

    .line 180
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getWindow()Landroid/view/Window;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v7

    .line 181
    .local v7, "deco":Landroid/view/View;
    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    move-result v23

    sub-int v23, v23, v22

    move/from16 v0, v23

    if-gt v0, v14, :cond_5

    .line 182
    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    move-result v23

    sub-int v14, v23, v22

    .line 185
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_flag:I

    move/from16 v23, v0

    packed-switch v23, :pswitch_data_0

    .line 200
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_flag:I

    move/from16 v23, v0

    if-nez v23, :cond_7

    .line 201
    new-instance v23, Landroid/widget/PopupWindow;

    move-object/from16 v0, v23

    invoke-direct {v0, v13, v15, v14}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mPopup:Landroid/widget/PopupWindow;

    .line 207
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mPopup:Landroid/widget/PopupWindow;

    move-object/from16 v23, v0

    new-instance v24, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$3;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$3;-><init>(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)V

    invoke-virtual/range {v23 .. v24}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_listView:Landroid/widget/ListView;

    move-object/from16 v23, v0

    new-instance v24, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$4;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$4;-><init>(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)V

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 253
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f0201a6

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 254
    .local v5, "background":Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mPopup:Landroid/widget/PopupWindow;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 255
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mPopup:Landroid/widget/PopupWindow;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    invoke-virtual/range {v23 .. v24}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 256
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mPopup:Landroid/widget/PopupWindow;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    invoke-virtual/range {v23 .. v24}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    .line 257
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mPopup:Landroid/widget/PopupWindow;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    invoke-virtual/range {v23 .. v24}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 259
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mPopup:Landroid/widget/PopupWindow;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v23

    const/16 v24, 0x1

    invoke-virtual/range {v23 .. v24}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 260
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mPopup:Landroid/widget/PopupWindow;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v23

    new-instance v24, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$5;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$5;-><init>(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)V

    invoke-virtual/range {v23 .. v24}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 278
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_flag:I

    move/from16 v23, v0

    packed-switch v23, :pswitch_data_1

    .line 296
    :cond_6
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mPopup:Landroid/widget/PopupWindow;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/PopupWindow;->update()V

    .line 297
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->isClicked:Z

    .line 298
    return-void

    .line 187
    .end local v5    # "background":Landroid/graphics/drawable/Drawable;
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    invoke-virtual/range {v23 .. v24}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setMenuButtonSelected(Z)V

    goto/16 :goto_1

    .line 190
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    invoke-virtual/range {v23 .. v24}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setFormatIconSelected(Z)V

    goto/16 :goto_1

    .line 193
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    invoke-virtual/range {v23 .. v24}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setFindOptionIconSelected(Z)V

    goto/16 :goto_1

    .line 196
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    invoke-virtual/range {v23 .. v24}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setReplaceOptionIconSelected(Z)V

    goto/16 :goto_1

    .line 202
    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_flag:I

    move/from16 v23, v0

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_8

    .line 203
    new-instance v23, Landroid/widget/PopupWindow;

    move-object/from16 v0, v23

    invoke-direct {v0, v13, v15, v14}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mPopup:Landroid/widget/PopupWindow;

    goto/16 :goto_2

    .line 205
    :cond_8
    new-instance v23, Landroid/widget/PopupWindow;

    move-object/from16 v0, v23

    invoke-direct {v0, v13, v15, v14}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mPopup:Landroid/widget/PopupWindow;

    goto/16 :goto_2

    .line 280
    .restart local v5    # "background":Landroid/graphics/drawable/Drawable;
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-object/from16 v23, v0

    const v24, 0x7f0b024e

    invoke-virtual/range {v23 .. v24}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageButton;

    .line 281
    .local v9, "ibMenu":Landroid/widget/ImageButton;
    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v12, v0, [I

    .line 282
    .local v12, "nMenuButtonPos":[I
    invoke-virtual {v9, v12}, Landroid/widget/ImageButton;->getLocationInWindow([I)V

    .line 283
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getTextEditorActionBar()Landroid/widget/LinearLayout;

    move-result-object v4

    .line 284
    .local v4, "actionBar":Landroid/widget/LinearLayout;
    if-eqz v4, :cond_6

    .line 285
    const/16 v23, 0x0

    const/16 v24, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v4, v0, v1}, Landroid/widget/LinearLayout;->measure(II)V

    .line 286
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mPopup:Landroid/widget/PopupWindow;

    move-object/from16 v23, v0

    const/16 v24, 0x35

    const/16 v25, 0x0

    const/16 v26, 0x1

    aget v26, v12, v26

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v27

    add-int v26, v26, v27

    move-object/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v0, v9, v1, v2, v3}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    goto/16 :goto_3

    .line 290
    .end local v4    # "actionBar":Landroid/widget/LinearLayout;
    .end local v9    # "ibMenu":Landroid/widget/ImageButton;
    .end local v12    # "nMenuButtonPos":[I
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mPopup:Landroid/widget/PopupWindow;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-object/from16 v24, v0

    const v25, 0x7f0b0249

    invoke-virtual/range {v24 .. v25}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v24

    const/16 v25, 0x0

    const/16 v26, 0x0

    invoke-virtual/range {v23 .. v26}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    goto/16 :goto_3

    .line 293
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mPopup:Landroid/widget/PopupWindow;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-object/from16 v24, v0

    const v25, 0x7f0b0250

    invoke-virtual/range {v24 .. v25}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v24

    const/16 v25, 0x0

    const/16 v26, 0x0

    invoke-virtual/range {v23 .. v26}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    goto/16 :goto_3

    .line 185
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 278
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public setCheckMatchCase(Z)V
    .locals 4
    .param p1, "m_bCheckMatchCase"    # Z

    .prologue
    const/4 v3, 0x0

    .line 491
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_bCheckMatchCase:Z

    .line 492
    const/4 v1, 0x1

    .line 493
    .local v1, "TRUE":I
    const/4 v0, 0x0

    .line 495
    .local v0, "FALSE":I
    if-eqz p1, :cond_0

    .line 496
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/MultiListItem;

    invoke-virtual {v2, v1}, Lcom/infraware/polarisoffice5/common/MultiListItem;->setRightBtnState(I)V

    .line 500
    :goto_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    new-instance v3, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$6;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$6;-><init>(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)V

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 507
    return-void

    .line 498
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/MultiListItem;

    invoke-virtual {v2, v0}, Lcom/infraware/polarisoffice5/common/MultiListItem;->setRightBtnState(I)V

    goto :goto_0
.end method

.method public setCheckWholeWord(Z)V
    .locals 4
    .param p1, "m_bCheckWholeWord"    # Z

    .prologue
    const/4 v3, 0x1

    .line 514
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_bCheckWholeWord:Z

    .line 515
    const/4 v1, 0x1

    .line 516
    .local v1, "TRUE":I
    const/4 v0, 0x0

    .line 518
    .local v0, "FALSE":I
    if-eqz p1, :cond_0

    .line 519
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/MultiListItem;

    invoke-virtual {v2, v1}, Lcom/infraware/polarisoffice5/common/MultiListItem;->setRightBtnState(I)V

    .line 523
    :goto_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    new-instance v3, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$7;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$7;-><init>(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)V

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 530
    return-void

    .line 521
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/MultiListItem;

    invoke-virtual {v2, v0}, Lcom/infraware/polarisoffice5/common/MultiListItem;->setRightBtnState(I)V

    goto :goto_0
.end method
