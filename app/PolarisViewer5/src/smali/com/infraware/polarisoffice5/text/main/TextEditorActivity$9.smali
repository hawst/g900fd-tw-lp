.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$9;
.super Ljava/lang/Object;
.source "TextEditorActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setEventListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
    .locals 0

    .prologue
    .line 1419
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$9;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 1422
    const/16 v0, 0x7d0

    .line 1424
    .local v0, "delay":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$9;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mDetector:Landroid/view/GestureDetector;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1200(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/view/GestureDetector;

    move-result-object v3

    invoke-virtual {v3, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1425
    const/4 v2, 0x1

    .line 1448
    :cond_0
    :goto_0
    return v2

    .line 1427
    :cond_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$9;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$9;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isShown()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1428
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$9;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->hidePanel()V

    .line 1430
    :cond_2
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$9;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hasAnyUndoRedo()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1431
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$9;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setChanged(Z)V

    .line 1433
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    .line 1435
    .local v1, "timer":Ljava/util/Timer;
    new-instance v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$9$1;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$9$1;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$9;)V

    int-to-long v4, v0

    invoke-virtual {v1, v3, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0
.end method
