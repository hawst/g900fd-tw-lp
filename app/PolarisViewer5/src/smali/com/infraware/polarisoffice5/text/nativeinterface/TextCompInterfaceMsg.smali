.class Lcom/infraware/polarisoffice5/text/nativeinterface/TextCompInterfaceMsg;
.super Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;
.source "TextCompInterfaceMsg.java"


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;-><init>()V

    .line 9
    return-void
.end method


# virtual methods
.method public IMakeImage(Ljava/lang/String;)I
    .locals 1
    .param p1, "line"    # Ljava/lang/String;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/nativeinterface/TextCompInterfaceMsg;->Native:Lcom/infraware/polarisoffice5/text/nativeinterface/TextNative;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/text/nativeinterface/TextNative;->IMakeImage(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public ISetInitializeInfo(Ljava/lang/String;II)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "dipInfo"    # I
    .param p3, "maxline"    # I

    .prologue
    .line 12
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/nativeinterface/TextCompInterfaceMsg;->Native:Lcom/infraware/polarisoffice5/text/nativeinterface/TextNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/polarisoffice5/text/nativeinterface/TextNative;->ISetInitializeInfo(Ljava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method public IStopPrint()Z
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/nativeinterface/TextCompInterfaceMsg;->Native:Lcom/infraware/polarisoffice5/text/nativeinterface/TextNative;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/nativeinterface/TextNative;->IStopPrint()Z

    move-result v0

    return v0
.end method
