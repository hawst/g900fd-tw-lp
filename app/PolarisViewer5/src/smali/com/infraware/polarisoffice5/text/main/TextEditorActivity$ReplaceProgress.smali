.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;
.super Ljava/lang/Thread;
.source "TextEditorActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReplaceProgress"
.end annotation


# instance fields
.field private nOffset:I

.field private pd:Landroid/app/ProgressDialog;

.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4464
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 4461
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;->pd:Landroid/app/ProgressDialog;

    .line 4462
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;->nOffset:I

    .line 4465
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;->pd:Landroid/app/ProgressDialog;

    .line 4466
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 4467
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 4468
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700ab

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 4469
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 4471
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->requestFocus()Z

    .line 4472
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hideSoftKeyboard()Z

    .line 4473
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplaceFind:Landroid/widget/EditText;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$4500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplace:Landroid/widget/EditText;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$5000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachCase:Z
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$4400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Z

    move-result v2

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachWholeWord:Z
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$4300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Z

    move-result v3

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->replaceText(Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 4475
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setLoadingProgress(Z)V

    .line 4476
    return-void
.end method

.method static synthetic access$5100(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;

    .prologue
    .line 4460
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;->nOffset:I

    return v0
.end method

.method static synthetic access$5200(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;

    .prologue
    .line 4460
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;->pd:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 4479
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplaceFind:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$4500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachCase:Z
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$4400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Z

    move-result v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachWholeWord:Z
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$4300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Z

    move-result v3

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findNextText(Ljava/lang/String;ZZZ)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;->nOffset:I

    .line 4481
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress$1;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress$1;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 4489
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress$2;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress$2;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 4499
    return-void
.end method
