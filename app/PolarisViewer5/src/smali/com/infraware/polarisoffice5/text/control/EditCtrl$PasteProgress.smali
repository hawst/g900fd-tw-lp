.class Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;
.super Ljava/lang/Thread;
.source "EditCtrl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/text/control/EditCtrl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PasteProgress"
.end annotation


# instance fields
.field private pd:Landroid/app/ProgressDialog;

.field private pos:I

.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 3734
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 3731
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;->pd:Landroid/app/ProgressDialog;

    .line 3735
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;->pd:Landroid/app/ProgressDialog;

    .line 3737
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 3738
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 3739
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070304

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 3741
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 3743
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setLoadingProgress(Z)V

    .line 3744
    return-void
.end method

.method static synthetic access$2400(Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;

    .prologue
    .line 3730
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;->pos:I

    return v0
.end method

.method static synthetic access$2600(Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;

    .prologue
    .line 3730
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;->pd:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 3747
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # invokes: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->onPaste()I
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$2300(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;->pos:I

    .line 3749
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$2200(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress$1;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress$1;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 3760
    return-void
.end method
