.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress$1;
.super Ljava/lang/Object;
.source "TextEditorActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;)V
    .locals 0

    .prologue
    .line 4572
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 4575
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->pos:I
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->access$5600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 4576
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->pos:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->access$5600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->undoRedoViewProcess(I)V

    .line 4579
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->undoredoStateChange()V

    .line 4580
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->pd:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->access$5700(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 4582
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setLoadingProgress(Z)V

    .line 4583
    return-void
.end method
