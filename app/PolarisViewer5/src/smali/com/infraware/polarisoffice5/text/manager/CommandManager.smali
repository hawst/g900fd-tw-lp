.class public Lcom/infraware/polarisoffice5/text/manager/CommandManager;
.super Ljava/lang/Object;
.source "CommandManager.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Size;


# instance fields
.field private currentStack:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/infraware/polarisoffice5/text/manager/UndoCommand;",
            ">;"
        }
    .end annotation
.end field

.field private m_EditCtrl:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

.field private m_bOneAction:Z

.field private m_bOneActionAddCmd:Z

.field private m_bOneWord:Z

.field private m_nCmdNum:I

.field private numStack:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private redoStack:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/infraware/polarisoffice5/text/manager/UndoCommand;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V
    .locals 2
    .param p1, "editCtrl"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .prologue
    const/4 v1, 0x0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->m_EditCtrl:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .line 15
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->m_bOneWord:Z

    .line 16
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->m_bOneAction:Z

    .line 17
    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->m_nCmdNum:I

    .line 18
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->m_bOneActionAddCmd:Z

    .line 22
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->m_EditCtrl:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->currentStack:Ljava/util/List;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->redoStack:Ljava/util/List;

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->numStack:Ljava/util/List;

    .line 26
    return-void
.end method

.method private addNumStack()V
    .locals 2

    .prologue
    .line 216
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->m_nCmdNum:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->m_nCmdNum:I

    .line 217
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->numStack:Ljava/util/List;

    iget v1, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->m_nCmdNum:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->m_nCmdNum:I

    const/16 v1, 0xa

    if-lt v0, v1, :cond_0

    .line 219
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->m_nCmdNum:I

    .line 220
    :cond_0
    return-void
.end method

.method private checkOneWord(ILjava/lang/String;IIZ)Z
    .locals 10
    .param p1, "nBlock"    # I
    .param p2, "strText"    # Ljava/lang/String;
    .param p3, "nStart"    # I
    .param p4, "nLen"    # I
    .param p5, "bInsert"    # Z

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 88
    :try_start_0
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->currentStack:Ljava/util/List;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->currentStackLength()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;

    .line 89
    .local v1, "lastCmd":Lcom/infraware/polarisoffice5/text/manager/UndoCommand;
    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->getText()Ljava/lang/String;

    move-result-object v4

    .line 91
    .local v4, "strLastTxt":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->isInsert()Z

    move-result v8

    if-ne v8, p5, :cond_0

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->getBlock()I

    move-result v8

    if-eq v8, p1, :cond_1

    .line 132
    .end local v1    # "lastCmd":Lcom/infraware/polarisoffice5/text/manager/UndoCommand;
    .end local v4    # "strLastTxt":Ljava/lang/String;
    :cond_0
    :goto_0
    return v6

    .line 94
    .restart local v1    # "lastCmd":Lcom/infraware/polarisoffice5/text/manager/UndoCommand;
    .restart local v4    # "strLastTxt":Ljava/lang/String;
    :cond_1
    const-string/jumbo v5, ""

    .line 95
    .local v5, "strNewText":Ljava/lang/String;
    if-eqz p5, :cond_5

    .line 96
    const-string/jumbo v2, ""

    .line 97
    .local v2, "strFirstChar":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_2

    .line 98
    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-virtual {v4, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 100
    :cond_2
    const-string/jumbo v8, " "

    invoke-virtual {v2, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_3

    const-string/jumbo v8, "\n"

    invoke-virtual {v2, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_3

    const-string/jumbo v8, "\t"

    invoke-virtual {v2, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_4

    .line 101
    :cond_3
    const-string/jumbo v8, " "

    invoke-virtual {p2, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_4

    const-string/jumbo v8, "\n"

    invoke-virtual {p2, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_4

    const-string/jumbo v8, "\t"

    invoke-virtual {p2, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_0

    .line 105
    :cond_4
    add-int v8, p3, p4

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->getStart()I

    move-result v9

    if-ne v8, v9, :cond_0

    .line 108
    invoke-virtual {v1, p3}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->setStart(I)V

    .line 109
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 126
    .end local v2    # "strFirstChar":Ljava/lang/String;
    :goto_1
    invoke-virtual {v1, v5}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->setText(Ljava/lang/String;)V

    .line 127
    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->getLength()I

    move-result v8

    add-int/2addr v8, p4

    invoke-virtual {v1, v8}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->setLength(I)V

    move v6, v7

    .line 132
    goto :goto_0

    .line 111
    :cond_5
    const-string/jumbo v3, ""

    .line 112
    .local v3, "strLastChar":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_6

    .line 113
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v4, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 115
    :cond_6
    const-string/jumbo v8, " "

    invoke-virtual {v3, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_7

    const-string/jumbo v8, "\n"

    invoke-virtual {v3, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_7

    const-string/jumbo v8, "\t"

    invoke-virtual {v3, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_8

    .line 116
    :cond_7
    const-string/jumbo v8, " "

    invoke-virtual {p2, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_8

    const-string/jumbo v8, "\n"

    invoke-virtual {p2, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_8

    const-string/jumbo v8, "\t"

    invoke-virtual {p2, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_0

    .line 120
    :cond_8
    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->getStart()I

    move-result v8

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->getLength()I

    move-result v9

    add-int/2addr v8, v9

    if-ne v8, p3, :cond_0

    .line 123
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    goto :goto_1

    .line 128
    .end local v1    # "lastCmd":Lcom/infraware/polarisoffice5/text/manager/UndoCommand;
    .end local v3    # "strLastChar":Ljava/lang/String;
    .end local v4    # "strLastTxt":Ljava/lang/String;
    .end local v5    # "strNewText":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 129
    .local v0, "e":Ljava/lang/Exception;
    goto/16 :goto_0
.end method

.method private removeFirstStack()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 74
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->currentStack:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->getNum()I

    move-result v0

    .line 75
    .local v0, "nNum":I
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->currentStackLength()I

    move-result v1

    if-lez v1, :cond_0

    .line 76
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->currentStack:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->getNum()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 77
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->currentStack:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 82
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->numStack:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 83
    return-void
.end method


# virtual methods
.method public addCommand(ILjava/lang/String;IIZ)V
    .locals 12
    .param p1, "nBlock"    # I
    .param p2, "strText"    # Ljava/lang/String;
    .param p3, "nStart"    # I
    .param p4, "nLen"    # I
    .param p5, "bInsert"    # Z

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->currentStackLength()I

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->m_bOneWord:Z

    if-nez v1, :cond_1

    .line 50
    invoke-direct/range {p0 .. p5}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->checkOneWord(ILjava/lang/String;IIZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 71
    :cond_0
    :goto_0
    return-void

    .line 54
    :cond_1
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->m_bOneAction:Z

    if-eqz v1, :cond_2

    .line 55
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->m_bOneActionAddCmd:Z

    .line 59
    :goto_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->m_bOneWord:Z

    .line 60
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->redoStack:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 61
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v8

    .line 62
    .local v8, "freeHeapMem":J
    new-instance v0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->m_EditCtrl:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget v7, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->m_nCmdNum:I

    move v2, p1

    move-object v3, p2

    move v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;ILjava/lang/String;IIZI)V

    .line 63
    .local v0, "command":Lcom/infraware/polarisoffice5/text/manager/UndoCommand;
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v1

    sub-long v10, v8, v1

    .line 64
    .local v10, "nMemSize":J
    :goto_2
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v1

    cmp-long v1, v10, v1

    if-lez v1, :cond_3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->currentStackLength()I

    move-result v1

    if-eqz v1, :cond_3

    .line 65
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->removeFirstStack()V

    goto :goto_2

    .line 57
    .end local v0    # "command":Lcom/infraware/polarisoffice5/text/manager/UndoCommand;
    .end local v8    # "freeHeapMem":J
    .end local v10    # "nMemSize":J
    :cond_2
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->addNumStack()V

    goto :goto_1

    .line 67
    .restart local v0    # "command":Lcom/infraware/polarisoffice5/text/manager/UndoCommand;
    .restart local v8    # "freeHeapMem":J
    .restart local v10    # "nMemSize":J
    :cond_3
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->currentStack:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->numStack:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/16 v2, 0xa

    if-le v1, v2, :cond_0

    .line 69
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->removeFirstStack()V

    goto :goto_0
.end method

.method public clearCommand()V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->redoStack:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 30
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->currentStack:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 31
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->numStack:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->m_nCmdNum:I

    .line 33
    return-void
.end method

.method public currentStackLength()I
    .locals 2

    .prologue
    .line 136
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->currentStack:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    array-length v0, v1

    .line 137
    .local v0, "length":I
    return v0
.end method

.method public hasMoreRedo()Z
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->redoStack:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMoreUndo()Z
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->currentStack:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public redo()I
    .locals 6

    .prologue
    .line 176
    const/4 v2, -0x1

    .line 177
    .local v2, "nPos":I
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->redoStack:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v4

    array-length v0, v4

    .line 178
    .local v0, "length":I
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->redoStack:Ljava/util/List;

    add-int/lit8 v5, v0, -0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->getNum()I

    move-result v1

    .line 180
    .local v1, "nNum":I
    :goto_0
    if-lez v0, :cond_1

    .line 181
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->redoStack:Ljava/util/List;

    add-int/lit8 v5, v0, -0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->getNum()I

    move-result v4

    if-ne v1, v4, :cond_1

    .line 182
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->redoStack:Ljava/util/List;

    add-int/lit8 v5, v0, -0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;

    .line 183
    .local v3, "redoCommand":Lcom/infraware/polarisoffice5/text/manager/UndoCommand;
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->redoStack:Ljava/util/List;

    add-int/lit8 v5, v0, -0x1

    invoke-interface {v4, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 184
    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->execute()I

    move-result v2

    .line 186
    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->isInsert()Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v4, 0x1

    :goto_1
    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->setInsert(Z)V

    .line 187
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->currentStack:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->redoStack:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v4

    array-length v0, v4

    .line 190
    goto :goto_0

    .line 186
    :cond_0
    const/4 v4, 0x0

    goto :goto_1

    .line 195
    .end local v3    # "redoCommand":Lcom/infraware/polarisoffice5/text/manager/UndoCommand;
    :cond_1
    return v2
.end method

.method public replaceCommand(ILjava/lang/String;IIZ)V
    .locals 4
    .param p1, "nBlock"    # I
    .param p2, "strText"    # Ljava/lang/String;
    .param p3, "nStart"    # I
    .param p4, "nLen"    # I
    .param p5, "bInsert"    # Z

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->currentStackLength()I

    move-result v2

    if-nez v2, :cond_1

    .line 46
    :cond_0
    :goto_0
    return-void

    .line 38
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->currentStack:Ljava/util/List;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->currentStackLength()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;

    .line 40
    .local v0, "lastCmd":Lcom/infraware/polarisoffice5/text/manager/UndoCommand;
    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->getBlock()I

    move-result v2

    if-ne p1, v2, :cond_0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->getStart()I

    move-result v2

    if-lt p3, v2, :cond_0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->getLength()I

    move-result v2

    if-gt p4, v2, :cond_0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->isInsert()Z

    move-result v2

    if-ne p5, v2, :cond_0

    .line 41
    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->getText()Ljava/lang/String;

    move-result-object v1

    .line 42
    .local v1, "strReplaceText":Ljava/lang/String;
    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->getLength()I

    move-result v3

    sub-int/2addr v3, p4

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 43
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 44
    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->setText(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setOneAction(Z)V
    .locals 2
    .param p1, "bOneAction"    # Z

    .prologue
    .line 203
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->m_bOneAction:Z

    .line 204
    if-eqz p1, :cond_1

    .line 205
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->addNumStack()V

    .line 206
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->m_bOneActionAddCmd:Z

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 208
    :cond_1
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->m_bOneActionAddCmd:Z

    if-nez v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->numStack:Ljava/util/List;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->numStack:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 210
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->m_nCmdNum:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->m_nCmdNum:I

    goto :goto_0
.end method

.method public setOneWord()V
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->m_bOneWord:Z

    .line 200
    return-void
.end method

.method public undo()I
    .locals 6

    .prologue
    .line 141
    const/4 v2, -0x1

    .line 142
    .local v2, "nPos":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->currentStackLength()I

    move-result v0

    .line 145
    .local v0, "length":I
    if-lez v0, :cond_1

    .line 146
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->currentStack:Ljava/util/List;

    add-int/lit8 v5, v0, -0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->getNum()I

    move-result v1

    .line 148
    .local v1, "nNum":I
    :goto_0
    if-lez v0, :cond_1

    .line 149
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->currentStack:Ljava/util/List;

    add-int/lit8 v5, v0, -0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->getNum()I

    move-result v4

    if-ne v1, v4, :cond_1

    .line 150
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->currentStack:Ljava/util/List;

    add-int/lit8 v5, v0, -0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;

    .line 151
    .local v3, "undoCommand":Lcom/infraware/polarisoffice5/text/manager/UndoCommand;
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->currentStack:Ljava/util/List;

    add-int/lit8 v5, v0, -0x1

    invoke-interface {v4, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 152
    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->execute()I

    move-result v2

    .line 154
    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->isInsert()Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v4, 0x1

    :goto_1
    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->setInsert(Z)V

    .line 155
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->redoStack:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->currentStackLength()I

    move-result v0

    .line 158
    goto :goto_0

    .line 154
    :cond_0
    const/4 v4, 0x0

    goto :goto_1

    .line 164
    .end local v1    # "nNum":I
    .end local v3    # "undoCommand":Lcom/infraware/polarisoffice5/text/manager/UndoCommand;
    :cond_1
    return v2
.end method
