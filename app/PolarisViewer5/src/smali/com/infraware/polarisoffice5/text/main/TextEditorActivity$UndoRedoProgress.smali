.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;
.super Ljava/lang/Thread;
.source "TextEditorActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UndoRedoProgress"
.end annotation


# instance fields
.field private pd:Landroid/app/ProgressDialog;

.field private pos:I

.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

.field private undo:Z


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Z)V
    .locals 3
    .param p2, "bUndo"    # Z

    .prologue
    const/4 v1, 0x0

    .line 4551
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 4547
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->pd:Landroid/app/ProgressDialog;

    .line 4552
    iput-boolean p2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->undo:Z

    .line 4554
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->pd:Landroid/app/ProgressDialog;

    .line 4555
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 4556
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 4557
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->undo:Z

    if-eqz v0, :cond_0

    .line 4558
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070307

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 4561
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 4563
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setLoadingProgress(Z)V

    .line 4564
    return-void

    .line 4560
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070305

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic access$5600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;

    .prologue
    .line 4546
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->pos:I

    return v0
.end method

.method static synthetic access$5700(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;

    .prologue
    .line 4546
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->pd:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 4567
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->undo:Z

    if-eqz v0, :cond_0

    .line 4568
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->undo()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->pos:I

    .line 4572
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress$1;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress$1;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 4585
    return-void

    .line 4570
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->redo()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->pos:I

    goto :goto_0
.end method
