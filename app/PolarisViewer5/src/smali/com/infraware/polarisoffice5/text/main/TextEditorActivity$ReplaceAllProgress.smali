.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;
.super Ljava/lang/Thread;
.source "TextEditorActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReplaceAllProgress"
.end annotation


# instance fields
.field private pd:Landroid/app/ProgressDialog;

.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 4505
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 4503
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->pd:Landroid/app/ProgressDialog;

    .line 4507
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->pd:Landroid/app/ProgressDialog;

    .line 4508
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 4509
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 4510
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700aa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 4511
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 4513
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setLoadingProgress(Z)V

    .line 4515
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->requestFocus()Z

    .line 4516
    return-void
.end method

.method static synthetic access$5500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;

    .prologue
    .line 4502
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->pd:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 4519
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplaceFind:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$4500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplace:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$5000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachCase:Z
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$4400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Z

    move-result v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachWholeWord:Z
    invoke-static {v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$4300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Z

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->replaceAllText(Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 4521
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress$1;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress$1;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 4543
    return-void
.end method
