.class Lcom/infraware/polarisoffice5/text/control/EditCtrl$PopupViewerSearchAdapter;
.super Landroid/widget/ArrayAdapter;
.source "EditCtrl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/text/control/EditCtrl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PopupViewerSearchAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final resId:I

.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "textViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2798
    .local p4, "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PopupViewerSearchAdapter;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .line 2800
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 2802
    iput-object p2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PopupViewerSearchAdapter;->mContext:Landroid/content/Context;

    .line 2803
    iput-object p4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PopupViewerSearchAdapter;->mDataList:Ljava/util/List;

    .line 2804
    iput p3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PopupViewerSearchAdapter;->resId:I

    .line 2805
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 2811
    move-object v2, p2

    .line 2813
    .local v2, "row":Landroid/view/View;
    if-nez v2, :cond_1

    .line 2815
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PopupViewerSearchAdapter;->mContext:Landroid/content/Context;

    const-string/jumbo v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 2816
    .local v1, "inflator":Landroid/view/LayoutInflater;
    iget v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PopupViewerSearchAdapter;->resId:I

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 2817
    new-instance v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PopupViewerSearchViewHolder;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PopupViewerSearchAdapter;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-direct {v0, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PopupViewerSearchViewHolder;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V

    .line 2818
    .local v0, "holder":Lcom/infraware/polarisoffice5/text/control/EditCtrl$PopupViewerSearchViewHolder;
    const v4, 0x7f0b01c1

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PopupViewerSearchViewHolder;->tvMenuItem:Landroid/widget/TextView;

    .line 2822
    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2826
    .end local v1    # "inflator":Landroid/view/LayoutInflater;
    :goto_0
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PopupViewerSearchAdapter;->mDataList:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2828
    .local v3, "to":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 2829
    iget-object v4, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PopupViewerSearchViewHolder;->tvMenuItem:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2833
    :cond_0
    return-object v2

    .line 2824
    .end local v0    # "holder":Lcom/infraware/polarisoffice5/text/control/EditCtrl$PopupViewerSearchViewHolder;
    .end local v3    # "to":Ljava/lang/String;
    :cond_1
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PopupViewerSearchViewHolder;

    .restart local v0    # "holder":Lcom/infraware/polarisoffice5/text/control/EditCtrl$PopupViewerSearchViewHolder;
    goto :goto_0
.end method
