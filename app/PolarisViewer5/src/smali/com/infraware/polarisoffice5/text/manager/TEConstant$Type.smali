.class public interface abstract Lcom/infraware/polarisoffice5/text/manager/TEConstant$Type;
.super Ljava/lang/Object;
.source "TEConstant.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/text/manager/TEConstant;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Type"
.end annotation


# static fields
.field public static final TYPE_DEFAULT_FONT_SIZE:I = 0x0

.field public static final TYPE_DISPLAYINFO_HDPI:I = 0x0

.field public static final TYPE_DISPLAYINFO_HEIGHT:I = 0x1

.field public static final TYPE_DISPLAYINFO_WIDTH:I = 0x0

.field public static final TYPE_DISPLAYINFO_XHDPI:I = 0x1

.field public static final TYPE_DPIINFO_A4_100:I = 0x1

.field public static final TYPE_DPIINFO_A4_150:I = 0x2

.field public static final TYPE_DPIINFO_A4_200:I = 0x3

.field public static final TYPE_DPIINFO_A4_300:I = 0x4

.field public static final TYPE_DPIINFO_A4_600:I = 0x5

.field public static final TYPE_DPIINFO_A4_72:I = 0x0

.field public static final TYPE_FIND_NEXT_PROGRESS:I = 0x0

.field public static final TYPE_FIND_PREV_PROGRESS:I = 0x1

.field public static final TYPE_POPUPWINDOW_FINDREPLACE:I = 0x1

.field public static final TYPE_POPUPWINDOW_MOREPOPUP:I = 0x2

.field public static final TYPE_POPUPWINDOW_TOAST:I = 0x0

.field public static final TYPE_REPLACE_FIND_PROGRESS:I = 0x2

.field public static final TYPE_SEARCH_BY_DICTIONARY:I = 0x2

.field public static final TYPE_SEARCH_BY_GOOGLE:I = 0x0

.field public static final TYPE_SEARCH_BY_WIKI:I = 0x1

.field public static final TYPE_SHOW_FIND_MODE:I = 0x2

.field public static final TYPE_SHOW_REPLACE_MODE:I = 0x3

.field public static final TYPE_SHOW_TITLE_MODE:I = 0x1

.field public static final TYPE_SHOW_TTS_MODE:I = 0x4

.field public static final TYPE_THEME_BLACK:I = 0x2

.field public static final TYPE_THEME_LIGHTGRAY:I = 0x1

.field public static final TYPE_THEME_YELLOW:I = 0x0

.field public static final TYPE_TOASTPOPUP_DOWN_CENTER:I = 0x4

.field public static final TYPE_TOASTPOPUP_DOWN_LEFT:I = 0x3

.field public static final TYPE_TOASTPOPUP_DOWN_RIGHT:I = 0x5

.field public static final TYPE_TOASTPOPUP_LONG:I = 0x0

.field public static final TYPE_TOASTPOPUP_MORE:I = 0x6

.field public static final TYPE_TOASTPOPUP_SHORT:I = 0x1

.field public static final TYPE_TOASTPOPUP_UP_CENTER:I = 0x1

.field public static final TYPE_TOASTPOPUP_UP_LEFT:I = 0x0

.field public static final TYPE_TOASTPOPUP_UP_RIGHT:I = 0x2
