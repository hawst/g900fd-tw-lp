.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;
.super Ljava/lang/Thread;
.source "TextEditorActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OffsetToCaret"
.end annotation


# instance fields
.field byteoffset:[I

.field caretposition:[I

.field list:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/infraware/polarisoffice5/text/manager/TextBlock;",
            ">;"
        }
    .end annotation
.end field

.field pd:Landroid/app/ProgressDialog;

.field resultBlock:I

.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;[I)V
    .locals 3
    .param p2, "pos"    # [I

    .prologue
    const/4 v1, 0x0

    .line 2353
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 2345
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->pd:Landroid/app/ProgressDialog;

    .line 2351
    iput v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->resultBlock:I

    .line 2354
    iput-object p2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->byteoffset:[I

    .line 2356
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockOffsetList()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->list:Ljava/util/LinkedList;

    .line 2358
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->pd:Landroid/app/ProgressDialog;

    .line 2359
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 2360
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 2361
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0702ea

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 2362
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 2363
    return-void
.end method

.method private findBlock()I
    .locals 6

    .prologue
    .line 2452
    const/4 v2, 0x0

    .line 2454
    .local v2, "result":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->list:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 2455
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->list:Ljava/util/LinkedList;

    invoke-virtual {v3, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    .line 2456
    .local v0, "blockInfo":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    iget v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nStartOffset:I

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->byteoffset:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    if-ge v3, v4, :cond_0

    .line 2457
    iget v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nEndOffset:I

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->byteoffset:[I

    const/4 v5, 0x1

    aget v4, v4, v5

    if-ge v3, v4, :cond_1

    .line 2454
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2460
    :cond_1
    move v2, v1

    .line 2466
    .end local v0    # "blockInfo":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :cond_2
    return v2
.end method

.method private findCaret()[I
    .locals 12

    .prologue
    .line 2401
    const/4 v9, 0x2

    new-array v5, v9, [I

    fill-array-data v5, :array_0

    .line 2403
    .local v5, "result":[I
    const/4 v2, 0x0

    .line 2404
    .local v2, "findLeftSuccess":Z
    const/4 v3, 0x0

    .line 2406
    .local v3, "findRightSuccess":Z
    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v9}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v9

    iget v10, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->resultBlock:I

    invoke-virtual {v9, v10}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockText(I)Ljava/lang/String;

    move-result-object v6

    .line 2408
    .local v6, "str":Ljava/lang/String;
    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v9}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getCurCharset()Ljava/lang/String;

    move-result-object v1

    .line 2410
    .local v1, "charsetName":Ljava/lang/String;
    const-string/jumbo v8, ""

    .line 2411
    .local v8, "tempStr":Ljava/lang/String;
    const/4 v7, 0x0

    .line 2414
    .local v7, "tempByte":[B
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    :try_start_0
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v4, v9, :cond_2

    .line 2415
    const/4 v9, 0x0

    invoke-virtual {v6, v9, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 2416
    invoke-virtual {v8, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v7

    .line 2418
    array-length v10, v7

    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->list:Ljava/util/LinkedList;

    iget v11, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->resultBlock:I

    invoke-virtual {v9, v11}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    iget v9, v9, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nStartOffset:I

    add-int/2addr v9, v10

    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->byteoffset:[I

    const/4 v11, 0x0

    aget v10, v10, v11

    if-lt v9, v10, :cond_0

    if-nez v2, :cond_0

    .line 2420
    const/4 v2, 0x1

    .line 2422
    const/4 v9, 0x0

    aput v4, v5, v9

    .line 2425
    :cond_0
    array-length v10, v7

    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->list:Ljava/util/LinkedList;

    iget v11, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->resultBlock:I

    invoke-virtual {v9, v11}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    iget v9, v9, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nStartOffset:I

    add-int/2addr v9, v10

    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->byteoffset:[I

    const/4 v11, 0x1

    aget v10, v10, v11

    if-lt v9, v10, :cond_1

    if-nez v3, :cond_1

    .line 2427
    const/4 v3, 0x1

    .line 2429
    const/4 v9, 0x1

    aput v4, v5, v9

    .line 2432
    :cond_1
    if-eqz v2, :cond_4

    if-eqz v3, :cond_4

    .line 2434
    iget v9, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->resultBlock:I

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v9}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v9

    invoke-virtual {v9}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isSelectAllSupport()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 2448
    :cond_2
    :goto_1
    return-object v5

    .line 2437
    :cond_3
    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->list:Ljava/util/LinkedList;

    iget v10, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->resultBlock:I

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {v9, v10}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    iget v0, v9, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    .line 2438
    .local v0, "charCount":I
    const/4 v9, 0x0

    aget v10, v5, v9

    add-int/2addr v10, v0

    aput v10, v5, v9

    .line 2439
    const/4 v9, 0x1

    aget v10, v5, v9

    add-int/2addr v10, v0

    aput v10, v5, v9
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2444
    .end local v0    # "charCount":I
    :catch_0
    move-exception v9

    goto :goto_1

    .line 2414
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2401
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 2369
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->findBlock()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->resultBlock:I

    .line 2371
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->findCaret()[I

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->caretposition:[I

    .line 2373
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2398
    return-void
.end method
