.class public Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
.super Landroid/app/Activity;
.source "TextEditorActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/infraware/common/event/SdCardListener;
.implements Lcom/infraware/office/baseframe/EvBaseE$PopupDialogEventType;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Flag;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Path;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Size;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$StringReference;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Type;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;,
        Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$BufferProgress;,
        Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ScreenReceiver;,
        Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$NfcCallback;,
        Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;,
        Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;,
        Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;,
        Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;,
        Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;,
        Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;
    }
.end annotation


# static fields
.field private static final ALLOWED:Ljava/lang/String; = "[^,.!\\-\\s]"

.field private static final LOOKAHEAD:Ljava/lang/String; = "(?![^,.!\\-\\s])"

.field private static final LOOKBEHIND:Ljava/lang/String; = "(?<![^,.!\\-\\s])"

.field private static final WORD_BOUNDARY:Ljava/lang/String; = "\\b"

.field private static m_currentOpenPathList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private cdt:Landroid/os/CountDownTimer;

.field private mActivity:Landroid/app/Activity;

.field private mCheckBox:Landroid/widget/CheckBox;

.field private mContentSearchWord:Ljava/lang/String;

.field private mDetector:Landroid/view/GestureDetector;

.field private mDlgSearch:Landroid/app/AlertDialog;

.field private mFindText:Ljava/lang/String;

.field private mGestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

.field mHandler:Landroid/os/Handler;

.field private mIsChangedFontSizePref:Z

.field private mIsChangedTextEncodingPref:Z

.field private mIsContentSearch:Z

.field public mIsMultiWindow:Z

.field private mKeyContoller:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

.field private mMWDnDOperator:Lcom/infraware/common/multiwindow/MWDnDOperator;

.field private mNewOpenHandler:Landroid/os/Handler;

.field private mNfcAdapter:Landroid/nfc/NfcAdapter;

.field private mNfcCallback:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$NfcCallback;

.field private mOfficePrintManager:Lcom/infraware/polarisoffice5/print/OfficePrintManager;

.field private mPageInfoCurrent:Landroid/widget/TextView;

.field private mPageInfoPage:Landroid/widget/LinearLayout;

.field private mPageInfoTotal:Landroid/widget/TextView;

.field private mSBeamEnabled:Z

.field private mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

.field private mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

.field private mSearchGoogleString:Ljava/lang/String;

.field private mTempPreferenceFontSize:I

.field private mTempPreferenceTextEncoding:I

.field private mTouchListener:Landroid/view/View$OnTouchListener;

.field private mUri:Landroid/net/Uri;

.field private m_AvailableCharsetList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

.field private m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

.field private m_Findedt:Ljava/lang/String;

.field private m_FindedtPosition:I

.field private m_Handler:Landroid/os/Handler;

.field private m_Qshandler:Landroid/os/Handler;

.field private m_Qsrunnable:Ljava/lang/Runnable;

.field private m_ScrollThumbHandler:Landroid/os/Handler;

.field private m_ScrollThumbThread:Ljava/lang/Runnable;

.field private m_TTSmanager:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

.field private m_Toast:Landroid/widget/Toast;

.field private m_ViewMode:I

.field private m_ViewRoot:Landroid/widget/LinearLayout;

.field private m_alertDialog:Landroid/app/AlertDialog;

.field private m_bBeforeChange:Z

.field private m_bByMultipleLauncher:Z

.field private m_bEdtFindText:Z

.field private m_bEdtReplaceFindText:Z

.field private m_bEdtReplaceText:Z

.field m_bFindAtLeastOneWord:Z

.field private m_bFindDirect:Z

.field private m_bFindMatchCase:Z

.field private m_bFindSuccess:Z

.field private m_bFindWhole:Z

.field private m_bFind_MachCase:Z

.field private m_bFind_MachWholeWord:Z

.field private m_bIsASCFile:Z

.field private m_bIsOver10mb:Z

.field private m_bLandActionBar:Z

.field private m_bPrintstop:Z

.field private m_bSamsungPrint:Z

.field private m_bSaved:Z

.field private m_bfindStopflag:Z

.field private m_br:Landroid/content/BroadcastReceiver;

.field private m_btnDone:Landroid/widget/ImageView;

.field private m_btnDone_View:Landroid/widget/ImageView;

.field private m_btnFind:Landroid/widget/ImageView;

.field private m_btnFindNextFind:Landroid/widget/ImageView;

.field private m_btnFindNextFind_View:Landroid/widget/ImageView;

.field private m_btnFindOption:Landroid/widget/ImageView;

.field private m_btnFindOption_View:Landroid/widget/ImageView;

.field private m_btnFindPrevFind:Landroid/widget/ImageView;

.field private m_btnFindPrevFind_View:Landroid/widget/ImageView;

.field private m_btnFind_View:Landroid/widget/ImageView;

.field private m_btnFormatIcon:Landroid/widget/ImageView;

.field private m_btnFormatIcon_View:Landroid/widget/ImageView;

.field private m_btnMenu:Landroid/widget/ImageView;

.field private m_btnMenu_View:Landroid/widget/ImageView;

.field private m_btnProperties:Landroid/widget/ImageView;

.field private m_btnProperties_View:Landroid/widget/ImageView;

.field private m_btnRedo:Landroid/widget/ImageView;

.field private m_btnRedo_View:Landroid/widget/ImageView;

.field private m_btnReplace:Landroid/widget/ImageView;

.field private m_btnReplaceAll:Landroid/widget/ImageView;

.field private m_btnReplaceAll_View:Landroid/widget/ImageView;

.field private m_btnReplaceNextFind:Landroid/widget/ImageView;

.field private m_btnReplaceNextFind_View:Landroid/widget/ImageView;

.field private m_btnReplaceOption:Landroid/widget/ImageView;

.field private m_btnReplaceOption_View:Landroid/widget/ImageView;

.field private m_btnReplace_View:Landroid/widget/ImageView;

.field private m_btnUndo:Landroid/widget/ImageView;

.field private m_btnUndo_View:Landroid/widget/ImageView;

.field private m_curcharsetName:Ljava/lang/String;

.field private m_displayType:I

.field private m_edtFind:Landroid/widget/EditText;

.field private m_edtFindDelete:Lcom/infraware/common/control/custom/DeleteImageButton;

.field private m_edtFindDelete_View:Lcom/infraware/common/control/custom/DeleteImageButton;

.field private m_edtFind_View:Landroid/widget/EditText;

.field private m_edtReplace:Landroid/widget/EditText;

.field private m_edtReplaceDelete:Lcom/infraware/common/control/custom/DeleteImageButton;

.field private m_edtReplaceDelete_View:Lcom/infraware/common/control/custom/DeleteImageButton;

.field private m_edtReplaceFind:Landroid/widget/EditText;

.field private m_edtReplaceFindDelete:Lcom/infraware/common/control/custom/DeleteImageButton;

.field private m_edtReplaceFindDelete_View:Lcom/infraware/common/control/custom/DeleteImageButton;

.field private m_edtReplaceFind_View:Landroid/widget/EditText;

.field private m_edtReplace_View:Landroid/widget/EditText;

.field private m_findopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

.field private m_flScrollPos:Landroid/widget/FrameLayout;

.field private m_formaticonMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

.field private m_ibScrollThumb:Landroid/widget/ImageButton;

.field private m_llActionBar:Landroid/widget/LinearLayout;

.field private m_llActionBar_View:Landroid/widget/LinearLayout;

.field private m_llFind:Landroid/widget/LinearLayout;

.field private m_llFindOfReplace:Landroid/widget/LinearLayout;

.field private m_llFindOfReplace_View:Landroid/widget/LinearLayout;

.field private m_llFind_View:Landroid/widget/LinearLayout;

.field private m_llReplace:Landroid/widget/LinearLayout;

.field private m_llReplaceOfReplace:Landroid/widget/LinearLayout;

.field private m_llReplaceOfReplace_View:Landroid/widget/LinearLayout;

.field private m_llReplace_View:Landroid/widget/LinearLayout;

.field private m_llTTS:Landroid/widget/LinearLayout;

.field private m_llTTS_View:Landroid/widget/LinearLayout;

.field private m_llTTSbar:Landroid/widget/LinearLayout;

.field private m_menuPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

.field private m_msg:Landroid/widget/TextView;

.field private m_msg_delaytime:Landroid/widget/TextView;

.field private m_nEncoding:I

.field private m_nFindBeginBlock:I

.field private m_nFindBeginPos:I

.field private m_nFindBlock:I

.field private m_nFindFirstPos:I

.field private m_nLocaleType:I

.field private m_nOrientation:I

.field private m_nQsEditHeight:I

.field private m_nQsPos:I

.field private m_nReplaceCnt:I

.field private m_nScrollThumbPos:I

.field private m_nShowMode:I

.field private m_oInputFilter:Lcom/infraware/polarisoffice5/common/EditInputFilter;

.field private m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

.field private m_optionKeyMenu:Landroid/view/Menu;

.field private m_optionMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

.field private m_printlistener:Lcom/infraware/polarisoffice5/text/manager/PrintManager$PrintManagerListener;

.field private m_printmanager:Lcom/infraware/polarisoffice5/text/manager/PrintManager;

.field private m_printresult:I

.field private m_replaceopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

.field private m_saveBehavior:I

.field private m_sizequickthumbsize:I

.field private m_strFile:Ljava/lang/String;

.field private m_strFilePath:Ljava/lang/String;

.field private m_strFind:Ljava/lang/String;

.field private m_svEdit:Landroid/widget/ScrollView;

.field private m_ttsMode:I

.field private m_ttsbarBtnNext:Landroid/widget/ImageView;

.field private m_ttsbarBtnPlay:Landroid/widget/ImageView;

.field private m_ttsbarBtnPrev:Landroid/widget/ImageView;

.field private m_tvTTSTitle:Landroid/widget/TextView;

.field private m_tvTTSTitle_View:Landroid/widget/TextView;

.field private m_tvTitle:Landroid/widget/TextView;

.field private m_tvTitle_View:Landroid/widget/TextView;

.field private m_viewInScroll:Landroid/view/View;

.field private mbFinishCalled:Z

.field private mbIsTTSPlayFlag:Z

.field onEditTextTouchListener:Landroid/view/View$OnTouchListener;

.field onEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

.field onGoolgeSearchClickListener:Landroid/content/DialogInterface$OnClickListener;

.field onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field onNetWorkUseAlarmClickListener:Landroid/content/DialogInterface$OnClickListener;

.field onRunShareClickListener:Landroid/content/DialogInterface$OnClickListener;

.field onSamsungAppsPolairsOfficeClickListener:Landroid/content/DialogInterface$OnClickListener;

.field onSendEmailAlarmClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private v:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x0

    sput-object v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_currentOpenPathList:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/16 v4, 0xa

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 143
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 153
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bLandActionBar:Z

    .line 156
    iput v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewMode:I

    .line 157
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bSamsungPrint:Z

    .line 158
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_printmanager:Lcom/infraware/polarisoffice5/text/manager/PrintManager;

    .line 159
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_alertDialog:Landroid/app/AlertDialog;

    .line 160
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_printresult:I

    .line 161
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bPrintstop:Z

    .line 253
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ScrollThumbHandler:Landroid/os/Handler;

    .line 254
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ScrollThumbThread:Ljava/lang/Runnable;

    .line 255
    iput v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nScrollThumbPos:I

    .line 256
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Handler:Landroid/os/Handler;

    .line 258
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_menuPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    .line 259
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_optionMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    .line 260
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_formaticonMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    .line 261
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_findopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    .line 262
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_replaceopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    .line 264
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    .line 268
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mDlgSearch:Landroid/app/AlertDialog;

    .line 271
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    .line 272
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFile:Ljava/lang/String;

    .line 273
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Findedt:Ljava/lang/String;

    .line 274
    iput v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_FindedtPosition:I

    .line 275
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bSaved:Z

    .line 276
    iput v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nShowMode:I

    .line 277
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachCase:Z

    .line 278
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachWholeWord:Z

    .line 279
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bEdtReplaceFindText:Z

    .line 280
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bEdtReplaceText:Z

    .line 281
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bEdtFindText:Z

    .line 282
    iput v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nEncoding:I

    .line 283
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindSuccess:Z

    .line 284
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bBeforeChange:Z

    .line 286
    iput v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_saveBehavior:I

    .line 288
    iput v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_displayType:I

    .line 290
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_AvailableCharsetList:Ljava/util/ArrayList;

    .line 292
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    .line 298
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFind:Ljava/lang/String;

    .line 299
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mFindText:Ljava/lang/String;

    .line 303
    iput v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nReplaceCnt:I

    .line 307
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 308
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$NfcCallback;

    .line 309
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mSBeamEnabled:Z

    .line 311
    iput v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nLocaleType:I

    .line 312
    iput v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nOrientation:I

    .line 315
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_optionKeyMenu:Landroid/view/Menu;

    .line 317
    const/16 v0, 0x62

    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_sizequickthumbsize:I

    .line 332
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mPageInfoPage:Landroid/widget/LinearLayout;

    .line 333
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mPageInfoCurrent:Landroid/widget/TextView;

    .line 334
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mPageInfoTotal:Landroid/widget/TextView;

    .line 336
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mGestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    .line 337
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mDetector:Landroid/view/GestureDetector;

    .line 339
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 342
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    .line 345
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bIsOver10mb:Z

    .line 347
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bfindStopflag:Z

    .line 349
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bIsASCFile:Z

    .line 351
    iput v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsMode:I

    .line 353
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_printlistener:Lcom/infraware/polarisoffice5/text/manager/PrintManager$PrintManagerListener;

    .line 355
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Toast:Landroid/widget/Toast;

    .line 357
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    .line 358
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 361
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_msg_delaytime:Landroid/widget/TextView;

    .line 362
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_msg:Landroid/widget/TextView;

    .line 363
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->v:Landroid/view/View;

    .line 377
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_oInputFilter:Lcom/infraware/polarisoffice5/common/EditInputFilter;

    .line 380
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mIsChangedFontSizePref:Z

    .line 381
    iput v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mTempPreferenceFontSize:I

    .line 382
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mIsChangedTextEncodingPref:Z

    .line 383
    iput v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mTempPreferenceTextEncoding:I

    .line 387
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mMWDnDOperator:Lcom/infraware/common/multiwindow/MWDnDOperator;

    .line 394
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mbFinishCalled:Z

    .line 395
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bByMultipleLauncher:Z

    .line 396
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mNewOpenHandler:Landroid/os/Handler;

    .line 399
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mContentSearchWord:Ljava/lang/String;

    .line 400
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mIsContentSearch:Z

    .line 404
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    .line 405
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mUri:Landroid/net/Uri;

    .line 407
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mSearchGoogleString:Ljava/lang/String;

    .line 409
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mIsMultiWindow:Z

    .line 410
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 414
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mbIsTTSPlayFlag:Z

    .line 1861
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$16;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$16;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mHandler:Landroid/os/Handler;

    .line 1993
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$17;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$17;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 1999
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$18;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$18;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onNetWorkUseAlarmClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 2015
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$19;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$19;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onSendEmailAlarmClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 2031
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$20;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$20;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onRunShareClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 2047
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$21;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$21;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onGoolgeSearchClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 3126
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindAtLeastOneWord:Z

    .line 3580
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$23;

    const-wide/16 v2, 0x4e20

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$23;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;JJ)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->cdt:Landroid/os/CountDownTimer;

    .line 3823
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$27;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$27;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onSamsungAppsPolairsOfficeClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 5373
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$36;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$36;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onEditTextTouchListener:Landroid/view/View$OnTouchListener;

    .line 5387
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$37;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$37;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    return-void
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindPrevFind:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindNextFind:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/view/GestureDetector;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mDetector:Landroid/view/GestureDetector;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llFind:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnDone:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTSbar:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/manager/TTSManager;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_TTSmanager:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/common/util/SbeamHelper;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_printresult:I

    return v0
.end method

.method static synthetic access$2002(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    .param p1, "x1"    # I

    .prologue
    .line 143
    iput p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_printresult:I

    return p1
.end method

.method static synthetic access$2100(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bPrintstop:Z

    return v0
.end method

.method static synthetic access$2102(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 143
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bPrintstop:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_alertDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/manager/PrintManager;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_printmanager:Lcom/infraware/polarisoffice5/text/manager/PrintManager;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Lcom/infraware/polarisoffice5/text/manager/PrintManager;)Lcom/infraware/polarisoffice5/text/manager/PrintManager;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    .param p1, "x1"    # Lcom/infraware/polarisoffice5/text/manager/PrintManager;

    .prologue
    .line 143
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_printmanager:Lcom/infraware/polarisoffice5/text/manager/PrintManager;

    return-object p1
.end method

.method static synthetic access$2400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFile:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mFindText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->connectHelpPage()V

    return-void
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->connectOnShare()V

    return-void
.end method

.method static synthetic access$3200(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mSearchGoogleString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 143
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->connectGooleSearch(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$3400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_msg_delaytime:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/os/CountDownTimer;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->cdt:Landroid/os/CountDownTimer;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->connectUriSamungAppsPolarisOffice()V

    return-void
.end method

.method static synthetic access$3700(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ibScrollThumb:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nQsPos:I

    return v0
.end method

.method static synthetic access$3900(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nQsEditHeight:I

    return v0
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bEdtReplaceFindText:Z

    return v0
.end method

.method static synthetic access$4000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_sizequickthumbsize:I

    return v0
.end method

.method static synthetic access$402(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 143
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bEdtReplaceFindText:Z

    return p1
.end method

.method static synthetic access$4100(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->showFileNotFoundDialog()V

    return-void
.end method

.method static synthetic access$4200(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 143
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->hasSpecialWord(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$4300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachWholeWord:Z

    return v0
.end method

.method static synthetic access$4302(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 143
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachWholeWord:Z

    return p1
.end method

.method static synthetic access$4400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachCase:Z

    return v0
.end method

.method static synthetic access$4402(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 143
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachCase:Z

    return p1
.end method

.method static synthetic access$4500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplaceFind:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBlock:I

    return v0
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bEdtReplaceText:Z

    return v0
.end method

.method static synthetic access$5000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplace:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$502(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 143
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bEdtReplaceText:Z

    return p1
.end method

.method static synthetic access$5300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nReplaceCnt:I

    return v0
.end method

.method static synthetic access$5400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginPos:I

    return v0
.end method

.method static synthetic access$5800(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 143
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->saveFile(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplace:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$6200(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mSBeamEnabled:Z

    return v0
.end method

.method static synthetic access$6202(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 143
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mSBeamEnabled:Z

    return p1
.end method

.method static synthetic access$6400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFormatIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$6500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnUndo:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$6600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnRedo:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$6700(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFind:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$6800(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnProperties:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$6900(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnMenu:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceAll:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$7000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindOption:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$7100(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceOption:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceNextFind:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$8800(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nShowMode:I

    return v0
.end method

.method static synthetic access$8900(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    return-object v0
.end method

.method static synthetic access$900(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bEdtFindText:Z

    return v0
.end method

.method static synthetic access$9000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 143
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onMultiWindowStatusChanged(Z)V

    return-void
.end method

.method static synthetic access$902(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 143
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bEdtFindText:Z

    return p1
.end method

.method public static declared-synchronized addOpenPath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 4318
    const-class v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_currentOpenPathList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 4319
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_currentOpenPathList:Ljava/util/ArrayList;

    .line 4321
    :cond_0
    sget-object v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_currentOpenPathList:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 4322
    const/4 v0, 0x1

    .line 4325
    :goto_0
    monitor-exit v1

    return v0

    .line 4324
    :cond_1
    :try_start_1
    sget-object v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_currentOpenPathList:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4325
    const/4 v0, 0x0

    goto :goto_0

    .line 4318
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private asyncLoading()V
    .locals 2

    .prologue
    .line 4107
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Qsrunnable:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 4108
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$29;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$29;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Qsrunnable:Ljava/lang/Runnable;

    .line 4117
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Qshandler:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 4118
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Qshandler:Landroid/os/Handler;

    .line 4122
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->showScrollThumb()V

    .line 4124
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Qshandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Qsrunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 4125
    return-void

    .line 4120
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Qshandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Qsrunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private bufferClear()V
    .locals 4

    .prologue
    .line 581
    new-instance v0, Ljava/io/File;

    sget-object v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->PATH_TEXTBUFFER:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 582
    .local v0, "file1":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 583
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 585
    :cond_0
    new-instance v1, Ljava/io/File;

    sget-object v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->PATH_LINELIST:Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 586
    .local v1, "file2":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 587
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 589
    :cond_1
    new-instance v2, Ljava/io/File;

    sget-object v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->PATH_TEXTBUFFER_TEMP:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 590
    .local v2, "file3":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 591
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 592
    :cond_2
    return-void
.end method

.method private checkASCFile(Ljava/lang/String;)V
    .locals 5
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1232
    const/16 v3, 0x2f

    invoke-virtual {p1, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1233
    .local v1, "fileName":Ljava/lang/String;
    const/16 v3, 0x2e

    invoke-virtual {v1, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 1235
    .local v2, "nIndex":I
    if-gez v2, :cond_1

    .line 1243
    :cond_0
    :goto_0
    return-void

    .line 1237
    :cond_1
    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1239
    .local v0, "ext":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 1241
    const-string/jumbo v3, "asc"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1242
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setIsASCFile(Z)V

    goto :goto_0
.end method

.method private connectGooleSearch(Ljava/lang/String;)V
    .locals 5
    .param p1, "searchStr"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 5556
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v4, "android.intent.action.WEB_SEARCH"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 5557
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 5558
    .local v3, "packageManager":Landroid/content/pm/PackageManager;
    invoke-virtual {v3, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 5559
    .local v0, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    const/4 v2, 0x1

    .line 5561
    .local v2, "isIntentSafe":Z
    :cond_0
    if-eqz v2, :cond_1

    .line 5562
    const-string/jumbo v4, "query"

    invoke-virtual {v1, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5563
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->startActivity(Landroid/content/Intent;)V

    .line 5565
    :cond_1
    return-void
.end method

.method private connectHelpPage()V
    .locals 5

    .prologue
    .line 2068
    invoke-static {p0}, Lcom/infraware/common/notice/NoticeNotifyManager;->getNoticeUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 2069
    .local v1, "strUrl":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 2070
    const v3, 0x7f070031

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2077
    :cond_0
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_WEB_VIEW()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2078
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/infraware/polarisoffice5/OfficeWebView;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2079
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v3, "key_web_title"

    const v4, 0x7f0702bd

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2080
    const-string/jumbo v3, "key_web_url"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2081
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->startActivity(Landroid/content/Intent;)V

    .line 2095
    :goto_0
    return-void

    .line 2084
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.VIEW"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2085
    .restart local v0    # "intent":Landroid/content/Intent;
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2086
    .local v2, "uri":Landroid/net/Uri;
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2087
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private connectOnShare()V
    .locals 5

    .prologue
    .line 5509
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.SEND"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 5510
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "text/plain"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 5512
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 5514
    .local v1, "strSelected":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 5515
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/infraware/common/define/CMModelDefine$Diotek;->isSendTextFileAsExtraText(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5517
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$Diotek;->getExtraAppName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "PolarisOffice"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5518
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$Diotek;->getExtraTextKey()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 5520
    :cond_0
    const-string/jumbo v2, "android.intent.extra.TEXT"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5522
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0702c9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->startActivity(Landroid/content/Intent;)V

    .line 5523
    return-void
.end method

.method private connectUriSamungAppsPolarisOffice()V
    .locals 3

    .prologue
    .line 3840
    const-string/jumbo v1, "samsungapps://ProductDetail/com.infraware.polarisoffice5"

    .line 3842
    .local v1, "string_of_uri":Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 3843
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 3844
    const v2, 0x14000020

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 3847
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 3848
    return-void
.end method

.method private copyFile(Ljava/io/File;Ljava/io/File;)Z
    .locals 26
    .param p1, "sourceFile"    # Ljava/io/File;
    .param p2, "targetFile"    # Ljava/io/File;

    .prologue
    .line 4654
    const/4 v14, 0x0

    .line 4655
    .local v14, "fis":Ljava/io/FileInputStream;
    const/16 v16, 0x0

    .line 4656
    .local v16, "fos":Ljava/io/FileOutputStream;
    const/4 v2, 0x0

    .line 4657
    .local v2, "fcIn":Ljava/nio/channels/FileChannel;
    const/4 v7, 0x0

    .line 4659
    .local v7, "fcOut":Ljava/nio/channels/FileChannel;
    const/high16 v8, 0x10000

    .line 4660
    .local v8, "CHANNEL_BUFFER_SIZE":I
    const/high16 v9, 0x500000

    .line 4662
    .local v9, "HUGE_FILE_SIZE":I
    const/16 v18, 0x0

    .line 4663
    .local v18, "freeBlock":I
    const/4 v10, 0x0

    .line 4665
    .local v10, "blockSize":I
    :try_start_0
    new-instance v21, Landroid/os/StatFs;

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-direct {v0, v5}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 4666
    .local v21, "sf":Landroid/os/StatFs;
    invoke-virtual/range {v21 .. v21}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v18

    .line 4667
    invoke-virtual/range {v21 .. v21}, Landroid/os/StatFs;->getBlockSize()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v10

    .line 4674
    :try_start_1
    new-instance v15, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v15, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4675
    .end local v14    # "fis":Ljava/io/FileInputStream;
    .local v15, "fis":Ljava/io/FileInputStream;
    :try_start_2
    invoke-virtual {v15}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    .line 4676
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v22

    .line 4678
    .local v22, "size":J
    new-instance v17, Ljava/io/FileOutputStream;

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 4679
    .end local v16    # "fos":Ljava/io/FileOutputStream;
    .local v17, "fos":Ljava/io/FileOutputStream;
    :try_start_3
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v7

    .line 4681
    int-to-long v5, v9

    cmp-long v5, v22, v5

    if-lez v5, :cond_5

    const/high16 v5, 0x280000

    int-to-long v5, v5

    add-long v5, v5, v22

    :goto_0
    int-to-long v0, v10

    move-wide/from16 v24, v0

    div-long v19, v5, v24

    .line 4683
    .local v19, "minFree":J
    move/from16 v0, v18

    int-to-long v5, v0

    cmp-long v5, v5, v19

    if-gez v5, :cond_6

    .line 4684
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->delete()Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 4685
    const/4 v5, 0x0

    .line 4700
    if-eqz v2, :cond_0

    .line 4701
    :try_start_4
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 4702
    :cond_0
    if-eqz v15, :cond_1

    .line 4703
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V

    .line 4704
    :cond_1
    if-eqz v7, :cond_2

    .line 4705
    invoke-virtual {v7}, Ljava/nio/channels/FileChannel;->close()V

    .line 4706
    :cond_2
    if-eqz v17, :cond_3

    .line 4707
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_3
    move-object/from16 v16, v17

    .end local v17    # "fos":Ljava/io/FileOutputStream;
    .restart local v16    # "fos":Ljava/io/FileOutputStream;
    move-object v14, v15

    .line 4720
    .end local v15    # "fis":Ljava/io/FileInputStream;
    .end local v19    # "minFree":J
    .end local v21    # "sf":Landroid/os/StatFs;
    .end local v22    # "size":J
    .restart local v14    # "fis":Ljava/io/FileInputStream;
    :cond_4
    :goto_1
    return v5

    .line 4668
    :catch_0
    move-exception v13

    .line 4669
    .local v13, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->delete()Z

    .line 4670
    const/4 v5, 0x0

    goto :goto_1

    .line 4681
    .end local v13    # "e":Ljava/lang/IllegalArgumentException;
    .end local v14    # "fis":Ljava/io/FileInputStream;
    .end local v16    # "fos":Ljava/io/FileOutputStream;
    .restart local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "fos":Ljava/io/FileOutputStream;
    .restart local v21    # "sf":Landroid/os/StatFs;
    .restart local v22    # "size":J
    :cond_5
    const-wide/16 v5, 0x2

    :try_start_5
    div-long v5, v22, v5

    add-long v5, v5, v22

    goto :goto_0

    .line 4708
    .restart local v19    # "minFree":J
    :catch_1
    move-exception v13

    .line 4709
    .local v13, "e":Ljava/io/IOException;
    const/4 v5, 0x0

    move-object/from16 v16, v17

    .end local v17    # "fos":Ljava/io/FileOutputStream;
    .restart local v16    # "fos":Ljava/io/FileOutputStream;
    move-object v14, v15

    .end local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "fis":Ljava/io/FileInputStream;
    goto :goto_1

    .line 4688
    .end local v13    # "e":Ljava/io/IOException;
    .end local v14    # "fis":Ljava/io/FileInputStream;
    .end local v16    # "fos":Ljava/io/FileOutputStream;
    .restart local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "fos":Ljava/io/FileOutputStream;
    :cond_6
    const-wide/16 v3, 0x0

    .line 4689
    .local v3, "position":J
    const-wide/16 v11, 0x0

    .line 4690
    .local v11, "count":J
    :goto_2
    cmp-long v5, v3, v22

    if-gez v5, :cond_7

    .line 4691
    int-to-long v5, v8

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->transferTo(JJLjava/nio/channels/WritableByteChannel;)J
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-result-wide v5

    add-long/2addr v3, v5

    .line 4693
    const-wide/16 v5, 0x1

    add-long/2addr v11, v5

    goto :goto_2

    .line 4700
    :cond_7
    if-eqz v2, :cond_8

    .line 4701
    :try_start_6
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 4702
    :cond_8
    if-eqz v15, :cond_9

    .line 4703
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V

    .line 4704
    :cond_9
    if-eqz v7, :cond_a

    .line 4705
    invoke-virtual {v7}, Ljava/nio/channels/FileChannel;->close()V

    .line 4706
    :cond_a
    if-eqz v17, :cond_b

    .line 4707
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 4716
    :cond_b
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xb

    if-lt v5, v6, :cond_c

    .line 4717
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onMediaDBBroadCast(Ljava/lang/String;)V

    .line 4720
    :cond_c
    const/4 v5, 0x1

    move-object/from16 v16, v17

    .end local v17    # "fos":Ljava/io/FileOutputStream;
    .restart local v16    # "fos":Ljava/io/FileOutputStream;
    move-object v14, v15

    .end local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "fis":Ljava/io/FileInputStream;
    goto :goto_1

    .line 4708
    .end local v14    # "fis":Ljava/io/FileInputStream;
    .end local v16    # "fos":Ljava/io/FileOutputStream;
    .restart local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "fos":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v13

    .line 4709
    .restart local v13    # "e":Ljava/io/IOException;
    const/4 v5, 0x0

    move-object/from16 v16, v17

    .end local v17    # "fos":Ljava/io/FileOutputStream;
    .restart local v16    # "fos":Ljava/io/FileOutputStream;
    move-object v14, v15

    .end local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "fis":Ljava/io/FileInputStream;
    goto :goto_1

    .line 4695
    .end local v3    # "position":J
    .end local v11    # "count":J
    .end local v13    # "e":Ljava/io/IOException;
    .end local v19    # "minFree":J
    .end local v22    # "size":J
    :catch_3
    move-exception v13

    .line 4696
    .local v13, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_7
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 4697
    const/4 v5, 0x0

    .line 4700
    if-eqz v2, :cond_d

    .line 4701
    :try_start_8
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 4702
    :cond_d
    if-eqz v14, :cond_e

    .line 4703
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V

    .line 4704
    :cond_e
    if-eqz v7, :cond_f

    .line 4705
    invoke-virtual {v7}, Ljava/nio/channels/FileChannel;->close()V

    .line 4706
    :cond_f
    if-eqz v16, :cond_4

    .line 4707
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto :goto_1

    .line 4708
    :catch_4
    move-exception v13

    .line 4709
    .local v13, "e":Ljava/io/IOException;
    const/4 v5, 0x0

    goto :goto_1

    .line 4699
    .end local v13    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 4700
    :goto_4
    if-eqz v2, :cond_10

    .line 4701
    :try_start_9
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 4702
    :cond_10
    if-eqz v14, :cond_11

    .line 4703
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V

    .line 4704
    :cond_11
    if-eqz v7, :cond_12

    .line 4705
    invoke-virtual {v7}, Ljava/nio/channels/FileChannel;->close()V

    .line 4706
    :cond_12
    if-eqz v16, :cond_13

    .line 4707
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    .line 4709
    :cond_13
    throw v5

    .line 4708
    :catch_5
    move-exception v13

    .line 4709
    .restart local v13    # "e":Ljava/io/IOException;
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 4699
    .end local v13    # "e":Ljava/io/IOException;
    .end local v14    # "fis":Ljava/io/FileInputStream;
    .restart local v15    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v5

    move-object v14, v15

    .end local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .end local v14    # "fis":Ljava/io/FileInputStream;
    .end local v16    # "fos":Ljava/io/FileOutputStream;
    .restart local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "fos":Ljava/io/FileOutputStream;
    .restart local v22    # "size":J
    :catchall_2
    move-exception v5

    move-object/from16 v16, v17

    .end local v17    # "fos":Ljava/io/FileOutputStream;
    .restart local v16    # "fos":Ljava/io/FileOutputStream;
    move-object v14, v15

    .end local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .line 4695
    .end local v14    # "fis":Ljava/io/FileInputStream;
    .end local v22    # "size":J
    .restart local v15    # "fis":Ljava/io/FileInputStream;
    :catch_6
    move-exception v13

    move-object v14, v15

    .end local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "fis":Ljava/io/FileInputStream;
    goto :goto_3

    .end local v14    # "fis":Ljava/io/FileInputStream;
    .end local v16    # "fos":Ljava/io/FileOutputStream;
    .restart local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "fos":Ljava/io/FileOutputStream;
    .restart local v22    # "size":J
    :catch_7
    move-exception v13

    move-object/from16 v16, v17

    .end local v17    # "fos":Ljava/io/FileOutputStream;
    .restart local v16    # "fos":Ljava/io/FileOutputStream;
    move-object v14, v15

    .end local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "fis":Ljava/io/FileInputStream;
    goto :goto_3
.end method

.method private directChangeEncoding(Landroid/content/Intent;)V
    .locals 3
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 2471
    const-string/jumbo v1, "Encoding"

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 2473
    .local v0, "newEncoding":I
    iget v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nEncoding:I

    if-eq v1, v0, :cond_0

    .line 2474
    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nEncoding:I

    .line 2477
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setCurBlock(I)V

    .line 2478
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    .line 2479
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setLastDisplayPosition()V

    .line 2481
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v1

    const/16 v2, 0x17

    invoke-virtual {v1, p0, v2, v0}, Lcom/infraware/common/config/RuntimeConfig;->setIntPreference(Landroid/content/Context;II)V

    .line 2482
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setBufferManagerReset()V

    .line 2484
    if-nez v0, :cond_1

    .line 2485
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->openFile(Ljava/lang/String;)V

    .line 2491
    :cond_0
    :goto_0
    return-void

    .line 2488
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->directReload(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private fileOperationInfo()V
    .locals 3

    .prologue
    .line 2098
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/infraware/filemanager/FileInfoActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2099
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "key_current_file"

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2100
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->startActivity(Landroid/content/Intent;)V

    .line 2101
    return-void
.end method

.method private findWholeWordIndex(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 7
    .param p1, "strText"    # Ljava/lang/String;
    .param p2, "strFind"    # Ljava/lang/String;
    .param p3, "nCaretPos"    # I

    .prologue
    const/4 v4, -0x1

    .line 2904
    :try_start_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v5

    if-gtz v5, :cond_1

    .line 2922
    :cond_0
    :goto_0
    return v4

    .line 2906
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x0

    invoke-virtual {p2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string/jumbo v5, "\\b"

    :goto_1
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {p2}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {p2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v5

    if-eqz v5, :cond_3

    const-string/jumbo v5, "\\b"

    :goto_2
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2909
    .local v3, "regex":Ljava/lang/String;
    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 2910
    .local v2, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 2912
    .local v1, "matcher":Ljava/util/regex/Matcher;
    if-ltz p3, :cond_0

    .line 2913
    invoke-virtual {v1, p3}, Ljava/util/regex/Matcher;->find(I)Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 2914
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v4

    goto :goto_0

    .line 2906
    .end local v1    # "matcher":Ljava/util/regex/Matcher;
    .end local v2    # "pattern":Ljava/util/regex/Pattern;
    .end local v3    # "regex":Ljava/lang/String;
    :cond_2
    const-string/jumbo v5, "(?<![^,.!\\-\\s])"

    goto :goto_1

    :cond_3
    const-string/jumbo v5, "(?![^,.!\\-\\s])"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 2921
    :catch_0
    move-exception v0

    .line 2922
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method private findWholeWordLastIndex(Ljava/lang/String;Ljava/lang/String;II)I
    .locals 9
    .param p1, "strText"    # Ljava/lang/String;
    .param p2, "strFind"    # Ljava/lang/String;
    .param p3, "nOffset"    # I
    .param p4, "nBeginPos"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v5, -0x1

    .line 2927
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    if-gtz v4, :cond_0

    move v4, v5

    .line 2958
    :goto_0
    return v4

    .line 2929
    :cond_0
    if-gez p4, :cond_1

    move v4, v5

    .line 2930
    goto :goto_0

    .line 2931
    :cond_1
    if-ne p3, v5, :cond_2

    move v4, p3

    .line 2932
    goto :goto_0

    .line 2933
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-lt p4, v4, :cond_3

    .line 2934
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 p4, v4, -0x1

    .line 2936
    :cond_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v7}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string/jumbo v4, "\\b"

    :goto_1
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p2}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {p2, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string/jumbo v4, "\\b"

    :goto_2
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2939
    .local v3, "regex":Ljava/lang/String;
    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 2940
    .local v2, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {p1, v7, p4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 2942
    .local v1, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v1, p3}, Ljava/util/regex/Matcher;->find(I)Z

    move-result v4

    if-ne v4, v8, :cond_6

    .line 2943
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v4

    goto :goto_0

    .line 2936
    .end local v1    # "matcher":Ljava/util/regex/Matcher;
    .end local v2    # "pattern":Ljava/util/regex/Pattern;
    .end local v3    # "regex":Ljava/lang/String;
    :cond_4
    const-string/jumbo v4, "(?<![^,.!\\-\\s])"

    goto :goto_1

    :cond_5
    const-string/jumbo v4, "(?![^,.!\\-\\s])"

    goto :goto_2

    .line 2945
    .restart local v1    # "matcher":Ljava/util/regex/Matcher;
    .restart local v2    # "pattern":Ljava/util/regex/Pattern;
    .restart local v3    # "regex":Ljava/lang/String;
    :cond_6
    if-eq p3, v5, :cond_7

    .line 2946
    add-int/lit8 v4, p3, -0x1

    invoke-virtual {p1, p2, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;I)I

    move-result p3

    .line 2947
    if-eq p3, v5, :cond_6

    .line 2949
    :try_start_0
    invoke-virtual {v1, p3}, Ljava/util/regex/Matcher;->find(I)Z

    move-result v4

    if-ne v4, v8, :cond_6

    .line 2950
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    goto/16 :goto_0

    .line 2951
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    move v4, v5

    .line 2952
    goto/16 :goto_0

    .end local v0    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_7
    move v4, v5

    .line 2958
    goto/16 :goto_0
.end method

.method private getCharset([BI)Ljava/lang/String;
    .locals 5
    .param p1, "bytes"    # [B
    .param p2, "nEncoding"    # I

    .prologue
    .line 2216
    const-string/jumbo v2, ""

    .line 2218
    .local v2, "strCharset":Ljava/lang/String;
    if-eqz p2, :cond_1

    .line 2219
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_AvailableCharsetList:Ljava/util/ArrayList;

    add-int/lit8 v4, p2, -0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "strCharset":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 2240
    .restart local v2    # "strCharset":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v2

    .line 2221
    :cond_1
    array-length v3, p1

    const/4 v4, 0x2

    if-lt v3, v4, :cond_2

    .line 2222
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getNoPrefixUnicodeCharset([B)Ljava/lang/String;

    move-result-object v2

    .line 2226
    :cond_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2227
    new-instance v0, Lcom/infraware/common/util/text/CharsetDetector;

    invoke-direct {v0, p0}, Lcom/infraware/common/util/text/CharsetDetector;-><init>(Landroid/content/Context;)V

    .line 2228
    .local v0, "detector":Lcom/infraware/common/util/text/CharsetDetector;
    invoke-virtual {v0, p1}, Lcom/infraware/common/util/text/CharsetDetector;->setText([B)Lcom/infraware/common/util/text/CharsetDetector;

    .line 2229
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/infraware/common/util/text/CharsetDetector;->setDefaultLanguage(Ljava/lang/String;)V

    .line 2230
    invoke-virtual {v0}, Lcom/infraware/common/util/text/CharsetDetector;->detect()Lcom/infraware/common/util/text/CharsetMatch;

    move-result-object v1

    .line 2231
    .local v1, "match":Lcom/infraware/common/util/text/CharsetMatch;
    invoke-virtual {v1}, Lcom/infraware/common/util/text/CharsetMatch;->getName()Ljava/lang/String;

    move-result-object v2

    .line 2236
    .end local v0    # "detector":Lcom/infraware/common/util/text/CharsetDetector;
    .end local v1    # "match":Lcom/infraware/common/util/text/CharsetMatch;
    :cond_3
    invoke-static {v2}, Ljava/nio/charset/Charset;->isSupported(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2237
    const-string/jumbo v2, "UTF-8"

    goto :goto_0
.end method

.method private getNoPrefixUnicodeCharset([B)Ljava/lang/String;
    .locals 4
    .param p1, "bytes"    # [B

    .prologue
    .line 2244
    const-string/jumbo v2, "UTF-16BE"

    .line 2245
    .local v2, "strUnicodeCharset":Ljava/lang/String;
    const/4 v0, 0x1

    .line 2247
    .local v0, "bUnicode":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p1

    if-ge v1, v3, :cond_0

    .line 2248
    aget-byte v3, p1, v1

    if-eqz v3, :cond_1

    .line 2249
    const/4 v0, 0x0

    .line 2254
    :cond_0
    if-eqz v0, :cond_2

    move-object v3, v2

    .line 2269
    :goto_1
    return-object v3

    .line 2247
    :cond_1
    add-int/lit8 v1, v1, 0x2

    goto :goto_0

    .line 2257
    :cond_2
    const-string/jumbo v2, "UTF-16LE"

    .line 2258
    const/4 v0, 0x1

    .line 2259
    const/4 v1, 0x1

    :goto_2
    array-length v3, p1

    if-ge v1, v3, :cond_3

    .line 2260
    aget-byte v3, p1, v1

    if-eqz v3, :cond_4

    .line 2261
    const/4 v0, 0x0

    .line 2266
    :cond_3
    if-eqz v0, :cond_5

    move-object v3, v2

    .line 2267
    goto :goto_1

    .line 2259
    :cond_4
    add-int/lit8 v1, v1, 0x2

    goto :goto_2

    .line 2269
    :cond_5
    const-string/jumbo v3, ""

    goto :goto_1
.end method

.method private getSdcardIntentFilter()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 1692
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1693
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1694
    const-string/jumbo v1, "android.intent.action.MEDIA_SHARED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1695
    const-string/jumbo v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1696
    const-string/jumbo v1, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1697
    const-string/jumbo v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1698
    const-string/jumbo v1, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1699
    const-string/jumbo v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1700
    const-string/jumbo v1, "android.intent.action.MEDIA_NOFS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1701
    const-string/jumbo v1, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1702
    const-string/jumbo v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 1703
    return-object v0
.end method

.method private hasSpecialWord(Ljava/lang/String;)Z
    .locals 6
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2962
    const/16 v4, 0xb

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v4, "!"

    aput-object v4, v1, v3

    const-string/jumbo v4, "@"

    aput-object v4, v1, v2

    const/4 v4, 0x2

    const-string/jumbo v5, "#"

    aput-object v5, v1, v4

    const/4 v4, 0x3

    const-string/jumbo v5, "$"

    aput-object v5, v1, v4

    const/4 v4, 0x4

    const-string/jumbo v5, "%"

    aput-object v5, v1, v4

    const/4 v4, 0x5

    const-string/jumbo v5, "^"

    aput-object v5, v1, v4

    const/4 v4, 0x6

    const-string/jumbo v5, "&"

    aput-object v5, v1, v4

    const/4 v4, 0x7

    const-string/jumbo v5, "*"

    aput-object v5, v1, v4

    const/16 v4, 0x8

    const-string/jumbo v5, "("

    aput-object v5, v1, v4

    const/16 v4, 0x9

    const-string/jumbo v5, ")"

    aput-object v5, v1, v4

    const/16 v4, 0xa

    const-string/jumbo v5, "?"

    aput-object v5, v1, v4

    .line 2963
    .local v1, "sWP":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v1

    if-ge v0, v4, :cond_1

    .line 2964
    aget-object v4, v1, v0

    invoke-virtual {p1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2968
    :goto_1
    return v2

    .line 2963
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v2, v3

    .line 2968
    goto :goto_1
.end method

.method private initControls()V
    .locals 10

    .prologue
    const v9, 0x7f0b0058

    const/4 v8, -0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1007
    const v3, 0x7f0b0248

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    .line 1010
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    const v4, 0x7f0b0230

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar_View:Landroid/widget/LinearLayout;

    .line 1013
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    const v4, 0x7f0b024f

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llFind_View:Landroid/widget/LinearLayout;

    .line 1015
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    const v4, 0x7f0b0255

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llReplace_View:Landroid/widget/LinearLayout;

    .line 1017
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    const v4, 0x7f0b0256

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llFindOfReplace_View:Landroid/widget/LinearLayout;

    .line 1019
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    const v4, 0x7f0b025b

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llReplaceOfReplace_View:Landroid/widget/LinearLayout;

    .line 1021
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    const v4, 0x7f0b0260

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTS_View:Landroid/widget/LinearLayout;

    .line 1024
    const v3, 0x7f0b0261

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .line 1025
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3, p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->initialize(Landroid/app/Activity;)V

    .line 1026
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->newInfo()V

    .line 1027
    invoke-static {}, Lcom/infraware/common/multiwindow/SFeatureWrapper;->getInstance()Lcom/infraware/common/multiwindow/SFeatureWrapper;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3, v4}, Lcom/infraware/common/multiwindow/SFeatureWrapper;->turnOffWritingBuddy(Landroid/widget/EditText;)V

    .line 1030
    invoke-static {}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getInstance()Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->isMultiWindowSupported(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1031
    new-instance v3, Lcom/infraware/common/multiwindow/MWDnDOperator;

    invoke-direct {v3, p0}, Lcom/infraware/common/multiwindow/MWDnDOperator;-><init>(Landroid/app/Activity;)V

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mMWDnDOperator:Lcom/infraware/common/multiwindow/MWDnDOperator;

    .line 1032
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    new-instance v4, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$3;

    invoke-direct {v4, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$3;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 1050
    :cond_0
    invoke-static {p0}, Lcom/infraware/common/define/CMModelDefine$Diotek;->USE_DICTIONARY_SEARCH(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1052
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3, p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setActivityForDicSearch(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    move-result-object v3

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    .line 1053
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    const v5, 0x422aae14    # 42.67f

    invoke-virtual {v4, v5}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->dipToPx(F)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setActionbarHeight(I)V

    .line 1055
    :cond_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_tvTitle_View:Landroid/widget/TextView;

    .line 1056
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    const v4, 0x7f0b024a

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnUndo_View:Landroid/widget/ImageView;

    .line 1057
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    const v4, 0x7f0b024b

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnRedo_View:Landroid/widget/ImageView;

    .line 1059
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnRedo_View:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1061
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    const v4, 0x7f0b024c

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFind_View:Landroid/widget/ImageView;

    .line 1062
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    const v4, 0x7f0b024d

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnProperties_View:Landroid/widget/ImageView;

    .line 1063
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    const v4, 0x7f0b024e

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnMenu_View:Landroid/widget/ImageView;

    .line 1065
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    const v4, 0x7f0b0249

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFormatIcon_View:Landroid/widget/ImageView;

    .line 1079
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTS_View:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_tvTTSTitle_View:Landroid/widget/TextView;

    .line 1082
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    const v4, 0x7f0b0250

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindOption_View:Landroid/widget/ImageView;

    .line 1085
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    const v4, 0x7f0b0253

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind_View:Landroid/widget/EditText;

    .line 1088
    new-instance v3, Lcom/infraware/polarisoffice5/common/EditInputFilter;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/infraware/polarisoffice5/common/EditInputFilter;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_oInputFilter:Lcom/infraware/polarisoffice5/common/EditInputFilter;

    .line 1089
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_oInputFilter:Lcom/infraware/polarisoffice5/common/EditInputFilter;

    const/16 v4, 0x28

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/common/EditInputFilter;->setMaxLength(I)V

    .line 1090
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind_View:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_oInputFilter:Lcom/infraware/polarisoffice5/common/EditInputFilter;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/common/EditInputFilter;->getFilters()[Landroid/text/InputFilter;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 1092
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    const v4, 0x7f0b0254

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/infraware/common/control/custom/DeleteImageButton;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFindDelete_View:Lcom/infraware/common/control/custom/DeleteImageButton;

    .line 1093
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFindDelete_View:Lcom/infraware/common/control/custom/DeleteImageButton;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind_View:Landroid/widget/EditText;

    invoke-virtual {v3, v4}, Lcom/infraware/common/control/custom/DeleteImageButton;->init(Landroid/widget/EditText;)V

    .line 1096
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    const v4, 0x7f0b0252

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindPrevFind_View:Landroid/widget/ImageView;

    .line 1098
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindPrevFind_View:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1099
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindPrevFind_View:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 1103
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    const v4, 0x7f0b0251

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindNextFind_View:Landroid/widget/ImageView;

    .line 1105
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindNextFind_View:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1106
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindNextFind_View:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 1110
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    const v4, 0x7f0b0257

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceOption_View:Landroid/widget/ImageView;

    .line 1113
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    const v4, 0x7f0b0259

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplaceFind_View:Landroid/widget/EditText;

    .line 1114
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    const v4, 0x7f0b025a

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/infraware/common/control/custom/DeleteImageButton;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplaceFindDelete_View:Lcom/infraware/common/control/custom/DeleteImageButton;

    .line 1115
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplaceFindDelete_View:Lcom/infraware/common/control/custom/DeleteImageButton;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplaceFind_View:Landroid/widget/EditText;

    invoke-virtual {v3, v4}, Lcom/infraware/common/control/custom/DeleteImageButton;->init(Landroid/widget/EditText;)V

    .line 1118
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    const v4, 0x7f0b0258

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceNextFind_View:Landroid/widget/ImageView;

    .line 1120
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceNextFind_View:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1122
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    const v4, 0x7f0b025c

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplace_View:Landroid/widget/ImageView;

    .line 1124
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplace_View:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1127
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    const v4, 0x7f0b025e

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplace_View:Landroid/widget/EditText;

    .line 1128
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    const v4, 0x7f0b025f

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/infraware/common/control/custom/DeleteImageButton;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplaceDelete_View:Lcom/infraware/common/control/custom/DeleteImageButton;

    .line 1129
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplaceDelete_View:Lcom/infraware/common/control/custom/DeleteImageButton;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplace_View:Landroid/widget/EditText;

    invoke-virtual {v3, v4}, Lcom/infraware/common/control/custom/DeleteImageButton;->init(Landroid/widget/EditText;)V

    .line 1132
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    const v4, 0x7f0b025d

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceAll_View:Landroid/widget/ImageView;

    .line 1134
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceAll_View:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1137
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewRoot:Landroid/widget/LinearLayout;

    const v4, 0x7f0b0231

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnDone_View:Landroid/widget/ImageView;

    .line 1140
    const v3, 0x7f0b0262

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ScrollView;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_svEdit:Landroid/widget/ScrollView;

    .line 1141
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v8, v8}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1145
    .local v2, "svLp":Landroid/view/ViewGroup$LayoutParams;
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1146
    .local v1, "llScroll":Landroid/widget/LinearLayout;
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1147
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_svEdit:Landroid/widget/ScrollView;

    invoke-virtual {v3, v1}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 1148
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_svEdit:Landroid/widget/ScrollView;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 1149
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_svEdit:Landroid/widget/ScrollView;

    invoke-virtual {v3, v6}, Landroid/widget/ScrollView;->setFocusable(Z)V

    .line 1151
    new-instance v3, Landroid/view/View;

    invoke-direct {v3, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_viewInScroll:Landroid/view/View;

    .line 1152
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_viewInScroll:Landroid/view/View;

    const v4, 0xf3f3f3

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1153
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v3, 0x8

    invoke-direct {v0, v3, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1156
    .local v0, "llLp":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_viewInScroll:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1157
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_viewInScroll:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1159
    new-instance v3, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-direct {v3, p0, v6}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;I)V

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_optionMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    .line 1160
    new-instance v3, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-direct {v3, p0, v7}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;I)V

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_formaticonMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    .line 1161
    new-instance v3, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    const/4 v4, 0x2

    invoke-direct {v3, p0, v4}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;I)V

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_findopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    .line 1162
    new-instance v3, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    const/4 v4, 0x3

    invoke-direct {v3, p0, v4}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;I)V

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_replaceopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    .line 1164
    const v3, 0x7f0b0263

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_flScrollPos:Landroid/widget/FrameLayout;

    .line 1165
    const v3, 0x7f0b0264

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ibScrollThumb:Landroid/widget/ImageButton;

    .line 1166
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Handler:Landroid/os/Handler;

    .line 1168
    new-instance v3, Lcom/infraware/polarisoffice5/text/manager/PrintManager;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-direct {v3, v4, v7}, Lcom/infraware/polarisoffice5/text/manager/PrintManager;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;I)V

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_printmanager:Lcom/infraware/polarisoffice5/text/manager/PrintManager;

    .line 1169
    new-instance v3, Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_TTSmanager:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    .line 1171
    const v3, 0x7f0b0265

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mPageInfoPage:Landroid/widget/LinearLayout;

    .line 1172
    const v3, 0x7f0b0266

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mPageInfoCurrent:Landroid/widget/TextView;

    .line 1173
    const v3, 0x7f0b0267

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mPageInfoTotal:Landroid/widget/TextView;

    .line 1175
    const v3, 0x7f0b0268

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTSbar:Landroid/widget/LinearLayout;

    .line 1179
    const v3, 0x7f0b0269

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnPrev:Landroid/widget/ImageView;

    .line 1180
    const v3, 0x7f0b026a

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnPlay:Landroid/widget/ImageView;

    .line 1181
    const v3, 0x7f0b026b

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnNext:Landroid/widget/ImageView;

    .line 1182
    invoke-static {p0}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HOVERING(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1184
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnPrev:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 1185
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnPlay:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 1186
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnNext:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 1189
    :cond_2
    invoke-static {}, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->getInsTance()Lcom/infraware/polarisoffice5/print/OfficePrintManager;

    move-result-object v3

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mOfficePrintManager:Lcom/infraware/polarisoffice5/print/OfficePrintManager;

    .line 1190
    return-void
.end method

.method private loadAvailableCharsetList()V
    .locals 4

    .prologue
    .line 995
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_AvailableCharsetList:Ljava/util/ArrayList;

    .line 997
    invoke-static {}, Lcom/infraware/common/util/text/CharsetDetector;->getAllDetectableCharsets()[Ljava/lang/String;

    move-result-object v0

    .line 998
    .local v0, "encodingItems":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 999
    aget-object v2, v0, v1

    invoke-static {v2}, Ljava/nio/charset/Charset;->isSupported(Ljava/lang/String;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 1000
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_AvailableCharsetList:Ljava/util/ArrayList;

    aget-object v3, v0, v1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 998
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1002
    :cond_1
    return-void
.end method

.method private loadEditConfig()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1269
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v2

    const/16 v3, 0x15

    const/16 v4, 0xa

    invoke-virtual {v2, p0, v3, v4}, Lcom/infraware/common/config/RuntimeConfig;->getIntPreference(Landroid/content/Context;II)I

    move-result v0

    .line 1270
    .local v0, "nFontSize":I
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v2

    const/16 v3, 0x16

    invoke-virtual {v2, p0, v3, v5}, Lcom/infraware/common/config/RuntimeConfig;->getIntPreference(Landroid/content/Context;II)I

    move-result v1

    .line 1272
    .local v1, "nTheme":I
    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mTempPreferenceFontSize:I

    .line 1273
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v2

    const/16 v3, 0x17

    invoke-virtual {v2, p0, v3, v5}, Lcom/infraware/common/config/RuntimeConfig;->getIntPreference(Landroid/content/Context;II)I

    move-result v2

    iput v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mTempPreferenceTextEncoding:I

    .line 1276
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setFontSize(I)V

    .line 1277
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setFontPoolSync(I)V

    .line 1278
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setFontColor(I)V

    .line 1279
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setBackgroundTheme(I)V

    .line 1280
    return-void
.end method

.method private makeBackupFile()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 4632
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v6

    const/16 v7, 0xc

    invoke-virtual {v6, p0, v7, v8}, Lcom/infraware/common/config/RuntimeConfig;->getBooleanPreference(Landroid/content/Context;IZ)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 4635
    new-instance v1, Ljava/io/File;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 4638
    .local v1, "sourceFile":Ljava/io/File;
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    const/16 v7, 0x2e

    invoke-virtual {v6, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 4639
    .local v0, "nIndex":I
    if-lez v0, :cond_1

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x6

    if-le v0, v6, :cond_1

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-eq v0, v6, :cond_1

    .line 4641
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    invoke-virtual {v6, v8, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 4642
    .local v5, "tempName":Ljava/lang/String;
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    add-int/lit8 v7, v0, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 4643
    .local v4, "tempExt":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "_backup."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4647
    .end local v4    # "tempExt":Ljava/lang/String;
    .end local v5    # "tempName":Ljava/lang/String;
    .local v3, "targetFileName":Ljava/lang/String;
    :goto_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 4649
    .local v2, "targetFile":Ljava/io/File;
    invoke-direct {p0, v1, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->copyFile(Ljava/io/File;Ljava/io/File;)Z

    .line 4651
    .end local v0    # "nIndex":I
    .end local v1    # "sourceFile":Ljava/io/File;
    .end local v2    # "targetFile":Ljava/io/File;
    .end local v3    # "targetFileName":Ljava/lang/String;
    :cond_0
    return-void

    .line 4645
    .restart local v0    # "nIndex":I
    .restart local v1    # "sourceFile":Ljava/io/File;
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ".bak"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "targetFileName":Ljava/lang/String;
    goto :goto_0
.end method

.method private moveThumbByScroll()V
    .locals 5

    .prologue
    .line 4132
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getHeight()I

    move-result v0

    .line 4133
    .local v0, "nEditHeight":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getScrollPos()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getScrollHeight()I

    move-result v4

    sub-int/2addr v4, v0

    int-to-float v4, v4

    div-float v1, v3, v4

    .line 4134
    .local v1, "nRatio":F
    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v3, v1, v3

    if-lez v3, :cond_0

    .line 4135
    const/high16 v1, 0x3f800000    # 1.0f

    .line 4138
    :cond_0
    iget v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_sizequickthumbsize:I

    sub-int v3, v0, v3

    int-to-float v3, v3

    mul-float/2addr v3, v1

    float-to-int v3, v3

    iput v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nQsPos:I

    .line 4141
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_flScrollPos:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 4142
    .local v2, "params":Landroid/widget/LinearLayout$LayoutParams;
    iget v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nQsPos:I

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 4143
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_flScrollPos:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4144
    return-void
.end method

.method private onCreateOnKKOver()V
    .locals 2

    .prologue
    .line 5569
    new-instance v0, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 5571
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$38;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$38;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->setStateChangeListener(Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;)Z

    .line 5588
    return-void
.end method

.method private onHelp()V
    .locals 11

    .prologue
    const/4 v6, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 1952
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    const/16 v4, 0x130

    invoke-static {v3, v4}, Lcom/infraware/common/define/CMModelDefine;->isShowConnetNetWorkPopup(Landroid/content/Context;I)Z

    move-result v3

    if-ne v3, v8, :cond_2

    .line 1955
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1956
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030006

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1958
    .local v1, "networkView":Landroid/view/View;
    const v3, 0x7f0b003b

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1959
    .local v2, "tv":Landroid/widget/TextView;
    const v3, 0x7f0b003c

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mCheckBox:Landroid/widget/CheckBox;

    .line 1960
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/infraware/common/util/Utils;->getIsConnectWlan(Landroid/app/Activity;)Z

    move-result v3

    if-ne v3, v8, :cond_0

    .line 1961
    const v3, 0x7f07005d

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1962
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_WLAN_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onNetWorkUseAlarmClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v7, v6, v8

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v7, v6, v10

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    .line 1979
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    .end local v1    # "networkView":Landroid/view/View;
    .end local v2    # "tv":Landroid/widget/TextView;
    :goto_0
    return-void

    .line 1967
    .restart local v0    # "inflater":Landroid/view/LayoutInflater;
    .restart local v1    # "networkView":Landroid/view/View;
    .restart local v2    # "tv":Landroid/widget/TextView;
    :cond_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/infraware/common/util/Utils;->getIsConnectMobileNetwork(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1968
    const v3, 0x7f07005b

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1969
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_MOBILE_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onNetWorkUseAlarmClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v7, v6, v8

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v7, v6, v10

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1973
    :cond_1
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_NETWORK_NOT_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1977
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    .end local v1    # "networkView":Landroid/view/View;
    .end local v2    # "tv":Landroid/widget/TextView;
    :cond_2
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->connectHelpPage()V

    goto :goto_0
.end method

.method private onMediaDBBroadCast(Ljava/lang/String;)V
    .locals 5
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 3422
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 3423
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "file://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 3425
    return-void
.end method

.method private onMultiWindowStatusChanged(Z)V
    .locals 4
    .param p1, "mIsMaximized"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 5596
    invoke-static {}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getInstance()Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->isNormalWindow(Landroid/app/Activity;)Z

    move-result v3

    if-nez v3, :cond_1

    move v0, v1

    .line 5597
    .local v0, "isMW":Z
    :goto_0
    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mIsMultiWindow:Z

    if-ne v3, v0, :cond_2

    .line 5598
    const-string/jumbo v1, "onMultiWindowStatusChanged"

    const-string/jumbo v2, "Even"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 5610
    :cond_0
    :goto_1
    return-void

    .end local v0    # "isMW":Z
    :cond_1
    move v0, v2

    .line 5596
    goto :goto_0

    .line 5601
    .restart local v0    # "isMW":Z
    :cond_2
    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mIsMultiWindow:Z

    if-nez v3, :cond_3

    if-eqz v0, :cond_3

    .line 5602
    const-string/jumbo v2, "onMultiWindowStatusChanged"

    const-string/jumbo v3, "Normal -> Multi"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 5603
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onChangedFromNormalWindowToMultiWindow()V

    .line 5604
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mIsMultiWindow:Z

    goto :goto_1

    .line 5605
    :cond_3
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mIsMultiWindow:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 5606
    const-string/jumbo v1, "onMultiWindowStatusChanged"

    const-string/jumbo v3, "Multi -> Normal"

    invoke-static {v1, v3}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 5607
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onChangedFromMultiwindowToNormalWindow()V

    .line 5608
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mIsMultiWindow:Z

    goto :goto_1
.end method

.method private onOrientationChanged()V
    .locals 5

    .prologue
    const/high16 v4, 0x42400000    # 48.0f

    const/high16 v3, 0x42200000    # 40.0f

    .line 4849
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nOrientation:I

    .line 4853
    iget v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nOrientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 4855
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 4856
    .local v0, "linearLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    invoke-static {p0, v4}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 4857
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4859
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llFind:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "linearLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 4860
    .restart local v0    # "linearLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    invoke-static {p0, v4}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 4861
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llFind:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4863
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llReplace:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "linearLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 4864
    .restart local v0    # "linearLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v1, 0x42c00000    # 96.0f

    invoke-static {p0, v1}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 4865
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llReplace:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4867
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llFindOfReplace:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "linearLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 4868
    .restart local v0    # "linearLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    invoke-static {p0, v4}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 4869
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llFindOfReplace:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4871
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llReplaceOfReplace:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "linearLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 4872
    .restart local v0    # "linearLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    invoke-static {p0, v4}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 4873
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llReplaceOfReplace:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4875
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTS:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "linearLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 4876
    .restart local v0    # "linearLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    invoke-static {p0, v4}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 4877
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTS:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4905
    :goto_0
    return-void

    .line 4881
    .end local v0    # "linearLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 4882
    .restart local v0    # "linearLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    invoke-static {p0, v3}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 4883
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4885
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llFind:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "linearLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 4886
    .restart local v0    # "linearLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    invoke-static {p0, v3}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 4887
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llFind:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4889
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llReplace:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "linearLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 4890
    .restart local v0    # "linearLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v1, 0x42a00000    # 80.0f

    invoke-static {p0, v1}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 4891
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llReplace:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4893
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llFindOfReplace:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "linearLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 4894
    .restart local v0    # "linearLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    invoke-static {p0, v3}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 4895
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llFindOfReplace:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4897
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llReplaceOfReplace:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "linearLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 4898
    .restart local v0    # "linearLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    invoke-static {p0, v3}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 4899
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llReplaceOfReplace:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4901
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTS:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "linearLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 4902
    .restart local v0    # "linearLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    invoke-static {p0, v3}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 4903
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTS:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private onPrint()V
    .locals 4

    .prologue
    .line 3594
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_optionMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->dismissPopupWindow()Z

    .line 3595
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_formaticonMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->dismissPopupWindow()Z

    .line 3597
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->printDialogInflater()V

    .line 3599
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_msg_delaytime:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070301

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070302

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3602
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_msg_delaytime:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3604
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_msg:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701c3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3606
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v1

    invoke-direct {v0, p0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701ad

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->v:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07005f

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    new-instance v2, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$24;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$24;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_alertDialog:Landroid/app/AlertDialog;

    .line 3618
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_alertDialog:Landroid/app/AlertDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 3620
    :try_start_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_alertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3624
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_alertDialog:Landroid/app/AlertDialog;

    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$25;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$25;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 3633
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$26;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$26;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$26;->start()V

    .line 3644
    return-void

    .line 3621
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private onSendEmail()V
    .locals 13

    .prologue
    const/4 v8, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 3940
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isChanged()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 3941
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->showNeedSaveFileMsg()V

    .line 3981
    :goto_0
    return-void

    .line 3945
    :cond_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isNewFile()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 3946
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->showNeedSaveFileMsg()V

    goto :goto_0

    .line 3950
    :cond_1
    new-instance v0, Ljava/io/File;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3952
    .local v0, "f":Ljava/io/File;
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    .line 3954
    .local v4, "uri":Landroid/net/Uri;
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    const/16 v6, 0x131

    invoke-static {v5, v6}, Lcom/infraware/common/define/CMModelDefine;->isShowConnetNetWorkPopup(Landroid/content/Context;I)Z

    move-result v5

    if-ne v5, v10, :cond_4

    .line 3956
    iput-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mUri:Landroid/net/Uri;

    .line 3957
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    const-string/jumbo v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 3958
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f030006

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 3960
    .local v2, "networkView":Landroid/view/View;
    const v5, 0x7f0b003b

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 3961
    .local v3, "tv":Landroid/widget/TextView;
    const v5, 0x7f0b003c

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    iput-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mCheckBox:Landroid/widget/CheckBox;

    .line 3962
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->getIsConnectWlan(Landroid/app/Activity;)Z

    move-result v5

    if-ne v5, v10, :cond_2

    .line 3963
    const v5, 0x7f07005d

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(I)V

    .line 3964
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    sget-object v7, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_WLAN_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v8, v8, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    aput-object v9, v8, v11

    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onSendEmailAlarmClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v9, v8, v10

    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v9, v8, v12

    invoke-virtual {v5, v6, v7, v8}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 3969
    :cond_2
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->getIsConnectMobileNetwork(Landroid/app/Activity;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 3970
    const v5, 0x7f07005b

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(I)V

    .line 3971
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    sget-object v7, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_MOBILE_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v8, v8, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    aput-object v9, v8, v11

    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onSendEmailAlarmClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v9, v8, v10

    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v9, v8, v12

    invoke-virtual {v5, v6, v7, v8}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 3975
    :cond_3
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    sget-object v7, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_NETWORK_NOT_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v8, v11, [Ljava/lang/Object;

    invoke-virtual {v5, v6, v7, v8}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 3979
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    .end local v2    # "networkView":Landroid/view/View;
    .end local v3    # "tv":Landroid/widget/TextView;
    :cond_4
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->exportEmail(Landroid/net/Uri;)V

    goto/16 :goto_0
.end method

.method private openFile(Ljava/lang/String;)V
    .locals 18
    .param p1, "strFileName"    # Ljava/lang/String;

    .prologue
    .line 2611
    :try_start_0
    new-instance v5, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2612
    .local v5, "file":Ljava/io/File;
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 2614
    .local v6, "in":Ljava/io/InputStream;
    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v13

    if-eqz v13, :cond_1

    .line 2615
    new-instance v13, Ljava/io/IOException;

    invoke-direct {v13}, Ljava/io/IOException;-><init>()V

    throw v13
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 2657
    .end local v5    # "file":Ljava/io/File;
    .end local v6    # "in":Ljava/io/InputStream;
    :catch_0
    move-exception v4

    .line 2659
    .local v4, "e":Ljava/io/FileNotFoundException;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setIsNewFile(Z)V

    .line 2660
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v13

    const-string/jumbo v14, "INTCMD"

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 2661
    .local v2, "OpenType":I
    const/4 v13, 0x1

    if-ne v2, v13, :cond_7

    .line 2662
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    .line 2663
    .local v7, "intent":Landroid/content/Intent;
    const-string/jumbo v13, "key_new_file"

    invoke-virtual {v7, v13}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/infraware/common/util/FileUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFile:Ljava/lang/String;

    .line 2664
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFile:Ljava/lang/String;

    if-eqz v13, :cond_0

    .line 2665
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setTitleText()V

    .line 2678
    .end local v2    # "OpenType":I
    .end local v4    # "e":Ljava/io/FileNotFoundException;
    .end local v7    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 2617
    .restart local v5    # "file":Ljava/io/File;
    .restart local v6    # "in":Ljava/io/InputStream;
    :cond_1
    :try_start_1
    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v13

    const-wide/32 v15, 0x7fffffff

    cmp-long v13, v13, v15

    if-lez v13, :cond_2

    .line 2618
    new-instance v13, Ljava/lang/Exception;

    invoke-direct {v13}, Ljava/lang/Exception;-><init>()V

    throw v13
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 2671
    .end local v5    # "file":Ljava/io/File;
    .end local v6    # "in":Ljava/io/InputStream;
    :catch_1
    move-exception v4

    .line 2672
    .local v4, "e":Ljava/io/IOException;
    const-string/jumbo v13, "%s%s"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFile:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    const/4 v15, 0x1

    const v16, 0x7f070317

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getString(I)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 2673
    .local v12, "strMsg":Ljava/lang/String;
    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v12, v13}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 2620
    .end local v4    # "e":Ljava/io/IOException;
    .end local v12    # "strMsg":Ljava/lang/String;
    .restart local v5    # "file":Ljava/io/File;
    .restart local v6    # "in":Ljava/io/InputStream;
    :cond_2
    :try_start_2
    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v13

    const-wide/16 v15, 0x2328

    cmp-long v13, v13, v15

    if-lez v13, :cond_3

    const-wide/16 v13, 0x2328

    :goto_1
    long-to-int v9, v13

    .line 2621
    .local v9, "nLen":I
    new-array v3, v9, [B

    .line 2622
    .local v3, "bytes":[B
    const/4 v11, 0x0

    .local v11, "offset":I
    const/4 v10, 0x0

    .line 2624
    .local v10, "numRead":I
    :goto_2
    array-length v13, v3

    if-ge v11, v13, :cond_4

    array-length v13, v3

    sub-int/2addr v13, v11

    invoke-virtual {v6, v3, v11, v13}, Ljava/io/InputStream;->read([BII)I

    move-result v10

    if-ltz v10, :cond_4

    .line 2625
    add-int/2addr v11, v10

    goto :goto_2

    .line 2620
    .end local v3    # "bytes":[B
    .end local v9    # "nLen":I
    .end local v10    # "numRead":I
    .end local v11    # "offset":I
    :cond_3
    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v13

    goto :goto_1

    .line 2628
    .restart local v3    # "bytes":[B
    .restart local v9    # "nLen":I
    .restart local v10    # "numRead":I
    .restart local v11    # "offset":I
    :cond_4
    array-length v13, v3

    if-ge v11, v13, :cond_5

    .line 2629
    new-instance v13, Ljava/io/IOException;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "Could not completely read file "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v13
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 2674
    .end local v3    # "bytes":[B
    .end local v5    # "file":Ljava/io/File;
    .end local v6    # "in":Ljava/io/InputStream;
    .end local v9    # "nLen":I
    .end local v10    # "numRead":I
    .end local v11    # "offset":I
    :catch_2
    move-exception v4

    .line 2675
    .local v4, "e":Ljava/lang/Exception;
    const-string/jumbo v13, "%s%s"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFile:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    const/4 v15, 0x1

    const v16, 0x7f070317

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getString(I)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 2676
    .restart local v12    # "strMsg":Ljava/lang/String;
    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v12, v13}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 2631
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v12    # "strMsg":Ljava/lang/String;
    .restart local v3    # "bytes":[B
    .restart local v5    # "file":Ljava/io/File;
    .restart local v6    # "in":Ljava/io/InputStream;
    .restart local v9    # "nLen":I
    .restart local v10    # "numRead":I
    .restart local v11    # "offset":I
    :cond_5
    :try_start_3
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 2635
    move-object/from16 v0, p0

    iget v8, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mTempPreferenceTextEncoding:I
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 2640
    .local v8, "nEncoding":I
    :try_start_4
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v8}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getCharset([BI)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_curcharsetName:Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 2645
    :goto_3
    :try_start_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_curcharsetName:Ljava/lang/String;

    if-nez v13, :cond_6

    const-string/jumbo v13, "UTF-8"

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_curcharsetName:Ljava/lang/String;

    .line 2647
    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_curcharsetName:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v13, v0, v14}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setReadInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 2648
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v13}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->requestFocus()Z

    .line 2650
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    .line 2651
    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFile:Ljava/lang/String;

    .line 2652
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bSaved:Z

    .line 2653
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setChanged(Z)V

    .line 2654
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setTitleText()V

    goto/16 :goto_0

    .line 2641
    :catch_3
    move-exception v4

    .line 2642
    .restart local v4    # "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 2643
    const-string/jumbo v13, "UTF-8"

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_curcharsetName:Ljava/lang/String;
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_3

    .line 2669
    .end local v3    # "bytes":[B
    .end local v5    # "file":Ljava/io/File;
    .end local v6    # "in":Ljava/io/InputStream;
    .end local v8    # "nEncoding":I
    .end local v9    # "nLen":I
    .end local v10    # "numRead":I
    .end local v11    # "offset":I
    .restart local v2    # "OpenType":I
    .local v4, "e":Ljava/io/FileNotFoundException;
    :cond_7
    const-string/jumbo v13, "%s%s"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFile:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    const/4 v15, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f070314

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 2670
    .restart local v12    # "strMsg":Ljava/lang/String;
    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v12, v13}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0
.end method

.method private prepareEnterBookmarkActivity()[I
    .locals 5

    .prologue
    .line 3911
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v4

    sub-int v0, v3, v4

    .line 3912
    .local v0, "gap":I
    const/4 v1, 0x1

    .line 3914
    .local v1, "isSelected":Z
    if-nez v0, :cond_0

    .line 3915
    const/4 v1, 0x0

    .line 3917
    :cond_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->prepareEnterBookmarkActivity()[I

    move-result-object v2

    .line 3919
    .local v2, "pos":[I
    if-nez v1, :cond_1

    .line 3920
    const/4 v2, 0x0

    .line 3922
    :cond_1
    return-object v2
.end method

.method private printDialogInflater()V
    .locals 3

    .prologue
    .line 3574
    const-string/jumbo v1, "layout_inflater"

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 3575
    .local v0, "vi":Landroid/view/LayoutInflater;
    const v1, 0x7f030055

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->v:Landroid/view/View;

    .line 3576
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->v:Landroid/view/View;

    const v2, 0x7f0b01d5

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_msg:Landroid/widget/TextView;

    .line 3577
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->v:Landroid/view/View;

    const v2, 0x7f0b0245

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_msg_delaytime:Landroid/widget/TextView;

    .line 3578
    return-void
.end method

.method private printListenerInitialize()Z
    .locals 2

    .prologue
    .line 1574
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$11;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$11;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_printlistener:Lcom/infraware/polarisoffice5/text/manager/PrintManager$PrintManagerListener;

    .line 1600
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_printlistener:Lcom/infraware/polarisoffice5/text/manager/PrintManager$PrintManagerListener;

    if-nez v0, :cond_0

    .line 1601
    const/4 v0, 0x0

    .line 1604
    :goto_0
    return v0

    .line 1603
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_printmanager:Lcom/infraware/polarisoffice5/text/manager/PrintManager;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_printlistener:Lcom/infraware/polarisoffice5/text/manager/PrintManager$PrintManagerListener;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->setPrintManagerListener(Lcom/infraware/polarisoffice5/text/manager/PrintManager$PrintManagerListener;)V

    .line 1604
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private registerOnClickListener()V
    .locals 1

    .prologue
    .line 1528
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnUndo:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1529
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnRedo:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1530
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFind:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1531
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnProperties:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1532
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnMenu:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1533
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindOption:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1534
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindPrevFind:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1535
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindNextFind:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1536
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceOption:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1537
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceNextFind:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1538
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplace:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1539
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceAll:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1540
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnDone:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1542
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnNext:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1543
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnPlay:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1544
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnPrev:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1546
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFormatIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1548
    return-void
.end method

.method public static removeOpenfileList(Ljava/lang/String;)V
    .locals 2
    .param p0, "strFilePath"    # Ljava/lang/String;

    .prologue
    .line 5463
    sget-object v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_currentOpenPathList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 5464
    sget-object v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_currentOpenPathList:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 5465
    .local v0, "index":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 5466
    sget-object v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_currentOpenPathList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 5468
    .end local v0    # "index":I
    :cond_0
    return-void
.end method

.method private resultWebStorage(ILandroid/content/Intent;)V
    .locals 2
    .param p1, "resultCode"    # I
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2495
    const/4 v0, 0x0

    .line 2497
    .local v0, "nErrMsgId":I
    packed-switch p1, :pswitch_data_0

    .line 2510
    :goto_0
    if-lez v0, :cond_0

    .line 2511
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 2512
    :cond_0
    return-void

    .line 2500
    :pswitch_0
    const/4 v0, 0x0

    .line 2501
    goto :goto_0

    .line 2503
    :pswitch_1
    const v0, 0x7f070258

    .line 2504
    goto :goto_0

    .line 2506
    :pswitch_2
    const v0, 0x7f070265

    goto :goto_0

    .line 2497
    nop

    :pswitch_data_0
    .packed-switch 0x1002
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private saveFile(Ljava/lang/String;)V
    .locals 8
    .param p1, "strFileName"    # Ljava/lang/String;

    .prologue
    const v7, 0x7f0702ed

    const v6, 0x7f0702ec

    const v5, 0x7f070042

    const/4 v4, 0x0

    .line 2683
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2685
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->canWrite()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->canWrite()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2687
    :cond_1
    const v2, 0x7f070042

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_2

    .line 2688
    const v2, 0x7f0702ed

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 2692
    :goto_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->requestFocus()Z

    .line 2694
    const/4 v1, 0x0

    .line 2716
    .end local v1    # "file":Ljava/io/File;
    :goto_1
    return-void

    .line 2690
    .restart local v1    # "file":Ljava/io/File;
    :cond_2
    const v2, 0x7f0702ec

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2707
    .end local v1    # "file":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 2709
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_4

    .line 2710
    invoke-static {p0, v7, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 2715
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_2
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->requestFocus()Z

    goto :goto_1

    .line 2698
    .restart local v1    # "file":Ljava/io/File;
    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2, p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->saveTextInfo(Ljava/lang/String;)V

    .line 2700
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    .line 2701
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFile:Ljava/lang/String;

    .line 2702
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bSaved:Z

    .line 2703
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setChanged(Z)V

    .line 2704
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setTitleText()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 2712
    .end local v1    # "file":Ljava/io/File;
    .restart local v0    # "e":Ljava/lang/Exception;
    :cond_4
    invoke-static {p0, v6, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_2
.end method

.method private setDisplayType()V
    .locals 4

    .prologue
    .line 739
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201d2

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 741
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    iput v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_sizequickthumbsize:I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 751
    :goto_0
    return-void

    .line 742
    :catch_0
    move-exception v1

    .line 743
    .local v1, "e":Ljava/lang/NullPointerException;
    const/16 v2, 0x62

    iput v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_sizequickthumbsize:I

    goto :goto_0
.end method

.method private setEventListener()V
    .locals 3

    .prologue
    .line 1283
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    new-instance v2, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$5;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$5;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1325
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplaceFind:Landroid/widget/EditText;

    new-instance v2, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$6;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$6;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1353
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplace:Landroid/widget/EditText;

    new-instance v2, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$7;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$7;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1377
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;

    new-instance v2, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$8;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$8;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1406
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1407
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplaceFind:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1409
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ibScrollThumb:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1411
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setGestureListener()V

    .line 1413
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setViewchangeListener()V

    .line 1415
    new-instance v1, Lcom/infraware/common/event/SdCardEvent;

    invoke-direct {v1}, Lcom/infraware/common/event/SdCardEvent;-><init>()V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    .line 1416
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    invoke-virtual {v1, p0}, Lcom/infraware/common/event/SdCardEvent;->setSdListener(Lcom/infraware/common/event/SdCardListener;)V

    .line 1417
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getSdcardIntentFilter()Landroid/content/IntentFilter;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1419
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$9;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$9;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1452
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->printListenerInitialize()Z

    .line 1454
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    .line 1509
    .local v0, "mLongClickListenr":Landroid/view/View$OnLongClickListener;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceOption:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1510
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindOption:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1511
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFormatIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1512
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnUndo:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1513
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnRedo:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1514
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFind:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1515
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnProperties:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1516
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnMenu:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1517
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindNextFind:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1518
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindPrevFind:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1519
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnDone:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1520
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnPrev:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1521
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnPlay:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1522
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnNext:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1524
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->registerOnClickListener()V

    .line 1525
    return-void
.end method

.method private setGestureListener()V
    .locals 2

    .prologue
    .line 1609
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$12;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$12;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 1625
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$13;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$13;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mGestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    .line 1673
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mGestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    invoke-direct {v0, p0, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mDetector:Landroid/view/GestureDetector;

    .line 1674
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mDetector:Landroid/view/GestureDetector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 1675
    return-void
.end method

.method private setNfcCallback()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 4729
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v0

    if-nez v0, :cond_1

    .line 4737
    :cond_0
    :goto_0
    return-void

    .line 4731
    :cond_1
    invoke-static {p0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 4732
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    if-eqz v0, :cond_0

    .line 4733
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$NfcCallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$NfcCallback;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$1;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$NfcCallback;

    .line 4734
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$NfcCallback;

    new-array v2, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p0, v2}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 4735
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$NfcCallback;

    new-array v2, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p0, v2}, Landroid/nfc/NfcAdapter;->setOnNdefPushCompleteCallback(Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    goto :goto_0
.end method

.method private setPreSelection(II)V
    .locals 3
    .param p1, "nBlock"    # I
    .param p2, "nSelection"    # I

    .prologue
    const/4 v2, 0x0

    .line 3317
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCount()I

    move-result v0

    if-le p1, v0, :cond_0

    .line 3318
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCount()I

    move-result v1

    invoke-virtual {v0, v1, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->loadBlock(IZ)V

    .line 3319
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    .line 3328
    :goto_0
    return-void

    .line 3321
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getCurBlock()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 3322
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0, p1, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->loadBlock(IZ)V

    .line 3323
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->length()I

    move-result v0

    if-le p2, v0, :cond_2

    .line 3324
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    goto :goto_0

    .line 3326
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0, p2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    goto :goto_0
.end method

.method private setPreferences(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x0

    .line 2188
    const-string/jumbo v3, "FontSize"

    const/16 v4, 0xa

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 2189
    .local v1, "nFontSize":I
    const-string/jumbo v3, "Theme"

    invoke-virtual {p1, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 2190
    .local v2, "nTheme":I
    const-string/jumbo v3, "Encoding"

    invoke-virtual {p1, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 2193
    .local v0, "nEncoding":I
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onFontSizeChanged(I)V

    .line 2195
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v3

    const/16 v4, 0x16

    invoke-virtual {v3, p0, v4, v2}, Lcom/infraware/common/config/RuntimeConfig;->setIntPreference(Landroid/content/Context;II)V

    .line 2196
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onTextEncodeChanged(I)V

    .line 2200
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setFontSize(I)V

    .line 2201
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setFontPoolSync(I)V

    .line 2202
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setBackgroundTheme(I)V

    .line 2204
    iget v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nEncoding:I

    if-ne v3, v0, :cond_0

    .line 2205
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Handler:Landroid/os/Handler;

    new-instance v4, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$22;

    invoke-direct {v4, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$22;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2213
    :cond_0
    return-void
.end method

.method private setTTSMode(I)V
    .locals 2
    .param p1, "flag"    # I

    .prologue
    .line 2881
    iput p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsMode:I

    .line 2882
    packed-switch p1, :pswitch_data_0

    .line 2890
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_TTSmanager:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->setTTSMode(I)V

    .line 2891
    return-void

    .line 2884
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_tvTTSTitle:Landroid/widget/TextView;

    const v1, 0x7f070311

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 2887
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_tvTTSTitle:Landroid/widget/TextView;

    const v1, 0x7f070310

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 2882
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setValueFromIntent()V
    .locals 2

    .prologue
    .line 1246
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1247
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "key_filename"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    .line 1249
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->checkASCFile(Ljava/lang/String;)V

    .line 1260
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setViewMode(I)V

    .line 1263
    const-string/jumbo v1, "search-key"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mContentSearchWord:Ljava/lang/String;

    .line 1266
    return-void
.end method

.method private setViewchangeListener()V
    .locals 2

    .prologue
    .line 1678
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mTouchListener:Landroid/view/View$OnTouchListener;

    if-eqz v0, :cond_0

    .line 1679
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1680
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFormatIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1681
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_tvTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1682
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFind:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1683
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnProperties:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1684
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnMenu:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1685
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnUndo:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1686
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnRedo:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1688
    :cond_0
    return-void
.end method

.method private showFileNotFoundDialog()V
    .locals 4

    .prologue
    .line 4294
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v2

    invoke-direct {v1, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const/high16 v2, 0x7f070000

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f070263

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070063

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$35;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$35;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$34;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$34;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 4313
    .local v0, "oDialog":Landroid/app/AlertDialog;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 4314
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 4315
    return-void
.end method

.method private showNeedSaveFileMsg()V
    .locals 4

    .prologue
    .line 3926
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v2

    invoke-direct {v1, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0701f1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0701e8

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f070063

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    new-instance v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$28;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$28;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 3935
    .local v0, "builder":Landroid/app/AlertDialog;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 3936
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 3937
    return-void
.end method

.method private showPreferences()V
    .locals 7

    .prologue
    .line 2112
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hideSoftKeyboard()Z

    .line 2114
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    invoke-direct {v0, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2117
    .local v0, "intent":Landroid/content/Intent;
    iget v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mTempPreferenceFontSize:I

    .line 2118
    .local v2, "nFontSize":I
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v4

    const/16 v5, 0x16

    const/4 v6, 0x0

    invoke-virtual {v4, p0, v5, v6}, Lcom/infraware/common/config/RuntimeConfig;->getIntPreference(Landroid/content/Context;II)I

    move-result v3

    .line 2120
    .local v3, "nTheme":I
    iget v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mTempPreferenceTextEncoding:I

    .line 2123
    .local v1, "nEncoding":I
    const-string/jumbo v4, "FontSize"

    invoke-virtual {v0, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2124
    const-string/jumbo v4, "Theme"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2125
    const-string/jumbo v4, "Encoding"

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2127
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->isASCFile()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2128
    const-string/jumbo v4, "UTF-8"

    const-string/jumbo v5, "UTF-8"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2130
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setLastDisplayPosition()V

    .line 2132
    const/4 v4, 0x1

    invoke-virtual {p0, v0, v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2133
    return-void
.end method

.method private showSdcardUnmountDialog()V
    .locals 4

    .prologue
    .line 4269
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v2

    invoke-direct {v1, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const/high16 v2, 0x7f070000

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0700a5

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070063

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$33;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$33;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$32;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$32;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 4288
    .local v0, "oDialog":Landroid/app/AlertDialog;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 4289
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 4290
    return-void
.end method

.method private unregisterOnClickListener()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1551
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnUndo:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1552
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnRedo:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1553
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFind:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1554
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnProperties:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1555
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnMenu:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1556
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindOption:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1557
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindPrevFind:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1558
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindNextFind:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1559
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceOption:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1560
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceNextFind:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1561
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplace:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1562
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceAll:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1563
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnDone:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1565
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnNext:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1566
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnPlay:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1567
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnPrev:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1569
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFormatIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1571
    return-void
.end method

.method public static updateOpenPathList(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "deletePath"    # Ljava/lang/String;
    .param p1, "addPath"    # Ljava/lang/String;

    .prologue
    .line 4330
    sget-object v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_currentOpenPathList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 4331
    sget-object v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_currentOpenPathList:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4332
    sget-object v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_currentOpenPathList:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 4333
    sget-object v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_currentOpenPathList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4336
    :cond_0
    return-void
.end method


# virtual methods
.method public ShowIme()V
    .locals 1

    .prologue
    .line 5407
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-static {v0}, Lcom/infraware/office/util/EvUtil;->showIme(Landroid/view/View;)V

    .line 5408
    return-void
.end method

.method public actionBarVisible(Z)V
    .locals 2
    .param p1, "m_b"    # Z

    .prologue
    .line 3687
    if-eqz p1, :cond_1

    .line 3688
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3693
    :cond_0
    :goto_0
    return-void

    .line 3690
    :cond_1
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bLandActionBar:Z

    if-nez v0, :cond_0

    .line 3691
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public adjustEditScroll(I)V
    .locals 4
    .param p1, "nScroll"    # I

    .prologue
    .line 3667
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_viewInScroll:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 3668
    .local v0, "llLp":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->calcScrollHeight()V

    .line 3669
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getScrollHeight()I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 3670
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_viewInScroll:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3673
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2, p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->calcScrollPos(I)V

    .line 3674
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getScrollPos()I

    move-result v1

    .line 3675
    .local v1, "nPos":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 3676
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_svEdit:Landroid/widget/ScrollView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, Landroid/widget/ScrollView;->scrollTo(II)V

    .line 3677
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->moveThumbByScroll()V

    .line 3679
    :cond_0
    return-void
.end method

.method public checkSamsungPrint()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 3656
    :try_start_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string/jumbo v2, "com.sec.android.app.mobileprint"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 3657
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bSamsungPrint:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3662
    :goto_0
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bSamsungPrint:Z

    return v1

    .line 3658
    :catch_0
    move-exception v0

    .line 3659
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    iput-boolean v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bSamsungPrint:Z

    goto :goto_0
.end method

.method public exportEmail(Landroid/net/Uri;)V
    .locals 13
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 3985
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v11

    const-string/jumbo v12, "FT03"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 3986
    const/4 v5, 0x0

    .line 3988
    .local v5, "mimeType":Ljava/lang/String;
    iget-object v11, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    const/16 v12, 0x2e

    invoke-virtual {v11, v12}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 3989
    .local v2, "idx_exe":I
    iget-object v11, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    add-int/lit8 v12, v2, 0x1

    invoke-virtual {v11, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 3991
    .local v10, "temp":Ljava/lang/String;
    invoke-virtual {v10}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/infraware/common/util/FileUtils;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 3993
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 3995
    .local v9, "targetShareIntents":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v11, "android.intent.action.SENDTO"

    invoke-direct {v3, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3996
    .local v3, "intent":Landroid/content/Intent;
    const-string/jumbo v11, "message/rfc822"

    invoke-virtual {v3, v11}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 3997
    const-string/jumbo v11, "android.intent.extra.STREAM"

    invoke-virtual {v3, v11, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 3998
    const-string/jumbo v11, "mailto:"

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v3, v11}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 4000
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v11

    const/high16 v12, 0x10000

    invoke-virtual {v11, v3, v12}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v4

    .line 4001
    .local v4, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_2

    .line 4002
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/pm/ResolveInfo;

    .line 4003
    .local v7, "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v11, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v11, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 4005
    .local v6, "packageName":Ljava/lang/String;
    const-string/jumbo v11, "com.glympse.android.glympse"

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 4006
    new-instance v8, Landroid/content/Intent;

    const-string/jumbo v11, "android.intent.action.SEND"

    invoke-direct {v8, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4007
    .local v8, "targetIntent":Landroid/content/Intent;
    invoke-virtual {v8, v5}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 4008
    const/high16 v11, 0x10000000

    invoke-virtual {v8, v11}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 4009
    const-string/jumbo v11, "android.intent.extra.STREAM"

    invoke-virtual {v8, v11, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 4010
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 4011
    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 4014
    .end local v6    # "packageName":Ljava/lang/String;
    .end local v7    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    .end local v8    # "targetIntent":Landroid/content/Intent;
    :cond_1
    const/4 v11, 0x0

    invoke-interface {v9, v11}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/content/Intent;

    const/4 v12, 0x0

    invoke-static {v11, v12}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 4015
    .local v0, "chooserIntent":Landroid/content/Intent;
    const-string/jumbo v12, "android.intent.extra.INITIAL_INTENTS"

    const/4 v11, 0x0

    new-array v11, v11, [Landroid/os/Parcelable;

    invoke-interface {v9, v11}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [Landroid/os/Parcelable;

    invoke-virtual {v0, v12, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 4017
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->startActivity(Landroid/content/Intent;)V

    .line 4032
    .end local v0    # "chooserIntent":Landroid/content/Intent;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "idx_exe":I
    .end local v4    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v5    # "mimeType":Ljava/lang/String;
    .end local v9    # "targetShareIntents":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    .end local v10    # "temp":Ljava/lang/String;
    :cond_2
    :goto_1
    return-void

    .line 4021
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_3
    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v11, "android.intent.action.SEND"

    invoke-direct {v3, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4022
    .restart local v3    # "intent":Landroid/content/Intent;
    const-string/jumbo v11, "message/rfc822"

    invoke-virtual {v3, v11}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 4023
    const-string/jumbo v11, "android.intent.extra.STREAM"

    invoke-virtual {v3, v11, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 4028
    const/high16 v11, 0x10000000

    invoke-virtual {v3, v11}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 4030
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method public declared-synchronized findNextText(Ljava/lang/String;ZZZ)I
    .locals 11
    .param p1, "strFind"    # Ljava/lang/String;
    .param p2, "bMatchCase"    # Z
    .param p3, "bWholeWord"    # Z
    .param p4, "bDirect"    # Z

    .prologue
    const/4 v10, -0x1

    const/4 v7, 0x0

    .line 2980
    monitor-enter p0

    const/4 v2, -0x1

    .line 2981
    .local v2, "nOffset":I
    :try_start_0
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 2984
    .local v6, "strText":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-gtz v8, :cond_0

    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCount()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_0

    .line 2985
    const/4 v7, 0x0

    iput v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBlock:I

    .line 2986
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindSuccess:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v3, v2

    .line 3063
    .end local v2    # "nOffset":I
    .local v3, "nOffset":I
    :goto_0
    monitor-exit p0

    return v3

    .line 2990
    .end local v3    # "nOffset":I
    .restart local v2    # "nOffset":I
    :cond_0
    :try_start_1
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v1

    .line 2992
    .local v1, "nCaretPos":I
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v8

    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v9}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v9

    invoke-virtual {v6, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 2993
    .local v5, "strSelText":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {v5, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_1

    .line 2994
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v1

    .line 2996
    :cond_1
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFind:Ljava/lang/String;

    invoke-virtual {p1, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_2

    iget-boolean v8, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindMatchCase:Z

    if-ne p2, v8, :cond_2

    iget-boolean v8, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindWhole:Z

    if-ne p3, v8, :cond_2

    iget-boolean v8, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindDirect:Z

    if-eq v8, p4, :cond_3

    .line 2997
    :cond_2
    iput-boolean p4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindDirect:Z

    .line 2998
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFind:Ljava/lang/String;

    .line 2999
    iput-boolean p2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindMatchCase:Z

    .line 3000
    iput-boolean p3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindWhole:Z

    .line 3002
    iput v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginPos:I

    .line 3003
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getCurBlock()I

    move-result v8

    iput v8, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginBlock:I

    .line 3004
    const/4 v8, -0x1

    iput v8, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindFirstPos:I

    .line 3007
    :cond_3
    if-nez p2, :cond_4

    .line 3008
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 3009
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    .line 3012
    :cond_4
    invoke-virtual {v6, p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    .line 3014
    if-eqz p3, :cond_5

    .line 3015
    invoke-direct {p0, v6, p1, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findWholeWordIndex(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    .line 3017
    :cond_5
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getCurBlock()I

    move-result v0

    .line 3019
    .local v0, "nBlock":I
    :cond_6
    if-ne v2, v10, :cond_7

    .line 3020
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getFindStopflag()Z

    move-result v8

    if-eqz v8, :cond_a

    .line 3021
    const/4 v2, -0x1

    .line 3022
    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setFindStopflag(Z)V

    .line 3048
    :cond_7
    :goto_1
    if-nez v0, :cond_f

    .line 3049
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCharCount(I)I

    move-result v7

    if-le v2, v7, :cond_8

    .line 3050
    add-int/lit8 v0, v0, 0x1

    .line 3059
    :cond_8
    :goto_2
    if-ne v2, v10, :cond_9

    .line 3060
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindSuccess:Z

    .line 3061
    :cond_9
    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBlock:I

    move v3, v2

    .line 3063
    .end local v2    # "nOffset":I
    .restart local v3    # "nOffset":I
    goto/16 :goto_0

    .line 3026
    .end local v3    # "nOffset":I
    .restart local v2    # "nOffset":I
    :cond_a
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-eq v0, v8, :cond_d

    .line 3027
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockBufferText(I)Ljava/lang/String;

    move-result-object v6

    .line 3032
    :goto_3
    if-nez v0, :cond_e

    move v1, v7

    .line 3034
    :goto_4
    if-nez p2, :cond_b

    .line 3035
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 3036
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    .line 3039
    :cond_b
    invoke-virtual {v6, p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    .line 3040
    if-eqz p3, :cond_c

    .line 3041
    invoke-direct {p0, v6, p1, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findWholeWordIndex(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    .line 3043
    :cond_c
    iget v8, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginBlock:I

    if-ne v8, v0, :cond_6

    goto :goto_1

    .line 3029
    :cond_d
    const/4 v0, 0x0

    .line 3030
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v8, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockBufferText(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_3

    .line 3032
    :cond_e
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    add-int/lit8 v9, v0, -0x1

    invoke-virtual {v8, v9}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCharCount(I)I

    move-result v1

    goto :goto_4

    .line 3052
    :cond_f
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    add-int/lit8 v8, v0, -0x1

    invoke-virtual {v7, v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCharCount(I)I

    move-result v7

    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v8, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCharCount(I)I

    move-result v8

    add-int v4, v7, v8

    .line 3053
    .local v4, "nTwoBlockSize":I
    if-le v2, v4, :cond_8

    .line 3054
    add-int/lit8 v0, v0, 0x1

    .line 3055
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    add-int/lit8 v8, v0, -0x1

    invoke-virtual {v7, v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCharCount(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v7

    sub-int/2addr v2, v7

    goto :goto_2

    .line 2980
    .end local v0    # "nBlock":I
    .end local v1    # "nCaretPos":I
    .end local v4    # "nTwoBlockSize":I
    .end local v5    # "strSelText":Ljava/lang/String;
    .end local v6    # "strText":Ljava/lang/String;
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7
.end method

.method public declared-synchronized findNextViewProcess(Ljava/lang/String;IZ)V
    .locals 9
    .param p1, "strFind"    # Ljava/lang/String;
    .param p2, "nOffset"    # I
    .param p3, "bMessage"    # Z

    .prologue
    const/4 v7, -0x1

    .line 3068
    monitor-enter p0

    :try_start_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 3069
    .local v4, "str":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    .line 3071
    .local v1, "length":I
    if-ne p2, v7, :cond_5

    .line 3072
    iget-boolean v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindSuccess:Z

    if-nez v5, :cond_0

    iget-boolean v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindAtLeastOneWord:Z

    if-eqz v5, :cond_3

    .line 3073
    :cond_0
    if-eqz p3, :cond_1

    .line 3074
    const v5, 0x7f0701dc

    const/4 v6, 0x0

    invoke-static {p0, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 3076
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindAtLeastOneWord:Z

    .line 3082
    :cond_1
    :goto_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getCurBlock()I

    move-result v5

    iget v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginBlock:I

    if-eq v5, v6, :cond_2

    .line 3083
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginBlock:I

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->loadBlock(IZ)V

    .line 3086
    :cond_2
    iget v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginPos:I

    if-ge v5, v1, :cond_4

    .line 3087
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginPos:I

    invoke-virtual {v5, v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    .line 3091
    :goto_1
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindSuccess:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3124
    :goto_2
    monitor-exit p0

    return-void

    .line 3079
    :cond_3
    const v5, 0x7f0701dc

    const/4 v6, 0x0

    :try_start_1
    invoke-static {p0, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 3068
    .end local v1    # "length":I
    .end local v4    # "str":Ljava/lang/String;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 3089
    .restart local v1    # "length":I
    .restart local v4    # "str":Ljava/lang/String;
    :cond_4
    :try_start_2
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    goto :goto_1

    .line 3093
    :cond_5
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getCurBlock()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockOffsetInFile(I)I

    move-result v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockStartOffset()I

    move-result v6

    sub-int/2addr v5, v6

    add-int v2, v5, p2

    .line 3094
    .local v2, "nFindCurPos":I
    iget-boolean v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindSuccess:Z

    if-eqz v5, :cond_8

    iget v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindFirstPos:I

    if-ne v2, v5, :cond_8

    if-eqz p3, :cond_8

    .line 3097
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getCurBlock()I

    move-result v5

    iget v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginBlock:I

    if-eq v5, v6, :cond_6

    .line 3098
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginBlock:I

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->loadBlock(IZ)V

    .line 3101
    :cond_6
    iget v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginPos:I

    if-ge v5, v1, :cond_7

    .line 3102
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginPos:I

    invoke-virtual {v5, v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    .line 3106
    :goto_3
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindSuccess:Z

    .line 3108
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iget-boolean v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachCase:Z

    iget-boolean v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachWholeWord:Z

    const/4 v8, 0x1

    invoke-virtual {p0, v5, v6, v7, v8}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findNextText(Ljava/lang/String;ZZZ)I

    move-result v3

    .line 3109
    .local v3, "offset":I
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {p0, v5, v3, v6}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findNextViewProcess(Ljava/lang/String;IZ)V

    goto :goto_2

    .line 3104
    .end local v3    # "offset":I
    :cond_7
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    goto :goto_3

    .line 3111
    :cond_8
    iget v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindFirstPos:I

    if-ne v5, v7, :cond_9

    .line 3112
    iput v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindFirstPos:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3114
    :cond_9
    :try_start_3
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v6, p2

    invoke-virtual {v5, p2, v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(II)V
    :try_end_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3119
    :goto_4
    const/4 v5, 0x1

    :try_start_4
    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindSuccess:Z

    .line 3121
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindAtLeastOneWord:Z

    goto/16 :goto_2

    .line 3115
    :catch_0
    move-exception v0

    .line 3117
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_4
.end method

.method public declared-synchronized findPreText(Ljava/lang/String;ZZZ)I
    .locals 11
    .param p1, "strFind"    # Ljava/lang/String;
    .param p2, "bMatchCase"    # Z
    .param p3, "bWholeWord"    # Z
    .param p4, "bDirect"    # Z

    .prologue
    const/4 v10, -0x1

    .line 3129
    monitor-enter p0

    const/4 v2, -0x1

    .line 3130
    .local v2, "nOffset":I
    :try_start_0
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 3132
    .local v6, "strText":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-gtz v7, :cond_0

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCount()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_0

    .line 3133
    const/4 v7, 0x0

    iput v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBlock:I

    .line 3134
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindSuccess:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v3, v2

    .line 3216
    .end local v2    # "nOffset":I
    .local v3, "nOffset":I
    :goto_0
    monitor-exit p0

    return v3

    .line 3138
    .end local v3    # "nOffset":I
    .restart local v2    # "nOffset":I
    :cond_0
    :try_start_1
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v7

    add-int/lit8 v1, v7, -0x1

    .line 3139
    .local v1, "nCaretPos":I
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v7

    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v8

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 3140
    .local v5, "strSelText":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v5, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v7

    if-eqz v7, :cond_1

    .line 3141
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v1

    .line 3143
    :cond_1
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFind:Ljava/lang/String;

    invoke-virtual {p1, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v7

    if-nez v7, :cond_2

    iget-boolean v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindMatchCase:Z

    if-ne p2, v7, :cond_2

    iget-boolean v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindWhole:Z

    if-ne p3, v7, :cond_2

    iget-boolean v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindDirect:Z

    if-eq v7, p4, :cond_3

    .line 3144
    :cond_2
    iput-boolean p4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindDirect:Z

    .line 3145
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFind:Ljava/lang/String;

    .line 3146
    iput-boolean p2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindMatchCase:Z

    .line 3147
    iput-boolean p3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindWhole:Z

    .line 3149
    add-int/lit8 v7, v1, 0x1

    iput v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginPos:I

    .line 3150
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getCurBlock()I

    move-result v7

    iput v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginBlock:I

    .line 3151
    const/4 v7, -0x1

    iput v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindFirstPos:I

    .line 3154
    :cond_3
    if-nez p2, :cond_4

    .line 3155
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 3156
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    .line 3159
    :cond_4
    invoke-virtual {v6, p1, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;I)I

    move-result v2

    .line 3160
    if-eqz p3, :cond_5

    if-eq v2, v10, :cond_5

    .line 3161
    invoke-direct {p0, v6, p1, v2, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findWholeWordLastIndex(Ljava/lang/String;Ljava/lang/String;II)I

    move-result v2

    .line 3163
    :cond_5
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getCurBlock()I

    move-result v0

    .line 3165
    .local v0, "nBlock":I
    :cond_6
    if-ne v2, v10, :cond_7

    .line 3166
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getFindStopflag()Z

    move-result v7

    if-eqz v7, :cond_a

    .line 3167
    const/4 v2, -0x1

    .line 3168
    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setFindStopflag(Z)V

    .line 3196
    :cond_7
    :goto_1
    if-eqz v0, :cond_10

    .line 3198
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v7, v2

    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    add-int/lit8 v9, v0, -0x1

    invoke-virtual {v8, v9}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCharCount(I)I

    move-result v8

    if-gt v7, v8, :cond_8

    .line 3199
    add-int/lit8 v0, v0, -0x1

    .line 3200
    if-eqz v0, :cond_8

    .line 3201
    if-ne v2, v10, :cond_f

    .line 3202
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindSuccess:Z

    .line 3213
    :cond_8
    :goto_2
    if-eq v2, v10, :cond_9

    .line 3214
    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBlock:I

    :cond_9
    move v3, v2

    .line 3216
    .end local v2    # "nOffset":I
    .restart local v3    # "nOffset":I
    goto/16 :goto_0

    .line 3172
    .end local v3    # "nOffset":I
    .restart local v2    # "nOffset":I
    :cond_a
    if-eqz v0, :cond_d

    .line 3173
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v7, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockBufferText(I)Ljava/lang/String;

    move-result-object v6

    .line 3179
    :goto_3
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCharCount(I)I

    move-result v7

    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    add-int/lit8 v9, v0, 0x1

    invoke-virtual {v8, v9}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCharCount(I)I

    move-result v8

    add-int v4, v7, v8

    .line 3180
    .local v4, "nPrevTwoBlockSize":I
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    if-ne v0, v7, :cond_e

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v1

    .line 3182
    :goto_4
    if-nez p2, :cond_b

    .line 3183
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 3184
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    .line 3187
    :cond_b
    invoke-virtual {v6, p1, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;I)I

    move-result v2

    .line 3188
    if-eqz p3, :cond_c

    .line 3189
    invoke-direct {p0, v6, p1, v2, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findWholeWordLastIndex(Ljava/lang/String;Ljava/lang/String;II)I

    move-result v2

    .line 3191
    :cond_c
    iget v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginBlock:I

    if-ne v7, v0, :cond_6

    goto :goto_1

    .line 3175
    .end local v4    # "nPrevTwoBlockSize":I
    :cond_d
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCount()I

    move-result v7

    add-int/lit8 v0, v7, -0x1

    .line 3176
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockBufferText(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_3

    .restart local v4    # "nPrevTwoBlockSize":I
    :cond_e
    move v1, v4

    .line 3180
    goto :goto_4

    .line 3204
    .end local v4    # "nPrevTwoBlockSize":I
    :cond_f
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    add-int/lit8 v8, v0, -0x1

    invoke-virtual {v7, v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCharCount(I)I

    move-result v7

    add-int/2addr v2, v7

    goto :goto_2

    .line 3209
    :cond_10
    if-ne v2, v10, :cond_8

    .line 3210
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindSuccess:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 3129
    .end local v0    # "nBlock":I
    .end local v1    # "nCaretPos":I
    .end local v5    # "strSelText":Ljava/lang/String;
    .end local v6    # "strText":Ljava/lang/String;
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7
.end method

.method public declared-synchronized findPrevViewProcess(Ljava/lang/String;I)V
    .locals 6
    .param p1, "strFind"    # Ljava/lang/String;
    .param p2, "nOffset"    # I

    .prologue
    const/4 v4, -0x1

    .line 3220
    monitor-enter p0

    if-ne p2, v4, :cond_2

    .line 3221
    :try_start_0
    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindSuccess:Z

    if-eqz v2, :cond_1

    .line 3227
    :goto_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getCurBlock()I

    move-result v2

    iget v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginBlock:I

    if-eq v2, v3, :cond_0

    .line 3228
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginBlock:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->loadBlock(IZ)V

    .line 3229
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginPos:I

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    .line 3231
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindSuccess:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3251
    :goto_1
    monitor-exit p0

    return-void

    .line 3224
    :cond_1
    const v2, 0x7f0701dc

    const/4 v3, 0x0

    :try_start_1
    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 3220
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 3233
    :cond_2
    :try_start_2
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getCurBlock()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockOffsetInFile(I)I

    move-result v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockStartOffset()I

    move-result v3

    sub-int/2addr v2, v3

    add-int v0, v2, p2

    .line 3234
    .local v0, "nFindCurPos":I
    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindSuccess:Z

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindFirstPos:I

    if-ne v0, v2, :cond_4

    .line 3237
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getCurBlock()I

    move-result v2

    iget v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginBlock:I

    if-eq v2, v3, :cond_3

    .line 3238
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginBlock:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->loadBlock(IZ)V

    .line 3239
    :cond_3
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginPos:I

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    .line 3241
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindSuccess:Z

    .line 3242
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachCase:Z

    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachWholeWord:Z

    const/4 v5, 0x0

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findPreText(Ljava/lang/String;ZZZ)I

    move-result v1

    .line 3243
    .local v1, "offset":I
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findPrevViewProcess(Ljava/lang/String;I)V

    goto :goto_1

    .line 3245
    .end local v1    # "offset":I
    :cond_4
    iget v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindFirstPos:I

    if-ne v2, v4, :cond_5

    .line 3246
    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindFirstPos:I

    .line 3247
    :cond_5
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v2, p2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(II)V

    .line 3248
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindSuccess:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public finish()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 815
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mIsContentSearch:Z

    .line 818
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mbFinishCalled:Z

    .line 819
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bByMultipleLauncher:Z

    if-nez v0, :cond_0

    .line 820
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/infraware/common/util/FileUtils;->deleteDirectory(Ljava/io/File;Z)V

    .line 824
    :cond_0
    invoke-static {}, Lcom/infraware/polarisoffice5/MyApplication;->getInstance()Lcom/infraware/polarisoffice5/MyApplication;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/MyApplication;->setCurrentTextEditor(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    .line 825
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 826
    return-void
.end method

.method public fisnishbyMultipleLauncher(Landroid/os/Handler;)V
    .locals 2
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 831
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mNewOpenHandler:Landroid/os/Handler;

    .line 832
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 833
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.infraware.polarisviewer5.CLOSE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 834
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 836
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bByMultipleLauncher:Z

    .line 837
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->finish()V

    .line 838
    return-void
.end method

.method public getActionbarLayout()Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 5369
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public getCharsetList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4239
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_AvailableCharsetList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getCurCharset()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2273
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_curcharsetName:Ljava/lang/String;

    return-object v0
.end method

.method public getDictionaryPanelMain()Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;
    .locals 1

    .prologue
    .line 713
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    return-object v0
.end method

.method public getDisplaySize()[I
    .locals 4

    .prologue
    .line 727
    const/4 v2, 0x2

    new-array v0, v2, [I

    .line 729
    .local v0, "displayInfo":[I
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 731
    .local v1, "displayMetrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 732
    const/4 v2, 0x0

    iget v3, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    aput v3, v0, v2

    .line 733
    const/4 v2, 0x1

    iget v3, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    aput v3, v0, v2

    .line 735
    return-object v0
.end method

.method public getDisplayType()I
    .locals 1

    .prologue
    .line 754
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_displayType:I

    return v0
.end method

.method public getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    .locals 1

    .prologue
    .line 4223
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    return-object v0
.end method

.method public getFindStopflag()Z
    .locals 1

    .prologue
    .line 2976
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bfindStopflag:Z

    return v0
.end method

.method public getFormatIconBtn()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 5365
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFormatIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getMWDnD()Lcom/infraware/common/multiwindow/MWDnDOperator;
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mMWDnDOperator:Lcom/infraware/common/multiwindow/MWDnDOperator;

    return-object v0
.end method

.method public getMarkedString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4828
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getMarkedString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMenuBtn()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 5361
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnMenu:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getOpenFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5459
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public getPrintStopFlag()Z
    .locals 1

    .prologue
    .line 3647
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bPrintstop:Z

    return v0
.end method

.method public getShowMode()I
    .locals 1

    .prologue
    .line 2742
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nShowMode:I

    return v0
.end method

.method public getSvEdit()Landroid/widget/ScrollView;
    .locals 1

    .prologue
    .line 3683
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_svEdit:Landroid/widget/ScrollView;

    return-object v0
.end method

.method public getTTSManager()Lcom/infraware/polarisoffice5/text/manager/TTSManager;
    .locals 1

    .prologue
    .line 4235
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_TTSmanager:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    return-object v0
.end method

.method public getTTSMode()I
    .locals 1

    .prologue
    .line 2894
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsMode:I

    return v0
.end method

.method public getTTSToolbar()Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 4227
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTSbar:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public getTextEditorActionBar()Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 5454
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public getViewMode()I
    .locals 1

    .prologue
    .line 723
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewMode:I

    return v0
.end method

.method public hideDictionaryPanel()V
    .locals 1

    .prologue
    .line 717
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v0, :cond_0

    .line 718
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->hidePanel()V

    .line 720
    :cond_0
    return-void
.end method

.method public hideScrollThumb()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 4201
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_flScrollPos:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 4202
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ibScrollThumb:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 4203
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mPageInfoPage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 4204
    return-void
.end method

.method public insertThumbnail()V
    .locals 0

    .prologue
    .line 806
    return-void
.end method

.method public isASCFile()Z
    .locals 1

    .prologue
    .line 1205
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bIsASCFile:Z

    return v0
.end method

.method public isContentSearch()Z
    .locals 1

    .prologue
    .line 2747
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mIsContentSearch:Z

    return v0
.end method

.method public isFindReplaceMode()Z
    .locals 1

    .prologue
    .line 5637
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llFind:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llFind:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llReplace:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llReplace:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 5639
    :cond_1
    const/4 v0, 0x1

    .line 5640
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isOfficeInstalled()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 3852
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 3853
    .local v3, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v2

    .line 3855
    .local v2, "pis":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/PackageInfo;

    .line 3856
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    iget-object v5, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string/jumbo v6, "com.infraware.polarisoffice5"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 3857
    const/4 v4, 0x1

    .line 3860
    .end local v1    # "pi":Landroid/content/pm/PackageInfo;
    :cond_1
    return v4
.end method

.method public isOver10mb()Z
    .locals 1

    .prologue
    .line 1193
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bIsOver10mb:Z

    return v0
.end method

.method public isSamsungEmailOpen()Z
    .locals 2

    .prologue
    .line 5472
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    const-string/jumbo v1, "data/com.android.email/cache"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5473
    const/4 v0, 0x1

    .line 5475
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSdCardAction(Ljava/lang/String;)V
    .locals 4
    .param p1, "nowAction"    # Ljava/lang/String;

    .prologue
    .line 4245
    const-string/jumbo v0, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4248
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    invoke-static {v0}, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->isSdCardFile(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4250
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->showSdcardUnmountDialog()V

    .line 4265
    :cond_0
    :goto_0
    return-void

    .line 4254
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Handler:Landroid/os/Handler;

    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$31;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$31;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public isShownDictionaryPanel()Z
    .locals 1

    .prologue
    .line 4818
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v0, :cond_0

    .line 4820
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4821
    const/4 v0, 0x1

    .line 4823
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTTSToolbarEnable()Z
    .locals 1

    .prologue
    .line 3539
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnPlay:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v0

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 2279
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2281
    if-eqz p3, :cond_0

    .line 2283
    const-string/jumbo v2, "SdCardUnMounted"

    invoke-virtual {p3, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-ne v2, v4, :cond_0

    .line 2285
    const-string/jumbo v2, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->isSdCardAction(Ljava/lang/String;)V

    .line 2289
    :cond_0
    const/4 v2, -0x1

    if-ne p2, v2, :cond_4

    .line 2290
    sparse-switch p1, :sswitch_data_0

    .line 2341
    :goto_0
    return-void

    .line 2292
    :sswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    .line 2294
    .local v0, "oldPath":Ljava/lang/String;
    const-string/jumbo v2, "key_new_file"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    .line 2296
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->updateOpenPathList(Ljava/lang/String;Ljava/lang/String;)V

    .line 2298
    invoke-static {}, Lcom/infraware/filemanager/database/recent/RecentFileManager;->getInstance()Lcom/infraware/filemanager/database/recent/RecentFileManager;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    invoke-virtual {v2, p0, v3}, Lcom/infraware/filemanager/database/recent/RecentFileManager;->InsertFileInfoToDB(Landroid/content/Context;Ljava/lang/String;)V

    .line 2300
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onMediaDBBroadCast(Ljava/lang/String;)V

    .line 2302
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isNewFile()Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_saveBehavior:I

    if-ne v2, v4, :cond_1

    .line 2303
    new-instance v2, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;

    invoke-direct {v2, p0, v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Z)V

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;->start()V

    goto :goto_0

    .line 2305
    :cond_1
    new-instance v2, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;

    invoke-direct {v2, p0, v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Z)V

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;->start()V

    goto :goto_0

    .line 2309
    .end local v0    # "oldPath":Ljava/lang/String;
    :sswitch_1
    invoke-direct {p0, p3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setPreferences(Landroid/content/Intent;)V

    .line 2310
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setShowMode(I)V

    .line 2311
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 2312
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->actionBarVisible(Z)V

    .line 2313
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->insertThumbnail()V

    .line 2315
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isChanged()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isNewFile()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2316
    invoke-direct {p0, p3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->directChangeEncoding(Landroid/content/Intent;)V

    .line 2318
    :cond_3
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->callOnClick()Z

    goto :goto_0

    .line 2322
    :sswitch_2
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onPrint()V

    goto :goto_0

    .line 2325
    :sswitch_3
    const-string/jumbo v2, "Bookmark"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v1

    .line 2329
    .local v1, "pos":[I
    new-instance v2, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;

    invoke-direct {v2, p0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;[I)V

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->start()V

    goto/16 :goto_0

    .line 2335
    .end local v1    # "pos":[I
    :cond_4
    packed-switch p1, :pswitch_data_0

    goto/16 :goto_0

    .line 2337
    :pswitch_0
    invoke-direct {p0, p2, p3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->resultWebStorage(ILandroid/content/Intent;)V

    goto/16 :goto_0

    .line 2290
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x8 -> :sswitch_3
        0xc -> :sswitch_0
        0xf -> :sswitch_2
    .end sparse-switch

    .line 2335
    :pswitch_data_0
    .packed-switch 0x32
        :pswitch_0
    .end packed-switch
.end method

.method protected onChangedFromMultiwindowToNormalWindow()V
    .locals 0

    .prologue
    .line 5630
    return-void
.end method

.method protected onChangedFromNormalWindowToMultiWindow()V
    .locals 1

    .prologue
    .line 5613
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_menuPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_menuPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5614
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_menuPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->dismiss()V

    .line 5616
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_optionMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_optionMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5617
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_optionMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->dismissPopupWindow()Z

    .line 5619
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_formaticonMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_formaticonMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5620
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_formaticonMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->dismissPopupWindow()Z

    .line 5622
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_findopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_findopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 5623
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_findopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->dismissPopupWindow()Z

    .line 5625
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_replaceopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_replaceopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 5626
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_replaceopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->dismissPopupWindow()Z

    .line 5627
    :cond_4
    return-void
.end method

.method public onCheckboxClicked(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1983
    move-object v1, p1

    check-cast v1, Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 1986
    .local v0, "checked":Z
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1991
    :goto_0
    return-void

    .line 1988
    :pswitch_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 1986
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b003c
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 3430
    invoke-static {p0}, Lcom/infraware/common/define/CMModelDefine$Diotek;->USE_DICTIONARY_SEARCH(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v0, :cond_0

    .line 3431
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->hidePanel()V

    .line 3433
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 3530
    :goto_0
    return-void

    .line 3435
    :sswitch_0
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;

    invoke-direct {v0, p0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Z)V

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->start()V

    goto :goto_0

    .line 3438
    :sswitch_1
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;

    invoke-direct {v0, p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Z)V

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$UndoRedoProgress;->start()V

    goto :goto_0

    .line 3452
    :sswitch_2
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setShowMode(I)V

    goto :goto_0

    .line 3456
    :sswitch_3
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->showPreferences()V

    goto :goto_0

    .line 3459
    :sswitch_4
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hideSoftKeyboard()Z

    .line 3460
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->removePopupCallback()V

    .line 3462
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnMenu:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 3464
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_optionMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->optionPopupwindow()V

    goto :goto_0

    .line 3467
    :sswitch_5
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hideSoftKeyboard()Z

    .line 3468
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->removePopupCallback()V

    .line 3470
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindOption:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 3472
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_findopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->optionPopupwindow()V

    .line 3473
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_findopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachCase:Z

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->setCheckMatchCase(Z)V

    .line 3474
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_findopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachWholeWord:Z

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->setCheckWholeWord(Z)V

    goto :goto_0

    .line 3478
    :sswitch_6
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;

    invoke-direct {v0, p0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;I)V

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->start()V

    goto :goto_0

    .line 3481
    :sswitch_7
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;

    invoke-direct {v0, p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;I)V

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->start()V

    goto :goto_0

    .line 3484
    :sswitch_8
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hideSoftKeyboard()Z

    .line 3485
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->removePopupCallback()V

    .line 3487
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceOption:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 3489
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_replaceopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->optionPopupwindow()V

    .line 3490
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_replaceopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachCase:Z

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->setCheckMatchCase(Z)V

    .line 3491
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_replaceopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachWholeWord:Z

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->setCheckWholeWord(Z)V

    goto/16 :goto_0

    .line 3495
    :sswitch_9
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;

    invoke-direct {v0, p0, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;I)V

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->start()V

    goto/16 :goto_0

    .line 3498
    :sswitch_a
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceProgress;->start()V

    goto/16 :goto_0

    .line 3501
    :sswitch_b
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->start()V

    goto/16 :goto_0

    .line 3504
    :sswitch_c
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_TTSmanager:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->doneTTS()V

    goto/16 :goto_0

    .line 3508
    :sswitch_d
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_TTSmanager:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->rewind()V

    .line 3509
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->ttsButtonImageChange(Z)V

    goto/16 :goto_0

    .line 3512
    :sswitch_e
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onPlayPauseTTSButton()V

    goto/16 :goto_0

    .line 3516
    :sswitch_f
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_TTSmanager:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->forward()V

    .line 3517
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->ttsButtonImageChange(Z)V

    goto/16 :goto_0

    .line 3520
    :sswitch_10
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hideSoftKeyboard()Z

    .line 3521
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->removePopupCallback()V

    .line 3523
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFormatIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 3525
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_formaticonMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->optionPopupwindow()V

    goto/16 :goto_0

    .line 3433
    :sswitch_data_0
    .sparse-switch
        0x7f0b0231 -> :sswitch_c
        0x7f0b0249 -> :sswitch_10
        0x7f0b024a -> :sswitch_0
        0x7f0b024b -> :sswitch_1
        0x7f0b024c -> :sswitch_2
        0x7f0b024d -> :sswitch_3
        0x7f0b024e -> :sswitch_4
        0x7f0b0250 -> :sswitch_5
        0x7f0b0251 -> :sswitch_7
        0x7f0b0252 -> :sswitch_6
        0x7f0b0257 -> :sswitch_8
        0x7f0b0258 -> :sswitch_9
        0x7f0b025c -> :sswitch_a
        0x7f0b025d -> :sswitch_b
        0x7f0b0269 -> :sswitch_d
        0x7f0b026a -> :sswitch_e
        0x7f0b026b -> :sswitch_f
    .end sparse-switch
.end method

.method public onCloseOpenLoadingProgress()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2751
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mIsContentSearch:Z

    if-eqz v0, :cond_0

    .line 2753
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setShowMode(I)V

    .line 2754
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mContentSearchWord:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2755
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mIsContentSearch:Z

    .line 2756
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;

    invoke-direct {v0, p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;I)V

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->start()V

    .line 2758
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 12
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v11, 0x4

    const/4 v8, 0x3

    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v7, 0x2

    .line 1724
    const/4 v0, 0x0

    .line 1726
    .local v0, "isSamsungClipboardOpened":Z
    const/4 v4, 0x0

    .local v4, "selStart":I
    const/4 v3, 0x0

    .line 1727
    .local v3, "selEnd":I
    const/4 v1, 0x0

    .line 1729
    .local v1, "isSelected":Z
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSamsungClipboard()Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1730
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSamsungClipboard()Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

    move-result-object v5

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->isSamsungClipboardOpened()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1731
    const/4 v0, 0x1

    .line 1735
    :cond_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v5, :cond_1

    .line 1736
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v5, p1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1738
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v5

    if-ne v5, v7, :cond_2

    .line 1739
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Findedt:Ljava/lang/String;

    .line 1740
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v5

    iput v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_FindedtPosition:I

    .line 1743
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v5

    if-eq v5, v7, :cond_3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v5

    if-ne v5, v8, :cond_4

    .line 1744
    :cond_3
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v4

    .line 1745
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v3

    .line 1747
    if-eq v4, v3, :cond_4

    .line 1748
    const/4 v1, 0x1

    .line 1752
    :cond_4
    iget-object v5, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->getLocaleType(Ljava/util/Locale;)I

    move-result v2

    .line 1753
    .local v2, "nlocale":I
    iget v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nLocaleType:I

    if-eq v5, v2, :cond_5

    .line 1754
    iput v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nLocaleType:I

    .line 1755
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onLocaleChange()V

    .line 1757
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getViewMode()I

    move-result v5

    if-nez v5, :cond_5

    .line 1758
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->requestFocus()Z

    .line 1759
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->showSoftKeyboard()Z

    .line 1762
    :cond_5
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1787
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_findopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->dismissPopupWindow()Z

    .line 1788
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_replaceopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->dismissPopupWindow()Z

    .line 1790
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mDlgSearch:Landroid/app/AlertDialog;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mDlgSearch:Landroid/app/AlertDialog;

    invoke-virtual {v5}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1791
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mDlgSearch:Landroid/app/AlertDialog;

    invoke-virtual {v5}, Landroid/app/AlertDialog;->dismiss()V

    .line 1792
    const/16 v5, 0x2c

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->removeDialog(I)V

    .line 1793
    const/16 v5, 0x2c

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->showDialog(I)V

    .line 1796
    :cond_6
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_optionMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    if-eqz v5, :cond_7

    .line 1797
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_optionMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->dismissPopupWindow()Z

    .line 1799
    :cond_7
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_menuPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    if-eqz v5, :cond_8

    .line 1800
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_menuPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->dismiss()V

    .line 1802
    :cond_8
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_formaticonMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    if-eqz v5, :cond_9

    .line 1803
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_formaticonMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->dismissPopupWindow()Z

    .line 1805
    :cond_9
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v5

    if-ne v5, v7, :cond_a

    .line 1806
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Findedt:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1807
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;

    iget v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_FindedtPosition:I

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setSelection(I)V

    .line 1810
    :cond_a
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v5

    if-eq v5, v7, :cond_b

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v5

    if-ne v5, v8, :cond_c

    .line 1811
    :cond_b
    if-eqz v1, :cond_c

    .line 1812
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5, v4, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(II)V

    .line 1816
    :cond_c
    if-eqz v0, :cond_d

    .line 1817
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Handler:Landroid/os/Handler;

    new-instance v6, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$14;

    invoke-direct {v6, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$14;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    const-wide/16 v7, 0x12c

    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1825
    :cond_d
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    if-eqz v5, :cond_e

    .line 1826
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->dismissPopupNotHideKeyboard()V

    .line 1828
    :cond_e
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v5

    if-ne v5, v11, :cond_11

    .line 1829
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v5

    if-eqz v5, :cond_11

    .line 1830
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v5

    if-ne v5, v11, :cond_11

    .line 1831
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getTTSManager()Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->getTTSMode()I

    move-result v5

    if-ne v5, v9, :cond_f

    .line 1832
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getTTSManager()Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    move-result-object v5

    invoke-virtual {v5, v10}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->setTTSMode(I)V

    .line 1835
    :cond_f
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_TTSmanager:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->isSpeaking()Z

    move-result v5

    if-ne v9, v5, :cond_10

    .line 1836
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_TTSmanager:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->pause()V

    .line 1839
    :cond_10
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Handler:Landroid/os/Handler;

    new-instance v6, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$15;

    invoke-direct {v6, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$15;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    const-wide/16 v7, 0x1f4

    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1852
    :cond_11
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v5

    if-ne v5, v9, :cond_12

    .line 1853
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5, v10}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setFlingFlag(Z)V

    .line 1855
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionEnd()I

    move-result v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionStart()I

    move-result v6

    sub-int/2addr v5, v6

    if-lez v5, :cond_12

    .line 1856
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionEnd()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    .line 1859
    :cond_12
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const v6, 0x7f0b0261

    .line 418
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 420
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->requestWindowFeature(I)Z

    .line 422
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 424
    invoke-static {p0}, Lcom/infraware/common/util/Utils;->fullScreenActivity(Landroid/app/Activity;)V

    .line 428
    :cond_0
    invoke-static {}, Lcom/infraware/common/util/Utils;->isL()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 429
    const v4, 0x103012c

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setTheme(I)V

    .line 431
    :cond_1
    invoke-static {}, Lcom/infraware/polarisoffice5/MyApplication;->getInstance()Lcom/infraware/polarisoffice5/MyApplication;

    move-result-object v4

    invoke-virtual {v4, p0}, Lcom/infraware/polarisoffice5/MyApplication;->setCurrentTextEditor(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    .line 432
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4}, Lcom/infraware/common/util/Utils;->getCurrentLocaleType(Landroid/content/res/Resources;)I

    move-result v4

    iput v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nLocaleType:I

    .line 434
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    iput v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nOrientation:I

    .line 438
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->bufferClear()V

    .line 441
    const v4, 0x7f030057

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setContentView(I)V

    .line 443
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setDisplayType()V

    .line 445
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->loadAvailableCharsetList()V

    .line 446
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->initControls()V

    .line 448
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setValueFromIntent()V

    .line 450
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setEventListener()V

    .line 451
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->loadEditConfig()V

    .line 453
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->openFile(Ljava/lang/String;)V

    .line 456
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mContentSearchWord:Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 457
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mIsContentSearch:Z

    .line 465
    :cond_2
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 466
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string/jumbo v4, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 467
    const-string/jumbo v4, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 468
    new-instance v4, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ScreenReceiver;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ScreenReceiver;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$1;)V

    iput-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_br:Landroid/content/BroadcastReceiver;

    .line 469
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_br:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 473
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 474
    invoke-static {}, Lcom/infraware/common/util/Utils;->isJB()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 475
    new-instance v4, Lcom/infraware/common/util/SbeamHelper;

    const-string/jumbo v5, "text/DirectSharePolarisEditor"

    invoke-direct {v4, p0, v5}, Lcom/infraware/common/util/SbeamHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    .line 476
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 477
    .local v1, "intentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v4, "com.android.systemui.statusbar.COLLAPSED"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 478
    new-instance v4, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$1;

    invoke-direct {v4, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$1;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    iput-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 490
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 496
    .end local v1    # "intentFilter":Landroid/content/IntentFilter;
    :cond_3
    :goto_0
    new-instance v4, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    invoke-direct {v4, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    iput-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mKeyContoller:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    .line 515
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onEditTextTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 516
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplaceFind:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onEditTextTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 517
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplace:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onEditTextTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 519
    sget-object v4, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->TEMP_PATH:Ljava/lang/String;

    invoke-static {v4, v3, p0}, Lcom/infraware/common/util/FileUtils;->makeDirectory(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;

    .line 522
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v4, v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setNextFocusDownId(I)V

    .line 523
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v4, v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setNextFocusForwardId(I)V

    .line 524
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v4, v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setNextFocusUpId(I)V

    .line 525
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v4, v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setNextFocusLeftId(I)V

    .line 526
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v4, v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setNextFocusRightId(I)V

    .line 528
    iput-object p0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    .line 530
    invoke-static {}, Lcom/infraware/common/util/Utils;->isKKOver()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 531
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onCreateOnKKOver()V

    .line 534
    :cond_4
    invoke-static {}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getInstance()Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    move-result-object v4

    invoke-virtual {v4, p0}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->isNormalWindow(Landroid/app/Activity;)Z

    move-result v4

    if-nez v4, :cond_6

    :goto_1
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mIsMultiWindow:Z

    .line 535
    return-void

    .line 494
    :cond_5
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setNfcCallback()V

    goto :goto_0

    :cond_6
    move v2, v3

    .line 534
    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 842
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->saveTextInfo(Ljava/lang/String;)V

    .line 843
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v5, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->saveProcess(Ljava/lang/String;ZZ)V

    .line 846
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getPd()Landroid/app/ProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getPd()Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 847
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getPd()Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V

    .line 849
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    if-eqz v2, :cond_1

    .line 850
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 852
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_br:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_2

    .line 853
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_br:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 855
    :cond_2
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_TTSmanager:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    if-eqz v2, :cond_3

    .line 856
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_TTSmanager:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->unregisterReceiver()V

    .line 858
    :cond_3
    sget-object v2, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_currentOpenPathList:Ljava/util/ArrayList;

    if-eqz v2, :cond_4

    .line 859
    sget-object v2, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_currentOpenPathList:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 860
    .local v1, "index":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_4

    .line 861
    sget-object v2, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_currentOpenPathList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 864
    .end local v1    # "index":I
    :cond_4
    invoke-static {}, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->folderInitialize()V

    .line 865
    new-instance v0, Ljava/io/File;

    sget-object v2, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->PATH_PRINTSTORAGE:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 866
    .local v0, "dirPath":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 867
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 870
    :cond_5
    sget-object v2, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->OFFICE_ROOT_PATH:Ljava/lang/String;

    invoke-static {v2, v5}, Lcom/infraware/common/util/FileUtils;->deleteDirectory(Ljava/lang/String;Z)V

    .line 873
    invoke-static {p0}, Lcom/infraware/common/define/CMModelDefine$Diotek;->USE_DICTIONARY_SEARCH(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v2, :cond_6

    .line 875
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->stopDictionaryService()V

    .line 876
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$Diotek;->initialize()V

    .line 879
    :cond_6
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 882
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_7

    .line 883
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 884
    iput-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 888
    :cond_7
    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mIsChangedFontSizePref:Z

    if-eqz v2, :cond_8

    .line 889
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v2

    const/16 v3, 0x15

    iget v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mTempPreferenceFontSize:I

    invoke-virtual {v2, p0, v3, v4}, Lcom/infraware/common/config/RuntimeConfig;->setIntPreference(Landroid/content/Context;II)V

    .line 891
    :cond_8
    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mIsChangedTextEncodingPref:Z

    if-eqz v2, :cond_9

    .line 892
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v2

    const/16 v3, 0x17

    iget v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mTempPreferenceTextEncoding:I

    invoke-virtual {v2, p0, v3, v4}, Lcom/infraware/common/config/RuntimeConfig;->setIntPreference(Landroid/content/Context;II)V

    .line 897
    :cond_9
    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mbFinishCalled:Z

    if-nez v2, :cond_a

    .line 898
    invoke-static {}, Lcom/infraware/polarisoffice5/MyApplication;->getInstance()Lcom/infraware/polarisoffice5/MyApplication;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/infraware/polarisoffice5/MyApplication;->setCurrentTextEditor(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    .line 905
    :cond_a
    return-void
.end method

.method public onFontSizeChanged(I)V
    .locals 1
    .param p1, "aValue"    # I

    .prologue
    .line 909
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mIsChangedFontSizePref:Z

    .line 910
    iput p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mTempPreferenceFontSize:I

    .line 911
    return-void
.end method

.method public onGooleSearch(Ljava/lang/String;)V
    .locals 11
    .param p1, "searchStr"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 5526
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    const/16 v4, 0x135

    invoke-static {v3, v4}, Lcom/infraware/common/define/CMModelDefine;->isShowConnetNetWorkPopup(Landroid/content/Context;I)Z

    move-result v3

    if-ne v3, v8, :cond_2

    .line 5528
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mSearchGoogleString:Ljava/lang/String;

    .line 5529
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 5530
    .local v0, "inflater1":Landroid/view/LayoutInflater;
    const v3, 0x7f030006

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 5532
    .local v1, "networkView":Landroid/view/View;
    const v3, 0x7f0b003b

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 5533
    .local v2, "tv":Landroid/widget/TextView;
    const v3, 0x7f0b003c

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mCheckBox:Landroid/widget/CheckBox;

    .line 5534
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/infraware/common/util/Utils;->getIsConnectWlan(Landroid/app/Activity;)Z

    move-result v3

    if-ne v3, v8, :cond_0

    .line 5535
    const v3, 0x7f07005d

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 5536
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_WLAN_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onGoolgeSearchClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v7, v6, v8

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v7, v6, v10

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    .line 5553
    .end local v0    # "inflater1":Landroid/view/LayoutInflater;
    .end local v1    # "networkView":Landroid/view/View;
    .end local v2    # "tv":Landroid/widget/TextView;
    :goto_0
    return-void

    .line 5541
    .restart local v0    # "inflater1":Landroid/view/LayoutInflater;
    .restart local v1    # "networkView":Landroid/view/View;
    .restart local v2    # "tv":Landroid/widget/TextView;
    :cond_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/infraware/common/util/Utils;->getIsConnectMobileNetwork(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 5542
    const v3, 0x7f07005b

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 5543
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_MOBILE_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onGoolgeSearchClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v7, v6, v8

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v7, v6, v10

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto :goto_0

    .line 5547
    :cond_1
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_NETWORK_NOT_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto :goto_0

    .line 5551
    .end local v0    # "inflater1":Landroid/view/LayoutInflater;
    .end local v1    # "networkView":Landroid/view/View;
    .end local v2    # "tv":Landroid/widget/TextView;
    :cond_2
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->connectGooleSearch(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    .line 2527
    sparse-switch p1, :sswitch_data_0

    .line 2605
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :cond_1
    :goto_1
    return v0

    .line 2529
    :sswitch_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_menuPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    if-eqz v1, :cond_2

    .line 2530
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_menuPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2531
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_menuPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->dismiss()V

    goto :goto_1

    .line 2536
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_formaticonMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    if-eqz v1, :cond_3

    .line 2537
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_formaticonMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2538
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_formaticonMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->dismissPopupWindow()Z

    goto :goto_1

    .line 2543
    :cond_3
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_optionMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    if-eqz v1, :cond_4

    .line 2544
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_optionMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2545
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_optionMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->dismissPopupWindow()Z

    goto :goto_1

    .line 2550
    :cond_4
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_findopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    if-eqz v1, :cond_5

    .line 2551
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_findopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2552
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_findopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->dismissPopupWindow()Z

    goto :goto_1

    .line 2557
    :cond_5
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_replaceopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    if-eqz v1, :cond_6

    .line 2558
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_replaceopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2559
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_replaceopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->dismissPopupWindow()Z

    goto :goto_1

    .line 2564
    :cond_6
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isToastpopupShowing()Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isMorepopupShowing()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2565
    :cond_7
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->dismissPopup()V

    goto :goto_1

    .line 2569
    :cond_8
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isShown()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2571
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->hidePanel()V

    goto/16 :goto_1

    .line 2574
    :cond_9
    iget v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nShowMode:I

    if-eq v1, v3, :cond_a

    iget v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nShowMode:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_b

    .line 2575
    :cond_a
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setShowMode(I)V

    .line 2576
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v3, :cond_1

    .line 2578
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bLandActionBar:Z

    if-nez v1, :cond_1

    .line 2579
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 2582
    :cond_b
    iget v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nShowMode:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_c

    .line 2583
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_TTSmanager:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->doneTTS()V

    goto/16 :goto_1

    .line 2585
    :cond_c
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isChanged()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2587
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isShowSoftKey()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2588
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hideSoftKeyboard()Z

    goto/16 :goto_0

    .line 2593
    :sswitch_1
    iget v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nShowMode:I

    if-ne v1, v3, :cond_0

    .line 2594
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2595
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setActionbarFocus(Z)V

    .line 2596
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->requestFocusFromTouch()Z

    goto/16 :goto_1

    .line 2527
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x14 -> :sswitch_1
    .end sparse-switch
.end method

.method public onLocaleChange()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const v5, 0x7f0701d6

    const/16 v6, 0xcc

    .line 1872
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 1873
    const-string/jumbo v3, "findtext"

    invoke-virtual {p0, v3, v7}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 1874
    .local v2, "pref":Landroid/content/SharedPreferences;
    const-string/jumbo v3, "findtext"

    const-string/jumbo v4, "0"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1875
    .local v1, "findtext":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_e

    .line 1876
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mFindText:Ljava/lang/String;

    .line 1877
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1885
    .end local v1    # "findtext":Ljava/lang/String;
    .end local v2    # "pref":Landroid/content/SharedPreferences;
    :cond_0
    :goto_0
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1886
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->onLocaleChanged()V

    .line 1889
    :cond_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_optionMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1890
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_optionMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->optionPopupwindow()V

    .line 1891
    :cond_2
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_formaticonMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1892
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_formaticonMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->optionPopupwindow()V

    .line 1893
    :cond_3
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_findopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1894
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_findopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->optionPopupwindow()V

    .line 1895
    :cond_4
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_replaceopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1896
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_replaceopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->optionPopupwindow()V

    .line 1898
    :cond_5
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;

    if-eqz v3, :cond_6

    .line 1900
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1901
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setHintTextColor(I)V

    .line 1903
    :cond_6
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplaceFind:Landroid/widget/EditText;

    if-eqz v3, :cond_7

    .line 1905
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplaceFind:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1906
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplaceFind:Landroid/widget/EditText;

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setHintTextColor(I)V

    .line 1908
    :cond_7
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplace:Landroid/widget/EditText;

    if-eqz v3, :cond_8

    .line 1910
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplace:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0701d7

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1911
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplace:Landroid/widget/EditText;

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setHintTextColor(I)V

    .line 1915
    :cond_8
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_tvTTSTitle:Landroid/widget/TextView;

    if-eqz v3, :cond_9

    .line 1916
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_tvTTSTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f07030a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1918
    :cond_9
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v3, :cond_b

    .line 1919
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isShown()Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_a

    .line 1920
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->hidePanel()V

    .line 1921
    :cond_a
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->onLocaleChange()V

    .line 1925
    :cond_b
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_alertDialog:Landroid/app/AlertDialog;

    if-eqz v3, :cond_c

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_alertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 1926
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_alertDialog:Landroid/app/AlertDialog;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0701ad

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 1927
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_msg:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0701c3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1928
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_msg_delaytime:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070301

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070302

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1930
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_alertDialog:Landroid/app/AlertDialog;

    const/4 v4, -0x2

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 1931
    .local v0, "button":Landroid/widget/Button;
    const v3, 0x7f07005f

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1932
    invoke-virtual {v0}, Landroid/widget/Button;->invalidate()V

    .line 1936
    .end local v0    # "button":Landroid/widget/Button;
    :cond_c
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTSbar:Landroid/widget/LinearLayout;

    if-eqz v3, :cond_d

    .line 1937
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnPrev:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070069

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1938
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnNext:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070067

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1940
    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mbIsTTSPlayFlag:Z

    if-eqz v3, :cond_f

    .line 1941
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnPlay:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f07006a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1945
    :cond_d
    :goto_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnDone:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070121

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1947
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->dismissPopup()V

    .line 1948
    return-void

    .line 1880
    .restart local v1    # "findtext":Ljava/lang/String;
    .restart local v2    # "pref":Landroid/content/SharedPreferences;
    :cond_e
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1881
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setHintTextColor(I)V

    goto/16 :goto_0

    .line 1943
    .end local v1    # "findtext":Ljava/lang/String;
    .end local v2    # "pref":Landroid/content/SharedPreferences;
    :cond_f
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnPlay:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070068

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public onPasteSamsungClipboard(Ljava/lang/String;)V
    .locals 1
    .param p1, "pasteStr"    # Ljava/lang/String;

    .prologue
    .line 4375
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->afterSamsungPaste(Ljava/lang/String;)V

    .line 4376
    return-void
.end method

.method protected onPause()V
    .locals 5

    .prologue
    .line 921
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->unregisterOnClickListener()V

    .line 923
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 924
    const-string/jumbo v3, "findtext"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 925
    .local v2, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 927
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 929
    .local v1, "findtext":Ljava/lang/String;
    const-string/jumbo v3, "findtext"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 930
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 932
    .end local v0    # "edit":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "findtext":Ljava/lang/String;
    .end local v2    # "pref":Landroid/content/SharedPreferences;
    :cond_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_optionMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    if-eqz v3, :cond_1

    .line 933
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_optionMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->dismissPopupWindow()Z

    .line 934
    :cond_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_menuPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    if-eqz v3, :cond_2

    .line 935
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_menuPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->dismiss()V

    .line 937
    :cond_2
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v3, :cond_3

    .line 938
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->onPause()V

    .line 941
    :cond_3
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_formaticonMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    if-eqz v3, :cond_4

    .line 942
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_formaticonMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->dismissPopupWindow()Z

    .line 944
    :cond_4
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_findopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    if-eqz v3, :cond_5

    .line 945
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_findopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->dismissPopupWindow()Z

    .line 947
    :cond_5
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 948
    return-void
.end method

.method public onPlayPauseTTSButton()V
    .locals 3

    .prologue
    .line 3556
    iget v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nShowMode:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 3557
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_TTSmanager:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->isPlay()Z

    move-result v0

    .line 3559
    .local v0, "flag":Z
    if-eqz v0, :cond_1

    .line 3563
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_TTSmanager:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->pause()V

    .line 3571
    .end local v0    # "flag":Z
    :cond_0
    :goto_0
    return-void

    .line 3568
    .restart local v0    # "flag":Z
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_TTSmanager:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->play()V

    goto :goto_0
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 539
    invoke-super {p0, p1}, Landroid/app/Activity;->onPostCreate(Landroid/os/Bundle;)V

    .line 541
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setChanged(Z)V

    .line 543
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bLandActionBar:Z

    if-nez v1, :cond_0

    .line 545
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getDisplaySize()[I

    move-result-object v0

    .line 547
    .local v0, "displayInfo":[I
    aget v1, v0, v3

    const/4 v2, 0x1

    aget v2, v0, v2

    if-ge v1, v2, :cond_2

    .line 548
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 554
    .end local v0    # "displayInfo":[I
    :cond_0
    :goto_0
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v1

    const/16 v2, 0x17

    const/4 v3, -0x1

    invoke-virtual {v1, p0, v2, v3}, Lcom/infraware/common/config/RuntimeConfig;->getIntPreference(Landroid/content/Context;II)I

    move-result v1

    iput v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nEncoding:I

    .line 556
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isNewFile()Z

    move-result v1

    if-nez v1, :cond_1

    .line 557
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->makeBackupFile()V

    .line 560
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setScrollThumbTimer()V

    .line 568
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Handler:Landroid/os/Handler;

    new-instance v2, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$2;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$2;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    const-wide/16 v3, 0x64

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 578
    return-void

    .line 550
    .restart local v0    # "displayInfo":[I
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 2517
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 2518
    packed-switch p1, :pswitch_data_0

    .line 2523
    :goto_0
    return-void

    .line 2520
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->removeDialog(I)V

    goto :goto_0

    .line 2518
    nop

    :pswitch_data_0
    .packed-switch 0x2c
        :pswitch_0
    .end packed-switch
.end method

.method public onRequestSamsungAppsPolarisOffice()V
    .locals 13

    .prologue
    const/4 v8, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 3864
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->isOfficeInstalled()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 3866
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 3867
    .local v1, "it":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string/jumbo v6, "key_filename"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3868
    .local v2, "key_filename":Ljava/lang/String;
    const-string/jumbo v5, "com.infraware.polarisoffice5"

    const-string/jumbo v6, "com.infraware.polarisoffice5.OfficeLauncherActivity"

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3869
    const-string/jumbo v5, "key_filename"

    invoke-virtual {v1, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3870
    const-string/jumbo v5, "previe"

    invoke-virtual {v1, v5, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3871
    const-string/jumbo v5, "sort_order"

    invoke-virtual {v1, v5, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3873
    const-string/jumbo v5, "android.intent.extra.STREAM"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "file://"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "/Sanity%20test.docx"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3874
    const-string/jumbo v5, "from-myfiles"

    invoke-virtual {v1, v5, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3875
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->startActivity(Landroid/content/Intent;)V

    .line 3876
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->finish()V

    .line 3908
    .end local v1    # "it":Landroid/content/Intent;
    .end local v2    # "key_filename":Ljava/lang/String;
    :goto_0
    return-void

    .line 3881
    :cond_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    const/16 v6, 0x132

    invoke-static {v5, v6}, Lcom/infraware/common/define/CMModelDefine;->isShowConnetNetWorkPopup(Landroid/content/Context;I)Z

    move-result v5

    if-ne v5, v11, :cond_3

    .line 3884
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    const-string/jumbo v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 3885
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f030006

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 3887
    .local v3, "networkView":Landroid/view/View;
    const v5, 0x7f0b003b

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 3888
    .local v4, "tv":Landroid/widget/TextView;
    const v5, 0x7f0b003c

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    iput-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mCheckBox:Landroid/widget/CheckBox;

    .line 3889
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->getIsConnectWlan(Landroid/app/Activity;)Z

    move-result v5

    if-ne v5, v11, :cond_1

    .line 3890
    const v5, 0x7f07005d

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 3891
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    sget-object v7, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_WLAN_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v8, v8, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    aput-object v9, v8, v10

    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onSamsungAppsPolairsOfficeClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v9, v8, v11

    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v9, v8, v12

    invoke-virtual {v5, v6, v7, v8}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto :goto_0

    .line 3896
    :cond_1
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->getIsConnectMobileNetwork(Landroid/app/Activity;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 3897
    const v5, 0x7f07005b

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 3898
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    sget-object v7, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_MOBILE_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v8, v8, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    aput-object v9, v8, v10

    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onSamsungAppsPolairsOfficeClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v9, v8, v11

    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v9, v8, v12

    invoke-virtual {v5, v6, v7, v8}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 3902
    :cond_2
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    sget-object v7, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_NETWORK_NOT_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v8, v10, [Ljava/lang/Object;

    invoke-virtual {v5, v6, v7, v8}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 3906
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    .end local v3    # "networkView":Landroid/view/View;
    .end local v4    # "tv":Landroid/widget/TextView;
    :cond_3
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->connectUriSamungAppsPolarisOffice()V

    goto/16 :goto_0
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 960
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->registerOnClickListener()V

    .line 962
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hasFocus()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTS:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_4

    .line 963
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getViewMode()I

    move-result v2

    if-nez v2, :cond_3

    .line 964
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->showSoftKeyboard()Z

    .line 976
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_tvTitle:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 977
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 980
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 981
    invoke-static {}, Lcom/infraware/common/util/Utils;->isJB()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 982
    const/4 v2, 0x1

    new-array v0, v2, [Landroid/net/Uri;

    .line 983
    .local v0, "filePath":[Landroid/net/Uri;
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    aput-object v2, v0, v4

    .line 984
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v0, p0, v3}, Lcom/infraware/common/util/SbeamHelper;->setBeamUris([Landroid/net/Uri;Landroid/app/Activity;Landroid/content/Context;)V

    .line 988
    .end local v0    # "filePath":[Landroid/net/Uri;
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 989
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setOpenDialog(Ljava/lang/String;)V

    .line 991
    :cond_2
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_tvTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 992
    return-void

    .line 966
    :cond_3
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hideSoftKeyboard()Z

    goto :goto_0

    .line 969
    :cond_4
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 971
    new-instance v1, Lcom/infraware/polarisoffice5/common/ImmManager;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/common/ImmManager;-><init>(Landroid/app/Activity;)V

    .line 972
    .local v1, "imm":Lcom/infraware/polarisoffice5/common/ImmManager;
    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/ImmManager;->showDelayedIme()V

    goto :goto_0
.end method

.method public onSetting()V
    .locals 3

    .prologue
    .line 2104
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setShowSoftKeyBoard(Z)V

    .line 2105
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/infraware/polarisoffice5/setting/SettingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2106
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "key_version"

    const v2, 0x7f070322

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2107
    const-string/jumbo v1, "key_interanl_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2108
    const/16 v1, 0x1015

    invoke-virtual {p0, v0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2109
    return-void
.end method

.method public onShare()V
    .locals 11

    .prologue
    const/4 v6, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 5479
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    const/16 v4, 0x134

    invoke-static {v3, v4}, Lcom/infraware/common/define/CMModelDefine;->isShowConnetNetWorkPopup(Landroid/content/Context;I)Z

    move-result v3

    if-ne v3, v8, :cond_2

    .line 5482
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 5483
    .local v0, "inflater1":Landroid/view/LayoutInflater;
    const v3, 0x7f030006

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 5485
    .local v1, "networkView":Landroid/view/View;
    const v3, 0x7f0b003b

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 5486
    .local v2, "tv":Landroid/widget/TextView;
    const v3, 0x7f0b003c

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mCheckBox:Landroid/widget/CheckBox;

    .line 5487
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/infraware/common/util/Utils;->getIsConnectWlan(Landroid/app/Activity;)Z

    move-result v3

    if-ne v3, v8, :cond_0

    .line 5488
    const v3, 0x7f07005d

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 5489
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_WLAN_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onRunShareClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v7, v6, v8

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v7, v6, v10

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    .line 5506
    .end local v0    # "inflater1":Landroid/view/LayoutInflater;
    .end local v1    # "networkView":Landroid/view/View;
    .end local v2    # "tv":Landroid/widget/TextView;
    :goto_0
    return-void

    .line 5494
    .restart local v0    # "inflater1":Landroid/view/LayoutInflater;
    .restart local v1    # "networkView":Landroid/view/View;
    .restart local v2    # "tv":Landroid/widget/TextView;
    :cond_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/infraware/common/util/Utils;->getIsConnectMobileNetwork(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 5495
    const v3, 0x7f07005b

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 5496
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_MOBILE_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onRunShareClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v7, v6, v8

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v7, v6, v10

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto :goto_0

    .line 5500
    :cond_1
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_NETWORK_NOT_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto :goto_0

    .line 5504
    .end local v0    # "inflater1":Landroid/view/LayoutInflater;
    .end local v1    # "networkView":Landroid/view/View;
    .end local v2    # "tv":Landroid/widget/TextView;
    :cond_2
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->connectOnShare()V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 953
    const-string/jumbo v0, "power"

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 954
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_TTSmanager:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->onPause()V

    .line 955
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 956
    return-void
.end method

.method public onTextEncodeChanged(I)V
    .locals 1
    .param p1, "aValue"    # I

    .prologue
    .line 914
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mIsChangedTextEncodingPref:Z

    .line 915
    iput p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mTempPreferenceTextEncoding:I

    .line 916
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 4036
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v2, v3

    .line 4038
    .local v2, "y":I
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 4103
    :cond_0
    :goto_0
    return v8

    .line 4040
    :pswitch_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBufferChanged()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 4041
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLoadingProgress()Z

    move-result v3

    if-nez v3, :cond_0

    .line 4042
    new-instance v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$BufferProgress;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$BufferProgress;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$BufferProgress;->start()V

    goto :goto_0

    .line 4045
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_1

    goto :goto_0

    .line 4048
    :pswitch_1
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 4049
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v3

    if-ne v3, v9, :cond_2

    .line 4050
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getTTSManager()Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->pause()V

    .line 4053
    :cond_2
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->asyncLoading()V

    .line 4054
    invoke-virtual {p1, v7}, Landroid/view/View;->setPressed(Z)V

    .line 4055
    iput v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nScrollThumbPos:I

    goto :goto_0

    .line 4058
    :pswitch_2
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->asyncLoading()V

    .line 4059
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v3

    if-nez v3, :cond_3

    .line 4060
    invoke-virtual {p1, v7}, Landroid/view/View;->setPressed(Z)V

    .line 4062
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    add-int/2addr v3, v2

    iget v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nScrollThumbPos:I

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nQsPos:I

    .line 4064
    iget v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nQsPos:I

    if-gez v3, :cond_4

    .line 4065
    iput v8, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nQsPos:I

    .line 4067
    :cond_4
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getHeight()I

    move-result v3

    iput v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nQsEditHeight:I

    .line 4069
    iget v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nQsPos:I

    iget v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_sizequickthumbsize:I

    add-int/2addr v3, v4

    iget v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nQsEditHeight:I

    if-le v3, v4, :cond_5

    .line 4070
    iget v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nQsEditHeight:I

    iget v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_sizequickthumbsize:I

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nQsPos:I

    .line 4072
    :cond_5
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_flScrollPos:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 4073
    .local v0, "params":Landroid/widget/LinearLayout$LayoutParams;
    iget v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nQsPos:I

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 4074
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_flScrollPos:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 4078
    .end local v0    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :pswitch_3
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getHeight()I

    move-result v3

    iput v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nQsEditHeight:I

    .line 4079
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nQsPos:I

    iget v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nQsEditHeight:I

    iget v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_sizequickthumbsize:I

    sub-int/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->scrollByThumb(II)V

    .line 4080
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setScrollThumbTimer()V

    .line 4082
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 4083
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v3

    if-ne v3, v9, :cond_0

    .line 4084
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getTTSManager()Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->getTTSMode()I

    move-result v3

    if-ne v3, v7, :cond_6

    .line 4085
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getTTSManager()Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    move-result-object v3

    invoke-virtual {v3, v8}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->setTTSMode(I)V

    .line 4088
    :cond_6
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getCurrentTopCaret()I

    move-result v1

    .line 4089
    .local v1, "topCaretPos":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3, v1, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setTTSSelection(II)V

    .line 4091
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getTTSManager()Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    move-result-object v4

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f07030b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->textReadAndPlay(Lcom/infraware/polarisoffice5/text/manager/TTSManager;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 4038
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0264
        :pswitch_0
    .end packed-switch

    .line 4045
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public onWindowStatusChanged(ZZZ)V
    .locals 3
    .param p1, "isMaximized"    # Z
    .param p2, "isMinimized"    # Z
    .param p3, "isPinup"    # Z

    .prologue
    .line 5591
    const-string/jumbo v0, "multiwindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onWindowStatusChanged(max["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "]min["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "]pinup["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5592
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onMultiWindowStatusChanged(Z)V

    .line 5593
    return-void

    .line 5592
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public optionPopupItemEvent(ILandroid/view/View;)V
    .locals 8
    .param p1, "event"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 3696
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_optionMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->dismissPopupWindow()Z

    .line 3697
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_formaticonMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->dismissPopupWindow()Z

    .line 3699
    packed-switch p1, :pswitch_data_0

    .line 3821
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 3701
    :pswitch_1
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v4

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v5

    sub-int/2addr v4, v5

    if-eqz v4, :cond_1

    .line 3702
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    .line 3705
    :cond_1
    invoke-direct {p0, v7}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setTTSMode(I)V

    .line 3706
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->dismissPopup()V

    .line 3707
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_TTSmanager:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->onTTS()V

    goto :goto_0

    .line 3715
    :pswitch_2
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v4}, Lcom/infraware/common/define/CMModelDefine$B;->USE_GOOGLE_PRINT(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_POLARIS_PRINT()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 3716
    :cond_2
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mOfficePrintManager:Lcom/infraware/polarisoffice5/print/OfficePrintManager;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    invoke-virtual {v4, v5, v6, v7}, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->print(Landroid/app/Activity;Lcom/infraware/polarisoffice5/text/control/EditCtrl;Ljava/lang/String;)V

    goto :goto_0

    .line 3721
    :cond_3
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_printmanager:Lcom/infraware/polarisoffice5/text/manager/PrintManager;

    if-nez v4, :cond_4

    .line 3722
    new-instance v4, Lcom/infraware/polarisoffice5/text/manager/PrintManager;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-direct {v4, v5, v6}, Lcom/infraware/polarisoffice5/text/manager/PrintManager;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;I)V

    iput-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_printmanager:Lcom/infraware/polarisoffice5/text/manager/PrintManager;

    .line 3724
    :cond_4
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_printmanager:Lcom/infraware/polarisoffice5/text/manager/PrintManager;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->isListenerRegistered()Z

    move-result v4

    if-nez v4, :cond_5

    .line 3725
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->printListenerInitialize()Z

    move-result v3

    .line 3726
    .local v3, "printinitresult":Z
    if-eqz v3, :cond_0

    .line 3727
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onPrint()V

    goto :goto_0

    .line 3730
    .end local v3    # "printinitresult":Z
    :cond_5
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onPrint()V

    goto :goto_0

    .line 3734
    :pswitch_3
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onSendEmail()V

    goto :goto_0

    .line 3741
    :pswitch_4
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->fileOperationInfo()V

    goto :goto_0

    .line 3744
    :pswitch_5
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onHelp()V

    goto :goto_0

    .line 3747
    :pswitch_6
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onSetting()V

    goto :goto_0

    .line 3751
    :pswitch_7
    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachCase:Z

    if-eqz v4, :cond_6

    .line 3752
    iput-boolean v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachCase:Z

    .line 3756
    :goto_1
    const/16 v4, 0xa

    if-ne p1, v4, :cond_7

    .line 3757
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_findopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    iget-boolean v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachCase:Z

    invoke-virtual {v4, v5}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->setCheckMatchCase(Z)V

    .line 3761
    :goto_2
    invoke-static {p2}, Lcom/infraware/common/util/Utils;->setCheckBoxAccessibility(Landroid/view/View;)V

    goto/16 :goto_0

    .line 3754
    :cond_6
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachCase:Z

    goto :goto_1

    .line 3759
    :cond_7
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_replaceopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    iget-boolean v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachCase:Z

    invoke-virtual {v4, v5}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->setCheckMatchCase(Z)V

    goto :goto_2

    .line 3766
    :pswitch_8
    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachWholeWord:Z

    if-eqz v4, :cond_8

    .line 3767
    iput-boolean v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachWholeWord:Z

    .line 3771
    :goto_3
    const/16 v4, 0xc

    if-ne p1, v4, :cond_9

    .line 3772
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_findopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    iget-boolean v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachWholeWord:Z

    invoke-virtual {v4, v5}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->setCheckWholeWord(Z)V

    .line 3776
    :goto_4
    invoke-static {p2}, Lcom/infraware/common/util/Utils;->setCheckBoxAccessibility(Landroid/view/View;)V

    goto/16 :goto_0

    .line 3769
    :cond_8
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachWholeWord:Z

    goto :goto_3

    .line 3774
    :cond_9
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_replaceopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    iget-boolean v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachWholeWord:Z

    invoke-virtual {v4, v5}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->setCheckWholeWord(Z)V

    goto :goto_4

    .line 3780
    :pswitch_9
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_findopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->dismissPopupWindow()Z

    .line 3781
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setShowMode(I)V

    goto/16 :goto_0

    .line 3784
    :pswitch_a
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_replaceopMenu:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->dismissPopupWindow()Z

    .line 3785
    const/4 v4, 0x2

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setShowMode(I)V

    goto/16 :goto_0

    .line 3790
    :pswitch_b
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->prepareEnterBookmarkActivity()[I

    move-result-object v2

    .line 3792
    .local v2, "pos":[I
    new-instance v0, Landroid/content/Intent;

    const-class v4, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;

    invoke-direct {v0, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3794
    .local v0, "bookmarkIntent":Landroid/content/Intent;
    const-string/jumbo v4, "istexteditoractivity"

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3796
    if-nez v2, :cond_a

    .line 3805
    :goto_5
    const-string/jumbo v4, "FilePath"

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3807
    const/16 v4, 0x8

    invoke-virtual {p0, v0, v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 3802
    :cond_a
    const-string/jumbo v4, "Bookmark"

    invoke-virtual {v0, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    goto :goto_5

    .line 3812
    .end local v0    # "bookmarkIntent":Landroid/content/Intent;
    .end local v2    # "pos":[I
    :pswitch_c
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onRequestSamsungAppsPolarisOffice()V

    goto/16 :goto_0

    .line 3817
    :pswitch_d
    new-instance v1, Landroid/content/Intent;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    const-class v5, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3818
    .local v1, "i":Landroid/content/Intent;
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 3699
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_d
        :pswitch_c
    .end packed-switch
.end method

.method public declared-synchronized replaceAllText(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 7
    .param p1, "strFind"    # Ljava/lang/String;
    .param p2, "strReplace"    # Ljava/lang/String;
    .param p3, "bMatchCase"    # Z
    .param p4, "bWholeWord"    # Z

    .prologue
    const/16 v6, 0x64

    .line 3399
    monitor-enter p0

    const/4 v2, -0x1

    .line 3400
    .local v2, "nOffset":I
    :try_start_0
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nReplaceCnt:I

    if-ge v0, v6, :cond_0

    .line 3401
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBlock:I

    .line 3402
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginPos:I

    .line 3405
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nReplaceCnt:I

    .line 3407
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setUndoOneAction(Z)V

    .line 3408
    :goto_0
    invoke-virtual {p0, p1, p3, p4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->replaceFindText(Ljava/lang/String;ZZ)I

    move-result v2

    const/4 v0, -0x1

    if-eq v2, v0, :cond_1

    .line 3409
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nReplaceCnt:I

    if-lt v0, v6, :cond_2

    .line 3418
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setUndoOneAction(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3419
    monitor-exit p0

    return-void

    .line 3412
    :cond_2
    :try_start_1
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nReplaceCnt:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nReplaceCnt:I

    .line 3415
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBlock:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v2

    move-object v4, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->replaceSubBuffer(IIILjava/lang/String;Ljava/lang/String;)V

    .line 3416
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v2

    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginPos:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 3399
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized replaceFindText(Ljava/lang/String;ZZ)I
    .locals 8
    .param p1, "strFind"    # Ljava/lang/String;
    .param p2, "bMatchCase"    # Z
    .param p3, "bWholeWord"    # Z

    .prologue
    .line 3331
    monitor-enter p0

    const/4 v2, -0x1

    .line 3332
    .local v2, "nOffset":I
    :try_start_0
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBlock:I

    invoke-virtual {v6, v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockBufferText(I)Ljava/lang/String;

    move-result-object v5

    .line 3335
    .local v5, "strText":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-gtz v6, :cond_0

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCount()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 3336
    const/4 v6, 0x0

    iput v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBlock:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v3, v2

    .line 3395
    .end local v2    # "nOffset":I
    .local v3, "nOffset":I
    :goto_0
    monitor-exit p0

    return v3

    .line 3340
    .end local v3    # "nOffset":I
    .restart local v2    # "nOffset":I
    :cond_0
    :try_start_1
    iget v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginPos:I

    .line 3341
    .local v1, "nCaretPos":I
    iget v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBlock:I

    if-lez v6, :cond_1

    .line 3342
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBlock:I

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCharCount(I)I

    move-result v6

    add-int/2addr v1, v6

    .line 3345
    :cond_1
    if-nez p2, :cond_2

    .line 3346
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 3347
    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    .line 3350
    :cond_2
    invoke-virtual {v5, p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    .line 3352
    if-eqz p3, :cond_3

    .line 3353
    invoke-direct {p0, v5, p1, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findWholeWordIndex(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    .line 3355
    :cond_3
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBlock:I

    .line 3356
    .local v0, "nBlock":I
    :cond_4
    :goto_1
    const/4 v6, -0x1

    if-ne v2, v6, :cond_6

    .line 3357
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-eq v0, v6, :cond_6

    .line 3358
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v6, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockBufferText(I)Ljava/lang/String;

    move-result-object v5

    .line 3359
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    add-int/lit8 v7, v0, -0x1

    invoke-virtual {v6, v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCharCount(I)I

    move-result v1

    .line 3361
    if-nez p2, :cond_5

    .line 3362
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 3363
    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    .line 3366
    :cond_5
    invoke-virtual {v5, p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    .line 3367
    if-eqz p3, :cond_4

    .line 3368
    invoke-direct {p0, v5, p1, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findWholeWordIndex(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    goto :goto_1

    .line 3374
    :cond_6
    if-nez v0, :cond_9

    .line 3375
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v6, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCharCount(I)I

    move-result v6

    if-le v2, v6, :cond_7

    .line 3376
    add-int/lit8 v0, v0, 0x1

    .line 3379
    :cond_7
    const/16 v6, 0xbb8

    if-lt v2, v6, :cond_8

    .line 3380
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v6, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCharCount(I)I

    move-result v6

    sub-int/2addr v2, v6

    .line 3393
    :cond_8
    :goto_2
    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBlock:I

    move v3, v2

    .line 3395
    .end local v2    # "nOffset":I
    .restart local v3    # "nOffset":I
    goto :goto_0

    .line 3382
    .end local v3    # "nOffset":I
    .restart local v2    # "nOffset":I
    :cond_9
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    add-int/lit8 v7, v0, -0x1

    invoke-virtual {v6, v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCharCount(I)I

    move-result v6

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCharCount(I)I

    move-result v7

    add-int v4, v6, v7

    .line 3384
    .local v4, "nTwoBlockSize":I
    if-lt v2, v4, :cond_a

    .line 3385
    add-int/lit8 v0, v0, 0x1

    .line 3386
    sub-int/2addr v2, v4

    goto :goto_2

    .line 3388
    :cond_a
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    add-int/lit8 v7, v0, -0x1

    invoke-virtual {v6, v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCharCount(I)I

    move-result v6

    if-lt v2, v6, :cond_8

    .line 3389
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    add-int/lit8 v7, v0, -0x1

    invoke-virtual {v6, v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCharCount(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v6

    sub-int/2addr v2, v6

    goto :goto_2

    .line 3331
    .end local v0    # "nBlock":I
    .end local v1    # "nCaretPos":I
    .end local v4    # "nTwoBlockSize":I
    .end local v5    # "strText":Ljava/lang/String;
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6
.end method

.method public declared-synchronized replaceText(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 5
    .param p1, "strFind"    # Ljava/lang/String;
    .param p2, "strReplace"    # Ljava/lang/String;
    .param p3, "bMatchCase"    # Z
    .param p4, "bWholeWord"    # Z

    .prologue
    .line 3254
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v1

    .line 3255
    .local v1, "nStart":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v0

    .line 3257
    .local v0, "nEnd":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 3259
    .local v2, "strSelected":Ljava/lang/String;
    if-nez p3, :cond_0

    .line 3260
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 3261
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 3264
    :cond_0
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3265
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setUndoOneAction(Z)V

    .line 3266
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3, v1, v0}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 3267
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3, v1, p2}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 3268
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setUndoOneAction(Z)V

    .line 3269
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setChanged(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3271
    :cond_1
    monitor-exit p0

    return-void

    .line 3254
    .end local v0    # "nEnd":I
    .end local v1    # "nStart":I
    .end local v2    # "strSelected":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized replaceViewProcess(Ljava/lang/String;IZ)V
    .locals 2
    .param p1, "strFind"    # Ljava/lang/String;
    .param p2, "nOffset"    # I
    .param p3, "bMessage"    # Z

    .prologue
    .line 3274
    monitor-enter p0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    .line 3275
    :try_start_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceNextFind:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 3276
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindSuccess:Z

    if-eqz v0, :cond_1

    .line 3277
    if-eqz p3, :cond_0

    .line 3289
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceNextFind:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 3307
    :cond_0
    :goto_0
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginBlock:I

    iget v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginPos:I

    invoke-direct {p0, v0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setPreSelection(II)V

    .line 3309
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindSuccess:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3314
    :goto_1
    monitor-exit p0

    return-void

    .line 3303
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceNextFind:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 3304
    const v0, 0x7f0701dc

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 3274
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 3311
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, p2

    invoke-virtual {v0, p2, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(II)V

    .line 3312
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFindSuccess:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public requestFocusFindeEditText()V
    .locals 1

    .prologue
    .line 5633
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    return-void
.end method

.method public returnLastDisplayPosition()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, -0x1

    const/4 v8, 0x0

    .line 2157
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-string/jumbo v7, "textinfo"

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 2159
    .local v3, "sP":Landroid/content/SharedPreferences;
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    const-string/jumbo v7, "null"

    invoke-interface {v3, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2161
    .local v2, "item":Ljava/lang/String;
    const-string/jumbo v6, "null"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2185
    :cond_0
    :goto_0
    return-void

    .line 2165
    :cond_1
    new-instance v4, Ljava/util/StringTokenizer;

    const-string/jumbo v6, "|"

    invoke-direct {v4, v2, v6}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2166
    .local v4, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 2167
    .local v5, "topcaret":I
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 2169
    .local v0, "curblock":I
    if-eq v5, v9, :cond_0

    .line 2173
    if-eq v0, v9, :cond_0

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCount()I

    move-result v6

    if-ge v0, v6, :cond_0

    .line 2177
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v6, v0, v10}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->loadBlock(IZ)V

    .line 2180
    :try_start_0
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v6, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2181
    :catch_0
    move-exception v1

    .line 2182
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v6, v8, v10}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->loadBlock(IZ)V

    .line 2183
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v6, v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    goto :goto_0
.end method

.method public sendDictionaryMessage(Ljava/lang/String;)V
    .locals 1
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    .line 4725
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4726
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setSearchKeyword(Ljava/lang/String;)Z

    .line 4727
    :cond_0
    return-void
.end method

.method public setActionbarFocus(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 4833
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mKeyContoller:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->setActionBarFocus(Z)V

    .line 4834
    if-eqz p1, :cond_0

    .line 4835
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnMenu:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->requestFocusFromTouch()Z

    .line 4840
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nShowMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 4841
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindNextFind:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->requestFocus()Z

    .line 4845
    :cond_0
    return-void
.end method

.method public setFindOptionIconSelected(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 4215
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindOption:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 4216
    return-void
.end method

.method public setFindStopflag(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 2972
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bfindStopflag:Z

    .line 2973
    return-void
.end method

.method public setFormatIconSelected(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 4211
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFormatIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 4212
    return-void
.end method

.method public setIsASCFile(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 1201
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bIsASCFile:Z

    .line 1202
    return-void
.end method

.method public setLastDisplayPosition()V
    .locals 8

    .prologue
    .line 2136
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v4

    .line 2137
    .local v4, "topcaret":I
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getCurBlock()I

    move-result v0

    .line 2139
    .local v0, "curblock":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-string/jumbo v6, "textinfo"

    const v7, 0x8000

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 2140
    .local v3, "sP":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 2142
    .local v1, "edit":Landroid/content/SharedPreferences$Editor;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "|"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2143
    .local v2, "item":Ljava/lang/String;
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;

    invoke-interface {v1, v5, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2145
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2154
    return-void
.end method

.method public setMenuButtonSelected(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 4207
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnMenu:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 4208
    return-void
.end method

.method public setOver10mb(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 1197
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bIsOver10mb:Z

    .line 1198
    return-void
.end method

.method public setPrintStopFlag(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 3651
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bPrintstop:Z

    .line 3652
    return-void
.end method

.method public setReplaceOptionIconSelected(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 4219
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceOption:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 4220
    return-void
.end method

.method public setScrollThumbTimer()V
    .locals 5

    .prologue
    .line 4147
    const/16 v0, 0x7d0

    .line 4149
    .local v0, "nDelay":I
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ScrollThumbThread:Ljava/lang/Runnable;

    if-nez v1, :cond_0

    .line 4150
    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$30;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$30;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ScrollThumbThread:Ljava/lang/Runnable;

    .line 4159
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ScrollThumbHandler:Landroid/os/Handler;

    if-nez v1, :cond_2

    .line 4160
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ScrollThumbHandler:Landroid/os/Handler;

    .line 4164
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isEnableScroll()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 4166
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->showScrollThumb()V

    .line 4169
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ScrollThumbHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ScrollThumbThread:Ljava/lang/Runnable;

    int-to-long v3, v0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 4170
    return-void

    .line 4162
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ScrollThumbHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ScrollThumbThread:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public setShowMode(I)V
    .locals 6
    .param p1, "nShowMode"    # I

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 2763
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isToastpopupShowing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isMorepopupShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2764
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->dismissPopup()V

    .line 2766
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->removePopupCallback()V

    .line 2768
    packed-switch p1, :pswitch_data_0

    .line 2877
    :cond_2
    :goto_0
    iput p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nShowMode:I

    .line 2878
    return-void

    .line 2770
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_menuPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    if-eqz v0, :cond_3

    .line 2771
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_menuPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2772
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_menuPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->dismiss()V

    .line 2776
    :cond_3
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getViewMode()I

    move-result v0

    if-nez v0, :cond_4

    .line 2777
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setCursorVisible(Z)V

    .line 2781
    :goto_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2782
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Findedt:Ljava/lang/String;

    .line 2784
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bLandActionBar:Z

    if-nez v0, :cond_5

    .line 2786
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v5, :cond_5

    .line 2787
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2788
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llFind:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2789
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llReplace:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2790
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTS:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 2779
    :cond_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setCursorVisible(Z)V

    goto :goto_1

    .line 2794
    :cond_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2795
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llFind:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2796
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llReplace:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2797
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_tvTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2798
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnUndo:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2799
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnRedo:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2800
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTS:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2801
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTSbar:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2802
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nShowMode:I

    if-eq v0, v5, :cond_6

    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nShowMode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_7

    .line 2803
    :cond_6
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    .line 2805
    :cond_7
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getViewMode()I

    move-result v0

    if-eq v0, v4, :cond_2

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getViewMode()I

    move-result v0

    if-eq v0, v5, :cond_2

    .line 2808
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isChanged()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2811
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_tvTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2812
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnUndo:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2813
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnRedo:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2814
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTS:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2815
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTSbar:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 2818
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setCursorVisible(Z)V

    .line 2820
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2821
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llFind:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2822
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llReplace:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2823
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTS:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2824
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTSbar:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2825
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hideSoftKeyboard()Z

    .line 2828
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Findedt:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 2829
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Findedt:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2831
    :cond_8
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    .line 2833
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 2834
    new-instance v0, Lcom/infraware/polarisoffice5/common/ImmManager;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/ImmManager;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/ImmManager;->showDelayedIme()V

    goto/16 :goto_0

    .line 2837
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setCursorVisible(Z)V

    .line 2839
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2840
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llFind:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2841
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llReplace:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2842
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTS:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2843
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTSbar:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2844
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hideSoftKeyboard()Z

    .line 2847
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplaceFind:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2848
    iput v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nReplaceCnt:I

    .line 2849
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplaceFind:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 2850
    new-instance v0, Lcom/infraware/polarisoffice5/common/ImmManager;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/ImmManager;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/ImmManager;->showDelayedIme()V

    goto/16 :goto_0

    .line 2853
    :pswitch_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setCursorVisible(Z)V

    .line 2855
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2856
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llFind:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2857
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llReplace:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2858
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTS:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2859
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTSbar:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2861
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_9

    .line 2862
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTS:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    .line 2864
    :cond_9
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setTTSToolbarEnable(Z)V

    .line 2866
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hideSoftKeyboard()Z

    .line 2868
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v1

    sub-int/2addr v0, v1

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getCursorStatus()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2869
    :cond_a
    invoke-direct {p0, v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setTTSMode(I)V

    goto/16 :goto_0

    .line 2871
    :cond_b
    invoke-direct {p0, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setTTSMode(I)V

    goto/16 :goto_0

    .line 2768
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setTTSToolbarEnable(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 3533
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnNext:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 3534
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnPlay:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 3535
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnPrev:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 3536
    return-void
.end method

.method public setTitleText()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 2719
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFile:Ljava/lang/String;

    .line 2721
    .local v2, "strTitle":Ljava/lang/String;
    const-string/jumbo v1, "TextEditor"

    .line 2722
    .local v1, "displayTitle":Ljava/lang/String;
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mPageInfoPage:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2723
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getCurBlock()I

    move-result v4

    add-int/lit8 v0, v4, 0x1

    .line 2724
    .local v0, "curBlock":I
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCount()I

    move-result v3

    .line 2726
    .local v3, "totalBlock":I
    if-le v0, v3, :cond_0

    .line 2727
    move v3, v0

    .line 2730
    :cond_0
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mPageInfoCurrent:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "%d"

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2731
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mPageInfoTotal:Landroid/widget/TextView;

    const-string/jumbo v5, "%d"

    new-array v6, v10, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2733
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setScrollThumbTimer()V

    .line 2735
    const-string/jumbo v4, "TextEditor"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2736
    move-object v1, v2

    .line 2738
    :cond_1
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_tvTitle:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2739
    return-void
.end method

.method public setToolbarFocus()V
    .locals 1

    .prologue
    .line 4231
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnPlay:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->requestFocus()Z

    .line 4232
    return-void
.end method

.method public setViewMode(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x1

    .line 595
    iput p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ViewMode:I

    .line 597
    packed-switch p1, :pswitch_data_0

    .line 694
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HOVERING(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v2, :cond_1

    .line 695
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFormatIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 696
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFind:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 697
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnMenu:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 698
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindOption:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 699
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindPrevFind:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 700
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindNextFind:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 702
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFormatIcon:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070131

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 703
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFind:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07004f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 704
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnMenu:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070052

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 706
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindOption:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070255

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 707
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindPrevFind:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070059

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 708
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindNextFind:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070053

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 710
    :cond_1
    return-void

    .line 600
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFormatIcon_View:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFormatIcon:Landroid/widget/ImageView;

    .line 601
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_tvTitle_View:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_tvTitle:Landroid/widget/TextView;

    .line 602
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFind_View:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFind:Landroid/widget/ImageView;

    .line 603
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnProperties_View:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnProperties:Landroid/widget/ImageView;

    .line 604
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnMenu_View:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnMenu:Landroid/widget/ImageView;

    .line 605
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnUndo_View:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnUndo:Landroid/widget/ImageView;

    .line 606
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnRedo_View:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnRedo:Landroid/widget/ImageView;

    .line 609
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar_View:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar:Landroid/widget/LinearLayout;

    .line 611
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llFind_View:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llFind:Landroid/widget/LinearLayout;

    .line 612
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llReplace_View:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llReplace:Landroid/widget/LinearLayout;

    .line 613
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llFindOfReplace_View:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llFindOfReplace:Landroid/widget/LinearLayout;

    .line 614
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llReplaceOfReplace_View:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llReplaceOfReplace:Landroid/widget/LinearLayout;

    .line 615
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTS_View:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTS:Landroid/widget/LinearLayout;

    .line 617
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_tvTTSTitle_View:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_tvTTSTitle:Landroid/widget/TextView;

    .line 619
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindOption_View:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindOption:Landroid/widget/ImageView;

    .line 620
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind_View:Landroid/widget/EditText;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;

    .line 621
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindPrevFind_View:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindPrevFind:Landroid/widget/ImageView;

    .line 622
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindNextFind_View:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindNextFind:Landroid/widget/ImageView;

    .line 624
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceOption_View:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceOption:Landroid/widget/ImageView;

    .line 625
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplaceFind_View:Landroid/widget/EditText;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplaceFind:Landroid/widget/EditText;

    .line 626
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceNextFind_View:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceNextFind:Landroid/widget/ImageView;

    .line 627
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplace_View:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplace:Landroid/widget/ImageView;

    .line 629
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplace_View:Landroid/widget/EditText;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplace:Landroid/widget/EditText;

    .line 630
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceAll_View:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceAll:Landroid/widget/ImageView;

    .line 632
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnDone_View:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnDone:Landroid/widget/ImageView;

    .line 635
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnRedo:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 636
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnUndo:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 638
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceOption:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 640
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    if-eqz v0, :cond_0

    .line 641
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setCursorVisible(Z)V

    .line 642
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 643
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hideSoftKeyboard()Z

    goto/16 :goto_0

    .line 597
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public showOver10mbMsg()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1209
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v4

    invoke-direct {v1, p0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1211
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0702fb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1212
    .local v3, "limitMsg":Ljava/lang/String;
    const/16 v4, 0xa

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 1213
    .local v2, "limitMB":Ljava/lang/String;
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1215
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1216
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070061

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$4;

    invoke-direct {v5, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$4;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    invoke-virtual {v1, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1223
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 1224
    .local v0, "alert":Landroid/app/AlertDialog;
    invoke-virtual {v0, v6}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 1225
    invoke-virtual {v0, v6}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 1226
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 1227
    return-void
.end method

.method public showScrollThumb()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 4182
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getScrollHeight()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 4198
    :cond_0
    :goto_0
    return-void

    .line 4185
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isNewFile()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 4186
    :cond_2
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4187
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_flScrollPos:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 4188
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ibScrollThumb:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 4189
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mPageInfoPage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 4191
    :cond_3
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 4192
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_flScrollPos:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 4193
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ibScrollThumb:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 4194
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mPageInfoPage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public ttsButtonImageChange(Z)V
    .locals 3
    .param p1, "showplay"    # Z

    .prologue
    .line 3543
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mbIsTTSPlayFlag:Z

    .line 3544
    if-eqz p1, :cond_0

    .line 3545
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnPlay:Landroid/widget/ImageView;

    const v1, 0x7f020037

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 3546
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnPlay:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07006a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3547
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_TTSmanager:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->setIsPlay(Z)V

    .line 3553
    :goto_0
    return-void

    .line 3549
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnPlay:Landroid/widget/ImageView;

    const v1, 0x7f020036

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 3550
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ttsbarBtnPlay:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070068

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3551
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_TTSmanager:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->setIsPlay(Z)V

    goto :goto_0
.end method

.method public undoredoStateChange()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1707
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hasMoreUndo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1708
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnUndo:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1712
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hasMoreRedo()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1713
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnRedo:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1718
    :goto_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnUndo:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 1719
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnRedo:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 1720
    return-void

    .line 1710
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnUndo:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_0

    .line 1715
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnRedo:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_1
.end method
