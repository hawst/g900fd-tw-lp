.class public Lcom/infraware/polarisoffice5/text/manager/TTSManager;
.super Ljava/lang/Object;
.source "TTSManager.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;
.implements Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Flag;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$StringReference;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Type;


# instance fields
.field private BGMResumeHandler:Landroid/os/Handler;

.field private final BGM_RESUME:I

.field private final MINIMUM_TIME_FOR_A_LINE:J

.field private MSG_DELAY_PLAY_TTS:I

.field private hasAudioFocus:Z

.field private lineE:I

.field private lineS:I

.field private mPrevHandler:Landroid/os/Handler;

.field private mPrevStartTime:J

.field private mReceiver:Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;

.field private final mStopHandler:Landroid/os/Handler;

.field private m_AudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private m_AudioManager:Landroid/media/AudioManager;

.field private m_TTS:Landroid/speech/tts/TextToSpeech;

.field private m_TTSMode:I

.field private m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

.field private m_bIsPlay:Z

.field private m_bIsSelected:Z

.field private m_bPressBtn:Z

.field private m_bStopDuringTTS:Z

.field private m_finish:Z

.field private m_isPrev:Z

.field private m_stopTTSPosition:I

.field private m_ttsSelectionPosition:I

.field private m_ttsSelectionPosition2:I

.field private m_ttsStartingPosition:I


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
    .locals 2
    .param p1, "parent"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    const/4 v1, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->BGM_RESUME:I

    .line 43
    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_stopTTSPosition:I

    .line 44
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_bStopDuringTTS:Z

    .line 45
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_bIsSelected:Z

    .line 49
    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_ttsStartingPosition:I

    .line 50
    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_ttsSelectionPosition:I

    .line 51
    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_ttsSelectionPosition2:I

    .line 53
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_finish:Z

    .line 58
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_bPressBtn:Z

    .line 60
    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTSMode:I

    .line 62
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_bIsPlay:Z

    .line 64
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_isPrev:Z

    .line 66
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->hasAudioFocus:Z

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->mReceiver:Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;

    .line 109
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->mPrevStartTime:J

    .line 110
    const-wide/16 v0, 0x1f4

    iput-wide v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->MINIMUM_TIME_FOR_A_LINE:J

    .line 111
    const/4 v0, 0x5

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->MSG_DELAY_PLAY_TTS:I

    .line 113
    new-instance v0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager$2;-><init>(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->mPrevHandler:Landroid/os/Handler;

    .line 599
    new-instance v0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$6;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager$6;-><init>(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->BGMResumeHandler:Landroid/os/Handler;

    .line 801
    new-instance v0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$8;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager$8;-><init>(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->mStopHandler:Landroid/os/Handler;

    .line 71
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .line 73
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->initialize()V

    .line 74
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->registerReceiver()V

    .line 75
    return-void
.end method

.method private BGMPause()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 619
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_AudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->hasAudioFocus:Z

    if-nez v0, :cond_0

    .line 620
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_AudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_AudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 621
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->hasAudioFocus:Z

    .line 623
    :cond_0
    return-void
.end method

.method private BGMResume()V
    .locals 2

    .prologue
    .line 627
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_AudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->hasAudioFocus:Z

    if-eqz v0, :cond_0

    .line 628
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_AudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_AudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 629
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->hasAudioFocus:Z

    .line 631
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->sayText()V

    return-void
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    .prologue
    .line 35
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_stopTTSPosition:I

    return v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/text/manager/TTSManager;I)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/manager/TTSManager;
    .param p1, "x1"    # I

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->onTTS(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)Landroid/speech/tts/TextToSpeech;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    return-object v0
.end method

.method static synthetic access$402(Lcom/infraware/polarisoffice5/text/manager/TTSManager;Landroid/speech/tts/TextToSpeech;)Landroid/speech/tts/TextToSpeech;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/manager/TTSManager;
    .param p1, "x1"    # Landroid/speech/tts/TextToSpeech;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    return-object p1
.end method

.method static synthetic access$502(Lcom/infraware/polarisoffice5/text/manager/TTSManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/manager/TTSManager;
    .param p1, "x1"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_finish:Z

    return p1
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->BGMResumeHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    .prologue
    .line 35
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTSMode:I

    return v0
.end method

.method static synthetic access$800(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->BGMResume()V

    return-void
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    const-string/jumbo v1, "audio"

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_AudioManager:Landroid/media/AudioManager;

    .line 80
    new-instance v0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager$1;-><init>(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_AudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 99
    return-void
.end method

.method private movePrevLine()V
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 403
    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v10

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTTSSelectStart()I

    move-result v9

    .line 405
    .local v9, "trackingStartPosition":I
    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v10

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v10

    invoke-virtual {v10, v9}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v0

    .line 408
    .local v0, "curline":I
    :cond_0
    :goto_0
    add-int/lit8 v9, v9, -0x1

    .line 409
    if-gez v9, :cond_1

    .line 410
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->negativeCase()V

    .line 511
    :goto_1
    return-void

    .line 413
    :cond_1
    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v10

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v10

    invoke-virtual {v10, v9}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v1

    .line 415
    .local v1, "line":I
    add-int/lit8 v10, v0, -0x1

    if-ne v10, v1, :cond_0

    .line 417
    move v4, v9

    .line 418
    .local v4, "prevTrackingStartPosition":I
    add-int/lit8 v3, v9, 0x1

    .line 421
    .local v3, "prevTrackingEndPosition":I
    :cond_2
    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v10

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v10

    invoke-virtual {v10, v4}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v8

    .line 423
    .local v8, "templine":I
    if-ne v1, v8, :cond_3

    .line 424
    add-int/lit8 v4, v4, -0x1

    .line 425
    if-gez v4, :cond_2

    .line 426
    iput-boolean v13, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_isPrev:Z

    .line 427
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->negativeCase()V

    .line 428
    iput-boolean v12, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_isPrev:Z

    goto :goto_1

    .line 436
    :cond_3
    add-int/lit8 v4, v4, 0x1

    .line 441
    invoke-direct {p0, v4, v3}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->trim(II)[I

    move-result-object v2

    .line 442
    .local v2, "pos":[I
    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v10

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 444
    .local v5, "str":Ljava/lang/String;
    aget v10, v2, v12

    aget v11, v2, v13

    invoke-virtual {v5, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 445
    .local v6, "strSpeak":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 447
    const-string/jumbo v10, "\r\n"

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_4

    const-string/jumbo v10, "\n\r"

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_4

    const-string/jumbo v10, "\r"

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_4

    const-string/jumbo v10, "\n"

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_4

    const-string/jumbo v10, ""

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    :cond_4
    if-lez v0, :cond_5

    .line 450
    add-int/lit8 v0, v0, -0x1

    goto/16 :goto_0

    .line 489
    :cond_5
    if-gez v9, :cond_6

    .line 490
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->negativeCase()V

    goto/16 :goto_1

    .line 493
    :cond_6
    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v10

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v10

    invoke-virtual {v10, v9}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v1

    .line 496
    :cond_7
    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v10

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v10

    invoke-virtual {v10, v9}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v7

    .line 497
    .local v7, "temp":I
    if-ne v1, v7, :cond_8

    .line 498
    add-int/lit8 v9, v9, -0x1

    .line 499
    if-gez v9, :cond_7

    .line 500
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->negativeCase()V

    goto/16 :goto_1

    .line 506
    :cond_8
    add-int/lit8 v9, v9, 0x1

    .line 507
    iput v9, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_ttsStartingPosition:I

    .line 508
    iput-boolean v13, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_isPrev:Z

    .line 509
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->sayText()V

    .line 510
    iput-boolean v12, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_isPrev:Z

    goto/16 :goto_1
.end method

.method private negativeCase()V
    .locals 1

    .prologue
    .line 398
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_ttsStartingPosition:I

    .line 399
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->sayText()V

    .line 400
    return-void
.end method

.method private onTTS(I)V
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 592
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setShowMode(I)V

    .line 593
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->BGMPause()V

    .line 594
    new-instance v0, Landroid/speech/tts/TextToSpeech;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {v0, v1, p0}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    .line 596
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0, p1, p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setTTSSelection(II)V

    .line 597
    return-void
.end method

.method private onTTSSettingFail()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 261
    const-string/jumbo v1, "TextEditorActivity"

    const-string/jumbo v2, "Language is not available."

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07030c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 267
    .local v0, "toast":Landroid/widget/Toast;
    const/16 v1, 0x11

    invoke-virtual {v0, v1, v4, v4}, Landroid/widget/Toast;->setGravity(III)V

    .line 268
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 285
    return-void
.end method

.method private registerReceiver()V
    .locals 3

    .prologue
    .line 811
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->mReceiver:Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;

    if-eqz v1, :cond_0

    .line 821
    :goto_0
    return-void

    .line 814
    :cond_0
    new-instance v1, Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;

    invoke-direct {v1}, Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;-><init>()V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->mReceiver:Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;

    .line 815
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->mReceiver:Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->mStopHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;->setHandler(Landroid/os/Handler;)V

    .line 817
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 818
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string/jumbo v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 820
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->mReceiver:Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private sayText()V
    .locals 12

    .prologue
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 634
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    if-nez v8, :cond_1

    .line 735
    :cond_0
    :goto_0
    return-void

    .line 637
    :cond_1
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 639
    .local v2, "str":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    iget v9, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_ttsStartingPosition:I

    if-gt v8, v9, :cond_2

    .line 641
    invoke-virtual {p0, v11}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->setPressBtn(Z)V

    .line 642
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->doneTTS()V

    goto :goto_0

    .line 646
    :cond_2
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v8

    iget v9, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_ttsStartingPosition:I

    invoke-virtual {v8, v9}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v8

    iput v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->lineS:I

    .line 647
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v8

    iget v9, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_ttsStartingPosition:I

    invoke-virtual {v8, v9}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v8

    iput v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->lineE:I

    .line 648
    iget v6, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_ttsStartingPosition:I

    .line 649
    .local v6, "ttsStart":I
    iget v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_ttsStartingPosition:I

    .line 651
    .local v5, "ttsEnd":I
    :goto_1
    iget-boolean v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_finish:Z

    if-eqz v8, :cond_3

    iget-boolean v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_isPrev:Z

    if-eqz v8, :cond_6

    .line 652
    :cond_3
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v8

    invoke-virtual {v8, v5}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v8

    iput v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->lineE:I

    .line 654
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    if-lt v5, v8, :cond_5

    .line 656
    iget-boolean v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_isPrev:Z

    if-eqz v8, :cond_4

    .line 657
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->doneTTS()V

    goto :goto_0

    .line 660
    :cond_4
    iput-boolean v10, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_finish:Z

    goto :goto_1

    .line 668
    :cond_5
    iget-boolean v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_finish:Z

    if-eqz v8, :cond_6

    iget-boolean v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_isPrev:Z

    if-eqz v8, :cond_6

    .line 669
    iput-boolean v11, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_finish:Z

    .line 672
    :cond_6
    iget v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->lineS:I

    iget v9, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->lineE:I

    if-ne v8, v9, :cond_7

    iget-boolean v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_finish:Z

    if-nez v8, :cond_7

    .line 674
    add-int/lit8 v5, v5, 0x1

    .line 675
    goto :goto_1

    .line 677
    :cond_7
    if-lt v6, v5, :cond_8

    .line 678
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->doneTTS()V

    goto/16 :goto_0

    .line 682
    :cond_8
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v8, v5, :cond_9

    .line 683
    invoke-virtual {p0, v11}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->setPressBtn(Z)V

    .line 684
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->doneTTS()V

    goto/16 :goto_0

    .line 688
    :cond_9
    invoke-virtual {v2, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 689
    .local v3, "strSpeak":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 690
    .local v4, "strSpeakTrim":Ljava/lang/String;
    const-string/jumbo v8, ""

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 691
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v8

    add-int/lit8 v9, v5, 0x1

    invoke-virtual {v8, v9}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v8

    iput v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->lineS:I

    .line 692
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v8

    add-int/lit8 v9, v5, 0x1

    invoke-virtual {v8, v9}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v8

    iput v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->lineE:I

    .line 693
    iput v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_ttsStartingPosition:I

    .line 694
    iget v6, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_ttsStartingPosition:I

    .line 695
    add-int/lit8 v5, v5, 0x1

    .line 696
    goto/16 :goto_1

    .line 698
    :cond_a
    invoke-direct {p0, v6, v5}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->trim(II)[I

    move-result-object v1

    .line 699
    .local v1, "pos":[I
    if-eqz v1, :cond_0

    .line 703
    iput v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_ttsStartingPosition:I

    .line 705
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 706
    .local v0, "params":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v7, "textEditorTTS"

    .line 707
    .local v7, "utterance":Ljava/lang/String;
    const-string/jumbo v8, "utteranceId"

    invoke-virtual {v0, v8, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 708
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    if-eqz v8, :cond_0

    .line 709
    const-string/jumbo v8, "\r\n"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_b

    const-string/jumbo v8, "\n\r"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_b

    const-string/jumbo v8, "\n"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_b

    const-string/jumbo v8, "\r"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_b

    const-string/jumbo v8, ""

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 712
    :cond_b
    const-string/jumbo v8, "textEditorTTS"

    invoke-virtual {p0, v8}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->onUtteranceCompleted(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 716
    :cond_c
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v8

    aget v9, v1, v11

    aget v10, v1, v10

    invoke-virtual {v8, v9, v10}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setTTSSelection(II)V

    .line 718
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v8, v4, v11, v0}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    .line 720
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    new-instance v9, Lcom/infraware/polarisoffice5/text/manager/TTSManager$7;

    invoke-direct {v9, p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager$7;-><init>(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)V

    invoke-virtual {v8, v9}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0
.end method

.method private stop()V
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I

    .line 325
    :cond_0
    return-void
.end method

.method private trim(II)[I
    .locals 10
    .param p1, "s"    # I
    .param p2, "e"    # I

    .prologue
    const/16 v9, 0xd

    const/16 v8, 0xa

    const/16 v7, 0x9

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 738
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 740
    .local v3, "str":Ljava/lang/String;
    const/4 v4, 0x2

    new-array v2, v4, [I

    .line 742
    .local v2, "pos":[I
    aput p1, v2, v5

    .line 743
    aput p2, v2, v6

    .line 746
    move v1, p1

    .local v1, "i":I
    :goto_0
    add-int/lit8 v4, p2, 0x1

    if-ge v1, v4, :cond_0

    .line 747
    :try_start_0
    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x20

    if-eq v4, v5, :cond_2

    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-eq v4, v8, :cond_2

    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-eq v4, v9, :cond_2

    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-eq v4, v7, :cond_2

    .line 749
    const/4 v4, 0x0

    aput v1, v2, v4

    .line 754
    :cond_0
    move v1, p2

    :goto_1
    if-lt v1, p1, :cond_1

    .line 755
    if-nez v1, :cond_3

    .line 769
    .end local v2    # "pos":[I
    :cond_1
    :goto_2
    return-object v2

    .line 746
    .restart local v2    # "pos":[I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 757
    :cond_3
    add-int/lit8 v4, v1, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x20

    if-eq v4, v5, :cond_4

    add-int/lit8 v4, v1, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-eq v4, v8, :cond_4

    add-int/lit8 v4, v1, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-eq v4, v9, :cond_4

    add-int/lit8 v4, v1, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-eq v4, v7, :cond_4

    .line 760
    const/4 v4, 0x1

    aput v1, v2, v4
    :try_end_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 764
    :catch_0
    move-exception v0

    .line 765
    .local v0, "exception":Ljava/lang/StringIndexOutOfBoundsException;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->doneTTS()V

    .line 766
    const/4 v2, 0x0

    goto :goto_2

    .line 754
    .end local v0    # "exception":Ljava/lang/StringIndexOutOfBoundsException;
    :cond_4
    add-int/lit8 v1, v1, -0x1

    goto :goto_1
.end method


# virtual methods
.method public checkLanguage()[Ljava/util/Locale;
    .locals 10

    .prologue
    const/16 v5, 0xb

    const/4 v9, 0x1

    .line 241
    const/4 v3, 0x0

    .line 242
    .local v3, "nCount":I
    new-array v0, v5, [Ljava/util/Locale;

    .line 243
    .local v0, "arrAvailable":[Ljava/util/Locale;
    new-array v1, v5, [Ljava/util/Locale;

    const/4 v5, 0x0

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    aput-object v6, v1, v5

    sget-object v5, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    aput-object v5, v1, v9

    const/4 v5, 0x2

    sget-object v6, Ljava/util/Locale;->UK:Ljava/util/Locale;

    aput-object v6, v1, v5

    const/4 v5, 0x3

    sget-object v6, Ljava/util/Locale;->GERMAN:Ljava/util/Locale;

    aput-object v6, v1, v5

    const/4 v5, 0x4

    sget-object v6, Ljava/util/Locale;->FRANCE:Ljava/util/Locale;

    aput-object v6, v1, v5

    const/4 v5, 0x5

    sget-object v6, Ljava/util/Locale;->ITALY:Ljava/util/Locale;

    aput-object v6, v1, v5

    const/4 v5, 0x6

    new-instance v6, Ljava/util/Locale;

    const-string/jumbo v7, "spa"

    const-string/jumbo v8, "ESP"

    invoke-direct {v6, v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v1, v5

    const/4 v5, 0x7

    sget-object v6, Ljava/util/Locale;->SIMPLIFIED_CHINESE:Ljava/util/Locale;

    aput-object v6, v1, v5

    const/16 v5, 0x8

    sget-object v6, Ljava/util/Locale;->TRADITIONAL_CHINESE:Ljava/util/Locale;

    aput-object v6, v1, v5

    const/16 v5, 0x9

    sget-object v6, Ljava/util/Locale;->TAIWAN:Ljava/util/Locale;

    aput-object v6, v1, v5

    const/16 v5, 0xa

    sget-object v6, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    aput-object v6, v1, v5

    .line 248
    .local v1, "arrLocaleList":[Ljava/util/Locale;
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    if-nez v5, :cond_1

    .line 257
    :cond_0
    return-object v0

    .line 251
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v5, v1

    if-ge v2, v5, :cond_0

    .line 252
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    aget-object v6, v1, v2

    invoke-virtual {v5, v6}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v5

    if-ne v5, v9, :cond_2

    .line 253
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "nCount":I
    .local v4, "nCount":I
    aget-object v5, v1, v2

    aput-object v5, v0, v3

    move v3, v4

    .line 251
    .end local v4    # "nCount":I
    .restart local v3    # "nCount":I
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public doneTTS()V
    .locals 2

    .prologue
    .line 518
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    new-instance v1, Lcom/infraware/polarisoffice5/text/manager/TTSManager$5;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager$5;-><init>(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)V

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 547
    return-void
.end method

.method public forward()V
    .locals 2

    .prologue
    .line 375
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    if-nez v0, :cond_0

    .line 384
    :goto_0
    return-void

    .line 377
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->isSpeaking()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 378
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->setPressBtn(Z)V

    .line 379
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->stop()V

    .line 381
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->mPrevHandler:Landroid/os/Handler;

    iget v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->MSG_DELAY_PLAY_TTS:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 382
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTTSSelectEnd()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_ttsStartingPosition:I

    .line 383
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->sayText()V

    goto :goto_0
.end method

.method public getTTSMode()I
    .locals 1

    .prologue
    .line 777
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTSMode:I

    return v0
.end method

.method public isPlay()Z
    .locals 1

    .prologue
    .line 233
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_bIsPlay:Z

    return v0
.end method

.method public isPressBtn()Z
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_bPressBtn:Z

    return v0
.end method

.method public isSpeaking()Z
    .locals 1

    .prologue
    .line 833
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_0

    .line 834
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->isSpeaking()Z

    move-result v0

    .line 836
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onInit(I)V
    .locals 13
    .param p1, "status"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v12, -0x1

    const/4 v11, -0x2

    const/4 v2, 0x0

    .line 142
    if-nez p1, :cond_d

    .line 143
    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    if-nez v9, :cond_0

    .line 230
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v10}, Landroid/speech/tts/TextToSpeech;->getDefaultEngine()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/speech/tts/TextToSpeech;->setEngineByPackageName(Ljava/lang/String;)I

    .line 147
    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v9, p0}, Landroid/speech/tts/TextToSpeech;->setOnUtteranceCompletedListener(Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;)I

    .line 150
    :try_start_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->checkLanguage()[Ljava/util/Locale;

    move-result-object v0

    .line 151
    .local v0, "arrAvailableLanguage":[Ljava/util/Locale;
    const/4 v9, 0x0

    aget-object v9, v0, v9

    if-eqz v9, :cond_1

    move v2, v8

    .line 153
    .local v2, "hasAvailableLanguage":Z
    :cond_1
    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v9}, Landroid/speech/tts/TextToSpeech;->getLanguage()Ljava/util/Locale;

    move-result-object v9

    if-nez v9, :cond_4

    .line 154
    if-eqz v2, :cond_2

    .line 155
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    const/4 v9, 0x0

    aget-object v9, v0, v9

    invoke-virtual {v8, v9}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    .line 197
    :cond_2
    :goto_1
    if-eqz v2, :cond_3

    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v9}, Landroid/speech/tts/TextToSpeech;->getLanguage()Ljava/util/Locale;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v8

    if-ne v8, v11, :cond_c

    .line 199
    :cond_3
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->onTTSSettingFail()V

    .line 200
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->doneTTS()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 220
    .end local v0    # "arrAvailableLanguage":[Ljava/util/Locale;
    .end local v2    # "hasAvailableLanguage":Z
    :catch_0
    move-exception v1

    .line 223
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->onTTSSettingFail()V

    .line 224
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->doneTTS()V

    goto :goto_0

    .line 156
    .end local v1    # "e":Ljava/lang/NullPointerException;
    .restart local v0    # "arrAvailableLanguage":[Ljava/util/Locale;
    .restart local v2    # "hasAvailableLanguage":Z
    :cond_4
    :try_start_1
    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v9}, Landroid/speech/tts/TextToSpeech;->getLanguage()Ljava/util/Locale;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 157
    if-eqz v2, :cond_2

    .line 158
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    const/4 v9, 0x0

    aget-object v9, v0, v9

    invoke-virtual {v8, v9}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    goto :goto_1

    .line 159
    :cond_5
    invoke-static {}, Lcom/infraware/common/util/Utils;->isL()Z

    move-result v9

    if-eqz v9, :cond_7

    .line 160
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v9}, Landroid/speech/tts/TextToSpeech;->getLanguage()Ljava/util/Locale;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    move-result v3

    .line 161
    .local v3, "languageResult":I
    if-eq v3, v11, :cond_6

    if-ne v3, v12, :cond_2

    .line 163
    :cond_6
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v8, v9}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    goto :goto_1

    .line 171
    .end local v3    # "languageResult":I
    :cond_7
    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v9}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string/jumbo v10, "tts_default_synth"

    invoke-static {v9, v10}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 174
    .local v6, "mDefaultSynth":Ljava/lang/String;
    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v9}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string/jumbo v10, "tts_default_locale"

    invoke-static {v9, v10}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 177
    .local v5, "mDefaultLocale":Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/infraware/common/util/Utils;->parseEnginePrefFromList(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 179
    .local v7, "strLocale":Ljava/lang/String;
    if-eqz v7, :cond_a

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_a

    .line 180
    new-instance v4, Ljava/util/Locale;

    invoke-direct {v4, v7, v7}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    .local v4, "locale":Ljava/util/Locale;
    :goto_2
    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v9, v4}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v9

    if-eqz v9, :cond_8

    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v9, v4}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v9

    if-eq v9, v8, :cond_8

    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v8, v4}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v8

    const/4 v9, 0x2

    if-ne v8, v9, :cond_b

    .line 187
    :cond_8
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v8, v4}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    move-result v3

    .line 188
    .restart local v3    # "languageResult":I
    if-eq v3, v11, :cond_9

    if-ne v3, v12, :cond_2

    .line 190
    :cond_9
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v8, v9}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    goto/16 :goto_1

    .line 182
    .end local v3    # "languageResult":I
    .end local v4    # "locale":Ljava/util/Locale;
    :cond_a
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    .restart local v4    # "locale":Ljava/util/Locale;
    goto :goto_2

    .line 193
    :cond_b
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v8, v9}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    goto/16 :goto_1

    .line 202
    .end local v4    # "locale":Ljava/util/Locale;
    .end local v5    # "mDefaultLocale":Ljava/lang/String;
    .end local v6    # "mDefaultSynth":Ljava/lang/String;
    .end local v7    # "strLocale":Ljava/lang/String;
    :cond_c
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v8

    new-instance v9, Lcom/infraware/polarisoffice5/text/manager/TTSManager$3;

    invoke-direct {v9, p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager$3;-><init>(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)V

    const-wide/16 v10, 0x12c

    invoke-virtual {v8, v9, v10, v11}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 228
    .end local v0    # "arrAvailableLanguage":[Ljava/util/Locale;
    .end local v2    # "hasAvailableLanguage":Z
    :cond_d
    const-string/jumbo v8, "TextEditorActivity"

    const-string/jumbo v9, "Could not initialize TextToSpeech."

    invoke-static {v8, v9}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 288
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_0

    .line 289
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->doneTTS()V

    .line 290
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_bStopDuringTTS:Z

    .line 292
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionEnd()I

    move-result v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionStart()I

    move-result v1

    sub-int/2addr v0, v1

    if-lez v0, :cond_1

    .line 293
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_bIsSelected:Z

    .line 294
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionStart()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_ttsSelectionPosition:I

    .line 295
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionEnd()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_ttsSelectionPosition2:I

    .line 301
    :cond_0
    :goto_0
    return-void

    .line 297
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_bIsSelected:Z

    .line 298
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_ttsStartingPosition:I

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_stopTTSPosition:I

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 304
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_bStopDuringTTS:Z

    if-eqz v0, :cond_0

    .line 305
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_bStopDuringTTS:Z

    .line 307
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_bIsSelected:Z

    if-eqz v0, :cond_1

    .line 308
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    iget v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_ttsSelectionPosition:I

    iget v2, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_ttsSelectionPosition2:I

    invoke-virtual {v0, v1, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(II)V

    .line 313
    :goto_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/infraware/polarisoffice5/text/manager/TTSManager$4;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager$4;-><init>(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 320
    :cond_0
    return-void

    .line 310
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    iget v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_stopTTSPosition:I

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    goto :goto_0
.end method

.method public onTTS()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 551
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xd

    if-le v5, v6, :cond_1

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v5

    if-nez v5, :cond_1

    .line 553
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    const-string/jumbo v6, "accessibility"

    invoke-virtual {v5, v6}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 554
    .local v0, "am":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0, v8}, Landroid/view/accessibility/AccessibilityManager;->getEnabledAccessibilityServiceList(I)Ljava/util/List;

    move-result-object v1

    .line 555
    .local v1, "asList":Ljava/util/List;, "Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v3, v5, :cond_1

    .line 557
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/accessibilityservice/AccessibilityServiceInfo;

    invoke-virtual {v5}, Landroid/accessibilityservice/AccessibilityServiceInfo;->getId()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "com.google.android.marvin.talkback/.TalkBackService"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 560
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v5

    if-nez v5, :cond_0

    .line 561
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    const v7, 0x7f070313

    invoke-virtual {v6, v7}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 589
    .end local v0    # "am":Landroid/view/accessibility/AccessibilityManager;
    .end local v1    # "asList":Ljava/util/List;, "Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    .end local v3    # "i":I
    :goto_1
    return-void

    .line 555
    .restart local v0    # "am":Landroid/view/accessibility/AccessibilityManager;
    .restart local v1    # "asList":Ljava/util/List;, "Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    .restart local v3    # "i":I
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 568
    .end local v0    # "am":Landroid/view/accessibility/AccessibilityManager;
    .end local v1    # "asList":Ljava/util/List;, "Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    .end local v3    # "i":I
    :cond_1
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setShowMode(I)V

    .line 569
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->BGMPause()V

    .line 570
    new-instance v5, Landroid/speech/tts/TextToSpeech;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {v5, v6, p0}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    iput-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    .line 572
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionEnd()I

    move-result v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v6

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionStart()I

    move-result v6

    sub-int/2addr v5, v6

    if-lez v5, :cond_3

    .line 573
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v6

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionStart()I

    move-result v6

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v7

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionStart()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setTTSSelection(II)V

    .line 574
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionStart()I

    move-result v5

    iput v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_ttsStartingPosition:I

    .line 576
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v5

    invoke-virtual {v5, v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectboundRect(Z)Landroid/graphics/Rect;

    move-result-object v4

    .line 577
    .local v4, "selRect":Landroid/graphics/Rect;
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 578
    .local v2, "drawingRect":Landroid/graphics/Rect;
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 580
    iget v5, v2, Landroid/graphics/Rect;->top:I

    iget v6, v4, Landroid/graphics/Rect;->top:I

    if-le v5, v6, :cond_2

    .line 581
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v5

    const/4 v6, 0x0

    iget v7, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v5, v6, v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->scrollTo(II)V

    .line 584
    :cond_2
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v5

    iget v6, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_ttsStartingPosition:I

    invoke-virtual {v5, v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    goto/16 :goto_1

    .line 586
    .end local v2    # "drawingRect":Landroid/graphics/Rect;
    .end local v4    # "selRect":Landroid/graphics/Rect;
    :cond_3
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setTTSStartSelection()V

    .line 587
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTTSSelectStart()I

    move-result v5

    iput v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_ttsStartingPosition:I

    goto/16 :goto_1
.end method

.method public onUtteranceCompleted(Ljava/lang/String;)V
    .locals 6
    .param p1, "utteranceId"    # Ljava/lang/String;

    .prologue
    const-wide/16 v4, 0x1f4

    .line 124
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->isPressBtn()Z

    move-result v0

    if-nez v0, :cond_2

    .line 125
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_finish:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->isPlay()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 127
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->mPrevStartTime:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-gez v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->mPrevHandler:Landroid/os/Handler;

    iget v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->MSG_DELAY_PLAY_TTS:I

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 137
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->mPrevStartTime:J

    .line 138
    return-void

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->mPrevHandler:Landroid/os/Handler;

    iget v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->MSG_DELAY_PLAY_TTS:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 133
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->doneTTS()V

    goto :goto_0

    .line 135
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->setPressBtn(Z)V

    goto :goto_0
.end method

.method public pause()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 367
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->ttsButtonImageChange(Z)V

    .line 370
    :cond_0
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->setPressBtn(Z)V

    .line 371
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->stop()V

    .line 372
    return-void
.end method

.method public play()V
    .locals 7

    .prologue
    .line 329
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    if-eqz v5, :cond_0

    .line 330
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->ttsButtonImageChange(Z)V

    .line 332
    :cond_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 334
    .local v4, "str":Ljava/lang/String;
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTTSSelectEnd()I

    move-result v5

    add-int/lit8 v3, v5, -0x1

    .line 335
    .local v3, "start":I
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTTSSelectEnd()I

    move-result v5

    add-int/lit8 v1, v5, -0x1

    .line 337
    .local v1, "end":I
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v2

    .line 338
    .local v2, "prevline":I
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v0

    .line 340
    .local v0, "curline":I
    if-nez v0, :cond_2

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineCount()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2

    .line 342
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->textReadAndPlay(Ljava/lang/String;)Z

    .line 359
    :cond_1
    :goto_0
    return-void

    .line 346
    :cond_2
    :goto_1
    if-ne v2, v0, :cond_3

    if-lez v3, :cond_3

    .line 348
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v5

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v5, v3}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v2

    goto :goto_1

    .line 351
    :cond_3
    if-eq v2, v0, :cond_4

    .line 352
    add-int/lit8 v3, v3, 0x1

    .line 356
    :cond_4
    if-ltz v3, :cond_1

    .line 357
    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v4, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->textReadAndPlay(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public rewind()V
    .locals 2

    .prologue
    .line 387
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    if-nez v0, :cond_0

    .line 395
    :goto_0
    return-void

    .line 389
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->isSpeaking()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 390
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->setPressBtn(Z)V

    .line 391
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->stop()V

    .line 393
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->mPrevHandler:Landroid/os/Handler;

    iget v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->MSG_DELAY_PLAY_TTS:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 394
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->movePrevLine()V

    goto :goto_0
.end method

.method public setIsPlay(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 237
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_bIsPlay:Z

    .line 238
    return-void
.end method

.method public setPressBtn(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 106
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_bPressBtn:Z

    .line 107
    return-void
.end method

.method public setTTSMode(I)V
    .locals 0
    .param p1, "flag"    # I

    .prologue
    .line 773
    iput p1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTSMode:I

    .line 774
    return-void
.end method

.method public setTTSStartPos(I)V
    .locals 0
    .param p1, "nPos"    # I

    .prologue
    .line 362
    iput p1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_ttsStartingPosition:I

    .line 363
    return-void
.end method

.method public textReadAndPlay(Ljava/lang/String;)Z
    .locals 8
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 782
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 783
    .local v0, "params":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v2, "textEditorTTS"

    .line 784
    .local v2, "utterance":Ljava/lang/String;
    const-string/jumbo v3, "utteranceId"

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 785
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->invalidate()V

    .line 786
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    if-eqz v3, :cond_0

    .line 787
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->isPlay()Z

    move-result v3

    if-ne v6, v3, :cond_1

    .line 788
    invoke-virtual {p0, v7}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->setPressBtn(Z)V

    .line 794
    :goto_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v7, v0}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    .line 796
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTTSSelectEnd()I

    move-result v3

    iput v3, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_ttsStartingPosition:I

    .line 798
    :cond_0
    return v6

    .line 790
    :cond_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->calcPosition()I

    move-result v1

    .line 791
    .local v1, "position":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07030e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 792
    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->setPressBtn(Z)V

    goto :goto_0
.end method

.method public unregisterReceiver()V
    .locals 2

    .prologue
    .line 825
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->mReceiver:Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;

    if-eqz v0, :cond_0

    .line 827
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->mReceiver:Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 829
    :cond_0
    return-void
.end method
