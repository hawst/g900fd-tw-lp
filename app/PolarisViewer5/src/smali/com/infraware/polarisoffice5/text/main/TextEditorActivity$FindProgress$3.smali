.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress$3;
.super Ljava/lang/Object;
.source "TextEditorActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;)V
    .locals 0

    .prologue
    .line 4434
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 4437
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->nMode:I
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->access$4700(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 4453
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->pd:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->access$4900(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 4454
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setLoadingProgress(Z)V

    .line 4455
    return-void

    .line 4439
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$2600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->nOffset:I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->access$4800(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;)I

    move-result v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findNextViewProcess(Ljava/lang/String;IZ)V

    .line 4440
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindNextFind:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1100(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->requestFocus()Z

    goto :goto_0

    .line 4443
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$2600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->nOffset:I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->access$4800(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findPrevViewProcess(Ljava/lang/String;I)V

    .line 4444
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindPrevFind:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->requestFocus()Z

    goto :goto_0

    .line 4447
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplaceFind:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$4500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->nOffset:I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->access$4800(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;)I

    move-result v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findNextViewProcess(Ljava/lang/String;IZ)V

    goto/16 :goto_0

    .line 4437
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
