.class Lcom/infraware/polarisoffice5/text/manager/TTSManager$1;
.super Ljava/lang/Object;
.source "TTSManager.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/manager/TTSManager;->initialize()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$1;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 1
    .param p1, "focusChange"    # I

    .prologue
    .line 82
    packed-switch p1, :pswitch_data_0

    .line 97
    :goto_0
    :pswitch_0
    return-void

    .line 85
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$1;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->doneTTS()V

    goto :goto_0

    .line 89
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$1;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->pause()V

    goto :goto_0

    .line 92
    :pswitch_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$1;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->play()V

    goto :goto_0

    .line 82
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
