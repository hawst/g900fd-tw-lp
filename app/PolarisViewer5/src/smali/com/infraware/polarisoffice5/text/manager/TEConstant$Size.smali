.class public interface abstract Lcom/infraware/polarisoffice5/text/manager/TEConstant$Size;
.super Ljava/lang/Object;
.source "TEConstant.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/text/manager/TEConstant;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Size"
.end annotation


# static fields
.field public static final SIZE_AUTOSCROLL_GAP:I = 0x5a

.field public static final SIZE_BLOCK:I = 0xbb8

.field public static final SIZE_BUFFER_HALF:I = 0x3

.field public static final SIZE_DEFAULT_ENGINE_DPI:I = 0xc8

.field public static final SIZE_DEFAULT_TWIP_PER_INCH:I = 0x5a0

.field public static final SIZE_DISPLAY_THRESHOLD:I = 0x320

.field public static final SIZE_DISTANCE:I = 0x32

.field public static final SIZE_DPI_MULTIPLIER_100:F = 1.39f

.field public static final SIZE_DPI_MULTIPLIER_150:F = 2.08f

.field public static final SIZE_DPI_MULTIPLIER_200:F = 2.78f

.field public static final SIZE_DPI_MULTIPLIER_300:F = 4.17f

.field public static final SIZE_DPI_MULTIPLIER_600:F = 8.33f

.field public static final SIZE_EDIT_LIMIT:I = 0xa

.field public static final SIZE_ENCODING_ROW_HEIGHT:I = 0x41

.field public static final SIZE_FIND_EDIT_LAND_WIDTH:I = 0x21d

.field public static final SIZE_FIND_EDIT_PORT_WIDTH:I = 0xdd

.field public static final SIZE_FLING_RESTRICT_VELOCITY:I = 0x3e8

.field public static final SIZE_FONTSIZE_10:I = 0xa

.field public static final SIZE_FONTSIZE_11:I = 0xb

.field public static final SIZE_FONTSIZE_12:I = 0xc

.field public static final SIZE_FONTSIZE_16:I = 0x10

.field public static final SIZE_FONTSIZE_20:I = 0x14

.field public static final SIZE_FONTSIZE_8:I = 0x8

.field public static final SIZE_FONTSIZE_9:I = 0x9

.field public static final SIZE_FONT_LAND_WIDTH_CENTER:I = 0x98

.field public static final SIZE_FONT_LAND_WIDTH_SIDE:I = 0x9a

.field public static final SIZE_FONT_PORT_WIDTH_CENTER:I = 0x58

.field public static final SIZE_FONT_PORT_WIDTH_SIDE:I = 0x59

.field public static final SIZE_MIN_EXAMINE_ENCODING:I = 0x68

.field public static final SIZE_PRINT_STANDARDHEIGHT:I = 0x34a

.field public static final SIZE_PRINT_STANDARDWIDTH:I = 0x253

.field public static final SIZE_REPLACE_EDIT_LAND_WIDTH:I = 0x229

.field public static final SIZE_REPLACE_EDIT_PORT_WIDTH:I = 0xe9

.field public static final SIZE_REPLACE_MAX_COUNT:I = 0x64

.field public static final SIZE_SELECTIONIMAGE_HEIGHT:I = 0x37

.field public static final SIZE_SELECTIONIMAGE_WIDTH:I = 0x20

.field public static final SIZE_STACKCOUNT:I = 0xa

.field public static final SIZE_TOASTPOPUP_BUTTON_HEIGHT:I = 0x64

.field public static final SIZE_TOASTPOPUP_BUTTON_WIDTH:I = 0x5a

.field public static final SIZE_TOUCH_OVERSCOPE_H:I = 0x3c

.field public static final SIZE_TOUCH_OVERSCOPE_V:I = 0x28

.field public static final SIZE_TOUCH_SELECTION_BAR_WIDTH:I = 0x3

.field public static final SIZE_TOUCH_SELECTION_IMAGE_SIZE:I = 0x2a

.field public static final SIZE_TOUCH_SELECTION_MARGIN:I = 0xf

.field public static final SIZE_VELOCITY:I = 0x32
