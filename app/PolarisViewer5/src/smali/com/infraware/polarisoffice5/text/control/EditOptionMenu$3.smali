.class Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$3;
.super Ljava/lang/Object;
.source "EditOptionMenu.java"

# interfaces
.implements Landroid/widget/PopupWindow$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->optionPopupwindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$3;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 211
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$3;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->isClicked:Z
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->access$300(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$3;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->access$400(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getViewMode()I

    move-result v0

    if-nez v0, :cond_0

    .line 212
    new-instance v0, Lcom/infraware/polarisoffice5/common/ImmManager;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$3;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->access$400(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/ImmManager;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/ImmManager;->showDelayedIme()V

    .line 213
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$3;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->access$400(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setMenuButtonSelected(Z)V

    .line 214
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$3;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->access$400(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setFormatIconSelected(Z)V

    .line 215
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$3;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->access$400(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setFindOptionIconSelected(Z)V

    .line 216
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$3;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->access$400(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setReplaceOptionIconSelected(Z)V

    .line 217
    return-void
.end method
