.class Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;
.super Ljava/lang/Thread;
.source "EditCtrl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/text/control/EditCtrl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ChangeBufferProgress"
.end annotation


# instance fields
.field private before:I

.field private count:I

.field private insert:Z

.field private pd:Landroid/app/ProgressDialog;

.field private s:Ljava/lang/CharSequence;

.field private start:I

.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;ZLjava/lang/CharSequence;III)V
    .locals 5
    .param p2, "bInsert"    # Z
    .param p3, "cs"    # Ljava/lang/CharSequence;
    .param p4, "nStart"    # I
    .param p5, "nBefore"    # I
    .param p6, "nCount"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 3841
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 3834
    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->pd:Landroid/app/ProgressDialog;

    .line 3835
    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->s:Ljava/lang/CharSequence;

    .line 3839
    iput-boolean v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->insert:Z

    .line 3842
    iput-boolean p2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->insert:Z

    .line 3843
    iput-object p3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->s:Ljava/lang/CharSequence;

    .line 3844
    iput p4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->start:I

    .line 3845
    iput p5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->before:I

    .line 3846
    iput p6, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->count:I

    .line 3848
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->pd:Landroid/app/ProgressDialog;

    .line 3849
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 3850
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 3851
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701bf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 3852
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 3854
    invoke-virtual {p1, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setLoadingProgress(Z)V

    .line 3855
    return-void
.end method

.method static synthetic access$3100(Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;

    .prologue
    .line 3833
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->pd:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;

    .prologue
    .line 3833
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->insert:Z

    return v0
.end method

.method static synthetic access$3300(Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;)Ljava/lang/CharSequence;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;

    .prologue
    .line 3833
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->s:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;

    .prologue
    .line 3833
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->start:I

    return v0
.end method

.method static synthetic access$3500(Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;

    .prologue
    .line 3833
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->before:I

    return v0
.end method

.method static synthetic access$3600(Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;

    .prologue
    .line 3833
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->count:I

    return v0
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 3858
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->loadBuffer()V

    .line 3860
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$2200(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress$1;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress$1;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 3873
    return-void
.end method
