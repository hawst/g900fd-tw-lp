.class public Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "EditGestureDetector.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Flag;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Size;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Type;


# instance fields
.field m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

.field private m_bAfterFling:Z


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V
    .locals 1
    .param p1, "aEdit"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_bAfterFling:Z

    .line 21
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .line 22
    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "evt"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    .line 142
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    .line 154
    :cond_0
    :goto_0
    return v4

    .line 147
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelectWord(II)V

    .line 148
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionStart()I

    move-result v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionEnd()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionStart()I

    move-result v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionEnd()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 150
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionStart()I

    move-result v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionEnd()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 151
    .local v0, "keyword":Ljava/lang/String;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->sendDictionaryMessage(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 161
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 163
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v1

    sub-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    const/16 v3, 0x64

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->showToastPopup(FFI)V

    .line 166
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "evt"    # Landroid/view/MotionEvent;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isFlingFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 27
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->stopFling()V

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_bAfterFling:Z

    .line 30
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 2
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 41
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->dismissPopup()V

    .line 43
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isSelectionChanged()Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    neg-float v1, p4

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->flickProcess(I)V

    .line 46
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 10
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x1

    .line 108
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v5

    const/4 v9, 0x2

    if-eq v5, v9, :cond_0

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v5

    const/4 v9, 0x3

    if-eq v5, v9, :cond_0

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v5

    const/4 v9, 0x4

    if-ne v5, v9, :cond_1

    .line 138
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v5

    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v9}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v9

    sub-int/2addr v5, v9

    if-eqz v5, :cond_3

    move v7, v2

    .line 116
    .local v7, "isSelection":Z
    :goto_1
    invoke-static {}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getInstance()Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    move-result-object v5

    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v9}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v9

    invoke-virtual {v5, v9}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->isNormalWindow(Landroid/app/Activity;)Z

    move-result v5

    if-nez v5, :cond_4

    move v6, v2

    .line 117
    .local v6, "isMW":Z
    :goto_2
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getMWDnD()Lcom/infraware/common/multiwindow/MWDnDOperator;

    move-result-object v0

    .line 119
    .local v0, "oMWDnD":Lcom/infraware/common/multiwindow/MWDnDOperator;
    const/4 v8, 0x0

    .line 120
    .local v8, "isSelectionArea":Z
    if-eqz v0, :cond_2

    if-ne v6, v2, :cond_2

    .line 121
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v0, v3, v5}, Lcom/infraware/common/multiwindow/MWDnDOperator;->isTextMarkArea(II)Z

    move-result v8

    .line 124
    :cond_2
    if-eqz v7, :cond_5

    if-eqz v6, :cond_5

    if-eqz v8, :cond_5

    .line 125
    invoke-virtual {v0}, Lcom/infraware/common/multiwindow/MWDnDOperator;->preoccupyOnEditCopy()V

    .line 126
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getMarkedString()Ljava/lang/String;

    move-result-object v3

    move v2, v1

    move v5, v1

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/common/multiwindow/MWDnDOperator;->onEditCopy(IILjava/lang/String;Ljava/lang/String;I)Z

    .line 127
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0, v1, v4}, Lcom/infraware/common/multiwindow/MWDnDOperator;->startDragText(Landroid/view/View;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .end local v0    # "oMWDnD":Lcom/infraware/common/multiwindow/MWDnDOperator;
    .end local v6    # "isMW":Z
    .end local v7    # "isSelection":Z
    .end local v8    # "isSelectionArea":Z
    :cond_3
    move v7, v3

    .line 115
    goto :goto_1

    .restart local v7    # "isSelection":Z
    :cond_4
    move v6, v3

    .line 116
    goto :goto_2

    .line 132
    .restart local v0    # "oMWDnD":Lcom/infraware/common/multiwindow/MWDnDOperator;
    .restart local v6    # "isMW":Z
    .restart local v8    # "isSelectionArea":Z
    :cond_5
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setIsLongTapEvent(Z)V

    .line 134
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelectWord(II)V

    .line 136
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v2

    sub-int/2addr v1, v2

    if-eqz v1, :cond_0

    .line 137
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->showDelayToastPopup()V

    goto/16 :goto_0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 11
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 171
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setIsScrolling(Z)V

    .line 173
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v2

    .line 175
    .local v2, "layout":Landroid/text/Layout;
    if-nez v2, :cond_0

    .line 176
    const/4 v7, 0x0

    .line 255
    :goto_0
    return v7

    .line 178
    :cond_0
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v7

    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v8

    sub-int/2addr v7, v8

    if-lez v7, :cond_c

    .line 179
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getShowMode()I

    move-result v7

    const/4 v8, 0x2

    if-eq v7, v8, :cond_1

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getShowMode()I

    move-result v7

    const/4 v8, 0x3

    if-ne v7, v8, :cond_2

    .line 180
    :cond_1
    const/4 v7, 0x0

    goto :goto_0

    .line 182
    :cond_2
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v5

    .line 183
    .local v5, "nStart":I
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v3

    .line 185
    .local v3, "nEnd":I
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isSelStartDown()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 186
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v7

    invoke-virtual {v2, v7}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v7

    if-nez v7, :cond_7

    .line 187
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    float-to-int v8, v8

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    float-to-int v9, v9

    add-int/lit8 v9, v9, -0x1b

    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineHeight()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    sub-int/2addr v9, v10

    invoke-virtual {v7, v8, v9}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTouchOffset(II)I

    move-result v5

    .line 194
    :goto_1
    if-lt v5, v3, :cond_3

    .line 195
    add-int/lit8 v5, v3, -0x1

    .line 196
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->selDownChange()V

    .line 200
    :cond_3
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7, v5, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(II)V

    .line 238
    .end local v3    # "nEnd":I
    .end local v5    # "nStart":I
    :cond_4
    :goto_2
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 239
    .local v1, "gr":Landroid/graphics/Rect;
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 241
    .local v0, "dr":Landroid/graphics/Rect;
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 243
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v7

    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v8

    if-eq v7, v8, :cond_6

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isSelEndDown()Z

    move-result v7

    if-nez v7, :cond_5

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isSelStartDown()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 244
    :cond_5
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v7

    iget v8, v1, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v8, v8, -0x5a

    int-to-float v8, v8

    cmpl-float v7, v7, v8

    if-lez v7, :cond_f

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v7

    invoke-virtual {v2, v7}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v7

    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-eq v7, v8, :cond_f

    .line 245
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 246
    iget v7, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineHeight()I

    move-result v8

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getScrollHeight()I

    move-result v8

    if-gt v7, v8, :cond_6

    .line 247
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v9}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineHeight()I

    move-result v9

    invoke-virtual {v7, v8, v9}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->scrollBy(II)V

    .line 255
    :cond_6
    :goto_3
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 191
    .end local v0    # "dr":Landroid/graphics/Rect;
    .end local v1    # "gr":Landroid/graphics/Rect;
    .restart local v3    # "nEnd":I
    .restart local v5    # "nStart":I
    :cond_7
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    float-to-int v8, v8

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    float-to-int v9, v9

    add-int/lit8 v9, v9, 0x1b

    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineHeight()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    add-int/2addr v9, v10

    invoke-virtual {v7, v8, v9}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTouchOffset(II)I

    move-result v5

    goto/16 :goto_1

    .line 201
    :cond_8
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isSelEndDown()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 202
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 203
    .local v6, "rect":Landroid/graphics/Rect;
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7, v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 204
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionEnd()I

    move-result v7

    invoke-virtual {v2, v7}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v7

    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-eq v7, v8, :cond_9

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionEnd()I

    move-result v7

    invoke-virtual {v2, v7}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v7

    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x2

    if-ne v7, v8, :cond_b

    :cond_9
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineCount()I

    move-result v7

    const/4 v8, 0x1

    if-eq v7, v8, :cond_b

    iget v7, v6, Landroid/graphics/Rect;->top:I

    if-eqz v7, :cond_b

    .line 205
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    float-to-int v8, v8

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    float-to-int v9, v9

    add-int/lit8 v9, v9, 0x1b

    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineHeight()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    add-int/2addr v9, v10

    invoke-virtual {v7, v8, v9}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTouchOffset(II)I

    move-result v3

    .line 211
    :goto_4
    if-gt v3, v5, :cond_a

    .line 212
    add-int/lit8 v3, v5, 0x1

    .line 213
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->selDownChange()V

    .line 217
    :cond_a
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7, v5, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(II)V

    goto/16 :goto_2

    .line 208
    :cond_b
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    float-to-int v8, v8

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    float-to-int v9, v9

    add-int/lit8 v9, v9, -0x1b

    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineHeight()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    sub-int/2addr v9, v10

    invoke-virtual {v7, v8, v9}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTouchOffset(II)I

    move-result v3

    goto :goto_4

    .line 220
    .end local v3    # "nEnd":I
    .end local v5    # "nStart":I
    .end local v6    # "rect":Landroid/graphics/Rect;
    :cond_c
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v4

    .line 222
    .local v4, "nSelection":I
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isSelEndDown()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 223
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 224
    .restart local v6    # "rect":Landroid/graphics/Rect;
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7, v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 225
    invoke-virtual {v2, v4}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v7

    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-eq v7, v8, :cond_d

    invoke-virtual {v2, v4}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v7

    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x2

    if-ne v7, v8, :cond_e

    :cond_d
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineCount()I

    move-result v7

    const/4 v8, 0x1

    if-eq v7, v8, :cond_e

    iget v7, v6, Landroid/graphics/Rect;->top:I

    if-eqz v7, :cond_e

    .line 226
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    float-to-int v8, v8

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    float-to-int v9, v9

    add-int/lit8 v9, v9, 0x1b

    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineHeight()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    add-int/2addr v9, v10

    invoke-virtual {v7, v8, v9}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTouchOffset(II)I

    move-result v4

    .line 233
    :goto_5
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    goto/16 :goto_2

    .line 229
    :cond_e
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    float-to-int v8, v8

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    float-to-int v9, v9

    add-int/lit8 v9, v9, -0x1b

    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineHeight()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    sub-int/2addr v9, v10

    invoke-virtual {v7, v8, v9}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTouchOffset(II)I

    move-result v4

    goto :goto_5

    .line 248
    .end local v4    # "nSelection":I
    .end local v6    # "rect":Landroid/graphics/Rect;
    .restart local v0    # "dr":Landroid/graphics/Rect;
    .restart local v1    # "gr":Landroid/graphics/Rect;
    :cond_f
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v7

    iget v8, v1, Landroid/graphics/Rect;->top:I

    add-int/lit8 v8, v8, 0x5a

    int-to-float v8, v8

    cmpg-float v7, v7, v8

    if-gez v7, :cond_6

    .line 249
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v7, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 250
    iget v7, v0, Landroid/graphics/Rect;->top:I

    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineHeight()I

    move-result v8

    sub-int/2addr v7, v8

    if-ltz v7, :cond_6

    .line 251
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v9}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineHeight()I

    move-result v9

    neg-int v9, v9

    invoke-virtual {v7, v8, v9}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->scrollBy(II)V

    goto/16 :goto_3
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "evt"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 51
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_bAfterFling:Z

    if-eqz v1, :cond_1

    .line 52
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_bAfterFling:Z

    .line 53
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_bAfterFling:Z

    .line 103
    :cond_0
    :goto_0
    return v0

    .line 56
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v1

    if-eq v1, v6, :cond_2

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_5

    .line 58
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v2

    sub-int/2addr v1, v2

    if-lez v1, :cond_4

    .line 59
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isFitTouchSelectionStart(II)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isFitTouchSelectionEnd(II)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 60
    :cond_3
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->showToastPopup(FF)V

    .line 64
    :cond_4
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->requestFocusFindeEditText()V

    goto :goto_0

    .line 68
    :cond_5
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v2

    sub-int/2addr v1, v2

    if-lez v1, :cond_7

    .line 69
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hideDictionaryPanel()V

    .line 70
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isFitTouchSelectionStart(II)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isFitTouchSelectionEnd(II)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 71
    :cond_6
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->showToastPopup(FF)V

    .line 80
    :cond_7
    :goto_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getViewMode()I

    move-result v1

    if-eq v1, v5, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getViewMode()I

    move-result v1

    if-eq v1, v6, :cond_0

    .line 86
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTouchOffset(II)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    .line 88
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isShowSoftKey()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 89
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isShowCursor()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 90
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->showToastPopup(FF)V

    .line 91
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isTexteditorInstance()Z

    move-result v1

    if-nez v1, :cond_0

    .line 92
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->clearFocus()V

    goto/16 :goto_0

    .line 73
    :cond_8
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTouchOffset(II)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    .line 76
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isToastpopupShowing()Z

    move-result v1

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isMorepopupShowing()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 77
    :cond_9
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->dismissPopup()V

    goto/16 :goto_1

    .line 95
    :cond_a
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setShowCursor(Z)V

    .line 96
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->invalidate()V

    goto/16 :goto_0

    .line 99
    :cond_b
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->showSoftKeyboard()Z

    goto/16 :goto_0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "evt"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    .line 35
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;->m_Edit:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setFlingFlag(Z)V

    .line 36
    return v1
.end method
