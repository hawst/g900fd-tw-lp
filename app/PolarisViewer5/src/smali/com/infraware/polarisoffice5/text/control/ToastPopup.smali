.class public Lcom/infraware/polarisoffice5/text/control/ToastPopup;
.super Ljava/lang/Object;
.source "ToastPopup.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Flag;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Type;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/text/control/ToastPopup$ToastPopupListener;
    }
.end annotation


# instance fields
.field private mListener:Lcom/infraware/polarisoffice5/text/control/ToastPopup$ToastPopupListener;

.field private m_ContentView:Landroid/view/View;

.field private final m_Parent:Landroid/view/View;

.field private final m_PopupWnd:Landroid/widget/PopupWindow;

.field private m_handler:Landroid/os/Handler;

.field private m_isshowing:Z

.field private m_morepopupWidth:I

.field private m_viewmode:I


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2
    .param p1, "parent"    # Landroid/view/View;

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_viewmode:I

    .line 35
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_isshowing:Z

    .line 37
    new-instance v0, Lcom/infraware/polarisoffice5/text/control/ToastPopup$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/control/ToastPopup$1;-><init>(Lcom/infraware/polarisoffice5/text/control/ToastPopup;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_handler:Landroid/os/Handler;

    .line 48
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_Parent:Landroid/view/View;

    .line 49
    new-instance v0, Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_Parent:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_PopupWnd:Landroid/widget/PopupWindow;

    .line 51
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_PopupWnd:Landroid/widget/PopupWindow;

    new-instance v1, Lcom/infraware/polarisoffice5/text/control/ToastPopup$2;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/text/control/ToastPopup$2;-><init>(Lcom/infraware/polarisoffice5/text/control/ToastPopup;)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setTouchInterceptor(Landroid/view/View$OnTouchListener;)V

    .line 63
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/text/control/ToastPopup;)Landroid/widget/PopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_PopupWnd:Landroid/widget/PopupWindow;

    return-object v0
.end method


# virtual methods
.method public dismiss()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 174
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_isshowing:Z

    .line 175
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 176
    return-void
.end method

.method public getPopup()Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_PopupWnd:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method public getViewMode()I
    .locals 1

    .prologue
    .line 187
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_viewmode:I

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 179
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_isshowing:Z

    return v0
.end method

.method public setContentView(I)V
    .locals 3
    .param p1, "layoutResID"    # I

    .prologue
    .line 80
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_Parent:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 81
    .local v0, "inflator":Landroid/view/LayoutInflater;
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->setContentView(Landroid/view/View;)V

    .line 82
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1
    .param p1, "contentView"    # Landroid/view/View;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_ContentView:Landroid/view/View;

    .line 76
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_PopupWnd:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 77
    return-void
.end method

.method public setFocusable(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 93
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_PopupWnd:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 94
    return-void
.end method

.method public setHeight(I)V
    .locals 1
    .param p1, "height"    # I

    .prologue
    .line 162
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_PopupWnd:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 163
    return-void
.end method

.method public setListener(Lcom/infraware/polarisoffice5/text/control/ToastPopup$ToastPopupListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/infraware/polarisoffice5/text/control/ToastPopup$ToastPopupListener;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->mListener:Lcom/infraware/polarisoffice5/text/control/ToastPopup$ToastPopupListener;

    .line 67
    return-void
.end method

.method public setMorePopupWidth(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 89
    iput p1, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_morepopupWidth:I

    .line 90
    return-void
.end method

.method public setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/widget/PopupWindow$OnDismissListener;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_PopupWnd:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 86
    return-void
.end method

.method public setViewMode(I)V
    .locals 0
    .param p1, "viewmode"    # I

    .prologue
    .line 183
    iput p1, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_viewmode:I

    .line 184
    return-void
.end method

.method public setWidth(I)V
    .locals 1
    .param p1, "width"    # I

    .prologue
    .line 166
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_PopupWnd:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 167
    return-void
.end method

.method public showToastPopup(III)V
    .locals 10
    .param p1, "type"    # I
    .param p2, "aXOffset"    # I
    .param p3, "yOffset"    # I

    .prologue
    .line 97
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_ContentView:Landroid/view/View;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_PopupWnd:Landroid/widget/PopupWindow;

    if-nez v5, :cond_1

    .line 159
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    if-gez p2, :cond_4

    const/4 v4, 0x0

    .line 103
    .local v4, "xOffset":I
    :goto_1
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_Parent:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 105
    .local v0, "assetManager":Landroid/content/res/AssetManager;
    if-eqz p1, :cond_2

    const/4 v5, 0x2

    if-ne p1, v5, :cond_5

    .line 109
    :cond_2
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_PopupWnd:Landroid/widget/PopupWindow;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 113
    :goto_2
    const/4 v5, 0x2

    if-ne p1, v5, :cond_6

    .line 114
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_PopupWnd:Landroid/widget/PopupWindow;

    iget v6, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_morepopupWidth:I

    invoke-virtual {v5, v6}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 117
    :goto_3
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_PopupWnd:Landroid/widget/PopupWindow;

    const/4 v6, -0x2

    invoke-virtual {v5, v6}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 118
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_PopupWnd:Landroid/widget/PopupWindow;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    .line 119
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_PopupWnd:Landroid/widget/PopupWindow;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 120
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_PopupWnd:Landroid/widget/PopupWindow;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 122
    const/4 v5, 0x2

    if-ne p1, v5, :cond_7

    .line 123
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_ContentView:Landroid/view/View;

    new-instance v6, Landroid/view/ViewGroup$LayoutParams;

    iget v7, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_morepopupWidth:I

    const/4 v8, -0x2

    invoke-direct {v6, v7, v8}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 124
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_ContentView:Landroid/view/View;

    iget v6, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_morepopupWidth:I

    const/4 v7, -0x2

    invoke-virtual {v5, v6, v7}, Landroid/view/View;->measure(II)V

    .line 130
    :goto_4
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_Parent:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .local v1, "context":Landroid/content/Context;
    move-object v3, v1

    .line 131
    check-cast v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .line 133
    .local v3, "tAct":Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 134
    .local v2, "rect":Landroid/graphics/Rect;
    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 136
    iget v5, v2, Landroid/graphics/Rect;->top:I

    if-ge p3, v5, :cond_3

    .line 137
    iget v5, v2, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_Parent:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    const/high16 v7, 0x41200000    # 10.0f

    invoke-static {v6, v7}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v6

    add-int p3, v5, v6

    .line 140
    :cond_3
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_0

    .line 143
    :pswitch_1
    const/4 v5, 0x1

    :try_start_0
    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_isshowing:Z

    .line 144
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_PopupWnd:Landroid/widget/PopupWindow;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_Parent:Landroid/view/View;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7, v4, p3}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 145
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->mListener:Lcom/infraware/polarisoffice5/text/control/ToastPopup$ToastPopupListener;

    invoke-interface {v5, v4, p3}, Lcom/infraware/polarisoffice5/text/control/ToastPopup$ToastPopupListener;->onToastPopupShowFinished(II)V
    :try_end_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 146
    :catch_0
    move-exception v5

    goto/16 :goto_0

    .end local v0    # "assetManager":Landroid/content/res/AssetManager;
    .end local v1    # "context":Landroid/content/Context;
    .end local v2    # "rect":Landroid/graphics/Rect;
    .end local v3    # "tAct":Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    .end local v4    # "xOffset":I
    :cond_4
    move v4, p2

    .line 101
    goto/16 :goto_1

    .line 111
    .restart local v0    # "assetManager":Landroid/content/res/AssetManager;
    .restart local v4    # "xOffset":I
    :cond_5
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_PopupWnd:Landroid/widget/PopupWindow;

    new-instance v6, Landroid/graphics/drawable/BitmapDrawable;

    new-instance v7, Landroid/content/res/Resources;

    new-instance v8, Landroid/util/DisplayMetrics;

    invoke-direct {v8}, Landroid/util/DisplayMetrics;-><init>()V

    new-instance v9, Landroid/content/res/Configuration;

    invoke-direct {v9}, Landroid/content/res/Configuration;-><init>()V

    invoke-direct {v7, v0, v8, v9}, Landroid/content/res/Resources;-><init>(Landroid/content/res/AssetManager;Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;)V

    invoke-direct {v6, v7}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v5, v6}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 116
    :cond_6
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_PopupWnd:Landroid/widget/PopupWindow;

    const/4 v6, -0x2

    invoke-virtual {v5, v6}, Landroid/widget/PopupWindow;->setWidth(I)V

    goto/16 :goto_3

    .line 126
    :cond_7
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_ContentView:Landroid/view/View;

    new-instance v6, Landroid/view/ViewGroup$LayoutParams;

    const/4 v7, -0x2

    const/4 v8, -0x2

    invoke-direct {v6, v7, v8}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 127
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_ContentView:Landroid/view/View;

    const/4 v6, -0x2

    const/4 v7, -0x2

    invoke-virtual {v5, v6, v7}, Landroid/view/View;->measure(II)V

    goto :goto_4

    .line 152
    .restart local v1    # "context":Landroid/content/Context;
    .restart local v2    # "rect":Landroid/graphics/Rect;
    .restart local v3    # "tAct":Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    :pswitch_2
    const/4 v5, 0x1

    :try_start_1
    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_isshowing:Z

    .line 153
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_PopupWnd:Landroid/widget/PopupWindow;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->m_Parent:Landroid/view/View;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7, v4, p3}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V
    :try_end_1
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 154
    :catch_1
    move-exception v5

    goto/16 :goto_0

    .line 140
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
