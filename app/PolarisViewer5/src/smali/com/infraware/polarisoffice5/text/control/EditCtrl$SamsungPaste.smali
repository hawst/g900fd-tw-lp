.class Lcom/infraware/polarisoffice5/text/control/EditCtrl$SamsungPaste;
.super Ljava/lang/Thread;
.source "EditCtrl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/text/control/EditCtrl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SamsungPaste"
.end annotation


# instance fields
.field clip:Ljava/lang/String;

.field pd:Landroid/app/ProgressDialog;

.field pos:I

.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 3769
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$SamsungPaste;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 3764
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$SamsungPaste;->pd:Landroid/app/ProgressDialog;

    .line 3770
    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_pasteStrSamsung:Ljava/lang/String;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$2700(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$SamsungPaste;->clip:Ljava/lang/String;

    .line 3772
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$SamsungPaste;->pd:Landroid/app/ProgressDialog;

    .line 3774
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$SamsungPaste;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 3775
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$SamsungPaste;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 3776
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$SamsungPaste;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070304

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 3778
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$SamsungPaste;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 3779
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 3782
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$SamsungPaste;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$SamsungPaste;->clip:Ljava/lang/String;

    # invokes: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->onSamsungPaste(Ljava/lang/String;)I
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$2800(Lcom/infraware/polarisoffice5/text/control/EditCtrl;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$SamsungPaste;->pos:I

    .line 3784
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$SamsungPaste;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$2200(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/infraware/polarisoffice5/text/control/EditCtrl$SamsungPaste$1;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$SamsungPaste$1;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl$SamsungPaste;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 3803
    return-void
.end method
