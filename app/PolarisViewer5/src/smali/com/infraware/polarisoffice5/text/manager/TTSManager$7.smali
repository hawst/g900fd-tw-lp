.class Lcom/infraware/polarisoffice5/text/manager/TTSManager$7;
.super Ljava/lang/Object;
.source "TTSManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/manager/TTSManager;->sayText()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)V
    .locals 0

    .prologue
    .line 720
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$7;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 723
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$7;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->access$100(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->isTTSToolbarEnable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 724
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$7;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->access$100(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setTTSToolbarEnable(Z)V

    .line 726
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$7;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->access$100(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getTTSToolbar()Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->invalidate()V

    .line 727
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$7;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->access$100(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->invalidate()V

    .line 728
    return-void
.end method
