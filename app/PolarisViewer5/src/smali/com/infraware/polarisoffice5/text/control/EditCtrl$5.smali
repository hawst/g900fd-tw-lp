.class Lcom/infraware/polarisoffice5/text/control/EditCtrl$5;
.super Ljava/lang/Object;
.source "EditCtrl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastPopupHandlerInitialize()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V
    .locals 0

    .prologue
    .line 1904
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$5;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1909
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$5;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$300(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/app/Activity;

    move-result-object v3

    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1910
    .local v0, "inflator":Landroid/view/LayoutInflater;
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$5;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    const v4, 0x7f030058

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    # setter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastPopupView:Landroid/view/View;
    invoke-static {v3, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$402(Lcom/infraware/polarisoffice5/text/control/EditCtrl;Landroid/view/View;)Landroid/view/View;

    .line 1912
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$5;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$500(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$5;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastPopupView:Landroid/view/View;
    invoke-static {v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$400(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->setContentView(Landroid/view/View;)V

    .line 1914
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$5;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$500(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$5;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopupListener:Lcom/infraware/polarisoffice5/text/control/ToastPopup$ToastPopupListener;
    invoke-static {v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$600(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Lcom/infraware/polarisoffice5/text/control/ToastPopup$ToastPopupListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->setListener(Lcom/infraware/polarisoffice5/text/control/ToastPopup$ToastPopupListener;)V

    .line 1916
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$5;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$5;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;
    invoke-static {v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$500(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    move-result-object v4

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$5;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastPopupView:Landroid/view/View;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$400(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/view/View;

    move-result-object v5

    # invokes: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setToastPopupEventListener(Lcom/infraware/polarisoffice5/text/control/ToastPopup;Landroid/view/View;)V
    invoke-static {v3, v4, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$700(Lcom/infraware/polarisoffice5/text/control/EditCtrl;Lcom/infraware/polarisoffice5/text/control/ToastPopup;Landroid/view/View;)V

    .line 1917
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$5;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastPopupView:Landroid/view/View;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$400(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6, v6}, Landroid/view/View;->measure(II)V

    .line 1918
    const/4 v1, 0x0

    .line 1919
    .local v1, "inscreen":Z
    const/4 v3, 0x2

    new-array v2, v3, [I

    .line 1920
    .local v2, "popupPos":[I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$5;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$5;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastPopupView:Landroid/view/View;
    invoke-static {v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$400(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$5;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastPopupView:Landroid/view/View;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$400(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    # invokes: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getPozOfPopupwindow([III)Z
    invoke-static {v3, v2, v4, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$800(Lcom/infraware/polarisoffice5/text/control/EditCtrl;[III)Z

    move-result v1

    .line 1921
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$5;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$500(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    .line 1922
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$5;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$500(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->getPopup()Landroid/widget/PopupWindow;

    move-result-object v3

    aget v4, v2, v6

    aget v5, v2, v7

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$5;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;
    invoke-static {v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$500(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    move-result-object v6

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->getPopup()Landroid/widget/PopupWindow;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v6

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$5;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$500(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    move-result-object v7

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->getPopup()Landroid/widget/PopupWindow;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/PopupWindow;->getHeight()I

    move-result v7

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/widget/PopupWindow;->update(IIII)V

    .line 1927
    :goto_0
    return-void

    .line 1923
    :cond_0
    if-eqz v1, :cond_1

    .line 1924
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$5;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$500(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    move-result-object v3

    aget v4, v2, v6

    aget v5, v2, v7

    invoke-virtual {v3, v6, v4, v5}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->showToastPopup(III)V

    goto :goto_0

    .line 1926
    :cond_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$5;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$500(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->dismiss()V

    goto :goto_0
.end method
