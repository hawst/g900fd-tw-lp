.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$13;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "TextEditorActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setGestureListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
    .locals 0

    .prologue
    .line 1625
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$13;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v4, 0x1

    const/high16 v3, 0x42480000    # 50.0f

    const/4 v2, 0x0

    .line 1628
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$13;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getViewMode()I

    move-result v0

    const/16 v1, -0x63

    if-eq v0, v1, :cond_1

    .line 1670
    :cond_0
    :goto_0
    return v2

    .line 1631
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$13;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->isOver10mb()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1634
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    .line 1636
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$13;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getViewMode()I

    move-result v0

    if-eq v0, v4, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$13;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getViewMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 1639
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$13;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setViewMode(I)V

    .line 1640
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    goto :goto_0

    .line 1653
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$13;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0, v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setViewMode(I)V

    .line 1654
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    goto :goto_0
.end method
