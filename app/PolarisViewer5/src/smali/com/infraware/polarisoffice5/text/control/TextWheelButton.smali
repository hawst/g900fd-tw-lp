.class public Lcom/infraware/polarisoffice5/text/control/TextWheelButton;
.super Lcom/infraware/polarisoffice5/common/WheelButton;
.source "TextWheelButton.java"


# instance fields
.field private m_Context:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/common/WheelButton;-><init>(Landroid/content/Context;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/infraware/polarisoffice5/common/WheelButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    check-cast p1, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/TextWheelButton;->m_Context:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    .line 21
    return-void
.end method

.method private btnEnableCheck(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 45
    if-nez p1, :cond_1

    .line 46
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/TextWheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 47
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/TextWheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 48
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/TextWheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getMaxDataIndex()I

    move-result v0

    if-ne p1, v0, :cond_2

    .line 50
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/TextWheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 51
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/TextWheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0

    .line 54
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/TextWheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_3

    .line 55
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/TextWheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 56
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/TextWheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/TextWheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0
.end method


# virtual methods
.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 63
    invoke-super {p0, p1}, Lcom/infraware/polarisoffice5/common/WheelButton;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 64
    invoke-static {}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->configurationChanged()V

    .line 65
    return-void
.end method

.method public onItemScroll(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/text/control/TextWheelButton;->btnEnableCheck(I)V

    .line 71
    add-int/lit8 v0, p1, 0x1

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    add-int/lit8 v0, p1, 0x1

    const/16 v1, 0x9

    if-eq v0, v1, :cond_0

    add-int/lit8 v0, p1, 0x1

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    add-int/lit8 v0, p1, 0x1

    const/16 v1, 0xc

    if-eq v0, v1, :cond_0

    add-int/lit8 v0, p1, 0x1

    const/16 v1, 0xe

    if-eq v0, v1, :cond_0

    add-int/lit8 v0, p1, 0x1

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    add-int/lit8 v0, p1, 0x1

    const/16 v1, 0x14

    if-ne v0, v1, :cond_1

    .line 75
    :cond_0
    add-int/lit8 p1, p1, 0x1

    .line 76
    packed-switch p1, :pswitch_data_0

    .line 103
    :goto_0
    :pswitch_0
    return-void

    .line 78
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/TextWheelButton;->m_Context:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setFontSizeSelected(I)V

    goto :goto_0

    .line 81
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/TextWheelButton;->m_Context:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setFontSizeSelected(I)V

    goto :goto_0

    .line 84
    :pswitch_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/TextWheelButton;->m_Context:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setFontSizeSelected(I)V

    goto :goto_0

    .line 87
    :pswitch_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/TextWheelButton;->m_Context:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setFontSizeSelected(I)V

    goto :goto_0

    .line 90
    :pswitch_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/TextWheelButton;->m_Context:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setFontSizeSelected(I)V

    goto :goto_0

    .line 93
    :pswitch_6
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/TextWheelButton;->m_Context:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setFontSizeSelected(I)V

    goto :goto_0

    .line 96
    :pswitch_7
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/TextWheelButton;->m_Context:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setFontSizeSelected(I)V

    goto :goto_0

    .line 100
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/TextWheelButton;->m_Context:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    const/16 v1, -0x63

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setFontSizeSelected(I)V

    .line 101
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/TextWheelButton;->m_Context:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setFontSizeAtWheelButton(I)V

    goto :goto_0

    .line 76
    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method public onItemSelect(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v2, 0x1

    .line 28
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/TextWheelButton;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->createInputNumberPopupWindow(Landroid/app/Activity;)Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    move-result-object v0

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/TextWheelButton;->getIntData()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->setDefaultValue(I)Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->setMin(I)Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    move-result-object v0

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->setMax(I)Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    move-result-object v0

    const v1, 0x7f07013e

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->setTitle(I)Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->setPopupType(I)Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    move-result-object v0

    new-instance v1, Lcom/infraware/polarisoffice5/text/control/TextWheelButton$1;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/text/control/TextWheelButton$1;-><init>(Lcom/infraware/polarisoffice5/text/control/TextWheelButton;)V

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->setInputNumberListener(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$InputNumberListener;)Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->show()V

    .line 42
    return-void
.end method
