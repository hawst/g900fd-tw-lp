.class public Lcom/infraware/polarisoffice5/text/control/EditCtrl;
.super Landroid/widget/EditText;
.source "EditCtrl.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Flag;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Size;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$StringReference;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Type;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;,
        Lcom/infraware/polarisoffice5/text/control/EditCtrl$LoadBufferProgress;,
        Lcom/infraware/polarisoffice5/text/control/EditCtrl$SamsungPaste;,
        Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;,
        Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;,
        Lcom/infraware/polarisoffice5/text/control/EditCtrl$PopupViewerSearchViewHolder;,
        Lcom/infraware/polarisoffice5/text/control/EditCtrl$PopupViewerSearchAdapter;
    }
.end annotation


# static fields
.field private static final SCROLL_END:I = 0x1


# instance fields
.field isPreDraw:Z

.field private mContext:Landroid/content/Context;

.field mOnClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private m_ActParent:Landroid/app/Activity;

.field private m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

.field private m_ClipboardMgr:Landroid/text/ClipboardManager;

.field private m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

.field private m_GestureDetector:Landroid/view/GestureDetector;

.field private m_LastDistance:F

.field private m_Scroller:Landroid/widget/Scroller;

.field private m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

.field private m_ToastPopupListener:Lcom/infraware/polarisoffice5/text/control/ToastPopup$ToastPopupListener;

.field private m_UndoRedoMgr:Lcom/infraware/polarisoffice5/text/manager/CommandManager;

.field private m_ad:Landroid/app/AlertDialog;

.field private m_bBufferChanged:Z

.field private m_bChanged:Z

.field private m_bEnableDrawScroll:Z

.field private m_bEnableInsert:Z

.field private m_bEnableScroll:Z

.field private m_bFlickbStart:Z

.field private m_bFling:Z

.field private m_bHasText:Z

.field private m_bIsLongTapEvent:Z

.field private m_bIsScrolling:Z

.field private m_bLoadingProgress:Z

.field private m_bNextBlockLoad:Z

.field private m_bNowChangeFontsize:Z

.field private m_bPinchScaleUp:Z

.field private m_bPreBlockLoad:Z

.field private m_bSelEndDown:Z

.field private m_bSelStartDown:Z

.field private m_bSelectMultiLine:Z

.field private m_bSelectionchanged:Z

.field private m_bShowCursor:Z

.field private m_bShowSoftKeyboard:Z

.field private m_bTTSDrawline:Z

.field private m_bUndoRedoFlag:Z

.field private m_clipboardService:I

.field private m_drawSelectionBottom:I

.field private m_drawSelectionTop:I

.field private m_fontSizePool:[I

.field private m_fontsizeIndex:I

.field private m_handler:Landroid/os/Handler;

.field private m_isLoadingCanceled:Z

.field private m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

.field private m_morepopupCoordinate:[I

.field private m_moretoastPopupRunnable:Ljava/lang/Runnable;

.field private m_nBlockEndOffset:I

.field private m_nBlockStartOffset:I

.field private m_nCurBlock:I

.field private m_nFlickStopPos:I

.field private m_nScrollHeight:I

.field private m_nScrollPos:I

.field private m_nScrollY:I

.field private m_nTTSEnd:I

.field private m_nTTSStart:I

.field private m_nToastPopupState:I

.field private m_nVelocityY:I

.field private m_pasteStrSamsung:Ljava/lang/String;

.field private m_pd:Landroid/app/ProgressDialog;

.field private m_rectLeftCursor:Landroid/graphics/Rect;

.field private m_rectRightCursor:Landroid/graphics/Rect;

.field private m_secClipboard:Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

.field private m_theme:I

.field private m_toastPopupRunnable:Ljava/lang/Runnable;

.field private morePopupView:Landroid/view/View;

.field private toastHandler:Landroid/os/Handler;

.field private toastPopupView:Landroid/view/View;

.field private toastpopupRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 187
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 76
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    .line 77
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bChanged:Z

    .line 78
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableInsert:Z

    .line 79
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bNextBlockLoad:Z

    .line 80
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bPreBlockLoad:Z

    .line 82
    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nToastPopupState:I

    .line 83
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nTTSStart:I

    .line 84
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nTTSEnd:I

    .line 86
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    .line 87
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    .line 88
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableScroll:Z

    .line 90
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelStartDown:Z

    .line 91
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelEndDown:Z

    .line 93
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bShowCursor:Z

    .line 94
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bShowSoftKeyboard:Z

    .line 96
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    .line 97
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_UndoRedoMgr:Lcom/infraware/polarisoffice5/text/manager/CommandManager;

    .line 98
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_GestureDetector:Landroid/view/GestureDetector;

    .line 99
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ClipboardMgr:Landroid/text/ClipboardManager;

    .line 101
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    .line 102
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollY:I

    .line 104
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_Scroller:Landroid/widget/Scroller;

    .line 105
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_handler:Landroid/os/Handler;

    .line 107
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollHeight:I

    .line 108
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollPos:I

    .line 109
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bFlickbStart:Z

    .line 110
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nFlickStopPos:I

    .line 112
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    .line 113
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    .line 115
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_clipboardService:I

    .line 116
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_pasteStrSamsung:Ljava/lang/String;

    .line 118
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableDrawScroll:Z

    .line 119
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bLoadingProgress:Z

    .line 120
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bHasText:Z

    .line 122
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bIsScrolling:Z

    .line 123
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_drawSelectionTop:I

    .line 124
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_drawSelectionBottom:I

    .line 126
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bUndoRedoFlag:Z

    .line 128
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_theme:I

    .line 134
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nVelocityY:I

    .line 136
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    .line 138
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_isLoadingCanceled:Z

    .line 139
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bTTSDrawline:Z

    .line 141
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->mContext:Landroid/content/Context;

    .line 154
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bBufferChanged:Z

    .line 155
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bFling:Z

    .line 158
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_LastDistance:F

    .line 159
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bPinchScaleUp:Z

    .line 161
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_fontSizePool:[I

    .line 162
    const/4 v0, 0x3

    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_fontsizeIndex:I

    .line 164
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bNowChangeFontsize:Z

    .line 169
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_pd:Landroid/app/ProgressDialog;

    .line 171
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ad:Landroid/app/AlertDialog;

    .line 173
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelectionchanged:Z

    .line 175
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastHandler:Landroid/os/Handler;

    .line 177
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelectMultiLine:Z

    .line 179
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bIsLongTapEvent:Z

    .line 181
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_toastPopupRunnable:Ljava/lang/Runnable;

    .line 182
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_moretoastPopupRunnable:Ljava/lang/Runnable;

    .line 1184
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isPreDraw:Z

    .line 2761
    new-instance v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$13;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$13;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->mOnClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 188
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->mContext:Landroid/content/Context;

    .line 189
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->newInfo()V

    .line 190
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setEventListener()V

    .line 192
    return-void

    .line 161
    nop

    :array_0
    .array-data 4
        0x8
        0x9
        0xa
        0xc
        0xe
        0x10
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 195
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 76
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    .line 77
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bChanged:Z

    .line 78
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableInsert:Z

    .line 79
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bNextBlockLoad:Z

    .line 80
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bPreBlockLoad:Z

    .line 82
    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nToastPopupState:I

    .line 83
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nTTSStart:I

    .line 84
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nTTSEnd:I

    .line 86
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    .line 87
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    .line 88
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableScroll:Z

    .line 90
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelStartDown:Z

    .line 91
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelEndDown:Z

    .line 93
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bShowCursor:Z

    .line 94
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bShowSoftKeyboard:Z

    .line 96
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    .line 97
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_UndoRedoMgr:Lcom/infraware/polarisoffice5/text/manager/CommandManager;

    .line 98
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_GestureDetector:Landroid/view/GestureDetector;

    .line 99
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ClipboardMgr:Landroid/text/ClipboardManager;

    .line 101
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    .line 102
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollY:I

    .line 104
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_Scroller:Landroid/widget/Scroller;

    .line 105
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_handler:Landroid/os/Handler;

    .line 107
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollHeight:I

    .line 108
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollPos:I

    .line 109
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bFlickbStart:Z

    .line 110
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nFlickStopPos:I

    .line 112
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    .line 113
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    .line 115
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_clipboardService:I

    .line 116
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_pasteStrSamsung:Ljava/lang/String;

    .line 118
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableDrawScroll:Z

    .line 119
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bLoadingProgress:Z

    .line 120
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bHasText:Z

    .line 122
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bIsScrolling:Z

    .line 123
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_drawSelectionTop:I

    .line 124
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_drawSelectionBottom:I

    .line 126
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bUndoRedoFlag:Z

    .line 128
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_theme:I

    .line 134
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nVelocityY:I

    .line 136
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    .line 138
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_isLoadingCanceled:Z

    .line 139
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bTTSDrawline:Z

    .line 141
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->mContext:Landroid/content/Context;

    .line 154
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bBufferChanged:Z

    .line 155
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bFling:Z

    .line 158
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_LastDistance:F

    .line 159
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bPinchScaleUp:Z

    .line 161
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_fontSizePool:[I

    .line 162
    const/4 v0, 0x3

    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_fontsizeIndex:I

    .line 164
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bNowChangeFontsize:Z

    .line 169
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_pd:Landroid/app/ProgressDialog;

    .line 171
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ad:Landroid/app/AlertDialog;

    .line 173
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelectionchanged:Z

    .line 175
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastHandler:Landroid/os/Handler;

    .line 177
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelectMultiLine:Z

    .line 179
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bIsLongTapEvent:Z

    .line 181
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_toastPopupRunnable:Ljava/lang/Runnable;

    .line 182
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_moretoastPopupRunnable:Ljava/lang/Runnable;

    .line 1184
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isPreDraw:Z

    .line 2761
    new-instance v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$13;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$13;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->mOnClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 196
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->mContext:Landroid/content/Context;

    .line 198
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 199
    return-void

    .line 161
    :array_0
    .array-data 4
        0x8
        0x9
        0xa
        0xc
        0xe
        0x10
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 202
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 76
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    .line 77
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bChanged:Z

    .line 78
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableInsert:Z

    .line 79
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bNextBlockLoad:Z

    .line 80
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bPreBlockLoad:Z

    .line 82
    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nToastPopupState:I

    .line 83
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nTTSStart:I

    .line 84
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nTTSEnd:I

    .line 86
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    .line 87
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    .line 88
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableScroll:Z

    .line 90
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelStartDown:Z

    .line 91
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelEndDown:Z

    .line 93
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bShowCursor:Z

    .line 94
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bShowSoftKeyboard:Z

    .line 96
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    .line 97
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_UndoRedoMgr:Lcom/infraware/polarisoffice5/text/manager/CommandManager;

    .line 98
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_GestureDetector:Landroid/view/GestureDetector;

    .line 99
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ClipboardMgr:Landroid/text/ClipboardManager;

    .line 101
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    .line 102
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollY:I

    .line 104
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_Scroller:Landroid/widget/Scroller;

    .line 105
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_handler:Landroid/os/Handler;

    .line 107
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollHeight:I

    .line 108
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollPos:I

    .line 109
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bFlickbStart:Z

    .line 110
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nFlickStopPos:I

    .line 112
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    .line 113
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    .line 115
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_clipboardService:I

    .line 116
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_pasteStrSamsung:Ljava/lang/String;

    .line 118
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableDrawScroll:Z

    .line 119
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bLoadingProgress:Z

    .line 120
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bHasText:Z

    .line 122
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bIsScrolling:Z

    .line 123
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_drawSelectionTop:I

    .line 124
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_drawSelectionBottom:I

    .line 126
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bUndoRedoFlag:Z

    .line 128
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_theme:I

    .line 134
    iput v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nVelocityY:I

    .line 136
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    .line 138
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_isLoadingCanceled:Z

    .line 139
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bTTSDrawline:Z

    .line 141
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->mContext:Landroid/content/Context;

    .line 154
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bBufferChanged:Z

    .line 155
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bFling:Z

    .line 158
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_LastDistance:F

    .line 159
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bPinchScaleUp:Z

    .line 161
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_fontSizePool:[I

    .line 162
    const/4 v0, 0x3

    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_fontsizeIndex:I

    .line 164
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bNowChangeFontsize:Z

    .line 169
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_pd:Landroid/app/ProgressDialog;

    .line 171
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ad:Landroid/app/AlertDialog;

    .line 173
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelectionchanged:Z

    .line 175
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastHandler:Landroid/os/Handler;

    .line 177
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelectMultiLine:Z

    .line 179
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bIsLongTapEvent:Z

    .line 181
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_toastPopupRunnable:Ljava/lang/Runnable;

    .line 182
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_moretoastPopupRunnable:Ljava/lang/Runnable;

    .line 1184
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isPreDraw:Z

    .line 2761
    new-instance v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$13;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$13;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->mOnClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 203
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->mContext:Landroid/content/Context;

    .line 204
    return-void

    .line 161
    nop

    :array_0
    .array-data 4
        0x8
        0x9
        0xa
        0xc
        0xe
        0x10
    .end array-data
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)[I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morepopupCoordinate:[I

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastpopupUpdate()V

    return-void
.end method

.method static synthetic access$1000(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->morePopupView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Lcom/infraware/polarisoffice5/text/control/ToastPopup;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->onCut()V

    return-void
.end method

.method static synthetic access$1300(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->onCopy()V

    return-void
.end method

.method static synthetic access$1400(Lcom/infraware/polarisoffice5/text/control/EditCtrl;I)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    .param p1, "x1"    # I

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->onClickSearchItem(I)V

    return-void
.end method

.method static synthetic access$1500(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_pd:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/infraware/polarisoffice5/text/control/EditCtrl;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    .param p1, "x1"    # Landroid/app/ProgressDialog;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_pd:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_isLoadingCanceled:Z

    return v0
.end method

.method static synthetic access$1602(Lcom/infraware/polarisoffice5/text/control/EditCtrl;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    .param p1, "x1"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_isLoadingCanceled:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .prologue
    .line 73
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    return v0
.end method

.method static synthetic access$1802(Lcom/infraware/polarisoffice5/text/control/EditCtrl;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    .param p1, "x1"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    return p1
.end method

.method static synthetic access$2002(Lcom/infraware/polarisoffice5/text/control/EditCtrl;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    .param p1, "x1"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableInsert:Z

    return p1
.end method

.method static synthetic access$202(Lcom/infraware/polarisoffice5/text/control/EditCtrl;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    .param p1, "x1"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bNowChangeFontsize:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setCurBlockBound()V

    return-void
.end method

.method static synthetic access$2200(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->onPaste()I

    move-result v0

    return v0
.end method

.method static synthetic access$2500(Lcom/infraware/polarisoffice5/text/control/EditCtrl;I)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    .param p1, "x1"    # I

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->pasteViewProcess(I)V

    return-void
.end method

.method static synthetic access$2700(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_pasteStrSamsung:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/infraware/polarisoffice5/text/control/EditCtrl;Ljava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->onSamsungPaste(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$2900(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastPopupView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$402(Lcom/infraware/polarisoffice5/text/control/EditCtrl;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastPopupView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Lcom/infraware/polarisoffice5/text/control/ToastPopup;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    return-object v0
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Lcom/infraware/polarisoffice5/text/control/ToastPopup$ToastPopupListener;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopupListener:Lcom/infraware/polarisoffice5/text/control/ToastPopup$ToastPopupListener;

    return-object v0
.end method

.method static synthetic access$700(Lcom/infraware/polarisoffice5/text/control/EditCtrl;Lcom/infraware/polarisoffice5/text/control/ToastPopup;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    .param p1, "x1"    # Lcom/infraware/polarisoffice5/text/control/ToastPopup;
    .param p2, "x2"    # Landroid/view/View;

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setToastPopupEventListener(Lcom/infraware/polarisoffice5/text/control/ToastPopup;Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$800(Lcom/infraware/polarisoffice5/text/control/EditCtrl;[III)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    .param p1, "x1"    # [I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 73
    invoke-direct {p0, p1, p2, p3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getPozOfPopupwindow([III)Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setmorePopuplayout()V

    return-void
.end method

.method private buildLookupUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "select"    # Ljava/lang/String;
    .param p2, "lang"    # Ljava/lang/String;

    .prologue
    .line 2874
    move-object v3, p2

    .line 2875
    .local v3, "m_strTargetLanguage":Ljava/lang/String;
    move-object v2, p1

    .line 2876
    .local v2, "m_strLookupWord":Ljava/lang/String;
    const/4 v1, 0x0

    .line 2879
    .local v1, "m_strLookupUrl":Ljava/lang/String;
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "http://"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ".wikipedia.org/wiki/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "UTF-8"

    invoke-static {v2, v5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2881
    const-string/jumbo v4, "\\+"

    const-string/jumbo v5, "%20"

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2886
    :goto_0
    return-object v1

    .line 2882
    :catch_0
    move-exception v0

    .line 2883
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method private checkBeforeDiff(Ljava/lang/CharSequence;II)V
    .locals 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I

    .prologue
    .line 1576
    add-int v2, p2, p3

    invoke-interface {p1, p2, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1577
    .local v0, "strBefore":Ljava/lang/CharSequence;
    iget v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    add-int v3, p2, p3

    invoke-virtual {p0, v2, p2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSubBufferText(III)Ljava/lang/String;

    move-result-object v1

    .line 1580
    .local v1, "strBufBefore":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    .line 1581
    iget v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    add-int v3, p2, p3

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v2, p2, v3, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->replaceBuffer(IIILjava/lang/String;)V

    .line 1582
    :cond_0
    return-void
.end method

.method private drawCursor(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v11, 0x0

    const/4 v13, 0x0

    .line 1016
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v5

    .line 1017
    .local v5, "nSelStart":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v4

    .line 1018
    .local v4, "nSelEnd":I
    const/4 v3, 0x0

    .line 1020
    .local v3, "isLastLine":Z
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 1021
    .local v6, "rect":Landroid/graphics/Rect;
    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 1023
    sub-int v9, v4, v5

    if-nez v9, :cond_0

    .line 1024
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v9

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v9

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineCount()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    if-ne v9, v10, :cond_2

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineCount()I

    move-result v9

    const/4 v10, 0x1

    if-eq v9, v10, :cond_2

    iget v9, v6, Landroid/graphics/Rect;->top:I

    if-eqz v9, :cond_2

    .line 1025
    const/4 v3, 0x1

    .line 1031
    :cond_0
    :goto_0
    sub-int v9, v4, v5

    if-nez v9, :cond_1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isShowSoftKey()Z

    move-result v9

    if-eqz v9, :cond_1

    iget-boolean v9, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bShowCursor:Z

    if-eqz v9, :cond_1

    .line 1032
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1034
    invoke-direct {p0, v11}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBoundRect(Z)Landroid/graphics/Rect;

    move-result-object v8

    .line 1036
    .local v8, "rectCursor":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f02031c

    invoke-static {v9, v10}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1038
    .local v0, "bmBar":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f02031f

    invoke-static {v9, v10}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1039
    .local v1, "cursorNormal":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f020320

    invoke-static {v9, v10}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1041
    .local v2, "cursorPressed":Landroid/graphics/Bitmap;
    new-instance v7, Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    invoke-direct {v7, v11, v11, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1042
    .local v7, "rectBar":Landroid/graphics/Rect;
    iget v9, v8, Landroid/graphics/Rect;->right:I

    add-int/lit8 v9, v9, 0x2

    iput v9, v8, Landroid/graphics/Rect;->right:I

    .line 1043
    iget v9, v8, Landroid/graphics/Rect;->left:I

    add-int/lit8 v9, v9, -0x2

    iput v9, v8, Landroid/graphics/Rect;->left:I

    .line 1045
    invoke-virtual {p1, v0, v7, v8, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1047
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1049
    iget-boolean v9, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelEndDown:Z

    if-nez v9, :cond_4

    .line 1050
    if-eqz v3, :cond_3

    .line 1051
    invoke-direct {p0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->makeReverseBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v9

    iget v10, v8, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    sub-int/2addr v10, v11

    add-int/lit8 v10, v10, 0x2

    int-to-float v10, v10

    iget v11, v8, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    sub-int/2addr v11, v12

    add-int/lit8 v11, v11, 0xf

    int-to-float v11, v11

    invoke-virtual {p1, v9, v10, v11, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1068
    :goto_1
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1069
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 1071
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1073
    .end local v0    # "bmBar":Landroid/graphics/Bitmap;
    .end local v1    # "cursorNormal":Landroid/graphics/Bitmap;
    .end local v2    # "cursorPressed":Landroid/graphics/Bitmap;
    .end local v7    # "rectBar":Landroid/graphics/Rect;
    .end local v8    # "rectCursor":Landroid/graphics/Rect;
    :cond_1
    return-void

    .line 1027
    :cond_2
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 1054
    .restart local v0    # "bmBar":Landroid/graphics/Bitmap;
    .restart local v1    # "cursorNormal":Landroid/graphics/Bitmap;
    .restart local v2    # "cursorPressed":Landroid/graphics/Bitmap;
    .restart local v7    # "rectBar":Landroid/graphics/Rect;
    .restart local v8    # "rectCursor":Landroid/graphics/Rect;
    :cond_3
    iget v9, v8, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    sub-int/2addr v9, v10

    add-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    iget v10, v8, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v10, v10, -0xf

    int-to-float v10, v10

    invoke-virtual {p1, v1, v9, v10, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 1059
    :cond_4
    if-eqz v3, :cond_5

    .line 1060
    invoke-direct {p0, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->makeReverseBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v9

    iget v10, v8, Landroid/graphics/Rect;->left:I

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    sub-int/2addr v10, v11

    add-int/lit8 v10, v10, 0x2

    int-to-float v10, v10

    iget v11, v8, Landroid/graphics/Rect;->top:I

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    sub-int/2addr v11, v12

    add-int/lit8 v11, v11, 0xf

    int-to-float v11, v11

    invoke-virtual {p1, v9, v10, v11, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 1063
    :cond_5
    iget v9, v8, Landroid/graphics/Rect;->left:I

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    sub-int/2addr v9, v10

    add-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    iget v10, v8, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v10, v10, -0xf

    int-to-float v10, v10

    invoke-virtual {p1, v2, v9, v10, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1
.end method

.method private drawSelectBound(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v12, 0x1

    const/4 v13, 0x0

    .line 860
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v5

    .line 861
    .local v5, "nSelStart":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v4

    .line 863
    .local v4, "nSelEnd":I
    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_rectLeftCursor:Landroid/graphics/Rect;

    if-nez v9, :cond_0

    .line 864
    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    iput-object v9, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_rectLeftCursor:Landroid/graphics/Rect;

    .line 865
    :cond_0
    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_rectRightCursor:Landroid/graphics/Rect;

    if-nez v9, :cond_1

    .line 866
    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    iput-object v9, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_rectRightCursor:Landroid/graphics/Rect;

    .line 868
    :cond_1
    sub-int v9, v4, v5

    if-lez v9, :cond_2

    .line 869
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 871
    invoke-direct {p0, v12}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBoundRect(Z)Landroid/graphics/Rect;

    move-result-object v8

    .line 872
    .local v8, "rectStart":Landroid/graphics/Rect;
    const/4 v9, 0x0

    invoke-direct {p0, v9}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBoundRect(Z)Landroid/graphics/Rect;

    move-result-object v7

    .line 874
    .local v7, "rectEnd":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f020329

    invoke-static {v9, v10}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 875
    .local v0, "cursorLeftNormal":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f02032a

    invoke-static {v9, v10}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 876
    .local v1, "cursorLeftPressed":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f02031d

    invoke-static {v9, v10}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 877
    .local v2, "cursorRightNormal":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f02031e

    invoke-static {v9, v10}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 879
    .local v3, "cursorRightPressed":Landroid/graphics/Bitmap;
    iget-boolean v9, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelStartDown:Z

    if-nez v9, :cond_6

    iget-boolean v9, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelEndDown:Z

    if-nez v9, :cond_6

    .line 880
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v9

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v9

    if-nez v9, :cond_4

    .line 882
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v9

    if-nez v9, :cond_3

    .line 883
    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->makeReverseSymmetryBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v9

    iget v10, v8, Landroid/graphics/Rect;->left:I

    int-to-float v10, v10

    iget v11, v8, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v11, v11, -0xf

    int-to-float v11, v11

    invoke-virtual {p1, v9, v10, v11, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 894
    :goto_0
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 895
    .local v6, "rect":Landroid/graphics/Rect;
    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 897
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v9

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v9

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineCount()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    if-ne v9, v10, :cond_5

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineCount()I

    move-result v9

    if-eq v9, v12, :cond_5

    iget v9, v6, Landroid/graphics/Rect;->top:I

    if-eqz v9, :cond_5

    .line 898
    invoke-direct {p0, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->makeReverseBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v9

    iget v10, v7, Landroid/graphics/Rect;->left:I

    add-int/lit8 v10, v10, -0x2

    int-to-float v10, v10

    iget v11, v7, Landroid/graphics/Rect;->top:I

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    sub-int/2addr v11, v12

    add-int/lit8 v11, v11, 0xf

    int-to-float v11, v11

    invoke-virtual {p1, v9, v10, v11, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 931
    :goto_1
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 932
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 933
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 934
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 936
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 938
    .end local v0    # "cursorLeftNormal":Landroid/graphics/Bitmap;
    .end local v1    # "cursorLeftPressed":Landroid/graphics/Bitmap;
    .end local v2    # "cursorRightNormal":Landroid/graphics/Bitmap;
    .end local v3    # "cursorRightPressed":Landroid/graphics/Bitmap;
    .end local v6    # "rect":Landroid/graphics/Rect;
    .end local v7    # "rectEnd":Landroid/graphics/Rect;
    .end local v8    # "rectStart":Landroid/graphics/Rect;
    :cond_2
    return-void

    .line 887
    .restart local v0    # "cursorLeftNormal":Landroid/graphics/Bitmap;
    .restart local v1    # "cursorLeftPressed":Landroid/graphics/Bitmap;
    .restart local v2    # "cursorRightNormal":Landroid/graphics/Bitmap;
    .restart local v3    # "cursorRightPressed":Landroid/graphics/Bitmap;
    .restart local v7    # "rectEnd":Landroid/graphics/Rect;
    .restart local v8    # "rectStart":Landroid/graphics/Rect;
    :cond_3
    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->makeReverseBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v9

    iget v10, v8, Landroid/graphics/Rect;->left:I

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    sub-int/2addr v10, v11

    add-int/lit8 v10, v10, 0x2

    int-to-float v10, v10

    iget v11, v8, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v11, v11, -0xf

    int-to-float v11, v11

    invoke-virtual {p1, v9, v10, v11, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 891
    :cond_4
    iget v9, v8, Landroid/graphics/Rect;->left:I

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    sub-int/2addr v9, v10

    add-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    iget v10, v8, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    sub-int/2addr v10, v11

    add-int/lit8 v10, v10, 0xf

    int-to-float v10, v10

    invoke-virtual {p1, v0, v9, v10, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 901
    .restart local v6    # "rect":Landroid/graphics/Rect;
    :cond_5
    iget v9, v7, Landroid/graphics/Rect;->left:I

    add-int/lit8 v9, v9, -0x2

    int-to-float v9, v9

    iget v10, v7, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v10, v10, -0xf

    int-to-float v10, v10

    invoke-virtual {p1, v2, v9, v10, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 906
    .end local v6    # "rect":Landroid/graphics/Rect;
    :cond_6
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v9

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v9

    if-nez v9, :cond_8

    .line 908
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v9

    if-nez v9, :cond_7

    .line 909
    invoke-direct {p0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->makeReverseSymmetryBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v9

    iget v10, v8, Landroid/graphics/Rect;->left:I

    int-to-float v10, v10

    iget v11, v8, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v11, v11, -0xf

    int-to-float v11, v11

    invoke-virtual {p1, v9, v10, v11, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 920
    :goto_2
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 921
    .restart local v6    # "rect":Landroid/graphics/Rect;
    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 922
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v9

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v9

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineCount()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    if-ne v9, v10, :cond_9

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineCount()I

    move-result v9

    if-eq v9, v12, :cond_9

    iget v9, v6, Landroid/graphics/Rect;->top:I

    if-eqz v9, :cond_9

    .line 923
    invoke-direct {p0, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->makeReverseBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v9

    iget v10, v7, Landroid/graphics/Rect;->left:I

    add-int/lit8 v10, v10, -0x2

    int-to-float v10, v10

    iget v11, v7, Landroid/graphics/Rect;->top:I

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    sub-int/2addr v11, v12

    add-int/lit8 v11, v11, 0xf

    int-to-float v11, v11

    invoke-virtual {p1, v9, v10, v11, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 913
    .end local v6    # "rect":Landroid/graphics/Rect;
    :cond_7
    invoke-direct {p0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->makeReverseBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v9

    iget v10, v8, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    sub-int/2addr v10, v11

    add-int/lit8 v10, v10, 0x2

    int-to-float v10, v10

    iget v11, v8, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v11, v11, -0xf

    int-to-float v11, v11

    invoke-virtual {p1, v9, v10, v11, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_2

    .line 917
    :cond_8
    iget v9, v8, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    sub-int/2addr v9, v10

    add-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    iget v10, v8, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    sub-int/2addr v10, v11

    add-int/lit8 v10, v10, 0xf

    int-to-float v10, v10

    invoke-virtual {p1, v1, v9, v10, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_2

    .line 926
    .restart local v6    # "rect":Landroid/graphics/Rect;
    :cond_9
    iget v9, v7, Landroid/graphics/Rect;->left:I

    add-int/lit8 v9, v9, -0x2

    int-to-float v9, v9

    iget v10, v7, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v10, v10, -0xf

    int-to-float v10, v10

    invoke-virtual {p1, v3, v9, v10, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_1
.end method

.method private drawSelection(Landroid/graphics/Canvas;)V
    .locals 16
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 941
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v6

    .line 942
    .local v6, "nStart":I
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v3

    .line 944
    .local v3, "nEnd":I
    sub-int v14, v3, v6

    if-lez v14, :cond_2

    .line 946
    invoke-static {}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getInstance()Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    invoke-virtual {v14, v15}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->isNormalWindow(Landroid/app/Activity;)Z

    move-result v14

    if-nez v14, :cond_3

    const/4 v2, 0x1

    .line 947
    .local v2, "isMW":Z
    :goto_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    instance-of v14, v14, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    if-eqz v14, :cond_4

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v14, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-object v13, v14

    .line 948
    .local v13, "tea":Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    :goto_1
    if-eqz v13, :cond_5

    invoke-virtual {v13}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getMWDnD()Lcom/infraware/common/multiwindow/MWDnDOperator;

    move-result-object v8

    .line 949
    .local v8, "oMWDnD":Lcom/infraware/common/multiwindow/MWDnDOperator;
    :goto_2
    if-eqz v2, :cond_0

    if-eqz v8, :cond_0

    .line 950
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getMarkedString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v8, v14}, Lcom/infraware/common/multiwindow/MWDnDOperator;->setMarkedText(Ljava/lang/String;)V

    .line 954
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 956
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v14

    invoke-virtual {v14, v6}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v7

    .line 957
    .local v7, "nStartLine":I
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v14

    invoke-virtual {v14, v3}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v4

    .line 959
    .local v4, "nEndLine":I
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTotalPaddingLeft()I

    move-result v9

    .line 960
    .local v9, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTotalPaddingTop()I

    move-result v10

    .line 962
    .local v10, "paddingTop":I
    new-instance v11, Landroid/graphics/Paint;

    invoke-direct {v11}, Landroid/graphics/Paint;-><init>()V

    .line 963
    .local v11, "paint":Landroid/graphics/Paint;
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_TEXTMARK_REVERSE()Z

    move-result v14

    if-nez v14, :cond_6

    .line 964
    const v14, 0x4dd56108

    invoke-virtual {v11, v14}, Landroid/graphics/Paint;->setColor(I)V

    .line 968
    :goto_3
    new-instance v12, Landroid/graphics/Rect;

    invoke-direct {v12}, Landroid/graphics/Rect;-><init>()V

    .line 969
    .local v12, "rect":Landroid/graphics/Rect;
    if-ne v7, v4, :cond_7

    .line 970
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v14

    invoke-virtual {v14, v6}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v14

    float-to-int v14, v14

    add-int/2addr v14, v9

    iput v14, v12, Landroid/graphics/Rect;->left:I

    .line 971
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v14

    invoke-virtual {v14, v3}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v14

    float-to-int v14, v14

    add-int/2addr v14, v9

    iput v14, v12, Landroid/graphics/Rect;->right:I

    .line 972
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v14

    invoke-virtual {v14, v7}, Landroid/text/Layout;->getLineTop(I)I

    move-result v14

    add-int/2addr v14, v10

    iput v14, v12, Landroid/graphics/Rect;->top:I

    .line 973
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v14

    invoke-virtual {v14, v7}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v14

    add-int/2addr v14, v10

    iput v14, v12, Landroid/graphics/Rect;->bottom:I

    .line 974
    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 975
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelectMultiLine:Z

    .line 978
    if-eqz v2, :cond_1

    invoke-virtual {v8, v12}, Lcom/infraware/common/multiwindow/MWDnDOperator;->addLastTextMarkArea(Landroid/graphics/Rect;)V

    .line 1009
    :cond_1
    :goto_4
    iget v14, v12, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iput v14, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_drawSelectionBottom:I

    .line 1011
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 1013
    .end local v2    # "isMW":Z
    .end local v4    # "nEndLine":I
    .end local v7    # "nStartLine":I
    .end local v8    # "oMWDnD":Lcom/infraware/common/multiwindow/MWDnDOperator;
    .end local v9    # "paddingLeft":I
    .end local v10    # "paddingTop":I
    .end local v11    # "paint":Landroid/graphics/Paint;
    .end local v12    # "rect":Landroid/graphics/Rect;
    .end local v13    # "tea":Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    :cond_2
    return-void

    .line 946
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 947
    .restart local v2    # "isMW":Z
    :cond_4
    const/4 v13, 0x0

    goto/16 :goto_1

    .line 948
    .restart local v13    # "tea":Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    :cond_5
    const/4 v8, 0x0

    goto/16 :goto_2

    .line 966
    .restart local v4    # "nEndLine":I
    .restart local v7    # "nStartLine":I
    .restart local v8    # "oMWDnD":Lcom/infraware/common/multiwindow/MWDnDOperator;
    .restart local v9    # "paddingLeft":I
    .restart local v10    # "paddingTop":I
    .restart local v11    # "paint":Landroid/graphics/Paint;
    :cond_6
    const v14, -0x215d83

    invoke-virtual {v11, v14}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_3

    .line 981
    .restart local v12    # "rect":Landroid/graphics/Rect;
    :cond_7
    const/4 v1, 0x0

    .line 982
    .local v1, "cnt":I
    move v5, v7

    .local v5, "nLine":I
    :goto_5
    if-gt v5, v4, :cond_b

    .line 983
    if-ne v5, v7, :cond_9

    .line 984
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v14

    invoke-virtual {v14, v6}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v14

    float-to-int v14, v14

    add-int/2addr v14, v9

    iput v14, v12, Landroid/graphics/Rect;->left:I

    .line 985
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getWidth()I

    move-result v14

    sub-int/2addr v14, v9

    iput v14, v12, Landroid/graphics/Rect;->right:I

    .line 987
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v14

    invoke-virtual {v14, v7}, Landroid/text/Layout;->getLineTop(I)I

    move-result v14

    add-int/2addr v14, v10

    move-object/from16 v0, p0

    iput v14, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_drawSelectionTop:I

    .line 995
    :goto_6
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v14

    invoke-virtual {v14, v5}, Landroid/text/Layout;->getLineTop(I)I

    move-result v14

    add-int/2addr v14, v10

    iput v14, v12, Landroid/graphics/Rect;->top:I

    .line 996
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v14

    invoke-virtual {v14, v5}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v14

    add-int/2addr v14, v10

    iput v14, v12, Landroid/graphics/Rect;->bottom:I

    .line 997
    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 998
    add-int/lit8 v1, v1, 0x1

    .line 1000
    if-eqz v2, :cond_8

    invoke-virtual {v8, v12}, Lcom/infraware/common/multiwindow/MWDnDOperator;->addLastTextMarkArea(Landroid/graphics/Rect;)V

    .line 982
    :cond_8
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 988
    :cond_9
    if-ne v5, v4, :cond_a

    .line 989
    iput v9, v12, Landroid/graphics/Rect;->left:I

    .line 990
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v14

    invoke-virtual {v14, v3}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v14

    float-to-int v14, v14

    add-int/2addr v14, v9

    iput v14, v12, Landroid/graphics/Rect;->right:I

    goto :goto_6

    .line 992
    :cond_a
    iput v9, v12, Landroid/graphics/Rect;->left:I

    .line 993
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getWidth()I

    move-result v14

    sub-int/2addr v14, v9

    iput v14, v12, Landroid/graphics/Rect;->right:I

    goto :goto_6

    .line 1004
    :cond_b
    const/16 v14, 0xa

    if-le v1, v14, :cond_c

    .line 1005
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelectMultiLine:Z

    goto/16 :goto_4

    .line 1007
    :cond_c
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelectMultiLine:Z

    goto/16 :goto_4
.end method

.method private drawTTSSelection(Landroid/graphics/Canvas;)V
    .locals 20
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 1076
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTTSSelectStart()I

    move-result v14

    .line 1077
    .local v14, "nStart":I
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTTSSelectEnd()I

    move-result v9

    .line 1079
    .local v9, "nEnd":I
    sub-int v2, v9, v14

    if-lez v2, :cond_2

    .line 1080
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 1082
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2, v14}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v15

    .line 1083
    .local v15, "nStartLine":I
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v10

    .line 1085
    .local v10, "nEndLine":I
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTotalPaddingLeft()I

    move-result v17

    .line 1086
    .local v17, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTotalPaddingTop()I

    move-result v18

    .line 1088
    .local v18, "paddingTop":I
    new-instance v19, Landroid/graphics/Rect;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Rect;-><init>()V

    .line 1090
    .local v19, "rect":Landroid/graphics/Rect;
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 1091
    .local v7, "paint":Landroid/graphics/Paint;
    const/high16 v2, -0x220000

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1092
    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1094
    const/4 v13, 0x0

    .local v13, "nSX":I
    const/4 v8, 0x0

    .local v8, "nEX":I
    const/16 v16, 0x0

    .line 1095
    .local v16, "nY":I
    if-ne v15, v10, :cond_4

    .line 1096
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2, v14}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v2

    float-to-int v2, v2

    add-int v13, v2, v17

    .line 1097
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v2

    float-to-int v2, v2

    add-int v8, v2, v17

    .line 1098
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2, v15}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v2

    add-int v16, v2, v18

    .line 1099
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 1101
    move-object/from16 v0, v19

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x42400000    # 48.0f

    invoke-static {v3, v4}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v3

    sub-int/2addr v2, v3

    move-object/from16 v0, v19

    iput v2, v0, Landroid/graphics/Rect;->bottom:I

    .line 1103
    move-object/from16 v0, v19

    iget v2, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineHeight()I

    move-result v3

    sub-int v3, v16, v3

    if-le v2, v3, :cond_3

    .line 1104
    const/4 v2, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineHeight()I

    move-result v3

    sub-int v3, v16, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->scrollTo(II)V

    .line 1109
    :cond_0
    :goto_0
    int-to-float v3, v13

    move/from16 v0, v16

    int-to-float v4, v0

    int-to-float v5, v8

    move/from16 v0, v16

    int-to-float v6, v0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1141
    :cond_1
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 1143
    .end local v7    # "paint":Landroid/graphics/Paint;
    .end local v8    # "nEX":I
    .end local v10    # "nEndLine":I
    .end local v13    # "nSX":I
    .end local v15    # "nStartLine":I
    .end local v16    # "nY":I
    .end local v17    # "paddingLeft":I
    .end local v18    # "paddingTop":I
    .end local v19    # "rect":Landroid/graphics/Rect;
    :cond_2
    return-void

    .line 1105
    .restart local v7    # "paint":Landroid/graphics/Paint;
    .restart local v8    # "nEX":I
    .restart local v10    # "nEndLine":I
    .restart local v13    # "nSX":I
    .restart local v15    # "nStartLine":I
    .restart local v16    # "nY":I
    .restart local v17    # "paddingLeft":I
    .restart local v18    # "paddingTop":I
    .restart local v19    # "rect":Landroid/graphics/Rect;
    :cond_3
    move-object/from16 v0, v19

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v0, v16

    if-le v0, v2, :cond_0

    .line 1106
    const/4 v2, 0x0

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int v3, v16, v3

    invoke-virtual {v7}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v4

    float-to-int v4, v4

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->scrollTo(II)V

    .line 1107
    move-object/from16 v0, v19

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v7}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v3

    float-to-int v3, v3

    mul-int/lit8 v3, v3, 0x2

    sub-int v16, v2, v3

    goto :goto_0

    .line 1111
    :cond_4
    const/4 v12, 0x0

    .line 1112
    .local v12, "nLineCnt":I
    move v11, v15

    .local v11, "nLine":I
    :goto_2
    if-gt v11, v10, :cond_7

    .line 1113
    if-ne v11, v15, :cond_5

    .line 1114
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2, v14}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v2

    float-to-int v2, v2

    add-int v13, v2, v17

    .line 1115
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getWidth()I

    move-result v2

    sub-int v8, v2, v17

    .line 1123
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2, v11}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v2

    add-int v16, v2, v18

    .line 1124
    int-to-float v3, v13

    move/from16 v0, v16

    int-to-float v4, v0

    int-to-float v5, v8

    move/from16 v0, v16

    int-to-float v6, v0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1125
    add-int/lit8 v12, v12, 0x1

    .line 1112
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 1116
    :cond_5
    if-ne v11, v10, :cond_6

    .line 1117
    move/from16 v13, v17

    .line 1118
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v2

    float-to-int v2, v2

    add-int v8, v2, v17

    goto :goto_3

    .line 1120
    :cond_6
    move/from16 v13, v17

    .line 1121
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getWidth()I

    move-result v2

    sub-int v8, v2, v17

    goto :goto_3

    .line 1127
    :cond_7
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 1129
    move-object/from16 v0, v19

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x42400000    # 48.0f

    invoke-static {v3, v4}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v3

    sub-int/2addr v2, v3

    move-object/from16 v0, v19

    iput v2, v0, Landroid/graphics/Rect;->bottom:I

    .line 1132
    move-object/from16 v0, v19

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v0, v16

    if-le v0, v2, :cond_1

    .line 1133
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineHeight()I

    move-result v3

    mul-int/2addr v3, v12

    mul-int/lit8 v3, v3, 0x2

    if-ge v2, v3, :cond_8

    .line 1134
    const/4 v2, 0x0

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int v3, v16, v3

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineHeight()I

    move-result v4

    sub-int/2addr v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->scrollTo(II)V

    goto/16 :goto_1

    .line 1137
    :cond_8
    const/4 v2, 0x0

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int v3, v16, v3

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineHeight()I

    move-result v4

    mul-int/2addr v4, v12

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->scrollTo(II)V

    goto/16 :goto_1
.end method

.method private getLineBottomForOffset(IZ)I
    .locals 5
    .param p1, "nOffset"    # I
    .param p2, "bPadding"    # Z

    .prologue
    .line 3560
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v0

    .line 3561
    .local v0, "layout":Landroid/text/Layout;
    if-eqz v0, :cond_1

    .line 3562
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v1

    .line 3563
    .local v1, "nLine":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v2

    .line 3564
    .local v2, "nLineBottom":I
    if-eqz p2, :cond_0

    .line 3565
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTotalPaddingTop()I

    move-result v3

    .line 3566
    .local v3, "paddingTop":I
    add-int/2addr v2, v3

    .line 3571
    .end local v1    # "nLine":I
    .end local v2    # "nLineBottom":I
    .end local v3    # "paddingTop":I
    :cond_0
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getLineTopForOffset(IZ)I
    .locals 5
    .param p1, "nOffset"    # I
    .param p2, "bPadding"    # Z

    .prologue
    .line 3545
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v0

    .line 3546
    .local v0, "layout":Landroid/text/Layout;
    if-eqz v0, :cond_1

    .line 3547
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v1

    .line 3548
    .local v1, "nLine":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/text/Layout;->getLineTop(I)I

    move-result v2

    .line 3549
    .local v2, "nLineTop":I
    if-eqz p2, :cond_0

    .line 3550
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTotalPaddingTop()I

    move-result v3

    .line 3551
    .local v3, "paddingTop":I
    add-int/2addr v2, v3

    .line 3556
    .end local v1    # "nLine":I
    .end local v2    # "nLineTop":I
    .end local v3    # "paddingTop":I
    :cond_0
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getPozOfPopupwindow([III)Z
    .locals 21
    .param p1, "pos"    # [I
    .param p2, "nWidth"    # I
    .param p3, "nHeight"    # I

    .prologue
    .line 1734
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v5, v0, [I

    .line 1735
    .local v5, "location":[I
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLocationInWindow([I)V

    .line 1736
    invoke-direct/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionRect()Landroid/graphics/Rect;

    move-result-object v10

    .line 1737
    .local v10, "rectSelect":Landroid/graphics/Rect;
    new-instance v11, Landroid/graphics/Rect;

    invoke-direct {v11}, Landroid/graphics/Rect;-><init>()V

    .line 1738
    .local v11, "rectVisible":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 1739
    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v18, v0

    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v19

    div-int/lit8 v19, v19, 0x2

    add-int v6, v18, v19

    .line 1740
    .local v6, "nPosX":I
    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v18, v0

    iget v0, v11, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    sub-int v18, v18, v19

    move/from16 v0, v18

    iput v0, v10, Landroid/graphics/Rect;->bottom:I

    .line 1741
    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    iget v0, v11, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    sub-int v18, v18, v19

    move/from16 v0, v18

    iput v0, v10, Landroid/graphics/Rect;->top:I

    .line 1742
    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v18, v0

    if-ltz v18, :cond_0

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    invoke-virtual {v11}, Landroid/graphics/Rect;->height()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_1

    .line 1743
    :cond_0
    const/16 v18, 0x0

    .line 1893
    :goto_0
    return v18

    .line 1745
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v18

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v16

    .line 1746
    .local v16, "startline":I
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v18

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v4

    .line 1748
    .local v4, "endline":I
    move/from16 v0, v16

    if-eq v0, v4, :cond_9

    .line 1749
    const/16 v18, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getWidth()I

    move-result v19

    sub-int v19, v19, p2

    div-int/lit8 v19, v19, 0x2

    aput v19, p1, v18

    .line 1753
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getContext()Landroid/content/Context;

    move-result-object v18

    const/high16 v19, 0x41d40000    # 26.5f

    invoke-static/range {v18 .. v19}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v15

    .line 1754
    .local v15, "selectionsizey":I
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getContext()Landroid/content/Context;

    move-result-object v18

    const/high16 v19, 0x41500000    # 13.0f

    invoke-static/range {v18 .. v19}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v14

    .line 1755
    .local v14, "selectionsizex":I
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getContext()Landroid/content/Context;

    move-result-object v18

    const/high16 v19, 0x40c00000    # 6.0f

    invoke-static/range {v18 .. v19}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v7

    .line 1756
    .local v7, "padding":I
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getContext()Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTextSize()F

    move-result v19

    invoke-static/range {v18 .. v19}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v17

    .line 1757
    .local v17, "textsize":I
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v18

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionStart()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v18

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTotalPaddingLeft()I

    move-result v19

    add-int v9, v18, v19

    .line 1758
    .local v9, "positionXofstartselection":I
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v18

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionEnd()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v18

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTotalPaddingLeft()I

    move-result v19

    add-int v8, v18, v19

    .line 1759
    .local v8, "positionXofEndselection":I
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getWidth()I

    move-result v18

    sub-int v18, v18, p2

    div-int/lit8 v12, v18, 0x2

    .line 1760
    .local v12, "screencenterx":I
    const/16 v18, 0x1

    aget v18, v5, v18

    invoke-virtual {v11}, Landroid/graphics/Rect;->height()I

    move-result v19

    div-int/lit8 v19, v19, 0x2

    add-int v18, v18, v19

    div-int/lit8 v19, p3, 0x2

    sub-int v13, v18, v19

    .line 1762
    .local v13, "screencentery":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nToastPopupState:I

    move/from16 v18, v0

    if-nez v18, :cond_1a

    .line 1764
    move/from16 v0, v16

    if-eq v0, v4, :cond_14

    .line 1766
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getHeight()I

    move-result v18

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    sub-int v18, v18, v19

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_a

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    add-int v19, v15, v7

    add-int v19, v19, p3

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_a

    .line 1767
    const/16 v18, 0x1

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    const/16 v20, 0x1

    aget v20, v5, v20

    add-int v19, v19, v20

    sub-int v19, v19, p3

    sub-int v19, v19, v15

    sub-int v19, v19, v7

    aput v19, p1, v18

    .line 1817
    :goto_2
    const/16 v18, 0x1

    aget v18, p1, v18

    const/16 v19, 0x1

    aget v19, v5, v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_2

    .line 1819
    const/16 v18, 0x1

    const/16 v19, 0x1

    aget v19, v5, v19

    aput v19, p1, v18

    .line 1820
    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    move/from16 v0, p3

    move/from16 v1, v18

    if-le v0, v1, :cond_2

    .line 1822
    const/16 v18, 0x0

    aput v12, p1, v18

    .line 1823
    const/16 v18, 0x1

    aput v13, p1, v18

    .line 1826
    :cond_2
    const/16 v18, 0x1

    aget v18, p1, v18

    add-int v18, v18, p3

    const/16 v19, 0x1

    aget v19, v5, v19

    invoke-virtual {v11}, Landroid/graphics/Rect;->height()I

    move-result v20

    add-int v19, v19, v20

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_3

    .line 1828
    const/16 v18, 0x1

    const/16 v19, 0x1

    aget v19, v5, v19

    invoke-virtual {v11}, Landroid/graphics/Rect;->height()I

    move-result v20

    add-int v19, v19, v20

    sub-int v19, v19, p3

    aput v19, p1, v18

    .line 1829
    const/16 v18, 0x1

    aget v18, p1, v18

    const/16 v19, 0x1

    aget v19, v5, v19

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v20, v0

    add-int v19, v19, v20

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_3

    .line 1831
    const/16 v18, 0x0

    aput v12, p1, v18

    .line 1832
    const/16 v18, 0x1

    aput v13, p1, v18

    .line 1874
    :cond_3
    :goto_3
    const/16 v18, 0x1

    aget v18, p1, v18

    const/16 v19, 0x1

    aget v19, v5, v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_4

    .line 1876
    const/16 v18, 0x1

    const/16 v19, 0x1

    aget v19, v5, v19

    aput v19, p1, v18

    .line 1878
    :cond_4
    const/16 v18, 0x1

    aget v18, p1, v18

    add-int v18, v18, p3

    const/16 v19, 0x1

    aget v19, v5, v19

    invoke-virtual {v11}, Landroid/graphics/Rect;->height()I

    move-result v20

    add-int v19, v19, v20

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_5

    .line 1880
    const/16 v18, 0x1

    const/16 v19, 0x1

    aget v19, v5, v19

    invoke-virtual {v11}, Landroid/graphics/Rect;->height()I

    move-result v20

    add-int v19, v19, v20

    sub-int v19, v19, p3

    aput v19, p1, v18

    .line 1883
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getContext()Landroid/content/Context;

    move-result-object v18

    check-cast v18, Landroid/app/Activity;

    invoke-virtual/range {v18 .. v18}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getWidth()I

    move-result v3

    .line 1884
    .local v3, "appWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getContext()Landroid/content/Context;

    move-result-object v18

    check-cast v18, Landroid/app/Activity;

    invoke-virtual/range {v18 .. v18}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 1885
    .local v2, "appHeight":I
    const/16 v18, 0x0

    aget v18, p1, v18

    add-int v18, v18, p2

    move/from16 v0, v18

    if-le v0, v3, :cond_6

    .line 1886
    const/16 v18, 0x0

    sub-int v19, v3, p2

    aput v19, p1, v18

    .line 1887
    :cond_6
    const/16 v18, 0x0

    aget v18, p1, v18

    if-gez v18, :cond_7

    .line 1888
    const/16 v18, 0x0

    const/16 v19, 0x0

    aput v19, p1, v18

    .line 1890
    :cond_7
    const/16 v18, 0x1

    aget v18, p1, v18

    add-int v18, v18, p3

    move/from16 v0, v18

    if-le v0, v2, :cond_8

    .line 1891
    const/16 v18, 0x1

    sub-int v19, v2, p3

    aput v19, p1, v18

    .line 1893
    :cond_8
    const/16 v18, 0x1

    goto/16 :goto_0

    .line 1751
    .end local v2    # "appHeight":I
    .end local v3    # "appWidth":I
    .end local v7    # "padding":I
    .end local v8    # "positionXofEndselection":I
    .end local v9    # "positionXofstartselection":I
    .end local v12    # "screencenterx":I
    .end local v13    # "screencentery":I
    .end local v14    # "selectionsizex":I
    .end local v15    # "selectionsizey":I
    .end local v17    # "textsize":I
    :cond_9
    const/16 v18, 0x0

    div-int/lit8 v19, p2, 0x2

    sub-int v19, v6, v19

    aput v19, p1, v18

    goto/16 :goto_1

    .line 1768
    .restart local v7    # "padding":I
    .restart local v8    # "positionXofEndselection":I
    .restart local v9    # "positionXofstartselection":I
    .restart local v12    # "screencenterx":I
    .restart local v13    # "screencentery":I
    .restart local v14    # "selectionsizex":I
    .restart local v15    # "selectionsizey":I
    .restart local v17    # "textsize":I
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getHeight()I

    move-result v18

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    sub-int v18, v18, v19

    add-int v19, v15, v7

    add-int v19, v19, p3

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_b

    .line 1769
    const/16 v18, 0x1

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    const/16 v20, 0x1

    aget v20, v5, v20

    add-int v19, v19, v20

    add-int v19, v19, v15

    add-int v19, v19, v7

    aput v19, p1, v18

    goto/16 :goto_2

    .line 1771
    :cond_b
    add-int v18, p2, v14

    add-int v18, v18, v7

    move/from16 v0, v18

    if-le v9, v0, :cond_c

    .line 1772
    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    add-int v18, v18, v17

    move/from16 v0, v18

    iput v0, v10, Landroid/graphics/Rect;->top:I

    .line 1773
    :cond_c
    iget v0, v11, Landroid/graphics/Rect;->right:I

    move/from16 v18, v0

    sub-int v18, v18, v8

    add-int v19, p2, v14

    add-int v19, v19, v7

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_d

    .line 1774
    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v18, v0

    sub-int v18, v18, v17

    move/from16 v0, v18

    iput v0, v10, Landroid/graphics/Rect;->bottom:I

    .line 1775
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getHeight()I

    move-result v18

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    sub-int v18, v18, v19

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_10

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    move/from16 v0, v18

    move/from16 v1, p3

    if-le v0, v1, :cond_10

    .line 1776
    add-int v18, p2, v14

    add-int v18, v18, v7

    move/from16 v0, v18

    if-le v9, v0, :cond_e

    .line 1778
    const/16 v18, 0x0

    sub-int v19, v9, p2

    sub-int v19, v19, v14

    sub-int v19, v19, v7

    aput v19, p1, v18

    .line 1779
    const/16 v18, 0x1

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    const/16 v20, 0x1

    aget v20, v5, v20

    add-int v19, v19, v20

    sub-int v19, v19, p3

    sub-int v19, v19, v15

    sub-int v19, v19, v7

    sub-int v19, v19, v17

    aput v19, p1, v18

    goto/16 :goto_2

    .line 1783
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getWidth()I

    move-result v18

    sub-int v18, v18, v9

    add-int v19, p2, v7

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_f

    .line 1785
    const/16 v18, 0x0

    add-int v19, v9, v7

    aput v19, p1, v18

    .line 1786
    const/16 v18, 0x1

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    const/16 v20, 0x1

    aget v20, v5, v20

    add-int v19, v19, v20

    sub-int v19, v19, p3

    sub-int v19, v19, v15

    sub-int v19, v19, v7

    aput v19, p1, v18

    goto/16 :goto_2

    .line 1790
    :cond_f
    const/16 v18, 0x0

    aput v12, p1, v18

    .line 1791
    const/16 v18, 0x1

    aput v13, p1, v18

    goto/16 :goto_2

    .line 1795
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getHeight()I

    move-result v18

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    sub-int v18, v18, v19

    move/from16 v0, v18

    move/from16 v1, p3

    if-le v0, v1, :cond_13

    .line 1796
    iget v0, v11, Landroid/graphics/Rect;->right:I

    move/from16 v18, v0

    sub-int v18, v18, v8

    add-int v19, p2, v14

    add-int v19, v19, v7

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_11

    .line 1798
    const/16 v18, 0x0

    add-int v19, v8, v14

    add-int v19, v19, v7

    aput v19, p1, v18

    .line 1799
    const/16 v18, 0x1

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    const/16 v20, 0x1

    aget v20, v5, v20

    add-int v19, v19, v20

    add-int v19, v19, v15

    add-int v19, v19, v7

    add-int v19, v19, v17

    aput v19, p1, v18

    goto/16 :goto_2

    .line 1803
    :cond_11
    add-int v18, p2, v7

    move/from16 v0, v18

    if-le v8, v0, :cond_12

    .line 1805
    const/16 v18, 0x0

    sub-int v19, v8, p2

    sub-int v19, v19, v7

    aput v19, p1, v18

    .line 1806
    const/16 v18, 0x1

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    const/16 v20, 0x1

    aget v20, v5, v20

    add-int v19, v19, v20

    add-int v19, v19, v15

    add-int v19, v19, v7

    aput v19, p1, v18

    goto/16 :goto_2

    .line 1810
    :cond_12
    const/16 v18, 0x0

    aput v12, p1, v18

    .line 1811
    const/16 v18, 0x1

    aput v13, p1, v18

    goto/16 :goto_2

    .line 1815
    :cond_13
    const/16 v18, 0x1

    aput v13, p1, v18

    goto/16 :goto_2

    .line 1838
    :cond_14
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getHeight()I

    move-result v18

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    sub-int v18, v18, v19

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_15

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    add-int v19, v15, v7

    add-int v19, v19, p3

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_15

    .line 1839
    const/16 v18, 0x1

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    const/16 v20, 0x1

    aget v20, v5, v20

    add-int v19, v19, v20

    sub-int v19, v19, p3

    sub-int v19, v19, v15

    sub-int v19, v19, v7

    aput v19, p1, v18

    goto/16 :goto_3

    .line 1840
    :cond_15
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getHeight()I

    move-result v18

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    sub-int v18, v18, v19

    add-int v19, v15, v7

    add-int v19, v19, p3

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_16

    .line 1841
    const/16 v18, 0x1

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    const/16 v20, 0x1

    aget v20, v5, v20

    add-int v19, v19, v20

    add-int v19, v19, v15

    add-int v19, v19, v7

    aput v19, p1, v18

    goto/16 :goto_3

    .line 1844
    :cond_16
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getWidth()I

    move-result v18

    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v19, v0

    sub-int v18, v18, v19

    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_17

    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v18, v0

    add-int v19, p2, v15

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_17

    .line 1845
    const/16 v18, 0x0

    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v19, v0

    sub-int v19, v19, p2

    sub-int v19, v19, v14

    aput v19, p1, v18

    .line 1846
    const/16 v18, 0x1

    const/16 v19, 0x1

    aget v19, v5, v19

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v20, v0

    add-int v19, v19, v20

    add-int v19, v19, v15

    sub-int v19, v19, p3

    aput v19, p1, v18

    goto/16 :goto_3

    .line 1847
    :cond_17
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getWidth()I

    move-result v18

    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v19, v0

    sub-int v18, v18, v19

    add-int v19, p2, v15

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_18

    .line 1848
    const/16 v18, 0x0

    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v19, v0

    add-int v19, v19, v14

    aput v19, p1, v18

    .line 1849
    const/16 v18, 0x1

    const/16 v19, 0x1

    aget v19, v5, v19

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    add-int v19, v19, v20

    sub-int v19, v19, v15

    aput v19, p1, v18

    goto/16 :goto_3

    .line 1851
    :cond_18
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getHeight()I

    move-result v18

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    sub-int v18, v18, v19

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_19

    .line 1852
    const/16 v18, 0x1

    const/16 v19, 0x1

    aget v19, v5, v19

    aput v19, p1, v18

    goto/16 :goto_3

    .line 1854
    :cond_19
    const/16 v18, 0x1

    const/16 v19, 0x1

    aget v19, v5, v19

    invoke-virtual {v11}, Landroid/graphics/Rect;->height()I

    move-result v20

    add-int v19, v19, v20

    sub-int v19, v19, p3

    aput v19, p1, v18

    goto/16 :goto_3

    .line 1859
    :cond_1a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nToastPopupState:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_3

    .line 1861
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getHeight()I

    move-result v18

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    sub-int v18, v18, v19

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_1b

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    add-int v19, v15, v7

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_1b

    .line 1862
    const/16 v18, 0x1

    const/16 v19, 0x1

    aget v19, v5, v19

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    add-int v19, v19, v20

    sub-int v19, v19, p3

    sub-int v19, v19, v7

    aput v19, p1, v18

    goto/16 :goto_3

    .line 1863
    :cond_1b
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getHeight()I

    move-result v18

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    sub-int v18, v18, v19

    add-int v19, v7, p3

    add-int v19, v19, v15

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_1c

    .line 1864
    const/16 v18, 0x1

    const/16 v19, 0x1

    aget v19, v5, v19

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v20, v0

    add-int v19, v19, v20

    add-int v19, v19, v7

    add-int v19, v19, v15

    aput v19, p1, v18

    goto/16 :goto_3

    .line 1866
    :cond_1c
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getWidth()I

    move-result v18

    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v19, v0

    sub-int v18, v18, v19

    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_1e

    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v18, v0

    add-int v19, p2, v15

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_1e

    .line 1867
    const/16 v18, 0x0

    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v19, v0

    sub-int v19, v19, p2

    sub-int v19, v19, v14

    aput v19, p1, v18

    .line 1871
    :cond_1d
    :goto_4
    const/16 v18, 0x1

    const/16 v19, 0x1

    aget v19, v5, v19

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    add-int v19, v19, v20

    sub-int v19, v19, v15

    aput v19, p1, v18

    goto/16 :goto_3

    .line 1868
    :cond_1e
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getWidth()I

    move-result v18

    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v19, v0

    sub-int v18, v18, v19

    add-int v19, p2, v15

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_1d

    .line 1869
    const/16 v18, 0x0

    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v19, v0

    add-int v19, v19, v14

    aput v19, p1, v18

    goto :goto_4
.end method

.method private getSelectBoundRect(Z)Landroid/graphics/Rect;
    .locals 7
    .param p1, "bStart"    # Z

    .prologue
    const/4 v6, 0x1

    .line 788
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 790
    .local v3, "rectRet":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v4

    if-nez v4, :cond_1

    .line 826
    :cond_0
    :goto_0
    return-object v3

    .line 793
    :cond_1
    const/4 v0, 0x0

    .line 794
    .local v0, "nSelect":I
    if-eqz p1, :cond_2

    .line 795
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v0

    .line 799
    :goto_1
    invoke-direct {p0, v0, v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineTopForOffset(IZ)I

    move-result v4

    iput v4, v3, Landroid/graphics/Rect;->top:I

    .line 800
    invoke-direct {p0, v0, v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineBottomForOffset(IZ)I

    move-result v4

    iput v4, v3, Landroid/graphics/Rect;->bottom:I

    .line 801
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, v3, Landroid/graphics/Rect;->left:I

    .line 802
    iget v4, v3, Landroid/graphics/Rect;->left:I

    iput v4, v3, Landroid/graphics/Rect;->right:I

    .line 804
    if-eqz p1, :cond_3

    .line 805
    iget v4, v3, Landroid/graphics/Rect;->top:I

    add-int/lit8 v4, v4, -0xf

    iput v4, v3, Landroid/graphics/Rect;->top:I

    .line 809
    :goto_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTotalPaddingLeft()I

    move-result v1

    .line 810
    .local v1, "paddingLeft":I
    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, Landroid/graphics/Rect;->offset(II)V

    .line 812
    if-eqz p1, :cond_4

    .line 813
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v4

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v4

    if-nez v4, :cond_0

    .line 814
    iget v4, v3, Landroid/graphics/Rect;->top:I

    add-int/lit8 v4, v4, 0xf

    iput v4, v3, Landroid/graphics/Rect;->top:I

    .line 815
    iget v4, v3, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v4, v4, 0xf

    iput v4, v3, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    .line 797
    .end local v1    # "paddingLeft":I
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v0

    goto :goto_1

    .line 807
    :cond_3
    iget v4, v3, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v4, v4, 0xf

    iput v4, v3, Landroid/graphics/Rect;->bottom:I

    goto :goto_2

    .line 818
    .restart local v1    # "paddingLeft":I
    :cond_4
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 819
    .local v2, "rect":Landroid/graphics/Rect;
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 820
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v4

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ne v4, v5, :cond_0

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineCount()I

    move-result v4

    if-eq v4, v6, :cond_0

    iget v4, v2, Landroid/graphics/Rect;->top:I

    if-eqz v4, :cond_0

    .line 821
    iget v4, v3, Landroid/graphics/Rect;->top:I

    add-int/lit8 v4, v4, -0xf

    iput v4, v3, Landroid/graphics/Rect;->top:I

    .line 822
    iget v4, v3, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v4, v4, -0xf

    iput v4, v3, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_0
.end method

.method private getSelectionRect()Landroid/graphics/Rect;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2530
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v2

    .line 2531
    .local v2, "nSelectStart":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v1

    .line 2533
    .local v1, "nSelectEnd":I
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 2534
    .local v4, "rectSelect":Landroid/graphics/Rect;
    iput v6, v4, Landroid/graphics/Rect;->top:I

    .line 2535
    iput v6, v4, Landroid/graphics/Rect;->bottom:I

    .line 2536
    iput v6, v4, Landroid/graphics/Rect;->left:I

    .line 2537
    iput v6, v4, Landroid/graphics/Rect;->right:I

    .line 2539
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v0

    .line 2540
    .local v0, "layout":Landroid/text/Layout;
    if-eqz v0, :cond_0

    .line 2541
    invoke-direct {p0, v2, v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineTopForOffset(IZ)I

    move-result v5

    iput v5, v4, Landroid/graphics/Rect;->top:I

    .line 2542
    invoke-virtual {v0, v2}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v5

    float-to-int v5, v5

    iput v5, v4, Landroid/graphics/Rect;->left:I

    .line 2543
    invoke-direct {p0, v1, v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineBottomForOffset(IZ)I

    move-result v5

    iput v5, v4, Landroid/graphics/Rect;->bottom:I

    .line 2544
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v5

    float-to-int v5, v5

    iput v5, v4, Landroid/graphics/Rect;->right:I

    .line 2546
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTotalPaddingLeft()I

    move-result v3

    .line 2547
    .local v3, "paddingLeft":I
    invoke-virtual {v4, v3, v6}, Landroid/graphics/Rect;->offset(II)V

    .line 2548
    invoke-virtual {v4}, Landroid/graphics/Rect;->sort()V

    .line 2551
    .end local v3    # "paddingLeft":I
    :cond_0
    return-object v4
.end method

.method private isSelectionOverBound()Z
    .locals 10

    .prologue
    .line 3585
    const/4 v0, 0x0

    .line 3587
    .local v0, "bRet":Z
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v5

    .line 3588
    .local v5, "nSelStart":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v3

    .line 3589
    .local v3, "nSelEnd":I
    const/4 v6, 0x0

    .line 3590
    .local v6, "nStartLine":I
    const/4 v2, 0x0

    .line 3592
    .local v2, "nEndLine":I
    :try_start_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v8

    invoke-virtual {v8, v5}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v6

    .line 3593
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v8

    invoke-virtual {v8, v3}, Landroid/text/Layout;->getLineForOffset(I)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 3598
    sub-int v8, v2, v6

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineHeight()I

    move-result v9

    mul-int v4, v8, v9

    .line 3600
    .local v4, "nSelHeight":I
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    .line 3601
    .local v7, "rect":Landroid/graphics/Rect;
    invoke-virtual {p0, v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 3603
    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v8

    if-gt v8, v4, :cond_0

    .line 3604
    const/4 v0, 0x1

    :cond_0
    move v8, v0

    .line 3606
    .end local v4    # "nSelHeight":I
    .end local v7    # "rect":Landroid/graphics/Rect;
    :goto_0
    return v8

    .line 3595
    :catch_0
    move-exception v1

    .line 3596
    .local v1, "e":Ljava/lang/NullPointerException;
    const/4 v8, 0x0

    goto :goto_0
.end method

.method private isTextmark()Z
    .locals 4

    .prologue
    .line 3970
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionStart()I

    move-result v2

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionEnd()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 3971
    .local v0, "text":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 3972
    const/4 v1, 0x1

    .line 3973
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private makeReverseBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "src"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v1, 0x0

    .line 1147
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 1148
    .local v5, "matrix":Landroid/graphics/Matrix;
    const/high16 v0, 0x3f800000    # 1.0f

    const/high16 v2, -0x40800000    # -1.0f

    invoke-virtual {v5, v0, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1149
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move-object v0, p1

    move v2, v1

    move v6, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 1151
    .local v7, "reverse":Landroid/graphics/Bitmap;
    return-object v7
.end method

.method private makeReverseSymmetryBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "src"    # Landroid/graphics/Bitmap;

    .prologue
    const/high16 v0, -0x40800000    # -1.0f

    const/4 v1, 0x0

    .line 1157
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 1158
    .local v5, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v5, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1159
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move-object v0, p1

    move v2, v1

    move v6, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 1161
    .local v7, "reverseSymmetry":Landroid/graphics/Bitmap;
    return-object v7
.end method

.method private morePopupHandlerInitialize()V
    .locals 1

    .prologue
    .line 1932
    new-instance v0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    .line 1933
    new-instance v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$6;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$6;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_moretoastPopupRunnable:Ljava/lang/Runnable;

    .line 1949
    return-void
.end method

.method private multiDivide(III)I
    .locals 1
    .param p1, "twip"    # I
    .param p2, "resNscale"    # I
    .param p3, "twipdensity"    # I

    .prologue
    .line 1657
    mul-int v0, p1, p2

    div-int/2addr v0, p3

    return v0
.end method

.method private onClickSearchItem(I)V
    .locals 5
    .param p1, "itemId"    # I

    .prologue
    const/4 v4, 0x2

    .line 2770
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    .line 2772
    .local v0, "activity":Landroid/app/Activity;
    packed-switch p1, :pswitch_data_0

    .line 2790
    :cond_0
    :goto_0
    return-void

    .line 2775
    :pswitch_0
    if-eqz v0, :cond_0

    .line 2776
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v2

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->onSearch(Ljava/lang/String;I)V

    goto :goto_0

    .line 2779
    :pswitch_1
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine;->isChina()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2780
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v2

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->onSearch(Ljava/lang/String;I)V

    goto :goto_0

    .line 2782
    :cond_1
    if-eqz v0, :cond_0

    .line 2783
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v2

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->onSearch(Ljava/lang/String;I)V

    goto :goto_0

    .line 2787
    :pswitch_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v2

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->onSearch(Ljava/lang/String;I)V

    goto :goto_0

    .line 2772
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private onCopy()V
    .locals 5

    .prologue
    .line 2578
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 2580
    .local v1, "strSelected":Ljava/lang/String;
    iget v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_clipboardService:I

    packed-switch v2, :pswitch_data_0

    .line 2591
    :goto_0
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "FT03"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2594
    .local v0, "isSamsung":Z
    if-nez v0, :cond_0

    .line 2595
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700c6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 2597
    :cond_0
    return-void

    .line 2582
    .end local v0    # "isSamsung":Z
    :pswitch_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ClipboardMgr:Landroid/text/ClipboardManager;

    invoke-virtual {v2, v1}, Landroid/text/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2585
    :pswitch_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_secClipboard:Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

    invoke-virtual {v2, v1}, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->setText(Ljava/lang/String;)V

    goto :goto_0

    .line 2580
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private onCut()V
    .locals 5

    .prologue
    .line 2555
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 2557
    .local v1, "strSelected":Ljava/lang/String;
    iget v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_clipboardService:I

    packed-switch v2, :pswitch_data_0

    .line 2566
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 2567
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_UndoRedoMgr:Lcom/infraware/polarisoffice5/text/manager/CommandManager;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->setOneWord()V

    .line 2569
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "FT03"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2572
    .local v0, "isSamsung":Z
    if-nez v0, :cond_0

    .line 2573
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700c7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 2575
    :cond_0
    return-void

    .line 2559
    .end local v0    # "isSamsung":Z
    :pswitch_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ClipboardMgr:Landroid/text/ClipboardManager;

    invoke-virtual {v2, v1}, Landroid/text/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2557
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private onPaste()I
    .locals 7

    .prologue
    .line 2600
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ClipboardMgr:Landroid/text/ClipboardManager;

    invoke-virtual {v5}, Landroid/text/ClipboardManager;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    if-nez v5, :cond_0

    .line 2601
    const/4 v0, -0x1

    .line 2624
    :goto_0
    return v0

    .line 2603
    :cond_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ClipboardMgr:Landroid/text/ClipboardManager;

    invoke-virtual {v5}, Landroid/text/ClipboardManager;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2604
    .local v3, "strClip":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v2

    .line 2605
    .local v2, "nSelStart":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v1

    .line 2606
    .local v1, "nSelEnd":I
    const-string/jumbo v4, ""

    .line 2608
    .local v4, "strRemove":Ljava/lang/String;
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setUndoOneAction(Z)V

    .line 2609
    sub-int v5, v1, v2

    if-lez v5, :cond_1

    .line 2610
    iget v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    invoke-virtual {p0, v5, v2, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSubBufferText(III)Ljava/lang/String;

    move-result-object v4

    .line 2611
    iget v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    invoke-virtual {p0, v5, v2, v1, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->deleteBuffer(IIILjava/lang/String;)V

    .line 2613
    :cond_1
    iget v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {p0, v5, v2, v6, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->insertBuffer(IIILjava/lang/String;)V

    .line 2614
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setUndoOneAction(Z)V

    .line 2616
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_UndoRedoMgr:Lcom/infraware/polarisoffice5/text/manager/CommandManager;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->setOneWord()V

    .line 2618
    const/4 v0, -0x1

    .line 2619
    .local v0, "nPos":I
    iget v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    if-nez v5, :cond_2

    .line 2620
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    add-int v0, v2, v5

    goto :goto_0

    .line 2622
    :cond_2
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    iget v6, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockOffsetInFile(I)I

    move-result v5

    add-int/2addr v5, v2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int v0, v5, v6

    goto :goto_0
.end method

.method private onSamsungPaste(Ljava/lang/String;)I
    .locals 7
    .param p1, "pasteClip"    # Ljava/lang/String;

    .prologue
    .line 2628
    move-object v3, p1

    .line 2629
    .local v3, "strClip":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v2

    .line 2630
    .local v2, "nSelStart":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v1

    .line 2631
    .local v1, "nSelEnd":I
    const-string/jumbo v4, ""

    .line 2632
    .local v4, "strRemove":Ljava/lang/String;
    sub-int v5, v1, v2

    if-lez v5, :cond_0

    .line 2633
    iget v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    invoke-virtual {p0, v5, v2, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSubBufferText(III)Ljava/lang/String;

    move-result-object v4

    .line 2634
    iget v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    invoke-virtual {p0, v5, v2, v1, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->deleteBuffer(IIILjava/lang/String;)V

    .line 2636
    :cond_0
    iget v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {p0, v5, v2, v6, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->insertBuffer(IIILjava/lang/String;)V

    .line 2637
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_UndoRedoMgr:Lcom/infraware/polarisoffice5/text/manager/CommandManager;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->setOneWord()V

    .line 2639
    const/4 v0, -0x1

    .line 2640
    .local v0, "nPos":I
    iget v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    if-nez v5, :cond_1

    .line 2641
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    add-int v0, v2, v5

    .line 2645
    :goto_0
    return v0

    .line 2643
    :cond_1
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    iget v6, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockOffsetInFile(I)I

    move-result v5

    add-int/2addr v5, v2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int v0, v5, v6

    goto :goto_0
.end method

.method private onSearch(Ljava/lang/String;I)V
    .locals 4
    .param p1, "searchStr"    # Ljava/lang/String;
    .param p2, "searchType"    # I

    .prologue
    .line 2848
    packed-switch p2, :pswitch_data_0

    .line 2870
    :cond_0
    :goto_0
    return-void

    .line 2850
    :pswitch_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    instance-of v3, v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    if-eqz v3, :cond_0

    .line 2851
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v3, p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onGooleSearch(Ljava/lang/String;)V

    goto :goto_0

    .line 2855
    :pswitch_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 2856
    .local v1, "locale":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 2857
    const-string/jumbo v1, "en"

    .line 2860
    :cond_1
    invoke-direct {p0, p1, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->buildLookupUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2861
    .local v2, "uri":Landroid/net/Uri;
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.VIEW"

    invoke-direct {v0, v3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2862
    .local v0, "it":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 2865
    .end local v0    # "it":Landroid/content/Intent;
    .end local v1    # "locale":Ljava/lang/String;
    .end local v2    # "uri":Landroid/net/Uri;
    :pswitch_2
    if-eqz p1, :cond_0

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v3, :cond_0

    .line 2866
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v3, p1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setSearchKeyword(Ljava/lang/String;)Z

    goto :goto_0

    .line 2848
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private onShare()V
    .locals 1

    .prologue
    .line 2842
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    instance-of v0, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    if-eqz v0, :cond_0

    .line 2843
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onShare()V

    .line 2845
    :cond_0
    return-void
.end method

.method private pasteViewProcess(I)V
    .locals 5
    .param p1, "nPos"    # I

    .prologue
    const/4 v4, 0x1

    .line 2649
    const/4 v1, 0x0

    .line 2650
    .local v1, "nBlock":I
    const/4 v2, 0x0

    .line 2652
    .local v2, "nOffset":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isNewFile()Z

    move-result v3

    if-ne v3, v4, :cond_0

    .line 2653
    div-int/lit16 v1, p1, 0xbb8

    .line 2654
    rem-int/lit16 v2, p1, 0xbb8

    .line 2661
    :goto_0
    const/4 v3, 0x0

    invoke-virtual {p0, v1, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->loadBlock(IZ)V

    .line 2663
    :try_start_0
    iget v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    add-int/2addr v3, v2

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2668
    :goto_1
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setChanged(Z)V

    .line 2669
    return-void

    .line 2657
    :cond_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v3, p1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockIndexFromOffset(I)I

    move-result v1

    .line 2658
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v3, p1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockOffsetFromOffset(I)I

    move-result v2

    goto :goto_0

    .line 2664
    :catch_0
    move-exception v0

    .line 2665
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    goto :goto_1
.end method

.method private resizePixelByWidth(II)I
    .locals 1
    .param p1, "width"    # I
    .param p2, "pixel"    # I

    .prologue
    .line 4310
    mul-int v0, p2, p1

    div-int/lit16 v0, v0, 0x2d0

    return v0
.end method

.method private setCurBlockBound()V
    .locals 3

    .prologue
    const/16 v2, 0xbb8

    .line 1223
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    if-eqz v0, :cond_4

    .line 1224
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    iget v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCharCount(I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    .line 1225
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    if-gez v0, :cond_0

    .line 1226
    iput v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    .line 1228
    :cond_0
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-eq v0, v1, :cond_3

    .line 1229
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    iget v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCharCount(I)I

    move-result v0

    if-gez v0, :cond_2

    .line 1230
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    add-int/lit16 v0, v0, 0xbb8

    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    .line 1246
    :cond_1
    :goto_0
    return-void

    .line 1232
    :cond_2
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    iget v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCharCount(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    goto :goto_0

    .line 1235
    :cond_3
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->length()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    goto :goto_0

    .line 1237
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    .line 1238
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-eq v0, v1, :cond_5

    .line 1239
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    iget v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCharCount(I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    .line 1240
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    if-gez v0, :cond_1

    .line 1241
    iput v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    goto :goto_0

    .line 1244
    :cond_5
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->length()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    goto :goto_0
.end method

.method private setEventListener()V
    .locals 3

    .prologue
    .line 297
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/text/control/EditGestureDetector;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_GestureDetector:Landroid/view/GestureDetector;

    .line 299
    new-instance v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$1;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopupListener:Lcom/infraware/polarisoffice5/text/control/ToastPopup$ToastPopupListener;

    .line 310
    new-instance v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$2;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 320
    return-void
.end method

.method private setToastPopupEventListener(Lcom/infraware/polarisoffice5/text/control/ToastPopup;Landroid/view/View;)V
    .locals 12
    .param p1, "toastPopup"    # Lcom/infraware/polarisoffice5/text/control/ToastPopup;
    .param p2, "toastPopupView"    # Landroid/view/View;

    .prologue
    .line 2128
    const v10, 0x7f0b026c

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    .line 2129
    .local v9, "tvSelectAll":Landroid/widget/ImageView;
    const v10, 0x7f0b026e

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 2130
    .local v6, "tvCut":Landroid/widget/ImageView;
    const v10, 0x7f0b0270

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 2131
    .local v5, "tvCopy":Landroid/widget/ImageView;
    const v10, 0x7f0b0272

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 2132
    .local v8, "tvPaste":Landroid/widget/ImageView;
    const v10, 0x7f0b0274

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 2134
    .local v7, "tvMore":Landroid/widget/ImageView;
    const v10, 0x7f0b026d

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 2135
    .local v4, "ivSelectAll":Landroid/widget/ImageView;
    const v10, 0x7f0b026f

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 2136
    .local v2, "ivCut":Landroid/widget/ImageView;
    const v10, 0x7f0b0271

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 2137
    .local v1, "ivCopy":Landroid/widget/ImageView;
    const v10, 0x7f0b0273

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 2139
    .local v3, "ivPaste":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hasSelection()Z

    move-result v10

    if-eqz v10, :cond_5

    .line 2140
    const/4 v10, 0x0

    iput v10, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nToastPopupState:I

    .line 2145
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isSelectAllSupport()Z

    move-result v10

    if-nez v10, :cond_0

    .line 2148
    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 2151
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 2154
    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 2157
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v10

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v11

    sub-int/2addr v10, v11

    if-nez v10, :cond_6

    .line 2158
    const/16 v10, 0x8

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2159
    const/16 v10, 0x8

    invoke-virtual {v2, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2160
    const/16 v10, 0x8

    invoke-virtual {v5, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2161
    const/16 v10, 0x8

    invoke-virtual {v7, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2173
    :goto_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v10

    iget v10, v10, Landroid/content/res/Configuration;->orientation:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_2

    .line 2177
    invoke-virtual {v8}, Landroid/widget/ImageView;->getVisibility()I

    move-result v10

    if-nez v10, :cond_2

    .line 2178
    const/16 v10, 0x8

    invoke-virtual {v3, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2181
    :cond_2
    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v10, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getViewMode()I

    move-result v10

    if-nez v10, :cond_3

    .line 2184
    :try_start_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hasText()Z

    move-result v10

    if-nez v10, :cond_3

    .line 2185
    const/16 v10, 0x8

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2186
    iget v10, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nToastPopupState:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_3

    invoke-virtual {v9}, Landroid/widget/ImageView;->getVisibility()I

    move-result v10

    const/16 v11, 0x8

    if-ne v10, v11, :cond_3

    .line 2187
    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->dismiss()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2198
    :cond_3
    :goto_2
    iget v10, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nToastPopupState:I

    packed-switch v10, :pswitch_data_0

    .line 2231
    :cond_4
    :goto_3
    new-instance v10, Lcom/infraware/polarisoffice5/text/control/EditCtrl$7;

    invoke-direct {v10, p0, v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$7;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;Landroid/widget/ImageView;)V

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2278
    new-instance v10, Lcom/infraware/polarisoffice5/text/control/EditCtrl$8;

    invoke-direct {v10, p0, v5, p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$8;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;Landroid/widget/ImageView;Lcom/infraware/polarisoffice5/text/control/ToastPopup;)V

    invoke-virtual {v5, v10}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2326
    new-instance v10, Lcom/infraware/polarisoffice5/text/control/EditCtrl$9;

    invoke-direct {v10, p0, v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$9;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;Landroid/widget/ImageView;)V

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2380
    new-instance v10, Lcom/infraware/polarisoffice5/text/control/EditCtrl$10;

    invoke-direct {v10, p0, v7, p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$10;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;Landroid/widget/ImageView;Lcom/infraware/polarisoffice5/text/control/ToastPopup;)V

    invoke-virtual {v7, v10}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2426
    new-instance v10, Lcom/infraware/polarisoffice5/text/control/EditCtrl$11;

    invoke-direct {v10, p0, v9, p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$11;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;Landroid/widget/ImageView;Lcom/infraware/polarisoffice5/text/control/ToastPopup;)V

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2474
    return-void

    .line 2142
    :cond_5
    const/4 v10, 0x1

    iput v10, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nToastPopupState:I

    goto/16 :goto_0

    .line 2163
    :cond_6
    const/4 v10, 0x0

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2164
    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2165
    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2166
    const/4 v10, 0x0

    invoke-virtual {v1, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2167
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2168
    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2169
    const/4 v10, 0x0

    invoke-virtual {v7, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 2190
    :catch_0
    move-exception v0

    .line 2191
    .local v0, "e":Ljava/lang/NullPointerException;
    const/16 v10, 0x8

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2192
    iget v10, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nToastPopupState:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_3

    invoke-virtual {v9}, Landroid/widget/ImageView;->getVisibility()I

    move-result v10

    const/16 v11, 0x8

    if-ne v10, v11, :cond_3

    .line 2193
    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->dismiss()V

    goto :goto_2

    .line 2200
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :pswitch_0
    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v10, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getViewMode()I

    move-result v10

    const/4 v11, 0x1

    if-eq v10, v11, :cond_7

    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v10, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getViewMode()I

    move-result v10

    const/4 v11, 0x2

    if-ne v10, v11, :cond_8

    .line 2203
    :cond_7
    const/16 v10, 0x8

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2204
    const/16 v10, 0x8

    invoke-virtual {v2, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2205
    const/16 v10, 0x8

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2206
    const/16 v10, 0x8

    invoke-virtual {v3, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 2209
    :cond_8
    const/4 v10, 0x0

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2210
    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 2213
    :pswitch_1
    const/16 v10, 0x8

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2214
    const/16 v10, 0x8

    invoke-virtual {v2, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2215
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getShowMode()I

    move-result v10

    const/4 v11, 0x2

    if-eq v10, v11, :cond_9

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getShowMode()I

    move-result v10

    const/4 v11, 0x3

    if-eq v10, v11, :cond_9

    .line 2216
    const/16 v10, 0x8

    invoke-virtual {v5, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2217
    const/16 v10, 0x8

    invoke-virtual {v1, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2223
    :goto_4
    invoke-virtual {v7}, Landroid/widget/ImageView;->getVisibility()I

    move-result v10

    if-eqz v10, :cond_4

    .line 2224
    const/16 v10, 0x8

    invoke-virtual {v3, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 2219
    :cond_9
    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2220
    const/16 v10, 0x8

    invoke-virtual {v4, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2221
    const/16 v10, 0x8

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4

    .line 2198
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setmorePopuplayout()V
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x0

    const/16 v10, 0x8

    .line 1953
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v8, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getDictionaryPanelMain()Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    move-result-object v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v8, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getDictionaryPanelMain()Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isShown()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1954
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v8, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getDictionaryPanelMain()Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->hidePanel()V

    .line 1955
    :cond_0
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    const-string/jumbo v9, "layout_inflater"

    invoke-virtual {v8, v9}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 1956
    .local v1, "inflator":Landroid/view/LayoutInflater;
    const v8, 0x7f030053

    const/4 v9, 0x0

    invoke-virtual {v1, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->morePopupView:Landroid/view/View;

    .line 1958
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->morePopupView:Landroid/view/View;

    const v9, 0x7f0b022d

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 1959
    .local v7, "textTTS":Landroid/widget/TextView;
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->morePopupView:Landroid/view/View;

    const v9, 0x7f0b022e

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 1960
    .local v6, "textShare":Landroid/widget/TextView;
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->morePopupView:Landroid/view/View;

    const v9, 0x7f0b022f

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 1962
    .local v5, "textSearch":Landroid/widget/TextView;
    invoke-virtual {v7, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1963
    invoke-virtual {v6, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1964
    invoke-virtual {v5, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1969
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v8

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v9

    sub-int/2addr v8, v9

    if-nez v8, :cond_1

    .line 1970
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    invoke-virtual {v8}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f070310

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1972
    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1973
    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1976
    :cond_1
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/infraware/common/util/Utils;->isKnoxMode(Landroid/content/Context;)Z

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_2

    .line 1977
    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1978
    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1981
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getShowMode()I

    move-result v8

    if-eq v8, v12, :cond_3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getShowMode()I

    move-result v8

    const/4 v9, 0x3

    if-ne v8, v9, :cond_4

    .line 1982
    :cond_3
    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1986
    :cond_4
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    invoke-virtual {v8}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/infraware/common/util/Utils;->isActiveTTS(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 1987
    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1990
    :cond_5
    const-string/jumbo v8, "android.intent.action.WEB_SEARCH"

    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    invoke-static {v8, v9}, Lcom/infraware/common/util/Utils;->isIntentSafe(Ljava/lang/String;Landroid/app/Activity;)Z

    move-result v2

    .line 1992
    .local v2, "isPossibeSearch":Z
    if-nez v2, :cond_7

    .line 1994
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    invoke-static {v8}, Lcom/infraware/common/define/CMModelDefine$Diotek;->USE_DICTIONARY_SEARCH(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$Diotek;->isLoadComplete()Z

    move-result v8

    if-nez v8, :cond_7

    .line 1995
    :cond_6
    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2000
    :cond_7
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->morePopupView:Landroid/view/View;

    invoke-virtual {v8, v11, v11}, Landroid/view/View;->measure(II)V

    .line 2001
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->morePopupView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 2003
    .local v3, "nCurMorePopupWidth":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v0, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2004
    .local v0, "getPhoneWidth":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget v8, v8, Landroid/content/res/Configuration;->orientation:I

    if-ne v8, v12, :cond_8

    .line 2005
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v0, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 2006
    :cond_8
    const/16 v8, 0x1b4

    invoke-direct {p0, v0, v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->resizePixelByWidth(II)I

    move-result v4

    .line 2008
    .local v4, "nMaxMorePopupWidth":I
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->morePopupView:Landroid/view/View;

    invoke-virtual {v8, v9}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->setContentView(Landroid/view/View;)V

    .line 2010
    if-lt v3, v4, :cond_9

    .line 2011
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v8, v4}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->setMorePopupWidth(I)V

    .line 2012
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->morePopupView:Landroid/view/View;

    invoke-virtual {v8, v11, v11}, Landroid/view/View;->measure(II)V

    .line 2017
    :goto_0
    return-void

    .line 2015
    :cond_9
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v8, v3}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->setMorePopupWidth(I)V

    goto :goto_0
.end method

.method private toastPopupHandlerInitialize()V
    .locals 1

    .prologue
    .line 1903
    new-instance v0, Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    .line 1904
    new-instance v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$5;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$5;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_toastPopupRunnable:Ljava/lang/Runnable;

    .line 1929
    return-void
.end method

.method private toastpopupUpdate()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x12c

    .line 4345
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    if-eqz v0, :cond_0

    .line 4346
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4348
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_toastPopupRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 4354
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    if-eqz v0, :cond_1

    .line 4355
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4357
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_moretoastPopupRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 4364
    :cond_1
    :goto_1
    return-void

    .line 4349
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_toastPopupRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasCallbacks(Ljava/lang/Runnable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4351
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_toastPopupRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 4352
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_toastPopupRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 4358
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_moretoastPopupRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasCallbacks(Ljava/lang/Runnable;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4360
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_moretoastPopupRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 4361
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_moretoastPopupRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method


# virtual methods
.method public OnSearchMenu()V
    .locals 11

    .prologue
    const/16 v10, 0xe

    const/high16 v9, 0x41c80000    # 25.0f

    const/4 v8, 0x0

    .line 2709
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2710
    .local v1, "alItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PopupViewerSearchAdapter;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    const v6, 0x7f03003d

    invoke-direct {v0, p0, v5, v6, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PopupViewerSearchAdapter;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;Landroid/content/Context;ILjava/util/List;)V

    .line 2714
    .local v0, "adapter":Lcom/infraware/polarisoffice5/text/control/EditCtrl$PopupViewerSearchAdapter;
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070166

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2716
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine;->isChina()Z

    move-result v5

    if-nez v5, :cond_0

    .line 2717
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070251

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2719
    :cond_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    invoke-static {v5}, Lcom/infraware/common/define/CMModelDefine$Diotek;->USE_DICTIONARY_SEARCH(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$Diotek;->isLoadComplete()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2720
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v6

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v6, 0x64

    if-ge v5, v6, :cond_1

    .line 2721
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f07008a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2725
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2

    .line 2726
    invoke-direct {p0, v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->onClickSearchItem(I)V

    .line 2759
    :goto_0
    return-void

    .line 2730
    :cond_2
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v5, v10, :cond_4

    .line 2731
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v6

    invoke-direct {v2, v5, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 2735
    .local v2, "builder":Landroid/app/AlertDialog$Builder;
    :goto_1
    const v5, 0x7f0702c7

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 2736
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->mOnClickListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v0, v5}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2738
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    iput-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ad:Landroid/app/AlertDialog;

    .line 2739
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ad:Landroid/app/AlertDialog;

    invoke-virtual {v5}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v4

    .line 2742
    .local v4, "lv":Landroid/widget/ListView;
    invoke-static {}, Lcom/infraware/common/util/Utils;->isL()Z

    move-result v5

    if-nez v5, :cond_5

    .line 2743
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f05002e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2744
    .local v3, "colorValue":Ljava/lang/String;
    new-instance v5, Landroid/graphics/drawable/ColorDrawable;

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-direct {v5, v6}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 2745
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    const/high16 v6, 0x3f000000    # 0.5f

    invoke-static {v5, v6}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 2748
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v5, v10, :cond_3

    .line 2750
    const v5, 0x7f020130

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setSelector(I)V

    .line 2757
    .end local v3    # "colorValue":Ljava/lang/String;
    :cond_3
    :goto_2
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ad:Landroid/app/AlertDialog;

    invoke-virtual {v5, v8}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 2758
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ad:Landroid/app/AlertDialog;

    invoke-virtual {v5}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 2733
    .end local v2    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v4    # "lv":Landroid/widget/ListView;
    :cond_4
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    invoke-direct {v2, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .restart local v2    # "builder":Landroid/app/AlertDialog$Builder;
    goto :goto_1

    .line 2754
    .restart local v4    # "lv":Landroid/widget/ListView;
    :cond_5
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    invoke-static {v5, v9}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    const/high16 v7, 0x41200000    # 10.0f

    invoke-static {v6, v7}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v6

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    invoke-static {v7, v9}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v7

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/widget/ListView;->setPadding(IIII)V

    goto :goto_2
.end method

.method public addBoundBlockText(I)V
    .locals 4
    .param p1, "nBlock"    # I

    .prologue
    const/4 v3, 0x0

    .line 3251
    if-lez p1, :cond_0

    .line 3252
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableInsert:Z

    .line 3253
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableScroll:Z

    .line 3254
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    add-int/lit8 v2, p1, -0x1

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockText(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v3, v1}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 3257
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge p1, v0, :cond_1

    .line 3258
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableInsert:Z

    .line 3259
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableScroll:Z

    .line 3260
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    add-int/lit8 v2, p1, 0x1

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockText(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    .line 3262
    :cond_1
    return-void
.end method

.method public addBoundNextBlockText(I)V
    .locals 5
    .param p1, "nBlock"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 3267
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bChanged:Z

    if-ne v1, v2, :cond_1

    .line 3268
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v1, p1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockText(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setText(Ljava/lang/CharSequence;)V

    .line 3269
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->addBoundBlockText(I)V

    .line 3289
    :cond_0
    :goto_0
    return-void

    .line 3273
    :cond_1
    if-le p1, v2, :cond_2

    .line 3274
    iput-boolean v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableInsert:Z

    .line 3275
    iput-boolean v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableScroll:Z

    .line 3277
    :try_start_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v1

    const/4 v2, 0x0

    add-int/lit8 v3, p1, -0x2

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCharCount(I)I

    move-result v3

    invoke-interface {v1, v2, v3}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3284
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge p1, v1, :cond_0

    .line 3285
    iput-boolean v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableInsert:Z

    .line 3286
    iput-boolean v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableScroll:Z

    .line 3287
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    add-int/lit8 v3, p1, 0x1

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockText(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_0

    .line 3279
    :catch_0
    move-exception v0

    .line 3280
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-interface {v1, v4, v2}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_1
.end method

.method public addBoundPreBlockText(I)V
    .locals 6
    .param p1, "nBlock"    # I

    .prologue
    const/4 v5, 0x0

    .line 3294
    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bChanged:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 3295
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v2, p1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setText(Ljava/lang/CharSequence;)V

    .line 3296
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->addBoundBlockText(I)V

    .line 3316
    :cond_0
    :goto_0
    return-void

    .line 3300
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    if-ge p1, v2, :cond_2

    .line 3301
    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableInsert:Z

    .line 3302
    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableScroll:Z

    .line 3303
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCharCount(I)I

    move-result v2

    add-int/lit8 v3, p1, 0x1

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCharCount(I)I

    move-result v3

    add-int v1, v2, v3

    .line 3304
    .local v1, "nStart":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v0

    .line 3305
    .local v0, "nEnd":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 3308
    .end local v0    # "nEnd":I
    .end local v1    # "nStart":I
    :cond_2
    if-lez p1, :cond_0

    .line 3309
    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableInsert:Z

    .line 3310
    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableScroll:Z

    .line 3313
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    add-int/lit8 v4, p1, -0x1

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockText(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v5, v3}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_0
.end method

.method public afterSamsungPaste(Ljava/lang/String;)V
    .locals 2
    .param p1, "pasteStr"    # Ljava/lang/String;

    .prologue
    .line 2672
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_pasteStrSamsung:Ljava/lang/String;

    .line 2674
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_handler:Landroid/os/Handler;

    new-instance v1, Lcom/infraware/polarisoffice5/text/control/EditCtrl$12;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$12;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2679
    return-void
.end method

.method public bringPointIntoView(I)Z
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 4030
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bFling:Z

    if-eqz v0, :cond_0

    .line 4031
    const/4 v0, 0x1

    .line 4033
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/EditText;->bringPointIntoView(I)Z

    move-result v0

    goto :goto_0
.end method

.method public calcPosition()I
    .locals 5

    .prologue
    .line 530
    iget v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollPos:I

    int-to-double v1, v1

    iget v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollHeight:I

    int-to-double v3, v3

    div-double/2addr v1, v3

    const-wide/high16 v3, 0x4059000000000000L    # 100.0

    mul-double/2addr v1, v3

    double-to-int v0, v1

    .line 532
    .local v0, "result":I
    return v0
.end method

.method public calcScrollHeight()V
    .locals 13

    .prologue
    const/16 v12, 0xbb8

    .line 3322
    const/4 v4, 0x0

    .line 3323
    .local v4, "nHeight":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineHeight()I

    move-result v11

    add-int/lit8 v5, v11, -0x1

    .line 3325
    .local v5, "nLineHeight":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->length()I

    move-result v11

    if-ge v11, v12, :cond_0

    .line 3326
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineCount()I

    move-result v11

    mul-int v4, v11, v5

    .line 3358
    :goto_0
    iput v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollHeight:I

    .line 3359
    return-void

    .line 3331
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v0

    .line 3334
    .local v0, "layout":Landroid/text/Layout;
    const/4 v3, 0x0

    .line 3335
    .local v3, "nBlockLine":I
    if-eqz v0, :cond_1

    .line 3336
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v11

    invoke-virtual {v11, v12}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v3

    .line 3337
    :cond_1
    mul-int v2, v3, v5

    .line 3339
    .local v2, "nBlockHeight":I
    const/4 v8, 0x0

    .line 3340
    .local v8, "nSubHeight":I
    iget-object v11, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getTotalTextLen(Z)I

    move-result v10

    .line 3341
    .local v10, "nTotalLen":I
    iget v11, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCount()I

    move-result v12

    add-int/lit8 v12, v12, -0x2

    if-lt v11, v12, :cond_2

    .line 3342
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->length()I

    move-result v9

    .line 3343
    .local v9, "nSubTotal":I
    sub-int/2addr v10, v9

    .line 3344
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineCount()I

    move-result v11

    mul-int v8, v11, v5

    .line 3347
    .end local v9    # "nSubTotal":I
    :cond_2
    div-int/lit16 v1, v10, 0xbb8

    .line 3348
    .local v1, "nBlockCount":I
    rem-int/lit16 v7, v10, 0xbb8

    .line 3350
    .local v7, "nModLen":I
    const/4 v6, 0x0

    .line 3352
    .local v6, "nModHeight":I
    if-eqz v0, :cond_3

    .line 3353
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v11

    invoke-virtual {v11, v7}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v11

    mul-int v6, v11, v5

    .line 3355
    :cond_3
    mul-int v11, v2, v1

    add-int/2addr v11, v6

    add-int v4, v11, v8

    goto :goto_0
.end method

.method public calcScrollPos(I)V
    .locals 10
    .param p1, "nScroll"    # I

    .prologue
    .line 3366
    const/4 v7, 0x0

    .line 3367
    .local v7, "nPos":I
    iget v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    if-nez v8, :cond_0

    .line 3368
    move v7, p1

    .line 3391
    :goto_0
    iput v7, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollPos:I

    .line 3392
    return-void

    .line 3371
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineHeight()I

    move-result v8

    add-int/lit8 v3, v8, -0x1

    .line 3373
    .local v3, "nLineHeight":I
    const/4 v2, 0x0

    .line 3374
    .local v2, "nBlockHeight":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v0

    .line 3376
    .local v0, "layout":Landroid/text/Layout;
    if-eqz v0, :cond_1

    .line 3377
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v8

    const/16 v9, 0xbb8

    invoke-virtual {v8, v9}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v8

    mul-int v2, v8, v3

    .line 3381
    :cond_1
    iget v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {p0, v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockOffsetInFile(I)I

    move-result v6

    .line 3382
    .local v6, "nOffset":I
    div-int/lit16 v1, v6, 0xbb8

    .line 3383
    .local v1, "nBlockCount":I
    rem-int/lit16 v4, v6, 0xbb8

    .line 3385
    .local v4, "nMod":I
    const/4 v5, 0x0

    .line 3386
    .local v5, "nModOffset":I
    if-eqz v0, :cond_2

    .line 3387
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v8

    invoke-virtual {v8, v4}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v8

    mul-int v5, v8, v3

    .line 3388
    :cond_2
    mul-int v8, v2, v1

    add-int/2addr v8, p1

    add-int v7, v8, v5

    goto :goto_0
.end method

.method public changeBlock(I)V
    .locals 7
    .param p1, "nDirection"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1249
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 1250
    .local v2, "visibleRect":Landroid/graphics/Rect;
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 1252
    iget v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    invoke-direct {p0, v3, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineTopForOffset(IZ)I

    move-result v1

    .line 1253
    .local v1, "nBlockTop":I
    iget v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    invoke-direct {p0, v3, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineBottomForOffset(IZ)I

    move-result v0

    .line 1255
    .local v0, "nBlockBottom":I
    const/4 v3, 0x5

    if-le p1, v3, :cond_2

    iget v3, v2, Landroid/graphics/Rect;->bottom:I

    if-le v3, v0, :cond_2

    iget v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bPreBlockLoad:Z

    if-nez v3, :cond_2

    .line 1256
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBufferChanged()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1257
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLoadingProgress()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1258
    new-instance v3, Lcom/infraware/polarisoffice5/text/control/EditCtrl$LoadBufferProgress;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$LoadBufferProgress;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$LoadBufferProgress;->start()V

    .line 1273
    :cond_0
    :goto_0
    return-void

    .line 1260
    :cond_1
    invoke-virtual {p0, v5, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->loadNextBlock(ZZ)V

    goto :goto_0

    .line 1262
    :cond_2
    const/4 v3, -0x5

    if-ge p1, v3, :cond_4

    iget v3, v2, Landroid/graphics/Rect;->top:I

    if-ge v3, v1, :cond_4

    iget v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    if-ltz v3, :cond_4

    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bNextBlockLoad:Z

    if-nez v3, :cond_4

    .line 1263
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBufferChanged()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1264
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLoadingProgress()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1265
    new-instance v3, Lcom/infraware/polarisoffice5/text/control/EditCtrl$LoadBufferProgress;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$LoadBufferProgress;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$LoadBufferProgress;->start()V

    goto :goto_0

    .line 1267
    :cond_3
    invoke-virtual {p0, v5, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->loadPreBlock(ZZ)V

    goto :goto_0

    .line 1270
    :cond_4
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bNextBlockLoad:Z

    .line 1271
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bPreBlockLoad:Z

    goto :goto_0
.end method

.method public changedText(Ljava/lang/CharSequence;III)V
    .locals 8
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    const/16 v3, 0xbb8

    const/4 v2, 0x1

    .line 1549
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableInsert:Z

    if-eqz v0, :cond_5

    .line 1550
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLoadingProgress()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1573
    :goto_0
    return-void

    .line 1553
    :cond_0
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setChanged(Z)V

    .line 1554
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setCurBlock(I)V

    .line 1555
    if-ne p4, p3, :cond_1

    .line 1556
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    add-int v1, p2, p4

    invoke-virtual {v0, p2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 1558
    .local v7, "strInsert":Ljava/lang/String;
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    add-int v1, p2, p4

    invoke-virtual {p0, v0, p2, v1, v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->replaceBuffer(IIILjava/lang/String;)V

    goto :goto_0

    .line 1559
    .end local v7    # "strInsert":Ljava/lang/String;
    :cond_1
    if-le p4, p3, :cond_3

    .line 1560
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBufferChangedLen()I

    move-result v0

    sub-int v1, p4, p3

    add-int/2addr v0, v1

    if-lt v0, v3, :cond_2

    .line 1561
    new-instance v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;ZLjava/lang/CharSequence;III)V

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->start()V

    goto :goto_0

    .line 1563
    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->insertText(Ljava/lang/CharSequence;III)V

    goto :goto_0

    .line 1565
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBufferChangedLen()I

    move-result v0

    sub-int v1, p3, p4

    add-int/2addr v0, v1

    if-lt v0, v3, :cond_4

    .line 1566
    new-instance v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;

    const/4 v2, 0x0

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;ZLjava/lang/CharSequence;III)V

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->start()V

    goto :goto_0

    .line 1568
    :cond_4
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->deleteText(Ljava/lang/CharSequence;III)V

    goto :goto_0

    .line 1571
    :cond_5
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableInsert:Z

    goto :goto_0
.end method

.method public deleteBuffer(IIILjava/lang/String;)V
    .locals 6
    .param p1, "nBlock"    # I
    .param p2, "nStartOffset"    # I
    .param p3, "nEndOffset"    # I
    .param p4, "strRemove"    # Ljava/lang/String;

    .prologue
    .line 3156
    if-ne p2, p3, :cond_0

    .line 3174
    :goto_0
    return-void

    .line 3159
    :cond_0
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    if-ge p2, v0, :cond_1

    .line 3160
    add-int/lit8 p1, p1, -0x1

    .line 3171
    :goto_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_UndoRedoMgr:Lcom/infraware/polarisoffice5/text/manager/CommandManager;

    sub-int v4, p3, p2

    const/4 v5, 0x1

    move v1, p1

    move-object v2, p4

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->addCommand(ILjava/lang/String;IIZ)V

    .line 3173
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->deleteMemBuffer(III)V

    goto :goto_0

    .line 3161
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    if-le p2, v0, :cond_2

    .line 3162
    add-int/lit8 p1, p1, 0x1

    .line 3163
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    sub-int/2addr p2, v0

    .line 3164
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    sub-int/2addr p3, v0

    goto :goto_1

    .line 3166
    :cond_2
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    sub-int/2addr p2, v0

    .line 3167
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    sub-int/2addr p3, v0

    goto :goto_1
.end method

.method public deleteSubBuffer(III)V
    .locals 1
    .param p1, "nBlock"    # I
    .param p2, "nStartOffset"    # I
    .param p3, "nEndOffset"    # I

    .prologue
    .line 3243
    if-ne p2, p3, :cond_0

    .line 3247
    :goto_0
    return-void

    .line 3246
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->deleteMemBuffer(III)V

    goto :goto_0
.end method

.method public deleteText(Ljava/lang/CharSequence;III)V
    .locals 6
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    const/4 v5, 0x0

    .line 1613
    iget v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    add-int v3, p2, p4

    add-int v4, p2, p3

    invoke-virtual {p0, v2, v3, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSubBufferText(III)Ljava/lang/String;

    move-result-object v1

    .line 1616
    .local v1, "strRemove":Ljava/lang/String;
    iget v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    add-int v3, p2, p4

    add-int v4, p2, p3

    invoke-virtual {p0, v2, v3, v4, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->deleteBuffer(IIILjava/lang/String;)V

    .line 1617
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v2

    iget v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    if-ge v2, v3, :cond_1

    .line 1618
    const/4 v2, 0x1

    invoke-virtual {p0, v5, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->loadPreBlock(ZZ)V

    .line 1624
    :cond_0
    :goto_0
    return-void

    .line 1619
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->length()I

    move-result v2

    iget v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    if-ge v2, v3, :cond_0

    .line 1620
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v0

    .line 1621
    .local v0, "nSelection":I
    iget v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    invoke-virtual {p0, v2, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->loadBlock(IZ)V

    .line 1622
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    goto :goto_0
.end method

.method public directReload(Ljava/lang/String;I)V
    .locals 6
    .param p1, "strFilePath"    # Ljava/lang/String;
    .param p2, "encodingIndex"    # I

    .prologue
    .line 1337
    if-eqz p2, :cond_0

    .line 1338
    add-int/lit8 p2, p2, -0x1

    .line 1340
    :cond_0
    invoke-static {}, Lcom/infraware/common/util/text/CharsetDetector;->getAllDetectableCharsets()[Ljava/lang/String;

    move-result-object v1

    .line 1341
    .local v1, "encodingItems":[Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1342
    .local v0, "availableCharsetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, v1

    if-ge v2, v4, :cond_2

    .line 1343
    aget-object v4, v1, v2

    invoke-static {v4}, Ljava/nio/charset/Charset;->isSupported(Ljava/lang/String;)Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 1344
    aget-object v4, v1, v2

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1342
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1348
    :cond_2
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1350
    .local v3, "strEncoding":Ljava/lang/String;
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v4, v3}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->setLoadingEncoding(Ljava/lang/String;)V

    .line 1352
    new-instance v4, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5, p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;ILjava/lang/String;)V

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->start()V

    .line 1353
    return-void
.end method

.method public dismissPopup()V
    .locals 1

    .prologue
    .line 2074
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    if-eqz v0, :cond_0

    .line 2075
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2076
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->dismiss()V

    .line 2078
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    if-eqz v0, :cond_1

    .line 2079
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2081
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->dismiss()V

    .line 2082
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hideSoftKeyboard()Z

    .line 2085
    :cond_1
    return-void
.end method

.method public dismissPopupNotHideKeyboard()V
    .locals 1

    .prologue
    .line 2088
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    if-eqz v0, :cond_0

    .line 2089
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2090
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->dismiss()V

    .line 2092
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    if-eqz v0, :cond_1

    .line 2093
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2095
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->dismiss()V

    .line 2098
    :cond_1
    return-void
.end method

.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 562
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 563
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isShowSoftKey()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 564
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setShowSoftKeyBoard(Z)V

    .line 565
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 566
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->dismiss()V

    .line 567
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 568
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->dismiss()V

    .line 572
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/EditText;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public doPaste()V
    .locals 1

    .prologue
    .line 2124
    new-instance v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;->start()V

    .line 2125
    return-void
.end method

.method public flickProcess(I)V
    .locals 11
    .param p1, "velocityY"    # I

    .prologue
    const/4 v1, 0x0

    .line 3519
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bFling:Z

    .line 3520
    iput p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nVelocityY:I

    .line 3522
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v2, 0x3e8

    if-ge v0, v2, :cond_0

    .line 3542
    :goto_0
    return-void

    .line 3525
    :cond_0
    invoke-direct {p0, v1, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineTopForOffset(IZ)I

    move-result v7

    .line 3526
    .local v7, "nMinY":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->length()I

    move-result v0

    invoke-direct {p0, v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineBottomForOffset(IZ)I

    move-result v8

    .line 3527
    .local v8, "nMaxY":I
    const/4 v10, 0x0

    .line 3529
    .local v10, "nPadding":I
    :try_start_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTotalPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTotalPaddingBottom()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    add-int v10, v0, v2

    .line 3534
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getHeight()I

    move-result v0

    sub-int/2addr v0, v10

    sub-int/2addr v8, v0

    .line 3536
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_Scroller:Landroid/widget/Scroller;

    iget v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollY:I

    move v3, v1

    move v4, p1

    move v5, v1

    move v6, v1

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    goto :goto_0

    .line 3530
    :catch_0
    move-exception v9

    .line 3531
    .local v9, "e":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    .locals 1

    .prologue
    .line 1730
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    return-object v0
.end method

.method public getAlertDialog()Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 525
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ad:Landroid/app/AlertDialog;

    return-object v0
.end method

.method public getBackgroundTheme()I
    .locals 1

    .prologue
    .line 1689
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_theme:I

    return v0
.end method

.method public getBlockBufferText(I)Ljava/lang/String;
    .locals 6
    .param p1, "nBlock"    # I

    .prologue
    .line 3199
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-le p1, v3, :cond_2

    .line 3200
    :cond_0
    const-string/jumbo v2, ""

    .line 3222
    :cond_1
    :goto_0
    return-object v2

    .line 3203
    :cond_2
    const-string/jumbo v2, ""

    .line 3204
    .local v2, "strBlockBuffer":Ljava/lang/String;
    const/4 v1, 0x0

    .line 3205
    .local v1, "nStartOffset":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v3, p1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCharCount(I)I

    move-result v0

    .line 3206
    .local v0, "nEndOffset":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v3, p1, v1, v0}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getSubBufferText(III)Ljava/lang/String;

    move-result-object v2

    .line 3209
    if-lez p1, :cond_3

    .line 3210
    const/4 v1, 0x0

    .line 3211
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    add-int/lit8 v4, p1, -0x1

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCharCount(I)I

    move-result v0

    .line 3212
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    add-int/lit8 v5, p1, -0x1

    invoke-virtual {v4, v5, v1, v0}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getSubBufferText(III)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3216
    :cond_3
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge p1, v3, :cond_1

    .line 3217
    const/4 v1, 0x0

    .line 3218
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    add-int/lit8 v4, p1, 0x1

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCharCount(I)I

    move-result v0

    .line 3219
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    add-int/lit8 v5, p1, 0x1

    invoke-virtual {v4, v5, v1, v0}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getSubBufferText(III)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public getBlockCharCount(I)I
    .locals 1
    .param p1, "nBlock"    # I

    .prologue
    .line 4015
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCharCount(I)I

    move-result v0

    return v0
.end method

.method public getBlockCount()I
    .locals 1

    .prologue
    .line 1409
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCount()I

    move-result v0

    return v0
.end method

.method public getBlockEndOffset()I
    .locals 1

    .prologue
    .line 3895
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    return v0
.end method

.method public getBlockIndexFromOffset(I)I
    .locals 1
    .param p1, "nOffset"    # I

    .prologue
    .line 4010
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockIndexFromOffset(I)I

    move-result v0

    return v0
.end method

.method public getBlockOffsetFromOffset(I)I
    .locals 1
    .param p1, "nOffset"    # I

    .prologue
    .line 4005
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockOffsetFromOffset(I)I

    move-result v0

    return v0
.end method

.method public getBlockOffsetInBuffer(I)I
    .locals 1
    .param p1, "nBlock"    # I

    .prologue
    .line 3995
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockOffsetInBuffer(I)I

    move-result v0

    return v0
.end method

.method public getBlockOffsetInFile(I)I
    .locals 1
    .param p1, "nBlock"    # I

    .prologue
    .line 4000
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockOffsetInFile(I)I

    move-result v0

    return v0
.end method

.method public getBlockOffsetList()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/infraware/polarisoffice5/text/manager/TextBlock;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4341
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockOffsetList()Ljava/util/LinkedList;

    move-result-object v0

    return-object v0
.end method

.method public getBlockStartOffset()I
    .locals 1

    .prologue
    .line 3891
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    return v0
.end method

.method public getBlockText(I)Ljava/lang/String;
    .locals 1
    .param p1, "nBlock"    # I

    .prologue
    .line 3915
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockTextFromFile(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBufferChanged()Z
    .locals 1

    .prologue
    .line 3932
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBufferChanged()Z

    move-result v0

    return v0
.end method

.method public getCalcPositionString(Z)Ljava/lang/String;
    .locals 6
    .param p1, "afterscroll"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 510
    const/4 v0, 0x0

    .line 511
    .local v0, "currentPosition":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->calcPosition()I

    move-result v0

    .line 512
    const-string/jumbo v1, ""

    .line 514
    .local v1, "positionMent":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 515
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07030b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 520
    :goto_0
    return-object v1

    .line 517
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07030d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getCurBlock()I
    .locals 1

    .prologue
    .line 1400
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    return v0
.end method

.method public getCurrentTopCaret()I
    .locals 7

    .prologue
    .line 1357
    :try_start_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTotalPaddingTop()I

    move-result v2

    .line 1358
    .local v2, "paddingTop":I
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 1359
    .local v3, "rectVisible":Landroid/graphics/Rect;
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 1360
    const/4 v1, 0x0

    .line 1361
    .local v1, "nLine":I
    iget v5, v3, Landroid/graphics/Rect;->top:I

    if-eqz v5, :cond_0

    .line 1362
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v5

    iget v6, v3, Landroid/graphics/Rect;->top:I

    add-int/2addr v6, v2

    invoke-virtual {v5, v6}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v5

    add-int/lit8 v1, v5, 0x1

    .line 1364
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/text/Layout;->getLineStart(I)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 1368
    .end local v1    # "nLine":I
    .end local v2    # "paddingTop":I
    .end local v3    # "rectVisible":Landroid/graphics/Rect;
    :goto_0
    return v4

    .line 1367
    :catch_0
    move-exception v0

    .line 1368
    .local v0, "e":Ljava/lang/NullPointerException;
    const/4 v4, -0x1

    goto :goto_0
.end method

.method public getCursorStatus()Z
    .locals 1

    .prologue
    .line 780
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bShowCursor:Z

    return v0
.end method

.method public getEnableInsert()Z
    .locals 1

    .prologue
    .line 3911
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableInsert:Z

    return v0
.end method

.method public getLoadingProgress()Z
    .locals 1

    .prologue
    .line 3903
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bLoadingProgress:Z

    return v0
.end method

.method public getMarkedString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 3986
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPd()Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 4368
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_pd:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method public getSamsungClipboard()Lcom/infraware/polarisoffice5/text/manager/SecClipboard;
    .locals 1

    .prologue
    .line 4291
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_secClipboard:Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

    if-eqz v0, :cond_0

    .line 4292
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_secClipboard:Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

    .line 4294
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getScrollHeight()I
    .locals 1

    .prologue
    .line 3362
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollHeight:I

    return v0
.end method

.method public getScrollPos()I
    .locals 1

    .prologue
    .line 3395
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollPos:I

    return v0
.end method

.method public getSelectBegin()I
    .locals 2

    .prologue
    .line 3977
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionStart()I

    move-result v0

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionEnd()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public getSelectEnd()I
    .locals 2

    .prologue
    .line 3981
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionStart()I

    move-result v0

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectionEnd()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public getSelectboundRect(Z)Landroid/graphics/Rect;
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 784
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBoundRect(Z)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public getShowMode()I
    .locals 2

    .prologue
    .line 1897
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v0

    .line 1899
    .local v0, "result":I
    return v0
.end method

.method public getSubBufferText(III)Ljava/lang/String;
    .locals 2
    .param p1, "nBlock"    # I
    .param p2, "nStartOffset"    # I
    .param p3, "nEndOffset"    # I

    .prologue
    .line 3177
    if-ne p2, p3, :cond_0

    .line 3178
    const-string/jumbo v0, ""

    .line 3194
    :goto_0
    return-object v0

    .line 3180
    :cond_0
    const-string/jumbo v0, ""

    .line 3181
    .local v0, "strSubBuffer":Ljava/lang/String;
    iget v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    if-ge p2, v1, :cond_1

    .line 3182
    add-int/lit8 p1, p1, -0x1

    .line 3192
    :goto_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v1, p1, p2, p3}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getSubBufferText(III)Ljava/lang/String;

    move-result-object v0

    .line 3194
    goto :goto_0

    .line 3183
    :cond_1
    iget v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    if-le p2, v1, :cond_2

    .line 3184
    add-int/lit8 p1, p1, 0x1

    .line 3185
    iget v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    sub-int/2addr p2, v1

    .line 3186
    iget v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    sub-int/2addr p3, v1

    goto :goto_1

    .line 3188
    :cond_2
    iget v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    sub-int/2addr p2, v1

    .line 3189
    iget v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    sub-int/2addr p3, v1

    goto :goto_1
.end method

.method public getTTSSelectEnd()I
    .locals 1

    .prologue
    .line 2943
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nTTSEnd:I

    return v0
.end method

.method public getTTSSelectStart()I
    .locals 1

    .prologue
    .line 2939
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nTTSStart:I

    return v0
.end method

.method public getTempFileCount()I
    .locals 1

    .prologue
    .line 3991
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getTempFileCount()I

    move-result v0

    return v0
.end method

.method public getTouchOffset(II)I
    .locals 7
    .param p1, "nPx"    # I
    .param p2, "nPy"    # I

    .prologue
    .line 2966
    const/4 v1, 0x0

    .line 2967
    .local v1, "nPos":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTotalPaddingTop()I

    move-result v3

    .line 2968
    .local v3, "paddingTop":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTotalPaddingLeft()I

    move-result v2

    .line 2969
    .local v2, "paddingLeft":I
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 2970
    .local v4, "rectVisible":Landroid/graphics/Rect;
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 2971
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v5

    iget v6, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v6, p2

    sub-int/2addr v6, v3

    invoke-virtual {v5, v6}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v0

    .line 2972
    .local v0, "nLine":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v5

    iget v6, v4, Landroid/graphics/Rect;->left:I

    add-int/2addr v6, p1

    sub-int/2addr v6, v2

    int-to-float v6, v6

    invoke-virtual {v5, v0, v6}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v1

    .line 2974
    return v1
.end method

.method public getUndoRedoFlag()Z
    .locals 1

    .prologue
    .line 3923
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bUndoRedoFlag:Z

    return v0
.end method

.method public hasAnyUndoRedo()Z
    .locals 2

    .prologue
    .line 1292
    const/4 v0, 0x0

    .line 1294
    .local v0, "result":Z
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_UndoRedoMgr:Lcom/infraware/polarisoffice5/text/manager/CommandManager;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->hasMoreRedo()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_UndoRedoMgr:Lcom/infraware/polarisoffice5/text/manager/CommandManager;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->hasMoreUndo()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1295
    :cond_0
    const/4 v0, 0x1

    .line 1298
    :cond_1
    return v0
.end method

.method public hasMoreRedo()Z
    .locals 1

    .prologue
    .line 1722
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_UndoRedoMgr:Lcom/infraware/polarisoffice5/text/manager/CommandManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->hasMoreRedo()Z

    move-result v0

    return v0
.end method

.method public hasMoreUndo()Z
    .locals 1

    .prologue
    .line 1718
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_UndoRedoMgr:Lcom/infraware/polarisoffice5/text/manager/CommandManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->hasMoreUndo()Z

    move-result v0

    return v0
.end method

.method public hasText()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2683
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ClipboardMgr:Landroid/text/ClipboardManager;

    if-eqz v2, :cond_1

    .line 2684
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ClipboardMgr:Landroid/text/ClipboardManager;

    invoke-virtual {v2}, Landroid/text/ClipboardManager;->hasText()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2685
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bHasText:Z

    .line 2699
    :goto_0
    return v0

    .line 2688
    :cond_0
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bHasText:Z

    :cond_1
    move v0, v1

    .line 2699
    goto :goto_0
.end method

.method public hideDictionaryPanel()V
    .locals 1

    .prologue
    .line 3952
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v0, :cond_0

    .line 3954
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->hidePanel()V

    .line 3956
    :cond_0
    return-void
.end method

.method public hideSoftKeyboard()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 3060
    const/4 v3, 0x0

    :try_start_0
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bShowSoftKeyboard:Z

    .line 3061
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bShowCursor:Z

    .line 3063
    new-instance v1, Lcom/infraware/polarisoffice5/common/ImmManager;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    invoke-direct {v1, v3}, Lcom/infraware/polarisoffice5/common/ImmManager;-><init>(Landroid/app/Activity;)V

    .line 3065
    .local v1, "manager":Lcom/infraware/polarisoffice5/common/ImmManager;
    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/ImmManager;->hideForcedIme()V

    .line 3066
    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/ImmManager;->removeKeyboardCallback()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3068
    const/4 v2, 0x1

    .line 3072
    .end local v1    # "manager":Lcom/infraware/polarisoffice5/common/ImmManager;
    :goto_0
    return v2

    .line 3069
    :catch_0
    move-exception v0

    .line 3070
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public initialize(Landroid/app/Activity;)V
    .locals 11
    .param p1, "actParent"    # Landroid/app/Activity;

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 224
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    .line 225
    new-instance v5, Lcom/infraware/polarisoffice5/text/manager/CommandManager;

    invoke-direct {v5, p0}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V

    iput-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_UndoRedoMgr:Lcom/infraware/polarisoffice5/text/manager/CommandManager;

    .line 226
    const-string/jumbo v5, "clipboard"

    invoke-virtual {p1, v5}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/text/ClipboardManager;

    iput-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ClipboardMgr:Landroid/text/ClipboardManager;

    .line 227
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setEventListener()V

    .line 229
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_TEXTMARK_REVERSE()Z

    move-result v5

    if-ne v5, v8, :cond_4

    .line 230
    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setHighlightColor(I)V

    .line 234
    :goto_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    instance-of v5, v5, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    if-eqz v5, :cond_1

    .line 235
    const-string/jumbo v4, "UTF-16LE"

    .line 236
    .local v4, "strCharset":Ljava/lang/String;
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    const/16 v7, 0x17

    invoke-virtual {v5, v6, v7, v8}, Lcom/infraware/common/config/RuntimeConfig;->getIntPreference(Landroid/content/Context;II)I

    move-result v2

    .line 237
    .local v2, "nEncoding":I
    if-eqz v2, :cond_0

    .line 238
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v5, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getCharsetList()Ljava/util/ArrayList;

    move-result-object v5

    add-int/lit8 v6, v2, -0x1

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "strCharset":Ljava/lang/String;
    check-cast v4, Ljava/lang/String;

    .line 239
    .restart local v4    # "strCharset":Ljava/lang/String;
    :cond_0
    new-instance v5, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v6

    invoke-direct {v5, v6, v4}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    .line 242
    .end local v2    # "nEncoding":I
    .end local v4    # "strCharset":Ljava/lang/String;
    :cond_1
    new-instance v5, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_Scroller:Landroid/widget/Scroller;

    .line 243
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_Scroller:Landroid/widget/Scroller;

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setScroller(Landroid/widget/Scroller;)V

    .line 245
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    iput-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_handler:Landroid/os/Handler;

    .line 246
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morepopupCoordinate:[I

    if-nez v5, :cond_2

    .line 247
    const/4 v5, 0x2

    new-array v5, v5, [I

    iput-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morepopupCoordinate:[I

    .line 256
    :cond_2
    :try_start_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    const-string/jumbo v6, "clipboardEx"

    invoke-virtual {v5, v6}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/sec/clipboard/ClipboardExManager;

    .line 257
    .local v3, "samsung":Landroid/sec/clipboard/ClipboardExManager;
    if-eqz v3, :cond_3

    .line 258
    new-instance v5, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    invoke-direct {v5, v6, v3}, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;-><init>(Landroid/app/Activity;Landroid/sec/clipboard/ClipboardExManager;)V

    iput-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_secClipboard:Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

    .line 260
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_secClipboard:Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->isSamsungClipboard()Z

    move-result v5

    if-nez v5, :cond_3

    .line 263
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_secClipboard:Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->thisFinalize()V

    .line 264
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_secClipboard:Lcom/infraware/polarisoffice5/text/manager/SecClipboard;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    .line 277
    .end local v3    # "samsung":Landroid/sec/clipboard/ClipboardExManager;
    :cond_3
    :goto_1
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_secClipboard:Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

    if-eqz v5, :cond_7

    .line 278
    iput v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_clipboardService:I

    .line 283
    :goto_2
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastPopupHandlerInitialize()V

    .line 284
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->morePopupHandlerInitialize()V

    .line 285
    return-void

    .line 232
    :cond_4
    const/4 v5, -0x1

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setHighlightColor(I)V

    goto/16 :goto_0

    .line 267
    :catch_0
    move-exception v0

    .line 268
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_secClipboard:Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

    if-eqz v5, :cond_5

    .line 269
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_secClipboard:Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->thisFinalize()V

    .line 270
    :cond_5
    iput-object v9, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_secClipboard:Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

    goto :goto_1

    .line 271
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v1

    .line 272
    .local v1, "e2":Ljava/lang/NoSuchMethodError;
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_secClipboard:Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

    if-eqz v5, :cond_6

    .line 273
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_secClipboard:Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->thisFinalize()V

    .line 274
    :cond_6
    iput-object v9, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_secClipboard:Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

    goto :goto_1

    .line 280
    .end local v1    # "e2":Ljava/lang/NoSuchMethodError;
    :cond_7
    iput v10, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_clipboardService:I

    goto :goto_2
.end method

.method public insertBuffer(IIILjava/lang/String;)V
    .locals 6
    .param p1, "nBlock"    # I
    .param p2, "nOffset"    # I
    .param p3, "nLen"    # I
    .param p4, "strInsert"    # Ljava/lang/String;

    .prologue
    .line 3140
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    if-ge p2, v0, :cond_0

    .line 3141
    add-int/lit8 p1, p1, -0x1

    .line 3150
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_UndoRedoMgr:Lcom/infraware/polarisoffice5/text/manager/CommandManager;

    const/4 v5, 0x0

    move v1, p1

    move-object v2, p4

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->addCommand(ILjava/lang/String;IIZ)V

    .line 3152
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v0, p4, p1, p2}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->insertMemBuffer(Ljava/lang/String;II)V

    .line 3153
    return-void

    .line 3142
    :cond_0
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    if-le p2, v0, :cond_1

    .line 3143
    add-int/lit8 p1, p1, 0x1

    .line 3144
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    sub-int/2addr p2, v0

    goto :goto_0

    .line 3146
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    sub-int/2addr p2, v0

    goto :goto_0
.end method

.method public insertSubBuffer(IILjava/lang/String;)V
    .locals 1
    .param p1, "nBlock"    # I
    .param p2, "nOffset"    # I
    .param p3, "strInsert"    # Ljava/lang/String;

    .prologue
    .line 3239
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v0, p3, p1, p2}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->insertMemBuffer(Ljava/lang/String;II)V

    .line 3240
    return-void
.end method

.method public insertText(Ljava/lang/CharSequence;III)V
    .locals 7
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    const/4 v6, 0x0

    .line 1590
    invoke-direct {p0, p1, p2, p3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->checkBeforeDiff(Ljava/lang/CharSequence;II)V

    .line 1592
    add-int v2, p2, p3

    add-int v3, p2, p4

    invoke-interface {p1, v2, v3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1595
    .local v1, "strInsert":Ljava/lang/CharSequence;
    iget v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    add-int v3, p2, p3

    sub-int v4, p4, p3

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->insertBuffer(IIILjava/lang/String;)V

    .line 1596
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    iget v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    add-int/lit16 v3, v3, 0xbb8

    if-le v2, v3, :cond_0

    .line 1597
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v2

    iget v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    add-int/lit16 v3, v3, 0xbb8

    add-int/lit8 v3, v3, -0x1

    if-le v2, v3, :cond_1

    .line 1598
    const/4 v2, 0x1

    invoke-virtual {p0, v6, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->loadNextBlock(ZZ)V

    .line 1605
    :cond_0
    :goto_0
    return-void

    .line 1599
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->length()I

    move-result v2

    iget v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    add-int/lit16 v3, v3, 0x1770

    if-le v2, v3, :cond_0

    .line 1600
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v0

    .line 1601
    .local v0, "nSelection":I
    iget v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    invoke-virtual {p0, v2, v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->loadBlock(IZ)V

    .line 1602
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    goto :goto_0
.end method

.method public isChanged()Z
    .locals 1

    .prologue
    .line 1302
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bChanged:Z

    return v0
.end method

.method public isEnableScroll()Z
    .locals 1

    .prologue
    .line 4299
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableScroll:Z

    return v0
.end method

.method public isFitSelectText(FF)Z
    .locals 13
    .param p1, "pX"    # F
    .param p2, "pY"    # F

    .prologue
    .line 2477
    const/4 v0, 0x0

    .line 2479
    .local v0, "bFit":Z
    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    .line 2480
    .local v9, "rectVisible":Landroid/graphics/Rect;
    invoke-virtual {p0, v9}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 2482
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v4

    .line 2483
    .local v4, "nStart":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v1

    .line 2485
    .local v1, "nEnd":I
    sub-int v10, v1, v4

    if-lez v10, :cond_0

    .line 2487
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v10

    invoke-virtual {v10, v4}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v5

    .line 2488
    .local v5, "nStartLine":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v10

    invoke-virtual {v10, v1}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v2

    .line 2490
    .local v2, "nEndLine":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTotalPaddingLeft()I

    move-result v6

    .line 2491
    .local v6, "paddingLeft":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTotalPaddingTop()I

    move-result v7

    .line 2493
    .local v7, "paddingTop":I
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    .line 2494
    .local v8, "rectSelect":Landroid/graphics/Rect;
    if-ne v5, v2, :cond_1

    .line 2495
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v10

    invoke-virtual {v10, v4}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v10

    float-to-int v10, v10

    add-int/2addr v10, v6

    iput v10, v8, Landroid/graphics/Rect;->left:I

    .line 2496
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v10

    invoke-virtual {v10, v1}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v10

    float-to-int v10, v10

    add-int/2addr v10, v6

    iput v10, v8, Landroid/graphics/Rect;->right:I

    .line 2497
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v10

    invoke-virtual {v10, v5}, Landroid/text/Layout;->getLineTop(I)I

    move-result v10

    add-int/2addr v10, v7

    iput v10, v8, Landroid/graphics/Rect;->top:I

    .line 2498
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v10

    invoke-virtual {v10, v5}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v10

    add-int/2addr v10, v7

    iput v10, v8, Landroid/graphics/Rect;->bottom:I

    .line 2500
    iget v10, v9, Landroid/graphics/Rect;->left:I

    float-to-int v11, p1

    add-int/2addr v10, v11

    iget v11, v9, Landroid/graphics/Rect;->top:I

    float-to-int v12, p2

    add-int/2addr v11, v12

    invoke-virtual {v8, v10, v11}, Landroid/graphics/Rect;->contains(II)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 2501
    const/4 v0, 0x1

    .line 2526
    .end local v2    # "nEndLine":I
    .end local v5    # "nStartLine":I
    .end local v6    # "paddingLeft":I
    .end local v7    # "paddingTop":I
    .end local v8    # "rectSelect":Landroid/graphics/Rect;
    :cond_0
    :goto_0
    return v0

    .line 2504
    .restart local v2    # "nEndLine":I
    .restart local v5    # "nStartLine":I
    .restart local v6    # "paddingLeft":I
    .restart local v7    # "paddingTop":I
    .restart local v8    # "rectSelect":Landroid/graphics/Rect;
    :cond_1
    move v3, v5

    .local v3, "nLine":I
    :goto_1
    if-gt v3, v2, :cond_0

    .line 2505
    if-ne v3, v5, :cond_2

    .line 2506
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v10

    invoke-virtual {v10, v4}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v10

    float-to-int v10, v10

    add-int/2addr v10, v6

    iput v10, v8, Landroid/graphics/Rect;->left:I

    .line 2507
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getWidth()I

    move-result v10

    sub-int/2addr v10, v6

    iput v10, v8, Landroid/graphics/Rect;->right:I

    .line 2515
    :goto_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v10

    invoke-virtual {v10, v3}, Landroid/text/Layout;->getLineTop(I)I

    move-result v10

    add-int/2addr v10, v7

    iput v10, v8, Landroid/graphics/Rect;->top:I

    .line 2516
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v10

    invoke-virtual {v10, v3}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v10

    add-int/2addr v10, v7

    iput v10, v8, Landroid/graphics/Rect;->bottom:I

    .line 2518
    iget v10, v9, Landroid/graphics/Rect;->left:I

    float-to-int v11, p1

    add-int/2addr v10, v11

    iget v11, v9, Landroid/graphics/Rect;->top:I

    float-to-int v12, p2

    add-int/2addr v11, v12

    invoke-virtual {v8, v10, v11}, Landroid/graphics/Rect;->contains(II)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 2519
    const/4 v0, 0x1

    .line 2520
    goto :goto_0

    .line 2508
    :cond_2
    if-ne v3, v2, :cond_3

    .line 2509
    iput v6, v8, Landroid/graphics/Rect;->left:I

    .line 2510
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v10

    invoke-virtual {v10, v1}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v10

    float-to-int v10, v10

    add-int/2addr v10, v6

    iput v10, v8, Landroid/graphics/Rect;->right:I

    goto :goto_2

    .line 2512
    :cond_3
    iput v6, v8, Landroid/graphics/Rect;->left:I

    .line 2513
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getWidth()I

    move-result v10

    sub-int/2addr v10, v6

    iput v10, v8, Landroid/graphics/Rect;->right:I

    goto :goto_2

    .line 2504
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public isFitTouchSelectionEnd(II)Z
    .locals 8
    .param p1, "nPx"    # I
    .param p2, "nPy"    # I

    .prologue
    const/4 v6, 0x0

    .line 3011
    const/4 v0, 0x0

    .line 3013
    .local v0, "bFit":Z
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 3014
    .local v5, "rectVisible":Landroid/graphics/Rect;
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 3016
    invoke-direct {p0, v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBoundRect(Z)Landroid/graphics/Rect;

    move-result-object v3

    .line 3017
    .local v3, "rectEnd":Landroid/graphics/Rect;
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 3019
    .local v4, "rectTouchSelection":Landroid/graphics/Rect;
    iget v7, v3, Landroid/graphics/Rect;->left:I

    add-int/lit8 v7, v7, -0x10

    add-int/lit8 v7, v7, -0x3c

    iput v7, v4, Landroid/graphics/Rect;->left:I

    .line 3020
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 3021
    .local v2, "rect":Landroid/graphics/Rect;
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 3022
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v1

    .line 3023
    .local v1, "layout":Landroid/text/Layout;
    if-nez v1, :cond_0

    .line 3037
    :goto_0
    return v6

    .line 3025
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v6

    invoke-virtual {v1, v6}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v6

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    if-ne v6, v7, :cond_2

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineCount()I

    move-result v6

    const/4 v7, 0x1

    if-eq v6, v7, :cond_2

    iget v6, v2, Landroid/graphics/Rect;->top:I

    if-eqz v6, :cond_2

    .line 3026
    iget v6, v3, Landroid/graphics/Rect;->top:I

    add-int/lit8 v6, v6, -0x37

    add-int/lit8 v6, v6, 0xf

    add-int/lit8 v6, v6, -0x28

    iput v6, v4, Landroid/graphics/Rect;->top:I

    .line 3030
    :goto_1
    iget v6, v3, Landroid/graphics/Rect;->right:I

    add-int/lit8 v6, v6, 0x10

    add-int/lit8 v6, v6, 0x3c

    iput v6, v4, Landroid/graphics/Rect;->right:I

    .line 3031
    iget v6, v4, Landroid/graphics/Rect;->top:I

    add-int/lit8 v6, v6, 0x37

    add-int/lit8 v6, v6, 0x28

    iput v6, v4, Landroid/graphics/Rect;->bottom:I

    .line 3033
    iget v6, v5, Landroid/graphics/Rect;->left:I

    add-int/2addr v6, p1

    iget v7, v5, Landroid/graphics/Rect;->top:I

    add-int/2addr v7, p2

    invoke-virtual {v4, v6, v7}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 3034
    const/4 v0, 0x1

    :cond_1
    move v6, v0

    .line 3037
    goto :goto_0

    .line 3028
    :cond_2
    iget v6, v3, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v6, v6, -0x28

    iput v6, v4, Landroid/graphics/Rect;->top:I

    goto :goto_1
.end method

.method public isFitTouchSelectionStart(II)Z
    .locals 7
    .param p1, "nPx"    # I
    .param p2, "nPy"    # I

    .prologue
    .line 2978
    const/4 v0, 0x0

    .line 2980
    .local v0, "bFit":Z
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 2981
    .local v4, "rectVisible":Landroid/graphics/Rect;
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 2983
    const/4 v5, 0x1

    invoke-direct {p0, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBoundRect(Z)Landroid/graphics/Rect;

    move-result-object v2

    .line 2984
    .local v2, "rectStart":Landroid/graphics/Rect;
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 2987
    .local v3, "rectTouchSelection":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v5

    if-nez v5, :cond_0

    .line 2988
    iget v5, v2, Landroid/graphics/Rect;->left:I

    add-int/lit8 v5, v5, 0x10

    add-int/lit8 v5, v5, -0x3c

    iput v5, v3, Landroid/graphics/Rect;->left:I

    .line 2992
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v1

    .line 2993
    .local v1, "layout":Landroid/text/Layout;
    if-nez v1, :cond_1

    .line 2994
    const/4 v5, 0x0

    .line 3007
    :goto_1
    return v5

    .line 2990
    .end local v1    # "layout":Landroid/text/Layout;
    :cond_0
    iget v5, v2, Landroid/graphics/Rect;->left:I

    add-int/lit8 v5, v5, -0x10

    add-int/lit8 v5, v5, -0x3c

    iput v5, v3, Landroid/graphics/Rect;->left:I

    goto :goto_0

    .line 2995
    .restart local v1    # "layout":Landroid/text/Layout;
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v5

    if-nez v5, :cond_3

    .line 2996
    iget v5, v2, Landroid/graphics/Rect;->bottom:I

    iput v5, v3, Landroid/graphics/Rect;->top:I

    .line 3000
    :goto_2
    iget v5, v3, Landroid/graphics/Rect;->left:I

    add-int/lit8 v5, v5, 0x20

    add-int/lit8 v5, v5, 0x3c

    iput v5, v3, Landroid/graphics/Rect;->right:I

    .line 3001
    iget v5, v3, Landroid/graphics/Rect;->top:I

    add-int/lit8 v5, v5, 0x37

    add-int/lit8 v5, v5, 0x28

    iput v5, v3, Landroid/graphics/Rect;->bottom:I

    .line 3003
    iget v5, v4, Landroid/graphics/Rect;->left:I

    add-int/2addr v5, p1

    iget v6, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v6, p2

    invoke-virtual {v3, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 3004
    const/4 v0, 0x1

    :cond_2
    move v5, v0

    .line 3007
    goto :goto_1

    .line 2998
    :cond_3
    iget v5, v2, Landroid/graphics/Rect;->top:I

    add-int/lit8 v5, v5, -0x37

    add-int/lit8 v5, v5, 0xf

    add-int/lit8 v5, v5, -0x28

    iput v5, v3, Landroid/graphics/Rect;->top:I

    goto :goto_2
.end method

.method public isFlingFinished()Z
    .locals 1

    .prologue
    .line 3575
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_Scroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    return v0
.end method

.method public isLongTapEvent()Z
    .locals 1

    .prologue
    .line 549
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bIsLongTapEvent:Z

    return v0
.end method

.method public isMorepopupShowing()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2064
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    if-eqz v1, :cond_0

    .line 2065
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2066
    const/4 v0, 0x1

    .line 2070
    :cond_0
    return v0
.end method

.method public isNewFile()Z
    .locals 1

    .prologue
    .line 3941
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->isNewFile()Z

    move-result v0

    return v0
.end method

.method public isSelEndDown()Z
    .locals 1

    .prologue
    .line 3045
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelEndDown:Z

    return v0
.end method

.method public isSelStartDown()Z
    .locals 1

    .prologue
    .line 3041
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelStartDown:Z

    return v0
.end method

.method public isSelectAllSupport()Z
    .locals 3

    .prologue
    .line 2101
    const/4 v0, 0x0

    .line 2103
    .local v0, "result":Z
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCount()I

    move-result v1

    const/4 v2, 0x3

    if-le v1, v2, :cond_0

    .line 2104
    const/4 v0, 0x0

    .line 2108
    :goto_0
    return v0

    .line 2106
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isSelectionChanged()Z
    .locals 1

    .prologue
    .line 696
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelectionchanged:Z

    return v0
.end method

.method public isShowCursor()Z
    .locals 1

    .prologue
    .line 3104
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bShowCursor:Z

    return v0
.end method

.method public isShowSoftKey()Z
    .locals 1

    .prologue
    .line 3096
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bShowSoftKeyboard:Z

    return v0
.end method

.method public isTextSelection()Z
    .locals 4

    .prologue
    .line 2112
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v2

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v3

    sub-int v1, v2, v3

    .line 2113
    .local v1, "temp":I
    const/4 v0, 0x0

    .line 2115
    .local v0, "result":Z
    if-nez v1, :cond_0

    .line 2116
    const/4 v0, 0x0

    .line 2120
    :goto_0
    return v0

    .line 2118
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isTexteditorInstance()Z
    .locals 1

    .prologue
    .line 3112
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    instance-of v0, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    if-eqz v0, :cond_0

    .line 3113
    const/4 v0, 0x1

    .line 3115
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isToastpopupShowing()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2054
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    if-eqz v1, :cond_0

    .line 2055
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ToastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2056
    const/4 v0, 0x1

    .line 2060
    :cond_0
    return v0
.end method

.method public loadBlock(IZ)V
    .locals 3
    .param p1, "nBlock"    # I
    .param p2, "bShowTitle"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1416
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 1417
    :cond_0
    const/4 p1, 0x0

    .line 1419
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    add-int/lit8 v0, v0, 0x1

    if-ne p1, v0, :cond_3

    .line 1420
    invoke-virtual {p0, p2, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->loadNextBlock(ZZ)V

    .line 1421
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    .line 1446
    :cond_2
    :goto_0
    return-void

    .line 1423
    :cond_3
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_4

    .line 1424
    invoke-virtual {p0, p2, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->loadPreBlock(ZZ)V

    .line 1425
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    goto :goto_0

    .line 1428
    :cond_4
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableInsert:Z

    .line 1429
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockText(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setText(Ljava/lang/CharSequence;)V

    .line 1430
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    .line 1431
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->addBoundBlockText(I)V

    .line 1432
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setCurBlock(I)V

    .line 1433
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bNextBlockLoad:Z

    .line 1434
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bPreBlockLoad:Z

    .line 1436
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    instance-of v0, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    if-eqz v0, :cond_2

    if-eqz p2, :cond_2

    .line 1437
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setTitleText()V

    .line 1438
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v2, :cond_2

    .line 1439
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    .line 1442
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setShowMode(I)V

    goto :goto_0
.end method

.method public loadBuffer()V
    .locals 2

    .prologue
    .line 3927
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBufferChanged()Z

    move-result v0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bBufferChanged:Z

    .line 3928
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    iget v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->loadBuffer(I)V

    .line 3929
    return-void
.end method

.method public loadNextBlock(ZZ)V
    .locals 12
    .param p1, "bShowTitle"    # Z
    .param p2, "bScroll"    # Z

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x1

    const/4 v8, 0x0

    .line 1451
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v7

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v9

    sub-int/2addr v7, v9

    if-eqz v7, :cond_0

    .line 1452
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getContext()Landroid/content/Context;

    move-result-object v7

    const v9, 0x7f0702eb

    invoke-static {v7, v9, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    .line 1454
    :cond_0
    iput-boolean v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableInsert:Z

    .line 1456
    iget v7, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    invoke-direct {p0, v7, v10}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineTopForOffset(IZ)I

    move-result v1

    .line 1457
    .local v1, "nBlockTop":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getScrollY()I

    move-result v7

    sub-int v2, v7, v1

    .line 1458
    .local v2, "nScrollY":I
    iget v7, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nTTSEnd:I

    iget v9, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    sub-int v4, v7, v9

    .line 1459
    .local v4, "nTTSOffset":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v7

    iget v9, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    if-le v7, v9, :cond_5

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v7

    iget v9, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    sub-int v3, v7, v9

    .line 1461
    .local v3, "nSelection":I
    :goto_0
    iget v7, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {p0, v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->addBoundNextBlockText(I)V

    .line 1462
    iget v7, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {p0, v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setCurBlock(I)V

    .line 1463
    iget v7, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    add-int v6, v3, v7

    .line 1465
    .local v6, "selection":I
    :try_start_0
    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1469
    :goto_1
    iput-boolean v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableScroll:Z

    .line 1471
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v7, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v7

    if-ne v7, v11, :cond_2

    .line 1472
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v5

    .line 1473
    .local v5, "nTTSPos":I
    if-lez v4, :cond_1

    .line 1474
    add-int/2addr v5, v4

    .line 1475
    :cond_1
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v7, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getTTSManager()Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->setTTSStartPos(I)V

    .line 1476
    invoke-virtual {p0, v5, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setTTSSelection(II)V

    .line 1479
    .end local v5    # "nTTSPos":I
    :cond_2
    if-ne p2, v10, :cond_3

    .line 1480
    invoke-virtual {p0, v8, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->scrollTo(II)V

    .line 1481
    iput v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollY:I

    .line 1484
    :cond_3
    iput-boolean v10, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bNextBlockLoad:Z

    .line 1486
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    instance-of v7, v7, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    if-eqz v7, :cond_4

    if-eqz p1, :cond_4

    .line 1487
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v7, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setTitleText()V

    .line 1488
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget v7, v7, Landroid/content/res/Configuration;->orientation:I

    if-ne v7, v10, :cond_4

    .line 1489
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v7, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v7

    const/4 v9, 0x2

    if-eq v7, v9, :cond_4

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v7, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v7

    const/4 v9, 0x3

    if-eq v7, v9, :cond_4

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v7, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v7

    if-eq v7, v11, :cond_4

    .line 1492
    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v7, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v7, v10}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setShowMode(I)V

    .line 1495
    :cond_4
    iput-boolean v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableScroll:Z

    .line 1496
    return-void

    .end local v3    # "nSelection":I
    .end local v6    # "selection":I
    :cond_5
    move v3, v8

    .line 1459
    goto/16 :goto_0

    .line 1466
    .restart local v3    # "nSelection":I
    .restart local v6    # "selection":I
    :catch_0
    move-exception v0

    .line 1467
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {p0, v8}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    goto :goto_1
.end method

.method public loadPreBlock(ZZ)V
    .locals 7
    .param p1, "bShowTitle"    # Z
    .param p2, "bScroll"    # Z

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1501
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v4

    sub-int/2addr v3, v4

    if-eqz v3, :cond_0

    .line 1502
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0702eb

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 1503
    :cond_0
    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bFling:Z

    .line 1504
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableInsert:Z

    .line 1506
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getScrollY()I

    move-result v1

    .line 1507
    .local v1, "nScrollY":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v3

    iget v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    sub-int v2, v3, v4

    .line 1510
    .local v2, "nSelection":I
    iget v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->addBoundPreBlockText(I)V

    .line 1511
    iget v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setCurBlock(I)V

    .line 1514
    iget v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    add-int/2addr v2, v3

    .line 1515
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->length()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 1516
    if-gez v2, :cond_1

    .line 1517
    const/4 v2, 0x0

    .line 1518
    :cond_1
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    .line 1522
    :cond_2
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableScroll:Z

    .line 1523
    if-ne p2, v5, :cond_3

    .line 1524
    iget v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    invoke-direct {p0, v3, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineTopForOffset(IZ)I

    move-result v0

    .line 1525
    .local v0, "nBlockTop":I
    iget v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollY:I

    add-int/2addr v3, v0

    invoke-virtual {p0, v6, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->scrollTo(II)V

    .line 1526
    add-int v3, v1, v0

    iput v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollY:I

    .line 1529
    .end local v0    # "nBlockTop":I
    :cond_3
    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bPreBlockLoad:Z

    .line 1530
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    instance-of v3, v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    if-eqz v3, :cond_4

    if-eqz p1, :cond_4

    .line 1531
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setTitleText()V

    .line 1532
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v5, :cond_4

    .line 1533
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_4

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_4

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v3

    const/4 v4, 0x4

    if-eq v3, v4, :cond_4

    .line 1536
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v3, v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setShowMode(I)V

    .line 1539
    :cond_4
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableScroll:Z

    .line 1540
    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bFling:Z

    .line 1541
    return-void
.end method

.method public newInfo()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 207
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setFontSize(I)V

    .line 208
    const/high16 v1, -0x1000000

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setFontColor(I)V

    .line 210
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    const/16 v3, 0x16

    invoke-virtual {v1, v2, v3, v4}, Lcom/infraware/common/config/RuntimeConfig;->getIntPreference(Landroid/content/Context;II)I

    move-result v0

    .line 212
    .local v0, "theme":I
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setBackgroundTheme(I)V

    .line 213
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    .line 215
    iput-boolean v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bChanged:Z

    .line 216
    const-string/jumbo v1, ""

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setText(Ljava/lang/CharSequence;)V

    .line 218
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setCurBlock(I)V

    .line 220
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_UndoRedoMgr:Lcom/infraware/polarisoffice5/text/manager/CommandManager;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->clearCommand()V

    .line 221
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 2891
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2917
    :goto_0
    return-void

    .line 2893
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getTTSManager()Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->onTTS()V

    .line 2894
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->dismiss()V

    goto :goto_0

    .line 2898
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->dismiss()V

    .line 2899
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->onShare()V

    goto :goto_0

    .line 2903
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->dismiss()V

    .line 2904
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->OnSearchMenu()V

    goto :goto_0

    .line 2891
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b022d
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/ContextMenu;

    .prologue
    .line 558
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v1, 0x0

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 710
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hasFocus()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v4, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v4

    if-eq v4, v8, :cond_0

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v4, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v4

    if-eq v4, v9, :cond_0

    .line 712
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->requestFocus()Z

    .line 715
    :cond_0
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    instance-of v4, v4, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    if-eqz v4, :cond_c

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v4, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-object v3, v4

    .line 716
    .local v3, "tea":Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    :goto_0
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getMWDnD()Lcom/infraware/common/multiwindow/MWDnDOperator;

    move-result-object v1

    .line 717
    .local v1, "oMWDnD":Lcom/infraware/common/multiwindow/MWDnDOperator;
    :cond_1
    if-eqz v1, :cond_2

    .line 718
    invoke-virtual {v1}, Lcom/infraware/common/multiwindow/MWDnDOperator;->clearLastTextMarkArea()V

    .line 722
    :cond_2
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_TEXTMARK_REVERSE()Z

    move-result v4

    if-ne v4, v7, :cond_3

    .line 724
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->drawSelection(Landroid/graphics/Canvas;)V

    .line 727
    :cond_3
    invoke-super {p0, p1}, Landroid/widget/EditText;->onDraw(Landroid/graphics/Canvas;)V

    .line 729
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    instance-of v4, v4, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    if-eqz v4, :cond_12

    .line 730
    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableDrawScroll:Z

    if-nez v4, :cond_d

    .line 731
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->stopFling()V

    .line 732
    iput-boolean v7, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableDrawScroll:Z

    .line 751
    :cond_4
    :goto_1
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v4, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getViewMode()I

    move-result v4

    if-eq v4, v7, :cond_5

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v4, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getViewMode()I

    move-result v4

    if-ne v4, v8, :cond_6

    .line 754
    :cond_5
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bShowCursor:Z

    .line 755
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bShowSoftKeyboard:Z

    .line 758
    :cond_6
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_TEXTMARK_REVERSE()Z

    move-result v4

    if-nez v4, :cond_7

    .line 759
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->drawSelection(Landroid/graphics/Canvas;)V

    .line 761
    :cond_7
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v4, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v2

    .line 763
    .local v2, "showMode":I
    if-eq v2, v8, :cond_8

    if-eq v2, v9, :cond_8

    .line 764
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->drawSelectBound(Landroid/graphics/Canvas;)V

    .line 766
    :cond_8
    if-ne v2, v7, :cond_9

    .line 767
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->drawCursor(Landroid/graphics/Canvas;)V

    .line 768
    :cond_9
    const/4 v4, 0x4

    if-ne v2, v4, :cond_b

    .line 769
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isShowSoftKey()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 770
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hideSoftKeyboard()Z

    .line 771
    :cond_a
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->drawTTSSelection(Landroid/graphics/Canvas;)V

    .line 775
    :cond_b
    return-void

    .end local v1    # "oMWDnD":Lcom/infraware/common/multiwindow/MWDnDOperator;
    .end local v2    # "showMode":I
    .end local v3    # "tea":Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    :cond_c
    move-object v3, v1

    .line 715
    goto :goto_0

    .line 733
    .restart local v1    # "oMWDnD":Lcom/infraware/common/multiwindow/MWDnDOperator;
    .restart local v3    # "tea":Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    :cond_d
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_Scroller:Landroid/widget/Scroller;

    invoke-virtual {v4}, Landroid/widget/Scroller;->isFinished()Z

    move-result v4

    if-nez v4, :cond_4

    .line 735
    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bPreBlockLoad:Z

    if-nez v4, :cond_e

    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bNextBlockLoad:Z

    if-eqz v4, :cond_f

    :cond_e
    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bFling:Z

    if-eqz v4, :cond_f

    .line 736
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_Scroller:Landroid/widget/Scroller;

    invoke-virtual {v4}, Landroid/widget/Scroller;->getDuration()I

    move-result v4

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_Scroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->timePassed()I

    move-result v5

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nVelocityY:I

    mul-int/2addr v4, v5

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_Scroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->getDuration()I

    move-result v5

    div-int v0, v4, v5

    .line 737
    .local v0, "nVelocityY":I
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_Scroller:Landroid/widget/Scroller;

    iget v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollY:I

    invoke-virtual {v4, v6, v5, v6, v6}, Landroid/widget/Scroller;->startScroll(IIII)V

    .line 738
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->flickProcess(I)V

    .line 740
    .end local v0    # "nVelocityY":I
    :cond_f
    iget v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nVelocityY:I

    if-gez v4, :cond_10

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_Scroller:Landroid/widget/Scroller;

    invoke-virtual {v4}, Landroid/widget/Scroller;->getCurrY()I

    move-result v4

    iget v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollY:I

    sub-int/2addr v4, v5

    if-ltz v4, :cond_11

    :cond_10
    iget v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nVelocityY:I

    if-lez v4, :cond_4

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_Scroller:Landroid/widget/Scroller;

    invoke-virtual {v4}, Landroid/widget/Scroller;->getCurrY()I

    move-result v4

    iget v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollY:I

    sub-int/2addr v4, v5

    if-lez v4, :cond_4

    .line 742
    :cond_11
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_Scroller:Landroid/widget/Scroller;

    invoke-virtual {v4}, Landroid/widget/Scroller;->getCurrY()I

    move-result v4

    iget v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollY:I

    invoke-virtual {p0, v6, v4, v6, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->onScrollChanged(IIII)V

    goto/16 :goto_1

    .line 748
    :cond_12
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bFling:Z

    goto/16 :goto_1
.end method

.method protected onFocusLost()V
    .locals 0

    .prologue
    .line 4304
    invoke-super {p0}, Landroid/widget/EditText;->onFocusLost()V

    .line 4306
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->dismissPopup()V

    .line 4307
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 11
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v10, 0x14

    const/4 v9, 0x4

    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 600
    const/16 v0, 0x1000

    .line 601
    .local v0, "METAKEY_CTRL_ON":I
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v7

    and-int/lit16 v7, v7, 0x1000

    const/16 v8, 0x1000

    if-ne v7, v8, :cond_1

    move v2, v6

    .line 604
    .local v2, "isCtrl":Z
    :goto_0
    const/16 v4, 0x14

    .line 606
    .local v4, "scrollY":I
    sparse-switch p1, :sswitch_data_0

    .line 688
    :cond_0
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v5

    :goto_2
    return v5

    .end local v2    # "isCtrl":Z
    .end local v4    # "scrollY":I
    :cond_1
    move v2, v5

    .line 601
    goto :goto_0

    .restart local v2    # "isCtrl":Z
    .restart local v4    # "scrollY":I
    :sswitch_0
    move v5, v6

    .line 611
    goto :goto_2

    .line 613
    :sswitch_1
    new-instance v6, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;

    invoke-direct {v6, p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;->start()V

    goto :goto_2

    .line 619
    :sswitch_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v1

    .line 620
    .local v1, "activiy":Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->isFindReplaceMode()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v5

    if-eq v5, v9, :cond_0

    .line 622
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    new-instance v6, Lcom/infraware/polarisoffice5/text/control/EditCtrl$3;

    invoke-direct {v6, p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$3;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V

    const-wide/16 v7, 0xc8

    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 633
    .end local v1    # "activiy":Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    :sswitch_3
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->dismissPopup()V

    .line 635
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getScrollY()I

    move-result v7

    if-lt v7, v10, :cond_2

    .line 636
    const/16 v7, -0x14

    invoke-virtual {p0, v5, v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->scrollBy(II)V

    .line 640
    :goto_3
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDeviceId()I

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v7

    if-ne v7, v6, :cond_3

    move v3, v6

    .line 642
    .local v3, "isInnerLongKey":Z
    :goto_4
    if-ne v2, v6, :cond_4

    if-ne v3, v6, :cond_4

    .line 643
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v5

    invoke-virtual {v5, v6}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setActionbarFocus(Z)V

    move v5, v6

    .line 644
    goto :goto_2

    .line 638
    .end local v3    # "isInnerLongKey":Z
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setActionbarFocus(Z)V

    goto :goto_3

    :cond_3
    move v3, v5

    .line 640
    goto :goto_4

    .line 648
    .restart local v3    # "isInnerLongKey":Z
    :cond_4
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v5, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v5

    const/4 v7, 0x2

    if-ne v5, v7, :cond_5

    .line 649
    if-ne v2, v6, :cond_5

    .line 650
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v5

    invoke-virtual {v5, v6}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setActionbarFocus(Z)V

    move v5, v6

    .line 651
    goto :goto_2

    .line 657
    .end local v3    # "isInnerLongKey":Z
    :cond_5
    :sswitch_4
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->dismissPopup()V

    .line 658
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getCurBlock()I

    move-result v5

    if-lez v5, :cond_0

    .line 659
    invoke-virtual {p0, v6, v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->loadPreBlock(ZZ)V

    goto/16 :goto_1

    .line 663
    :sswitch_5
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->dismissPopup()V

    .line 665
    invoke-virtual {p0, v5, v10}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->scrollBy(II)V

    .line 667
    :sswitch_6
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->dismissPopup()V

    .line 668
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v7

    if-ne v7, v9, :cond_6

    .line 669
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setFocusable(Z)V

    .line 670
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setToolbarFocus()V

    goto/16 :goto_1

    .line 672
    :cond_6
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v5

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->length()I

    move-result v7

    if-ne v5, v7, :cond_0

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getCurBlock()I

    move-result v5

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    if-eq v5, v7, :cond_0

    .line 673
    invoke-virtual {p0, v6, v6}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->loadNextBlock(ZZ)V

    goto/16 :goto_1

    :sswitch_7
    move v5, v6

    .line 678
    goto/16 :goto_2

    .line 682
    :sswitch_8
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setActionbarFocus(Z)V

    goto/16 :goto_2

    .line 606
    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_3
        0x14 -> :sswitch_5
        0x15 -> :sswitch_4
        0x16 -> :sswitch_6
        0x39 -> :sswitch_8
        0x3a -> :sswitch_8
        0x3d -> :sswitch_7
        0x4f -> :sswitch_0
        0x52 -> :sswitch_2
        0x7e -> :sswitch_0
        0x7f -> :sswitch_0
        0xdd -> :sswitch_1
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x1

    .line 577
    sparse-switch p1, :sswitch_data_0

    .line 594
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 583
    :sswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 585
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onPlayPauseTTSButton()V

    move v0, v1

    .line 586
    goto :goto_0

    .line 590
    :sswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v0

    if-eq v0, v2, :cond_1

    .line 591
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setShowMode(I)V

    :cond_1
    move v0, v1

    .line 592
    goto :goto_0

    .line 577
    nop

    :sswitch_data_0
    .sparse-switch
        0x4f -> :sswitch_0
        0x54 -> :sswitch_1
        0x7e -> :sswitch_0
        0x7f -> :sswitch_0
    .end sparse-switch
.end method

.method public onPreDraw()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1188
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isPreDraw:Z

    .line 1189
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isSelectionOverBound()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1192
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/widget/EditText;->onPreDraw()Z

    move-result v0

    goto :goto_0
.end method

.method protected onScrollChanged(IIII)V
    .locals 3
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "oldl"    # I
    .param p4, "oldt"    # I

    .prologue
    const/4 v2, 0x0

    .line 1197
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableScroll:Z

    if-nez v0, :cond_0

    .line 1198
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableScroll:Z

    .line 1220
    :goto_0
    return-void

    .line 1202
    :cond_0
    iput p2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollY:I

    .line 1203
    sub-int v0, p2, p4

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->changeBlock(I)V

    .line 1205
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    instance-of v0, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLoadingProgress()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1206
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setTitleText()V

    .line 1207
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollY:I

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->adjustEditScroll(I)V

    .line 1208
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 1209
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->actionBarVisible(Z)V

    .line 1211
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    .line 1212
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setScrollThumbTimer()V

    .line 1214
    :cond_2
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isPreDraw:Z

    if-nez v0, :cond_4

    .line 1215
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getSvEdit()Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 1219
    :cond_3
    :goto_1
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastpopupUpdate()V

    goto :goto_0

    .line 1217
    :cond_4
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isPreDraw:Z

    goto :goto_1
.end method

.method protected onSelectionChanged(II)V
    .locals 1
    .param p1, "selStart"    # I
    .param p2, "selEnd"    # I

    .prologue
    .line 701
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->invalidate()V

    .line 703
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelectionChanged(Z)V

    .line 704
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 23
    .param p1, "evt"    # Landroid/view/MotionEvent;

    .prologue
    .line 357
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v12

    .line 358
    .local v12, "pointerCount":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v4

    .line 360
    .local v4, "actionMasked":I
    const/4 v11, 0x0

    .line 361
    .local v11, "isTTSMode":Z
    const/4 v15, 0x0

    .line 362
    .local v15, "textEditor":Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    const/16 v17, 0x0

    .line 364
    .local v17, "ttsManager":Lcom/infraware/polarisoffice5/text/manager/TTSManager;
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isLongTapEvent()Z

    move-result v18

    if-nez v18, :cond_0

    .line 365
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->removePopupCallback()V

    .line 368
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    instance-of v0, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move/from16 v18, v0

    if-eqz v18, :cond_2

    .line 369
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    move-object/from16 v18, v0

    check-cast v18, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual/range {v18 .. v18}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v18

    const/16 v19, 0x4

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_2

    .line 370
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v18

    if-eqz v18, :cond_3

    .line 371
    const/4 v11, 0x1

    .line 372
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    .end local v15    # "textEditor":Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    check-cast v15, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .line 373
    .restart local v15    # "textEditor":Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-virtual {v15}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getTTSManager()Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    move-result-object v17

    .line 375
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->isTTSToolbarEnable()Z

    move-result v18

    if-eqz v18, :cond_1

    .line 376
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v18

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setTTSToolbarEnable(Z)V

    .line 377
    :cond_1
    invoke-virtual/range {v17 .. v17}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->isSpeaking()Z

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_2

    .line 378
    invoke-virtual/range {v17 .. v17}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->pause()V

    .line 384
    :cond_2
    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setFocusableInTouchMode(Z)V

    .line 387
    const/16 v18, 0x2

    move/from16 v0, v18

    if-ne v12, v0, :cond_6

    const/16 v18, 0x2

    move/from16 v0, v18

    if-ne v4, v0, :cond_6

    if-nez v11, :cond_6

    .line 388
    const/16 v18, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v18

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v19

    sub-float v8, v18, v19

    .line 389
    .local v8, "distX":F
    const/16 v18, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v18

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v19

    sub-float v9, v18, v19

    .line 390
    .local v9, "distY":F
    mul-float v18, v8, v8

    mul-float v19, v9, v9

    add-float v18, v18, v19

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-float v10, v0

    .line 391
    .local v10, "distance":F
    const/4 v6, 0x0

    .line 393
    .local v6, "diff":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_LastDistance:F

    move/from16 v18, v0

    const/16 v19, 0x1

    cmpl-float v18, v18, v19

    if-nez v18, :cond_4

    .line 394
    move-object/from16 v0, p0

    iput v10, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_LastDistance:F

    .line 400
    :goto_0
    const/16 v18, 0x0

    cmpl-float v18, v6, v18

    if-lez v18, :cond_5

    .line 401
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bPinchScaleUp:Z

    .line 405
    :goto_1
    const/4 v14, 0x0

    .line 506
    .end local v6    # "diff":F
    .end local v8    # "distX":F
    .end local v9    # "distY":F
    .end local v10    # "distance":F
    :goto_2
    return v14

    .line 380
    :cond_3
    const/4 v14, 0x1

    goto :goto_2

    .line 396
    .restart local v6    # "diff":F
    .restart local v8    # "distX":F
    .restart local v9    # "distY":F
    .restart local v10    # "distance":F
    :cond_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_LastDistance:F

    move/from16 v18, v0

    sub-float v6, v10, v18

    .line 397
    move-object/from16 v0, p0

    iput v10, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_LastDistance:F

    goto :goto_0

    .line 403
    :cond_5
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bPinchScaleUp:Z

    goto :goto_1

    .line 406
    .end local v6    # "diff":F
    .end local v8    # "distX":F
    .end local v9    # "distY":F
    .end local v10    # "distance":F
    :cond_6
    const/16 v18, 0x6

    move/from16 v0, v18

    if-ne v4, v0, :cond_7

    .line 407
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bPinchScaleUp:Z

    move/from16 v18, v0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->scaleEvent(Z)Z

    move-result v14

    .line 409
    .local v14, "result":Z
    goto :goto_2

    .line 410
    .end local v14    # "result":Z
    :cond_7
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v18

    if-nez v18, :cond_f

    .line 412
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v18

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_8

    .line 413
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hideSoftKeyboard()Z

    .line 416
    :cond_8
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v0, v11, :cond_9

    .line 417
    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setTTSSelection(II)V

    .line 419
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v18

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v19

    sub-int v18, v18, v19

    if-lez v18, :cond_a

    .line 420
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v18

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v19

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isFitTouchSelectionStart(II)Z

    move-result v18

    if-eqz v18, :cond_a

    .line 421
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelStartDown:Z

    .line 424
    :cond_a
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v18

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v19

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isFitTouchSelectionEnd(II)Z

    move-result v18

    if-eqz v18, :cond_b

    .line 425
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelEndDown:Z

    .line 428
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_Scroller:Landroid/widget/Scroller;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/Scroller;->isFinished()Z

    move-result v18

    if-nez v18, :cond_c

    .line 429
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bFlickbStart:Z

    .line 430
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_Scroller:Landroid/widget/Scroller;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/Scroller;->getCurrY()I

    move-result v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nFlickStopPos:I

    .line 491
    :cond_c
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_GestureDetector:Landroid/view/GestureDetector;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    .line 492
    .local v5, "bRet":Z
    if-nez v5, :cond_d

    .line 493
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelStartDown:Z

    move/from16 v18, v0

    if-nez v18, :cond_16

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelEndDown:Z

    move/from16 v18, v0

    if-nez v18, :cond_16

    .line 495
    invoke-super/range {p0 .. p1}, Landroid/widget/EditText;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    .line 503
    :cond_d
    :goto_4
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_e

    .line 504
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelectionChanged(Z)V

    :cond_e
    move v14, v5

    .line 506
    goto/16 :goto_2

    .line 434
    .end local v5    # "bRet":Z
    :cond_f
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_14

    .line 435
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v0, v11, :cond_11

    .line 436
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getTTSManager()Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->getTTSMode()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_10

    .line 437
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getTTSManager()Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    move-result-object v18

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->setTTSMode(I)V

    .line 439
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getCurrentTopCaret()I

    move-result v16

    .line 440
    .local v16, "topCaretPos":I
    move-object/from16 v0, p0

    move/from16 v1, v16

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setTTSSelection(II)V

    .line 442
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f07030b

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    move/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->textReadAndPlay(Lcom/infraware/polarisoffice5/text/manager/TTSManager;Ljava/lang/String;Z)V

    .line 443
    const/4 v14, 0x1

    goto/16 :goto_2

    .line 446
    .end local v16    # "topCaretPos":I
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isLongTapEvent()Z

    move-result v18

    if-eqz v18, :cond_12

    .line 447
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setIsLongTapEvent(Z)V

    .line 448
    const/4 v14, 0x1

    goto/16 :goto_2

    .line 451
    :cond_12
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelStartDown:Z

    .line 452
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelEndDown:Z

    .line 453
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->invalidate()V

    .line 456
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bIsScrolling:Z

    move/from16 v18, v0

    if-eqz v18, :cond_c

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v18

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v19

    sub-int v18, v18, v19

    if-eqz v18, :cond_c

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isSelectionChanged()Z

    move-result v18

    if-eqz v18, :cond_c

    .line 459
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bIsScrolling:Z

    .line 461
    new-instance v13, Landroid/graphics/Rect;

    invoke-direct {v13}, Landroid/graphics/Rect;-><init>()V

    .line 462
    .local v13, "r":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 465
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_drawSelectionTop:I

    move/from16 v18, v0

    iget v0, v13, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-lt v0, v1, :cond_13

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_drawSelectionBottom:I

    move/from16 v18, v0

    iget v0, v13, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-gt v0, v1, :cond_13

    .line 466
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    move-object/from16 v18, v0

    check-cast v18, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual/range {v18 .. v18}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_c

    .line 467
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v18

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v19

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->showToastPopup(FF)V

    goto/16 :goto_3

    .line 470
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    move-object/from16 v18, v0

    check-cast v18, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual/range {v18 .. v18}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_c

    .line 471
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getDisplaySize()[I

    move-result-object v7

    .line 472
    .local v7, "displaysize":[I
    const/16 v18, 0x0

    aget v18, v7, v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    const/16 v19, 0x1

    aget v19, v7, v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->showToastPopup(FF)V

    goto/16 :goto_3

    .line 478
    .end local v7    # "displaysize":[I
    .end local v13    # "r":Landroid/graphics/Rect;
    :cond_14
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v18

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_c

    .line 479
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_Scroller:Landroid/widget/Scroller;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/Scroller;->isFinished()Z

    move-result v18

    if-eqz v18, :cond_15

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bFlickbStart:Z

    move/from16 v18, v0

    if-eqz v18, :cond_15

    .line 480
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_Scroller:Landroid/widget/Scroller;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nFlickStopPos:I

    move/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    invoke-virtual/range {v18 .. v22}, Landroid/widget/Scroller;->startScroll(IIII)V

    .line 481
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bFlickbStart:Z

    .line 484
    :cond_15
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->dismissPopup()V

    goto/16 :goto_3

    .line 500
    .restart local v5    # "bRet":Z
    :cond_16
    const/4 v5, 0x1

    goto/16 :goto_4
.end method

.method public prepareEnterBookmarkActivity()[I
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 4314
    const/4 v8, 0x2

    new-array v0, v8, [I

    .line 4315
    .local v0, "caretPos":[I
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockOffsetList()Ljava/util/LinkedList;

    move-result-object v3

    .line 4317
    .local v3, "offsetList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/infraware/polarisoffice5/text/manager/TextBlock;>;"
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectBegin()I

    move-result v9

    invoke-virtual {v8, v10, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 4318
    .local v6, "stringStart":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getSelectEnd()I

    move-result v9

    invoke-virtual {v8, v10, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 4320
    .local v4, "stringEnd":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getCurCharset()Ljava/lang/String;

    move-result-object v1

    .line 4322
    .local v1, "charsetname":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v6, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v7

    .line 4323
    .local v7, "stringStartByte":[B
    invoke-virtual {v4, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v5

    .line 4326
    .local v5, "stringEndByte":[B
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getCurBlock()I

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->isSelectAllSupport()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 4327
    :cond_0
    const/4 v8, 0x0

    array-length v9, v7

    aput v9, v0, v8

    .line 4328
    const/4 v8, 0x1

    array-length v9, v5

    aput v9, v0, v8

    .line 4337
    .end local v5    # "stringEndByte":[B
    .end local v7    # "stringStartByte":[B
    :goto_0
    return-object v0

    .line 4330
    .restart local v5    # "stringEndByte":[B
    .restart local v7    # "stringStartByte":[B
    :cond_1
    const/4 v9, 0x0

    array-length v10, v7

    iget v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v3, v8}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    iget v8, v8, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nStartOffset:I

    add-int/2addr v8, v10

    aput v8, v0, v9

    .line 4331
    const/4 v9, 0x1

    array-length v10, v5

    iget v8, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v3, v8}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    iget v8, v8, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nStartOffset:I

    add-int/2addr v8, v10

    aput v8, v0, v9
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 4333
    .end local v5    # "stringEndByte":[B
    .end local v7    # "stringStartByte":[B
    :catch_0
    move-exception v2

    .line 4334
    .local v2, "e":Ljava/io/UnsupportedEncodingException;
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public redo()I
    .locals 1

    .prologue
    .line 1697
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_UndoRedoMgr:Lcom/infraware/polarisoffice5/text/manager/CommandManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->redo()I

    move-result v0

    return v0
.end method

.method public removePopupCallback()V
    .locals 2

    .prologue
    .line 2020
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_toastPopupRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasCallbacks(Ljava/lang/Runnable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2021
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_toastPopupRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2022
    :cond_0
    return-void
.end method

.method public replaceBuffer(IIILjava/lang/String;)V
    .locals 6
    .param p1, "nBlock"    # I
    .param p2, "nStartOffset"    # I
    .param p3, "nEndOffset"    # I
    .param p4, "strReplace"    # Ljava/lang/String;

    .prologue
    .line 3120
    if-ne p2, p3, :cond_0

    .line 3137
    :goto_0
    return-void

    .line 3123
    :cond_0
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    if-ge p2, v0, :cond_1

    .line 3124
    add-int/lit8 p1, p1, -0x1

    .line 3134
    :goto_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_UndoRedoMgr:Lcom/infraware/polarisoffice5/text/manager/CommandManager;

    sub-int v4, p3, p2

    const/4 v5, 0x0

    move v1, p1

    move-object v2, p4

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->replaceCommand(ILjava/lang/String;IIZ)V

    .line 3136
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v0, p4, p1, p2, p3}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->replaceMemBuffer(Ljava/lang/String;III)V

    goto :goto_0

    .line 3125
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    if-le p2, v0, :cond_2

    .line 3126
    add-int/lit8 p1, p1, 0x1

    .line 3127
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    sub-int/2addr p2, v0

    .line 3128
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockEndOffset:I

    sub-int/2addr p3, v0

    goto :goto_1

    .line 3130
    :cond_2
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    sub-int/2addr p2, v0

    .line 3131
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    sub-int/2addr p3, v0

    goto :goto_1
.end method

.method public replaceSubBuffer(IIILjava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "nBlock"    # I
    .param p2, "nStartOffset"    # I
    .param p3, "nEndOffset"    # I
    .param p4, "strText"    # Ljava/lang/String;
    .param p5, "strReplace"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 3226
    if-ne p2, p3, :cond_0

    .line 3236
    :goto_0
    return-void

    .line 3229
    :cond_0
    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bChanged:Z

    .line 3232
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_UndoRedoMgr:Lcom/infraware/polarisoffice5/text/manager/CommandManager;

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v4

    move v1, p1

    move-object v2, p4

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->addCommand(ILjava/lang/String;IIZ)V

    .line 3233
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_UndoRedoMgr:Lcom/infraware/polarisoffice5/text/manager/CommandManager;

    invoke-virtual {p5}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x0

    move v1, p1

    move-object v2, p5

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->addCommand(ILjava/lang/String;IIZ)V

    .line 3235
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v0, p5, p1, p2, p3}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->replaceMemBuffer(Ljava/lang/String;III)V

    goto :goto_0
.end method

.method public saveProcess(Ljava/lang/String;ZZ)V
    .locals 1
    .param p1, "strFileName"    # Ljava/lang/String;
    .param p2, "bSave"    # Z
    .param p3, "bDeleteBuf"    # Z

    .prologue
    .line 1395
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->processSave(Ljava/lang/String;ZZ)V

    .line 1396
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_UndoRedoMgr:Lcom/infraware/polarisoffice5/text/manager/CommandManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->clearCommand()V

    .line 1397
    return-void
.end method

.method public saveTextInfo(Ljava/lang/String;)V
    .locals 6
    .param p1, "strFilePath"    # Ljava/lang/String;

    .prologue
    .line 1373
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v3

    const-string/jumbo v4, "textinfo"

    const v5, 0x8000

    invoke-virtual {v3, v4, v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1374
    .local v1, "sP":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1378
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v3, "notexist"

    invoke-interface {v1, p1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1380
    .local v2, "temp":Ljava/lang/String;
    const-string/jumbo v3, "notexist"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1381
    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1382
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1384
    :cond_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setLastDisplayPosition()V

    .line 1391
    return-void
.end method

.method public scaleEvent(Z)Z
    .locals 4
    .param p1, "flag"    # Z

    .prologue
    const/4 v3, 0x0

    .line 323
    if-eqz p1, :cond_0

    .line 324
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_fontsizeIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_fontsizeIndex:I

    .line 329
    :goto_0
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_fontsizeIndex:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_fontSizePool:[I

    array-length v1, v1

    if-ne v0, v1, :cond_1

    .line 330
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_fontSizePool:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_fontsizeIndex:I

    .line 351
    :goto_1
    return v3

    .line 326
    :cond_0
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_fontsizeIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_fontsizeIndex:I

    goto :goto_0

    .line 333
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_fontsizeIndex:I

    if-gez v0, :cond_2

    .line 334
    iput v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_fontsizeIndex:I

    goto :goto_1

    .line 338
    :cond_2
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bNowChangeFontsize:Z

    if-nez v0, :cond_4

    .line 339
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_fontSizePool:[I

    iget v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_fontsizeIndex:I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setFontSize(I)V

    .line 342
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    instance-of v0, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    if-eqz v0, :cond_3

    .line 343
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_fontSizePool:[I

    iget v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_fontsizeIndex:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onFontSizeChanged(I)V

    .line 346
    :cond_3
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->callOnClick()Z

    .line 349
    :cond_4
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_LastDistance:F

    goto :goto_1
.end method

.method public scrollByThumb(II)V
    .locals 19
    .param p1, "nThumbPos"    # I
    .param p2, "nThumbRange"    # I

    .prologue
    .line 3464
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v17

    if-nez v17, :cond_0

    .line 3516
    :goto_0
    return-void

    .line 3466
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->stopFling()V

    .line 3467
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bFling:Z

    .line 3471
    move/from16 v0, p1

    int-to-float v0, v0

    move/from16 v17, v0

    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v18, v0

    div-float v16, v17, v18

    .line 3474
    .local v16, "thumbRatio":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getTotalTextLen(Z)I

    move-result v12

    .line 3475
    .local v12, "nTotalLen":I
    int-to-float v0, v12

    move/from16 v17, v0

    mul-float v17, v17, v16

    move/from16 v0, v17

    float-to-int v13, v0

    .line 3477
    .local v13, "nTotalOffset":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockIndexFromOffset(I)I

    move-result v3

    .line 3480
    .local v3, "nBlock":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-eq v3, v0, :cond_1

    .line 3481
    const/16 v17, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->loadBlock(IZ)V

    .line 3484
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->calcScrollHeight()V

    .line 3486
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollHeight:I

    move/from16 v17, v0

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getHeight()I

    move-result v18

    sub-int v17, v17, v18

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    mul-float v17, v17, v16

    move/from16 v0, v17

    float-to-int v14, v0

    .line 3487
    .local v14, "nTotalScrollPos":I
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLineHeight()I

    move-result v17

    add-int/lit8 v6, v17, -0x1

    .line 3488
    .local v6, "nLineHeight":I
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v17

    const/16 v18, 0xbb8

    invoke-virtual/range {v17 .. v18}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v17

    mul-int v5, v17, v6

    .line 3490
    .local v5, "nBlockHeight":I
    add-int/lit8 v17, v3, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockOffsetInFile(I)I

    move-result v9

    .line 3491
    .local v9, "nPreBlockOffset":I
    div-int/lit16 v4, v9, 0xbb8

    .line 3492
    .local v4, "nBlockCount":I
    rem-int/lit16 v7, v9, 0xbb8

    .line 3493
    .local v7, "nMod":I
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v17

    mul-int v8, v17, v6

    .line 3494
    .local v8, "nModOffset":I
    mul-int v17, v5, v4

    add-int v10, v17, v8

    .line 3497
    .local v10, "nPreHeight":I
    sub-int v11, v14, v10

    .line 3498
    .local v11, "nScrollPos":I
    if-gez v11, :cond_2

    .line 3499
    const/4 v11, 0x0

    .line 3501
    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    move/from16 v17, v0

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCount()I

    move-result v18

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_3

    const/high16 v17, 0x3f800000    # 1.0f

    cmpl-float v17, v16, v17

    if-nez v17, :cond_3

    .line 3503
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTotalPaddingTop()I

    move-result v17

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTotalPaddingBottom()I

    move-result v18

    add-int v15, v17, v18

    .line 3504
    .local v15, "padding":I
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v2

    .line 3506
    .local v2, "layout":Landroid/text/Layout;
    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v17

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getHeight()I

    move-result v18

    sub-int v18, v18, v15

    sub-int v11, v17, v18

    .line 3510
    .end local v2    # "layout":Landroid/text/Layout;
    .end local v15    # "padding":I
    :cond_3
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableScroll:Z

    .line 3512
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1, v11}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->scrollTo(II)V

    .line 3513
    move-object/from16 v0, p0

    iput v11, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nScrollY:I

    .line 3514
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->calcScrollPos(I)V

    .line 3515
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableDrawScroll:Z

    goto/16 :goto_0
.end method

.method public selDownChange()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3049
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelStartDown:Z

    if-eqz v0, :cond_0

    .line 3050
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelStartDown:Z

    .line 3051
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelEndDown:Z

    .line 3056
    :goto_0
    return-void

    .line 3053
    :cond_0
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelStartDown:Z

    .line 3054
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelEndDown:Z

    goto :goto_0
.end method

.method public sendDictionaryMessage(Ljava/lang/String;)V
    .locals 1
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    .line 3960
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v0, :cond_0

    .line 3962
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    instance-of v0, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    if-eqz v0, :cond_0

    .line 3963
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->sendDictionaryMessage(Ljava/lang/String;)V

    .line 3965
    :cond_0
    return-void
.end method

.method public setActivityForDicSearch(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;
    .locals 1
    .param p1, "activity"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 3946
    new-instance v0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-direct {v0, p1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    .line 3947
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    return-object v0
.end method

.method public setBackgroundTheme(I)V
    .locals 4
    .param p1, "theme"    # I

    .prologue
    const/16 v3, 0x19

    const/4 v2, 0x0

    .line 1665
    packed-switch p1, :pswitch_data_0

    .line 1685
    :goto_0
    invoke-virtual {p0, v3, v2, v3, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setPadding(IIII)V

    .line 1686
    return-void

    .line 1667
    :pswitch_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_theme:I

    .line 1668
    const v0, 0x7f020278

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setBackgroundResource(I)V

    .line 1669
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0501af

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setFontColor(I)V

    goto :goto_0

    .line 1672
    :pswitch_1
    iput v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_theme:I

    .line 1673
    const v0, 0x7f020277

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setBackgroundResource(I)V

    .line 1674
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0501b0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setFontColor(I)V

    goto :goto_0

    .line 1677
    :pswitch_2
    const/4 v0, 0x2

    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_theme:I

    .line 1678
    const v0, 0x7f020279

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setBackgroundResource(I)V

    .line 1679
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0501b1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setFontColor(I)V

    goto :goto_0

    .line 1665
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public setBufferManagerReset()V
    .locals 6

    .prologue
    .line 4020
    const-string/jumbo v1, "UTF-16LE"

    .line 4021
    .local v1, "strCharset":Ljava/lang/String;
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v2

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v3

    const/16 v4, 0x17

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, Lcom/infraware/common/config/RuntimeConfig;->getIntPreference(Landroid/content/Context;II)I

    move-result v0

    .line 4022
    .local v0, "nEncoding":I
    if-eqz v0, :cond_0

    .line 4023
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getCharsetList()Ljava/util/ArrayList;

    move-result-object v2

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "strCharset":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 4025
    .restart local v1    # "strCharset":Ljava/lang/String;
    :cond_0
    new-instance v2, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    invoke-direct {v2, v3, v1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    .line 4026
    return-void
.end method

.method public setChanged(Z)V
    .locals 2
    .param p1, "bChanged"    # Z

    .prologue
    .line 1277
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bChanged:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hasAnyUndoRedo()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1289
    :cond_0
    :goto_0
    return-void

    .line 1280
    :cond_1
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bChanged:Z

    .line 1281
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    instance-of v0, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    if-eqz v0, :cond_0

    .line 1282
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getShowMode()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 1286
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setShowMode(I)V

    goto :goto_0
.end method

.method public setCurBlock(I)V
    .locals 0
    .param p1, "nCurBlock"    # I

    .prologue
    .line 1404
    iput p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I

    .line 1405
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setCurBlockBound()V

    .line 1406
    return-void
.end method

.method public setFlingFlag(Z)V
    .locals 0
    .param p1, "bFlag"    # Z

    .prologue
    .line 4287
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bFling:Z

    .line 4288
    return-void
.end method

.method public setFontColor(I)V
    .locals 0
    .param p1, "textColor"    # I

    .prologue
    .line 1661
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setTextColor(I)V

    .line 1662
    return-void
.end method

.method public setFontPoolSync(I)V
    .locals 2
    .param p1, "nFontSize"    # I

    .prologue
    .line 288
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_fontSizePool:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 289
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_fontSizePool:[I

    aget v1, v1, v0

    if-ne p1, v1, :cond_1

    .line 290
    iput v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_fontsizeIndex:I

    .line 294
    :cond_0
    return-void

    .line 288
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setFontSize(I)V
    .locals 2
    .param p1, "nFontSize"    # I

    .prologue
    const/4 v1, 0x1

    .line 1627
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bNowChangeFontsize:Z

    .line 1630
    mul-int/lit8 v0, p1, 0x2

    int-to-float v0, v0

    invoke-virtual {p0, v1, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setTextSize(IF)V

    .line 1632
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_handler:Landroid/os/Handler;

    new-instance v1, Lcom/infraware/polarisoffice5/text/control/EditCtrl$4;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$4;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1639
    return-void
.end method

.method public setIsLongTapEvent(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 545
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bIsLongTapEvent:Z

    .line 546
    return-void
.end method

.method public setIsNewFile(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 3936
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->setIsNewFile(Z)V

    .line 3937
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    const-string/jumbo v1, "UTF-16LE"

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->setLoadingEncoding(Ljava/lang/String;)V

    .line 3938
    return-void
.end method

.method public setIsScrolling(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 553
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bIsScrolling:Z

    .line 554
    return-void
.end method

.method public setLoadingProgress(Z)V
    .locals 0
    .param p1, "bLoadingProgress"    # Z

    .prologue
    .line 3899
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bLoadingProgress:Z

    .line 3900
    return-void
.end method

.method public setOpenDialog(Ljava/lang/String;)V
    .locals 4
    .param p1, "strFilePath"    # Ljava/lang/String;

    .prologue
    .line 3610
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_pd:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    .line 3611
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_pd:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3612
    const-string/jumbo v1, "/"

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 3613
    .local v0, "displayPath":Ljava/lang/String;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_pd:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 3614
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_pd:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0701bf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 3618
    .end local v0    # "displayPath":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public setReadInfo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "strFilePath"    # Ljava/lang/String;
    .param p2, "strEncoding"    # Ljava/lang/String;

    .prologue
    .line 1306
    const/4 v0, 0x0

    .line 1308
    .local v0, "caret":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v5

    const-string/jumbo v6, "textinfo"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 1311
    .local v2, "sP":Landroid/content/SharedPreferences;
    const-string/jumbo v5, "notexist"

    invoke-interface {v2, p1, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1313
    .local v4, "temp":Ljava/lang/String;
    new-instance v3, Ljava/util/StringTokenizer;

    const-string/jumbo v5, "|"

    invoke-direct {v3, v4, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1315
    .local v3, "st":Ljava/util/StringTokenizer;
    const-string/jumbo v5, "notexist"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1317
    :try_start_0
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1318
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1326
    :goto_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v5, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->isASCFile()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1327
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    const-string/jumbo v6, "UTF-8"

    invoke-virtual {v5, v6}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->setLoadingEncoding(Ljava/lang/String;)V

    .line 1331
    :goto_1
    new-instance v5, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    invoke-direct {v5, p0, v0, p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;ILjava/lang/String;)V

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->start()V

    .line 1332
    return-void

    .line 1320
    :catch_0
    move-exception v1

    .line 1321
    .local v1, "e":Ljava/lang/NumberFormatException;
    const/4 v0, 0x0

    .line 1322
    goto :goto_0

    .line 1324
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1329
    :cond_1
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v5, p2}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->setLoadingEncoding(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public setSelectWord(II)V
    .locals 8
    .param p1, "nPx"    # I
    .param p2, "nPy"    # I

    .prologue
    const/4 v7, -0x1

    .line 2947
    invoke-virtual {p0, p1, p2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTouchOffset(II)I

    move-result v2

    .line 2949
    .local v2, "nPos":I
    const/4 v3, 0x0

    .local v3, "nStart":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v0

    .line 2950
    .local v0, "nEnd":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "\n"

    add-int/lit8 v6, v2, -0x1

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;I)I

    move-result v1

    .line 2951
    .local v1, "nEnterPos":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, " "

    add-int/lit8 v6, v2, -0x1

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;I)I

    move-result v3

    .line 2952
    if-le v1, v3, :cond_0

    .line 2953
    move v3, v1

    .line 2955
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "\n"

    invoke-virtual {v4, v5, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    .line 2956
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 2957
    if-ne v0, v7, :cond_1

    .line 2958
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v0

    .line 2959
    :cond_1
    if-ge v1, v0, :cond_2

    if-eq v1, v7, :cond_2

    .line 2960
    move v0, v1

    .line 2962
    :cond_2
    add-int/lit8 v4, v3, 0x1

    invoke-virtual {p0, v4, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(II)V

    .line 2963
    return-void
.end method

.method public setSelectionChanged(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 692
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bSelectionchanged:Z

    .line 693
    return-void
.end method

.method public setShowCursor(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 3108
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bShowCursor:Z

    .line 3109
    return-void
.end method

.method public setShowSoftKeyBoard(Z)V
    .locals 0
    .param p1, "bShow"    # Z

    .prologue
    .line 3100
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bShowSoftKeyboard:Z

    .line 3101
    return-void
.end method

.method public setTTSSelection(II)V
    .locals 0
    .param p1, "nStart"    # I
    .param p2, "nEnd"    # I

    .prologue
    .line 2934
    iput p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nTTSStart:I

    .line 2935
    iput p2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nTTSEnd:I

    .line 2936
    return-void
.end method

.method public setTTSStartSelection()V
    .locals 5

    .prologue
    .line 2920
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getTotalPaddingTop()I

    move-result v1

    .line 2921
    .local v1, "paddingTop":I
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 2922
    .local v2, "rectVisible":Landroid/graphics/Rect;
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 2923
    const/4 v0, 0x0

    .line 2926
    .local v0, "nLine":I
    iget v3, v2, Landroid/graphics/Rect;->top:I

    if-ltz v3, :cond_0

    .line 2927
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v3

    iget v4, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v4, v1

    invoke-virtual {v3, v4}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v0

    .line 2929
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/text/Layout;->getLineStart(I)I

    move-result v3

    iput v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nTTSStart:I

    .line 2930
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/text/Layout;->getLineStart(I)I

    move-result v3

    iput v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nTTSEnd:I

    .line 2931
    return-void
.end method

.method public setToastPopupState(I)V
    .locals 0
    .param p1, "nToastPopupState"    # I

    .prologue
    .line 1726
    iput p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nToastPopupState:I

    .line 1727
    return-void
.end method

.method public setUndoOneAction(Z)V
    .locals 1
    .param p1, "bOneAction"    # Z

    .prologue
    .line 3907
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_UndoRedoMgr:Lcom/infraware/polarisoffice5/text/manager/CommandManager;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->setOneAction(Z)V

    .line 3908
    return-void
.end method

.method public setUndoRedo(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 3919
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bUndoRedoFlag:Z

    .line 3920
    return-void
.end method

.method public showDelayToastPopup()V
    .locals 1

    .prologue
    .line 2041
    const/16 v0, 0x28a

    .line 2042
    .local v0, "delaytime":I
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->showToastPopup(I)V

    .line 2043
    return-void
.end method

.method public showMorePopup()V
    .locals 4

    .prologue
    .line 2704
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hideSoftKeyboard()Z

    .line 2705
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_moretoastPopupRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2706
    return-void
.end method

.method public showRectInfo(Landroid/graphics/Rect;)V
    .locals 0
    .param p1, "r"    # Landroid/graphics/Rect;

    .prologue
    .line 2051
    return-void
.end method

.method public showSoftKeyboard()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3076
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isShown()Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    .line 3092
    :goto_0
    return v1

    .line 3079
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    check-cast v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getViewMode()I

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    .line 3080
    goto :goto_0

    .line 3083
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hideDictionaryPanel()V

    .line 3084
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bShowSoftKeyboard:Z

    .line 3085
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bShowCursor:Z

    .line 3086
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->invalidate()V

    .line 3087
    new-instance v1, Lcom/infraware/polarisoffice5/common/ImmManager;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;

    invoke-direct {v1, v4}, Lcom/infraware/polarisoffice5/common/ImmManager;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/ImmManager;->showDelayedIme()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v3

    .line 3088
    goto :goto_0

    .line 3089
    :catch_0
    move-exception v0

    .line 3090
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v1, v2

    .line 3092
    goto :goto_0
.end method

.method public showToastPopup()V
    .locals 2

    .prologue
    .line 2025
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_toastPopupRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2026
    return-void
.end method

.method public showToastPopup(FF)V
    .locals 0
    .param p1, "pX"    # F
    .param p2, "pY"    # F

    .prologue
    .line 2033
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->showToastPopup()V

    .line 2034
    return-void
.end method

.method public showToastPopup(FFI)V
    .locals 0
    .param p1, "pX"    # F
    .param p2, "pY"    # F
    .param p3, "delaytime"    # I

    .prologue
    .line 2037
    invoke-virtual {p0, p3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->showToastPopup(I)V

    .line 2038
    return-void
.end method

.method public showToastPopup(I)V
    .locals 4
    .param p1, "delaytime"    # I

    .prologue
    .line 2029
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->toastHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_toastPopupRunnable:Ljava/lang/Runnable;

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2030
    return-void
.end method

.method public stopFling()V
    .locals 1

    .prologue
    .line 3579
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_Scroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3580
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_Scroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 3581
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bFling:Z

    .line 3582
    return-void
.end method

.method public textReadAndPlay(Lcom/infraware/polarisoffice5/text/manager/TTSManager;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "ttsManager"    # Lcom/infraware/polarisoffice5/text/manager/TTSManager;
    .param p2, "ment"    # Ljava/lang/String;
    .param p3, "afterscroll"    # Z

    .prologue
    .line 536
    invoke-virtual {p0, p3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getCalcPositionString(Z)Ljava/lang/String;

    move-result-object v0

    .line 538
    .local v0, "positionMent":Ljava/lang/String;
    if-eqz p3, :cond_0

    .line 539
    invoke-virtual {p1, v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->textReadAndPlay(Ljava/lang/String;)Z

    .line 542
    :goto_0
    return-void

    .line 541
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->textReadAndPlay(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public undo()I
    .locals 1

    .prologue
    .line 1693
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_UndoRedoMgr:Lcom/infraware/polarisoffice5/text/manager/CommandManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/CommandManager;->undo()I

    move-result v0

    return v0
.end method

.method public undoRedoViewProcess(I)V
    .locals 5
    .param p1, "nPos"    # I

    .prologue
    const/4 v4, 0x0

    .line 1702
    iput-boolean v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bFling:Z

    .line 1703
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v3, p1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockIndexFromOffset(I)I

    move-result v1

    .line 1704
    .local v1, "nBlock":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v3, p1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockOffsetFromOffset(I)I

    move-result v2

    .line 1706
    .local v2, "nOffset":I
    invoke-virtual {p0, v1, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->loadBlock(IZ)V

    .line 1710
    :try_start_0
    iget v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nBlockStartOffset:I

    add-int/2addr v3, v2

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1715
    :goto_0
    return-void

    .line 1712
    :catch_0
    move-exception v0

    .line 1713
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    goto :goto_0
.end method
