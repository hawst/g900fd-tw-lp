.class public Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;
.super Lcom/infraware/polarisoffice5/word/BookmarkActivity;
.source "TextBookmarkActivity.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Path;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$StringReference;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;
    }
.end annotation


# instance fields
.field private m_adapter:Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;

.field private m_bIsEditMode:Z

.field private m_bookmarkList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/text/manager/BookmarkData;",
            ">;"
        }
    .end annotation
.end field

.field private m_in:Ljava/io/BufferedReader;

.field private m_incomingPos:[I

.field private m_listView:Landroid/widget/ListView;

.field private m_out:Ljava/io/BufferedWriter;

.field private m_strBookmarkFileFullPath:Ljava/lang/String;

.field private m_strBookmarkFileName:Ljava/lang/String;

.field private m_strBookmarkFilePath:Ljava/lang/String;

.field private m_strOriginalFileFullPath:Ljava/lang/String;

.field private m_strOriginalFileName:Ljava/lang/String;

.field private m_strOriginalFilePath:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;-><init>()V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_bIsEditMode:Z

    .line 59
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_out:Ljava/io/BufferedWriter;

    .line 60
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_in:Ljava/io/BufferedReader;

    .line 499
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_bookmarkList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->initBookmarkEditText()V

    return-void
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->initItemLayout()V

    return-void
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;)Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_adapter:Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;

    return-object v0
.end method

.method private initBookmarkData()V
    .locals 0

    .prologue
    .line 229
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->initItemLayout()V

    .line 230
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->initBookmarkEditText()V

    .line 231
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->initListView()V

    .line 232
    return-void
.end method

.method private initBookmarkEditText()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 175
    const/4 v0, 0x0

    .line 176
    .local v0, "item":Lcom/infraware/polarisoffice5/common/MultiListItem;
    const/4 v2, 0x0

    .line 177
    .local v2, "strBookmark":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0700df

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 178
    .local v9, "strTemp":Ljava/lang/String;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_bookmarkList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 180
    .local v8, "nRet":I
    if-lez v8, :cond_3

    .line 181
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v8, :cond_0

    .line 182
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_bookmarkList:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->getBookmarkName()Ljava/lang/String;

    move-result-object v2

    .line 183
    new-instance v0, Lcom/infraware/polarisoffice5/common/MultiListItem;

    .end local v0    # "item":Lcom/infraware/polarisoffice5/common/MultiListItem;
    const/4 v1, 0x5

    const v3, 0x7f0200ab

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02005a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->isEditMode()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/infraware/polarisoffice5/common/MultiListItem;-><init>(ILjava/lang/String;IIZ)V

    .line 184
    .restart local v0    # "item":Lcom/infraware/polarisoffice5/common/MultiListItem;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->mSrcListItem:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 187
    :cond_0
    new-instance v10, Ljava/lang/String;

    invoke-direct {v10}, Ljava/lang/String;-><init>()V

    .line 188
    .local v10, "temp":Ljava/lang/String;
    const/4 v7, 0x1

    .local v7, "k":I
    :goto_1
    add-int/lit8 v1, v8, 0x1

    if-ge v7, v1, :cond_1

    .line 190
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "%02d"

    new-array v4, v11, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->mSrcListItem:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/2addr v5, v7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v12

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 191
    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->initBookmarkName(Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v11, :cond_2

    .line 193
    move-object v9, v10

    .line 201
    .end local v6    # "i":I
    .end local v7    # "k":I
    .end local v10    # "temp":Ljava/lang/String;
    :cond_1
    :goto_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->mAddBookmarkEdit:Landroid/widget/EditText;

    invoke-virtual {v1, v9}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 202
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->mAddBookmarkEdit:Landroid/widget/EditText;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setSelection(I)V

    .line 204
    return-void

    .line 188
    .restart local v6    # "i":I
    .restart local v7    # "k":I
    .restart local v10    # "temp":Ljava/lang/String;
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 198
    .end local v6    # "i":I
    .end local v7    # "k":I
    .end local v10    # "temp":Ljava/lang/String;
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "%02d"

    new-array v4, v11, [Ljava/lang/Object;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v12

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto :goto_2
.end method

.method private initFilename(Ljava/lang/String;)V
    .locals 4
    .param p1, "fullpath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x2f

    .line 114
    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_strOriginalFilePath:Ljava/lang/String;

    .line 115
    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_strOriginalFileName:Ljava/lang/String;

    .line 117
    sget-object v0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->PATH_BOOKMARKDATA:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_strBookmarkFileFullPath:Ljava/lang/String;

    .line 118
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_strBookmarkFileFullPath:Ljava/lang/String;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_strBookmarkFileFullPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_strBookmarkFilePath:Ljava/lang/String;

    .line 119
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_strBookmarkFileFullPath:Ljava/lang/String;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_strBookmarkFileFullPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_strBookmarkFileName:Ljava/lang/String;

    .line 120
    return-void
.end method

.method private initItemLayout()V
    .locals 6

    .prologue
    const v5, 0x7f0b002e

    const v4, 0x7f0b002c

    const v3, 0x7f0b0026

    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 151
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->isEditMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 153
    const v0, 0x7f0b0029

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 154
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_bookmarkList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 155
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 156
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 172
    :goto_0
    return-void

    .line 158
    :cond_0
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 159
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 160
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 164
    :cond_1
    const v0, 0x7f0b0029

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 165
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 166
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 167
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_bookmarkList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 168
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 170
    :cond_2
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private initListView()V
    .locals 4

    .prologue
    .line 207
    const v1, 0x7f0b002d

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_listView:Landroid/widget/ListView;

    .line 208
    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->mSrcListItem:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->messageHandler:Landroid/os/Handler;

    invoke-direct {v1, p0, p0, v2, v3}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;-><init>(Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;Landroid/content/Context;Ljava/util/ArrayList;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_adapter:Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;

    .line 209
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_listView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_adapter:Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 210
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_listView:Landroid/widget/ListView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 211
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020010

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 212
    .local v0, "selector":Landroid/graphics/drawable/Drawable;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_listView:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 213
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_listView:Landroid/widget/ListView;

    new-instance v2, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$2;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$2;-><init>(Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 226
    return-void
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 123
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_bookmarkList:Ljava/util/ArrayList;

    .line 125
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->PATH_BOOKMARKDATA:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 126
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 127
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 130
    :cond_0
    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$1;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$1;-><init>(Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;)V

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->setBookmarkCallback(Lcom/infraware/polarisoffice5/word/BookmarkActivity$BookmarkCallback;)V

    .line 146
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->readBookmarkData()Z

    .line 147
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->initBookmarkData()V

    .line 148
    return-void
.end method

.method private onFinish()V
    .locals 0

    .prologue
    .line 246
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->writeBookmarkData()V

    .line 248
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->finish()V

    .line 249
    return-void
.end method

.method private readBookmarkData()Z
    .locals 15

    .prologue
    .line 290
    new-instance v10, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->PATH_BOOKMARKDATA:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_strOriginalFileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "meta"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v10, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 291
    .local v10, "file":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 292
    const/4 v1, 0x0

    .line 332
    :goto_0
    return v1

    .line 294
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_in:Ljava/io/BufferedReader;

    if-nez v1, :cond_1

    .line 296
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->PATH_BOOKMARKDATA:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_strOriginalFileName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "meta"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_in:Ljava/io/BufferedReader;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 303
    :cond_1
    const/4 v1, 0x6

    new-array v14, v1, [Ljava/lang/String;

    .line 306
    .local v14, "temp":[Ljava/lang/String;
    :try_start_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_in:Ljava/io/BufferedReader;

    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v12

    .line 307
    .local v12, "line":Ljava/lang/String;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_in:Ljava/io/BufferedReader;

    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v12

    .line 308
    :goto_1
    if-eqz v12, :cond_3

    .line 309
    new-instance v13, Ljava/util/StringTokenizer;

    const-string/jumbo v1, "|"

    invoke-direct {v13, v12, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    .local v13, "st":Ljava/util/StringTokenizer;
    const/4 v11, 0x0

    .line 311
    .local v11, "index":I
    :goto_2
    invoke-virtual {v13}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 312
    invoke-virtual {v13}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v14, v11
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 313
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 297
    .end local v11    # "index":I
    .end local v12    # "line":Ljava/lang/String;
    .end local v13    # "st":Ljava/util/StringTokenizer;
    .end local v14    # "temp":[Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 298
    .local v9, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v9}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 299
    const/4 v1, 0x0

    goto :goto_0

    .line 316
    .end local v9    # "e":Ljava/io/FileNotFoundException;
    .restart local v11    # "index":I
    .restart local v12    # "line":Ljava/lang/String;
    .restart local v13    # "st":Ljava/util/StringTokenizer;
    .restart local v14    # "temp":[Ljava/lang/String;
    :cond_2
    :try_start_2
    new-instance v0, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;

    const/4 v1, 0x0

    aget-object v1, v14, v1

    const/4 v2, 0x1

    aget-object v2, v14, v2

    const/4 v3, 0x2

    aget-object v3, v14, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x3

    aget-object v4, v14, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x5

    aget-object v5, v14, v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    const/4 v7, 0x4

    aget-object v7, v14, v7

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-direct/range {v0 .. v8}, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;-><init>(Ljava/lang/String;Ljava/lang/String;IIJJ)V

    .line 322
    .local v0, "data":Lcom/infraware/polarisoffice5/text/manager/BookmarkData;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_bookmarkList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 324
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_in:Ljava/io/BufferedReader;

    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v12

    .line 325
    goto :goto_1

    .line 327
    .end local v0    # "data":Lcom/infraware/polarisoffice5/text/manager/BookmarkData;
    .end local v11    # "index":I
    .end local v13    # "st":Ljava/util/StringTokenizer;
    :cond_3
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_in:Ljava/io/BufferedReader;

    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 332
    .end local v12    # "line":Ljava/lang/String;
    :goto_3
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 328
    :catch_1
    move-exception v1

    goto :goto_3
.end method

.method private writeBookmarkData()V
    .locals 10

    .prologue
    .line 336
    new-instance v2, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->PATH_BOOKMARKDATA:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_strOriginalFileName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "meta"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 337
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_0

    .line 339
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 349
    :goto_0
    :try_start_1
    new-instance v6, Ljava/io/BufferedWriter;

    new-instance v7, Ljava/io/FileWriter;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->PATH_BOOKMARKDATA:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_strOriginalFileName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "meta"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    invoke-direct {v6, v7}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    iput-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_out:Ljava/io/BufferedWriter;

    .line 351
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_out:Ljava/io/BufferedWriter;

    const-string/jumbo v7, "[bookmarkDataStart]"

    invoke-virtual {v6, v7}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 352
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_out:Ljava/io/BufferedWriter;

    invoke-virtual {v6}, Ljava/io/BufferedWriter;->newLine()V

    .line 354
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 355
    .local v4, "sb":Ljava/lang/StringBuilder;
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string/jumbo v6, "yyyyMMdd"

    invoke-direct {v5, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 356
    .local v5, "sdf":Ljava/text/SimpleDateFormat;
    const-string/jumbo v0, ""

    .line 358
    .local v0, "dateString":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_bookmarkList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v3, v6, :cond_1

    .line 359
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_bookmarkList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->getFileName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 360
    const-string/jumbo v6, "|"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 361
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_bookmarkList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->getBookmarkName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 362
    const-string/jumbo v6, "|"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 363
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_bookmarkList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->getOffsetStart()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 364
    const-string/jumbo v6, "|"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 365
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_bookmarkList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->getOffsetEnd()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 366
    const-string/jumbo v6, "|"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 367
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_bookmarkList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->getLastmodified()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 368
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 369
    const-string/jumbo v6, "|"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 370
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_bookmarkList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->getFilesize()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 372
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_out:Ljava/io/BufferedWriter;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 373
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_out:Ljava/io/BufferedWriter;

    invoke-virtual {v6}, Ljava/io/BufferedWriter;->newLine()V

    .line 374
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->setLength(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 358
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 340
    .end local v0    # "dateString":Ljava/lang/String;
    .end local v3    # "i":I
    .end local v4    # "sb":Ljava/lang/StringBuilder;
    .end local v5    # "sdf":Ljava/text/SimpleDateFormat;
    :catch_0
    move-exception v1

    .line 341
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 381
    .end local v1    # "e":Ljava/io/IOException;
    :goto_2
    return-void

    .line 345
    :cond_0
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto/16 :goto_0

    .line 377
    .restart local v0    # "dateString":Ljava/lang/String;
    .restart local v3    # "i":I
    .restart local v4    # "sb":Ljava/lang/StringBuilder;
    .restart local v5    # "sdf":Ljava/text/SimpleDateFormat;
    :cond_1
    :try_start_2
    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_out:Ljava/io/BufferedWriter;

    invoke-virtual {v6}, Ljava/io/BufferedWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 378
    .end local v0    # "dateString":Ljava/lang/String;
    .end local v3    # "i":I
    .end local v4    # "sb":Ljava/lang/StringBuilder;
    .end local v5    # "sdf":Ljava/text/SimpleDateFormat;
    :catch_1
    move-exception v1

    .line 379
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method


# virtual methods
.method protected AddBookmark()V
    .locals 15

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 255
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->mAddBookmarkEdit:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v14

    .line 257
    .local v14, "strAddBookmark":Ljava/lang/String;
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->mSrcListItem:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v12, v1, :cond_1

    .line 258
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->mSrcListItem:Ljava/util/ArrayList;

    invoke-virtual {v1, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/infraware/polarisoffice5/common/MultiListItem;

    .line 259
    .local v13, "item":Lcom/infraware/polarisoffice5/common/MultiListItem;
    invoke-virtual {v13}, Lcom/infraware/polarisoffice5/common/MultiListItem;->getText1()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v5, :cond_0

    .line 260
    new-instance v9, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v1

    invoke-direct {v9, p0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 261
    .local v9, "builder":Landroid/app/AlertDialog$Builder;
    const-string/jumbo v1, ""

    invoke-virtual {v9, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 262
    const v1, 0x7f0700dd

    invoke-virtual {v9, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 263
    const v1, 0x7f070063

    const/4 v2, 0x0

    invoke-virtual {v9, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 264
    invoke-virtual {v9}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v10

    .line 265
    .local v10, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v10, v4}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 266
    invoke-virtual {v10}, Landroid/app/AlertDialog;->show()V

    .line 287
    .end local v9    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v10    # "dialog":Landroid/app/AlertDialog;
    .end local v13    # "item":Lcom/infraware/polarisoffice5/common/MultiListItem;
    :goto_1
    return-void

    .line 257
    .restart local v13    # "item":Lcom/infraware/polarisoffice5/common/MultiListItem;
    :cond_0
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 271
    .end local v13    # "item":Lcom/infraware/polarisoffice5/common/MultiListItem;
    :cond_1
    new-instance v11, Ljava/io/File;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_strOriginalFileFullPath:Ljava/lang/String;

    invoke-direct {v11, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 273
    .local v11, "file":Ljava/io/File;
    new-instance v0, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_strOriginalFileName:Ljava/lang/String;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->mAddBookmarkEdit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_incomingPos:[I

    aget v3, v3, v4

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_incomingPos:[I

    aget v4, v4, v5

    invoke-virtual {v11}, Ljava/io/File;->length()J

    move-result-wide v5

    invoke-virtual {v11}, Ljava/io/File;->lastModified()J

    move-result-wide v7

    invoke-direct/range {v0 .. v8}, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;-><init>(Ljava/lang/String;Ljava/lang/String;IIJJ)V

    .line 279
    .local v0, "data":Lcom/infraware/polarisoffice5/text/manager/BookmarkData;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_bookmarkList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 281
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->finish()V

    goto :goto_1
.end method

.method public isEditMode()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_bIsEditMode:Z

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 67
    invoke-super {p0, p1}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->onCreate(Landroid/os/Bundle;)V

    .line 68
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 70
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "Bookmark"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v1

    .line 71
    .local v1, "pos":[I
    const-string/jumbo v2, "FilePath"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_strOriginalFileFullPath:Ljava/lang/String;

    .line 74
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_strOriginalFileFullPath:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->initFilename(Ljava/lang/String;)V

    .line 76
    if-nez v1, :cond_0

    .line 77
    const v2, 0x7f0b0029

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 78
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->setEditMode(Z)V

    .line 87
    :goto_0
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->initialize()V

    .line 88
    return-void

    .line 81
    :cond_0
    const/4 v2, 0x2

    new-array v2, v2, [I

    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_incomingPos:[I

    .line 82
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_incomingPos:[I

    aget v3, v1, v4

    aput v3, v2, v4

    .line 83
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_incomingPos:[I

    aget v3, v1, v5

    aput v3, v2, v5

    .line 84
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->setEditMode(Z)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 236
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->onFinish()V

    .line 238
    invoke-super {p0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->onDestroy()V

    .line 239
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x1

    .line 92
    invoke-super {p0, p1}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 94
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->isEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->mAddBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 97
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->mAddBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 98
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->mAddBookmarkEdit:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 99
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->mAddBookmarkEdit:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 101
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 103
    :cond_0
    return-void
.end method

.method public setEditMode(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 106
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_bIsEditMode:Z

    .line 107
    return-void
.end method
