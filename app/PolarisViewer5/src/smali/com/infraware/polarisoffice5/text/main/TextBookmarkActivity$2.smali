.class Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$2;
.super Ljava/lang/Object;
.source "TextBookmarkActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->initListView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 215
    .local p1, "a":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 217
    .local v0, "intent":Landroid/content/Intent;
    const/4 v2, 0x2

    new-array v1, v2, [I

    .line 218
    .local v1, "pos":[I
    const/4 v3, 0x0

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_bookmarkList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->access$000(Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->getOffsetStart()I

    move-result v2

    aput v2, v1, v3

    .line 219
    const/4 v3, 0x1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_bookmarkList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->access$000(Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->getOffsetEnd()I

    move-result v2

    aput v2, v1, v3

    .line 221
    const-string/jumbo v2, "Bookmark"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 222
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;

    const/4 v3, -0x1

    invoke-virtual {v2, v3, v0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->setResult(ILandroid/content/Intent;)V

    .line 223
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->finish()V

    .line 224
    return-void
.end method
