.class Lcom/infraware/polarisoffice5/text/control/EditCtrl$9;
.super Ljava/lang/Object;
.source "EditCtrl.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setToastPopupEventListener(Lcom/infraware/polarisoffice5/text/control/ToastPopup;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

.field final synthetic val$tvPaste:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 2326
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$9;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iput-object p2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$9;->val$tvPaste:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "evt"    # Landroid/view/MotionEvent;

    .prologue
    const v7, 0x7f02000f

    const/4 v6, 0x0

    .line 2330
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    .line 2331
    .local v0, "lPadding":I
    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    .line 2333
    .local v2, "rPadding":I
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 2334
    .local v1, "r":Landroid/graphics/Rect;
    const/4 v3, 0x1

    .line 2336
    .local v3, "result":Z
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 2376
    :cond_0
    :goto_0
    const/4 v4, 0x1

    return v4

    .line 2338
    :pswitch_0
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$9;->val$tvPaste:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 2339
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$9;->val$tvPaste:Landroid/widget/ImageView;

    invoke-virtual {v4, v0, v6, v2, v6}, Landroid/widget/ImageView;->setPadding(IIII)V

    goto :goto_0

    .line 2343
    :pswitch_1
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$9;->val$tvPaste:Landroid/widget/ImageView;

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 2345
    invoke-virtual {p1, v1}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 2346
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v1, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    .line 2348
    if-eqz v3, :cond_0

    .line 2357
    new-instance v4, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$9;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-direct {v4, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$PasteProgress;->start()V

    .line 2361
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$9;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->dismissPopup()V

    .line 2363
    :pswitch_2
    invoke-virtual {p1, v1}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 2364
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v1, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    .line 2366
    if-eqz v3, :cond_1

    .line 2367
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$9;->val$tvPaste:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 2369
    :cond_1
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$9;->val$tvPaste:Landroid/widget/ImageView;

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_0

    .line 2336
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
