.class public Lcom/infraware/polarisoffice5/text/manager/SecClipboard;
.super Ljava/lang/Object;
.source "SecClipboard.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/text/manager/SecClipboard$IClipboardDataPasteEventImpl;
    }
.end annotation


# instance fields
.field private m_activity:Landroid/app/Activity;

.field private m_clipboard:Landroid/sec/clipboard/ClipboardExManager;

.field private m_nowProgress:Z

.field private m_pasteEvent:Lcom/infraware/polarisoffice5/text/manager/SecClipboard$IClipboardDataPasteEventImpl;

.field private messageHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/sec/clipboard/ClipboardExManager;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "secclipboard"    # Landroid/sec/clipboard/ClipboardExManager;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_activity:Landroid/app/Activity;

    .line 25
    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_clipboard:Landroid/sec/clipboard/ClipboardExManager;

    .line 27
    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_pasteEvent:Lcom/infraware/polarisoffice5/text/manager/SecClipboard$IClipboardDataPasteEventImpl;

    .line 29
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_nowProgress:Z

    .line 31
    new-instance v0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/manager/SecClipboard$1;-><init>(Lcom/infraware/polarisoffice5/text/manager/SecClipboard;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->messageHandler:Landroid/os/Handler;

    .line 41
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_activity:Landroid/app/Activity;

    .line 42
    iput-object p2, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_clipboard:Landroid/sec/clipboard/ClipboardExManager;

    .line 44
    new-instance v0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard$IClipboardDataPasteEventImpl;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/manager/SecClipboard$IClipboardDataPasteEventImpl;-><init>(Lcom/infraware/polarisoffice5/text/manager/SecClipboard;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_pasteEvent:Lcom/infraware/polarisoffice5/text/manager/SecClipboard$IClipboardDataPasteEventImpl;

    .line 46
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_nowProgress:Z

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/text/manager/SecClipboard;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_activity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/text/manager/SecClipboard;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->messageHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/text/manager/SecClipboard;)Landroid/sec/clipboard/ClipboardExManager;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_clipboard:Landroid/sec/clipboard/ClipboardExManager;

    return-object v0
.end method


# virtual methods
.method public dismissUIPanel()V
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_clipboard:Landroid/sec/clipboard/ClipboardExManager;

    invoke-virtual {v0}, Landroid/sec/clipboard/ClipboardExManager;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_clipboard:Landroid/sec/clipboard/ClipboardExManager;

    invoke-virtual {v0}, Landroid/sec/clipboard/ClipboardExManager;->dismissUIDataDialog()V

    .line 183
    :cond_0
    return-void
.end method

.method public doPaste()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 186
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_clipboard:Landroid/sec/clipboard/ClipboardExManager;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_pasteEvent:Lcom/infraware/polarisoffice5/text/manager/SecClipboard$IClipboardDataPasteEventImpl;

    if-eqz v2, :cond_0

    .line 188
    :try_start_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_clipboard:Landroid/sec/clipboard/ClipboardExManager;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_activity:Landroid/app/Activity;

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_pasteEvent:Lcom/infraware/polarisoffice5/text/manager/SecClipboard$IClipboardDataPasteEventImpl;

    invoke-virtual {v2, v3, v4, v5}, Landroid/sec/clipboard/ClipboardExManager;->getData(Landroid/content/Context;ILandroid/sec/clipboard/IClipboardDataPasteEvent;)Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    const/4 v1, 0x1

    .line 196
    :cond_0
    :goto_0
    return v1

    .line 190
    :catch_0
    move-exception v0

    .line 191
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    goto :goto_0
.end method

.method public getDataListSize()I
    .locals 2

    .prologue
    .line 208
    :try_start_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_clipboard:Landroid/sec/clipboard/ClipboardExManager;

    invoke-virtual {v1}, Landroid/sec/clipboard/ClipboardExManager;->getDataListSize()I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 213
    :goto_0
    return v0

    .line 210
    :catch_0
    move-exception v1

    .line 213
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized getNowProgress()Z
    .locals 1

    .prologue
    .line 221
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_nowProgress:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getText()Ljava/lang/String;
    .locals 5

    .prologue
    .line 62
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_clipboard:Landroid/sec/clipboard/ClipboardExManager;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_activity:Landroid/app/Activity;

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Landroid/sec/clipboard/ClipboardExManager;->getData(Landroid/content/Context;I)Landroid/sec/clipboard/data/ClipboardData;

    move-result-object v0

    .line 64
    .local v0, "data":Landroid/sec/clipboard/data/ClipboardData;
    if-eqz v0, :cond_0

    move-object v1, v0

    .line 65
    check-cast v1, Landroid/sec/clipboard/data/list/ClipboardDataText;

    .line 66
    .local v1, "text":Landroid/sec/clipboard/data/list/ClipboardDataText;
    invoke-virtual {v1}, Landroid/sec/clipboard/data/list/ClipboardDataText;->GetText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 69
    .end local v1    # "text":Landroid/sec/clipboard/data/list/ClipboardDataText;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isSamsungClipboard()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 86
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_clipboard:Landroid/sec/clipboard/ClipboardExManager;

    if-nez v2, :cond_1

    .line 101
    :cond_0
    :goto_0
    return v1

    .line 90
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_pasteEvent:Lcom/infraware/polarisoffice5/text/manager/SecClipboard$IClipboardDataPasteEventImpl;

    if-eqz v2, :cond_0

    .line 95
    :try_start_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_clipboard:Landroid/sec/clipboard/ClipboardExManager;

    invoke-virtual {v2}, Landroid/sec/clipboard/ClipboardExManager;->getDataListSize()I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    const/4 v1, 0x1

    goto :goto_0

    .line 97
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    goto :goto_0
.end method

.method public isSamsungClipboardOpened()Z
    .locals 2

    .prologue
    .line 73
    const/4 v0, 0x0

    .line 75
    .local v0, "result":Z
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_clipboard:Landroid/sec/clipboard/ClipboardExManager;

    invoke-virtual {v1}, Landroid/sec/clipboard/ClipboardExManager;->isShowing()Z

    move-result v0

    .line 77
    return v0
.end method

.method public openClipboard()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_clipboard:Landroid/sec/clipboard/ClipboardExManager;

    invoke-virtual {v0}, Landroid/sec/clipboard/ClipboardExManager;->showUIDataDialog()V

    .line 82
    return-void
.end method

.method public declared-synchronized setNowProgress(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 217
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_nowProgress:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 218
    monitor-exit p0

    return-void

    .line 217
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setText(Ljava/lang/String;)V
    .locals 3
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 50
    new-instance v0, Landroid/sec/clipboard/data/list/ClipboardDataText;

    invoke-direct {v0}, Landroid/sec/clipboard/data/list/ClipboardDataText;-><init>()V

    .line 52
    .local v0, "text":Landroid/sec/clipboard/data/list/ClipboardDataText;
    invoke-virtual {v0, p1}, Landroid/sec/clipboard/data/list/ClipboardDataText;->SetText(Ljava/lang/CharSequence;)Z

    .line 55
    :try_start_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_clipboard:Landroid/sec/clipboard/ClipboardExManager;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_activity:Landroid/app/Activity;

    invoke-virtual {v1, v2, v0}, Landroid/sec/clipboard/ClipboardExManager;->setData(Landroid/content/Context;Landroid/sec/clipboard/data/ClipboardData;)Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    :goto_0
    return-void

    .line 56
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public thisFinalize()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 200
    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_activity:Landroid/app/Activity;

    .line 201
    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_clipboard:Landroid/sec/clipboard/ClipboardExManager;

    .line 202
    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_pasteEvent:Lcom/infraware/polarisoffice5/text/manager/SecClipboard$IClipboardDataPasteEventImpl;

    .line 203
    return-void
.end method
