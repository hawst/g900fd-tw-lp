.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$37;
.super Ljava/lang/Object;
.source "TextEditorActivity.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
    .locals 0

    .prologue
    .line 5387
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$37;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "arg0"    # Landroid/widget/TextView;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5392
    const/4 v2, 0x3

    if-ne p2, v2, :cond_0

    .line 5394
    new-instance v2, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$37;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {v2, v3, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;I)V

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->start()V

    .line 5402
    :goto_0
    return v0

    .line 5397
    :cond_0
    const/4 v2, 0x5

    if-ne p2, v2, :cond_1

    .line 5399
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$37;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplace:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$5000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    :cond_1
    move v0, v1

    .line 5402
    goto :goto_0
.end method
