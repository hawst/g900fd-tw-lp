.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress$1;
.super Ljava/lang/Object;
.source "TextEditorActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;)V
    .locals 0

    .prologue
    .line 4521
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 4524
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nReplaceCnt:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$5300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)I

    move-result v1

    const/16 v2, 0x64

    if-lt v1, v2, :cond_0

    .line 4525
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBlock:I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$4600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)I

    move-result v2

    invoke-virtual {v1, v2, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->loadBlock(IZ)V

    .line 4526
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nFindBeginPos:I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$5400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    .line 4527
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    const v2, 0x7f0702f6

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 4538
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->pd:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->access$5500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 4540
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setLoadingProgress(Z)V

    .line 4541
    return-void

    .line 4529
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v1

    invoke-virtual {v1, v4, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->loadBlock(IZ)V

    .line 4530
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nReplaceCnt:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$5300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)I

    move-result v1

    if-nez v1, :cond_1

    .line 4531
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    const v2, 0x7f0701dc

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 4533
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070308

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nReplaceCnt:I
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$5300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 4534
    .local v0, "resultmsg":Ljava/lang/String;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$ReplaceAllProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-static {v1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
