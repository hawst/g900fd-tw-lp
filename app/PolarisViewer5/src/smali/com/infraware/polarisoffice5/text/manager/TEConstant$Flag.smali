.class public interface abstract Lcom/infraware/polarisoffice5/text/manager/TEConstant$Flag;
.super Ljava/lang/Object;
.source "TEConstant.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/text/manager/TEConstant;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Flag"
.end annotation


# static fields
.field public static final FLAG_ACTIONBAR_ABOUT_PRODUCT_EVENT:I = 0x11

.field public static final FLAG_ACTIONBAR_BOOKMARK:I = 0x10

.field public static final FLAG_ACTIONBAR_EDIT_EVENT:I = 0x12

.field public static final FLAG_ACTIONBAR_FINDOPBTN:I = 0xe

.field public static final FLAG_ACTIONBAR_FINDOPMATCHCASE:I = 0xa

.field public static final FLAG_ACTIONBAR_FINDOPWHOLEWORD:I = 0xc

.field public static final FLAG_ACTIONBAR_HELP:I = 0x7

.field public static final FLAG_ACTIONBAR_INFO:I = 0x8

.field public static final FLAG_ACTIONBAR_INSTALLED:I = 0x6

.field public static final FLAG_ACTIONBAR_NOT_INSTALLED:I = 0x5

.field public static final FLAG_ACTIONBAR_POPUP_FIND:I = 0x2

.field public static final FLAG_ACTIONBAR_POPUP_FORMATICON:I = 0x1

.field public static final FLAG_ACTIONBAR_POPUP_MENU:I = 0x0

.field public static final FLAG_ACTIONBAR_POPUP_REPLACE:I = 0x3

.field public static final FLAG_ACTIONBAR_PRINT_EVENT:I = 0x3

.field public static final FLAG_ACTIONBAR_REPLACEOPBTN:I = 0xf

.field public static final FLAG_ACTIONBAR_REPLACEOPMATCHCASE:I = 0xb

.field public static final FLAG_ACTIONBAR_REPLACEOPWHOLEWORD:I = 0xd

.field public static final FLAG_ACTIONBAR_SAVEAS_EVENT:I = 0x1

.field public static final FLAG_ACTIONBAR_SAVE_EVENT:I = 0x0

.field public static final FLAG_ACTIONBAR_SENDFILE:I = 0x4

.field public static final FLAG_ACTIONBAR_SETTINGS:I = 0x9

.field public static final FLAG_ACTIONBAR_TTS_EVENT:I = 0x2

.field public static final FLAG_ADAPTER_ENCODING:I = 0x0

.field public static final FLAG_ANIMATION_TOLEFT:I = 0x0

.field public static final FLAG_ANIMATION_TORIGHT:I = 0x1

.field public static final FLAG_CLIPBOARD_DEFAULT:I = 0x0

.field public static final FLAG_CLIPBOARD_SAMSUNG:I = 0x1

.field public static final FLAG_EDITMODE:I = 0x0

.field public static final FLAG_INVALID:I = -0x63

.field public static final FLAG_PRINT_FAILED:I = -0x1

.field public static final FLAG_REQUEST_PREFERENCE:I = 0x1

.field public static final FLAG_SAVE:I = 0x0

.field public static final FLAG_SAVE_N_FINISH:I = 0x1

.field public static final FLAG_SP_UPDATE_COPY:I = 0x2

.field public static final FLAG_SP_UPDATE_DELETE:I = 0x1

.field public static final FLAG_SP_UPDATE_MOVE:I = 0x3

.field public static final FLAG_TTSMODE_SELECTED:I = 0x1

.field public static final FLAG_TTSMODE_WHOLE:I = 0x0

.field public static final FLAG_VIEWERINEDITMODE:I = 0x2

.field public static final FLAG_VIEWERMODE:I = 0x1
