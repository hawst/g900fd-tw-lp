.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$ReplaceEditKeyEvent;
.super Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;
.source "TextEditorActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReplaceEditKeyEvent"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;


# direct methods
.method private constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;)V
    .locals 1

    .prologue
    .line 5154
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$ReplaceEditKeyEvent;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;
    .param p2, "x1"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$1;

    .prologue
    .line 5154
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$ReplaceEditKeyEvent;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;)V

    return-void
.end method


# virtual methods
.method protected onKeyCode(II)Z
    .locals 1
    .param p1, "a_keyCode"    # I
    .param p2, "a_event"    # I

    .prologue
    .line 5158
    const/4 v0, 0x1

    return v0
.end method

.method protected onKeyLeft()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 5164
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$ReplaceEditKeyEvent;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplace:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 5165
    const/4 v0, 0x0

    .line 5167
    :cond_0
    return v0
.end method

.method protected onKeyRight()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 5173
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$ReplaceEditKeyEvent;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceAll:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$700(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 5174
    const/4 v0, 0x0

    .line 5176
    :cond_0
    return v0
.end method

.method protected onKeyUp()Z
    .locals 1

    .prologue
    .line 5181
    const/4 v0, 0x0

    return v0
.end method
