.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;
.super Ljava/lang/Thread;
.source "TextEditorActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SaveProgress"
.end annotation


# instance fields
.field private delete:Z

.field private pd:Landroid/app/ProgressDialog;

.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Z)V
    .locals 3
    .param p2, "bDelete"    # Z

    .prologue
    const/4 v2, 0x0

    .line 4592
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 4589
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;->pd:Landroid/app/ProgressDialog;

    .line 4590
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;->delete:Z

    .line 4593
    iput-boolean p2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;->delete:Z

    .line 4595
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;->pd:Landroid/app/ProgressDialog;

    .line 4596
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;->pd:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress$1;

    invoke-direct {v1, p0, p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress$1;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 4604
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 4605
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 4606
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701c4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 4607
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 4609
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$100(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->saveFile(Ljava/lang/String;)V
    invoke-static {p1, v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$5800(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Ljava/lang/String;)V

    .line 4611
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setLoadingProgress(Z)V

    .line 4612
    return-void
.end method

.method static synthetic access$5900(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;

    .prologue
    .line 4588
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;->pd:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$6000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;

    .prologue
    .line 4588
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;->delete:Z

    return v0
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 4615
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$100(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;->delete:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->saveProcess(Ljava/lang/String;ZZ)V

    .line 4617
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress$2;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress$2;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$SaveProgress;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 4628
    return-void
.end method
