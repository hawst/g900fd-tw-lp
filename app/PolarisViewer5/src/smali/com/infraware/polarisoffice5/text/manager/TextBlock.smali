.class public Lcom/infraware/polarisoffice5/text/manager/TextBlock;
.super Ljava/lang/Object;
.source "TextBlock.java"


# instance fields
.field public m_nCharCount:I

.field public m_nEndOffset:I

.field public m_nFileNum:I

.field public m_nStartOffset:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nFileNum:I

    .line 12
    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nStartOffset:I

    .line 13
    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nEndOffset:I

    .line 14
    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    .line 17
    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nFileNum:I

    .line 18
    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nStartOffset:I

    .line 19
    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nEndOffset:I

    .line 20
    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    .line 21
    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 1
    .param p1, "nFileNum"    # I
    .param p2, "nStartOffset"    # I
    .param p3, "nEndOffset"    # I
    .param p4, "nCharCount"    # I

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nFileNum:I

    .line 12
    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nStartOffset:I

    .line 13
    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nEndOffset:I

    .line 14
    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    .line 24
    iput p1, p0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nFileNum:I

    .line 25
    iput p2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nStartOffset:I

    .line 26
    iput p3, p0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nEndOffset:I

    .line 27
    iput p4, p0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    .line 28
    return-void
.end method
