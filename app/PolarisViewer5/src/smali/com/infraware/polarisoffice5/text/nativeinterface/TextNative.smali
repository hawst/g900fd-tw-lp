.class public Lcom/infraware/polarisoffice5/text/nativeinterface/TextNative;
.super Ljava/lang/Object;
.source "TextNative.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mInterface:Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5
    const-class v0, Lcom/infraware/polarisoffice5/text/nativeinterface/TextNative;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/infraware/polarisoffice5/text/nativeinterface/TextNative;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;)V
    .locals 1
    .param p1, "a_interface"    # Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/nativeinterface/TextNative;->mInterface:Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;

    .line 11
    const-string/jumbo v0, "EX_Engine5v"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 12
    const-string/jumbo v0, "polaristextprint5v"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 14
    sget-boolean v0, Lcom/infraware/polarisoffice5/text/nativeinterface/TextNative;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/nativeinterface/TextNative;->mInterface:Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 16
    :cond_0
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/nativeinterface/TextNative;->mInterface:Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;

    .line 17
    return-void
.end method


# virtual methods
.method native IMakeImage(Ljava/lang/String;)I
.end method

.method native ISetInitializeInfo(Ljava/lang/String;II)Z
.end method

.method native IStopPrint()Z
.end method
