.class public Lcom/infraware/polarisoffice5/text/manager/PreviewManager;
.super Ljava/lang/Object;
.source "PreviewManager.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Size;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Type;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;
    }
.end annotation


# instance fields
.field private m_AvailableCharsetList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

.field private m_bOnlyFirstPage:Z

.field private m_bitmapCnt:I

.field private m_bitmapHeight:I

.field private m_bitmapWidth:I

.field private m_blockCnt:I

.field private m_blockText:Ljava/lang/StringBuffer;

.field private m_context:Landroid/content/Context;

.field private m_currentBlockCnt:I

.field private m_drawingTool:Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;

.field private m_fontSize:F

.field private m_lineHeight:I

.field private m_lineString:Ljava/lang/String;

.field private m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

.field private m_marginDefault:F

.field private m_marginTop:F

.field private m_oneLineWidth:F

.field private m_outputHeight:I

.field private m_outputWidth:I

.field private m_paperColor:I

.field private m_penColor:I

.field private m_requestHeight:I

.field private m_requestPage:I

.field private m_resultBitmap:Landroid/graphics/Bitmap;

.field private m_strFilePath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;III)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "strPath"    # Ljava/lang/String;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "page"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    .line 31
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_context:Landroid/content/Context;

    .line 33
    const v0, 0x42a1f5c3    # 80.98f

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_marginDefault:F

    .line 34
    const v0, 0x42b93333    # 92.6f

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_marginTop:F

    .line 42
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_AvailableCharsetList:Ljava/util/ArrayList;

    .line 43
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_resultBitmap:Landroid/graphics/Bitmap;

    .line 44
    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bitmapCnt:I

    .line 46
    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bitmapWidth:I

    .line 47
    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bitmapHeight:I

    .line 48
    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_outputWidth:I

    .line 49
    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_outputHeight:I

    .line 51
    iput v3, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockCnt:I

    .line 52
    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_currentBlockCnt:I

    .line 54
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_strFilePath:Ljava/lang/String;

    .line 55
    const/high16 v0, 0x41400000    # 12.0f

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_fontSize:F

    .line 56
    const/16 v0, 0x12

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_lineHeight:I

    .line 57
    iput v3, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_requestPage:I

    .line 58
    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_requestHeight:I

    .line 60
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_penColor:I

    .line 61
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_paperColor:I

    .line 64
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_oneLineWidth:F

    .line 65
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    .line 68
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bOnlyFirstPage:Z

    .line 69
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_drawingTool:Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;

    .line 70
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_lineString:Ljava/lang/String;

    .line 73
    iput-object p2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_strFilePath:Ljava/lang/String;

    .line 74
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_context:Landroid/content/Context;

    .line 75
    iput p4, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_requestHeight:I

    .line 76
    iput p5, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_requestPage:I

    .line 78
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->initialize()V

    .line 79
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->initializeVariable()V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;IIIZ)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "strPath"    # Ljava/lang/String;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "page"    # I
    .param p6, "forfirstpage"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    .line 31
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_context:Landroid/content/Context;

    .line 33
    const v0, 0x42a1f5c3    # 80.98f

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_marginDefault:F

    .line 34
    const v0, 0x42b93333    # 92.6f

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_marginTop:F

    .line 42
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_AvailableCharsetList:Ljava/util/ArrayList;

    .line 43
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_resultBitmap:Landroid/graphics/Bitmap;

    .line 44
    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bitmapCnt:I

    .line 46
    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bitmapWidth:I

    .line 47
    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bitmapHeight:I

    .line 48
    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_outputWidth:I

    .line 49
    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_outputHeight:I

    .line 51
    iput v3, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockCnt:I

    .line 52
    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_currentBlockCnt:I

    .line 54
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_strFilePath:Ljava/lang/String;

    .line 55
    const/high16 v0, 0x41400000    # 12.0f

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_fontSize:F

    .line 56
    const/16 v0, 0x12

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_lineHeight:I

    .line 57
    iput v3, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_requestPage:I

    .line 58
    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_requestHeight:I

    .line 60
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_penColor:I

    .line 61
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_paperColor:I

    .line 64
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_oneLineWidth:F

    .line 65
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    .line 68
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bOnlyFirstPage:Z

    .line 69
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_drawingTool:Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;

    .line 70
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_lineString:Ljava/lang/String;

    .line 83
    iput-object p2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_strFilePath:Ljava/lang/String;

    .line 84
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_context:Landroid/content/Context;

    .line 85
    iput p4, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_requestHeight:I

    .line 86
    iput p5, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_requestPage:I

    .line 88
    iput-boolean p6, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bOnlyFirstPage:Z

    .line 90
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->initialize()V

    .line 91
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->initializeVariable()V

    .line 92
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/text/manager/PreviewManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/manager/PreviewManager;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->getParentTheme()V

    return-void
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/text/manager/PreviewManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/manager/PreviewManager;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->getPaperColor()I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/text/manager/PreviewManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/manager/PreviewManager;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->getPenColor()I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/text/manager/PreviewManager;)F
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/manager/PreviewManager;

    .prologue
    .line 28
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_fontSize:F

    return v0
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/text/manager/PreviewManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/manager/PreviewManager;

    .prologue
    .line 28
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bitmapHeight:I

    return v0
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/text/manager/PreviewManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/manager/PreviewManager;

    .prologue
    .line 28
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_lineHeight:I

    return v0
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/text/manager/PreviewManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/manager/PreviewManager;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_lineString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/infraware/polarisoffice5/text/manager/PreviewManager;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/manager/PreviewManager;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->save(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private cut()V
    .locals 19

    .prologue
    .line 166
    new-instance v9, Landroid/graphics/Paint;

    invoke-direct {v9}, Landroid/graphics/Paint;-><init>()V

    .line 167
    .local v9, "measurePaint":Landroid/graphics/Paint;
    const/4 v6, 0x0

    .line 169
    .local v6, "isLast":Z
    move-object/from16 v0, p0

    iget v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bitmapWidth:I

    int-to-float v8, v15

    .line 170
    .local v8, "maxWidth":F
    const/4 v12, 0x0

    .line 172
    .local v12, "startPosition":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuffer;->length()I

    move-result v17

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v9, v15}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v15

    cmpg-float v15, v15, v8

    if-gez v15, :cond_4

    .line 173
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuffer;->length()I

    move-result v17

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 174
    .local v13, "temp":Ljava/lang/String;
    const-string/jumbo v15, "\n"

    invoke-virtual {v13, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 176
    .local v11, "splitString":[Ljava/lang/String;
    move-object v1, v11

    .local v1, "arr$":[Ljava/lang/String;
    array-length v7, v1

    .local v7, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v7, :cond_3

    aget-object v10, v1, v5

    .line 177
    .local v10, "oTemp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bOnlyFirstPage:Z

    if-eqz v15, :cond_1

    .line 178
    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_lineString:Ljava/lang/String;

    .line 179
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->getDrawingTool()Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;

    move-result-object v15

    invoke-virtual {v15}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->draw()Z

    move-result v15

    if-eqz v15, :cond_2

    .line 316
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v7    # "len$":I
    .end local v10    # "oTemp":Ljava/lang/String;
    .end local v11    # "splitString":[Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 183
    .restart local v1    # "arr$":[Ljava/lang/String;
    .restart local v5    # "i$":I
    .restart local v7    # "len$":I
    .restart local v10    # "oTemp":Ljava/lang/String;
    .restart local v11    # "splitString":[Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v15, v10}, Lcom/infraware/polarisoffice5/text/manager/LineList;->add(Ljava/lang/String;)V

    .line 176
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 186
    .end local v10    # "oTemp":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v15}, Lcom/infraware/polarisoffice5/text/manager/LineList;->finishAdd()V

    goto :goto_1

    .line 188
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v7    # "len$":I
    .end local v11    # "splitString":[Ljava/lang/String;
    .end local v13    # "temp":Ljava/lang/String;
    :cond_4
    const/4 v4, 0x0

    .line 190
    .local v4, "i":I
    :goto_2
    :try_start_0
    move-object/from16 v0, p0

    iget v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_oneLineWidth:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    move-object/from16 v16, v0

    add-int v17, v12, v4

    add-int/lit8 v17, v17, -0x1

    add-int v18, v12, v4

    invoke-virtual/range {v16 .. v18}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v16

    add-float v15, v15, v16

    move-object/from16 v0, p0

    iput v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_oneLineWidth:F

    .line 192
    move-object/from16 v0, p0

    iget v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_oneLineWidth:F

    cmpl-float v15, v15, v8

    if-gtz v15, :cond_5

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    add-int v16, v12, v4

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    const/16 v16, 0xa

    move/from16 v0, v16

    if-ne v15, v0, :cond_7

    .line 193
    :cond_5
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_oneLineWidth:F

    .line 194
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    add-int v16, v12, v4

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    const/16 v16, 0xa

    move/from16 v0, v16

    if-ne v15, v0, :cond_9

    .line 195
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    add-int v16, v12, v4

    move/from16 v0, v16

    invoke-virtual {v15, v12, v0}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 196
    .restart local v13    # "temp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bOnlyFirstPage:Z

    if-eqz v15, :cond_8

    .line 197
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_lineString:Ljava/lang/String;

    .line 198
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->getDrawingTool()Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;

    move-result-object v15

    invoke-virtual {v15}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->draw()Z

    move-result v15

    if-nez v15, :cond_0

    .line 204
    :goto_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    add-int v16, v12, v4

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    const/16 v16, 0xa

    move/from16 v0, v16

    if-ne v15, v0, :cond_6

    .line 205
    add-int/lit8 v12, v12, 0x1

    .line 206
    :cond_6
    add-int/2addr v12, v4

    .line 207
    const/4 v4, 0x0

    .line 188
    .end local v13    # "temp":Ljava/lang/String;
    :cond_7
    :goto_4
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    .line 202
    .restart local v13    # "temp":Ljava/lang/String;
    :cond_8
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v15, v13}, Lcom/infraware/polarisoffice5/text/manager/LineList;->add(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 271
    .end local v13    # "temp":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 272
    .local v3, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_currentBlockCnt:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockCnt:I

    move/from16 v16, v0

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, v16

    if-ge v15, v0, :cond_13

    .line 275
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    invoke-virtual {v15, v12}, Ljava/lang/StringBuffer;->substring(I)Ljava/lang/String;

    move-result-object v13

    .line 276
    .restart local v13    # "temp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_currentBlockCnt:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_currentBlockCnt:I

    .line 277
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 278
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    invoke-virtual {v15, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 279
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_currentBlockCnt:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockTextFromFile(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 283
    const/4 v4, 0x0

    .line 284
    const/4 v12, 0x0

    .line 285
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_oneLineWidth:F

    goto :goto_4

    .line 208
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v13    # "temp":Ljava/lang/String;
    :cond_9
    :try_start_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    add-int v16, v12, v4

    add-int/lit8 v16, v16, -0x2

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    const/16 v16, 0x20

    move/from16 v0, v16

    if-eq v15, v0, :cond_10

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    add-int v16, v12, v4

    add-int/lit8 v16, v16, -0x1

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    const/16 v16, 0x20

    move/from16 v0, v16

    if-eq v15, v0, :cond_10

    .line 211
    add-int v15, v12, v4

    add-int/lit8 v2, v15, -0x2

    .line 213
    .local v2, "breakPosition":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    add-int/lit8 v16, v2, -0x1

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    const/16 v16, 0x20

    move/from16 v0, v16

    if-ne v15, v0, :cond_b

    .line 214
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    invoke-virtual {v15, v12, v2}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 215
    .restart local v13    # "temp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bOnlyFirstPage:Z

    if-eqz v15, :cond_a

    .line 216
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_lineString:Ljava/lang/String;

    .line 217
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->getDrawingTool()Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;

    move-result-object v15

    invoke-virtual {v15}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->draw()Z

    move-result v15

    if-nez v15, :cond_0

    .line 222
    :goto_5
    move v12, v2

    .line 223
    const/4 v4, 0x0

    .line 224
    goto/16 :goto_4

    .line 221
    :cond_a
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v15, v13}, Lcom/infraware/polarisoffice5/text/manager/LineList;->add(Ljava/lang/String;)V

    goto :goto_5

    .line 225
    .end local v13    # "temp":Ljava/lang/String;
    :cond_b
    const/4 v14, 0x2

    .line 227
    .local v14, "z":I
    :goto_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    sub-int v16, v2, v14

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    const/16 v16, 0x20

    move/from16 v0, v16

    if-ne v15, v0, :cond_d

    .line 228
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    sub-int v16, v2, v14

    add-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v15, v12, v0}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 229
    .restart local v13    # "temp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bOnlyFirstPage:Z

    if-eqz v15, :cond_c

    .line 230
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_lineString:Ljava/lang/String;

    .line 231
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->getDrawingTool()Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;

    move-result-object v15

    invoke-virtual {v15}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->draw()Z

    move-result v15

    if-nez v15, :cond_0

    .line 236
    :goto_7
    sub-int v15, v2, v14

    add-int/lit8 v12, v15, 0x1

    .line 237
    const/4 v4, 0x0

    .line 238
    goto/16 :goto_4

    .line 235
    :cond_c
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v15, v13}, Lcom/infraware/polarisoffice5/text/manager/LineList;->add(Ljava/lang/String;)V

    goto :goto_7

    .line 239
    .end local v13    # "temp":Ljava/lang/String;
    :cond_d
    const/16 v15, 0xa

    if-le v14, v15, :cond_f

    .line 240
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    add-int/lit8 v16, v2, 0x1

    move/from16 v0, v16

    invoke-virtual {v15, v12, v0}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 241
    .restart local v13    # "temp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bOnlyFirstPage:Z

    if-eqz v15, :cond_e

    .line 242
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_lineString:Ljava/lang/String;

    .line 243
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->getDrawingTool()Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;

    move-result-object v15

    invoke-virtual {v15}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->draw()Z

    move-result v15

    if-nez v15, :cond_0

    .line 248
    :goto_8
    add-int/lit8 v12, v2, 0x1

    .line 249
    const/4 v4, 0x0

    .line 250
    goto/16 :goto_4

    .line 247
    :cond_e
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v15, v13}, Lcom/infraware/polarisoffice5/text/manager/LineList;->add(Ljava/lang/String;)V

    goto :goto_8

    .line 252
    .end local v13    # "temp":Ljava/lang/String;
    :cond_f
    add-int/lit8 v14, v14, 0x1

    goto :goto_6

    .line 257
    .end local v2    # "breakPosition":I
    .end local v14    # "z":I
    :cond_10
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    add-int v16, v12, v4

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, v16

    invoke-virtual {v15, v12, v0}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 258
    .restart local v13    # "temp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bOnlyFirstPage:Z

    if-eqz v15, :cond_12

    .line 259
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_lineString:Ljava/lang/String;

    .line 260
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->getDrawingTool()Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;

    move-result-object v15

    invoke-virtual {v15}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->draw()Z

    move-result v15

    if-nez v15, :cond_0

    .line 265
    :goto_9
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    add-int v16, v12, v4

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    const/16 v16, 0xa

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    .line 266
    add-int/lit8 v12, v12, 0x1

    .line 267
    :cond_11
    add-int/lit8 v15, v4, -0x1

    add-int/2addr v12, v15

    .line 268
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 264
    :cond_12
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v15, v13}, Lcom/infraware/polarisoffice5/text/manager/LineList;->add(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_9

    .line 288
    .end local v13    # "temp":Ljava/lang/String;
    .restart local v3    # "e":Ljava/lang/Exception;
    :cond_13
    move-object/from16 v0, p0

    iget v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_currentBlockCnt:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockCnt:I

    move/from16 v16, v0

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_14

    if-nez v6, :cond_14

    .line 289
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    invoke-virtual {v15, v12}, Ljava/lang/StringBuffer;->substring(I)Ljava/lang/String;

    move-result-object v13

    .line 290
    .restart local v13    # "temp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 291
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    invoke-virtual {v15, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 294
    const/4 v4, 0x0

    .line 295
    const/4 v12, 0x0

    .line 296
    const/4 v6, 0x1

    .line 297
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_oneLineWidth:F

    goto/16 :goto_4

    .line 300
    .end local v13    # "temp":Ljava/lang/String;
    :cond_14
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    invoke-virtual {v15, v12}, Ljava/lang/StringBuffer;->substring(I)Ljava/lang/String;

    move-result-object v13

    .line 301
    .restart local v13    # "temp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bOnlyFirstPage:Z

    if-eqz v15, :cond_15

    .line 302
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_lineString:Ljava/lang/String;

    .line 303
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->getDrawingTool()Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;

    move-result-object v15

    invoke-virtual {v15}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->draw()Z

    move-result v15

    if-nez v15, :cond_0

    .line 308
    :goto_a
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v15}, Lcom/infraware/polarisoffice5/text/manager/LineList;->finishAdd()V

    .line 309
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_oneLineWidth:F

    goto/16 :goto_1

    .line 307
    :cond_15
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v15, v13}, Lcom/infraware/polarisoffice5/text/manager/LineList;->add(Ljava/lang/String;)V

    goto :goto_a
.end method

.method private draw()V
    .locals 17

    .prologue
    .line 345
    move-object/from16 v0, p0

    iget v3, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bitmapWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bitmapHeight:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 347
    .local v8, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v8}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 348
    .local v1, "canvas":Landroid/graphics/Canvas;
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 350
    .local v7, "textpaint":Landroid/graphics/Paint;
    invoke-direct/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->getParentTheme()V

    .line 352
    invoke-direct/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->getPaperColor()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 354
    sget-object v3, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v16

    .line 356
    .local v16, "tf":Landroid/graphics/Typeface;
    invoke-direct/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->getPenColor()I

    move-result v3

    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 357
    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 358
    move-object/from16 v0, p0

    iget v3, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_fontSize:F

    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 360
    move-object/from16 v0, p0

    iget v13, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bitmapHeight:I

    .line 361
    .local v13, "maxHeight":I
    move-object/from16 v0, p0

    iget v11, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_lineHeight:I

    .line 362
    .local v11, "lineHeight":I
    const/4 v12, 0x0

    .line 363
    .local v12, "lineNum":I
    const/4 v15, 0x0

    .line 367
    .local v15, "size":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/manager/LineList;->get()Ljava/lang/String;

    move-result-object v14

    .line 368
    .local v14, "result":Ljava/lang/String;
    if-eqz v14, :cond_0

    .line 369
    add-int/lit8 v15, v15, 0x1

    .line 372
    goto :goto_0

    .line 374
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/manager/LineList;->reset()V

    .line 376
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    if-ge v10, v15, :cond_3

    .line 377
    add-int/lit8 v3, v12, 0x1

    mul-int/2addr v3, v11

    if-ge v3, v13, :cond_1

    .line 378
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/manager/LineList;->get()Ljava/lang/String;

    move-result-object v2

    .line 379
    .local v2, "temp":Ljava/lang/String;
    add-int/lit8 v12, v12, 0x1

    .line 380
    const/4 v3, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x0

    mul-int v6, v11, v12

    int-to-float v6, v6

    invoke-virtual/range {v1 .. v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    .line 376
    :goto_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 383
    .end local v2    # "temp":Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bitmapCnt:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bitmapCnt:I

    .line 384
    move-object/from16 v0, p0

    iget v3, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bitmapCnt:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_requestPage:I

    if-ne v3, v4, :cond_2

    .line 385
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->save(Landroid/graphics/Bitmap;)V

    .line 401
    :goto_3
    return-void

    .line 388
    :cond_2
    new-instance v9, Landroid/graphics/Paint;

    invoke-direct {v9}, Landroid/graphics/Paint;-><init>()V

    .line 389
    .local v9, "clearPaint":Landroid/graphics/Paint;
    invoke-direct/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->getPaperColor()I

    move-result v3

    invoke-virtual {v9, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 390
    invoke-virtual {v1, v9}, Landroid/graphics/Canvas;->drawPaint(Landroid/graphics/Paint;)V

    .line 391
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/manager/LineList;->get()Ljava/lang/String;

    move-result-object v2

    .line 392
    .restart local v2    # "temp":Ljava/lang/String;
    const/4 v12, 0x1

    .line 393
    const/4 v3, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x0

    mul-int v6, v11, v12

    int-to-float v6, v6

    invoke-virtual/range {v1 .. v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    goto :goto_2

    .line 397
    .end local v2    # "temp":Ljava/lang/String;
    .end local v9    # "clearPaint":Landroid/graphics/Paint;
    :cond_3
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->save(Landroid/graphics/Bitmap;)V

    .line 399
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    .line 400
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bitmapCnt:I

    goto :goto_3
.end method

.method private getPaperColor()I
    .locals 1

    .prologue
    .line 341
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_paperColor:I

    return v0
.end method

.method private getParentTheme()V
    .locals 3

    .prologue
    .line 319
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_context:Landroid/content/Context;

    check-cast v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBackgroundTheme()I

    move-result v0

    .line 320
    .local v0, "theme":I
    packed-switch v0, :pswitch_data_0

    .line 334
    :goto_0
    return-void

    .line 322
    :pswitch_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0501af

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_penColor:I

    .line 323
    const v1, -0xb0b0c

    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_paperColor:I

    goto :goto_0

    .line 326
    :pswitch_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0501b0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_penColor:I

    .line 327
    const v1, -0xe1053

    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_paperColor:I

    goto :goto_0

    .line 330
    :pswitch_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0501b1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_penColor:I

    .line 331
    const v1, -0xdadadb

    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_paperColor:I

    goto :goto_0

    .line 320
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private getPenColor()I
    .locals 1

    .prologue
    .line 337
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_penColor:I

    return v0
.end method

.method private initialize()V
    .locals 6

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->loadAvailableCharsetList()V

    .line 97
    const-string/jumbo v1, "euc-kr"

    .line 98
    .local v1, "strCharset":Ljava/lang/String;
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_context:Landroid/content/Context;

    const/16 v4, 0x17

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, Lcom/infraware/common/config/RuntimeConfig;->getIntPreference(Landroid/content/Context;II)I

    move-result v0

    .line 99
    .local v0, "nEncoding":I
    if-eqz v0, :cond_0

    .line 100
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_AvailableCharsetList:Ljava/util/ArrayList;

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "strCharset":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 101
    .restart local v1    # "strCharset":Ljava/lang/String;
    :cond_0
    new-instance v2, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_context:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    .line 103
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_strFilePath:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 104
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_strFilePath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->initBufferFromFile(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 119
    :cond_1
    :goto_0
    return-void

    .line 108
    :cond_2
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    iget v3, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_currentBlockCnt:I

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->loadBuffer(I)V

    .line 111
    :cond_3
    iget v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_marginDefault:F

    iget v3, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_marginTop:F

    add-float/2addr v2, v3

    float-to-int v2, v2

    rsub-int v2, v2, 0x34a

    iput v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bitmapHeight:I

    .line 112
    iget v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_marginDefault:F

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    rsub-int v2, v2, 0x253

    iput v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bitmapWidth:I

    .line 113
    const/16 v2, 0x34a

    iput v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_outputHeight:I

    .line 114
    const/16 v2, 0x253

    iput v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_outputWidth:I

    .line 116
    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bOnlyFirstPage:Z

    if-eqz v2, :cond_1

    .line 117
    new-instance v2, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;-><init>(Lcom/infraware/polarisoffice5/text/manager/PreviewManager;)V

    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_drawingTool:Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;

    goto :goto_0
.end method

.method private initializeVariable()V
    .locals 3

    .prologue
    .line 138
    const v0, 0x42b93333    # 92.6f

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_marginTop:F

    .line 139
    const v0, 0x42a1f5c3    # 80.98f

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_marginDefault:F

    .line 141
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    .line 142
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockText:Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    iget v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_currentBlockCnt:I

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockTextFromFile(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 144
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCount()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_blockCnt:I

    .line 146
    new-instance v0, Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-direct {v0}, Lcom/infraware/polarisoffice5/text/manager/LineList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    .line 147
    return-void
.end method

.method private loadAvailableCharsetList()V
    .locals 4

    .prologue
    .line 122
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_AvailableCharsetList:Ljava/util/ArrayList;

    .line 124
    invoke-static {}, Lcom/infraware/common/util/text/CharsetDetector;->getAllDetectableCharsets()[Ljava/lang/String;

    move-result-object v0

    .line 125
    .local v0, "encodingItems":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 126
    aget-object v2, v0, v1

    invoke-static {v2}, Ljava/nio/charset/Charset;->isSupported(Ljava/lang/String;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 127
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_AvailableCharsetList:Ljava/util/ArrayList;

    aget-object v3, v0, v1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 129
    :cond_1
    return-void
.end method

.method private makeProcess()V
    .locals 1

    .prologue
    .line 154
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->cut()V

    .line 155
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->draw()V

    .line 158
    :try_start_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/LineList;->finishGet()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    :goto_0
    return-void

    .line 159
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private declared-synchronized save(Landroid/graphics/Bitmap;)V
    .locals 9
    .param p1, "incomingBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 404
    monitor-enter p0

    :try_start_0
    iget v4, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_outputWidth:I

    iget v5, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_outputHeight:I

    sget-object v6, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 405
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 406
    .local v1, "canvas":Landroid/graphics/Canvas;
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->getPaperColor()I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 407
    new-instance v3, Landroid/graphics/Rect;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 408
    .local v3, "rectStart":Landroid/graphics/Rect;
    new-instance v2, Landroid/graphics/Rect;

    iget v4, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_marginDefault:F

    float-to-int v4, v4

    iget v5, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_marginTop:F

    float-to-int v5, v5

    iget v6, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_marginDefault:F

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    int-to-float v7, v7

    add-float/2addr v6, v7

    float-to-int v6, v6

    iget v7, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_marginTop:F

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    int-to-float v8, v8

    add-float/2addr v7, v8

    float-to-int v7, v7

    invoke-direct {v2, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 409
    .local v2, "rectDst":Landroid/graphics/Rect;
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v1, p1, v3, v2, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 412
    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_resultBitmap:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413
    monitor-exit p0

    return-void

    .line 404
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    .end local v2    # "rectDst":Landroid/graphics/Rect;
    .end local v3    # "rectStart":Landroid/graphics/Rect;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method


# virtual methods
.method public getDrawingTool()Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_drawingTool:Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;

    return-object v0
.end method

.method public getPreview()Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    .line 416
    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bOnlyFirstPage:Z

    if-nez v2, :cond_0

    .line 417
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->makeProcess()V

    .line 419
    :cond_0
    iget v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_requestHeight:I

    mul-int/lit16 v2, v2, 0x253

    div-int/lit16 v1, v2, 0x34a

    .line 420
    .local v1, "requestWidth":I
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_resultBitmap:Landroid/graphics/Bitmap;

    iget v3, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_requestHeight:I

    const/4 v4, 0x1

    invoke-static {v2, v1, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 422
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1

    .line 425
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object v0

    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public makeOnePage()V
    .locals 0

    .prologue
    .line 150
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->cut()V

    .line 151
    return-void
.end method
