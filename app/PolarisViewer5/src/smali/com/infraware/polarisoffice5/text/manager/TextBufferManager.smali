.class public Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;
.super Ljava/lang/Object;
.source "TextBufferManager.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Path;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Size;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$StringReference;


# instance fields
.field private m_BlockCharCountList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private m_BlockOffsetList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/infraware/polarisoffice5/text/manager/TextBlock;",
            ">;"
        }
    .end annotation
.end field

.field private m_Buffer:Ljava/lang/StringBuffer;

.field private m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

.field private m_bBufferChanged:Z

.field private m_bIsNewFile:Z

.field private m_nBufferBlockCount:I

.field private m_nBufferBlockEnd:I

.field private m_nBufferBlockStart:I

.field private m_nChangedBlock:I

.field private m_nLastBlockLen:I

.field private m_nLoadingBlock:I

.field private m_nLoadingBufLen:I

.field private m_nTempFileCount:I

.field private m_nTotalTextLen:I

.field private m_stopReading:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private m_strLoadingEncoding:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "strEncoding"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-string/jumbo v2, ""

    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_strLoadingEncoding:Ljava/lang/String;

    .line 20
    iput-object v4, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    .line 21
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    .line 22
    const/4 v2, 0x1

    iput v2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockCount:I

    .line 23
    iput v3, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockStart:I

    .line 24
    iput v3, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockEnd:I

    .line 25
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_bBufferChanged:Z

    .line 26
    iput v3, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nLastBlockLen:I

    .line 27
    iput-object v4, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_stopReading:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 28
    iput v3, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nLoadingBlock:I

    .line 29
    const/4 v2, -0x1

    iput v2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nChangedBlock:I

    .line 31
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_bIsNewFile:Z

    .line 32
    iput v3, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTempFileCount:I

    .line 33
    iput v3, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTotalTextLen:I

    .line 40
    check-cast p1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .line 41
    iput-object p2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_strLoadingEncoding:Ljava/lang/String;

    .line 42
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    .line 44
    new-instance v1, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    invoke-direct {v1}, Lcom/infraware/polarisoffice5/text/manager/TextBlock;-><init>()V

    .line 45
    .local v1, "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 47
    new-instance v0, Ljava/io/File;

    sget-object v2, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->OFFICE_ROOT_PATH:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 48
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 49
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 51
    :cond_0
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_stopReading:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 53
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockCharCountList:Ljava/util/ArrayList;

    .line 54
    return-void
.end method

.method private calcBlockCharCount()V
    .locals 5

    .prologue
    .line 1307
    const/4 v1, 0x0

    .line 1309
    .local v1, "offset":I
    const/4 v2, 0x0

    .line 1311
    .local v2, "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 1313
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    check-cast v2, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    .line 1314
    .restart local v2    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    iget v3, v2, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    add-int/2addr v1, v3

    .line 1315
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockCharCountList:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1311
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1317
    :cond_0
    return-void
.end method

.method private isUnicode16Charset(Ljava/nio/ByteBuffer;)Z
    .locals 6
    .param p1, "buffer"    # Ljava/nio/ByteBuffer;

    .prologue
    const/4 v5, -0x1

    const/4 v4, -0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 57
    const/4 v3, 0x2

    new-array v0, v3, [B

    .line 58
    .local v0, "bytes":[B
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 59
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 61
    aget-byte v3, v0, v1

    if-ne v3, v5, :cond_0

    aget-byte v3, v0, v2

    if-eq v3, v4, :cond_1

    :cond_0
    aget-byte v3, v0, v1

    if-ne v3, v4, :cond_2

    aget-byte v3, v0, v2

    if-ne v3, v5, :cond_2

    :cond_1
    move v1, v2

    .line 65
    :cond_2
    return v1
.end method


# virtual methods
.method public checkMergeBlock(II)I
    .locals 8
    .param p1, "nChangedBlock"    # I
    .param p2, "nChangedSize"    # I

    .prologue
    const-wide v6, 0x40b1940000000000L    # 4500.0

    .line 1077
    const/4 v3, 0x0

    .line 1078
    .local v3, "nMergeBlockSize":I
    const/4 v0, 0x0

    .line 1079
    .local v0, "bMerge":Z
    const/4 v2, -0x1

    .line 1080
    .local v2, "nMergeBlock":I
    const/4 v1, -0x1

    .line 1083
    .local v1, "nCase":I
    if-lez p1, :cond_3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge p1, v4, :cond_3

    .line 1084
    const/4 v1, 0x0

    .line 1085
    add-int/lit8 v4, p1, -0x1

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCharCount(I)I

    move-result v4

    add-int v3, v4, p2

    .line 1086
    int-to-double v4, v3

    cmpg-double v4, v4, v6

    if-gez v4, :cond_0

    .line 1087
    const/4 v0, 0x1

    .line 1088
    add-int/lit8 v2, p1, -0x1

    .line 1090
    :cond_0
    add-int/lit8 v4, p1, 0x1

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCharCount(I)I

    move-result v4

    add-int v3, v4, p2

    .line 1091
    if-nez v0, :cond_1

    int-to-double v4, v3

    cmpg-double v4, v4, v6

    if-gez v4, :cond_1

    .line 1092
    const/4 v0, 0x1

    .line 1093
    add-int/lit8 v2, p1, 0x1

    .line 1114
    :cond_1
    :goto_0
    if-nez v0, :cond_2

    int-to-double v4, p2

    const-wide v6, 0x4097700000000000L    # 1500.0

    cmpg-double v4, v4, v6

    if-gez v4, :cond_2

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_2

    .line 1115
    const/4 v0, 0x1

    .line 1116
    packed-switch v1, :pswitch_data_0

    .line 1124
    :goto_1
    const/4 v0, 0x0

    .line 1128
    :cond_2
    :goto_2
    return v2

    .line 1096
    :cond_3
    if-nez p1, :cond_4

    .line 1097
    const/4 v1, 0x1

    .line 1098
    add-int/lit8 v4, p1, 0x1

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCharCount(I)I

    move-result v4

    add-int v3, v4, p2

    .line 1099
    int-to-double v4, v3

    cmpg-double v4, v4, v6

    if-gez v4, :cond_1

    .line 1100
    const/4 v0, 0x1

    .line 1101
    add-int/lit8 v2, p1, 0x1

    goto :goto_0

    .line 1105
    :cond_4
    const/4 v1, 0x2

    .line 1106
    add-int/lit8 v4, p1, -0x1

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCharCount(I)I

    move-result v4

    add-int v3, v4, p2

    .line 1107
    int-to-double v4, v3

    cmpg-double v4, v4, v6

    if-gez v4, :cond_1

    .line 1108
    const/4 v0, 0x1

    .line 1109
    add-int/lit8 v2, p1, -0x1

    goto :goto_0

    .line 1119
    :pswitch_0
    add-int/lit8 v2, p1, -0x1

    .line 1120
    goto :goto_2

    .line 1122
    :pswitch_1
    add-int/lit8 v2, p1, 0x1

    goto :goto_1

    .line 1116
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public deleteMemBuffer(III)V
    .locals 5
    .param p1, "nBlock"    # I
    .param p2, "nStartPos"    # I
    .param p3, "nEndPos"    # I

    .prologue
    .line 1131
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_bBufferChanged:Z

    .line 1133
    iput p1, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nChangedBlock:I

    .line 1134
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCharCount(I)I

    move-result v2

    if-lt p2, v2, :cond_0

    .line 1135
    add-int/lit8 v2, p1, 0x1

    iput v2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nChangedBlock:I

    .line 1138
    :cond_0
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockOffsetInBuffer(I)I

    move-result v1

    .line 1141
    .local v1, "nBlockOffset":I
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    add-int v3, v1, p2

    add-int v4, v1, p3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 1144
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v2, p1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    iget v0, v2, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    .line 1145
    .local v0, "nBlockLen":I
    sub-int v2, p3, p2

    sub-int/2addr v0, v2

    .line 1146
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v2, p1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    iput v0, v2, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    .line 1148
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBufferChangedLen()I

    move-result v2

    const/16 v3, 0xbb8

    if-lt v2, v3, :cond_1

    .line 1149
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->loadBuffer(I)V

    .line 1150
    :cond_1
    return-void
.end method

.method public getBlockCharCount(I)I
    .locals 4
    .param p1, "nBlock"    # I

    .prologue
    .line 1321
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-ge p1, v2, :cond_0

    if-gez p1, :cond_1

    .line 1322
    :cond_0
    const/4 v0, -0x1

    .line 1326
    :goto_0
    return v0

    .line 1324
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v2, p1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    .line 1325
    .local v1, "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    iget v0, v1, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    .line 1326
    .local v0, "nCharCount":I
    goto :goto_0
.end method

.method public getBlockCount()I
    .locals 1

    .prologue
    .line 1156
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    return v0
.end method

.method public getBlockIndexFromOffset(I)I
    .locals 3
    .param p1, "nOffset"    # I

    .prologue
    .line 1212
    const/4 v1, 0x0

    .line 1216
    .local v1, "nBlock":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockCharCountList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1217
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockCharCountList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-gt p1, v2, :cond_1

    .line 1219
    move v1, v0

    .line 1232
    :cond_0
    return v1

    .line 1216
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getBlockOffsetFromOffset(I)I
    .locals 4
    .param p1, "nOffset"    # I

    .prologue
    .line 1237
    const/4 v1, 0x0

    .line 1240
    .local v1, "nBlockOffset":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 1241
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    .line 1242
    .local v2, "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    iget v3, v2, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    if-gt p1, v3, :cond_1

    .line 1243
    move v1, p1

    .line 1250
    .end local v2    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :cond_0
    return v1

    .line 1247
    .restart local v2    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :cond_1
    iget v3, v2, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    sub-int/2addr p1, v3

    .line 1240
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getBlockOffsetInBuffer(I)I
    .locals 6
    .param p1, "nBlock"    # I

    .prologue
    .line 1255
    const/4 v2, 0x0

    .line 1257
    .local v2, "nStart":I
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    if-eqz v5, :cond_0

    iget v5, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockStart:I

    if-gt v5, p1, :cond_0

    iget v5, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockEnd:I

    if-ge v5, p1, :cond_1

    :cond_0
    move v3, v2

    .line 1270
    .end local v2    # "nStart":I
    .local v3, "nStart":I
    :goto_0
    return v3

    .line 1260
    .end local v3    # "nStart":I
    .restart local v2    # "nStart":I
    :cond_1
    iget v1, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockStart:I

    .local v1, "i":I
    :goto_1
    if-ge v1, p1, :cond_2

    .line 1261
    const/4 v4, 0x0

    .line 1263
    .local v4, "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :try_start_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v5, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    check-cast v4, Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1267
    .restart local v4    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    iget v5, v4, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    add-int/2addr v2, v5

    .line 1260
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1264
    .end local v4    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :catch_0
    move-exception v0

    :cond_2
    move v3, v2

    .line 1270
    .end local v2    # "nStart":I
    .restart local v3    # "nStart":I
    goto :goto_0
.end method

.method public getBlockOffsetInFile(I)I
    .locals 2
    .param p1, "nBlock"    # I

    .prologue
    const/4 v0, 0x0

    .line 1275
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockCharCountList:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 1289
    :cond_0
    :goto_0
    return v0

    .line 1279
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockCharCountList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1282
    if-lez p1, :cond_0

    .line 1285
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockCharCountList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_2

    .line 1286
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockCharCountList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockCharCountList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 1289
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockCharCountList:Ljava/util/ArrayList;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public getBlockOffsetList()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/infraware/polarisoffice5/text/manager/TextBlock;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1337
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    return-object v0
.end method

.method public getBlockText(I)Ljava/lang/String;
    .locals 5
    .param p1, "nBlock"    # I

    .prologue
    .line 297
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockStart:I

    if-gt v4, p1, :cond_0

    iget v4, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockEnd:I

    if-ge v4, p1, :cond_1

    .line 298
    :cond_0
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->loadBuffer(I)V

    .line 300
    :cond_1
    const-string/jumbo v3, ""

    .line 301
    .local v3, "strBlockText":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockOffsetInBuffer(I)I

    move-result v2

    .line 305
    .local v2, "nStart":I
    if-ltz v2, :cond_3

    :try_start_0
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-gt v2, v4, :cond_3

    .line 306
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v4, p1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    iget v4, v4, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    add-int v1, v2, v4

    .line 307
    .local v1, "nEnd":I
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-le v1, v4, :cond_2

    .line 308
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    .line 309
    :cond_2
    sub-int v4, v1, v2

    if-lez v4, :cond_3

    .line 310
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v4, v2, v1}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 316
    .end local v1    # "nEnd":I
    :cond_3
    :goto_0
    return-object v3

    .line 312
    :catch_0
    move-exception v0

    .line 313
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string/jumbo v3, ""

    goto :goto_0
.end method

.method public getBlockTextFromFile(I)Ljava/lang/String;
    .locals 19
    .param p1, "nBlock"    # I

    .prologue
    .line 320
    const-string/jumbo v13, ""

    .line 324
    .local v13, "strBlockText":Ljava/lang/String;
    const/4 v2, 0x0

    .line 325
    .local v2, "bufferChannel":Ljava/nio/channels/FileChannel;
    const/4 v9, 0x0

    .line 327
    .local v9, "raf":Ljava/io/RandomAccessFile;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/LinkedList;->size()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NegativeArraySizeException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v17

    add-int/lit8 v17, v17, -0x1

    move/from16 v0, p1

    move/from16 v1, v17

    if-le v0, v1, :cond_2

    .line 365
    if-eqz v2, :cond_0

    :try_start_1
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 366
    :cond_0
    if-eqz v9, :cond_1

    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_0
    move-object v14, v13

    .line 372
    .end local v13    # "strBlockText":Ljava/lang/String;
    .local v14, "strBlockText":Ljava/lang/String;
    :goto_1
    return-object v14

    .line 367
    .end local v14    # "strBlockText":Ljava/lang/String;
    .restart local v13    # "strBlockText":Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 369
    .local v5, "e1":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 330
    .end local v5    # "e1":Ljava/io/IOException;
    :cond_2
    const/4 v7, 0x0

    .local v7, "nStart":I
    const/4 v6, 0x0

    .line 332
    .local v6, "nEnd":I
    if-gez p1, :cond_3

    .line 333
    const/16 p1, 0x0

    .line 334
    :cond_3
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    .line 335
    .local v16, "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    move-object/from16 v0, v16

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nFileNum:I

    move/from16 v17, v0

    if-nez v17, :cond_7

    .line 336
    sget-object v15, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->PATH_TEXTBUFFER:Ljava/lang/String;

    .line 340
    .local v15, "strTempFileName":Ljava/lang/String;
    :goto_2
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 341
    .local v3, "bufferFile":Ljava/io/File;
    new-instance v10, Ljava/io/RandomAccessFile;

    const-string/jumbo v17, "rw"

    move-object/from16 v0, v17

    invoke-direct {v10, v3, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/NegativeArraySizeException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 342
    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .local v10, "raf":Ljava/io/RandomAccessFile;
    :try_start_3
    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    .line 344
    move-object/from16 v0, v16

    iget v7, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nStartOffset:I

    .line 345
    move-object/from16 v0, v16

    iget v6, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nEndOffset:I

    .line 347
    sub-int v17, v6, v7

    invoke-static/range {v17 .. v17}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v11

    .line 348
    .local v11, "readBuf":Ljava/nio/ByteBuffer;
    const-wide/16 v17, 0x0

    move-wide/from16 v0, v17

    invoke-virtual {v2, v0, v1}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 349
    int-to-long v0, v7

    move-wide/from16 v17, v0

    move-wide/from16 v0, v17

    invoke-virtual {v2, v11, v0, v1}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;J)I

    move-result v8

    .line 350
    .local v8, "numRead":I
    new-array v12, v8, [B

    .line 351
    .local v12, "readBytes":[B
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 352
    invoke-virtual {v11, v12}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 353
    new-instance v14, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_strLoadingEncoding:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-direct {v14, v12, v0}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_9
    .catch Ljava/lang/NegativeArraySizeException; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 355
    .end local v13    # "strBlockText":Ljava/lang/String;
    .restart local v14    # "strBlockText":Ljava/lang/String;
    :try_start_4
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 356
    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_a
    .catch Ljava/lang/NegativeArraySizeException; {:try_start_4 .. :try_end_4} :catch_8
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 365
    if-eqz v2, :cond_4

    :try_start_5
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 366
    :cond_4
    if-eqz v10, :cond_5

    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    :cond_5
    move-object v9, v10

    .end local v10    # "raf":Ljava/io/RandomAccessFile;
    .restart local v9    # "raf":Ljava/io/RandomAccessFile;
    move-object v13, v14

    .end local v3    # "bufferFile":Ljava/io/File;
    .end local v6    # "nEnd":I
    .end local v7    # "nStart":I
    .end local v8    # "numRead":I
    .end local v11    # "readBuf":Ljava/nio/ByteBuffer;
    .end local v12    # "readBytes":[B
    .end local v14    # "strBlockText":Ljava/lang/String;
    .end local v15    # "strTempFileName":Ljava/lang/String;
    .end local v16    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    .restart local v13    # "strBlockText":Ljava/lang/String;
    :cond_6
    :goto_3
    move-object v14, v13

    .line 372
    .end local v13    # "strBlockText":Ljava/lang/String;
    .restart local v14    # "strBlockText":Ljava/lang/String;
    goto :goto_1

    .line 338
    .end local v14    # "strBlockText":Ljava/lang/String;
    .restart local v6    # "nEnd":I
    .restart local v7    # "nStart":I
    .restart local v13    # "strBlockText":Ljava/lang/String;
    .restart local v16    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :cond_7
    :try_start_6
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v18, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->PATH_TEXTBUFFER:Ljava/lang/String;

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v16

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nFileNum:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/NegativeArraySizeException; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v15

    .restart local v15    # "strTempFileName":Ljava/lang/String;
    goto :goto_2

    .line 367
    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .end local v13    # "strBlockText":Ljava/lang/String;
    .restart local v3    # "bufferFile":Ljava/io/File;
    .restart local v8    # "numRead":I
    .restart local v10    # "raf":Ljava/io/RandomAccessFile;
    .restart local v11    # "readBuf":Ljava/nio/ByteBuffer;
    .restart local v12    # "readBytes":[B
    .restart local v14    # "strBlockText":Ljava/lang/String;
    :catch_1
    move-exception v5

    .line 369
    .restart local v5    # "e1":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    move-object v9, v10

    .end local v10    # "raf":Ljava/io/RandomAccessFile;
    .restart local v9    # "raf":Ljava/io/RandomAccessFile;
    move-object v13, v14

    .line 371
    .end local v14    # "strBlockText":Ljava/lang/String;
    .restart local v13    # "strBlockText":Ljava/lang/String;
    goto :goto_3

    .line 357
    .end local v3    # "bufferFile":Ljava/io/File;
    .end local v5    # "e1":Ljava/io/IOException;
    .end local v6    # "nEnd":I
    .end local v7    # "nStart":I
    .end local v8    # "numRead":I
    .end local v11    # "readBuf":Ljava/nio/ByteBuffer;
    .end local v12    # "readBytes":[B
    .end local v15    # "strTempFileName":Ljava/lang/String;
    .end local v16    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :catch_2
    move-exception v4

    .line 358
    .local v4, "e":Ljava/io/IOException;
    :goto_4
    :try_start_7
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 365
    if-eqz v2, :cond_8

    :try_start_8
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 366
    :cond_8
    if-eqz v9, :cond_6

    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    goto :goto_3

    .line 367
    :catch_3
    move-exception v5

    .line 369
    .restart local v5    # "e1":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 359
    .end local v4    # "e":Ljava/io/IOException;
    .end local v5    # "e1":Ljava/io/IOException;
    :catch_4
    move-exception v4

    .line 360
    .local v4, "e":Ljava/lang/NegativeArraySizeException;
    :goto_5
    :try_start_9
    invoke-virtual {v4}, Ljava/lang/NegativeArraySizeException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 365
    if-eqz v2, :cond_9

    :try_start_a
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 366
    :cond_9
    if-eqz v9, :cond_6

    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    goto :goto_3

    .line 367
    :catch_5
    move-exception v5

    .line 369
    .restart local v5    # "e1":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 364
    .end local v4    # "e":Ljava/lang/NegativeArraySizeException;
    .end local v5    # "e1":Ljava/io/IOException;
    :catchall_0
    move-exception v17

    .line 365
    :goto_6
    if-eqz v2, :cond_a

    :try_start_b
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 366
    :cond_a
    if-eqz v9, :cond_b

    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6

    .line 370
    :cond_b
    :goto_7
    throw v17

    .line 367
    :catch_6
    move-exception v5

    .line 369
    .restart local v5    # "e1":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 364
    .end local v5    # "e1":Ljava/io/IOException;
    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .restart local v3    # "bufferFile":Ljava/io/File;
    .restart local v6    # "nEnd":I
    .restart local v7    # "nStart":I
    .restart local v10    # "raf":Ljava/io/RandomAccessFile;
    .restart local v15    # "strTempFileName":Ljava/lang/String;
    .restart local v16    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :catchall_1
    move-exception v17

    move-object v9, v10

    .end local v10    # "raf":Ljava/io/RandomAccessFile;
    .restart local v9    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_6

    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .end local v13    # "strBlockText":Ljava/lang/String;
    .restart local v8    # "numRead":I
    .restart local v10    # "raf":Ljava/io/RandomAccessFile;
    .restart local v11    # "readBuf":Ljava/nio/ByteBuffer;
    .restart local v12    # "readBytes":[B
    .restart local v14    # "strBlockText":Ljava/lang/String;
    :catchall_2
    move-exception v17

    move-object v9, v10

    .end local v10    # "raf":Ljava/io/RandomAccessFile;
    .restart local v9    # "raf":Ljava/io/RandomAccessFile;
    move-object v13, v14

    .end local v14    # "strBlockText":Ljava/lang/String;
    .restart local v13    # "strBlockText":Ljava/lang/String;
    goto :goto_6

    .line 359
    .end local v8    # "numRead":I
    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .end local v11    # "readBuf":Ljava/nio/ByteBuffer;
    .end local v12    # "readBytes":[B
    .restart local v10    # "raf":Ljava/io/RandomAccessFile;
    :catch_7
    move-exception v4

    move-object v9, v10

    .end local v10    # "raf":Ljava/io/RandomAccessFile;
    .restart local v9    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_5

    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .end local v13    # "strBlockText":Ljava/lang/String;
    .restart local v8    # "numRead":I
    .restart local v10    # "raf":Ljava/io/RandomAccessFile;
    .restart local v11    # "readBuf":Ljava/nio/ByteBuffer;
    .restart local v12    # "readBytes":[B
    .restart local v14    # "strBlockText":Ljava/lang/String;
    :catch_8
    move-exception v4

    move-object v9, v10

    .end local v10    # "raf":Ljava/io/RandomAccessFile;
    .restart local v9    # "raf":Ljava/io/RandomAccessFile;
    move-object v13, v14

    .end local v14    # "strBlockText":Ljava/lang/String;
    .restart local v13    # "strBlockText":Ljava/lang/String;
    goto :goto_5

    .line 357
    .end local v8    # "numRead":I
    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .end local v11    # "readBuf":Ljava/nio/ByteBuffer;
    .end local v12    # "readBytes":[B
    .restart local v10    # "raf":Ljava/io/RandomAccessFile;
    :catch_9
    move-exception v4

    move-object v9, v10

    .end local v10    # "raf":Ljava/io/RandomAccessFile;
    .restart local v9    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_4

    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .end local v13    # "strBlockText":Ljava/lang/String;
    .restart local v8    # "numRead":I
    .restart local v10    # "raf":Ljava/io/RandomAccessFile;
    .restart local v11    # "readBuf":Ljava/nio/ByteBuffer;
    .restart local v12    # "readBytes":[B
    .restart local v14    # "strBlockText":Ljava/lang/String;
    :catch_a
    move-exception v4

    move-object v9, v10

    .end local v10    # "raf":Ljava/io/RandomAccessFile;
    .restart local v9    # "raf":Ljava/io/RandomAccessFile;
    move-object v13, v14

    .end local v14    # "strBlockText":Ljava/lang/String;
    .restart local v13    # "strBlockText":Ljava/lang/String;
    goto :goto_4
.end method

.method public getBufferChanged()Z
    .locals 1

    .prologue
    .line 1176
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_bBufferChanged:Z

    return v0
.end method

.method public getBufferChangedLen()I
    .locals 2

    .prologue
    .line 1180
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    iget v1, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nLoadingBufLen:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    return v0
.end method

.method public getLastBlockLen(Z)I
    .locals 4
    .param p1, "bBuffer"    # Z

    .prologue
    .line 1164
    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_bBufferChanged:Z

    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    .line 1165
    iget v2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nLoadingBlock:I

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->loadBuffer(I)V

    .line 1167
    :cond_0
    const/4 v0, 0x0

    .line 1168
    .local v0, "nLastBlockLen":I
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    .line 1169
    .local v1, "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    iget v0, v1, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    .line 1170
    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nLastBlockLen:I

    .line 1172
    iget v2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nLastBlockLen:I

    return v2
.end method

.method public getStopFlag()Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_stopReading:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method public getSubBufferText(III)Ljava/lang/String;
    .locals 6
    .param p1, "nBlock"    # I
    .param p2, "nStartPos"    # I
    .param p3, "nEndPos"    # I

    .prologue
    .line 615
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    if-eqz v5, :cond_0

    iget v5, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockStart:I

    if-gt v5, p1, :cond_0

    iget v5, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockEnd:I

    if-ge v5, p1, :cond_1

    .line 616
    :cond_0
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->loadBuffer(I)V

    .line 618
    :cond_1
    const-string/jumbo v4, ""

    .line 619
    .local v4, "strSubBuffer":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockOffsetInBuffer(I)I

    move-result v1

    .line 620
    .local v1, "nBlockOffset":I
    add-int v3, v1, p2

    .line 622
    .local v3, "nStart":I
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    if-gt v3, v5, :cond_3

    .line 623
    add-int v2, v1, p3

    .line 624
    .local v2, "nEnd":I
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    if-le v2, v5, :cond_2

    .line 625
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    .line 627
    :cond_2
    :try_start_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v5, v3, v2}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 633
    .end local v2    # "nEnd":I
    :cond_3
    :goto_0
    return-object v4

    .line 628
    .restart local v2    # "nEnd":I
    :catch_0
    move-exception v0

    .line 629
    .local v0, "e":Ljava/lang/StringIndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/StringIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0
.end method

.method public getTempFileCount()I
    .locals 1

    .prologue
    .line 1332
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTempFileCount:I

    return v0
.end method

.method public getTotalTextLen(Z)I
    .locals 5
    .param p1, "bBuffer"    # Z

    .prologue
    .line 1194
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_bBufferChanged:Z

    .line 1196
    .local v0, "bBufferChanged":Z
    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_bBufferChanged:Z

    if-eqz v3, :cond_0

    if-eqz p1, :cond_0

    .line 1197
    iget v3, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nLoadingBlock:I

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->loadBuffer(I)V

    .line 1199
    :cond_0
    const/4 v3, 0x1

    if-ne v0, v3, :cond_1

    .line 1200
    const/4 v3, 0x0

    iput v3, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTotalTextLen:I

    .line 1201
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 1202
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v3, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    .line 1203
    .local v2, "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    iget v3, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTotalTextLen:I

    iget v4, v2, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    add-int/2addr v3, v4

    iput v3, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTotalTextLen:I

    .line 1201
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1207
    .end local v1    # "i":I
    .end local v2    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :cond_1
    iget v3, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTotalTextLen:I

    return v3
.end method

.method public initBufferFromFile(Ljava/lang/String;)Z
    .locals 23
    .param p1, "strFilePath"    # Ljava/lang/String;

    .prologue
    .line 70
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTempFileCount:I

    move/from16 v20, v0

    if-lez v20, :cond_2

    .line 71
    sget-object v16, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->PATH_TEXTBUFFER:Ljava/lang/String;

    .line 72
    .local v16, "strTempFile":Ljava/lang/String;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTempFileCount:I

    move/from16 v20, v0

    move/from16 v0, v20

    if-gt v7, v0, :cond_1

    .line 73
    if-nez v7, :cond_0

    .line 74
    sget-object v16, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->PATH_TEXTBUFFER:Ljava/lang/String;

    .line 77
    :goto_1
    new-instance v20, Ljava/io/File;

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->delete()Z

    .line 72
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 76
    :cond_0
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->PATH_TEXTBUFFER:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    goto :goto_1

    .line 79
    :cond_1
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTempFileCount:I

    .line 83
    .end local v7    # "i":I
    .end local v16    # "strTempFile":Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->length()I

    move-result v20

    if-lez v20, :cond_3

    .line 84
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->length()I

    move-result v22

    invoke-virtual/range {v20 .. v22}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 87
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/LinkedList;->clear()V

    .line 90
    :try_start_0
    new-instance v5, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 91
    .local v5, "file":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    sget-object v20, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->PATH_TEXTBUFFER:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 92
    .local v3, "bufferFile":Ljava/io/File;
    new-instance v20, Ljava/io/RandomAccessFile;

    const-string/jumbo v21, "r"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v0, v5, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual/range {v20 .. v20}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v6

    .line 93
    .local v6, "fileChannel":Ljava/nio/channels/FileChannel;
    new-instance v20, Ljava/io/RandomAccessFile;

    const-string/jumbo v21, "rw"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v0, v3, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual/range {v20 .. v20}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    .line 94
    .local v2, "bufferChannel":Ljava/nio/channels/FileChannel;
    const-wide/16 v20, 0x0

    move-wide/from16 v0, v20

    invoke-virtual {v2, v0, v1}, Ljava/nio/channels/FileChannel;->truncate(J)Ljava/nio/channels/FileChannel;

    .line 95
    const-wide/16 v20, 0x0

    move-wide/from16 v0, v20

    invoke-virtual {v2, v0, v1}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 97
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTotalTextLen:I

    .line 100
    const/16 v20, 0x2ee0

    invoke-static/range {v20 .. v20}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v12

    .line 101
    .local v12, "readBuf":Ljava/nio/ByteBuffer;
    const/16 v18, 0x0

    .line 103
    .local v18, "writeBuf":Ljava/nio/ByteBuffer;
    const-string/jumbo v15, ""

    .line 104
    .local v15, "strTempEncode":Ljava/lang/String;
    const/4 v9, 0x0

    .local v9, "nStrLen":I
    const/4 v10, 0x0

    .local v10, "numRead":I
    const/4 v8, 0x0

    .line 105
    .local v8, "nStartOffset":I
    const/16 v20, 0x2

    invoke-static/range {v20 .. v20}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v11

    .line 106
    .local v11, "prefixBuf":Ljava/nio/ByteBuffer;
    invoke-virtual {v6, v11}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v10

    .line 107
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->isUnicode16Charset(Ljava/nio/ByteBuffer;)Z

    move-result v20

    if-nez v20, :cond_4

    .line 108
    const-wide/16 v20, 0x0

    move-wide/from16 v0, v20

    invoke-virtual {v6, v0, v1}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 110
    :cond_4
    :goto_2
    if-ltz v10, :cond_6

    .line 111
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getStopFlag()Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v20

    if-eqz v20, :cond_5

    .line 112
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V

    .line 113
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 114
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 115
    const/16 v20, 0x0

    .line 201
    .end local v2    # "bufferChannel":Ljava/nio/channels/FileChannel;
    .end local v3    # "bufferFile":Ljava/io/File;
    .end local v5    # "file":Ljava/io/File;
    .end local v6    # "fileChannel":Ljava/nio/channels/FileChannel;
    .end local v8    # "nStartOffset":I
    .end local v9    # "nStrLen":I
    .end local v10    # "numRead":I
    .end local v11    # "prefixBuf":Ljava/nio/ByteBuffer;
    .end local v12    # "readBuf":Ljava/nio/ByteBuffer;
    .end local v15    # "strTempEncode":Ljava/lang/String;
    .end local v18    # "writeBuf":Ljava/nio/ByteBuffer;
    :goto_3
    return v20

    .line 118
    .restart local v2    # "bufferChannel":Ljava/nio/channels/FileChannel;
    .restart local v3    # "bufferFile":Ljava/io/File;
    .restart local v5    # "file":Ljava/io/File;
    .restart local v6    # "fileChannel":Ljava/nio/channels/FileChannel;
    .restart local v8    # "nStartOffset":I
    .restart local v9    # "nStrLen":I
    .restart local v10    # "numRead":I
    .restart local v11    # "prefixBuf":Ljava/nio/ByteBuffer;
    .restart local v12    # "readBuf":Ljava/nio/ByteBuffer;
    .restart local v15    # "strTempEncode":Ljava/lang/String;
    .restart local v18    # "writeBuf":Ljava/nio/ByteBuffer;
    :cond_5
    invoke-virtual {v12}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 119
    invoke-virtual {v6, v12}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v10

    .line 120
    if-gez v10, :cond_8

    .line 121
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/LinkedList;->size()I

    move-result v20

    if-nez v20, :cond_6

    .line 122
    new-instance v17, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    invoke-direct/range {v17 .. v17}, Lcom/infraware/polarisoffice5/text/manager/TextBlock;-><init>()V

    .line 123
    .local v17, "tmpBlk":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 188
    .end local v17    # "tmpBlk":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/LinkedList;->size()I

    move-result v20

    if-nez v20, :cond_7

    .line 189
    new-instance v17, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    invoke-direct/range {v17 .. v17}, Lcom/infraware/polarisoffice5/text/manager/TextBlock;-><init>()V

    .line 190
    .restart local v17    # "tmpBlk":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 193
    .end local v17    # "tmpBlk":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :cond_7
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V

    .line 194
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 199
    .end local v2    # "bufferChannel":Ljava/nio/channels/FileChannel;
    .end local v3    # "bufferFile":Ljava/io/File;
    .end local v5    # "file":Ljava/io/File;
    .end local v6    # "fileChannel":Ljava/nio/channels/FileChannel;
    .end local v8    # "nStartOffset":I
    .end local v9    # "nStrLen":I
    .end local v10    # "numRead":I
    .end local v11    # "prefixBuf":Ljava/nio/ByteBuffer;
    .end local v12    # "readBuf":Ljava/nio/ByteBuffer;
    .end local v15    # "strTempEncode":Ljava/lang/String;
    .end local v18    # "writeBuf":Ljava/nio/ByteBuffer;
    :goto_4
    invoke-direct/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->calcBlockCharCount()V

    .line 201
    const/16 v20, 0x1

    goto :goto_3

    .line 127
    .restart local v2    # "bufferChannel":Ljava/nio/channels/FileChannel;
    .restart local v3    # "bufferFile":Ljava/io/File;
    .restart local v5    # "file":Ljava/io/File;
    .restart local v6    # "fileChannel":Ljava/nio/channels/FileChannel;
    .restart local v8    # "nStartOffset":I
    .restart local v9    # "nStrLen":I
    .restart local v10    # "numRead":I
    .restart local v11    # "prefixBuf":Ljava/nio/ByteBuffer;
    .restart local v12    # "readBuf":Ljava/nio/ByteBuffer;
    .restart local v15    # "strTempEncode":Ljava/lang/String;
    .restart local v18    # "writeBuf":Ljava/nio/ByteBuffer;
    :cond_8
    :try_start_1
    new-array v13, v10, [B

    .line 128
    .local v13, "readBytes":[B
    invoke-virtual {v12}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 129
    invoke-virtual {v12, v13}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 130
    new-instance v14, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_strLoadingEncoding:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-direct {v14, v13, v0}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 131
    .local v14, "strRead":Ljava/lang/String;
    invoke-virtual {v12}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 132
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v9

    .line 134
    const/16 v20, 0xbb8

    move/from16 v0, v20

    if-le v9, v0, :cond_9

    .line 135
    const/16 v20, 0x0

    const/16 v21, 0xbb8

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v14, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    .line 136
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_strLoadingEncoding:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v19

    .line 137
    .local v19, "writeBytes":[B
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v20, v0

    invoke-static/range {v20 .. v20}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v18

    .line 138
    invoke-virtual/range {v18 .. v19}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 139
    invoke-virtual/range {v18 .. v18}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 140
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-int v8, v0

    .line 141
    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 144
    new-instance v17, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    invoke-direct/range {v17 .. v17}, Lcom/infraware/polarisoffice5/text/manager/TextBlock;-><init>()V

    .line 145
    .restart local v17    # "tmpBlk":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTempFileCount:I

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nFileNum:I

    .line 146
    move-object/from16 v0, v17

    iput v8, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nStartOffset:I

    .line 147
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nEndOffset:I

    .line 148
    const/16 v20, 0xbb8

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    .line 149
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 151
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v20

    move-wide/from16 v0, v20

    invoke-virtual {v6, v0, v1}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 152
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTotalTextLen:I

    move/from16 v20, v0

    move/from16 v0, v20

    add-int/lit16 v0, v0, 0xbb8

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTotalTextLen:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    .line 195
    .end local v2    # "bufferChannel":Ljava/nio/channels/FileChannel;
    .end local v3    # "bufferFile":Ljava/io/File;
    .end local v5    # "file":Ljava/io/File;
    .end local v6    # "fileChannel":Ljava/nio/channels/FileChannel;
    .end local v8    # "nStartOffset":I
    .end local v9    # "nStrLen":I
    .end local v10    # "numRead":I
    .end local v11    # "prefixBuf":Ljava/nio/ByteBuffer;
    .end local v12    # "readBuf":Ljava/nio/ByteBuffer;
    .end local v13    # "readBytes":[B
    .end local v14    # "strRead":Ljava/lang/String;
    .end local v15    # "strTempEncode":Ljava/lang/String;
    .end local v17    # "tmpBlk":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    .end local v18    # "writeBuf":Ljava/nio/ByteBuffer;
    .end local v19    # "writeBytes":[B
    :catch_0
    move-exception v4

    .line 196
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_4

    .line 154
    .end local v4    # "e":Ljava/io/IOException;
    .restart local v2    # "bufferChannel":Ljava/nio/channels/FileChannel;
    .restart local v3    # "bufferFile":Ljava/io/File;
    .restart local v5    # "file":Ljava/io/File;
    .restart local v6    # "fileChannel":Ljava/nio/channels/FileChannel;
    .restart local v8    # "nStartOffset":I
    .restart local v9    # "nStrLen":I
    .restart local v10    # "numRead":I
    .restart local v11    # "prefixBuf":Ljava/nio/ByteBuffer;
    .restart local v12    # "readBuf":Ljava/nio/ByteBuffer;
    .restart local v13    # "readBytes":[B
    .restart local v14    # "strRead":Ljava/lang/String;
    .restart local v15    # "strTempEncode":Ljava/lang/String;
    .restart local v18    # "writeBuf":Ljava/nio/ByteBuffer;
    :cond_9
    :try_start_2
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-int v8, v0

    .line 155
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nLastBlockLen:I

    .line 156
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_strLoadingEncoding:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v19

    .line 157
    .restart local v19    # "writeBytes":[B
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v20, v0

    invoke-static/range {v20 .. v20}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v18

    .line 158
    invoke-virtual/range {v18 .. v19}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 159
    invoke-virtual/range {v18 .. v18}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 160
    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 162
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTotalTextLen:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nLastBlockLen:I

    move/from16 v21, v0

    add-int v20, v20, v21

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTotalTextLen:I

    .line 170
    new-instance v17, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    invoke-direct/range {v17 .. v17}, Lcom/infraware/polarisoffice5/text/manager/TextBlock;-><init>()V

    .line 171
    .restart local v17    # "tmpBlk":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTempFileCount:I

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nFileNum:I

    .line 172
    move-object/from16 v0, v17

    iput v8, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nStartOffset:I

    .line 173
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nEndOffset:I

    .line 174
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nLastBlockLen:I

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    .line 175
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_2
.end method

.method public insertMemBuffer(Ljava/lang/String;II)V
    .locals 4
    .param p1, "strText"    # Ljava/lang/String;
    .param p2, "nBlock"    # I
    .param p3, "nPos"    # I

    .prologue
    .line 639
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_bBufferChanged:Z

    .line 640
    iput p2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nChangedBlock:I

    .line 643
    invoke-virtual {p0, p2}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockOffsetInBuffer(I)I

    move-result v1

    add-int v0, v1, p3

    .line 646
    .local v0, "nStartPos":I
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v1, v0, p1}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 649
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v1, p2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    iget v2, v1, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    .line 652
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBufferChangedLen()I

    move-result v1

    const/16 v2, 0xbb8

    if-lt v1, v2, :cond_0

    .line 653
    invoke-virtual {p0, p2}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->loadBuffer(I)V

    .line 654
    :cond_0
    return-void
.end method

.method public isNewFile()Z
    .locals 1

    .prologue
    .line 1188
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_bIsNewFile:Z

    return v0
.end method

.method public loadBuffer(I)V
    .locals 22
    .param p1, "nBlock"    # I

    .prologue
    .line 472
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/LinkedList;->size()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    move/from16 v0, p1

    move/from16 v1, v19

    if-le v0, v1, :cond_0

    .line 612
    :goto_0
    return-void

    .line 475
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_bBufferChanged:Z

    move/from16 v19, v0

    if-eqz v19, :cond_1

    .line 476
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockStart:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockEnd:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->replaceFileBufferM(Ljava/lang/String;II)V

    .line 484
    :cond_1
    const/4 v10, 0x0

    .line 485
    .local v10, "nBufferBlockCount":I
    move/from16 v12, p1

    .line 486
    .local v12, "nBufferBlockStart":I
    move/from16 v11, p1

    .line 489
    .local v11, "nBufferBlockEnd":I
    const/16 v18, 0x0

    .line 490
    .local v18, "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    const/4 v15, 0x0

    .line 492
    .local v15, "nFirstStoreCount":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v18

    .end local v18    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    check-cast v18, Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 497
    .restart local v18    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :goto_1
    move-object/from16 v0, v18

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    move/from16 v19, v0

    add-int v15, v15, v19

    .line 498
    const/4 v10, 0x1

    .line 500
    const/16 v17, 0x1

    .local v17, "nStep":I
    const/4 v7, 0x0

    .line 501
    .local v7, "m":I
    const/4 v4, 0x1

    .line 502
    .local v4, "bDirection":Z
    :cond_2
    const/16 v19, 0x5208

    move/from16 v0, v19

    if-ge v15, v0, :cond_4

    .line 504
    sub-int v7, p1, v17

    .line 505
    if-ltz v7, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/LinkedList;->size()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    move/from16 v0, v19

    if-gt v7, v0, :cond_3

    .line 506
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v18

    .end local v18    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    check-cast v18, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    .line 507
    .restart local v18    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    move-object/from16 v0, v18

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    move/from16 v19, v0

    add-int v15, v15, v19

    .line 508
    invoke-static {v7, v12}, Ljava/lang/Math;->min(II)I

    move-result v12

    .line 509
    invoke-static {v7, v11}, Ljava/lang/Math;->max(II)I

    move-result v11

    .line 510
    add-int/lit8 v10, v10, 0x1

    .line 513
    :cond_3
    const/16 v19, 0x1

    move/from16 v0, v19

    if-ne v4, v0, :cond_5

    .line 514
    const/4 v4, 0x0

    .line 515
    move/from16 v0, v17

    neg-int v0, v0

    move/from16 v17, v0

    .line 523
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/LinkedList;->size()I

    move-result v19

    move/from16 v0, v19

    if-lt v10, v0, :cond_2

    .line 528
    :cond_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockStart:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-ne v12, v0, :cond_6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockEnd:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-ne v11, v0, :cond_6

    .line 529
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->length()I

    move-result v19

    if-nez v19, :cond_d

    .line 530
    move v6, v12

    .local v6, "i":I
    :goto_3
    if-gt v6, v11, :cond_d

    .line 531
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockTextFromFile(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 530
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 493
    .end local v4    # "bDirection":Z
    .end local v6    # "i":I
    .end local v7    # "m":I
    .end local v17    # "nStep":I
    .end local v18    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :catch_0
    move-exception v5

    .line 494
    .local v5, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    .restart local v18    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    goto/16 :goto_1

    .line 518
    .end local v5    # "e":Ljava/lang/Exception;
    .restart local v4    # "bDirection":Z
    .restart local v7    # "m":I
    .restart local v17    # "nStep":I
    :cond_5
    const/4 v4, 0x1

    .line 519
    move/from16 v0, v17

    neg-int v0, v0

    move/from16 v17, v0

    .line 520
    add-int/lit8 v17, v17, 0x1

    goto :goto_2

    .line 535
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockEnd:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-gt v12, v0, :cond_7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockStart:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v11, v0, :cond_8

    .line 536
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuffer;->length()I

    move-result v21

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 538
    move-object/from16 v0, p0

    iput v12, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockStart:I

    .line 539
    move-object/from16 v0, p0

    iput v11, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockEnd:I

    .line 541
    move v6, v12

    .restart local v6    # "i":I
    :goto_4
    if-gt v6, v11, :cond_d

    .line 542
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockTextFromFile(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 541
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 547
    .end local v6    # "i":I
    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockStart:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v12, v0, :cond_9

    .line 548
    const/16 v16, 0x0

    .line 549
    .local v16, "nPos":I
    move v6, v12

    .restart local v6    # "i":I
    :goto_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockStart:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v6, v0, :cond_b

    .line 551
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockTextFromFile(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move/from16 v1, v16

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    .line 555
    :goto_6
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCharCount(I)I

    move-result v19

    add-int v16, v16, v19

    .line 549
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 552
    :catch_1
    move-exception v5

    .line 553
    .local v5, "e":Ljava/lang/StringIndexOutOfBoundsException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockTextFromFile(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_6

    .line 558
    .end local v5    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    .end local v6    # "i":I
    .end local v16    # "nPos":I
    :cond_9
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockStart:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-le v12, v0, :cond_e

    .line 559
    const/4 v13, 0x0

    .line 560
    .local v13, "nDel":I
    move-object/from16 v0, p0

    iget v6, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockStart:I

    .restart local v6    # "i":I
    :goto_7
    if-ge v6, v12, :cond_a

    .line 561
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCharCount(I)I

    move-result v19

    add-int v13, v13, v19

    .line 560
    add-int/lit8 v6, v6, 0x1

    goto :goto_7

    .line 563
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->length()I

    move-result v19

    if-lez v19, :cond_b

    .line 564
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v0, v1, v13}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 572
    .end local v6    # "i":I
    .end local v13    # "nDel":I
    :cond_b
    :goto_8
    move-object/from16 v0, p0

    iput v12, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockStart:I

    .line 575
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockEnd:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v11, v0, :cond_10

    .line 576
    add-int/lit8 v19, v11, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockOffsetInBuffer(I)I

    move-result v14

    .line 578
    .local v14, "nDelOffset":I
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->length()I

    move-result v20

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v0, v14, v1}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;
    :try_end_2
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_2

    .line 604
    .end local v14    # "nDelOffset":I
    :cond_c
    :goto_9
    move-object/from16 v0, p0

    iput v11, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockEnd:I

    .line 607
    :cond_d
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nLoadingBlock:I

    .line 608
    move-object/from16 v0, p0

    iput v10, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockCount:I

    .line 609
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->length()I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nLoadingBufLen:I

    .line 610
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_bBufferChanged:Z

    .line 611
    const/16 v19, -0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nChangedBlock:I

    goto/16 :goto_0

    .line 567
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->length()I

    move-result v19

    if-nez v19, :cond_b

    .line 568
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockTextFromFile(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_8

    .line 579
    .restart local v14    # "nDelOffset":I
    :catch_2
    move-exception v5

    .line 582
    .restart local v5    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->length()I

    move-result v19

    move/from16 v0, v19

    if-le v14, v0, :cond_f

    .line 583
    move v8, v14

    .line 584
    .local v8, "max":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->length()I

    move-result v9

    .line 593
    .local v9, "min":I
    :goto_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v9, v8}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    goto :goto_9

    .line 586
    .end local v8    # "max":I
    .end local v9    # "min":I
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->length()I

    move-result v8

    .line 587
    .restart local v8    # "max":I
    move v9, v14

    .restart local v9    # "min":I
    goto :goto_a

    .line 596
    .end local v5    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    .end local v8    # "max":I
    .end local v9    # "min":I
    .end local v14    # "nDelOffset":I
    :cond_10
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockEnd:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-le v11, v0, :cond_c

    .line 597
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockEnd:I

    move/from16 v19, v0

    add-int/lit8 v6, v19, 0x1

    .restart local v6    # "i":I
    :goto_b
    if-gt v6, v11, :cond_c

    .line 598
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockTextFromFile(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 597
    add-int/lit8 v6, v6, 0x1

    goto :goto_b
.end method

.method public loadBuffer_old(I)V
    .locals 13
    .param p1, "nBlock"    # I

    .prologue
    const/4 v12, 0x0

    .line 385
    const/4 v6, 0x0

    .line 388
    .local v6, "nFirstStoreCount":I
    iget v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockStart:I

    add-int/lit8 v10, v10, -0x3

    if-lt p1, v10, :cond_0

    iget v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockEnd:I

    add-int/lit8 v10, v10, 0x3

    if-ge v10, p1, :cond_4

    .line 389
    :cond_0
    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    iget-object v11, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->length()I

    move-result v11

    invoke-virtual {v10, v12, v11}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 390
    iput v12, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockCount:I

    .line 391
    iput p1, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockStart:I

    .line 392
    iput p1, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockEnd:I

    .line 430
    :goto_0
    const/4 v7, 0x1

    .local v7, "nStep":I
    const/4 v2, 0x0

    .line 431
    .local v2, "m":I
    const/4 v0, 0x1

    .line 432
    .local v0, "bDirection":Z
    :cond_1
    const/16 v10, 0x5208

    if-ge v6, v10, :cond_3

    .line 434
    sub-int v2, p1, v7

    .line 435
    if-ltz v2, :cond_2

    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->size()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    if-gt v2, v10, :cond_2

    .line 436
    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v10, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    .line 437
    .local v9, "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    iget v10, v9, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    add-int/2addr v6, v10

    .line 438
    iget v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockStart:I

    invoke-static {v2, v10}, Ljava/lang/Math;->min(II)I

    move-result v10

    iput v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockStart:I

    .line 439
    iget v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockEnd:I

    invoke-static {v2, v10}, Ljava/lang/Math;->max(II)I

    move-result v10

    iput v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockEnd:I

    .line 440
    iget v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockCount:I

    add-int/lit8 v10, v10, 0x1

    iput v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockCount:I

    .line 443
    .end local v9    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :cond_2
    const/4 v10, 0x1

    if-ne v0, v10, :cond_8

    .line 444
    const/4 v0, 0x0

    .line 445
    neg-int v7, v7

    .line 453
    :goto_1
    iget v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockCount:I

    iget-object v11, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v11}, Ljava/util/LinkedList;->size()I

    move-result v11

    if-lt v10, v11, :cond_1

    .line 458
    :cond_3
    iget v1, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockStart:I

    .local v1, "i":I
    :goto_2
    iget v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockEnd:I

    if-gt v1, v10, :cond_9

    .line 459
    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockTextFromFile(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 458
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 395
    .end local v0    # "bDirection":Z
    .end local v1    # "i":I
    .end local v2    # "m":I
    .end local v7    # "nStep":I
    :cond_4
    iget v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nLoadingBlock:I

    sub-int v5, p1, v10

    .line 396
    .local v5, "nDiffCount":I
    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->length()I

    move-result v6

    .line 398
    if-lez v5, :cond_7

    .line 399
    const/4 v4, 0x0

    .line 400
    .local v4, "nDeleteLen":I
    iget v1, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockStart:I

    .restart local v1    # "i":I
    :goto_3
    iget v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockStart:I

    add-int/2addr v10, v5

    if-ge v1, v10, :cond_5

    .line 401
    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v10, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    .line 402
    .local v8, "tmpBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    iget v10, v8, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    add-int/2addr v4, v10

    .line 400
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 404
    .end local v8    # "tmpBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :cond_5
    sub-int/2addr v6, v4

    .line 405
    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v10, v12, v4}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 412
    .end local v1    # "i":I
    .end local v4    # "nDeleteLen":I
    :cond_6
    :goto_4
    iget v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockCount:I

    add-int/2addr v10, v5

    iput v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockCount:I

    .line 413
    iget v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockStart:I

    add-int/2addr v10, v5

    iput v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockStart:I

    .line 414
    iget v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockEnd:I

    add-int/2addr v10, v5

    iput v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockEnd:I

    goto/16 :goto_0

    .line 407
    :cond_7
    if-gez v5, :cond_6

    .line 408
    iget v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockEnd:I

    sub-int/2addr v10, v5

    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockOffsetInBuffer(I)I

    move-result v3

    .line 409
    .local v3, "nDelOffet":I
    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    iget-object v11, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->length()I

    move-result v11

    invoke-virtual {v10, v3, v11}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 410
    move v6, v3

    goto :goto_4

    .line 448
    .end local v3    # "nDelOffet":I
    .end local v5    # "nDiffCount":I
    .restart local v0    # "bDirection":Z
    .restart local v2    # "m":I
    .restart local v7    # "nStep":I
    :cond_8
    const/4 v0, 0x1

    .line 449
    neg-int v7, v7

    .line 450
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 462
    .restart local v1    # "i":I
    :cond_9
    iput p1, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nLoadingBlock:I

    .line 463
    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->length()I

    move-result v10

    iput v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nLoadingBufLen:I

    .line 464
    iput-boolean v12, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_bBufferChanged:Z

    .line 465
    const/4 v10, -0x1

    iput v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nChangedBlock:I

    .line 466
    return-void
.end method

.method public processBlockChange(ILjava/lang/String;Z)V
    .locals 15
    .param p1, "nBlock"    # I
    .param p2, "strText"    # Ljava/lang/String;
    .param p3, "bNewCreate"    # Z

    .prologue
    .line 1019
    const/4 v5, 0x0

    .line 1020
    .local v5, "nBlockLen":I
    :try_start_0
    const-string/jumbo v6, ""

    .line 1023
    .local v6, "strTemp":Ljava/lang/String;
    iget v11, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTempFileCount:I

    add-int/lit8 v11, v11, 0x1

    iput v11, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTempFileCount:I

    .line 1025
    new-instance v8, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->PATH_TEXTBUFFER:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTempFileCount:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v8, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1026
    .local v8, "tempBufferFile":Ljava/io/File;
    new-instance v11, Ljava/io/RandomAccessFile;

    const-string/jumbo v12, "rw"

    invoke-direct {v11, v8, v12}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v7

    .line 1030
    .local v7, "tempBufferChannel":Ljava/nio/channels/FileChannel;
    const/4 v11, 0x1

    move/from16 v0, p3

    if-ne v0, v11, :cond_1

    .line 1031
    const/16 v5, 0xbb8

    .line 1038
    :goto_0
    const/4 v11, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 1039
    iget-object v11, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_strLoadingEncoding:Ljava/lang/String;

    invoke-virtual {v6, v11}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v11

    array-length v3, v11

    .line 1040
    .local v3, "byteLen":I
    new-instance v9, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    iget v11, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTempFileCount:I

    const/4 v12, 0x0

    invoke-direct {v9, v11, v12, v3, v5}, Lcom/infraware/polarisoffice5/text/manager/TextBlock;-><init>(IIII)V

    .line 1041
    .local v9, "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    iget-object v11, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    .line 1042
    iget-object v11, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move/from16 v0, p1

    invoke-virtual {v11, v0, v9}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V

    .line 1045
    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1046
    .local v1, "buffer":Ljava/nio/ByteBuffer;
    iget-object v11, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_strLoadingEncoding:Ljava/lang/String;

    invoke-virtual {v6, v11}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1047
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 1048
    invoke-virtual {v7, v1}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 1049
    iget v11, v9, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nEndOffset:I

    int-to-long v11, v11

    invoke-virtual {v7, v11, v12}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 1052
    const/4 v11, 0x1

    move/from16 v0, p3

    if-ne v0, v11, :cond_0

    .line 1054
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v11

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 1055
    iget-object v11, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_strLoadingEncoding:Ljava/lang/String;

    invoke-virtual {v6, v11}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v11

    array-length v3, v11

    .line 1056
    new-instance v10, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    iget v11, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTempFileCount:I

    iget v12, v9, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nEndOffset:I

    iget v13, v9, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nEndOffset:I

    add-int/2addr v13, v3

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v14

    invoke-direct {v10, v11, v12, v13, v14}, Lcom/infraware/polarisoffice5/text/manager/TextBlock;-><init>(IIII)V

    .line 1057
    .local v10, "txtBlock1":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    iget-object v11, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    add-int/lit8 v12, p1, 0x1

    invoke-virtual {v11, v12, v10}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V

    .line 1060
    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1061
    .local v2, "buffer1":Ljava/nio/ByteBuffer;
    iget-object v11, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_strLoadingEncoding:Ljava/lang/String;

    invoke-virtual {v6, v11}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1062
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 1063
    invoke-virtual {v7, v2}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 1067
    .end local v2    # "buffer1":Ljava/nio/ByteBuffer;
    .end local v10    # "txtBlock1":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :cond_0
    invoke-virtual {v7}, Ljava/nio/channels/FileChannel;->close()V

    .line 1072
    .end local v1    # "buffer":Ljava/nio/ByteBuffer;
    .end local v3    # "byteLen":I
    .end local v6    # "strTemp":Ljava/lang/String;
    .end local v7    # "tempBufferChannel":Ljava/nio/channels/FileChannel;
    .end local v8    # "tempBufferFile":Ljava/io/File;
    .end local v9    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :goto_1
    return-void

    .line 1034
    .restart local v6    # "strTemp":Ljava/lang/String;
    .restart local v7    # "tempBufferChannel":Ljava/nio/channels/FileChannel;
    .restart local v8    # "tempBufferFile":Ljava/io/File;
    :cond_1
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    goto/16 :goto_0

    .line 1068
    .end local v6    # "strTemp":Ljava/lang/String;
    .end local v7    # "tempBufferChannel":Ljava/nio/channels/FileChannel;
    .end local v8    # "tempBufferFile":Ljava/io/File;
    :catch_0
    move-exception v4

    .line 1069
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public processBlockMerge(ILjava/lang/String;ZI)V
    .locals 17
    .param p1, "nChangeBlock"    # I
    .param p2, "strText"    # Ljava/lang/String;
    .param p3, "bNewCreate"    # Z
    .param p4, "nMergeBlock"    # I

    .prologue
    .line 949
    const/4 v7, 0x0

    .line 950
    .local v7, "nBlockLen":I
    :try_start_0
    const-string/jumbo v8, ""

    .line 951
    .local v8, "strTemp":Ljava/lang/String;
    const/4 v6, 0x0

    .line 954
    .local v6, "nBlock":I
    move-object/from16 v0, p0

    iget v13, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTempFileCount:I

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTempFileCount:I

    .line 956
    new-instance v10, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v14, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->PATH_TEXTBUFFER:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTempFileCount:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v10, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 957
    .local v10, "tempBufferFile":Ljava/io/File;
    new-instance v13, Ljava/io/RandomAccessFile;

    const-string/jumbo v14, "rw"

    invoke-direct {v13, v10, v14}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v9

    .line 961
    .local v9, "tempBufferChannel":Ljava/nio/channels/FileChannel;
    move/from16 v0, p1

    move/from16 v1, p4

    if-ge v0, v1, :cond_1

    .line 962
    move/from16 v6, p1

    .line 967
    :goto_0
    const/4 v13, 0x1

    move/from16 v0, p3

    if-ne v0, v13, :cond_2

    .line 968
    const/16 v7, 0xbb8

    .line 976
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    add-int/lit8 v14, v6, 0x1

    invoke-virtual {v13, v14}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    .line 977
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v13, v6}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    .line 979
    const/4 v13, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 980
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_strLoadingEncoding:Ljava/lang/String;

    invoke-virtual {v8, v13}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v13

    array-length v4, v13

    .line 981
    .local v4, "byteLen":I
    new-instance v11, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    move-object/from16 v0, p0

    iget v13, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTempFileCount:I

    const/4 v14, 0x0

    invoke-direct {v11, v13, v14, v4, v7}, Lcom/infraware/polarisoffice5/text/manager/TextBlock;-><init>(IIII)V

    .line 982
    .local v11, "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v13, v6, v11}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V

    .line 985
    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 986
    .local v2, "buffer":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_strLoadingEncoding:Ljava/lang/String;

    invoke-virtual {v8, v13}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 987
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 988
    invoke-virtual {v9, v2}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 989
    iget v13, v11, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nEndOffset:I

    int-to-long v13, v13

    invoke-virtual {v9, v13, v14}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 992
    const/4 v13, 0x1

    move/from16 v0, p3

    if-ne v0, v13, :cond_0

    .line 994
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v13

    move-object/from16 v0, p2

    invoke-virtual {v0, v7, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 995
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_strLoadingEncoding:Ljava/lang/String;

    invoke-virtual {v8, v13}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v13

    array-length v4, v13

    .line 996
    new-instance v12, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    move-object/from16 v0, p0

    iget v13, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTempFileCount:I

    iget v14, v11, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nEndOffset:I

    iget v15, v11, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nEndOffset:I

    add-int/2addr v15, v4

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v16

    invoke-direct/range {v12 .. v16}, Lcom/infraware/polarisoffice5/text/manager/TextBlock;-><init>(IIII)V

    .line 997
    .local v12, "txtBlock1":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    add-int/lit8 v14, v6, 0x1

    invoke-virtual {v13, v14, v12}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V

    .line 1000
    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1001
    .local v3, "buffer1":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_strLoadingEncoding:Ljava/lang/String;

    invoke-virtual {v8, v13}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v13

    invoke-virtual {v3, v13}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1002
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 1003
    invoke-virtual {v9, v3}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 1007
    .end local v3    # "buffer1":Ljava/nio/ByteBuffer;
    .end local v12    # "txtBlock1":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :cond_0
    invoke-virtual {v9}, Ljava/nio/channels/FileChannel;->close()V

    .line 1012
    .end local v2    # "buffer":Ljava/nio/ByteBuffer;
    .end local v4    # "byteLen":I
    .end local v6    # "nBlock":I
    .end local v8    # "strTemp":Ljava/lang/String;
    .end local v9    # "tempBufferChannel":Ljava/nio/channels/FileChannel;
    .end local v10    # "tempBufferFile":Ljava/io/File;
    .end local v11    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :goto_2
    return-void

    .line 964
    .restart local v6    # "nBlock":I
    .restart local v8    # "strTemp":Ljava/lang/String;
    .restart local v9    # "tempBufferChannel":Ljava/nio/channels/FileChannel;
    .restart local v10    # "tempBufferFile":Ljava/io/File;
    :cond_1
    move/from16 v6, p4

    goto/16 :goto_0

    .line 971
    :cond_2
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    goto/16 :goto_1

    .line 1008
    .end local v6    # "nBlock":I
    .end local v8    # "strTemp":Ljava/lang/String;
    .end local v9    # "tempBufferChannel":Ljava/nio/channels/FileChannel;
    .end local v10    # "tempBufferFile":Ljava/io/File;
    :catch_0
    move-exception v5

    .line 1009
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method public processSave(Ljava/lang/String;ZZ)V
    .locals 21
    .param p1, "strFileName"    # Ljava/lang/String;
    .param p2, "bSave"    # Z
    .param p3, "bDelete"    # Z

    .prologue
    .line 214
    if-eqz p2, :cond_8

    .line 216
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_bBufferChanged:Z

    if-eqz v2, :cond_0

    .line 217
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockStart:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nBufferBlockEnd:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->replaceFileBufferM(Ljava/lang/String;II)V

    .line 220
    :cond_0
    new-instance v11, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v11, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 221
    .local v11, "file":Ljava/io/File;
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string/jumbo v3, "rw"

    invoke-direct {v2, v11, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 222
    .local v1, "fileChannel":Ljava/nio/channels/FileChannel;
    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/nio/channels/FileChannel;->truncate(J)Ljava/nio/channels/FileChannel;

    .line 223
    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 226
    const/4 v15, 0x0

    .line 227
    .local v15, "nOffset":I
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->isNewFile()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->setIsNewFile(Z)V

    .line 228
    :cond_1
    const/4 v2, 0x2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 229
    .local v7, "buffer":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_strLoadingEncoding:Ljava/lang/String;

    const-string/jumbo v3, "UTF-16BE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 230
    const/4 v2, -0x2

    invoke-virtual {v7, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 231
    const/4 v2, -0x1

    invoke-virtual {v7, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 232
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 233
    invoke-virtual {v1, v7}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 234
    const/4 v15, 0x2

    .line 246
    :cond_2
    :goto_0
    const/16 v17, 0x0

    .local v17, "nStart":I
    const/4 v13, 0x0

    .local v13, "nEnd":I
    const/4 v14, 0x0

    .local v14, "nLen":I
    const/16 v16, -0x1

    .line 247
    .local v16, "nOldFileNum":I
    const/4 v9, 0x0

    .line 248
    .local v9, "bufferFile":Ljava/io/File;
    const/4 v8, 0x0

    .line 249
    .local v8, "bufferChannel":Ljava/nio/channels/FileChannel;
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-ge v12, v2, :cond_7

    .line 251
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v2, v12}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    .line 252
    .local v20, "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    move-object/from16 v0, v20

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nStartOffset:I

    move/from16 v17, v0

    .line 253
    move-object/from16 v0, v20

    iget v13, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nEndOffset:I

    .line 255
    move-object/from16 v0, v20

    iget v2, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nFileNum:I

    move/from16 v0, v16

    if-eq v0, v2, :cond_4

    .line 256
    const/4 v2, -0x1

    move/from16 v0, v16

    if-eq v0, v2, :cond_3

    .line 257
    invoke-virtual {v8}, Ljava/nio/channels/FileChannel;->close()V

    .line 259
    :cond_3
    move-object/from16 v0, v20

    iget v2, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nFileNum:I

    if-nez v2, :cond_6

    .line 260
    sget-object v19, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->PATH_TEXTBUFFER:Ljava/lang/String;

    .line 264
    .local v19, "strTempFileName":Ljava/lang/String;
    :goto_2
    new-instance v9, Ljava/io/File;

    .end local v9    # "bufferFile":Ljava/io/File;
    move-object/from16 v0, v19

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 265
    .restart local v9    # "bufferFile":Ljava/io/File;
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string/jumbo v3, "rw"

    invoke-direct {v2, v9, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v8

    .line 267
    move-object/from16 v0, v20

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nFileNum:I

    move/from16 v16, v0

    .line 270
    .end local v19    # "strTempFileName":Ljava/lang/String;
    :cond_4
    sub-int v14, v13, v17

    .line 271
    move/from16 v0, v17

    int-to-long v2, v0

    invoke-virtual {v8, v2, v3}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    move-result-object v2

    int-to-long v3, v15

    int-to-long v5, v14

    invoke-virtual/range {v1 .. v6}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J

    .line 272
    add-int/2addr v15, v14

    .line 249
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 235
    .end local v8    # "bufferChannel":Ljava/nio/channels/FileChannel;
    .end local v9    # "bufferFile":Ljava/io/File;
    .end local v12    # "i":I
    .end local v13    # "nEnd":I
    .end local v14    # "nLen":I
    .end local v16    # "nOldFileNum":I
    .end local v17    # "nStart":I
    .end local v20    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_strLoadingEncoding:Ljava/lang/String;

    const-string/jumbo v3, "UTF-16LE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 236
    const/4 v2, -0x1

    invoke-virtual {v7, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 237
    const/4 v2, -0x2

    invoke-virtual {v7, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 238
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 239
    invoke-virtual {v1, v7}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 240
    const/4 v15, 0x2

    goto/16 :goto_0

    .line 262
    .restart local v8    # "bufferChannel":Ljava/nio/channels/FileChannel;
    .restart local v9    # "bufferFile":Ljava/io/File;
    .restart local v12    # "i":I
    .restart local v13    # "nEnd":I
    .restart local v14    # "nLen":I
    .restart local v16    # "nOldFileNum":I
    .restart local v17    # "nStart":I
    .restart local v20    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->PATH_TEXTBUFFER:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v20

    iget v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nFileNum:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .restart local v19    # "strTempFileName":Ljava/lang/String;
    goto :goto_2

    .line 275
    .end local v19    # "strTempFileName":Ljava/lang/String;
    .end local v20    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :cond_7
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 276
    if-eqz v8, :cond_8

    .line 277
    invoke-virtual {v8}, Ljava/nio/channels/FileChannel;->close()V

    .line 280
    .end local v1    # "fileChannel":Ljava/nio/channels/FileChannel;
    .end local v7    # "buffer":Ljava/nio/ByteBuffer;
    .end local v8    # "bufferChannel":Ljava/nio/channels/FileChannel;
    .end local v9    # "bufferFile":Ljava/io/File;
    .end local v11    # "file":Ljava/io/File;
    .end local v12    # "i":I
    .end local v13    # "nEnd":I
    .end local v14    # "nLen":I
    .end local v15    # "nOffset":I
    .end local v16    # "nOldFileNum":I
    .end local v17    # "nStart":I
    :cond_8
    if-eqz p3, :cond_b

    .line 281
    sget-object v18, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->PATH_TEXTBUFFER:Ljava/lang/String;

    .line 282
    .local v18, "strTempFile":Ljava/lang/String;
    const/4 v12, 0x0

    .restart local v12    # "i":I
    :goto_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTempFileCount:I

    if-gt v12, v2, :cond_a

    .line 283
    if-nez v12, :cond_9

    .line 284
    sget-object v18, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->PATH_TEXTBUFFER:Ljava/lang/String;

    .line 287
    :goto_4
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, v18

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 282
    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    .line 286
    :cond_9
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->PATH_TEXTBUFFER:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    goto :goto_4

    .line 289
    :cond_a
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTempFileCount:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 294
    .end local v12    # "i":I
    .end local v18    # "strTempFile":Ljava/lang/String;
    :cond_b
    :goto_5
    return-void

    .line 291
    :catch_0
    move-exception v10

    .line 292
    .local v10, "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5
.end method

.method public replaceFileBuffer(Ljava/lang/String;II)V
    .locals 31
    .param p1, "strText"    # Ljava/lang/String;
    .param p2, "nStartBlock"    # I
    .param p3, "nEndBlock"    # I

    .prologue
    .line 689
    :try_start_0
    new-instance v15, Ljava/io/File;

    sget-object v3, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->PATH_TEXTBUFFER:Ljava/lang/String;

    invoke-direct {v15, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 690
    .local v15, "bufferFile":Ljava/io/File;
    new-instance v27, Ljava/io/File;

    sget-object v3, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->PATH_TEXTBUFFER_TEMP:Ljava/lang/String;

    move-object/from16 v0, v27

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 691
    .local v27, "tempBufferFile":Ljava/io/File;
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string/jumbo v4, "rw"

    invoke-direct {v3, v15, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    .line 692
    .local v2, "bufferChannel":Ljava/nio/channels/FileChannel;
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string/jumbo v4, "rw"

    move-object/from16 v0, v27

    invoke-direct {v3, v0, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v7

    .line 695
    .local v7, "tempBufferChannel":Ljava/nio/channels/FileChannel;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    .line 696
    .local v30, "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    move-object/from16 v0, v30

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nStartOffset:I

    move/from16 v19, v0

    .line 698
    .local v19, "nBufferStartOffset":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    move/from16 v0, p3

    if-lt v0, v3, :cond_0

    .line 699
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v3

    long-to-int v0, v3

    move/from16 v18, v0

    .line 719
    .local v18, "nBufferEndOffset":I
    :goto_0
    move/from16 v0, v18

    int-to-long v3, v0

    :try_start_1
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v5

    move/from16 v0, v18

    int-to-long v8, v0

    sub-long/2addr v5, v8

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->transferTo(JJLjava/nio/channels/WritableByteChannel;)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 727
    move/from16 v0, v19

    int-to-long v3, v0

    :try_start_2
    invoke-virtual {v2, v3, v4}, Ljava/nio/channels/FileChannel;->truncate(J)Ljava/nio/channels/FileChannel;

    .line 728
    move/from16 v0, v19

    int-to-long v3, v0

    invoke-virtual {v2, v3, v4}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 731
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_strLoadingEncoding:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    array-length v3, v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v14

    .line 732
    .local v14, "buffer":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_strLoadingEncoding:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v14, v3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 733
    invoke-virtual {v14}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 734
    invoke-virtual {v2, v14}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 737
    const-wide/16 v3, 0x0

    invoke-virtual {v7, v3, v4}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    move-result-object v9

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v10

    invoke-virtual {v7}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v12

    move-object v8, v2

    invoke-virtual/range {v8 .. v13}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J

    .line 741
    const/16 v3, 0x2ee0

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v23

    .line 745
    .local v23, "readBuf":Ljava/nio/ByteBuffer;
    const-string/jumbo v26, ""

    .line 746
    .local v26, "strTempEncode":Ljava/lang/String;
    const/16 v21, 0x0

    .local v21, "nStrLen":I
    const/16 v22, 0x0

    .line 747
    .local v22, "numRead":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    iget v0, v3, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nStartOffset:I

    move/from16 v20, v0

    .line 748
    .local v20, "nOffset":I
    move/from16 v0, v20

    int-to-long v3, v0

    invoke-virtual {v2, v3, v4}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 751
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    add-int/lit8 v17, v3, -0x1

    .local v17, "i":I
    :goto_1
    move/from16 v0, v17

    move/from16 v1, p2

    if-lt v0, v1, :cond_1

    .line 752
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    .line 751
    add-int/lit8 v17, v17, -0x1

    goto :goto_1

    .line 702
    .end local v14    # "buffer":Ljava/nio/ByteBuffer;
    .end local v17    # "i":I
    .end local v18    # "nBufferEndOffset":I
    .end local v20    # "nOffset":I
    .end local v21    # "nStrLen":I
    .end local v22    # "numRead":I
    .end local v23    # "readBuf":Ljava/nio/ByteBuffer;
    .end local v26    # "strTempEncode":Ljava/lang/String;
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v30

    .end local v30    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    check-cast v30, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    .line 703
    .restart local v30    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    move-object/from16 v0, v30

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nEndOffset:I

    move/from16 v18, v0

    .restart local v18    # "nBufferEndOffset":I
    goto/16 :goto_0

    .line 720
    :catch_0
    move-exception v16

    .line 816
    .end local v2    # "bufferChannel":Ljava/nio/channels/FileChannel;
    .end local v7    # "tempBufferChannel":Ljava/nio/channels/FileChannel;
    .end local v15    # "bufferFile":Ljava/io/File;
    .end local v18    # "nBufferEndOffset":I
    .end local v19    # "nBufferStartOffset":I
    .end local v27    # "tempBufferFile":Ljava/io/File;
    .end local v30    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :goto_2
    return-void

    .line 755
    .restart local v2    # "bufferChannel":Ljava/nio/channels/FileChannel;
    .restart local v7    # "tempBufferChannel":Ljava/nio/channels/FileChannel;
    .restart local v14    # "buffer":Ljava/nio/ByteBuffer;
    .restart local v15    # "bufferFile":Ljava/io/File;
    .restart local v17    # "i":I
    .restart local v18    # "nBufferEndOffset":I
    .restart local v19    # "nBufferStartOffset":I
    .restart local v20    # "nOffset":I
    .restart local v21    # "nStrLen":I
    .restart local v22    # "numRead":I
    .restart local v23    # "readBuf":Ljava/nio/ByteBuffer;
    .restart local v26    # "strTempEncode":Ljava/lang/String;
    .restart local v27    # "tempBufferFile":Ljava/io/File;
    .restart local v30    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :cond_1
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTotalTextLen:I

    .line 756
    :goto_3
    if-ltz v22, :cond_2

    .line 757
    invoke-virtual/range {v23 .. v23}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 758
    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v22

    .line 759
    if-gez v22, :cond_4

    .line 803
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    if-nez v3, :cond_3

    .line 804
    new-instance v30, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    .end local v30    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    invoke-direct/range {v30 .. v30}, Lcom/infraware/polarisoffice5/text/manager/TextBlock;-><init>()V

    .line 805
    .restart local v30    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 808
    :cond_3
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 809
    invoke-virtual {v7}, Ljava/nio/channels/FileChannel;->close()V

    .line 812
    new-instance v3, Ljava/io/File;

    sget-object v4, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->PATH_TEXTBUFFER_TEMP:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 813
    .end local v2    # "bufferChannel":Ljava/nio/channels/FileChannel;
    .end local v7    # "tempBufferChannel":Ljava/nio/channels/FileChannel;
    .end local v14    # "buffer":Ljava/nio/ByteBuffer;
    .end local v15    # "bufferFile":Ljava/io/File;
    .end local v17    # "i":I
    .end local v18    # "nBufferEndOffset":I
    .end local v19    # "nBufferStartOffset":I
    .end local v20    # "nOffset":I
    .end local v21    # "nStrLen":I
    .end local v22    # "numRead":I
    .end local v23    # "readBuf":Ljava/nio/ByteBuffer;
    .end local v26    # "strTempEncode":Ljava/lang/String;
    .end local v27    # "tempBufferFile":Ljava/io/File;
    .end local v30    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :catch_1
    move-exception v16

    .line 814
    .local v16, "e":Ljava/io/IOException;
    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 761
    .end local v16    # "e":Ljava/io/IOException;
    .restart local v2    # "bufferChannel":Ljava/nio/channels/FileChannel;
    .restart local v7    # "tempBufferChannel":Ljava/nio/channels/FileChannel;
    .restart local v14    # "buffer":Ljava/nio/ByteBuffer;
    .restart local v15    # "bufferFile":Ljava/io/File;
    .restart local v17    # "i":I
    .restart local v18    # "nBufferEndOffset":I
    .restart local v19    # "nBufferStartOffset":I
    .restart local v20    # "nOffset":I
    .restart local v21    # "nStrLen":I
    .restart local v22    # "numRead":I
    .restart local v23    # "readBuf":Ljava/nio/ByteBuffer;
    .restart local v26    # "strTempEncode":Ljava/lang/String;
    .restart local v27    # "tempBufferFile":Ljava/io/File;
    .restart local v30    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :cond_4
    :try_start_3
    move/from16 v0, v22

    new-array v0, v0, [B

    move-object/from16 v24, v0

    .line 762
    .local v24, "readBytes":[B
    invoke-virtual/range {v23 .. v23}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 763
    invoke-virtual/range {v23 .. v24}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 764
    new-instance v25, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_strLoadingEncoding:Ljava/lang/String;

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-direct {v0, v1, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 765
    .local v25, "strRead":Ljava/lang/String;
    invoke-virtual/range {v23 .. v23}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 766
    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v21

    .line 768
    const/16 v3, 0xbb8

    move/from16 v0, v21

    if-le v0, v3, :cond_6

    .line 769
    const/4 v3, 0x0

    const/16 v4, 0xbb8

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v26

    .line 770
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_strLoadingEncoding:Ljava/lang/String;

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v28

    .line 771
    .local v28, "tempBytes":[B
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    if-lez v3, :cond_5

    .line 772
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    iget v0, v3, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nEndOffset:I

    move/from16 v20, v0

    .line 776
    :goto_4
    new-instance v29, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    invoke-direct/range {v29 .. v29}, Lcom/infraware/polarisoffice5/text/manager/TextBlock;-><init>()V

    .line 777
    .local v29, "tmpBlk":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    move/from16 v0, v20

    move-object/from16 v1, v29

    iput v0, v1, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nStartOffset:I

    .line 778
    move-object/from16 v0, v28

    array-length v3, v0

    add-int v3, v3, v20

    move-object/from16 v0, v29

    iput v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nEndOffset:I

    .line 779
    move-object/from16 v0, p0

    iget v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTempFileCount:I

    move-object/from16 v0, v29

    iput v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nFileNum:I

    .line 780
    const/16 v3, 0xbb8

    move-object/from16 v0, v29

    iput v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    .line 781
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move-object/from16 v0, v29

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 783
    move-object/from16 v0, v29

    iget v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nEndOffset:I

    int-to-long v3, v3

    invoke-virtual {v2, v3, v4}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 784
    move-object/from16 v0, p0

    iget v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTotalTextLen:I

    add-int/lit16 v3, v3, 0xbb8

    move-object/from16 v0, p0

    iput v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTotalTextLen:I

    goto/16 :goto_3

    .line 774
    .end local v29    # "tmpBlk":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :cond_5
    const/16 v20, 0x0

    goto :goto_4

    .line 786
    .end local v28    # "tempBytes":[B
    :cond_6
    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nLastBlockLen:I

    .line 787
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    if-lez v3, :cond_7

    .line 788
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    iget v0, v3, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nEndOffset:I

    move/from16 v20, v0

    .line 792
    :goto_5
    new-instance v29, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    invoke-direct/range {v29 .. v29}, Lcom/infraware/polarisoffice5/text/manager/TextBlock;-><init>()V

    .line 793
    .restart local v29    # "tmpBlk":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    move/from16 v0, v20

    move-object/from16 v1, v29

    iput v0, v1, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nStartOffset:I

    .line 794
    add-int v3, v20, v22

    move-object/from16 v0, v29

    iput v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nEndOffset:I

    .line 795
    move-object/from16 v0, p0

    iget v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTempFileCount:I

    move-object/from16 v0, v29

    iput v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nFileNum:I

    .line 796
    move-object/from16 v0, p0

    iget v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nLastBlockLen:I

    move-object/from16 v0, v29

    iput v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    .line 797
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move-object/from16 v0, v29

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 799
    move-object/from16 v0, p0

    iget v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTotalTextLen:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nLastBlockLen:I

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTotalTextLen:I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_3

    .line 790
    .end local v29    # "tmpBlk":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :cond_7
    const/16 v20, 0x0

    goto :goto_5
.end method

.method public replaceFileBuffer1(Ljava/lang/String;II)V
    .locals 13
    .param p1, "strText"    # Ljava/lang/String;
    .param p2, "nStartBlock"    # I
    .param p3, "nEndBlock"    # I

    .prologue
    .line 823
    iget v9, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nChangedBlock:I

    invoke-virtual {p0, v9}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockOffsetInBuffer(I)I

    move-result v6

    .line 824
    .local v6, "nStartOffset":I
    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    iget v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nChangedBlock:I

    invoke-virtual {v9, v10}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    .line 825
    .local v0, "changeBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->length()I

    move-result v9

    iget v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nLoadingBufLen:I

    sub-int v1, v9, v10

    .line 826
    .local v1, "nChangeSize":I
    iget v9, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    add-int v2, v9, v1

    .line 827
    .local v2, "nChangedSize":I
    add-int v3, v6, v2

    .line 828
    .local v3, "nEndOffset":I
    invoke-virtual {p1, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 832
    .local v7, "strChangedText":Ljava/lang/String;
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v9

    int-to-double v9, v9

    const-wide v11, 0x40b1940000000000L    # 4500.0

    cmpl-double v9, v9, v11

    if-ltz v9, :cond_0

    .line 833
    iget v9, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nChangedBlock:I

    const/4 v10, 0x1

    invoke-virtual {p0, v9, v7, v10}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->processBlockChange(ILjava/lang/String;Z)V

    .line 865
    :goto_0
    return-void

    .line 835
    :cond_0
    if-gtz v2, :cond_1

    .line 836
    iget-object v9, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    iget v10, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nChangedBlock:I

    invoke-virtual {v9, v10}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 840
    :cond_1
    iget v9, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nChangedBlock:I

    invoke-virtual {p0, v9, v2}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->checkMergeBlock(II)I

    move-result v4

    .line 843
    .local v4, "nMergeBlock":I
    if-ltz v4, :cond_4

    .line 844
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCharCount(I)I

    move-result v9

    add-int v5, v2, v9

    .line 845
    .local v5, "nMergeBlockSize":I
    const-string/jumbo v8, ""

    .line 847
    .local v8, "strTemp":Ljava/lang/String;
    int-to-double v9, v5

    const-wide v11, 0x40b1940000000000L    # 4500.0

    cmpl-double v9, v9, v11

    if-ltz v9, :cond_3

    .line 848
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockTextFromFile(I)Ljava/lang/String;

    move-result-object v8

    .line 849
    iget v9, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nChangedBlock:I

    if-le v9, v4, :cond_2

    .line 850
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 855
    :goto_1
    iget v9, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nChangedBlock:I

    const/4 v10, 0x1

    invoke-virtual {p0, v9, v7, v10, v4}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->processBlockMerge(ILjava/lang/String;ZI)V

    goto :goto_0

    .line 852
    :cond_2
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 858
    :cond_3
    iget v9, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nChangedBlock:I

    const/4 v10, 0x0

    invoke-virtual {p0, v9, v7, v10, v4}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->processBlockMerge(ILjava/lang/String;ZI)V

    goto :goto_0

    .line 862
    .end local v5    # "nMergeBlockSize":I
    .end local v8    # "strTemp":Ljava/lang/String;
    :cond_4
    iget v9, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nChangedBlock:I

    const/4 v10, 0x0

    invoke-virtual {p0, v9, v7, v10}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->processBlockChange(ILjava/lang/String;Z)V

    goto :goto_0
.end method

.method public replaceFileBuffer2(Ljava/lang/String;II)V
    .locals 23
    .param p1, "strText"    # Ljava/lang/String;
    .param p2, "nStartBlock"    # I
    .param p3, "nEndBlock"    # I

    .prologue
    .line 873
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v11

    .line 874
    .local v11, "nLength":I
    div-int/lit16 v9, v11, 0xbb8

    .line 875
    .local v9, "nCount":I
    rem-int/lit16 v12, v11, 0xbb8
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 877
    .local v12, "nMod":I
    if-lez v12, :cond_0

    int-to-double v0, v12

    move-wide/from16 v19, v0

    const-wide v21, 0x4097700000000000L    # 1500.0

    cmpg-double v19, v19, v21

    if-gez v19, :cond_0

    .line 878
    add-int/lit8 v9, v9, -0x1

    .line 882
    :cond_0
    move/from16 v8, p2

    .local v8, "n":I
    :goto_0
    move/from16 v0, p3

    if-gt v8, v0, :cond_1

    .line 883
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 882
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 885
    :catch_0
    move-exception v19

    .line 890
    :cond_1
    :try_start_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTempFileCount:I

    move/from16 v19, v0

    add-int/lit8 v19, v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTempFileCount:I

    .line 892
    new-instance v17, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v20, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->PATH_TEXTBUFFER:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTempFileCount:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 893
    .local v17, "tempBufferFile":Ljava/io/File;
    new-instance v19, Ljava/io/RandomAccessFile;

    const-string/jumbo v20, "rw"

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual/range {v19 .. v19}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v16

    .line 896
    .local v16, "tempBufferChannel":Ljava/nio/channels/FileChannel;
    const/4 v13, 0x0

    .line 897
    .local v13, "nOffset":I
    const/4 v14, 0x0

    .local v14, "nStartPos":I
    const/4 v10, 0x0

    .local v10, "nEndPos":I
    const/4 v7, 0x0

    .line 898
    .local v7, "i":I
    move/from16 v7, p2

    :goto_1
    add-int v19, p2, v9

    move/from16 v0, v19

    if-ge v7, v0, :cond_2

    .line 901
    sub-int v19, v7, p2

    move/from16 v0, v19

    mul-int/lit16 v14, v0, 0xbb8

    .line 902
    add-int/lit16 v10, v14, 0xbb8

    .line 905
    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    .line 906
    .local v15, "strTemp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_strLoadingEncoding:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v19

    move-object/from16 v0, v19

    array-length v5, v0

    .line 907
    .local v5, "byteLen":I
    new-instance v18, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTempFileCount:I

    move/from16 v19, v0

    add-int v20, v13, v5

    const/16 v21, 0xbb8

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-direct {v0, v1, v13, v2, v3}, Lcom/infraware/polarisoffice5/text/manager/TextBlock;-><init>(IIII)V

    .line 908
    .local v18, "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v7, v1}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V

    .line 911
    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 912
    .local v4, "buffer":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_strLoadingEncoding:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 913
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 914
    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 915
    move-object/from16 v0, v18

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nEndOffset:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v19, v0

    move-object/from16 v0, v16

    move-wide/from16 v1, v19

    invoke-virtual {v0, v1, v2}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 917
    move-object/from16 v0, v18

    iget v13, v0, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nEndOffset:I

    .line 898
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 921
    .end local v4    # "buffer":Ljava/nio/ByteBuffer;
    .end local v5    # "byteLen":I
    .end local v15    # "strTemp":Ljava/lang/String;
    .end local v18    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :cond_2
    if-eqz v12, :cond_3

    .line 922
    move v14, v10

    .line 923
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v10

    .line 925
    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    .line 926
    .restart local v15    # "strTemp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_strLoadingEncoding:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v19

    move-object/from16 v0, v19

    array-length v5, v0

    .line 927
    .restart local v5    # "byteLen":I
    new-instance v18, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nTempFileCount:I

    move/from16 v19, v0

    add-int v20, v13, v5

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v21

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-direct {v0, v1, v13, v2, v3}, Lcom/infraware/polarisoffice5/text/manager/TextBlock;-><init>(IIII)V

    .line 928
    .restart local v18    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v7, v1}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V

    .line 931
    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 932
    .restart local v4    # "buffer":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_strLoadingEncoding:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 933
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 934
    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 938
    .end local v4    # "buffer":Ljava/nio/ByteBuffer;
    .end local v5    # "byteLen":I
    .end local v15    # "strTemp":Ljava/lang/String;
    .end local v18    # "txtBlock":Lcom/infraware/polarisoffice5/text/manager/TextBlock;
    :cond_3
    invoke-virtual/range {v16 .. v16}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 942
    .end local v7    # "i":I
    .end local v8    # "n":I
    .end local v9    # "nCount":I
    .end local v10    # "nEndPos":I
    .end local v11    # "nLength":I
    .end local v12    # "nMod":I
    .end local v13    # "nOffset":I
    .end local v14    # "nStartPos":I
    .end local v16    # "tempBufferChannel":Ljava/nio/channels/FileChannel;
    .end local v17    # "tempBufferFile":Ljava/io/File;
    :goto_2
    return-void

    .line 939
    :catch_1
    move-exception v6

    .line 940
    .local v6, "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method public replaceFileBufferM(Ljava/lang/String;II)V
    .locals 2
    .param p1, "strText"    # Ljava/lang/String;
    .param p2, "nStartBlock"    # I
    .param p3, "nEndBlock"    # I

    .prologue
    .line 675
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v0

    .line 676
    .local v0, "nBlockCount":I
    const/4 v1, 0x7

    if-gt v0, v1, :cond_0

    .line 677
    invoke-virtual {p0, p1, p2, p3}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->replaceFileBuffer(Ljava/lang/String;II)V

    .line 682
    :goto_0
    return-void

    .line 680
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->replaceFileBuffer2(Ljava/lang/String;II)V

    goto :goto_0
.end method

.method public replaceMemBuffer(Ljava/lang/String;III)V
    .locals 5
    .param p1, "strText"    # Ljava/lang/String;
    .param p2, "nBlock"    # I
    .param p3, "nStartPos"    # I
    .param p4, "nEndPos"    # I

    .prologue
    .line 657
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_bBufferChanged:Z

    .line 658
    iput p2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_nChangedBlock:I

    .line 661
    invoke-virtual {p0, p2}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockOffsetInBuffer(I)I

    move-result v1

    .line 663
    .local v1, "nBlockOffset":I
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_Buffer:Ljava/lang/StringBuffer;

    add-int v3, v1, p3

    add-int v4, v1, p4

    invoke-virtual {v2, v3, v4, p1}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 666
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v2, p2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    iget v0, v2, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    .line 667
    .local v0, "nBlockLen":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v0

    sub-int v3, p4, p3

    sub-int v0, v2, v3

    .line 668
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_BlockOffsetList:Ljava/util/LinkedList;

    invoke-virtual {v2, p2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/text/manager/TextBlock;

    iput v0, v2, Lcom/infraware/polarisoffice5/text/manager/TextBlock;->m_nCharCount:I

    .line 670
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBufferChangedLen()I

    move-result v2

    const/16 v3, 0xbb8

    if-lt v2, v3, :cond_0

    .line 671
    invoke-virtual {p0, p2}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->loadBuffer(I)V

    .line 672
    :cond_0
    return-void
.end method

.method public setIsNewFile(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 1184
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_bIsNewFile:Z

    .line 1185
    return-void
.end method

.method public setLoadingEncoding(Ljava/lang/String;)V
    .locals 0
    .param p1, "strEncoding"    # Ljava/lang/String;

    .prologue
    .line 1160
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_strLoadingEncoding:Ljava/lang/String;

    .line 1161
    return-void
.end method

.method public setStopFlag(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 205
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->m_stopReading:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 206
    return-void
.end method
