.class public Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;
.super Landroid/app/Activity;
.source "PreferencesActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Flag;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Size;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$StringReference;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Type;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$CloseActionReceiver;,
        Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$EncodingAdapter;
    }
.end annotation


# instance fields
.field mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

.field mHandler:Landroid/os/Handler;

.field private mScrollHandler:Landroid/os/Handler;

.field private mScrollTask:Ljava/lang/Runnable;

.field private m_Activity:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

.field private m_EncodingAdapter:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$EncodingAdapter;

.field private m_FontsizeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private m_btnDone:Landroid/widget/ImageView;

.field private m_displayType:I

.field private m_encodingTextColor:Landroid/content/res/ColorStateList;

.field private m_ivThemeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private m_llActionBar:Landroid/widget/LinearLayout;

.field private m_lvEncoding:Landroid/widget/ListView;

.field private m_nEncoding:I

.field private m_nFontSize:I

.field private m_nLocaleType:I

.field private m_nOrientation:I

.field private m_nTheme:I

.field private m_oCloseFilter:Landroid/content/IntentFilter;

.field private m_oCloseReceiver:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$CloseActionReceiver;

.field private m_svPreferences:Landroid/widget/ScrollView;

.field private m_themeSelectLayout:Landroid/widget/LinearLayout;

.field private m_tvEncoding:Landroid/widget/TextView;

.field private m_tvFont1:Landroid/widget/TextView;

.field private m_tvFont2:Landroid/widget/TextView;

.field private m_tvFont3:Landroid/widget/TextView;

.field private m_tvFont4:Landroid/widget/TextView;

.field private m_tvFont5:Landroid/widget/TextView;

.field private m_tvFont6:Landroid/widget/TextView;

.field private m_tvFontsize:Landroid/widget/TextView;

.field private m_tvTheme:Landroid/widget/TextView;

.field private m_tvTitle:Landroid/widget/TextView;

.field private m_utf8index:I

.field private m_wheelbtn:Lcom/infraware/polarisoffice5/text/control/TextWheelButton;

.field private m_wheelfontsize:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 75
    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_nLocaleType:I

    .line 79
    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_nOrientation:I

    .line 81
    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_utf8index:I

    .line 87
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$CloseActionReceiver;

    .line 88
    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    .line 196
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$1;-><init>(Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->mHandler:Landroid/os/Handler;

    .line 306
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$3;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$3;-><init>(Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 547
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->mScrollHandler:Landroid/os/Handler;

    .line 548
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$4;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$4;-><init>(Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->mScrollTask:Ljava/lang/Runnable;

    .line 589
    return-void
.end method

.method static synthetic access$002(Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_wheelfontsize:I

    return p1
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;)Lcom/infraware/polarisoffice5/text/control/TextWheelButton;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_wheelbtn:Lcom/infraware/polarisoffice5/text/control/TextWheelButton;

    return-object v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_lvEncoding:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_svPreferences:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;)Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_Activity:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_llActionBar:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_btnDone:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;)Landroid/content/res/ColorStateList;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_encodingTextColor:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method static synthetic access$800(Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    .prologue
    .line 44
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_nEncoding:I

    return v0
.end method

.method private getScrollTime()I
    .locals 1

    .prologue
    .line 555
    const/16 v0, 0x190

    return v0
.end method

.method private initControls()V
    .locals 17

    .prologue
    .line 203
    const v1, 0x7f0b0230

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_llActionBar:Landroid/widget/LinearLayout;

    .line 204
    const v1, 0x7f0b0231

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_btnDone:Landroid/widget/ImageView;

    .line 205
    const v1, 0x7f0b0058

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_tvTitle:Landroid/widget/TextView;

    .line 206
    const v1, 0x7f0b0232

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_tvFontsize:Landroid/widget/TextView;

    .line 207
    const v1, 0x7f0b023a

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_tvTheme:Landroid/widget/TextView;

    .line 208
    const v1, 0x7f0b0242

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_tvEncoding:Landroid/widget/TextView;

    .line 209
    const v1, 0x7f0b0244

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_lvEncoding:Landroid/widget/ListView;

    .line 210
    const v1, 0x7f0b0243

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_svPreferences:Landroid/widget/ScrollView;

    .line 211
    const v1, 0x7f0b0233

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_tvFont1:Landroid/widget/TextView;

    .line 212
    const v1, 0x7f0b0234

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_tvFont2:Landroid/widget/TextView;

    .line 213
    const v1, 0x7f0b0235

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_tvFont3:Landroid/widget/TextView;

    .line 214
    const v1, 0x7f0b0236

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_tvFont4:Landroid/widget/TextView;

    .line 215
    const v1, 0x7f0b0237

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_tvFont5:Landroid/widget/TextView;

    .line 216
    const v1, 0x7f0b0238

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_tvFont6:Landroid/widget/TextView;

    .line 218
    const v1, 0x7f0b023b

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_themeSelectLayout:Landroid/widget/LinearLayout;

    .line 220
    const v1, 0x7f0b023e

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    .line 221
    .local v12, "ivLightGrayTheme":Landroid/widget/ImageView;
    const v1, 0x7f0b023c

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    .line 222
    .local v13, "ivYellowTheme":Landroid/widget/ImageView;
    const v1, 0x7f0b0240

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    .line 225
    .local v11, "ivBlackTheme":Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_btnDone:Landroid/widget/ImageView;

    const v2, 0x7f070061

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 226
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_btnDone:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 228
    move-object/from16 v0, p0

    iget v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_displayType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 229
    invoke-virtual {v13}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    check-cast v14, Landroid/widget/RelativeLayout$LayoutParams;

    .line 230
    .local v14, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iget v1, v14, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    mul-int/lit8 v1, v1, 0x2

    iput v1, v14, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 231
    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 232
    invoke-virtual {v12}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    .end local v14    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v14, Landroid/widget/RelativeLayout$LayoutParams;

    .line 233
    .restart local v14    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iget v1, v14, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    mul-int/lit8 v1, v1, 0x2

    iput v1, v14, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 234
    iget v1, v14, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    mul-int/lit8 v1, v1, 0x2

    iput v1, v14, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 235
    invoke-virtual {v12, v14}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 236
    invoke-virtual {v11}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    .end local v14    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v14, Landroid/widget/RelativeLayout$LayoutParams;

    .line 237
    .restart local v14    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iget v1, v14, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    mul-int/lit8 v1, v1, 0x2

    iput v1, v14, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 238
    invoke-virtual {v11, v14}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 241
    .end local v14    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_ivThemeList:Ljava/util/ArrayList;

    .line 242
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_ivThemeList:Ljava/util/ArrayList;

    invoke-virtual {v1, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 243
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_ivThemeList:Ljava/util/ArrayList;

    invoke-virtual {v1, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 244
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_ivThemeList:Ljava/util/ArrayList;

    invoke-virtual {v1, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 246
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_tvTheme:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 247
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_themeSelectLayout:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 249
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_FontsizeList:Ljava/util/ArrayList;

    .line 250
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_FontsizeList:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_tvFont1:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 251
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_FontsizeList:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_tvFont2:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 252
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_FontsizeList:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_tvFont3:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 253
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_FontsizeList:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_tvFont4:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 254
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_FontsizeList:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_tvFont5:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 255
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_FontsizeList:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_tvFont6:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 257
    invoke-static {}, Lcom/infraware/common/util/text/CharsetDetector;->getAllDetectableCharsets()[Ljava/lang/String;

    move-result-object v8

    .line 258
    .local v8, "encodingItems":[Ljava/lang/String;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 259
    .local v7, "availableCharsetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    array-length v1, v8

    if-ge v9, v1, :cond_2

    .line 260
    aget-object v1, v8, v9

    invoke-static {v1}, Ljava/nio/charset/Charset;->isSupported(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 261
    aget-object v1, v8, v9

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 262
    aget-object v1, v8, v9

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "utf-8"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 263
    add-int/lit8 v1, v9, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_utf8index:I

    .line 259
    :cond_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 266
    :cond_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    .line 267
    .local v5, "items":[Ljava/lang/String;
    const/4 v1, 0x0

    const v2, 0x7f0702e0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    .line 268
    const/4 v9, 0x1

    :goto_1
    array-length v1, v5

    if-ge v9, v1, :cond_3

    .line 269
    add-int/lit8 v1, v9, -0x1

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v5, v9

    .line 268
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 270
    :cond_3
    new-instance v1, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$EncodingAdapter;

    const v4, 0x1090003

    const/4 v6, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p0

    invoke-direct/range {v1 .. v6}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$EncodingAdapter;-><init>(Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;Landroid/content/Context;I[Ljava/lang/String;I)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_EncodingAdapter:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$EncodingAdapter;

    .line 272
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_lvEncoding:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_EncodingAdapter:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$EncodingAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 277
    invoke-static/range {p0 .. p0}, Lcom/infraware/common/util/Utils;->getDensityDpi(Landroid/app/Activity;)I

    move-result v15

    .line 278
    .local v15, "nDpi":I
    const v1, 0x422b851f    # 42.88f

    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v10

    .line 280
    .local v10, "itemHeight":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_lvEncoding:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v16

    check-cast v16, Landroid/widget/LinearLayout$LayoutParams;

    .line 282
    .local v16, "params":Landroid/widget/LinearLayout$LayoutParams;
    array-length v1, v5

    int-to-float v1, v1

    mul-float v2, v10, v1

    const/16 v1, 0xc8

    if-ne v15, v1, :cond_4

    const/4 v1, 0x0

    :goto_2
    int-to-float v1, v1

    add-float/2addr v1, v2

    const/high16 v2, 0x41c80000    # 25.0f

    sub-float/2addr v1, v2

    float-to-int v1, v1

    move-object/from16 v0, v16

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 284
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_lvEncoding:Landroid/widget/ListView;

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 286
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0501e4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_encodingTextColor:Landroid/content/res/ColorStateList;

    .line 289
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_lvEncoding:Landroid/widget/ListView;

    new-instance v2, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$2;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$2;-><init>(Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 304
    return-void

    .line 282
    :cond_4
    array-length v1, v5

    div-int/lit8 v1, v1, 0x3

    goto :goto_2
.end method

.method private onOrientationChanged()V
    .locals 3

    .prologue
    .line 563
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_nOrientation:I

    .line 566
    iget v1, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_nOrientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 568
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_llActionBar:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 569
    .local v0, "linearLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v1, 0x42400000    # 48.0f

    invoke-static {p0, v1}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 570
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_llActionBar:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 578
    :goto_0
    return-void

    .line 574
    .end local v0    # "linearLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_llActionBar:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 575
    .restart local v0    # "linearLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v1, 0x42200000    # 40.0f

    invoke-static {p0, v1}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 576
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_llActionBar:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private setDisplayType()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 186
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->getDisplaySize()[I

    move-result-object v0

    .line 187
    .local v0, "displayInfo":[I
    aget v2, v0, v4

    aget v3, v0, v5

    if-le v2, v3, :cond_0

    aget v1, v0, v4

    .line 189
    .local v1, "max":I
    :goto_0
    const/16 v2, 0x320

    if-le v1, v2, :cond_1

    .line 190
    iput v5, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_displayType:I

    .line 194
    :goto_1
    return-void

    .line 187
    .end local v1    # "max":I
    :cond_0
    aget v1, v0, v5

    goto :goto_0

    .line 192
    .restart local v1    # "max":I
    :cond_1
    iput v4, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_displayType:I

    goto :goto_1
.end method

.method private setDone()V
    .locals 3

    .prologue
    .line 430
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 431
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "FontSize"

    iget v2, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_nFontSize:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 432
    const-string/jumbo v1, "Theme"

    iget v2, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_nTheme:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 433
    const-string/jumbo v1, "Encoding"

    iget v2, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_nEncoding:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 435
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setResult(ILandroid/content/Intent;)V

    .line 437
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->finish()V

    .line 438
    return-void
.end method

.method private setEncodingSelected(I)V
    .locals 1
    .param p1, "nIndex"    # I

    .prologue
    .line 425
    iput p1, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_nEncoding:I

    .line 426
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_EncodingAdapter:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$EncodingAdapter;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$EncodingAdapter;->notifyDataSetChanged()V

    .line 427
    return-void
.end method

.method private setEventListener()V
    .locals 2

    .prologue
    .line 324
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_btnDone:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 326
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_ivThemeList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 327
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_ivThemeList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 326
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 329
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_FontsizeList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 330
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_FontsizeList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 329
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 332
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_lvEncoding:Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 333
    return-void
.end method

.method private setThemeSelected(I)V
    .locals 8
    .param p1, "nIndex"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x4

    .line 396
    iput p1, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_nTheme:I

    .line 397
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_ivThemeList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 398
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_ivThemeList:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    if-ne v0, p1, :cond_0

    const/4 v5, 0x1

    :goto_1
    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 397
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v5, v6

    .line 398
    goto :goto_1

    .line 401
    :cond_1
    const v4, 0x7f0b023d

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 402
    .local v3, "selYellow":Landroid/widget/ImageView;
    const v4, 0x7f0b023f

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 403
    .local v2, "selLightGray":Landroid/widget/ImageView;
    const v4, 0x7f0b0241

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 405
    .local v1, "selBlack":Landroid/widget/ImageView;
    iget v4, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_nTheme:I

    packed-switch v4, :pswitch_data_0

    .line 422
    :goto_2
    return-void

    .line 407
    :pswitch_0
    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 408
    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 409
    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 412
    :pswitch_1
    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 413
    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 414
    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 417
    :pswitch_2
    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 418
    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 419
    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 405
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setValueFromIntent()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 336
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 338
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "FontSize"

    const/16 v3, 0xa

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 339
    .local v0, "fontsize":I
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setFontSizeSelected(I)V

    .line 353
    const-string/jumbo v2, "Theme"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setThemeSelected(I)V

    .line 354
    const-string/jumbo v2, "UTF-8"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 355
    iget v2, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_utf8index:I

    invoke-direct {p0, v2}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setEncodingSelected(I)V

    .line 358
    :goto_0
    return-void

    .line 357
    :cond_0
    const-string/jumbo v2, "Encoding"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setEncodingSelected(I)V

    goto :goto_0
.end method


# virtual methods
.method public getDisplaySize()[I
    .locals 4

    .prologue
    .line 174
    const/4 v2, 0x2

    new-array v0, v2, [I

    .line 176
    .local v0, "displayInfo":[I
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 178
    .local v1, "displayMetrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 179
    const/4 v2, 0x0

    iget v3, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    aput v3, v0, v2

    .line 180
    const/4 v2, 0x1

    iget v3, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    aput v3, v0, v2

    .line 182
    return-object v0
.end method

.method public getFontSize()I
    .locals 1

    .prologue
    .line 558
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_nFontSize:I

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 448
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 482
    :goto_0
    :pswitch_0
    return-void

    .line 450
    :pswitch_1
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setDone()V

    goto :goto_0

    .line 453
    :pswitch_2
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setFontSizeSelected(I)V

    goto :goto_0

    .line 456
    :pswitch_3
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setFontSizeSelected(I)V

    goto :goto_0

    .line 459
    :pswitch_4
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setFontSizeSelected(I)V

    goto :goto_0

    .line 462
    :pswitch_5
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setFontSizeSelected(I)V

    goto :goto_0

    .line 465
    :pswitch_6
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setFontSizeSelected(I)V

    goto :goto_0

    .line 468
    :pswitch_7
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setFontSizeSelected(I)V

    goto :goto_0

    .line 471
    :pswitch_8
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setThemeSelected(I)V

    goto :goto_0

    .line 474
    :pswitch_9
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setThemeSelected(I)V

    goto :goto_0

    .line 477
    :pswitch_a
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setThemeSelected(I)V

    goto :goto_0

    .line 448
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0231
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_a
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 124
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 146
    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v1}, Lcom/infraware/common/util/Utils;->getLocaleType(Ljava/util/Locale;)I

    move-result v0

    .line 147
    .local v0, "nlocale":I
    iget v1, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_nLocaleType:I

    if-eq v1, v0, :cond_0

    .line 148
    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_nLocaleType:I

    .line 149
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->onLocaleChanged()V

    .line 153
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->mScrollHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->mScrollTask:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 154
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->mScrollHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->mScrollTask:Ljava/lang/Runnable;

    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->getScrollTime()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 155
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->initControls()V

    .line 156
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->onOrientationChanged()V

    .line 157
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 93
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 95
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->requestWindowFeature(I)Z

    .line 97
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    invoke-static {p0}, Lcom/infraware/common/util/Utils;->fullScreenActivity(Landroid/app/Activity;)V

    .line 102
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/infraware/common/util/Utils;->getCurrentLocaleType(Landroid/content/res/Resources;)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_nLocaleType:I

    .line 104
    const v0, 0x7f030054

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setContentView(I)V

    .line 107
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$CloseActionReceiver;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$CloseActionReceiver;-><init>(Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$CloseActionReceiver;

    .line 108
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    .line 109
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    const-string/jumbo v1, "com.infraware.polarisviewer5.CLOSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$CloseActionReceiver;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 112
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setDisplayType()V

    .line 114
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->initControls()V

    .line 115
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setEventListener()V

    .line 116
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setValueFromIntent()V

    .line 117
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->onOrientationChanged()V

    .line 118
    iput-object p0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_Activity:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    .line 119
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 582
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$CloseActionReceiver;

    if-eqz v0, :cond_0

    .line 583
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$CloseActionReceiver;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 585
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 586
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 486
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    check-cast p1, Landroid/widget/ListView;

    .end local p1    # "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$EncodingAdapter;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$EncodingAdapter;->getAdapterType()I

    move-result v0

    .line 488
    .local v0, "type":I
    packed-switch v0, :pswitch_data_0

    .line 493
    :goto_0
    return-void

    .line 490
    :pswitch_0
    invoke-direct {p0, p3}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->setEncodingSelected(I)V

    goto :goto_0

    .line 488
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method onLocaleChanged()V
    .locals 3

    .prologue
    .line 161
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_tvTitle:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_tvTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0702fc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_tvFontsize:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_tvFontsize:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0702ff

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_tvTheme:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_tvTheme:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070300

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_tvEncoding:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_tvEncoding:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0702fd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    :cond_3
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2
    .param p1, "hasFocus"    # Z

    .prologue
    const/4 v1, 0x0

    .line 169
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 170
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_svPreferences:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1, v1}, Landroid/widget/ScrollView;->scrollTo(II)V

    .line 171
    return-void
.end method

.method public setFontSizeAtWheelButton(I)V
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 361
    add-int/lit8 v0, p1, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_nFontSize:I

    .line 363
    return-void
.end method

.method public setFontSizeSelected(I)V
    .locals 4
    .param p1, "size"    # I

    .prologue
    .line 366
    const/4 v1, 0x0

    .line 367
    .local v1, "nIndex":I
    packed-switch p1, :pswitch_data_0

    .line 388
    :goto_0
    :pswitch_0
    iput p1, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_nFontSize:I

    .line 390
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_FontsizeList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 391
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_FontsizeList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-ne v0, v1, :cond_0

    const/4 v3, 0x1

    :goto_2
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setSelected(Z)V

    .line 390
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 369
    .end local v0    # "i":I
    :pswitch_1
    const/4 v1, 0x0

    .line 370
    goto :goto_0

    .line 372
    :pswitch_2
    const/4 v1, 0x1

    .line 373
    goto :goto_0

    .line 375
    :pswitch_3
    const/4 v1, 0x2

    .line 376
    goto :goto_0

    .line 378
    :pswitch_4
    const/4 v1, 0x3

    .line 379
    goto :goto_0

    .line 381
    :pswitch_5
    const/4 v1, 0x4

    .line 382
    goto :goto_0

    .line 384
    :pswitch_6
    const/4 v1, 0x5

    goto :goto_0

    .line 391
    .restart local v0    # "i":I
    :cond_0
    const/4 v3, 0x0

    goto :goto_2

    .line 393
    :cond_1
    return-void

    .line 367
    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method
