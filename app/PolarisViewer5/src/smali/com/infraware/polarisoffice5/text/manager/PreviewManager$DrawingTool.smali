.class public Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;
.super Ljava/lang/Object;
.source "PreviewManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/text/manager/PreviewManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DrawingTool"
.end annotation


# instance fields
.field private bitmap:Landroid/graphics/Bitmap;

.field private canvas:Landroid/graphics/Canvas;

.field lineHeight:I

.field lineNum:I

.field maxHeight:I

.field private textpaint:Landroid/graphics/Paint;

.field private tf:Landroid/graphics/Typeface;

.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/manager/PreviewManager;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/manager/PreviewManager;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 450
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->this$0:Lcom/infraware/polarisoffice5/text/manager/PreviewManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 451
    const/16 v0, 0x253

    const/16 v1, 0x34a

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->bitmap:Landroid/graphics/Bitmap;

    .line 452
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->bitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->canvas:Landroid/graphics/Canvas;

    .line 453
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->textpaint:Landroid/graphics/Paint;

    .line 455
    # invokes: Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->getParentTheme()V
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->access$000(Lcom/infraware/polarisoffice5/text/manager/PreviewManager;)V

    .line 457
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->canvas:Landroid/graphics/Canvas;

    # invokes: Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->getPaperColor()I
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->access$100(Lcom/infraware/polarisoffice5/text/manager/PreviewManager;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 459
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-static {v0, v3}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->tf:Landroid/graphics/Typeface;

    .line 461
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->textpaint:Landroid/graphics/Paint;

    # invokes: Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->getPenColor()I
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->access$200(Lcom/infraware/polarisoffice5/text/manager/PreviewManager;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 462
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->textpaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->tf:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 463
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->textpaint:Landroid/graphics/Paint;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_fontSize:F
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->access$300(Lcom/infraware/polarisoffice5/text/manager/PreviewManager;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 465
    # getter for: Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_bitmapHeight:I
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->access$400(Lcom/infraware/polarisoffice5/text/manager/PreviewManager;)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->maxHeight:I

    .line 466
    # getter for: Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_lineHeight:I
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->access$500(Lcom/infraware/polarisoffice5/text/manager/PreviewManager;)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->lineHeight:I

    .line 467
    iput v3, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->lineNum:I

    .line 468
    return-void
.end method


# virtual methods
.method public draw()Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 471
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->lineHeight:I

    iget v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->lineNum:I

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->maxHeight:I

    if-ge v0, v1, :cond_0

    .line 472
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->lineNum:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->lineNum:I

    .line 473
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->canvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->this$0:Lcom/infraware/polarisoffice5/text/manager/PreviewManager;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_lineString:Ljava/lang/String;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->access$600(Lcom/infraware/polarisoffice5/text/manager/PreviewManager;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->this$0:Lcom/infraware/polarisoffice5/text/manager/PreviewManager;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->m_lineString:Ljava/lang/String;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->access$600(Lcom/infraware/polarisoffice5/text/manager/PreviewManager;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x0

    iget v5, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->lineHeight:I

    iget v6, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->lineNum:I

    mul-int/2addr v5, v6

    int-to-float v5, v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->textpaint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    .line 476
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public getThumbNail()[B
    .locals 6

    .prologue
    .line 482
    :try_start_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->this$0:Lcom/infraware/polarisoffice5/text/manager/PreviewManager;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->bitmap:Landroid/graphics/Bitmap;

    # invokes: Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->save(Landroid/graphics/Bitmap;)V
    invoke-static {v3, v4}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->access$700(Lcom/infraware/polarisoffice5/text/manager/PreviewManager;Landroid/graphics/Bitmap;)V

    .line 483
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 484
    .local v1, "byteArray":Ljava/io/ByteArrayOutputStream;
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/PreviewManager$DrawingTool;->this$0:Lcom/infraware/polarisoffice5/text/manager/PreviewManager;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/manager/PreviewManager;->getPreview()Landroid/graphics/Bitmap;

    move-result-object v3

    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x64

    invoke-virtual {v3, v4, v5, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 486
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 490
    .end local v1    # "byteArray":Ljava/io/ByteArrayOutputStream;
    :goto_0
    return-object v0

    .line 489
    :catch_0
    move-exception v2

    .line 490
    .local v2, "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    goto :goto_0
.end method
