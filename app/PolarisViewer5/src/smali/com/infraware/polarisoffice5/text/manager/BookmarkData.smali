.class public Lcom/infraware/polarisoffice5/text/manager/BookmarkData;
.super Ljava/lang/Object;
.source "BookmarkData.java"


# instance fields
.field private m_bookmarkName:Ljava/lang/String;

.field private m_fileName:Ljava/lang/String;

.field private m_filesize:J

.field private m_lastmodified:J

.field private m_offsetEnd:I

.field private m_offsetStart:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IIJJ)V
    .locals 0
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "bookmarkname"    # Ljava/lang/String;
    .param p3, "offsetStart"    # I
    .param p4, "offsetEnd"    # I
    .param p5, "filesize"    # J
    .param p7, "lastmodified"    # J

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->setFileName(Ljava/lang/String;)V

    .line 26
    invoke-virtual {p0, p2}, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->setBookmarkName(Ljava/lang/String;)V

    .line 27
    invoke-virtual {p0, p5, p6}, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->setFilesize(J)V

    .line 28
    invoke-virtual {p0, p7, p8}, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->setLastmodified(J)V

    .line 29
    invoke-virtual {p0, p3}, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->setOffsetStart(I)V

    .line 30
    invoke-virtual {p0, p4}, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->setOffsetEnd(I)V

    .line 31
    return-void
.end method


# virtual methods
.method public getBookmarkName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->m_bookmarkName:Ljava/lang/String;

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->m_fileName:Ljava/lang/String;

    return-object v0
.end method

.method public getFilesize()J
    .locals 2

    .prologue
    .line 50
    iget-wide v0, p0, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->m_filesize:J

    return-wide v0
.end method

.method public getLastmodified()J
    .locals 2

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->m_lastmodified:J

    return-wide v0
.end method

.method public getOffsetEnd()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->m_offsetEnd:I

    return v0
.end method

.method public getOffsetStart()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->m_offsetStart:I

    return v0
.end method

.method public setBookmarkName(Ljava/lang/String;)V
    .locals 0
    .param p1, "m_bookmarkName"    # Ljava/lang/String;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->m_bookmarkName:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public setFileName(Ljava/lang/String;)V
    .locals 0
    .param p1, "m_fileName"    # Ljava/lang/String;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->m_fileName:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public setFilesize(J)V
    .locals 0
    .param p1, "m_filesize"    # J

    .prologue
    .line 54
    iput-wide p1, p0, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->m_filesize:J

    .line 55
    return-void
.end method

.method public setLastmodified(J)V
    .locals 0
    .param p1, "m_lastmodified"    # J

    .prologue
    .line 62
    iput-wide p1, p0, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->m_lastmodified:J

    .line 63
    return-void
.end method

.method public setOffsetEnd(I)V
    .locals 0
    .param p1, "m_offsetEnd"    # I

    .prologue
    .line 78
    iput p1, p0, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->m_offsetEnd:I

    .line 79
    return-void
.end method

.method public setOffsetStart(I)V
    .locals 0
    .param p1, "m_offsetStart"    # I

    .prologue
    .line 70
    iput p1, p0, Lcom/infraware/polarisoffice5/text/manager/BookmarkData;->m_offsetStart:I

    .line 71
    return-void
.end method
