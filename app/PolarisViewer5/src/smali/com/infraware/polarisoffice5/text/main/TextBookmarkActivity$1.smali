.class Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$1;
.super Ljava/lang/Object;
.source "TextBookmarkActivity.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/word/BookmarkActivity$BookmarkCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->initialize()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$1;->this$0:Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteButtonClicked(I)V
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 138
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$1;->this$0:Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_bookmarkList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->access$000(Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 139
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$1;->this$0:Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;

    # invokes: Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->initBookmarkEditText()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->access$100(Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;)V

    .line 140
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$1;->this$0:Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;

    # invokes: Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->initItemLayout()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->access$200(Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;)V

    .line 142
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$1;->this$0:Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->m_adapter:Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;)Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;->notifyDataSetChanged()V

    .line 143
    return-void
.end method
