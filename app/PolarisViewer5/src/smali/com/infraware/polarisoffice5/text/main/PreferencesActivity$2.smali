.class Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$2;
.super Ljava/lang/Object;
.source "PreferencesActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->initControls()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;)V
    .locals 0

    .prologue
    .line 289
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$2;->this$0:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x0

    .line 292
    packed-switch p2, :pswitch_data_0

    .line 301
    :goto_0
    return v2

    .line 295
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$2;->this$0:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_lvEncoding:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->access$200(Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getSelectedView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$2;->this$0:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_svPreferences:Landroid/widget/ScrollView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;)Landroid/widget/ScrollView;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$2;->this$0:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_lvEncoding:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->access$200(Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getSelectedView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v2, v1}, Landroid/widget/ScrollView;->scrollTo(II)V

    goto :goto_0

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$2;->this$0:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_svPreferences:Landroid/widget/ScrollView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;)Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/widget/ScrollView;->scrollTo(II)V

    goto :goto_0

    .line 292
    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
