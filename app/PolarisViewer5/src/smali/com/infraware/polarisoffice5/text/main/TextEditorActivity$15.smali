.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$15;
.super Ljava/lang/Object;
.source "TextEditorActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
    .locals 0

    .prologue
    .line 1839
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$15;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 1843
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$15;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getCurrentTopCaret()I

    move-result v0

    .line 1844
    .local v0, "topCaretPos":I
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$15;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v1

    invoke-virtual {v1, v0, v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setTTSSelection(II)V

    .line 1845
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$15;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$15;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getTTSManager()Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    move-result-object v2

    const-string/jumbo v3, ""

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->textReadAndPlay(Lcom/infraware/polarisoffice5/text/manager/TTSManager;Ljava/lang/String;Z)V

    .line 1846
    return-void
.end method
