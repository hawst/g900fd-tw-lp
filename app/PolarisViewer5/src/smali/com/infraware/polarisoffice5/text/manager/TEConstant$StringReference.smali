.class public interface abstract Lcom/infraware/polarisoffice5/text/manager/TEConstant$StringReference;
.super Ljava/lang/Object;
.source "TEConstant.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/text/manager/TEConstant;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "StringReference"
.end annotation


# static fields
.field public static final SR_BGM_INTENT:Ljava/lang/String; = "com.android.music.musicservicecommand"

.field public static final SR_BOOKMARKBYTEPOSITION:Ljava/lang/String; = "Bookmark"

.field public static final SR_DEFAULT_EDIT_CHARSET:Ljava/lang/String; = "UTF-16LE"

.field public static final SR_DEFAULT_READ_CHARSET:Ljava/lang/String; = "UTF-8"

.field public static final SR_ENCODING:Ljava/lang/String; = "Encoding"

.field public static final SR_FILEPATH:Ljava/lang/String; = "FilePath"

.field public static final SR_FONTSIZE:Ljava/lang/String; = "FontSize"

.field public static final SR_HEADSET_BRAODCAST_INTENT:Ljava/lang/String; = "android.intent.action.HEADSET_PLUG"

.field public static final SR_ISASCFILE:Ljava/lang/String; = "UTF-8"

.field public static final SR_ISTEXTEDITORACTIVITY:Ljava/lang/String; = "istexteditoractivity"

.field public static final SR_POLARISOFFICE_INTENT:Ljava/lang/String; = "com.infraware.polarisoffice5"

.field public static final SR_SAMSUNGPRINT:Ljava/lang/String; = "com.sec.android.app.mobileprint"

.field public static final SR_SAMSUNGPRINT_INTENT:Ljava/lang/String; = "com.sec.android.app.mobileprint.PRINT"

.field public static final SR_SP_NOT_EXIST:Ljava/lang/String; = "notexist"

.field public static final SR_SP_TEXT_INFO:Ljava/lang/String; = "textinfo"

.field public static final SR_THEME:Ljava/lang/String; = "Theme"

.field public static final SR_URL_INFIX_WIKI:Ljava/lang/String; = ".wikipedia.org/wiki/"

.field public static final SR_URL_PREFIX_WIKI:Ljava/lang/String; = "http://"
