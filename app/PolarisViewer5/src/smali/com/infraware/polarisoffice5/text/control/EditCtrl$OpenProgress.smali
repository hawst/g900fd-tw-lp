.class Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;
.super Ljava/lang/Thread;
.source "EditCtrl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/text/control/EditCtrl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OpenProgress"
.end annotation


# instance fields
.field private nCurCaretPos:I

.field strFilePath:Ljava/lang/String;

.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;ILjava/lang/String;)V
    .locals 6
    .param p2, "nCurCaretPos"    # I
    .param p3, "strFilePath"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 3624
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 3621
    iput v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->nCurCaretPos:I

    .line 3622
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->strFilePath:Ljava/lang/String;

    .line 3625
    iput p2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->nCurCaretPos:I

    .line 3626
    iput-object p3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->strFilePath:Ljava/lang/String;

    .line 3628
    const-string/jumbo v1, "/"

    invoke-virtual {p3, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p3, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 3630
    .local v0, "displayPath":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0701bf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v1

    # setter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_pd:Landroid/app/ProgressDialog;
    invoke-static {p1, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$1502(Lcom/infraware/polarisoffice5/text/control/EditCtrl;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 3631
    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_pd:Landroid/app/ProgressDialog;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$1500(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/app/ProgressDialog;

    move-result-object v1

    new-instance v2, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$1;

    invoke-direct {v2, p0, p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$1;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 3639
    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_pd:Landroid/app/ProgressDialog;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$1500(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 3640
    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_pd:Landroid/app/ProgressDialog;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$1500(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 3641
    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_pd:Landroid/app/ProgressDialog;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$1500(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/app/ProgressDialog;

    move-result-object v1

    new-instance v2, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$2;

    invoke-direct {v2, p0, p1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$2;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 3649
    invoke-virtual {p1, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setLoadingProgress(Z)V

    .line 3650
    return-void
.end method

.method static synthetic access$1900(Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    .prologue
    .line 3620
    iget v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->nCurCaretPos:I

    return v0
.end method

.method static synthetic access$1902(Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;
    .param p1, "x1"    # I

    .prologue
    .line 3620
    iput p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->nCurCaretPos:I

    return p1
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 3653
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$1700(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->strFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->initBufferFromFile(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3654
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->finish()V

    .line 3727
    :goto_0
    return-void

    .line 3658
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$2200(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
