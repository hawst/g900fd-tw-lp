.class Lcom/infraware/polarisoffice5/text/manager/TTSManager$3;
.super Ljava/lang/Object;
.source "TTSManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/manager/TTSManager;->onInit(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$3;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 205
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$3;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->setIsPlay(Z)V

    .line 206
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 207
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$3;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->getTTSMode()I

    move-result v1

    if-nez v1, :cond_0

    .line 208
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$3;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->access$100(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070243

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 209
    .local v0, "ment":Ljava/lang/String;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$3;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->access$100(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$3;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    invoke-virtual {v1, v2, v0, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->textReadAndPlay(Lcom/infraware/polarisoffice5/text/manager/TTSManager;Ljava/lang/String;Z)V

    .line 216
    .end local v0    # "ment":Ljava/lang/String;
    :goto_0
    return-void

    .line 211
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$3;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->access$100(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$3;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    const-string/jumbo v3, ""

    invoke-virtual {v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->textReadAndPlay(Lcom/infraware/polarisoffice5/text/manager/TTSManager;Ljava/lang/String;Z)V

    goto :goto_0

    .line 214
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$3;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    # invokes: Lcom/infraware/polarisoffice5/text/manager/TTSManager;->sayText()V
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->access$000(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)V

    goto :goto_0
.end method
