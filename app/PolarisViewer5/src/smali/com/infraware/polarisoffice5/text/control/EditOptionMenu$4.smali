.class Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$4;
.super Ljava/lang/Object;
.source "EditOptionMenu.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->optionPopupwindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)V
    .locals 0

    .prologue
    .line 220
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$4;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 224
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$4;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->popupitem:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->access$500(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/MultiListItem;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/MultiListItem;->getItemId()I

    move-result v0

    .line 226
    .local v0, "itemId":I
    packed-switch v0, :pswitch_data_0

    .line 250
    :goto_0
    :pswitch_0
    return-void

    .line 246
    :pswitch_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$4;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    const/4 v2, 0x1

    # setter for: Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->isClicked:Z
    invoke-static {v1, v2}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->access$302(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;Z)Z

    .line 247
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$4;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->access$400(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v1

    invoke-virtual {v1, v0, p2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->optionPopupItemEvent(ILandroid/view/View;)V

    goto :goto_0

    .line 226
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
