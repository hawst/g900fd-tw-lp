.class Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$EncodingAdapter;
.super Landroid/widget/ArrayAdapter;
.source "PreferencesActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EncodingAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private Encodingchecksize:I

.field private adapterType:I

.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;Landroid/content/Context;I[Ljava/lang/String;I)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "textViewResourceId"    # I
    .param p4, "mItems"    # [Ljava/lang/String;
    .param p5, "type"    # I

    .prologue
    .line 500
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$EncodingAdapter;->this$0:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    .line 501
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 502
    iput p5, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$EncodingAdapter;->adapterType:I

    .line 503
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$EncodingAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x422b851f    # 42.88f

    invoke-static {v0, v1}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$EncodingAdapter;->Encodingchecksize:I

    .line 504
    return-void
.end method


# virtual methods
.method public getAdapterType()I
    .locals 1

    .prologue
    .line 507
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$EncodingAdapter;->adapterType:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x0

    .line 515
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$EncodingAdapter;->this$0:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    const-string/jumbo v7, "layout_inflater"

    invoke-virtual {v5, v7}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 516
    .local v2, "layoutInflater":Landroid/view/LayoutInflater;
    const v5, 0x7f030051

    const/4 v7, 0x0

    invoke-virtual {v2, v5, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 518
    .local v4, "viewLocal":Landroid/view/View;
    if-eqz v4, :cond_1

    .line 519
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$EncodingAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 520
    .local v0, "item":Ljava/lang/String;
    const v5, 0x7f0b0224

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 521
    .local v3, "tvEncodingItem":Landroid/widget/TextView;
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 522
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$EncodingAdapter;->this$0:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_encodingTextColor:Landroid/content/res/ColorStateList;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->access$700(Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;)Landroid/content/res/ColorStateList;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 523
    const v5, 0x7f0b0225

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 525
    .local v1, "ivCheckIcon":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$EncodingAdapter;->getAdapterType()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 536
    :cond_0
    :goto_0
    invoke-virtual {v3}, Landroid/widget/TextView;->isSelected()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 537
    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 542
    .end local v0    # "item":Ljava/lang/String;
    .end local v1    # "ivCheckIcon":Landroid/widget/ImageView;
    .end local v3    # "tvEncodingItem":Landroid/widget/TextView;
    :cond_1
    :goto_1
    return-object v4

    .line 527
    .restart local v0    # "item":Ljava/lang/String;
    .restart local v1    # "ivCheckIcon":Landroid/widget/ImageView;
    .restart local v3    # "tvEncodingItem":Landroid/widget/TextView;
    :pswitch_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$EncodingAdapter;->this$0:Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->m_nEncoding:I
    invoke-static {v5}, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;->access$800(Lcom/infraware/polarisoffice5/text/main/PreferencesActivity;)I

    move-result v5

    if-ne p1, v5, :cond_2

    const/4 v5, 0x1

    :goto_2
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setSelected(Z)V

    .line 528
    invoke-virtual {v3}, Landroid/widget/TextView;->isSelected()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 530
    const/high16 v5, -0x1000000

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 531
    iget v5, p0, Lcom/infraware/polarisoffice5/text/main/PreferencesActivity$EncodingAdapter;->Encodingchecksize:I

    invoke-virtual {v3, v6, v6, v5, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_0

    :cond_2
    move v5, v6

    .line 527
    goto :goto_2

    .line 539
    :cond_3
    const/4 v5, 0x4

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 525
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
