.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;
.super Ljava/lang/Thread;
.source "TextEditorActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FindProgress"
.end annotation


# instance fields
.field private nMode:I

.field private nOffset:I

.field private pd:Landroid/app/ProgressDialog;

.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;I)V
    .locals 5
    .param p2, "mode"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4383
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 4379
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->pd:Landroid/app/ProgressDialog;

    .line 4381
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->nOffset:I

    .line 4384
    iput p2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->nMode:I

    .line 4386
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->pd:Landroid/app/ProgressDialog;

    .line 4387
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 4388
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 4389
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700a9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 4390
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 4392
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setFlingFlag(Z)V

    .line 4394
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->pd:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress$1;

    invoke-direct {v1, p0, p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress$1;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 4401
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setLoadingProgress(Z)V

    .line 4403
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->requestFocus()Z

    .line 4404
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hideSoftKeyboard()Z

    .line 4405
    return-void
.end method

.method static synthetic access$4700(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;

    .prologue
    .line 4378
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->nMode:I

    return v0
.end method

.method static synthetic access$4800(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;

    .prologue
    .line 4378
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->nOffset:I

    return v0
.end method

.method static synthetic access$4900(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;

    .prologue
    .line 4378
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->pd:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 4409
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$2600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->hasSpecialWord(Ljava/lang/String;)Z
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$4200(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4410
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # setter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachWholeWord:Z
    invoke-static {v0, v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$4302(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Z)Z

    .line 4412
    :cond_0
    iget v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->nMode:I

    packed-switch v0, :pswitch_data_0

    .line 4426
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress$2;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress$2;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 4434
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress$3;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress$3;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 4457
    return-void

    .line 4414
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$2600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachCase:Z
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$4400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Z

    move-result v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachWholeWord:Z
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$4300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3, v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findNextText(Ljava/lang/String;ZZZ)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->nOffset:I

    goto :goto_0

    .line 4417
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$2600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachCase:Z
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$4400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Z

    move-result v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachWholeWord:Z
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$4300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findPreText(Ljava/lang/String;ZZZ)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->nOffset:I

    goto :goto_0

    .line 4420
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplaceFind:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$4500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachCase:Z
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$4400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Z

    move-result v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachWholeWord:Z
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$4300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3, v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findNextText(Ljava/lang/String;ZZZ)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$FindProgress;->nOffset:I

    goto/16 :goto_0

    .line 4412
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
