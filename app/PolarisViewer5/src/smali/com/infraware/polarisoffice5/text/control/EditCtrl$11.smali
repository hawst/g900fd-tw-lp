.class Lcom/infraware/polarisoffice5/text/control/EditCtrl$11;
.super Ljava/lang/Object;
.source "EditCtrl.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setToastPopupEventListener(Lcom/infraware/polarisoffice5/text/control/ToastPopup;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

.field final synthetic val$toastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

.field final synthetic val$tvSelectAll:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;Landroid/widget/ImageView;Lcom/infraware/polarisoffice5/text/control/ToastPopup;)V
    .locals 0

    .prologue
    .line 2426
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$11;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iput-object p2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$11;->val$tvSelectAll:Landroid/widget/ImageView;

    iput-object p3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$11;->val$toastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "view"    # Landroid/view/View;
    .param p2, "evt"    # Landroid/view/MotionEvent;

    .prologue
    const v8, 0x7f02000f

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 2429
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    .line 2430
    .local v0, "lPadding":I
    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    .line 2432
    .local v2, "rPadding":I
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 2433
    .local v1, "r":Landroid/graphics/Rect;
    const/4 v3, 0x1

    .line 2435
    .local v3, "result":Z
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 2471
    :cond_0
    :goto_0
    const/4 v4, 0x1

    return v4

    .line 2437
    :pswitch_0
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$11;->val$tvSelectAll:Landroid/widget/ImageView;

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 2438
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$11;->val$tvSelectAll:Landroid/widget/ImageView;

    invoke-virtual {v4, v0, v6, v2, v6}, Landroid/widget/ImageView;->setPadding(IIII)V

    goto :goto_0

    .line 2442
    :pswitch_1
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$11;->val$tvSelectAll:Landroid/widget/ImageView;

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 2444
    invoke-virtual {p1, v1}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 2445
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v1, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    .line 2447
    if-eqz v3, :cond_0

    .line 2451
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$11;->val$toastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->dismiss()V

    .line 2453
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$11;->val$tvSelectAll:Landroid/widget/ImageView;

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->playSoundEffect(I)V

    .line 2455
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$11;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$11;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v5

    invoke-virtual {v4, v6, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(II)V

    .line 2456
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$11;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->dismissPopup()V

    .line 2457
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$11;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v4, v7, v7}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->showToastPopup(FF)V

    goto :goto_0

    .line 2460
    :pswitch_2
    invoke-virtual {p1, v1}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 2461
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v1, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    .line 2463
    if-eqz v3, :cond_1

    .line 2464
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$11;->val$tvSelectAll:Landroid/widget/ImageView;

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 2466
    :cond_1
    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$11;->val$tvSelectAll:Landroid/widget/ImageView;

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_0

    .line 2435
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
