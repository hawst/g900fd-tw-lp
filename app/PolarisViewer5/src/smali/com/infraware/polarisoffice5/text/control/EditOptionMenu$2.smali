.class Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$2;
.super Ljava/lang/Object;
.source "EditOptionMenu.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->optionPopupwindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$2;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 111
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    .line 112
    sparse-switch p2, :sswitch_data_0

    :cond_0
    :goto_0
    move v0, v1

    .line 134
    :goto_1
    return v0

    .line 114
    :sswitch_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$2;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_listView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->access$000(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$2;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_listView:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->access$000(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_0

    .line 115
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$2;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_listView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->access$000(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)Landroid/widget/ListView;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$2;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    # invokes: Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->getEnabledTopPosition()I
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->access$100(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setSelection(I)V

    .line 116
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$2;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_listView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->access$000(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->playSoundEffect(I)V

    goto :goto_1

    .line 121
    :sswitch_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$2;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    # invokes: Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->getEnabledTopPosition()I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->access$100(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 122
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$2;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_listView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->access$000(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$2;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    # invokes: Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->getEnabledTopPosition()I
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->access$100(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 123
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$2;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_listView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->access$000(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)Landroid/widget/ListView;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$2;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    # invokes: Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->getEnabledBottomPosition()I
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->access$200(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setSelection(I)V

    .line 124
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$2;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_listView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->access$000(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->playSoundEffect(I)V

    goto :goto_1

    .line 130
    :sswitch_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$2;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->dismissPopupWindow()Z

    goto :goto_0

    .line 112
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_1
        0x14 -> :sswitch_0
        0x6f -> :sswitch_2
    .end sparse-switch
.end method
