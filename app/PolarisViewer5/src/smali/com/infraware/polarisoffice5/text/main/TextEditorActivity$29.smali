.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$29;
.super Ljava/lang/Object;
.source "TextEditorActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->asyncLoading()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
    .locals 0

    .prologue
    .line 4108
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$29;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 4111
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$29;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_ibScrollThumb:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$3700(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isPressed()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 4112
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$29;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$29;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nQsPos:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$3800(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)I

    move-result v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$29;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_nQsEditHeight:I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$3900(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)I

    move-result v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$29;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_sizequickthumbsize:I
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$4000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->scrollByThumb(II)V

    .line 4113
    :cond_0
    return-void
.end method
