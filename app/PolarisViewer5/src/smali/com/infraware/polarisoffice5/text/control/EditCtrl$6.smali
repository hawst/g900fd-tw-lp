.class Lcom/infraware/polarisoffice5/text/control/EditCtrl$6;
.super Ljava/lang/Object;
.source "EditCtrl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/control/EditCtrl;->morePopupHandlerInitialize()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V
    .locals 0

    .prologue
    .line 1933
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$6;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1937
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$6;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # invokes: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setmorePopuplayout()V
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$900(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V

    .line 1939
    new-array v0, v6, [I

    .line 1940
    .local v0, "popupPos":[I
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$6;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$6;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->morePopupView:Landroid/view/View;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$1000(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$6;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->morePopupView:Landroid/view/View;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$1000(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    # invokes: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getPozOfPopupwindow([III)Z
    invoke-static {v1, v0, v2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$800(Lcom/infraware/polarisoffice5/text/control/EditCtrl;[III)Z

    .line 1941
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$6;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$1100(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1942
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$6;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$1100(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->getPopup()Landroid/widget/PopupWindow;

    move-result-object v1

    aget v2, v0, v4

    aget v3, v0, v5

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$6;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;
    invoke-static {v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$1100(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    move-result-object v4

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->getPopup()Landroid/widget/PopupWindow;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$6;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$1100(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    move-result-object v5

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->getPopup()Landroid/widget/PopupWindow;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/PopupWindow;->getHeight()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/PopupWindow;->update(IIII)V

    .line 1947
    :goto_0
    return-void

    .line 1944
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$6;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_morePopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$1100(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    move-result-object v1

    aget v2, v0, v4

    aget v3, v0, v5

    invoke-virtual {v1, v6, v2, v3}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->showToastPopup(III)V

    goto :goto_0
.end method
