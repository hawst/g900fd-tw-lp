.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$19;
.super Ljava/lang/Object;
.source "TextEditorActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
    .locals 0

    .prologue
    .line 2015
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$19;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 2018
    packed-switch p2, :pswitch_data_0

    .line 2028
    :goto_0
    return-void

    .line 2021
    :pswitch_0
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$19;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$2700(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/app/Activity;

    move-result-object v1

    const/16 v2, 0x131

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$19;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$2800(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/common/config/RuntimeConfig;->setBooleanPreference(Landroid/content/Context;IZ)V

    .line 2022
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$19;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$19;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mUri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$3000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->exportEmail(Landroid/net/Uri;)V

    goto :goto_0

    .line 2018
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method
