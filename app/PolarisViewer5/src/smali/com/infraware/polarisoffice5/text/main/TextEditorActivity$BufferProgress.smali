.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$BufferProgress;
.super Ljava/lang/Thread;
.source "TextEditorActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BufferProgress"
.end annotation


# instance fields
.field private pd:Landroid/app/ProgressDialog;

.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 4792
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$BufferProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 4790
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$BufferProgress;->pd:Landroid/app/ProgressDialog;

    .line 4793
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$BufferProgress;->pd:Landroid/app/ProgressDialog;

    .line 4794
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$BufferProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 4795
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$BufferProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 4796
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$BufferProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701bf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 4797
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$BufferProgress;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 4799
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setLoadingProgress(Z)V

    .line 4800
    return-void
.end method

.method static synthetic access$6300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$BufferProgress;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$BufferProgress;

    .prologue
    .line 4789
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$BufferProgress;->pd:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 4803
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$BufferProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->loadBuffer()V

    .line 4805
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$BufferProgress;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_Handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$BufferProgress$1;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$BufferProgress$1;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$BufferProgress;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 4813
    return-void
.end method
