.class Lcom/infraware/polarisoffice5/text/manager/TTSManager$5;
.super Ljava/lang/Object;
.source "TTSManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/manager/TTSManager;->doneTTS()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)V
    .locals 0

    .prologue
    .line 518
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$5;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 522
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$5;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->access$100(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setShowMode(I)V

    .line 524
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$5;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->access$100(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setFocusable(Z)V

    .line 525
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$5;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->access$100(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setTTSSelection(II)V

    .line 527
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$5;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->access$100(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->invalidate()V

    .line 529
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$5;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    invoke-virtual {v0, v3}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->setIsPlay(Z)V

    .line 530
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$5;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->access$100(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->ttsButtonImageChange(Z)V

    .line 532
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$5;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->access$400(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)Landroid/speech/tts/TextToSpeech;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 533
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$5;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->access$400(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)Landroid/speech/tts/TextToSpeech;

    move-result-object v0

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I

    .line 534
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$5;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->access$400(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)Landroid/speech/tts/TextToSpeech;

    move-result-object v0

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    .line 535
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$5;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    const/4 v1, 0x0

    # setter for: Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTS:Landroid/speech/tts/TextToSpeech;
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->access$402(Lcom/infraware/polarisoffice5/text/manager/TTSManager;Landroid/speech/tts/TextToSpeech;)Landroid/speech/tts/TextToSpeech;

    .line 536
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$5;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    # setter for: Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_finish:Z
    invoke-static {v0, v3}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->access$502(Lcom/infraware/polarisoffice5/text/manager/TTSManager;Z)Z

    .line 538
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$5;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/TTSManager;->BGMResumeHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->access$600(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v4, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 540
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$5;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTSMode:I
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->access$700(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)I

    move-result v0

    if-nez v0, :cond_2

    .line 541
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$5;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->access$100(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$5;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->access$100(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070312

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 545
    :cond_1
    :goto_0
    return-void

    .line 542
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$5;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_TTSMode:I
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->access$700(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 543
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$5;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->access$100(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/TTSManager$5;->this$0:Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/TTSManager;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->access$100(Lcom/infraware/polarisoffice5/text/manager/TTSManager;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07030f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
