.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;
.super Ljava/lang/Object;
.source "TextEditorActivity.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setEventListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
    .locals 0

    .prologue
    .line 1454
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    .line 1456
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1457
    .local v0, "rect":Landroid/graphics/Rect;
    invoke-virtual {p1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1458
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 1505
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 1461
    :sswitch_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/LinearLayout;

    move-result-object v3

    const v4, 0x7f070131

    invoke-static {v2, v3, p1, v4}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 1464
    :sswitch_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/LinearLayout;

    move-result-object v3

    const v4, 0x7f070244

    invoke-static {v2, v3, p1, v4}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 1467
    :sswitch_2
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/LinearLayout;

    move-result-object v3

    const v4, 0x7f0701cb

    invoke-static {v2, v3, p1, v4}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 1470
    :sswitch_3
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/LinearLayout;

    move-result-object v3

    const v4, 0x7f07004f

    invoke-static {v2, v3, p1, v4}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 1473
    :sswitch_4
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/LinearLayout;

    move-result-object v3

    const v4, 0x7f0702fc

    invoke-static {v2, v3, p1, v4}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 1476
    :sswitch_5
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llActionBar:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/LinearLayout;

    move-result-object v3

    const v4, 0x7f0702c4

    invoke-static {v2, v3, p1, v4}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 1479
    :sswitch_6
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llFind:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/LinearLayout;

    move-result-object v3

    const v4, 0x7f070255

    invoke-static {v2, v3, p1, v4}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 1482
    :sswitch_7
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llFind:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/LinearLayout;

    move-result-object v3

    const v4, 0x7f070053

    invoke-static {v2, v3, p1, v4}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 1485
    :sswitch_8
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llFind:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/LinearLayout;

    move-result-object v3

    const v4, 0x7f070256

    invoke-static {v2, v3, p1, v4}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto/16 :goto_0

    .line 1488
    :sswitch_9
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnDone:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1700(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v3

    const v4, 0x7f070061

    invoke-static {v2, v3, p1, v4}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto/16 :goto_0

    .line 1491
    :sswitch_a
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTSbar:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1800(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/LinearLayout;

    move-result-object v3

    const v4, 0x7f070069

    invoke-static {v2, v3, p1, v4}, Lcom/infraware/common/util/Utils;->showTTSbarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto/16 :goto_0

    .line 1494
    :sswitch_b
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_TTSmanager:Lcom/infraware/polarisoffice5/text/manager/TTSManager;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1900(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/manager/TTSManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/manager/TTSManager;->isPlay()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1495
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTSbar:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1800(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/LinearLayout;

    move-result-object v3

    const v4, 0x7f070068

    invoke-static {v2, v3, p1, v4}, Lcom/infraware/common/util/Utils;->showTTSbarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto/16 :goto_0

    .line 1497
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTSbar:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1800(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/LinearLayout;

    move-result-object v3

    const v4, 0x7f07006a

    invoke-static {v2, v3, p1, v4}, Lcom/infraware/common/util/Utils;->showTTSbarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto/16 :goto_0

    .line 1500
    :sswitch_c
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$10;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_llTTSbar:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1800(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/LinearLayout;

    move-result-object v3

    const v4, 0x7f070067

    invoke-static {v2, v3, p1, v4}, Lcom/infraware/common/util/Utils;->showTTSbarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto/16 :goto_0

    .line 1458
    :sswitch_data_0
    .sparse-switch
        0x7f0b0231 -> :sswitch_9
        0x7f0b0249 -> :sswitch_0
        0x7f0b024a -> :sswitch_1
        0x7f0b024b -> :sswitch_2
        0x7f0b024c -> :sswitch_3
        0x7f0b024d -> :sswitch_4
        0x7f0b024e -> :sswitch_5
        0x7f0b0250 -> :sswitch_6
        0x7f0b0251 -> :sswitch_7
        0x7f0b0252 -> :sswitch_8
        0x7f0b0269 -> :sswitch_a
        0x7f0b026a -> :sswitch_b
        0x7f0b026b -> :sswitch_c
    .end sparse-switch
.end method
