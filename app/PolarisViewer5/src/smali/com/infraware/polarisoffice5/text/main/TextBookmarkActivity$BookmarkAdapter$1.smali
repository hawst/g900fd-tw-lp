.class Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter$1;
.super Ljava/lang/Object;
.source "TextBookmarkActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;)V
    .locals 0

    .prologue
    .line 553
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 555
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 556
    .local v2, "pos":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 557
    .local v1, "message":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 558
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v3, "LIST_RIGHT_BTN"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 559
    const-string/jumbo v3, "BUTTON_POS"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 560
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 561
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 562
    return-void
.end method
