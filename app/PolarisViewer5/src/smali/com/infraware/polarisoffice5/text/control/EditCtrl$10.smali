.class Lcom/infraware/polarisoffice5/text/control/EditCtrl$10;
.super Ljava/lang/Object;
.source "EditCtrl.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setToastPopupEventListener(Lcom/infraware/polarisoffice5/text/control/ToastPopup;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field r:Landroid/graphics/Rect;

.field result:Z

.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

.field final synthetic val$toastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

.field final synthetic val$tvMore:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;Landroid/widget/ImageView;Lcom/infraware/polarisoffice5/text/control/ToastPopup;)V
    .locals 1

    .prologue
    .line 2380
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$10;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iput-object p2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$10;->val$tvMore:Landroid/widget/ImageView;

    iput-object p3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$10;->val$toastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2381
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$10;->r:Landroid/graphics/Rect;

    .line 2382
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$10;->result:Z

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "evt"    # Landroid/view/MotionEvent;

    .prologue
    const v6, 0x7f02000f

    const/4 v5, 0x0

    .line 2386
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    .line 2387
    .local v0, "lPadding":I
    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    .line 2389
    .local v1, "rPadding":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 2422
    :cond_0
    :goto_0
    const/4 v2, 0x1

    return v2

    .line 2391
    :pswitch_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$10;->val$tvMore:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 2392
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$10;->val$tvMore:Landroid/widget/ImageView;

    invoke-virtual {v2, v0, v5, v1, v5}, Landroid/widget/ImageView;->setPadding(IIII)V

    goto :goto_0

    .line 2396
    :pswitch_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$10;->val$tvMore:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 2398
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$10;->r:Landroid/graphics/Rect;

    invoke-virtual {p1, v2}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 2399
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$10;->r:Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$10;->result:Z

    .line 2401
    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$10;->result:Z

    if-eqz v2, :cond_0

    .line 2405
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$10;->val$toastPopup:Lcom/infraware/polarisoffice5/text/control/ToastPopup;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/ToastPopup;->dismiss()V

    .line 2407
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$10;->val$tvMore:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->playSoundEffect(I)V

    .line 2408
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$10;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->showMorePopup()V

    goto :goto_0

    .line 2411
    :pswitch_2
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$10;->r:Landroid/graphics/Rect;

    invoke-virtual {p1, v2}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 2412
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$10;->r:Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$10;->result:Z

    .line 2414
    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$10;->result:Z

    if-eqz v2, :cond_1

    .line 2415
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$10;->val$tvMore:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 2417
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$10;->val$tvMore:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_0

    .line 2389
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
