.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$MatchWholeKeyEvent;
.super Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;
.source "TextEditorActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MatchWholeKeyEvent"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;


# direct methods
.method private constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;)V
    .locals 1

    .prologue
    .line 5228
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$MatchWholeKeyEvent;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;
    .param p2, "x1"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$1;

    .prologue
    .line 5228
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$MatchWholeKeyEvent;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;)V

    return-void
.end method


# virtual methods
.method protected onKeyCode(II)Z
    .locals 5
    .param p1, "a_keyCode"    # I
    .param p2, "a_event"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 5233
    if-eq p2, v1, :cond_0

    .line 5246
    :goto_0
    return v1

    .line 5236
    :cond_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$MatchWholeKeyEvent;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mCbMatchWhole:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->access$8400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-ne v3, v1, :cond_1

    .line 5237
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$MatchWholeKeyEvent;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$MatchWholeKeyEvent;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mCbMatchWhole:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->access$8400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    # setter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachWholeWord:Z
    invoke-static {v1, v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$4302(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Z)Z

    .line 5238
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$MatchWholeKeyEvent;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mCbMatchWhole:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->access$8400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 5244
    :goto_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$MatchWholeKeyEvent;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v3, "audio"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 5245
    .local v0, "mgr":Landroid/media/AudioManager;
    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->playSoundEffect(I)V

    move v1, v2

    .line 5246
    goto :goto_0

    .line 5241
    .end local v0    # "mgr":Landroid/media/AudioManager;
    :cond_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$MatchWholeKeyEvent;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$MatchWholeKeyEvent;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mCbMatchWhole:Landroid/widget/CheckBox;
    invoke-static {v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->access$8400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;)Landroid/widget/CheckBox;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    # setter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bFind_MachWholeWord:Z
    invoke-static {v3, v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$4302(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Z)Z

    .line 5242
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$MatchWholeKeyEvent;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mCbMatchWhole:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->access$8400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1
.end method

.method protected onKeyDown()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 5256
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$MatchWholeKeyEvent;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getViewMode()I

    move-result v1

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$MatchWholeKeyEvent;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getViewMode()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 5259
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onKeyUp()Z
    .locals 1

    .prologue
    .line 5251
    const/4 v0, 0x0

    return v0
.end method
