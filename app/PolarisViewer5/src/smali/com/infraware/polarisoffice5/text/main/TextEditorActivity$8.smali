.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$8;
.super Ljava/lang/Object;
.source "TextEditorActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setEventListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
    .locals 0

    .prologue
    .line 1377
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3
    .param p1, "arg0"    # Landroid/text/Editable;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1391
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bEdtFindText:Z
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$900(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1392
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindPrevFind:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1393
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindPrevFind:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 1394
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindNextFind:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1100(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1395
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindNextFind:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1100(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 1402
    :goto_0
    return-void

    .line 1397
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindPrevFind:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1398
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindPrevFind:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 1399
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindNextFind:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1100(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1400
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindNextFind:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1100(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 1388
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "arg0"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 1381
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1382
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    const/4 v1, 0x1

    # setter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bEdtFindText:Z
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$902(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Z)Z

    .line 1386
    :goto_0
    return-void

    .line 1384
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    const/4 v1, 0x0

    # setter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bEdtFindText:Z
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$902(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Z)Z

    goto :goto_0
.end method
