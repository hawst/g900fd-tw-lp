.class public Lcom/infraware/polarisoffice5/text/manager/LineList;
.super Ljava/lang/Object;
.source "LineList.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Path;


# instance fields
.field private in:Ljava/io/BufferedReader;

.field private m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

.field private out:Ljava/io/BufferedWriter;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/LineList;->out:Ljava/io/BufferedWriter;

    .line 22
    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/LineList;->in:Ljava/io/BufferedReader;

    .line 23
    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/LineList;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .line 27
    :try_start_0
    new-instance v0, Ljava/io/BufferedWriter;

    new-instance v1, Ljava/io/FileWriter;

    sget-object v2, Lcom/infraware/polarisoffice5/text/manager/LineList;->PATH_LINELIST:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/LineList;->out:Ljava/io/BufferedWriter;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 31
    :goto_0
    return-void

    .line 29
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/LineList;->out:Ljava/io/BufferedWriter;

    .line 22
    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/LineList;->in:Ljava/io/BufferedReader;

    .line 23
    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/LineList;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .line 34
    check-cast p1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/manager/LineList;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .line 37
    :try_start_0
    new-instance v0, Ljava/io/BufferedWriter;

    new-instance v1, Ljava/io/FileWriter;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/LineList;->m_activity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    sget-object v2, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->PATH_LINELIST:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/LineList;->out:Ljava/io/BufferedWriter;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41
    :goto_0
    return-void

    .line 39
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public add(Ljava/lang/String;)V
    .locals 2
    .param p1, "line"    # Ljava/lang/String;

    .prologue
    .line 45
    :try_start_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/LineList;->out:Ljava/io/BufferedWriter;

    if-eqz v1, :cond_0

    .line 46
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/LineList;->out:Ljava/io/BufferedWriter;

    invoke-virtual {v1, p1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 47
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/LineList;->out:Ljava/io/BufferedWriter;

    invoke-virtual {v1}, Ljava/io/BufferedWriter;->newLine()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    :cond_0
    :goto_0
    return-void

    .line 49
    :catch_0
    move-exception v0

    .line 50
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public delete()V
    .locals 2

    .prologue
    .line 80
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/infraware/polarisoffice5/text/manager/LineList;->PATH_LINELIST:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 81
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 83
    :cond_0
    return-void
.end method

.method public finishAdd()V
    .locals 2

    .prologue
    .line 56
    :try_start_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/LineList;->out:Ljava/io/BufferedWriter;

    if-eqz v1, :cond_0

    .line 57
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/LineList;->out:Ljava/io/BufferedWriter;

    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V

    .line 58
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/LineList;->out:Ljava/io/BufferedWriter;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    :cond_0
    :goto_0
    return-void

    .line 60
    :catch_0
    move-exception v0

    .line 61
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public finishGet()V
    .locals 3

    .prologue
    .line 67
    :try_start_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/LineList;->in:Ljava/io/BufferedReader;

    if-eqz v2, :cond_0

    .line 68
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/LineList;->in:Ljava/io/BufferedReader;

    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 69
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/LineList;->in:Ljava/io/BufferedReader;

    .line 71
    :cond_0
    new-instance v1, Ljava/io/File;

    sget-object v2, Lcom/infraware/polarisoffice5/text/manager/LineList;->PATH_LINELIST:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 72
    .local v1, "printtemp":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 73
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    .end local v1    # "printtemp":Ljava/io/File;
    :cond_1
    :goto_0
    return-void

    .line 74
    :catch_0
    move-exception v0

    .line 75
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public get()Ljava/lang/String;
    .locals 5

    .prologue
    .line 86
    const/4 v1, 0x0

    .line 88
    .local v1, "result":Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/LineList;->in:Ljava/io/BufferedReader;

    if-nez v2, :cond_0

    .line 89
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    sget-object v4, Lcom/infraware/polarisoffice5/text/manager/LineList;->PATH_LINELIST:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/LineList;->in:Ljava/io/BufferedReader;

    .line 90
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/LineList;->in:Ljava/io/BufferedReader;

    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 95
    :goto_0
    return-object v1

    .line 91
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 100
    :try_start_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/LineList;->in:Ljava/io/BufferedReader;

    if-eqz v1, :cond_0

    .line 101
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/LineList;->in:Ljava/io/BufferedReader;

    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 102
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/LineList;->in:Ljava/io/BufferedReader;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    :cond_0
    :goto_0
    return-void

    .line 104
    :catch_0
    move-exception v0

    .line 105
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
