.class Lcom/infraware/polarisoffice5/text/manager/SecClipboard$IClipboardDataPasteEventImpl;
.super Ljava/lang/Object;
.source "SecClipboard.java"

# interfaces
.implements Landroid/sec/clipboard/IClipboardDataPasteEvent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/text/manager/SecClipboard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "IClipboardDataPasteEventImpl"
.end annotation


# instance fields
.field private final mBinder:Landroid/sec/clipboard/IClipboardDataPasteEvent$Stub;

.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/manager/SecClipboard;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/manager/SecClipboard;)V
    .locals 1

    .prologue
    .line 104
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    new-instance v0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard$IClipboardDataPasteEventImpl$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/manager/SecClipboard$IClipboardDataPasteEventImpl$1;-><init>(Lcom/infraware/polarisoffice5/text/manager/SecClipboard$IClipboardDataPasteEventImpl;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard$IClipboardDataPasteEventImpl;->mBinder:Landroid/sec/clipboard/IClipboardDataPasteEvent$Stub;

    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard$IClipboardDataPasteEventImpl;->mBinder:Landroid/sec/clipboard/IClipboardDataPasteEvent$Stub;

    return-object v0
.end method

.method public onClipboardDataPaste(Landroid/sec/clipboard/data/ClipboardData;)V
    .locals 10
    .param p1, "data"    # Landroid/sec/clipboard/data/ClipboardData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 126
    const/4 v5, 0x0

    .line 127
    .local v5, "textCS":Ljava/lang/CharSequence;
    const/4 v4, 0x0

    .line 128
    .local v4, "strText":Ljava/lang/String;
    const/4 v2, 0x0

    .line 130
    .local v2, "isText":Z
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->getNowProgress()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 176
    :goto_0
    return-void

    .line 134
    :cond_0
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->setNowProgress(Z)V

    .line 136
    invoke-virtual {p1}, Landroid/sec/clipboard/data/ClipboardData;->GetFomat()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    .line 164
    :cond_1
    :goto_1
    :pswitch_0
    if-eqz v2, :cond_3

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_3

    .line 165
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->messageHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->access$100(Lcom/infraware/polarisoffice5/text/manager/SecClipboard;)Landroid/os/Handler;

    move-result-object v8

    invoke-virtual {v8}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v3

    .line 166
    .local v3, "message":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 167
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v8, "data"

    invoke-virtual {v0, v8, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    invoke-virtual {v3, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 170
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->messageHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->access$100(Lcom/infraware/polarisoffice5/text/manager/SecClipboard;)Landroid/os/Handler;

    move-result-object v8

    invoke-virtual {v8, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 171
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

    # getter for: Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->m_clipboard:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->access$200(Lcom/infraware/polarisoffice5/text/manager/SecClipboard;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v8

    invoke-virtual {v8}, Landroid/sec/clipboard/ClipboardExManager;->dismissUIDataDialog()V

    goto :goto_0

    .line 140
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v3    # "message":Landroid/os/Message;
    :pswitch_1
    invoke-virtual {p1}, Landroid/sec/clipboard/data/ClipboardData;->GetFomat()I

    move-result v7

    .line 142
    .local v7, "type":I
    const/4 v8, 0x4

    if-ne v7, v8, :cond_2

    move-object v1, p1

    .line 143
    check-cast v1, Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;

    .line 144
    .local v1, "html":Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;
    invoke-virtual {v1}, Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;->getText()Ljava/lang/String;

    move-result-object v5

    .line 150
    .end local v1    # "html":Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;
    :goto_2
    if-eqz v5, :cond_1

    .line 152
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 153
    if-eqz v4, :cond_1

    .line 156
    const/4 v2, 0x1

    .line 158
    goto :goto_1

    :cond_2
    move-object v6, p1

    .line 146
    check-cast v6, Landroid/sec/clipboard/data/list/ClipboardDataText;

    .line 147
    .local v6, "txt":Landroid/sec/clipboard/data/list/ClipboardDataText;
    invoke-virtual {v6}, Landroid/sec/clipboard/data/list/ClipboardDataText;->GetText()Ljava/lang/CharSequence;

    move-result-object v5

    goto :goto_2

    .line 174
    .end local v6    # "txt":Landroid/sec/clipboard/data/list/ClipboardDataText;
    .end local v7    # "type":I
    :cond_3
    iget-object v8, p0, Lcom/infraware/polarisoffice5/text/manager/SecClipboard$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/polarisoffice5/text/manager/SecClipboard;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/infraware/polarisoffice5/text/manager/SecClipboard;->setNowProgress(Z)V

    goto :goto_0

    .line 136
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
