.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$6;
.super Ljava/lang/Object;
.source "TextEditorActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setEventListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
    .locals 0

    .prologue
    .line 1325
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$6;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1339
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$6;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bEdtReplaceFindText:Z
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$6;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bEdtReplaceText:Z
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1340
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$6;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplace:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1341
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$6;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceAll:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$700(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1342
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$6;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceNextFind:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$800(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1350
    :goto_0
    return-void

    .line 1343
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$6;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bEdtReplaceFindText:Z
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$6;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bEdtReplaceText:Z
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1344
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$6;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceNextFind:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$800(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_0

    .line 1346
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$6;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplace:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1347
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$6;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceAll:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$700(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1348
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$6;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceNextFind:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$800(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 1336
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 1328
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1329
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$6;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    const/4 v1, 0x1

    # setter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bEdtReplaceFindText:Z
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$402(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Z)Z

    .line 1333
    :goto_0
    return-void

    .line 1331
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$6;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    const/4 v1, 0x0

    # setter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bEdtReplaceFindText:Z
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$402(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Z)Z

    goto :goto_0
.end method
