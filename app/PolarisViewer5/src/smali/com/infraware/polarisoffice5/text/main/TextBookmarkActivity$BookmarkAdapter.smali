.class Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;
.super Landroid/widget/BaseAdapter;
.source "TextBookmarkActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BookmarkAdapter"
.end annotation


# instance fields
.field arSrcItem:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/common/MultiListItem;",
            ">;"
        }
    .end annotation
.end field

.field mHandler:Landroid/os/Handler;

.field mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;Landroid/content/Context;Ljava/util/ArrayList;Landroid/os/Handler;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p4, "messageHandler"    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/common/MultiListItem;",
            ">;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 505
    .local p3, "arItem":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/infraware/polarisoffice5/common/MultiListItem;>;"
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;->this$0:Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 506
    const-string/jumbo v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 507
    iput-object p3, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;->arSrcItem:Ljava/util/ArrayList;

    .line 508
    iput-object p4, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;->mHandler:Landroid/os/Handler;

    .line 509
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 513
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/infraware/polarisoffice5/common/MultiListItem;
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 518
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/MultiListItem;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 499
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;->getItem(I)Lcom/infraware/polarisoffice5/common/MultiListItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 523
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 528
    const v1, 0x7f030023

    .line 530
    .local v1, "res":I
    if-nez p2, :cond_0

    .line 531
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;->mInflater:Landroid/view/LayoutInflater;

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 535
    :cond_0
    const v3, 0x7f0b0104

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 536
    .local v2, "text":Landroid/widget/TextView;
    if-eqz v2, :cond_1

    .line 538
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/infraware/polarisoffice5/common/MultiListItem;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/common/MultiListItem;->getText1()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 541
    :cond_1
    const v3, 0x7f0b0105

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/ListImgRightButton;

    .line 542
    .local v0, "RightBtn":Lcom/infraware/polarisoffice5/common/ListImgRightButton;
    if-eqz v0, :cond_2

    .line 544
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/infraware/polarisoffice5/common/MultiListItem;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/common/MultiListItem;->isEnable()Z

    move-result v3

    if-ne v3, v6, :cond_3

    .line 545
    invoke-virtual {v0, v5}, Lcom/infraware/polarisoffice5/common/ListImgRightButton;->setVisibility(I)V

    .line 546
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/infraware/polarisoffice5/common/ListImgRightButton;->setTag(Ljava/lang/Object;)V

    .line 547
    invoke-virtual {v0, v6}, Lcom/infraware/polarisoffice5/common/ListImgRightButton;->setClickable(Z)V

    .line 548
    invoke-virtual {v0, v5}, Lcom/infraware/polarisoffice5/common/ListImgRightButton;->setFocusable(Z)V

    .line 549
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/infraware/polarisoffice5/common/MultiListItem;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/common/MultiListItem;->getIconNormalRes()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/infraware/polarisoffice5/common/ListImgRightButton;->setImageResource(I)V

    .line 551
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;->mHandler:Landroid/os/Handler;

    if-eqz v3, :cond_2

    .line 553
    new-instance v3, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter$1;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter$1;-><init>(Lcom/infraware/polarisoffice5/text/main/TextBookmarkActivity$BookmarkAdapter;)V

    invoke-virtual {v0, v3}, Lcom/infraware/polarisoffice5/common/ListImgRightButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 569
    :cond_2
    :goto_0
    return-object p2

    .line 566
    :cond_3
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Lcom/infraware/polarisoffice5/common/ListImgRightButton;->setVisibility(I)V

    goto :goto_0
.end method
