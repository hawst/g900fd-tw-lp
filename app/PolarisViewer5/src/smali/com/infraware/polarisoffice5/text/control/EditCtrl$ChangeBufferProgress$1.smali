.class Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress$1;
.super Ljava/lang/Object;
.source "EditCtrl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;)V
    .locals 0

    .prologue
    .line 3860
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 3863
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->pd:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->access$3100(Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 3865
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setLoadingProgress(Z)V

    .line 3867
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->insert:Z
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->access$3200(Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3868
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->s:Ljava/lang/CharSequence;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->access$3300(Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;)Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->start:I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->access$3400(Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;)I

    move-result v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->before:I
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->access$3500(Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;)I

    move-result v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->count:I
    invoke-static {v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->access$3600(Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->insertText(Ljava/lang/CharSequence;III)V

    .line 3871
    :goto_0
    return-void

    .line 3870
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->s:Ljava/lang/CharSequence;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->access$3300(Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;)Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->start:I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->access$3400(Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;)I

    move-result v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->before:I
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->access$3500(Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;)I

    move-result v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress$1;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->count:I
    invoke-static {v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;->access$3600(Lcom/infraware/polarisoffice5/text/control/EditCtrl$ChangeBufferProgress;)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->deleteText(Ljava/lang/CharSequence;III)V

    goto :goto_0
.end method
