.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$11;
.super Ljava/lang/Object;
.source "TextEditorActivity.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/text/manager/PrintManager$PrintManagerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->printListenerInitialize()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
    .locals 0

    .prologue
    .line 1574
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$11;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrintFinished(I)V
    .locals 4
    .param p1, "result"    # I

    .prologue
    const/4 v3, 0x1

    .line 1577
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$11;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # setter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_printresult:I
    invoke-static {v1, p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$2002(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;I)I

    .line 1579
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$11;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_printresult:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$2000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$11;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_bPrintstop:Z
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$2100(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1580
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$11;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_alertDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$2200(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 1581
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.sec.android.app.mobileprint.PRINT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1582
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "android.intent.extra.STREAM"

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$11;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_printmanager:Lcom/infraware/polarisoffice5/text/manager/PrintManager;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$2300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/manager/PrintManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->getStoragePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1583
    const-string/jumbo v1, "android.intent.extra.TITLE"

    const-string/jumbo v2, "Polaris Office 5"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1585
    const-string/jumbo v1, "android.intent.extra.SUBJECT"

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$11;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFile:Ljava/lang/String;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$2400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1586
    const-string/jumbo v1, "android.intent.extra.shortcut.NAME"

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$11;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_strFilePath:Ljava/lang/String;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$100(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1587
    const-string/jumbo v1, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1588
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1589
    const-string/jumbo v1, "large_document"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1591
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$11;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v1, v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->startActivity(Landroid/content/Intent;)V

    .line 1597
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 1594
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$11;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    const/4 v2, 0x0

    # setter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_printmanager:Lcom/infraware/polarisoffice5/text/manager/PrintManager;
    invoke-static {v1, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$2302(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Lcom/infraware/polarisoffice5/text/manager/PrintManager;)Lcom/infraware/polarisoffice5/text/manager/PrintManager;

    .line 1595
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$11;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_alertDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$2200(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    goto :goto_0
.end method
