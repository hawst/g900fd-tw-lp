.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1$1;
.super Ljava/lang/Object;
.source "TextEditorActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1;)V
    .locals 0

    .prologue
    .line 2379
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1$1;->this$2:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2383
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1$1;->this$2:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getLayout()Landroid/text/Layout;

    move-result-object v1

    .line 2384
    .local v1, "layout":Landroid/text/Layout;
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1$1;->this$2:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->caretposition:[I

    aget v3, v3, v7

    invoke-virtual {v1, v3}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v0

    .line 2385
    .local v0, "caretline":I
    invoke-virtual {v1, v0}, Landroid/text/Layout;->getLineTop(I)I

    move-result v2

    .line 2387
    .local v2, "line":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1$1;->this$2:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1$1;->this$2:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1;

    iget-object v4, v4, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;

    iget-object v4, v4, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->caretposition:[I

    aget v4, v4, v7

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1$1;->this$2:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1;

    iget-object v5, v5, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;

    iget-object v5, v5, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->caretposition:[I

    const/4 v6, 0x1

    aget v5, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(II)V

    .line 2389
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1$1;->this$2:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1$1;->this$2:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1;

    iget-object v4, v4, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret$1;->this$1:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;

    iget-object v4, v4, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$OffsetToCaret;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x3

    sub-int v4, v2, v4

    invoke-virtual {v3, v7, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->scrollTo(II)V

    .line 2392
    return-void
.end method
