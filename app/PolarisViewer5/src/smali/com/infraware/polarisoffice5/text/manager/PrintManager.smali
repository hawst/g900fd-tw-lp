.class public Lcom/infraware/polarisoffice5/text/manager/PrintManager;
.super Ljava/lang/Object;
.source "PrintManager.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Flag;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Path;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Size;
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Type;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/text/manager/PrintManager$PrintManagerListener;
    }
.end annotation


# instance fields
.field private m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

.field private m_bIsSupportDPI:Z

.field private m_bitmapWidth:I

.field private m_blockCnt:I

.field private m_blockText:Ljava/lang/StringBuffer;

.field private m_currentBlockCnt:I

.field private m_dpiInfo:I

.field private m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

.field private m_listener:Lcom/infraware/polarisoffice5/text/manager/PrintManager$PrintManagerListener;

.field private m_marginDefault:F

.field private m_multiinfo:F

.field private m_oneLineWidth:F


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;I)V
    .locals 4
    .param p1, "edit"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    .param p2, "dpi"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .line 30
    const v0, 0x42a1f5c3    # 80.98f

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_marginDefault:F

    .line 39
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_bIsSupportDPI:Z

    .line 41
    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_bitmapWidth:I

    .line 42
    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_dpiInfo:I

    .line 43
    iput v3, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockCnt:I

    .line 44
    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_currentBlockCnt:I

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_oneLineWidth:F

    .line 47
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_multiinfo:F

    .line 49
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    .line 51
    iput-object v2, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_listener:Lcom/infraware/polarisoffice5/text/manager/PrintManager$PrintManagerListener;

    .line 54
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .line 55
    iput p2, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_dpiInfo:I

    .line 56
    return-void
.end method

.method private cut()V
    .locals 19

    .prologue
    .line 173
    new-instance v9, Landroid/graphics/Paint;

    invoke-direct {v9}, Landroid/graphics/Paint;-><init>()V

    .line 174
    .local v9, "measurePaint":Landroid/graphics/Paint;
    const/4 v6, 0x0

    .line 176
    .local v6, "isLast":Z
    move-object/from16 v0, p0

    iget v8, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_bitmapWidth:I

    .line 177
    .local v8, "maxWidth":I
    const/4 v12, 0x0

    .line 179
    .local v12, "startPosition":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuffer;->length()I

    move-result v17

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v9, v15}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_multiinfo:F

    move/from16 v16, v0

    mul-float v15, v15, v16

    int-to-float v0, v8

    move/from16 v16, v0

    cmpg-float v15, v15, v16

    if-gez v15, :cond_1

    .line 180
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuffer;->length()I

    move-result v17

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 181
    .local v13, "temp":Ljava/lang/String;
    const-string/jumbo v15, "\n"

    invoke-virtual {v13, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 183
    .local v11, "splitString":[Ljava/lang/String;
    move-object v1, v11

    .local v1, "arr$":[Ljava/lang/String;
    array-length v7, v1

    .local v7, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v7, :cond_0

    aget-object v10, v1, v5

    .line 184
    .local v10, "oTemp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v15, v10}, Lcom/infraware/polarisoffice5/text/manager/LineList;->add(Ljava/lang/String;)V

    .line 183
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 185
    .end local v10    # "oTemp":Ljava/lang/String;
    :cond_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v15}, Lcom/infraware/polarisoffice5/text/manager/LineList;->finishAdd()V

    .line 280
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v7    # "len$":I
    .end local v11    # "splitString":[Ljava/lang/String;
    .end local v13    # "temp":Ljava/lang/String;
    :goto_1
    return-void

    .line 187
    :cond_1
    const/4 v4, 0x0

    .line 188
    .local v4, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v15}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v15

    invoke-virtual {v15}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getPrintStopFlag()Z

    move-result v15

    if-eqz v15, :cond_2

    .line 189
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v15}, Lcom/infraware/polarisoffice5/text/manager/LineList;->finishAdd()V

    goto :goto_1

    .line 193
    :cond_2
    :try_start_0
    move-object/from16 v0, p0

    iget v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_oneLineWidth:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    move-object/from16 v16, v0

    add-int v17, v12, v4

    add-int/lit8 v17, v17, -0x1

    add-int v18, v12, v4

    invoke-virtual/range {v16 .. v18}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_multiinfo:F

    move/from16 v17, v0

    mul-float v16, v16, v17

    add-float v15, v15, v16

    move-object/from16 v0, p0

    iput v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_oneLineWidth:F

    .line 195
    move-object/from16 v0, p0

    iget v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_oneLineWidth:F

    int-to-float v0, v8

    move/from16 v16, v0

    cmpl-float v15, v15, v16

    if-gtz v15, :cond_3

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    add-int v16, v12, v4

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    const/16 v16, 0xa

    move/from16 v0, v16

    if-ne v15, v0, :cond_5

    .line 196
    :cond_3
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_oneLineWidth:F

    .line 197
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    add-int v16, v12, v4

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    const/16 v16, 0xa

    move/from16 v0, v16

    if-ne v15, v0, :cond_6

    .line 198
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    add-int v16, v12, v4

    move/from16 v0, v16

    invoke-virtual {v15, v12, v0}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 200
    .restart local v13    # "temp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v15, v13}, Lcom/infraware/polarisoffice5/text/manager/LineList;->add(Ljava/lang/String;)V

    .line 202
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    add-int v16, v12, v4

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    const/16 v16, 0xa

    move/from16 v0, v16

    if-ne v15, v0, :cond_4

    .line 203
    add-int/lit8 v12, v12, 0x1

    .line 204
    :cond_4
    add-int/2addr v12, v4

    .line 205
    const/4 v4, 0x0

    .line 187
    .end local v13    # "temp":Ljava/lang/String;
    :cond_5
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    .line 206
    :cond_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    add-int v16, v12, v4

    add-int/lit8 v16, v16, -0x2

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    const/16 v16, 0x20

    move/from16 v0, v16

    if-eq v15, v0, :cond_a

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    add-int v16, v12, v4

    add-int/lit8 v16, v16, -0x1

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    const/16 v16, 0x20

    move/from16 v0, v16

    if-eq v15, v0, :cond_a

    .line 209
    add-int v15, v12, v4

    add-int/lit8 v2, v15, -0x2

    .line 211
    .local v2, "breakPosition":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    add-int/lit8 v16, v2, -0x1

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    const/16 v16, 0x20

    move/from16 v0, v16

    if-ne v15, v0, :cond_7

    .line 212
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    invoke-virtual {v15, v12, v2}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 213
    .restart local v13    # "temp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v15, v13}, Lcom/infraware/polarisoffice5/text/manager/LineList;->add(Ljava/lang/String;)V

    .line 214
    move v12, v2

    .line 215
    const/4 v4, 0x0

    .line 216
    goto :goto_3

    .line 217
    .end local v13    # "temp":Ljava/lang/String;
    :cond_7
    const/4 v14, 0x2

    .line 219
    .local v14, "z":I
    :goto_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    sub-int v16, v2, v14

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    const/16 v16, 0x20

    move/from16 v0, v16

    if-ne v15, v0, :cond_8

    .line 220
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    sub-int v16, v2, v14

    add-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v15, v12, v0}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 221
    .restart local v13    # "temp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v15, v13}, Lcom/infraware/polarisoffice5/text/manager/LineList;->add(Ljava/lang/String;)V

    .line 222
    sub-int v15, v2, v14

    add-int/lit8 v12, v15, 0x1

    .line 223
    const/4 v4, 0x0

    .line 224
    goto :goto_3

    .line 225
    .end local v13    # "temp":Ljava/lang/String;
    :cond_8
    const/16 v15, 0xa

    if-le v14, v15, :cond_9

    .line 226
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    add-int/lit8 v16, v2, 0x1

    move/from16 v0, v16

    invoke-virtual {v15, v12, v0}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 227
    .restart local v13    # "temp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v15, v13}, Lcom/infraware/polarisoffice5/text/manager/LineList;->add(Ljava/lang/String;)V

    .line 228
    add-int/lit8 v12, v2, 0x1

    .line 229
    const/4 v4, 0x0

    .line 230
    goto/16 :goto_3

    .line 232
    .end local v13    # "temp":Ljava/lang/String;
    :cond_9
    add-int/lit8 v14, v14, 0x1

    goto :goto_4

    .line 237
    .end local v2    # "breakPosition":I
    .end local v14    # "z":I
    :cond_a
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    add-int v16, v12, v4

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, v16

    invoke-virtual {v15, v12, v0}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 238
    .restart local v13    # "temp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v15, v13}, Lcom/infraware/polarisoffice5/text/manager/LineList;->add(Ljava/lang/String;)V

    .line 239
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    add-int v16, v12, v4

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->charAt(I)C
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v15

    const/16 v16, 0xa

    move/from16 v0, v16

    if-ne v15, v0, :cond_b

    .line 240
    add-int/lit8 v12, v12, 0x1

    .line 241
    :cond_b
    add-int/lit8 v15, v4, -0x1

    add-int/2addr v12, v15

    .line 242
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 245
    .end local v13    # "temp":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 246
    .local v3, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_currentBlockCnt:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockCnt:I

    move/from16 v16, v0

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, v16

    if-ge v15, v0, :cond_c

    .line 249
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    invoke-virtual {v15, v12}, Ljava/lang/StringBuffer;->substring(I)Ljava/lang/String;

    move-result-object v13

    .line 250
    .restart local v13    # "temp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_currentBlockCnt:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_currentBlockCnt:I

    .line 251
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 252
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    invoke-virtual {v15, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 253
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_currentBlockCnt:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 255
    const/4 v4, 0x0

    .line 256
    const/4 v12, 0x0

    .line 257
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_oneLineWidth:F

    goto/16 :goto_3

    .line 260
    .end local v13    # "temp":Ljava/lang/String;
    :cond_c
    move-object/from16 v0, p0

    iget v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_currentBlockCnt:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockCnt:I

    move/from16 v16, v0

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_d

    if-nez v6, :cond_d

    .line 261
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    invoke-virtual {v15, v12}, Ljava/lang/StringBuffer;->substring(I)Ljava/lang/String;

    move-result-object v13

    .line 262
    .restart local v13    # "temp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 263
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    invoke-virtual {v15, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 265
    const/4 v4, 0x0

    .line 266
    const/4 v12, 0x0

    .line 267
    const/4 v6, 0x1

    .line 268
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_oneLineWidth:F

    goto/16 :goto_3

    .line 271
    .end local v13    # "temp":Ljava/lang/String;
    :cond_d
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    invoke-virtual {v15, v12}, Ljava/lang/StringBuffer;->substring(I)Ljava/lang/String;

    move-result-object v13

    .line 272
    .restart local v13    # "temp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v15, v13}, Lcom/infraware/polarisoffice5/text/manager/LineList;->add(Ljava/lang/String;)V

    .line 273
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v15}, Lcom/infraware/polarisoffice5/text/manager/LineList;->finishAdd()V

    .line 274
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_oneLineWidth:F

    goto/16 :goto_1
.end method

.method private draw()I
    .locals 13

    .prologue
    const/4 v9, -0x1

    .line 283
    const/4 v8, 0x0

    .line 286
    .local v8, "size":I
    :goto_0
    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/manager/LineList;->get()Ljava/lang/String;

    move-result-object v6

    .line 287
    .local v6, "result":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 288
    add-int/lit8 v8, v8, 0x1

    .line 291
    goto :goto_0

    .line 293
    :cond_0
    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/manager/LineList;->reset()V

    .line 295
    const/4 v6, -0x1

    .line 296
    .local v6, "result":I
    move v5, v8

    .line 298
    .local v5, "maxLine":I
    invoke-static {}, Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;->getInterface()Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->PATH_PRINTSTORAGE:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "/printPage"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    iget v12, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_dpiInfo:I

    invoke-virtual {v10, v11, v12, v5}, Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;->ISetInitializeInfo(Ljava/lang/String;II)Z

    move-result v4

    .line 300
    .local v4, "make":Z
    if-eqz v4, :cond_1

    .line 301
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v5, :cond_1

    .line 302
    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v10

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getPrintStopFlag()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 303
    invoke-static {}, Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;->getInterface()Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;

    move-result-object v10

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;->IStopPrint()Z

    .line 335
    .end local v2    # "i":I
    :cond_1
    :goto_2
    return v9

    .line 306
    .restart local v2    # "i":I
    :cond_2
    iget-object v10, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v10}, Lcom/infraware/polarisoffice5/text/manager/LineList;->get()Ljava/lang/String;

    move-result-object v7

    .line 308
    .local v7, "resultStr":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 310
    .local v0, "charArray":[C
    const/4 v1, 0x0

    .line 312
    .local v1, "hasTabChar":Z
    const/4 v3, 0x0

    .local v3, "i2":I
    :goto_3
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v10

    if-ge v3, v10, :cond_4

    .line 314
    aget-char v10, v0, v3

    const/16 v11, 0x9

    if-ne v10, v11, :cond_3

    .line 316
    const/4 v1, 0x1

    .line 317
    const/16 v10, 0x20

    aput-char v10, v0, v3

    .line 312
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 321
    :cond_4
    if-eqz v1, :cond_5

    .line 323
    new-instance v7, Ljava/lang/String;

    .end local v7    # "resultStr":Ljava/lang/String;
    invoke-direct {v7, v0}, Ljava/lang/String;-><init>([C)V

    .line 326
    .restart local v7    # "resultStr":Ljava/lang/String;
    :cond_5
    if-eqz v7, :cond_6

    .line 327
    invoke-static {}, Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;->getInterface()Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;

    move-result-object v10

    invoke-virtual {v10, v7}, Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;->IMakeImage(Ljava/lang/String;)I

    move-result v6

    .line 328
    :cond_6
    if-eq v6, v9, :cond_7

    .line 329
    add-int/lit8 v9, v6, -0x1

    goto :goto_2

    .line 301
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static folderInitialize()V
    .locals 6

    .prologue
    .line 74
    new-instance v0, Ljava/io/File;

    sget-object v4, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->PATH_PRINTSTORAGE:Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 75
    .local v0, "dirPath":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 76
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    .line 78
    .local v1, "filelist":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, v1

    if-ge v2, v4, :cond_1

    .line 79
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->PATH_PRINTSTORAGE:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v1, v2

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 80
    .local v3, "temp":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 78
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 83
    .end local v1    # "filelist":[Ljava/lang/String;
    .end local v2    # "i":I
    .end local v3    # "temp":Ljava/io/File;
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 91
    :cond_1
    return-void
.end method

.method private initializeVariable()V
    .locals 8

    .prologue
    const v7, 0x408570a4    # 4.17f

    const v6, 0x4031eb85    # 2.78f

    const v5, 0x40051eb8    # 2.08f

    const v4, 0x3fb1eb85    # 1.39f

    const/high16 v3, 0x40000000    # 2.0f

    .line 103
    const v0, 0x42a1f5c3    # 80.98f

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_marginDefault:F

    .line 107
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_currentBlockCnt:I

    .line 108
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    .line 109
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockText:Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget v2, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_currentBlockCnt:I

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockText(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 111
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCount()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_blockCnt:I

    .line 113
    new-instance v0, Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-direct {v0}, Lcom/infraware/polarisoffice5/text/manager/LineList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    .line 115
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_dpiInfo:I

    packed-switch v0, :pswitch_data_0

    .line 140
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_bIsSupportDPI:Z

    .line 143
    :goto_0
    return-void

    .line 117
    :pswitch_0
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_marginDefault:F

    mul-float/2addr v0, v3

    float-to-int v0, v0

    rsub-int v0, v0, 0x253

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_bitmapWidth:I

    goto :goto_0

    .line 120
    :pswitch_1
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_marginDefault:F

    mul-float/2addr v0, v4

    mul-float/2addr v0, v3

    float-to-int v0, v0

    rsub-int v0, v0, 0x33a

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_bitmapWidth:I

    .line 121
    iput v4, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_multiinfo:F

    goto :goto_0

    .line 124
    :pswitch_2
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_marginDefault:F

    mul-float/2addr v0, v5

    mul-float/2addr v0, v3

    float-to-int v0, v0

    rsub-int v0, v0, 0x4d8

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_bitmapWidth:I

    .line 125
    iput v5, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_multiinfo:F

    goto :goto_0

    .line 128
    :pswitch_3
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_marginDefault:F

    mul-float/2addr v0, v6

    mul-float/2addr v0, v3

    float-to-int v0, v0

    rsub-int v0, v0, 0x675

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_bitmapWidth:I

    .line 129
    iput v6, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_multiinfo:F

    goto :goto_0

    .line 132
    :pswitch_4
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_marginDefault:F

    mul-float/2addr v0, v7

    mul-float/2addr v0, v3

    float-to-int v0, v0

    rsub-int v0, v0, 0x9b0

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_bitmapWidth:I

    .line 133
    iput v7, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_multiinfo:F

    goto :goto_0

    .line 136
    :pswitch_5
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_marginDefault:F

    const v1, 0x410547ae    # 8.33f

    mul-float/2addr v0, v1

    mul-float/2addr v0, v3

    float-to-int v0, v0

    rsub-int v0, v0, 0x1361

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_bitmapWidth:I

    .line 137
    const v0, 0x410547ae    # 8.33f

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_multiinfo:F

    goto :goto_0

    .line 115
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private printProcess()I
    .locals 2

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->cut()V

    .line 161
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->draw()I

    move-result v0

    .line 164
    .local v0, "result":I
    :try_start_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_linelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/manager/LineList;->finishGet()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    :goto_0
    return v0

    .line 165
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public getStoragePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 339
    sget-object v0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->PATH_PRINTSTORAGE:Ljava/lang/String;

    .line 342
    .local v0, "result":Ljava/lang/String;
    return-object v0
.end method

.method public isListenerRegistered()Z
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_listener:Lcom/infraware/polarisoffice5/text/manager/PrintManager$PrintManagerListener;

    if-nez v0, :cond_0

    .line 68
    const/4 v0, 0x0

    .line 70
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public print()V
    .locals 3

    .prologue
    .line 146
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->initializeVariable()V

    .line 148
    invoke-static {}, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->folderInitialize()V

    .line 149
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_bIsSupportDPI:Z

    if-eqz v1, :cond_1

    .line 150
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->printProcess()I

    move-result v0

    .line 151
    .local v0, "result":I
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_listener:Lcom/infraware/polarisoffice5/text/manager/PrintManager$PrintManagerListener;

    if-eqz v1, :cond_0

    .line 152
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_listener:Lcom/infraware/polarisoffice5/text/manager/PrintManager$PrintManagerListener;

    invoke-interface {v1, v0}, Lcom/infraware/polarisoffice5/text/manager/PrintManager$PrintManagerListener;->onPrintFinished(I)V

    .line 157
    .end local v0    # "result":I
    :cond_0
    :goto_0
    return-void

    .line 154
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_listener:Lcom/infraware/polarisoffice5/text/manager/PrintManager$PrintManagerListener;

    if-eqz v1, :cond_0

    .line 155
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_listener:Lcom/infraware/polarisoffice5/text/manager/PrintManager$PrintManagerListener;

    const/4 v2, -0x1

    invoke-interface {v1, v2}, Lcom/infraware/polarisoffice5/text/manager/PrintManager$PrintManagerListener;->onPrintFinished(I)V

    goto :goto_0
.end method

.method public setPrintManagerListener(Lcom/infraware/polarisoffice5/text/manager/PrintManager$PrintManagerListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/infraware/polarisoffice5/text/manager/PrintManager$PrintManagerListener;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/manager/PrintManager;->m_listener:Lcom/infraware/polarisoffice5/text/manager/PrintManager$PrintManagerListener;

    .line 64
    return-void
.end method
