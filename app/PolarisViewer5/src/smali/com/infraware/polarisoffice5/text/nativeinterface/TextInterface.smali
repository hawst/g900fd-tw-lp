.class public abstract Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;
.super Ljava/lang/Object;
.source "TextInterface.java"


# static fields
.field protected static mInterface:Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;


# instance fields
.field protected Native:Lcom/infraware/polarisoffice5/text/nativeinterface/TextNative;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    new-instance v0, Lcom/infraware/polarisoffice5/text/nativeinterface/TextNative;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/nativeinterface/TextNative;-><init>(Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;->Native:Lcom/infraware/polarisoffice5/text/nativeinterface/TextNative;

    .line 9
    return-void
.end method

.method public static getInterface()Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;->mInterface:Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;

    if-nez v0, :cond_0

    .line 13
    new-instance v0, Lcom/infraware/polarisoffice5/text/nativeinterface/TextCompInterfaceMsg;

    invoke-direct {v0}, Lcom/infraware/polarisoffice5/text/nativeinterface/TextCompInterfaceMsg;-><init>()V

    sput-object v0, Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;->mInterface:Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;

    .line 16
    :cond_0
    sget-object v0, Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;->mInterface:Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;

    return-object v0
.end method


# virtual methods
.method public IMakeImage(Ljava/lang/String;)I
    .locals 1
    .param p1, "line"    # Ljava/lang/String;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;->Native:Lcom/infraware/polarisoffice5/text/nativeinterface/TextNative;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/text/nativeinterface/TextNative;->IMakeImage(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public ISetInitializeInfo(Ljava/lang/String;II)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "dipInfo"    # I
    .param p3, "maxline"    # I

    .prologue
    .line 20
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;->Native:Lcom/infraware/polarisoffice5/text/nativeinterface/TextNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/polarisoffice5/text/nativeinterface/TextNative;->ISetInitializeInfo(Ljava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method public IStopPrint()Z
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/nativeinterface/TextInterface;->Native:Lcom/infraware/polarisoffice5/text/nativeinterface/TextNative;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/nativeinterface/TextNative;->IStopPrint()Z

    move-result v0

    return v0
.end method
