.class Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;
.super Ljava/lang/Object;
.source "EditCtrl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;)V
    .locals 0

    .prologue
    .line 3658
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 3662
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$1800(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)I

    move-result v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$1700(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCount()I

    move-result v3

    if-lt v2, v3, :cond_0

    .line 3663
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # setter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I
    invoke-static {v2, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$1802(Lcom/infraware/polarisoffice5/text/control/EditCtrl;I)I

    .line 3664
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    # setter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->nCurCaretPos:I
    invoke-static {v2, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->access$1902(Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;I)I

    .line 3667
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$1700(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$1800(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->loadBuffer(I)V

    .line 3669
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$1700(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$1800(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockText(I)Ljava/lang/String;

    move-result-object v1

    .line 3670
    .local v1, "strBlockText":Ljava/lang/String;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->nCurCaretPos:I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->access$1900(Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;)I

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$1800(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)I

    move-result v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_BufferManager:Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$1700(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/text/manager/TextBufferManager;->getBlockCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_3

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3671
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    const/4 v3, 0x1

    # setter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableInsert:Z
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$2002(Lcom/infraware/polarisoffice5/text/control/EditCtrl;Z)Z

    .line 3675
    :goto_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setText(Ljava/lang/CharSequence;)V

    .line 3676
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->nCurCaretPos:I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->access$1900(Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;)I

    move-result v2

    if-nez v2, :cond_1

    .line 3677
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    .line 3679
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->nCurCaretPos:I
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->access$1900(Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3683
    :goto_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_nCurBlock:I
    invoke-static {v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$1800(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->addBoundBlockText(I)V

    .line 3684
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # invokes: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setCurBlockBound()V
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$2100(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)V

    .line 3687
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->stopFling()V

    .line 3689
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$300(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setTitleText()V

    .line 3691
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setChanged(Z)V

    .line 3693
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$300(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->returnLastDisplayPosition()V

    .line 3697
    :try_start_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_pd:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$1500(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/app/ProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_pd:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$1500(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3698
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_pd:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$1500(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 3703
    :cond_2
    :goto_2
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$2200(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3$1;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3$1;-><init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 3720
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setLoadingProgress(Z)V

    .line 3723
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_ActParent:Landroid/app/Activity;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$300(Lcom/infraware/polarisoffice5/text/control/EditCtrl;)Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onCloseOpenLoadingProgress()V

    .line 3725
    return-void

    .line 3673
    :cond_3
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    # setter for: Lcom/infraware/polarisoffice5/text/control/EditCtrl;->m_bEnableInsert:Z
    invoke-static {v2, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->access$2002(Lcom/infraware/polarisoffice5/text/control/EditCtrl;Z)Z

    goto/16 :goto_0

    .line 3680
    :catch_0
    move-exception v0

    .line 3681
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress$3;->this$1:Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/text/control/EditCtrl$OpenProgress;->this$0:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v2, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setSelection(I)V

    goto/16 :goto_1

    .line 3699
    .end local v0    # "e":Ljava/lang/IndexOutOfBoundsException;
    :catch_1
    move-exception v0

    .line 3700
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2
.end method
