.class public Lcom/infraware/polarisoffice5/text/manager/UndoCommand;
.super Ljava/lang/Object;
.source "UndoCommand.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/text/manager/TEConstant$Size;


# instance fields
.field private m_EditCtrl:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

.field private m_bInsert:Z

.field private m_nBlock:I

.field private m_nLength:I

.field private m_nNum:I

.field private m_nStart:I

.field private m_strText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/text/control/EditCtrl;ILjava/lang/String;IIZI)V
    .locals 2
    .param p1, "editCtrl"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    .param p2, "nBlock"    # I
    .param p3, "strText"    # Ljava/lang/String;
    .param p4, "nStart"    # I
    .param p5, "nLength"    # I
    .param p6, "bInsert"    # Z
    .param p7, "nNum"    # I

    .prologue
    const/4 v1, 0x0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_strText:Ljava/lang/String;

    .line 9
    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_nStart:I

    .line 10
    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_nLength:I

    .line 11
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_bInsert:Z

    .line 12
    iput v1, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_nNum:I

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_EditCtrl:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .line 17
    iput p2, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_nBlock:I

    .line 18
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_EditCtrl:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .line 19
    iput-object p3, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_strText:Ljava/lang/String;

    .line 20
    iput p4, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_nStart:I

    .line 21
    iput p5, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_nLength:I

    .line 22
    iput-boolean p6, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_bInsert:Z

    .line 23
    iput p7, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_nNum:I

    .line 24
    return-void
.end method


# virtual methods
.method public execute()I
    .locals 6

    .prologue
    const/16 v5, 0xbb8

    .line 35
    const/4 v1, -0x1

    .line 36
    .local v1, "nPos":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_EditCtrl:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget v4, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_nStart:I

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockOffsetFromOffset(I)I

    move-result v2

    .line 39
    .local v2, "nStart":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_EditCtrl:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget v4, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_nBlock:I

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockBufferText(I)Ljava/lang/String;

    .line 41
    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_bInsert:Z

    if-eqz v3, :cond_0

    .line 42
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_EditCtrl:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget v4, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_nBlock:I

    iget-object v5, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_strText:Ljava/lang/String;

    invoke-virtual {v3, v4, v2, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->insertSubBuffer(IILjava/lang/String;)V

    .line 43
    iget v3, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_nLength:I

    add-int v1, v2, v3

    .line 57
    :goto_0
    return v1

    .line 45
    :cond_0
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_nLength:I

    .line 46
    .local v0, "nLength":I
    :goto_1
    add-int v3, v2, v0

    if-le v3, v5, :cond_1

    .line 47
    rsub-int v3, v2, 0xbb8

    sub-int/2addr v0, v3

    .line 48
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_EditCtrl:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget v4, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_nBlock:I

    invoke-virtual {v3, v4, v2, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->deleteSubBuffer(III)V

    .line 49
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_EditCtrl:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget v4, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_nBlock:I

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockBufferText(I)Ljava/lang/String;

    goto :goto_1

    .line 52
    :cond_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_EditCtrl:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget v4, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_nBlock:I

    add-int v5, v2, v0

    invoke-virtual {v3, v4, v2, v5}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->deleteSubBuffer(III)V

    .line 54
    move v1, v2

    goto :goto_0
.end method

.method public getBlock()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_nBlock:I

    return v0
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_nLength:I

    return v0
.end method

.method public getNum()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_nNum:I

    return v0
.end method

.method public getStart()I
    .locals 3

    .prologue
    .line 70
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_EditCtrl:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget v2, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_nStart:I

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockOffsetFromOffset(I)I

    move-result v0

    .line 71
    .local v0, "nStart":I
    return v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_strText:Ljava/lang/String;

    return-object v0
.end method

.method public isInsert()Z
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_bInsert:Z

    return v0
.end method

.method public setInsert(Z)V
    .locals 0
    .param p1, "bInsert"    # Z

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_bInsert:Z

    .line 28
    return-void
.end method

.method public setLength(I)V
    .locals 0
    .param p1, "nLength"    # I

    .prologue
    .line 83
    iput p1, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_nLength:I

    .line 84
    return-void
.end method

.method public setStart(I)V
    .locals 2
    .param p1, "nStart"    # I

    .prologue
    .line 75
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_EditCtrl:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    iget v1, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_nBlock:I

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockOffsetInFile(I)I

    move-result v0

    add-int/2addr v0, p1

    iput v0, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_nStart:I

    .line 76
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 0
    .param p1, "strText"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/manager/UndoCommand;->m_strText:Ljava/lang/String;

    .line 67
    return-void
.end method
