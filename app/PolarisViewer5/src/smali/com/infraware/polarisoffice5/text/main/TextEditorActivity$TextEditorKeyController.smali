.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;
.super Ljava/lang/Object;
.source "TextEditorActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TextEditorKeyController"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$MatchWholeKeyEvent;,
        Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$MatchCaseKeyEvent;,
        Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$ReplaceAllKeyEventable;,
        Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$ReplaceEditKeyEvent;,
        Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$ReplaceKeyEventable;,
        Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$ReplaceNextKeyEventable;,
        Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$ReplaceFindEditKeyEvent;,
        Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$ReplaceOptionKeyEvent;,
        Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$FindNextKeyEvent;,
        Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$FindEditKeyEvent;,
        Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$FindOptionKeyEvent;,
        Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$FindKeyEvent;,
        Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;
    }
.end annotation


# instance fields
.field private final LEGACY_KEY_CTRL:I

.field private final METAKEY_CTRL_ON:I

.field private mCbMatchCase:Landroid/widget/CheckBox;

.field private mCbMatchWhole:Landroid/widget/CheckBox;

.field private mKeyEventable:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;

.field private onKeyListener:Landroid/view/View$OnKeyListener;

.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
    .locals 2

    .prologue
    .line 4916
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4909
    const/4 v0, 0x2

    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->LEGACY_KEY_CTRL:I

    .line 4910
    const/16 v0, 0x1000

    iput v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->METAKEY_CTRL_ON:I

    .line 5263
    new-instance v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$1;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->onKeyListener:Landroid/view/View$OnKeyListener;

    .line 4918
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFormatIcon:Landroid/widget/ImageView;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$6400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 4919
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnUndo:Landroid/widget/ImageView;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$6500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 4920
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnRedo:Landroid/widget/ImageView;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$6600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 4921
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFind:Landroid/widget/ImageView;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$6700(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 4922
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnProperties:Landroid/widget/ImageView;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$6800(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 4923
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnMenu:Landroid/widget/ImageView;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$6900(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 4926
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindOption:Landroid/widget/ImageView;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$7000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 4927
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtFind:Landroid/widget/EditText;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$2600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 4928
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindNextFind:Landroid/widget/ImageView;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1100(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 4931
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceOption:Landroid/widget/ImageView;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$7100(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 4932
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceNextFind:Landroid/widget/ImageView;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$800(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 4933
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplaceFind:Landroid/widget/EditText;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$4500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 4934
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplace:Landroid/widget/ImageView;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 4935
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_edtReplace:Landroid/widget/EditText;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$5000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 4936
    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnReplaceAll:Landroid/widget/ImageView;
    invoke-static {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$700(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 4937
    return-void
.end method

.method static synthetic access$8300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    .prologue
    .line 4907
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mCbMatchCase:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$8400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    .prologue
    .line 4907
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mCbMatchWhole:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$8500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # I
    .param p3, "x3"    # Landroid/view/KeyEvent;

    .prologue
    .line 4907
    invoke-direct {p0, p1, p2, p3}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->onKeyUp(Landroid/view/View;ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$8600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 4907
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->setKeyEventOwner(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$8700(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;

    .prologue
    .line 4907
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mKeyEventable:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;

    return-object v0
.end method

.method private onKeyUp(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "a_view"    # Landroid/view/View;
    .param p2, "a_keyCode"    # I
    .param p3, "a_event"    # Landroid/view/KeyEvent;

    .prologue
    .line 5345
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->setKeyEventOwner(Landroid/view/View;)V

    .line 5347
    const/4 v0, 0x0

    .line 5348
    .local v0, "isSuccess":Z
    sparse-switch p2, :sswitch_data_0

    .line 5355
    :goto_0
    return v0

    .line 5352
    :sswitch_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mKeyEventable:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    invoke-virtual {v1, p2, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;->onKeyCode(II)Z

    move-result v0

    goto :goto_0

    .line 5348
    nop

    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch
.end method

.method private setKeyEventOwner(Landroid/view/View;)V
    .locals 3
    .param p1, "a_view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 4986
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 4987
    .local v0, "viewId":I
    sparse-switch v0, :sswitch_data_0

    .line 5014
    :goto_0
    return-void

    .line 4990
    :sswitch_0
    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$FindKeyEvent;

    invoke-direct {v1, p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$FindKeyEvent;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$1;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mKeyEventable:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;

    goto :goto_0

    .line 4991
    :sswitch_1
    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;

    invoke-direct {v1, p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$1;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mKeyEventable:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;

    goto :goto_0

    .line 4992
    :sswitch_2
    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;

    invoke-direct {v1, p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$1;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mKeyEventable:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;

    goto :goto_0

    .line 4993
    :sswitch_3
    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;

    invoke-direct {v1, p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$1;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mKeyEventable:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;

    goto :goto_0

    .line 4996
    :sswitch_4
    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$FindOptionKeyEvent;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$FindOptionKeyEvent;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mKeyEventable:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;

    goto :goto_0

    .line 4997
    :sswitch_5
    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$FindEditKeyEvent;

    invoke-direct {v1, p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$FindEditKeyEvent;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$1;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mKeyEventable:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;

    goto :goto_0

    .line 4998
    :sswitch_6
    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$FindNextKeyEvent;

    invoke-direct {v1, p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$FindNextKeyEvent;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$1;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mKeyEventable:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;

    goto :goto_0

    .line 5001
    :sswitch_7
    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$ReplaceOptionKeyEvent;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$ReplaceOptionKeyEvent;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mKeyEventable:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;

    goto :goto_0

    .line 5002
    :sswitch_8
    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$ReplaceFindEditKeyEvent;

    invoke-direct {v1, p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$ReplaceFindEditKeyEvent;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$1;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mKeyEventable:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;

    goto :goto_0

    .line 5003
    :sswitch_9
    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$ReplaceNextKeyEventable;

    invoke-direct {v1, p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$ReplaceNextKeyEventable;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$1;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mKeyEventable:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;

    goto :goto_0

    .line 5004
    :sswitch_a
    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$ReplaceKeyEventable;

    invoke-direct {v1, p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$ReplaceKeyEventable;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$1;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mKeyEventable:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;

    goto :goto_0

    .line 5005
    :sswitch_b
    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$ReplaceEditKeyEvent;

    invoke-direct {v1, p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$ReplaceEditKeyEvent;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$1;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mKeyEventable:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;

    goto :goto_0

    .line 5006
    :sswitch_c
    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$ReplaceAllKeyEventable;

    invoke-direct {v1, p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$ReplaceAllKeyEventable;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$1;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mKeyEventable:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;

    goto :goto_0

    .line 5009
    :sswitch_d
    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$MatchCaseKeyEvent;

    invoke-direct {v1, p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$MatchCaseKeyEvent;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$1;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mKeyEventable:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;

    goto :goto_0

    .line 5010
    :sswitch_e
    new-instance v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$MatchWholeKeyEvent;

    invoke-direct {v1, p0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$MatchWholeKeyEvent;-><init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$1;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->mKeyEventable:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController$KeyEventable;

    goto :goto_0

    .line 4987
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0b0226 -> :sswitch_d
        0x7f0b0229 -> :sswitch_e
        0x7f0b0249 -> :sswitch_3
        0x7f0b024c -> :sswitch_0
        0x7f0b024d -> :sswitch_1
        0x7f0b024e -> :sswitch_2
        0x7f0b0250 -> :sswitch_4
        0x7f0b0251 -> :sswitch_6
        0x7f0b0253 -> :sswitch_5
        0x7f0b0257 -> :sswitch_7
        0x7f0b0258 -> :sswitch_9
        0x7f0b0259 -> :sswitch_8
        0x7f0b025c -> :sswitch_a
        0x7f0b025d -> :sswitch_c
        0x7f0b025e -> :sswitch_b
    .end sparse-switch
.end method


# virtual methods
.method public isActionBarFocused()Z
    .locals 1

    .prologue
    .line 4961
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFind:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$6700(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnProperties:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$6800(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnMenu:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$6900(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindOption:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$7000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindPrevFind:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindNextFind:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1100(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnUndo:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$6500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnRedo:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$6600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFormatIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$6400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4970
    :cond_0
    const/4 v0, 0x1

    .line 4972
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setActionBarFocus(Z)V
    .locals 2
    .param p1, "a_isFocus"    # Z

    .prologue
    const/4 v1, 0x1

    .line 4941
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFind:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$6700(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 4942
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnProperties:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$6800(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 4943
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnMenu:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$6900(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 4944
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindOption:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$7000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 4945
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindPrevFind:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 4946
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFindNextFind:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$1100(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 4947
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnUndo:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$6500(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 4948
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnRedo:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$6600(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 4949
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_btnFormatIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$6400(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 4951
    if-ne p1, v1, :cond_0

    .line 4952
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setFocusable(Z)V

    .line 4957
    :goto_0
    return-void

    .line 4954
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->setFocusable(Z)V

    .line 4955
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$TextEditorKeyController;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->callOnClick()Z

    goto :goto_0
.end method
