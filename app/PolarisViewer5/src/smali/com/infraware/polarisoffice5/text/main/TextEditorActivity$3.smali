.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$3;
.super Ljava/lang/Object;
.source "TextEditorActivity.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->initControls()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
    .locals 0

    .prologue
    .line 1032
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$3;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 1035
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$3;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->m_EditText:Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$300(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v2

    if-eq p1, v2, :cond_1

    const/4 v1, 0x0

    .line 1044
    :cond_0
    :goto_0
    return v1

    .line 1037
    :cond_1
    const/4 v1, 0x0

    .line 1038
    .local v1, "ret":Z
    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v0

    .line 1039
    .local v0, "action":I
    const/4 v2, 0x3

    if-ne v2, v0, :cond_2

    .line 1040
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$3;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getMWDnD()Lcom/infraware/common/multiwindow/MWDnDOperator;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lcom/infraware/common/multiwindow/MWDnDOperator;->onDropOnTxt(Landroid/view/View;Landroid/view/DragEvent;)Z

    move-result v1

    goto :goto_0

    .line 1041
    :cond_2
    const/4 v2, 0x4

    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 1042
    iget-object v2, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$3;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getMWDnD()Lcom/infraware/common/multiwindow/MWDnDOperator;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lcom/infraware/common/multiwindow/MWDnDOperator;->onDragEndedOnTxt(Landroid/view/View;Landroid/view/DragEvent;)Z

    goto :goto_0
.end method
