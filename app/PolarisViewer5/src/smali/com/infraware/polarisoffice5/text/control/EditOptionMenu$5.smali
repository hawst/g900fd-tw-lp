.class Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$5;
.super Ljava/lang/Object;
.source "EditOptionMenu.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->optionPopupwindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)V
    .locals 0

    .prologue
    .line 260
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$5;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "keycode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x0

    .line 264
    sparse-switch p2, :sswitch_data_0

    .line 274
    :goto_0
    return v2

    .line 266
    :sswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$5;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->dismissPopupWindow()Z

    .line 267
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$5;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->mParent:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->access$400(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->setActionbarFocus(Z)V

    goto :goto_0

    .line 270
    :sswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$5;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_listView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->access$000(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    .line 271
    iget-object v0, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$5;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    # getter for: Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->m_listView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->access$000(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu$5;->this$0:Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;

    # invokes: Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->getEnabledTopPosition()I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;->access$100(Lcom/infraware/polarisoffice5/text/control/EditOptionMenu;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    .line 264
    nop

    :sswitch_data_0
    .sparse-switch
        0x14 -> :sswitch_1
        0x52 -> :sswitch_0
    .end sparse-switch
.end method
