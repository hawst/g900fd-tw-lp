.class Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$38;
.super Ljava/lang/Object;
.source "TextEditorActivity.java"

# interfaces
.implements Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onCreateOnKKOver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
    .locals 0

    .prologue
    .line 5571
    iput-object p1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$38;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onModeChanged(Z)V
    .locals 2
    .param p1, "toMulti"    # Z

    .prologue
    .line 5584
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$38;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$8900(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isNormalWindow()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 5585
    .local v0, "bSplit":Z
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$38;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # invokes: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onMultiWindowStatusChanged(Z)V
    invoke-static {v1, v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$9000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Z)V

    .line 5586
    return-void

    .line 5584
    .end local v0    # "bSplit":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSizeChanged(Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "arg0"    # Landroid/graphics/Rect;

    .prologue
    .line 5578
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$38;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$8900(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isNormalWindow()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 5579
    .local v0, "bSplit":Z
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity$38;->this$0:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    # invokes: Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->onMultiWindowStatusChanged(Z)V
    invoke-static {v1, v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->access$9000(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;Z)V

    .line 5580
    return-void

    .line 5578
    .end local v0    # "bSplit":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onZoneChanged(I)V
    .locals 0
    .param p1, "arg0"    # I

    .prologue
    .line 5574
    return-void
.end method
