.class final Lcom/infraware/polarisoffice5/word/BookmarkActivity$4;
.super Ljava/lang/Object;
.source "BookmarkActivity.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/word/BookmarkActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/infraware/polarisoffice5/common/MultiListItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final collator:Ljava/text/Collator;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 297
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$4;->collator:Ljava/text/Collator;

    return-void
.end method


# virtual methods
.method public compare(Lcom/infraware/polarisoffice5/common/MultiListItem;Lcom/infraware/polarisoffice5/common/MultiListItem;)I
    .locals 3
    .param p1, "map1"    # Lcom/infraware/polarisoffice5/common/MultiListItem;
    .param p2, "map2"    # Lcom/infraware/polarisoffice5/common/MultiListItem;

    .prologue
    .line 300
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$4;->collator:Ljava/text/Collator;

    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/common/MultiListItem;->getText1()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/infraware/polarisoffice5/common/MultiListItem;->getText1()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 297
    check-cast p1, Lcom/infraware/polarisoffice5/common/MultiListItem;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/infraware/polarisoffice5/common/MultiListItem;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/infraware/polarisoffice5/word/BookmarkActivity$4;->compare(Lcom/infraware/polarisoffice5/common/MultiListItem;Lcom/infraware/polarisoffice5/common/MultiListItem;)I

    move-result v0

    return v0
.end method
