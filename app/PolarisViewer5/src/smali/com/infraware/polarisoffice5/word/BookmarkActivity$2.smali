.class Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;
.super Landroid/os/Handler;
.source "BookmarkActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/word/BookmarkActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 111
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    .line 112
    .local v2, "bundle":Landroid/os/Bundle;
    const-string/jumbo v6, "LIST_RIGHT_BTN"

    invoke-virtual {v2, v6, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 114
    .local v0, "bRightBtn":Z
    iget-object v6, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mbClick:Z
    invoke-static {v6}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->access$000(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 116
    if-ne v0, v10, :cond_0

    .line 118
    iget-object v6, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    const-string/jumbo v7, "BUTTON_POS"

    const/4 v8, -0x1

    invoke-virtual {v2, v7, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    # setter for: Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mBtnPos:I
    invoke-static {v6, v7}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->access$102(Lcom/infraware/polarisoffice5/word/BookmarkActivity;I)I

    .line 119
    iget-object v6, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mBtnPos:I
    invoke-static {v6}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->access$100(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)I

    move-result v6

    if-ltz v6, :cond_0

    .line 121
    iget-object v6, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    # setter for: Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mbClick:Z
    invoke-static {v6, v10}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->access$002(Lcom/infraware/polarisoffice5/word/BookmarkActivity;Z)Z

    .line 122
    iget-object v6, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    iget-object v6, v6, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mSrcListItem:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mBtnPos:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->access$100(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/infraware/polarisoffice5/common/MultiListItem;

    .line 123
    .local v5, "item":Lcom/infraware/polarisoffice5/common/MultiListItem;
    iget-object v6, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070193

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 124
    .local v3, "deleteStr":Ljava/lang/String;
    new-array v6, v10, [Ljava/lang/Object;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/common/MultiListItem;->getText1()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v3, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 125
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v7

    invoke-direct {v1, v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 126
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    const-string/jumbo v6, ""

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 127
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 128
    new-instance v6, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2$1;

    invoke-direct {v6, p0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2$1;-><init>(Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;)V

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 141
    new-instance v6, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2$2;

    invoke-direct {v6, p0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2$2;-><init>(Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;)V

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 146
    const v6, 0x7f07006b

    new-instance v7, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2$4;

    invoke-direct {v7, p0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2$4;-><init>(Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;)V

    invoke-virtual {v1, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f070062

    new-instance v8, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2$3;

    invoke-direct {v8, p0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2$3;-><init>(Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 177
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    .line 178
    .local v4, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v4, v9}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 179
    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    .line 183
    .end local v1    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v3    # "deleteStr":Ljava/lang/String;
    .end local v4    # "dialog":Landroid/app/AlertDialog;
    .end local v5    # "item":Lcom/infraware/polarisoffice5/common/MultiListItem;
    :cond_0
    return-void
.end method
