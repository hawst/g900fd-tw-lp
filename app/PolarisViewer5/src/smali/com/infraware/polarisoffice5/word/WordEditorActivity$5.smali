.class Lcom/infraware/polarisoffice5/word/WordEditorActivity$5;
.super Ljava/lang/Object;
.source "WordEditorActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/word/WordEditorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)V
    .locals 0

    .prologue
    .line 858
    iput-object p1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$5;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const v4, 0x7f070068

    const v3, 0x7f020036

    const/4 v2, 0x1

    .line 861
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$5;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$300(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    move-result-object v0

    if-nez v0, :cond_0

    .line 898
    :goto_0
    return-void

    .line 866
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 880
    :pswitch_0
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 881
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$5;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$300(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    move-result-object v0

    const v1, 0x7f0b012a

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->changeHelperStatus(I)V

    .line 883
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$5;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$400(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 884
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$5;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$400(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$5;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 885
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$5;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    iput-boolean v2, v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbIsTTSPlayFlag:Z

    .line 886
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$5;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    # invokes: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onPrevLineTTS()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$500(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)V

    goto :goto_0

    .line 870
    :pswitch_1
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 871
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$5;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$300(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    move-result-object v0

    const v1, 0x7f0b012b

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->changeHelperStatus(I)V

    .line 873
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$5;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbIsTTSPlayFlag:Z

    if-eqz v0, :cond_3

    .line 874
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$5;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onPauseTTSButton()V

    goto :goto_0

    .line 876
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$5;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onPlayTTSButton()V

    goto :goto_0

    .line 889
    :pswitch_2
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 890
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$5;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$300(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    move-result-object v0

    const v1, 0x7f0b012c

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->changeHelperStatus(I)V

    .line 892
    :cond_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$5;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$400(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 893
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$5;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$400(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$5;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 894
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$5;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    iput-boolean v2, v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbIsTTSPlayFlag:Z

    .line 895
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$5;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    # invokes: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onNextLineTTS()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$600(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)V

    goto/16 :goto_0

    .line 866
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b012a
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
