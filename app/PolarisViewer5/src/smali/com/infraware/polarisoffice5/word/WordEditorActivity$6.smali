.class Lcom/infraware/polarisoffice5/word/WordEditorActivity$6;
.super Ljava/lang/Object;
.source "WordEditorActivity.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/word/WordEditorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)V
    .locals 0

    .prologue
    .line 901
    iput-object p1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$6;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v0, 0x1

    .line 906
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 920
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 908
    :pswitch_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$6;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$6;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTSBar:Landroid/view/View;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$700(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f070069

    invoke-static {v1, v2, p1, v3}, Lcom/infraware/common/util/Utils;->showTTSbarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 911
    :pswitch_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$6;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$6;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTSBar:Landroid/view/View;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$700(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f070067

    invoke-static {v1, v2, p1, v3}, Lcom/infraware/common/util/Utils;->showTTSbarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 914
    :pswitch_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$6;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    iget-boolean v1, v1, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbIsTTSPlayFlag:Z

    if-eqz v1, :cond_0

    .line 915
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$6;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$6;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTSBar:Landroid/view/View;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$700(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f070068

    invoke-static {v1, v2, p1, v3}, Lcom/infraware/common/util/Utils;->showTTSbarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 917
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$6;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$6;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTSBar:Landroid/view/View;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$700(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f07006a

    invoke-static {v1, v2, p1, v3}, Lcom/infraware/common/util/Utils;->showTTSbarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 906
    :pswitch_data_0
    .packed-switch 0x7f0b012a
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
