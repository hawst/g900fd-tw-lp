.class public Lcom/infraware/polarisoffice5/word/BookmarkActivity;
.super Lcom/infraware/common/baseactivity/BaseSwitchableActivity;
.source "BookmarkActivity.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_BOOKMARK_EDITOR_MODE;
.implements Lcom/infraware/office/evengine/E$EV_EDIT_CURSOR_MODE;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/word/BookmarkActivity$CloseActionReceiver;,
        Lcom/infraware/polarisoffice5/word/BookmarkActivity$BookmarkCallback;
    }
.end annotation


# static fields
.field protected static final sDisplayNameComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/infraware/polarisoffice5/common/MultiListItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final LOG_CAT:Ljava/lang/String;

.field bookmarkTextWatcher:Landroid/text/TextWatcher;

.field private mAdapter:Lcom/infraware/polarisoffice5/common/MultiAdapter;

.field protected mAddBookmarkEdit:Landroid/widget/EditText;

.field protected mAddBtn:Landroid/widget/ImageButton;

.field private mBtnPos:I

.field mClickListener:Landroid/view/View$OnClickListener;

.field private mEvInterface:Lcom/infraware/office/evengine/EvInterface;

.field protected mSrcListItem:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/common/MultiListItem;",
            ">;"
        }
    .end annotation
.end field

.field private m_bookmarkCallback:Lcom/infraware/polarisoffice5/word/BookmarkActivity$BookmarkCallback;

.field private m_oCloseFilter:Landroid/content/IntentFilter;

.field private m_oCloseReceiver:Lcom/infraware/polarisoffice5/word/BookmarkActivity$CloseActionReceiver;

.field private m_oInputFilter:Lcom/infraware/polarisoffice5/common/EditInputFilter;

.field private mbClick:Z

.field private mbEditMode:Z

.field protected final messageHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 297
    new-instance v0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$4;

    invoke-direct {v0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity$4;-><init>()V

    sput-object v0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->sDisplayNameComparator:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 46
    invoke-direct {p0}, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;-><init>()V

    .line 47
    const-string/jumbo v0, "BookmarkActivity"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->LOG_CAT:Ljava/lang/String;

    .line 48
    iput-object v1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 49
    iput-object v1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mAddBookmarkEdit:Landroid/widget/EditText;

    .line 50
    iput-object v1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mAdapter:Lcom/infraware/polarisoffice5/common/MultiAdapter;

    .line 51
    iput-object v1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mAddBtn:Landroid/widget/ImageButton;

    .line 52
    iput v2, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mBtnPos:I

    .line 53
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mbClick:Z

    .line 54
    iput-object v1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->m_oInputFilter:Lcom/infraware/polarisoffice5/common/EditInputFilter;

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mbEditMode:Z

    .line 60
    iput-object v1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/word/BookmarkActivity$CloseActionReceiver;

    .line 61
    iput-object v1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    .line 93
    iput-object v1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mSrcListItem:Ljava/util/ArrayList;

    .line 94
    new-instance v0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity$1;-><init>(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mClickListener:Landroid/view/View$OnClickListener;

    .line 109
    new-instance v0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;-><init>(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->messageHandler:Landroid/os/Handler;

    .line 378
    new-instance v0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$5;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity$5;-><init>(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->bookmarkTextWatcher:Landroid/text/TextWatcher;

    .line 405
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mbClick:Z

    return v0
.end method

.method static synthetic access$002(Lcom/infraware/polarisoffice5/word/BookmarkActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/word/BookmarkActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mbClick:Z

    return p1
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    .prologue
    .line 46
    iget v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mBtnPos:I

    return v0
.end method

.method static synthetic access$102(Lcom/infraware/polarisoffice5/word/BookmarkActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/word/BookmarkActivity;
    .param p1, "x1"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mBtnPos:I

    return p1
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)Lcom/infraware/office/evengine/EvInterface;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)Lcom/infraware/polarisoffice5/word/BookmarkActivity$BookmarkCallback;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->m_bookmarkCallback:Lcom/infraware/polarisoffice5/word/BookmarkActivity$BookmarkCallback;

    return-object v0
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->initBookmark()V

    return-void
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)Lcom/infraware/polarisoffice5/common/MultiAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mAdapter:Lcom/infraware/polarisoffice5/common/MultiAdapter;

    return-object v0
.end method

.method private initBookmark()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 262
    const/4 v0, 0x0

    .line 263
    .local v0, "item":Lcom/infraware/polarisoffice5/common/MultiListItem;
    const/4 v2, 0x0

    .line 264
    .local v2, "strBookmark":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0700df

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 265
    .local v9, "strTemp":Ljava/lang/String;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v1, :cond_1

    .line 266
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetBookmarkCount_Editor()I

    move-result v8

    .line 268
    .local v8, "nRet":I
    const-string/jumbo v1, "BookmarkActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "GetBookmark nRet = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    if-lez v8, :cond_3

    .line 270
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v8, :cond_0

    .line 271
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1, v6}, Lcom/infraware/office/evengine/EvInterface;->IGetBookmarkInfo_Editor(I)Ljava/lang/String;

    move-result-object v2

    .line 273
    new-instance v0, Lcom/infraware/polarisoffice5/common/MultiListItem;

    .end local v0    # "item":Lcom/infraware/polarisoffice5/common/MultiListItem;
    const/4 v1, 0x5

    const v3, 0x7f0200ab

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02005a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    iget-boolean v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mbEditMode:Z

    invoke-direct/range {v0 .. v5}, Lcom/infraware/polarisoffice5/common/MultiListItem;-><init>(ILjava/lang/String;IIZ)V

    .line 274
    .restart local v0    # "item":Lcom/infraware/polarisoffice5/common/MultiListItem;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mSrcListItem:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 270
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 277
    :cond_0
    new-instance v10, Ljava/lang/String;

    invoke-direct {v10}, Ljava/lang/String;-><init>()V

    .line 278
    .local v10, "temp":Ljava/lang/String;
    const/4 v7, 0x1

    .local v7, "k":I
    :goto_1
    add-int/lit8 v1, v8, 0x1

    if-ge v7, v1, :cond_1

    .line 280
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "%02d"

    new-array v4, v11, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mSrcListItem:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/2addr v5, v7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v12

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 281
    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->initBookmarkName(Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v11, :cond_2

    .line 283
    move-object v9, v10

    .line 292
    .end local v6    # "i":I
    .end local v7    # "k":I
    .end local v8    # "nRet":I
    .end local v10    # "temp":Ljava/lang/String;
    :cond_1
    :goto_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mAddBookmarkEdit:Landroid/widget/EditText;

    invoke-virtual {v1, v9}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 293
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mAddBookmarkEdit:Landroid/widget/EditText;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setSelection(I)V

    .line 294
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mSrcListItem:Ljava/util/ArrayList;

    sget-object v3, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->sDisplayNameComparator:Ljava/util/Comparator;

    invoke-static {v1, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 295
    return-void

    .line 278
    .restart local v6    # "i":I
    .restart local v7    # "k":I
    .restart local v8    # "nRet":I
    .restart local v10    # "temp":Ljava/lang/String;
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 288
    .end local v6    # "i":I
    .end local v7    # "k":I
    .end local v10    # "temp":Ljava/lang/String;
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "%02d"

    new-array v4, v11, [Ljava/lang/Object;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v12

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto :goto_2
.end method


# virtual methods
.method protected AddBookmark()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 314
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mAddBookmarkEdit:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 315
    .local v4, "strAddBookmark":Ljava/lang/String;
    const-string/jumbo v5, "BookmarkActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "AddBookmark return ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mSrcListItem:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v2, v5, :cond_1

    .line 317
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mSrcListItem:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/infraware/polarisoffice5/common/MultiListItem;

    .line 318
    .local v3, "item":Lcom/infraware/polarisoffice5/common/MultiListItem;
    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/common/MultiListItem;->getText1()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-ne v5, v9, :cond_0

    .line 319
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v5

    invoke-direct {v0, p0, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 320
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const-string/jumbo v5, ""

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 321
    const v5, 0x7f0700dd

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 322
    const v5, 0x7f070063

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 323
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 324
    .local v1, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v1, v8}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 325
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 326
    iput-boolean v8, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mbClick:Z

    .line 338
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v1    # "dialog":Landroid/app/AlertDialog;
    .end local v3    # "item":Lcom/infraware/polarisoffice5/common/MultiListItem;
    :goto_1
    return-void

    .line 316
    .restart local v3    # "item":Lcom/infraware/polarisoffice5/common/MultiListItem;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 330
    .end local v3    # "item":Lcom/infraware/polarisoffice5/common/MultiListItem;
    :cond_1
    iput-boolean v9, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mbClick:Z

    .line 331
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v5, :cond_2

    .line 332
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v5, v8, v4}, Lcom/infraware/office/evengine/EvInterface;->IBookmarkEditor(ILjava/lang/String;)V

    .line 333
    :cond_2
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mSrcListItem:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 334
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->initBookmark()V

    .line 335
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mAdapter:Lcom/infraware/polarisoffice5/common/MultiAdapter;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/common/MultiAdapter;->notifyDataSetChanged()V

    .line 336
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0701da

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 337
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->finish()V

    goto :goto_1
.end method

.method protected initBookmarkName(Ljava/lang/String;)Z
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 305
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mSrcListItem:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 307
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mSrcListItem:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/MultiListItem;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/MultiListItem;->getText1()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 308
    const/4 v1, 0x0

    .line 310
    :goto_1
    return v1

    .line 305
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 310
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public initLayout()V
    .locals 6

    .prologue
    const v5, 0x7f0b002e

    const v4, 0x7f0b002c

    const v3, 0x7f0b0026

    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 341
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mbEditMode:Z

    if-nez v0, :cond_2

    .line 343
    const v0, 0x7f0b0029

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 344
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetBookmarkCount_Editor()I

    move-result v0

    if-nez v0, :cond_1

    .line 346
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 347
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 367
    :cond_0
    :goto_0
    return-void

    .line 350
    :cond_1
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 351
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 352
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 357
    :cond_2
    const v0, 0x7f0b0029

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 358
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 359
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 360
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    .line 361
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetBookmarkCount_Editor()I

    move-result v0

    if-nez v0, :cond_3

    .line 362
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 364
    :cond_3
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 187
    invoke-super {p0, p1}, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->onCreate(Landroid/os/Bundle;)V

    .line 188
    const v5, 0x7f030004

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->setContentView(I)V

    .line 191
    new-instance v5, Lcom/infraware/polarisoffice5/word/BookmarkActivity$CloseActionReceiver;

    invoke-direct {v5, p0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity$CloseActionReceiver;-><init>(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)V

    iput-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/word/BookmarkActivity$CloseActionReceiver;

    .line 192
    new-instance v5, Landroid/content/IntentFilter;

    invoke-direct {v5}, Landroid/content/IntentFilter;-><init>()V

    iput-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    .line 193
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    const-string/jumbo v6, "com.infraware.polarisviewer5.CLOSE"

    invoke-virtual {v5, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 194
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/word/BookmarkActivity$CloseActionReceiver;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v5, v6}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 196
    new-instance v5, Lcom/infraware/office/actionbar/ActionTitleBar;

    const v6, 0x7f0b001d

    invoke-direct {v5, p0, v6}, Lcom/infraware/office/actionbar/ActionTitleBar;-><init>(Landroid/app/Activity;I)V

    iput-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    .line 197
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const v6, 0x7f0700df

    invoke-virtual {v5, v6}, Lcom/infraware/office/actionbar/ActionTitleBar;->setTitle(I)V

    .line 198
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v5}, Lcom/infraware/office/actionbar/ActionTitleBar;->show()V

    .line 200
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mSrcListItem:Ljava/util/ArrayList;

    .line 202
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v5

    iput-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 203
    const v5, 0x7f0b002b

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    iput-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mAddBookmarkEdit:Landroid/widget/EditText;

    .line 205
    new-instance v5, Lcom/infraware/polarisoffice5/common/EditInputFilter;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/infraware/polarisoffice5/common/EditInputFilter;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->m_oInputFilter:Lcom/infraware/polarisoffice5/common/EditInputFilter;

    .line 206
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->m_oInputFilter:Lcom/infraware/polarisoffice5/common/EditInputFilter;

    const/16 v6, 0x28

    invoke-virtual {v5, v6}, Lcom/infraware/polarisoffice5/common/EditInputFilter;->setMaxLength(I)V

    .line 207
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mAddBookmarkEdit:Landroid/widget/EditText;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->m_oInputFilter:Lcom/infraware/polarisoffice5/common/EditInputFilter;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/common/EditInputFilter;->getFilters()[Landroid/text/InputFilter;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 209
    const v5, 0x7f0b002a

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mAddBtn:Landroid/widget/ImageButton;

    .line 210
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mAddBtn:Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 212
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 214
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v5, "istexteditoractivity"

    invoke-virtual {v0, v5, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 216
    .local v1, "isTextEditorActivity":Z
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v5, :cond_2

    .line 217
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v5}, Lcom/infraware/office/evengine/EvInterface;->IGetCaretInfo_Editor()Lcom/infraware/office/evengine/EV$CARET_INFO;

    move-result-object v5

    iget v3, v5, Lcom/infraware/office/evengine/EV$CARET_INFO;->bCaret:I

    .line 218
    .local v3, "nCaretMode":I
    if-eq v3, v8, :cond_0

    const/4 v5, 0x2

    if-eq v3, v5, :cond_0

    const/4 v5, 0x3

    if-ne v3, v5, :cond_3

    .line 219
    :cond_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mAddBtn:Landroid/widget/ImageButton;

    invoke-virtual {v5, v8}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 220
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mAddBookmarkEdit:Landroid/widget/EditText;

    invoke-virtual {v5, v8}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 221
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mAddBookmarkEdit:Landroid/widget/EditText;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->bookmarkTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 231
    :cond_1
    :goto_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v5}, Lcom/infraware/office/evengine/EvInterface;->IGetEditorMode_Editor()I

    move-result v5

    if-nez v5, :cond_2

    .line 232
    iput-boolean v7, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mbEditMode:Z

    .line 235
    .end local v3    # "nCaretMode":I
    :cond_2
    const-string/jumbo v5, "HYOHYUN"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "BookClip mbEditMode= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mbEditMode:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->initLayout()V

    .line 237
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->initBookmark()V

    .line 238
    const v5, 0x7f0b002d

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    .line 239
    .local v2, "listView":Landroid/widget/ListView;
    new-instance v5, Lcom/infraware/polarisoffice5/common/MultiAdapter;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mSrcListItem:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->messageHandler:Landroid/os/Handler;

    invoke-direct {v5, p0, v6, v7}, Lcom/infraware/polarisoffice5/common/MultiAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Landroid/os/Handler;)V

    iput-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mAdapter:Lcom/infraware/polarisoffice5/common/MultiAdapter;

    .line 240
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mAdapter:Lcom/infraware/polarisoffice5/common/MultiAdapter;

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 241
    invoke-virtual {v2, v8}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 242
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020010

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 243
    .local v4, "selector":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 244
    new-instance v5, Lcom/infraware/polarisoffice5/word/BookmarkActivity$3;

    invoke-direct {v5, p0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity$3;-><init>(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)V

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 259
    return-void

    .line 224
    .end local v2    # "listView":Landroid/widget/ListView;
    .end local v4    # "selector":Landroid/graphics/drawable/Drawable;
    .restart local v3    # "nCaretMode":I
    :cond_3
    if-nez v1, :cond_1

    .line 225
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mAddBtn:Landroid/widget/ImageButton;

    invoke-virtual {v5, v7}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 226
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mAddBtn:Landroid/widget/ImageButton;

    invoke-virtual {v5, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 227
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mAddBookmarkEdit:Landroid/widget/EditText;

    invoke-virtual {v5, v7}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 228
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mAddBookmarkEdit:Landroid/widget/EditText;

    invoke-virtual {v5, v7}, Landroid/widget/EditText;->setFocusable(Z)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mSrcListItem:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mSrcListItem:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 68
    iput-object v1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mSrcListItem:Ljava/util/ArrayList;

    .line 70
    :cond_0
    iput-object v1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->m_oInputFilter:Lcom/infraware/polarisoffice5/common/EditInputFilter;

    .line 73
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/word/BookmarkActivity$CloseActionReceiver;

    if-eqz v0, :cond_1

    .line 74
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/word/BookmarkActivity$CloseActionReceiver;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 76
    :cond_1
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->onDestroy()V

    .line 77
    return-void
.end method

.method public onLocaleChange(I)V
    .locals 1
    .param p1, "nLocale"    # I

    .prologue
    .line 372
    const v0, 0x7f0700df

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->setTitle(I)V

    .line 373
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mSrcListItem:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 374
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mAdapter:Lcom/infraware/polarisoffice5/common/MultiAdapter;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/MultiAdapter;->notifyDataSetChanged()V

    .line 375
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->initBookmark()V

    .line 376
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 82
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->onPause()V

    .line 83
    return-void
.end method

.method public setBookmarkCallback(Lcom/infraware/polarisoffice5/word/BookmarkActivity$BookmarkCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/infraware/polarisoffice5/word/BookmarkActivity$BookmarkCallback;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->m_bookmarkCallback:Lcom/infraware/polarisoffice5/word/BookmarkActivity$BookmarkCallback;

    .line 91
    return-void
.end method
