.class Lcom/infraware/polarisoffice5/word/BookmarkActivity$5;
.super Ljava/lang/Object;
.source "BookmarkActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/word/BookmarkActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)V
    .locals 0

    .prologue
    .line 378
    iput-object p1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$5;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 401
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 398
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 8
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 381
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$5;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    iget-object v5, v5, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mAddBookmarkEdit:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 383
    .local v1, "bookmarkTxt":Ljava/lang/String;
    const/4 v0, 0x0

    .line 384
    .local v0, "bEnabled":Z
    move-object v4, v1

    .line 385
    .local v4, "strTemp":Ljava/lang/String;
    const-string/jumbo v5, "BookmarkActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "HYOHYUN bookmarkTxt.length() = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    .line 388
    .local v3, "strChar":[B
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v5, v3

    if-ge v2, v5, :cond_1

    .line 390
    aget-byte v5, v3, v2

    const/16 v6, 0x20

    if-eq v5, v6, :cond_0

    .line 391
    const/4 v0, 0x1

    .line 388
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 394
    :cond_1
    iget-object v5, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$5;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    iget-object v5, v5, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mAddBtn:Landroid/widget/ImageButton;

    invoke-virtual {v5, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 395
    return-void
.end method
