.class Lcom/infraware/polarisoffice5/word/WordEditorActivity$NfcCallback;
.super Ljava/lang/Object;
.source "WordEditorActivity.java"

# interfaces
.implements Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;
.implements Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/word/WordEditorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NfcCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;


# direct methods
.method private constructor <init>(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)V
    .locals 0

    .prologue
    .line 1160
    iput-object p1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/infraware/polarisoffice5/word/WordEditorActivity;Lcom/infraware/polarisoffice5/word/WordEditorActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/infraware/polarisoffice5/word/WordEditorActivity;
    .param p2, "x1"    # Lcom/infraware/polarisoffice5/word/WordEditorActivity$1;

    .prologue
    .line 1160
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/word/WordEditorActivity$NfcCallback;-><init>(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)V

    return-void
.end method


# virtual methods
.method public createNdefMessage(Landroid/nfc/NfcEvent;)Landroid/nfc/NdefMessage;
    .locals 7
    .param p1, "event"    # Landroid/nfc/NfcEvent;

    .prologue
    const/4 v3, 0x0

    .line 1178
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1195
    :cond_0
    :goto_0
    return-object v3

    .line 1180
    :cond_1
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-static {v4}, Lcom/infraware/common/util/SBeamUtils;->isSBeamSupportedDevice(Landroid/content/Context;)Z

    move-result v2

    .line 1181
    .local v2, "sbeamSupport":Z
    if-eqz v2, :cond_0

    .line 1182
    iget-object v3, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-static {v4}, Lcom/infraware/common/util/SBeamUtils;->isSBeamEnabled(Landroid/content/Context;)Z

    move-result v4

    # setter for: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mSBeamEnabled:Z
    invoke-static {v3, v4}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$902(Lcom/infraware/polarisoffice5/word/WordEditorActivity;Z)Z

    .line 1184
    iget-object v3, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mstrOpenFilePath:Ljava/lang/String;
    invoke-static {v6}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$1000(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/infraware/common/util/SBeamUtils;->createSBeamNdefRecord(Landroid/content/Context;[Ljava/lang/String;)Landroid/nfc/NdefRecord;

    move-result-object v1

    .line 1185
    .local v1, "record":Landroid/nfc/NdefRecord;
    new-instance v0, Ljava/lang/String;

    invoke-virtual {v1}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>([B)V

    .line 1187
    .local v0, "payload":Ljava/lang/String;
    iget-object v3, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mSBeamEnabled:Z
    invoke-static {v3}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$900(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1188
    sget-object v3, Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;->ERROR_SBEAM_OFF:Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

    invoke-static {v3}, Lcom/infraware/common/util/SBeamUtils;->createSBeamNdefErrorRecord(Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;)Landroid/nfc/NdefRecord;

    move-result-object v1

    .line 1193
    :cond_2
    :goto_1
    invoke-static {v1}, Lcom/infraware/common/util/SBeamUtils;->createSBeamNdefMessage(Landroid/nfc/NdefRecord;)Landroid/nfc/NdefMessage;

    move-result-object v3

    goto :goto_0

    .line 1190
    :cond_3
    const-string/jumbo v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1191
    sget-object v3, Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;->ERROR_FILE_NOT_SELECTED:Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

    invoke-static {v3}, Lcom/infraware/common/util/SBeamUtils;->createSBeamNdefErrorRecord(Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;)Landroid/nfc/NdefRecord;

    move-result-object v1

    goto :goto_1
.end method

.method public onNdefPushComplete(Landroid/nfc/NfcEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/nfc/NfcEvent;

    .prologue
    .line 1163
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1174
    :cond_0
    :goto_0
    return-void

    .line 1165
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-static {v1}, Lcom/infraware/common/util/SBeamUtils;->isSBeamSupportedDevice(Landroid/content/Context;)Z

    move-result v0

    .line 1166
    .local v0, "sbeamSupport":Z
    if-eqz v0, :cond_0

    .line 1167
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mSBeamEnabled:Z
    invoke-static {v1}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$900(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1168
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-static {v1}, Lcom/infraware/common/util/SBeamUtils;->startSBeamDirectShareService(Landroid/content/Context;)V

    goto :goto_0

    .line 1171
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    sget-object v2, Lcom/infraware/common/util/SBeamUtils$SBeamPopupType;->POPUP_SBEAM_DISABLED:Lcom/infraware/common/util/SBeamUtils$SBeamPopupType;

    invoke-static {v1, v2}, Lcom/infraware/common/util/SBeamUtils;->startSBeamPopupActivity(Landroid/content/Context;Lcom/infraware/common/util/SBeamUtils$SBeamPopupType;)V

    goto :goto_0
.end method
