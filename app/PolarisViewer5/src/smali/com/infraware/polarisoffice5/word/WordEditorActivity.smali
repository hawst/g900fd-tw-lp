.class public Lcom/infraware/polarisoffice5/word/WordEditorActivity;
.super Lcom/infraware/office/baseframe/EvBaseEditorActivity;
.source "WordEditorActivity.java"

# interfaces
.implements Lcom/infraware/office/baseframe/EvBaseE$EV_ETC_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_BOOKMARK_EDITOR_MODE;
.implements Lcom/infraware/office/evengine/EvListener$WordEditorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/word/WordEditorActivity$NfcCallback;
    }
.end annotation


# static fields
.field public static final FLING_FOR_CANE_TEXT_TO_SPEECH:I = 0x4

.field public static final PAUSE_TOOLBAR_TEXT_TO_SPEECH:I = 0x2

.field public static final SHOW_TOOLBAR_TEXT_TO_SPEECH:I = 0x3

.field public static final STOP_TOOLBAR_TEXT_TO_SPEECH:I = 0x1


# instance fields
.field private final MEMO_CONTENT_MODE:I

.field private final MEMO_DIRECTION_MODE:I

.field private final MEMO_SETTING_MODE:I

.field private btnClickListener:Landroid/view/View$OnClickListener;

.field private mDialog:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

.field private mDlgHandler:Landroid/os/Handler;

.field mDocViewLayout:Landroid/widget/RelativeLayout;

.field private mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

.field private mHandler:Landroid/os/Handler;

.field public mIsMemoViewMode:Z

.field public mMemo_view:Lcom/infraware/polarisoffice5/common/MemoView;

.field private mNfcAdapter:Landroid/nfc/NfcAdapter;

.field private mNfcCallback:Lcom/infraware/polarisoffice5/word/WordEditorActivity$NfcCallback;

.field private mSBeamEnabled:Z

.field private mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

.field private mShowImeOfNewDocument:Z

.field private mTTSBar:Landroid/view/View;

.field private mTTSToolbarClickListener:Landroid/view/View$OnClickListener;

.field private mTTSToolbarKeyListener:Landroid/view/View$OnKeyListener;

.field private mTTSToolbarLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mTTS_Next:Landroid/widget/ImageView;

.field private mTTS_Prev:Landroid/widget/ImageView;

.field private mTTS_play_pause:Landroid/widget/ImageView;

.field mbIsTTSPlayFlag:Z

.field private mbNoMargin:Z

.field private mbReflowText:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 70
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;-><init>()V

    .line 73
    iput-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    .line 75
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbNoMargin:Z

    .line 76
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbReflowText:Z

    .line 78
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mShowImeOfNewDocument:Z

    .line 80
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mIsMemoViewMode:Z

    .line 81
    iput-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mMemo_view:Lcom/infraware/polarisoffice5/common/MemoView;

    .line 83
    iput-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 84
    iput-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/word/WordEditorActivity$NfcCallback;

    .line 85
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mSBeamEnabled:Z

    .line 87
    iput v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->MEMO_SETTING_MODE:I

    .line 88
    iput v2, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->MEMO_CONTENT_MODE:I

    .line 89
    const/4 v0, 0x2

    iput v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->MEMO_DIRECTION_MODE:I

    .line 91
    iput-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mDocViewLayout:Landroid/widget/RelativeLayout;

    .line 93
    iput-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    .line 94
    iput-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 158
    new-instance v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity$2;-><init>(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->btnClickListener:Landroid/view/View$OnClickListener;

    .line 428
    new-instance v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$3;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity$3;-><init>(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mHandler:Landroid/os/Handler;

    .line 700
    iput-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTSBar:Landroid/view/View;

    .line 701
    iput-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_Prev:Landroid/widget/ImageView;

    .line 702
    iput-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_Next:Landroid/widget/ImageView;

    .line 703
    iput-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    .line 704
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbIsTTSPlayFlag:Z

    .line 812
    new-instance v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$4;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity$4;-><init>(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTSToolbarKeyListener:Landroid/view/View$OnKeyListener;

    .line 858
    new-instance v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$5;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity$5;-><init>(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTSToolbarClickListener:Landroid/view/View$OnClickListener;

    .line 901
    new-instance v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$6;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity$6;-><init>(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTSToolbarLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 1268
    iput-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mDialog:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    .line 1310
    new-instance v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$8;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity$8;-><init>(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mDlgHandler:Landroid/os/Handler;

    return-void
.end method

.method private OnRefNote(I)V
    .locals 2
    .param p1, "nType"    # I

    .prologue
    .line 273
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, -0x1

    invoke-virtual {v0, p1, v1}, Lcom/infraware/office/evengine/EvInterface;->ISetRefNote(II)V

    .line 274
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mstrOpenFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Lcom/infraware/common/util/SbeamHelper;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mstrOpenFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbNoMargin:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbReflowText:Z

    return v0
.end method

.method static synthetic access$1300(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onNomarginMode()V

    return-void
.end method

.method static synthetic access$1400(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onInsertBookmarkActivity()V

    return-void
.end method

.method static synthetic access$1500(Lcom/infraware/polarisoffice5/word/WordEditorActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/word/WordEditorActivity;
    .param p1, "x1"    # I

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->OnRefNote(I)V

    return-void
.end method

.method static synthetic access$1600(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Lcom/infraware/office/baseframe/EvBaseView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onInitTTSToolbar()V

    return-void
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    return-object v0
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onPrevLineTTS()V

    return-void
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onNextLineTTS()V

    return-void
.end method

.method static synthetic access$700(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTSBar:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$900(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mSBeamEnabled:Z

    return v0
.end method

.method static synthetic access$902(Lcom/infraware/polarisoffice5/word/WordEditorActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/word/WordEditorActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mSBeamEnabled:Z

    return p1
.end method

.method private checkEnable(I)Z
    .locals 6
    .param p1, "event"    # I

    .prologue
    const/4 v5, 0x3

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 1212
    packed-switch p1, :pswitch_data_0

    .line 1241
    :cond_0
    :goto_0
    return v2

    .line 1215
    :pswitch_0
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HYPERLINK()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1218
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v4}, Lcom/infraware/office/evengine/EvInterface;->IGetHyperLinkInfo_Enable()Z

    move-result v4

    if-ne v4, v3, :cond_1

    move v2, v3

    .line 1220
    goto :goto_0

    .line 1223
    :cond_1
    :pswitch_1
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v4}, Lcom/infraware/office/evengine/EvInterface;->IGetCaretInfo_Editor()Lcom/infraware/office/evengine/EV$CARET_INFO;

    move-result-object v4

    iget v1, v4, Lcom/infraware/office/evengine/EV$CARET_INFO;->bCaret:I

    .line 1224
    .local v1, "nCaretMode":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/infraware/office/baseframe/EvBaseView;->GetObjCtrlType()I

    move-result v0

    .line 1226
    .local v0, "ObjType":I
    if-ne v5, v0, :cond_2

    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v4}, Lcom/infraware/office/evengine/EvInterface;->IGetHyperLinkInfo_Enable()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1229
    :cond_2
    const/4 v4, 0x5

    if-eq v4, v0, :cond_3

    const/4 v4, 0x7

    if-eq v4, v0, :cond_3

    const/4 v4, 0x6

    if-eq v4, v0, :cond_3

    const/16 v4, 0x12

    if-ne v4, v0, :cond_4

    :cond_3
    move v2, v3

    .line 1230
    goto :goto_0

    .line 1232
    :cond_4
    if-eqz v0, :cond_5

    if-ne v5, v0, :cond_0

    :cond_5
    if-eq v1, v3, :cond_6

    if-eq v1, v5, :cond_6

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    :cond_6
    move v2, v3

    .line 1235
    goto :goto_0

    .line 1212
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private onHideMemoView()Z
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mMemo_view:Lcom/infraware/polarisoffice5/common/MemoView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/MemoView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 416
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mMemo_view:Lcom/infraware/polarisoffice5/common/MemoView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/MemoView;->hide()V

    .line 418
    const/4 v0, 0x1

    .line 420
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onHideTTSToolbar()V
    .locals 2

    .prologue
    .line 749
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTSBar:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 750
    return-void
.end method

.method private onInitTTSToolbar()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 708
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTSBar:Landroid/view/View;

    if-nez v0, :cond_2

    .line 709
    const v0, 0x7f0b0129

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTSBar:Landroid/view/View;

    .line 711
    const v0, 0x7f0b012a

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_Prev:Landroid/widget/ImageView;

    .line 712
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_Prev:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTSToolbarClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 713
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_Prev:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTSToolbarKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 714
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_Prev:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTSToolbarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 715
    invoke-static {p0}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HOVERING(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 716
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_Prev:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 718
    :cond_0
    const v0, 0x7f0b012b

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    .line 719
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTSToolbarClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 720
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTSToolbarKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 721
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTSToolbarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 722
    invoke-static {p0}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HOVERING(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 723
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 725
    :cond_1
    const v0, 0x7f0b012c

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_Next:Landroid/widget/ImageView;

    .line 726
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_Next:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTSToolbarClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 727
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_Next:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTSToolbarKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 728
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_Next:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTSToolbarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 729
    invoke-static {p0}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HOVERING(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 730
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_Next:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 733
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTSBar:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 735
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbIsTTSPlayFlag:Z

    .line 736
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    const v1, 0x7f020036

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 737
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070068

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 738
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTSBar:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 739
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 740
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->enableTTSToolbar(Z)V

    .line 742
    :cond_3
    return-void
.end method

.method private onInsertBookmarkActivity()V
    .locals 2

    .prologue
    .line 321
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 323
    .local v0, "bookmarkIntent":Landroid/content/Intent;
    const/16 v1, 0x8

    invoke-virtual {p0, v0, v1}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 324
    return-void
.end method

.method private onNextLineTTS()V
    .locals 1

    .prologue
    .line 754
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    if-eqz v0, :cond_0

    .line 755
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->nextLine()V

    .line 756
    :cond_0
    return-void
.end method

.method private onNomarginMode()V
    .locals 1

    .prologue
    .line 354
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbNoMargin:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbNoMargin:Z

    .line 355
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->INoMarginView()V

    .line 356
    return-void

    .line 354
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onOpenLayoutDlg()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1245
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1247
    .local v0, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v4, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1249
    invoke-direct {p0, v3}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->checkEnable(I)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1250
    invoke-direct {p0, v5}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->checkEnable(I)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v5, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1252
    iget-object v2, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetCaretInfo_Editor()Lcom/infraware/office/evengine/EV$CARET_INFO;

    move-result-object v2

    iget v1, v2, Lcom/infraware/office/evengine/EV$CARET_INFO;->bCaret:I

    .line 1254
    .local v1, "nCaret":I
    if-ne v1, v3, :cond_0

    .line 1256
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v6, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1257
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v7, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1265
    :goto_0
    invoke-static {}, Lcom/infraware/polarisoffice5/common/DlgFactory;->getInstance()Lcom/infraware/polarisoffice5/common/DlgFactory;

    move-result-object v2

    const/16 v3, 0x30

    invoke-virtual {v2, p0, v3, v0}, Lcom/infraware/polarisoffice5/common/DlgFactory;->createLayoutDlg(Landroid/app/Activity;ILjava/util/ArrayList;)Lcom/infraware/polarisoffice5/common/LayoutDlg;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mDlgHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/LayoutDlg;->setEventHandler(Landroid/os/Handler;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 1267
    return-void

    .line 1261
    :cond_0
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v6, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1262
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v7, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method private onOpenViewDlg()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1271
    invoke-static {}, Lcom/infraware/polarisoffice5/common/DlgFactory;->getInstance()Lcom/infraware/polarisoffice5/common/DlgFactory;

    move-result-object v1

    const/16 v2, 0x2e

    invoke-virtual {v1, p0, v2}, Lcom/infraware/polarisoffice5/common/DlgFactory;->createViewSettDlg(Lcom/infraware/office/baseframe/EvBaseViewerActivity;I)Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mDialog:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    .line 1273
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-nez v1, :cond_0

    .line 1291
    :goto_0
    return-void

    .line 1274
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->getMemoCmdData()Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;

    move-result-object v0

    .line 1275
    .local v0, "memo":Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->clear()V

    .line 1276
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1, v4, v0}, Lcom/infraware/office/evengine/EvInterface;->IMemoCommand(ILcom/infraware/office/evengine/EV$MEMO_CMD_DATA;)Z

    .line 1278
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mDialog:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    const-string/jumbo v2, "MEMO"

    iget-boolean v3, v0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->bShow:Z

    invoke-virtual {v1, v2, v3}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->setDefaultChecked(Ljava/lang/String;Z)V

    .line 1279
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mDialog:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    const-string/jumbo v2, "REFLOW_TEXT"

    iget-object v3, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v3}, Lcom/infraware/office/actionbar/MainActionBar;->getTotalLoadState()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->setDefaultEnable(Ljava/lang/String;Z)V

    .line 1281
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetViewOption()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    .line 1282
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbNoMargin:Z

    .line 1286
    :goto_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mDialog:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    const-string/jumbo v2, "NO_MARGIN_VIEW"

    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbNoMargin:Z

    invoke-virtual {v1, v2, v3}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->setDefaultChecked(Ljava/lang/String;Z)V

    .line 1287
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mDialog:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    const-string/jumbo v2, "REFLOW_TEXT"

    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbReflowText:Z

    invoke-virtual {v1, v2, v3}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->setDefaultChecked(Ljava/lang/String;Z)V

    .line 1288
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mDialog:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    const-string/jumbo v2, "SPELL_CHECK"

    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbSpellCheck:Z

    invoke-virtual {v1, v2, v3}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->setDefaultChecked(Ljava/lang/String;Z)V

    .line 1289
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mDialog:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mDlgHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->setEventHandler(Landroid/os/Handler;)Landroid/app/AlertDialog;

    .line 1290
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mDialog:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->show()V

    goto :goto_0

    .line 1284
    :cond_1
    iput-boolean v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbNoMargin:Z

    goto :goto_1
.end method

.method private onPageLayoutActivity()V
    .locals 0

    .prologue
    .line 331
    return-void
.end method

.method private onPauseTTS()V
    .locals 2

    .prologue
    .line 774
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    if-eqz v0, :cond_0

    .line 775
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->pause()V

    .line 777
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 778
    return-void
.end method

.method private onPlayTextToSpeech(Z)V
    .locals 12
    .param p1, "bMore"    # Z

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 584
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xd

    if-le v4, v5, :cond_1

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v4

    if-nez v4, :cond_1

    .line 585
    const-string/jumbo v4, "accessibility"

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 586
    .local v0, "am":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0, v7}, Landroid/view/accessibility/AccessibilityManager;->getEnabledAccessibilityServiceList(I)Ljava/util/List;

    move-result-object v1

    .line 587
    .local v1, "asList":Ljava/util/List;, "Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 588
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/accessibilityservice/AccessibilityServiceInfo;

    invoke-virtual {v4}, Landroid/accessibilityservice/AccessibilityServiceInfo;->getId()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "com.google.android.marvin.talkback/.TalkBackService"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 590
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v4

    if-nez v4, :cond_0

    .line 591
    const v4, 0x7f070313

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 655
    .end local v0    # "am":Landroid/view/accessibility/AccessibilityManager;
    .end local v1    # "asList":Ljava/util/List;, "Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    .end local v2    # "i":I
    :goto_1
    return-void

    .line 587
    .restart local v0    # "am":Landroid/view/accessibility/AccessibilityManager;
    .restart local v1    # "asList":Ljava/util/List;, "Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    .restart local v2    # "i":I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 598
    .end local v0    # "am":Landroid/view/accessibility/AccessibilityManager;
    .end local v1    # "asList":Ljava/util/List;, "Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    .end local v2    # "i":I
    :cond_1
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v4}, Lcom/infraware/office/baseframe/EvBaseView;->clearFocus()V

    .line 599
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v4, v8}, Lcom/infraware/office/baseframe/EvBaseView;->setFocusable(Z)V

    .line 600
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v4, v8}, Lcom/infraware/office/baseframe/EvBaseView;->setFocusableInTouchMode(Z)V

    .line 601
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v4, v7}, Lcom/infraware/office/baseframe/EvBaseView;->setForceClear(Z)V

    .line 603
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "FT03"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 604
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->IsViewerMode()Z

    move-result v4

    if-nez v4, :cond_2

    .line 605
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v4

    invoke-virtual {v4, v8}, Lcom/infraware/office/baseframe/EvBaseView;->onShowIme(Z)V

    .line 606
    :cond_2
    if-eqz p1, :cond_3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mMemo_view:Lcom/infraware/polarisoffice5/common/MemoView;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mMemo_view:Lcom/infraware/polarisoffice5/common/MemoView;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/common/MemoView;->isShown()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 607
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onHideMemoView()Z

    .line 608
    :cond_3
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mHandler:Landroid/os/Handler;

    const-wide/16 v5, 0xa

    invoke-virtual {v4, v10, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 611
    :cond_4
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    if-nez v4, :cond_5

    .line 612
    new-instance v4, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-direct {v4, p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    .line 613
    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbReflowText:Z

    if-nez v4, :cond_5

    .line 614
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v4, v7}, Lcom/infraware/office/evengine/EvInterface;->ISetViewMode(I)V

    .line 617
    :cond_5
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v4}, Lcom/infraware/office/evengine/EvInterface;->IGetCaretInfo_Editor()Lcom/infraware/office/evengine/EV$CARET_INFO;

    move-result-object v4

    iget v3, v4, Lcom/infraware/office/evengine/EV$CARET_INFO;->bCaret:I

    .line 619
    .local v3, "nCaretMode":I
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 621
    if-eqz p1, :cond_7

    .line 622
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const v5, 0x7f070310

    invoke-virtual {v4, v5}, Lcom/infraware/office/actionbar/ActionTitleBar;->setTitle(I)V

    .line 623
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v4, v7}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->setFocusMode(Z)V

    .line 650
    :cond_6
    :goto_2
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v4}, Lcom/infraware/office/actionbar/MainActionBar;->hide()V

    .line 651
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Lcom/infraware/office/actionbar/ActionTitleBar;->setEvent(I)V

    .line 653
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v4}, Lcom/infraware/office/actionbar/ActionTitleBar;->show()V

    .line 654
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v4, v10}, Lcom/infraware/office/baseframe/EvBaseView;->setActionMode(I)V

    goto/16 :goto_1

    .line 625
    :cond_7
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const v5, 0x7f07023f

    invoke-virtual {v4, v5}, Lcom/infraware/office/actionbar/ActionTitleBar;->setTitle(I)V

    .line 626
    if-ne v3, v9, :cond_8

    .line 627
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v4, v11, v8}, Lcom/infraware/office/evengine/EvInterface;->ICaretMark(II)V

    goto :goto_2

    .line 628
    :cond_8
    if-eq v3, v7, :cond_6

    if-eq v3, v10, :cond_6

    if-eq v3, v9, :cond_6

    .line 629
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v4, v11, v8}, Lcom/infraware/office/evengine/EvInterface;->ICaretMark(II)V

    goto :goto_2

    .line 632
    :cond_9
    if-eqz p1, :cond_a

    .line 633
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const v5, 0x7f070310

    invoke-virtual {v4, v5}, Lcom/infraware/office/actionbar/ActionTitleBar;->setTitle(I)V

    .line 634
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v4, v7}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->setFocusMode(Z)V

    .line 635
    if-ne v3, v9, :cond_a

    .line 636
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v4, v7}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->initializeFocusTTS(Z)V

    .line 639
    :cond_a
    if-ne v3, v9, :cond_c

    .line 640
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v4, v11, v8}, Lcom/infraware/office/evengine/EvInterface;->ICaretMark(II)V

    .line 644
    :cond_b
    :goto_3
    if-nez p1, :cond_6

    .line 645
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const v5, 0x7f07023f

    invoke-virtual {v4, v5}, Lcom/infraware/office/actionbar/ActionTitleBar;->setTitle(I)V

    .line 646
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v4, v8}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->initializeWholeTTS(Z)V

    goto :goto_2

    .line 641
    :cond_c
    if-eq v3, v7, :cond_b

    if-eq v3, v10, :cond_b

    if-eq v3, v9, :cond_b

    .line 642
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v4, v11, v8}, Lcom/infraware/office/evengine/EvInterface;->ICaretMark(II)V

    goto :goto_3
.end method

.method private onPrevLineTTS()V
    .locals 1

    .prologue
    .line 760
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    if-eqz v0, :cond_0

    .line 761
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->prevLine()V

    .line 762
    :cond_0
    return-void
.end method

.method private onResultHeaderFooter(Landroid/content/Intent;)V
    .locals 0
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 316
    return-void
.end method

.method private onResultInsertBookmarkActivity(Landroid/content/Intent;)V
    .locals 4
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 334
    const-string/jumbo v2, "BOOKMARKMODE"

    const/4 v3, -0x1

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 335
    .local v0, "bookmarkMode":I
    const-string/jumbo v2, "BOOKMAKRNAME"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 336
    .local v1, "strBookmark":Ljava/lang/String;
    packed-switch v0, :pswitch_data_0

    .line 341
    :goto_0
    return-void

    .line 338
    :pswitch_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2, v0, v1}, Lcom/infraware/office/evengine/EvInterface;->IBookmarkEditor(ILjava/lang/String;)V

    goto :goto_0

    .line 336
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private onResumeTTS()V
    .locals 1

    .prologue
    .line 801
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    if-eqz v0, :cond_0

    .line 802
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->resume()V

    .line 803
    :cond_0
    return-void
.end method

.method private onShowMemoView()V
    .locals 4

    .prologue
    .line 395
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onHideMemoView()Z

    .line 399
    iget-object v2, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->getMemoCmdData()Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;

    move-result-object v0

    .line 400
    .local v0, "memo":Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v3, 0x7

    invoke-virtual {v2, v3, v0}, Lcom/infraware/office/evengine/EvInterface;->IMemoCommand(ILcom/infraware/office/evengine/EV$MEMO_CMD_DATA;)Z

    .line 401
    iget v1, v0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->nMemoId:I

    .line 402
    .local v1, "memo_id":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 404
    iget-object v2, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mMemo_view:Lcom/infraware/polarisoffice5/common/MemoView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/MemoView;->show()V

    .line 405
    iget-object v2, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mMemo_view:Lcom/infraware/polarisoffice5/common/MemoView;

    invoke-virtual {v2, v1}, Lcom/infraware/polarisoffice5/common/MemoView;->setActiveMemoId(I)V

    .line 411
    :cond_0
    return-void
.end method

.method private onUpdateViewDlg()V
    .locals 2

    .prologue
    .line 1304
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mDialog:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    if-eqz v0, :cond_0

    .line 1306
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mDialog:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    const-string/jumbo v1, "REFLOW_TEXT"

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->setEnableItem(Ljava/lang/String;)V

    .line 1308
    :cond_0
    return-void
.end method

.method private setNfcCallback()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1150
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1158
    :cond_0
    :goto_0
    return-void

    .line 1152
    :cond_1
    invoke-static {p0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 1153
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    if-eqz v0, :cond_0

    .line 1154
    new-instance v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$NfcCallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/infraware/polarisoffice5/word/WordEditorActivity$NfcCallback;-><init>(Lcom/infraware/polarisoffice5/word/WordEditorActivity;Lcom/infraware/polarisoffice5/word/WordEditorActivity$1;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/word/WordEditorActivity$NfcCallback;

    .line 1155
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/word/WordEditorActivity$NfcCallback;

    new-array v2, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p0, v2}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 1156
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/word/WordEditorActivity$NfcCallback;

    new-array v2, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p0, v2}, Landroid/nfc/NfcAdapter;->setOnNdefPushCompleteCallback(Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    goto :goto_0
.end method


# virtual methods
.method public ActivityMsgProc(IIIIILjava/lang/Object;)I
    .locals 7
    .param p1, "msg"    # I
    .param p2, "p1"    # I
    .param p3, "p2"    # I
    .param p4, "p3"    # I
    .param p5, "p4"    # I
    .param p6, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    .line 928
    sparse-switch p1, :sswitch_data_0

    .line 1033
    invoke-super/range {p0 .. p6}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    move-result v4

    .line 1035
    :goto_0
    return v4

    .line 930
    :sswitch_0
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v4, :cond_0

    .line 933
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v4

    iget-object v4, v4, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 934
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v4

    iget-object v4, v4, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismissAll()V

    .line 936
    :cond_0
    invoke-super/range {p0 .. p6}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    :cond_1
    :goto_1
    move v4, v5

    .line 1035
    goto :goto_0

    .line 939
    :sswitch_1
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    if-eqz v4, :cond_4

    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbReflowText:Z

    if-ne v4, v5, :cond_4

    .line 940
    const/4 v1, 0x0

    .local v1, "bBeforeLandScape":Z
    const/4 v0, 0x0

    .line 941
    .local v0, "bAfterLandScape":Z
    if-le p2, p3, :cond_2

    .line 942
    const/4 v1, 0x1

    .line 943
    :cond_2
    if-le p4, p5, :cond_3

    .line 944
    const/4 v0, 0x1

    .line 945
    :cond_3
    if-eq v1, v0, :cond_4

    .line 946
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onEndTextToSpeech()V

    .line 948
    .end local v0    # "bAfterLandScape":Z
    .end local v1    # "bBeforeLandScape":Z
    :cond_4
    invoke-super/range {p0 .. p6}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto :goto_1

    .line 951
    :sswitch_2
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 952
    .local v3, "rect":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 953
    const-string/jumbo v4, "window"

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    .line 956
    .local v2, "display":Landroid/view/Display;
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v4}, Lcom/infraware/office/baseframe/EvBaseView;->GetOpenType()I

    move-result v4

    if-ne v4, v5, :cond_5

    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mShowImeOfNewDocument:Z

    if-nez v4, :cond_5

    .line 958
    iget v4, v3, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v6

    if-eq v4, v6, :cond_5

    .line 959
    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mShowImeOfNewDocument:Z

    .line 960
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    const/16 v6, 0x213

    invoke-virtual {v4, v6}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 967
    :cond_5
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v4}, Lcom/infraware/office/baseframe/EvBaseView;->getActionMode()I

    move-result v4

    const/4 v6, 0x3

    if-ne v4, v6, :cond_6

    .line 968
    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbReflowText:Z

    if-nez v4, :cond_6

    .line 969
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v4, v5}, Lcom/infraware/office/evengine/EvInterface;->ISetViewMode(I)V

    .line 972
    :cond_6
    invoke-super/range {p0 .. p6}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto :goto_1

    .line 975
    .end local v2    # "display":Landroid/view/Display;
    .end local v3    # "rect":Landroid/graphics/Rect;
    :sswitch_3
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onShowMemoView()V

    goto :goto_1

    .line 978
    :sswitch_4
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onHideMemoView()Z

    goto :goto_1

    .line 989
    :sswitch_5
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mFreeDrawBar:Landroid/view/View;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mFreeDrawBar:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_1

    .line 992
    :cond_7
    invoke-static {p0}, Lcom/infraware/common/define/CMModelDefine$Diotek;->USE_DICTIONARY_SEARCH(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 996
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-nez v4, :cond_8

    .line 997
    new-instance v4, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-direct {v4, p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    goto/16 :goto_1

    .line 999
    :cond_8
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v4, :cond_1

    .line 1000
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v6}, Lcom/infraware/office/actionbar/MainActionBar;->getHeight()I

    move-result v6

    invoke-virtual {v4, v6}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setActionbarHeight(I)V

    goto/16 :goto_1

    .line 1002
    :cond_9
    invoke-static {p0}, Lcom/infraware/common/define/CMModelDefine$Diotek;->USE_DICTIONARY_SEARCH(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1003
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v4, p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->startService(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    goto/16 :goto_1

    .line 1010
    :sswitch_6
    invoke-direct {p0, v5}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onPlayTextToSpeech(Z)V

    goto/16 :goto_1

    .line 1013
    :sswitch_7
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getOrientation()I

    move-result v4

    const/4 v6, 0x2

    if-eq v4, v6, :cond_1

    .line 1015
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->isFreeDrawMode()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1016
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v4}, Lcom/infraware/office/actionbar/ActionTitleBar;->setFocus()V

    .line 1017
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v4}, Lcom/infraware/office/baseframe/EvBaseView;->requestLayout()V

    .line 1018
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v4}, Lcom/infraware/office/baseframe/EvBaseView;->clearFocus()V

    goto/16 :goto_1

    .line 1020
    :cond_a
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->isTableDrawMode()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 1021
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v4}, Lcom/infraware/office/actionbar/ActionTitleBar;->setFocus()V

    .line 1022
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v4}, Lcom/infraware/office/baseframe/EvBaseView;->requestLayout()V

    .line 1023
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v4}, Lcom/infraware/office/baseframe/EvBaseView;->clearFocus()V

    goto/16 :goto_1

    .line 1024
    :cond_b
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->isFindReplaceMode()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1026
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v4, :cond_1

    .line 1027
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v4}, Lcom/infraware/office/baseframe/EvBaseView;->requestLayout()V

    .line 1028
    iget-object v4, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v4}, Lcom/infraware/office/baseframe/EvBaseView;->clearFocus()V

    goto/16 :goto_1

    .line 928
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_5
        0x5 -> :sswitch_2
        0x9 -> :sswitch_0
        0xc -> :sswitch_1
        0xf -> :sswitch_3
        0x2a -> :sswitch_6
        0x31 -> :sswitch_7
        0x36 -> :sswitch_4
    .end sparse-switch
.end method

.method public OnActionBarEvent(I)V
    .locals 2
    .param p1, "eEventType"    # I

    .prologue
    .line 1115
    sparse-switch p1, :sswitch_data_0

    .line 1144
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnActionBarEvent(I)V

    .line 1147
    :goto_0
    return-void

    .line 1117
    :sswitch_0
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onOpenLayoutDlg()V

    goto :goto_0

    .line 1120
    :sswitch_1
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onInsertBookmarkActivity()V

    goto :goto_0

    .line 1123
    :sswitch_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->ISetViewMode(I)V

    goto :goto_0

    .line 1126
    :sswitch_3
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onPageLayoutActivity()V

    goto :goto_0

    .line 1129
    :sswitch_4
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onReflowText()V

    goto :goto_0

    .line 1132
    :sswitch_5
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onSetMemoSetting()V

    goto :goto_0

    .line 1135
    :sswitch_6
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onPlayTextToSpeech(Z)V

    goto :goto_0

    .line 1138
    :sswitch_7
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onEndTextToSpeech()V

    goto :goto_0

    .line 1141
    :sswitch_8
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onOpenViewDlg()V

    goto :goto_0

    .line 1115
    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_2
        0x7 -> :sswitch_6
        0x8 -> :sswitch_5
        0x9 -> :sswitch_3
        0x34 -> :sswitch_1
        0x38 -> :sswitch_4
        0x41 -> :sswitch_7
        0x77 -> :sswitch_8
        0x83 -> :sswitch_0
    .end sparse-switch
.end method

.method public OnGetRulerbarBitmap(II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 1429
    const/4 v0, 0x0

    return-object v0
.end method

.method public OnIMEInsertMode()V
    .locals 0

    .prologue
    .line 201
    return-void
.end method

.method public OnInsertTableMode()V
    .locals 0

    .prologue
    .line 197
    return-void
.end method

.method public OnLoadComplete(I)V
    .locals 2
    .param p1, "bBookmarkExist"    # I

    .prologue
    .line 1436
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getOrientation()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1437
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->ISetViewMode(I)V

    .line 1441
    :goto_0
    return-void

    .line 1439
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->ISetViewMode(I)V

    goto :goto_0
.end method

.method public OnOLEFormatInfo(Ljava/lang/String;)V
    .locals 0
    .param p1, "nWideString"    # Ljava/lang/String;

    .prologue
    .line 1447
    return-void
.end method

.method public OnObjectPoints(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V
    .locals 0
    .param p1, "param"    # Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;

    .prologue
    .line 191
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnObjectPoints(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V

    .line 192
    return-void
.end method

.method public OnPaperLayoutMode()V
    .locals 4

    .prologue
    .line 1201
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/infraware/polarisoffice5/word/WordEditorActivity$7;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity$7;-><init>(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1208
    return-void
.end method

.method public OnSpellCheck(Ljava/lang/String;IIIIIII)V
    .locals 0
    .param p1, "pWordStr"    # Ljava/lang/String;
    .param p2, "nLen"    # I
    .param p3, "bClass"    # I
    .param p4, "nPageNum"    # I
    .param p5, "nObjectID"    # I
    .param p6, "nNoteNum"    # I
    .param p7, "nParaIndex"    # I
    .param p8, "nColIndex"    # I

    .prologue
    .line 1424
    return-void
.end method

.method public OnTextToSpeachString(Ljava/lang/String;)V
    .locals 2
    .param p1, "strTTS"    # Ljava/lang/String;

    .prologue
    .line 456
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    if-eqz v0, :cond_0

    .line 461
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 462
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onTextToSpeachStringForCane(Ljava/lang/String;)V

    .line 471
    :cond_0
    :goto_0
    return-void

    .line 465
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0, p1}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->speechString(Ljava/lang/String;)V

    .line 467
    if-nez p1, :cond_0

    .line 468
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public OnTotalLoadComplete()V
    .locals 0

    .prologue
    .line 1298
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnTotalLoadComplete()V

    .line 1299
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onUpdateViewDlg()V

    .line 1300
    return-void
.end method

.method public OnWordCellDeleteMode()V
    .locals 0

    .prologue
    .line 210
    return-void
.end method

.method public OnWordCellInsertMode()V
    .locals 0

    .prologue
    .line 206
    return-void
.end method

.method public OnWordInsertStringMode()V
    .locals 0

    .prologue
    .line 214
    return-void
.end method

.method public OnWordMemoViewMode(Ljava/lang/String;I)V
    .locals 0
    .param p1, "strMemo"    # Ljava/lang/String;
    .param p2, "nExistDirection"    # I

    .prologue
    .line 241
    return-void
.end method

.method public OnWordMultiSelectCellMode()V
    .locals 0

    .prologue
    .line 222
    return-void
.end method

.method public OnWordOneSelectCellMode()V
    .locals 0

    .prologue
    .line 218
    return-void
.end method

.method public OnWordPageLayout()V
    .locals 0

    .prologue
    .line 226
    return-void
.end method

.method public enableTTSToolbar(Z)V
    .locals 4
    .param p1, "bEnable"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 555
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 580
    :goto_0
    return-void

    .line 558
    :cond_0
    if-eqz p1, :cond_2

    .line 560
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbIsTTSPlayFlag:Z

    if-eqz v0, :cond_1

    .line 562
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    const v1, 0x7f020036

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 563
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070068

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 570
    :goto_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 571
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_Prev:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 572
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_Next:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_0

    .line 567
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    const v1, 0x7f020037

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 568
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07006a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 576
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 577
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_Prev:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 578
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_Next:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_0
.end method

.method public getEvTextToSpeechHelper()Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    return-object v0
.end method

.method public getReflowText()Z
    .locals 1

    .prologue
    .line 359
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbReflowText:Z

    return v0
.end method

.method public getTTSToolbarPlayFlag()Z
    .locals 1

    .prologue
    .line 745
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbIsTTSPlayFlag:Z

    return v0
.end method

.method public hasComments()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IHasDocComments()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public headsetHook()V
    .locals 1

    .prologue
    .line 806
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbIsTTSPlayFlag:Z

    if-eqz v0, :cond_0

    .line 807
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onPauseTTSButton()V

    .line 810
    :goto_0
    return-void

    .line 809
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onPlayTTSButton()V

    goto :goto_0
.end method

.method public initGuideTextToSpeech()V
    .locals 3

    .prologue
    .line 516
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v0

    if-nez v0, :cond_1

    .line 529
    :cond_0
    :goto_0
    return-void

    .line 519
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    if-eqz v0, :cond_0

    .line 521
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->setGuideRequestFlag(Z)V

    .line 522
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 524
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    const v1, 0x7f020037

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 525
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07006a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 527
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->onStopForGuide()V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 256
    invoke-super {p0, p1, p2, p3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 257
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 258
    sparse-switch p1, :sswitch_data_0

    .line 269
    :cond_0
    :goto_0
    return-void

    .line 260
    :sswitch_0
    invoke-direct {p0, p3}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onResultInsertBookmarkActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 263
    :sswitch_1
    invoke-direct {p0, p3}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onResultHeaderFooter(Landroid/content/Intent;)V

    goto :goto_0

    .line 266
    :cond_1
    const/16 v0, 0x12

    if-ne p1, v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->show()V

    goto :goto_0

    .line 258
    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0x27 -> :sswitch_1
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1040
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    if-eqz v0, :cond_1

    .line 1041
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->finalizeSpeech()V

    .line 1042
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    .line 1056
    :cond_0
    :goto_0
    return-void

    .line 1046
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1047
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismissAll()V

    .line 1048
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->GetObjCtrlType()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->GetObjCtrlType()I

    move-result v0

    if-nez v0, :cond_0

    .line 1050
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0, v2}, Lcom/infraware/office/baseframe/EvBaseView;->onShowIme(Z)V

    goto :goto_0

    .line 1053
    :cond_3
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onHideMemoView()Z

    move-result v0

    if-eq v0, v2, :cond_0

    .line 1055
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onChangeScreen(I)V
    .locals 0
    .param p1, "nType"    # I

    .prologue
    .line 1385
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onChangeScreen(I)V

    .line 1386
    return-void
.end method

.method public onClickActionBar(I)V
    .locals 0
    .param p1, "nViewId"    # I

    .prologue
    .line 1103
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onClickActionBar(I)V

    .line 1104
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onHideMemoView()Z

    .line 1105
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 251
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x1

    .line 98
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/Window;->requestFeature(I)Z

    .line 99
    const v1, 0x7f03002b

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->setContentView(I)V

    .line 101
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onCreate(Landroid/os/Bundle;)V

    .line 102
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1, p0}, Lcom/infraware/office/baseframe/EvBaseView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 103
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->GetOpenType()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 104
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x14

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 109
    :cond_0
    const v1, 0x7f0b0132

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/MemoView;

    iput-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mMemo_view:Lcom/infraware/polarisoffice5/common/MemoView;

    .line 110
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mMemo_view:Lcom/infraware/polarisoffice5/common/MemoView;

    invoke-virtual {v1, p0}, Lcom/infraware/polarisoffice5/common/MemoView;->init(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)V

    .line 114
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 115
    invoke-static {}, Lcom/infraware/common/util/Utils;->isJB()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 116
    new-instance v1, Lcom/infraware/common/util/SbeamHelper;

    const-string/jumbo v2, "text/DirectSharePolarisEditor"

    invoke-direct {v1, p0, v2}, Lcom/infraware/common/util/SbeamHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    .line 117
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 118
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v1, "com.android.systemui.statusbar.COLLAPSED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 119
    new-instance v1, Lcom/infraware/polarisoffice5/word/WordEditorActivity$1;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity$1;-><init>(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 131
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 137
    .end local v0    # "intentFilter":Landroid/content/IntentFilter;
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->requestFocus()Z

    .line 138
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->requestFocusFromTouch()Z

    .line 139
    return-void

    .line 134
    :cond_2
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->setNfcCallback()V

    goto :goto_0
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    .line 246
    invoke-super {p0, p1, p2, p3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 247
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mMemo_view:Lcom/infraware/polarisoffice5/common/MemoView;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mMemo_view:Lcom/infraware/polarisoffice5/common/MemoView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/MemoView;->finallizeMemoView()V

    .line 149
    :cond_0
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onDestroy()V

    .line 152
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 153
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 154
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 156
    :cond_1
    return-void
.end method

.method public onEndTextToSpeech()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 659
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    if-eqz v0, :cond_0

    .line 660
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->finalizeSpeech()V

    .line 661
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    .line 664
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseView;->setFocusable(Z)V

    .line 665
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseView;->setFocusableInTouchMode(Z)V

    .line 666
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->requestFocus()Z

    .line 668
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->hide()V

    .line 669
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v0, :cond_1

    .line 670
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->show()V

    .line 672
    :cond_1
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 673
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->enableTTSToolbar(Z)V

    .line 675
    :cond_2
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "FT03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 676
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onHideTTSToolbar()V

    .line 678
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseView;->setActionMode(I)V

    .line 679
    return-void
.end method

.method public onGuideStartDelayTextToSpeech()V
    .locals 4

    .prologue
    .line 546
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    if-eqz v0, :cond_0

    .line 549
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 551
    :cond_0
    return-void
.end method

.method public onGuideStartTextToSpeech()V
    .locals 3

    .prologue
    .line 533
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v0

    if-nez v0, :cond_1

    .line 543
    :cond_0
    :goto_0
    return-void

    .line 537
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    if-eqz v0, :cond_0

    .line 538
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->setGuideRequestFlag(Z)V

    .line 539
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    const v1, 0x7f020036

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 540
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070068

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 541
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->onGuideStart()V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x0

    .line 837
    sparse-switch p1, :sswitch_data_0

    .line 854
    invoke-super {p0, p1, p2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    .line 842
    :sswitch_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->getActionMode()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 843
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1, v0}, Lcom/infraware/office/baseframe/EvBaseView;->setFocusable(Z)V

    .line 844
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 845
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->requestFocus()Z

    goto :goto_0

    .line 852
    :sswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 837
    nop

    :sswitch_data_0
    .sparse-switch
        0x14 -> :sswitch_0
        0x4f -> :sswitch_1
        0x7e -> :sswitch_1
        0x7f -> :sswitch_1
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 823
    sparse-switch p1, :sswitch_data_0

    .line 832
    invoke-super {p0, p1, p2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 828
    :sswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->getActionMode()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 829
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->headsetHook()V

    .line 830
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 823
    :sswitch_data_0
    .sparse-switch
        0x4f -> :sswitch_0
        0x7e -> :sswitch_0
        0x7f -> :sswitch_0
    .end sparse-switch
.end method

.method public onLocaleChange(I)V
    .locals 2
    .param p1, "nLocale"    # I

    .prologue
    .line 180
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onLocaleChange(I)V

    .line 183
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTSBar:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_Next:Landroid/widget/ImageView;

    const v1, 0x7f070067

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 185
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_Prev:Landroid/widget/ImageView;

    const v1, 0x7f070069

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 187
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 1060
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onPause()V

    .line 1061
    return-void
.end method

.method public onPauseTTSButton()V
    .locals 2

    .prologue
    .line 765
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->getActionMode()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 767
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onPauseTTS()V

    .line 769
    :cond_0
    return-void
.end method

.method public onPauseTextToSpeech()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 683
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    if-eqz v0, :cond_0

    .line 685
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0, v3}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->stop(Z)I

    move-result v0

    if-nez v0, :cond_1

    .line 687
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    const v1, 0x7f020037

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 688
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07006a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 689
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbIsTTSPlayFlag:Z

    .line 698
    :cond_0
    :goto_0
    return-void

    .line 693
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    const v1, 0x7f020036

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 694
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070068

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 695
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbIsTTSPlayFlag:Z

    goto :goto_0
.end method

.method public onPlayTTSButton()V
    .locals 3

    .prologue
    .line 781
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->getActionMode()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 783
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    const v1, 0x7f020036

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 784
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mTTS_play_pause:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070068

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 785
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbIsTTSPlayFlag:Z

    .line 786
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 788
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->getGuideSpokenFlag()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->getReqStrAfterGuideFlag()Z

    move-result v0

    if-nez v0, :cond_1

    .line 789
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->resumeForGuide()V

    .line 796
    :cond_0
    :goto_0
    return-void

    .line 791
    :cond_1
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onResumeTTS()V

    goto :goto_0

    .line 794
    :cond_2
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onResumeTTS()V

    goto :goto_0
.end method

.method public onRefNote(I)V
    .locals 4
    .param p1, "nRefType"    # I

    .prologue
    .line 1406
    if-eqz p1, :cond_0

    const/4 v1, 0x3

    if-ne p1, v1, :cond_1

    .line 1407
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 1408
    .local v0, "hanlder":Landroid/os/Handler;
    if-eqz v0, :cond_1

    .line 1409
    new-instance v1, Lcom/infraware/polarisoffice5/word/WordEditorActivity$9;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity$9;-><init>(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1416
    .end local v0    # "hanlder":Landroid/os/Handler;
    :cond_1
    return-void
.end method

.method public onReflowText()V
    .locals 1

    .prologue
    .line 344
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbReflowText:Z

    if-eqz v0, :cond_0

    .line 345
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbReflowText:Z

    .line 346
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISetPrintMode()V

    .line 351
    :goto_0
    return-void

    .line 348
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbReflowText:Z

    .line 349
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISetWebMode()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1083
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onResume()V

    .line 1086
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mIsViewerMode:Z

    if-nez v1, :cond_0

    .line 1087
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x4

    invoke-virtual {v1, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->ICaretMark(II)V

    .line 1088
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v1

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1089
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v1

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismissAll()V

    .line 1093
    :cond_0
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1094
    invoke-static {}, Lcom/infraware/common/util/Utils;->isJB()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1095
    const/4 v1, 0x1

    new-array v0, v1, [Landroid/net/Uri;

    .line 1096
    .local v0, "filePath":[Landroid/net/Uri;
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    aput-object v1, v0, v3

    .line 1097
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v0, p0, v2}, Lcom/infraware/common/util/SbeamHelper;->setBeamUris([Landroid/net/Uri;Landroid/app/Activity;Landroid/content/Context;)V

    .line 1100
    .end local v0    # "filePath":[Landroid/net/Uri;
    :cond_1
    return-void
.end method

.method public onSetMemoSetting()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 372
    iget-object v3, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->getMemoCmdData()Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;

    move-result-object v0

    .line 374
    .local v0, "memo":Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    iget-object v3, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3, v1, v0}, Lcom/infraware/office/evengine/EvInterface;->IMemoCommand(ILcom/infraware/office/evengine/EV$MEMO_CMD_DATA;)Z

    .line 377
    iget-boolean v3, v0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->bShow:Z

    if-eqz v3, :cond_0

    :goto_0
    iput-boolean v1, v0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->bShow:Z

    .line 378
    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1, v2, v0}, Lcom/infraware/office/evengine/EvInterface;->IMemoCommand(ILcom/infraware/office/evengine/EV$MEMO_CMD_DATA;)Z

    .line 380
    iget-boolean v1, v0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->bShow:Z

    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mIsMemoViewMode:Z

    .line 391
    return-void

    :cond_0
    move v1, v2

    .line 377
    goto :goto_0
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 1066
    const-string/jumbo v0, "power"

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1069
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->getActionMode()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 1071
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onEndTextToSpeech()V

    .line 1073
    :cond_0
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onStop()V

    .line 1074
    return-void
.end method

.method public onTextToSpeachStringForCane(Ljava/lang/String;)V
    .locals 2
    .param p1, "strTTS"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 475
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->getGuideRequestFlag()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 477
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->getGuideSpokenFlag()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->getStrSpokenAfterGuide()Z

    move-result v0

    if-nez v0, :cond_0

    .line 478
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->onStopForGuide()V

    .line 479
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->speechString(Ljava/lang/String;)V

    .line 512
    :cond_0
    :goto_0
    return-void

    .line 484
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->getReqGuideSpeakFlag()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->getGuideSpokenFlag()Z

    move-result v0

    if-nez v0, :cond_2

    .line 486
    if-eqz p1, :cond_0

    .line 487
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0, p1}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->setReadString(Ljava/lang/String;)V

    goto :goto_0

    .line 489
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->getGuideSpokenFlag()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->getStrSpokenAfterGuide()Z

    move-result v0

    if-nez v0, :cond_5

    .line 491
    if-eqz p1, :cond_4

    .line 492
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getTTSToolbarPlayFlag()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 493
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0, p1}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->setSpeakStringAfterScroll(Ljava/lang/String;)V

    goto :goto_0

    .line 496
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0, p1}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->setReadString(Ljava/lang/String;)V

    .line 497
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->enableTTSToolbar(Z)V

    goto :goto_0

    .line 500
    :cond_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0, p1}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->speechString(Ljava/lang/String;)V

    .line 501
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 506
    :cond_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvTextToSpeechHelper:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0, p1}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->speechString(Ljava/lang/String;)V

    .line 508
    if-nez p1, :cond_0

    .line 509
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public setEvListener()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 174
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p0

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->ISetListener(Lcom/infraware/office/evengine/EvListener$ViewerListener;Lcom/infraware/office/evengine/EvListener$EditorListener;Lcom/infraware/office/evengine/EvListener$WordEditorListener;Lcom/infraware/office/evengine/EvListener$PptEditorListener;Lcom/infraware/office/evengine/EvListener$SheetEditorListener;Lcom/infraware/office/evengine/EvListener$PdfViewerListener;)V

    .line 176
    :cond_0
    return-void
.end method
