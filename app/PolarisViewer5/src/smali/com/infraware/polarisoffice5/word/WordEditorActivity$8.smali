.class Lcom/infraware/polarisoffice5/word/WordEditorActivity$8;
.super Landroid/os/Handler;
.source "WordEditorActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/word/WordEditorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)V
    .locals 0

    .prologue
    .line 1310
    iput-object p1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 1313
    iget v7, p1, Landroid/os/Message;->what:I

    packed-switch v7, :pswitch_data_0

    .line 1374
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1316
    :pswitch_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 1318
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v7, "MEMO"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 1319
    .local v1, "isMemoViewChecked":Ljava/lang/Boolean;
    const-string/jumbo v7, "RULER"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    .line 1320
    .local v4, "isRulerChecked":Ljava/lang/Boolean;
    const-string/jumbo v7, "SPELL_CHECK"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    .line 1321
    .local v5, "isSpellChecked":Ljava/lang/Boolean;
    const-string/jumbo v7, "NO_MARGIN_VIEW"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    .line 1322
    .local v2, "isNoMarginChecked":Ljava/lang/Boolean;
    const-string/jumbo v7, "REFLOW_TEXT"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    .line 1324
    .local v3, "isReflowTextChecked":Ljava/lang/Boolean;
    if-eqz v1, :cond_1

    .line 1325
    iget-object v7, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    iget-object v7, v7, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v7}, Lcom/infraware/office/evengine/EvInterface;->getMemoCmdData()Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;

    move-result-object v6

    .line 1326
    .local v6, "memo":Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    invoke-virtual {v6}, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->clear()V

    .line 1327
    iget-object v7, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    iget-object v7, v7, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v7, v8, v6}, Lcom/infraware/office/evengine/EvInterface;->IMemoCommand(ILcom/infraware/office/evengine/EV$MEMO_CMD_DATA;)Z

    .line 1329
    iget-boolean v7, v6, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->bShow:Z

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eq v7, v8, :cond_1

    .line 1330
    iget-object v7, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->OnActionBarEvent(I)V

    .line 1334
    .end local v6    # "memo":Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    :cond_1
    if-eqz v2, :cond_3

    iget-object v7, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbNoMargin:Z
    invoke-static {v7}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$1100(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Z

    move-result v7

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eq v7, v8, :cond_3

    .line 1336
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-ne v7, v9, :cond_2

    iget-object v7, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbReflowText:Z
    invoke-static {v7}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$1200(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Z

    move-result v7

    if-ne v7, v9, :cond_2

    .line 1337
    iget-object v7, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onReflowText()V

    .line 1340
    :cond_2
    iget-object v7, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    # invokes: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onNomarginMode()V
    invoke-static {v7}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$1300(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)V

    .line 1343
    :cond_3
    if-eqz v3, :cond_0

    iget-object v7, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbReflowText:Z
    invoke-static {v7}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$1200(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Z

    move-result v7

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eq v7, v8, :cond_0

    .line 1345
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-ne v7, v9, :cond_4

    iget-object v7, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mbNoMargin:Z
    invoke-static {v7}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$1100(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)Z

    move-result v7

    if-ne v7, v9, :cond_4

    .line 1346
    iget-object v7, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    # invokes: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onNomarginMode()V
    invoke-static {v7}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$1300(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)V

    .line 1349
    :cond_4
    iget-object v7, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onReflowText()V

    goto/16 :goto_0

    .line 1355
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "isMemoViewChecked":Ljava/lang/Boolean;
    .end local v2    # "isNoMarginChecked":Ljava/lang/Boolean;
    .end local v3    # "isReflowTextChecked":Ljava/lang/Boolean;
    .end local v4    # "isRulerChecked":Ljava/lang/Boolean;
    .end local v5    # "isSpellChecked":Ljava/lang/Boolean;
    :pswitch_2
    iget v7, p1, Landroid/os/Message;->arg1:I

    packed-switch v7, :pswitch_data_1

    goto/16 :goto_0

    .line 1360
    :pswitch_3
    iget-object v7, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    const/16 v8, 0x10

    invoke-virtual {v7, v8}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->OnActionBarEvent(I)V

    goto/16 :goto_0

    .line 1363
    :pswitch_4
    iget-object v7, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    # invokes: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onInsertBookmarkActivity()V
    invoke-static {v7}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$1400(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)V

    goto/16 :goto_0

    .line 1366
    :pswitch_5
    iget-object v7, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    # invokes: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->OnRefNote(I)V
    invoke-static {v7, v8}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$1500(Lcom/infraware/polarisoffice5/word/WordEditorActivity;I)V

    goto/16 :goto_0

    .line 1369
    :pswitch_6
    iget-object v7, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    const/4 v8, 0x3

    # invokes: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->OnRefNote(I)V
    invoke-static {v7, v8}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$1500(Lcom/infraware/polarisoffice5/word/WordEditorActivity;I)V

    goto/16 :goto_0

    .line 1313
    :pswitch_data_0
    .packed-switch 0x2e
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 1355
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
