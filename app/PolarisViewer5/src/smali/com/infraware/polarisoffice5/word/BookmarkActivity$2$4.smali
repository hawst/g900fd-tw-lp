.class Lcom/infraware/polarisoffice5/word/BookmarkActivity$2$4;
.super Ljava/lang/Object;
.source "BookmarkActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2$4;->this$1:Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 149
    const-string/jumbo v0, "BookmarkActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "messageHandler  BtnPos= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2$4;->this$1:Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mBtnPos:I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->access$100(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2$4;->this$1:Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mSrcListItem:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2$4;->this$1:Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mBtnPos:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->access$100(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 152
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2$4;->this$1:Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->access$200(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2$4;->this$1:Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->access$200(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v1

    const/4 v2, 0x2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2$4;->this$1:Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mSrcListItem:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2$4;->this$1:Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mBtnPos:I
    invoke-static {v3}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->access$100(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/MultiListItem;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/MultiListItem;->getText1()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/infraware/office/evengine/EvInterface;->IBookmarkEditor(ILjava/lang/String;)V

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2$4;->this$1:Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mSrcListItem:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 157
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2$4;->this$1:Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/BookmarkActivity;->m_bookmarkCallback:Lcom/infraware/polarisoffice5/word/BookmarkActivity$BookmarkCallback;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->access$300(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)Lcom/infraware/polarisoffice5/word/BookmarkActivity$BookmarkCallback;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 158
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2$4;->this$1:Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/BookmarkActivity;->m_bookmarkCallback:Lcom/infraware/polarisoffice5/word/BookmarkActivity$BookmarkCallback;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->access$300(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)Lcom/infraware/polarisoffice5/word/BookmarkActivity$BookmarkCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2$4;->this$1:Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mBtnPos:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->access$100(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/infraware/polarisoffice5/word/BookmarkActivity$BookmarkCallback;->onDeleteButtonClicked(I)V

    .line 164
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2$4;->this$1:Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    # getter for: Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mAdapter:Lcom/infraware/polarisoffice5/common/MultiAdapter;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->access$500(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)Lcom/infraware/polarisoffice5/common/MultiAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/MultiAdapter;->notifyDataSetChanged()V

    .line 166
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2$4;->this$1:Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    const/4 v1, 0x0

    # setter for: Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mbClick:Z
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->access$002(Lcom/infraware/polarisoffice5/word/BookmarkActivity;Z)Z

    .line 167
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 168
    return-void

    .line 160
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2$4;->this$1:Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    # invokes: Lcom/infraware/polarisoffice5/word/BookmarkActivity;->initBookmark()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->access$400(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)V

    .line 161
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2$4;->this$1:Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$2;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->initLayout()V

    goto :goto_0
.end method
