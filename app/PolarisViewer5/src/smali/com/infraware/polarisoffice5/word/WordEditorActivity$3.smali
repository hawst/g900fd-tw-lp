.class Lcom/infraware/polarisoffice5/word/WordEditorActivity$3;
.super Landroid/os/Handler;
.source "WordEditorActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/word/WordEditorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)V
    .locals 0

    .prologue
    .line 428
    iput-object p1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$3;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 435
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 450
    :goto_0
    return-void

    .line 438
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$3;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onEndTextToSpeech()V

    goto :goto_0

    .line 441
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$3;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onPauseTextToSpeech()V

    goto :goto_0

    .line 444
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$3;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    # invokes: Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onInitTTSToolbar()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->access$200(Lcom/infraware/polarisoffice5/word/WordEditorActivity;)V

    goto :goto_0

    .line 447
    :pswitch_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity$3;->this$0:Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onGuideStartTextToSpeech()V

    goto :goto_0

    .line 435
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
