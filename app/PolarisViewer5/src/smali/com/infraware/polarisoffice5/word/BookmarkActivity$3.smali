.class Lcom/infraware/polarisoffice5/word/BookmarkActivity$3;
.super Ljava/lang/Object;
.source "BookmarkActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/word/BookmarkActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/word/BookmarkActivity;)V
    .locals 0

    .prologue
    .line 244
    iput-object p1, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$3;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 246
    .local p1, "a":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v2, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$3;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$3;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mAddBookmarkEdit:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/office/util/EvUtil;->hideIme(Landroid/content/Context;Landroid/os/IBinder;)V

    .line 247
    iget-object v2, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$3;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->mSrcListItem:Ljava/util/ArrayList;

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/MultiListItem;

    .line 248
    .local v1, "item":Lcom/infraware/polarisoffice5/common/MultiListItem;
    const-string/jumbo v2, "BookmarkActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "List Click position  = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "String = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/MultiListItem;->getText1()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    iget-object v2, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$3;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 250
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "BOOKMARKMODE"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 251
    const-string/jumbo v2, "BOOKMAKRNAME"

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/MultiListItem;->getText1()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 252
    iget-object v2, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$3;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    const/4 v3, -0x1

    invoke-virtual {v2, v3, v0}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->setResult(ILandroid/content/Intent;)V

    .line 253
    iget-object v2, p0, Lcom/infraware/polarisoffice5/word/BookmarkActivity$3;->this$0:Lcom/infraware/polarisoffice5/word/BookmarkActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/word/BookmarkActivity;->finish()V

    .line 254
    return-void
.end method
