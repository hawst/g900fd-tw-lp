.class public Lcom/infraware/office/evengine/EV$CARET_INFO;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CARET_INFO"
.end annotation


# instance fields
.field public bCaret:I

.field public bOnlyCaretMove:I

.field public nDirection:I

.field public nFrameType:I

.field public nHeight:I

.field public nType:I

.field public nWidth:I

.field public nX:I

.field public nY:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 901
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$CARET_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 914
    iput v0, p0, Lcom/infraware/office/evengine/EV$CARET_INFO;->bOnlyCaretMove:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$CARET_INFO;->bCaret:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$CARET_INFO;->nHeight:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$CARET_INFO;->nWidth:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$CARET_INFO;->nY:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$CARET_INFO;->nX:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$CARET_INFO;->nType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$CARET_INFO;->nFrameType:I

    .line 915
    iput v0, p0, Lcom/infraware/office/evengine/EV$CARET_INFO;->nDirection:I

    .line 916
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$CARET_INFO;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 919
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$CARET_INFO;

    .line 920
    .local v0, "o":Lcom/infraware/office/evengine/EV$CARET_INFO;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 901
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$CARET_INFO;->clone()Lcom/infraware/office/evengine/EV$CARET_INFO;

    move-result-object v0

    return-object v0
.end method
