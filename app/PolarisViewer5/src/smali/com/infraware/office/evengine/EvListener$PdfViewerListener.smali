.class public interface abstract Lcom/infraware/office/evengine/EvListener$PdfViewerListener;
.super Ljava/lang/Object;
.source "EvListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EvListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PdfViewerListener"
.end annotation


# virtual methods
.method public abstract OnEditCopyCut(IILjava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract OnPDFHyperLink(Ljava/lang/String;IIII)V
.end method

.method public abstract OnPDFPageRendered(I)V
.end method

.method public abstract OnPDFScreenOffset(II)V
.end method

.method public abstract OnReceiveAnnot()V
.end method

.method public abstract OnReceiveNextAnnot(IIIILjava/lang/String;FFFFIII)V
.end method

.method public abstract OnReceivePrevAnnot(IIIILjava/lang/String;FFFFIII)V
.end method

.method public abstract OnSaveDoc(I)V
.end method

.method public abstract OnSelectAnnots(IIII)V
.end method

.method public abstract OnSetCurrAnnot(IIIIFFFFLjava/lang/String;IIII)V
.end method

.method public abstract OnSetNextAnnot(IIIIFFFFLjava/lang/String;IIII)V
.end method

.method public abstract OnSetPrevAnnot(IIIIFFFFLjava/lang/String;IIII)V
.end method

.method public abstract OnSingleTap(II)V
.end method

.method public abstract printAnnot()V
.end method
