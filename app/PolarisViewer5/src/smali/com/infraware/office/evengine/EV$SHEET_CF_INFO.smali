.class public Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SHEET_CF_INFO"
.end annotation


# instance fields
.field public nAverageValue:F

.field public nDeletedRuleIndex:I

.field public nRuleType:I

.field public nRuleValueLen:I

.field public szCFRuleValue:Ljava/lang/String;

.field public tCFRuleRange:Lcom/infraware/office/evengine/EV$RANGE;

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 2

    .prologue
    .line 1328
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1330
    new-instance v0, Lcom/infraware/office/evengine/EV$RANGE;

    iget-object v1, p0, Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {v0, v1}, Lcom/infraware/office/evengine/EV$RANGE;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;->tCFRuleRange:Lcom/infraware/office/evengine/EV$RANGE;

    .line 1335
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;->szCFRuleValue:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 1338
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;->szCFRuleValue:Ljava/lang/String;

    .line 1339
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;->tCFRuleRange:Lcom/infraware/office/evengine/EV$RANGE;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$RANGE;->clear()V

    .line 1340
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;->nRuleType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;->nRuleValueLen:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;->nDeletedRuleIndex:I

    int-to-float v0, v0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;->nAverageValue:F

    .line 1341
    return-void
.end method

.method protected clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1344
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;

    .line 1345
    .local v0, "o":Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;
    return-object v0
.end method
