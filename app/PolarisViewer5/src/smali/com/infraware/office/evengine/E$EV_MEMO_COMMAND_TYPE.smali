.class public interface abstract Lcom/infraware/office/evengine/E$EV_MEMO_COMMAND_TYPE;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_MEMO_COMMAND_TYPE"
.end annotation


# static fields
.field public static final BR_MEMOCMD_ADD:I = 0x2

.field public static final BR_MEMOCMD_DELETE:I = 0x4

.field public static final BR_MEMOCMD_GETACTIVEID:I = 0x8

.field public static final BR_MEMOCMD_GETFIRSTID:I = 0xd

.field public static final BR_MEMOCMD_GETID:I = 0x7

.field public static final BR_MEMOCMD_GETLASTID:I = 0xe

.field public static final BR_MEMOCMD_GETNEXT:I = 0xb

.field public static final BR_MEMOCMD_GETPREV:I = 0xa

.field public static final BR_MEMOCMD_GETSTRING:I = 0x6

.field public static final BR_MEMOCMD_GOTOMEMO:I = 0xc

.field public static final BR_MEMOCMD_ISSHOW:I = 0x0

.field public static final BR_MEMOCMD_SETACTIVEID:I = 0x9

.field public static final BR_MEMOCMD_SETSHOW:I = 0x1

.field public static final BR_MEMOCMD_SETSTRING:I = 0x3
