.class Lcom/infraware/office/evengine/EvOpenObj;
.super Lcom/infraware/office/evengine/EvTaskObj;
.source "EvTaskObj.java"


# instance fields
.field private m_bLandScape:I

.field private m_eLoadType:I

.field private m_nHeight:I

.field private m_nLocale:I

.field private m_nOpenPageNum:I

.field private m_nOpenRotateAngle:I

.field private m_nOpenScale:I

.field private m_nOpenStartX:I

.field private m_nOpenStartY:I

.field private m_nPageBgColor:I

.field private m_nTextColor:I

.field private m_nWidth:I

.field private m_sBookMarkPath:Ljava/lang/String;

.field private m_sFilePath:Ljava/lang/String;

.field private m_sTempPath:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/infraware/office/evengine/EvNative;Ljava/lang/String;IIIIIIIIIIIILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "a_Native"    # Lcom/infraware/office/evengine/EvNative;
    .param p2, "a_sFilePath"    # Ljava/lang/String;
    .param p3, "a_nWidth"    # I
    .param p4, "a_nHeight"    # I
    .param p5, "a_eLoadType"    # I
    .param p6, "a_nPageBgColor"    # I
    .param p7, "a_nTextColor"    # I
    .param p8, "a_nOpenPageNum"    # I
    .param p9, "a_nOpenScale"    # I
    .param p10, "a_nOpenRotateAngle"    # I
    .param p11, "a_nOpenStartX"    # I
    .param p12, "a_nOpenStartY"    # I
    .param p13, "a_nLocale"    # I
    .param p14, "a_bLandScape"    # I
    .param p15, "a_sTempPath"    # Ljava/lang/String;
    .param p16, "a_sBookMarkPath"    # Ljava/lang/String;

    .prologue
    .line 108
    invoke-direct {p0, p1}, Lcom/infraware/office/evengine/EvTaskObj;-><init>(Lcom/infraware/office/evengine/EvNative;)V

    .line 109
    iput-object p2, p0, Lcom/infraware/office/evengine/EvOpenObj;->m_sFilePath:Ljava/lang/String;

    .line 110
    iput p3, p0, Lcom/infraware/office/evengine/EvOpenObj;->m_nWidth:I

    .line 111
    iput p4, p0, Lcom/infraware/office/evengine/EvOpenObj;->m_nHeight:I

    .line 112
    iput p5, p0, Lcom/infraware/office/evengine/EvOpenObj;->m_eLoadType:I

    .line 113
    iput p6, p0, Lcom/infraware/office/evengine/EvOpenObj;->m_nPageBgColor:I

    .line 114
    iput p7, p0, Lcom/infraware/office/evengine/EvOpenObj;->m_nTextColor:I

    .line 115
    iput p8, p0, Lcom/infraware/office/evengine/EvOpenObj;->m_nOpenPageNum:I

    .line 116
    iput p10, p0, Lcom/infraware/office/evengine/EvOpenObj;->m_nOpenScale:I

    .line 117
    iput p11, p0, Lcom/infraware/office/evengine/EvOpenObj;->m_nOpenStartX:I

    .line 118
    iput p12, p0, Lcom/infraware/office/evengine/EvOpenObj;->m_nOpenStartY:I

    .line 119
    iput p13, p0, Lcom/infraware/office/evengine/EvOpenObj;->m_nLocale:I

    .line 120
    iput p14, p0, Lcom/infraware/office/evengine/EvOpenObj;->m_bLandScape:I

    .line 121
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/infraware/office/evengine/EvOpenObj;->m_sTempPath:Ljava/lang/String;

    .line 122
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/infraware/office/evengine/EvOpenObj;->m_sBookMarkPath:Ljava/lang/String;

    .line 123
    return-void
.end method


# virtual methods
.method doTask()V
    .locals 17

    .prologue
    .line 127
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/evengine/EvOpenObj;->Native:Lcom/infraware/office/evengine/EvNative;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/evengine/EvOpenObj;->m_sFilePath:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/infraware/office/evengine/EvOpenObj;->m_nWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/infraware/office/evengine/EvOpenObj;->m_nHeight:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/infraware/office/evengine/EvOpenObj;->m_eLoadType:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/infraware/office/evengine/EvOpenObj;->m_nPageBgColor:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/infraware/office/evengine/EvOpenObj;->m_nTextColor:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/infraware/office/evengine/EvOpenObj;->m_nOpenPageNum:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/infraware/office/evengine/EvOpenObj;->m_nOpenScale:I

    move-object/from16 v0, p0

    iget v10, v0, Lcom/infraware/office/evengine/EvOpenObj;->m_nOpenRotateAngle:I

    move-object/from16 v0, p0

    iget v11, v0, Lcom/infraware/office/evengine/EvOpenObj;->m_nOpenStartX:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/infraware/office/evengine/EvOpenObj;->m_nOpenStartY:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/infraware/office/evengine/EvOpenObj;->m_nLocale:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/infraware/office/evengine/EvOpenObj;->m_bLandScape:I

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/office/evengine/EvOpenObj;->m_sTempPath:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/evengine/EvOpenObj;->m_sBookMarkPath:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v1 .. v16}, Lcom/infraware/office/evengine/EvNative;->IOpen(Ljava/lang/String;IIIIIIIIIIIILjava/lang/String;Ljava/lang/String;)V

    .line 142
    return-void
.end method
