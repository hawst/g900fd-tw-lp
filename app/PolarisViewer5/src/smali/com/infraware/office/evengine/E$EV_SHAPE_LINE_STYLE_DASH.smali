.class public interface abstract Lcom/infraware/office/evengine/E$EV_SHAPE_LINE_STYLE_DASH;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_SHAPE_LINE_STYLE_DASH"
.end annotation


# static fields
.field public static final BR_LINE_STYLE_DASH_DASHES:I = 0x4

.field public static final BR_LINE_STYLE_DASH_DASH_DOT:I = 0x5

.field public static final BR_LINE_STYLE_DASH_DASH_LONG_LINE:I = 0x6

.field public static final BR_LINE_STYLE_DASH_INVALID:I = 0x0

.field public static final BR_LINE_STYLE_DASH_LONG_DASH_DOT:I = 0x7

.field public static final BR_LINE_STYLE_DASH_LONG_DASH_DOT_DOT:I = 0x8

.field public static final BR_LINE_STYLE_DASH_ROUND_DOT:I = 0x2

.field public static final BR_LINE_STYLE_DASH_SOLID:I = 0x1

.field public static final BR_LINE_STYLE_DASH_SQUARE_DOT:I = 0x3
