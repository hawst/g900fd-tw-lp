.class public Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SHAPE_LINE_COLOR"
.end annotation


# instance fields
.field public nGradientAngle:I

.field public nGradientDirection:I

.field public nGradientPresetColors:I

.field public nGradientStopNumbers:I

.field public nGradientType:I

.field public nLineColorSelector:I

.field public nSolidColor:J

.field public nSolidTransparency:I

.field stGradientStop:[Lcom/infraware/office/evengine/EV$SHAPE_GRADIENT_STOP;

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 1

    .prologue
    .line 2389
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2403
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/infraware/office/evengine/EV$SHAPE_GRADIENT_STOP;

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;->stGradientStop:[Lcom/infraware/office/evengine/EV$SHAPE_GRADIENT_STOP;

    return-void
.end method


# virtual methods
.method clear()V
    .locals 5

    .prologue
    .line 2406
    const/4 v4, 0x0

    iput v4, p0, Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;->nGradientStopNumbers:I

    iput v4, p0, Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;->nGradientAngle:I

    iput v4, p0, Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;->nGradientDirection:I

    iput v4, p0, Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;->nGradientType:I

    iput v4, p0, Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;->nGradientPresetColors:I

    iput v4, p0, Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;->nSolidTransparency:I

    iput v4, p0, Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;->nLineColorSelector:I

    .line 2408
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;->stGradientStop:[Lcom/infraware/office/evengine/EV$SHAPE_GRADIENT_STOP;

    .local v0, "arr$":[Lcom/infraware/office/evengine/EV$SHAPE_GRADIENT_STOP;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 2409
    .local v1, "gradientStop":Lcom/infraware/office/evengine/EV$SHAPE_GRADIENT_STOP;
    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV$SHAPE_GRADIENT_STOP;->clear()V

    .line 2408
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2410
    .end local v1    # "gradientStop":Lcom/infraware/office/evengine/EV$SHAPE_GRADIENT_STOP;
    :cond_0
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2414
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;

    .line 2415
    .local v0, "o":Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2389
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;->clone()Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;

    move-result-object v0

    return-object v0
.end method
