.class public Lcom/infraware/office/evengine/EV$CONFIG_INFO$SCROLLINFO;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV$CONFIG_INFO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SCROLLINFO"
.end annotation


# instance fields
.field public possibleScrollDown:Z

.field public possibleScrollLeft:Z

.field public possibleScrollNextPage:Z

.field public possibleScrollPrevPage:Z

.field public possibleScrollRight:Z

.field public possibleScrollUp:Z

.field final synthetic this$1:Lcom/infraware/office/evengine/EV$CONFIG_INFO;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV$CONFIG_INFO;)V
    .locals 0

    .prologue
    .line 591
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$CONFIG_INFO$SCROLLINFO;->this$1:Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 600
    iput-boolean v0, p0, Lcom/infraware/office/evengine/EV$CONFIG_INFO$SCROLLINFO;->possibleScrollRight:Z

    iput-boolean v0, p0, Lcom/infraware/office/evengine/EV$CONFIG_INFO$SCROLLINFO;->possibleScrollUp:Z

    iput-boolean v0, p0, Lcom/infraware/office/evengine/EV$CONFIG_INFO$SCROLLINFO;->possibleScrollLeft:Z

    .line 601
    iput-boolean v0, p0, Lcom/infraware/office/evengine/EV$CONFIG_INFO$SCROLLINFO;->possibleScrollNextPage:Z

    iput-boolean v0, p0, Lcom/infraware/office/evengine/EV$CONFIG_INFO$SCROLLINFO;->possibleScrollPrevPage:Z

    iput-boolean v0, p0, Lcom/infraware/office/evengine/EV$CONFIG_INFO$SCROLLINFO;->possibleScrollDown:Z

    .line 602
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$CONFIG_INFO$SCROLLINFO;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 605
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO$SCROLLINFO;

    .line 606
    .local v0, "o":Lcom/infraware/office/evengine/EV$CONFIG_INFO$SCROLLINFO;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 591
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$CONFIG_INFO$SCROLLINFO;->clone()Lcom/infraware/office/evengine/EV$CONFIG_INFO$SCROLLINFO;

    move-result-object v0

    return-object v0
.end method
