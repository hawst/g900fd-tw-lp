.class public Lcom/infraware/office/evengine/EV$BWP_CHART;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "BWP_CHART"
.end annotation


# instance fields
.field public bExistHideCell:Z

.field public bPlotVisOnly:Z

.field public bSeriesInRows:Z

.field public nBarType:I

.field public nChartType:I

.field public nDimension:I

.field public nItemCnt:I

.field public nLegend:I

.field public nSerialCnt:I

.field public nSeriesIn:I

.field public nStyleID:I

.field public serialData:[Ljava/lang/String;

.field public strItemName:[Ljava/lang/String;

.field public strSerialName:[Ljava/lang/String;

.field public szTitle:Ljava/lang/String;

.field public szXAxis:Ljava/lang/String;

.field public szYAxis:Ljava/lang/String;

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 1827
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$BWP_CHART;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 3

    .prologue
    .line 1848
    const/4 v1, -0x1

    iput v1, p0, Lcom/infraware/office/evengine/EV$BWP_CHART;->nStyleID:I

    iput v1, p0, Lcom/infraware/office/evengine/EV$BWP_CHART;->nBarType:I

    iput v1, p0, Lcom/infraware/office/evengine/EV$BWP_CHART;->nDimension:I

    iput v1, p0, Lcom/infraware/office/evengine/EV$BWP_CHART;->nLegend:I

    iput v1, p0, Lcom/infraware/office/evengine/EV$BWP_CHART;->nSerialCnt:I

    iput v1, p0, Lcom/infraware/office/evengine/EV$BWP_CHART;->nItemCnt:I

    iput v1, p0, Lcom/infraware/office/evengine/EV$BWP_CHART;->nChartType:I

    .line 1849
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/infraware/office/evengine/EV$BWP_CHART;->bSeriesInRows:Z

    iput-boolean v1, p0, Lcom/infraware/office/evengine/EV$BWP_CHART;->bExistHideCell:Z

    iput-boolean v1, p0, Lcom/infraware/office/evengine/EV$BWP_CHART;->bPlotVisOnly:Z

    .line 1850
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$BWP_CHART;->strSerialName:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 1851
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$BWP_CHART;->strSerialName:[Ljava/lang/String;

    const-string/jumbo v2, ""

    aput-object v2, v1, v0

    .line 1850
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1853
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$BWP_CHART;->strItemName:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1854
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$BWP_CHART;->strItemName:[Ljava/lang/String;

    const-string/jumbo v2, ""

    aput-object v2, v1, v0

    .line 1853
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1856
    :cond_1
    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$BWP_CHART;->serialData:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 1857
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$BWP_CHART;->serialData:[Ljava/lang/String;

    const-string/jumbo v2, ""

    aput-object v2, v1, v0

    .line 1856
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1858
    :cond_2
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/infraware/office/evengine/EV$BWP_CHART;->szTitle:Ljava/lang/String;

    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/infraware/office/evengine/EV$BWP_CHART;->szXAxis:Ljava/lang/String;

    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/infraware/office/evengine/EV$BWP_CHART;->szYAxis:Ljava/lang/String;

    .line 1859
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$BWP_CHART;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1862
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$BWP_CHART;

    .line 1863
    .local v0, "o":Lcom/infraware/office/evengine/EV$BWP_CHART;
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$BWP_CHART;->serialData:[Ljava/lang/String;

    invoke-virtual {v1}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    iput-object v1, v0, Lcom/infraware/office/evengine/EV$BWP_CHART;->serialData:[Ljava/lang/String;

    .line 1864
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$BWP_CHART;->strSerialName:[Ljava/lang/String;

    invoke-virtual {v1}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    iput-object v1, v0, Lcom/infraware/office/evengine/EV$BWP_CHART;->strSerialName:[Ljava/lang/String;

    .line 1865
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$BWP_CHART;->strItemName:[Ljava/lang/String;

    invoke-virtual {v1}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    iput-object v1, v0, Lcom/infraware/office/evengine/EV$BWP_CHART;->strItemName:[Ljava/lang/String;

    .line 1866
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1827
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$BWP_CHART;->clone()Lcom/infraware/office/evengine/EV$BWP_CHART;

    move-result-object v0

    return-object v0
.end method
