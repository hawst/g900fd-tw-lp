.class public Lcom/infraware/office/evengine/EV$RULERBAR_PAGE_INFO;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RULERBAR_PAGE_INFO"
.end annotation


# instance fields
.field public nPageLeftLogical:I

.field public nPageRightLogical:I

.field public nPageRightPos:I

.field public nReturn:I

.field public nTextLeftPos:I

.field public nTextRightPos:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 1720
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$RULERBAR_PAGE_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1730
    iput v0, p0, Lcom/infraware/office/evengine/EV$RULERBAR_PAGE_INFO;->nPageRightLogical:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$RULERBAR_PAGE_INFO;->nPageRightPos:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$RULERBAR_PAGE_INFO;->nTextRightPos:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$RULERBAR_PAGE_INFO;->nTextLeftPos:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$RULERBAR_PAGE_INFO;->nPageLeftLogical:I

    .line 1731
    iput v0, p0, Lcom/infraware/office/evengine/EV$RULERBAR_PAGE_INFO;->nReturn:I

    .line 1732
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$RULERBAR_PAGE_INFO;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1735
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$RULERBAR_PAGE_INFO;

    .line 1736
    .local v0, "o":Lcom/infraware/office/evengine/EV$RULERBAR_PAGE_INFO;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1720
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$RULERBAR_PAGE_INFO;->clone()Lcom/infraware/office/evengine/EV$RULERBAR_PAGE_INFO;

    move-result-object v0

    return-object v0
.end method
