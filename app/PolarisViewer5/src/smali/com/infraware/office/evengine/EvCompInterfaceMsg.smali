.class Lcom/infraware/office/evengine/EvCompInterfaceMsg;
.super Lcom/infraware/office/evengine/EvInterface;
.source "EvCompInterfaceMsg.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;
    }
.end annotation


# static fields
.field private static mEvLock:Ljava/lang/Object;


# instance fields
.field protected final mHandler:Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;

.field private m_BackupZoom:I

.field public m_oZoomChangedListener:Lcom/infraware/office/evengine/EvInterface$OnZoomChangeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mEvLock:Ljava/lang/Object;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "appDir"    # Ljava/lang/String;

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/infraware/office/evengine/EvInterface;-><init>(Ljava/lang/String;)V

    .line 94
    new-instance v0, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;-><init>(Lcom/infraware/office/evengine/EvCompInterfaceMsg;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mHandler:Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;

    .line 227
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->m_BackupZoom:I

    .line 255
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->m_oZoomChangedListener:Lcom/infraware/office/evengine/EvInterface$OnZoomChangeListener;

    .line 107
    return-void
.end method


# virtual methods
.method public AddOpendFileList(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p1, "Path"    # Ljava/lang/String;
    .param p2, "TempPath"    # Ljava/lang/String;

    .prologue
    .line 2146
    invoke-virtual {p0, p1}, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->CheckOpenedFileList(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2148
    iget-object v6, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->InterfaceVector:Ljava/util/Vector;

    new-instance v0, Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->clone()Lcom/infraware/office/evengine/EV;

    move-result-object v2

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvNative;->IGetInterfaceHandleValue()I

    move-result v3

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;-><init>(Lcom/infraware/office/evengine/EvInterface;Lcom/infraware/office/evengine/EV;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 2149
    const/4 v0, 0x1

    .line 2152
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public CheckOpenedFileList(Ljava/lang/String;)Z
    .locals 1
    .param p1, "mstrOpenFilePath"    # Ljava/lang/String;

    .prologue
    .line 2131
    invoke-virtual {p0, p1}, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->SearchInterfaceVectorbyArg(Ljava/lang/String;)Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public DeleteOpenedFileList(Ljava/lang/String;)Z
    .locals 2
    .param p1, "mstrOpenFilePath"    # Ljava/lang/String;

    .prologue
    .line 2140
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->InterfaceVector:Ljava/util/Vector;

    invoke-virtual {p0, p1}, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->SearchInterfaceVectorbyArg(Ljava/lang/String;)Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public IAnnotationShow(Z)V
    .locals 1
    .param p1, "bEnable"    # Z

    .prologue
    .line 1941
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IAnnotationShow(Z)V

    .line 1942
    return-void
.end method

.method public IApplyBookClip(Lcom/infraware/office/evengine/EV$BOOK_CLIP;)V
    .locals 1
    .param p1, "a_pClip"    # Lcom/infraware/office/evengine/EV$BOOK_CLIP;

    .prologue
    .line 1542
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IApplyBookClip(Lcom/infraware/office/evengine/EV$BOOK_CLIP;)V

    .line 1543
    return-void
.end method

.method public IApplyBookMark()V
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IApplyBookMark()V

    .line 396
    return-void
.end method

.method public IBookMarkOnOff(I)V
    .locals 1
    .param p1, "a_bOn"    # I

    .prologue
    .line 387
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IBookMarkOnOff(I)V

    .line 388
    return-void
.end method

.method public IBookmarkEditor(ILjava/lang/String;)V
    .locals 1
    .param p1, "EEV_BOOKMARK_EDITOR_MODE"    # I
    .param p2, "a_pszBookmark"    # Ljava/lang/String;

    .prologue
    .line 722
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IBookmarkEditor(ILjava/lang/String;)V

    .line 723
    return-void
.end method

.method public IBulletNumbering(III)V
    .locals 1
    .param p1, "EEV_BULLET_NUMBERIG"    # I
    .param p2, "EEV_BULLETNUMBER_TYPE"    # I
    .param p3, "bNumberReset"    # I

    .prologue
    .line 637
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvNative;->IBulletNumbering(III)V

    .line 638
    return-void
.end method

.method public ICanCellDelete_Editor()Z
    .locals 2

    .prologue
    .line 1071
    const/4 v0, 0x0

    .line 1072
    .local v0, "nRet":Z
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvNative;->ICanCellDelete()Z

    move-result v0

    .line 1073
    return v0
.end method

.method public ICanExtendSortRange()V
    .locals 1

    .prologue
    .line 1418
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ICanExtendSortRange()I

    .line 1419
    return-void
.end method

.method public ICancel()V
    .locals 1

    .prologue
    .line 480
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ICancel()V

    .line 481
    return-void
.end method

.method public ICaretMark(II)V
    .locals 1
    .param p1, "EEV_CARET_MARK"    # I
    .param p2, "nSelectMode"    # I

    .prologue
    .line 634
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->ICaretMark(II)V

    .line 635
    return-void
.end method

.method public ICaretMove(II)V
    .locals 1
    .param p1, "EEV_CARET_MOVE"    # I
    .param p2, "a_eFunctionKey"    # I

    .prologue
    .line 619
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->ICaretMove(II)V

    .line 620
    return-void
.end method

.method public ICaretShow(I)V
    .locals 1
    .param p1, "a_bCaret"    # I

    .prologue
    .line 622
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ICaretShow(I)V

    .line 623
    return-void
.end method

.method public ICellEdit(II)V
    .locals 1
    .param p1, "EDIT_MODE"    # I
    .param p2, "EDIT_TYPE"    # I

    .prologue
    .line 705
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->ICellEdit(II)V

    .line 706
    return-void
.end method

.method public ICellEqualWidthHeight(I)V
    .locals 1
    .param p1, "EEV_WORD_CELL_WIDTH_HEIGHT"    # I

    .prologue
    .line 715
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ICellEqualWidthHeight(I)V

    .line 716
    return-void
.end method

.method public ICellInsertDelete(II)V
    .locals 1
    .param p1, "EEV_CELL_ISERT_DELETE"    # I
    .param p2, "a_nCellType"    # I

    .prologue
    .line 709
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->ICellInsertDelete(II)V

    .line 710
    return-void
.end method

.method public ICellMergeSeparate(III)V
    .locals 1
    .param p1, "EEV_WORD_CELL_MERGE_SEP"    # I
    .param p2, "a_nRow"    # I
    .param p3, "a_nCol"    # I

    .prologue
    .line 712
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvNative;->ICellMergeSeparate(III)V

    .line 713
    return-void
.end method

.method public IChangeCase(I)V
    .locals 1
    .param p1, "aType"    # I

    .prologue
    .line 2517
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IChangeCase(I)V

    .line 2518
    return-void
.end method

.method public IChangeDisplay(I)V
    .locals 0
    .param p1, "EEV_DISPLAY_MODE_TYPE"    # I

    .prologue
    .line 319
    return-void
.end method

.method public IChangeScreen(III)V
    .locals 6
    .param p1, "bLandScape"    # I
    .param p2, "a_nWidth"    # I
    .param p3, "a_nHeight"    # I

    .prologue
    const/4 v4, 0x0

    .line 313
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvNative;->IChangeScreen(IIIII)V

    .line 314
    return-void
.end method

.method public IChangeViewMode(IIIIII)V
    .locals 7
    .param p1, "EEV_VIEW_MODE"    # I
    .param p2, "nWidth"    # I
    .param p3, "nHeight"    # I
    .param p4, "bCanSelection"    # I
    .param p5, "bFixedHeader"    # I
    .param p6, "bDraw"    # I

    .prologue
    .line 515
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvNative;->IChangeViewMode(IIIIII)V

    .line 516
    return-void
.end method

.method public ICharInput()V
    .locals 1

    .prologue
    .line 1402
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ICharInput()V

    .line 1403
    return-void
.end method

.method public ICharInsert(III)V
    .locals 1
    .param p1, "EEV_CHAR_IPUT"    # I
    .param p2, "a_wCode"    # I
    .param p3, "a_nRepeat"    # I

    .prologue
    .line 701
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvNative;->ICharInsert(III)V

    .line 702
    return-void
.end method

.method public IChartAxesInfo(I[C[C[I[C[D[IC)V
    .locals 9
    .param p1, "a_nChartType"    # I
    .param p2, "a_bExistAxes"    # [C
    .param p3, "a_bAxesInfo"    # [C
    .param p4, "a_nAlignment"    # [I
    .param p5, "a_bScaleInfo"    # [C
    .param p6, "a_dLogBase"    # [D
    .param p7, "a_nUnitIndex"    # [I
    .param p8, "a_nHasMinusValue"    # C

    .prologue
    .line 1356
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lcom/infraware/office/evengine/EvNative;->IChartAxesInfo(I[C[C[I[C[D[IC)V

    .line 1357
    return-void
.end method

.method public IChartChangeDataRange(Z)V
    .locals 1
    .param p1, "bFinished"    # Z

    .prologue
    .line 1376
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IChartChangeDataRange(Z)V

    .line 1377
    return-void
.end method

.method public IChartChangeDataRangeEnd()V
    .locals 1

    .prologue
    .line 1379
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IChartChangeDataRangeEnd()V

    .line 1380
    return-void
.end method

.method public IChartDataLabelInfo(IIIIII)V
    .locals 7
    .param p1, "a_nChartType"    # I
    .param p2, "a_nFlag"    # I
    .param p3, "a_nLabelPos"    # I
    .param p4, "a_bEnableNumFmt"    # I
    .param p5, "a_nDecPlaces"    # I
    .param p6, "a_nNegativeType"    # I

    .prologue
    .line 1370
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvNative;->IChartDataLabelInfo(IIIIII)V

    .line 1371
    return-void
.end method

.method public IChartFontInfo(Ljava/lang/String;F)V
    .locals 1
    .param p1, "a_fName"    # Ljava/lang/String;
    .param p2, "a_fRatio"    # F

    .prologue
    .line 1361
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IChartFontInfo(Ljava/lang/String;F)V

    .line 1362
    return-void
.end method

.method public IChartStyleInfo(II)V
    .locals 1
    .param p1, "a_nChartStyle"    # I
    .param p2, "a_nCharteffect"    # I

    .prologue
    .line 1373
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IChartStyleInfo(II)V

    .line 1374
    return-void
.end method

.method public IChartTitleInfo(IIZZZZ)V
    .locals 7
    .param p1, "a_nChartType"    # I
    .param p2, "a_nChartStyle"    # I
    .param p3, "a_bShowTitle"    # Z
    .param p4, "a_bShowPlotBorder"    # Z
    .param p5, "a_bPlotVisOnly"    # Z
    .param p6, "a_bShowChartBorder"    # Z

    .prologue
    .line 1345
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvNative;->IChartTitleInfo(IIZZZZ)V

    .line 1346
    return-void
.end method

.method public ICheckVideoObjInGroupShape()Z
    .locals 1

    .prologue
    .line 2183
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ICheckVideoObjInGroupShape()Z

    move-result v0

    return v0
.end method

.method public IClearFrameSet()V
    .locals 1

    .prologue
    .line 2103
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IClearFrameSet()V

    .line 2104
    return-void
.end method

.method public IClose()V
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IClose()V

    .line 185
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mHandler:Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->setOperationTimer(Z)V

    .line 187
    invoke-super {p0}, Lcom/infraware/office/evengine/EvInterface;->IClose()V

    .line 188
    return-void
.end method

.method public ICreatePDFAnnotation(I)V
    .locals 1
    .param p1, "style"    # I

    .prologue
    .line 1952
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ICreatePDFAnnotation(I)V

    .line 1953
    return-void
.end method

.method public ICreatePDFStickyNote(II)V
    .locals 1
    .param p1, "posX"    # I
    .param p2, "posY"    # I

    .prologue
    .line 1976
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->ICreatePDFStickyNote(II)V

    .line 1977
    return-void
.end method

.method public ICreateTable(IIII)V
    .locals 1
    .param p1, "a_nRow"    # I
    .param p2, "a_nCol"    # I
    .param p3, "a_nColor"    # I
    .param p4, "a_nStyleNum"    # I

    .prologue
    .line 596
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/infraware/office/evengine/EvNative;->ICreateTable(IIII)V

    .line 597
    return-void
.end method

.method public IDeleteBookClip(Lcom/infraware/office/evengine/EV$BOOK_CLIP;)V
    .locals 1
    .param p1, "a_pClip"    # Lcom/infraware/office/evengine/EV$BOOK_CLIP;

    .prologue
    .line 1545
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IDeleteBookClip(Lcom/infraware/office/evengine/EV$BOOK_CLIP;)V

    .line 1546
    return-void
.end method

.method public IDeletePenDataForFreeDraw()V
    .locals 1

    .prologue
    .line 1737
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IDeletePenDataForFreeDraw()V

    .line 1738
    return-void
.end method

.method public IDeletePenDataForSlideShow()V
    .locals 1

    .prologue
    .line 1733
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IDeletePenDataForSlideShow()V

    .line 1734
    return-void
.end method

.method public IDocumentModified_Editor()Z
    .locals 2

    .prologue
    .line 1058
    const/4 v0, 0x0

    .line 1059
    .local v0, "nRet":Z
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvNative;->IDocumentModified()Z

    move-result v0

    .line 1060
    return v0
.end method

.method public IEditDocument(IILjava/lang/String;)V
    .locals 1
    .param p1, "EEV_EDIT_DOCUMENT"    # I
    .param p2, "nDataType"    # I
    .param p3, "data"    # Ljava/lang/String;

    .prologue
    .line 563
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvNative;->IEditDocument(IILjava/lang/String;)V

    .line 564
    return-void
.end method

.method public IEditPageRedrawBitmap()V
    .locals 1

    .prologue
    .line 659
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IEditPageRedrawBitmap()V

    .line 660
    return-void
.end method

.method public IExitPreview()V
    .locals 1

    .prologue
    .line 1017
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IExitPreview()V

    .line 1018
    return-void
.end method

.method public IExportPDF(Ljava/lang/String;I[I)V
    .locals 3
    .param p1, "a_pszFilePath"    # Ljava/lang/String;
    .param p2, "a_nCount"    # I
    .param p3, "a_nPageArray"    # [I

    .prologue
    .line 554
    const-string/jumbo v0, "EvCompinterface"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "IExportPDF = , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvNative;->IExportPDF(Ljava/lang/String;I[I)V

    .line 556
    return-void
.end method

.method public IFinalize()V
    .locals 2

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->isInit()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 154
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IFinalize()V

    .line 156
    :cond_0
    return-void
.end method

.method public IFindWordNext(I)V
    .locals 1
    .param p1, "a_bDirUp"    # I

    .prologue
    .line 433
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IFindWordNext(I)V

    .line 434
    return-void
.end method

.method public IFindWordNextByPos(II)V
    .locals 1
    .param p1, "a_nXpos"    # I
    .param p2, "a_nYpos"    # I

    .prologue
    .line 437
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IFindWordNextByPos(II)V

    .line 438
    return-void
.end method

.method public IFindWordStart(II)V
    .locals 1
    .param p1, "a_nXpos"    # I
    .param p2, "a_nYpos"    # I

    .prologue
    .line 429
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IFindWordStart(II)V

    .line 430
    return-void
.end method

.method public IFindWordStop()V
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IFindWordStop()V

    .line 442
    return-void
.end method

.method public IFlick(II)V
    .locals 1
    .param p1, "a_nVelocityX"    # I
    .param p2, "a_nVelocityY"    # I

    .prologue
    .line 209
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IFlick(II)V

    .line 210
    return-void
.end method

.method public IGetAllChartInfo()Lcom/infraware/office/evengine/EV$GUI_SHEET_ALL_CHART_EVENT;
    .locals 2

    .prologue
    .line 1486
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getGuiSheetAllChartEvent()Lcom/infraware/office/evengine/EV$GUI_SHEET_ALL_CHART_EVENT;

    move-result-object v0

    .line 1487
    .local v0, "chartInfo":Lcom/infraware/office/evengine/EV$GUI_SHEET_ALL_CHART_EVENT;
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1, v0}, Lcom/infraware/office/evengine/EvNative;->IGetAllChartInfo(Lcom/infraware/office/evengine/EV$GUI_SHEET_ALL_CHART_EVENT;)V

    .line 1488
    return-object v0
.end method

.method public IGetAnimationInfo(I)Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;
    .locals 3
    .param p1, "nIndex"    # I

    .prologue
    .line 975
    sget-object v1, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mEvLock:Ljava/lang/Object;

    monitor-enter v1

    .line 976
    :try_start_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v2, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV;->getAnimationInfo()Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Lcom/infraware/office/evengine/EvNative;->IGetAnimationInfo(ILcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;)V

    .line 977
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getAnimationInfo()Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 978
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public IGetApplyCellCount()I
    .locals 1

    .prologue
    .line 1648
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetApplyCellCount()I

    move-result v0

    return v0
.end method

.method public IGetAutoResizeFlag()Z
    .locals 1

    .prologue
    .line 2646
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetAutoResizeFlag()Z

    move-result v0

    return v0
.end method

.method public IGetBWPCellStatusInfo()I
    .locals 1

    .prologue
    .line 1652
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetBWPCellStatusInfo()I

    move-result v0

    return v0
.end method

.method public IGetBWPChartStyle()I
    .locals 1

    .prologue
    .line 1822
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetBWPChartStyle()I

    move-result v0

    return v0
.end method

.method public IGetBWPGrapAttrInfo_Editor()Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;
    .locals 2

    .prologue
    .line 1049
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getBwpGrapAttrInfo()Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetBWPGrapAttrInfo(Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;)V

    .line 1050
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getBwpGrapAttrInfo()Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;

    move-result-object v0

    return-object v0
.end method

.method public IGetBWPOpInfo_Editor()Lcom/infraware/office/evengine/EV$BWP_OP_INFO;
    .locals 2

    .prologue
    .line 1040
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getBwpOpInfo()Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetBWPOpInfo(Lcom/infraware/office/evengine/EV$BWP_OP_INFO;)V

    .line 1041
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getBwpOpInfo()Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    move-result-object v0

    return-object v0
.end method

.method public IGetBWPProtectStatusInfo()I
    .locals 1

    .prologue
    .line 1656
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetBWPProtectStatusInfo()I

    move-result v0

    return v0
.end method

.method public IGetBWPSplitCellMaxNum_Editor()Lcom/infraware/office/evengine/EV$BWP_SPLITCELL_MAXNUM;
    .locals 2

    .prologue
    .line 1045
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getBwpSplitCellMaxNum()Lcom/infraware/office/evengine/EV$BWP_SPLITCELL_MAXNUM;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetBWPSplitCellMaxNum(Lcom/infraware/office/evengine/EV$BWP_SPLITCELL_MAXNUM;)V

    .line 1046
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getBwpSplitCellMaxNum()Lcom/infraware/office/evengine/EV$BWP_SPLITCELL_MAXNUM;

    move-result-object v0

    return-object v0
.end method

.method public IGetBookClipCount(Lcom/infraware/office/evengine/EV$BOOK_CLIP;)I
    .locals 1
    .param p1, "a_pClip"    # Lcom/infraware/office/evengine/EV$BOOK_CLIP;

    .prologue
    .line 1539
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IGetBookClipCount(Lcom/infraware/office/evengine/EV$BOOK_CLIP;)I

    move-result v0

    return v0
.end method

.method public IGetBookClipName(ILcom/infraware/office/evengine/EV$BOOK_CLIP;)V
    .locals 1
    .param p1, "a_nIndex"    # I
    .param p2, "a_pClip"    # Lcom/infraware/office/evengine/EV$BOOK_CLIP;

    .prologue
    .line 1536
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IGetBookClipName(ILcom/infraware/office/evengine/EV$BOOK_CLIP;)V

    .line 1537
    return-void
.end method

.method public IGetBookmarkCount_Editor()I
    .locals 2

    .prologue
    .line 1123
    const/4 v0, 0x0

    .line 1124
    .local v0, "nCnt":I
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvNative;->IGetBookmarkCount_Editor()I

    move-result v0

    .line 1125
    return v0
.end method

.method public IGetBookmarkInfo_Editor(I)Ljava/lang/String;
    .locals 2
    .param p1, "a_nIndex"    # I

    .prologue
    .line 1129
    const/4 v0, 0x0

    .line 1130
    .local v0, "strTemp":Ljava/lang/String;
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1, p1}, Lcom/infraware/office/evengine/EvNative;->IGetBookmarkInfo_Editor(I)Ljava/lang/String;

    move-result-object v0

    .line 1131
    return-object v0
.end method

.method public IGetBookmarkLabel(I)Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;
    .locals 2
    .param p1, "a_nIndex"    # I

    .prologue
    .line 1020
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getBookmarkLabel()Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/infraware/office/evengine/EvNative;->IGetBookmarkLabel(ILcom/infraware/office/evengine/EV$BOOKMARK_LABEL;)V

    .line 1021
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getBookmarkLabel()Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;

    move-result-object v0

    return-object v0
.end method

.method public IGetBorderProperty()Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;
    .locals 2

    .prologue
    .line 1528
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getGuiBorderEvent()Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetBorderProperty(Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;)V

    .line 1529
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getGuiBorderEvent()Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;

    move-result-object v0

    return-object v0
.end method

.method public IGetBulletType_Editor()Lcom/infraware/office/evengine/EV$BULLET_TYPE;
    .locals 2

    .prologue
    .line 1107
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getBulletType()Lcom/infraware/office/evengine/EV$BULLET_TYPE;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetBulletType(Lcom/infraware/office/evengine/EV$BULLET_TYPE;)V

    .line 1108
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getBulletType()Lcom/infraware/office/evengine/EV$BULLET_TYPE;

    move-result-object v0

    return-object v0
.end method

.method public IGetBwpChart()Lcom/infraware/office/evengine/EV$BWP_CHART;
    .locals 2

    .prologue
    .line 1678
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getBwpChart()Lcom/infraware/office/evengine/EV$BWP_CHART;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetBwpChart(Lcom/infraware/office/evengine/EV$BWP_CHART;)Z

    .line 1679
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getBwpChart()Lcom/infraware/office/evengine/EV$BWP_CHART;

    move-result-object v0

    return-object v0
.end method

.method public IGetCaretAfterString(I)Ljava/lang/String;
    .locals 1
    .param p1, "a_length"    # I

    .prologue
    .line 1587
    const/4 v0, 0x0

    return-object v0
.end method

.method public IGetCaretBeforeString(I)Ljava/lang/String;
    .locals 1
    .param p1, "a_length"    # I

    .prologue
    .line 1581
    const/4 v0, 0x0

    return-object v0
.end method

.method public IGetCaretInfo_Editor()Lcom/infraware/office/evengine/EV$CARET_INFO;
    .locals 1

    .prologue
    .line 1033
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getCaretInfoEvent()Lcom/infraware/office/evengine/EV$CARET_INFO;

    move-result-object v0

    return-object v0
.end method

.method public IGetCaretPos()Lcom/infraware/office/evengine/EV$CARET_POS;
    .locals 2

    .prologue
    .line 625
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getCaretPos()Lcom/infraware/office/evengine/EV$CARET_POS;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetCaretPos(Lcom/infraware/office/evengine/EV$CARET_POS;)V

    .line 626
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getCaretPos()Lcom/infraware/office/evengine/EV$CARET_POS;

    move-result-object v0

    return-object v0
.end method

.method public IGetCellInfo()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;
    .locals 2

    .prologue
    .line 1429
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getSheetCellInfo()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetCellInfo(Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;)V

    .line 1430
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getSheetCellInfo()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    move-result-object v0

    return-object v0
.end method

.method public IGetCellMarkRectInfo([SI)I
    .locals 1
    .param p1, "CellRectInfos"    # [S
    .param p2, "short_len"    # I

    .prologue
    .line 2099
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IGetCellMarkRectInfo([SI)I

    move-result v0

    return v0
.end method

.method public IGetCellProperty_Editor()Lcom/infraware/office/evengine/EV$CELL_PROPERTY;
    .locals 2

    .prologue
    .line 1111
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getCellProperty()Lcom/infraware/office/evengine/EV$CELL_PROPERTY;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetCellProperty(Lcom/infraware/office/evengine/EV$CELL_PROPERTY;)V

    .line 1112
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getCellProperty()Lcom/infraware/office/evengine/EV$CELL_PROPERTY;

    move-result-object v0

    return-object v0
.end method

.method public IGetCellType()I
    .locals 1

    .prologue
    .line 1643
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetCellType()I

    move-result v0

    return v0
.end method

.method public IGetChartAxesInfo()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;
    .locals 2

    .prologue
    .line 1465
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getGuiSheetChartAxesInfoEvent()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;

    move-result-object v0

    .line 1466
    .local v0, "chartAxesInfo":Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1, v0}, Lcom/infraware/office/evengine/EvNative;->IGetChartAxesInfo(Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;)V

    .line 1467
    return-object v0
.end method

.method public IGetChartDataLabelInfo()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;
    .locals 2

    .prologue
    .line 1475
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getGuiSheetChartDataLabelInfoEvent()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;

    move-result-object v0

    .line 1476
    .local v0, "chartDataLabelInfo":Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1, v0}, Lcom/infraware/office/evengine/EvNative;->IGetChartDataLabelInfo(Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;)V

    .line 1477
    return-object v0
.end method

.method public IGetChartEffect()I
    .locals 1

    .prologue
    .line 2409
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetChartEffect()I

    move-result v0

    return v0
.end method

.method public IGetChartFontData()Lcom/infraware/office/evengine/EV$CHART_FONTDATA;
    .locals 2

    .prologue
    .line 1470
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getFontData()Lcom/infraware/office/evengine/EV$CHART_FONTDATA;

    move-result-object v0

    .line 1471
    .local v0, "chartFontData":Lcom/infraware/office/evengine/EV$CHART_FONTDATA;
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1, v0}, Lcom/infraware/office/evengine/EvNative;->IGetChartFontData(Lcom/infraware/office/evengine/EV$CHART_FONTDATA;)V

    .line 1472
    return-object v0
.end method

.method public IGetChartInfo()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;
    .locals 2

    .prologue
    .line 1454
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getGuiSheetChartEvent()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;

    move-result-object v0

    .line 1455
    .local v0, "chartInfo":Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1, v0}, Lcom/infraware/office/evengine/EvNative;->IGetChartInfo(Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;)V

    .line 1456
    return-object v0
.end method

.method public IGetChartStyleInfo()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_STYLEINFO_EVENT;
    .locals 2

    .prologue
    .line 1481
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getGuiSheetChartStyleInfoEvent()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_STYLEINFO_EVENT;

    move-result-object v0

    .line 1482
    .local v0, "chartStyleInfo":Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_STYLEINFO_EVENT;
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1, v0}, Lcom/infraware/office/evengine/EvNative;->IGetChartStyleInfo(Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_STYLEINFO_EVENT;)V

    .line 1483
    return-object v0
.end method

.method public IGetChartThumbnail(III)V
    .locals 1
    .param p1, "a_nTotalStyleNum"    # I
    .param p2, "a_nWidth"    # I
    .param p3, "a_nHeight"    # I

    .prologue
    .line 1397
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvNative;->IGetChartThumbnail(III)V

    .line 1398
    return-void
.end method

.method public IGetChartTitleInfo()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;
    .locals 2

    .prologue
    .line 1460
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getGuiSheetChartTitleInfoEvent()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;

    move-result-object v0

    .line 1461
    .local v0, "chartTitleInfo":Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1, v0}, Lcom/infraware/office/evengine/EvNative;->IGetChartTitleInfo(Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;)V

    .line 1462
    return-object v0
.end method

.method public IGetColumn()I
    .locals 1

    .prologue
    .line 1559
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetColumn()I

    move-result v0

    return v0
.end method

.method public IGetCommentText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1492
    const/4 v0, 0x0

    .line 1493
    .local v0, "strComment":Ljava/lang/String;
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvNative;->IGetCommentText()Ljava/lang/String;

    move-result-object v0

    .line 1494
    return-object v0
.end method

.method public IGetCompatibilityModeVersion()I
    .locals 1

    .prologue
    .line 1759
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetCompatibilityModeVersion()I

    move-result v0

    return v0
.end method

.method public IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;
    .locals 1

    .prologue
    .line 487
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    return-object v0
.end method

.method public IGetCurrentSheetIndex()I
    .locals 1

    .prologue
    .line 1412
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetCurrentSheetIndex()I

    move-result v0

    return v0
.end method

.method public IGetCurrentTableMaxRowColInfo_Editor([I)Z
    .locals 1
    .param p1, "arr"    # [I

    .prologue
    .line 2107
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IGetCurrentTableMaxRowColInfo_Editor([I)Z

    move-result v0

    return v0
.end method

.method public IGetDocType()I
    .locals 1

    .prologue
    .line 2090
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetDocType()I

    move-result v0

    return v0
.end method

.method public IGetDrawCellLine_Editor()Lcom/infraware/office/evengine/EV$DRAW_CELLLINE;
    .locals 2

    .prologue
    .line 2358
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getDrawCellLine()Lcom/infraware/office/evengine/EV$DRAW_CELLLINE;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetDrawCellLineProperty(Lcom/infraware/office/evengine/EV$DRAW_CELLLINE;)V

    .line 2359
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getDrawCellLine()Lcom/infraware/office/evengine/EV$DRAW_CELLLINE;

    move-result-object v0

    return-object v0
.end method

.method public IGetDualViewPosForSlideShow(II)Lcom/infraware/office/evengine/EV$DUALVIEW_POS;
    .locals 2
    .param p1, "a_nXPos"    # I
    .param p2, "a_nYPos"    # I

    .prologue
    .line 1742
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getDualViewPos()Lcom/infraware/office/evengine/EV$DUALVIEW_POS;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/infraware/office/evengine/EvNative;->IGetDualViewPosForSlideShow(IILcom/infraware/office/evengine/EV$DUALVIEW_POS;)V

    .line 1743
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getDualViewPos()Lcom/infraware/office/evengine/EV$DUALVIEW_POS;

    move-result-object v0

    return-object v0
.end method

.method public IGetEditStauts_Editor()J
    .locals 3

    .prologue
    .line 1102
    const-wide/16 v0, 0x0

    .line 1103
    .local v0, "a_Status":J
    iget-object v2, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvNative;->IGetEditStauts()J

    move-result-wide v0

    .line 1104
    return-wide v0
.end method

.method public IGetEditorMode()I
    .locals 1

    .prologue
    .line 2590
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetEditorMode()I

    move-result v0

    return v0
.end method

.method public IGetEditorMode_Editor()I
    .locals 1

    .prologue
    .line 1068
    iget v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mEditorMode:I

    return v0
.end method

.method public IGetFontAttInfo_Editor()Lcom/infraware/office/evengine/EV$FONT_INFO;
    .locals 2

    .prologue
    .line 1036
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getGuiFontEvent()Lcom/infraware/office/evengine/EV$FONT_INFO;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetFontAttInfo(Lcom/infraware/office/evengine/EV$FONT_INFO;)V

    .line 1037
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getGuiFontEvent()Lcom/infraware/office/evengine/EV$FONT_INFO;

    move-result-object v0

    return-object v0
.end method

.method public IGetFontStyle()I
    .locals 1

    .prologue
    .line 2065
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetFontStyle()I

    move-result v0

    return v0
.end method

.method public IGetFormatInfo()Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;
    .locals 2

    .prologue
    .line 1433
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getSheetFormatInfo()Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetFormatInfo(Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;)V

    .line 1434
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getSheetFormatInfo()Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    move-result-object v0

    return-object v0
.end method

.method public IGetFrameDetectionArea()Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;
    .locals 2

    .prologue
    .line 279
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getFrameDetectionArea()Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetFrameDetectionArea(Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;)V

    .line 280
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getFrameDetectionArea()Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;

    move-result-object v0

    return-object v0
.end method

.method public IGetGradientDrawColorInfo(Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;)Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;
    .locals 1
    .param p1, "oGradientDrawInfo"    # Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;

    .prologue
    .line 2187
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IGetGradientDrawColorInfo(Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;)V

    .line 2188
    return-object p1
.end method

.method public IGetGuides()Lcom/infraware/office/evengine/EV$GUIDES_INFO;
    .locals 1

    .prologue
    .line 2439
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getGuides()Lcom/infraware/office/evengine/EV$GUIDES_INFO;

    move-result-object v0

    return-object v0
.end method

.method public IGetHeaderFooterOption()Lcom/infraware/office/evengine/EV$HeaderFooterOption;
    .locals 1

    .prologue
    .line 2595
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->IGetHeaderFooterOption(I)Lcom/infraware/office/evengine/EV$HeaderFooterOption;

    move-result-object v0

    return-object v0
.end method

.method public IGetHeaderFooterOption(I)Lcom/infraware/office/evengine/EV$HeaderFooterOption;
    .locals 2
    .param p1, "nPageNum"    # I

    .prologue
    .line 2600
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getHeaderFooterOption()Lcom/infraware/office/evengine/EV$HeaderFooterOption;

    move-result-object v0

    .line 2601
    .local v0, "option":Lcom/infraware/office/evengine/EV$HeaderFooterOption;
    iput p1, v0, Lcom/infraware/office/evengine/EV$HeaderFooterOption;->a_nTargetPageNum:I

    .line 2603
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1, v0}, Lcom/infraware/office/evengine/EvNative;->IGetHeaderFooterOption(Lcom/infraware/office/evengine/EV$HeaderFooterOption;)V

    .line 2604
    return-object v0
.end method

.method public IGetHyperLinkInfoEx(II)Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;
    .locals 2
    .param p1, "a_nX"    # I
    .param p2, "a_nY"    # I

    .prologue
    .line 1571
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getHyperLinkInfo()Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-result-object v0

    .line 1572
    .local v0, "hi":Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1, p1, p2, v0}, Lcom/infraware/office/evengine/EvNative;->IGetHyperLinkInfoEx(IILcom/infraware/office/evengine/EV$HYPERLINK_INFO;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1573
    const/4 v1, 0x0

    iput v1, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->bUse:I

    .line 1577
    :goto_0
    return-object v0

    .line 1575
    :cond_0
    const/4 v1, 0x1

    iput v1, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->bUse:I

    goto :goto_0
.end method

.method public IGetHyperLinkInfo_Editor()Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;
    .locals 2

    .prologue
    .line 1086
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getHyperLinkInfo()Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-result-object v0

    .line 1087
    .local v0, "hi":Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1, v0}, Lcom/infraware/office/evengine/EvNative;->IGetHyperLinkInfo(Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1088
    const/4 v0, 0x0

    .line 1089
    .end local v0    # "hi":Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;
    :cond_0
    return-object v0
.end method

.method public IGetHyperLinkInfo_Enable()Z
    .locals 2

    .prologue
    .line 1078
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getHyperLinkInfo()Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-result-object v0

    .line 1079
    .local v0, "hi":Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1, v0}, Lcom/infraware/office/evengine/EvNative;->IGetHyperLinkInfo(Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1080
    const/4 v1, 0x0

    .line 1081
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public IGetImgToPDF([Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "path_src"    # [Ljava/lang/String;
    .param p2, "path_dst"    # Ljava/lang/String;

    .prologue
    .line 2167
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IGetImgToPDF([Ljava/lang/String;Ljava/lang/String;)V

    .line 2168
    return-void
.end method

.method public IGetInfraPenDrawMode()I
    .locals 1

    .prologue
    .line 2433
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetInfraPenDrawMode()I

    move-result v0

    return v0
.end method

.method public IGetInvalidRect_Editor()Lcom/infraware/office/evengine/EV$INVALID_DRAW_INFO;
    .locals 2

    .prologue
    .line 1054
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getInvalidDrawInfo()Lcom/infraware/office/evengine/EV$INVALID_DRAW_INFO;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetInvalidRect(Lcom/infraware/office/evengine/EV$INVALID_DRAW_INFO;)V

    .line 1055
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getInvalidDrawInfo()Lcom/infraware/office/evengine/EV$INVALID_DRAW_INFO;

    move-result-object v0

    return-object v0
.end method

.method public IGetIsSlideHide(I)Z
    .locals 1
    .param p1, "nPage"    # I

    .prologue
    .line 930
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IGetIsSlideHide(I)Z

    move-result v0

    return v0
.end method

.method public IGetIsValidateScreenSlides()Z
    .locals 1

    .prologue
    .line 948
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetIsValidateScreenSlides()Z

    move-result v0

    return v0
.end method

.method public IGetLineSpaceUnitChange(I)I
    .locals 1
    .param p1, "nUnit"    # I

    .prologue
    .line 2111
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IGetLineSpaceUnitChange(I)I

    move-result v0

    return v0
.end method

.method public IGetMarkString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1623
    const/4 v0, 0x0

    .line 1624
    .local v0, "strTemp":Ljava/lang/String;
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvNative;->IGetMarkString()Ljava/lang/String;

    move-result-object v0

    .line 1625
    return-object v0
.end method

.method public IGetMasterSlideImage(II)V
    .locals 1
    .param p1, "nWidth"    # I
    .param p2, "nHeight"    # I

    .prologue
    .line 1747
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IGetMasterSlideImage(II)V

    .line 1748
    return-void
.end method

.method public IGetMultiSelectPointInfo(I[I)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "infoArray"    # [I

    .prologue
    .line 2077
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IGetMultiSelectPointInfo(I[I)V

    .line 2078
    return-void
.end method

.method public IGetNextCommentText()V
    .locals 1

    .prologue
    .line 1510
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetNextCommentText()V

    .line 1511
    return-void
.end method

.method public IGetObjectCount()I
    .locals 1

    .prologue
    .line 2549
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetObjectCount()I

    move-result v0

    return v0
.end method

.method public IGetObjectType(II)I
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 2543
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IGetObjectType(II)I

    move-result v0

    return v0
.end method

.method public IGetPDFAnnotationCount()V
    .locals 1

    .prologue
    .line 1917
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetPDFAnnotationCount()V

    .line 1918
    return-void
.end method

.method public IGetPDFAnnotationListItem(ILcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;)V
    .locals 1
    .param p1, "nIndex"    # I
    .param p2, "out_item"    # Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;

    .prologue
    .line 1923
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IGetPDFAnnotationListItem(ILcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;)V

    .line 1924
    return-void
.end method

.method public IGetPDFAuthor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2031
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetPDFAuthor()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public IGetPDFBookmarkCount(J)I
    .locals 1
    .param p1, "a_item"    # J

    .prologue
    .line 1899
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IGetPDFBookmarkCount(J)I

    move-result v0

    return v0
.end method

.method public IGetPDFBookmarkList(JI[Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;)V
    .locals 1
    .param p1, "a_item"    # J
    .param p3, "nIndex"    # I
    .param p4, "out_item"    # [Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;

    .prologue
    .line 1912
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/infraware/office/evengine/EvNative;->IGetPDFBookmarkList(JI[Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;)V

    .line 1913
    return-void
.end method

.method public IGetPDFTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2036
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetPDFTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public IGetPPTChartBorder(I)Z
    .locals 1
    .param p1, "nType"    # I

    .prologue
    .line 2394
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IGetPPTChartBorder(I)Z

    move-result v0

    return v0
.end method

.method public IGetPageDisplayInfo()[Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;
    .locals 3

    .prologue
    .line 1777
    iget-object v2, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV;->getPageDisplayInfo()[Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;

    move-result-object v1

    .line 1778
    .local v1, "info":[Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 1779
    aget-object v2, v1, v0

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->clear()V

    .line 1778
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1782
    :cond_0
    iget-object v2, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v2, v1}, Lcom/infraware/office/evengine/EvNative;->IGetPageDisplayInfo([Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;)V

    .line 1783
    return-object v1
.end method

.method public IGetPageThumbnail(IIIILjava/lang/String;I)V
    .locals 8
    .param p1, "a_nSaveMode"    # I
    .param p2, "a_nPageNum"    # I
    .param p3, "a_nWidth"    # I
    .param p4, "a_nHeight"    # I
    .param p5, "a_sOutputPath"    # Ljava/lang/String;
    .param p6, "nOption"    # I

    .prologue
    .line 381
    sget-object v7, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mEvLock:Ljava/lang/Object;

    monitor-enter v7

    .line 382
    :try_start_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvNative;->IGetPageThumbnail(IIIILjava/lang/String;I)V

    .line 383
    monitor-exit v7

    .line 384
    return-void

    .line 383
    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public IGetPageToBitmap(III)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "page"    # I

    .prologue
    .line 2173
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvNative;->IGetPageToBitmap(III)V

    .line 2174
    return-void
.end method

.method public IGetPaperInfo(Lcom/infraware/office/evengine/EV$PAPER_INFO;)V
    .locals 1
    .param p1, "a_pPaperInfo"    # Lcom/infraware/office/evengine/EV$PAPER_INFO;

    .prologue
    .line 1765
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IGetPaperInfo(Lcom/infraware/office/evengine/EV$PAPER_INFO;)V

    .line 1766
    return-void
.end method

.method public IGetParaAttInfo_Editor()Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;
    .locals 2

    .prologue
    .line 1093
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getGuiSetParaAttEvent()Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetParaAttInfo(Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;)Z

    .line 1094
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getGuiSetParaAttEvent()Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;

    move-result-object v0

    return-object v0
.end method

.method public IGetPrevCommentText()V
    .locals 1

    .prologue
    .line 1506
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetPrevCommentText()V

    .line 1507
    return-void
.end method

.method public IGetProperties()Lcom/infraware/office/evengine/EV$PROPERTIES;
    .locals 3

    .prologue
    .line 494
    sget-object v1, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mEvLock:Ljava/lang/Object;

    monitor-enter v1

    .line 495
    :try_start_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v2, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV;->getProperties()Lcom/infraware/office/evengine/EV$PROPERTIES;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/infraware/office/evengine/EvNative;->IGetProperties(Lcom/infraware/office/evengine/EV$PROPERTIES;)V

    .line 496
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getProperties()Lcom/infraware/office/evengine/EV$PROPERTIES;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 497
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public IGetRulerbarImage(II)V
    .locals 1
    .param p1, "nWidth"    # I
    .param p2, "nHeight"    # I

    .prologue
    .line 1751
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IGetRulerbarImage(II)V

    .line 1752
    return-void
.end method

.method public IGetRulerbarPgInfo(Lcom/infraware/office/evengine/EV$RULERBAR_PAGE_INFO;)V
    .locals 1
    .param p1, "aRulerPageInfo"    # Lcom/infraware/office/evengine/EV$RULERBAR_PAGE_INFO;

    .prologue
    .line 1755
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IGetRulerbarPgInfo(Lcom/infraware/office/evengine/EV$RULERBAR_PAGE_INFO;)V

    .line 1756
    return-void
.end method

.method public IGetScreenPos()Lcom/infraware/office/evengine/EV$SCREEN_INFO;
    .locals 1

    .prologue
    .line 1008
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getScreenInfo()Lcom/infraware/office/evengine/EV$SCREEN_INFO;

    move-result-object v0

    return-object v0
.end method

.method public IGetScrollInfo_Editor()Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;
    .locals 1

    .prologue
    .line 1098
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getScrollInfoEditor()Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;

    move-result-object v0

    return-object v0
.end method

.method public IGetSectionInfo()Lcom/infraware/office/evengine/EV$SECTION_INFO;
    .locals 1

    .prologue
    .line 2631
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->IGetSectionInfo(I)Lcom/infraware/office/evengine/EV$SECTION_INFO;

    move-result-object v0

    return-object v0
.end method

.method public IGetSectionInfo(I)Lcom/infraware/office/evengine/EV$SECTION_INFO;
    .locals 2
    .param p1, "nPageNum"    # I

    .prologue
    .line 2636
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getSectionInfo()Lcom/infraware/office/evengine/EV$SECTION_INFO;

    move-result-object v0

    .line 2637
    .local v0, "sectionInfo":Lcom/infraware/office/evengine/EV$SECTION_INFO;
    iput p1, v0, Lcom/infraware/office/evengine/EV$SECTION_INFO;->nTargetPageNum:I

    .line 2639
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1, v0}, Lcom/infraware/office/evengine/EvNative;->IGetSectionInfo(Lcom/infraware/office/evengine/EV$SECTION_INFO;)V

    .line 2640
    return-object v0
.end method

.method public IGetSeparateMarkString_Editor()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1617
    const/4 v0, 0x0

    .line 1618
    .local v0, "strTemp":Ljava/lang/String;
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvNative;->IGetSeparateMarkString_Editor()Ljava/lang/String;

    move-result-object v0

    .line 1619
    return-object v0
.end method

.method public IGetShape3DFormatInfo()Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;
    .locals 2

    .prologue
    .line 2264
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getShape3DFormatInfo()Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetShape3DFormatInfo(Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;)V

    .line 2265
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getShape3DFormatInfo()Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;

    move-result-object v0

    return-object v0
.end method

.method public IGetShape3DRotationInfo()Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;
    .locals 2

    .prologue
    .line 2270
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getShape3DRotationInfo()Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetShape3DRotationInfo(Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;)V

    .line 2271
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getShape3DRotationInfo()Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;

    move-result-object v0

    return-object v0
.end method

.method public IGetShapeArtisticEffectInfo()Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;
    .locals 2

    .prologue
    .line 2288
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getShapeArtisticEffectInfo()Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetShapeArtisticEffectInfo(Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;)V

    .line 2289
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getShapeArtisticEffectInfo()Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;

    move-result-object v0

    return-object v0
.end method

.method public IGetShapeCroppingInfo()Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;
    .locals 2

    .prologue
    .line 2294
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getShapeCroppingInfo()Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetShapeCroppingInfo(Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;)V

    .line 2295
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getShapeCroppingInfo()Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;

    move-result-object v0

    return-object v0
.end method

.method public IGetShapeEffect(I)Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;
    .locals 1
    .param p1, "aMask"    # I

    .prologue
    .line 2374
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getShapeEffect()Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;

    move-result-object v0

    return-object v0
.end method

.method public IGetShapeFillInfo()Lcom/infraware/office/evengine/EV$SHAPE_FILL;
    .locals 2

    .prologue
    .line 2222
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getShapeFillInfo()Lcom/infraware/office/evengine/EV$SHAPE_FILL;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetShapeFillInfo(Lcom/infraware/office/evengine/EV$SHAPE_FILL;)V

    .line 2223
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getShapeFillInfo()Lcom/infraware/office/evengine/EV$SHAPE_FILL;

    move-result-object v0

    return-object v0
.end method

.method public IGetShapeGlowInfo()Lcom/infraware/office/evengine/EV$SHAPE_GLOW;
    .locals 2

    .prologue
    .line 2252
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getShapeGlowInfo()Lcom/infraware/office/evengine/EV$SHAPE_GLOW;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetShapeGlowInfo(Lcom/infraware/office/evengine/EV$SHAPE_GLOW;)V

    .line 2253
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getShapeGlowInfo()Lcom/infraware/office/evengine/EV$SHAPE_GLOW;

    move-result-object v0

    return-object v0
.end method

.method public IGetShapeLineColorInfo()Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;
    .locals 2

    .prologue
    .line 2228
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getShapeLineColorInfo()Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetShapeLineColorInfo(Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;)V

    .line 2229
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getShapeLineColorInfo()Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;

    move-result-object v0

    return-object v0
.end method

.method public IGetShapeLineStyleInfo()Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;
    .locals 2

    .prologue
    .line 2234
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getShapeLineStyleInfo()Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetShapeLineStyleInfo(Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;)V

    .line 2235
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getShapeLineStyleInfo()Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;

    move-result-object v0

    return-object v0
.end method

.method public IGetShapeLocationInfo()Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;
    .locals 2

    .prologue
    .line 2306
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getShapeLocationInfo()Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetShapeLocationInfo(Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;)V

    .line 2307
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getShapeLocationInfo()Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;

    move-result-object v0

    return-object v0
.end method

.method public IGetShapePictureColorInfo()Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;
    .locals 2

    .prologue
    .line 2282
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getShapePictureColorInfo()Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetShapePictureColorInfo(Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;)V

    .line 2283
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getShapePictureColorInfo()Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;

    move-result-object v0

    return-object v0
.end method

.method public IGetShapePictureCorrectionInfo()Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;
    .locals 2

    .prologue
    .line 2276
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getShapePictureCorrectionInfo()Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetShapePictureCorrectionInfo(Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;)V

    .line 2277
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getShapePictureCorrectionInfo()Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;

    move-result-object v0

    return-object v0
.end method

.method public IGetShapeQuickStyleInfo()Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;
    .locals 2

    .prologue
    .line 2216
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getShapeQuickStyleInfo()Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetShapeQuickStyleInfo(Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;)V

    .line 2217
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getShapeQuickStyleInfo()Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;

    move-result-object v0

    return-object v0
.end method

.method public IGetShapeReflectionInfo()Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;
    .locals 2

    .prologue
    .line 2246
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getShapeReflectionInfo()Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetShapeReflectionInfo(Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;)V

    .line 2247
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getShapeReflectionInfo()Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;

    move-result-object v0

    return-object v0
.end method

.method public IGetShapeShadowInfo()Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;
    .locals 2

    .prologue
    .line 2240
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getShapeShadowInfo()Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetShapeShadowInfo(Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;)V

    .line 2241
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getShapeShadowInfo()Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;

    move-result-object v0

    return-object v0
.end method

.method public IGetShapeSizeInfo()Lcom/infraware/office/evengine/EV$SHAPE_SIZE;
    .locals 2

    .prologue
    .line 2300
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getShapeSizeInfo()Lcom/infraware/office/evengine/EV$SHAPE_SIZE;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetShapeSizeInfo(Lcom/infraware/office/evengine/EV$SHAPE_SIZE;)V

    .line 2301
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getShapeSizeInfo()Lcom/infraware/office/evengine/EV$SHAPE_SIZE;

    move-result-object v0

    return-object v0
.end method

.method public IGetShapeSoftEdgeInfo()Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;
    .locals 2

    .prologue
    .line 2258
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getShapeSoftEdgeInfo()Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetShapeSoftEdgeInfo(Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;)V

    .line 2259
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getShapeSoftEdgeInfo()Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;

    move-result-object v0

    return-object v0
.end method

.method public IGetShapeStyleNum()I
    .locals 1

    .prologue
    .line 1789
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetShapeStyleNum()I

    move-result v0

    return v0
.end method

.method public IGetShapeTextBoxInfo()Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;
    .locals 2

    .prologue
    .line 2312
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getShapeTextboxInfo()Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetShapeTextBoxInfo(Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;)V

    .line 2313
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getShapeTextboxInfo()Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;

    move-result-object v0

    return-object v0
.end method

.method public IGetSheetCount()I
    .locals 1

    .prologue
    .line 1409
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetSheetCount()I

    move-result v0

    return v0
.end method

.method public IGetSheetHyperLinkInfo()Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;
    .locals 2

    .prologue
    .line 1449
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getSheetHyperLinkInfo()Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetSheetHyperLinkInfo(Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;)V

    .line 1450
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getSheetHyperLinkInfo()Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    move-result-object v0

    return-object v0
.end method

.method public IGetSheetInfo(Lcom/infraware/office/evengine/EV$SHEET_INFO;I)V
    .locals 1
    .param p1, "a_pSheetInfo"    # Lcom/infraware/office/evengine/EV$SHEET_INFO;
    .param p2, "a_nIndex"    # I

    .prologue
    .line 1437
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IGetSheetInfo(Lcom/infraware/office/evengine/EV$SHEET_INFO;I)V

    .line 1438
    return-void
.end method

.method public IGetSheetNameList()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 1415
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetSheetNameList()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public IGetSheetPdfPrintTotalPage(I)I
    .locals 1
    .param p1, "nPrintOptions"    # I

    .prologue
    .line 1609
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IGetSheetPdfPrintTotalPage(I)I

    move-result v0

    return v0
.end method

.method public IGetSheetPrintTotalPage(IIIIIII)I
    .locals 8
    .param p1, "nPaperSize"    # I
    .param p2, "nWidth"    # I
    .param p3, "nHeight"    # I
    .param p4, "nLeftPrintMargin"    # I
    .param p5, "nTopPrintMargin"    # I
    .param p6, "nRightPrintMargin"    # I
    .param p7, "nBottomPrintMargin"    # I

    .prologue
    .line 1605
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/infraware/office/evengine/EvNative;->IGetSheetPrintTotalPage(IIIIIII)I

    move-result v0

    return v0
.end method

.method public IGetSheetTextboxRectInfo([I)I
    .locals 1
    .param p1, "a_rect"    # [I

    .prologue
    .line 1297
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IGetSheetTextboxRectInfo([I)I

    move-result v0

    return v0
.end method

.method public IGetSlideAnimationList_Count()I
    .locals 1

    .prologue
    .line 987
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetSlideAnimationList_Count()I

    move-result v0

    return v0
.end method

.method public IGetSlideAreaForSlideShow(Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "a_info"    # Landroid/graphics/Rect;

    .prologue
    .line 990
    sget-object v1, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mEvLock:Ljava/lang/Object;

    monitor-enter v1

    .line 991
    :try_start_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IGetSlideAreaForSlideShow(Landroid/graphics/Rect;)V

    .line 992
    monitor-exit v1

    .line 993
    return-void

    .line 992
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public IGetSlideBackgroundColor(I)I
    .locals 1
    .param p1, "nPage"    # I

    .prologue
    .line 942
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IGetSlideBackgroundColor(I)I

    move-result v0

    return v0
.end method

.method public IGetSlideLayout(I)I
    .locals 1
    .param p1, "nPage"    # I

    .prologue
    .line 936
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IGetSlideLayout(I)I

    move-result v0

    return v0
.end method

.method public IGetSlideNoteString_Editor(I)Ljava/lang/String;
    .locals 2
    .param p1, "a_nPageNum"    # I

    .prologue
    .line 1116
    const/4 v0, 0x0

    .line 1118
    .local v0, "strTemp":Ljava/lang/String;
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1, p1}, Lcom/infraware/office/evengine/EvNative;->IGetSlideNoteStringEx(I)Ljava/lang/String;

    move-result-object v0

    .line 1119
    return-object v0
.end method

.method public IGetSlideShowEffect(I)Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;
    .locals 2
    .param p1, "nPage"    # I

    .prologue
    .line 951
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getTransitionInfo()Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/infraware/office/evengine/EvNative;->IGetSlideShowEffect(ILcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;)V

    .line 952
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getTransitionInfo()Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;

    move-result-object v0

    return-object v0
.end method

.method public IGetSlideShowPlay(II)I
    .locals 1
    .param p1, "nPageNum"    # I
    .param p2, "nAnimationIndex"    # I

    .prologue
    .line 849
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IGetSlideShowPlay(II)I

    move-result v0

    return v0
.end method

.method public IGetSlideShowVideoInfo(IILandroid/graphics/Rect;)Ljava/lang/String;
    .locals 1
    .param p1, "a_nX"    # I
    .param p2, "a_nY"    # I
    .param p3, "rcVideo"    # Landroid/graphics/Rect;

    .prologue
    .line 859
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvNative;->IGetSlideShowVideoInfo(IILandroid/graphics/Rect;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public IGetSortRange(Lcom/infraware/office/evengine/EV$RANGE;I)I
    .locals 2
    .param p1, "a_ppRange"    # Lcom/infraware/office/evengine/EV$RANGE;
    .param p2, "a_bExtendRange"    # I

    .prologue
    .line 1422
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IGetSortRange(Lcom/infraware/office/evengine/EV$RANGE;I)I

    move-result v0

    .line 1423
    .local v0, "nRet":I
    return v0
.end method

.method public IGetSummaryData()Lcom/infraware/office/evengine/EV$SUMMARY_DATA;
    .locals 2

    .prologue
    .line 2523
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getSummaryData()Lcom/infraware/office/evengine/EV$SUMMARY_DATA;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetSummaryData(Lcom/infraware/office/evengine/EV$SUMMARY_DATA;)V

    .line 2524
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getSummaryData()Lcom/infraware/office/evengine/EV$SUMMARY_DATA;

    move-result-object v0

    return-object v0
.end method

.method public IGetSystemFontCount()I
    .locals 1

    .prologue
    .line 1522
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetSystemFontCount()I

    move-result v0

    return v0
.end method

.method public IGetSystemFontNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 1525
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetSystemFontNames()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public IGetTableMode()I
    .locals 1

    .prologue
    .line 2352
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetTableDrawMode()I

    move-result v0

    return v0
.end method

.method public IGetTableStyleInfo()Lcom/infraware/office/evengine/EV$TABLE_STYLE_INFO;
    .locals 2

    .prologue
    .line 1801
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getTableStyleInfo()Lcom/infraware/office/evengine/EV$TABLE_STYLE_INFO;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->IGetTableStyleInfo(Lcom/infraware/office/evengine/EV$TABLE_STYLE_INFO;)V

    .line 1802
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getTableStyleInfo()Lcom/infraware/office/evengine/EV$TABLE_STYLE_INFO;

    move-result-object v0

    return-object v0
.end method

.method public IGetTextMarkRectInfo([SI)I
    .locals 1
    .param p1, "TextRectInfos"    # [S
    .param p2, "short_len"    # I

    .prologue
    .line 2095
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IGetTextMarkRectInfo([SI)I

    move-result v0

    return v0
.end method

.method public IGetTextToSpeachString(I)V
    .locals 1
    .param p1, "nTTSMode"    # I

    .prologue
    .line 1635
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IGetTextToSpeachString(I)V

    .line 1636
    return-void
.end method

.method public IGetTextWrapType()I
    .locals 1

    .prologue
    .line 1818
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetTextWrapType()I

    move-result v0

    return v0
.end method

.method public IGetTopRedoDataInfo()I
    .locals 1

    .prologue
    .line 612
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetTopRedoDataInfo()I

    move-result v0

    return v0
.end method

.method public IGetTopUndoDataInfo()I
    .locals 1

    .prologue
    .line 609
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetTopUndoDataInfo()I

    move-result v0

    return v0
.end method

.method public IGetTouchInfo(Lcom/infraware/office/evengine/EV$TOUCH_INFO;)V
    .locals 1
    .param p1, "a_info"    # Lcom/infraware/office/evengine/EV$TOUCH_INFO;

    .prologue
    .line 2156
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IGetTouchInfo(Lcom/infraware/office/evengine/EV$TOUCH_INFO;)V

    .line 2157
    return-void
.end method

.method public IGetTouchInfoEX(Lcom/infraware/office/evengine/EV$TOUCH_INFO;)V
    .locals 1
    .param p1, "a_info"    # Lcom/infraware/office/evengine/EV$TOUCH_INFO;

    .prologue
    .line 2161
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IGetTouchInfoEX(Lcom/infraware/office/evengine/EV$TOUCH_INFO;)V

    .line 2162
    return-void
.end method

.method public IGetTouchString(II)Ljava/lang/String;
    .locals 2
    .param p1, "nSx"    # I
    .param p2, "nSy"    # I

    .prologue
    .line 1629
    const/4 v0, 0x0

    .line 1630
    .local v0, "strTemp":Ljava/lang/String;
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IGetTouchString(II)Ljava/lang/String;

    move-result-object v0

    .line 1631
    return-object v0
.end method

.method public IGetUseFontCount()I
    .locals 1

    .prologue
    .line 1516
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetUseFontCount()I

    move-result v0

    return v0
.end method

.method public IGetUseFontNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 1519
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetUseFontNames()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public IGetVideoPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 999
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetVideoPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public IGetVideoRect(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "rcVideo"    # Landroid/graphics/Rect;

    .prologue
    .line 996
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IGetVideoRect(Landroid/graphics/Rect;)V

    .line 997
    return-void
.end method

.method public IGetViewOption()I
    .locals 1

    .prologue
    .line 2115
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetViewOption()I

    move-result v0

    return v0
.end method

.method public IGetWrongSpell(II)Ljava/lang/String;
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 2498
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IGetWrongSpell(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public IGotoAnnotation(IIIIFFFFZ)V
    .locals 10
    .param p1, "nAction"    # I
    .param p2, "nAnnotType"    # I
    .param p3, "nPageNum"    # I
    .param p4, "nAnnotIndex"    # I
    .param p5, "left"    # F
    .param p6, "top"    # F
    .param p7, "right"    # F
    .param p8, "bottom"    # F
    .param p9, "bClicked"    # Z

    .prologue
    .line 1935
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    invoke-virtual/range {v0 .. v9}, Lcom/infraware/office/evengine/EvNative;->IGotoAnnotation(IIIIFFFFZ)V

    .line 1936
    return-void
.end method

.method public IGotoPDFBookmark(J)V
    .locals 1
    .param p1, "a_item"    # J

    .prologue
    .line 1928
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IGotoPDFBookmark(J)V

    .line 1929
    return-void
.end method

.method public IHIDAction(IIIIII)V
    .locals 7
    .param p1, "EEV_HID_ACTION"    # I
    .param p2, "a_nXPos"    # I
    .param p3, "a_nYPos"    # I
    .param p4, "a_wModifiers"    # I
    .param p5, "a_nTime"    # I
    .param p6, "a_nPressure"    # I

    .prologue
    .line 693
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvNative;->IHIDAction(IIIIII)V

    .line 694
    return-void
.end method

.method public IHasDocComments()Z
    .locals 1

    .prologue
    .line 2042
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IHasDocComments()Z

    move-result v0

    return v0
.end method

.method public IHasPDFAnnots()Z
    .locals 1

    .prologue
    .line 1947
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IHasPDFAnnots()Z

    move-result v0

    return v0
.end method

.method public IHasPDFText()Z
    .locals 1

    .prologue
    .line 2020
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IHasPDFText()Z

    move-result v0

    return v0
.end method

.method public IHyperLink(III)V
    .locals 1
    .param p1, "EEV_HYPERLIK_MODE"    # I
    .param p2, "a_nXPos"    # I
    .param p3, "a_nYPos"    # I

    .prologue
    .line 413
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvNative;->IHyperLink(III)V

    .line 414
    return-void
.end method

.method public IHyperLinkEnd()V
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IHyperLinkEnd()V

    .line 418
    return-void
.end method

.method public IHyperlinkEditor(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "a_pszHyperText"    # Ljava/lang/String;
    .param p2, "a_pszHyperLink"    # Ljava/lang/String;

    .prologue
    .line 725
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IHyperlinkEditor(Ljava/lang/String;Ljava/lang/String;)V

    .line 726
    return-void
.end method

.method public IImageInsert(Ljava/lang/String;Landroid/graphics/Bitmap;IIZ)V
    .locals 6
    .param p1, "a_pImgPath"    # Ljava/lang/String;
    .param p2, "a_bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "a_nWidth"    # I
    .param p4, "a_nHeight"    # I
    .param p5, "a_bReplace"    # Z

    .prologue
    .line 646
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvNative;->IImageInsert(Ljava/lang/String;Landroid/graphics/Bitmap;IIZ)V

    .line 648
    return-void
.end method

.method public IIndentation(I)V
    .locals 1
    .param p1, "EEV_IDENTATION"    # I

    .prologue
    .line 719
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IIndentation(I)V

    .line 720
    return-void
.end method

.method public IInfraPenAllErase()V
    .locals 1

    .prologue
    .line 2492
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IInfraPenAllErase()V

    .line 2493
    return-void
.end method

.method public IInitialize(II)V
    .locals 9
    .param p1, "a_nWidth"    # I
    .param p2, "a_nHeight"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x0

    .line 132
    invoke-super {p0, p1, p2}, Lcom/infraware/office/evengine/EvInterface;->IInitialize(II)V

    .line 140
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    const/16 v3, 0x8

    const/16 v6, 0x10

    move v1, p1

    move v2, p2

    move v5, v4

    move v8, v7

    invoke-virtual/range {v0 .. v8}, Lcom/infraware/office/evengine/EvNative;->IInitialize(IIIIIIII)V

    .line 149
    return-void
.end method

.method public IInputChar(I)V
    .locals 1
    .param p1, "a_wCode"    # I

    .prologue
    .line 606
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IInputChar(I)V

    .line 607
    return-void
.end method

.method public IInsertShape(I)V
    .locals 1
    .param p1, "a_nShape"    # I

    .prologue
    .line 806
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IInsertShape(I)V

    .line 807
    return-void
.end method

.method public IInsertShapeStyle(II)V
    .locals 1
    .param p1, "a_nShape"    # I
    .param p2, "a_nStyleNum"    # I

    .prologue
    .line 809
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IInsertShapeStyle(II)V

    .line 810
    return-void
.end method

.method public IInsertString(Ljava/lang/String;III)V
    .locals 1
    .param p1, "aszTemp"    # Ljava/lang/String;
    .param p2, "nCompType"    # I
    .param p3, "nPos"    # I
    .param p4, "nStrLen"    # I

    .prologue
    .line 650
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/infraware/office/evengine/EvNative;->IInsertString(Ljava/lang/String;III)V

    .line 651
    return-void
.end method

.method public IInsertTextBox(Z)V
    .locals 1
    .param p1, "aHori"    # Z

    .prologue
    .line 800
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IInsertTextBox(Z)V

    .line 801
    return-void
.end method

.method public IIsComplexColumn()Z
    .locals 2

    .prologue
    .line 1661
    const/4 v0, 0x0

    .line 1662
    .local v0, "nRet":Z
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvNative;->IIsComplexColumn()Z

    move-result v0

    .line 1663
    return v0
.end method

.method public IIsContinuePageView_Editor()I
    .locals 1

    .prologue
    .line 2572
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IIsContinuePageView_Editor()I

    move-result v0

    return v0
.end method

.method public IIsItalicAttr()I
    .locals 1

    .prologue
    .line 1135
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IIsItalicAttr()I

    move-result v0

    return v0
.end method

.method public IIsLastSlideShow(Z)I
    .locals 2
    .param p1, "bInPage"    # Z

    .prologue
    .line 902
    sget-object v1, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mEvLock:Ljava/lang/Object;

    monitor-enter v1

    .line 903
    :try_start_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IIsLastSlideShow(Z)I

    move-result v0

    monitor-exit v1

    return v0

    .line 904
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized IIsNextEffect()Z
    .locals 2

    .prologue
    .line 869
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mEvLock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 870
    :try_start_1
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IIsNextEffect()Z

    move-result v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return v0

    .line 871
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 869
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized IIsNoneEffect(II)Z
    .locals 2
    .param p1, "a_PlayType"    # I
    .param p2, "a_nPage"    # I

    .prologue
    .line 863
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mEvLock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 864
    :try_start_1
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IIsNoneEffect(II)Z

    move-result v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return v0

    .line 865
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 863
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public IIsPDFSaveAble()Z
    .locals 1

    .prologue
    .line 2000
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IIsPDFSaveAble()Z

    move-result v0

    return v0
.end method

.method public IIsPenDataForFreeDraw()I
    .locals 1

    .prologue
    .line 1729
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IIsPenDataForFreeDraw()I

    move-result v0

    return v0
.end method

.method public IIsPenDataForSlideShow()I
    .locals 1

    .prologue
    .line 1721
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->IIsPenDataForSlideShow(I)I

    move-result v0

    return v0
.end method

.method public IIsPenDataForSlideShow(I)I
    .locals 1
    .param p1, "nPage"    # I

    .prologue
    .line 1725
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IIsPenDataForSlideShow(I)I

    move-result v0

    return v0
.end method

.method public IIsSlideShow()Z
    .locals 1

    .prologue
    .line 875
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IIsSlideShow()Z

    move-result v0

    return v0
.end method

.method public IIsSlideShowing()Z
    .locals 1

    .prologue
    .line 878
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IIsSlideShowing()Z

    move-result v0

    return v0
.end method

.method public IIsSupportFocusTextToSpeach()Z
    .locals 1

    .prologue
    .line 1639
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IIsSupportFocusTextToSpeach()Z

    move-result v0

    return v0
.end method

.method public IIsWaitingFlag()Z
    .locals 1

    .prologue
    .line 881
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IIsWaitingFlag()Z

    move-result v0

    return v0
.end method

.method public declared-synchronized IIsWaitingStatus()Z
    .locals 2

    .prologue
    .line 885
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mEvLock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 886
    :try_start_1
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IIsWaitingStatus()Z

    move-result v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return v0

    .line 887
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 885
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public IMemo(ILjava/lang/String;IIIII)V
    .locals 0
    .param p1, "EEV_MEMO_TYPE"    # I
    .param p2, "a_sText"    # Ljava/lang/String;
    .param p3, "a_nSelIndex"    # I
    .param p4, "a_nXpos"    # I
    .param p5, "a_nYpos"    # I
    .param p6, "a_nIconW"    # I
    .param p7, "a_nIconH"    # I

    .prologue
    .line 454
    return-void
.end method

.method public IMemoCommand(ILcom/infraware/office/evengine/EV$MEMO_CMD_DATA;)Z
    .locals 1
    .param p1, "command_type"    # I
    .param p2, "memo"    # Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;

    .prologue
    .line 2537
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IMemoCommand(ILcom/infraware/office/evengine/EV$MEMO_CMD_DATA;)Z

    move-result v0

    return v0
.end method

.method public IModifyPDFAnnotation(ILjava/lang/String;I)V
    .locals 1
    .param p1, "AnnotItem"    # I
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "color"    # I

    .prologue
    .line 1958
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvNative;->IModifyPDFAnnotation(ILjava/lang/String;I)V

    .line 1959
    return-void
.end method

.method public IMovePage(II)V
    .locals 1
    .param p1, "EEV_MOVE_TYPE"    # I
    .param p2, "a_nPage"    # I

    .prologue
    .line 196
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IMovePage(II)V

    .line 197
    return-void
.end method

.method public INewDocument(Ljava/lang/String;IIIIIILjava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "a_sFilePath"    # Ljava/lang/String;
    .param p2, "a_nWidth"    # I
    .param p3, "a_nHeight"    # I
    .param p4, "a_eLoadType"    # I
    .param p5, "a_eNewTemplatePPT"    # I
    .param p6, "a_nLocale"    # I
    .param p7, "bLandScape"    # I
    .param p8, "a_sTempPath"    # Ljava/lang/String;
    .param p9, "a_sBookMarkPath"    # Ljava/lang/String;

    .prologue
    .line 527
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-virtual/range {v0 .. v9}, Lcom/infraware/office/evengine/EvNative;->INewDocument(Ljava/lang/String;IIIIIILjava/lang/String;Ljava/lang/String;)V

    .line 536
    return-void
.end method

.method public INoMarginView()V
    .locals 1

    .prologue
    .line 1906
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->INoMarginView()V

    .line 1907
    return-void
.end method

.method public IOpen(Ljava/lang/String;IIIIILjava/lang/String;Ljava/lang/String;)V
    .locals 19
    .param p1, "a_sFilePath"    # Ljava/lang/String;
    .param p2, "a_nWidth"    # I
    .param p3, "a_nHeight"    # I
    .param p4, "EEV_FILE_LOAD_TYPE"    # I
    .param p5, "a_nLocale"    # I
    .param p6, "bLandScape"    # I
    .param p7, "a_sTempPath"    # Ljava/lang/String;
    .param p8, "a_sBookMarkPath"    # Ljava/lang/String;

    .prologue
    .line 170
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p7

    invoke-virtual {v0, v1, v2}, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->AddOpendFileList(Ljava/lang/String;Ljava/lang/String;)Z

    .line 171
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v4, p1

    move/from16 v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    move/from16 v15, p5

    move/from16 v16, p6

    move-object/from16 v17, p7

    move-object/from16 v18, p8

    invoke-virtual/range {v3 .. v18}, Lcom/infraware/office/evengine/EvNative;->IOpen(Ljava/lang/String;IIIIIIIIIIIILjava/lang/String;Ljava/lang/String;)V

    .line 176
    return-void
.end method

.method public IOpenEx(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "a_sFilePath"    # Ljava/lang/String;
    .param p2, "a_sPassword"    # Ljava/lang/String;
    .param p3, "a_sTempPath"    # Ljava/lang/String;

    .prologue
    .line 178
    invoke-virtual {p0, p1, p3}, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->AddOpendFileList(Ljava/lang/String;Ljava/lang/String;)Z

    .line 179
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IOpenEx(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    return-void
.end method

.method public IPDFMapRectToView(IIIIII[I)V
    .locals 8
    .param p1, "pageNum"    # I
    .param p2, "pageRectLeft"    # I
    .param p3, "pageRectTop"    # I
    .param p4, "pageRectRight"    # I
    .param p5, "pageRectBottom"    # I
    .param p6, "style"    # I
    .param p7, "tempobj"    # [I

    .prologue
    .line 2007
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/infraware/office/evengine/EvNative;->IPDFMapRectToView(IIIIII[I)V

    .line 2009
    return-void
.end method

.method public IPDFMapRectToViewEX(I[I)V
    .locals 1
    .param p1, "AnnotItem"    # I
    .param p2, "tempobj"    # [I

    .prologue
    .line 2014
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IPDFMapRectToViewEX(I[I)V

    .line 2015
    return-void
.end method

.method public IPDFSaveAnnot()V
    .locals 1

    .prologue
    .line 1994
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IPDFSaveAnnot()V

    .line 1995
    return-void
.end method

.method public IPDFUpdated()Z
    .locals 1

    .prologue
    .line 1988
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IPDFUpdated()Z

    move-result v0

    return v0
.end method

.method public IPageModified_Editor(I)Z
    .locals 2
    .param p1, "a_nPageNum"    # I

    .prologue
    .line 1063
    const/4 v0, 0x0

    .line 1064
    .local v0, "nRet":Z
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1, p1}, Lcom/infraware/office/evengine/EvNative;->IPageModified(I)Z

    move-result v0

    .line 1065
    return v0
.end method

.method public IParagraphAlign(I)V
    .locals 1
    .param p1, "EEV_PARAGRAPH_ALIGN"    # I

    .prologue
    .line 653
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IParagraphAlign(I)V

    .line 654
    return-void
.end method

.method public IPivotScreen(III)V
    .locals 1
    .param p1, "bLandScape"    # I
    .param p2, "a_nWidth"    # I
    .param p3, "a_nHeight"    # I

    .prologue
    .line 322
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvNative;->IPivotScreen(III)V

    .line 323
    return-void
.end method

.method public IPopupOffset(IIIII)V
    .locals 6
    .param p1, "EEV_POPUP_ONOFF"    # I
    .param p2, "a_nLeft"    # I
    .param p3, "a_nRight"    # I
    .param p4, "a_nTop"    # I
    .param p5, "a_nBottom"    # I

    .prologue
    .line 728
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvNative;->IPopupOffset(IIIII)V

    .line 729
    return-void
.end method

.method public IPrint(IIILjava/lang/String;)V
    .locals 0
    .param p1, "EEV_PRIT_PAPER_TYPE"    # I
    .param p2, "a_nStartPage"    # I
    .param p3, "a_nEndPage"    # I
    .param p4, "a_sFilePath"    # Ljava/lang/String;

    .prologue
    .line 472
    return-void
.end method

.method public IReDraw()V
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IReDraw()V

    .line 297
    return-void
.end method

.method public IRedoUndo(I)V
    .locals 1
    .param p1, "EEV_REDO_UNDO"    # I

    .prologue
    .line 603
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IRedoUndo(I)V

    .line 604
    return-void
.end method

.method public IRemoveAllBookMark()V
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IRemoveAllBookMark()V

    .line 404
    return-void
.end method

.method public IRemoveAllPDFAnnotation()V
    .locals 1

    .prologue
    .line 1970
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IRemoveAllPDFAnnotation()V

    .line 1971
    return-void
.end method

.method public IRemoveBookMark(Ljava/lang/String;)V
    .locals 1
    .param p1, "a_sFilePath"    # Ljava/lang/String;

    .prologue
    .line 399
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IRemoveBookMark(Ljava/lang/String;)V

    .line 400
    return-void
.end method

.method public IRemovePDFAnnotation(I)V
    .locals 1
    .param p1, "AnnotItem"    # I

    .prologue
    .line 1964
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IRemovePDFAnnotation(I)V

    .line 1965
    return-void
.end method

.method public IReplaceWrongSpell(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "wrongSpell"    # Ljava/lang/String;
    .param p2, "ReplaceSpell"    # Ljava/lang/String;

    .prologue
    .line 2505
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IReplaceWrongSpell(Ljava/lang/String;Ljava/lang/String;)V

    .line 2506
    return-void
.end method

.method public IRotateFrame(I)V
    .locals 1
    .param p1, "a_nAngle"    # I

    .prologue
    .line 293
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IRotateFrame(I)V

    .line 294
    return-void
.end method

.method public IRotatePage(II)V
    .locals 1
    .param p1, "EEV_ROTATE_TYPE"    # I
    .param p2, "a_nAngle"    # I

    .prologue
    .line 289
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IRotatePage(II)V

    .line 290
    return-void
.end method

.method public ISaveBookMark()V
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISaveBookMark()V

    .line 392
    return-void
.end method

.method public ISaveDocument(Ljava/lang/String;)V
    .locals 3
    .param p1, "a_pszFilePath"    # Ljava/lang/String;

    .prologue
    .line 539
    const-string/jumbo v0, "EvCompinterface"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "ISaveDocument = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 540
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/infraware/office/evengine/EvNative;->ISaveDocument(Ljava/lang/String;I)V

    .line 541
    return-void
.end method

.method public ISaveDocument(Ljava/lang/String;I)V
    .locals 3
    .param p1, "a_pszFilePath"    # Ljava/lang/String;
    .param p2, "nSaveOption"    # I

    .prologue
    .line 549
    const-string/jumbo v0, "EvCompinterface"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "ISaveDocument = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->ISaveDocument(Ljava/lang/String;I)V

    .line 551
    return-void
.end method

.method public ISaveThumbnailAt(IIIIILjava/lang/String;I)V
    .locals 8
    .param p1, "a_nPageSize"    # I
    .param p2, "a_nStartPage"    # I
    .param p3, "a_nEndPage"    # I
    .param p4, "a_nWidth"    # I
    .param p5, "a_nHeight"    # I
    .param p6, "a_szOutputPath"    # Ljava/lang/String;
    .param p7, "a_nSaveType"    # I

    .prologue
    .line 1613
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    move v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/infraware/office/evengine/EvNative;->ISaveThumbnailAt(IIIIILjava/lang/String;I)V

    .line 1614
    return-void
.end method

.method public IScreenCaptureModeOnOff(I)V
    .locals 0
    .param p1, "a_bOn"    # I

    .prologue
    .line 328
    return-void
.end method

.method public IScroll(IIIII)V
    .locals 6
    .param p1, "EEV_SCROLL_COMMAND_TYPE"    # I
    .param p2, "EEV_SCROLL_FACTOR_TYPE"    # I
    .param p3, "a_nOffsetX"    # I
    .param p4, "a_nOffsetY"    # I
    .param p5, "EEV_KEY_TYPE"    # I

    .prologue
    .line 269
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvNative;->IScroll(IIIII)V

    .line 270
    return-void
.end method

.method public IScrollAndFitToWidth(IIII)V
    .locals 1
    .param p1, "nPosX"    # I
    .param p2, "nPosY"    # I
    .param p3, "nWidth"    # I
    .param p4, "serverDeviceWidth"    # I

    .prologue
    .line 2460
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/infraware/office/evengine/EvNative;->IScrollAndFitToWidth(IIII)V

    .line 2461
    return-void
.end method

.method public ISearchStart(Ljava/lang/String;IIII)V
    .locals 6
    .param p1, "a_sFind"    # Ljava/lang/String;
    .param p2, "a_bMatchWord"    # I
    .param p3, "a_bCase"    # I
    .param p4, "a_bDirUp"    # I
    .param p5, "a_bFixedZoom"    # I

    .prologue
    .line 421
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvNative;->ISearchStart(Ljava/lang/String;IIII)V

    .line 422
    return-void
.end method

.method public ISearchStop()V
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISearchStop()V

    .line 426
    return-void
.end method

.method public ISelectAll()V
    .locals 1

    .prologue
    .line 763
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISelectAll()V

    .line 764
    return-void
.end method

.method public ISellectAll()V
    .locals 1

    .prologue
    .line 1139
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISellectAll()V

    .line 1140
    return-void
.end method

.method public ISetAnimationDelete(I)V
    .locals 1
    .param p1, "nIndex"    # I

    .prologue
    .line 984
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetAnimationDelete(I)V

    .line 985
    return-void
.end method

.method public ISetAnimationMove(II)V
    .locals 1
    .param p1, "nIndex"    # I
    .param p2, "nMIndex"    # I

    .prologue
    .line 981
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->ISetAnimationMove(II)V

    .line 982
    return-void
.end method

.method public ISetAutofilterButtonConfiguration(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "normal"    # Ljava/lang/String;
    .param p2, "pressed"    # Ljava/lang/String;

    .prologue
    .line 1261
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->ISetAutofilterButtonConfiguration(Ljava/lang/String;Ljava/lang/String;)V

    .line 1262
    return-void
.end method

.method public ISetBookClip(Lcom/infraware/office/evengine/EV$BOOK_CLIP;)I
    .locals 1
    .param p1, "a_pClip"    # Lcom/infraware/office/evengine/EV$BOOK_CLIP;

    .prologue
    .line 1533
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetBookClip(Lcom/infraware/office/evengine/EV$BOOK_CLIP;)I

    move-result v0

    return v0
.end method

.method public ISetBookmarkLabel(Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;I)V
    .locals 1
    .param p1, "a_pLabel"    # Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;
    .param p2, "a_bSaveBookmarkInfo"    # I

    .prologue
    .line 1024
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->ISetBookmarkLabel(Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;I)V

    .line 1025
    return-void
.end method

.method public ISetBorder(IIIIIIIIIIIZ)V
    .locals 13
    .param p1, "nBMask"    # I
    .param p2, "nBStyle"    # I
    .param p3, "nBLeftColor"    # I
    .param p4, "nBTopColor"    # I
    .param p5, "nBRightColor"    # I
    .param p6, "nBBottomColor"    # I
    .param p7, "nBHoriColor"    # I
    .param p8, "nBVertColor"    # I
    .param p9, "nBDownDiaColor"    # I
    .param p10, "nBUpDiaColor"    # I
    .param p11, "nBCellColor"    # I
    .param p12, "a_bUndo"    # Z

    .prologue
    .line 1846
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    move/from16 v11, p11

    move/from16 v12, p12

    invoke-virtual/range {v0 .. v12}, Lcom/infraware/office/evengine/EvNative;->ISetBorder(IIIIIIIIIIIZ)V

    .line 1848
    return-void
.end method

.method public ISetBwpChart(II[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)V
    .locals 17
    .param p1, "nMaskType"    # I
    .param p2, "nChartType"    # I
    .param p3, "serialDataArray"    # [Ljava/lang/String;
    .param p4, "serialNameArray"    # [Ljava/lang/String;
    .param p5, "itemNameArray"    # [Ljava/lang/String;
    .param p6, "nItemCnt"    # I
    .param p7, "nSerialCnt"    # I
    .param p8, "nSerialIn"    # I
    .param p9, "szTitle"    # Ljava/lang/String;
    .param p10, "szXAxis"    # Ljava/lang/String;
    .param p11, "szYAxis"    # Ljava/lang/String;
    .param p12, "nLegend"    # I
    .param p13, "nDimension"    # I
    .param p14, "nBarType"    # I
    .param p15, "nStyleID"    # I

    .prologue
    .line 1670
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move/from16 v2, p1

    move/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move/from16 v13, p12

    move/from16 v14, p13

    move/from16 v15, p14

    move/from16 v16, p15

    invoke-virtual/range {v1 .. v16}, Lcom/infraware/office/evengine/EvNative;->ISetBwpChart(II[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)V

    .line 1674
    return-void
.end method

.method public ISetCellProperty(IIIIIZ)V
    .locals 7
    .param p1, "nMask"    # I
    .param p2, "nBorderStyle"    # I
    .param p3, "nBorderThickness"    # I
    .param p4, "nBorderColor"    # I
    .param p5, "nFillColor"    # I
    .param p6, "a_bUndo"    # Z

    .prologue
    .line 2049
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvNative;->ISetCellProperty(IIIIIZ)V

    .line 2050
    return-void
.end method

.method public ISetChartEffect(I)V
    .locals 1
    .param p1, "aEffect"    # I

    .prologue
    .line 1689
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetPPTChartEffect(I)V

    .line 1690
    return-void
.end method

.method public ISetChartStyle(I)V
    .locals 1
    .param p1, "aStyleID"    # I

    .prologue
    .line 1684
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetChartStyle(I)V

    .line 1685
    return-void
.end method

.method public ISetClearAllPen()V
    .locals 1

    .prologue
    .line 1713
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISetClearAllPen()V

    .line 1714
    return-void
.end method

.method public ISetColors(III)V
    .locals 1
    .param p1, "nMask"    # I
    .param p2, "nForeColor"    # I
    .param p3, "nBackColor"    # I

    .prologue
    .line 776
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvNative;->ISetColors(III)V

    .line 777
    return-void
.end method

.method public ISetColumn(II)V
    .locals 1
    .param p1, "a_nColCnt"    # I
    .param p2, "a_bAllPage"    # I

    .prologue
    .line 1555
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->ISetColumn(II)V

    .line 1556
    return-void
.end method

.method public ISetCompBackColor(IIIIJJIII)V
    .locals 12
    .param p1, "a_nStart1"    # I
    .param p2, "a_nEnd1"    # I
    .param p3, "a_nStart2"    # I
    .param p4, "a_nEnd2"    # I
    .param p5, "a_dwColor1"    # J
    .param p7, "a_dwColor2"    # J
    .param p9, "a_bApplyFlag"    # I
    .param p10, "a_bDirectDraw"    # I
    .param p11, "a_nFakeCaretIndex"    # I

    .prologue
    .line 1563
    monitor-enter p0

    .line 1564
    :try_start_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move/from16 v4, p4

    move-wide/from16 v5, p5

    move-wide/from16 v7, p7

    move/from16 v9, p9

    move/from16 v10, p10

    move/from16 v11, p11

    invoke-virtual/range {v0 .. v11}, Lcom/infraware/office/evengine/EvNative;->ISetCompBackColor(IIIIJJIII)V

    .line 1565
    monitor-exit p0

    .line 1566
    return-void

    .line 1565
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public ISetCroppingMode(II)V
    .locals 1
    .param p1, "bCrop"    # I
    .param p2, "bApply"    # I

    .prologue
    .line 2073
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->ISetCroppingMode(II)V

    .line 2074
    return-void
.end method

.method public ISetDrawCellLineProperty(IIIIZ)V
    .locals 6
    .param p1, "nMask"    # I
    .param p2, "nType"    # I
    .param p3, "nWidth"    # I
    .param p4, "nColor"    # I
    .param p5, "aUndo"    # Z

    .prologue
    .line 2366
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvNative;->ISetDrawCellLineProperty(IIIIZ)V

    .line 2367
    return-void
.end method

.method public ISetFillColor(I)V
    .locals 1
    .param p1, "nColor"    # I

    .prologue
    .line 1701
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetFillColor(I)V

    .line 1702
    return-void
.end method

.method public ISetFindModeChange(I)V
    .locals 1
    .param p1, "bFlag"    # I

    .prologue
    .line 2477
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetFindModeChange(I)V

    .line 2478
    return-void
.end method

.method public ISetFontAttribute(Ljava/lang/String;IIIIIIIIIZ)V
    .locals 12
    .param p1, "a_pszFont"    # Ljava/lang/String;
    .param p2, "a_nFSize"    # I
    .param p3, "a_nUNum"    # I
    .param p4, "a_nMaskAtt"    # I
    .param p5, "a_nFontAtt"    # I
    .param p6, "a_nFColor"    # I
    .param p7, "a_nBColor"    # I
    .param p8, "a_nUColor"    # I
    .param p9, "a_nWidth"    # I
    .param p10, "a_nHeight"    # I
    .param p11, "aUndo"    # Z

    .prologue
    .line 577
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    move/from16 v11, p11

    invoke-virtual/range {v0 .. v11}, Lcom/infraware/office/evengine/EvNative;->ISetFontAttribute(Ljava/lang/String;IIIIIIIIIZ)V

    .line 579
    return-void
.end method

.method public ISetFontStyle(I)V
    .locals 1
    .param p1, "nStyleID"    # I

    .prologue
    .line 1838
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetFontStyle(I)V

    .line 1839
    return-void
.end method

.method public ISetFormCopyPaste(I)V
    .locals 1
    .param p1, "nMode"    # I

    .prologue
    .line 1832
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetFormCopyPaste(I)V

    .line 1833
    return-void
.end method

.method public ISetFrameDetectionArea(Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;)V
    .locals 1
    .param p1, "frameDetectionArea"    # Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;

    .prologue
    .line 274
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetFrameDetectionArea(Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;)V

    .line 275
    return-void
.end method

.method public ISetFrameGroup(I)V
    .locals 1
    .param p1, "nGroupType"    # I

    .prologue
    .line 2081
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetFrameGroup(I)V

    .line 2082
    return-void
.end method

.method public ISetGuides(IZ)V
    .locals 1
    .param p1, "a_nMask"    # I
    .param p2, "bValue"    # Z

    .prologue
    .line 2444
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->ISetGuides(IZ)V

    .line 2445
    return-void
.end method

.method public ISetHaderFooterAction(III)V
    .locals 1
    .param p1, "a_HeaderFooterType"    # I
    .param p2, "a_ActionType"    # I
    .param p3, "a_TemplateType"    # I

    .prologue
    .line 2619
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvNative;->ISetHaderFooterAction(III)V

    .line 2620
    return-void
.end method

.method public ISetHaderFooterNavigation(I)V
    .locals 1
    .param p1, "a_toFrame"    # I

    .prologue
    .line 2625
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetHaderFooterNavigation(I)V

    .line 2627
    return-void
.end method

.method public ISetHeaderFooterOption(IIZZ)V
    .locals 1
    .param p1, "a_linkToPrevHeader"    # I
    .param p2, "a_linkToPrevFooter"    # I
    .param p3, "a_diffFirstPage"    # Z
    .param p4, "a_diffOddEvenPage"    # Z

    .prologue
    .line 2613
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/infraware/office/evengine/EvNative;->ISetHeaderFooterOption(IIZZ)V

    .line 2614
    return-void
.end method

.method public ISetHeapSize(I)V
    .locals 1
    .param p1, "a_nHeapSize"    # I

    .prologue
    .line 111
    invoke-super {p0, p1}, Lcom/infraware/office/evengine/EvInterface;->ISetHeapSize(I)V

    .line 112
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetHeapSize(I)V

    .line 113
    return-void
.end method

.method public ISetImageEffect(IIIIIIZ)V
    .locals 8
    .param p1, "aMask"    # I
    .param p2, "aBright"    # I
    .param p3, "aContrast"    # I
    .param p4, "aTransparency"    # I
    .param p5, "aflip"    # I
    .param p6, "amirror"    # I
    .param p7, "aUndo"    # Z

    .prologue
    .line 1882
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/infraware/office/evengine/EvNative;->ISetImageEffect(IIIIIIZ)V

    .line 1883
    return-void
.end method

.method public ISetInfraPenDrawMode(I)V
    .locals 3
    .param p1, "bFlag"    # I

    .prologue
    .line 2427
    const-string/jumbo v0, "HYOHYUN"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "ISetInfraPenDrawMode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2428
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetInfraPenDrawMode(I)V

    .line 2429
    return-void
.end method

.method public ISetInfraPenShow(Z)V
    .locals 2
    .param p1, "bShow"    # Z

    .prologue
    .line 2450
    if-eqz p1, :cond_0

    .line 2451
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->ISetInfraPenShow(I)V

    .line 2454
    :goto_0
    return-void

    .line 2453
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->ISetInfraPenShow(I)V

    goto :goto_0
.end method

.method public ISetLineShape(IIII)V
    .locals 1
    .param p1, "EEV_BORDER_STYLE"    # I
    .param p2, "a_LineThickness"    # I
    .param p3, "EEV_LIE_ARROW_TYPE"    # I
    .param p4, "a_LineColor"    # I

    .prologue
    .line 769
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/infraware/office/evengine/EvNative;->ISetLineShape(IIII)V

    .line 770
    return-void
.end method

.method public ISetLineSpace(I)V
    .locals 1
    .param p1, "a_LineSpaceType"    # I

    .prologue
    .line 766
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetLineSpace(I)V

    .line 767
    return-void
.end method

.method public ISetListener(Lcom/infraware/office/evengine/EvListener$ViewerListener;Lcom/infraware/office/evengine/EvListener$EditorListener;Lcom/infraware/office/evengine/EvListener$WordEditorListener;Lcom/infraware/office/evengine/EvListener$PptEditorListener;Lcom/infraware/office/evengine/EvListener$SheetEditorListener;Lcom/infraware/office/evengine/EvListener$PdfViewerListener;)V
    .locals 7
    .param p1, "DvL"    # Lcom/infraware/office/evengine/EvListener$ViewerListener;
    .param p2, "EvL"    # Lcom/infraware/office/evengine/EvListener$EditorListener;
    .param p3, "EvWL"    # Lcom/infraware/office/evengine/EvListener$WordEditorListener;
    .param p4, "EvPL"    # Lcom/infraware/office/evengine/EvListener$PptEditorListener;
    .param p5, "EvSL"    # Lcom/infraware/office/evengine/EvListener$SheetEditorListener;
    .param p6, "EvPDFL"    # Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvNative;->SetListener(Lcom/infraware/office/evengine/EvListener$ViewerListener;Lcom/infraware/office/evengine/EvListener$EditorListener;Lcom/infraware/office/evengine/EvListener$WordEditorListener;Lcom/infraware/office/evengine/EvListener$PptEditorListener;Lcom/infraware/office/evengine/EvListener$SheetEditorListener;Lcom/infraware/office/evengine/EvListener$PdfViewerListener;)V

    .line 124
    return-void
.end method

.method public ISetMemoView(III)V
    .locals 1
    .param p1, "nMemoMode"    # I
    .param p2, "bShowMemo"    # I
    .param p3, "nDirection"    # I

    .prologue
    .line 1827
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvNative;->ISetMemoView(III)V

    .line 1828
    return-void
.end method

.method public ISetModeStatus(I)V
    .locals 1
    .param p1, "EEV_VIEW_MODE"    # I

    .prologue
    .line 656
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetModeStatus(I)V

    .line 657
    return-void
.end method

.method public ISetMultiObjectAlign(I)V
    .locals 1
    .param p1, "a_Align"    # I

    .prologue
    .line 2061
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetMultiObjectAlign(I)V

    .line 2062
    return-void
.end method

.method public ISetMultiSelect(I)V
    .locals 1
    .param p1, "bMulti"    # I

    .prologue
    .line 2069
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetMultiSelect(I)V

    .line 2070
    return-void
.end method

.method public ISetObjDelete()V
    .locals 1

    .prologue
    .line 1005
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISetObjDelete()V

    .line 1006
    return-void
.end method

.method public ISetObjPos(I)V
    .locals 1
    .param p1, "a_nSendToType"    # I

    .prologue
    .line 1030
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetObjPos(I)V

    .line 1031
    return-void
.end method

.method public ISetObjResize(II)V
    .locals 1
    .param p1, "a_nSizeX"    # I
    .param p2, "a_nSizeY"    # I

    .prologue
    .line 1027
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->ISetObjResize(II)V

    .line 1028
    return-void
.end method

.method public ISetObjTextEdit()V
    .locals 1

    .prologue
    .line 1002
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISetObjTextEdit()V

    .line 1003
    return-void
.end method

.method public ISetObjectAttribute(IIIIIIIIIIZ)V
    .locals 12
    .param p1, "aObjMastAtt"    # I
    .param p2, "aFillColor"    # I
    .param p3, "aGradient"    # I
    .param p4, "aBorderColor"    # I
    .param p5, "aBorderThick"    # I
    .param p6, "aBorderStyle"    # I
    .param p7, "aWidth"    # I
    .param p8, "aHeight"    # I
    .param p9, "aArrowType"    # I
    .param p10, "aRate"    # I
    .param p11, "aUndo"    # Z

    .prologue
    .line 593
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    move/from16 v11, p11

    invoke-virtual/range {v0 .. v11}, Lcom/infraware/office/evengine/EvNative;->ISetObjectAttribute(IIIIIIIIIIZ)V

    .line 594
    return-void
.end method

.method public ISetPDFAnnotationMoveable(Z)V
    .locals 1
    .param p1, "bMoveable"    # Z

    .prologue
    .line 1982
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetPDFAnnotationMoveable(Z)V

    .line 1983
    return-void
.end method

.method public ISetPDFBgColor(I)Z
    .locals 1
    .param p1, "a_rgb"    # I

    .prologue
    .line 2026
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetPDFBgColor(I)Z

    move-result v0

    return v0
.end method

.method public ISetPPTChartBorder(IZ)V
    .locals 1
    .param p1, "nType"    # I
    .param p2, "b"    # Z

    .prologue
    .line 2389
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->ISetPPTChartBorder(IZ)V

    .line 2390
    return-void
.end method

.method public ISetPPTChartEffect(I)V
    .locals 1
    .param p1, "nEffect"    # I

    .prologue
    .line 2404
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetPPTChartEffect(I)V

    .line 2405
    return-void
.end method

.method public ISetPPTChartRowColChange()V
    .locals 1

    .prologue
    .line 2399
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISetPPTChartRowColChange()V

    .line 2400
    return-void
.end method

.method public ISetPPTChartShowHideData(Z)V
    .locals 1
    .param p1, "b"    # Z

    .prologue
    .line 2555
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetPPTChartShowHideData(Z)V

    .line 2556
    return-void
.end method

.method public ISetPPTSlideGLSync()V
    .locals 2

    .prologue
    .line 2582
    sget-object v1, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mEvLock:Ljava/lang/Object;

    monitor-enter v1

    .line 2583
    :try_start_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISetPPTSlideGLSync()V

    .line 2584
    monitor-exit v1

    .line 2585
    return-void

    .line 2584
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public ISetPageMap(IIIIII)V
    .locals 7
    .param p1, "EEV_PAGEMAP_COMMAND_TYPE"    # I
    .param p2, "a_bDrawContents"    # I
    .param p3, "EEV_PAGEMAP_POSITION_TYPE"    # I
    .param p4, "a_bGenerateDrawEvt"    # I
    .param p5, "a_nRowMargin"    # I
    .param p6, "a_nColMargin"    # I

    .prologue
    .line 353
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvNative;->ISetPageMap(IIIIII)V

    .line 354
    return-void
.end method

.method public ISetPageMode(I)V
    .locals 1
    .param p1, "bOnePageMode"    # I

    .prologue
    .line 2179
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetPageMode(I)V

    .line 2180
    return-void
.end method

.method public ISetPaperInfo(ILcom/infraware/office/evengine/EV$PAPER_INFO;)V
    .locals 11
    .param p1, "mask"    # I
    .param p2, "aPPaper"    # Lcom/infraware/office/evengine/EV$PAPER_INFO;

    .prologue
    .line 1771
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget v2, p2, Lcom/infraware/office/evengine/EV$PAPER_INFO;->a_SizeType:I

    iget v3, p2, Lcom/infraware/office/evengine/EV$PAPER_INFO;->a_MarginType:I

    iget v4, p2, Lcom/infraware/office/evengine/EV$PAPER_INFO;->a_Column:I

    iget v5, p2, Lcom/infraware/office/evengine/EV$PAPER_INFO;->a_OrientationType:I

    iget v6, p2, Lcom/infraware/office/evengine/EV$PAPER_INFO;->a_Left:I

    iget v7, p2, Lcom/infraware/office/evengine/EV$PAPER_INFO;->a_Top:I

    iget v8, p2, Lcom/infraware/office/evengine/EV$PAPER_INFO;->a_Right:I

    iget v9, p2, Lcom/infraware/office/evengine/EV$PAPER_INFO;->a_Bottom:I

    iget v10, p2, Lcom/infraware/office/evengine/EV$PAPER_INFO;->a_bAllPage:I

    move v1, p1

    invoke-virtual/range {v0 .. v10}, Lcom/infraware/office/evengine/EvNative;->ISetPaperInfo(IIIIIIIIII)V

    .line 1773
    return-void
.end method

.method public ISetParaAttribute(IIIIIIIIII)V
    .locals 11
    .param p1, "a_VAlign"    # I
    .param p2, "a_HAlign"    # I
    .param p3, "a_nLeftMargineValue"    # I
    .param p4, "a_nRightMarginValue"    # I
    .param p5, "a_nFirstLineType"    # I
    .param p6, "a_nFirstLineValue"    # I
    .param p7, "a_nLineSpace"    # I
    .param p8, "a_nLineSpaceValue"    # I
    .param p9, "a_nParaTopValue"    # I
    .param p10, "a_nParaBottomValue"    # I

    .prologue
    .line 741
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    invoke-virtual/range {v0 .. v10}, Lcom/infraware/office/evengine/EvNative;->ISetParaAttribute(IIIIIIIIII)V

    .line 751
    return-void
.end method

.method public ISetParaAttributeMask(IIIIIIIIIIIIIZ)V
    .locals 15
    .param p1, "aNMask"    # I
    .param p2, "aVAlign"    # I
    .param p3, "aHAlign"    # I
    .param p4, "aNLeftMargineValue"    # I
    .param p5, "aNRightMarginValue"    # I
    .param p6, "aNFirstLineType"    # I
    .param p7, "aNFirstLineValue"    # I
    .param p8, "aNLineSpace"    # I
    .param p9, "aNLineSpaceValue"    # I
    .param p10, "aNParaTopValue"    # I
    .param p11, "aNParaBottomValue"    # I
    .param p12, "a_nParaBidi"    # I
    .param p13, "a_nTextFlow"    # I
    .param p14, "a_bUndo"    # Z

    .prologue
    .line 1860
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    move/from16 v11, p11

    move/from16 v12, p12

    move/from16 v13, p13

    move/from16 v14, p14

    invoke-virtual/range {v0 .. v14}, Lcom/infraware/office/evengine/EvNative;->ISetParaAttributeMask(IIIIIIIIIIIIIZ)V

    .line 1876
    return-void
.end method

.method public ISetPdfAnnotListener(Lcom/infraware/office/evengine/EvListener$PdfAnnotListener;)V
    .locals 1
    .param p1, "EvPDFAL"    # Lcom/infraware/office/evengine/EvListener$PdfAnnotListener;

    .prologue
    .line 2566
    if-eqz p1, :cond_0

    .line 2567
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->SetPdfAnnotListener(Lcom/infraware/office/evengine/EvListener$PdfAnnotListener;)V

    .line 2568
    :cond_0
    return-void
.end method

.method public ISetPenColor(I)V
    .locals 1
    .param p1, "nColor"    # I

    .prologue
    .line 1697
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetPenColor(I)V

    .line 1698
    return-void
.end method

.method public ISetPenMode(IZ)V
    .locals 1
    .param p1, "nMode"    # I
    .param p2, "bPenDraw"    # Z

    .prologue
    .line 1693
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->ISetPenMode(IZ)V

    .line 1694
    return-void
.end method

.method public ISetPenPosition([I[I[I[II)V
    .locals 6
    .param p1, "a_pX"    # [I
    .param p2, "a_pY"    # [I
    .param p3, "a_pTime"    # [I
    .param p4, "a_pPressure"    # [I
    .param p5, "nCnt"    # I

    .prologue
    .line 1717
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvNative;->ISetPenPosition([I[I[I[II)V

    .line 1718
    return-void
.end method

.method public ISetPenSize(I)V
    .locals 1
    .param p1, "nSize"    # I

    .prologue
    .line 1705
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetPenSize(I)V

    .line 1706
    return-void
.end method

.method public ISetPenTransparency(I)V
    .locals 1
    .param p1, "nTransparency"    # I

    .prologue
    .line 1709
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetPenTransparency(I)V

    .line 1710
    return-void
.end method

.method public ISetPreview(IILjava/lang/String;)V
    .locals 6
    .param p1, "a_nWidth"    # I
    .param p2, "a_nHeight"    # I
    .param p3, "a_szFilePath"    # Ljava/lang/String;

    .prologue
    .line 1011
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    const/16 v3, 0x10

    const/4 v5, 0x0

    move v1, p1

    move v2, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvNative;->ISetPreview(IIILjava/lang/String;I)V

    .line 1012
    return-void
.end method

.method public ISetPreviewTimerCB()V
    .locals 1

    .prologue
    .line 1014
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISetPreviewTimerCB()V

    .line 1015
    return-void
.end method

.method public ISetPrint(IIILjava/lang/String;I)V
    .locals 6
    .param p1, "a_PaperSize"    # I
    .param p2, "a_StartPage"    # I
    .param p3, "a_EndPage"    # I
    .param p4, "a_szFilePath"    # Ljava/lang/String;
    .param p5, "a_ReturnType"    # I

    .prologue
    .line 1593
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvNative;->ISetPrint(IIILjava/lang/String;I)V

    .line 1594
    return-void
.end method

.method public ISetPrintEx(IIILjava/lang/String;ILjava/lang/String;Ljava/lang/String;IIII)V
    .locals 12
    .param p1, "a_PaperSize"    # I
    .param p2, "a_StartPage"    # I
    .param p3, "a_EndPage"    # I
    .param p4, "a_szFilePath"    # Ljava/lang/String;
    .param p5, "a_ReturnType"    # I
    .param p6, "a_szPageBoundary"    # Ljava/lang/String;
    .param p7, "a_szOutputPath"    # Ljava/lang/String;
    .param p8, "a_leftMargin"    # I
    .param p9, "a_topMargin"    # I
    .param p10, "a_rightMargin"    # I
    .param p11, "a_bottomMargin"    # I

    .prologue
    .line 1600
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    move/from16 v11, p11

    invoke-virtual/range {v0 .. v11}, Lcom/infraware/office/evengine/EvNative;->ISetPrintEx(IIILjava/lang/String;ILjava/lang/String;Ljava/lang/String;IIII)V

    .line 1602
    return-void
.end method

.method public ISetPrintListener(Lcom/infraware/office/evengine/EvListener$PrintListener;)V
    .locals 1
    .param p1, "EvPrintL"    # Lcom/infraware/office/evengine/EvListener$PrintListener;

    .prologue
    .line 2561
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->SetPrintListener(Lcom/infraware/office/evengine/EvListener$PrintListener;)V

    .line 2562
    return-void
.end method

.method public ISetPrintMode()V
    .locals 1

    .prologue
    .line 760
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISetPrintMode()V

    .line 761
    return-void
.end method

.method public ISetProperties(Lcom/infraware/office/evengine/EV$PROPERTIES;)V
    .locals 2
    .param p1, "a_Properties"    # Lcom/infraware/office/evengine/EV$PROPERTIES;

    .prologue
    .line 503
    sget-object v1, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mEvLock:Ljava/lang/Object;

    monitor-enter v1

    .line 504
    :try_start_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetProperties(Lcom/infraware/office/evengine/EV$PROPERTIES;)V

    .line 505
    monitor-exit v1

    .line 506
    return-void

    .line 505
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public ISetRefNote(II)V
    .locals 1
    .param p1, "nMode"    # I
    .param p2, "nFormType"    # I

    .prologue
    .line 2414
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->ISetRefNote(II)V

    .line 2415
    return-void
.end method

.method public ISetReplace(Ljava/lang/String;IIILjava/lang/String;I)V
    .locals 7
    .param p1, "a_sFind"    # Ljava/lang/String;
    .param p2, "a_bMathchWord"    # I
    .param p3, "a_bCase"    # I
    .param p4, "a_bDirUp"    # I
    .param p5, "a_sReplace"    # Ljava/lang/String;
    .param p6, "bReplaceMode"    # I

    .prologue
    .line 754
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvNative;->ISetReplace(Ljava/lang/String;IIILjava/lang/String;I)V

    .line 755
    return-void
.end method

.method public ISetResetUndoData()I
    .locals 1

    .prologue
    .line 2120
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISetResetUndoData()I

    move-result v0

    return v0
.end method

.method public ISetSavePenDraw(Z)V
    .locals 0
    .param p1, "bSavePenDraw"    # Z

    .prologue
    .line 2423
    return-void
.end method

.method public ISetScreenMode(I)V
    .locals 1
    .param p1, "EV_SCREENMODE_TYPE"    # I

    .prologue
    .line 803
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetScreenMode(I)V

    .line 804
    return-void
.end method

.method public ISetShadowStyle(I)V
    .locals 1
    .param p1, "aStyle"    # I

    .prologue
    .line 1888
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetShadowStyle(I)V

    .line 1889
    return-void
.end method

.method public ISetShapeEffect(IIIIIIIIIIZ)V
    .locals 12
    .param p1, "aMask"    # I
    .param p2, "aShadow"    # I
    .param p3, "aReflect"    # I
    .param p4, "nReflectTrans"    # I
    .param p5, "nReflectSize"    # I
    .param p6, "nNeon"    # I
    .param p7, "nNeonColor"    # I
    .param p8, "nNeonSize"    # I
    .param p9, "nCube"    # I
    .param p10, "DDD"    # I
    .param p11, "aUndo"    # Z

    .prologue
    .line 2382
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    move/from16 v11, p11

    invoke-virtual/range {v0 .. v11}, Lcom/infraware/office/evengine/EvNative;->ISetShapeEffect(IIIIIIIIIIZ)V

    .line 2385
    return-void
.end method

.method public ISetShapeProperty(ILcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;Lcom/infraware/office/evengine/EV$SHAPE_FILL;Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;Lcom/infraware/office/evengine/EV$SHAPE_GLOW;Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;Lcom/infraware/office/evengine/EV$SHAPE_SIZE;Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;)V
    .locals 20
    .param p1, "nSelector"    # I
    .param p2, "pQuickStyle"    # Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;
    .param p3, "pFill"    # Lcom/infraware/office/evengine/EV$SHAPE_FILL;
    .param p4, "pLineColor"    # Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;
    .param p5, "pLineStyle"    # Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;
    .param p6, "pShadow"    # Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;
    .param p7, "pReflection"    # Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;
    .param p8, "pGlow"    # Lcom/infraware/office/evengine/EV$SHAPE_GLOW;
    .param p9, "pSoftEdge"    # Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;
    .param p10, "p3DFormat"    # Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;
    .param p11, "p3DRotation"    # Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;
    .param p12, "pPictureCorrection"    # Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;
    .param p13, "pPictureColor"    # Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;
    .param p14, "pArtisticEffect"    # Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;
    .param p15, "pCropping"    # Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;
    .param p16, "pSize"    # Lcom/infraware/office/evengine/EV$SHAPE_SIZE;
    .param p17, "pLocation"    # Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;
    .param p18, "pTextbox"    # Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;

    .prologue
    .line 2211
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    invoke-virtual/range {v1 .. v19}, Lcom/infraware/office/evengine/EvNative;->ISetShapeProperty(ILcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;Lcom/infraware/office/evengine/EV$SHAPE_FILL;Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;Lcom/infraware/office/evengine/EV$SHAPE_GLOW;Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;Lcom/infraware/office/evengine/EV$SHAPE_SIZE;Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;)V

    .line 2212
    return-void
.end method

.method public ISetShapeStyle(I)V
    .locals 1
    .param p1, "aStyle"    # I

    .prologue
    .line 1894
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetShapeStyle(I)V

    .line 1895
    return-void
.end method

.method public ISetShapeStyleNum(I)V
    .locals 1
    .param p1, "aNStyleNum"    # I

    .prologue
    .line 1795
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetShapeStyleNum(I)V

    .line 1796
    return-void
.end method

.method public ISetSheetHyperLinkInfo(Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;)V
    .locals 1
    .param p1, "a_Info"    # Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    .prologue
    .line 1446
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetSheetHyperLinkInfo(Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;)V

    .line 1447
    return-void
.end method

.method public ISetSheetScreenFirstSelection()V
    .locals 1

    .prologue
    .line 1442
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISetSheetScreenFirstSelection()V

    .line 1443
    return-void
.end method

.method public ISetSlideAnimation(IIIIIIII)V
    .locals 10
    .param p1, "nIndex"    # I
    .param p2, "nAnimationType"    # I
    .param p3, "nDirectionType"    # I
    .param p4, "nShapesType"    # I
    .param p5, "nSpokesType"    # I
    .param p6, "nVanishingPointType"    # I
    .param p7, "nPresetType"    # I
    .param p8, "nTriggerType"    # I

    .prologue
    .line 964
    sget-object v9, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mEvLock:Ljava/lang/Object;

    monitor-enter v9

    .line 965
    :try_start_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lcom/infraware/office/evengine/EvNative;->ISetSlideAnimation(IIIIIIII)V

    .line 966
    monitor-exit v9

    .line 967
    return-void

    .line 966
    :catchall_0
    move-exception v0

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public ISetSlideAnimationAdd(IIIIIIII)V
    .locals 9
    .param p1, "nPageNum"    # I
    .param p2, "nAnimationType"    # I
    .param p3, "nDirectionType"    # I
    .param p4, "nShapesType"    # I
    .param p5, "nSpokesType"    # I
    .param p6, "nVanishingPointType"    # I
    .param p7, "nPresetType"    # I
    .param p8, "nTriggerType"    # I

    .prologue
    .line 969
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lcom/infraware/office/evengine/EvNative;->ISetSlideAnimationAdd(IIIIIIII)V

    .line 970
    return-void
.end method

.method public ISetSlideBackgroundColor(II)V
    .locals 1
    .param p1, "nPage"    # I
    .param p2, "color"    # I

    .prologue
    .line 945
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->ISetSlideBackgroundColor(II)V

    .line 946
    return-void
.end method

.method public ISetSlideHide(IZ)V
    .locals 1
    .param p1, "nPage"    # I
    .param p2, "bHide"    # Z

    .prologue
    .line 933
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->ISetSlideHide(IZ)V

    .line 934
    return-void
.end method

.method public ISetSlideLayout(II)V
    .locals 1
    .param p1, "nPage"    # I
    .param p2, "layoutType"    # I

    .prologue
    .line 939
    const/4 v0, 0x6

    invoke-virtual {p0, v0, p1, p2}, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->ISlideManage(III)V

    .line 940
    return-void
.end method

.method public ISetSlideShowEffect(IIIIIII)V
    .locals 9
    .param p1, "nPage"    # I
    .param p2, "nEffectType"    # I
    .param p3, "nOptionType"    # I
    .param p4, "nDuration"    # I
    .param p5, "bAdvClick"    # I
    .param p6, "bAdvTime"    # I
    .param p7, "nAdvTime"    # I

    .prologue
    .line 957
    sget-object v8, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mEvLock:Ljava/lang/Object;

    monitor-enter v8

    .line 958
    :try_start_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/infraware/office/evengine/EvNative;->ISetSlideShowEffect(IIIIIII)V

    .line 959
    monitor-exit v8

    .line 960
    return-void

    .line 959
    :catchall_0
    move-exception v0

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public ISetSortRange(Lcom/infraware/office/evengine/EV$RANGE;)V
    .locals 1
    .param p1, "a_pRange"    # Lcom/infraware/office/evengine/EV$RANGE;

    .prologue
    .line 1426
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetSortRange(Lcom/infraware/office/evengine/EV$RANGE;)V

    .line 1427
    return-void
.end method

.method public ISetSummaryData(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "mask"    # I
    .param p2, "aTitle"    # Ljava/lang/String;
    .param p3, "aAuthor"    # Ljava/lang/String;
    .param p4, "aModifieBy"    # Ljava/lang/String;

    .prologue
    .line 2531
    return-void
.end method

.method public ISetTableCancleMode()V
    .locals 2

    .prologue
    .line 2340
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->ISetTableDrawMode(I)V

    .line 2341
    return-void
.end method

.method public ISetTableDrawMode()V
    .locals 2

    .prologue
    .line 2346
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->ISetTableDrawMode(I)V

    .line 2347
    return-void
.end method

.method public ISetTableEreaseMode()V
    .locals 2

    .prologue
    .line 2334
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->ISetTableDrawMode(I)V

    .line 2335
    return-void
.end method

.method public ISetTableProperty(IIIIIIZ)V
    .locals 8
    .param p1, "nMask"    # I
    .param p2, "nBorderMask"    # I
    .param p3, "nBorderStyle"    # I
    .param p4, "nBorderWidth"    # I
    .param p5, "nBorderColor"    # I
    .param p6, "nFillColor"    # I
    .param p7, "a_bUndo"    # Z

    .prologue
    .line 2057
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/infraware/office/evengine/EvNative;->ISetTableProperty(IIIIIIZ)V

    .line 2058
    return-void
.end method

.method public ISetTableStyleInfo(III)V
    .locals 1
    .param p1, "mask"    # I
    .param p2, "a_nStyleNum"    # I
    .param p3, "Option"    # I

    .prologue
    .line 1808
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvNative;->ISetTableStyleInfo(III)V

    .line 1809
    return-void
.end method

.method public ISetTemplateShape(II)V
    .locals 1
    .param p1, "a_nShapeType"    # I
    .param p2, "a_nShapeColor"    # I

    .prologue
    .line 772
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->ISetTemplateShape(II)V

    .line 773
    return-void
.end method

.method public ISetTextWrapType(I)V
    .locals 1
    .param p1, "aType"    # I

    .prologue
    .line 1813
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetTextWrapType(I)V

    .line 1814
    return-void
.end method

.method public ISetViewMode(I)V
    .locals 1
    .param p1, "EEV_VIEWMODE_TYPE"    # I

    .prologue
    .line 304
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetViewMode(I)V

    .line 305
    return-void
.end method

.method public ISetWebMode()V
    .locals 1

    .prologue
    .line 757
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISetWebMode()V

    .line 758
    return-void
.end method

.method public ISetZoom(IIIIIIIIIII)V
    .locals 16
    .param p1, "EEV_ZOOM_TYPE"    # I
    .param p2, "a_nScale"    # I
    .param p3, "a_nSx"    # I
    .param p4, "a_nSy"    # I
    .param p5, "a_nEx"    # I
    .param p6, "a_nEy"    # I
    .param p7, "EEV_KEY_TYPE"    # I
    .param p8, "a_bStep"    # I
    .param p9, "a_bHaveZoomCenter"    # I
    .param p10, "a_nZoomCenterX"    # I
    .param p11, "a_nZoomCenterY"    # I

    .prologue
    .line 230
    move-object/from16 v0, p0

    iget v1, v0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->m_BackupZoom:I

    const v2, 0x989680

    div-int v13, v1, v2

    .line 231
    .local v13, "keytype":I
    move-object/from16 v0, p0

    iget v1, v0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->m_BackupZoom:I

    const v2, 0xf4240

    div-int/2addr v1, v2

    rem-int/lit8 v15, v1, 0xa

    .line 232
    .local v15, "zoomtype":I
    move-object/from16 v0, p0

    iget v1, v0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->m_BackupZoom:I

    const v2, 0xf4240

    rem-int v14, v1, v2

    .line 234
    .local v14, "scale":I
    const/4 v1, 0x1

    if-ne v1, v13, :cond_0

    move/from16 v0, p7

    if-ne v0, v13, :cond_0

    if-nez v15, :cond_0

    move/from16 v0, p1

    if-ne v0, v15, :cond_0

    sub-int v1, v14, p2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    div-int/lit8 v2, p2, 0x64

    add-int/lit8 v2, v2, 0x14

    if-ge v1, v2, :cond_0

    .line 253
    :goto_0
    return-void

    .line 245
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move/from16 v11, p10

    move/from16 v12, p11

    invoke-virtual/range {v1 .. v12}, Lcom/infraware/office/evengine/EvNative;->ISetZoom(IIIIIIIIIII)V

    .line 252
    const v1, 0x989680

    mul-int v1, v1, p7

    const v2, 0xf4240

    mul-int v2, v2, p1

    add-int/2addr v1, v2

    add-int v1, v1, p2

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->m_BackupZoom:I

    goto :goto_0
.end method

.method public IShapeInsertEx(IIIIIII)V
    .locals 8
    .param p1, "nShape"    # I
    .param p2, "nStyle"    # I
    .param p3, "nX"    # I
    .param p4, "nY"    # I
    .param p5, "nWidth"    # I
    .param p6, "nHeight"    # I
    .param p7, "nAngle"    # I

    .prologue
    .line 812
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/infraware/office/evengine/EvNative;->IShapeInsertEx(IIIIIII)V

    .line 813
    return-void
.end method

.method public ISheetAutoFilterMenuListUpdate(I)V
    .locals 1
    .param p1, "a_nIndexOfIndexCell"    # I

    .prologue
    .line 1273
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISheetAutoFilterMenuListUpdate(I)V

    .line 1274
    return-void
.end method

.method public ISheetBorder(Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;)V
    .locals 1
    .param p1, "a_pInfo"    # Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    .prologue
    .line 1406
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISheetBorder(Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;)V

    .line 1407
    return-void
.end method

.method public ISheetClear(I)V
    .locals 1
    .param p1, "EEV_SHEET_CLEAR"    # I

    .prologue
    .line 1213
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISheetClear(I)V

    .line 1214
    return-void
.end method

.method public ISheetDeleteCommentText()V
    .locals 1

    .prologue
    .line 1502
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISheetDeleteCommentText()V

    .line 1503
    return-void
.end method

.method public ISheetDrawFormulaRange(ZZ)V
    .locals 1
    .param p1, "a_bSelectRect"    # Z
    .param p2, "a_bSelectIcon"    # Z

    .prologue
    .line 1144
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->ISheetDrawFormulaRange(ZZ)V

    .line 1145
    return-void
.end method

.method public ISheetEdit(ILjava/lang/String;IIII)V
    .locals 7
    .param p1, "EEV_SHEET_EDIT"    # I
    .param p2, "a_szSheetName"    # Ljava/lang/String;
    .param p3, "a_nSheetIndex"    # I
    .param p4, "a_nSheets"    # I
    .param p5, "a_nMoveIndex"    # I
    .param p6, "a_bCopy"    # I

    .prologue
    .line 1183
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvNative;->ISheetEdit(ILjava/lang/String;IIII)V

    .line 1184
    return-void
.end method

.method public ISheetEditCF(Lcom/infraware/office/evengine/EV$RANGE;III[C)V
    .locals 6
    .param p1, "a_tRange"    # Lcom/infraware/office/evengine/EV$RANGE;
    .param p2, "nRuleType"    # I
    .param p3, "nDeleteRuleIndex"    # I
    .param p4, "nRuleValueLen"    # I
    .param p5, "szCFRuleValue"    # [C

    .prologue
    .line 1283
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvNative;->ISheetEditCF(Lcom/infraware/office/evengine/EV$RANGE;III[C)V

    .line 1284
    return-void
.end method

.method public ISheetFilter()V
    .locals 1

    .prologue
    .line 1257
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISheetFilter()V

    .line 1258
    return-void
.end method

.method public ISheetFilterCommand(IILjava/lang/String;)V
    .locals 1
    .param p1, "a_nIndexedCell"    # I
    .param p2, "a_nFixedItem"    # I
    .param p3, "a_szCommandString"    # Ljava/lang/String;

    .prologue
    .line 1265
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvNative;->ISheetFilterCommand(IILjava/lang/String;)V

    .line 1266
    return-void
.end method

.method public ISheetFilterIsRunning()Z
    .locals 1

    .prologue
    .line 1269
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISheetFilterIsRunning()Z

    move-result v0

    return v0
.end method

.method public ISheetFindReplace(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1, "a_pszFindText"    # Ljava/lang/String;
    .param p2, "a_pszReplaceText"    # Ljava/lang/String;
    .param p3, "a_nFlag"    # I

    .prologue
    .line 1238
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvNative;->ISheetFindReplace(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1239
    return-void
.end method

.method public ISheetFixFrame()V
    .locals 1

    .prologue
    .line 1186
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISheetFixFrame()V

    .line 1187
    return-void
.end method

.method public ISheetFocus()V
    .locals 1

    .prologue
    .line 1241
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISheetFocus()V

    .line 1242
    return-void
.end method

.method public ISheetFormat(IIIIIIIIIZ)V
    .locals 11
    .param p1, "a_nFormat"    # I
    .param p2, "a_nDecimalPlaces"    # I
    .param p3, "a_bSeparate"    # I
    .param p4, "a_nCurrency"    # I
    .param p5, "a_nNegative"    # I
    .param p6, "a_nAccounting"    # I
    .param p7, "a_nDate"    # I
    .param p8, "a_nTime"    # I
    .param p9, "a_nFraction"    # I
    .param p10, "a_bUndo"    # Z

    .prologue
    .line 1167
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    invoke-virtual/range {v0 .. v10}, Lcom/infraware/office/evengine/EvNative;->ISheetFormat(IIIIIIIIIZ)V

    .line 1168
    return-void
.end method

.method public ISheetFunction(I)V
    .locals 1
    .param p1, "a_nFunctionIndex"    # I

    .prologue
    .line 1170
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISheetFunction(I)V

    .line 1171
    return-void
.end method

.method public ISheetGetCFInfo()Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;
    .locals 2

    .prologue
    .line 1315
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getSheetCFInfo()Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->ISheetGetCFInfo(Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;)V

    .line 1316
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getSheetCFInfo()Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;

    move-result-object v0

    return-object v0
.end method

.method public ISheetGetSortKeyInfo([S)Z
    .locals 1
    .param p1, "a_shortArray"    # [S

    .prologue
    .line 1235
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISheetGetSortKeyInfo([S)Z

    move-result v0

    return v0
.end method

.method public ISheetGetTextBoxText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1291
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISheetGetTextBoxText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public ISheetInputField(I)V
    .locals 1
    .param p1, "a_bCancel"    # I

    .prologue
    .line 1244
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISheetInputField(I)V

    .line 1245
    return-void
.end method

.method public ISheetInsertAllChart(IILcom/infraware/office/evengine/EV$RANGE;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZZ)V
    .locals 12
    .param p1, "a_nMainType"    # I
    .param p2, "a_nSubType"    # I
    .param p3, "a_tRange"    # Lcom/infraware/office/evengine/EV$RANGE;
    .param p4, "a_nSeriesIn"    # I
    .param p5, "a_szTitle"    # Ljava/lang/String;
    .param p6, "a_szXAxis"    # Ljava/lang/String;
    .param p7, "a_szYAxis"    # Ljava/lang/String;
    .param p8, "a_nLegend"    # I
    .param p9, "a_nChartStyle"    # I
    .param p10, "a_bExternData"    # Z
    .param p11, "a_bPlotVisOnly"    # Z

    .prologue
    .line 1392
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move-object v3, p3

    move/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    move/from16 v11, p11

    invoke-virtual/range {v0 .. v11}, Lcom/infraware/office/evengine/EvNative;->ISheetInsertAllChart(IILcom/infraware/office/evengine/EV$RANGE;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZZ)V

    .line 1394
    return-void
.end method

.method public ISheetInsertCell(II)V
    .locals 1
    .param p1, "a_bInsert"    # I
    .param p2, "EEV_SHEET_ISERT_CELL"    # I

    .prologue
    .line 1200
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->ISheetInsertCell(II)V

    .line 1201
    return-void
.end method

.method public ISheetInsertChart(ILcom/infraware/office/evengine/EV$RANGE;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZZZIZZ)V
    .locals 15
    .param p1, "a_nChartType"    # I
    .param p2, "a_tRange"    # Lcom/infraware/office/evengine/EV$RANGE;
    .param p3, "a_nSeriesIn"    # I
    .param p4, "a_szTitle"    # Ljava/lang/String;
    .param p5, "a_szXAxis"    # Ljava/lang/String;
    .param p6, "a_szYAxis"    # Ljava/lang/String;
    .param p7, "a_nLegend"    # I
    .param p8, "a_nDimension"    # I
    .param p9, "a_bStacked"    # Z
    .param p10, "a_bPercent"    # Z
    .param p11, "a_bCluster"    # Z
    .param p12, "a_nChartStyle"    # I
    .param p13, "a_bExternData"    # Z
    .param p14, "a_bPlotVisOnly"    # Z

    .prologue
    .line 1334
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    move/from16 v11, p11

    move/from16 v12, p12

    move/from16 v13, p13

    move/from16 v14, p14

    invoke-virtual/range {v0 .. v14}, Lcom/infraware/office/evengine/EvNative;->ISheetInsertChart(ILcom/infraware/office/evengine/EV$RANGE;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZZZIZZ)V

    .line 1337
    return-void
.end method

.method public ISheetInsertColumns(III)V
    .locals 1
    .param p1, "a_bInsert"    # I
    .param p2, "a_nCount"    # I
    .param p3, "a_bAutoFit"    # I

    .prologue
    .line 1206
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvNative;->ISheetInsertColumns(III)V

    .line 1207
    return-void
.end method

.method public ISheetInsertCommentText(Ljava/lang/String;)V
    .locals 1
    .param p1, "comment"    # Ljava/lang/String;

    .prologue
    .line 1498
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISheetInsertCommentText(Ljava/lang/String;)V

    .line 1499
    return-void
.end method

.method public ISheetInsertRows(III)V
    .locals 1
    .param p1, "a_bInsert"    # I
    .param p2, "a_nCount"    # I
    .param p3, "a_bAutoFit"    # I

    .prologue
    .line 1203
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvNative;->ISheetInsertRows(III)V

    .line 1204
    return-void
.end method

.method public ISheetIsClipboardData()Z
    .locals 1

    .prologue
    .line 1306
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISheetIsClipboardData()Z

    move-result v0

    return v0
.end method

.method public ISheetIsObjClicked()Z
    .locals 1

    .prologue
    .line 1309
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISheetIsObjClicked()Z

    move-result v0

    return v0
.end method

.method public ISheetIsSameObjSelected()Z
    .locals 1

    .prologue
    .line 1303
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISheetIsSameObjSelected()Z

    move-result v0

    return v0
.end method

.method public ISheetIsTextBox()Z
    .locals 1

    .prologue
    .line 1294
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISheetIsTextBox()Z

    move-result v0

    return v0
.end method

.method public ISheetIsWholeCols()Z
    .locals 1

    .prologue
    .line 1152
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISheetIsWholeCols()Z

    move-result v0

    return v0
.end method

.method public ISheetIsWholeRows()Z
    .locals 1

    .prologue
    .line 1156
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISheetIsWholeRows()Z

    move-result v0

    return v0
.end method

.method public ISheetMerge()V
    .locals 1

    .prologue
    .line 1248
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISheetMerge()V

    .line 1249
    return-void
.end method

.method public ISheetPageBreak()V
    .locals 1

    .prologue
    .line 1189
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISheetPageBreak()V

    .line 1190
    return-void
.end method

.method public ISheetProtection()V
    .locals 1

    .prologue
    .line 1192
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISheetProtection()V

    .line 1193
    return-void
.end method

.method public ISheetRecalculate()V
    .locals 1

    .prologue
    .line 1173
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISheetRecalculate()V

    .line 1174
    return-void
.end method

.method public ISheetSetAlignment(II)V
    .locals 1
    .param p1, "a_nHAlignment"    # I
    .param p2, "a_nVAlignment"    # I

    .prologue
    .line 1163
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->ISheetSetAlignment(II)V

    .line 1164
    return-void
.end method

.method public ISheetSetAutoFormula(I)V
    .locals 1
    .param p1, "nFunction"    # I

    .prologue
    .line 1312
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISheetSetAutoFormula(I)V

    .line 1313
    return-void
.end method

.method public ISheetSetColor(I)V
    .locals 1
    .param p1, "a_lColor"    # I

    .prologue
    .line 1160
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISheetSetColor(I)V

    .line 1161
    return-void
.end method

.method public ISheetSetFormulaRangeColor([I)V
    .locals 1
    .param p1, "a_nFormulaRangeColors"    # [I

    .prologue
    .line 1148
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISheetSetFormulaRangeColor([I)V

    .line 1149
    return-void
.end method

.method public ISheetSetRowColSize(III)V
    .locals 1
    .param p1, "EV_GUI_EVENT"    # I
    .param p2, "a_nValue"    # I
    .param p3, "a_bAutoFit"    # I

    .prologue
    .line 1229
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvNative;->ISheetSetRowColSize(III)V

    .line 1230
    return-void
.end method

.method public ISheetSetTextboxEditMode(I)V
    .locals 1
    .param p1, "bTextEdit"    # I

    .prologue
    .line 1300
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISheetSetTextboxEditMode(I)V

    .line 1301
    return-void
.end method

.method public ISheetSetTextboxText(Ljava/lang/String;I)V
    .locals 1
    .param p1, "strText"    # Ljava/lang/String;
    .param p2, "nLength"    # I

    .prologue
    .line 1287
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->ISheetSetTextboxText(Ljava/lang/String;I)V

    .line 1288
    return-void
.end method

.method public ISheetShowHideRowCol(III)V
    .locals 1
    .param p1, "EEV_SHEET_SHOW_ROWCOL"    # I
    .param p2, "a_nValue"    # I
    .param p3, "a_bAutoFit"    # I

    .prologue
    .line 1221
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvNative;->ISheetShowHideRowCol(III)V

    .line 1222
    return-void
.end method

.method public ISheetSort(IIII)V
    .locals 1
    .param p1, "a_bSortByRow"    # I
    .param p2, "a_nKey1"    # I
    .param p3, "a_nKey2"    # I
    .param p4, "a_nKey3"    # I

    .prologue
    .line 1232
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/infraware/office/evengine/EvNative;->ISheetSort(IIII)V

    .line 1233
    return-void
.end method

.method public ISheetWrap()V
    .locals 1

    .prologue
    .line 1253
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISheetWrap()V

    .line 1254
    return-void
.end method

.method public IShowHideImage(I)V
    .locals 1
    .param p1, "EEV_WORD_SHOW_HIDE"    # I

    .prologue
    .line 662
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IShowHideImage(I)V

    .line 663
    return-void
.end method

.method public ISlideAddMove(III)V
    .locals 0
    .param p1, "EV_SLIDE_ADD_TYPE"    # I
    .param p2, "a_nCurrentPageNumber"    # I
    .param p3, "EEV_SLIDE_TEMPLATE_TYPE"    # I

    .prologue
    .line 788
    invoke-virtual {p0, p1, p2, p3}, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->ISlideManage(III)V

    .line 789
    return-void
.end method

.method public ISlideCopy(I)V
    .locals 2
    .param p1, "nPage"    # I

    .prologue
    .line 791
    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->ISlideManage(III)V

    .line 792
    return-void
.end method

.method public ISlideCut(I)V
    .locals 2
    .param p1, "nPage"    # I

    .prologue
    .line 794
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->ISlideManage(III)V

    .line 795
    return-void
.end method

.method public ISlideManage(III)V
    .locals 1
    .param p1, "EV_SLIDE_MANAGE_TYPE"    # I
    .param p2, "a_nCurrentPageNumber"    # I
    .param p3, "EEV_SLIDE_TEMPLATE_TYPE"    # I

    .prologue
    .line 785
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvNative;->ISlideManage(III)V

    .line 786
    return-void
.end method

.method public ISlideNote(III)V
    .locals 1
    .param p1, "a_Width"    # I
    .param p2, "a_Height"    # I
    .param p3, "a_PageNum"    # I

    .prologue
    .line 907
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvNative;->ISlideNote(III)V

    .line 908
    return-void
.end method

.method public ISlideNoteInput(ILjava/lang/String;I)V
    .locals 1
    .param p1, "a_nSlidePage"    # I
    .param p2, "a_pszSlideNote"    # Ljava/lang/String;
    .param p3, "a_nLen"    # I

    .prologue
    .line 911
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->ISlideNoteInputEx(ILjava/lang/String;)V

    .line 912
    return-void
.end method

.method public ISlideObjInsert(III)V
    .locals 1
    .param p1, "a_Width"    # I
    .param p2, "a_Height"    # I
    .param p3, "a_PageCnt"    # I

    .prologue
    .line 927
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvNative;->ISlideObjInsert(III)V

    .line 928
    return-void
.end method

.method public declared-synchronized ISlideObjStart(III)V
    .locals 2
    .param p1, "a_Width"    # I
    .param p2, "a_Height"    # I
    .param p3, "a_PageCnt"    # I

    .prologue
    .line 915
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mEvLock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 916
    :try_start_1
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvNative;->ISlideObjStart(III)V

    .line 917
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 918
    monitor-exit p0

    return-void

    .line 917
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 915
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized ISlideObjStartEx(IIFIZ)V
    .locals 7
    .param p1, "a_Width"    # I
    .param p2, "a_Height"    # I
    .param p3, "thumbnail_quality"    # F
    .param p4, "a_PageNum"    # I
    .param p5, "a_bModified"    # Z

    .prologue
    .line 921
    monitor-enter p0

    :try_start_0
    sget-object v6, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mEvLock:Ljava/lang/Object;

    monitor-enter v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 922
    :try_start_1
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvNative;->ISlideObjStartEx(IIFIZ)V

    .line 923
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 924
    monitor-exit p0

    return-void

    .line 923
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 921
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public ISlidePaste(I)V
    .locals 2
    .param p1, "nPage"    # I

    .prologue
    .line 797
    const/16 v0, 0x9

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->ISlideManage(III)V

    .line 798
    return-void
.end method

.method public ISlideShow(IIIIIZ)V
    .locals 8
    .param p1, "a_Width"    # I
    .param p2, "a_Height"    # I
    .param p3, "a_StartPage"    # I
    .param p4, "a_DualViewWidth"    # I
    .param p5, "a_DualViewHeight"    # I
    .param p6, "a_bPreview"    # Z

    .prologue
    .line 817
    const/4 v7, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v7}, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->ISlideShow(IIIIIZZ)V

    .line 818
    return-void
.end method

.method public declared-synchronized ISlideShow(IIIIIZZ)V
    .locals 9
    .param p1, "a_Width"    # I
    .param p2, "a_Height"    # I
    .param p3, "a_StartPage"    # I
    .param p4, "a_DualViewWidth"    # I
    .param p5, "a_DualViewHeight"    # I
    .param p6, "a_bPreview"    # Z
    .param p7, "bIsSkipEffect"    # Z

    .prologue
    .line 822
    monitor-enter p0

    :try_start_0
    sget-object v8, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mEvLock:Ljava/lang/Object;

    monitor-enter v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 824
    :try_start_1
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ICancel()V

    .line 825
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/infraware/office/evengine/EvNative;->ISlideShow(IIIIIZZ)V

    .line 827
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 828
    monitor-exit p0

    return-void

    .line 827
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 822
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized ISlideShowContinue()V
    .locals 2

    .prologue
    .line 853
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mEvLock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 854
    :try_start_1
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ISlideShowContinue()V

    .line 855
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 856
    monitor-exit p0

    return-void

    .line 855
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 853
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public ISlideShowPlay(I)V
    .locals 1
    .param p1, "a_PlayType"    # I

    .prologue
    .line 831
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->ISlideShowPlay(IZ)V

    .line 832
    return-void
.end method

.method public ISlideShowPlay(III)V
    .locals 2
    .param p1, "a_PlayType"    # I
    .param p2, "nPage"    # I
    .param p3, "nAnimationIndex"    # I

    .prologue
    .line 839
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/infraware/office/evengine/EvNative;->ISlideShowPlay(IIIZ)V

    .line 840
    return-void
.end method

.method public declared-synchronized ISlideShowPlay(IIIZ)V
    .locals 2
    .param p1, "a_PlayType"    # I
    .param p2, "nPage"    # I
    .param p3, "nAnimationIndex"    # I
    .param p4, "bIsSkipEffect"    # Z

    .prologue
    .line 843
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mEvLock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 844
    :try_start_1
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/infraware/office/evengine/EvNative;->ISlideShowPlay(IIIZ)V

    .line 845
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 846
    monitor-exit p0

    return-void

    .line 845
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 843
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public ISlideShowPlay(IZ)V
    .locals 2
    .param p1, "a_PlayType"    # I
    .param p2, "bIsSkipEffect"    # Z

    .prologue
    .line 835
    const/4 v0, 0x1

    const/4 v1, -0x1

    invoke-virtual {p0, p1, v0, v1, p2}, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->ISlideShowPlay(IIIZ)V

    .line 836
    return-void
.end method

.method public ISpellCheckScreen(Z)V
    .locals 1
    .param p1, "bMode"    # Z

    .prologue
    .line 2511
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISpellCheckScreen(Z)V

    .line 2512
    return-void
.end method

.method public ISpellCheckWrong(Ljava/lang/String;IIIIIII)I
    .locals 9
    .param p1, "pWordStr"    # Ljava/lang/String;
    .param p2, "nLen"    # I
    .param p3, "bClass"    # I
    .param p4, "nPageNum"    # I
    .param p5, "nObjectID"    # I
    .param p6, "nNoteNum"    # I
    .param p7, "nParaIndex"    # I
    .param p8, "nColIndex"    # I

    .prologue
    .line 2468
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lcom/infraware/office/evengine/EvNative;->ISpellCheckWrong(Ljava/lang/String;IIIIIII)I

    move-result v0

    return v0
.end method

.method public IStopFlicking()V
    .locals 1

    .prologue
    .line 897
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IStopFlicking()V

    .line 898
    return-void
.end method

.method public declared-synchronized IStopSlideEffect()V
    .locals 2

    .prologue
    .line 891
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mEvLock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 892
    :try_start_1
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IStopSlideEffect()V

    .line 893
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 894
    monitor-exit p0

    return-void

    .line 893
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 891
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public ITempSaveDocument(Ljava/lang/String;)V
    .locals 3
    .param p1, "a_pszFilePath"    # Ljava/lang/String;

    .prologue
    .line 544
    const-string/jumbo v0, "EvCompinterface"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "ISaveDocument = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/infraware/office/evengine/EvNative;->ISaveDocument(Ljava/lang/String;I)V

    .line 546
    return-void
.end method

.method public ITextPlay(IIII)V
    .locals 0
    .param p1, "EEV_TEXTPLAY_TYPE"    # I
    .param p2, "a_bAction"    # I
    .param p3, "a_nSYPos"    # I
    .param p4, "a_nEYPos"    # I

    .prologue
    .line 463
    return-void
.end method

.method public IThreadSuspend(I)V
    .locals 1
    .param p1, "a_bSuspend"    # I

    .prologue
    .line 475
    iput p1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mbSuspend:I

    .line 476
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IThreadSuspend(I)V

    .line 477
    return-void
.end method

.method public IThumbnail(IIIII)V
    .locals 7
    .param p1, "EEV_THUMBNAIL_MODE_TYPE"    # I
    .param p2, "a_nPages"    # I
    .param p3, "a_nWidth"    # I
    .param p4, "a_nHeight"    # I
    .param p5, "a_nStartPage"    # I

    .prologue
    .line 372
    sget-object v6, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mEvLock:Ljava/lang/Object;

    monitor-enter v6

    .line 373
    :try_start_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvNative;->IThumbnail(IIIII)V

    .line 374
    monitor-exit v6

    .line 375
    return-void

    .line 374
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized ITimer()V
    .locals 2

    .prologue
    .line 90
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mEvLock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 91
    :try_start_1
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ITimer()V

    .line 92
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 93
    monitor-exit p0

    return-void

    .line 92
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 90
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public IVideoInsert(Ljava/lang/String;Landroid/graphics/Bitmap;IIZLjava/lang/String;Z)V
    .locals 8
    .param p1, "a_pImgPath"    # Ljava/lang/String;
    .param p2, "a_bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "a_nWidth"    # I
    .param p4, "a_nHeight"    # I
    .param p5, "a_bReplace"    # Z
    .param p6, "strVideoPath"    # Ljava/lang/String;
    .param p7, "a_bLinkedVideo"    # Z

    .prologue
    .line 641
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    move v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/infraware/office/evengine/EvNative;->IVideoInsert(Ljava/lang/String;Landroid/graphics/Bitmap;IIZLjava/lang/String;Z)V

    .line 642
    return-void
.end method

.method public IZoomInOut(II)V
    .locals 1
    .param p1, "a_bI"    # I
    .param p2, "EEV_KEY_TYPE"    # I

    .prologue
    .line 205
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IZoomInOut(II)V

    .line 206
    return-void
.end method

.method public IsInsertBookmark_Editor()I
    .locals 1

    .prologue
    .line 1548
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IIsInsertBookmark_Editor()I

    move-result v0

    return v0
.end method

.method public IsLassoViewMode_Editor()Z
    .locals 1

    .prologue
    .line 2577
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IsLassoViewMode_Editor()Z

    move-result v0

    return v0
.end method

.method public IsPenDrawFrameShow()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2483
    iget-object v1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvNative;->IsPenDrawFrameShow()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 2486
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public IsStartOfSentence_Editor()I
    .locals 1

    .prologue
    .line 1551
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IIsStartOfSentence_Editor()I

    move-result v0

    return v0
.end method

.method public IsWebMode()I
    .locals 1

    .prologue
    .line 2085
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IsWebMode()I

    move-result v0

    return v0
.end method

.method protected OnInitComplete(I)V
    .locals 0
    .param p1, "EEV_ERROR_CODE"    # I

    .prologue
    .line 127
    invoke-super {p0, p1}, Lcom/infraware/office/evengine/EvInterface;->OnInitComplete(I)V

    .line 128
    return-void
.end method

.method OnTimerStart()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mHandler:Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->setOperationTimer(Z)V

    .line 98
    return-void
.end method

.method OnTimerStop()V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mHandler:Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->setOperationTimer(Z)V

    .line 102
    return-void
.end method

.method public setOnZoomChangeListener(Lcom/infraware/office/evengine/EvInterface$OnZoomChangeListener;)V
    .locals 0
    .param p1, "oListener"    # Lcom/infraware/office/evengine/EvInterface$OnZoomChangeListener;

    .prologue
    .line 258
    iput-object p1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->m_oZoomChangedListener:Lcom/infraware/office/evengine/EvInterface$OnZoomChangeListener;

    .line 259
    return-void
.end method
