.class public Lcom/infraware/office/evengine/EV$SHEET_INFO;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SHEET_INFO"
.end annotation


# instance fields
.field public bArabic:I

.field public bFreeze:I

.field public bPageBreak:I

.field public bProtectSheet:I

.field public szSheetName:Ljava/lang/String;

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 1

    .prologue
    .line 1294
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SHEET_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1296
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_INFO;->szSheetName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 1302
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_INFO;->szSheetName:Ljava/lang/String;

    .line 1303
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHEET_INFO;->bArabic:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHEET_INFO;->bPageBreak:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHEET_INFO;->bFreeze:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHEET_INFO;->bProtectSheet:I

    .line 1304
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SHEET_INFO;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1307
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SHEET_INFO;

    .line 1308
    .local v0, "o":Lcom/infraware/office/evengine/EV$SHEET_INFO;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1294
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SHEET_INFO;->clone()Lcom/infraware/office/evengine/EV$SHEET_INFO;

    move-result-object v0

    return-object v0
.end method
