.class public Lcom/infraware/office/evengine/EV$GUIDES_INFO;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GUIDES_INFO"
.end annotation


# instance fields
.field public a_Center:Z

.field public a_Sides:Z

.field public a_SmartGuides:Z

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 1639
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$GUIDES_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 1646
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/evengine/EV$GUIDES_INFO;->a_SmartGuides:Z

    .line 1647
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/office/evengine/EV$GUIDES_INFO;->a_Sides:Z

    iput-boolean v0, p0, Lcom/infraware/office/evengine/EV$GUIDES_INFO;->a_Center:Z

    .line 1648
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$GUIDES_INFO;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1651
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$GUIDES_INFO;

    .line 1652
    .local v0, "o":Lcom/infraware/office/evengine/EV$GUIDES_INFO;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1639
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$GUIDES_INFO;->clone()Lcom/infraware/office/evengine/EV$GUIDES_INFO;

    move-result-object v0

    return-object v0
.end method
