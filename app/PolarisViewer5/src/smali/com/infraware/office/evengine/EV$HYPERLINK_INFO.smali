.class public Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "HYPERLINK_INFO"
.end annotation


# instance fields
.field public bAutoHyper:I

.field public bReDraw:I

.field public bUse:I

.field public szHyperLink:Ljava/lang/String;

.field public szHyperText:Ljava/lang/String;

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 766
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 775
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperText:Ljava/lang/String;

    .line 776
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->bAutoHyper:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->bReDraw:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->bUse:I

    .line 777
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 780
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    .line 781
    .local v0, "o":Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 766
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->clone()Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-result-object v0

    return-object v0
.end method
