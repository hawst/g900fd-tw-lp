.class public Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PDF_ANNOT_ITEM"
.end annotation


# instance fields
.field public AnnotItem:I

.field public nIndex:I

.field public nLineStyle:I

.field public nPageNum:I

.field public nStyle:I

.field public nSubType:I

.field public nTime:J

.field public nType:I

.field public pText:Ljava/lang/String;

.field public rect:[F

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 1

    .prologue
    .line 1917
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1918
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->rect:[F

    .line 1919
    return-void
.end method


# virtual methods
.method protected clone()Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1934
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;

    .line 1935
    .local v0, "o":Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->rect:[F

    invoke-virtual {v1}, [F->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [F

    iput-object v1, v0, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->rect:[F

    .line 1936
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1916
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->clone()Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;

    move-result-object v0

    return-object v0
.end method
