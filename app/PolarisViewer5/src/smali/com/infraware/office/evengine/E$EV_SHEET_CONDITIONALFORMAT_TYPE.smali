.class public interface abstract Lcom/infraware/office/evengine/E$EV_SHEET_CONDITIONALFORMAT_TYPE;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_SHEET_CONDITIONALFORMAT_TYPE"
.end annotation


# static fields
.field public static final BR_SHEET_INSERTCF_BOTTOM10N:I = 0x6

.field public static final BR_SHEET_INSERTCF_BOTTOM10P:I = 0x7

.field public static final BR_SHEET_INSERTCF_COLOR_GYR:I = 0x8

.field public static final BR_SHEET_INSERTCF_COLOR_RW:I = 0xb

.field public static final BR_SHEET_INSERTCF_COLOR_RYG:I = 0x9

.field public static final BR_SHEET_INSERTCF_COLOR_WR:I = 0xa

.field public static final BR_SHEET_INSERTCF_DELETE:I = 0xf

.field public static final BR_SHEET_INSERTCF_DUPLILCATEVALUES:I = 0x3

.field public static final BR_SHEET_INSERTCF_EQUAL:I = 0x2

.field public static final BR_SHEET_INSERTCF_GREATERTHAN:I = 0x0

.field public static final BR_SHEET_INSERTCF_ICON3:I = 0xc

.field public static final BR_SHEET_INSERTCF_ICON4:I = 0xd

.field public static final BR_SHEET_INSERTCF_ICON5:I = 0xe

.field public static final BR_SHEET_INSERTCF_LESSTHAN:I = 0x1

.field public static final BR_SHEET_INSERTCF_TOP10N:I = 0x4

.field public static final BR_SHEET_INSERTCF_TOP10P:I = 0x5
