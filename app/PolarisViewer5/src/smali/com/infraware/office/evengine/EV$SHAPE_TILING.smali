.class public Lcom/infraware/office/evengine/EV$SHAPE_TILING;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SHAPE_TILING"
.end annotation


# instance fields
.field public nAlignment:I

.field public nMirrorType:I

.field public nOffsetX:I

.field public nOffsetY:I

.field public nScaleX:I

.field public nScaleY:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 2795
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SHAPE_TILING;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 2804
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_TILING;->nMirrorType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_TILING;->nAlignment:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_TILING;->nScaleY:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_TILING;->nScaleX:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_TILING;->nOffsetY:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_TILING;->nOffsetX:I

    .line 2805
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SHAPE_TILING;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2809
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SHAPE_TILING;

    .line 2810
    .local v0, "o":Lcom/infraware/office/evengine/EV$SHAPE_TILING;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2795
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SHAPE_TILING;->clone()Lcom/infraware/office/evengine/EV$SHAPE_TILING;

    move-result-object v0

    return-object v0
.end method
