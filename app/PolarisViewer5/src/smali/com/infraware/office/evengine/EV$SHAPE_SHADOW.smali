.class public Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SHAPE_SHADOW"
.end annotation


# instance fields
.field public nAngle:F

.field public nBlur:F

.field public nColor:J

.field public nDistance:F

.field public nPreset:I

.field public nSize:F

.field public nTransparency:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 2466
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 2

    .prologue
    .line 2476
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;->nTransparency:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;->nPreset:I

    .line 2477
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;->nColor:J

    .line 2478
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;->nDistance:F

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;->nAngle:F

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;->nBlur:F

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;->nSize:F

    .line 2479
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2483
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;

    .line 2484
    .local v0, "o":Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2466
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;->clone()Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;

    move-result-object v0

    return-object v0
.end method
