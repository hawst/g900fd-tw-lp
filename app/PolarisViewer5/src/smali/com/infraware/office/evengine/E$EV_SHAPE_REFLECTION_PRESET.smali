.class public interface abstract Lcom/infraware/office/evengine/E$EV_SHAPE_REFLECTION_PRESET;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_SHAPE_REFLECTION_PRESET"
.end annotation


# static fields
.field public static final BR_FRAME_REFLECTION_EXIST:I = 0x1

.field public static final BR_FRAME_REFLECTION_FULL_4PT_OFFSET:I = 0x7

.field public static final BR_FRAME_REFLECTION_FULL_8PT_OFFSET:I = 0xa

.field public static final BR_FRAME_REFLECTION_FULL_TOUCHING:I = 0x4

.field public static final BR_FRAME_REFLECTION_HALF_4PT_OFFSET:I = 0x6

.field public static final BR_FRAME_REFLECTION_HALF_8PT_OFFSET:I = 0x9

.field public static final BR_FRAME_REFLECTION_HALF_TOUCHING:I = 0x3

.field public static final BR_FRAME_REFLECTION_MAX:I = 0xb

.field public static final BR_FRAME_REFLECTION_NONE:I = 0x0

.field public static final BR_FRAME_REFLECTION_TIGHT_4PT_OFFSET:I = 0x5

.field public static final BR_FRAME_REFLECTION_TIGHT_8PT_OFFSET:I = 0x8

.field public static final BR_FRAME_REFLECTION_TIGHT_TOUCHING:I = 0x2
