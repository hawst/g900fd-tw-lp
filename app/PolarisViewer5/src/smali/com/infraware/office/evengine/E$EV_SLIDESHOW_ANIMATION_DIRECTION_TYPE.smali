.class public interface abstract Lcom/infraware/office/evengine/E$EV_SLIDESHOW_ANIMATION_DIRECTION_TYPE;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_SLIDESHOW_ANIMATION_DIRECTION_TYPE"
.end annotation


# static fields
.field public static final eEV_SLIDE_ANIMATION_DIRECTION_FLOAT_DOWN:I = 0x12

.field public static final eEV_SLIDE_ANIMATION_DIRECTION_FLOAT_UP:I = 0x11

.field public static final eEV_SLIDE_ANIMATION_DIRECTION_FROM_BOTTOM:I = 0x1

.field public static final eEV_SLIDE_ANIMATION_DIRECTION_FROM_BOTTOMLEFT:I = 0x5

.field public static final eEV_SLIDE_ANIMATION_DIRECTION_FROM_BOTTOMRIGHT:I = 0x6

.field public static final eEV_SLIDE_ANIMATION_DIRECTION_FROM_LEFT:I = 0x2

.field public static final eEV_SLIDE_ANIMATION_DIRECTION_FROM_RIGHT:I = 0x3

.field public static final eEV_SLIDE_ANIMATION_DIRECTION_FROM_TOP:I = 0x4

.field public static final eEV_SLIDE_ANIMATION_DIRECTION_FROM_TOPLEFT:I = 0x7

.field public static final eEV_SLIDE_ANIMATION_DIRECTION_FROM_TOPRIGHT:I = 0x8

.field public static final eEV_SLIDE_ANIMATION_DIRECTION_HORIZONTAL:I = 0x13

.field public static final eEV_SLIDE_ANIMATION_DIRECTION_HORIZONTAL_IN:I = 0x17

.field public static final eEV_SLIDE_ANIMATION_DIRECTION_HORIZONTAL_OUT:I = 0x18

.field public static final eEV_SLIDE_ANIMATION_DIRECTION_IN:I = 0x15

.field public static final eEV_SLIDE_ANIMATION_DIRECTION_LEFT:I = 0xa

.field public static final eEV_SLIDE_ANIMATION_DIRECTION_NONE:I = 0x0

.field public static final eEV_SLIDE_ANIMATION_DIRECTION_OUT:I = 0x16

.field public static final eEV_SLIDE_ANIMATION_DIRECTION_RIGHT:I = 0xb

.field public static final eEV_SLIDE_ANIMATION_DIRECTION_TO_BOTTOM:I = 0x9

.field public static final eEV_SLIDE_ANIMATION_DIRECTION_TO_BOTTOMLEFT:I = 0xd

.field public static final eEV_SLIDE_ANIMATION_DIRECTION_TO_DOWNRIGHT:I = 0xe

.field public static final eEV_SLIDE_ANIMATION_DIRECTION_TO_TOP:I = 0xc

.field public static final eEV_SLIDE_ANIMATION_DIRECTION_TO_TOPLEFT:I = 0xf

.field public static final eEV_SLIDE_ANIMATION_DIRECTION_TO_TOPRIGHT:I = 0x10

.field public static final eEV_SLIDE_ANIMATION_DIRECTION_VERTICAL:I = 0x14

.field public static final eEV_SLIDE_ANIMATION_DIRECTION_VERTICAL_IN:I = 0x19

.field public static final eEV_SLIDE_ANIMATION_DIRECTION_VERTICAL_OUT:I = 0x1a
