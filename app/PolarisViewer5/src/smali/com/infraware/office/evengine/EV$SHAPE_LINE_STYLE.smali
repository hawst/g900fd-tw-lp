.class public Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SHAPE_LINE_STYLE"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE$SHAPE_LINE_ARROW;
    }
.end annotation


# instance fields
.field public nCapType:I

.field public nCompoundType:I

.field public nDashType:I

.field public nJoinType:I

.field public nWidth:I

.field public stArrow:Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE$SHAPE_LINE_ARROW;

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 1

    .prologue
    .line 2430
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2431
    new-instance v0, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE$SHAPE_LINE_ARROW;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE$SHAPE_LINE_ARROW;-><init>(Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;->stArrow:Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE$SHAPE_LINE_ARROW;

    .line 2432
    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 2454
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;->nJoinType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;->nCapType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;->nDashType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;->nCompoundType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;->nWidth:I

    .line 2455
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;->stArrow:Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE$SHAPE_LINE_ARROW;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE$SHAPE_LINE_ARROW;->clear()V

    .line 2456
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2460
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;

    .line 2461
    .local v0, "o":Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2420
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;->clone()Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;

    move-result-object v0

    return-object v0
.end method
