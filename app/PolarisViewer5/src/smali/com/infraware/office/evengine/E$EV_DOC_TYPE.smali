.class public interface abstract Lcom/infraware/office/evengine/E$EV_DOC_TYPE;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_DOC_TYPE"
.end annotation


# static fields
.field public static final eEV_DOCTYPE_ASCI:I = 0x4

.field public static final eEV_DOCTYPE_BMV:I = 0x8

.field public static final eEV_DOCTYPE_BORA:I = 0x0

.field public static final eEV_DOCTYPE_BWP:I = 0x9

.field public static final eEV_DOCTYPE_COMIC_CNI:I = 0x17

.field public static final eEV_DOCTYPE_COMIC_TNS:I = 0x16

.field public static final eEV_DOCTYPE_COMIC_ZIP:I = 0x15

.field public static final eEV_DOCTYPE_DOC:I = 0x3

.field public static final eEV_DOCTYPE_DOCX:I = 0xf

.field public static final eEV_DOCTYPE_EPUB:I = 0x11

.field public static final eEV_DOCTYPE_GUL:I = 0x5

.field public static final eEV_DOCTYPE_HTML:I = 0x1

.field public static final eEV_DOCTYPE_HWP:I = 0x2

.field public static final eEV_DOCTYPE_JOHAP:I = 0x6

.field public static final eEV_DOCTYPE_JUNGUMGUL:I = 0xd

.field public static final eEV_DOCTYPE_MFI:I = 0x12

.field public static final eEV_DOCTYPE_MHT:I = 0xe

.field public static final eEV_DOCTYPE_PDF:I = 0xc

.field public static final eEV_DOCTYPE_PPT:I = 0xb

.field public static final eEV_DOCTYPE_PPTX:I = 0x14

.field public static final eEV_DOCTYPE_RTF:I = 0x7

.field public static final eEV_DOCTYPE_SNB:I = 0x19

.field public static final eEV_DOCTYPE_XLS:I = 0xa

.field public static final eEV_DOCTYPE_XLSX:I = 0x13

.field public static final eEV_DOCTYPE_XLS_CHART:I = 0x18

.field public static final eEV_DOCTYPE_ZIP:I = 0x10
