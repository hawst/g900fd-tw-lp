.class public Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SHAPE_3D_FORMAT"
.end annotation


# instance fields
.field public nBackDepth:F

.field public nBevelBottomHeight:F

.field public nBevelBottomType:I

.field public nBevelBottomWidth:F

.field public nBevelTopHeight:F

.field public nBevelTopType:I

.field public nBevelTopWidth:F

.field public nContourColor:J

.field public nContourSize:F

.field public nDepthColor:J

.field public nSurfaceAngle:F

.field public nSurfaceLight:I

.field public nSurfaceMaterial:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 2545
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 2

    .prologue
    .line 2561
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nSurfaceMaterial:I

    int-to-float v0, v0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nContourSize:F

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBackDepth:F

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBevelBottomHeight:F

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBevelBottomWidth:F

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBevelTopHeight:F

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBevelTopWidth:F

    .line 2562
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nContourColor:J

    iput-wide v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nDepthColor:J

    .line 2563
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2567
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;

    .line 2568
    .local v0, "o":Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2545
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->clone()Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;

    move-result-object v0

    return-object v0
.end method
