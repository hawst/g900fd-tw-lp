.class public Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SHAPE_3D_ROTATION"
.end annotation


# instance fields
.field public n3DCameraType:I

.field public nObjectPos:I

.field public nPerspectiveAngle:F

.field public nPreset:I

.field public nXRotation:F

.field public nYRotation:F

.field public nZRotation:F

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 2573
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2583
    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->n3DCameraType:I

    .line 2584
    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nObjectPos:I

    int-to-float v0, v0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nPerspectiveAngle:F

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nZRotation:F

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nYRotation:F

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nXRotation:F

    .line 2585
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2589
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;

    .line 2590
    .local v0, "o":Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2573
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->clone()Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;

    move-result-object v0

    return-object v0
.end method
