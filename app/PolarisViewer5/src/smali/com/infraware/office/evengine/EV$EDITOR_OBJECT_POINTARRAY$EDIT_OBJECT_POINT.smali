.class public Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "EDIT_OBJECT_POINT"
.end annotation


# instance fields
.field public nObjectEditInfo:I

.field public nObjectType:I

.field public point:Landroid/graphics/Point;

.field final synthetic this$1:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V
    .locals 1

    .prologue
    .line 2077
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->this$1:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2079
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    return-void
.end method


# virtual methods
.method clear()V
    .locals 3

    .prologue
    .line 2084
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    iget-object v1, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    const/4 v2, 0x0

    iput v2, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->nObjectEditInfo:I

    iput v2, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->nObjectType:I

    iput v2, v1, Landroid/graphics/Point;->y:I

    iput v2, v0, Landroid/graphics/Point;->x:I

    .line 2085
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2088
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    .line 2089
    .local v0, "o":Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2077
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->clone()Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-result-object v0

    return-object v0
.end method
