.class public Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SHAPE_REFLECTION"
.end annotation


# instance fields
.field public nBlur:F

.field public nDistance:F

.field public nPreset:I

.field public nSize:F

.field public nTransparency:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 2489
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 2497
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;->nTransparency:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;->nPreset:I

    .line 2498
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;->nBlur:F

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;->nDistance:F

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;->nSize:F

    .line 2499
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2503
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;

    .line 2504
    .local v0, "o":Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2489
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;->clone()Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;

    move-result-object v0

    return-object v0
.end method
