.class public interface abstract Lcom/infraware/office/evengine/E$EV_BWP_CHART_MODIFY;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_BWP_CHART_MODIFY"
.end annotation


# static fields
.field public static final BR_BWP_MODIFY_CHART_BORDER:I = 0x800

.field public static final BR_BWP_MODIFY_CHART_COLROWCHANGE:I = 0x1000

.field public static final BR_BWP_MODIFY_CHART_COLUMN:I = 0x100

.field public static final BR_BWP_MODIFY_CHART_DIMENSION:I = 0x40

.field public static final BR_BWP_MODIFY_CHART_ITEM:I = 0x80

.field public static final BR_BWP_MODIFY_CHART_LEGEND:I = 0x20

.field public static final BR_BWP_MODIFY_CHART_MODE:I = 0x2

.field public static final BR_BWP_MODIFY_CHART_SHOWHIDEDATA:I = 0x2000

.field public static final BR_BWP_MODIFY_CHART_STYLE:I = 0x400

.field public static final BR_BWP_MODIFY_CHART_TITLE:I = 0x4

.field public static final BR_BWP_MODIFY_CHART_TYPE:I = 0x1

.field public static final BR_BWP_MODIFY_CHART_VALUE_DATA:I = 0x200

.field public static final BR_BWP_MODIFY_CHART_XTITLE:I = 0x8

.field public static final BR_BWP_MODIFY_CHART_YTITLE:I = 0x10
