.class Lcom/infraware/office/evengine/EvFindWordNextByPosObj;
.super Lcom/infraware/office/evengine/EvTaskObj;
.source "EvTaskObj.java"


# instance fields
.field private m_nXpos:I

.field private m_nYpos:I


# direct methods
.method constructor <init>(Lcom/infraware/office/evengine/EvNative;II)V
    .locals 0
    .param p1, "a_Native"    # Lcom/infraware/office/evengine/EvNative;
    .param p2, "a_nXpos"    # I
    .param p3, "a_nYpos"    # I

    .prologue
    .line 654
    invoke-direct {p0, p1}, Lcom/infraware/office/evengine/EvTaskObj;-><init>(Lcom/infraware/office/evengine/EvNative;)V

    .line 655
    iput p2, p0, Lcom/infraware/office/evengine/EvFindWordNextByPosObj;->m_nXpos:I

    .line 656
    iput p3, p0, Lcom/infraware/office/evengine/EvFindWordNextByPosObj;->m_nYpos:I

    .line 657
    return-void
.end method


# virtual methods
.method doTask()V
    .locals 3

    .prologue
    .line 661
    iget-object v0, p0, Lcom/infraware/office/evengine/EvFindWordNextByPosObj;->Native:Lcom/infraware/office/evengine/EvNative;

    iget v1, p0, Lcom/infraware/office/evengine/EvFindWordNextByPosObj;->m_nXpos:I

    iget v2, p0, Lcom/infraware/office/evengine/EvFindWordNextByPosObj;->m_nYpos:I

    invoke-virtual {v0, v1, v2}, Lcom/infraware/office/evengine/EvNative;->IFindWordNextByPos(II)V

    .line 662
    return-void
.end method
