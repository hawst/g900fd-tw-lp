.class public Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GUI_SHEET_CHART_DATALABELINFO_EVENT"
.end annotation


# instance fields
.field public bEnableNumFmt:I

.field public nChart:I

.field public nDecPlaces:I

.field public nFlag:I

.field public nLabelPos:I

.field public nNegativeType:I

.field public nType:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 1536
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 1546
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;->nNegativeType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;->nDecPlaces:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;->bEnableNumFmt:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;->nLabelPos:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;->nFlag:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;->nChart:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;->nType:I

    .line 1547
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1550
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;

    .line 1551
    .local v0, "o":Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1536
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;->clone()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;

    move-result-object v0

    return-object v0
.end method
