.class public interface abstract Lcom/infraware/office/evengine/E$EV_IMAGE_MASK;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_IMAGE_MASK"
.end annotation


# static fields
.field public static final eEV_IMAGE_MASK_BLACKWHITE:I = 0x10

.field public static final eEV_IMAGE_MASK_BRIGHTNESS:I = 0x2

.field public static final eEV_IMAGE_MASK_CONTRAST:I = 0x4

.field public static final eEV_IMAGE_MASK_FLIP:I = 0x80

.field public static final eEV_IMAGE_MASK_GRAY:I = 0x8

.field public static final eEV_IMAGE_MASK_INVERT:I = 0x20

.field public static final eEV_IMAGE_MASK_MIRROR:I = 0x40

.field public static final eEV_IMAGE_MASK_TRANSPARENCY:I = 0x1
