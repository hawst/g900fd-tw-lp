.class public Lcom/infraware/office/evengine/EV$SCREEN_INFO;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SCREEN_INFO"
.end annotation


# instance fields
.field public nHeight:I

.field public nMapHeight:I

.field public nMapWidth:I

.field public nMapX:I

.field public nMapY:I

.field public nWidth:I

.field public nX:I

.field public nY:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 846
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SCREEN_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 858
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SCREEN_INFO;->nMapHeight:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SCREEN_INFO;->nMapWidth:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SCREEN_INFO;->nMapY:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SCREEN_INFO;->nMapX:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SCREEN_INFO;->nHeight:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SCREEN_INFO;->nWidth:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SCREEN_INFO;->nY:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SCREEN_INFO;->nX:I

    .line 859
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SCREEN_INFO;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 862
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SCREEN_INFO;

    .line 863
    .local v0, "o":Lcom/infraware/office/evengine/EV$SCREEN_INFO;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 846
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SCREEN_INFO;->clone()Lcom/infraware/office/evengine/EV$SCREEN_INFO;

    move-result-object v0

    return-object v0
.end method
