.class public Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SHAPE_QUICK_STYLE"
.end annotation


# instance fields
.field public nLinePreset:I

.field public nPicturePreset:I

.field public nQuickStyleFrameType:I

.field public nShapePreset:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 2309
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 2317
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;->nPicturePreset:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;->nLinePreset:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;->nShapePreset:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;->nQuickStyleFrameType:I

    .line 2318
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2322
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;

    .line 2323
    .local v0, "o":Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2309
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;->clone()Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;

    move-result-object v0

    return-object v0
.end method
