.class Lcom/infraware/office/evengine/EvInitializeObj;
.super Lcom/infraware/office/evengine/EvTaskObj;
.source "EvTaskObj.java"


# instance fields
.field private m_bFrameBufferSwap:I

.field private m_bUseAutoBookmark:I

.field private m_nBits:I

.field private m_nBookmarkType:I

.field private m_nHeight:I

.field private m_nScrollModeType:I

.field private m_nWidth:I


# direct methods
.method constructor <init>(Lcom/infraware/office/evengine/EvNative;IIIIIII)V
    .locals 0
    .param p1, "a_Native"    # Lcom/infraware/office/evengine/EvNative;
    .param p2, "a_nWidth"    # I
    .param p3, "a_nHeight"    # I
    .param p4, "a_nScrollModeType"    # I
    .param p5, "a_nBookmarkType"    # I
    .param p6, "a_bUseAutoBookmark"    # I
    .param p7, "a_nBits"    # I
    .param p8, "a_bFrameBufferSwap"    # I

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/infraware/office/evengine/EvTaskObj;-><init>(Lcom/infraware/office/evengine/EvNative;)V

    .line 38
    iput p2, p0, Lcom/infraware/office/evengine/EvInitializeObj;->m_nWidth:I

    .line 39
    iput p3, p0, Lcom/infraware/office/evengine/EvInitializeObj;->m_nHeight:I

    .line 40
    iput p4, p0, Lcom/infraware/office/evengine/EvInitializeObj;->m_nScrollModeType:I

    .line 41
    iput p5, p0, Lcom/infraware/office/evengine/EvInitializeObj;->m_nBookmarkType:I

    .line 42
    iput p6, p0, Lcom/infraware/office/evengine/EvInitializeObj;->m_bUseAutoBookmark:I

    .line 43
    iput p7, p0, Lcom/infraware/office/evengine/EvInitializeObj;->m_nBits:I

    .line 44
    iput p8, p0, Lcom/infraware/office/evengine/EvInitializeObj;->m_bFrameBufferSwap:I

    .line 45
    return-void
.end method


# virtual methods
.method doTask()V
    .locals 9

    .prologue
    .line 49
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInitializeObj;->Native:Lcom/infraware/office/evengine/EvNative;

    iget v1, p0, Lcom/infraware/office/evengine/EvInitializeObj;->m_nWidth:I

    iget v2, p0, Lcom/infraware/office/evengine/EvInitializeObj;->m_nHeight:I

    iget v3, p0, Lcom/infraware/office/evengine/EvInitializeObj;->m_nScrollModeType:I

    iget v4, p0, Lcom/infraware/office/evengine/EvInitializeObj;->m_nBookmarkType:I

    iget v5, p0, Lcom/infraware/office/evengine/EvInitializeObj;->m_bUseAutoBookmark:I

    iget v6, p0, Lcom/infraware/office/evengine/EvInitializeObj;->m_nBits:I

    iget v7, p0, Lcom/infraware/office/evengine/EvInitializeObj;->m_bFrameBufferSwap:I

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Lcom/infraware/office/evengine/EvNative;->IInitialize(IIIIIIII)V

    .line 57
    return-void
.end method
