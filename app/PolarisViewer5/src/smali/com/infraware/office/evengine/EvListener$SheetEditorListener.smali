.class public interface abstract Lcom/infraware/office/evengine/EvListener$SheetEditorListener;
.super Ljava/lang/Object;
.source "EvListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EvListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SheetEditorListener"
.end annotation


# virtual methods
.method public abstract OnGetFormulaFieldSelection()[I
.end method

.method public abstract OnGetFormulaFieldText()Ljava/lang/String;
.end method

.method public abstract OnGetSheetScrollIInfo()[I
.end method

.method public abstract OnGetSystemDate(III)Ljava/lang/String;
.end method

.method public abstract OnGetSystemTime(IID)Ljava/lang/String;
.end method

.method public abstract OnSetDataRange(IIII)V
.end method

.method public abstract OnSetFormulaFieldSelection(II)V
.end method

.method public abstract OnSetFormulaFieldText(Ljava/lang/String;)V
.end method

.method public abstract OnSetFormulaSelectionEnabled(I)V
.end method

.method public abstract OnSetNameBoxText(Ljava/lang/String;)V
.end method

.method public abstract OnSetSheetScrollIInfo(IIIII)V
.end method

.method public abstract OnSheetAutoFilterContext([I[I[I[I)V
.end method

.method public abstract OnSheetAutoFilterIndexCellRect([I)V
.end method

.method public abstract OnSheetAutoFilterMenu(II[Ljava/lang/String;[Z[ZI[I)V
.end method

.method public abstract OnSheetAutoFilterMenuEx(Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;)V
.end method

.method public abstract OnSheetAutoFilterStartStateCallBack(I)V
.end method

.method public abstract OnSheetAutoFilterStatusChanged(I)V
.end method

.method public abstract OnSheetChart(I)V
.end method

.method public abstract OnSheetCircularReferenceWarning()V
.end method

.method public abstract OnSheetDynamicLoading(I)V
.end method

.method public abstract OnSheetEdit(II)V
.end method

.method public abstract OnSheetEditBlock()V
.end method

.method public abstract OnSheetFocus()V
.end method

.method public abstract OnSheetFormulaRangeRect(I[I[I)V
.end method

.method public abstract OnSheetFunction(III)V
.end method

.method public abstract OnSheetInputField(II)V
.end method

.method public abstract OnSheetMemoNavigate(I)V
.end method

.method public abstract OnSheetPartialLoad(I)V
.end method

.method public abstract OnSheetPivotTableInDocument(ZZI)V
.end method

.method public abstract OnSheetProtection(I)V
.end method

.method public abstract OnSheetSort(I)V
.end method
