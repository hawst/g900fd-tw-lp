.class public Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GUI_SHEET_CHART_TITLEINFO_EVENT"
.end annotation


# instance fields
.field public bPlotVisOnly:Z

.field public bShowChartBorder:Z

.field public bShowPlotBorder:Z

.field public bShowTitle:Z

.field public nChart:I

.field public nChartStyle:I

.field public nType:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 1445
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1455
    iput v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;->nChartStyle:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;->nChart:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;->nType:I

    .line 1456
    iput-boolean v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;->bShowChartBorder:Z

    iput-boolean v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;->bShowPlotBorder:Z

    iput-boolean v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;->bShowTitle:Z

    .line 1457
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;->bPlotVisOnly:Z

    .line 1458
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1461
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;

    .line 1462
    .local v0, "o":Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1445
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;->clone()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;

    move-result-object v0

    return-object v0
.end method
