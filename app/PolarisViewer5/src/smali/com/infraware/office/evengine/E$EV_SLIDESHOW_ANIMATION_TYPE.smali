.class public interface abstract Lcom/infraware/office/evengine/E$EV_SLIDESHOW_ANIMATION_TYPE;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_SLIDESHOW_ANIMATION_TYPE"
.end annotation


# static fields
.field public static final eEV_SLIDE_ANIMATION_APPEAR:I = 0x1

.field public static final eEV_SLIDE_ANIMATION_BOLD_FLASH:I = 0x1d

.field public static final eEV_SLIDE_ANIMATION_BOLD_REVEAL:I = 0x1e

.field public static final eEV_SLIDE_ANIMATION_BOUNCE:I = 0xd

.field public static final eEV_SLIDE_ANIMATION_BREAKS:I = 0x5

.field public static final eEV_SLIDE_ANIMATION_BRUSH_COLOR:I = 0x1a

.field public static final eEV_SLIDE_ANIMATION_COLOR_PULSE:I = 0xf

.field public static final eEV_SLIDE_ANIMATION_COMPLEMENTARY_COLOR:I = 0x17

.field public static final eEV_SLIDE_ANIMATION_CUSTOM_PATH:I = 0x27

.field public static final eEV_SLIDE_ANIMATION_DARKEN:I = 0x13

.field public static final eEV_SLIDE_ANIMATION_DESATURATE:I = 0x12

.field public static final eEV_SLIDE_ANIMATION_DISAPPEAR:I = 0x20

.field public static final eEV_SLIDE_ANIMATION_FADE:I = 0x2

.field public static final eEV_SLIDE_ANIMATION_FILL_COLOR:I = 0x19

.field public static final eEV_SLIDE_ANIMATION_FLOAT_IN:I = 0x4

.field public static final eEV_SLIDE_ANIMATION_FLOAT_OUT:I = 0x22

.field public static final eEV_SLIDE_ANIMATION_FLY_IN:I = 0x3

.field public static final eEV_SLIDE_ANIMATION_FLY_OUT:I = 0x21

.field public static final eEV_SLIDE_ANIMATION_FONT_COLOR:I = 0x1b

.field public static final eEV_SLIDE_ANIMATION_GROW_SHRINK:I = 0x11

.field public static final eEV_SLIDE_ANIMATION_GROW_TURN:I = 0xa

.field public static final eEV_SLIDE_ANIMATION_LIGHTEN:I = 0x14

.field public static final eEV_SLIDE_ANIMATION_LINE:I = 0x24

.field public static final eEV_SLIDE_ANIMATION_LINE_COLOR:I = 0x18

.field public static final eEV_SLIDE_ANIMATION_NONE:I = 0x0

.field public static final eEV_SLIDE_ANIMATION_NOT_SUPPORT:I = 0x28

.field public static final eEV_SLIDE_ANIMATION_OBJECT_COLOR:I = 0x16

.field public static final eEV_SLIDE_ANIMATION_OVAL:I = 0x25

.field public static final eEV_SLIDE_ANIMATION_PULSE:I = 0xe

.field public static final eEV_SLIDE_ANIMATION_RANDOM_BARS:I = 0x9

.field public static final eEV_SLIDE_ANIMATION_REPEAT:I = 0x26

.field public static final eEV_SLIDE_ANIMATION_ROTATE:I = 0xc

.field public static final eEV_SLIDE_ANIMATION_SHAPES:I = 0x7

.field public static final eEV_SLIDE_ANIMATION_SHRINK_TURN:I = 0x23

.field public static final eEV_SLIDE_ANIMATION_TEETER:I = 0x10

.field public static final eEV_SLIDE_ANIMATION_TRANSPARENCY:I = 0x15

.field public static final eEV_SLIDE_ANIMATION_UNDERLINE:I = 0x1c

.field public static final eEV_SLIDE_ANIMATION_WAVES:I = 0x1f

.field public static final eEV_SLIDE_ANIMATION_WHEEL:I = 0x8

.field public static final eEV_SLIDE_ANIMATION_WIPE:I = 0x6

.field public static final eEV_SLIDE_ANIMATION_ZOOM:I = 0xb
