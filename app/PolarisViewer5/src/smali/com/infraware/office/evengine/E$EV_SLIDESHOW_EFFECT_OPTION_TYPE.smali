.class public interface abstract Lcom/infraware/office/evengine/E$EV_SLIDESHOW_EFFECT_OPTION_TYPE;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_SLIDESHOW_EFFECT_OPTION_TYPE"
.end annotation


# static fields
.field public static final eEV_SLIDESHOW_EFFECTOPTION_CENTER:I = 0xb

.field public static final eEV_SLIDESHOW_EFFECTOPTION_CIRCLE:I = 0x28

.field public static final eEV_SLIDESHOW_EFFECTOPTION_CLOCKWISE:I = 0x2b

.field public static final eEV_SLIDESHOW_EFFECTOPTION_COUNTER_CLOCKWISE:I = 0x2c

.field public static final eEV_SLIDESHOW_EFFECTOPTION_DIAMOND:I = 0x29

.field public static final eEV_SLIDESHOW_EFFECTOPTION_DIAMONDS_FROM_BOTTOM:I = 0x18

.field public static final eEV_SLIDESHOW_EFFECTOPTION_DIAMONDS_FROM_LEFT:I = 0x19

.field public static final eEV_SLIDESHOW_EFFECTOPTION_DIAMONDS_FROM_RIGHT:I = 0x16

.field public static final eEV_SLIDESHOW_EFFECTOPTION_DIAMONDS_FROM_TOP:I = 0x17

.field public static final eEV_SLIDESHOW_EFFECTOPTION_FROM_BOTTOM:I = 0x6

.field public static final eEV_SLIDESHOW_EFFECTOPTION_FROM_BOTTOMLEFT:I = 0xa

.field public static final eEV_SLIDESHOW_EFFECTOPTION_FROM_BOTTOMRIGHT:I = 0x8

.field public static final eEV_SLIDESHOW_EFFECTOPTION_FROM_LEFT:I = 0x5

.field public static final eEV_SLIDESHOW_EFFECTOPTION_FROM_RIGHT:I = 0x3

.field public static final eEV_SLIDESHOW_EFFECTOPTION_FROM_TOP:I = 0x4

.field public static final eEV_SLIDESHOW_EFFECTOPTION_FROM_TOPLEFT:I = 0x9

.field public static final eEV_SLIDESHOW_EFFECTOPTION_FROM_TOPRIGHT:I = 0x7

.field public static final eEV_SLIDESHOW_EFFECTOPTION_HEXAGONS_FROM_BOTTOM:I = 0x14

.field public static final eEV_SLIDESHOW_EFFECTOPTION_HEXAGONS_FROM_LEFT:I = 0x15

.field public static final eEV_SLIDESHOW_EFFECTOPTION_HEXAGONS_FROM_RIGHT:I = 0x12

.field public static final eEV_SLIDESHOW_EFFECTOPTION_HEXAGONS_FROM_TOP:I = 0x13

.field public static final eEV_SLIDESHOW_EFFECTOPTION_HORIZONTAL:I = 0x1b

.field public static final eEV_SLIDESHOW_EFFECTOPTION_HORIZONTAL_IN:I = 0x1e

.field public static final eEV_SLIDESHOW_EFFECTOPTION_HORIZONTAL_OUT:I = 0x1f

.field public static final eEV_SLIDESHOW_EFFECTOPTION_IN:I = 0x24

.field public static final eEV_SLIDESHOW_EFFECTOPTION_IN_WITH_BOUNCE:I = 0x26

.field public static final eEV_SLIDESHOW_EFFECTOPTION_LEFT:I = 0xd

.field public static final eEV_SLIDESHOW_EFFECTOPTION_NONE:I = 0x0

.field public static final eEV_SLIDESHOW_EFFECTOPTION_OUT:I = 0x25

.field public static final eEV_SLIDESHOW_EFFECTOPTION_OUT_WITH_BOUNCE:I = 0x27

.field public static final eEV_SLIDESHOW_EFFECTOPTION_PARTICLES_IN:I = 0x22

.field public static final eEV_SLIDESHOW_EFFECTOPTION_PARTICLES_OUT:I = 0x23

.field public static final eEV_SLIDESHOW_EFFECTOPTION_PLUS:I = 0x2a

.field public static final eEV_SLIDESHOW_EFFECTOPTION_RIGHT:I = 0xc

.field public static final eEV_SLIDESHOW_EFFECTOPTION_SMOOTHLY_FROM_LEFT:I = 0xf

.field public static final eEV_SLIDESHOW_EFFECTOPTION_SMOOTHLY_FROM_RIGHT:I = 0xe

.field public static final eEV_SLIDESHOW_EFFECTOPTION_SOFTEN:I = 0x1

.field public static final eEV_SLIDESHOW_EFFECTOPTION_STRIPS_IN:I = 0x20

.field public static final eEV_SLIDESHOW_EFFECTOPTION_STRIPS_OUT:I = 0x21

.field public static final eEV_SLIDESHOW_EFFECTOPTION_THROUGH_BLACK:I = 0x2

.field public static final eEV_SLIDESHOW_EFFECTOPTION_THROUGH_BLACK_FROM_LEFT:I = 0x11

.field public static final eEV_SLIDESHOW_EFFECTOPTION_THROUGH_BLACK_FROM_RIGHT:I = 0x10

.field public static final eEV_SLIDESHOW_EFFECTOPTION_VERTICAL:I = 0x1a

.field public static final eEV_SLIDESHOW_EFFECTOPTION_VERTICAL_IN:I = 0x1c

.field public static final eEV_SLIDESHOW_EFFECTOPTION_VERTICAL_OUT:I = 0x1d

.field public static final eEV_SLIDESHOW_EFFECTOPTION_WEDGE:I = 0x2d
