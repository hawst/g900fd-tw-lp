.class public interface abstract Lcom/infraware/office/evengine/E$EV_PEN_MODE;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_PEN_MODE"
.end annotation


# static fields
.field public static final eEV_ARROW_MODE:I = 0xe

.field public static final eEV_BRUSH_MODE:I = 0x6

.field public static final eEV_CRAYON_MODE:I = 0x3

.field public static final eEV_DRAG_SELECT_MODE:I = 0x16

.field public static final eEV_ERASE_MODE:I = 0x9

.field public static final eEV_FREEERASE_MODE:I = 0x5

.field public static final eEV_FREEHIGHLIGHT_MODE:I = 0x4

.field public static final eEV_HIGHLIGHT_MODE:I = 0xa

.field public static final eEV_INK_MODE:I = 0x1

.field public static final eEV_LASSO_MODE:I = 0x8

.field public static final eEV_LINEAR_MODE:I = 0x7

.field public static final eEV_LINE_MODE:I = 0xf

.field public static final eEV_NORMAL_MODE:I = 0x0

.field public static final eEV_OVAL_MODE:I = 0x11

.field public static final eEV_PENCLE_MODE:I = 0x2

.field public static final eEV_RECTANGLE_MODE:I = 0x10

.field public static final eEV_STICKYNOTE_MODE:I = 0xd

.field public static final eEV_STRIKEOUT_MODE:I = 0xc

.field public static final eEV_UNDERLINE_MODE:I = 0xb
