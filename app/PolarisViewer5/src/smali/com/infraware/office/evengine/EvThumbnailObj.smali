.class Lcom/infraware/office/evengine/EvThumbnailObj;
.super Lcom/infraware/office/evengine/EvTaskObj;
.source "EvTaskObj.java"


# instance fields
.field m_eMode:I

.field m_nHeight:I

.field m_nPages:I

.field m_nStartPage:I

.field m_nWidth:I


# direct methods
.method constructor <init>(Lcom/infraware/office/evengine/EvNative;IIIII)V
    .locals 0
    .param p1, "a_Native"    # Lcom/infraware/office/evengine/EvNative;
    .param p2, "a_eMode"    # I
    .param p3, "a_nPages"    # I
    .param p4, "a_nWidth"    # I
    .param p5, "a_nHeight"    # I
    .param p6, "a_nStartPage"    # I

    .prologue
    .line 458
    invoke-direct {p0, p1}, Lcom/infraware/office/evengine/EvTaskObj;-><init>(Lcom/infraware/office/evengine/EvNative;)V

    .line 459
    iput p2, p0, Lcom/infraware/office/evengine/EvThumbnailObj;->m_eMode:I

    .line 460
    iput p3, p0, Lcom/infraware/office/evengine/EvThumbnailObj;->m_nPages:I

    .line 461
    iput p4, p0, Lcom/infraware/office/evengine/EvThumbnailObj;->m_nWidth:I

    .line 462
    iput p5, p0, Lcom/infraware/office/evengine/EvThumbnailObj;->m_nHeight:I

    .line 463
    iput p6, p0, Lcom/infraware/office/evengine/EvThumbnailObj;->m_nStartPage:I

    .line 464
    return-void
.end method


# virtual methods
.method doTask()V
    .locals 6

    .prologue
    .line 468
    iget-object v0, p0, Lcom/infraware/office/evengine/EvThumbnailObj;->Native:Lcom/infraware/office/evengine/EvNative;

    iget v1, p0, Lcom/infraware/office/evengine/EvThumbnailObj;->m_eMode:I

    iget v2, p0, Lcom/infraware/office/evengine/EvThumbnailObj;->m_nPages:I

    iget v3, p0, Lcom/infraware/office/evengine/EvThumbnailObj;->m_nWidth:I

    iget v4, p0, Lcom/infraware/office/evengine/EvThumbnailObj;->m_nHeight:I

    iget v5, p0, Lcom/infraware/office/evengine/EvThumbnailObj;->m_nStartPage:I

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvNative;->IThumbnail(IIIII)V

    .line 469
    return-void
.end method
