.class public Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SHEET_AUTOFILTER_MENU_INFO"
.end annotation


# instance fields
.field public aCellPositionInfo:[I

.field public aIsCheckedInfo:[Z

.field public aIsFixedInfo:[Z

.field public aItemTitle:[Ljava/lang/String;

.field public nFocusIndex:I

.field public nHandleId:I

.field public nItemCount:I


# direct methods
.method constructor <init>(I)V
    .locals 1
    .param p1, "a_nItemCount"    # I

    .prologue
    const/4 v0, 0x0

    .line 2950
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2945
    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->aItemTitle:[Ljava/lang/String;

    .line 2946
    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->aIsFixedInfo:[Z

    .line 2947
    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->aIsCheckedInfo:[Z

    .line 2948
    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->aCellPositionInfo:[I

    .line 2952
    iput p1, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->nItemCount:I

    .line 2954
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->aItemTitle:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2955
    iget v0, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->nItemCount:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->aItemTitle:[Ljava/lang/String;

    .line 2958
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->aIsFixedInfo:[Z

    if-nez v0, :cond_1

    .line 2959
    iget v0, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->nItemCount:I

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->aIsFixedInfo:[Z

    .line 2962
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->aIsCheckedInfo:[Z

    if-nez v0, :cond_2

    .line 2963
    iget v0, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->nItemCount:I

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->aIsCheckedInfo:[Z

    .line 2966
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->aCellPositionInfo:[I

    if-nez v0, :cond_3

    .line 2967
    const/4 v0, 0x4

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->aCellPositionInfo:[I

    .line 2969
    :cond_3
    return-void
.end method


# virtual methods
.method clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2972
    const/4 v0, -0x3

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->nHandleId:I

    .line 2973
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->nFocusIndex:I

    .line 2974
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->nItemCount:I

    .line 2976
    iput-object v1, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->aItemTitle:[Ljava/lang/String;

    .line 2977
    iput-object v1, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->aIsFixedInfo:[Z

    .line 2978
    iput-object v1, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->aIsCheckedInfo:[Z

    .line 2979
    iput-object v1, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->aCellPositionInfo:[I

    .line 2980
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2984
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;

    .line 2985
    .local v0, "o":Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2940
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->clone()Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;

    move-result-object v0

    return-object v0
.end method
