.class public Lcom/infraware/office/evengine/EV$SHAPE_STRETCH;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SHAPE_STRETCH"
.end annotation


# instance fields
.field public nOffsetAbove:I

.field public nOffsetBottom:I

.field public nOffsetLeft:I

.field public nOffsetRight:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 2777
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SHAPE_STRETCH;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 2784
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_STRETCH;->nOffsetBottom:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_STRETCH;->nOffsetAbove:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_STRETCH;->nOffsetRight:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_STRETCH;->nOffsetLeft:I

    .line 2785
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SHAPE_STRETCH;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2789
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SHAPE_STRETCH;

    .line 2790
    .local v0, "o":Lcom/infraware/office/evengine/EV$SHAPE_STRETCH;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2777
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SHAPE_STRETCH;->clone()Lcom/infraware/office/evengine/EV$SHAPE_STRETCH;

    move-result-object v0

    return-object v0
.end method
