.class public Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DRAW_GRADIENTCOLOR_INFO"
.end annotation


# instance fields
.field public bBright:Z

.field public bSupport:Z

.field public lColor1:J

.field public lColor2:J

.field public lColor3:J

.field public lSelectedColor:J

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 2285
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 2296
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;->bBright:Z

    .line 2297
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;->bSupport:Z

    .line 2298
    iput-wide v1, p0, Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;->lColor3:J

    iput-wide v1, p0, Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;->lColor2:J

    iput-wide v1, p0, Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;->lColor1:J

    .line 2299
    iput-wide v1, p0, Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;->lSelectedColor:J

    .line 2300
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2303
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;

    .line 2304
    .local v0, "o":Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2285
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;->clone()Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;

    move-result-object v0

    return-object v0
.end method
