.class public Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SHAPE_PICTURE_CORRECTION"
.end annotation


# instance fields
.field public nBrightness:F

.field public nContrast:F

.field public nSoftenSharpen:F

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 2595
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 2602
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;->nContrast:F

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;->nBrightness:F

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;->nSoftenSharpen:F

    .line 2603
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2607
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;

    .line 2608
    .local v0, "o":Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2595
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;->clone()Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;

    move-result-object v0

    return-object v0
.end method
