.class public interface abstract Lcom/infraware/office/evengine/E$EV_SHAPE_EFFECT_MASK;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_SHAPE_EFFECT_MASK"
.end annotation


# static fields
.field public static final eEV_SHAPE_MASK_ALL:I = 0x1ff

.field public static final eEV_SHAPE_MASK_CUBE:I = 0x80

.field public static final eEV_SHAPE_MASK_DDD:I = 0x100

.field public static final eEV_SHAPE_MASK_NEON:I = 0x10

.field public static final eEV_SHAPE_MASK_NEON_COLOR:I = 0x20

.field public static final eEV_SHAPE_MASK_NEON_SIZE:I = 0x40

.field public static final eEV_SHAPE_MASK_NONE:I = 0x0

.field public static final eEV_SHAPE_MASK_REFLECT:I = 0x2

.field public static final eEV_SHAPE_MASK_REFLECT_SIZE:I = 0x8

.field public static final eEV_SHAPE_MASK_REFLECT_TRANS:I = 0x4

.field public static final eEV_SHAPE_MASK_SHADOW:I = 0x1
