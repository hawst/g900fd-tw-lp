.class public Lcom/infraware/office/evengine/EvShapeInterfaceUtil;
.super Ljava/lang/Object;
.source "EvShapeInterfaceUtil.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_SHAPE_3DFORMAT_BEVELTYPE_PRESET;
.implements Lcom/infraware/office/evengine/E$EV_SHAPE_3DROTATION_PRESET;
.implements Lcom/infraware/office/evengine/E$EV_SHAPE_FORMAT_SELECTOR;
.implements Lcom/infraware/office/evengine/E$EV_SHAPE_SHADOW_PRESET;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "SHAPE"

.field private static final LOG_TRACE:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static TraceLog(Ljava/util/Hashtable;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 340
    .local p0, "nShapeInfos":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Object;>;"
    if-nez p0, :cond_0

    .line 526
    :goto_0
    return-void

    .line 343
    :cond_0
    const-string/jumbo v13, "SHAPE"

    const-string/jumbo v14, "---------- [ShapeInfo] START ----------"

    invoke-static {v13, v14}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    if-eqz v13, :cond_1

    .line 346
    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;

    .line 348
    .local v8, "quickStyle":Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;
    const-string/jumbo v13, "SHAPE"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "QuickStyleInfo [nQuickStyleFrameType : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v8, Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;->nQuickStyleFrameType:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nShapePreset : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v8, Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;->nShapePreset:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nLinePreset : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v8, Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;->nLinePreset:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nPicturePreset : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v8, Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;->nPicturePreset:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    .end local v8    # "quickStyle":Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;
    :cond_1
    const/4 v13, 0x2

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    if-eqz v13, :cond_2

    .line 356
    const/4 v13, 0x2

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/infraware/office/evengine/EV$SHAPE_FILL;

    .line 364
    .local v3, "fill":Lcom/infraware/office/evengine/EV$SHAPE_FILL;
    iget-object v13, v3, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->stPictureTextureDetail:Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;

    if-eqz v13, :cond_2

    .line 365
    const-string/jumbo v13, "SHAPE"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "fillInfo [nFillSelector : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v3, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->nFillSelector:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nPictureTextureSelector : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v3, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->stPictureTextureDetail:Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;

    iget v15, v15, Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;->nPictureTextureSelector:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nPictureTextureType : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v3, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->stPictureTextureDetail:Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;

    iget v15, v15, Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;->nPictureTextureType:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    .end local v3    # "fill":Lcom/infraware/office/evengine/EV$SHAPE_FILL;
    :cond_2
    const/4 v13, 0x4

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    if-eqz v13, :cond_3

    .line 374
    const/4 v13, 0x4

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;

    .line 376
    .local v5, "lineColor":Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;
    const-string/jumbo v13, "SHAPE"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "LineColorInfo [nLineColorSelector : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v5, Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;->nLineColorSelector:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nSolidColor : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-wide v15, v5, Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;->nSolidColor:J

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nSolidTransparency : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v5, Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;->nSolidTransparency:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nGradientPresetColors : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v5, Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;->nGradientPresetColors:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nGradientType : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v5, Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;->nGradientType:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nGradientDirection : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v5, Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;->nGradientDirection:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nGradientStopNumbers : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v5, Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;->nGradientStopNumbers:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    .end local v5    # "lineColor":Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;
    :cond_3
    const/16 v13, 0x8

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    if-eqz v13, :cond_4

    .line 387
    const/16 v13, 0x8

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;

    .line 389
    .local v6, "lineStyle":Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;
    const-string/jumbo v13, "SHAPE"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "LineStyleInfo [nWidth : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v6, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;->nWidth:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nCompoundType : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v6, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;->nCompoundType:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nDashType : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v6, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;->nDashType:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nCapType : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v6, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;->nCapType:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nJoinType : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v6, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;->nJoinType:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    const-string/jumbo v13, "SHAPE"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "LineStyleInfo ArrowInfo [nBeginType : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v6, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;->stArrow:Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE$SHAPE_LINE_ARROW;

    iget v15, v15, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE$SHAPE_LINE_ARROW;->nBeginType:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nEndType : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v6, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;->stArrow:Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE$SHAPE_LINE_ARROW;

    iget v15, v15, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE$SHAPE_LINE_ARROW;->nEndType:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nBeginSize : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v6, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;->stArrow:Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE$SHAPE_LINE_ARROW;

    iget v15, v15, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE$SHAPE_LINE_ARROW;->nBeginSize:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nEndSize : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v6, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;->stArrow:Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE$SHAPE_LINE_ARROW;

    iget v15, v15, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE$SHAPE_LINE_ARROW;->nEndSize:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    .end local v6    # "lineStyle":Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;
    :cond_4
    const/16 v13, 0x10

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    if-eqz v13, :cond_5

    .line 402
    const/16 v13, 0x10

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;

    .line 404
    .local v10, "shadow":Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;
    const-string/jumbo v13, "SHAPE"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "ShadowInfo [nPreset : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v10, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;->nPreset:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nColor : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-wide v15, v10, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;->nColor:J

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nTransparency : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v10, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;->nTransparency:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nSize : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v10, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;->nSize:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nBlur : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v10, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;->nBlur:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nAngle : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v10, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;->nAngle:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nDistance : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v10, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;->nDistance:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    .end local v10    # "shadow":Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;
    :cond_5
    const/16 v13, 0x20

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    if-eqz v13, :cond_6

    .line 415
    const/16 v13, 0x20

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;

    .line 417
    .local v9, "reflect":Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;
    const-string/jumbo v13, "SHAPE"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "ReflectionInfo [nPreset : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v9, Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;->nPreset:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nTransparency : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v9, Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;->nTransparency:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nSize : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v9, Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;->nSize:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nDistance : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v9, Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;->nDistance:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nBlur : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v9, Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;->nBlur:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    .end local v9    # "reflect":Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;
    :cond_6
    const/16 v13, 0x40

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    if-eqz v13, :cond_7

    .line 426
    const/16 v13, 0x40

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/infraware/office/evengine/EV$SHAPE_GLOW;

    .line 428
    .local v4, "glow":Lcom/infraware/office/evengine/EV$SHAPE_GLOW;
    const-string/jumbo v13, "SHAPE"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "GlowInfo [nPreset : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v4, Lcom/infraware/office/evengine/EV$SHAPE_GLOW;->nPreset:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nColor : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-wide v15, v4, Lcom/infraware/office/evengine/EV$SHAPE_GLOW;->nColor:J

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nSize : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v4, Lcom/infraware/office/evengine/EV$SHAPE_GLOW;->nSize:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nTransparency : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v4, Lcom/infraware/office/evengine/EV$SHAPE_GLOW;->nTransparency:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    .end local v4    # "glow":Lcom/infraware/office/evengine/EV$SHAPE_GLOW;
    :cond_7
    const/16 v13, 0x80

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    if-eqz v13, :cond_8

    .line 436
    const/16 v13, 0x80

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;

    .line 438
    .local v12, "softEdge":Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;
    const-string/jumbo v13, "SHAPE"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "SoftEdgeInfo [nPreset : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v12, Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;->nPreset:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nSize : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v12, Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;->nSize:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    .end local v12    # "softEdge":Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;
    :cond_8
    const/16 v13, 0x100

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    if-eqz v13, :cond_9

    .line 444
    const/16 v13, 0x100

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;

    .line 446
    .local v1, "_3dFormat":Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;
    const-string/jumbo v13, "SHAPE"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "3DFormatInfo [nBevelTopType : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v1, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBevelTopType:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nBevelTopWidth : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v1, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBevelTopWidth:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nBevelTopHeight : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v1, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBevelTopHeight:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nBevelBottomType : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v1, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBevelBottomType:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nBevelBottomWidth : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v1, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBevelBottomWidth:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nBevelBottomHeight : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v1, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBevelBottomHeight:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nDepthColor : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-wide v15, v1, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nDepthColor:J

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nBackDepth : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v1, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBackDepth:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nContourColor : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-wide v15, v1, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nContourColor:J

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nContourSize : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v1, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nContourSize:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nSurfaceMaterial : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v1, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nSurfaceMaterial:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nSurfaceLight : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v1, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nSurfaceLight:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nSurfaceAngle : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v1, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nSurfaceAngle:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    .end local v1    # "_3dFormat":Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;
    :cond_9
    const/16 v13, 0x200

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    if-eqz v13, :cond_a

    .line 463
    const/16 v13, 0x200

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;

    .line 465
    .local v2, "_3dRotation":Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;
    const-string/jumbo v13, "SHAPE"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "3DRotationInfo [nPreset : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v2, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nPreset:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , n3DCameraType : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v2, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->n3DCameraType:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nXRotation : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v2, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nXRotation:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nYRotation : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v2, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nYRotation:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nZRotation : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v2, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nZRotation:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nPerspectiveAngle : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v2, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nPerspectiveAngle:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nObjectPos : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v2, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nObjectPos:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    .end local v2    # "_3dRotation":Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;
    :cond_a
    const/16 v13, 0x400

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    if-eqz v13, :cond_b

    .line 476
    const/16 v13, 0x400

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;

    .line 478
    .local v7, "pPictureCorrection":Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;
    const-string/jumbo v13, "SHAPE"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "PictureCorrectionInfo [nSoftenSharpen : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v7, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;->nSoftenSharpen:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nBrightness : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v7, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;->nBrightness:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nContrast : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v7, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;->nContrast:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    .end local v7    # "pPictureCorrection":Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;
    :cond_b
    const/16 v13, 0x800

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    if-eqz v13, :cond_c

    .line 487
    const-string/jumbo v13, "SHAPE"

    const-string/jumbo v14, "PictureColorInfo "

    invoke-static {v13, v14}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    :cond_c
    const/16 v13, 0x1000

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    if-eqz v13, :cond_d

    .line 493
    const-string/jumbo v13, "SHAPE"

    const-string/jumbo v14, "ArtisticEffectInfo "

    invoke-static {v13, v14}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    :cond_d
    const/16 v13, 0x2000

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    if-eqz v13, :cond_e

    .line 499
    const-string/jumbo v13, "SHAPE"

    const-string/jumbo v14, "CroppingInfo "

    invoke-static {v13, v14}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    :cond_e
    const/16 v13, 0x4000

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    if-eqz v13, :cond_f

    .line 504
    const/16 v13, 0x4000

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;

    .line 506
    .local v11, "size":Lcom/infraware/office/evengine/EV$SHAPE_SIZE;
    const-string/jumbo v13, "SHAPE"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "SizeInfo [nWidth : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v11, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;->nWidth:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nHeight : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v11, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;->nHeight:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , bFlip : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-boolean v15, v11, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;->bFlip:Z

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , bMirror : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-boolean v15, v11, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;->bMirror:Z

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " , nRotation : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v11, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;->nRotation:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    .end local v11    # "size":Lcom/infraware/office/evengine/EV$SHAPE_SIZE;
    :cond_f
    const v13, 0x8000

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    if-eqz v13, :cond_10

    .line 516
    const-string/jumbo v13, "SHAPE"

    const-string/jumbo v14, "LocationInfo "

    invoke-static {v13, v14}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    :cond_10
    const/high16 v13, 0x10000

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    if-eqz v13, :cond_11

    .line 522
    const-string/jumbo v13, "SHAPE"

    const-string/jumbo v14, "TextboxInfo "

    invoke-static {v13, v14}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    :cond_11
    const-string/jumbo v13, "SHAPE"

    const-string/jumbo v14, "---------- [ShapeInfo] END ----------"

    invoke-static {v13, v14}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static getDefault3DFormat(Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;I)Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;
    .locals 2
    .param p0, "p3DFormat"    # Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;
    .param p1, "nPreset"    # I

    .prologue
    const/high16 v1, 0x40c00000    # 6.0f

    .line 576
    packed-switch p1, :pswitch_data_0

    .line 617
    iput p1, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBevelTopType:I

    .line 621
    :goto_0
    :pswitch_0
    if-eqz p1, :cond_0

    .line 622
    iput v1, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBevelTopWidth:F

    .line 623
    iput v1, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBevelTopHeight:F

    .line 626
    :cond_0
    return-object p0

    .line 581
    :pswitch_1
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBevelTopType:I

    goto :goto_0

    .line 584
    :pswitch_2
    const/4 v0, 0x2

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBevelTopType:I

    goto :goto_0

    .line 587
    :pswitch_3
    const/4 v0, 0x3

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBevelTopType:I

    goto :goto_0

    .line 590
    :pswitch_4
    const/4 v0, 0x4

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBevelTopType:I

    goto :goto_0

    .line 593
    :pswitch_5
    const/4 v0, 0x5

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBevelTopType:I

    goto :goto_0

    .line 596
    :pswitch_6
    const/4 v0, 0x6

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBevelTopType:I

    goto :goto_0

    .line 599
    :pswitch_7
    const/4 v0, 0x7

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBevelTopType:I

    goto :goto_0

    .line 602
    :pswitch_8
    const/16 v0, 0x8

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBevelTopType:I

    goto :goto_0

    .line 605
    :pswitch_9
    const/16 v0, 0x9

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBevelTopType:I

    goto :goto_0

    .line 608
    :pswitch_a
    const/16 v0, 0xa

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBevelTopType:I

    goto :goto_0

    .line 611
    :pswitch_b
    const/16 v0, 0xb

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBevelTopType:I

    goto :goto_0

    .line 614
    :pswitch_c
    const/16 v0, 0xc

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->nBevelTopType:I

    goto :goto_0

    .line 576
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public static getDefault3DRotation(Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;I)Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;
    .locals 8
    .param p0, "p3DRotation"    # Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;
    .param p1, "nPreset"    # I

    .prologue
    const/high16 v7, 0x439d0000    # 314.0f

    const v6, 0x43968000    # 301.0f

    const/high16 v5, 0x420c0000    # 35.0f

    const/high16 v4, 0x41900000    # 18.0f

    const/4 v3, 0x0

    .line 631
    const-string/jumbo v0, "EvShapeInterfaceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "3DRoatation nPreset "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    packed-switch p1, :pswitch_data_0

    .line 722
    :goto_0
    return-object p0

    .line 636
    :pswitch_0
    const/16 v0, 0x9

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nPreset:I

    .line 637
    const/high16 v0, 0x41d00000    # 26.0f

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nXRotation:F

    .line 638
    iput v4, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nYRotation:F

    .line 639
    iput v3, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nZRotation:F

    goto :goto_0

    .line 644
    :pswitch_1
    const/4 v0, 0x7

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nPreset:I

    .line 645
    const/high16 v0, 0x43a70000    # 334.0f

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nXRotation:F

    .line 646
    iput v4, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nYRotation:F

    .line 647
    iput v3, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nZRotation:F

    goto :goto_0

    .line 652
    :pswitch_2
    const/4 v0, 0x2

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nPreset:I

    .line 653
    const/high16 v0, 0x42340000    # 45.0f

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nXRotation:F

    .line 654
    iput v5, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nYRotation:F

    .line 655
    iput v3, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nZRotation:F

    goto :goto_0

    .line 660
    :pswitch_3
    const/4 v0, 0x3

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nPreset:I

    .line 661
    const v0, 0x439d8000    # 315.0f

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nXRotation:F

    .line 662
    iput v5, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nYRotation:F

    .line 663
    iput v3, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nZRotation:F

    goto :goto_0

    .line 668
    :pswitch_4
    const/4 v0, 0x6

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nPreset:I

    .line 669
    const/high16 v0, 0x42800000    # 64.0f

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nXRotation:F

    .line 670
    iput v4, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nYRotation:F

    .line 671
    iput v3, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nZRotation:F

    goto :goto_0

    .line 676
    :pswitch_5
    const/16 v0, 0xa

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nPreset:I

    .line 677
    const/high16 v0, 0x43940000    # 296.0f

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nXRotation:F

    .line 678
    iput v4, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nYRotation:F

    .line 679
    iput v3, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nZRotation:F

    goto :goto_0

    .line 684
    :pswitch_6
    const/4 v0, 0x4

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nPreset:I

    .line 685
    iput v7, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nXRotation:F

    .line 686
    const/high16 v0, 0x43a20000    # 324.0f

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nYRotation:F

    .line 687
    const/high16 v0, 0x42700000    # 60.0f

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nZRotation:F

    goto :goto_0

    .line 692
    :pswitch_7
    const/4 v0, 0x5

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nPreset:I

    .line 693
    iput v7, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nXRotation:F

    .line 694
    iput v5, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nYRotation:F

    .line 695
    const v0, 0x43958000    # 299.0f

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nZRotation:F

    goto :goto_0

    .line 700
    :pswitch_8
    const/16 v0, 0xb

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nPreset:I

    .line 701
    const/high16 v0, 0x42540000    # 53.0f

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nXRotation:F

    .line 702
    iput v6, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nYRotation:F

    .line 703
    const/high16 v0, 0x43970000    # 302.0f

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nZRotation:F

    goto :goto_0

    .line 708
    :pswitch_9
    const/16 v0, 0x8

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nPreset:I

    .line 709
    const/high16 v0, 0x43990000    # 306.0f

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nXRotation:F

    .line 710
    iput v6, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nYRotation:F

    .line 711
    const/high16 v0, 0x42640000    # 57.0f

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nZRotation:F

    goto/16 :goto_0

    .line 715
    :pswitch_a
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nPreset:I

    .line 716
    iput v3, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nXRotation:F

    .line 717
    iput v3, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nYRotation:F

    .line 718
    iput v3, p0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->nZRotation:F

    goto/16 :goto_0

    .line 632
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static getShadowInfo(Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;I)Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;
    .locals 7
    .param p0, "pShadow"    # Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;
    .param p1, "nPreset"    # I

    .prologue
    .line 530
    const-wide/16 v2, 0x0

    .line 531
    .local v2, "color":J
    const/4 v6, 0x0

    .line 532
    .local v6, "transparency":I
    const/4 v5, 0x0

    .local v5, "size":F
    const/4 v1, 0x0

    .local v1, "blur":F
    const/4 v0, 0x0

    .local v0, "angle":F
    const/4 v4, 0x0

    .line 534
    .local v4, "distance":F
    packed-switch p1, :pswitch_data_0

    .line 563
    :goto_0
    iput p1, p0, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;->nPreset:I

    .line 564
    iput-wide v2, p0, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;->nColor:J

    .line 565
    iput v6, p0, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;->nTransparency:I

    .line 566
    iput v5, p0, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;->nSize:F

    .line 567
    iput v1, p0, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;->nBlur:F

    .line 568
    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;->nAngle:F

    .line 569
    iput v4, p0, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;->nDistance:F

    .line 571
    return-object p0

    .line 535
    :pswitch_0
    const-wide/32 v2, -0x1000000

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    const/4 v4, 0x0

    goto :goto_0

    .line 536
    :pswitch_1
    const-wide/32 v2, -0x1000000

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    const/4 v4, 0x0

    goto :goto_0

    .line 537
    :pswitch_2
    const-wide/32 v2, -0x1000000

    const/16 v6, 0x3c

    const/high16 v5, 0x42c80000    # 100.0f

    const/high16 v1, 0x40800000    # 4.0f

    const/high16 v0, 0x43340000    # 180.0f

    const/high16 v4, 0x40400000    # 3.0f

    goto :goto_0

    .line 538
    :pswitch_3
    const-wide/32 v2, -0x1000000

    const/16 v6, 0x3c

    const/high16 v5, 0x42c80000    # 100.0f

    const/high16 v1, 0x40800000    # 4.0f

    const/4 v0, 0x0

    const/high16 v4, 0x40400000    # 3.0f

    goto :goto_0

    .line 539
    :pswitch_4
    const-wide/32 v2, -0x1000000

    const/16 v6, 0x3c

    const/high16 v5, 0x42c80000    # 100.0f

    const/high16 v1, 0x40800000    # 4.0f

    const/high16 v0, 0x43870000    # 270.0f

    const/high16 v4, 0x40400000    # 3.0f

    goto :goto_0

    .line 540
    :pswitch_5
    const-wide/32 v2, -0x1000000

    const/16 v6, 0x3c

    const/high16 v5, 0x42c80000    # 100.0f

    const/high16 v1, 0x40800000    # 4.0f

    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v4, 0x40400000    # 3.0f

    goto :goto_0

    .line 541
    :pswitch_6
    const-wide/32 v2, -0x1000000

    const/16 v6, 0x3c

    const/high16 v5, 0x42c80000    # 100.0f

    const/high16 v1, 0x40800000    # 4.0f

    const/high16 v0, 0x42340000    # 45.0f

    const/high16 v4, 0x40400000    # 3.0f

    goto :goto_0

    .line 542
    :pswitch_7
    const-wide/32 v2, -0x1000000

    const/16 v6, 0x3c

    const/high16 v5, 0x42c80000    # 100.0f

    const/high16 v1, 0x40800000    # 4.0f

    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v4, 0x40400000    # 3.0f

    goto :goto_0

    .line 543
    :pswitch_8
    const-wide/32 v2, -0x1000000

    const/16 v6, 0x3c

    const/high16 v5, 0x42c80000    # 100.0f

    const/high16 v1, 0x40800000    # 4.0f

    const v0, 0x439d8000    # 315.0f

    const/high16 v4, 0x40400000    # 3.0f

    goto/16 :goto_0

    .line 544
    :pswitch_9
    const-wide/32 v2, -0x1000000

    const/16 v6, 0x3c

    const/high16 v5, 0x42c80000    # 100.0f

    const/high16 v1, 0x40800000    # 4.0f

    const/high16 v0, 0x43600000    # 224.0f

    const/high16 v4, 0x40400000    # 3.0f

    goto/16 :goto_0

    .line 545
    :pswitch_a
    const-wide/32 v2, -0x1000000

    const/16 v6, 0x3c

    const/high16 v5, 0x42cc0000    # 102.0f

    const/high16 v1, 0x40a00000    # 5.0f

    const/4 v0, 0x0

    const/4 v4, 0x0

    goto/16 :goto_0

    .line 546
    :pswitch_b
    const-wide/32 v2, -0x1000000

    const/16 v6, 0x32

    const/4 v5, 0x0

    const/high16 v1, 0x40a00000    # 5.0f

    const/high16 v0, 0x43340000    # 180.0f

    const/high16 v4, 0x40800000    # 4.0f

    goto/16 :goto_0

    .line 547
    :pswitch_c
    const-wide/32 v2, -0x1000000

    const/16 v6, 0x32

    const/4 v5, 0x0

    const/high16 v1, 0x40a00000    # 5.0f

    const/4 v0, 0x0

    const/high16 v4, 0x40800000    # 4.0f

    goto/16 :goto_0

    .line 548
    :pswitch_d
    const-wide/32 v2, -0x1000000

    const/16 v6, 0x32

    const/4 v5, 0x0

    const/high16 v1, 0x40a00000    # 5.0f

    const/high16 v0, 0x43870000    # 270.0f

    const/high16 v4, 0x40800000    # 4.0f

    goto/16 :goto_0

    .line 549
    :pswitch_e
    const-wide/32 v2, -0x1000000

    const/16 v6, 0x32

    const/4 v5, 0x0

    const/high16 v1, 0x40a00000    # 5.0f

    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v4, 0x40800000    # 4.0f

    goto/16 :goto_0

    .line 550
    :pswitch_f
    const-wide/32 v2, -0x1000000

    const/16 v6, 0x32

    const/4 v5, 0x0

    const/high16 v1, 0x40a00000    # 5.0f

    const/high16 v0, 0x42340000    # 45.0f

    const/high16 v4, 0x40800000    # 4.0f

    goto/16 :goto_0

    .line 551
    :pswitch_10
    const-wide/32 v2, -0x1000000

    const/16 v6, 0x32

    const/4 v5, 0x0

    const/high16 v1, 0x40a00000    # 5.0f

    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v4, 0x40800000    # 4.0f

    goto/16 :goto_0

    .line 552
    :pswitch_11
    const-wide/32 v2, -0x1000000

    const/16 v6, 0x32

    const/4 v5, 0x0

    const/high16 v1, 0x40a00000    # 5.0f

    const v0, 0x439d8000    # 315.0f

    const/high16 v4, 0x40800000    # 4.0f

    goto/16 :goto_0

    .line 553
    :pswitch_12
    const-wide/32 v2, -0x1000000

    const/16 v6, 0x32

    const/4 v5, 0x0

    const/high16 v1, 0x40a00000    # 5.0f

    const/high16 v0, 0x43610000    # 225.0f

    const/high16 v4, 0x40800000    # 4.0f

    goto/16 :goto_0

    .line 554
    :pswitch_13
    const-wide/32 v2, -0x1000000

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/high16 v1, 0x41100000    # 9.0f

    const/4 v0, 0x0

    const/high16 v4, 0x40800000    # 4.0f

    goto/16 :goto_0

    .line 555
    :pswitch_14
    const-wide/32 v2, -0x1000000

    const/16 v6, 0x50

    const/high16 v5, 0x42c80000    # 100.0f

    const/high16 v1, 0x40c00000    # 6.0f

    const/high16 v0, 0x43610000    # 225.0f

    const/4 v4, 0x0

    goto/16 :goto_0

    .line 556
    :pswitch_15
    const-wide/32 v2, -0x1000000

    const/16 v6, 0x50

    const/high16 v5, 0x42c80000    # 100.0f

    const/high16 v1, 0x40c00000    # 6.0f

    const v0, 0x439d8000    # 315.0f

    const/4 v4, 0x0

    goto/16 :goto_0

    .line 557
    :pswitch_16
    const-wide/32 v2, -0x1000000

    const/16 v6, 0x55

    const/high16 v5, 0x42b40000    # 90.0f

    const/high16 v1, 0x41400000    # 12.0f

    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v4, 0x41c80000    # 25.0f

    goto/16 :goto_0

    .line 558
    :pswitch_17
    const-wide/32 v2, -0x1000000

    const/16 v6, 0x50

    const/high16 v5, 0x42c80000    # 100.0f

    const/high16 v1, 0x40c00000    # 6.0f

    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v4, 0x3f800000    # 1.0f

    goto/16 :goto_0

    .line 559
    :pswitch_18
    const-wide/32 v2, -0x1000000

    const/16 v6, 0x50

    const/high16 v5, 0x42c80000    # 100.0f

    const/high16 v1, 0x40c00000    # 6.0f

    const/high16 v0, 0x42340000    # 45.0f

    const/high16 v4, 0x3f800000    # 1.0f

    goto/16 :goto_0

    .line 560
    :pswitch_19
    const-wide/32 v2, -0x1000000

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    const/4 v4, 0x0

    goto/16 :goto_0

    .line 534
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
    .end packed-switch
.end method

.method public static getShapeInfo(Lcom/infraware/office/evengine/EvInterface;I)Ljava/util/Hashtable;
    .locals 17
    .param p0, "aInterface"    # Lcom/infraware/office/evengine/EvInterface;
    .param p1, "nSelector"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/infraware/office/evengine/EvInterface;",
            "I)",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    const/4 v8, 0x0

    .line 207
    .local v8, "nShapeInfos":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Object;>;"
    if-nez p1, :cond_0

    move-object v9, v8

    .line 336
    .end local v8    # "nShapeInfos":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Object;>;"
    .local v9, "nShapeInfos":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Object;>;"
    :goto_0
    return-object v9

    .line 210
    .end local v9    # "nShapeInfos":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Object;>;"
    .restart local v8    # "nShapeInfos":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Object;>;"
    :cond_0
    new-instance v8, Ljava/util/Hashtable;

    .end local v8    # "nShapeInfos":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Object;>;"
    invoke-direct {v8}, Ljava/util/Hashtable;-><init>()V

    .line 212
    .restart local v8    # "nShapeInfos":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Object;>;"
    and-int/lit8 v16, p1, 0x1

    if-lez v16, :cond_1

    .line 214
    if-eqz p0, :cond_1

    if-eqz v8, :cond_1

    .line 215
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/evengine/EvInterface;->IGetShapeQuickStyleInfo()Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;

    move-result-object v11

    .line 216
    .local v11, "quickStyle":Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;
    const/16 v16, 0x1

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v8, v0, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    .end local v11    # "quickStyle":Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;
    :cond_1
    and-int/lit8 v16, p1, 0x2

    if-lez v16, :cond_2

    .line 222
    if-eqz p0, :cond_2

    if-eqz v8, :cond_2

    .line 223
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/evengine/EvInterface;->IGetShapeFillInfo()Lcom/infraware/office/evengine/EV$SHAPE_FILL;

    move-result-object v10

    .line 224
    .local v10, "pFill":Lcom/infraware/office/evengine/EV$SHAPE_FILL;
    const/16 v16, 0x2

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v8, v0, v10}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    .end local v10    # "pFill":Lcom/infraware/office/evengine/EV$SHAPE_FILL;
    :cond_2
    and-int/lit8 v16, p1, 0x4

    if-lez v16, :cond_3

    .line 230
    if-eqz p0, :cond_3

    if-eqz v8, :cond_3

    .line 231
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/evengine/EvInterface;->IGetShapeLineColorInfo()Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;

    move-result-object v6

    .line 232
    .local v6, "lineColor":Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;
    const/16 v16, 0x4

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v8, v0, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    .end local v6    # "lineColor":Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;
    :cond_3
    and-int/lit8 v16, p1, 0x8

    if-lez v16, :cond_4

    .line 238
    if-eqz p0, :cond_4

    if-eqz v8, :cond_4

    .line 239
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/evengine/EvInterface;->IGetShapeLineStyleInfo()Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;

    move-result-object v7

    .line 240
    .local v7, "lineStyle":Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;
    const/16 v16, 0x8

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v8, v0, v7}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    .end local v7    # "lineStyle":Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;
    :cond_4
    and-int/lit8 v16, p1, 0x10

    if-lez v16, :cond_5

    .line 246
    if-eqz p0, :cond_5

    if-eqz v8, :cond_5

    .line 247
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/evengine/EvInterface;->IGetShapeShadowInfo()Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;

    move-result-object v13

    .line 248
    .local v13, "shadow":Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;
    const/16 v16, 0x10

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v8, v0, v13}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    .end local v13    # "shadow":Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;
    :cond_5
    and-int/lit8 v16, p1, 0x20

    if-lez v16, :cond_6

    .line 254
    if-eqz p0, :cond_6

    if-eqz v8, :cond_6

    .line 255
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/evengine/EvInterface;->IGetShapeReflectionInfo()Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;

    move-result-object v12

    .line 256
    .local v12, "reflection":Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;
    const/16 v16, 0x20

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v8, v0, v12}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    .end local v12    # "reflection":Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;
    :cond_6
    and-int/lit8 v16, p1, 0x40

    if-lez v16, :cond_7

    .line 262
    if-eqz p0, :cond_7

    if-eqz v8, :cond_7

    .line 263
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/evengine/EvInterface;->IGetShapeGlowInfo()Lcom/infraware/office/evengine/EV$SHAPE_GLOW;

    move-result-object v5

    .line 264
    .local v5, "glow":Lcom/infraware/office/evengine/EV$SHAPE_GLOW;
    const/16 v16, 0x40

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v8, v0, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    .end local v5    # "glow":Lcom/infraware/office/evengine/EV$SHAPE_GLOW;
    :cond_7
    move/from16 v0, p1

    and-int/lit16 v0, v0, 0x80

    move/from16 v16, v0

    if-lez v16, :cond_8

    .line 270
    if-eqz p0, :cond_8

    if-eqz v8, :cond_8

    .line 271
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/evengine/EvInterface;->IGetShapeSoftEdgeInfo()Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;

    move-result-object v15

    .line 272
    .local v15, "softEdge":Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;
    const/16 v16, 0x80

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v8, v0, v15}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    .end local v15    # "softEdge":Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;
    :cond_8
    move/from16 v0, p1

    and-int/lit16 v0, v0, 0x100

    move/from16 v16, v0

    if-lez v16, :cond_9

    .line 278
    if-eqz p0, :cond_9

    if-eqz v8, :cond_9

    .line 279
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/evengine/EvInterface;->IGetShape3DFormatInfo()Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;

    move-result-object v1

    .line 280
    .local v1, "_3DFormat":Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;
    const/16 v16, 0x100

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v8, v0, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    .end local v1    # "_3DFormat":Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;
    :cond_9
    move/from16 v0, p1

    and-int/lit16 v0, v0, 0x200

    move/from16 v16, v0

    if-lez v16, :cond_a

    .line 286
    if-eqz p0, :cond_a

    if-eqz v8, :cond_a

    .line 287
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/evengine/EvInterface;->IGetShape3DRotationInfo()Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;

    move-result-object v2

    .line 288
    .local v2, "_3DRotation":Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;
    const/16 v16, 0x200

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v8, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    .end local v2    # "_3DRotation":Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;
    :cond_a
    move/from16 v0, p1

    and-int/lit16 v0, v0, 0x400

    move/from16 v16, v0

    if-lez v16, :cond_b

    .line 294
    if-eqz p0, :cond_b

    if-eqz v8, :cond_b

    .line 295
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/evengine/EvInterface;->IGetShapePictureCorrectionInfo()Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;

    move-result-object v4

    .line 296
    .local v4, "_PictureCorrection":Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;
    const/16 v16, 0x400

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v8, v0, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    .end local v4    # "_PictureCorrection":Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;
    :cond_b
    move/from16 v0, p1

    and-int/lit16 v0, v0, 0x800

    move/from16 v16, v0

    if-lez v16, :cond_c

    .line 304
    :cond_c
    move/from16 v0, p1

    and-int/lit16 v0, v0, 0x1000

    move/from16 v16, v0

    if-lez v16, :cond_d

    .line 308
    :cond_d
    move/from16 v0, p1

    and-int/lit16 v0, v0, 0x2000

    move/from16 v16, v0

    if-lez v16, :cond_e

    .line 310
    if-eqz p0, :cond_e

    if-eqz v8, :cond_e

    .line 311
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/evengine/EvInterface;->IGetShapeCroppingInfo()Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;

    move-result-object v3

    .line 312
    .local v3, "_Cropping":Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;
    const/16 v16, 0x2000

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v8, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 317
    .end local v3    # "_Cropping":Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;
    :cond_e
    move/from16 v0, p1

    and-int/lit16 v0, v0, 0x4000

    move/from16 v16, v0

    if-lez v16, :cond_f

    .line 319
    if-eqz p0, :cond_f

    if-eqz v8, :cond_f

    .line 320
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/evengine/EvInterface;->IGetShapeSizeInfo()Lcom/infraware/office/evengine/EV$SHAPE_SIZE;

    move-result-object v14

    .line 321
    .local v14, "size":Lcom/infraware/office/evengine/EV$SHAPE_SIZE;
    const/16 v16, 0x4000

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v8, v0, v14}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    .end local v14    # "size":Lcom/infraware/office/evengine/EV$SHAPE_SIZE;
    :cond_f
    const v16, 0x8000

    and-int v16, v16, p1

    if-lez v16, :cond_10

    .line 329
    :cond_10
    const/high16 v16, 0x10000

    and-int v16, v16, p1

    if-lez v16, :cond_11

    :cond_11
    move-object v9, v8

    .line 336
    .end local v8    # "nShapeInfos":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Object;>;"
    .restart local v9    # "nShapeInfos":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Object;>;"
    goto/16 :goto_0
.end method

.method public static setShapeInfo(Lcom/infraware/office/evengine/EvInterface;Ljava/util/Hashtable;)V
    .locals 20
    .param p0, "aInterface"    # Lcom/infraware/office/evengine/EvInterface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/infraware/office/evengine/EvInterface;",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p1, "nShapeInfos":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Object;>;"
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljava/util/Hashtable;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 194
    :cond_0
    :goto_0
    return-void

    .line 52
    :cond_1
    const/4 v2, 0x0

    .line 53
    .local v2, "nSelector":I
    const/4 v3, 0x0

    .line 54
    .local v3, "pQuickStyle":Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;
    const/4 v4, 0x0

    .line 55
    .local v4, "pFill":Lcom/infraware/office/evengine/EV$SHAPE_FILL;
    const/4 v5, 0x0

    .line 56
    .local v5, "pLineColor":Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;
    const/4 v6, 0x0

    .line 57
    .local v6, "pLineStyle":Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;
    const/4 v7, 0x0

    .line 58
    .local v7, "pShadow":Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;
    const/4 v8, 0x0

    .line 59
    .local v8, "pReflection":Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;
    const/4 v9, 0x0

    .line 60
    .local v9, "pGlow":Lcom/infraware/office/evengine/EV$SHAPE_GLOW;
    const/4 v10, 0x0

    .line 61
    .local v10, "pSoftEdge":Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;
    const/4 v11, 0x0

    .line 62
    .local v11, "p3DFormat":Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;
    const/4 v12, 0x0

    .line 63
    .local v12, "p3DRotation":Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;
    const/4 v13, 0x0

    .line 64
    .local v13, "pPictureCorrection":Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;
    const/4 v14, 0x0

    .line 65
    .local v14, "pPictureColor":Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;
    const/4 v15, 0x0

    .line 66
    .local v15, "pArtisticEffect":Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;
    const/16 v16, 0x0

    .line 67
    .local v16, "pCropping":Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;
    const/16 v17, 0x0

    .line 68
    .local v17, "pSize":Lcom/infraware/office/evengine/EV$SHAPE_SIZE;
    const/16 v18, 0x0

    .line 69
    .local v18, "pLocation":Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;
    const/16 v19, 0x0

    .line 71
    .local v19, "pTextbox":Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 73
    or-int/lit8 v2, v2, 0x1

    .line 74
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "pQuickStyle":Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;
    check-cast v3, Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;

    .line 77
    .restart local v3    # "pQuickStyle":Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;
    :cond_2
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 79
    or-int/lit8 v2, v2, 0x2

    .line 80
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "pFill":Lcom/infraware/office/evengine/EV$SHAPE_FILL;
    check-cast v4, Lcom/infraware/office/evengine/EV$SHAPE_FILL;

    .line 83
    .restart local v4    # "pFill":Lcom/infraware/office/evengine/EV$SHAPE_FILL;
    :cond_3
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 85
    or-int/lit8 v2, v2, 0x4

    .line 86
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "pLineColor":Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;
    check-cast v5, Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;

    .line 89
    .restart local v5    # "pLineColor":Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;
    :cond_4
    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 91
    or-int/lit8 v2, v2, 0x8

    .line 92
    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "pLineStyle":Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;
    check-cast v6, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;

    .line 95
    .restart local v6    # "pLineStyle":Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;
    :cond_5
    const/16 v1, 0x10

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 97
    or-int/lit8 v2, v2, 0x10

    .line 98
    const/16 v1, 0x10

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "pShadow":Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;
    check-cast v7, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;

    .line 101
    .restart local v7    # "pShadow":Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;
    :cond_6
    const/16 v1, 0x20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 103
    or-int/lit8 v2, v2, 0x20

    .line 104
    const/16 v1, 0x20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "pReflection":Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;
    check-cast v8, Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;

    .line 107
    .restart local v8    # "pReflection":Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;
    :cond_7
    const/16 v1, 0x40

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 109
    or-int/lit8 v2, v2, 0x40

    .line 110
    const/16 v1, 0x40

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "pGlow":Lcom/infraware/office/evengine/EV$SHAPE_GLOW;
    check-cast v9, Lcom/infraware/office/evengine/EV$SHAPE_GLOW;

    .line 113
    .restart local v9    # "pGlow":Lcom/infraware/office/evengine/EV$SHAPE_GLOW;
    :cond_8
    const/16 v1, 0x80

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 115
    or-int/lit16 v2, v2, 0x80

    .line 116
    const/16 v1, 0x80

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "pSoftEdge":Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;
    check-cast v10, Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;

    .line 119
    .restart local v10    # "pSoftEdge":Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;
    :cond_9
    const/16 v1, 0x100

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 121
    or-int/lit16 v2, v2, 0x100

    .line 122
    const/16 v1, 0x100

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    .end local v11    # "p3DFormat":Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;
    check-cast v11, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;

    .line 125
    .restart local v11    # "p3DFormat":Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;
    :cond_a
    const/16 v1, 0x200

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_b

    .line 127
    or-int/lit16 v2, v2, 0x200

    .line 128
    const/16 v1, 0x200

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    .end local v12    # "p3DRotation":Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;
    check-cast v12, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;

    .line 131
    .restart local v12    # "p3DRotation":Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;
    :cond_b
    const/16 v1, 0x400

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 133
    or-int/lit16 v2, v2, 0x400

    .line 134
    const/16 v1, 0x400

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    .end local v13    # "pPictureCorrection":Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;
    check-cast v13, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;

    .line 137
    .restart local v13    # "pPictureCorrection":Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;
    :cond_c
    const/16 v1, 0x800

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_d

    .line 139
    or-int/lit16 v2, v2, 0x800

    .line 140
    const/16 v1, 0x800

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    .end local v14    # "pPictureColor":Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;
    check-cast v14, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;

    .line 143
    .restart local v14    # "pPictureColor":Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;
    :cond_d
    const/16 v1, 0x1000

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_e

    .line 145
    or-int/lit16 v2, v2, 0x1000

    .line 146
    const/16 v1, 0x1000

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    .end local v15    # "pArtisticEffect":Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;
    check-cast v15, Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;

    .line 149
    .restart local v15    # "pArtisticEffect":Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;
    :cond_e
    const/16 v1, 0x2000

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_f

    .line 151
    or-int/lit16 v2, v2, 0x2000

    .line 152
    const/16 v1, 0x2000

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "pCropping":Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;
    check-cast v16, Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;

    .line 155
    .restart local v16    # "pCropping":Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;
    :cond_f
    const/16 v1, 0x4000

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_10

    .line 157
    or-int/lit16 v2, v2, 0x4000

    .line 158
    const/16 v1, 0x4000

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    .end local v17    # "pSize":Lcom/infraware/office/evengine/EV$SHAPE_SIZE;
    check-cast v17, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;

    .line 161
    .restart local v17    # "pSize":Lcom/infraware/office/evengine/EV$SHAPE_SIZE;
    :cond_10
    const v1, 0x8000

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_11

    .line 163
    const v1, 0x8000

    or-int/2addr v2, v1

    .line 164
    const v1, 0x8000

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    .end local v18    # "pLocation":Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;
    check-cast v18, Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;

    .line 167
    .restart local v18    # "pLocation":Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;
    :cond_11
    const/high16 v1, 0x10000

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_12

    .line 169
    const/high16 v1, 0x10000

    or-int/2addr v2, v1

    .line 170
    const/high16 v1, 0x10000

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    .end local v19    # "pTextbox":Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;
    check-cast v19, Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;

    .line 173
    .restart local v19    # "pTextbox":Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;
    :cond_12
    if-eqz v2, :cond_0

    move-object/from16 v1, p0

    .line 176
    invoke-virtual/range {v1 .. v19}, Lcom/infraware/office/evengine/EvInterface;->ISetShapeProperty(ILcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;Lcom/infraware/office/evengine/EV$SHAPE_FILL;Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;Lcom/infraware/office/evengine/EV$SHAPE_GLOW;Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;Lcom/infraware/office/evengine/EV$SHAPE_SIZE;Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;)V

    goto/16 :goto_0
.end method
