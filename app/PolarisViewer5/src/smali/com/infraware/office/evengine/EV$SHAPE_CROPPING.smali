.class public Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SHAPE_CROPPING"
.end annotation


# instance fields
.field public bCroppingApply:I

.field public bCroppingMode:I

.field public nCropPositionHeight:F

.field public nCropPositionLeft:F

.field public nCropPositionTop:F

.field public nCropPositionWidth:F

.field public nCropSelector:I

.field public nPicturePositionHeight:F

.field public nPicturePositionOffsetX:F

.field public nPicturePositionOffsetY:F

.field public nPicturePositionWidth:F

.field public nShape:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 2649
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 2671
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;->nShape:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;->bCroppingApply:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;->bCroppingMode:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;->nCropSelector:I

    int-to-float v0, v0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;->nCropPositionTop:F

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;->nCropPositionLeft:F

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;->nCropPositionHeight:F

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;->nCropPositionWidth:F

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;->nPicturePositionOffsetY:F

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;->nPicturePositionOffsetX:F

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;->nPicturePositionHeight:F

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;->nPicturePositionWidth:F

    .line 2674
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2678
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;

    .line 2679
    .local v0, "o":Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2649
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;->clone()Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;

    move-result-object v0

    return-object v0
.end method
