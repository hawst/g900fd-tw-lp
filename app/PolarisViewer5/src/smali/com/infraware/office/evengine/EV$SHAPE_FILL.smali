.class public Lcom/infraware/office/evengine/EV$SHAPE_FILL;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SHAPE_FILL"
.end annotation


# instance fields
.field public bBright:Z

.field public bPictureAsTile:I

.field public nFillSelector:I

.field public nGradientAngle:I

.field public nGradientDirection:I

.field public nGradientEndColor:J

.field public nGradientPresetColors:J

.field public nGradientStartColor:J

.field public nGradientType:I

.field public nPatternBackColor:J

.field public nPatternForeColor:J

.field public nPatternType:I

.field public nPictureRotateWithShape:I

.field public nPictureTransparency:I

.field public nSolidColor:J

.field public nSolidTransparency:I

.field stPictureStretch:Lcom/infraware/office/evengine/EV$SHAPE_STRETCH;

.field public stPictureTextureDetail:Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;

.field stPictureTiling:Lcom/infraware/office/evengine/EV$SHAPE_TILING;

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2330
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2349
    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->stPictureTextureDetail:Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;

    .line 2353
    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->stPictureStretch:Lcom/infraware/office/evengine/EV$SHAPE_STRETCH;

    .line 2354
    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->stPictureTiling:Lcom/infraware/office/evengine/EV$SHAPE_TILING;

    return-void
.end method


# virtual methods
.method clear()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2365
    iput v2, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->nGradientType:I

    iput v2, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->nGradientDirection:I

    iput v2, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->nSolidTransparency:I

    iput v2, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->nFillSelector:I

    .line 2366
    iput v2, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->nGradientAngle:I

    int-to-long v0, v2

    iput-wide v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->nGradientPresetColors:J

    .line 2367
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->bBright:Z

    .line 2369
    iput v2, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->nPatternType:I

    iput v2, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->bPictureAsTile:I

    iput v2, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->nPictureRotateWithShape:I

    iput v2, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->nPictureTransparency:I

    .line 2370
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->nPatternBackColor:J

    iput-wide v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->nPatternForeColor:J

    iput-wide v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->nSolidColor:J

    .line 2373
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->stPictureTextureDetail:Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;

    if-eqz v0, :cond_0

    .line 2374
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->stPictureTextureDetail:Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;->clear()V

    .line 2375
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->stPictureStretch:Lcom/infraware/office/evengine/EV$SHAPE_STRETCH;

    if-eqz v0, :cond_1

    .line 2376
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->stPictureStretch:Lcom/infraware/office/evengine/EV$SHAPE_STRETCH;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SHAPE_STRETCH;->clear()V

    .line 2377
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->stPictureTiling:Lcom/infraware/office/evengine/EV$SHAPE_TILING;

    if-eqz v0, :cond_2

    .line 2378
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->stPictureTiling:Lcom/infraware/office/evengine/EV$SHAPE_TILING;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SHAPE_TILING;->clear()V

    .line 2379
    :cond_2
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SHAPE_FILL;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2383
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;

    .line 2384
    .local v0, "o":Lcom/infraware/office/evengine/EV$SHAPE_FILL;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2330
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->clone()Lcom/infraware/office/evengine/EV$SHAPE_FILL;

    move-result-object v0

    return-object v0
.end method
