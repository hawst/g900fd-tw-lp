.class public Lcom/infraware/office/evengine/EV;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;,
        Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;,
        Lcom/infraware/office/evengine/EV$SHAPE_RECOLOR_PRESET_DETAIL;,
        Lcom/infraware/office/evengine/EV$SHAPE_TILING;,
        Lcom/infraware/office/evengine/EV$SHAPE_STRETCH;,
        Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;,
        Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;,
        Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;,
        Lcom/infraware/office/evengine/EV$SHAPE_SIZE;,
        Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;,
        Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;,
        Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;,
        Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;,
        Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;,
        Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;,
        Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;,
        Lcom/infraware/office/evengine/EV$SHAPE_GLOW;,
        Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;,
        Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;,
        Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;,
        Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;,
        Lcom/infraware/office/evengine/EV$SHAPE_FILL;,
        Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;,
        Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;,
        Lcom/infraware/office/evengine/EV$SHAPE_GRADIENT_STOP;,
        Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;,
        Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;,
        Lcom/infraware/office/evengine/EV$TOUCH_INFO;,
        Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;,
        Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;,
        Lcom/infraware/office/evengine/EV$DUALVIEW_POS;,
        Lcom/infraware/office/evengine/EV$BWP_CHART;,
        Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;,
        Lcom/infraware/office/evengine/EV$SECTION_INFO;,
        Lcom/infraware/office/evengine/EV$PAPER_INFO;,
        Lcom/infraware/office/evengine/EV$RULERBAR_PAGE_INFO;,
        Lcom/infraware/office/evengine/EV$HeaderFooterOption;,
        Lcom/infraware/office/evengine/EV$HF_INFO;,
        Lcom/infraware/office/evengine/EV$TABLE_GUIDES;,
        Lcom/infraware/office/evengine/EV$GUIDES_INFO;,
        Lcom/infraware/office/evengine/EV$DRAW_CELLLINE;,
        Lcom/infraware/office/evengine/EV$CELL_PROPERTY;,
        Lcom/infraware/office/evengine/EV$BULLET_TYPE;,
        Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;,
        Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_STYLEINFO_EVENT;,
        Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;,
        Lcom/infraware/office/evengine/EV$CHART_FONTDATA;,
        Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_FONTINFO_EVENT;,
        Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;,
        Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;,
        Lcom/infraware/office/evengine/EV$GUI_SHEET_ALL_CHART_EVENT;,
        Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;,
        Lcom/infraware/office/evengine/EV$SUMMARY_DATA;,
        Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;,
        Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;,
        Lcom/infraware/office/evengine/EV$SHEET_INFO;,
        Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;,
        Lcom/infraware/office/evengine/EV$SHEET_FORMULA_ERR_INFO;,
        Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;,
        Lcom/infraware/office/evengine/EV$RANGE;,
        Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;,
        Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;,
        Lcom/infraware/office/evengine/EV$INVALID_DRAW_INFO;,
        Lcom/infraware/office/evengine/EV$BWP_SPLITCELL_MAXNUM;,
        Lcom/infraware/office/evengine/EV$BWP_OP_INFO;,
        Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;,
        Lcom/infraware/office/evengine/EV$BWP_FORMAT_ATTR_INFO;,
        Lcom/infraware/office/evengine/EV$FONT_INFO;,
        Lcom/infraware/office/evengine/EV$CARET_POS;,
        Lcom/infraware/office/evengine/EV$CARET_INFO;,
        Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;,
        Lcom/infraware/office/evengine/EV$SCREEN_INFO;,
        Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;,
        Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;,
        Lcom/infraware/office/evengine/EV$TABLE_STYLE_INFO;,
        Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;,
        Lcom/infraware/office/evengine/EV$BOOK_CLIP;,
        Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;,
        Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;,
        Lcom/infraware/office/evengine/EV$PROPERTIES;,
        Lcom/infraware/office/evengine/EV$CONFIG_INFO;
    }
.end annotation


# instance fields
.field ChartFontData:Lcom/infraware/office/evengine/EV$CHART_FONTDATA;

.field PdfAnnotationListItem:Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;

.field PdfBookmarkListItem:Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;

.field RulerbarPgInfo:Lcom/infraware/office/evengine/EV$RULERBAR_PAGE_INFO;

.field bookClip:Lcom/infraware/office/evengine/EV$BOOK_CLIP;

.field bookmarkLabel:Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;

.field bulletType:Lcom/infraware/office/evengine/EV$BULLET_TYPE;

.field bwpChart:Lcom/infraware/office/evengine/EV$BWP_CHART;

.field bwpGrapAttrInfo:Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;

.field bwpOpInfo:Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

.field bwpSplitCellMaxNum:Lcom/infraware/office/evengine/EV$BWP_SPLITCELL_MAXNUM;

.field caretInfoEvent:Lcom/infraware/office/evengine/EV$CARET_INFO;

.field caretPos:Lcom/infraware/office/evengine/EV$CARET_POS;

.field cellProperty:Lcom/infraware/office/evengine/EV$CELL_PROPERTY;

.field configInfo:Lcom/infraware/office/evengine/EV$CONFIG_INFO;

.field drawCellLine:Lcom/infraware/office/evengine/EV$DRAW_CELLLINE;

.field dualViewPos:Lcom/infraware/office/evengine/EV$DUALVIEW_POS;

.field protected editorObjectPointArray:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;

.field frameDetectionArea:Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;

.field guiBorderEvent:Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;

.field guiFontEvent:Lcom/infraware/office/evengine/EV$FONT_INFO;

.field guiSetParaAttEvent:Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;

.field guiSheetAllChartEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_ALL_CHART_EVENT;

.field guiSheetChartAxesInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;

.field guiSheetChartDataLabelInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;

.field guiSheetChartEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;

.field guiSheetChartFontInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_FONTINFO_EVENT;

.field guiSheetChartStyleInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_STYLEINFO_EVENT;

.field guiSheetChartTitleInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;

.field guidesInfo:Lcom/infraware/office/evengine/EV$GUIDES_INFO;

.field headerFooterOption:Lcom/infraware/office/evengine/EV$HeaderFooterOption;

.field hfInfo:Lcom/infraware/office/evengine/EV$HF_INFO;

.field hyperInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

.field invalidDrawInfo:Lcom/infraware/office/evengine/EV$INVALID_DRAW_INFO;

.field memoCmdData:Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;

.field nPageDisplayCount:I

.field pageDisplayInfo:[Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;

.field paperInfo:Lcom/infraware/office/evengine/EV$PAPER_INFO;

.field properties:Lcom/infraware/office/evengine/EV$PROPERTIES;

.field screenInfo:Lcom/infraware/office/evengine/EV$SCREEN_INFO;

.field scrollInfoEditor:Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;

.field sectionInfo:Lcom/infraware/office/evengine/EV$SECTION_INFO;

.field shape3DFormatInfo:Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;

.field shape3DRotationInfo:Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;

.field shapeArtisticEffectInfo:Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;

.field shapeCroppingInfo:Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;

.field shapeEffect:Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;

.field shapeFillInfo:Lcom/infraware/office/evengine/EV$SHAPE_FILL;

.field shapeGlowInfo:Lcom/infraware/office/evengine/EV$SHAPE_GLOW;

.field shapeGradientDrawInfo:Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;

.field shapeLineColorInfo:Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;

.field shapeLineStyleInfo:Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;

.field shapeLocationInfo:Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;

.field shapePictureColorInfo:Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;

.field shapePictureCorrectionInfo:Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;

.field shapeQuickStyleInfo:Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;

.field shapeReflectionInfo:Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;

.field shapeShadowInfo:Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;

.field shapeSizeInfo:Lcom/infraware/office/evengine/EV$SHAPE_SIZE;

.field shapeSoftEdgeInfo:Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;

.field shapeTextboxInfo:Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;

.field sheetAutoFilterContext:Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;

.field sheetCFInfo:Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;

.field sheetCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

.field sheetFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

.field sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

.field sheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

.field sheetRange:Lcom/infraware/office/evengine/EV$RANGE;

.field slideAnimationInfo:Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;

.field slideTransitionInfo:Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;

.field summaryInfo:Lcom/infraware/office/evengine/EV$SUMMARY_DATA;

.field tableGuides:Lcom/infraware/office/evengine/EV$TABLE_GUIDES;

.field tableStyleInfo:Lcom/infraware/office/evengine/EV$TABLE_STYLE_INFO;

.field touchInfo:Lcom/infraware/office/evengine/EV$TOUCH_INFO;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    const/4 v0, 0x5

    iput v0, p0, Lcom/infraware/office/evengine/EV;->nPageDisplayCount:I

    .line 2940
    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 523
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->configInfo:Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->configInfo:Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->clear()V

    .line 524
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->properties:Lcom/infraware/office/evengine/EV$PROPERTIES;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->properties:Lcom/infraware/office/evengine/EV$PROPERTIES;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$PROPERTIES;->clear()V

    .line 525
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bookmarkLabel:Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bookmarkLabel:Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;->clear()V

    .line 526
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bookClip:Lcom/infraware/office/evengine/EV$BOOK_CLIP;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bookClip:Lcom/infraware/office/evengine/EV$BOOK_CLIP;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$BOOK_CLIP;->clear()V

    .line 527
    :cond_3
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->screenInfo:Lcom/infraware/office/evengine/EV$SCREEN_INFO;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->screenInfo:Lcom/infraware/office/evengine/EV$SCREEN_INFO;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SCREEN_INFO;->clear()V

    .line 528
    :cond_4
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guiBorderEvent:Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guiBorderEvent:Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;->clear()V

    .line 529
    :cond_5
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->caretInfoEvent:Lcom/infraware/office/evengine/EV$CARET_INFO;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->caretInfoEvent:Lcom/infraware/office/evengine/EV$CARET_INFO;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$CARET_INFO;->clear()V

    .line 530
    :cond_6
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->caretPos:Lcom/infraware/office/evengine/EV$CARET_POS;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->caretPos:Lcom/infraware/office/evengine/EV$CARET_POS;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$CARET_POS;->clear()V

    .line 531
    :cond_7
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guiFontEvent:Lcom/infraware/office/evengine/EV$FONT_INFO;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guiFontEvent:Lcom/infraware/office/evengine/EV$FONT_INFO;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$FONT_INFO;->clear()V

    .line 532
    :cond_8
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bwpGrapAttrInfo:Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bwpGrapAttrInfo:Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->clear()V

    .line 533
    :cond_9
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bwpOpInfo:Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bwpOpInfo:Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->clear()V

    .line 534
    :cond_a
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bwpSplitCellMaxNum:Lcom/infraware/office/evengine/EV$BWP_SPLITCELL_MAXNUM;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bwpSplitCellMaxNum:Lcom/infraware/office/evengine/EV$BWP_SPLITCELL_MAXNUM;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$BWP_SPLITCELL_MAXNUM;->clear()V

    .line 535
    :cond_b
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->invalidDrawInfo:Lcom/infraware/office/evengine/EV$INVALID_DRAW_INFO;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->invalidDrawInfo:Lcom/infraware/office/evengine/EV$INVALID_DRAW_INFO;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$INVALID_DRAW_INFO;->clear()V

    .line 536
    :cond_c
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guiSetParaAttEvent:Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guiSetParaAttEvent:Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;->clear()V

    .line 537
    :cond_d
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;->clear()V

    .line 538
    :cond_e
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetRange:Lcom/infraware/office/evengine/EV$RANGE;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetRange:Lcom/infraware/office/evengine/EV$RANGE;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$RANGE;->clear()V

    .line 539
    :cond_f
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->clear()V

    .line 540
    :cond_10
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SHEET_INFO;->clear()V

    .line 541
    :cond_11
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->hyperInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->hyperInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->clear()V

    .line 542
    :cond_12
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guiSheetChartEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guiSheetChartEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->clear()V

    .line 543
    :cond_13
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->scrollInfoEditor:Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->scrollInfoEditor:Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;->clear()V

    .line 544
    :cond_14
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bulletType:Lcom/infraware/office/evengine/EV$BULLET_TYPE;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bulletType:Lcom/infraware/office/evengine/EV$BULLET_TYPE;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$BULLET_TYPE;->clear()V

    .line 545
    :cond_15
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->cellProperty:Lcom/infraware/office/evengine/EV$CELL_PROPERTY;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->cellProperty:Lcom/infraware/office/evengine/EV$CELL_PROPERTY;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$CELL_PROPERTY;->clear()V

    .line 546
    :cond_16
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guidesInfo:Lcom/infraware/office/evengine/EV$GUIDES_INFO;

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guidesInfo:Lcom/infraware/office/evengine/EV$GUIDES_INFO;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$GUIDES_INFO;->clear()V

    .line 547
    :cond_17
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->tableGuides:Lcom/infraware/office/evengine/EV$TABLE_GUIDES;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->tableGuides:Lcom/infraware/office/evengine/EV$TABLE_GUIDES;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$TABLE_GUIDES;->clear()V

    .line 548
    :cond_18
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->hfInfo:Lcom/infraware/office/evengine/EV$HF_INFO;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->hfInfo:Lcom/infraware/office/evengine/EV$HF_INFO;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$HF_INFO;->clear()V

    .line 549
    :cond_19
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->paperInfo:Lcom/infraware/office/evengine/EV$PAPER_INFO;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->paperInfo:Lcom/infraware/office/evengine/EV$PAPER_INFO;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$PAPER_INFO;->clear()V

    .line 550
    :cond_1a
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->sectionInfo:Lcom/infraware/office/evengine/EV$SECTION_INFO;

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->sectionInfo:Lcom/infraware/office/evengine/EV$SECTION_INFO;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SECTION_INFO;->clear()V

    .line 551
    :cond_1b
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bwpChart:Lcom/infraware/office/evengine/EV$BWP_CHART;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bwpChart:Lcom/infraware/office/evengine/EV$BWP_CHART;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$BWP_CHART;->clear()V

    .line 553
    :cond_1c
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeQuickStyleInfo:Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeQuickStyleInfo:Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;->clear()V

    .line 554
    :cond_1d
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeFillInfo:Lcom/infraware/office/evengine/EV$SHAPE_FILL;

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeFillInfo:Lcom/infraware/office/evengine/EV$SHAPE_FILL;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->clear()V

    .line 555
    :cond_1e
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeLineColorInfo:Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeLineColorInfo:Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;->clear()V

    .line 556
    :cond_1f
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeLineStyleInfo:Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;

    if-eqz v0, :cond_20

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeLineStyleInfo:Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;->clear()V

    .line 557
    :cond_20
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeShadowInfo:Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;

    if-eqz v0, :cond_21

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeShadowInfo:Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;->clear()V

    .line 558
    :cond_21
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeReflectionInfo:Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;

    if-eqz v0, :cond_22

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeReflectionInfo:Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;->clear()V

    .line 559
    :cond_22
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeGlowInfo:Lcom/infraware/office/evengine/EV$SHAPE_GLOW;

    if-eqz v0, :cond_23

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeGlowInfo:Lcom/infraware/office/evengine/EV$SHAPE_GLOW;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SHAPE_GLOW;->clear()V

    .line 560
    :cond_23
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeSoftEdgeInfo:Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;

    if-eqz v0, :cond_24

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeSoftEdgeInfo:Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;->clear()V

    .line 561
    :cond_24
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shape3DFormatInfo:Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;

    if-eqz v0, :cond_25

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shape3DFormatInfo:Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->clear()V

    .line 562
    :cond_25
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shape3DRotationInfo:Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shape3DRotationInfo:Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->clear()V

    .line 563
    :cond_26
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapePictureCorrectionInfo:Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;

    if-eqz v0, :cond_27

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapePictureCorrectionInfo:Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;->clear()V

    .line 564
    :cond_27
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapePictureColorInfo:Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;

    if-eqz v0, :cond_28

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapePictureColorInfo:Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;->clear()V

    .line 565
    :cond_28
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeArtisticEffectInfo:Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;

    if-eqz v0, :cond_29

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeArtisticEffectInfo:Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;->clear()V

    .line 566
    :cond_29
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeCroppingInfo:Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;

    if-eqz v0, :cond_2a

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeCroppingInfo:Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;->clear()V

    .line 567
    :cond_2a
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeSizeInfo:Lcom/infraware/office/evengine/EV$SHAPE_SIZE;

    if-eqz v0, :cond_2b

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeSizeInfo:Lcom/infraware/office/evengine/EV$SHAPE_SIZE;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;->clear()V

    .line 568
    :cond_2b
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeLocationInfo:Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;

    if-eqz v0, :cond_2c

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeLocationInfo:Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;->clear()V

    .line 569
    :cond_2c
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeTextboxInfo:Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;

    if-eqz v0, :cond_2d

    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeTextboxInfo:Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;->clear()V

    .line 570
    :cond_2d
    return-void
.end method

.method public clone()Lcom/infraware/office/evengine/EV;
    .locals 3

    .prologue
    .line 2863
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/office/evengine/EV;

    .line 2864
    .local v1, "o":Lcom/infraware/office/evengine/EV;
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->configInfo:Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->configInfo:Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->clone()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->configInfo:Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    .line 2865
    :cond_0
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->properties:Lcom/infraware/office/evengine/EV$PROPERTIES;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->properties:Lcom/infraware/office/evengine/EV$PROPERTIES;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$PROPERTIES;->clone()Lcom/infraware/office/evengine/EV$PROPERTIES;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->properties:Lcom/infraware/office/evengine/EV$PROPERTIES;

    .line 2866
    :cond_1
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->frameDetectionArea:Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->frameDetectionArea:Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->clone()Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->frameDetectionArea:Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;

    .line 2867
    :cond_2
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->bookmarkLabel:Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->bookmarkLabel:Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;->clone()Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->bookmarkLabel:Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;

    .line 2868
    :cond_3
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->bookClip:Lcom/infraware/office/evengine/EV$BOOK_CLIP;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->bookClip:Lcom/infraware/office/evengine/EV$BOOK_CLIP;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$BOOK_CLIP;->clone()Lcom/infraware/office/evengine/EV$BOOK_CLIP;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->bookClip:Lcom/infraware/office/evengine/EV$BOOK_CLIP;

    .line 2869
    :cond_4
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->screenInfo:Lcom/infraware/office/evengine/EV$SCREEN_INFO;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->screenInfo:Lcom/infraware/office/evengine/EV$SCREEN_INFO;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SCREEN_INFO;->clone()Lcom/infraware/office/evengine/EV$SCREEN_INFO;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->screenInfo:Lcom/infraware/office/evengine/EV$SCREEN_INFO;

    .line 2870
    :cond_5
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->guiBorderEvent:Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->guiBorderEvent:Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;->clone()Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->guiBorderEvent:Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;

    .line 2871
    :cond_6
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->caretInfoEvent:Lcom/infraware/office/evengine/EV$CARET_INFO;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->caretInfoEvent:Lcom/infraware/office/evengine/EV$CARET_INFO;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$CARET_INFO;->clone()Lcom/infraware/office/evengine/EV$CARET_INFO;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->caretInfoEvent:Lcom/infraware/office/evengine/EV$CARET_INFO;

    .line 2872
    :cond_7
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->caretPos:Lcom/infraware/office/evengine/EV$CARET_POS;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->caretPos:Lcom/infraware/office/evengine/EV$CARET_POS;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$CARET_POS;->clone()Lcom/infraware/office/evengine/EV$CARET_POS;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->caretPos:Lcom/infraware/office/evengine/EV$CARET_POS;

    .line 2873
    :cond_8
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->guiFontEvent:Lcom/infraware/office/evengine/EV$FONT_INFO;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->guiFontEvent:Lcom/infraware/office/evengine/EV$FONT_INFO;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$FONT_INFO;->clone()Lcom/infraware/office/evengine/EV$FONT_INFO;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->guiFontEvent:Lcom/infraware/office/evengine/EV$FONT_INFO;

    .line 2874
    :cond_9
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->bwpGrapAttrInfo:Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->bwpGrapAttrInfo:Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->clone()Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->bwpGrapAttrInfo:Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;

    .line 2875
    :cond_a
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->bwpOpInfo:Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->bwpOpInfo:Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->clone()Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->bwpOpInfo:Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    .line 2876
    :cond_b
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->bwpSplitCellMaxNum:Lcom/infraware/office/evengine/EV$BWP_SPLITCELL_MAXNUM;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->bwpSplitCellMaxNum:Lcom/infraware/office/evengine/EV$BWP_SPLITCELL_MAXNUM;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$BWP_SPLITCELL_MAXNUM;->clone()Lcom/infraware/office/evengine/EV$BWP_SPLITCELL_MAXNUM;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->bwpSplitCellMaxNum:Lcom/infraware/office/evengine/EV$BWP_SPLITCELL_MAXNUM;

    .line 2877
    :cond_c
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->invalidDrawInfo:Lcom/infraware/office/evengine/EV$INVALID_DRAW_INFO;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->invalidDrawInfo:Lcom/infraware/office/evengine/EV$INVALID_DRAW_INFO;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$INVALID_DRAW_INFO;->clone()Lcom/infraware/office/evengine/EV$INVALID_DRAW_INFO;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->invalidDrawInfo:Lcom/infraware/office/evengine/EV$INVALID_DRAW_INFO;

    .line 2878
    :cond_d
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->guiSetParaAttEvent:Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->guiSetParaAttEvent:Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;->clone()Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->guiSetParaAttEvent:Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;

    .line 2879
    :cond_e
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->sheetFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->sheetFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;->clone()Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->sheetFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    .line 2880
    :cond_f
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->sheetRange:Lcom/infraware/office/evengine/EV$RANGE;

    if-eqz v2, :cond_10

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->sheetRange:Lcom/infraware/office/evengine/EV$RANGE;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$RANGE;->clone()Lcom/infraware/office/evengine/EV$RANGE;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->sheetRange:Lcom/infraware/office/evengine/EV$RANGE;

    .line 2881
    :cond_10
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->sheetAutoFilterContext:Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->sheetAutoFilterContext:Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;->clone()Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->sheetAutoFilterContext:Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;

    .line 2882
    :cond_11
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->sheetCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    if-eqz v2, :cond_12

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->sheetCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->clone()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->sheetCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    .line 2883
    :cond_12
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->sheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

    if-eqz v2, :cond_13

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->sheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SHEET_INFO;->clone()Lcom/infraware/office/evengine/EV$SHEET_INFO;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->sheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

    .line 2884
    :cond_13
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->hyperInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->hyperInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->clone()Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->hyperInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    .line 2885
    :cond_14
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->tableStyleInfo:Lcom/infraware/office/evengine/EV$TABLE_STYLE_INFO;

    if-eqz v2, :cond_15

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->tableStyleInfo:Lcom/infraware/office/evengine/EV$TABLE_STYLE_INFO;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$TABLE_STYLE_INFO;->clone()Lcom/infraware/office/evengine/EV$TABLE_STYLE_INFO;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->tableStyleInfo:Lcom/infraware/office/evengine/EV$TABLE_STYLE_INFO;

    .line 2886
    :cond_15
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->slideTransitionInfo:Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;

    if-eqz v2, :cond_16

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->slideTransitionInfo:Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;->clone()Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->slideTransitionInfo:Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;

    .line 2887
    :cond_16
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->slideAnimationInfo:Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;

    if-eqz v2, :cond_17

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->slideAnimationInfo:Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;->clone()Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->slideAnimationInfo:Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;

    .line 2888
    :cond_17
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->guiSheetChartEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;

    if-eqz v2, :cond_18

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->guiSheetChartEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->clone()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->guiSheetChartEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;

    .line 2889
    :cond_18
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->summaryInfo:Lcom/infraware/office/evengine/EV$SUMMARY_DATA;

    if-eqz v2, :cond_19

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->summaryInfo:Lcom/infraware/office/evengine/EV$SUMMARY_DATA;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SUMMARY_DATA;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/office/evengine/EV$SUMMARY_DATA;

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->summaryInfo:Lcom/infraware/office/evengine/EV$SUMMARY_DATA;

    .line 2890
    :cond_19
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->guiSheetChartTitleInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;

    if-eqz v2, :cond_1a

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->guiSheetChartTitleInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;->clone()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->guiSheetChartTitleInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;

    .line 2891
    :cond_1a
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->guiSheetChartAxesInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;

    if-eqz v2, :cond_1b

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->guiSheetChartAxesInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->clone()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->guiSheetChartAxesInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;

    .line 2892
    :cond_1b
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->ChartFontData:Lcom/infraware/office/evengine/EV$CHART_FONTDATA;

    if-eqz v2, :cond_1c

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->ChartFontData:Lcom/infraware/office/evengine/EV$CHART_FONTDATA;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$CHART_FONTDATA;->clone()Lcom/infraware/office/evengine/EV$CHART_FONTDATA;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->ChartFontData:Lcom/infraware/office/evengine/EV$CHART_FONTDATA;

    .line 2893
    :cond_1c
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->guiSheetChartFontInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_FONTINFO_EVENT;

    if-eqz v2, :cond_1d

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->guiSheetChartFontInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_FONTINFO_EVENT;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_FONTINFO_EVENT;->clone()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_FONTINFO_EVENT;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->guiSheetChartFontInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_FONTINFO_EVENT;

    .line 2894
    :cond_1d
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->guiSheetChartDataLabelInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;

    if-eqz v2, :cond_1e

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->guiSheetChartDataLabelInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;->clone()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->guiSheetChartDataLabelInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;

    .line 2895
    :cond_1e
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->scrollInfoEditor:Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;

    if-eqz v2, :cond_1f

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->scrollInfoEditor:Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;->clone()Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->scrollInfoEditor:Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;

    .line 2896
    :cond_1f
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->bulletType:Lcom/infraware/office/evengine/EV$BULLET_TYPE;

    if-eqz v2, :cond_20

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->bulletType:Lcom/infraware/office/evengine/EV$BULLET_TYPE;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$BULLET_TYPE;->clone()Lcom/infraware/office/evengine/EV$BULLET_TYPE;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->bulletType:Lcom/infraware/office/evengine/EV$BULLET_TYPE;

    .line 2897
    :cond_20
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->cellProperty:Lcom/infraware/office/evengine/EV$CELL_PROPERTY;

    if-eqz v2, :cond_21

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->cellProperty:Lcom/infraware/office/evengine/EV$CELL_PROPERTY;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$CELL_PROPERTY;->clone()Lcom/infraware/office/evengine/EV$CELL_PROPERTY;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->cellProperty:Lcom/infraware/office/evengine/EV$CELL_PROPERTY;

    .line 2898
    :cond_21
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->drawCellLine:Lcom/infraware/office/evengine/EV$DRAW_CELLLINE;

    if-eqz v2, :cond_22

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->drawCellLine:Lcom/infraware/office/evengine/EV$DRAW_CELLLINE;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$DRAW_CELLLINE;->clone()Lcom/infraware/office/evengine/EV$DRAW_CELLLINE;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->drawCellLine:Lcom/infraware/office/evengine/EV$DRAW_CELLLINE;

    .line 2899
    :cond_22
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->guidesInfo:Lcom/infraware/office/evengine/EV$GUIDES_INFO;

    if-eqz v2, :cond_23

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->guidesInfo:Lcom/infraware/office/evengine/EV$GUIDES_INFO;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$GUIDES_INFO;->clone()Lcom/infraware/office/evengine/EV$GUIDES_INFO;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->guidesInfo:Lcom/infraware/office/evengine/EV$GUIDES_INFO;

    .line 2900
    :cond_23
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->tableGuides:Lcom/infraware/office/evengine/EV$TABLE_GUIDES;

    if-eqz v2, :cond_24

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->tableGuides:Lcom/infraware/office/evengine/EV$TABLE_GUIDES;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$TABLE_GUIDES;->clone()Lcom/infraware/office/evengine/EV$TABLE_GUIDES;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->tableGuides:Lcom/infraware/office/evengine/EV$TABLE_GUIDES;

    .line 2901
    :cond_24
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->hfInfo:Lcom/infraware/office/evengine/EV$HF_INFO;

    if-eqz v2, :cond_25

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->hfInfo:Lcom/infraware/office/evengine/EV$HF_INFO;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$HF_INFO;->clone()Lcom/infraware/office/evengine/EV$HF_INFO;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->hfInfo:Lcom/infraware/office/evengine/EV$HF_INFO;

    .line 2902
    :cond_25
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->headerFooterOption:Lcom/infraware/office/evengine/EV$HeaderFooterOption;

    if-eqz v2, :cond_26

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->headerFooterOption:Lcom/infraware/office/evengine/EV$HeaderFooterOption;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$HeaderFooterOption;->clone()Lcom/infraware/office/evengine/EV$HeaderFooterOption;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->headerFooterOption:Lcom/infraware/office/evengine/EV$HeaderFooterOption;

    .line 2903
    :cond_26
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->paperInfo:Lcom/infraware/office/evengine/EV$PAPER_INFO;

    if-eqz v2, :cond_27

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->paperInfo:Lcom/infraware/office/evengine/EV$PAPER_INFO;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$PAPER_INFO;->clone()Lcom/infraware/office/evengine/EV$PAPER_INFO;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->paperInfo:Lcom/infraware/office/evengine/EV$PAPER_INFO;

    .line 2904
    :cond_27
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->sectionInfo:Lcom/infraware/office/evengine/EV$SECTION_INFO;

    if-eqz v2, :cond_28

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->sectionInfo:Lcom/infraware/office/evengine/EV$SECTION_INFO;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SECTION_INFO;->clone()Lcom/infraware/office/evengine/EV$SECTION_INFO;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->sectionInfo:Lcom/infraware/office/evengine/EV$SECTION_INFO;

    .line 2905
    :cond_28
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->pageDisplayInfo:[Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;

    if-eqz v2, :cond_29

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->pageDisplayInfo:[Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;

    invoke-virtual {v2}, [Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;

    check-cast v2, [Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->pageDisplayInfo:[Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;

    .line 2906
    :cond_29
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->bwpChart:Lcom/infraware/office/evengine/EV$BWP_CHART;

    if-eqz v2, :cond_2a

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->bwpChart:Lcom/infraware/office/evengine/EV$BWP_CHART;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$BWP_CHART;->clone()Lcom/infraware/office/evengine/EV$BWP_CHART;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->bwpChart:Lcom/infraware/office/evengine/EV$BWP_CHART;

    .line 2907
    :cond_2a
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->dualViewPos:Lcom/infraware/office/evengine/EV$DUALVIEW_POS;

    if-eqz v2, :cond_2b

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->dualViewPos:Lcom/infraware/office/evengine/EV$DUALVIEW_POS;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$DUALVIEW_POS;->clone()Lcom/infraware/office/evengine/EV$DUALVIEW_POS;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->dualViewPos:Lcom/infraware/office/evengine/EV$DUALVIEW_POS;

    .line 2908
    :cond_2b
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->PdfBookmarkListItem:Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;

    if-eqz v2, :cond_2c

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->PdfBookmarkListItem:Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;->clone()Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->PdfBookmarkListItem:Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;

    .line 2909
    :cond_2c
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->touchInfo:Lcom/infraware/office/evengine/EV$TOUCH_INFO;

    if-eqz v2, :cond_2d

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->touchInfo:Lcom/infraware/office/evengine/EV$TOUCH_INFO;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$TOUCH_INFO;->clone()Lcom/infraware/office/evengine/EV$TOUCH_INFO;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->touchInfo:Lcom/infraware/office/evengine/EV$TOUCH_INFO;

    .line 2911
    :cond_2d
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeEffect:Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;

    if-eqz v2, :cond_2e

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->shapeEffect:Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;->clone()Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeEffect:Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;

    .line 2912
    :cond_2e
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeQuickStyleInfo:Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;

    if-eqz v2, :cond_2f

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->shapeQuickStyleInfo:Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;->clone()Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeQuickStyleInfo:Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;

    .line 2913
    :cond_2f
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeFillInfo:Lcom/infraware/office/evengine/EV$SHAPE_FILL;

    if-eqz v2, :cond_30

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->shapeFillInfo:Lcom/infraware/office/evengine/EV$SHAPE_FILL;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->clone()Lcom/infraware/office/evengine/EV$SHAPE_FILL;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeFillInfo:Lcom/infraware/office/evengine/EV$SHAPE_FILL;

    .line 2914
    :cond_30
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeLineColorInfo:Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;

    if-eqz v2, :cond_31

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->shapeLineColorInfo:Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;->clone()Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeLineColorInfo:Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;

    .line 2915
    :cond_31
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeLineStyleInfo:Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;

    if-eqz v2, :cond_32

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->shapeLineStyleInfo:Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;->clone()Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeLineStyleInfo:Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;

    .line 2916
    :cond_32
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeShadowInfo:Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;

    if-eqz v2, :cond_33

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->shapeShadowInfo:Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;->clone()Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeShadowInfo:Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;

    .line 2917
    :cond_33
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeReflectionInfo:Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;

    if-eqz v2, :cond_34

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->shapeReflectionInfo:Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;->clone()Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeReflectionInfo:Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;

    .line 2918
    :cond_34
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeGlowInfo:Lcom/infraware/office/evengine/EV$SHAPE_GLOW;

    if-eqz v2, :cond_35

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->shapeGlowInfo:Lcom/infraware/office/evengine/EV$SHAPE_GLOW;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SHAPE_GLOW;->clone()Lcom/infraware/office/evengine/EV$SHAPE_GLOW;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeGlowInfo:Lcom/infraware/office/evengine/EV$SHAPE_GLOW;

    .line 2919
    :cond_35
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeSoftEdgeInfo:Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;

    if-eqz v2, :cond_36

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->shapeSoftEdgeInfo:Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;->clone()Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeSoftEdgeInfo:Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;

    .line 2920
    :cond_36
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->shape3DFormatInfo:Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;

    if-eqz v2, :cond_37

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->shape3DFormatInfo:Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;->clone()Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->shape3DFormatInfo:Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;

    .line 2921
    :cond_37
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->shape3DRotationInfo:Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;

    if-eqz v2, :cond_38

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->shape3DRotationInfo:Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;->clone()Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->shape3DRotationInfo:Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;

    .line 2922
    :cond_38
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->shapePictureCorrectionInfo:Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;

    if-eqz v2, :cond_39

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->shapePictureCorrectionInfo:Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;->clone()Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->shapePictureCorrectionInfo:Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;

    .line 2923
    :cond_39
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->shapePictureColorInfo:Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;

    if-eqz v2, :cond_3a

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->shapePictureColorInfo:Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;->clone()Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->shapePictureColorInfo:Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;

    .line 2924
    :cond_3a
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeArtisticEffectInfo:Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;

    if-eqz v2, :cond_3b

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->shapeArtisticEffectInfo:Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;->clone()Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeArtisticEffectInfo:Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;

    .line 2925
    :cond_3b
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeCroppingInfo:Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;

    if-eqz v2, :cond_3c

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->shapeCroppingInfo:Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;->clone()Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeCroppingInfo:Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;

    .line 2926
    :cond_3c
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeSizeInfo:Lcom/infraware/office/evengine/EV$SHAPE_SIZE;

    if-eqz v2, :cond_3d

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->shapeSizeInfo:Lcom/infraware/office/evengine/EV$SHAPE_SIZE;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;->clone()Lcom/infraware/office/evengine/EV$SHAPE_SIZE;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeSizeInfo:Lcom/infraware/office/evengine/EV$SHAPE_SIZE;

    .line 2927
    :cond_3d
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeLocationInfo:Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;

    if-eqz v2, :cond_3e

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->shapeLocationInfo:Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;->clone()Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeLocationInfo:Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;

    .line 2928
    :cond_3e
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeTextboxInfo:Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;

    if-eqz v2, :cond_3f

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->shapeTextboxInfo:Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;->clone()Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->shapeTextboxInfo:Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;

    .line 2930
    :cond_3f
    iget-object v2, v1, Lcom/infraware/office/evengine/EV;->memoCmdData:Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;

    if-eqz v2, :cond_40

    iget-object v2, p0, Lcom/infraware/office/evengine/EV;->memoCmdData:Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->clone()Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/office/evengine/EV;->memoCmdData:Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2936
    .end local v1    # "o":Lcom/infraware/office/evengine/EV;
    :cond_40
    :goto_0
    return-object v1

    .line 2932
    :catch_0
    move-exception v0

    .line 2934
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v0}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    .line 2936
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV;->clone()Lcom/infraware/office/evengine/EV;

    move-result-object v0

    return-object v0
.end method

.method public getAnimationInfo()Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->slideAnimationInfo:Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->slideAnimationInfo:Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->slideAnimationInfo:Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;

    return-object v0
.end method

.method public getBookClip()Lcom/infraware/office/evengine/EV$BOOK_CLIP;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bookClip:Lcom/infraware/office/evengine/EV$BOOK_CLIP;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$BOOK_CLIP;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$BOOK_CLIP;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->bookClip:Lcom/infraware/office/evengine/EV$BOOK_CLIP;

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bookClip:Lcom/infraware/office/evengine/EV$BOOK_CLIP;

    return-object v0
.end method

.method public getBookmarkLabel()Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bookmarkLabel:Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->bookmarkLabel:Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bookmarkLabel:Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;

    return-object v0
.end method

.method public getBulletType()Lcom/infraware/office/evengine/EV$BULLET_TYPE;
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bulletType:Lcom/infraware/office/evengine/EV$BULLET_TYPE;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$BULLET_TYPE;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$BULLET_TYPE;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->bulletType:Lcom/infraware/office/evengine/EV$BULLET_TYPE;

    .line 321
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bulletType:Lcom/infraware/office/evengine/EV$BULLET_TYPE;

    return-object v0
.end method

.method public getBwpChart()Lcom/infraware/office/evengine/EV$BWP_CHART;
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bwpChart:Lcom/infraware/office/evengine/EV$BWP_CHART;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$BWP_CHART;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$BWP_CHART;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->bwpChart:Lcom/infraware/office/evengine/EV$BWP_CHART;

    .line 391
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bwpChart:Lcom/infraware/office/evengine/EV$BWP_CHART;

    return-object v0
.end method

.method public getBwpGrapAttrInfo()Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bwpGrapAttrInfo:Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->bwpGrapAttrInfo:Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bwpGrapAttrInfo:Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;

    return-object v0
.end method

.method public getBwpOpInfo()Lcom/infraware/office/evengine/EV$BWP_OP_INFO;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bwpOpInfo:Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->bwpOpInfo:Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    .line 212
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bwpOpInfo:Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    return-object v0
.end method

.method public getBwpSplitCellMaxNum()Lcom/infraware/office/evengine/EV$BWP_SPLITCELL_MAXNUM;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bwpSplitCellMaxNum:Lcom/infraware/office/evengine/EV$BWP_SPLITCELL_MAXNUM;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$BWP_SPLITCELL_MAXNUM;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$BWP_SPLITCELL_MAXNUM;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->bwpSplitCellMaxNum:Lcom/infraware/office/evengine/EV$BWP_SPLITCELL_MAXNUM;

    .line 217
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->bwpSplitCellMaxNum:Lcom/infraware/office/evengine/EV$BWP_SPLITCELL_MAXNUM;

    return-object v0
.end method

.method public getCaretInfoEvent()Lcom/infraware/office/evengine/EV$CARET_INFO;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->caretInfoEvent:Lcom/infraware/office/evengine/EV$CARET_INFO;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$CARET_INFO;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$CARET_INFO;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->caretInfoEvent:Lcom/infraware/office/evengine/EV$CARET_INFO;

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->caretInfoEvent:Lcom/infraware/office/evengine/EV$CARET_INFO;

    return-object v0
.end method

.method public getCaretPos()Lcom/infraware/office/evengine/EV$CARET_POS;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->caretPos:Lcom/infraware/office/evengine/EV$CARET_POS;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$CARET_POS;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$CARET_POS;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->caretPos:Lcom/infraware/office/evengine/EV$CARET_POS;

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->caretPos:Lcom/infraware/office/evengine/EV$CARET_POS;

    return-object v0
.end method

.method public getCellProperty()Lcom/infraware/office/evengine/EV$CELL_PROPERTY;
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->cellProperty:Lcom/infraware/office/evengine/EV$CELL_PROPERTY;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$CELL_PROPERTY;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$CELL_PROPERTY;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->cellProperty:Lcom/infraware/office/evengine/EV$CELL_PROPERTY;

    .line 328
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->cellProperty:Lcom/infraware/office/evengine/EV$CELL_PROPERTY;

    return-object v0
.end method

.method public getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->configInfo:Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$CONFIG_INFO;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->configInfo:Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->configInfo:Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    return-object v0
.end method

.method public getDrawCellLine()Lcom/infraware/office/evengine/EV$DRAW_CELLLINE;
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->drawCellLine:Lcom/infraware/office/evengine/EV$DRAW_CELLLINE;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$DRAW_CELLLINE;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$DRAW_CELLLINE;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->drawCellLine:Lcom/infraware/office/evengine/EV$DRAW_CELLLINE;

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->drawCellLine:Lcom/infraware/office/evengine/EV$DRAW_CELLLINE;

    return-object v0
.end method

.method public getDualViewPos()Lcom/infraware/office/evengine/EV$DUALVIEW_POS;
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->dualViewPos:Lcom/infraware/office/evengine/EV$DUALVIEW_POS;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$DUALVIEW_POS;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$DUALVIEW_POS;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->dualViewPos:Lcom/infraware/office/evengine/EV$DUALVIEW_POS;

    .line 397
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->dualViewPos:Lcom/infraware/office/evengine/EV$DUALVIEW_POS;

    return-object v0
.end method

.method public getEditorObjectPointArray()Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->editorObjectPointArray:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->editorObjectPointArray:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;

    .line 517
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->editorObjectPointArray:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;

    return-object v0
.end method

.method public getFontData()Lcom/infraware/office/evengine/EV$CHART_FONTDATA;
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->ChartFontData:Lcom/infraware/office/evengine/EV$CHART_FONTDATA;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$CHART_FONTDATA;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$CHART_FONTDATA;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->ChartFontData:Lcom/infraware/office/evengine/EV$CHART_FONTDATA;

    .line 295
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->ChartFontData:Lcom/infraware/office/evengine/EV$CHART_FONTDATA;

    return-object v0
.end method

.method public getFrameDetectionArea()Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->frameDetectionArea:Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->frameDetectionArea:Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->frameDetectionArea:Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;

    return-object v0
.end method

.method public getGradientDrawInfo()Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeGradientDrawInfo:Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeGradientDrawInfo:Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;

    .line 410
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeGradientDrawInfo:Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;

    return-object v0
.end method

.method public getGuiBorderEvent()Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guiBorderEvent:Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->guiBorderEvent:Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guiBorderEvent:Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;

    return-object v0
.end method

.method public getGuiFontEvent()Lcom/infraware/office/evengine/EV$FONT_INFO;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guiFontEvent:Lcom/infraware/office/evengine/EV$FONT_INFO;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$FONT_INFO;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$FONT_INFO;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->guiFontEvent:Lcom/infraware/office/evengine/EV$FONT_INFO;

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guiFontEvent:Lcom/infraware/office/evengine/EV$FONT_INFO;

    return-object v0
.end method

.method public getGuiSetParaAttEvent()Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guiSetParaAttEvent:Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->guiSetParaAttEvent:Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guiSetParaAttEvent:Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;

    return-object v0
.end method

.method public getGuiSheetAllChartEvent()Lcom/infraware/office/evengine/EV$GUI_SHEET_ALL_CHART_EVENT;
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guiSheetAllChartEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_ALL_CHART_EVENT;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$GUI_SHEET_ALL_CHART_EVENT;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$GUI_SHEET_ALL_CHART_EVENT;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->guiSheetAllChartEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_ALL_CHART_EVENT;

    .line 310
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guiSheetAllChartEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_ALL_CHART_EVENT;

    return-object v0
.end method

.method public getGuiSheetChartAxesInfoEvent()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guiSheetChartAxesInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->guiSheetChartAxesInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;

    .line 290
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guiSheetChartAxesInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;

    return-object v0
.end method

.method public getGuiSheetChartDataLabelInfoEvent()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guiSheetChartDataLabelInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->guiSheetChartDataLabelInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guiSheetChartDataLabelInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;

    return-object v0
.end method

.method public getGuiSheetChartEvent()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guiSheetChartEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->guiSheetChartEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;

    .line 267
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guiSheetChartEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;

    return-object v0
.end method

.method public getGuiSheetChartStyleInfoEvent()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_STYLEINFO_EVENT;
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guiSheetChartStyleInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_STYLEINFO_EVENT;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_STYLEINFO_EVENT;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_STYLEINFO_EVENT;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->guiSheetChartStyleInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_STYLEINFO_EVENT;

    .line 305
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guiSheetChartStyleInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_STYLEINFO_EVENT;

    return-object v0
.end method

.method public getGuiSheetChartTitleInfoEvent()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guiSheetChartTitleInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->guiSheetChartTitleInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;

    .line 285
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guiSheetChartTitleInfoEvent:Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;

    return-object v0
.end method

.method public getGuides()Lcom/infraware/office/evengine/EV$GUIDES_INFO;
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guidesInfo:Lcom/infraware/office/evengine/EV$GUIDES_INFO;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$GUIDES_INFO;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$GUIDES_INFO;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->guidesInfo:Lcom/infraware/office/evengine/EV$GUIDES_INFO;

    .line 342
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->guidesInfo:Lcom/infraware/office/evengine/EV$GUIDES_INFO;

    return-object v0
.end method

.method public getHFInfo()Lcom/infraware/office/evengine/EV$HF_INFO;
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->hfInfo:Lcom/infraware/office/evengine/EV$HF_INFO;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$HF_INFO;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$HF_INFO;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->hfInfo:Lcom/infraware/office/evengine/EV$HF_INFO;

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->hfInfo:Lcom/infraware/office/evengine/EV$HF_INFO;

    return-object v0
.end method

.method public getHeaderFooterOption()Lcom/infraware/office/evengine/EV$HeaderFooterOption;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->headerFooterOption:Lcom/infraware/office/evengine/EV$HeaderFooterOption;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$HeaderFooterOption;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$HeaderFooterOption;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->headerFooterOption:Lcom/infraware/office/evengine/EV$HeaderFooterOption;

    .line 279
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->headerFooterOption:Lcom/infraware/office/evengine/EV$HeaderFooterOption;

    return-object v0
.end method

.method public getHyperLinkInfo()Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->hyperInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->hyperInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->hyperInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    return-object v0
.end method

.method public getInvalidDrawInfo()Lcom/infraware/office/evengine/EV$INVALID_DRAW_INFO;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->invalidDrawInfo:Lcom/infraware/office/evengine/EV$INVALID_DRAW_INFO;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$INVALID_DRAW_INFO;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$INVALID_DRAW_INFO;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->invalidDrawInfo:Lcom/infraware/office/evengine/EV$INVALID_DRAW_INFO;

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->invalidDrawInfo:Lcom/infraware/office/evengine/EV$INVALID_DRAW_INFO;

    return-object v0
.end method

.method public getMemoCmdData()Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    .locals 1

    .prologue
    .line 506
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->memoCmdData:Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->memoCmdData:Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;

    .line 507
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->memoCmdData:Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;

    return-object v0
.end method

.method public getPageDisplayInfo()[Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;
    .locals 3

    .prologue
    .line 378
    iget-object v1, p0, Lcom/infraware/office/evengine/EV;->pageDisplayInfo:[Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;

    if-nez v1, :cond_0

    .line 379
    iget v1, p0, Lcom/infraware/office/evengine/EV;->nPageDisplayCount:I

    new-array v1, v1, [Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;

    iput-object v1, p0, Lcom/infraware/office/evengine/EV;->pageDisplayInfo:[Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;

    .line 381
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/infraware/office/evengine/EV;->pageDisplayInfo:[Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 382
    iget-object v1, p0, Lcom/infraware/office/evengine/EV;->pageDisplayInfo:[Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;

    new-instance v2, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;

    invoke-direct {v2, p0}, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;-><init>(Lcom/infraware/office/evengine/EV;)V

    aput-object v2, v1, v0

    .line 381
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 385
    .end local v0    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/infraware/office/evengine/EV;->pageDisplayInfo:[Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;

    return-object v1
.end method

.method public getPaperInfo()Lcom/infraware/office/evengine/EV$PAPER_INFO;
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->paperInfo:Lcom/infraware/office/evengine/EV$PAPER_INFO;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$PAPER_INFO;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$PAPER_INFO;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->paperInfo:Lcom/infraware/office/evengine/EV$PAPER_INFO;

    .line 367
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->paperInfo:Lcom/infraware/office/evengine/EV$PAPER_INFO;

    return-object v0
.end method

.method public getPdfAnnotationListItem()Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->PdfAnnotationListItem:Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;

    if-nez v0, :cond_0

    .line 153
    new-instance v0, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->PdfAnnotationListItem:Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->PdfAnnotationListItem:Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;

    return-object v0
.end method

.method public getPdfBookmarkListItem()Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->PdfBookmarkListItem:Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;

    if-nez v0, :cond_0

    .line 137
    new-instance v0, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->PdfBookmarkListItem:Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;

    .line 147
    :goto_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->PdfBookmarkListItem:Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;

    return-object v0

    .line 142
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->PdfBookmarkListItem:Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;->clone()Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->PdfBookmarkListItem:Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 143
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getProperties()Lcom/infraware/office/evengine/EV$PROPERTIES;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->properties:Lcom/infraware/office/evengine/EV$PROPERTIES;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$PROPERTIES;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$PROPERTIES;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->properties:Lcom/infraware/office/evengine/EV$PROPERTIES;

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->properties:Lcom/infraware/office/evengine/EV$PROPERTIES;

    return-object v0
.end method

.method public getRange()Lcom/infraware/office/evengine/EV$RANGE;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetRange:Lcom/infraware/office/evengine/EV$RANGE;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$RANGE;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$RANGE;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetRange:Lcom/infraware/office/evengine/EV$RANGE;

    .line 236
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetRange:Lcom/infraware/office/evengine/EV$RANGE;

    return-object v0
.end method

.method public getRulerbarPgInfo()Lcom/infraware/office/evengine/EV$RULERBAR_PAGE_INFO;
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->RulerbarPgInfo:Lcom/infraware/office/evengine/EV$RULERBAR_PAGE_INFO;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$RULERBAR_PAGE_INFO;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$RULERBAR_PAGE_INFO;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->RulerbarPgInfo:Lcom/infraware/office/evengine/EV$RULERBAR_PAGE_INFO;

    .line 361
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->RulerbarPgInfo:Lcom/infraware/office/evengine/EV$RULERBAR_PAGE_INFO;

    return-object v0
.end method

.method public getScreenInfo()Lcom/infraware/office/evengine/EV$SCREEN_INFO;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->screenInfo:Lcom/infraware/office/evengine/EV$SCREEN_INFO;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SCREEN_INFO;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SCREEN_INFO;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->screenInfo:Lcom/infraware/office/evengine/EV$SCREEN_INFO;

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->screenInfo:Lcom/infraware/office/evengine/EV$SCREEN_INFO;

    return-object v0
.end method

.method public getScrollInfoEditor()Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->scrollInfoEditor:Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->scrollInfoEditor:Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;

    .line 315
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->scrollInfoEditor:Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;

    return-object v0
.end method

.method public getSectionInfo()Lcom/infraware/office/evengine/EV$SECTION_INFO;
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->sectionInfo:Lcom/infraware/office/evengine/EV$SECTION_INFO;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SECTION_INFO;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SECTION_INFO;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->sectionInfo:Lcom/infraware/office/evengine/EV$SECTION_INFO;

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->sectionInfo:Lcom/infraware/office/evengine/EV$SECTION_INFO;

    return-object v0
.end method

.method public getShape3DFormatInfo()Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;
    .locals 1

    .prologue
    .line 461
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shape3DFormatInfo:Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->shape3DFormatInfo:Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;

    .line 462
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shape3DFormatInfo:Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;

    return-object v0
.end method

.method public getShape3DRotationInfo()Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shape3DRotationInfo:Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->shape3DRotationInfo:Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;

    .line 467
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shape3DRotationInfo:Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;

    return-object v0
.end method

.method public getShapeArtisticEffectInfo()Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeArtisticEffectInfo:Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeArtisticEffectInfo:Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;

    .line 482
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeArtisticEffectInfo:Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;

    return-object v0
.end method

.method public getShapeCroppingInfo()Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeCroppingInfo:Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeCroppingInfo:Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;

    .line 487
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeCroppingInfo:Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;

    return-object v0
.end method

.method public getShapeEffect()Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeEffect:Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeEffect:Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;

    .line 415
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeEffect:Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;

    return-object v0
.end method

.method public getShapeFillInfo()Lcom/infraware/office/evengine/EV$SHAPE_FILL;
    .locals 2

    .prologue
    .line 424
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeFillInfo:Lcom/infraware/office/evengine/EV$SHAPE_FILL;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SHAPE_FILL;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeFillInfo:Lcom/infraware/office/evengine/EV$SHAPE_FILL;

    .line 425
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeFillInfo:Lcom/infraware/office/evengine/EV$SHAPE_FILL;

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->stPictureTextureDetail:Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;

    if-nez v0, :cond_1

    .line 426
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeFillInfo:Lcom/infraware/office/evengine/EV$SHAPE_FILL;

    new-instance v1, Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;

    invoke-direct {v1, p0}, Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v1, v0, Lcom/infraware/office/evengine/EV$SHAPE_FILL;->stPictureTextureDetail:Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;

    .line 427
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeFillInfo:Lcom/infraware/office/evengine/EV$SHAPE_FILL;

    return-object v0
.end method

.method public getShapeGlowInfo()Lcom/infraware/office/evengine/EV$SHAPE_GLOW;
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeGlowInfo:Lcom/infraware/office/evengine/EV$SHAPE_GLOW;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SHAPE_GLOW;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SHAPE_GLOW;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeGlowInfo:Lcom/infraware/office/evengine/EV$SHAPE_GLOW;

    .line 452
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeGlowInfo:Lcom/infraware/office/evengine/EV$SHAPE_GLOW;

    return-object v0
.end method

.method public getShapeLineColorInfo()Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;
    .locals 1

    .prologue
    .line 431
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeLineColorInfo:Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeLineColorInfo:Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;

    .line 432
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeLineColorInfo:Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;

    return-object v0
.end method

.method public getShapeLineStyleInfo()Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;
    .locals 1

    .prologue
    .line 436
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeLineStyleInfo:Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeLineStyleInfo:Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;

    .line 437
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeLineStyleInfo:Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;

    return-object v0
.end method

.method public getShapeLocationInfo()Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;
    .locals 1

    .prologue
    .line 496
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeLocationInfo:Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeLocationInfo:Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;

    .line 497
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeLocationInfo:Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;

    return-object v0
.end method

.method public getShapePictureColorInfo()Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapePictureColorInfo:Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->shapePictureColorInfo:Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;

    .line 477
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapePictureColorInfo:Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;

    return-object v0
.end method

.method public getShapePictureCorrectionInfo()Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapePictureCorrectionInfo:Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->shapePictureCorrectionInfo:Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;

    .line 472
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapePictureCorrectionInfo:Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;

    return-object v0
.end method

.method public getShapeQuickStyleInfo()Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeQuickStyleInfo:Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeQuickStyleInfo:Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;

    .line 420
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeQuickStyleInfo:Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;

    return-object v0
.end method

.method public getShapeReflectionInfo()Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeReflectionInfo:Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeReflectionInfo:Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;

    .line 447
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeReflectionInfo:Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;

    return-object v0
.end method

.method public getShapeShadowInfo()Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeShadowInfo:Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeShadowInfo:Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;

    .line 442
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeShadowInfo:Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;

    return-object v0
.end method

.method public getShapeSizeInfo()Lcom/infraware/office/evengine/EV$SHAPE_SIZE;
    .locals 1

    .prologue
    .line 491
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeSizeInfo:Lcom/infraware/office/evengine/EV$SHAPE_SIZE;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeSizeInfo:Lcom/infraware/office/evengine/EV$SHAPE_SIZE;

    .line 492
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeSizeInfo:Lcom/infraware/office/evengine/EV$SHAPE_SIZE;

    return-object v0
.end method

.method public getShapeSoftEdgeInfo()Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeSoftEdgeInfo:Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeSoftEdgeInfo:Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;

    .line 457
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeSoftEdgeInfo:Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;

    return-object v0
.end method

.method public getShapeTextboxInfo()Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeTextboxInfo:Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeTextboxInfo:Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;

    .line 502
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->shapeTextboxInfo:Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;

    return-object v0
.end method

.method public getSheetAutoFilterContext()Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetAutoFilterContext:Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetAutoFilterContext:Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;

    .line 241
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetAutoFilterContext:Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;

    return-object v0
.end method

.method public getSheetCFInfo()Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetCFInfo:Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetCFInfo:Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetCFInfo:Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;

    return-object v0
.end method

.method public getSheetCellInfo()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    return-object v0
.end method

.method public getSheetFormatInfo()Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    return-object v0
.end method

.method public getSheetHyperLinkInfo()Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    return-object v0
.end method

.method public getSheetInfo()Lcom/infraware/office/evengine/EV$SHEET_INFO;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SHEET_INFO;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SHEET_INFO;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->sheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

    return-object v0
.end method

.method public getSummaryData()Lcom/infraware/office/evengine/EV$SUMMARY_DATA;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->summaryInfo:Lcom/infraware/office/evengine/EV$SUMMARY_DATA;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SUMMARY_DATA;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SUMMARY_DATA;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->summaryInfo:Lcom/infraware/office/evengine/EV$SUMMARY_DATA;

    .line 273
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->summaryInfo:Lcom/infraware/office/evengine/EV$SUMMARY_DATA;

    return-object v0
.end method

.method public getTableGuides()Lcom/infraware/office/evengine/EV$TABLE_GUIDES;
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->tableGuides:Lcom/infraware/office/evengine/EV$TABLE_GUIDES;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$TABLE_GUIDES;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$TABLE_GUIDES;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->tableGuides:Lcom/infraware/office/evengine/EV$TABLE_GUIDES;

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->tableGuides:Lcom/infraware/office/evengine/EV$TABLE_GUIDES;

    return-object v0
.end method

.method public getTableStyleInfo()Lcom/infraware/office/evengine/EV$TABLE_STYLE_INFO;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->tableStyleInfo:Lcom/infraware/office/evengine/EV$TABLE_STYLE_INFO;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$TABLE_STYLE_INFO;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$TABLE_STYLE_INFO;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->tableStyleInfo:Lcom/infraware/office/evengine/EV$TABLE_STYLE_INFO;

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->tableStyleInfo:Lcom/infraware/office/evengine/EV$TABLE_STYLE_INFO;

    return-object v0
.end method

.method public getTouchInfo()Lcom/infraware/office/evengine/EV$TOUCH_INFO;
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->touchInfo:Lcom/infraware/office/evengine/EV$TOUCH_INFO;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$TOUCH_INFO;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$TOUCH_INFO;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->touchInfo:Lcom/infraware/office/evengine/EV$TOUCH_INFO;

    .line 404
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->touchInfo:Lcom/infraware/office/evengine/EV$TOUCH_INFO;

    return-object v0
.end method

.method public getTransitionInfo()Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->slideTransitionInfo:Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;

    if-nez v0, :cond_0

    new-instance v0, Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV;->slideTransitionInfo:Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EV;->slideTransitionInfo:Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;

    return-object v0
.end method
