.class public Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SLIDE_ANIMATION_INFO"
.end annotation


# instance fields
.field public nAnimationType:I

.field public nDelay:I

.field public nDirectionType:I

.field public nDuration:I

.field public nFrameID:I

.field public nFrameType:I

.field public nIndex:I

.field public nMoveIndex:I

.field public nOrder:I

.field public nPageNum:I

.field public nPresetType:I

.field public nShapesType:I

.field public nSpokesType:I

.field public nTriggerType:I

.field public nVanishingPointType:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 816
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 834
    iput v0, p0, Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;->nVanishingPointType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;->nSpokesType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;->nShapesType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;->nDirectionType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;->nAnimationType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;->nMoveIndex:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;->nDuration:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;->nIndex:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;->nPageNum:I

    .line 835
    iput v0, p0, Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;->nFrameID:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;->nFrameType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;->nOrder:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;->nDelay:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;->nDuration:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;->nTriggerType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;->nPresetType:I

    .line 836
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 839
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;

    .line 840
    .local v0, "o":Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 816
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;->clone()Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;

    move-result-object v0

    return-object v0
.end method
