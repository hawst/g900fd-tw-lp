.class public Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SHAPE_PICTURE_COLOR"
.end annotation


# instance fields
.field public nColorScale:I

.field public nRecolorPreset:I

.field public nSaturation:F

.field stRecolorPresetDetail:Lcom/infraware/office/evengine/EV$SHAPE_RECOLOR_PRESET_DETAIL;

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 1

    .prologue
    .line 2613
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2618
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;->stRecolorPresetDetail:Lcom/infraware/office/evengine/EV$SHAPE_RECOLOR_PRESET_DETAIL;

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 2621
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;->nRecolorPreset:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;->nColorScale:I

    int-to-float v0, v0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;->nSaturation:F

    .line 2622
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;->stRecolorPresetDetail:Lcom/infraware/office/evengine/EV$SHAPE_RECOLOR_PRESET_DETAIL;

    if-eqz v0, :cond_0

    .line 2623
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;->stRecolorPresetDetail:Lcom/infraware/office/evengine/EV$SHAPE_RECOLOR_PRESET_DETAIL;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SHAPE_RECOLOR_PRESET_DETAIL;->clear()V

    .line 2624
    :cond_0
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2628
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;

    .line 2629
    .local v0, "o":Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2613
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;->clone()Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;

    move-result-object v0

    return-object v0
.end method
