.class public Lcom/infraware/office/evengine/EV$PROPERTIES$PAGEMAPPROPERTIES;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV$PROPERTIES;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PAGEMAPPROPERTIES"
.end annotation


# instance fields
.field public bBluringPagemap:I

.field public bExternalPagemap:I

.field public nPagemapHeight:I

.field public nPagemapWidth:I

.field final synthetic this$1:Lcom/infraware/office/evengine/EV$PROPERTIES;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV$PROPERTIES;)V
    .locals 0

    .prologue
    .line 662
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$PROPERTIES$PAGEMAPPROPERTIES;->this$1:Lcom/infraware/office/evengine/EV$PROPERTIES;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 669
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$PROPERTIES$PAGEMAPPROPERTIES;->nPagemapHeight:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$PROPERTIES$PAGEMAPPROPERTIES;->nPagemapWidth:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$PROPERTIES$PAGEMAPPROPERTIES;->bBluringPagemap:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$PROPERTIES$PAGEMAPPROPERTIES;->bExternalPagemap:I

    .line 670
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$PROPERTIES$PAGEMAPPROPERTIES;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 673
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$PROPERTIES$PAGEMAPPROPERTIES;

    .line 674
    .local v0, "o":Lcom/infraware/office/evengine/EV$PROPERTIES$PAGEMAPPROPERTIES;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 662
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$PROPERTIES$PAGEMAPPROPERTIES;->clone()Lcom/infraware/office/evengine/EV$PROPERTIES$PAGEMAPPROPERTIES;

    move-result-object v0

    return-object v0
.end method
