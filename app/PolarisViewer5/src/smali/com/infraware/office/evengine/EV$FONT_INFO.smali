.class public Lcom/infraware/office/evengine/EV$FONT_INFO;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FONT_INFO"
.end annotation


# instance fields
.field public nBColor:I

.field public nFColor:I

.field public nFSize:I

.field public nFontAtt:I

.field public nHeight:I

.field public nMaskFontAtt:I

.field public nType:I

.field public nUColor:I

.field public nUNum:I

.field public nWidth:I

.field public szFontName:Ljava/lang/String;

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 940
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$FONT_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 955
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$FONT_INFO;->szFontName:Ljava/lang/String;

    .line 956
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$FONT_INFO;->nHeight:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$FONT_INFO;->nWidth:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$FONT_INFO;->nUColor:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$FONT_INFO;->nBColor:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$FONT_INFO;->nFColor:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$FONT_INFO;->nMaskFontAtt:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$FONT_INFO;->nFontAtt:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$FONT_INFO;->nUNum:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$FONT_INFO;->nFSize:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$FONT_INFO;->nType:I

    .line 957
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$FONT_INFO;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 960
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$FONT_INFO;

    .line 961
    .local v0, "o":Lcom/infraware/office/evengine/EV$FONT_INFO;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 940
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$FONT_INFO;->clone()Lcom/infraware/office/evengine/EV$FONT_INFO;

    move-result-object v0

    return-object v0
.end method
