.class public Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SLIDE_TRANSITION_INFO"
.end annotation


# instance fields
.field public bAdvClick:I

.field public bAdvTime:I

.field public nAdvTime:I

.field public nDuration:I

.field public nEffectType:I

.field public nOptionType:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 799
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 808
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;->nAdvTime:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;->bAdvTime:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;->bAdvClick:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;->nDuration:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;->nOptionType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;->nEffectType:I

    .line 809
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 812
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;

    .line 813
    .local v0, "o":Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 799
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;->clone()Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;

    move-result-object v0

    return-object v0
.end method
