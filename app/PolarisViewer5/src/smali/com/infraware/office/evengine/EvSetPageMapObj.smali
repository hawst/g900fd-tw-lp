.class Lcom/infraware/office/evengine/EvSetPageMapObj;
.super Lcom/infraware/office/evengine/EvTaskObj;
.source "EvTaskObj.java"


# instance fields
.field private m_bDrawContents:I

.field private m_bGenerateDrawEvt:I

.field private m_ePos:I

.field private m_nColMargin:I

.field private m_nMode:I

.field private m_nRowMargin:I


# direct methods
.method constructor <init>(Lcom/infraware/office/evengine/EvNative;IIIIII)V
    .locals 0
    .param p1, "a_Native"    # Lcom/infraware/office/evengine/EvNative;
    .param p2, "a_nMode"    # I
    .param p3, "a_bDrawContents"    # I
    .param p4, "a_ePos"    # I
    .param p5, "a_bGenerateDrawEvt"    # I
    .param p6, "a_nRowMargin"    # I
    .param p7, "a_nColMargin"    # I

    .prologue
    .line 428
    invoke-direct {p0, p1}, Lcom/infraware/office/evengine/EvTaskObj;-><init>(Lcom/infraware/office/evengine/EvNative;)V

    .line 429
    iput p2, p0, Lcom/infraware/office/evengine/EvSetPageMapObj;->m_nMode:I

    .line 430
    iput p3, p0, Lcom/infraware/office/evengine/EvSetPageMapObj;->m_bDrawContents:I

    .line 431
    iput p4, p0, Lcom/infraware/office/evengine/EvSetPageMapObj;->m_ePos:I

    .line 432
    iput p5, p0, Lcom/infraware/office/evengine/EvSetPageMapObj;->m_bGenerateDrawEvt:I

    .line 433
    iput p6, p0, Lcom/infraware/office/evengine/EvSetPageMapObj;->m_nRowMargin:I

    .line 434
    iput p7, p0, Lcom/infraware/office/evengine/EvSetPageMapObj;->m_nColMargin:I

    .line 435
    return-void
.end method


# virtual methods
.method doTask()V
    .locals 7

    .prologue
    .line 439
    iget-object v0, p0, Lcom/infraware/office/evengine/EvSetPageMapObj;->Native:Lcom/infraware/office/evengine/EvNative;

    iget v1, p0, Lcom/infraware/office/evengine/EvSetPageMapObj;->m_nMode:I

    iget v2, p0, Lcom/infraware/office/evengine/EvSetPageMapObj;->m_bDrawContents:I

    iget v3, p0, Lcom/infraware/office/evengine/EvSetPageMapObj;->m_ePos:I

    iget v4, p0, Lcom/infraware/office/evengine/EvSetPageMapObj;->m_bGenerateDrawEvt:I

    iget v5, p0, Lcom/infraware/office/evengine/EvSetPageMapObj;->m_nRowMargin:I

    iget v6, p0, Lcom/infraware/office/evengine/EvSetPageMapObj;->m_nColMargin:I

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvNative;->ISetPageMap(IIIIII)V

    .line 440
    return-void
.end method
