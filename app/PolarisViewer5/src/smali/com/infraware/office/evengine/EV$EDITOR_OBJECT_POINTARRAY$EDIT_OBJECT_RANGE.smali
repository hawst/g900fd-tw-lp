.class public Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "EDIT_OBJECT_RANGE"
.end annotation


# instance fields
.field public b2007DocxVML:Z

.field public b3D:Z

.field public b3DBevel:Z

.field public bDML:Z

.field public bGroupEnabled:I

.field public bHyperlinkEnabled:I

.field public bPureImage:I

.field public bRotationEnabled:I

.field public bSupport3D:Z

.field public bSupport3DBevel:Z

.field public eController:I

.field public eEditing:I

.field public editingEndPoint:Landroid/graphics/Point;

.field public editingStartPoint:Landroid/graphics/Point;

.field public endMarkDirection:I

.field public endPoint:Landroid/graphics/Point;

.field public nEditingAngle:I

.field public nFrameID:I

.field public nMarkingRectCount:I

.field public nObjectEditInfo:I

.field public nObjectType:I

.field public nRotateAngle:I

.field public sObjectSize:Landroid/graphics/Point;

.field public startMarkDirection:I

.field public startPoint:Landroid/graphics/Point;

.field public startPointFromPage:Landroid/graphics/Point;

.field final synthetic this$1:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V
    .locals 1

    .prologue
    .line 2009
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->this$1:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2011
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startPoint:Landroid/graphics/Point;

    .line 2012
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->endPoint:Landroid/graphics/Point;

    .line 2017
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->sObjectSize:Landroid/graphics/Point;

    .line 2025
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingStartPoint:Landroid/graphics/Point;

    .line 2026
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingEndPoint:Landroid/graphics/Point;

    .line 2030
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startPointFromPage:Landroid/graphics/Point;

    return-void
.end method


# virtual methods
.method clear()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2050
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startPoint:Landroid/graphics/Point;

    iget-object v1, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startPoint:Landroid/graphics/Point;

    iget-object v2, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->endPoint:Landroid/graphics/Point;

    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->endPoint:Landroid/graphics/Point;

    iget-object v4, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->sObjectSize:Landroid/graphics/Point;

    iget-object v5, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->sObjectSize:Landroid/graphics/Point;

    iput v6, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nObjectEditInfo:I

    iput v6, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nObjectType:I

    iput v6, v5, Landroid/graphics/Point;->y:I

    iput v6, v4, Landroid/graphics/Point;->x:I

    iput v6, v3, Landroid/graphics/Point;->y:I

    iput v6, v2, Landroid/graphics/Point;->x:I

    iput v6, v1, Landroid/graphics/Point;->y:I

    iput v6, v0, Landroid/graphics/Point;->x:I

    .line 2051
    iput v6, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nEditingAngle:I

    iput v6, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nRotateAngle:I

    .line 2052
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingStartPoint:Landroid/graphics/Point;

    iget-object v1, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingStartPoint:Landroid/graphics/Point;

    iget-object v2, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingEndPoint:Landroid/graphics/Point;

    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingEndPoint:Landroid/graphics/Point;

    iput v6, v3, Landroid/graphics/Point;->y:I

    iput v6, v2, Landroid/graphics/Point;->x:I

    iput v6, v1, Landroid/graphics/Point;->y:I

    iput v6, v0, Landroid/graphics/Point;->x:I

    .line 2053
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startPointFromPage:Landroid/graphics/Point;

    iget-object v1, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startPointFromPage:Landroid/graphics/Point;

    iput v6, v1, Landroid/graphics/Point;->y:I

    iput v6, v0, Landroid/graphics/Point;->x:I

    .line 2055
    iput v6, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nFrameID:I

    iput v6, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->eEditing:I

    .line 2056
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->eController:I

    .line 2057
    iput v6, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->bRotationEnabled:I

    .line 2058
    iput v6, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->bGroupEnabled:I

    .line 2059
    iput v6, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->bPureImage:I

    .line 2061
    iput-boolean v6, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->b3DBevel:Z

    iput-boolean v6, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->b3D:Z

    iput-boolean v6, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->bSupport3DBevel:Z

    iput-boolean v6, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->bSupport3D:Z

    iput-boolean v6, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->b2007DocxVML:Z

    iput-boolean v6, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->bDML:Z

    .line 2063
    iput v6, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nMarkingRectCount:I

    .line 2064
    iput v6, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->bHyperlinkEnabled:I

    .line 2066
    iput v6, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startMarkDirection:I

    .line 2067
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->endMarkDirection:I

    .line 2069
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2072
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    .line 2073
    .local v0, "o":Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2009
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->clone()Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-result-object v0

    return-object v0
.end method
