.class public Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GUI_BORDER_EVENT"
.end annotation


# instance fields
.field public bBorderHorizantalEnabled:Z

.field public bBorderVerticalEnabled:Z

.field public nBorderBottomColor:I

.field public nBorderHorizontalColor:I

.field public nBorderLeftColor:I

.field public nBorderMask:I

.field public nBorderRightColor:I

.field public nBorderStyle:I

.field public nBorderTopColor:I

.field public nBorderVerticalColor:I

.field public nBorderWidth:I

.field public nCellColor:I

.field public nStyleID:I

.field public nType:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 867
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 885
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;->nCellColor:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;->nBorderHorizontalColor:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;->nBorderVerticalColor:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;->nBorderBottomColor:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;->nBorderRightColor:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;->nBorderTopColor:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;->nBorderLeftColor:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;->nBorderWidth:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;->nBorderStyle:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;->nBorderMask:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;->nStyleID:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;->nType:I

    .line 888
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 891
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;

    .line 892
    .local v0, "o":Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 867
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;->clone()Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;

    move-result-object v0

    return-object v0
.end method
