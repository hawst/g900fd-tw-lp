.class Lcom/infraware/office/evengine/EvPivotScreenObj;
.super Lcom/infraware/office/evengine/EvTaskObj;
.source "EvTaskObj.java"


# instance fields
.field private m_bLandScape:I

.field private m_nHeight:I

.field private m_nWidth:I


# direct methods
.method constructor <init>(Lcom/infraware/office/evengine/EvNative;III)V
    .locals 0
    .param p1, "a_Native"    # Lcom/infraware/office/evengine/EvNative;
    .param p2, "bLandScape"    # I
    .param p3, "a_nWidth"    # I
    .param p4, "a_nHeight"    # I

    .prologue
    .line 388
    invoke-direct {p0, p1}, Lcom/infraware/office/evengine/EvTaskObj;-><init>(Lcom/infraware/office/evengine/EvNative;)V

    .line 389
    iput p2, p0, Lcom/infraware/office/evengine/EvPivotScreenObj;->m_bLandScape:I

    .line 390
    iput p3, p0, Lcom/infraware/office/evengine/EvPivotScreenObj;->m_nWidth:I

    .line 391
    iput p4, p0, Lcom/infraware/office/evengine/EvPivotScreenObj;->m_nHeight:I

    .line 392
    return-void
.end method


# virtual methods
.method doTask()V
    .locals 4

    .prologue
    .line 396
    iget-object v0, p0, Lcom/infraware/office/evengine/EvPivotScreenObj;->Native:Lcom/infraware/office/evengine/EvNative;

    iget v1, p0, Lcom/infraware/office/evengine/EvPivotScreenObj;->m_bLandScape:I

    iget v2, p0, Lcom/infraware/office/evengine/EvPivotScreenObj;->m_nWidth:I

    iget v3, p0, Lcom/infraware/office/evengine/EvPivotScreenObj;->m_nHeight:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/office/evengine/EvNative;->IPivotScreen(III)V

    .line 397
    return-void
.end method
