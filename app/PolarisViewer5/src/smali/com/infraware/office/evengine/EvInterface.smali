.class public abstract Lcom/infraware/office/evengine/EvInterface;
.super Ljava/lang/Object;
.source "EvInterface.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_DOCEXTENSION_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_DOCTYPE;
.implements Lcom/infraware/office/evengine/E;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/office/evengine/EvInterface$OnZoomChangeListener;,
        Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;
    }
.end annotation


# static fields
.field protected static mInterface:Lcom/infraware/office/evengine/EvInterface;


# instance fields
.field protected Ev:Lcom/infraware/office/evengine/EV;

.field protected HasAppDirInfo:Z

.field protected InterfaceVector:Ljava/util/Vector;

.field protected Native:Lcom/infraware/office/evengine/EvNative;

.field protected mEditorMode:I

.field protected mHeapSize:I

.field protected mNativeInterfaceHandle:I

.field private mSlideShowSync:Ljava/lang/Object;

.field protected mbInit:Z

.field protected mbSuspend:I


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "AppDir"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput v1, p0, Lcom/infraware/office/evengine/EvInterface;->mHeapSize:I

    .line 23
    iput-boolean v1, p0, Lcom/infraware/office/evengine/EvInterface;->mbInit:Z

    .line 24
    iput v1, p0, Lcom/infraware/office/evengine/EvInterface;->mbSuspend:I

    .line 25
    iput v1, p0, Lcom/infraware/office/evengine/EvInterface;->mEditorMode:I

    .line 26
    iput v1, p0, Lcom/infraware/office/evengine/EvInterface;->mNativeInterfaceHandle:I

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->InterfaceVector:Ljava/util/Vector;

    .line 29
    iput-boolean v1, p0, Lcom/infraware/office/evengine/EvInterface;->HasAppDirInfo:Z

    .line 30
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->mSlideShowSync:Ljava/lang/Object;

    .line 46
    if-eqz p1, :cond_0

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/office/evengine/EvInterface;->HasAppDirInfo:Z

    .line 48
    :cond_0
    new-instance v0, Lcom/infraware/office/evengine/EV;

    invoke-direct {v0}, Lcom/infraware/office/evengine/EV;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Ev:Lcom/infraware/office/evengine/EV;

    .line 49
    new-instance v0, Lcom/infraware/office/evengine/EvNative;

    invoke-direct {v0, p0, p1}, Lcom/infraware/office/evengine/EvNative;-><init>(Lcom/infraware/office/evengine/EvInterface;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    .line 50
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->InterfaceVector:Ljava/util/Vector;

    .line 51
    return-void
.end method

.method public static getInterface()Lcom/infraware/office/evengine/EvInterface;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 58
    :try_start_0
    sget-object v2, Lcom/infraware/office/evengine/EvInterface;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    if-nez v2, :cond_0

    .line 67
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    return-object v1

    .line 62
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    sget-object v1, Lcom/infraware/office/evengine/EvInterface;->mInterface:Lcom/infraware/office/evengine/EvInterface;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 63
    :catch_0
    move-exception v0

    .line 65
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getInterface(Ljava/lang/String;)Lcom/infraware/office/evengine/EvInterface;
    .locals 1
    .param p0, "appDir"    # Ljava/lang/String;

    .prologue
    .line 74
    sget-object v0, Lcom/infraware/office/evengine/EvInterface;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    if-nez v0, :cond_0

    .line 75
    new-instance v0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EvCompInterfaceMsg;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/infraware/office/evengine/EvInterface;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 80
    :cond_0
    sget-object v0, Lcom/infraware/office/evengine/EvInterface;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    return-object v0
.end method


# virtual methods
.method public abstract AddOpendFileList(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract CheckOpenedFileList(Ljava/lang/String;)Z
.end method

.method public abstract DeleteOpenedFileList(Ljava/lang/String;)Z
.end method

.method public EV()Lcom/infraware/office/evengine/EV;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Ev:Lcom/infraware/office/evengine/EV;

    return-object v0
.end method

.method public abstract IAnnotationShow(Z)V
.end method

.method public abstract IApplyBookClip(Lcom/infraware/office/evengine/EV$BOOK_CLIP;)V
.end method

.method public abstract IApplyBookMark()V
.end method

.method public abstract IBookMarkOnOff(I)V
.end method

.method public abstract IBookmarkEditor(ILjava/lang/String;)V
.end method

.method public abstract IBulletNumbering(III)V
.end method

.method public abstract ICanCellDelete_Editor()Z
.end method

.method public abstract ICanExtendSortRange()V
.end method

.method public abstract ICancel()V
.end method

.method public abstract ICaretMark(II)V
.end method

.method public abstract ICaretMove(II)V
.end method

.method public abstract ICaretShow(I)V
.end method

.method public abstract ICellEdit(II)V
.end method

.method public abstract ICellEqualWidthHeight(I)V
.end method

.method public abstract ICellInsertDelete(II)V
.end method

.method public abstract ICellMergeSeparate(III)V
.end method

.method public abstract IChangeCase(I)V
.end method

.method public abstract IChangeDisplay(I)V
.end method

.method public abstract IChangeScreen(III)V
.end method

.method public abstract IChangeViewMode(IIIIII)V
.end method

.method public abstract ICharInput()V
.end method

.method public abstract ICharInsert(III)V
.end method

.method public abstract IChartAxesInfo(I[C[C[I[C[D[IC)V
.end method

.method public abstract IChartChangeDataRange(Z)V
.end method

.method public abstract IChartChangeDataRangeEnd()V
.end method

.method public abstract IChartDataLabelInfo(IIIIII)V
.end method

.method public abstract IChartFontInfo(Ljava/lang/String;F)V
.end method

.method public abstract IChartStyleInfo(II)V
.end method

.method public abstract IChartTitleInfo(IIZZZZ)V
.end method

.method public abstract ICheckVideoObjInGroupShape()Z
.end method

.method public abstract IClearFrameSet()V
.end method

.method public IClose()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 390
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvNative;->SetListener(Lcom/infraware/office/evengine/EvListener$ViewerListener;Lcom/infraware/office/evengine/EvListener$EditorListener;Lcom/infraware/office/evengine/EvListener$WordEditorListener;Lcom/infraware/office/evengine/EvListener$PptEditorListener;Lcom/infraware/office/evengine/EvListener$SheetEditorListener;Lcom/infraware/office/evengine/EvListener$PdfViewerListener;)V

    .line 391
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->SetPrintListener(Lcom/infraware/office/evengine/EvListener$PrintListener;)V

    .line 392
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->SetPdfAnnotListener(Lcom/infraware/office/evengine/EvListener$PdfAnnotListener;)V

    .line 393
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->clear()V

    .line 394
    return-void
.end method

.method public abstract ICreatePDFAnnotation(I)V
.end method

.method public abstract ICreatePDFStickyNote(II)V
.end method

.method public abstract ICreateTable(IIII)V
.end method

.method public abstract IDeleteBookClip(Lcom/infraware/office/evengine/EV$BOOK_CLIP;)V
.end method

.method public abstract IDeletePenDataForFreeDraw()V
.end method

.method public abstract IDeletePenDataForSlideShow()V
.end method

.method public abstract IDocumentModified_Editor()Z
.end method

.method public abstract IEditDocument(IILjava/lang/String;)V
.end method

.method public abstract IEditPageRedrawBitmap()V
.end method

.method public abstract IExitPreview()V
.end method

.method public abstract IExportPDF(Ljava/lang/String;I[I)V
.end method

.method public abstract IFinalize()V
.end method

.method public abstract IFindWordNext(I)V
.end method

.method public abstract IFindWordNextByPos(II)V
.end method

.method public abstract IFindWordStart(II)V
.end method

.method public abstract IFindWordStop()V
.end method

.method public abstract IFlick(II)V
.end method

.method public abstract IGetAllChartInfo()Lcom/infraware/office/evengine/EV$GUI_SHEET_ALL_CHART_EVENT;
.end method

.method public abstract IGetAnimationInfo(I)Lcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;
.end method

.method public abstract IGetApplyCellCount()I
.end method

.method public abstract IGetAutoResizeFlag()Z
.end method

.method public abstract IGetBWPCellStatusInfo()I
.end method

.method public abstract IGetBWPChartStyle()I
.end method

.method public abstract IGetBWPGrapAttrInfo_Editor()Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;
.end method

.method public abstract IGetBWPOpInfo_Editor()Lcom/infraware/office/evengine/EV$BWP_OP_INFO;
.end method

.method public abstract IGetBWPProtectStatusInfo()I
.end method

.method public abstract IGetBWPSplitCellMaxNum_Editor()Lcom/infraware/office/evengine/EV$BWP_SPLITCELL_MAXNUM;
.end method

.method public abstract IGetBookClipCount(Lcom/infraware/office/evengine/EV$BOOK_CLIP;)I
.end method

.method public abstract IGetBookClipName(ILcom/infraware/office/evengine/EV$BOOK_CLIP;)V
.end method

.method public abstract IGetBookmarkCount_Editor()I
.end method

.method public abstract IGetBookmarkInfo_Editor(I)Ljava/lang/String;
.end method

.method public abstract IGetBookmarkLabel(I)Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;
.end method

.method public abstract IGetBorderProperty()Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;
.end method

.method public abstract IGetBulletType_Editor()Lcom/infraware/office/evengine/EV$BULLET_TYPE;
.end method

.method public abstract IGetBwpChart()Lcom/infraware/office/evengine/EV$BWP_CHART;
.end method

.method public abstract IGetCaretAfterString(I)Ljava/lang/String;
.end method

.method public abstract IGetCaretBeforeString(I)Ljava/lang/String;
.end method

.method public abstract IGetCaretInfo_Editor()Lcom/infraware/office/evengine/EV$CARET_INFO;
.end method

.method public abstract IGetCaretPos()Lcom/infraware/office/evengine/EV$CARET_POS;
.end method

.method public abstract IGetCellInfo()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;
.end method

.method public abstract IGetCellMarkRectInfo([SI)I
.end method

.method public abstract IGetCellProperty_Editor()Lcom/infraware/office/evengine/EV$CELL_PROPERTY;
.end method

.method public abstract IGetCellType()I
.end method

.method public abstract IGetChartAxesInfo()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;
.end method

.method public abstract IGetChartDataLabelInfo()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;
.end method

.method public abstract IGetChartEffect()I
.end method

.method public abstract IGetChartFontData()Lcom/infraware/office/evengine/EV$CHART_FONTDATA;
.end method

.method public abstract IGetChartInfo()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;
.end method

.method public abstract IGetChartStyleInfo()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_STYLEINFO_EVENT;
.end method

.method public abstract IGetChartThumbnail(III)V
.end method

.method public abstract IGetChartTitleInfo()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;
.end method

.method public abstract IGetColumn()I
.end method

.method public abstract IGetCommentText()Ljava/lang/String;
.end method

.method public abstract IGetCompatibilityModeVersion()I
.end method

.method public abstract IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;
.end method

.method public abstract IGetCurrentSheetIndex()I
.end method

.method public abstract IGetCurrentTableMaxRowColInfo_Editor([I)Z
.end method

.method public abstract IGetDocType()I
.end method

.method public abstract IGetDrawCellLine_Editor()Lcom/infraware/office/evengine/EV$DRAW_CELLLINE;
.end method

.method public abstract IGetDualViewPosForSlideShow(II)Lcom/infraware/office/evengine/EV$DUALVIEW_POS;
.end method

.method public IGetDvLister()Lcom/infraware/office/evengine/EvListener$ViewerListener;
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->GetDvListener()Lcom/infraware/office/evengine/EvListener$ViewerListener;

    move-result-object v0

    return-object v0
.end method

.method public abstract IGetEditStauts_Editor()J
.end method

.method public abstract IGetEditorMode()I
.end method

.method public abstract IGetEditorMode_Editor()I
.end method

.method public IGetEvLister()Lcom/infraware/office/evengine/EvListener$EditorListener;
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->GetEvListener()Lcom/infraware/office/evengine/EvListener$EditorListener;

    move-result-object v0

    return-object v0
.end method

.method public IGetEvPDFLister()Lcom/infraware/office/evengine/EvListener$PdfViewerListener;
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->GetEvPDFListener()Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

    move-result-object v0

    return-object v0
.end method

.method public IGetEvPLister()Lcom/infraware/office/evengine/EvListener$PptEditorListener;
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->GetEvPListener()Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    move-result-object v0

    return-object v0
.end method

.method public IGetEvSLister()Lcom/infraware/office/evengine/EvListener$SheetEditorListener;
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->GetEvSListener()Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    move-result-object v0

    return-object v0
.end method

.method public IGetEvWLister()Lcom/infraware/office/evengine/EvListener$WordEditorListener;
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->GetEvWListener()Lcom/infraware/office/evengine/EvListener$WordEditorListener;

    move-result-object v0

    return-object v0
.end method

.method public abstract IGetFontAttInfo_Editor()Lcom/infraware/office/evengine/EV$FONT_INFO;
.end method

.method public abstract IGetFontStyle()I
.end method

.method public abstract IGetFormatInfo()Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;
.end method

.method public abstract IGetFrameDetectionArea()Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;
.end method

.method public abstract IGetGradientDrawColorInfo(Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;)Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;
.end method

.method public abstract IGetGuides()Lcom/infraware/office/evengine/EV$GUIDES_INFO;
.end method

.method public abstract IGetHeaderFooterOption()Lcom/infraware/office/evengine/EV$HeaderFooterOption;
.end method

.method public abstract IGetHeaderFooterOption(I)Lcom/infraware/office/evengine/EV$HeaderFooterOption;
.end method

.method public abstract IGetHyperLinkInfoEx(II)Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;
.end method

.method public abstract IGetHyperLinkInfo_Editor()Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;
.end method

.method public abstract IGetHyperLinkInfo_Enable()Z
.end method

.method public abstract IGetImgToPDF([Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract IGetInfraPenDrawMode()I
.end method

.method public IGetInitialHeapSize()I
    .locals 1

    .prologue
    .line 264
    iget v0, p0, Lcom/infraware/office/evengine/EvInterface;->mHeapSize:I

    return v0
.end method

.method public abstract IGetInvalidRect_Editor()Lcom/infraware/office/evengine/EV$INVALID_DRAW_INFO;
.end method

.method public abstract IGetIsSlideHide(I)Z
.end method

.method public abstract IGetIsValidateScreenSlides()Z
.end method

.method public abstract IGetLineSpaceUnitChange(I)I
.end method

.method public abstract IGetMarkString()Ljava/lang/String;
.end method

.method public abstract IGetMasterSlideImage(II)V
.end method

.method public abstract IGetMultiSelectPointInfo(I[I)V
.end method

.method public abstract IGetNextCommentText()V
.end method

.method public abstract IGetObjectCount()I
.end method

.method public abstract IGetObjectType(II)I
.end method

.method public abstract IGetPDFAnnotationCount()V
.end method

.method public abstract IGetPDFAnnotationListItem(ILcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;)V
.end method

.method public abstract IGetPDFAuthor()Ljava/lang/String;
.end method

.method public abstract IGetPDFBookmarkCount(J)I
.end method

.method public abstract IGetPDFBookmarkList(JI[Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;)V
.end method

.method public abstract IGetPDFTitle()Ljava/lang/String;
.end method

.method public abstract IGetPPTChartBorder(I)Z
.end method

.method public abstract IGetPageDisplayInfo()[Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;
.end method

.method public abstract IGetPageThumbnail(IIIILjava/lang/String;I)V
.end method

.method public abstract IGetPageToBitmap(III)V
.end method

.method public abstract IGetPaperInfo(Lcom/infraware/office/evengine/EV$PAPER_INFO;)V
.end method

.method public abstract IGetParaAttInfo_Editor()Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;
.end method

.method public abstract IGetPrevCommentText()V
.end method

.method public abstract IGetProperties()Lcom/infraware/office/evengine/EV$PROPERTIES;
.end method

.method public abstract IGetRulerbarImage(II)V
.end method

.method public abstract IGetRulerbarPgInfo(Lcom/infraware/office/evengine/EV$RULERBAR_PAGE_INFO;)V
.end method

.method public abstract IGetScreenPos()Lcom/infraware/office/evengine/EV$SCREEN_INFO;
.end method

.method public abstract IGetScrollInfo_Editor()Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;
.end method

.method public abstract IGetSectionInfo()Lcom/infraware/office/evengine/EV$SECTION_INFO;
.end method

.method public abstract IGetSectionInfo(I)Lcom/infraware/office/evengine/EV$SECTION_INFO;
.end method

.method public abstract IGetSeparateMarkString_Editor()Ljava/lang/String;
.end method

.method public abstract IGetShape3DFormatInfo()Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;
.end method

.method public abstract IGetShape3DRotationInfo()Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;
.end method

.method public abstract IGetShapeArtisticEffectInfo()Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;
.end method

.method public abstract IGetShapeCroppingInfo()Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;
.end method

.method public abstract IGetShapeEffect(I)Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;
.end method

.method public abstract IGetShapeFillInfo()Lcom/infraware/office/evengine/EV$SHAPE_FILL;
.end method

.method public abstract IGetShapeGlowInfo()Lcom/infraware/office/evengine/EV$SHAPE_GLOW;
.end method

.method public abstract IGetShapeLineColorInfo()Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;
.end method

.method public abstract IGetShapeLineStyleInfo()Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;
.end method

.method public abstract IGetShapeLocationInfo()Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;
.end method

.method public abstract IGetShapePictureColorInfo()Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;
.end method

.method public abstract IGetShapePictureCorrectionInfo()Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;
.end method

.method public abstract IGetShapeQuickStyleInfo()Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;
.end method

.method public abstract IGetShapeReflectionInfo()Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;
.end method

.method public abstract IGetShapeShadowInfo()Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;
.end method

.method public abstract IGetShapeSizeInfo()Lcom/infraware/office/evengine/EV$SHAPE_SIZE;
.end method

.method public abstract IGetShapeSoftEdgeInfo()Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;
.end method

.method public abstract IGetShapeStyleNum()I
.end method

.method public abstract IGetShapeTextBoxInfo()Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;
.end method

.method public abstract IGetSheetCount()I
.end method

.method public abstract IGetSheetHyperLinkInfo()Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;
.end method

.method public abstract IGetSheetInfo(Lcom/infraware/office/evengine/EV$SHEET_INFO;I)V
.end method

.method public abstract IGetSheetNameList()[Ljava/lang/String;
.end method

.method public abstract IGetSheetPdfPrintTotalPage(I)I
.end method

.method public abstract IGetSheetPrintTotalPage(IIIIIII)I
.end method

.method public abstract IGetSheetTextboxRectInfo([I)I
.end method

.method public abstract IGetSlideAnimationList_Count()I
.end method

.method public abstract IGetSlideAreaForSlideShow(Landroid/graphics/Rect;)V
.end method

.method public abstract IGetSlideBackgroundColor(I)I
.end method

.method public abstract IGetSlideLayout(I)I
.end method

.method public abstract IGetSlideNoteString_Editor(I)Ljava/lang/String;
.end method

.method public abstract IGetSlideShowEffect(I)Lcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;
.end method

.method public abstract IGetSlideShowPlay(II)I
.end method

.method public abstract IGetSlideShowVideoInfo(IILandroid/graphics/Rect;)Ljava/lang/String;
.end method

.method public abstract IGetSortRange(Lcom/infraware/office/evengine/EV$RANGE;I)I
.end method

.method public abstract IGetSummaryData()Lcom/infraware/office/evengine/EV$SUMMARY_DATA;
.end method

.method public abstract IGetSystemFontCount()I
.end method

.method public abstract IGetSystemFontNames()[Ljava/lang/String;
.end method

.method public abstract IGetTableMode()I
.end method

.method public abstract IGetTableStyleInfo()Lcom/infraware/office/evengine/EV$TABLE_STYLE_INFO;
.end method

.method public abstract IGetTextMarkRectInfo([SI)I
.end method

.method public abstract IGetTextToSpeachString(I)V
.end method

.method public abstract IGetTextWrapType()I
.end method

.method public abstract IGetTopRedoDataInfo()I
.end method

.method public abstract IGetTopUndoDataInfo()I
.end method

.method public abstract IGetTouchInfo(Lcom/infraware/office/evengine/EV$TOUCH_INFO;)V
.end method

.method public abstract IGetTouchInfoEX(Lcom/infraware/office/evengine/EV$TOUCH_INFO;)V
.end method

.method public abstract IGetTouchString(II)Ljava/lang/String;
.end method

.method public abstract IGetUseFontCount()I
.end method

.method public abstract IGetUseFontNames()[Ljava/lang/String;
.end method

.method public abstract IGetVideoPath()Ljava/lang/String;
.end method

.method public abstract IGetVideoRect(Landroid/graphics/Rect;)V
.end method

.method public abstract IGetViewOption()I
.end method

.method public abstract IGetWrongSpell(II)Ljava/lang/String;
.end method

.method public abstract IGotoAnnotation(IIIIFFFFZ)V
.end method

.method public abstract IGotoPDFBookmark(J)V
.end method

.method public abstract IHIDAction(IIIIII)V
.end method

.method public abstract IHasDocComments()Z
.end method

.method public abstract IHasPDFAnnots()Z
.end method

.method public abstract IHasPDFText()Z
.end method

.method public abstract IHyperLink(III)V
.end method

.method public abstract IHyperLinkEnd()V
.end method

.method public abstract IHyperlinkEditor(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract IImageInsert(Ljava/lang/String;Landroid/graphics/Bitmap;IIZ)V
.end method

.method public abstract IIndentation(I)V
.end method

.method public abstract IInfraPenAllErase()V
.end method

.method public IInitInterfaceHandleAddress()I
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IInitInterfaceHandle()I

    move-result v0

    return v0
.end method

.method public IInitialize(II)V
    .locals 2
    .param p1, "a_nWidth"    # I
    .param p2, "a_nHeight"    # I

    .prologue
    .line 364
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    iget v1, p0, Lcom/infraware/office/evengine/EvInterface;->mHeapSize:I

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->ISetHeapSize(I)V

    .line 365
    return-void
.end method

.method public abstract IInputChar(I)V
.end method

.method public abstract IInsertShape(I)V
.end method

.method public abstract IInsertShapeStyle(II)V
.end method

.method public abstract IInsertString(Ljava/lang/String;III)V
.end method

.method public abstract IInsertTextBox(Z)V
.end method

.method public abstract IIsComplexColumn()Z
.end method

.method public abstract IIsContinuePageView_Editor()I
.end method

.method public abstract IIsItalicAttr()I
.end method

.method public abstract IIsLastSlideShow(Z)I
.end method

.method public abstract IIsNextEffect()Z
.end method

.method public abstract IIsNoneEffect(II)Z
.end method

.method public abstract IIsPDFSaveAble()Z
.end method

.method public abstract IIsPenDataForFreeDraw()I
.end method

.method public abstract IIsPenDataForSlideShow()I
.end method

.method public abstract IIsPenDataForSlideShow(I)I
.end method

.method public abstract IIsSlideShow()Z
.end method

.method public abstract IIsSlideShowing()Z
.end method

.method public abstract IIsSupportFocusTextToSpeach()Z
.end method

.method public abstract IIsWaitingFlag()Z
.end method

.method public abstract IIsWaitingStatus()Z
.end method

.method public abstract IMemo(ILjava/lang/String;IIIII)V
.end method

.method public abstract IMemoCommand(ILcom/infraware/office/evengine/EV$MEMO_CMD_DATA;)Z
.end method

.method public abstract IModifyPDFAnnotation(ILjava/lang/String;I)V
.end method

.method public abstract IMovePage(II)V
.end method

.method public abstract INewDocument(Ljava/lang/String;IIIIIILjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract INoMarginView()V
.end method

.method public abstract IOpen(Ljava/lang/String;IIIIILjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract IOpenEx(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract IPDFMapRectToView(IIIIII[I)V
.end method

.method public abstract IPDFMapRectToViewEX(I[I)V
.end method

.method public abstract IPDFSaveAnnot()V
.end method

.method public abstract IPDFUpdated()Z
.end method

.method public abstract IPageModified_Editor(I)Z
.end method

.method public abstract IParagraphAlign(I)V
.end method

.method public abstract IPivotScreen(III)V
.end method

.method public abstract IPopupOffset(IIIII)V
.end method

.method public abstract IPrint(IIILjava/lang/String;)V
.end method

.method public abstract IReDraw()V
.end method

.method public abstract IRedoUndo(I)V
.end method

.method public abstract IRemoveAllBookMark()V
.end method

.method public abstract IRemoveAllPDFAnnotation()V
.end method

.method public abstract IRemoveBookMark(Ljava/lang/String;)V
.end method

.method public abstract IRemovePDFAnnotation(I)V
.end method

.method public abstract IReplaceWrongSpell(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract IRotateFrame(I)V
.end method

.method public abstract IRotatePage(II)V
.end method

.method public abstract ISaveBookMark()V
.end method

.method public abstract ISaveDocument(Ljava/lang/String;)V
.end method

.method public abstract ISaveDocument(Ljava/lang/String;I)V
.end method

.method public abstract ISaveThumbnailAt(IIIIILjava/lang/String;I)V
.end method

.method public IScreenCaptureModeOnOff(I)V
    .locals 0
    .param p1, "a_bOn"    # I

    .prologue
    .line 509
    return-void
.end method

.method public abstract IScroll(IIIII)V
.end method

.method public abstract IScrollAndFitToWidth(IIII)V
.end method

.method public abstract ISearchStart(Ljava/lang/String;IIII)V
.end method

.method public abstract ISearchStop()V
.end method

.method public abstract ISelectAll()V
.end method

.method public abstract ISellectAll()V
.end method

.method public abstract ISetAnimationDelete(I)V
.end method

.method public abstract ISetAnimationMove(II)V
.end method

.method public abstract ISetAutofilterButtonConfiguration(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract ISetBookClip(Lcom/infraware/office/evengine/EV$BOOK_CLIP;)I
.end method

.method public abstract ISetBookmarkLabel(Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;I)V
.end method

.method public abstract ISetBorder(IIIIIIIIIIIZ)V
.end method

.method public abstract ISetBwpChart(II[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)V
.end method

.method public abstract ISetCellProperty(IIIIIZ)V
.end method

.method public abstract ISetChartEffect(I)V
.end method

.method public abstract ISetChartStyle(I)V
.end method

.method public abstract ISetClearAllPen()V
.end method

.method public abstract ISetColors(III)V
.end method

.method public abstract ISetColumn(II)V
.end method

.method public abstract ISetCompBackColor(IIIIJJIII)V
.end method

.method public abstract ISetCroppingMode(II)V
.end method

.method public abstract ISetDrawCellLineProperty(IIIIZ)V
.end method

.method ISetEditorMode_Editor(I)V
    .locals 0
    .param p1, "EV_EDITMODETYPE"    # I

    .prologue
    .line 360
    iput p1, p0, Lcom/infraware/office/evengine/EvInterface;->mEditorMode:I

    .line 361
    return-void
.end method

.method public abstract ISetFillColor(I)V
.end method

.method public abstract ISetFindModeChange(I)V
.end method

.method public abstract ISetFontAttribute(Ljava/lang/String;IIIIIIIIIZ)V
.end method

.method public abstract ISetFontStyle(I)V
.end method

.method public abstract ISetFormCopyPaste(I)V
.end method

.method public abstract ISetFrameDetectionArea(Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;)V
.end method

.method public abstract ISetFrameGroup(I)V
.end method

.method public abstract ISetGuides(IZ)V
.end method

.method public abstract ISetHaderFooterAction(III)V
.end method

.method public abstract ISetHaderFooterNavigation(I)V
.end method

.method public abstract ISetHeaderFooterOption(IIZZ)V
.end method

.method public ISetHeapSize(I)V
    .locals 1
    .param p1, "a_nHeapSize"    # I

    .prologue
    .line 256
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetHeapSize(I)V

    .line 257
    return-void
.end method

.method public abstract ISetImageEffect(IIIIIIZ)V
.end method

.method public abstract ISetInfraPenDrawMode(I)V
.end method

.method public abstract ISetInfraPenShow(Z)V
.end method

.method public abstract ISetLineShape(IIII)V
.end method

.method public abstract ISetLineSpace(I)V
.end method

.method public abstract ISetListener(Lcom/infraware/office/evengine/EvListener$ViewerListener;Lcom/infraware/office/evengine/EvListener$EditorListener;Lcom/infraware/office/evengine/EvListener$WordEditorListener;Lcom/infraware/office/evengine/EvListener$PptEditorListener;Lcom/infraware/office/evengine/EvListener$SheetEditorListener;Lcom/infraware/office/evengine/EvListener$PdfViewerListener;)V
.end method

.method public ISetLocale(I)V
    .locals 1
    .param p1, "a_nLocale"    # I

    .prologue
    .line 260
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetLocale(I)V

    .line 261
    return-void
.end method

.method public ISetMarkingByPen(IIII)V
    .locals 1
    .param p1, "a_nStartX"    # I
    .param p2, "a_nStartY"    # I
    .param p3, "a_nEndX"    # I
    .param p4, "a_nEndY"    # I

    .prologue
    .line 761
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/infraware/office/evengine/EvNative;->ISetMarkingByPen(IIII)V

    .line 762
    return-void
.end method

.method public abstract ISetMemoView(III)V
.end method

.method public abstract ISetModeStatus(I)V
.end method

.method public abstract ISetMultiObjectAlign(I)V
.end method

.method public abstract ISetMultiSelect(I)V
.end method

.method public abstract ISetObjDelete()V
.end method

.method public abstract ISetObjPos(I)V
.end method

.method public abstract ISetObjResize(II)V
.end method

.method public abstract ISetObjTextEdit()V
.end method

.method public abstract ISetObjectAttribute(IIIIIIIIIIZ)V
.end method

.method public abstract ISetPDFAnnotationMoveable(Z)V
.end method

.method public abstract ISetPDFBgColor(I)Z
.end method

.method public abstract ISetPPTChartBorder(IZ)V
.end method

.method public abstract ISetPPTChartEffect(I)V
.end method

.method public abstract ISetPPTChartRowColChange()V
.end method

.method public abstract ISetPPTChartShowHideData(Z)V
.end method

.method public abstract ISetPPTSlideGLSync()V
.end method

.method public abstract ISetPageMap(IIIIII)V
.end method

.method public abstract ISetPageMode(I)V
.end method

.method public abstract ISetPaperInfo(ILcom/infraware/office/evengine/EV$PAPER_INFO;)V
.end method

.method public abstract ISetParaAttribute(IIIIIIIIII)V
.end method

.method public abstract ISetParaAttributeMask(IIIIIIIIIIIIIZ)V
.end method

.method public abstract ISetPdfAnnotListener(Lcom/infraware/office/evengine/EvListener$PdfAnnotListener;)V
.end method

.method public abstract ISetPenColor(I)V
.end method

.method public abstract ISetPenMode(IZ)V
.end method

.method public abstract ISetPenPosition([I[I[I[II)V
.end method

.method public abstract ISetPenSize(I)V
.end method

.method public abstract ISetPenTransparency(I)V
.end method

.method public abstract ISetPreview(IILjava/lang/String;)V
.end method

.method public abstract ISetPreviewTimerCB()V
.end method

.method public abstract ISetPrint(IIILjava/lang/String;I)V
.end method

.method public abstract ISetPrintEx(IIILjava/lang/String;ILjava/lang/String;Ljava/lang/String;IIII)V
.end method

.method public abstract ISetPrintListener(Lcom/infraware/office/evengine/EvListener$PrintListener;)V
.end method

.method public abstract ISetPrintMode()V
.end method

.method public abstract ISetProperties(Lcom/infraware/office/evengine/EV$PROPERTIES;)V
.end method

.method public abstract ISetRefNote(II)V
.end method

.method public abstract ISetReplace(Ljava/lang/String;IIILjava/lang/String;I)V
.end method

.method public abstract ISetResetUndoData()I
.end method

.method public abstract ISetSavePenDraw(Z)V
.end method

.method public abstract ISetScreenMode(I)V
.end method

.method public abstract ISetShadowStyle(I)V
.end method

.method public abstract ISetShapeEffect(IIIIIIIIIIZ)V
.end method

.method public abstract ISetShapeProperty(ILcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;Lcom/infraware/office/evengine/EV$SHAPE_FILL;Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;Lcom/infraware/office/evengine/EV$SHAPE_GLOW;Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;Lcom/infraware/office/evengine/EV$SHAPE_SIZE;Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;)V
.end method

.method public abstract ISetShapeStyle(I)V
.end method

.method public abstract ISetShapeStyleNum(I)V
.end method

.method public abstract ISetSheetHyperLinkInfo(Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;)V
.end method

.method public abstract ISetSheetScreenFirstSelection()V
.end method

.method public abstract ISetSlideAnimation(IIIIIIII)V
.end method

.method public abstract ISetSlideAnimationAdd(IIIIIIII)V
.end method

.method public abstract ISetSlideBackgroundColor(II)V
.end method

.method public abstract ISetSlideHide(IZ)V
.end method

.method public abstract ISetSlideLayout(II)V
.end method

.method public abstract ISetSlideShowEffect(IIIIIII)V
.end method

.method public abstract ISetSortRange(Lcom/infraware/office/evengine/EV$RANGE;)V
.end method

.method public abstract ISetSummaryData(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract ISetTableCancleMode()V
.end method

.method public abstract ISetTableDrawMode()V
.end method

.method public abstract ISetTableEreaseMode()V
.end method

.method public abstract ISetTableProperty(IIIIIIZ)V
.end method

.method public abstract ISetTableStyleInfo(III)V
.end method

.method public abstract ISetTemplateShape(II)V
.end method

.method public abstract ISetTextWrapType(I)V
.end method

.method public abstract ISetViewMode(I)V
.end method

.method public abstract ISetWebMode()V
.end method

.method public abstract ISetZoom(IIIIIIIIIII)V
.end method

.method public abstract IShapeInsertEx(IIIIIII)V
.end method

.method public abstract ISheetAutoFilterMenuListUpdate(I)V
.end method

.method public abstract ISheetBorder(Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;)V
.end method

.method public abstract ISheetClear(I)V
.end method

.method public abstract ISheetDeleteCommentText()V
.end method

.method public abstract ISheetDrawFormulaRange(ZZ)V
.end method

.method public abstract ISheetEdit(ILjava/lang/String;IIII)V
.end method

.method public abstract ISheetEditCF(Lcom/infraware/office/evengine/EV$RANGE;III[C)V
.end method

.method public abstract ISheetFilter()V
.end method

.method public abstract ISheetFilterCommand(IILjava/lang/String;)V
.end method

.method public abstract ISheetFilterIsRunning()Z
.end method

.method public abstract ISheetFindReplace(Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract ISheetFixFrame()V
.end method

.method public abstract ISheetFocus()V
.end method

.method public abstract ISheetFormat(IIIIIIIIIZ)V
.end method

.method public abstract ISheetFunction(I)V
.end method

.method public abstract ISheetGetCFInfo()Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;
.end method

.method public abstract ISheetGetSortKeyInfo([S)Z
.end method

.method public abstract ISheetGetTextBoxText()Ljava/lang/String;
.end method

.method public abstract ISheetInputField(I)V
.end method

.method public abstract ISheetInsertAllChart(IILcom/infraware/office/evengine/EV$RANGE;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZZ)V
.end method

.method public abstract ISheetInsertCell(II)V
.end method

.method public abstract ISheetInsertChart(ILcom/infraware/office/evengine/EV$RANGE;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZZZIZZ)V
.end method

.method public abstract ISheetInsertColumns(III)V
.end method

.method public abstract ISheetInsertCommentText(Ljava/lang/String;)V
.end method

.method public abstract ISheetInsertRows(III)V
.end method

.method public abstract ISheetIsClipboardData()Z
.end method

.method public abstract ISheetIsObjClicked()Z
.end method

.method public abstract ISheetIsSameObjSelected()Z
.end method

.method public abstract ISheetIsTextBox()Z
.end method

.method public abstract ISheetIsWholeCols()Z
.end method

.method public abstract ISheetIsWholeRows()Z
.end method

.method public abstract ISheetMerge()V
.end method

.method public abstract ISheetPageBreak()V
.end method

.method public abstract ISheetProtection()V
.end method

.method public abstract ISheetRecalculate()V
.end method

.method public abstract ISheetSetAlignment(II)V
.end method

.method public abstract ISheetSetAutoFormula(I)V
.end method

.method public abstract ISheetSetColor(I)V
.end method

.method public abstract ISheetSetFormulaRangeColor([I)V
.end method

.method public abstract ISheetSetRowColSize(III)V
.end method

.method public abstract ISheetSetTextboxEditMode(I)V
.end method

.method public abstract ISheetSetTextboxText(Ljava/lang/String;I)V
.end method

.method public abstract ISheetShowHideRowCol(III)V
.end method

.method public abstract ISheetSort(IIII)V
.end method

.method public abstract ISheetWrap()V
.end method

.method public abstract IShowHideImage(I)V
.end method

.method public abstract ISlideAddMove(III)V
.end method

.method public abstract ISlideCopy(I)V
.end method

.method public abstract ISlideCut(I)V
.end method

.method public abstract ISlideManage(III)V
.end method

.method public abstract ISlideNote(III)V
.end method

.method public abstract ISlideNoteInput(ILjava/lang/String;I)V
.end method

.method public abstract ISlideObjInsert(III)V
.end method

.method public abstract ISlideObjStart(III)V
.end method

.method public abstract ISlideObjStartEx(IIFIZ)V
.end method

.method public abstract ISlidePaste(I)V
.end method

.method public abstract ISlideShow(IIIIIZ)V
.end method

.method public abstract ISlideShow(IIIIIZZ)V
.end method

.method public abstract ISlideShowContinue()V
.end method

.method public abstract ISlideShowPlay(I)V
.end method

.method public abstract ISlideShowPlay(III)V
.end method

.method public abstract ISlideShowPlay(IIIZ)V
.end method

.method public abstract ISlideShowPlay(IZ)V
.end method

.method public abstract ISpellCheckScreen(Z)V
.end method

.method public abstract ISpellCheckWrong(Ljava/lang/String;IIIIIII)I
.end method

.method public abstract IStopFlicking()V
.end method

.method public abstract IStopSlideEffect()V
.end method

.method public abstract ITempSaveDocument(Ljava/lang/String;)V
.end method

.method public abstract ITextPlay(IIII)V
.end method

.method public abstract IThreadSuspend(I)V
.end method

.method public abstract IThumbnail(IIIII)V
.end method

.method public abstract IVideoInsert(Ljava/lang/String;Landroid/graphics/Bitmap;IIZLjava/lang/String;Z)V
.end method

.method public abstract IZoomInOut(II)V
.end method

.method public abstract IsInsertBookmark_Editor()I
.end method

.method public abstract IsLassoViewMode_Editor()Z
.end method

.method public abstract IsPenDrawFrameShow()Z
.end method

.method public abstract IsStartOfSentence_Editor()I
.end method

.method public abstract IsWebMode()I
.end method

.method OnFinalizeComplete()V
    .locals 1

    .prologue
    .line 356
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/evengine/EvInterface;->mbInit:Z

    .line 357
    return-void
.end method

.method OnInitComplete(I)V
    .locals 1
    .param p1, "EEV_ERROR_CODE"    # I

    .prologue
    .line 352
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/office/evengine/EvInterface;->mbInit:Z

    .line 353
    return-void
.end method

.method abstract OnTimerStart()V
.end method

.method abstract OnTimerStop()V
.end method

.method protected SearchInterfaceVectorbyArg(I)Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;
    .locals 2
    .param p1, "Address"    # I

    .prologue
    .line 242
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/infraware/office/evengine/EvInterface;->InterfaceVector:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 243
    iget-object v1, p0, Lcom/infraware/office/evengine/EvInterface;->InterfaceVector:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;->getHandle()I

    move-result v1

    if-ne v1, p1, :cond_0

    .line 244
    iget-object v1, p0, Lcom/infraware/office/evengine/EvInterface;->InterfaceVector:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;

    .line 247
    :goto_1
    return-object v1

    .line 242
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 247
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method protected SearchInterfaceVectorbyArg(Ljava/lang/String;)Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;
    .locals 2
    .param p1, "Path"    # Ljava/lang/String;

    .prologue
    .line 231
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/infraware/office/evengine/EvInterface;->InterfaceVector:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 232
    iget-object v1, p0, Lcom/infraware/office/evengine/EvInterface;->InterfaceVector:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 234
    iget-object v1, p0, Lcom/infraware/office/evengine/EvInterface;->InterfaceVector:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;

    .line 237
    :goto_1
    return-object v1

    .line 231
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 237
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public SetInterfaceHandleAddress(I)V
    .locals 3
    .param p1, "InterfaceHandleAddress"    # I

    .prologue
    .line 291
    iget-object v1, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvNative;->IGetInterfaceHandleValue()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/infraware/office/evengine/EvInterface;->SearchInterfaceVectorbyArg(I)Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;

    move-result-object v0

    .line 293
    .local v0, "temp":Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/infraware/office/evengine/EvInterface;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;->getEv()Lcom/infraware/office/evengine/EV;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 294
    iget-object v1, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvNative;->IGetInterfaceHandleValue()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/infraware/office/evengine/EvInterface;->SearchInterfaceVectorbyArg(I)Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/evengine/EvInterface;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV;->clone()Lcom/infraware/office/evengine/EV;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;->setEv(Ljava/lang/Object;)V

    .line 297
    :cond_0
    iget-object v1, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v1, p1}, Lcom/infraware/office/evengine/EvNative;->ISetInterfaceHandle(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 303
    invoke-virtual {p0, p1}, Lcom/infraware/office/evengine/EvInterface;->SearchInterfaceVectorbyArg(I)Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;

    move-result-object v0

    .line 304
    if-eqz v0, :cond_1

    .line 305
    iget-object v1, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;->getTempPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lcom/infraware/office/evengine/EvNative;->setTempFolder(Ljava/lang/String;I)V

    .line 306
    iget-object v1, p0, Lcom/infraware/office/evengine/EvInterface;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;->getEv()Lcom/infraware/office/evengine/EV;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 307
    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;->getEv()Lcom/infraware/office/evengine/EV;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->clone()Lcom/infraware/office/evengine/EV;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/office/evengine/EvInterface;->Ev:Lcom/infraware/office/evengine/EV;

    .line 312
    :cond_1
    return-void
.end method

.method public deleteInterfaceHandle(I)V
    .locals 1
    .param p1, "interfaceHandleAddress"    # I

    .prologue
    .line 281
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->IDeleteInterfaceHandle(I)V

    .line 283
    return-void
.end method

.method public getDocFileExtentionType(Ljava/lang/String;)I
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 141
    const/16 v0, 0xff

    .line 142
    .local v0, "EV_DOCEXTENSION_TYPE":I
    const/16 v3, 0x2e

    invoke-virtual {p1, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 143
    .local v1, "nIndex":I
    if-ltz v1, :cond_0

    .line 144
    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 145
    .local v2, "strExt":Ljava/lang/String;
    const-string/jumbo v3, ".doc"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1

    .line 146
    const/4 v0, 0x2

    .line 186
    .end local v2    # "strExt":Ljava/lang/String;
    :cond_0
    :goto_0
    return v0

    .line 147
    .restart local v2    # "strExt":Ljava/lang/String;
    :cond_1
    const-string/jumbo v3, ".dot"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_2

    .line 148
    const/4 v0, 0x2

    goto :goto_0

    .line 149
    :cond_2
    const-string/jumbo v3, ".docx"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_3

    .line 150
    const/16 v0, 0x12

    goto :goto_0

    .line 151
    :cond_3
    const-string/jumbo v3, ".dotx"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_4

    .line 152
    const/16 v0, 0x12

    goto :goto_0

    .line 153
    :cond_4
    const-string/jumbo v3, ".ppt"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_5

    .line 154
    const/4 v0, 0x1

    goto :goto_0

    .line 155
    :cond_5
    const-string/jumbo v3, ".pot"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_6

    .line 156
    const/4 v0, 0x1

    goto :goto_0

    .line 157
    :cond_6
    const-string/jumbo v3, ".pps"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_7

    .line 158
    const/16 v0, 0x27

    goto :goto_0

    .line 159
    :cond_7
    const-string/jumbo v3, ".pptx"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_8

    .line 160
    const/16 v0, 0x13

    goto :goto_0

    .line 161
    :cond_8
    const-string/jumbo v3, ".potx"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_9

    .line 162
    const/16 v0, 0x13

    goto :goto_0

    .line 163
    :cond_9
    const-string/jumbo v3, ".ppsx"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_a

    .line 164
    const/16 v0, 0x28

    goto :goto_0

    .line 165
    :cond_a
    const-string/jumbo v3, ".xls"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_b

    .line 166
    const/4 v0, 0x5

    goto :goto_0

    .line 167
    :cond_b
    const-string/jumbo v3, ".xlt"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_c

    .line 168
    const/4 v0, 0x5

    goto :goto_0

    .line 169
    :cond_c
    const-string/jumbo v3, ".xlsx"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_d

    .line 170
    const/16 v0, 0x14

    goto/16 :goto_0

    .line 171
    :cond_d
    const-string/jumbo v3, ".xltx"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_e

    .line 172
    const/16 v0, 0x14

    goto/16 :goto_0

    .line 173
    :cond_e
    const-string/jumbo v3, ".csv"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_f

    .line 174
    const/16 v0, 0x26

    goto/16 :goto_0

    .line 175
    :cond_f
    const-string/jumbo v3, ".pdf"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_10

    .line 176
    const/4 v0, 0x6

    goto/16 :goto_0

    .line 177
    :cond_10
    const-string/jumbo v3, ".txt"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_11

    const-string/jumbo v3, ".text"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_12

    .line 179
    :cond_11
    const/16 v0, 0xc

    goto/16 :goto_0

    .line 180
    :cond_12
    const-string/jumbo v3, ".hwp"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_13

    .line 181
    const/4 v0, 0x3

    goto/16 :goto_0

    .line 182
    :cond_13
    const-string/jumbo v3, ".rtf"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_0

    .line 183
    const/16 v0, 0x25

    goto/16 :goto_0
.end method

.method public getFontFileList()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->GetFontFileList()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getJNIInterfaceHandleValue()I
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IGetInterfaceHandleValue()I

    move-result v0

    return v0
.end method

.method public getMemoCmdData()Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    .locals 1

    .prologue
    .line 1637
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getMemoCmdData()Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;

    move-result-object v0

    return-object v0
.end method

.method public getNativeInterfaceHandle()I
    .locals 1

    .prologue
    .line 318
    iget v0, p0, Lcom/infraware/office/evengine/EvInterface;->mNativeInterfaceHandle:I

    return v0
.end method

.method isInit()Z
    .locals 1

    .prologue
    .line 348
    iget-boolean v0, p0, Lcom/infraware/office/evengine/EvInterface;->mbInit:Z

    return v0
.end method

.method public setNativeInterfaceHandle(I)V
    .locals 0
    .param p1, "Address"    # I

    .prologue
    .line 322
    iput p1, p0, Lcom/infraware/office/evengine/EvInterface;->mNativeInterfaceHandle:I

    .line 323
    return-void
.end method

.method public abstract setOnZoomChangeListener(Lcom/infraware/office/evengine/EvInterface$OnZoomChangeListener;)V
.end method
