.class public Lcom/infraware/office/evengine/EV$PAPER_INFO;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PAPER_INFO"
.end annotation


# instance fields
.field public a_Bottom:I

.field public a_Column:I

.field public a_Left:I

.field public a_MarginType:I

.field public a_OrientationType:I

.field public a_Right:I

.field public a_SizeType:I

.field public a_Top:I

.field public a_bAllPage:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 1740
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$PAPER_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 1755
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$PAPER_INFO;->a_bAllPage:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$PAPER_INFO;->a_Bottom:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$PAPER_INFO;->a_Right:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$PAPER_INFO;->a_Top:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$PAPER_INFO;->a_Left:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$PAPER_INFO;->a_Column:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$PAPER_INFO;->a_OrientationType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$PAPER_INFO;->a_SizeType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$PAPER_INFO;->a_MarginType:I

    .line 1756
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$PAPER_INFO;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1759
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$PAPER_INFO;

    .line 1760
    .local v0, "o":Lcom/infraware/office/evengine/EV$PAPER_INFO;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1740
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$PAPER_INFO;->clone()Lcom/infraware/office/evengine/EV$PAPER_INFO;

    move-result-object v0

    return-object v0
.end method
