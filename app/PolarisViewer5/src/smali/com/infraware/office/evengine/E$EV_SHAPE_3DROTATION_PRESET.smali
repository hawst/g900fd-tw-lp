.class public interface abstract Lcom/infraware/office/evengine/E$EV_SHAPE_3DROTATION_PRESET;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_SHAPE_3DROTATION_PRESET"
.end annotation


# static fields
.field public static final BR_FRAME_3DROTATION_EXIST:I = 0x1

.field public static final BR_FRAME_3DROTATION_ISOMETRIC_BOTTOM_DOWN:I = 0x5

.field public static final BR_FRAME_3DROTATION_ISOMETRIC_LEFT_DOWN:I = 0x2

.field public static final BR_FRAME_3DROTATION_ISOMETRIC_RIGHT_UP:I = 0x3

.field public static final BR_FRAME_3DROTATION_ISOMETRIC_TOP_UP:I = 0x4

.field public static final BR_FRAME_3DROTATION_MAX:I = 0x1b

.field public static final BR_FRAME_3DROTATION_NONE:I = 0x0

.field public static final BR_FRAME_3DROTATION_OFF_AXIS_1_LEFT:I = 0x6

.field public static final BR_FRAME_3DROTATION_OFF_AXIS_1_RIGHT:I = 0x7

.field public static final BR_FRAME_3DROTATION_OFF_AXIS_1_TOP:I = 0x8

.field public static final BR_FRAME_3DROTATION_OFF_AXIS_2_LEFT:I = 0x9

.field public static final BR_FRAME_3DROTATION_OFF_AXIS_2_RIGHT:I = 0xa

.field public static final BR_FRAME_3DROTATION_OFF_AXIS_2_TOP:I = 0xb

.field public static final BR_FRAME_3DROTATION_OFF_OBLIQUE_BOTTOM_LEFT:I = 0x19

.field public static final BR_FRAME_3DROTATION_OFF_OBLIQUE_BOTTOM_RIGHT:I = 0x1a

.field public static final BR_FRAME_3DROTATION_OFF_OBLIQUE_TOP_LEFT:I = 0x17

.field public static final BR_FRAME_3DROTATION_OFF_OBLIQUE_TOP_RIGHT:I = 0x18

.field public static final BR_FRAME_3DROTATION_OFF_PERSPECITVE_ABOVE:I = 0x10

.field public static final BR_FRAME_3DROTATION_OFF_PERSPECITVE_BELOW:I = 0xf

.field public static final BR_FRAME_3DROTATION_OFF_PERSPECITVE_CONTRASTING_LEFT:I = 0x13

.field public static final BR_FRAME_3DROTATION_OFF_PERSPECITVE_CONTRASTING_RIGHT:I = 0x14

.field public static final BR_FRAME_3DROTATION_OFF_PERSPECITVE_FRONT:I = 0xc

.field public static final BR_FRAME_3DROTATION_OFF_PERSPECITVE_HEROIC_EXTREME_LEFT:I = 0x15

.field public static final BR_FRAME_3DROTATION_OFF_PERSPECITVE_HEROIC_EXTREME_RIGHT:I = 0x16

.field public static final BR_FRAME_3DROTATION_OFF_PERSPECITVE_LEFT:I = 0xd

.field public static final BR_FRAME_3DROTATION_OFF_PERSPECITVE_RELAXED:I = 0x12

.field public static final BR_FRAME_3DROTATION_OFF_PERSPECITVE_RELAXED_MODERATELY:I = 0x11

.field public static final BR_FRAME_3DROTATION_OFF_PERSPECITVE_RIGHT:I = 0xe
