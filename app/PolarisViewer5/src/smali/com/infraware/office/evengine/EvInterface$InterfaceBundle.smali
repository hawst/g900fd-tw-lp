.class Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;
.super Ljava/lang/Object;
.source "EvInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EvInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "InterfaceBundle"
.end annotation


# instance fields
.field protected mAddress:I

.field protected mEv:Lcom/infraware/office/evengine/EV;

.field protected mPath:Ljava/lang/String;

.field protected mTempPath:Ljava/lang/String;

.field final synthetic this$0:Lcom/infraware/office/evengine/EvInterface;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EvInterface;Lcom/infraware/office/evengine/EV;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "inEv"    # Lcom/infraware/office/evengine/EV;
    .param p3, "address"    # I
    .param p4, "inPath"    # Ljava/lang/String;
    .param p5, "inTempPath"    # Ljava/lang/String;

    .prologue
    .line 201
    iput-object p1, p0, Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;->this$0:Lcom/infraware/office/evengine/EvInterface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 202
    iput-object p2, p0, Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;->mEv:Lcom/infraware/office/evengine/EV;

    .line 203
    iput p3, p0, Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;->mAddress:I

    .line 204
    iput-object p4, p0, Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;->mPath:Ljava/lang/String;

    .line 205
    iput-object p5, p0, Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;->mTempPath:Ljava/lang/String;

    .line 206
    return-void
.end method


# virtual methods
.method public getEv()Lcom/infraware/office/evengine/EV;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;->mEv:Lcom/infraware/office/evengine/EV;

    return-object v0
.end method

.method public getHandle()I
    .locals 1

    .prologue
    .line 217
    iget v0, p0, Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;->mAddress:I

    return v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method public getTempPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;->mTempPath:Ljava/lang/String;

    return-object v0
.end method

.method public setEv(Ljava/lang/Object;)V
    .locals 0
    .param p1, "clone"    # Ljava/lang/Object;

    .prologue
    .line 225
    check-cast p1, Lcom/infraware/office/evengine/EV;

    .end local p1    # "clone":Ljava/lang/Object;
    iput-object p1, p0, Lcom/infraware/office/evengine/EvInterface$InterfaceBundle;->mEv:Lcom/infraware/office/evengine/EV;

    .line 226
    return-void
.end method
