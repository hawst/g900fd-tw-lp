.class public interface abstract Lcom/infraware/office/evengine/E$EV_SLIDESHOW_EFFECT_TYPE;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_SLIDESHOW_EFFECT_TYPE"
.end annotation


# static fields
.field public static final eEV_SLIDESHOW_EFFECT_APPEAR:I = 0x6

.field public static final eEV_SLIDESHOW_EFFECT_BLINDS:I = 0xe

.field public static final eEV_SLIDESHOW_EFFECT_BLINK:I = 0xb

.field public static final eEV_SLIDESHOW_EFFECT_BOX:I = 0x1a

.field public static final eEV_SLIDESHOW_EFFECT_BREAKS:I = 0x5

.field public static final eEV_SLIDESHOW_EFFECT_CHECKERBOARD:I = 0xd

.field public static final eEV_SLIDESHOW_EFFECT_CLOCK:I = 0xf

.field public static final eEV_SLIDESHOW_EFFECT_CONVEYOR:I = 0x1e

.field public static final eEV_SLIDESHOW_EFFECT_COVER:I = 0xa

.field public static final eEV_SLIDESHOW_EFFECT_CROP:I = 0x1

.field public static final eEV_SLIDESHOW_EFFECT_CUBE:I = 0x18

.field public static final eEV_SLIDESHOW_EFFECT_DISSOLVE:I = 0xc

.field public static final eEV_SLIDESHOW_EFFECT_DOORS:I = 0x19

.field public static final eEV_SLIDESHOW_EFFECT_ETC:I = 0x23

.field public static final eEV_SLIDESHOW_EFFECT_FADE:I = 0x2

.field public static final eEV_SLIDESHOW_EFFECT_FERRISWHEEL:I = 0x1d

.field public static final eEV_SLIDESHOW_EFFECT_FLIP:I = 0x16

.field public static final eEV_SLIDESHOW_EFFECT_FLYTHROUGH:I = 0x22

.field public static final eEV_SLIDESHOW_EFFECT_GALLERY:I = 0x17

.field public static final eEV_SLIDESHOW_EFFECT_GLITTER:I = 0x12

.field public static final eEV_SLIDESHOW_EFFECT_GOTO:I = 0x1c

.field public static final eEV_SLIDESHOW_EFFECT_HONEYCOMB:I = 0x11

.field public static final eEV_SLIDESHOW_EFFECT_MAIZE:I = 0x13

.field public static final eEV_SLIDESHOW_EFFECT_NONE:I = 0x0

.field public static final eEV_SLIDESHOW_EFFECT_ORBIT:I = 0x21

.field public static final eEV_SLIDESHOW_EFFECT_PUSH:I = 0x3

.field public static final eEV_SLIDESHOW_EFFECT_RANDOMBARS:I = 0x7

.field public static final eEV_SLIDESHOW_EFFECT_ROTATE:I = 0x1f

.field public static final eEV_SLIDESHOW_EFFECT_SHAPES:I = 0x8

.field public static final eEV_SLIDESHOW_EFFECT_SHRED:I = 0x14

.field public static final eEV_SLIDESHOW_EFFECT_SWITCH:I = 0x15

.field public static final eEV_SLIDESHOW_EFFECT_UNCOVER:I = 0x9

.field public static final eEV_SLIDESHOW_EFFECT_WATER:I = 0x10

.field public static final eEV_SLIDESHOW_EFFECT_WINDOW:I = 0x20

.field public static final eEV_SLIDESHOW_EFFECT_WIPE:I = 0x4

.field public static final eEV_SLIDESHOW_EFFECT_ZOOM:I = 0x1b
