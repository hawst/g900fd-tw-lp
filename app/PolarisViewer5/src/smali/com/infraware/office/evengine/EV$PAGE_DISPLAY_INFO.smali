.class public Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PAGE_DISPLAY_INFO"
.end annotation


# instance fields
.field public nPageDisplayHeight:I

.field public nPageDisplayPosX:I

.field public nPageDisplayPosY:I

.field public nPageDisplayWidth:I

.field public nPageFooterBoundaryHeight:I

.field public nPageHeaderBoundaryHeight:I

.field public nPageHeight:I

.field public nPageNum:I

.field public nPageWidth:I

.field public nScreenPagePosX:I

.field public nScreenPagePosY:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 1786
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 1803
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->nPageFooterBoundaryHeight:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->nPageHeaderBoundaryHeight:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->nPageDisplayHeight:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->nPageDisplayWidth:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->nPageDisplayPosY:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->nPageDisplayPosX:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->nScreenPagePosY:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->nScreenPagePosX:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->nPageHeight:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->nPageWidth:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->nPageNum:I

    .line 1806
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1822
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;

    .line 1823
    .local v0, "o":Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1786
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->clone()Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;

    move-result-object v0

    return-object v0
.end method

.method public isValidInfo()Z
    .locals 1

    .prologue
    .line 1809
    iget v0, p0, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->nPageNum:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->nPageWidth:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->nPageHeight:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->nScreenPagePosX:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->nScreenPagePosY:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->nPageDisplayPosX:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->nPageDisplayPosY:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->nPageDisplayWidth:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->nPageDisplayHeight:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->nPageHeaderBoundaryHeight:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->nPageFooterBoundaryHeight:I

    if-nez v0, :cond_0

    .line 1814
    const/4 v0, 0x0

    .line 1817
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
