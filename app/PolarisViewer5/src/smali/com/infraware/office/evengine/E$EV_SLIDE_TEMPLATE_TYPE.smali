.class public interface abstract Lcom/infraware/office/evengine/E$EV_SLIDE_TEMPLATE_TYPE;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_SLIDE_TEMPLATE_TYPE"
.end annotation


# static fields
.field public static final eEV_SLIDE_TEMPLATE_BLANK:I = 0x7

.field public static final eEV_SLIDE_TEMPLATE_NONE:I = 0x0

.field public static final eEV_SLIDE_TEMPLATE_OBJECT:I = 0xc

.field public static final eEV_SLIDE_TEMPLATE_PICTURE_CAPTION:I = 0x9

.field public static final eEV_SLIDE_TEMPLATE_SECTION_HEADER:I = 0x3

.field public static final eEV_SLIDE_TEMPLATE_TITLE:I = 0x1

.field public static final eEV_SLIDE_TEMPLATE_TITLE_OBJECT:I = 0x2

.field public static final eEV_SLIDE_TEMPLATE_TITLE_OBJECT_CAPTION:I = 0x8

.field public static final eEV_SLIDE_TEMPLATE_TITLE_ONLY:I = 0x6

.field public static final eEV_SLIDE_TEMPLATE_TWO_OBJECTS:I = 0x4

.field public static final eEV_SLIDE_TEMPLATE_TWO_TEXT_TWO_OBJECTS:I = 0x5

.field public static final eEV_SLIDE_TEMPLATE_VERTICALTEXT:I = 0xa

.field public static final eEV_SLIDE_TEMPLATE_VERTICALTITLE_TEXT:I = 0xb
