.class public Lcom/infraware/office/evengine/EV$SHAPE_SIZE;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SHAPE_SIZE"
.end annotation


# instance fields
.field public bFlip:Z

.field public bMirror:Z

.field public nHeight:F

.field public nRotation:F

.field public nRotationPreset:I

.field public nWidth:F

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 2684
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2694
    iput v1, p0, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;->nRotationPreset:I

    int-to-float v0, v1

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;->nRotation:F

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;->nHeight:F

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;->nWidth:F

    .line 2695
    iput-boolean v1, p0, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;->bMirror:Z

    iput-boolean v1, p0, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;->bFlip:Z

    .line 2696
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SHAPE_SIZE;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2700
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;

    .line 2701
    .local v0, "o":Lcom/infraware/office/evengine/EV$SHAPE_SIZE;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2684
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;->clone()Lcom/infraware/office/evengine/EV$SHAPE_SIZE;

    move-result-object v0

    return-object v0
.end method
