.class public interface abstract Lcom/infraware/office/evengine/EvListener$EditorListener;
.super Ljava/lang/Object;
.source "EvListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EvListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EditorListener"
.end annotation


# virtual methods
.method public abstract OnBookMarkEditorMode()V
.end method

.method public abstract OnCoreNotify(I)V
.end method

.method public abstract OnCoreNotify2(II)V
.end method

.method public abstract OnEditOrViewMode(II)V
.end method

.method public abstract OnFlick(I)V
.end method

.method public abstract OnHidAction(I)V
.end method

.method public abstract OnIMEInsertMode()V
.end method

.method public abstract OnInsertFreeformShapes()V
.end method

.method public abstract OnInsertTableMode()V
.end method

.method public abstract OnNewDoc(I)V
.end method

.method public abstract OnOLEFormatInfo(Ljava/lang/String;)V
.end method

.method public abstract OnObjectPoints(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V
.end method

.method public abstract OnSaveDoc(I)V
.end method

.method public abstract OnSpellCheck(Ljava/lang/String;IIIIIII)V
.end method

.method public abstract OnUndoOrRedo(ZIII)V
.end method
