.class public Lcom/infraware/office/evengine/EV$CHART_FONTDATA;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CHART_FONTDATA"
.end annotation


# instance fields
.field public fNames:[Ljava/lang/String;

.field public fRatio:F

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 1

    .prologue
    .line 1518
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$CHART_FONTDATA;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1520
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$CHART_FONTDATA;->fNames:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method clear()V
    .locals 3

    .prologue
    .line 1523
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x6

    if-ge v0, v1, :cond_0

    .line 1524
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$CHART_FONTDATA;->fNames:[Ljava/lang/String;

    const-string/jumbo v2, ""

    aput-object v2, v1, v0

    .line 1525
    const/4 v1, 0x0

    iput v1, p0, Lcom/infraware/office/evengine/EV$CHART_FONTDATA;->fRatio:F

    .line 1523
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1527
    :cond_0
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$CHART_FONTDATA;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1530
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$CHART_FONTDATA;

    .line 1531
    .local v0, "o":Lcom/infraware/office/evengine/EV$CHART_FONTDATA;
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$CHART_FONTDATA;->fNames:[Ljava/lang/String;

    invoke-virtual {v1}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    iput-object v1, v0, Lcom/infraware/office/evengine/EV$CHART_FONTDATA;->fNames:[Ljava/lang/String;

    .line 1532
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1518
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$CHART_FONTDATA;->clone()Lcom/infraware/office/evengine/EV$CHART_FONTDATA;

    move-result-object v0

    return-object v0
.end method
