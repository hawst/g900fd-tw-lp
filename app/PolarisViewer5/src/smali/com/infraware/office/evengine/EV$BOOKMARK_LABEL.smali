.class public Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "BOOKMARK_LABEL"
.end annotation


# instance fields
.field public szFilePath:Ljava/lang/String;

.field public szLabel:Ljava/lang/String;

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 734
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 740
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;->szFilePath:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;->szLabel:Ljava/lang/String;

    .line 741
    return-void
.end method

.method public clone()Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 744
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;

    .line 745
    .local v0, "o":Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;->szLabel:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;->szLabel:Ljava/lang/String;

    .line 746
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;->szFilePath:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;->szFilePath:Ljava/lang/String;

    .line 747
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 734
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;->clone()Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;

    move-result-object v0

    return-object v0
.end method
