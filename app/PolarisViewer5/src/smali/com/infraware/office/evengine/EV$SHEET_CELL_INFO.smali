.class public Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SHEET_CELL_INFO"
.end annotation


# instance fields
.field public oFormulaErrInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMULA_ERR_INFO;

.field public tActiveRange:Lcom/infraware/office/evengine/EV$RANGE;

.field public tColumnHeader:Lcom/infraware/office/evengine/EV$RANGE;

.field public tEditTextRange:Lcom/infraware/office/evengine/EV$RANGE;

.field public tRowHeader:Lcom/infraware/office/evengine/EV$RANGE;

.field public tSelectedRange:Lcom/infraware/office/evengine/EV$RANGE;

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;

.field public wColWidth:I

.field public wRowHeight:I


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 2

    .prologue
    .line 1265
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1267
    new-instance v0, Lcom/infraware/office/evengine/EV$RANGE;

    iget-object v1, p0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {v0, v1}, Lcom/infraware/office/evengine/EV$RANGE;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tRowHeader:Lcom/infraware/office/evengine/EV$RANGE;

    .line 1268
    new-instance v0, Lcom/infraware/office/evengine/EV$RANGE;

    iget-object v1, p0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {v0, v1}, Lcom/infraware/office/evengine/EV$RANGE;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tColumnHeader:Lcom/infraware/office/evengine/EV$RANGE;

    .line 1273
    new-instance v0, Lcom/infraware/office/evengine/EV$RANGE;

    iget-object v1, p0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {v0, v1}, Lcom/infraware/office/evengine/EV$RANGE;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tActiveRange:Lcom/infraware/office/evengine/EV$RANGE;

    .line 1274
    new-instance v0, Lcom/infraware/office/evengine/EV$RANGE;

    iget-object v1, p0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {v0, v1}, Lcom/infraware/office/evengine/EV$RANGE;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tSelectedRange:Lcom/infraware/office/evengine/EV$RANGE;

    .line 1275
    new-instance v0, Lcom/infraware/office/evengine/EV$RANGE;

    iget-object v1, p0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {v0, v1}, Lcom/infraware/office/evengine/EV$RANGE;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tEditTextRange:Lcom/infraware/office/evengine/EV$RANGE;

    .line 1276
    new-instance v0, Lcom/infraware/office/evengine/EV$SHEET_FORMULA_ERR_INFO;

    iget-object v1, p0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {v0, v1}, Lcom/infraware/office/evengine/EV$SHEET_FORMULA_ERR_INFO;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->oFormulaErrInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMULA_ERR_INFO;

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 1279
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tRowHeader:Lcom/infraware/office/evengine/EV$RANGE;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$RANGE;->clear()V

    .line 1280
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tColumnHeader:Lcom/infraware/office/evengine/EV$RANGE;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$RANGE;->clear()V

    .line 1281
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->wColWidth:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->wRowHeight:I

    .line 1282
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tActiveRange:Lcom/infraware/office/evengine/EV$RANGE;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$RANGE;->clear()V

    .line 1283
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tSelectedRange:Lcom/infraware/office/evengine/EV$RANGE;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$RANGE;->clear()V

    .line 1284
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tEditTextRange:Lcom/infraware/office/evengine/EV$RANGE;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$RANGE;->clear()V

    .line 1285
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->oFormulaErrInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMULA_ERR_INFO;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$SHEET_FORMULA_ERR_INFO;->clear()V

    .line 1286
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1289
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    .line 1290
    .local v0, "o":Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1265
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->clone()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    move-result-object v0

    return-object v0
.end method
