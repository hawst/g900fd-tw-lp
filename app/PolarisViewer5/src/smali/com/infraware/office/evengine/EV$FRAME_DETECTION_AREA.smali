.class public Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FRAME_DETECTION_AREA"
.end annotation


# instance fields
.field final ADJUST_HANDLE_DETECTION_MARGIN:I

.field final CTRLBOX_MARGIN:I

.field final FRAME_DETECTION_MARGIN:I

.field final ROTATION_CTRLBOX_HEIGHT:I

.field final SHEET_DETECTION_MARGIN:I

.field final VML_SHAPE_HANDLE_POS_MARGIN:I

.field public m_dDeviceDIP:D

.field public m_nAdjustHandleDetectionMargin:I

.field public m_nCtrlBoxMargin:I

.field public m_nFrameDetectionMargin:I

.field public m_nRotCtrlBoxHeight:I

.field public m_nSheetDetectionMargin:I

.field public m_nVMLShapeHandlePosMargin:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 6

    .prologue
    const/16 v5, 0x37

    const/16 v4, 0x28

    const/16 v3, 0x1a

    const/16 v2, 0x14

    const/16 v1, 0xa

    .line 718
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 703
    iput v3, p0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->CTRLBOX_MARGIN:I

    .line 704
    iput v5, p0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->ROTATION_CTRLBOX_HEIGHT:I

    .line 705
    iput v1, p0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->FRAME_DETECTION_MARGIN:I

    .line 706
    iput v4, p0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->SHEET_DETECTION_MARGIN:I

    .line 707
    const/16 v0, 0x12c

    iput v0, p0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->VML_SHAPE_HANDLE_POS_MARGIN:I

    .line 708
    iput v2, p0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->ADJUST_HANDLE_DETECTION_MARGIN:I

    .line 719
    iput v3, p0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_nCtrlBoxMargin:I

    .line 720
    iput v5, p0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_nRotCtrlBoxHeight:I

    .line 721
    iput v1, p0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_nFrameDetectionMargin:I

    .line 722
    iput v4, p0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_nSheetDetectionMargin:I

    .line 723
    const/16 v0, 0x12c

    iput v0, p0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_nVMLShapeHandlePosMargin:I

    .line 724
    iput v2, p0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_nAdjustHandleDetectionMargin:I

    .line 725
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iput-wide v0, p0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_dDeviceDIP:D

    .line 726
    return-void
.end method


# virtual methods
.method protected clone()Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 729
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;

    .line 730
    .local v0, "o":Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 700
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->clone()Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;

    move-result-object v0

    return-object v0
.end method
