.class public Lcom/infraware/office/evengine/EV$SHAPE_GRADIENT_STOP;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SHAPE_GRADIENT_STOP"
.end annotation


# instance fields
.field public nGradientStopBright:I

.field public nGradientStopColor:J

.field public nGradientStopLocation:I

.field public nGradientStopTranparency:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 2267
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SHAPE_GRADIENT_STOP;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 2

    .prologue
    .line 2274
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_GRADIENT_STOP;->nGradientStopColor:J

    .line 2275
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_GRADIENT_STOP;->nGradientStopTranparency:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_GRADIENT_STOP;->nGradientStopBright:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_GRADIENT_STOP;->nGradientStopLocation:I

    .line 2276
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SHAPE_GRADIENT_STOP;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2280
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SHAPE_GRADIENT_STOP;

    .line 2281
    .local v0, "o":Lcom/infraware/office/evengine/EV$SHAPE_GRADIENT_STOP;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2267
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SHAPE_GRADIENT_STOP;->clone()Lcom/infraware/office/evengine/EV$SHAPE_GRADIENT_STOP;

    move-result-object v0

    return-object v0
.end method
