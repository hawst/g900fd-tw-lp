.class public interface abstract Lcom/infraware/office/evengine/E$EV_SHAPE_GLOW_PRESET;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_SHAPE_GLOW_PRESET"
.end annotation


# static fields
.field public static final BR_FRAME_GLOW_BLUE_11PT:I = 0x4

.field public static final BR_FRAME_GLOW_BLUE_18PT:I = 0x5

.field public static final BR_FRAME_GLOW_BLUE_5PT:I = 0x2

.field public static final BR_FRAME_GLOW_BLUE_8PT:I = 0x3

.field public static final BR_FRAME_GLOW_EXIST:I = 0x1

.field public static final BR_FRAME_GLOW_MAX:I = 0x1a

.field public static final BR_FRAME_GLOW_MOSS_GREEN_11PT:I = 0xc

.field public static final BR_FRAME_GLOW_MOSS_GREEN_18PT:I = 0xd

.field public static final BR_FRAME_GLOW_MOSS_GREEN_5PT:I = 0xa

.field public static final BR_FRAME_GLOW_MOSS_GREEN_8PT:I = 0xb

.field public static final BR_FRAME_GLOW_NONE:I = 0x0

.field public static final BR_FRAME_GLOW_ORANGE_11PT:I = 0x18

.field public static final BR_FRAME_GLOW_ORANGE_18PT:I = 0x19

.field public static final BR_FRAME_GLOW_ORANGE_5PT:I = 0x16

.field public static final BR_FRAME_GLOW_ORANGE_8PT:I = 0x17

.field public static final BR_FRAME_GLOW_PURPLE_11PT:I = 0x10

.field public static final BR_FRAME_GLOW_PURPLE_18PT:I = 0x11

.field public static final BR_FRAME_GLOW_PURPLE_5PT:I = 0xe

.field public static final BR_FRAME_GLOW_PURPLE_8PT:I = 0xf

.field public static final BR_FRAME_GLOW_RED_11PT:I = 0x8

.field public static final BR_FRAME_GLOW_RED_18PT:I = 0x9

.field public static final BR_FRAME_GLOW_RED_5PT:I = 0x6

.field public static final BR_FRAME_GLOW_RED_8PT:I = 0x7

.field public static final BR_FRAME_GLOW_SEA_11PT:I = 0x14

.field public static final BR_FRAME_GLOW_SEA_18PT:I = 0x15

.field public static final BR_FRAME_GLOW_SEA_5PT:I = 0x12

.field public static final BR_FRAME_GLOW_SEA_8PT:I = 0x13
