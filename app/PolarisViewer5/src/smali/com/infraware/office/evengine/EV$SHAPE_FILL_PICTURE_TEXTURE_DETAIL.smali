.class public Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SHAPE_FILL_PICTURE_TEXTURE_DETAIL"
.end annotation


# instance fields
.field public eBitmapFormat:I

.field public nBitmapHeight:I

.field public nBitmapWidth:I

.field public nPictureTextureSelector:I

.field public nPictureTextureType:I

.field public pBitmapData:Landroid/graphics/Bitmap;

.field public szPictureFilePath:Ljava/lang/String;

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 2743
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 2760
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;->nBitmapHeight:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;->nBitmapWidth:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;->eBitmapFormat:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;->nPictureTextureType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;->nPictureTextureSelector:I

    .line 2761
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;->szPictureFilePath:Ljava/lang/String;

    .line 2762
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;->pBitmapData:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 2763
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;->pBitmapData:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 2764
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;->pBitmapData:Landroid/graphics/Bitmap;

    .line 2765
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2769
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;

    .line 2770
    .local v0, "o":Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;->szPictureFilePath:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;->szPictureFilePath:Ljava/lang/String;

    .line 2771
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;->pBitmapData:Landroid/graphics/Bitmap;

    invoke-static {v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, v0, Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;->pBitmapData:Landroid/graphics/Bitmap;

    .line 2772
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2743
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;->clone()Lcom/infraware/office/evengine/EV$SHAPE_FILL_PICTURE_TEXTURE_DETAIL;

    move-result-object v0

    return-object v0
.end method
