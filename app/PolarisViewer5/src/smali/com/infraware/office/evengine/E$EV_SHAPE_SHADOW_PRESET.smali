.class public interface abstract Lcom/infraware/office/evengine/E$EV_SHAPE_SHADOW_PRESET;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_SHAPE_SHADOW_PRESET"
.end annotation


# static fields
.field public static final BR_FRAME_SHADOW_EXIST:I = 0x1

.field public static final BR_FRAME_SHADOW_INSIDE_BOTTOM:I = 0xe

.field public static final BR_FRAME_SHADOW_INSIDE_CENTER:I = 0x13

.field public static final BR_FRAME_SHADOW_INSIDE_LEFT:I = 0xb

.field public static final BR_FRAME_SHADOW_INSIDE_LEFT_BOTTOM:I = 0x10

.field public static final BR_FRAME_SHADOW_INSIDE_LEFT_TOP:I = 0x12

.field public static final BR_FRAME_SHADOW_INSIDE_RIGHT:I = 0xc

.field public static final BR_FRAME_SHADOW_INSIDE_RIGHT_BOTTOM:I = 0xf

.field public static final BR_FRAME_SHADOW_INSIDE_RIGHT_TOP:I = 0x11

.field public static final BR_FRAME_SHADOW_INSIDE_TOP:I = 0xd

.field public static final BR_FRAME_SHADOW_MAX:I = 0x19

.field public static final BR_FRAME_SHADOW_NONE:I = 0x0

.field public static final BR_FRAME_SHADOW_OFFSET_BOTTOM:I = 0x5

.field public static final BR_FRAME_SHADOW_OFFSET_CENTER:I = 0xa

.field public static final BR_FRAME_SHADOW_OFFSET_LEFT:I = 0x2

.field public static final BR_FRAME_SHADOW_OFFSET_LEFT_BOTTOM:I = 0x7

.field public static final BR_FRAME_SHADOW_OFFSET_LEFT_TOP:I = 0x9

.field public static final BR_FRAME_SHADOW_OFFSET_RIGHT:I = 0x3

.field public static final BR_FRAME_SHADOW_OFFSET_RIGHT_BOTTOM:I = 0x6

.field public static final BR_FRAME_SHADOW_OFFSET_RIGHT_TOP:I = 0x8

.field public static final BR_FRAME_SHADOW_OFFSET_TOP:I = 0x4

.field public static final BR_FRAME_SHADOW_PERSPECTIVE_BOTTOM:I = 0x16

.field public static final BR_FRAME_SHADOW_PERSPECTIVE_LOWER_LEFT:I = 0x17

.field public static final BR_FRAME_SHADOW_PERSPECTIVE_LOWER_RIGHT:I = 0x18

.field public static final BR_FRAME_SHADOW_PERSPECTIVE_UPPER_LEFT:I = 0x14

.field public static final BR_FRAME_SHADOW_PERSPECTIVE_UPPER_RIGHT:I = 0x15
