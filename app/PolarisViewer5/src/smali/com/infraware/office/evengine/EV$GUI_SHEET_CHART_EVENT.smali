.class public Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GUI_SHEET_CHART_EVENT"
.end annotation


# instance fields
.field public bCluster:Z

.field public bExternData:Z

.field public bPercent:Z

.field public bPlotVisOnly:Z

.field public bStacked:Z

.field public nChart:I

.field public nChartStyle:I

.field public nDimension:I

.field public nLegend:I

.field public nResult:I

.field public nSeriesIn:I

.field public nType:I

.field public szTitle:Ljava/lang/String;

.field public szXAxis:Ljava/lang/String;

.field public szYAxis:Ljava/lang/String;

.field public tRange:Lcom/infraware/office/evengine/EV$RANGE;

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 2

    .prologue
    .line 1369
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1373
    new-instance v0, Lcom/infraware/office/evengine/EV$RANGE;

    iget-object v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {v0, v1}, Lcom/infraware/office/evengine/EV$RANGE;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->tRange:Lcom/infraware/office/evengine/EV$RANGE;

    .line 1375
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->szTitle:Ljava/lang/String;

    .line 1376
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->szXAxis:Ljava/lang/String;

    .line 1377
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->szYAxis:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1390
    iput v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->nChart:I

    iput v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->nType:I

    .line 1391
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->tRange:Lcom/infraware/office/evengine/EV$RANGE;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$RANGE;->clear()V

    .line 1392
    iput v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->nSeriesIn:I

    .line 1393
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->szYAxis:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->szXAxis:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->szTitle:Ljava/lang/String;

    .line 1394
    iput v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->nLegend:I

    .line 1395
    iput v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->nDimension:I

    .line 1396
    iput-boolean v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->bStacked:Z

    .line 1397
    iput-boolean v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->bPercent:Z

    .line 1398
    iput-boolean v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->bCluster:Z

    .line 1399
    iput-boolean v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->bExternData:Z

    .line 1400
    iput-boolean v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->bPlotVisOnly:Z

    .line 1401
    iput v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->nChartStyle:I

    .line 1402
    iput v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->nResult:I

    .line 1403
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1406
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;

    .line 1407
    .local v0, "o":Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->tRange:Lcom/infraware/office/evengine/EV$RANGE;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV$RANGE;->clone()Lcom/infraware/office/evengine/EV$RANGE;

    move-result-object v1

    iput-object v1, v0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->tRange:Lcom/infraware/office/evengine/EV$RANGE;

    .line 1408
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1369
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;->clone()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;

    move-result-object v0

    return-object v0
.end method
