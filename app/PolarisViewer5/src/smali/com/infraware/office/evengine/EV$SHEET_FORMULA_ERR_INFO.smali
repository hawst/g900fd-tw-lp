.class public Lcom/infraware/office/evengine/EV$SHEET_FORMULA_ERR_INFO;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SHEET_FORMULA_ERR_INFO"
.end annotation


# instance fields
.field public nErrorType:I

.field public tIndicatorPos:Lcom/infraware/office/evengine/EV$RANGE;

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 2

    .prologue
    .line 1249
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SHEET_FORMULA_ERR_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1252
    new-instance v0, Lcom/infraware/office/evengine/EV$RANGE;

    iget-object v1, p0, Lcom/infraware/office/evengine/EV$SHEET_FORMULA_ERR_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {v0, v1}, Lcom/infraware/office/evengine/EV$RANGE;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_FORMULA_ERR_INFO;->tIndicatorPos:Lcom/infraware/office/evengine/EV$RANGE;

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 1255
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHEET_FORMULA_ERR_INFO;->nErrorType:I

    .line 1256
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_FORMULA_ERR_INFO;->tIndicatorPos:Lcom/infraware/office/evengine/EV$RANGE;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$RANGE;->clear()V

    .line 1257
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SHEET_FORMULA_ERR_INFO;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1260
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SHEET_FORMULA_ERR_INFO;

    .line 1261
    .local v0, "o":Lcom/infraware/office/evengine/EV$SHEET_FORMULA_ERR_INFO;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1249
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SHEET_FORMULA_ERR_INFO;->clone()Lcom/infraware/office/evengine/EV$SHEET_FORMULA_ERR_INFO;

    move-result-object v0

    return-object v0
.end method
