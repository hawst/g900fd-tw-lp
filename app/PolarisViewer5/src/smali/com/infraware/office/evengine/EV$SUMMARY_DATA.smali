.class public Lcom/infraware/office/evengine/EV$SUMMARY_DATA;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SUMMARY_DATA"
.end annotation


# instance fields
.field public nPage:I

.field public nWords:I

.field public szAuthor:Ljava/lang/String;

.field public szModifiedBy:Ljava/lang/String;

.field public szTitle:Ljava/lang/String;

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 1

    .prologue
    .line 1350
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SUMMARY_DATA;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1352
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SUMMARY_DATA;->szTitle:Ljava/lang/String;

    .line 1353
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SUMMARY_DATA;->szAuthor:Ljava/lang/String;

    .line 1354
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SUMMARY_DATA;->szModifiedBy:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 1359
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SUMMARY_DATA;->szModifiedBy:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SUMMARY_DATA;->szAuthor:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SUMMARY_DATA;->szTitle:Ljava/lang/String;

    .line 1360
    iget v0, p0, Lcom/infraware/office/evengine/EV$SUMMARY_DATA;->nWords:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SUMMARY_DATA;->nPage:I

    .line 1361
    return-void
.end method

.method protected clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1364
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SUMMARY_DATA;

    .line 1365
    .local v0, "o":Lcom/infraware/office/evengine/EV$SUMMARY_DATA;
    return-object v0
.end method
