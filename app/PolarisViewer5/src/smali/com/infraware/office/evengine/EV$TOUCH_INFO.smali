.class public Lcom/infraware/office/evengine/EV$TOUCH_INFO;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TOUCH_INFO"
.end annotation


# instance fields
.field public bResult:I

.field public bSPos:I

.field public nHeight:I

.field public nPage:I

.field public nWidth:I

.field public nX:I

.field public nXOffset:I

.field public nY:I

.field public nYOffset:I

.field public nZoomScale:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 1941
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$TOUCH_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 1955
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$TOUCH_INFO;->nZoomScale:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$TOUCH_INFO;->nHeight:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$TOUCH_INFO;->nWidth:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$TOUCH_INFO;->nYOffset:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$TOUCH_INFO;->nXOffset:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$TOUCH_INFO;->bResult:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$TOUCH_INFO;->bSPos:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$TOUCH_INFO;->nPage:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$TOUCH_INFO;->nY:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$TOUCH_INFO;->nX:I

    .line 1956
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$TOUCH_INFO;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1960
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$TOUCH_INFO;

    .line 1961
    .local v0, "o":Lcom/infraware/office/evengine/EV$TOUCH_INFO;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1941
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$TOUCH_INFO;->clone()Lcom/infraware/office/evengine/EV$TOUCH_INFO;

    move-result-object v0

    return-object v0
.end method
