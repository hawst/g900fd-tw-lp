.class public interface abstract Lcom/infraware/office/evengine/E$EV_EDIT_OBJECT_TYPE;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_EDIT_OBJECT_TYPE"
.end annotation


# static fields
.field public static final eEV_OBJECT_BLANKIMAGE:I = 0x11

.field public static final eEV_OBJECT_BLANKOBJECT:I = 0x12

.field public static final eEV_OBJECT_CELL:I = 0x1

.field public static final eEV_OBJECT_CELLMARK:I = 0x2

.field public static final eEV_OBJECT_CHART:I = 0x8

.field public static final eEV_OBJECT_CHARTIMAGE:I = 0xd

.field public static final eEV_OBJECT_FREEFORM:I = 0xf

.field public static final eEV_OBJECT_GROUP:I = 0xa

.field public static final eEV_OBJECT_HEADER_COLUMN:I = 0xc

.field public static final eEV_OBJECT_HEADER_ROW:I = 0xb

.field public static final eEV_OBJECT_IMAGE:I = 0x5

.field public static final eEV_OBJECT_INFRAPEN:I = 0x13

.field public static final eEV_OBJECT_INSERT:I = 0x1001

.field public static final eEV_OBJECT_LINE:I = 0x9

.field public static final eEV_OBJECT_MAX:I = 0x7fff

.field public static final eEV_OBJECT_MULTI:I = 0x1f

.field public static final eEV_OBJECT_NONE:I = 0x0

.field public static final eEV_OBJECT_OLEIMAGE:I = 0xe

.field public static final eEV_OBJECT_ONLY:I = 0xfff

.field public static final eEV_OBJECT_RECTANGLE:I = 0x6

.field public static final eEV_OBJECT_SELECTED_CLICK:I = 0x1000

.field public static final eEV_OBJECT_TABLE:I = 0x4

.field public static final eEV_OBJECT_TABLE_DRAW:I = 0x20

.field public static final eEV_OBJECT_TABLE_ERASE:I = 0x30

.field public static final eEV_OBJECT_TABLE_NEW_DRAW:I = 0x50

.field public static final eEV_OBJECT_TABLE_REDRW:I = 0x40

.field public static final eEV_OBJECT_TEXTFRAME:I = 0x7

.field public static final eEV_OBJECT_TEXTMARK:I = 0x3

.field public static final eEV_OBJECT_VIDEO:I = 0x10
