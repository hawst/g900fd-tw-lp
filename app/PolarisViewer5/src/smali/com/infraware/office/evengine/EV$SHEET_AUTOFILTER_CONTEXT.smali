.class public Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SHEET_AUTOFILTER_CONTEXT"
.end annotation


# instance fields
.field public tDataRange:Lcom/infraware/office/evengine/EV$RANGE;

.field public tFilterRange:Lcom/infraware/office/evengine/EV$RANGE;

.field public tIndexRange:Lcom/infraware/office/evengine/EV$RANGE;

.field public tStartRange:Lcom/infraware/office/evengine/EV$RANGE;

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 2

    .prologue
    .line 1229
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1231
    new-instance v0, Lcom/infraware/office/evengine/EV$RANGE;

    iget-object v1, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {v0, v1}, Lcom/infraware/office/evengine/EV$RANGE;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;->tFilterRange:Lcom/infraware/office/evengine/EV$RANGE;

    .line 1232
    new-instance v0, Lcom/infraware/office/evengine/EV$RANGE;

    iget-object v1, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {v0, v1}, Lcom/infraware/office/evengine/EV$RANGE;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;->tIndexRange:Lcom/infraware/office/evengine/EV$RANGE;

    .line 1233
    new-instance v0, Lcom/infraware/office/evengine/EV$RANGE;

    iget-object v1, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {v0, v1}, Lcom/infraware/office/evengine/EV$RANGE;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;->tDataRange:Lcom/infraware/office/evengine/EV$RANGE;

    .line 1234
    new-instance v0, Lcom/infraware/office/evengine/EV$RANGE;

    iget-object v1, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {v0, v1}, Lcom/infraware/office/evengine/EV$RANGE;-><init>(Lcom/infraware/office/evengine/EV;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;->tStartRange:Lcom/infraware/office/evengine/EV$RANGE;

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 1237
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;->tFilterRange:Lcom/infraware/office/evengine/EV$RANGE;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$RANGE;->clear()V

    .line 1238
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;->tIndexRange:Lcom/infraware/office/evengine/EV$RANGE;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$RANGE;->clear()V

    .line 1239
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;->tDataRange:Lcom/infraware/office/evengine/EV$RANGE;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$RANGE;->clear()V

    .line 1240
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;->tStartRange:Lcom/infraware/office/evengine/EV$RANGE;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$RANGE;->clear()V

    .line 1241
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1244
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;

    .line 1245
    .local v0, "o":Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1229
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;->clone()Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_CONTEXT;

    move-result-object v0

    return-object v0
.end method
