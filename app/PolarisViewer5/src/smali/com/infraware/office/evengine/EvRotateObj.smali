.class Lcom/infraware/office/evengine/EvRotateObj;
.super Lcom/infraware/office/evengine/EvTaskObj;
.source "EvTaskObj.java"


# instance fields
.field private m_eRotateType:I

.field private m_nAngle:I


# direct methods
.method constructor <init>(Lcom/infraware/office/evengine/EvNative;II)V
    .locals 0
    .param p1, "a_Native"    # Lcom/infraware/office/evengine/EvNative;
    .param p2, "a_eRotateType"    # I
    .param p3, "a_nAngle"    # I

    .prologue
    .line 309
    invoke-direct {p0, p1}, Lcom/infraware/office/evengine/EvTaskObj;-><init>(Lcom/infraware/office/evengine/EvNative;)V

    .line 310
    iput p2, p0, Lcom/infraware/office/evengine/EvRotateObj;->m_eRotateType:I

    .line 311
    iput p3, p0, Lcom/infraware/office/evengine/EvRotateObj;->m_nAngle:I

    .line 312
    return-void
.end method


# virtual methods
.method doTask()V
    .locals 3

    .prologue
    .line 316
    iget-object v0, p0, Lcom/infraware/office/evengine/EvRotateObj;->Native:Lcom/infraware/office/evengine/EvNative;

    iget v1, p0, Lcom/infraware/office/evengine/EvRotateObj;->m_eRotateType:I

    iget v2, p0, Lcom/infraware/office/evengine/EvRotateObj;->m_nAngle:I

    invoke-virtual {v0, v1, v2}, Lcom/infraware/office/evengine/EvNative;->IRotatePage(II)V

    .line 317
    return-void
.end method
