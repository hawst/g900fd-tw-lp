.class public interface abstract Lcom/infraware/office/evengine/E$EV_BORDER_STYLE;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_BORDER_STYLE"
.end annotation


# static fields
.field public static final eEV_BORDER_STYLE_1_THIN:I = 0x1

.field public static final eEV_BORDER_STYLE_2_HAIR:I = 0x7

.field public static final eEV_BORDER_STYLE_3_DOTTED:I = 0x4

.field public static final eEV_BORDER_STYLE_4_DASHED:I = 0x3

.field public static final eEV_BORDER_STYLE_5_DASHDOT:I = 0x9

.field public static final eEV_BORDER_STYLE_6_MEDIUMDASH:I = 0x8

.field public static final eEV_BORDER_STYLE_7_MEDIUMDASHDOT:I = 0xa

.field public static final eEV_BORDER_STYLE_8_MEDIUMDASHDOTDOT:I = 0xc

.field public static final eEV_BORDER_STYLE_COMPLEX:I = 0xe

.field public static final eEV_BORDER_STYLE_DASHDOT:I = 0x9

.field public static final eEV_BORDER_STYLE_DASHDOTDOT:I = 0xb

.field public static final eEV_BORDER_STYLE_DASHED:I = 0x3

.field public static final eEV_BORDER_STYLE_DOTTED:I = 0x4

.field public static final eEV_BORDER_STYLE_DOUBLE:I = 0x6

.field public static final eEV_BORDER_STYLE_HAIR:I = 0x7

.field public static final eEV_BORDER_STYLE_MAX:I = 0x16

.field public static final eEV_BORDER_STYLE_MEDIUM:I = 0x2

.field public static final eEV_BORDER_STYLE_MEDIUMDASH:I = 0x8

.field public static final eEV_BORDER_STYLE_MEDIUMDASHDOT:I = 0xa

.field public static final eEV_BORDER_STYLE_MEDIUMDASHDOTDOT:I = 0xc

.field public static final eEV_BORDER_STYLE_MEDIUMDOT:I = 0x10

.field public static final eEV_BORDER_STYLE_MEDIUMROUNDDOT:I = 0x12

.field public static final eEV_BORDER_STYLE_NONE:I = 0x0

.field public static final eEV_BORDER_STYLE_ROUNDDOT:I = 0x11

.field public static final eEV_BORDER_STYLE_SLANTEDDASHDOT:I = 0xd

.field public static final eEV_BORDER_STYLE_SOLID:I = 0xf

.field public static final eEV_BORDER_STYLE_THICK:I = 0x5

.field public static final eEV_BORDER_STYLE_THICKTHIN:I = 0x13

.field public static final eEV_BORDER_STYLE_THIN:I = 0x1

.field public static final eEV_BORDER_STYLE_THINTHICK:I = 0x14

.field public static final eEV_BORDER_STYLE_TRIPLE:I = 0x15

.field public static final eEV_SHEET_BORDER_STYLE_10_MEDIUM:I = 0x2

.field public static final eEV_SHEET_BORDER_STYLE_11_THICK:I = 0x5

.field public static final eEV_SHEET_BORDER_STYLE_12_DOUBLE:I = 0x6

.field public static final eEV_SHEET_BORDER_STYLE_1_THIN:I = 0x1

.field public static final eEV_SHEET_BORDER_STYLE_2_HAIR:I = 0x7

.field public static final eEV_SHEET_BORDER_STYLE_3_DOTTED:I = 0x4

.field public static final eEV_SHEET_BORDER_STYLE_4_DASHED:I = 0x3

.field public static final eEV_SHEET_BORDER_STYLE_5_DASHDOT:I = 0x9

.field public static final eEV_SHEET_BORDER_STYLE_6_DASHDOTDOT:I = 0xb

.field public static final eEV_SHEET_BORDER_STYLE_7_MEDIUMDASHDOTDOT:I = 0xc

.field public static final eEV_SHEET_BORDER_STYLE_8_MEDIUMDASHDOT:I = 0xa

.field public static final eEV_SHEET_BORDER_STYLE_9_MEDIUMDASH:I = 0x8
