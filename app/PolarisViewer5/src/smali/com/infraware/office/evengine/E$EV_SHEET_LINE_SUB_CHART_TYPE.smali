.class public interface abstract Lcom/infraware/office/evengine/E$EV_SHEET_LINE_SUB_CHART_TYPE;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_SHEET_LINE_SUB_CHART_TYPE"
.end annotation


# static fields
.field public static final eEV_SHEET_CHART_3D_LINE:I = 0x7

.field public static final eEV_SHEET_CHART_LINE:I = 0x1

.field public static final eEV_SHEET_CHART_LINE_WITH_MARKERS:I = 0x4

.field public static final eEV_SHEET_CHART_PERCENT_LINE:I = 0x3

.field public static final eEV_SHEET_CHART_PERCENT_LINE_WITH_MARKERS:I = 0x6

.field public static final eEV_SHEET_CHART_STACKED_LINE:I = 0x2

.field public static final eEV_SHEET_CHART_STACKED_LINE_WITH_MARKERS:I = 0x5
