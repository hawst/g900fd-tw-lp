.class public Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;
.super Landroid/os/Handler;
.source "EvCompInterfaceMsg.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EvCompInterfaceMsg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "HandlerTask"
.end annotation


# static fields
.field private static final RUNTIMER:I


# instance fields
.field private mbAlive:Z

.field final synthetic this$0:Lcom/infraware/office/evengine/EvCompInterfaceMsg;


# direct methods
.method protected constructor <init>(Lcom/infraware/office/evengine/EvCompInterfaceMsg;)V
    .locals 1

    .prologue
    .line 53
    iput-object p1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->this$0:Lcom/infraware/office/evengine/EvCompInterfaceMsg;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->mbAlive:Z

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 70
    invoke-virtual {p0, v1}, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->removeMessages(I)V

    .line 71
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 87
    :cond_0
    :goto_0
    return-void

    .line 74
    :pswitch_0
    iget-boolean v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->mbAlive:Z

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->this$0:Lcom/infraware/office/evengine/EvCompInterfaceMsg;

    iget v0, v0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mbSuspend:I

    if-nez v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->this$0:Lcom/infraware/office/evengine/EvCompInterfaceMsg;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->ITimer()V

    .line 82
    :cond_1
    iget-boolean v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->mbAlive:Z

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->this$0:Lcom/infraware/office/evengine/EvCompInterfaceMsg;

    iget v0, v0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mbSuspend:I

    if-nez v0, :cond_0

    .line 84
    invoke-virtual {p0, v1}, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 71
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method setOperationTimer(Z)V
    .locals 3
    .param p1, "bStart"    # Z

    .prologue
    const/4 v2, 0x0

    .line 59
    iput-boolean p1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->mbAlive:Z

    .line 60
    iget-boolean v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->mbAlive:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 62
    invoke-virtual {p0, v2}, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->sendEmptyMessage(I)Z

    .line 67
    :goto_0
    return-void

    .line 64
    :cond_0
    const-string/jumbo v0, "EvCompInterfaceMsg"

    const-string/jumbo v1, "remove timer1"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-virtual {p0, v2}, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->removeMessages(I)V

    goto :goto_0
.end method
