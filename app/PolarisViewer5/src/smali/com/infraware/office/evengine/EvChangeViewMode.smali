.class Lcom/infraware/office/evengine/EvChangeViewMode;
.super Lcom/infraware/office/evengine/EvTaskObj;
.source "EvTaskObj.java"


# instance fields
.field EEV_VIEW_MODE:I

.field bCanSelection:I

.field bDraw:I

.field bFixedHeader:I

.field nHeight:I

.field nWidth:I


# direct methods
.method constructor <init>(Lcom/infraware/office/evengine/EvNative;IIIIII)V
    .locals 0
    .param p1, "a_Native"    # Lcom/infraware/office/evengine/EvNative;
    .param p2, "a_EEV_VIEW_MODE"    # I
    .param p3, "a_nWidth"    # I
    .param p4, "a_nHeight"    # I
    .param p5, "a_bCanSelection"    # I
    .param p6, "a_bFixedHeader"    # I
    .param p7, "a_bDraw"    # I

    .prologue
    .line 847
    invoke-direct {p0, p1}, Lcom/infraware/office/evengine/EvTaskObj;-><init>(Lcom/infraware/office/evengine/EvNative;)V

    .line 848
    iput p2, p0, Lcom/infraware/office/evengine/EvChangeViewMode;->EEV_VIEW_MODE:I

    .line 849
    iput p3, p0, Lcom/infraware/office/evengine/EvChangeViewMode;->nWidth:I

    .line 850
    iput p4, p0, Lcom/infraware/office/evengine/EvChangeViewMode;->nHeight:I

    .line 851
    iput p5, p0, Lcom/infraware/office/evengine/EvChangeViewMode;->bCanSelection:I

    .line 852
    iput p6, p0, Lcom/infraware/office/evengine/EvChangeViewMode;->bFixedHeader:I

    .line 853
    iput p7, p0, Lcom/infraware/office/evengine/EvChangeViewMode;->bDraw:I

    .line 854
    return-void
.end method


# virtual methods
.method doTask()V
    .locals 7

    .prologue
    .line 858
    iget v0, p0, Lcom/infraware/office/evengine/EvChangeViewMode;->EEV_VIEW_MODE:I

    if-nez v0, :cond_0

    .line 859
    iget-object v0, p0, Lcom/infraware/office/evengine/EvChangeViewMode;->Native:Lcom/infraware/office/evengine/EvNative;

    iget v1, p0, Lcom/infraware/office/evengine/EvChangeViewMode;->EEV_VIEW_MODE:I

    iget v2, p0, Lcom/infraware/office/evengine/EvChangeViewMode;->nWidth:I

    iget v3, p0, Lcom/infraware/office/evengine/EvChangeViewMode;->nHeight:I

    iget v4, p0, Lcom/infraware/office/evengine/EvChangeViewMode;->bCanSelection:I

    iget v5, p0, Lcom/infraware/office/evengine/EvChangeViewMode;->bFixedHeader:I

    iget v6, p0, Lcom/infraware/office/evengine/EvChangeViewMode;->bDraw:I

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvNative;->IChangeViewMode(IIIIII)V

    .line 860
    :cond_0
    return-void
.end method
