.class public Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PDF_BOOKMARK_LIST_ITEM"
.end annotation


# instance fields
.field public BookmarkType:I

.field public HasKids:Z

.field public color:[F

.field public font_style:I

.field public item:J

.field public nTitleLen:I

.field public nURLLen:I

.field public szTitle:Ljava/lang/String;

.field public szURL:Ljava/lang/String;

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1889
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1892
    iput v2, p0, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;->font_style:I

    iput v2, p0, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;->nURLLen:I

    iput v2, p0, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;->BookmarkType:I

    iput v2, p0, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;->nTitleLen:I

    int-to-long v0, v2

    iput-wide v0, p0, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;->item:J

    .line 1893
    iput-boolean v2, p0, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;->HasKids:Z

    .line 1894
    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;->color:[F

    .line 1896
    return-void
.end method


# virtual methods
.method protected clone()Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1909
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;

    .line 1910
    .local v0, "o":Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;->color:[F

    invoke-virtual {v1}, [F->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [F

    iput-object v1, v0, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;->color:[F

    .line 1911
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1887
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;->clone()Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;

    move-result-object v0

    return-object v0
.end method
