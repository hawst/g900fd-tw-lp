.class public Lcom/infraware/office/evengine/EvNative;
.super Ljava/lang/Object;
.source "EvNative.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_GUI_EVENT;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private LOG_CAT:Ljava/lang/String;

.field private mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

.field private mEvCodeConversion:Lcom/infraware/office/evengine/EvCodeConversion;

.field private mEvImageUtil:Lcom/infraware/office/evengine/EvImageUtil;

.field private mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

.field private mEvPdfAnnotListener:Lcom/infraware/office/evengine/EvListener$PdfAnnotListener;

.field private mEvPdfViewerListener:Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

.field private mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

.field private mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

.field private mEvWordListener:Lcom/infraware/office/evengine/EvListener$WordEditorListener;

.field private mInterface:Lcom/infraware/office/evengine/EvInterface;

.field private mPrintListener:Lcom/infraware/office/evengine/EvListener$PrintListener;

.field private mSystemFontFilePaths:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/infraware/office/evengine/EvNative;->$assertionsDisabled:Z

    .line 1103
    const-string/jumbo v0, "EX_Engine5v"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 1104
    const-string/jumbo v0, "polarisviewer5"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 1105
    const-string/jumbo v0, "polarisviewerSDK"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 1106
    return-void

    .line 24
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/infraware/office/evengine/EvInterface;Ljava/lang/String;)V
    .locals 2
    .param p1, "a_interface"    # Lcom/infraware/office/evengine/EvInterface;
    .param p2, "appDir"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const-string/jumbo v0, "EvNative"

    iput-object v0, p0, Lcom/infraware/office/evengine/EvNative;->LOG_CAT:Ljava/lang/String;

    .line 27
    iput-object v1, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 28
    iput-object v1, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    .line 29
    iput-object v1, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    .line 30
    iput-object v1, p0, Lcom/infraware/office/evengine/EvNative;->mPrintListener:Lcom/infraware/office/evengine/EvListener$PrintListener;

    .line 31
    iput-object v1, p0, Lcom/infraware/office/evengine/EvNative;->mEvWordListener:Lcom/infraware/office/evengine/EvListener$WordEditorListener;

    .line 32
    iput-object v1, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    .line 33
    iput-object v1, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    .line 35
    iput-object v1, p0, Lcom/infraware/office/evengine/EvNative;->mEvPdfViewerListener:Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

    .line 36
    iput-object v1, p0, Lcom/infraware/office/evengine/EvNative;->mEvPdfAnnotListener:Lcom/infraware/office/evengine/EvListener$PdfAnnotListener;

    .line 37
    iput-object v1, p0, Lcom/infraware/office/evengine/EvNative;->mEvCodeConversion:Lcom/infraware/office/evengine/EvCodeConversion;

    .line 38
    iput-object v1, p0, Lcom/infraware/office/evengine/EvNative;->mEvImageUtil:Lcom/infraware/office/evengine/EvImageUtil;

    .line 39
    iput-object v1, p0, Lcom/infraware/office/evengine/EvNative;->mSystemFontFilePaths:[Ljava/lang/String;

    .line 101
    sget-boolean v0, Lcom/infraware/office/evengine/EvNative;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 103
    :cond_0
    iput-object p1, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 104
    invoke-static {}, Lcom/infraware/office/evengine/EvCodeConversion;->getCodeConversion()Lcom/infraware/office/evengine/EvCodeConversion;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvCodeConversion:Lcom/infraware/office/evengine/EvCodeConversion;

    .line 105
    invoke-static {}, Lcom/infraware/office/evengine/EvImageUtil;->getEvImageUtil()Lcom/infraware/office/evengine/EvImageUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvImageUtil:Lcom/infraware/office/evengine/EvImageUtil;

    .line 106
    invoke-direct {p0}, Lcom/infraware/office/evengine/EvNative;->MakeSystemFontFileNames()V

    .line 107
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvCodeConversion:Lcom/infraware/office/evengine/EvCodeConversion;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvNative;->mEvImageUtil:Lcom/infraware/office/evengine/EvImageUtil;

    invoke-direct {p0, v0, v1, p2}, Lcom/infraware/office/evengine/EvNative;->IBeginNative(Lcom/infraware/office/evengine/EvCodeConversion;Lcom/infraware/office/evengine/EvImageUtil;Ljava/lang/String;)I

    .line 108
    return-void
.end method

.method private GetBitmap(IIZ)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "notuse"    # Z

    .prologue
    .line 745
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1, p2}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->GetBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 747
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private GetChartThumbnailBitmap(III)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "index"    # I
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    .line 735
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->GetChartThumbnailBitmap(III)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private GetPageThumbnailBitmap(III)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "nPageNum"    # I
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    .line 732
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->GetPageThumbnailBitmap(III)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private GetPrintBitmap(II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 772
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1, p2}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->GetPrintBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 774
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private GetThumbnailBitmap(III)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "nPageNum"    # I
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    .line 730
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->GetThumbnailBitmap(III)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private native IBeginNative(Lcom/infraware/office/evengine/EvCodeConversion;Lcom/infraware/office/evengine/EvImageUtil;Ljava/lang/String;)I
.end method

.method private MakeSystemFontFileNames()V
    .locals 13

    .prologue
    .line 43
    const/16 v10, 0xf

    new-array v9, v10, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string/jumbo v11, "arial"

    aput-object v11, v9, v10

    const/4 v10, 0x1

    const-string/jumbo v11, "HanumanNew"

    aput-object v11, v9, v10

    const/4 v10, 0x2

    const-string/jumbo v11, "udgothic-regular"

    aput-object v11, v9, v10

    const/4 v10, 0x3

    const-string/jumbo v11, "udgothic-italic"

    aput-object v11, v9, v10

    const/4 v10, 0x4

    const-string/jumbo v11, "nanum"

    aput-object v11, v9, v10

    const/4 v10, 0x5

    const-string/jumbo v11, "droids"

    aput-object v11, v9, v10

    const/4 v10, 0x6

    const-string/jumbo v11, "droidnask"

    aput-object v11, v9, v10

    const/4 v10, 0x7

    const-string/jumbo v11, "samsung"

    aput-object v11, v9, v10

    const/16 v10, 0x8

    const-string/jumbo v11, "lohit"

    aput-object v11, v9, v10

    const/16 v10, 0x9

    const-string/jumbo v11, "thai"

    aput-object v11, v9, v10

    const/16 v10, 0xa

    const-string/jumbo v11, "gp"

    aput-object v11, v9, v10

    const/16 v10, 0xb

    const-string/jumbo v11, "arab"

    aput-object v11, v9, v10

    const/16 v10, 0xc

    const-string/jumbo v11, "emoji"

    aput-object v11, v9, v10

    const/16 v10, 0xd

    const-string/jumbo v11, "Chococooky"

    aput-object v11, v9, v10

    const/16 v10, 0xe

    const-string/jumbo v11, "noto"

    aput-object v11, v9, v10

    .line 44
    .local v9, "useString":[Ljava/lang/String;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 45
    .local v6, "fontPaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v10, Ljava/io/File;

    const-string/jumbo v11, "/system/fonts/"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 46
    .local v3, "fontFiles":[Ljava/io/File;
    array-length v2, v3

    .line 72
    .local v2, "fontCount":I
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_0
    array-length v10, v9

    if-ge v8, v10, :cond_3

    .line 74
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    if-ge v7, v2, :cond_2

    .line 75
    aget-object v10, v3, v7

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    .line 78
    .local v4, "fontName":Ljava/lang/String;
    const-string/jumbo v10, "bold"

    invoke-virtual {v4, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    const-string/jumbo v10, "lindseyforsamsung-regular"

    invoke-virtual {v4, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 74
    :cond_0
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 81
    :cond_1
    aget-object v10, v3, v7

    invoke-virtual {v10}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 83
    .local v0, "fileSize":J
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "(?i).*"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    aget-object v11, v9, v8

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, ".*"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 84
    aget-object v10, v3, v7

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_0

    .line 85
    aget-object v10, v3, v7

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v6, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 72
    .end local v0    # "fileSize":J
    .end local v4    # "fontName":Ljava/lang/String;
    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 91
    .end local v7    # "i":I
    :cond_3
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v5

    .line 92
    .local v5, "fontNameSize":I
    new-array v10, v5, [Ljava/lang/String;

    iput-object v10, p0, Lcom/infraware/office/evengine/EvNative;->mSystemFontFilePaths:[Ljava/lang/String;

    .line 93
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_3
    if-ge v7, v5, :cond_4

    .line 94
    iget-object v11, p0, Lcom/infraware/office/evengine/EvNative;->mSystemFontFilePaths:[Ljava/lang/String;

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    aput-object v10, v11, v7

    .line 95
    const-string/jumbo v10, "FontFileNames"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "mSystemFontFilePaths["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "] = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/infraware/office/evengine/EvNative;->mSystemFontFilePaths:[Ljava/lang/String;

    aget-object v12, v12, v7

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 97
    :cond_4
    return-void
.end method

.method private OnBFinalDrawBitmap(I)V
    .locals 1
    .param p1, "nReserved"    # I

    .prologue
    .line 789
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    if-lez p1, :cond_0

    .line 790
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnBFinalDrawBitmap(I)V

    .line 791
    :cond_0
    return-void
.end method

.method private OnBookMarkEditorMode()V
    .locals 1

    .prologue
    .line 819
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$EditorListener;->OnBookMarkEditorMode()V

    :cond_0
    return-void
.end method

.method private OnCloseDoc()V
    .locals 1

    .prologue
    .line 716
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    .line 717
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnCloseDoc()V

    :cond_0
    return-void
.end method

.method private OnCoreNotify(I)V
    .locals 1
    .param p1, "nNotifyCode"    # I

    .prologue
    .line 836
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$EditorListener;->OnCoreNotify(I)V

    :cond_0
    return-void
.end method

.method private OnCoreNotify2(II)V
    .locals 1
    .param p1, "nNotifyCode2"    # I
    .param p2, "nData"    # I

    .prologue
    .line 837
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    invoke-interface {v0, p1, p2}, Lcom/infraware/office/evengine/EvListener$EditorListener;->OnCoreNotify2(II)V

    :cond_0
    return-void
.end method

.method private OnDrawBitmap(III)V
    .locals 3
    .param p1, "nCallId"    # I
    .param p2, "bShowAutomap"    # I
    .param p3, "nCurrentMode"    # I

    .prologue
    const v2, 0xffff

    .line 752
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->EV()Lcom/infraware/office/evengine/EV;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/infraware/office/evengine/EvNative;->IGetConfig(Lcom/infraware/office/evengine/EV$CONFIG_INFO;)V

    .line 753
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->EV()Lcom/infraware/office/evengine/EV;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getScreenInfo()Lcom/infraware/office/evengine/EV$SCREEN_INFO;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/infraware/office/evengine/EvNative;->IGetScreenPos(Lcom/infraware/office/evengine/EV$SCREEN_INFO;)V

    .line 755
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->EV()Lcom/infraware/office/evengine/EV;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getScrollInfoEditor()Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/infraware/office/evengine/EvNative;->IGetScrollInfo_Editor(Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;)V

    .line 756
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->EV()Lcom/infraware/office/evengine/EV;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getCaretInfoEvent()Lcom/infraware/office/evengine/EV$CARET_INFO;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/infraware/office/evengine/EvNative;->IGetCaretInfo(Lcom/infraware/office/evengine/EV$CARET_INFO;)V

    .line 758
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    if-eqz v0, :cond_0

    .line 759
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, p3}, Lcom/infraware/office/evengine/EvInterface;->ISetEditorMode_Editor(I)V

    .line 762
    :cond_0
    if-ne p1, v2, :cond_1

    .line 763
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->LOG_CAT:Ljava/lang/String;

    const-string/jumbo v1, "CallId == eEV_GUI_MAX_EVENT"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_2

    if-eq p1, v2, :cond_2

    .line 766
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1, p2}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnDrawBitmap(II)V

    .line 767
    :cond_2
    return-void
.end method

.method private OnDrawGetChartThumbnail(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 736
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnDrawGetChartThumbnail(I)V

    :cond_0
    return-void
.end method

.method private OnDrawGetPageThumbnail(I)V
    .locals 1
    .param p1, "nPageNum"    # I

    .prologue
    .line 733
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnDrawGetPageThumbnail(I)V

    :cond_0
    return-void
.end method

.method private OnDrawPreviewBitmap()V
    .locals 0

    .prologue
    .line 1069
    return-void
.end method

.method private OnDrawThumbnailBitmap(I)V
    .locals 1
    .param p1, "nPageNum"    # I

    .prologue
    .line 731
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnDrawThumbnailBitmap(I)V

    :cond_0
    return-void
.end method

.method private OnEditCopyCut(IIILjava/lang/String;Ljava/lang/String;I)V
    .locals 7
    .param p1, "caller"    # I
    .param p2, "nType"    # I
    .param p3, "result"    # I
    .param p4, "text"    # Ljava/lang/String;
    .param p5, "data"    # Ljava/lang/String;
    .param p6, "nMode"    # I

    .prologue
    .line 852
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPdfViewerListener:Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

    if-eqz v0, :cond_1

    .line 853
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPdfViewerListener:Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

    move v1, p1

    move v2, p2

    move-object v3, p4

    move-object v4, p5

    move v5, p6

    invoke-interface/range {v0 .. v5}, Lcom/infraware/office/evengine/EvListener$PdfViewerListener;->OnEditCopyCut(IILjava/lang/String;Ljava/lang/String;I)V

    .line 856
    :cond_0
    :goto_0
    return-void

    .line 854
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    .line 855
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnEditCopyCut(IIILjava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private OnEditOrViewMode(II)V
    .locals 1
    .param p1, "bEditMode"    # I
    .param p2, "EV_EDITMODETYPE"    # I

    .prologue
    .line 820
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    invoke-interface {v0, p1, p2}, Lcom/infraware/office/evengine/EvListener$EditorListener;->OnEditOrViewMode(II)V

    :cond_0
    return-void
.end method

.method private OnEventMacroInitComplete()V
    .locals 2

    .prologue
    .line 152
    const-string/jumbo v0, "EventMacro"

    const-string/jumbo v1, "OnEventMacroInitComplete()"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    return-void
.end method

.method private OnEventMacroRunEvent(ILjava/lang/String;)I
    .locals 1
    .param p1, "nType"    # I
    .param p2, "sReserved"    # Ljava/lang/String;

    .prologue
    .line 155
    const/4 v0, 0x1

    return v0
.end method

.method private OnEventMacroRunFinish()V
    .locals 0

    .prologue
    .line 158
    return-void
.end method

.method private OnExitPreviewMode(I)V
    .locals 0
    .param p1, "a_nError"    # I

    .prologue
    .line 1074
    return-void
.end method

.method private OnFinalizeComplete()V
    .locals 1

    .prologue
    .line 704
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->OnFinalizeComplete()V

    return-void
.end method

.method private OnFlick(I)V
    .locals 1
    .param p1, "nRetData"    # I

    .prologue
    .line 843
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$EditorListener;->OnFlick(I)V

    :cond_0
    return-void
.end method

.method private OnGetDeviceInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 802
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    return-object v0
.end method

.method private OnGetFormulaFieldSelection()[I
    .locals 1

    .prologue
    .line 901
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    .line 902
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnGetFormulaFieldSelection()[I

    move-result-object v0

    .line 903
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private OnGetFormulaFieldText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 890
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    .line 891
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnGetFormulaFieldText()Ljava/lang/String;

    move-result-object v0

    .line 892
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private OnGetPreviewBitmap(II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 1068
    const/4 v0, 0x0

    return-object v0
.end method

.method private OnGetResStringID(I)Ljava/lang/String;
    .locals 1
    .param p1, "nStrID"    # I

    .prologue
    .line 795
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    .line 796
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnGetResID(I)Ljava/lang/String;

    move-result-object v0

    .line 798
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method private OnGetRulerbarBitmap(II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 994
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvWordListener:Lcom/infraware/office/evengine/EvListener$WordEditorListener;

    if-eqz v0, :cond_0

    .line 995
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvWordListener:Lcom/infraware/office/evengine/EvListener$WordEditorListener;

    invoke-interface {v0, p1, p2}, Lcom/infraware/office/evengine/EvListener$WordEditorListener;->OnGetRulerbarBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 997
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private OnGetSheetScrollIInfo()[I
    .locals 1

    .prologue
    .line 915
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    .line 916
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnGetSheetScrollIInfo()[I

    move-result-object v0

    .line 917
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private OnGetString(I)Ljava/lang/String;
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 1078
    packed-switch p1, :pswitch_data_0

    .line 1086
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1081
    :pswitch_0
    sget-object v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mClipboardRootPath:Ljava/lang/String;

    goto :goto_0

    .line 1078
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private OnGetSystemDate(III)Ljava/lang/String;
    .locals 1
    .param p1, "nYear"    # I
    .param p2, "nMonth"    # I
    .param p3, "nDay"    # I

    .prologue
    .line 881
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    .line 882
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnGetSystemDate(III)Ljava/lang/String;

    move-result-object v0

    .line 883
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private OnGetSystemTime(IID)Ljava/lang/String;
    .locals 1
    .param p1, "nHour"    # I
    .param p2, "nMinute"    # I
    .param p3, "nSecond"    # D

    .prologue
    .line 876
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    .line 877
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnGetSystemTime(IID)Ljava/lang/String;

    move-result-object v0

    .line 878
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private OnHidAction(I)V
    .locals 1
    .param p1, "EEV_HID_ACTION"    # I

    .prologue
    .line 832
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$EditorListener;->OnHidAction(I)V

    :cond_0
    return-void
.end method

.method private OnIMEInsertMode()V
    .locals 1

    .prologue
    .line 834
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$EditorListener;->OnIMEInsertMode()V

    :cond_0
    return-void
.end method

.method private OnInitComplete(I)V
    .locals 1
    .param p1, "EEV_ERROR_CODE"    # I

    .prologue
    .line 702
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvInterface;->OnInitComplete(I)V

    return-void
.end method

.method private OnInsertFreeformShapes()V
    .locals 1

    .prologue
    .line 842
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$EditorListener;->OnInsertFreeformShapes()V

    :cond_0
    return-void
.end method

.method private OnInsertTableMode()V
    .locals 1

    .prologue
    .line 833
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$EditorListener;->OnInsertTableMode()V

    :cond_0
    return-void
.end method

.method private OnLoadComplete(I)V
    .locals 1
    .param p1, "bBookmarkExist"    # I

    .prologue
    .line 706
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->EV()Lcom/infraware/office/evengine/EV;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/infraware/office/evengine/EvNative;->IGetConfig(Lcom/infraware/office/evengine/EV$CONFIG_INFO;)V

    .line 707
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnLoadComplete(I)V

    .line 708
    :cond_0
    return-void
.end method

.method private OnLoadFail(I)V
    .locals 1
    .param p1, "EEV_ERROR_CODE"    # I

    .prologue
    .line 713
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    .line 714
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnLoadFail(I)V

    :cond_0
    return-void
.end method

.method private OnNewDoc(I)V
    .locals 1
    .param p1, "bOk"    # I

    .prologue
    .line 822
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->EV()Lcom/infraware/office/evengine/EV;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/infraware/office/evengine/EvNative;->IGetConfig(Lcom/infraware/office/evengine/EV$CONFIG_INFO;)V

    .line 823
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$EditorListener;->OnNewDoc(I)V

    .line 824
    :cond_0
    return-void
.end method

.method private OnOLEFormatInfo(Ljava/lang/String;)V
    .locals 1
    .param p1, "nWideString"    # Ljava/lang/String;

    .prologue
    .line 844
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$EditorListener;->OnOLEFormatInfo(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private OnObjectPoints([I)V
    .locals 2
    .param p1, "param"    # [I

    .prologue
    .line 826
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    if-eqz v0, :cond_0

    .line 827
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, v1, Lcom/infraware/office/evengine/EvInterface;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getEditorObjectPointArray()Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->setValue([I)Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/infraware/office/evengine/EvListener$EditorListener;->OnObjectPoints(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V

    .line 828
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_1

    .line 829
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, v1, Lcom/infraware/office/evengine/EvInterface;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getEditorObjectPointArray()Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->setValue([I)Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnObjectPoints(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V

    .line 831
    :cond_1
    return-void
.end method

.method private OnPDFAnnotationCount(I)I
    .locals 1
    .param p1, "nAnnotCnt"    # I

    .prologue
    .line 1058
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPdfAnnotListener:Lcom/infraware/office/evengine/EvListener$PdfAnnotListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$PdfAnnotListener;->OnPDFAnnotationCount(I)I

    move-result v0

    return v0
.end method

.method private OnPDFHyperLink(Ljava/lang/String;IIII)V
    .locals 6
    .param p1, "strString"    # Ljava/lang/String;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 1053
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPdfViewerListener:Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/infraware/office/evengine/EvListener$PdfViewerListener;->OnPDFHyperLink(Ljava/lang/String;IIII)V

    .line 1054
    return-void
.end method

.method private OnPDFIdleStatus()V
    .locals 0

    .prologue
    .line 1025
    return-void
.end method

.method private OnPDFPageRendered(I)V
    .locals 1
    .param p1, "nPageNum"    # I

    .prologue
    .line 1064
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPdfViewerListener:Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$PdfViewerListener;->OnPDFPageRendered(I)V

    .line 1065
    return-void
.end method

.method private OnPDFPenComplete()V
    .locals 0

    .prologue
    .line 1039
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EvNative;->ICreatePDFPendraw()V

    .line 1040
    return-void
.end method

.method private OnPDFScreenOffset(II)V
    .locals 1
    .param p1, "offsetX"    # I
    .param p2, "offsetY"    # I

    .prologue
    .line 1033
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPdfViewerListener:Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

    invoke-interface {v0, p1, p2}, Lcom/infraware/office/evengine/EvListener$PdfViewerListener;->OnPDFScreenOffset(II)V

    .line 1034
    return-void
.end method

.method private OnPDFSelectAnnots(IIII)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 1028
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPdfViewerListener:Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/infraware/office/evengine/EvListener$PdfViewerListener;->OnSelectAnnots(IIII)V

    .line 1029
    return-void
.end method

.method private OnPDFSingleTap(II)V
    .locals 1
    .param p1, "posX"    # I
    .param p2, "posY"    # I

    .prologue
    .line 1020
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPdfViewerListener:Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

    invoke-interface {v0, p1, p2}, Lcom/infraware/office/evengine/EvListener$PdfViewerListener;->OnSingleTap(II)V

    .line 1021
    return-void
.end method

.method private OnPageMove(III)V
    .locals 1
    .param p1, "nCurrentPage"    # I
    .param p2, "nTotalPage"    # I
    .param p3, "nErrorCode"    # I

    .prologue
    .line 719
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->EV()Lcom/infraware/office/evengine/EV;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/infraware/office/evengine/EvNative;->IGetConfig(Lcom/infraware/office/evengine/EV$CONFIG_INFO;)V

    .line 720
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnPageMove(III)V

    .line 721
    :cond_0
    return-void
.end method

.method private OnPaperLayoutMode()V
    .locals 1

    .prologue
    .line 985
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvWordListener:Lcom/infraware/office/evengine/EvListener$WordEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvWordListener:Lcom/infraware/office/evengine/EvListener$WordEditorListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$WordEditorListener;->OnPaperLayoutMode()V

    :cond_0
    return-void
.end method

.method private OnPermission(Ljava/lang/String;II)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "mode"    # I
    .param p3, "bRecursive"    # I

    .prologue
    .line 1090
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1092
    .local v0, "dir":Ljava/io/File;
    if-nez p3, :cond_0

    const/4 v2, 0x0

    :goto_0
    :try_start_0
    invoke-static {v0, p2, v2}, Lcom/infraware/common/util/FileUtils;->changePermissons(Ljava/io/File;IZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1098
    const/4 v0, 0x0

    .line 1100
    :goto_1
    return-void

    .line 1092
    :cond_0
    const/4 v2, 0x1

    goto :goto_0

    .line 1093
    :catch_0
    move-exception v1

    .line 1095
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1098
    const/4 v0, 0x0

    .line 1099
    goto :goto_1

    .line 1098
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    const/4 v0, 0x0

    throw v2
.end method

.method private OnPptDrawSlidesBitmap(I)V
    .locals 1
    .param p1, "pageNum"    # I

    .prologue
    .line 982
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$PptEditorListener;->OnPptDrawSlidesBitmap(I)V

    :cond_0
    return-void
.end method

.method private OnPptGetSlidenoteBitmap(II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 971
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    invoke-interface {v0, p1, p2}, Lcom/infraware/office/evengine/EvListener$PptEditorListener;->OnPptGetSlidenoteBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private OnPptGetSlidesBitmap(IIIIZLjava/lang/String;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "bBitmapIamage"    # I
    .param p2, "nPageNum"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I
    .param p5, "isHideSlide"    # Z
    .param p6, "strSlideTitle"    # Ljava/lang/String;

    .prologue
    .line 970
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/evengine/EvListener$PptEditorListener;->OnPptGetSlidesBitmap(IIIIZLjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private OnPptInsertchart(I)V
    .locals 0
    .param p1, "PPT_CHART_STAUTS"    # I

    .prologue
    .line 968
    return-void
.end method

.method private OnPptOnDrawSlidenote(I)V
    .locals 1
    .param p1, "pageNum"    # I

    .prologue
    .line 972
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$PptEditorListener;->OnPptOnDrawSlidenote(I)V

    :cond_0
    return-void
.end method

.method private OnPptSlideDelete()V
    .locals 1

    .prologue
    .line 977
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$PptEditorListener;->OnPptSlideDelete()V

    :cond_0
    return-void
.end method

.method private OnPptSlideMoveNext()V
    .locals 1

    .prologue
    .line 976
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$PptEditorListener;->OnPptSlideMoveNext()V

    :cond_0
    return-void
.end method

.method private OnPptSlideMovePrev()V
    .locals 1

    .prologue
    .line 975
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$PptEditorListener;->OnPptSlideMovePrev()V

    :cond_0
    return-void
.end method

.method private OnPptSlideShowDrawBitmap()V
    .locals 1

    .prologue
    .line 979
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$PptEditorListener;->OnPptSlideShowDrawBitmap()V

    :cond_0
    return-void
.end method

.method private OnPptSlideShowEffectEnd(I)V
    .locals 1
    .param p1, "nSubType"    # I

    .prologue
    .line 981
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$PptEditorListener;->OnPptSlideShowEffectEnd(I)V

    :cond_0
    return-void
.end method

.method private OnPptSlideShowGetBitmap(II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 978
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    invoke-interface {v0, p1, p2}, Lcom/infraware/office/evengine/EvListener$PptEditorListener;->OnPptSlideShowGetBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private OnPptSlideexInsert()V
    .locals 1

    .prologue
    .line 974
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$PptEditorListener;->OnPptSlideexInsert()V

    :cond_0
    return-void
.end method

.method private OnPptSlidenoteStart()V
    .locals 1

    .prologue
    .line 973
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$PptEditorListener;->OnPptSlidenoteStart()V

    :cond_0
    return-void
.end method

.method private OnPreviewTimerStart()V
    .locals 0

    .prologue
    .line 1070
    return-void
.end method

.method private OnPreviewTimerStop()V
    .locals 0

    .prologue
    .line 1071
    return-void
.end method

.method private OnPrintCompleted(I)V
    .locals 2
    .param p1, "zoomScale"    # I

    .prologue
    .line 780
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->LOG_CAT:Ljava/lang/String;

    const-string/jumbo v1, "print completed"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 783
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    .line 784
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnPrintCompleted(I)V

    .line 785
    :cond_0
    return-void
.end method

.method private OnPrintMode(Ljava/lang/String;)V
    .locals 1
    .param p1, "strPrintFile"    # Ljava/lang/String;

    .prologue
    .line 738
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnPrintMode(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private OnPrintedCount(I)V
    .locals 1
    .param p1, "nTotalCount"    # I

    .prologue
    .line 739
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnPrintedCount(I)V

    :cond_0
    return-void
.end method

.method private OnProgress(II)V
    .locals 1
    .param p1, "EV_PROGRESS_TYPE"    # I
    .param p2, "nProgressValue"    # I

    .prologue
    .line 725
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1, p2}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnProgress(II)V

    :cond_0
    return-void
.end method

.method private OnProgressStart(I)V
    .locals 1
    .param p1, "EV_PROGRESS_TYPE"    # I

    .prologue
    .line 723
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnProgressStart(I)V

    :cond_0
    return-void
.end method

.method private OnRefNote(I)V
    .locals 1
    .param p1, "nRefType"    # I

    .prologue
    .line 991
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvWordListener:Lcom/infraware/office/evengine/EvListener$WordEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvWordListener:Lcom/infraware/office/evengine/EvListener$WordEditorListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$WordEditorListener;->onRefNote(I)V

    :cond_0
    return-void
.end method

.method private OnRenewBookmarkList()V
    .locals 0

    .prologue
    .line 1072
    return-void
.end method

.method private OnSaveDoc(I)V
    .locals 1
    .param p1, "bOk"    # I

    .prologue
    .line 812
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    if-eqz v0, :cond_2

    .line 813
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$EditorListener;->OnSaveDoc(I)V

    .line 816
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mPrintListener:Lcom/infraware/office/evengine/EvListener$PrintListener;

    if-eqz v0, :cond_1

    .line 817
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mPrintListener:Lcom/infraware/office/evengine/EvListener$PrintListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$PrintListener;->OnSaveDoc(I)V

    .line 818
    :cond_1
    return-void

    .line 814
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPdfViewerListener:Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

    if-eqz v0, :cond_0

    .line 815
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPdfViewerListener:Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$PdfViewerListener;->OnSaveDoc(I)V

    goto :goto_0
.end method

.method private OnSearchMode(IIII)V
    .locals 1
    .param p1, "EEV_SEARCH_TYPE"    # I
    .param p2, "nCurrentPage"    # I
    .param p3, "nTotalPage"    # I
    .param p4, "nReplaceAllCount"    # I

    .prologue
    .line 729
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnSearchMode(IIII)V

    :cond_0
    return-void
.end method

.method private OnSetAnnotation(IIIIIFFFFLjava/lang/String;IIII)V
    .locals 14
    .param p1, "pos"    # I
    .param p2, "nPageNum"    # I
    .param p3, "nIndex"    # I
    .param p4, "nType"    # I
    .param p5, "nStyle"    # I
    .param p6, "left"    # F
    .param p7, "top"    # F
    .param p8, "right"    # F
    .param p9, "bottom"    # F
    .param p10, "strString"    # Ljava/lang/String;
    .param p11, "AnnotItem"    # I
    .param p12, "color"    # I
    .param p13, "Thickness"    # I
    .param p14, "fillColor"    # I

    .prologue
    .line 1005
    if-nez p1, :cond_1

    .line 1006
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPdfViewerListener:Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

    move/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    move/from16 v4, p5

    move/from16 v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    move/from16 v8, p9

    move-object/from16 v9, p10

    move/from16 v10, p11

    move/from16 v11, p12

    move/from16 v12, p13

    move/from16 v13, p14

    invoke-interface/range {v0 .. v13}, Lcom/infraware/office/evengine/EvListener$PdfViewerListener;->OnSetPrevAnnot(IIIIFFFFLjava/lang/String;IIII)V

    .line 1016
    :cond_0
    :goto_0
    return-void

    .line 1008
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 1009
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPdfViewerListener:Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

    move/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    move/from16 v4, p5

    move/from16 v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    move/from16 v8, p9

    move-object/from16 v9, p10

    move/from16 v10, p11

    move/from16 v11, p12

    move/from16 v12, p13

    move/from16 v13, p14

    invoke-interface/range {v0 .. v13}, Lcom/infraware/office/evengine/EvListener$PdfViewerListener;->OnSetCurrAnnot(IIIIFFFFLjava/lang/String;IIII)V

    goto :goto_0

    .line 1011
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 1012
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPdfViewerListener:Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

    move/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    move/from16 v4, p5

    move/from16 v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    move/from16 v8, p9

    move-object/from16 v9, p10

    move/from16 v10, p11

    move/from16 v11, p12

    move/from16 v12, p13

    move/from16 v13, p14

    invoke-interface/range {v0 .. v13}, Lcom/infraware/office/evengine/EvListener$PdfViewerListener;->OnSetNextAnnot(IIIIFFFFLjava/lang/String;IIII)V

    .line 1013
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPdfViewerListener:Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$PdfViewerListener;->OnReceiveAnnot()V

    goto :goto_0
.end method

.method private OnSetDataRange(IIII)V
    .locals 1
    .param p1, "row1"    # I
    .param p2, "col1"    # I
    .param p3, "row2"    # I
    .param p4, "col2"    # I

    .prologue
    .line 886
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    .line 887
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnSetDataRange(IIII)V

    .line 888
    :cond_0
    return-void
.end method

.method private OnSetFormulaFieldSelection(II)V
    .locals 1
    .param p1, "nStartPos"    # I
    .param p2, "nEndPos"    # I

    .prologue
    .line 907
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    .line 908
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0, p1, p2}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnSetFormulaFieldSelection(II)V

    .line 909
    :cond_0
    return-void
.end method

.method private OnSetFormulaFieldText(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 896
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    .line 897
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnSetFormulaFieldText(Ljava/lang/String;)V

    .line 898
    :cond_0
    return-void
.end method

.method private OnSetFormulaSelectionEnabled(I)V
    .locals 1
    .param p1, "nEnabled"    # I

    .prologue
    .line 911
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    .line 912
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnSetFormulaSelectionEnabled(I)V

    .line 913
    :cond_0
    return-void
.end method

.method private OnSetNameBoxText(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 871
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    .line 872
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnSetNameBoxText(Ljava/lang/String;)V

    .line 873
    :cond_0
    return-void
.end method

.method private OnSetSheetScrollIInfo(IIIII)V
    .locals 6
    .param p1, "nValue"    # I
    .param p2, "nMin"    # I
    .param p3, "nMax"    # I
    .param p4, "nSize"    # I
    .param p5, "bHorizontal"    # I

    .prologue
    .line 921
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    .line 922
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnSetSheetScrollIInfo(IIIII)V

    .line 923
    :cond_0
    return-void
.end method

.method private OnSheetAutoFilterContext([I[I[I[I)V
    .locals 1
    .param p1, "a_aFilterRange"    # [I
    .param p2, "a_aIndexRange"    # [I
    .param p3, "a_aDataRange"    # [I
    .param p4, "a_aStartRange"    # [I

    .prologue
    .line 926
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnSheetAutoFilterContext([I[I[I[I)V

    .line 927
    :cond_0
    return-void
.end method

.method private OnSheetAutoFilterIndexCellRect([I)V
    .locals 1
    .param p1, "a_aRect"    # [I

    .prologue
    .line 943
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnSheetAutoFilterIndexCellRect([I)V

    .line 944
    :cond_0
    return-void
.end method

.method private OnSheetAutoFilterMenu(II[Ljava/lang/String;[Z[ZI[I)V
    .locals 8
    .param p1, "nHandleId"    # I
    .param p2, "nFocusedIndex"    # I
    .param p3, "filterObjArr"    # [Ljava/lang/String;
    .param p4, "nFixedItem"    # [Z
    .param p5, "nCheckedItem"    # [Z
    .param p6, "nCount"    # I
    .param p7, "nCellPos"    # [I

    .prologue
    .line 934
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    move-object v7, p7

    invoke-interface/range {v0 .. v7}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnSheetAutoFilterMenu(II[Ljava/lang/String;[Z[ZI[I)V

    .line 935
    :cond_0
    return-void
.end method

.method private OnSheetAutoFilterMenuEx(Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;)V
    .locals 1
    .param p1, "autofilterMenuInfo"    # Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;

    .prologue
    .line 939
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnSheetAutoFilterMenuEx(Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;)V

    .line 940
    :cond_0
    return-void
.end method

.method private OnSheetAutoFilterStartStateCallBack(I)V
    .locals 1
    .param p1, "nStart"    # I

    .prologue
    .line 949
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnSheetAutoFilterStartStateCallBack(I)V

    .line 950
    :cond_0
    return-void
.end method

.method private OnSheetAutoFilterStatusChanged(I)V
    .locals 1
    .param p1, "nAutoFilterResult"    # I

    .prologue
    .line 930
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnSheetAutoFilterStatusChanged(I)V

    .line 931
    :cond_0
    return-void
.end method

.method private OnSheetChart(I)V
    .locals 1
    .param p1, "EEV_SHEET_EDITOR_STATUS"    # I

    .prologue
    .line 867
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnSheetChart(I)V

    :cond_0
    return-void
.end method

.method private OnSheetCircularReferenceWarning()V
    .locals 1

    .prologue
    .line 964
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnSheetCircularReferenceWarning()V

    .line 965
    :cond_0
    return-void
.end method

.method private OnSheetDynamicLoading(I)V
    .locals 1
    .param p1, "a_aProgress"    # I

    .prologue
    .line 960
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnSheetDynamicLoading(I)V

    .line 961
    :cond_0
    return-void
.end method

.method private OnSheetEdit(II)V
    .locals 1
    .param p1, "EEV_SHEET_EDIT"    # I
    .param p2, "EEV_SHEET_EDITOR_RESULT"    # I

    .prologue
    .line 861
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0, p1, p2}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnSheetEdit(II)V

    :cond_0
    return-void
.end method

.method private OnSheetEditBlock()V
    .locals 1

    .prologue
    .line 956
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnSheetEditBlock()V

    .line 957
    :cond_0
    return-void
.end method

.method private OnSheetFocus()V
    .locals 1

    .prologue
    .line 864
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnSheetFocus()V

    :cond_0
    return-void
.end method

.method private OnSheetFormulaRangeRect(I[I[I)V
    .locals 1
    .param p1, "a_nCount"    # I
    .param p2, "a_aRangeRect"    # [I
    .param p3, "a_aCurRect"    # [I

    .prologue
    .line 952
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnSheetFormulaRangeRect(I[I[I)V

    .line 953
    :cond_0
    return-void
.end method

.method private OnSheetFunction(III)V
    .locals 1
    .param p1, "EEV_SHEET_EDITOR_STATUS"    # I
    .param p2, "EEV_SHEET_FUNCTION_ERROR"    # I
    .param p3, "EEV_SHEET_FUNCTION_ERROR_CODE"    # I

    .prologue
    .line 860
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnSheetFunction(III)V

    :cond_0
    return-void
.end method

.method private OnSheetInputField(II)V
    .locals 1
    .param p1, "EEV_SHEET_EDITOR_RESULT"    # I
    .param p2, "EEV_SHEET_IPUTFIELD_RESULT"    # I

    .prologue
    .line 866
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0, p1, p2}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnSheetInputField(II)V

    :cond_0
    return-void
.end method

.method private OnSheetMemoNavigate(I)V
    .locals 1
    .param p1, "EEV_SHEET_EDITOR_STATUS"    # I

    .prologue
    .line 868
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnSheetMemoNavigate(I)V

    :cond_0
    return-void
.end method

.method private OnSheetPartialLoad(I)V
    .locals 1
    .param p1, "EEV_SHEET_LOAD_PAGE"    # I

    .prologue
    .line 859
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnSheetPartialLoad(I)V

    :cond_0
    return-void
.end method

.method private OnSheetPivotTableInDocument(ZZI)V
    .locals 1
    .param p1, "bExistPivotTableInDocument"    # Z
    .param p2, "bExistPivotTableInCurrentSheet"    # Z
    .param p3, "nSheetIndex"    # I

    .prologue
    .line 946
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnSheetPivotTableInDocument(ZZI)V

    .line 947
    :cond_0
    return-void
.end method

.method private OnSheetProtection(I)V
    .locals 1
    .param p1, "EEV_SHEET_EDITOR_RESULT"    # I

    .prologue
    .line 862
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnSheetProtection(I)V

    :cond_0
    return-void
.end method

.method private OnSheetSort(I)V
    .locals 1
    .param p1, "EEV_SHEET_EDITOR_RESULT"    # I

    .prologue
    .line 863
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$SheetEditorListener;->OnSheetSort(I)V

    :cond_0
    return-void
.end method

.method private OnSpellCheck(Ljava/lang/String;IIIIIII)V
    .locals 9
    .param p1, "pWordStr"    # Ljava/lang/String;
    .param p2, "nLen"    # I
    .param p3, "bClass"    # I
    .param p4, "nPageNum"    # I
    .param p5, "nObjectID"    # I
    .param p6, "nNoteNum"    # I
    .param p7, "nParaIndex"    # I
    .param p8, "nColIndex"    # I

    .prologue
    .line 840
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-interface/range {v0 .. v8}, Lcom/infraware/office/evengine/EvListener$EditorListener;->OnSpellCheck(Ljava/lang/String;IIIIIII)V

    .line 841
    :cond_0
    return-void
.end method

.method private OnTerminate()V
    .locals 1

    .prologue
    .line 727
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnTerminate()V

    :cond_0
    return-void
.end method

.method private OnTextToSpeachString(Ljava/lang/String;)V
    .locals 1
    .param p1, "strTTS"    # Ljava/lang/String;

    .prologue
    .line 741
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnTextToSpeachString(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private OnTimerStart()V
    .locals 1

    .prologue
    .line 806
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->OnTimerStart()V

    :cond_0
    return-void
.end method

.method private OnTimerStop()V
    .locals 1

    .prologue
    .line 807
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->OnTimerStop()V

    :cond_0
    return-void
.end method

.method private OnTotalLoadComplete()V
    .locals 1

    .prologue
    .line 710
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    .line 711
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnTotalLoadComplete()V

    :cond_0
    return-void
.end method

.method private OnUndoOrRedo(ZIII)V
    .locals 1
    .param p1, "bUndo"    # Z
    .param p2, "nAction"    # I
    .param p3, "nPage1"    # I
    .param p4, "nPage2"    # I

    .prologue
    .line 835
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/infraware/office/evengine/EvListener$EditorListener;->OnUndoOrRedo(ZIII)V

    :cond_0
    return-void
.end method

.method private OnUpdateAnnotation(IIIIILjava/lang/String;FFFFIII)V
    .locals 13
    .param p1, "nAction"    # I
    .param p2, "nPageNum"    # I
    .param p3, "nIndex"    # I
    .param p4, "nType"    # I
    .param p5, "nStyle"    # I
    .param p6, "strString"    # Ljava/lang/String;
    .param p7, "left"    # F
    .param p8, "top"    # F
    .param p9, "right"    # F
    .param p10, "bottom"    # F
    .param p11, "AnnotItem"    # I
    .param p12, "color"    # I
    .param p13, "Thickness"    # I

    .prologue
    .line 1044
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 1045
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPdfViewerListener:Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

    move v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    move/from16 v4, p5

    move-object/from16 v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    move/from16 v8, p9

    move/from16 v9, p10

    move/from16 v10, p11

    move/from16 v11, p12

    move/from16 v12, p13

    invoke-interface/range {v0 .. v12}, Lcom/infraware/office/evengine/EvListener$PdfViewerListener;->OnReceivePrevAnnot(IIIILjava/lang/String;FFFFIII)V

    .line 1048
    :cond_0
    :goto_0
    return-void

    .line 1046
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 1047
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPdfViewerListener:Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

    move v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    move/from16 v4, p5

    move-object/from16 v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    move/from16 v8, p9

    move/from16 v9, p10

    move/from16 v10, p11

    move/from16 v11, p12

    move/from16 v12, p13

    invoke-interface/range {v0 .. v12}, Lcom/infraware/office/evengine/EvListener$PdfViewerListener;->OnReceiveNextAnnot(IIIILjava/lang/String;FFFFIII)V

    goto :goto_0
.end method

.method private OnWordCellDeleteMode()V
    .locals 1

    .prologue
    .line 987
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvWordListener:Lcom/infraware/office/evengine/EvListener$WordEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvWordListener:Lcom/infraware/office/evengine/EvListener$WordEditorListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$WordEditorListener;->OnWordCellDeleteMode()V

    :cond_0
    return-void
.end method

.method private OnWordCellInsertMode()V
    .locals 1

    .prologue
    .line 986
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvWordListener:Lcom/infraware/office/evengine/EvListener$WordEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvWordListener:Lcom/infraware/office/evengine/EvListener$WordEditorListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$WordEditorListener;->OnWordCellInsertMode()V

    :cond_0
    return-void
.end method

.method private OnWordMemoViewMode(Ljava/lang/String;I)V
    .locals 1
    .param p1, "strMemo"    # Ljava/lang/String;
    .param p2, "nExistDirection"    # I

    .prologue
    .line 990
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvWordListener:Lcom/infraware/office/evengine/EvListener$WordEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvWordListener:Lcom/infraware/office/evengine/EvListener$WordEditorListener;

    invoke-interface {v0, p1, p2}, Lcom/infraware/office/evengine/EvListener$WordEditorListener;->OnWordMemoViewMode(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method private OnWordMultiSelectCellMode()V
    .locals 1

    .prologue
    .line 989
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvWordListener:Lcom/infraware/office/evengine/EvListener$WordEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvWordListener:Lcom/infraware/office/evengine/EvListener$WordEditorListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$WordEditorListener;->OnWordMultiSelectCellMode()V

    :cond_0
    return-void
.end method

.method private OnWordOneSelectCellMode()V
    .locals 1

    .prologue
    .line 988
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvWordListener:Lcom/infraware/office/evengine/EvListener$WordEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvWordListener:Lcom/infraware/office/evengine/EvListener$WordEditorListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$WordEditorListener;->OnWordOneSelectCellMode()V

    :cond_0
    return-void
.end method


# virtual methods
.method GetDvListener()Lcom/infraware/office/evengine/EvListener$ViewerListener;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    return-object v0
.end method

.method GetEvListener()Lcom/infraware/office/evengine/EvListener$EditorListener;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    return-object v0
.end method

.method GetEvPDFListener()Lcom/infraware/office/evengine/EvListener$PdfViewerListener;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPdfViewerListener:Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

    return-object v0
.end method

.method GetEvPListener()Lcom/infraware/office/evengine/EvListener$PptEditorListener;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    return-object v0
.end method

.method GetEvPdfAnnotListener()Lcom/infraware/office/evengine/EvListener$PdfAnnotListener;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvPdfAnnotListener:Lcom/infraware/office/evengine/EvListener$PdfAnnotListener;

    return-object v0
.end method

.method GetEvPrintListener()Lcom/infraware/office/evengine/EvListener$PrintListener;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mPrintListener:Lcom/infraware/office/evengine/EvListener$PrintListener;

    return-object v0
.end method

.method GetEvSListener()Lcom/infraware/office/evengine/EvListener$SheetEditorListener;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    return-object v0
.end method

.method GetEvWListener()Lcom/infraware/office/evengine/EvListener$WordEditorListener;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvWordListener:Lcom/infraware/office/evengine/EvListener$WordEditorListener;

    return-object v0
.end method

.method GetFontFileList()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 700
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mSystemFontFilePaths:[Ljava/lang/String;

    return-object v0
.end method

.method native IAnnotationShow(Z)V
.end method

.method native IApplyBookClip(Lcom/infraware/office/evengine/EV$BOOK_CLIP;)V
.end method

.method native IApplyBookMark()V
.end method

.method native IBookMarkOnOff(I)V
.end method

.method native IBookmarkEditor(ILjava/lang/String;)V
.end method

.method native IBulletNumbering(III)V
.end method

.method native ICanCellDelete()Z
.end method

.method native ICanExtendSortRange()I
.end method

.method native ICancel()V
.end method

.method native ICaretMark(II)V
.end method

.method native ICaretMove(II)V
.end method

.method native ICaretShow(I)V
.end method

.method native ICellEdit(II)V
.end method

.method native ICellEqualWidthHeight(I)V
.end method

.method native ICellInsertDelete(II)V
.end method

.method native ICellMergeSeparate(III)V
.end method

.method native IChaneCase(I)V
.end method

.method native IChangeCase(I)V
.end method

.method native IChangeDisplay(I)V
.end method

.method native IChangeScreen(IIIII)V
.end method

.method native IChangeViewMode(IIIIII)V
.end method

.method native ICharInput()V
.end method

.method native ICharInsert(III)V
.end method

.method native IChartAxesInfo(I[C[C[I[C[D[IC)V
.end method

.method native IChartChangeDataRange(Z)V
.end method

.method native IChartChangeDataRangeEnd()V
.end method

.method native IChartDataLabelInfo(IIIIII)V
.end method

.method native IChartFontInfo(Ljava/lang/String;F)V
.end method

.method native IChartStyleInfo(II)V
.end method

.method native IChartTitleInfo(IIZZZZ)V
.end method

.method native ICheckVideoObjInGroupShape()Z
.end method

.method native IClearFrameSet()V
.end method

.method native IClose()V
.end method

.method native ICreatePDFAnnotation(I)V
.end method

.method native ICreatePDFPendraw()V
.end method

.method native ICreatePDFStickyNote(II)V
.end method

.method native ICreateTable(IIII)V
.end method

.method native IDeleteBookClip(Lcom/infraware/office/evengine/EV$BOOK_CLIP;)V
.end method

.method native IDeleteInterfaceHandle(I)V
.end method

.method native IDeletePenDataForFreeDraw()V
.end method

.method native IDeletePenDataForSlideShow()V
.end method

.method native IDocumentModified()Z
.end method

.method native IEditDocument(IILjava/lang/String;)V
.end method

.method native IEditPageRedrawBitmap()V
.end method

.method native IExitPreview()V
.end method

.method native IExportPDF(Ljava/lang/String;I[I)V
.end method

.method native IFinalize()V
.end method

.method native IFindWordNext(I)V
.end method

.method native IFindWordNextByPos(II)V
.end method

.method native IFindWordStart(II)V
.end method

.method native IFindWordStop()V
.end method

.method native IFlick(II)V
.end method

.method native IGetAllChartInfo(Lcom/infraware/office/evengine/EV$GUI_SHEET_ALL_CHART_EVENT;)V
.end method

.method native IGetAnimationInfo(ILcom/infraware/office/evengine/EV$SLIDE_ANIMATION_INFO;)V
.end method

.method native IGetApplyCellCount()I
.end method

.method native IGetAutoResizeFlag()Z
.end method

.method native IGetBWPCellStatusInfo()I
.end method

.method native IGetBWPChartStyle()I
.end method

.method native IGetBWPGrapAttrInfo(Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;)V
.end method

.method native IGetBWPOpInfo(Lcom/infraware/office/evengine/EV$BWP_OP_INFO;)V
.end method

.method native IGetBWPProtectStatusInfo()I
.end method

.method native IGetBWPSplitCellMaxNum(Lcom/infraware/office/evengine/EV$BWP_SPLITCELL_MAXNUM;)V
.end method

.method native IGetBookClipCount(Lcom/infraware/office/evengine/EV$BOOK_CLIP;)I
.end method

.method native IGetBookClipName(ILcom/infraware/office/evengine/EV$BOOK_CLIP;)V
.end method

.method native IGetBookmarkCount_Editor()I
.end method

.method native IGetBookmarkInfo()[Ljava/lang/String;
.end method

.method native IGetBookmarkInfo_Editor(I)Ljava/lang/String;
.end method

.method native IGetBookmarkLabel(ILcom/infraware/office/evengine/EV$BOOKMARK_LABEL;)V
.end method

.method native IGetBorderProperty(Lcom/infraware/office/evengine/EV$GUI_BORDER_EVENT;)V
.end method

.method native IGetBulletType(Lcom/infraware/office/evengine/EV$BULLET_TYPE;)V
.end method

.method native IGetBwpChart(Lcom/infraware/office/evengine/EV$BWP_CHART;)Z
.end method

.method native IGetCaretAfterString(I)Ljava/lang/String;
.end method

.method native IGetCaretBeforeString(I)Ljava/lang/String;
.end method

.method native IGetCaretInfo(Lcom/infraware/office/evengine/EV$CARET_INFO;)V
.end method

.method native IGetCaretPos(Lcom/infraware/office/evengine/EV$CARET_POS;)V
.end method

.method native IGetCellInfo(Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;)V
.end method

.method native IGetCellMarkRectInfo([SI)I
.end method

.method native IGetCellProperty(Lcom/infraware/office/evengine/EV$CELL_PROPERTY;)V
.end method

.method native IGetCellType()I
.end method

.method native IGetChartAxesInfo(Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;)V
.end method

.method native IGetChartDataLabelInfo(Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_DATALABELINFO_EVENT;)V
.end method

.method native IGetChartEffect()I
.end method

.method native IGetChartFontData(Lcom/infraware/office/evengine/EV$CHART_FONTDATA;)V
.end method

.method native IGetChartInfo(Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_EVENT;)V
.end method

.method native IGetChartStyleInfo(Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_STYLEINFO_EVENT;)V
.end method

.method native IGetChartThumbnail(III)V
.end method

.method native IGetChartTitleInfo(Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_TITLEINFO_EVENT;)V
.end method

.method native IGetColumn()I
.end method

.method native IGetCommentText()Ljava/lang/String;
.end method

.method native IGetCompatibilityModeVersion()I
.end method

.method native IGetConfig(Lcom/infraware/office/evengine/EV$CONFIG_INFO;)V
.end method

.method native IGetCurrentSheetIndex()I
.end method

.method native IGetCurrentTableMaxRowColInfo_Editor([I)Z
.end method

.method native IGetDocType()I
.end method

.method native IGetDrawCellLineProperty(Lcom/infraware/office/evengine/EV$DRAW_CELLLINE;)V
.end method

.method native IGetDualViewPosForSlideShow(IILcom/infraware/office/evengine/EV$DUALVIEW_POS;)V
.end method

.method native IGetEditStauts()J
.end method

.method native IGetEditorMode()I
.end method

.method native IGetFontAttInfo(Lcom/infraware/office/evengine/EV$FONT_INFO;)V
.end method

.method native IGetFontStyle()I
.end method

.method native IGetFormatInfo(Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;)V
.end method

.method native IGetFrameDetectionArea(Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;)V
.end method

.method native IGetGradientDrawColorInfo(Lcom/infraware/office/evengine/EV$DRAW_GRADIENTCOLOR_INFO;)V
.end method

.method native IGetGuides(Lcom/infraware/office/evengine/EV$GUIDES_INFO;)V
.end method

.method native IGetHeaderFooterOption(Lcom/infraware/office/evengine/EV$HeaderFooterOption;)V
.end method

.method native IGetHyperLinkInfo(Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;)Z
.end method

.method native IGetHyperLinkInfoEx(IILcom/infraware/office/evengine/EV$HYPERLINK_INFO;)Z
.end method

.method native IGetImgToPDF([Ljava/lang/String;Ljava/lang/String;)V
.end method

.method native IGetInfraPenDrawMode()I
.end method

.method native IGetInterfaceHandleValue()I
.end method

.method native IGetInvalidRect(Lcom/infraware/office/evengine/EV$INVALID_DRAW_INFO;)V
.end method

.method native IGetIsSlideHide(I)Z
.end method

.method native IGetIsValidateScreenSlides()Z
.end method

.method native IGetLineSpaceUnitChange(I)I
.end method

.method native IGetMarkString()Ljava/lang/String;
.end method

.method native IGetMasterSlideImage(II)V
.end method

.method native IGetMultiSelectPointInfo(I[I)V
.end method

.method native IGetNextCommentText()V
.end method

.method native IGetObjectCount()I
.end method

.method native IGetObjectType(II)I
.end method

.method native IGetPDFAnnotationCount()V
.end method

.method native IGetPDFAnnotationListItem(ILcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;)V
.end method

.method native IGetPDFAuthor()Ljava/lang/String;
.end method

.method native IGetPDFBookmarkCount(J)I
.end method

.method native IGetPDFBookmarkList(JI[Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;)V
.end method

.method native IGetPDFTitle()Ljava/lang/String;
.end method

.method native IGetPPTChartBorder(I)Z
.end method

.method native IGetPageDisplayInfo([Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;)V
.end method

.method native IGetPageThumbnail(IIIILjava/lang/String;I)V
.end method

.method native IGetPageToBitmap(III)V
.end method

.method native IGetPaperInfo(Lcom/infraware/office/evengine/EV$PAPER_INFO;)V
.end method

.method native IGetParaAttInfo(Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;)Z
.end method

.method native IGetPrevCommentText()V
.end method

.method native IGetProperties(Lcom/infraware/office/evengine/EV$PROPERTIES;)V
.end method

.method native IGetRulerbarImage(II)V
.end method

.method native IGetRulerbarPgInfo(Lcom/infraware/office/evengine/EV$RULERBAR_PAGE_INFO;)V
.end method

.method native IGetScreenPos(Lcom/infraware/office/evengine/EV$SCREEN_INFO;)V
.end method

.method native IGetScrollInfo_Editor(Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;)V
.end method

.method native IGetSectionInfo(Lcom/infraware/office/evengine/EV$SECTION_INFO;)V
.end method

.method native IGetSeparateMarkString_Editor()Ljava/lang/String;
.end method

.method native IGetShape3DFormatInfo(Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;)V
.end method

.method native IGetShape3DRotationInfo(Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;)V
.end method

.method native IGetShapeArtisticEffectInfo(Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;)V
.end method

.method native IGetShapeCroppingInfo(Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;)V
.end method

.method native IGetShapeEffect(Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;)V
.end method

.method native IGetShapeFillInfo(Lcom/infraware/office/evengine/EV$SHAPE_FILL;)V
.end method

.method native IGetShapeGlowInfo(Lcom/infraware/office/evengine/EV$SHAPE_GLOW;)V
.end method

.method native IGetShapeLineColorInfo(Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;)V
.end method

.method native IGetShapeLineStyleInfo(Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;)V
.end method

.method native IGetShapeLocationInfo(Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;)V
.end method

.method native IGetShapePictureColorInfo(Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;)V
.end method

.method native IGetShapePictureCorrectionInfo(Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;)V
.end method

.method native IGetShapeQuickStyleInfo(Lcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;)V
.end method

.method native IGetShapeReflectionInfo(Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;)V
.end method

.method native IGetShapeShadowInfo(Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;)V
.end method

.method native IGetShapeSizeInfo(Lcom/infraware/office/evengine/EV$SHAPE_SIZE;)V
.end method

.method native IGetShapeSoftEdgeInfo(Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;)V
.end method

.method native IGetShapeStyleNum()I
.end method

.method native IGetShapeTextBoxInfo(Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;)V
.end method

.method native IGetSheetCount()I
.end method

.method native IGetSheetHyperLinkInfo(Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;)V
.end method

.method native IGetSheetInfo(Lcom/infraware/office/evengine/EV$SHEET_INFO;I)V
.end method

.method native IGetSheetNameList()[Ljava/lang/String;
.end method

.method native IGetSheetPdfPrintTotalPage(I)I
.end method

.method native IGetSheetPrintTotalPage(IIIIIII)I
.end method

.method native IGetSheetTextboxRectInfo([I)I
.end method

.method native IGetSlideAnimationList_Count()I
.end method

.method native IGetSlideAreaForSlideShow(Landroid/graphics/Rect;)V
.end method

.method native IGetSlideBackgroundColor(I)I
.end method

.method native IGetSlideLayout(I)I
.end method

.method native IGetSlideNoteString(I)Ljava/lang/String;
.end method

.method native IGetSlideNoteStringEx(I)Ljava/lang/String;
.end method

.method native IGetSlideShowEffect(ILcom/infraware/office/evengine/EV$SLIDE_TRANSITION_INFO;)V
.end method

.method native IGetSlideShowPlay(II)I
.end method

.method native IGetSlideShowSetting()Z
.end method

.method native IGetSlideShowVideoInfo(IILandroid/graphics/Rect;)Ljava/lang/String;
.end method

.method native IGetSortRange(Lcom/infraware/office/evengine/EV$RANGE;I)I
.end method

.method native IGetSummaryData(Lcom/infraware/office/evengine/EV$SUMMARY_DATA;)V
.end method

.method native IGetSystemFontCount()I
.end method

.method native IGetSystemFontNames()[Ljava/lang/String;
.end method

.method native IGetTableDrawMode()I
.end method

.method native IGetTableGuides(Lcom/infraware/office/evengine/EV$TABLE_GUIDES;)V
.end method

.method native IGetTableStyleInfo(Lcom/infraware/office/evengine/EV$TABLE_STYLE_INFO;)V
.end method

.method native IGetTextMarkRectInfo([SI)I
.end method

.method native IGetTextToSpeachString(I)V
.end method

.method native IGetTextWrapType()I
.end method

.method native IGetTopRedoDataInfo()I
.end method

.method native IGetTopUndoDataInfo()I
.end method

.method native IGetTouchInfo(Lcom/infraware/office/evengine/EV$TOUCH_INFO;)V
.end method

.method native IGetTouchInfoEX(Lcom/infraware/office/evengine/EV$TOUCH_INFO;)V
.end method

.method native IGetTouchString(II)Ljava/lang/String;
.end method

.method native IGetUseFontCount()I
.end method

.method native IGetUseFontNames()[Ljava/lang/String;
.end method

.method native IGetVideoPath()Ljava/lang/String;
.end method

.method native IGetVideoRect(Landroid/graphics/Rect;)V
.end method

.method native IGetViewOption()I
.end method

.method native IGetWrongSpell(II)Ljava/lang/String;
.end method

.method native IGotoAnnotation(IIIIFFFFZ)V
.end method

.method native IGotoPDFBookmark(J)V
.end method

.method native IHIDAction(IIIIII)V
.end method

.method native IHasDocComments()Z
.end method

.method native IHasPDFAnnots()Z
.end method

.method native IHasPDFText()Z
.end method

.method native IHyperLink(III)V
.end method

.method native IHyperLinkEnd()V
.end method

.method native IHyperlinkEditor(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method native IImageInsert(Ljava/lang/String;Landroid/graphics/Bitmap;IIZ)V
.end method

.method native IIndentation(I)V
.end method

.method native IInfraPenAllErase()V
.end method

.method native IInitInterfaceHandle()I
.end method

.method native IInitialize(IIIIIIII)V
.end method

.method native IInputChar(I)V
.end method

.method native IInsertShape(I)V
.end method

.method native IInsertShapeStyle(II)V
.end method

.method native IInsertString(Ljava/lang/String;III)V
.end method

.method native IInsertTextBox(Z)V
.end method

.method native IIsComplexColumn()Z
.end method

.method native IIsContinuePageView_Editor()I
.end method

.method native IIsInsertBookmark_Editor()I
.end method

.method native IIsItalicAttr()I
.end method

.method native IIsLastSlideShow()I
.end method

.method native IIsLastSlideShow(Z)I
.end method

.method native IIsNextEffect()Z
.end method

.method native IIsNoneEffect(II)Z
.end method

.method native IIsPDFSaveAble()Z
.end method

.method native IIsPenDataForFreeDraw()I
.end method

.method native IIsPenDataForSlideShow(I)I
.end method

.method native IIsShowMemoSetting()I
.end method

.method native IIsSlideShow()Z
.end method

.method native IIsSlideShowing()Z
.end method

.method native IIsStartOfSentence_Editor()I
.end method

.method native IIsSupportFocusTextToSpeach()Z
.end method

.method native IIsWaitingFlag()Z
.end method

.method native IIsWaitingStatus()Z
.end method

.method public native IMacroBindFiles(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public native IMacroGetRecordingFlag()I
.end method

.method public native IMacroInit()V
.end method

.method public native IMacroRecording_Start(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public native IMacroRecording_Stop()V
.end method

.method public native IMacroRelease()V
.end method

.method public native IMacroRun_FileClose()V
.end method

.method public native IMacroRun_FileOpen(Ljava/lang/String;)V
.end method

.method public native IMacroRun_SetPause(I)V
.end method

.method public native IMacroSetEnvInfo(I)V
.end method

.method native IMemoCommand(ILcom/infraware/office/evengine/EV$MEMO_CMD_DATA;)Z
.end method

.method native IModifyPDFAnnotation(ILjava/lang/String;I)V
.end method

.method native IMovePage(II)V
.end method

.method native INewDocument(Ljava/lang/String;IIIIIILjava/lang/String;Ljava/lang/String;)V
.end method

.method native INoMarginView()V
.end method

.method native IOpen(Ljava/lang/String;IIIIIIIIIIIILjava/lang/String;Ljava/lang/String;)V
.end method

.method native IOpenEx(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method native IPDFGetExtBitmap(Lcom/infraware/office/evengine/EV$TOUCH_INFO;)Z
.end method

.method native IPDFMapRectToView(IIIIII[I)V
.end method

.method native IPDFMapRectToViewEX(I[I)V
.end method

.method native IPDFSaveAnnot()V
.end method

.method native IPDFUpdated()Z
.end method

.method native IPageModified(I)Z
.end method

.method native IParagraphAlign(I)V
.end method

.method native IPivotScreen(III)V
.end method

.method native IPopupOffset(IIIII)V
.end method

.method native IReDraw()V
.end method

.method native IRedoUndo(I)V
.end method

.method native IRemoveAllBookMark()V
.end method

.method native IRemoveAllPDFAnnotation()V
.end method

.method native IRemoveBookMark(Ljava/lang/String;)V
.end method

.method native IRemovePDFAnnotation(I)V
.end method

.method native IReplaceWrongSpell(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method native IRotateFrame(I)V
.end method

.method native IRotatePage(II)V
.end method

.method native ISaveBookMark()V
.end method

.method native ISaveDocument(Ljava/lang/String;I)V
.end method

.method native ISaveThumbnailAt(IIIIILjava/lang/String;I)V
.end method

.method native IScroll(IIIII)V
.end method

.method native IScrollAndFitToWidth(IIII)V
.end method

.method native ISearchStart(Ljava/lang/String;IIII)V
.end method

.method native ISearchStop()V
.end method

.method native ISelectAll()V
.end method

.method native ISellectAll()V
.end method

.method native ISetAnimationDelete(I)V
.end method

.method native ISetAnimationMove(II)V
.end method

.method native ISetAutofilterButtonConfiguration(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method native ISetBookClip(Lcom/infraware/office/evengine/EV$BOOK_CLIP;)I
.end method

.method native ISetBookmarkLabel(Lcom/infraware/office/evengine/EV$BOOKMARK_LABEL;I)V
.end method

.method native ISetBorder(IIIIIIIIIIIZ)V
.end method

.method native ISetBwpChart(II[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)V
.end method

.method native ISetCellProperty(IIIIIZ)V
.end method

.method native ISetChartStyle(I)V
.end method

.method native ISetClearAllPen()V
.end method

.method native ISetColors(III)V
.end method

.method native ISetColumn(II)V
.end method

.method native ISetCompBackColor(IIIIJJIII)V
.end method

.method native ISetCroppingMode(II)V
.end method

.method native ISetDrawCellLineProperty(IIIIZ)V
.end method

.method native ISetFillColor(I)V
.end method

.method native ISetFindModeChange(I)V
.end method

.method native ISetFontAttribute(Ljava/lang/String;IIIIIIIIIZ)V
.end method

.method native ISetFontStyle(I)V
.end method

.method native ISetForceDocumentModified(Z)V
.end method

.method native ISetFormCopyPaste(I)V
.end method

.method native ISetFrameDetectionArea(Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;)V
.end method

.method native ISetFrameGroup(I)V
.end method

.method native ISetGuides(IZ)V
.end method

.method native ISetHaderFooterAction(III)V
.end method

.method native ISetHaderFooterNavigation(I)V
.end method

.method native ISetHeaderFooterOption(IIZZ)V
.end method

.method native ISetHeapSize(I)V
.end method

.method native ISetImageEffect(IIIIIIZ)V
.end method

.method native ISetInfraPenDrawMode(I)V
.end method

.method native ISetInfraPenShow(I)V
.end method

.method native ISetInterfaceHandle(I)Z
.end method

.method native ISetLineShape(IIII)V
.end method

.method native ISetLineSpace(I)V
.end method

.method native ISetLocale(I)V
.end method

.method native ISetMarkingByPen(IIII)V
.end method

.method native ISetMemoView(III)V
.end method

.method native ISetModeStatus(I)V
.end method

.method native ISetMultiObjectAlign(I)V
.end method

.method native ISetMultiSelect(I)V
.end method

.method native ISetObjDelete()V
.end method

.method native ISetObjPos(I)V
.end method

.method native ISetObjResize(II)V
.end method

.method native ISetObjTextEdit()V
.end method

.method native ISetObjectAttribute(IIIIIIIIIIZ)V
.end method

.method native ISetPDFAnnotationMoveable(Z)V
.end method

.method native ISetPDFBgColor(I)Z
.end method

.method native ISetPPTChartBorder(IZ)V
.end method

.method native ISetPPTChartEffect(I)V
.end method

.method native ISetPPTChartRowColChange()V
.end method

.method native ISetPPTChartShowHideData(Z)V
.end method

.method native ISetPPTSlideGLSync()V
.end method

.method native ISetPageMap(IIIIII)V
.end method

.method native ISetPageMode(I)V
.end method

.method native ISetPaperInfo(IIIIIIIIII)V
.end method

.method native ISetParaAttribute(IIIIIIIIII)V
.end method

.method native ISetParaAttributeMask(IIIIIIIIIIIIIZ)V
.end method

.method native ISetPenColor(I)V
.end method

.method native ISetPenMode(IZ)V
.end method

.method native ISetPenPosition([I[I[I[II)V
.end method

.method native ISetPenSize(I)V
.end method

.method native ISetPenTransparency(I)V
.end method

.method native ISetPreview(IIILjava/lang/String;I)V
.end method

.method native ISetPreviewTimerCB()V
.end method

.method native ISetPrint(IIILjava/lang/String;I)V
.end method

.method native ISetPrintEx(IIILjava/lang/String;ILjava/lang/String;Ljava/lang/String;IIII)V
.end method

.method native ISetPrintMode()V
.end method

.method native ISetProperties(Lcom/infraware/office/evengine/EV$PROPERTIES;)V
.end method

.method native ISetRefNote(II)V
.end method

.method native ISetReplace(Ljava/lang/String;IIILjava/lang/String;I)V
.end method

.method native ISetResetUndoData()I
.end method

.method native ISetScreenMode(I)V
.end method

.method native ISetShadowStyle(I)V
.end method

.method native ISetShapeEffect(IIIIIIIIIIZ)V
.end method

.method native ISetShapeProperty(ILcom/infraware/office/evengine/EV$SHAPE_QUICK_STYLE;Lcom/infraware/office/evengine/EV$SHAPE_FILL;Lcom/infraware/office/evengine/EV$SHAPE_LINE_COLOR;Lcom/infraware/office/evengine/EV$SHAPE_LINE_STYLE;Lcom/infraware/office/evengine/EV$SHAPE_SHADOW;Lcom/infraware/office/evengine/EV$SHAPE_REFLECTION;Lcom/infraware/office/evengine/EV$SHAPE_GLOW;Lcom/infraware/office/evengine/EV$SHAPE_SOFTEDGE;Lcom/infraware/office/evengine/EV$SHAPE_3D_FORMAT;Lcom/infraware/office/evengine/EV$SHAPE_3D_ROTATION;Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_CORRECTION;Lcom/infraware/office/evengine/EV$SHAPE_PICTURE_COLOR;Lcom/infraware/office/evengine/EV$SHAPE_ARTISTIC_EFFECT;Lcom/infraware/office/evengine/EV$SHAPE_CROPPING;Lcom/infraware/office/evengine/EV$SHAPE_SIZE;Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;Lcom/infraware/office/evengine/EV$SHAPE_TEXTBOX;)V
.end method

.method native ISetShapeStyle(I)V
.end method

.method native ISetShapeStyleNum(I)V
.end method

.method native ISetSheetHyperLinkInfo(Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;)V
.end method

.method native ISetSheetScreenFirstSelection()V
.end method

.method native ISetSlideAnimation(IIIIIIII)V
.end method

.method native ISetSlideAnimationAdd(IIIIIIII)V
.end method

.method native ISetSlideBackgroundColor(II)V
.end method

.method native ISetSlideHide(IZ)V
.end method

.method native ISetSlideShowEffect(IIIIIII)V
.end method

.method native ISetSlideShowSetting(Z)Z
.end method

.method native ISetSortRange(Lcom/infraware/office/evengine/EV$RANGE;)V
.end method

.method native ISetSummaryData(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method native ISetTableDrawMode(I)V
.end method

.method native ISetTableProperty(IIIIIIZ)V
.end method

.method native ISetTableStyleInfo(III)V
.end method

.method native ISetTemplateShape(II)V
.end method

.method native ISetTextWrapType(I)V
.end method

.method native ISetViewMode(I)V
.end method

.method native ISetWebMode()V
.end method

.method native ISetZoom(IIIIIIIIIII)V
.end method

.method native IShapeInsertEx(IIIIIII)V
.end method

.method native ISheetAutoFilterMenuListUpdate(I)V
.end method

.method native ISheetBorder(Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;)V
.end method

.method native ISheetClear(I)V
.end method

.method native ISheetDeleteCommentText()V
.end method

.method native ISheetDrawFormulaRange(ZZ)V
.end method

.method native ISheetEdit(ILjava/lang/String;IIII)V
.end method

.method native ISheetEditCF(Lcom/infraware/office/evengine/EV$RANGE;III[C)V
.end method

.method native ISheetFilter()V
.end method

.method native ISheetFilterCommand(IILjava/lang/String;)V
.end method

.method native ISheetFilterIsRunning()Z
.end method

.method native ISheetFindReplace(Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method native ISheetFixFrame()V
.end method

.method native ISheetFocus()V
.end method

.method native ISheetFormat(IIIIIIIIIZ)V
.end method

.method native ISheetFunction(I)V
.end method

.method native ISheetGetCFInfo(Lcom/infraware/office/evengine/EV$SHEET_CF_INFO;)V
.end method

.method native ISheetGetSortKeyInfo([S)Z
.end method

.method native ISheetGetTextBoxText()Ljava/lang/String;
.end method

.method native ISheetInputField(I)V
.end method

.method native ISheetInsertAllChart(IILcom/infraware/office/evengine/EV$RANGE;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZZ)V
.end method

.method native ISheetInsertCell(II)V
.end method

.method native ISheetInsertChart(ILcom/infraware/office/evengine/EV$RANGE;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZZZIZZ)V
.end method

.method native ISheetInsertColumns(III)V
.end method

.method native ISheetInsertCommentText(Ljava/lang/String;)V
.end method

.method native ISheetInsertRows(III)V
.end method

.method native ISheetIsClipboardData()Z
.end method

.method native ISheetIsObjClicked()Z
.end method

.method native ISheetIsSameObjSelected()Z
.end method

.method native ISheetIsTextBox()Z
.end method

.method native ISheetIsWholeCols()Z
.end method

.method native ISheetIsWholeRows()Z
.end method

.method native ISheetMerge()V
.end method

.method native ISheetPageBreak()V
.end method

.method native ISheetProtection()V
.end method

.method native ISheetRecalculate()V
.end method

.method native ISheetSetAlignment(II)V
.end method

.method native ISheetSetAutoFormula(I)V
.end method

.method native ISheetSetColor(I)V
.end method

.method native ISheetSetFormulaRangeColor([I)V
.end method

.method native ISheetSetRowColSize(III)V
.end method

.method native ISheetSetTextboxEditMode(I)V
.end method

.method native ISheetSetTextboxText(Ljava/lang/String;I)V
.end method

.method native ISheetShowHideRowCol(III)V
.end method

.method native ISheetSort(IIII)V
.end method

.method native ISheetWrap()V
.end method

.method native IShowHideImage(I)V
.end method

.method native ISlideManage(III)V
.end method

.method native ISlideNote(III)V
.end method

.method native ISlideNoteInput(ILjava/lang/String;I)V
.end method

.method native ISlideNoteInputEx(ILjava/lang/String;)V
.end method

.method native ISlideObjInsert(III)V
.end method

.method native ISlideObjStart(III)V
.end method

.method native ISlideObjStartEx(IIFIZ)V
.end method

.method native ISlideShow(IIIIIZZ)V
.end method

.method native ISlideShowBegin()V
.end method

.method native ISlideShowContinue()V
.end method

.method native ISlideShowPlay(IIIZ)V
.end method

.method native ISpellCheckScreen(Z)V
.end method

.method native ISpellCheckWrong(Ljava/lang/String;IIIIIII)I
.end method

.method native IStopFlicking()V
.end method

.method native IStopSlideEffect()V
.end method

.method native IThreadSuspend(I)V
.end method

.method native IThumbnail(IIIII)V
.end method

.method native ITimer()V
.end method

.method native IVideoInsert(Ljava/lang/String;Landroid/graphics/Bitmap;IIZLjava/lang/String;Z)V
.end method

.method native IZoomInOut(II)V
.end method

.method native IsLassoViewMode_Editor()Z
.end method

.method native IsPenDrawFrameShow()I
.end method

.method native IsWebMode()I
.end method

.method SetListener(Lcom/infraware/office/evengine/EvListener$ViewerListener;Lcom/infraware/office/evengine/EvListener$EditorListener;Lcom/infraware/office/evengine/EvListener$WordEditorListener;Lcom/infraware/office/evengine/EvListener$PptEditorListener;Lcom/infraware/office/evengine/EvListener$SheetEditorListener;Lcom/infraware/office/evengine/EvListener$PdfViewerListener;)V
    .locals 0
    .param p1, "DvL"    # Lcom/infraware/office/evengine/EvListener$ViewerListener;
    .param p2, "EvL"    # Lcom/infraware/office/evengine/EvListener$EditorListener;
    .param p3, "EvWL"    # Lcom/infraware/office/evengine/EvListener$WordEditorListener;
    .param p4, "EvPL"    # Lcom/infraware/office/evengine/EvListener$PptEditorListener;
    .param p5, "EvSL"    # Lcom/infraware/office/evengine/EvListener$SheetEditorListener;
    .param p6, "EvPDFL"    # Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    .line 113
    iput-object p2, p0, Lcom/infraware/office/evengine/EvNative;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    .line 114
    iput-object p3, p0, Lcom/infraware/office/evengine/EvNative;->mEvWordListener:Lcom/infraware/office/evengine/EvListener$WordEditorListener;

    .line 115
    iput-object p4, p0, Lcom/infraware/office/evengine/EvNative;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    .line 116
    iput-object p5, p0, Lcom/infraware/office/evengine/EvNative;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    .line 117
    iput-object p6, p0, Lcom/infraware/office/evengine/EvNative;->mEvPdfViewerListener:Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

    .line 118
    return-void
.end method

.method SetPdfAnnotListener(Lcom/infraware/office/evengine/EvListener$PdfAnnotListener;)V
    .locals 0
    .param p1, "EvPDFAL"    # Lcom/infraware/office/evengine/EvListener$PdfAnnotListener;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/infraware/office/evengine/EvNative;->mEvPdfAnnotListener:Lcom/infraware/office/evengine/EvListener$PdfAnnotListener;

    .line 126
    return-void
.end method

.method SetPrintListener(Lcom/infraware/office/evengine/EvListener$PrintListener;)V
    .locals 0
    .param p1, "EvPrintL"    # Lcom/infraware/office/evengine/EvListener$PrintListener;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/infraware/office/evengine/EvNative;->mPrintListener:Lcom/infraware/office/evengine/EvListener$PrintListener;

    .line 122
    return-void
.end method

.method native setTempFolder(Ljava/lang/String;I)V
.end method
