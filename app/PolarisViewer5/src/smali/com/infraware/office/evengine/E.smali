.class public interface abstract Lcom/infraware/office/evengine/E;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/office/evengine/E$EV_PAGELAYOUT_SCOPE;,
        Lcom/infraware/office/evengine/E$EV_MEMO_COMMAND_TYPE;,
        Lcom/infraware/office/evengine/E$EV_REF_NOTE_FORM_TYPE;,
        Lcom/infraware/office/evengine/E$EV_REF_NOTE_TYPE;,
        Lcom/infraware/office/evengine/E$EV_FRAME_ROTATION_PRESET;,
        Lcom/infraware/office/evengine/E$EV_SHAPE_GLOW_PRESET;,
        Lcom/infraware/office/evengine/E$EV_SHAPE_REFLECTION_PRESET;,
        Lcom/infraware/office/evengine/E$EV_SHAPE_3DROTATION_PRESET;,
        Lcom/infraware/office/evengine/E$EV_SHAPE_3DFORMAT_BEVELTYPE_PRESET;,
        Lcom/infraware/office/evengine/E$EV_SHAPE_SHADOW_PRESET;,
        Lcom/infraware/office/evengine/E$EV_SHAPE_LINE_ARROW_SIZE;,
        Lcom/infraware/office/evengine/E$EV_SHAPE_LINE_ARROW_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SHAPE_LINE_STYLE_JOIN;,
        Lcom/infraware/office/evengine/E$EV_SHAPE_LINE_STYLE_CAP;,
        Lcom/infraware/office/evengine/E$EV_SHAPE_LINE_STYLE_DASH;,
        Lcom/infraware/office/evengine/E$EV_SHAPE_LINE_STYLE_COMPOUND;,
        Lcom/infraware/office/evengine/E$EV_SHAPE_CROPPING_SELECTOR;,
        Lcom/infraware/office/evengine/E$EV_SHAPE_LINE_SELECTOR;,
        Lcom/infraware/office/evengine/E$EV_SHAPE_LINE_COLOR_SELECTOR;,
        Lcom/infraware/office/evengine/E$EV_SHAPE_FILL_SELECTOR;,
        Lcom/infraware/office/evengine/E$EV_SHAPE_GRADIENT_DIRECTION;,
        Lcom/infraware/office/evengine/E$EV_SHAPE_GRADIENT_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SHAPE_QUICK_STYLE;,
        Lcom/infraware/office/evengine/E$EV_SHAPE_FORMAT_SELECTOR;,
        Lcom/infraware/office/evengine/E$EV_SHAPE_EFFECT_MASK;,
        Lcom/infraware/office/evengine/E$EV_CORENOTIFY;,
        Lcom/infraware/office/evengine/E$EV_TEXTMARK_DIRECTION_TYPE;,
        Lcom/infraware/office/evengine/E$EV_PROTECT_INFO;,
        Lcom/infraware/office/evengine/E$EV_SHEET_INPUT_FIELD_TYPE;,
        Lcom/infraware/office/evengine/E$EV_HEADERFOOTER_NAVIGATION;,
        Lcom/infraware/office/evengine/E$EV_HEADERFOOTER_FRAME;,
        Lcom/infraware/office/evengine/E$EV_HEADERFOOTER_ACTION;,
        Lcom/infraware/office/evengine/E$EV_HEADERFOOTER_TEMPLATE;,
        Lcom/infraware/office/evengine/E$EV_ON_EDIT;,
        Lcom/infraware/office/evengine/E$EV_PAPER_LAYOUT_MASK;,
        Lcom/infraware/office/evengine/E$EV_CARET_DIRECTION_TYPE;,
        Lcom/infraware/office/evengine/E$EV_PARA_TEXTFLOW_TYPE;,
        Lcom/infraware/office/evengine/E$EV_TABLE_RESULT;,
        Lcom/infraware/office/evengine/E$EV_PEN_LINE_CAP_TYPE;,
        Lcom/infraware/office/evengine/E$EV_GUI_EVENT;,
        Lcom/infraware/office/evengine/E$EV_OBJECT_EDITING_TYPE;,
        Lcom/infraware/office/evengine/E$EV_TEXT_WRAP_TYPE;,
        Lcom/infraware/office/evengine/E$EV_PEN_MODE;,
        Lcom/infraware/office/evengine/E$EV_CHART_LEGEND;,
        Lcom/infraware/office/evengine/E$EV_BWP_CHART_MODIFY;,
        Lcom/infraware/office/evengine/E$EV_CHART_EFFECT;,
        Lcom/infraware/office/evengine/E$EV_RES_STRING_ID;,
        Lcom/infraware/office/evengine/E$EDVA_PAGE_INFO_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SHEET_CONDITIONALFORMAT_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SHEET_COMMENT_EDIT;,
        Lcom/infraware/office/evengine/E$EV_SHEET_CLEAR;,
        Lcom/infraware/office/evengine/E$EV_SHEET_SHPW_ROWCOL;,
        Lcom/infraware/office/evengine/E$EV_SHEET_INSERT_ROWCOL;,
        Lcom/infraware/office/evengine/E$EV_SHEET_FORMULA_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SHEET_CELL_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SHEET_CHART_FONTLIST_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SHEET_CHART_FONT_SIZE;,
        Lcom/infraware/office/evengine/E$EV_SHEET_CHART_STYLE;,
        Lcom/infraware/office/evengine/E$EV_SHEET_CHART_AXIS;,
        Lcom/infraware/office/evengine/E$EV_SHEET_CHART_LEGEND;,
        Lcom/infraware/office/evengine/E$EV_SHEERT_CHART_DIMENSIONS;,
        Lcom/infraware/office/evengine/E$EV_SHEET_CHART_SERIES;,
        Lcom/infraware/office/evengine/E$EV_CHART_BAR_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SHEET_RADAR_SUB_CHART_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SHEET_BUBBLE_SUB_CHART_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SHEET_DOUGHNUT_SUB_CHART_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SHEET_SURFACE_SUB_CHART_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SHEET_SCATTER_SUB_CHART_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SHEET_AREA_SUB_CHART_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SHEET_BAR_SUB_CHART_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SHEET_PIE_SUB_CHART_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SHEET_LINE_SUB_CHART_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SHEET_COLUMN_SUB_CHART_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SHEET_CHART_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SHEET_FIND_REPLACE;,
        Lcom/infraware/office/evengine/E$EV_SHEET_INSERT_CELL;,
        Lcom/infraware/office/evengine/E$EV_SHEET_INPUTFIELD_RESULT;,
        Lcom/infraware/office/evengine/E$EV_SHEET_EDITOR_RESULT;,
        Lcom/infraware/office/evengine/E$EV_SHEET_EDIT;,
        Lcom/infraware/office/evengine/E$EV_SHEET_FUNCTION;,
        Lcom/infraware/office/evengine/E$EV_SHEET_FRACTION;,
        Lcom/infraware/office/evengine/E$EV_SHEET_TIME;,
        Lcom/infraware/office/evengine/E$EV_SHEET_DATE;,
        Lcom/infraware/office/evengine/E$EV_SHEET_NEGATIVE;,
        Lcom/infraware/office/evengine/E$EV_SHEET_CURRENCY;,
        Lcom/infraware/office/evengine/E$EV_SHEET_FORMAT;,
        Lcom/infraware/office/evengine/E$EV_SHEET_COLOR;,
        Lcom/infraware/office/evengine/E$EV_SHEET_HYPERLINK;,
        Lcom/infraware/office/evengine/E$EV_SHEET_INPUT_CONFIRM;,
        Lcom/infraware/office/evengine/E$EV_SHEET_EDITOR_STATUS;,
        Lcom/infraware/office/evengine/E$EV_POPUP_ONOFF;,
        Lcom/infraware/office/evengine/E$EV_INDENTATION;,
        Lcom/infraware/office/evengine/E$EV_WORD_CELL_EQUAL_WIDTH_HEIGHT;,
        Lcom/infraware/office/evengine/E$EV_WORD_CELL_MERGE_SEP;,
        Lcom/infraware/office/evengine/E$EV_CELL_INSERT_DELETE;,
        Lcom/infraware/office/evengine/E$EV_WORD_SHOW_HIDE;,
        Lcom/infraware/office/evengine/E$EV_CHAR_INPUT;,
        Lcom/infraware/office/evengine/E$EV_BULLETNUMBER_TYPE;,
        Lcom/infraware/office/evengine/E$EV_BULLET_NUMBERING;,
        Lcom/infraware/office/evengine/E$EV_SELECT_CELL_MODE;,
        Lcom/infraware/office/evengine/E$EV_CARET_MARK;,
        Lcom/infraware/office/evengine/E$EV_CARET_MOVE;,
        Lcom/infraware/office/evengine/E$EV_REDO_UNDO;,
        Lcom/infraware/office/evengine/E$EV_FONT_ATTRIBUTE;,
        Lcom/infraware/office/evengine/E$EV_LINE_SPACE_UNIT;,
        Lcom/infraware/office/evengine/E$EV_CLIPBOARD_MODE;,
        Lcom/infraware/office/evengine/E$EV_EDIT_MODE;,
        Lcom/infraware/office/evengine/E$EV_CELL_DELETE_MODE;,
        Lcom/infraware/office/evengine/E$EV_CELL_INSERT_MODE;,
        Lcom/infraware/office/evengine/E$EV_CELL_EDIT_MODE;,
        Lcom/infraware/office/evengine/E$EV_EDIT_CURSOR_MODE;,
        Lcom/infraware/office/evengine/E$EV_BOOKMARK_EDITOR_MODE;,
        Lcom/infraware/office/evengine/E$EV_CAN_CELL_DELETE_TYPE;,
        Lcom/infraware/office/evengine/E$EV_EDIT_OBJECT_POINT_TYPE;,
        Lcom/infraware/office/evengine/E$EV_EDIT_OBJECT_BASE_TYPE;,
        Lcom/infraware/office/evengine/E$EV_EDIT_OBJECT_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SLIDESHOW_ANIMATION_FRAME_TYPE;,
        Lcom/infraware/office/evengine/E$EV_TTS_START_MODE;,
        Lcom/infraware/office/evengine/E$EV_TTS_REQUEST_TYPE;,
        Lcom/infraware/office/evengine/E$EV_TTS_HELPER_STATUS;,
        Lcom/infraware/office/evengine/E$EV_VIDEO_STATUS;,
        Lcom/infraware/office/evengine/E$EV_SLIDESHOW_ANIMATION_TRIGGER_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SLIDESHOW_ANIMATION_PRESET_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SLIDESHOW_ANIMATION_VANISHING_POINT_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SLIDESHOW_ANIMATION_SPOKES_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SLIDESHOW_ANIMATION_SHAPE_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SLIDESHOW_ANIMATION_DIRECTION_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SLIDESHOW_ANIMATION_OPTION_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SLIDESHOW_ANIMATION_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SLIDESHOW_EFFECT_OPTION_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SLIDESHOW_EFFECT_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SLIDESHOW_PLAY_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SLIDE_MANAGE_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SLIDE_TEMPLATE_TYPE;,
        Lcom/infraware/office/evengine/E$EV_LINE_ARROW_TYPE;,
        Lcom/infraware/office/evengine/E$EV_LINE_THICK;,
        Lcom/infraware/office/evengine/E$EV_BORDER_STYLE;,
        Lcom/infraware/office/evengine/E$EV_BORDER_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SUMMARY_MASK;,
        Lcom/infraware/office/evengine/E$EV_TYPECASE;,
        Lcom/infraware/office/evengine/E$EV_TABLE_STYPE_OPTION;,
        Lcom/infraware/office/evengine/E$EV_TABLE_STYLE_MASK;,
        Lcom/infraware/office/evengine/E$EV_GUIDES_MASK;,
        Lcom/infraware/office/evengine/E$EV_HID_ACTION;,
        Lcom/infraware/office/evengine/E$EV_PARAGRAPH_MASK;,
        Lcom/infraware/office/evengine/E$EV_PARAGRAPH_ALIGN;,
        Lcom/infraware/office/evengine/E$EV_VIEW_MODE;,
        Lcom/infraware/office/evengine/E$EV_PROTECT_STATUS;,
        Lcom/infraware/office/evengine/E$EV_STATUS;,
        Lcom/infraware/office/evengine/E$EV_MASK_ATT;,
        Lcom/infraware/office/evengine/E$EV_VKEYS;,
        Lcom/infraware/office/evengine/E$EV_LOCALE_TYPE;,
        Lcom/infraware/office/evengine/E$EV_FILE_ATTRIBUTE;,
        Lcom/infraware/office/evengine/E$EV_BOOKCLIP_STATUS;,
        Lcom/infraware/office/evengine/E$EV_TIMER_ID;,
        Lcom/infraware/office/evengine/E$EV_DISPLAY_MODE_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SENDINTERNALSTRINGMODE;,
        Lcom/infraware/office/evengine/E$EV_FINDWORD_MODE_TYPE;,
        Lcom/infraware/office/evengine/E$EV_FiNDWORD_DIRECTORY_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SEARCH_MARKING_TYPE;,
        Lcom/infraware/office/evengine/E$EV_PAGEEDGE_POSITION_TYPE;,
        Lcom/infraware/office/evengine/E$EV_UNREDO_ACTION;,
        Lcom/infraware/office/evengine/E$EV_PAGEEDGE_WIDTH_TYPE;,
        Lcom/infraware/office/evengine/E$EV_HYPERBOX_TYPE;,
        Lcom/infraware/office/evengine/E$EV_FONT_TYPE;,
        Lcom/infraware/office/evengine/E$EV_HYPERLINK_MODE;,
        Lcom/infraware/office/evengine/E$EV_KEY_TYPE;,
        Lcom/infraware/office/evengine/E$EV_THUMBNAIL_OPTION_TYPE;,
        Lcom/infraware/office/evengine/E$EV_THUMBNAIL_MODE_TYPE;,
        Lcom/infraware/office/evengine/E$EV_PAGEMAP_POSITION_TYPE;,
        Lcom/infraware/office/evengine/E$EV_PRINT_XLS_MODE;,
        Lcom/infraware/office/evengine/E$EV_PRINT_MARGIN_MODE;,
        Lcom/infraware/office/evengine/E$EV_PRINT_RETURN_TYPE;,
        Lcom/infraware/office/evengine/E$EV_PRINT_PAGER_TYPE;,
        Lcom/infraware/office/evengine/E$EV_PAGEMAP_COMMAND_TYPE;,
        Lcom/infraware/office/evengine/E$EV_PROGRESS_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SEARCH_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SCROLL_FACTOR_TYPE;,
        Lcom/infraware/office/evengine/E$EV_REPLACE_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SCREENMODE_TYPE;,
        Lcom/infraware/office/evengine/E$EV_VIEWMODE_TYPE;,
        Lcom/infraware/office/evengine/E$EV_DOC_TYPE;,
        Lcom/infraware/office/evengine/E$EV_DOCEXTENSION_TYPE;,
        Lcom/infraware/office/evengine/E$EV_CLIPDATATYPE;,
        Lcom/infraware/office/evengine/E$EV_DOCTYPE;,
        Lcom/infraware/office/evengine/E$EV_FILE_LOAD_TYPE;,
        Lcom/infraware/office/evengine/E$EV_ROTATE_TYPE;,
        Lcom/infraware/office/evengine/E$EV_ZOOM_TYPE;,
        Lcom/infraware/office/evengine/E$EV_MOVE_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SCROLL_COMMAND_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SHADOW_STYLE;,
        Lcom/infraware/office/evengine/E$EV_IMAGE_MASK;,
        Lcom/infraware/office/evengine/E$EV_FILE_POS_TYPE;,
        Lcom/infraware/office/evengine/E$EV_FRAME_ALIGN_TYPE;,
        Lcom/infraware/office/evengine/E$EV_BOOKMARK_TYPE;,
        Lcom/infraware/office/evengine/E$EV_SCROLL_MODE;,
        Lcom/infraware/office/evengine/E$EV_SAVE_OPTION;,
        Lcom/infraware/office/evengine/E$EV_EVENT_SET_ERROR_VALUE;,
        Lcom/infraware/office/evengine/E$EV_ERROR_CODE;,
        Lcom/infraware/office/evengine/E$EV_PDF_SMOOTH_DEBUG;
    }
.end annotation


# static fields
.field public static final EVFALSE:I = 0x0

.field public static final EVTRUE:I = 0x1

.field public static final EV_BOLD:I = 0x400

.field public static final EV_DSTRIKEOUT:I = 0x4000

.field public static final EV_EMBOSS:I = 0x8

.field public static final EV_ENGRAVE:I = 0x4

.field public static final EV_FIND_DIR_GLOBAL:I = 0x2

.field public static final EV_FIND_DIR_NEXT:I = 0x0

.field public static final EV_FIND_DIR_PREV:I = 0x1

.field public static final EV_IME_COMPOSITING:I = 0x0

.field public static final EV_IME_COMPOSITING_RESULT:I = 0x1

.field public static final EV_LINESPACE_DECREASE:I = 0x1

.field public static final EV_LINESPACE_INCREASE:I = 0x0

.field public static final EV_MAX_DECIMAL_SIZE:I = 0x1e

.field public static final EV_MAX_EDIT_LENGTH:I = 0x400

.field public static final EV_MAX_OBJ_SIZE:I = 0x960

.field public static final EV_MAX_RC_SIZE:I = 0x5a0

.field public static final EV_MIN_OBJ_SIZE:I = 0x2

.field public static final EV_OBJECT_ARROW_LEFT:I = 0x800

.field public static final EV_OBJECT_ARROW_RIGHT:I = 0x1000

.field public static final EV_OBJECT_FILL_COLOR:I = 0x1

.field public static final EV_OBJECT_FILL_NONE_COLOR:I = 0x2

.field public static final EV_OBJECT_FIX_RATE:I = 0x200

.field public static final EV_OBJECT_GRADIENT_COLOR:I = 0x4

.field public static final EV_OBJECT_MOVE_BACK:I = 0x2

.field public static final EV_OBJECT_MOVE_FIRST:I = 0x3

.field public static final EV_OBJECT_MOVE_FRONT:I = 0x1

.field public static final EV_OBJECT_MOVE_LAST:I = 0x4

.field public static final EV_OBJECT_OUTLINE_BORDER_STYLE:I = 0x40

.field public static final EV_OBJECT_OUTLINE_BORDER_THICKNESS:I = 0x20

.field public static final EV_OBJECT_OUTLINE_COLOR:I = 0x10

.field public static final EV_OBJECT_OUTLINT_ALPHA:I = 0x8000

.field public static final EV_OBJECT_SENDTO:I = 0x400

.field public static final EV_OBJECT_SHADOW:I = 0x2000

.field public static final EV_OBJECT_SIZE:I = 0x100

.field public static final EV_OBJECT_STYLE:I = 0x4000

.field public static final EV_OUTLINE:I = 0x20

.field public static final EV_REVERSE:I = 0x10

.field public static final EV_RIGHTITALIC:I = 0x200

.field public static final EV_SHADOW:I = 0x40

.field public static final EV_SHEETNAME_LENGTH:I = 0x1f

.field public static final EV_STRIKEOUT:I = 0x80

.field public static final EV_SUBSCRIPT:I = 0x2

.field public static final EV_SUPERSCRIPT:I = 0x1

.field public static final EV_TABLE_PROPERTY_BORDER_COLOR:I = 0x200

.field public static final EV_TABLE_PROPERTY_BORDER_STYLE:I = 0x400

.field public static final EV_TABLE_PROPERTY_BORDER_TYPE:I = 0x800

.field public static final EV_TABLE_PROPERTY_CELL_COLOR:I = 0x100

.field public static final EV_UNDERLINE:I = 0x100

.field public static final EV_UNDERLINE_STYLE_DASH:I = 0x5

.field public static final EV_UNDERLINE_STYLE_DOTDASH:I = 0x7

.field public static final EV_UNDERLINE_STYLE_DOTTED:I = 0x4

.field public static final EV_UNDERLINE_STYLE_DOUBLE:I = 0x3

.field public static final EV_UNDERLINE_STYLE_NONE:I = 0x0

.field public static final EV_UNDERLINE_STYLE_SHEET_ACCOUNTING_DOUBLE:I = 0x16

.field public static final EV_UNDERLINE_STYLE_SHEET_ACCOUNTING_SINGLE:I = 0x15

.field public static final EV_UNDERLINE_STYLE_SINGLE:I = 0x1

.field public static final EV_UNDERLINE_STYLE_THICK:I = 0xb

.field public static final EV_UNDERLINE_STYLE_WAVY:I = 0x9

.field public static final EV_UNDERLINE_STYLE_WAVYDOUBLE:I = 0xa

.field public static final EV_UNDERLINE_STYLE_WAVYHEAVYD:I = 0x10

.field public static final FREE_FROM_LINE:I = 0x70

.field public static final eEV_WORD_DETAIL_MARK_FIRST:I = 0x10

.field public static final eEV_WORD_DETAIL_MARK_LAST:I = 0x20

.field public static final eEV_WORD_DETAIL_MARK_NEXT:I = 0x2

.field public static final eEV_WORD_DETAIL_MARK_PREV:I = 0x1

.field public static final eEV_WORD_TABLE_PROPERTY_BORDER_COLOR:I = 0x200

.field public static final eEV_WORD_TABLE_PROPERTY_BORDER_STYLE:I = 0x400

.field public static final eEV_WORD_TABLE_PROPERTY_BORDER_TYPE:I = 0x800

.field public static final eEV_WORD_TABLE_PROPERTY_BORDER_WIDTH:I = 0x1000

.field public static final eEV_WORD_TABLE_PROPERTY_CELL_COLOR:I = 0x100
