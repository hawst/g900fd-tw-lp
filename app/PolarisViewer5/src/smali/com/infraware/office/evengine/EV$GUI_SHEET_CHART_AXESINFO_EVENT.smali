.class public Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GUI_SHEET_CHART_AXESINFO_EVENT"
.end annotation


# instance fields
.field public bAxesInfo:[C

.field public bExistAxes:[C

.field public bScaleInfo:[C

.field public dLogBase:[D

.field public nAlignment:[I

.field public nChart:I

.field public nType:I

.field public nUnitIndex:[I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 2

    .prologue
    const/4 v1, 0x5

    .line 1466
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1470
    new-array v0, v1, [C

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->bExistAxes:[C

    .line 1471
    new-array v0, v1, [C

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->bAxesInfo:[C

    .line 1472
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->nAlignment:[I

    .line 1473
    new-array v0, v1, [C

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->bScaleInfo:[C

    .line 1474
    new-array v0, v1, [D

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->dLogBase:[D

    .line 1475
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->nUnitIndex:[I

    return-void
.end method


# virtual methods
.method clear()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1478
    iput v4, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->nChart:I

    iput v4, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->nType:I

    .line 1479
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    .line 1480
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->bExistAxes:[C

    aput-char v4, v1, v0

    .line 1481
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->bAxesInfo:[C

    aput-char v4, v1, v0

    .line 1482
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->nAlignment:[I

    aput v4, v1, v0

    .line 1483
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->bScaleInfo:[C

    aput-char v4, v1, v0

    .line 1484
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->dLogBase:[D

    const-wide/16 v2, 0x0

    aput-wide v2, v1, v0

    .line 1485
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->nUnitIndex:[I

    aput v4, v1, v0

    .line 1479
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1487
    :cond_0
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1490
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;

    .line 1491
    .local v0, "o":Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->bExistAxes:[C

    invoke-virtual {v1}, [C->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [C

    iput-object v1, v0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->bExistAxes:[C

    .line 1492
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->bAxesInfo:[C

    invoke-virtual {v1}, [C->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [C

    iput-object v1, v0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->bAxesInfo:[C

    .line 1493
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->nAlignment:[I

    invoke-virtual {v1}, [I->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    iput-object v1, v0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->nAlignment:[I

    .line 1494
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->bScaleInfo:[C

    invoke-virtual {v1}, [C->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [C

    iput-object v1, v0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->bScaleInfo:[C

    .line 1495
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->dLogBase:[D

    invoke-virtual {v1}, [D->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [D

    iput-object v1, v0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->dLogBase:[D

    .line 1496
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->nUnitIndex:[I

    invoke-virtual {v1}, [I->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    iput-object v1, v0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->nUnitIndex:[I

    .line 1497
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1466
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;->clone()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_AXESINFO_EVENT;

    move-result-object v0

    return-object v0
.end method
