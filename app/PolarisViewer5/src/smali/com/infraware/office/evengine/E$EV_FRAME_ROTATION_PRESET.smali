.class public interface abstract Lcom/infraware/office/evengine/E$EV_FRAME_ROTATION_PRESET;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_FRAME_ROTATION_PRESET"
.end annotation


# static fields
.field public static final BR_ROTATION_FLIP:I = 0x4

.field public static final BR_ROTATION_MIRROR:I = 0x5

.field public static final BR_ROTATION_MORE_180_DEGREE:I = 0x2

.field public static final BR_ROTATION_MORE_270_DEGREE:I = 0x3

.field public static final BR_ROTATION_MORE_90_DEGREE:I = 0x1

.field public static final BR_ROTATION_NONE:I
