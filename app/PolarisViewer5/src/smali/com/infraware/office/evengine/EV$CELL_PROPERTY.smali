.class public Lcom/infraware/office/evengine/EV$CELL_PROPERTY;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CELL_PROPERTY"
.end annotation


# instance fields
.field public a_BorderColor:I

.field public a_BorderLineStyle:I

.field public a_BorderLineWidthType:I

.field public a_CellBorderType:I

.field public a_CellColor:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 1602
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$CELL_PROPERTY;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 1611
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$CELL_PROPERTY;->a_BorderLineStyle:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$CELL_PROPERTY;->a_BorderLineWidthType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$CELL_PROPERTY;->a_CellBorderType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$CELL_PROPERTY;->a_BorderColor:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$CELL_PROPERTY;->a_CellColor:I

    .line 1612
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$CELL_PROPERTY;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1615
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$CELL_PROPERTY;

    .line 1616
    .local v0, "o":Lcom/infraware/office/evengine/EV$CELL_PROPERTY;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1602
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$CELL_PROPERTY;->clone()Lcom/infraware/office/evengine/EV$CELL_PROPERTY;

    move-result-object v0

    return-object v0
.end method
