.class public Lcom/infraware/office/evengine/EV$HeaderFooterOption;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "HeaderFooterOption"
.end annotation


# instance fields
.field public a_diffFirstPage:Z

.field public a_diffOddEvenPages:Z

.field public a_linkToPrevFooter:I

.field public a_linkToPrevHeader:I

.field public a_nTargetPageNum:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 1699
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$HeaderFooterOption;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 1709
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/office/evengine/EV$HeaderFooterOption;->a_linkToPrevFooter:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$HeaderFooterOption;->a_linkToPrevHeader:I

    .line 1710
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/evengine/EV$HeaderFooterOption;->a_diffOddEvenPages:Z

    iput-boolean v0, p0, Lcom/infraware/office/evengine/EV$HeaderFooterOption;->a_diffFirstPage:Z

    .line 1711
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$HeaderFooterOption;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1715
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$HeaderFooterOption;

    .line 1716
    .local v0, "o":Lcom/infraware/office/evengine/EV$HeaderFooterOption;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1699
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$HeaderFooterOption;->clone()Lcom/infraware/office/evengine/EV$HeaderFooterOption;

    move-result-object v0

    return-object v0
.end method
