.class public Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "EDITOR_OBJECT_POINTARRAY"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;,
        Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final MAX_ADJUSTHANDLE_ARRSIZE:I = 0xa

.field public static final MAX_SMART_GUIDES:I = 0x6


# instance fields
.field public bClipEnable:I

.field public clipEnd:Landroid/graphics/Point;

.field public clipStart:Landroid/graphics/Point;

.field public nAdjustHandleCnt:I

.field public nAnimationOrderCnt:I

.field public nConnectionPointCnt:I

.field public nIsLineEndPointLock:I

.field public nIsLineStartPointLock:I

.field public nMultiObj:I

.field public nObjPointCnt:I

.field public nSmartGuidesCnt:I

.field public ptAdjustHandlePoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

.field public ptAnimationOrder:[I

.field public ptConnectionPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

.field public ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

.field public ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

.field public ptSmartGuidesEnd:[Landroid/graphics/Point;

.field public ptSmartGuidesStart:[Landroid/graphics/Point;

.field public ptSmartGuidesType:[I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1966
    const-class v0, Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 6

    .prologue
    const/16 v5, 0xa

    const/4 v4, 0x6

    .line 1994
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1968
    new-instance v1, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    invoke-direct {v1, p0}, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;-><init>(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V

    iput-object v1, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    .line 1970
    const/4 v1, 0x2

    new-array v1, v1, [Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    iput-object v1, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    .line 1972
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->clipStart:Landroid/graphics/Point;

    .line 1973
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->clipEnd:Landroid/graphics/Point;

    .line 1979
    new-array v1, v5, [Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    iput-object v1, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptAdjustHandlePoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    .line 1980
    new-array v1, v5, [I

    iput-object v1, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptAnimationOrder:[I

    .line 1983
    new-array v1, v5, [Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    iput-object v1, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptConnectionPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    .line 1989
    new-array v1, v4, [I

    fill-array-data v1, :array_0

    iput-object v1, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptSmartGuidesType:[I

    .line 1990
    new-array v1, v4, [Landroid/graphics/Point;

    iput-object v1, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptSmartGuidesStart:[Landroid/graphics/Point;

    .line 1991
    new-array v1, v4, [Landroid/graphics/Point;

    iput-object v1, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptSmartGuidesEnd:[Landroid/graphics/Point;

    .line 1995
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    const/4 v2, 0x0

    new-instance v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    invoke-direct {v3, p0}, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;-><init>(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V

    aput-object v3, v1, v2

    .line 1996
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    const/4 v2, 0x1

    new-instance v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    invoke-direct {v3, p0}, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;-><init>(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V

    aput-object v3, v1, v2

    .line 1998
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v5, :cond_0

    .line 1999
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptAdjustHandlePoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    new-instance v2, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    invoke-direct {v2, p0}, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;-><init>(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V

    aput-object v2, v1, v0

    .line 2000
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptConnectionPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    new-instance v2, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    invoke-direct {v2, p0}, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;-><init>(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V

    aput-object v2, v1, v0

    .line 1998
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2002
    :cond_0
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_1

    .line 2003
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptSmartGuidesStart:[Landroid/graphics/Point;

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    aput-object v2, v1, v0

    .line 2004
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptSmartGuidesEnd:[Landroid/graphics/Point;

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    aput-object v2, v1, v0

    .line 2002
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2007
    :cond_1
    return-void

    .line 1989
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method clear()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 2211
    iput v2, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->nMultiObj:I

    .line 2212
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->clear()V

    .line 2213
    iput v2, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->nObjPointCnt:I

    .line 2214
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->clear()V

    .line 2215
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->clear()V

    .line 2217
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->clipStart:Landroid/graphics/Point;

    invoke-virtual {v0, v2, v2}, Landroid/graphics/Point;->set(II)V

    .line 2218
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->clipEnd:Landroid/graphics/Point;

    invoke-virtual {v0, v2, v2}, Landroid/graphics/Point;->set(II)V

    .line 2219
    iput v2, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->bClipEnable:I

    .line 2221
    iput v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->nAdjustHandleCnt:I

    .line 2222
    iput v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->nIsLineEndPointLock:I

    .line 2223
    iput v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->nIsLineStartPointLock:I

    .line 2224
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2227
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;

    .line 2228
    .local v0, "o":Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    invoke-virtual {v1}, [Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    iput-object v1, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    .line 2229
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->clone()Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-result-object v1

    iput-object v1, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    .line 2230
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1966
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->clone()Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;

    move-result-object v0

    return-object v0
.end method

.method setValue([I)Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;
    .locals 8
    .param p1, "param"    # [I

    .prologue
    const/16 v7, 0xa

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 2096
    sget-boolean v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    array-length v3, p1

    const/16 v4, 0x6b

    if-eq v3, v4, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 2097
    :cond_0
    const/4 v1, 0x0

    .line 2098
    .local v1, "n":I
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .local v2, "n":I
    aget v3, p1, v1

    iput v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->nMultiObj:I

    .line 2099
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startPoint:Landroid/graphics/Point;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v4, p1, v2

    iput v4, v3, Landroid/graphics/Point;->x:I

    .line 2100
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startPoint:Landroid/graphics/Point;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v4, p1, v1

    iput v4, v3, Landroid/graphics/Point;->y:I

    .line 2101
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->endPoint:Landroid/graphics/Point;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v4, p1, v2

    iput v4, v3, Landroid/graphics/Point;->x:I

    .line 2102
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->endPoint:Landroid/graphics/Point;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v4, p1, v1

    iput v4, v3, Landroid/graphics/Point;->y:I

    .line 2104
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v4, p1, v2

    iput v4, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nRotateAngle:I

    .line 2106
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->sObjectSize:Landroid/graphics/Point;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v4, p1, v1

    iput v4, v3, Landroid/graphics/Point;->x:I

    .line 2107
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->sObjectSize:Landroid/graphics/Point;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v4, p1, v2

    iput v4, v3, Landroid/graphics/Point;->y:I

    .line 2108
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v4, p1, v1

    iput v4, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nObjectType:I

    .line 2110
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v4, p1, v2

    iput v4, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nObjectEditInfo:I

    .line 2112
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v4, p1, v1

    iput v4, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->eEditing:I

    .line 2113
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v4, p1, v2

    iput v4, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->eController:I

    .line 2115
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingStartPoint:Landroid/graphics/Point;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v4, p1, v1

    iput v4, v3, Landroid/graphics/Point;->x:I

    .line 2116
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingStartPoint:Landroid/graphics/Point;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v4, p1, v2

    iput v4, v3, Landroid/graphics/Point;->y:I

    .line 2117
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingEndPoint:Landroid/graphics/Point;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v4, p1, v1

    iput v4, v3, Landroid/graphics/Point;->x:I

    .line 2118
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingEndPoint:Landroid/graphics/Point;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v4, p1, v2

    iput v4, v3, Landroid/graphics/Point;->y:I

    .line 2119
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v4, p1, v1

    iput v4, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nEditingAngle:I

    .line 2121
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v4, p1, v2

    iput v4, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nFrameID:I

    .line 2124
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startPointFromPage:Landroid/graphics/Point;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v4, p1, v1

    iput v4, v3, Landroid/graphics/Point;->x:I

    .line 2125
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startPointFromPage:Landroid/graphics/Point;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v4, p1, v2

    iput v4, v3, Landroid/graphics/Point;->y:I

    .line 2127
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v4, p1, v1

    iput v4, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->bRotationEnabled:I

    .line 2128
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v4, p1, v2

    iput v4, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->bGroupEnabled:I

    .line 2129
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v4, p1, v1

    iput v4, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->bPureImage:I

    .line 2131
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->clipStart:Landroid/graphics/Point;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v4, p1, v2

    iput v4, v3, Landroid/graphics/Point;->x:I

    .line 2132
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->clipStart:Landroid/graphics/Point;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v4, p1, v1

    iput v4, v3, Landroid/graphics/Point;->y:I

    .line 2133
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->clipEnd:Landroid/graphics/Point;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v4, p1, v2

    iput v4, v3, Landroid/graphics/Point;->x:I

    .line 2134
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->clipEnd:Landroid/graphics/Point;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v4, p1, v1

    iput v4, v3, Landroid/graphics/Point;->y:I

    .line 2135
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v3, p1, v2

    iput v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->bClipEnable:I

    .line 2136
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v4, p1, v1

    iput v4, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nMarkingRectCount:I

    .line 2138
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v3, p1, v2

    iput v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->nObjPointCnt:I

    .line 2139
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    aget-object v3, v3, v6

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v4, p1, v1

    iput v4, v3, Landroid/graphics/Point;->x:I

    .line 2140
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    aget-object v3, v3, v6

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v4, p1, v2

    iput v4, v3, Landroid/graphics/Point;->y:I

    .line 2141
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    aget-object v3, v3, v6

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v4, p1, v1

    iput v4, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->nObjectType:I

    .line 2142
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v4, p1, v2

    iput v4, v3, Landroid/graphics/Point;->x:I

    .line 2143
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v4, p1, v1

    iput v4, v3, Landroid/graphics/Point;->y:I

    .line 2144
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    aget-object v3, v3, v5

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v4, p1, v2

    iput v4, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->nObjectType:I

    .line 2145
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v4, p1, v1

    iput v4, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startMarkDirection:I

    .line 2146
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v4, p1, v2

    iput v4, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->endMarkDirection:I

    .line 2149
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v3, p1, v1

    if-ne v3, v5, :cond_1

    .line 2150
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    iput-boolean v5, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->bDML:Z

    .line 2154
    :goto_0
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v3, p1, v2

    if-ne v3, v5, :cond_2

    .line 2155
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    iput-boolean v5, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->b2007DocxVML:Z

    .line 2159
    :goto_1
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v3, p1, v1

    if-ne v3, v5, :cond_3

    .line 2160
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    iput-boolean v5, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->bSupport3D:Z

    .line 2164
    :goto_2
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v3, p1, v2

    if-ne v3, v5, :cond_4

    .line 2165
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    iput-boolean v5, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->bSupport3DBevel:Z

    .line 2169
    :goto_3
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v3, p1, v1

    if-ne v3, v5, :cond_5

    .line 2170
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    iput-boolean v5, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->b3D:Z

    .line 2173
    :goto_4
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v3, p1, v2

    if-ne v3, v5, :cond_6

    .line 2174
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    iput-boolean v5, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->b3DBevel:Z

    .line 2178
    :goto_5
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v3, p1, v1

    iput v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->nSmartGuidesCnt:I

    .line 2179
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_6
    const/4 v3, 0x6

    if-ge v0, v3, :cond_7

    .line 2180
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptSmartGuidesType:[I

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v4, p1, v2

    aput v4, v3, v0

    .line 2181
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptSmartGuidesStart:[Landroid/graphics/Point;

    aget-object v3, v3, v0

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v4, p1, v1

    iput v4, v3, Landroid/graphics/Point;->x:I

    .line 2182
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptSmartGuidesStart:[Landroid/graphics/Point;

    aget-object v3, v3, v0

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v4, p1, v2

    iput v4, v3, Landroid/graphics/Point;->y:I

    .line 2183
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptSmartGuidesEnd:[Landroid/graphics/Point;

    aget-object v3, v3, v0

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v4, p1, v1

    iput v4, v3, Landroid/graphics/Point;->x:I

    .line 2184
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptSmartGuidesEnd:[Landroid/graphics/Point;

    aget-object v3, v3, v0

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v4, p1, v2

    iput v4, v3, Landroid/graphics/Point;->y:I

    .line 2179
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    goto :goto_6

    .line 2152
    .end local v0    # "i":I
    :cond_1
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    iput-boolean v6, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->bDML:Z

    goto :goto_0

    .line 2157
    .end local v2    # "n":I
    .restart local v1    # "n":I
    :cond_2
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    iput-boolean v6, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->b2007DocxVML:Z

    goto :goto_1

    .line 2162
    .end local v1    # "n":I
    .restart local v2    # "n":I
    :cond_3
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    iput-boolean v6, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->bSupport3D:Z

    goto :goto_2

    .line 2167
    .end local v2    # "n":I
    .restart local v1    # "n":I
    :cond_4
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    iput-boolean v6, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->bSupport3DBevel:Z

    goto :goto_3

    .line 2172
    .end local v1    # "n":I
    .restart local v2    # "n":I
    :cond_5
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    iput-boolean v6, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->b3D:Z

    goto :goto_4

    .line 2176
    .end local v2    # "n":I
    .restart local v1    # "n":I
    :cond_6
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    iput-boolean v6, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->b3DBevel:Z

    goto :goto_5

    .line 2187
    .end local v1    # "n":I
    .restart local v0    # "i":I
    .restart local v2    # "n":I
    :cond_7
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v3, p1, v2

    iput v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->nAdjustHandleCnt:I

    .line 2188
    const/4 v0, 0x0

    move v2, v1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    :goto_7
    if-ge v0, v7, :cond_8

    .line 2189
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptAdjustHandlePoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v4, p1, v2

    iput v4, v3, Landroid/graphics/Point;->x:I

    .line 2190
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptAdjustHandlePoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v4, p1, v1

    iput v4, v3, Landroid/graphics/Point;->y:I

    .line 2188
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 2192
    :cond_8
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v3, p1, v2

    iput v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->nAnimationOrderCnt:I

    .line 2193
    const/4 v0, 0x0

    move v2, v1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    :goto_8
    if-ge v0, v7, :cond_9

    .line 2194
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptAnimationOrder:[I

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v4, p1, v2

    aput v4, v3, v0

    .line 2193
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    goto :goto_8

    .line 2199
    :cond_9
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v3, p1, v2

    iput v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->nConnectionPointCnt:I

    .line 2200
    const/4 v0, 0x0

    move v2, v1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    :goto_9
    if-ge v0, v7, :cond_a

    .line 2201
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptConnectionPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v4, p1, v2

    iput v4, v3, Landroid/graphics/Point;->x:I

    .line 2202
    iget-object v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptConnectionPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v4, p1, v1

    iput v4, v3, Landroid/graphics/Point;->y:I

    .line 2200
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 2205
    :cond_a
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    aget v3, p1, v2

    iput v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->nIsLineStartPointLock:I

    .line 2206
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget v3, p1, v1

    iput v3, p0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->nIsLineEndPointLock:I

    .line 2207
    return-object p0
.end method
