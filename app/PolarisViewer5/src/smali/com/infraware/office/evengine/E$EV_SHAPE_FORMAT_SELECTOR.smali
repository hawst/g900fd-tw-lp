.class public interface abstract Lcom/infraware/office/evengine/E$EV_SHAPE_FORMAT_SELECTOR;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_SHAPE_FORMAT_SELECTOR"
.end annotation


# static fields
.field public static final eEV_SHAPE_FORMAT_3D_FORMAT:I = 0x100

.field public static final eEV_SHAPE_FORMAT_3D_ROATION:I = 0x200

.field public static final eEV_SHAPE_FORMAT_ALL:I = 0x1ffff

.field public static final eEV_SHAPE_FORMAT_ARTISITC_EFFECT:I = 0x1000

.field public static final eEV_SHAPE_FORMAT_CROPPING:I = 0x2000

.field public static final eEV_SHAPE_FORMAT_FILL:I = 0x2

.field public static final eEV_SHAPE_FORMAT_GLOW:I = 0x40

.field public static final eEV_SHAPE_FORMAT_INVALID:I = 0x0

.field public static final eEV_SHAPE_FORMAT_LINE_COLOR:I = 0x4

.field public static final eEV_SHAPE_FORMAT_LINE_STYLE:I = 0x8

.field public static final eEV_SHAPE_FORMAT_LOCATION:I = 0x8000

.field public static final eEV_SHAPE_FORMAT_PICTURE_COLOR:I = 0x800

.field public static final eEV_SHAPE_FORMAT_PICTURE_CORRECTION:I = 0x400

.field public static final eEV_SHAPE_FORMAT_QUICK_STYLE:I = 0x1

.field public static final eEV_SHAPE_FORMAT_REFLECTION:I = 0x20

.field public static final eEV_SHAPE_FORMAT_SHADOW:I = 0x10

.field public static final eEV_SHAPE_FORMAT_SIZE:I = 0x4000

.field public static final eEV_SHAPE_FORMAT_SOFTEDGE:I = 0x80

.field public static final eEV_SHAPE_FORMAT_TEXTBOX:I = 0x10000
