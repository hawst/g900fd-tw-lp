.class public Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SET_PARAATT_INFO"
.end annotation


# instance fields
.field public FirstLineType:I

.field public FirstLineValue:I

.field public ParaBottomValue:I

.field public ParaTopValue:I

.field public nHAlignType:I

.field public nLeftMarginValue:I

.field public nLineHeight:I

.field public nLineSpace:I

.field public nLineSpaceValue:I

.field public nParaDirection:I

.field public nRightMarginValue:I

.field public nTextFlow:I

.field public nType:I

.field public nVAlignType:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 1112
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 1130
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;->nTextFlow:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;->nParaDirection:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;->nLineHeight:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;->ParaBottomValue:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;->ParaTopValue:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;->nLineSpaceValue:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;->nLineSpace:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;->FirstLineValue:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;->FirstLineType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;->nRightMarginValue:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;->nLeftMarginValue:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;->nHAlignType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;->nVAlignType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;->nType:I

    .line 1133
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1136
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;

    .line 1137
    .local v0, "o":Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1112
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;->clone()Lcom/infraware/office/evengine/EV$SET_PARAATT_INFO;

    move-result-object v0

    return-object v0
.end method
