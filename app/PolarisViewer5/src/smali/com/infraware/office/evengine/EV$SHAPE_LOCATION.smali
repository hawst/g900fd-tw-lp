.class public Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SHAPE_LOCATION"
.end annotation


# instance fields
.field public nHorizontalDistance:F

.field public nHorizontalLocationFrom:I

.field public nVerticalDistance:F

.field public nVerticalLocationFrom:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 2706
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 2714
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;->nVerticalLocationFrom:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;->nHorizontalLocationFrom:I

    int-to-float v0, v0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;->nVerticalDistance:F

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;->nHorizontalDistance:F

    .line 2715
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2719
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;

    .line 2720
    .local v0, "o":Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2706
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;->clone()Lcom/infraware/office/evengine/EV$SHAPE_LOCATION;

    move-result-object v0

    return-object v0
.end method
