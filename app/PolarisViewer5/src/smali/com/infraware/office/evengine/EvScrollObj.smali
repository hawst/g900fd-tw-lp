.class Lcom/infraware/office/evengine/EvScrollObj;
.super Lcom/infraware/office/evengine/EvTaskObj;
.source "EvTaskObj.java"


# instance fields
.field private m_eFactor:I

.field private m_eKey:I

.field private m_eScrollType:I

.field private m_nOffsetX:I

.field private m_nOffsetY:I


# direct methods
.method constructor <init>(Lcom/infraware/office/evengine/EvNative;IIIII)V
    .locals 0
    .param p1, "a_Native"    # Lcom/infraware/office/evengine/EvNative;
    .param p2, "a_eScrollType"    # I
    .param p3, "a_eFactor"    # I
    .param p4, "a_nOffsetX"    # I
    .param p5, "a_nOffsetY"    # I
    .param p6, "a_eKey"    # I

    .prologue
    .line 288
    invoke-direct {p0, p1}, Lcom/infraware/office/evengine/EvTaskObj;-><init>(Lcom/infraware/office/evengine/EvNative;)V

    .line 289
    iput p2, p0, Lcom/infraware/office/evengine/EvScrollObj;->m_eScrollType:I

    .line 290
    iput p3, p0, Lcom/infraware/office/evengine/EvScrollObj;->m_eFactor:I

    .line 291
    iput p4, p0, Lcom/infraware/office/evengine/EvScrollObj;->m_nOffsetX:I

    .line 292
    iput p5, p0, Lcom/infraware/office/evengine/EvScrollObj;->m_nOffsetY:I

    .line 293
    iput p6, p0, Lcom/infraware/office/evengine/EvScrollObj;->m_eKey:I

    .line 294
    return-void
.end method


# virtual methods
.method doTask()V
    .locals 6

    .prologue
    .line 298
    iget-object v0, p0, Lcom/infraware/office/evengine/EvScrollObj;->Native:Lcom/infraware/office/evengine/EvNative;

    iget v1, p0, Lcom/infraware/office/evengine/EvScrollObj;->m_eScrollType:I

    iget v2, p0, Lcom/infraware/office/evengine/EvScrollObj;->m_eFactor:I

    iget v3, p0, Lcom/infraware/office/evengine/EvScrollObj;->m_nOffsetX:I

    iget v4, p0, Lcom/infraware/office/evengine/EvScrollObj;->m_nOffsetY:I

    iget v5, p0, Lcom/infraware/office/evengine/EvScrollObj;->m_eKey:I

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvNative;->IScroll(IIIII)V

    .line 299
    return-void
.end method
