.class public Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SHAPE_EFFECT"
.end annotation


# instance fields
.field public n3DFormat:I

.field public n3DRotation:I

.field public nMask:I

.field public nNeon:I

.field public nNeonColor:I

.field public nNeonSize:I

.field public nReflect:I

.field public nReflectSize:I

.field public nReflectTrans:I

.field public nShadow:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 2241
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 2254
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;->n3DRotation:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;->n3DFormat:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;->nNeonSize:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;->nNeonColor:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;->nNeon:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;->nReflectSize:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;->nReflectTrans:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;->nReflect:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;->nShadow:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;->nMask:I

    .line 2256
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2260
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;

    .line 2261
    .local v0, "o":Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2241
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;->clone()Lcom/infraware/office/evengine/EV$SHAPE_EFFECT;

    move-result-object v0

    return-object v0
.end method
