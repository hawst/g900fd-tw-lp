.class public interface abstract Lcom/infraware/office/evengine/E$EV_SLIDE_MANAGE_TYPE;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_SLIDE_MANAGE_TYPE"
.end annotation


# static fields
.field public static final eEV_GUI_PPT_SLIDECOPY_EVENT:I = 0x7

.field public static final eEV_GUI_PPT_SLIDECUT_EVENT:I = 0x8

.field public static final eEV_GUI_PPT_SLIDEDELETE_EVENT:I = 0x2

.field public static final eEV_GUI_PPT_SLIDEDUPLICATE_EVENT:I = 0x0

.field public static final eEV_GUI_PPT_SLIDEINSERT_EVENT:I = 0x1

.field public static final eEV_GUI_PPT_SLIDELAYOUT_EVENT:I = 0x6

.field public static final eEV_GUI_PPT_SLIDEMOVEEX_EVENT:I = 0x5

.field public static final eEV_GUI_PPT_SLIDEMOVENEXT_EVENT:I = 0x3

.field public static final eEV_GUI_PPT_SLIDEMOVEPREV_EVENT:I = 0x4

.field public static final eEV_GUI_PPT_SLIDEPASTE_EVENT:I = 0x9
