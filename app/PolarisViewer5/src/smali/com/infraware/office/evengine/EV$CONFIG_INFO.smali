.class public Lcom/infraware/office/evengine/EV$CONFIG_INFO;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CONFIG_INFO"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/office/evengine/EV$CONFIG_INFO$SCROLLINFO;
    }
.end annotation


# instance fields
.field public bBGLoad:I

.field public bContinuousMode:I

.field public nCurPage:I

.field public nDocExtType:I

.field public nFitToHeightZoomValue:I

.field public nFitToWidthZoomValue:I

.field public nMaxZoom:I

.field public nMinZoom:I

.field public nOnlyOnePage:I

.field public nReflowState:I

.field public nRotateAngle:I

.field public nTotalPages:I

.field public nZoomLevel:I

.field public nZoomRatio:I

.field public scrollInfo:Lcom/infraware/office/evengine/EV$CONFIG_INFO$SCROLLINFO;

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 1

    .prologue
    .line 590
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO$SCROLLINFO;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EV$CONFIG_INFO$SCROLLINFO;-><init>(Lcom/infraware/office/evengine/EV$CONFIG_INFO;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->scrollInfo:Lcom/infraware/office/evengine/EV$CONFIG_INFO$SCROLLINFO;

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 611
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nOnlyOnePage:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nDocExtType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nFitToHeightZoomValue:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nFitToWidthZoomValue:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->bContinuousMode:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomLevel:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nReflowState:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nMaxZoom:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nMinZoom:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nRotateAngle:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->bBGLoad:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nTotalPages:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    .line 615
    iget-object v0, p0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->scrollInfo:Lcom/infraware/office/evengine/EV$CONFIG_INFO$SCROLLINFO;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$CONFIG_INFO$SCROLLINFO;->clear()V

    .line 616
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$CONFIG_INFO;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 619
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    .line 620
    .local v0, "o":Lcom/infraware/office/evengine/EV$CONFIG_INFO;
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->scrollInfo:Lcom/infraware/office/evengine/EV$CONFIG_INFO$SCROLLINFO;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV$CONFIG_INFO$SCROLLINFO;->clone()Lcom/infraware/office/evengine/EV$CONFIG_INFO$SCROLLINFO;

    move-result-object v1

    iput-object v1, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->scrollInfo:Lcom/infraware/office/evengine/EV$CONFIG_INFO$SCROLLINFO;

    .line 621
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 573
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->clone()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    return-object v0
.end method
