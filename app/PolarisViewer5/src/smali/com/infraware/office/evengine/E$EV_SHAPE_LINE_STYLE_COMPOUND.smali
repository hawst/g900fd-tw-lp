.class public interface abstract Lcom/infraware/office/evengine/E$EV_SHAPE_LINE_STYLE_COMPOUND;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_SHAPE_LINE_STYLE_COMPOUND"
.end annotation


# static fields
.field public static final BR_LINE_STYLE_COMPOUND_DOUBLE:I = 0x2

.field public static final BR_LINE_STYLE_COMPOUND_INVALID:I = 0x0

.field public static final BR_LINE_STYLE_COMPOUND_SIMPLE:I = 0x1

.field public static final BR_LINE_STYLE_COMPOUND_THICKTHIN:I = 0x3

.field public static final BR_LINE_STYLE_COMPOUND_THINTHICK:I = 0x4

.field public static final BR_LINE_STYLE_COMPOUND_TRIPLE:I = 0x5
