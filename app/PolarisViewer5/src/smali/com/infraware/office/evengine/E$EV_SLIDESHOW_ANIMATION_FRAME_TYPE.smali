.class public interface abstract Lcom/infraware/office/evengine/E$EV_SLIDESHOW_ANIMATION_FRAME_TYPE;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_SLIDESHOW_ANIMATION_FRAME_TYPE"
.end annotation


# static fields
.field public static final eEV_SLIDE_ANIMATION_FRAME_10POINT_STAR:I = 0x7e

.field public static final eEV_SLIDE_ANIMATION_FRAME_12POINT_STAR:I = 0x7f

.field public static final eEV_SLIDE_ANIMATION_FRAME_16POINT_STAR:I = 0x80

.field public static final eEV_SLIDE_ANIMATION_FRAME_24POINT_STAR:I = 0x81

.field public static final eEV_SLIDE_ANIMATION_FRAME_32POINT_STAR:I = 0x82

.field public static final eEV_SLIDE_ANIMATION_FRAME_4POINT_STAR:I = 0x79

.field public static final eEV_SLIDE_ANIMATION_FRAME_5POINT_STAR:I = 0x7a

.field public static final eEV_SLIDE_ANIMATION_FRAME_6POINT_STAR:I = 0x7b

.field public static final eEV_SLIDE_ANIMATION_FRAME_7POINT_STAR:I = 0x7c

.field public static final eEV_SLIDE_ANIMATION_FRAME_8POINT_STAR:I = 0x7d

.field public static final eEV_SLIDE_ANIMATION_FRAME_ACTIONBUTTONS_BACK_OR_PREVIOUS:I = 0x9b

.field public static final eEV_SLIDE_ANIMATION_FRAME_ACTIONBUTTONS_BEGINNING:I = 0x9d

.field public static final eEV_SLIDE_ANIMATION_FRAME_ACTIONBUTTONS_CUSTOM:I = 0xa6

.field public static final eEV_SLIDE_ANIMATION_FRAME_ACTIONBUTTONS_DOCUMENT:I = 0xa3

.field public static final eEV_SLIDE_ANIMATION_FRAME_ACTIONBUTTONS_END:I = 0x9e

.field public static final eEV_SLIDE_ANIMATION_FRAME_ACTIONBUTTONS_FORWARD_OR_NEXT:I = 0x9c

.field public static final eEV_SLIDE_ANIMATION_FRAME_ACTIONBUTTONS_HELP:I = 0xa5

.field public static final eEV_SLIDE_ANIMATION_FRAME_ACTIONBUTTONS_HOME:I = 0x9f

.field public static final eEV_SLIDE_ANIMATION_FRAME_ACTIONBUTTONS_INFORMATION:I = 0xa0

.field public static final eEV_SLIDE_ANIMATION_FRAME_ACTIONBUTTONS_MOVIE:I = 0xa2

.field public static final eEV_SLIDE_ANIMATION_FRAME_ACTIONBUTTONS_RETURN:I = 0xa1

.field public static final eEV_SLIDE_ANIMATION_FRAME_ACTIONBUTTONS_SOUND:I = 0xa4

.field public static final eEV_SLIDE_ANIMATION_FRAME_ARC:I = 0x33

.field public static final eEV_SLIDE_ANIMATION_FRAME_AUDIO:I = 0xac

.field public static final eEV_SLIDE_ANIMATION_FRAME_BENT_ARROW:I = 0x42

.field public static final eEV_SLIDE_ANIMATION_FRAME_BENT_UP_ARROW:I = 0x45

.field public static final eEV_SLIDE_ANIMATION_FRAME_BEVEL:I = 0x28

.field public static final eEV_SLIDE_ANIMATION_FRAME_BLOCK_ARC:I = 0x2b

.field public static final eEV_SLIDE_ANIMATION_FRAME_CAN:I = 0x26

.field public static final eEV_SLIDE_ANIMATION_FRAME_CHART:I = 0xaa

.field public static final eEV_SLIDE_ANIMATION_FRAME_CHEVRON_UP:I = 0x4d

.field public static final eEV_SLIDE_ANIMATION_FRAME_CHORD:I = 0x1e

.field public static final eEV_SLIDE_ANIMATION_FRAME_CIRCULAR_ARROW:I = 0x54

.field public static final eEV_SLIDE_ANIMATION_FRAME_CLOUD:I = 0x32

.field public static final eEV_SLIDE_ANIMATION_FRAME_CLOUD_CALLOUT:I = 0x8e

.field public static final eEV_SLIDE_ANIMATION_FRAME_CUBE:I = 0x27

.field public static final eEV_SLIDE_ANIMATION_FRAME_CURVED_CONNECTOR:I = 0x4

.field public static final eEV_SLIDE_ANIMATION_FRAME_CURVED_DOWN_ARROW:I = 0x49

.field public static final eEV_SLIDE_ANIMATION_FRAME_CURVED_DOWN_RIBBON:I = 0x86

.field public static final eEV_SLIDE_ANIMATION_FRAME_CURVED_LEFT_ARROW:I = 0x47

.field public static final eEV_SLIDE_ANIMATION_FRAME_CURVED_RIGHT_ARROW:I = 0x46

.field public static final eEV_SLIDE_ANIMATION_FRAME_CURVED_UP_ARROW:I = 0x48

.field public static final eEV_SLIDE_ANIMATION_FRAME_CURVED_UP_RIBBON:I = 0x85

.field public static final eEV_SLIDE_ANIMATION_FRAME_DECAGON:I = 0x1b

.field public static final eEV_SLIDE_ANIMATION_FRAME_DIAGONAL_STRIPE:I = 0x23

.field public static final eEV_SLIDE_ANIMATION_FRAME_DIAGRAM:I = 0xa9

.field public static final eEV_SLIDE_ANIMATION_FRAME_DIAMOND:I = 0x16

.field public static final eEV_SLIDE_ANIMATION_FRAME_DODECAGON:I = 0x1c

.field public static final eEV_SLIDE_ANIMATION_FRAME_DONUT:I = 0x29

.field public static final eEV_SLIDE_ANIMATION_FRAME_DOUBLE_BRACE:I = 0x35

.field public static final eEV_SLIDE_ANIMATION_FRAME_DOUBLE_BRACKET:I = 0x34

.field public static final eEV_SLIDE_ANIMATION_FRAME_DOUBLE_WAVE:I = 0x8a

.field public static final eEV_SLIDE_ANIMATION_FRAME_DOWN_ARROW:I = 0x3d

.field public static final eEV_SLIDE_ANIMATION_FRAME_DOWN_ARROW_CALLOUT:I = 0x4f

.field public static final eEV_SLIDE_ANIMATION_FRAME_DOWN_RIBBON:I = 0x84

.field public static final eEV_SLIDE_ANIMATION_FRAME_ELBOW_CONNECTOR:I = 0x3

.field public static final eEV_SLIDE_ANIMATION_FRAME_EQUATION_DIVISION:I = 0x58

.field public static final eEV_SLIDE_ANIMATION_FRAME_EQUATION_EQUAL:I = 0x59

.field public static final eEV_SLIDE_ANIMATION_FRAME_EQUATION_MINUS:I = 0x56

.field public static final eEV_SLIDE_ANIMATION_FRAME_EQUATION_MULTIPLY:I = 0x57

.field public static final eEV_SLIDE_ANIMATION_FRAME_EQUATION_NOT_EQUAL:I = 0x5a

.field public static final eEV_SLIDE_ANIMATION_FRAME_EQUATION_PLUS:I = 0x55

.field public static final eEV_SLIDE_ANIMATION_FRAME_EXPLOSION1:I = 0x77

.field public static final eEV_SLIDE_ANIMATION_FRAME_EXPLOSION2:I = 0x78

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_ALTERNATE_PROCESS:I = 0x5c

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_CARD:I = 0x69

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_COLLATE:I = 0x6d

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_CONNECTOR:I = 0x67

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_DATA:I = 0x5e

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_DECISION:I = 0x5d

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_DELAY:I = 0x72

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_DIRECT_ACCESS_STORAGE:I = 0x75

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_DISPLAY:I = 0x76

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_DOCUMENT:I = 0x61

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_EXTRACT:I = 0x6f

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_INTERNAL_STORAGE:I = 0x60

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_MAGNETIC_DISK:I = 0x74

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_MANUAL_INPUT:I = 0x65

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_MANUAL_OPERATION:I = 0x66

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_MERGE:I = 0x70

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_MULTIDOCUMENT:I = 0x62

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_OFF_PAGE_CONNECTOR:I = 0x68

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_OR:I = 0x6c

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_PREDEFINED_PROCESS:I = 0x5f

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_PREPARATION:I = 0x64

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_PROCESS:I = 0x5b

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_PUNCHED_TAPE:I = 0x6a

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_SEQUENTIAL_ACCESS_STORAGE:I = 0x73

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_SORT:I = 0x6e

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_STORED_DATA:I = 0x71

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_SUMMING_JUNCTION:I = 0x6b

.field public static final eEV_SLIDE_ANIMATION_FRAME_FLOWCHART_TERMINATOR:I = 0x63

.field public static final eEV_SLIDE_ANIMATION_FRAME_FOLDED_CORNER:I = 0x2c

.field public static final eEV_SLIDE_ANIMATION_FRAME_FRAME:I = 0x20

.field public static final eEV_SLIDE_ANIMATION_FRAME_FREEFORM:I = 0x5

.field public static final eEV_SLIDE_ANIMATION_FRAME_GROUP:I = 0xad

.field public static final eEV_SLIDE_ANIMATION_FRAME_HALF_FRAME:I = 0x21

.field public static final eEV_SLIDE_ANIMATION_FRAME_HEART:I = 0x2e

.field public static final eEV_SLIDE_ANIMATION_FRAME_HEPTAGON:I = 0x19

.field public static final eEV_SLIDE_ANIMATION_FRAME_HEXAGON:I = 0x18

.field public static final eEV_SLIDE_ANIMATION_FRAME_HORIZONTAL_SCROLL:I = 0x88

.field public static final eEV_SLIDE_ANIMATION_FRAME_ISOSCELES_TRIANGLE:I = 0x12

.field public static final eEV_SLIDE_ANIMATION_FRAME_LEFT_ARROW:I = 0x3b

.field public static final eEV_SLIDE_ANIMATION_FRAME_LEFT_ARROW_CALLOUT:I = 0x50

.field public static final eEV_SLIDE_ANIMATION_FRAME_LEFT_BRACE:I = 0x38

.field public static final eEV_SLIDE_ANIMATION_FRAME_LEFT_BRACKET:I = 0x36

.field public static final eEV_SLIDE_ANIMATION_FRAME_LEFT_RIGHT_ARROW:I = 0x3e

.field public static final eEV_SLIDE_ANIMATION_FRAME_LEFT_RIGHT_ARROW_CALLOUT:I = 0x52

.field public static final eEV_SLIDE_ANIMATION_FRAME_LEFT_RIGHT_UP_ARROW:I = 0x41

.field public static final eEV_SLIDE_ANIMATION_FRAME_LEFT_UP_ARROW:I = 0x44

.field public static final eEV_SLIDE_ANIMATION_FRAME_LIGHTNING_BOLT:I = 0x2f

.field public static final eEV_SLIDE_ANIMATION_FRAME_LINE:I = 0x1

.field public static final eEV_SLIDE_ANIMATION_FRAME_LINE_CALLOUT1:I = 0x8f

.field public static final eEV_SLIDE_ANIMATION_FRAME_LINE_CALLOUT1_ACCENT_BAR:I = 0x92

.field public static final eEV_SLIDE_ANIMATION_FRAME_LINE_CALLOUT1_BORDER_AND_ACCENT_BAR:I = 0x98

.field public static final eEV_SLIDE_ANIMATION_FRAME_LINE_CALLOUT1_NO_BORDER:I = 0x95

.field public static final eEV_SLIDE_ANIMATION_FRAME_LINE_CALLOUT2:I = 0x90

.field public static final eEV_SLIDE_ANIMATION_FRAME_LINE_CALLOUT2_ACCENT_BAR:I = 0x93

.field public static final eEV_SLIDE_ANIMATION_FRAME_LINE_CALLOUT2_BORDER_AND_ACCENT_BAR:I = 0x99

.field public static final eEV_SLIDE_ANIMATION_FRAME_LINE_CALLOUT2_NO_BORDER:I = 0x96

.field public static final eEV_SLIDE_ANIMATION_FRAME_LINE_CALLOUT3:I = 0x91

.field public static final eEV_SLIDE_ANIMATION_FRAME_LINE_CALLOUT3_ACCENT_BAR:I = 0x94

.field public static final eEV_SLIDE_ANIMATION_FRAME_LINE_CALLOUT3_BORDER_AND_ACCENT_BAR:I = 0x9a

.field public static final eEV_SLIDE_ANIMATION_FRAME_LINE_CALLOUT3_NO_BORDER:I = 0x97

.field public static final eEV_SLIDE_ANIMATION_FRAME_L_SHAPE:I = 0x22

.field public static final eEV_SLIDE_ANIMATION_FRAME_MOON:I = 0x31

.field public static final eEV_SLIDE_ANIMATION_FRAME_NORMAL:I = 0x0

.field public static final eEV_SLIDE_ANIMATION_FRAME_NOTCHED_RIGHT_ARROW:I = 0x4b

.field public static final eEV_SLIDE_ANIMATION_FRAME_NO_SYMBOL:I = 0x2a

.field public static final eEV_SLIDE_ANIMATION_FRAME_OBJECT:I = 0xae

.field public static final eEV_SLIDE_ANIMATION_FRAME_OCTAGON:I = 0x1a

.field public static final eEV_SLIDE_ANIMATION_FRAME_OVAL:I = 0x11

.field public static final eEV_SLIDE_ANIMATION_FRAME_OVAL_CALLOUT:I = 0x8d

.field public static final eEV_SLIDE_ANIMATION_FRAME_PARALLELOGRAM:I = 0x14

.field public static final eEV_SLIDE_ANIMATION_FRAME_PENTAGON:I = 0x4c

.field public static final eEV_SLIDE_ANIMATION_FRAME_PICTURE:I = 0xa8

.field public static final eEV_SLIDE_ANIMATION_FRAME_PIE:I = 0x1d

.field public static final eEV_SLIDE_ANIMATION_FRAME_PLACEHOLDER_OBJECT:I = 0xb1

.field public static final eEV_SLIDE_ANIMATION_FRAME_PLACEHOLDER_PICTURE:I = 0xb3

.field public static final eEV_SLIDE_ANIMATION_FRAME_PLACEHOLDER_SUBTITLE:I = 0xb0

.field public static final eEV_SLIDE_ANIMATION_FRAME_PLACEHOLDER_TEXT:I = 0xb2

.field public static final eEV_SLIDE_ANIMATION_FRAME_PLACEHOLDER_TITLE:I = 0xaf

.field public static final eEV_SLIDE_ANIMATION_FRAME_PLACEHOLDER_VERTICAL_TEXT:I = 0xb5

.field public static final eEV_SLIDE_ANIMATION_FRAME_PLACEHOLDER_VERTICAL_TITLE:I = 0xb4

.field public static final eEV_SLIDE_ANIMATION_FRAME_PLAQUE:I = 0x25

.field public static final eEV_SLIDE_ANIMATION_FRAME_PLUS:I = 0x24

.field public static final eEV_SLIDE_ANIMATION_FRAME_QUAD_ARROW:I = 0x40

.field public static final eEV_SLIDE_ANIMATION_FRAME_QUAD_ARROW_CALLOUT:I = 0x53

.field public static final eEV_SLIDE_ANIMATION_FRAME_RECTANGLE:I = 0x6

.field public static final eEV_SLIDE_ANIMATION_FRAME_RECTANGULAR_CALLOUT:I = 0x8b

.field public static final eEV_SLIDE_ANIMATION_FRAME_REGULAR_PENTAGON:I = 0x17

.field public static final eEV_SLIDE_ANIMATION_FRAME_RIGHT_ARROW:I = 0x3a

.field public static final eEV_SLIDE_ANIMATION_FRAME_RIGHT_ARROW_CALLOUT:I = 0x4e

.field public static final eEV_SLIDE_ANIMATION_FRAME_RIGHT_BRACE:I = 0x39

.field public static final eEV_SLIDE_ANIMATION_FRAME_RIGHT_BRACKET:I = 0x37

.field public static final eEV_SLIDE_ANIMATION_FRAME_RIGHT_TRIANGLE:I = 0x13

.field public static final eEV_SLIDE_ANIMATION_FRAME_ROUNDED_RECTANGLE:I = 0x7

.field public static final eEV_SLIDE_ANIMATION_FRAME_ROUNDED_RECTANGULAR_CALLOUT:I = 0x8c

.field public static final eEV_SLIDE_ANIMATION_FRAME_ROUND_DIAGONAL_CORNER_RECTANGLE:I = 0xe

.field public static final eEV_SLIDE_ANIMATION_FRAME_ROUND_SAME_SIDE_CORNER_RECTANGLE:I = 0xd

.field public static final eEV_SLIDE_ANIMATION_FRAME_ROUND_SINGLE_CORNER_RECTANGLE:I = 0xc

.field public static final eEV_SLIDE_ANIMATION_FRAME_SMILEY_FACE:I = 0x2d

.field public static final eEV_SLIDE_ANIMATION_FRAME_SNIP_AND_ROUND_SINGLE_CORNER_RECTANGLE:I = 0xb

.field public static final eEV_SLIDE_ANIMATION_FRAME_SNIP_DIAGONAL_CORNER_RECTANGLE:I = 0xa

.field public static final eEV_SLIDE_ANIMATION_FRAME_SNIP_SAME_SIDE_CORNER_RECTANGLE:I = 0x9

.field public static final eEV_SLIDE_ANIMATION_FRAME_SNIP_SINGLE_CORNER_RECTANGLE:I = 0x8

.field public static final eEV_SLIDE_ANIMATION_FRAME_STRAIGHT_CONNECTOR:I = 0x2

.field public static final eEV_SLIDE_ANIMATION_FRAME_STRIPED_RIGHT_ARROW:I = 0x4a

.field public static final eEV_SLIDE_ANIMATION_FRAME_SUN:I = 0x30

.field public static final eEV_SLIDE_ANIMATION_FRAME_TABLE:I = 0xa7

.field public static final eEV_SLIDE_ANIMATION_FRAME_TEARDROP:I = 0x1f

.field public static final eEV_SLIDE_ANIMATION_FRAME_TEXT_BOX:I = 0xf

.field public static final eEV_SLIDE_ANIMATION_FRAME_TRAPEZOID:I = 0x15

.field public static final eEV_SLIDE_ANIMATION_FRAME_UP_ARROW:I = 0x3c

.field public static final eEV_SLIDE_ANIMATION_FRAME_UP_ARROW_CALLOUT:I = 0x51

.field public static final eEV_SLIDE_ANIMATION_FRAME_UP_DOWN_ARROW:I = 0x3f

.field public static final eEV_SLIDE_ANIMATION_FRAME_UP_RIBBON:I = 0x83

.field public static final eEV_SLIDE_ANIMATION_FRAME_U_TURN_ARROW:I = 0x43

.field public static final eEV_SLIDE_ANIMATION_FRAME_VERTICAL_SCROLL:I = 0x87

.field public static final eEV_SLIDE_ANIMATION_FRAME_VERTICAL_TEXT_BOX:I = 0x10

.field public static final eEV_SLIDE_ANIMATION_FRAME_VIDEO:I = 0xab

.field public static final eEV_SLIDE_ANIMATION_FRAME_WAVES:I = 0x89
