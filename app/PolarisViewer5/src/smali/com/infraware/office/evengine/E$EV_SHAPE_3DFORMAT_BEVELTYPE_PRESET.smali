.class public interface abstract Lcom/infraware/office/evengine/E$EV_SHAPE_3DFORMAT_BEVELTYPE_PRESET;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_SHAPE_3DFORMAT_BEVELTYPE_PRESET"
.end annotation


# static fields
.field public static final BR_SHAPE_3DFORMAT_BEVEL_ANGLE:I = 0x5

.field public static final BR_SHAPE_3DFORMAT_BEVEL_ART_DECO:I = 0xc

.field public static final BR_SHAPE_3DFORMAT_BEVEL_CIRCLE:I = 0x1

.field public static final BR_SHAPE_3DFORMAT_BEVEL_CONVEX:I = 0x7

.field public static final BR_SHAPE_3DFORMAT_BEVEL_COOL_SLANT:I = 0x4

.field public static final BR_SHAPE_3DFORMAT_BEVEL_CROSS:I = 0x3

.field public static final BR_SHAPE_3DFORMAT_BEVEL_DIVOT:I = 0x9

.field public static final BR_SHAPE_3DFORMAT_BEVEL_HARD_EDGE:I = 0xb

.field public static final BR_SHAPE_3DFORMAT_BEVEL_RELAXED_INSERT:I = 0x2

.field public static final BR_SHAPE_3DFORMAT_BEVEL_RIBLET:I = 0xa

.field public static final BR_SHAPE_3DFORMAT_BEVEL_SLOPE:I = 0x8

.field public static final BR_SHAPE_3DFORMAT_BEVEL_SOFT_ROUND:I = 0x6

.field public static final BR_SHAPE_3DFORMAT_NONE:I
