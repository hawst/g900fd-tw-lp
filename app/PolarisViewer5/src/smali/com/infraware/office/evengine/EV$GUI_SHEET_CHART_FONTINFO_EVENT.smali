.class public Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_FONTINFO_EVENT;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GUI_SHEET_CHART_FONTINFO_EVENT"
.end annotation


# instance fields
.field public fName:Ljava/lang/String;

.field public fRatio:F

.field public nType:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 1

    .prologue
    .line 1501
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_FONTINFO_EVENT;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1504
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_FONTINFO_EVENT;->fName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 1507
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_FONTINFO_EVENT;->nType:I

    .line 1508
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_FONTINFO_EVENT;->fRatio:F

    .line 1509
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_FONTINFO_EVENT;->fName:Ljava/lang/String;

    .line 1510
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_FONTINFO_EVENT;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1513
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_FONTINFO_EVENT;

    .line 1514
    .local v0, "o":Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_FONTINFO_EVENT;
    iget-object v1, p0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_FONTINFO_EVENT;->fName:Ljava/lang/String;

    iput-object v1, v0, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_FONTINFO_EVENT;->fName:Ljava/lang/String;

    .line 1515
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1501
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_FONTINFO_EVENT;->clone()Lcom/infraware/office/evengine/EV$GUI_SHEET_CHART_FONTINFO_EVENT;

    move-result-object v0

    return-object v0
.end method
