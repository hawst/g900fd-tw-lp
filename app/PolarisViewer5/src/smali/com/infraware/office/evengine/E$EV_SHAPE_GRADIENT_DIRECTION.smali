.class public interface abstract Lcom/infraware/office/evengine/E$EV_SHAPE_GRADIENT_DIRECTION;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_SHAPE_GRADIENT_DIRECTION"
.end annotation


# static fields
.field public static final BR_GRADIENT_DIRECTION_BOTTOM_LEFT_TO_TOP_RIGHT:I = 0x7

.field public static final BR_GRADIENT_DIRECTION_BOTTOM_RIGHT_TO_TOP_LEFT:I = 0x8

.field public static final BR_GRADIENT_DIRECTION_DOWN:I = 0x1

.field public static final BR_GRADIENT_DIRECTION_FROM_BOTTOM_LEFT_CORNER:I = 0xc

.field public static final BR_GRADIENT_DIRECTION_FROM_BOTTOM_RIGHT_CORNER:I = 0xd

.field public static final BR_GRADIENT_DIRECTION_FROM_CENTER:I = 0x9

.field public static final BR_GRADIENT_DIRECTION_FROM_TOP_LEFT_CORNER:I = 0xa

.field public static final BR_GRADIENT_DIRECTION_FROM_TOP_RIGHT_CORNER:I = 0xb

.field public static final BR_GRADIENT_DIRECTION_INVALID:I = 0x0

.field public static final BR_GRADIENT_DIRECTION_LEFT:I = 0x3

.field public static final BR_GRADIENT_DIRECTION_RIGHT:I = 0x4

.field public static final BR_GRADIENT_DIRECTION_TOP_LEFT_TO_BOTTOM_RIGHT:I = 0x5

.field public static final BR_GRADIENT_DIRECTION_TOP_RIGHT_TO_BOTTOM_LEFT:I = 0x6

.field public static final BR_GRADIENT_DIRECTION_UP:I = 0x2

.field public static final BR_GRADIENT_LINEAR_ANGLE:I = 0xe
