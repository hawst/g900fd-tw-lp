.class public Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "BWP_GRAP_ATTR_INFO"
.end annotation


# instance fields
.field public b3DStyle:I

.field public bFlip:I

.field public bHyperlinkAllowed:I

.field public bMirror:I

.field public bOLE:I

.field public bReplacementAllowed:I

.field public bRotationAllowed:I

.field public eObjectType:I

.field public nArrowType:I

.field public nBorderColor:I

.field public nBorderStyle:I

.field public nBorderWidth:I

.field public nBright:I

.field public nContrast:I

.field public nFillColor:I

.field public nGradient:I

.field public nHeight:I

.field public nRate:I

.field public nShadowStyle:I

.field public nShapeStyle:I

.field public nShapeType:I

.field public nTableStyleID:I

.field public nTableStyleOption:I

.field public nTransparency:I

.field public nWidth:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 1011
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 1043
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->bHyperlinkAllowed:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->bReplacementAllowed:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->bRotationAllowed:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->bOLE:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->b3DStyle:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->nTableStyleOption:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->nTableStyleID:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->nShapeType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->bFlip:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->bMirror:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->nShapeStyle:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->nShadowStyle:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->nTransparency:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->nContrast:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->nBright:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->eObjectType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->nRate:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->nHeight:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->nWidth:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->nArrowType:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->nBorderStyle:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->nBorderWidth:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->nBorderColor:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->nGradient:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->nFillColor:I

    .line 1047
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1050
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;

    .line 1051
    .local v0, "o":Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1011
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->clone()Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;

    move-result-object v0

    return-object v0
.end method
