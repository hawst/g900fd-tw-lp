.class public Lcom/infraware/office/evengine/EV$SHAPE_RECOLOR_PRESET_DETAIL;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SHAPE_RECOLOR_PRESET_DETAIL"
.end annotation


# instance fields
.field public nBlackWhiteScale:I

.field public nDarkColor:D

.field public nLightColor:D

.field public nTransformColor:D

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 2814
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SHAPE_RECOLOR_PRESET_DETAIL;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 2

    .prologue
    .line 2821
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_RECOLOR_PRESET_DETAIL;->nBlackWhiteScale:I

    int-to-double v0, v0

    iput-wide v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_RECOLOR_PRESET_DETAIL;->nTransformColor:D

    iput-wide v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_RECOLOR_PRESET_DETAIL;->nLightColor:D

    iput-wide v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_RECOLOR_PRESET_DETAIL;->nDarkColor:D

    .line 2822
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SHAPE_RECOLOR_PRESET_DETAIL;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2826
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SHAPE_RECOLOR_PRESET_DETAIL;

    .line 2827
    .local v0, "o":Lcom/infraware/office/evengine/EV$SHAPE_RECOLOR_PRESET_DETAIL;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2814
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SHAPE_RECOLOR_PRESET_DETAIL;->clone()Lcom/infraware/office/evengine/EV$SHAPE_RECOLOR_PRESET_DETAIL;

    move-result-object v0

    return-object v0
.end method
