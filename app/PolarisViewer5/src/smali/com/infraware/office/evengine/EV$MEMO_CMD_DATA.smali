.class public Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MEMO_CMD_DATA"
.end annotation


# instance fields
.field public bShow:Z

.field public nMemoId:I

.field public nPageNum:I

.field public nParentId:I

.field public nXPos:I

.field public nYPos:I

.field public strAuthor:Ljava/lang/String;

.field public strDate:Ljava/lang/String;

.field public strMemo:Ljava/lang/String;

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 1

    .prologue
    .line 2835
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2838
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->strMemo:Ljava/lang/String;

    .line 2841
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->strAuthor:Ljava/lang/String;

    .line 2842
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->strDate:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 2847
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->bShow:Z

    .line 2848
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->nPageNum:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->nParentId:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->nYPos:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->nXPos:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->nMemoId:I

    .line 2849
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->strDate:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->strAuthor:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->strMemo:Ljava/lang/String;

    .line 2850
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2854
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;

    .line 2855
    .local v0, "o":Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2835
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->clone()Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;

    move-result-object v0

    return-object v0
.end method
