.class public Lcom/infraware/office/evengine/EV$SHAPE_GLOW;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SHAPE_GLOW"
.end annotation


# instance fields
.field public nColor:J

.field public nPreset:I

.field public nSize:F

.field public nTransparency:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 2509
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SHAPE_GLOW;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 2

    .prologue
    .line 2516
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_GLOW;->nTransparency:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_GLOW;->nPreset:I

    .line 2517
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_GLOW;->nColor:J

    long-to-float v0, v0

    iput v0, p0, Lcom/infraware/office/evengine/EV$SHAPE_GLOW;->nSize:F

    .line 2518
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SHAPE_GLOW;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2522
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SHAPE_GLOW;

    .line 2523
    .local v0, "o":Lcom/infraware/office/evengine/EV$SHAPE_GLOW;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2509
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SHAPE_GLOW;->clone()Lcom/infraware/office/evengine/EV$SHAPE_GLOW;

    move-result-object v0

    return-object v0
.end method
