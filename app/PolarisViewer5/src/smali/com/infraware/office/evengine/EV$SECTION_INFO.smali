.class public Lcom/infraware/office/evengine/EV$SECTION_INFO;
.super Ljava/lang/Object;
.source "EV.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SECTION_INFO"
.end annotation


# instance fields
.field public bSectionEvenPage:Z

.field public bSectionFirstPage:Z

.field public bSectionOddPage:Z

.field public nSectionNum:I

.field public nTargetPageNum:I

.field public nTotalSectionNum:I

.field final synthetic this$0:Lcom/infraware/office/evengine/EV;


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV;)V
    .locals 0

    .prologue
    .line 1764
    iput-object p1, p0, Lcom/infraware/office/evengine/EV$SECTION_INFO;->this$0:Lcom/infraware/office/evengine/EV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1775
    iput v0, p0, Lcom/infraware/office/evengine/EV$SECTION_INFO;->nTotalSectionNum:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SECTION_INFO;->nSectionNum:I

    iput v0, p0, Lcom/infraware/office/evengine/EV$SECTION_INFO;->nTargetPageNum:I

    .line 1776
    iput-boolean v0, p0, Lcom/infraware/office/evengine/EV$SECTION_INFO;->bSectionEvenPage:Z

    iput-boolean v0, p0, Lcom/infraware/office/evengine/EV$SECTION_INFO;->bSectionOddPage:Z

    iput-boolean v0, p0, Lcom/infraware/office/evengine/EV$SECTION_INFO;->bSectionFirstPage:Z

    .line 1777
    return-void
.end method

.method protected clone()Lcom/infraware/office/evengine/EV$SECTION_INFO;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1781
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/evengine/EV$SECTION_INFO;

    .line 1782
    .local v0, "o":Lcom/infraware/office/evengine/EV$SECTION_INFO;
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1764
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EV$SECTION_INFO;->clone()Lcom/infraware/office/evengine/EV$SECTION_INFO;

    move-result-object v0

    return-object v0
.end method
