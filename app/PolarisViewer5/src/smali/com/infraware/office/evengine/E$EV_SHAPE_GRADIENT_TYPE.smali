.class public interface abstract Lcom/infraware/office/evengine/E$EV_SHAPE_GRADIENT_TYPE;
.super Ljava/lang/Object;
.source "E.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_SHAPE_GRADIENT_TYPE"
.end annotation


# static fields
.field public static final BR_GRADIENT_TYPE_INVALID:I = 0x0

.field public static final BR_GRADIENT_TYPE_LINEAR:I = 0x1

.field public static final BR_GRADIENT_TYPE_PATH:I = 0x4

.field public static final BR_GRADIENT_TYPE_RADAR:I = 0x2

.field public static final BR_GRADIENT_TYPE_SQUARE:I = 0x3
