.class Lcom/infraware/office/evengine/EvSearchStartObj;
.super Lcom/infraware/office/evengine/EvTaskObj;
.source "EvTaskObj.java"


# instance fields
.field private m_bCase:I

.field private m_bDirUp:I

.field private m_bFixedZoom:I

.field private m_bMatchWord:I

.field private m_sFind:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/infraware/office/evengine/EvNative;Ljava/lang/String;IIII)V
    .locals 0
    .param p1, "a_Native"    # Lcom/infraware/office/evengine/EvNative;
    .param p2, "a_sFind"    # Ljava/lang/String;
    .param p3, "a_bMatchWord"    # I
    .param p4, "a_bCase"    # I
    .param p5, "a_bDirUp"    # I
    .param p6, "a_bFixedZoom"    # I

    .prologue
    .line 586
    invoke-direct {p0, p1}, Lcom/infraware/office/evengine/EvTaskObj;-><init>(Lcom/infraware/office/evengine/EvNative;)V

    .line 587
    iput-object p2, p0, Lcom/infraware/office/evengine/EvSearchStartObj;->m_sFind:Ljava/lang/String;

    .line 588
    iput p3, p0, Lcom/infraware/office/evengine/EvSearchStartObj;->m_bMatchWord:I

    .line 589
    iput p4, p0, Lcom/infraware/office/evengine/EvSearchStartObj;->m_bCase:I

    .line 590
    iput p5, p0, Lcom/infraware/office/evengine/EvSearchStartObj;->m_bDirUp:I

    .line 591
    iput p6, p0, Lcom/infraware/office/evengine/EvSearchStartObj;->m_bFixedZoom:I

    .line 592
    return-void
.end method


# virtual methods
.method doTask()V
    .locals 6

    .prologue
    .line 596
    iget-object v0, p0, Lcom/infraware/office/evengine/EvSearchStartObj;->Native:Lcom/infraware/office/evengine/EvNative;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvSearchStartObj;->m_sFind:Ljava/lang/String;

    iget v2, p0, Lcom/infraware/office/evengine/EvSearchStartObj;->m_bMatchWord:I

    iget v3, p0, Lcom/infraware/office/evengine/EvSearchStartObj;->m_bCase:I

    iget v4, p0, Lcom/infraware/office/evengine/EvSearchStartObj;->m_bDirUp:I

    iget v5, p0, Lcom/infraware/office/evengine/EvSearchStartObj;->m_bFixedZoom:I

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvNative;->ISearchStart(Ljava/lang/String;IIII)V

    .line 597
    return-void
.end method
