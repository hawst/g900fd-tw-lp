.class Lcom/infraware/office/evengine/EvSetZoomObj;
.super Lcom/infraware/office/evengine/EvTaskObj;
.source "EvTaskObj.java"


# instance fields
.field private m_bHaveZoomCenter:I

.field private m_bStep:I

.field private m_eKey:I

.field private m_eZoomType:I

.field private m_nEx:I

.field private m_nEy:I

.field private m_nScale:I

.field private m_nSx:I

.field private m_nSy:I

.field private m_nZoomCenterX:I

.field private m_nZoomCenterY:I


# direct methods
.method constructor <init>(Lcom/infraware/office/evengine/EvNative;IIIIIIIIIII)V
    .locals 0
    .param p1, "a_Native"    # Lcom/infraware/office/evengine/EvNative;
    .param p2, "a_eZoomType"    # I
    .param p3, "a_nScale"    # I
    .param p4, "a_nSx"    # I
    .param p5, "a_nSy"    # I
    .param p6, "a_nEx"    # I
    .param p7, "a_nEy"    # I
    .param p8, "a_eKey"    # I
    .param p9, "a_bStep"    # I
    .param p10, "a_bHaveZoomCenter"    # I
    .param p11, "a_nZoomCenterX"    # I
    .param p12, "a_nZoomCenterY"    # I

    .prologue
    .line 248
    invoke-direct {p0, p1}, Lcom/infraware/office/evengine/EvTaskObj;-><init>(Lcom/infraware/office/evengine/EvNative;)V

    .line 249
    iput p2, p0, Lcom/infraware/office/evengine/EvSetZoomObj;->m_eZoomType:I

    .line 250
    iput p3, p0, Lcom/infraware/office/evengine/EvSetZoomObj;->m_nScale:I

    .line 251
    iput p4, p0, Lcom/infraware/office/evengine/EvSetZoomObj;->m_nSx:I

    .line 252
    iput p5, p0, Lcom/infraware/office/evengine/EvSetZoomObj;->m_nSy:I

    .line 253
    iput p6, p0, Lcom/infraware/office/evengine/EvSetZoomObj;->m_nEx:I

    .line 254
    iput p7, p0, Lcom/infraware/office/evengine/EvSetZoomObj;->m_nEy:I

    .line 255
    iput p8, p0, Lcom/infraware/office/evengine/EvSetZoomObj;->m_eKey:I

    .line 256
    iput p9, p0, Lcom/infraware/office/evengine/EvSetZoomObj;->m_bStep:I

    .line 257
    iput p10, p0, Lcom/infraware/office/evengine/EvSetZoomObj;->m_bHaveZoomCenter:I

    .line 258
    iput p11, p0, Lcom/infraware/office/evengine/EvSetZoomObj;->m_nZoomCenterX:I

    .line 259
    iput p12, p0, Lcom/infraware/office/evengine/EvSetZoomObj;->m_nZoomCenterY:I

    .line 260
    return-void
.end method


# virtual methods
.method doTask()V
    .locals 12

    .prologue
    .line 264
    iget-object v0, p0, Lcom/infraware/office/evengine/EvSetZoomObj;->Native:Lcom/infraware/office/evengine/EvNative;

    iget v1, p0, Lcom/infraware/office/evengine/EvSetZoomObj;->m_eZoomType:I

    iget v2, p0, Lcom/infraware/office/evengine/EvSetZoomObj;->m_nScale:I

    iget v3, p0, Lcom/infraware/office/evengine/EvSetZoomObj;->m_nSx:I

    iget v4, p0, Lcom/infraware/office/evengine/EvSetZoomObj;->m_nSy:I

    iget v5, p0, Lcom/infraware/office/evengine/EvSetZoomObj;->m_nEx:I

    iget v6, p0, Lcom/infraware/office/evengine/EvSetZoomObj;->m_nEy:I

    iget v7, p0, Lcom/infraware/office/evengine/EvSetZoomObj;->m_eKey:I

    iget v8, p0, Lcom/infraware/office/evengine/EvSetZoomObj;->m_bStep:I

    iget v9, p0, Lcom/infraware/office/evengine/EvSetZoomObj;->m_bHaveZoomCenter:I

    iget v10, p0, Lcom/infraware/office/evengine/EvSetZoomObj;->m_nZoomCenterX:I

    iget v11, p0, Lcom/infraware/office/evengine/EvSetZoomObj;->m_nZoomCenterY:I

    invoke-virtual/range {v0 .. v11}, Lcom/infraware/office/evengine/EvNative;->ISetZoom(IIIIIIIIIII)V

    .line 275
    return-void
.end method
