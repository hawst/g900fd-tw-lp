.class Lcom/infraware/office/util/InputValueFilter$1;
.super Ljava/lang/Object;
.source "InputValueFilter.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/office/util/InputValueFilter;-><init>(Landroid/content/Context;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/util/InputValueFilter;


# direct methods
.method constructor <init>(Lcom/infraware/office/util/InputValueFilter;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/infraware/office/util/InputValueFilter$1;->this$0:Lcom/infraware/office/util/InputValueFilter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 4
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    .line 35
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    .line 36
    .local v0, "checked":Ljava/lang/String;
    sub-int v2, p3, p2

    if-lez v2, :cond_2

    .line 37
    const/4 v1, 0x0

    .line 38
    .local v1, "nIdx":I
    :goto_0
    sub-int v2, p3, p2

    if-ge v1, v2, :cond_1

    .line 40
    iget-object v2, p0, Lcom/infraware/office/util/InputValueFilter$1;->this$0:Lcom/infraware/office/util/InputValueFilter;

    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    # invokes: Lcom/infraware/office/util/InputValueFilter;->isValidChar(C)Z
    invoke-static {v2, v3}, Lcom/infraware/office/util/InputValueFilter;->access$000(Lcom/infraware/office/util/InputValueFilter;C)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 42
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 44
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 46
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 48
    iget-object v2, p0, Lcom/infraware/office/util/InputValueFilter$1;->this$0:Lcom/infraware/office/util/InputValueFilter;

    # invokes: Lcom/infraware/office/util/InputValueFilter;->onToastMessage()V
    invoke-static {v2}, Lcom/infraware/office/util/InputValueFilter;->access$100(Lcom/infraware/office/util/InputValueFilter;)V

    move-object v2, v0

    .line 52
    .end local v1    # "nIdx":I
    :goto_1
    return-object v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method
