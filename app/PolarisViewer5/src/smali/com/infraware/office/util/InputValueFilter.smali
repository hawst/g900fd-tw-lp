.class public Lcom/infraware/office/util/InputValueFilter;
.super Ljava/lang/Object;
.source "InputValueFilter.java"

# interfaces
.implements Lcom/infraware/office/baseframe/EvBaseE$PopupDialogEventType;


# instance fields
.field private filters:[Landroid/text/InputFilter;

.field private m_Context:Landroid/content/Context;

.field private m_ToastMsg:Landroid/widget/Toast;

.field private m_nMaxLength:I

.field private m_nMaxValue:I


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "nMaxLangth"    # I
    .param p3, "nMaxValue"    # I

    .prologue
    const/4 v2, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/text/InputFilter;

    iput-object v0, p0, Lcom/infraware/office/util/InputValueFilter;->filters:[Landroid/text/InputFilter;

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/office/util/InputValueFilter;->m_ToastMsg:Landroid/widget/Toast;

    .line 17
    iput v2, p0, Lcom/infraware/office/util/InputValueFilter;->m_nMaxLength:I

    .line 18
    iput v2, p0, Lcom/infraware/office/util/InputValueFilter;->m_nMaxValue:I

    .line 26
    iput-object p1, p0, Lcom/infraware/office/util/InputValueFilter;->m_Context:Landroid/content/Context;

    .line 27
    iput p2, p0, Lcom/infraware/office/util/InputValueFilter;->m_nMaxLength:I

    .line 28
    iput p3, p0, Lcom/infraware/office/util/InputValueFilter;->m_nMaxValue:I

    .line 30
    iget-object v0, p0, Lcom/infraware/office/util/InputValueFilter;->filters:[Landroid/text/InputFilter;

    new-instance v1, Lcom/infraware/office/util/InputValueFilter$1;

    invoke-direct {v1, p0}, Lcom/infraware/office/util/InputValueFilter$1;-><init>(Lcom/infraware/office/util/InputValueFilter;)V

    aput-object v1, v0, v2

    .line 56
    iget-object v0, p0, Lcom/infraware/office/util/InputValueFilter;->filters:[Landroid/text/InputFilter;

    const/4 v1, 0x1

    new-instance v2, Lcom/infraware/office/util/InputValueFilter$2;

    invoke-direct {v2, p0}, Lcom/infraware/office/util/InputValueFilter$2;-><init>(Lcom/infraware/office/util/InputValueFilter;)V

    aput-object v2, v0, v1

    .line 71
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/office/util/InputValueFilter;C)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/util/InputValueFilter;
    .param p1, "x1"    # C

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/infraware/office/util/InputValueFilter;->isValidChar(C)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/infraware/office/util/InputValueFilter;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/office/util/InputValueFilter;

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/infraware/office/util/InputValueFilter;->onToastMessage()V

    return-void
.end method

.method static synthetic access$200(Lcom/infraware/office/util/InputValueFilter;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/util/InputValueFilter;

    .prologue
    .line 12
    iget v0, p0, Lcom/infraware/office/util/InputValueFilter;->m_nMaxLength:I

    return v0
.end method

.method private isValidChar(C)Z
    .locals 1
    .param p1, "ch"    # C

    .prologue
    .line 86
    const/16 v0, 0x30

    if-lt p1, v0, :cond_0

    const/16 v0, 0x39

    if-gt p1, v0, :cond_0

    .line 88
    const/4 v0, 0x1

    .line 90
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onToastMessage()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 75
    iget-object v1, p0, Lcom/infraware/office/util/InputValueFilter;->m_Context:Landroid/content/Context;

    const v2, 0x7f07012d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/infraware/office/util/InputValueFilter;->m_nMaxValue:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 76
    .local v0, "strText":Ljava/lang/String;
    iget-object v1, p0, Lcom/infraware/office/util/InputValueFilter;->m_ToastMsg:Landroid/widget/Toast;

    if-nez v1, :cond_0

    .line 77
    iget-object v1, p0, Lcom/infraware/office/util/InputValueFilter;->m_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/office/util/InputValueFilter;->m_ToastMsg:Landroid/widget/Toast;

    .line 80
    :goto_0
    iget-object v1, p0, Lcom/infraware/office/util/InputValueFilter;->m_ToastMsg:Landroid/widget/Toast;

    const/16 v2, 0x11

    invoke-virtual {v1, v2, v4, v4}, Landroid/widget/Toast;->setGravity(III)V

    .line 81
    iget-object v1, p0, Lcom/infraware/office/util/InputValueFilter;->m_ToastMsg:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 82
    return-void

    .line 79
    :cond_0
    iget-object v1, p0, Lcom/infraware/office/util/InputValueFilter;->m_ToastMsg:Landroid/widget/Toast;

    invoke-virtual {v1, v0}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public getFilters()[Landroid/text/InputFilter;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/infraware/office/util/InputValueFilter;->filters:[Landroid/text/InputFilter;

    return-object v0
.end method
