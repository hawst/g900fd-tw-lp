.class public Lcom/infraware/office/util/EvUtil;
.super Ljava/lang/Object;
.source "EvUtil.java"


# static fields
.field private static mImm:Landroid/view/inputmethod/InputMethodManager;

.field private static mRect:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/infraware/office/util/EvUtil;->mRect:Landroid/graphics/Rect;

    .line 21
    const/4 v0, 0x0

    sput-object v0, Lcom/infraware/office/util/EvUtil;->mImm:Landroid/view/inputmethod/InputMethodManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Landroid/view/inputmethod/InputMethodManager;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/infraware/office/util/EvUtil;->mImm:Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method

.method public static getCenterXOfRect(Landroid/graphics/Rect;)I
    .locals 2
    .param p0, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 32
    iget v0, p0, Landroid/graphics/Rect;->left:I

    iget v1, p0, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public static getCenterXOfView(Landroid/view/View;)I
    .locals 2
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 24
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    return v0
.end method

.method public static getCenterYOfRect(Landroid/graphics/Rect;)I
    .locals 2
    .param p0, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 36
    iget v0, p0, Landroid/graphics/Rect;->top:I

    iget v1, p0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public static getCenterYOfView(Landroid/view/View;)I
    .locals 2
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 28
    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    return v0
.end method

.method public static getInputMethodManager(Landroid/content/Context;)Landroid/view/inputmethod/InputMethodManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    sget-object v0, Lcom/infraware/office/util/EvUtil;->mImm:Landroid/view/inputmethod/InputMethodManager;

    if-nez v0, :cond_0

    .line 41
    const-string/jumbo v0, "input_method"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    sput-object v0, Lcom/infraware/office/util/EvUtil;->mImm:Landroid/view/inputmethod/InputMethodManager;

    .line 42
    :cond_0
    sget-object v0, Lcom/infraware/office/util/EvUtil;->mImm:Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method

.method public static final getItemRectByCenter(Landroid/view/View;II)Landroid/graphics/Rect;
    .locals 7
    .param p0, "v"    # Landroid/view/View;
    .param p1, "centerX"    # I
    .param p2, "centerY"    # I

    .prologue
    .line 99
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    div-int/lit8 v1, v2, 0x2

    .line 100
    .local v1, "halfWidth":I
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v2

    div-int/lit8 v0, v2, 0x2

    .line 101
    .local v0, "halfHeight":I
    sget-object v2, Lcom/infraware/office/util/EvUtil;->mRect:Landroid/graphics/Rect;

    sub-int v3, p1, v1

    sub-int v4, p2, v0

    add-int v5, p1, v1

    add-int v6, p2, v0

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 102
    sget-object v2, Lcom/infraware/office/util/EvUtil;->mRect:Landroid/graphics/Rect;

    return-object v2
.end method

.method public static getTextIndex(Landroid/widget/TextView;Landroid/view/MotionEvent;Landroid/app/Activity;)I
    .locals 7
    .param p0, "view"    # Landroid/widget/TextView;
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 117
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v4, v6

    .line 118
    .local v4, "positionX":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v5, v6

    .line 120
    .local v5, "positionY":I
    invoke-virtual {p0}, Landroid/widget/TextView;->getTotalPaddingLeft()I

    move-result v6

    sub-int/2addr v4, v6

    .line 121
    invoke-virtual {p0}, Landroid/widget/TextView;->getTotalPaddingTop()I

    move-result v6

    sub-int/2addr v5, v6

    .line 123
    invoke-virtual {p0}, Landroid/widget/TextView;->getScrollX()I

    move-result v6

    add-int/2addr v4, v6

    .line 124
    invoke-virtual {p0}, Landroid/widget/TextView;->getScrollY()I

    move-result v6

    add-int/2addr v5, v6

    .line 126
    invoke-static {p2}, Lcom/infraware/office/util/EvUtil;->getcontentViewTop(Landroid/app/Activity;)I

    move-result v6

    sub-int/2addr v5, v6

    .line 128
    invoke-virtual {p0}, Landroid/widget/TextView;->getLineHeight()I

    move-result v6

    div-int v2, v5, v6

    .line 130
    .local v2, "lines":I
    invoke-virtual {p0}, Landroid/widget/TextView;->getLineHeight()I

    move-result v6

    rem-int v6, v5, v6

    if-eqz v6, :cond_0

    .line 131
    add-int/lit8 v2, v2, 0x1

    .line 134
    :cond_0
    invoke-virtual {p0}, Landroid/widget/TextView;->getLineCount()I

    move-result v6

    if-le v2, v6, :cond_1

    .line 135
    const/4 v2, -0x1

    .line 138
    :cond_1
    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    .line 140
    .local v0, "layout":Landroid/text/Layout;
    invoke-virtual {v0, v5}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v1

    .line 141
    .local v1, "line":I
    int-to-float v6, v4

    invoke-virtual {v0, v1, v6}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v3

    .line 143
    .local v3, "off":I
    return v3
.end method

.method private static getcontentViewTop(Landroid/app/Activity;)I
    .locals 4
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 148
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 149
    .local v1, "rect":Landroid/graphics/Rect;
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 150
    .local v2, "window":Landroid/view/Window;
    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 151
    const v3, 0x1020002

    invoke-virtual {v2, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v0

    .line 153
    .local v0, "contentViewTop":I
    return v0
.end method

.method public static hideIme(Landroid/content/Context;Landroid/os/IBinder;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "windowToken"    # Landroid/os/IBinder;

    .prologue
    .line 86
    const/4 v0, 0x0

    .line 87
    .local v0, "_imm":Landroid/view/inputmethod/InputMethodManager;
    sget-object v1, Lcom/infraware/office/util/EvUtil;->mImm:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v1, :cond_1

    .line 88
    sget-object v0, Lcom/infraware/office/util/EvUtil;->mImm:Landroid/view/inputmethod/InputMethodManager;

    .line 94
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isActive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 95
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 96
    :cond_0
    return-void

    .line 90
    :cond_1
    const-string/jumbo v1, "input_method"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "_imm":Landroid/view/inputmethod/InputMethodManager;
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 91
    .restart local v0    # "_imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-static {v0}, Lcom/infraware/office/util/EvUtil;->setInputMethodManager(Landroid/view/inputmethod/InputMethodManager;)V

    goto :goto_0
.end method

.method public static isActiveKeyboard(Landroid/content/Context;I)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "Height"    # I

    .prologue
    .line 51
    const-string/jumbo v2, "window"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 52
    .local v1, "dis":Landroid/view/Display;
    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v0

    .line 54
    .local v0, "DisHeight":I
    div-int/lit8 v2, v0, 0x2

    if-ge v2, p1, :cond_0

    .line 55
    const/4 v2, 0x0

    .line 56
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static setInputMethodManager(Landroid/view/inputmethod/InputMethodManager;)V
    .locals 3
    .param p0, "imm"    # Landroid/view/inputmethod/InputMethodManager;

    .prologue
    .line 46
    const-string/jumbo v0, "EvUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setInputMethodManager imm: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    sput-object p0, Lcom/infraware/office/util/EvUtil;->mImm:Landroid/view/inputmethod/InputMethodManager;

    .line 48
    return-void
.end method

.method public static showIme(Landroid/view/View;)V
    .locals 6
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 60
    const/4 v0, 0x0

    .line 61
    .local v0, "_imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 62
    .local v1, "context":Landroid/content/Context;
    sget-object v2, Lcom/infraware/office/util/EvUtil;->mImm:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v2, :cond_0

    .line 63
    sget-object v0, Lcom/infraware/office/util/EvUtil;->mImm:Landroid/view/inputmethod/InputMethodManager;

    .line 72
    :goto_0
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/infraware/office/util/EvUtil$1;

    invoke-direct {v3, p0, v1}, Lcom/infraware/office/util/EvUtil$1;-><init>(Landroid/view/View;Landroid/content/Context;)V

    const-wide/16 v4, 0x32

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 83
    return-void

    .line 65
    :cond_0
    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "_imm":Landroid/view/inputmethod/InputMethodManager;
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 66
    .restart local v0    # "_imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-static {v0}, Lcom/infraware/office/util/EvUtil;->setInputMethodManager(Landroid/view/inputmethod/InputMethodManager;)V

    goto :goto_0
.end method

.method public static stringToHexa(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 107
    const/16 v3, 0x20

    .line 108
    .local v3, "s":C
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    mul-int/lit8 v4, v4, 0x3

    invoke-direct {v0, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 109
    .local v0, "buff":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 110
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 111
    .local v1, "c":I
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 109
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 113
    .end local v1    # "c":I
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method
