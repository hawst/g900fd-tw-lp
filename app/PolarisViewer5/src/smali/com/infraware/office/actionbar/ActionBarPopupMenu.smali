.class public Lcom/infraware/office/actionbar/ActionBarPopupMenu;
.super Landroid/widget/PopupWindow;
.source "ActionBarPopupMenu.java"

# interfaces
.implements Lcom/infraware/office/baseframe/EvBaseE$EV_ACTIONBAR_EVENT;
.implements Lcom/infraware/office/evengine/E$EV_DOCTYPE;
.implements Lcom/infraware/polarisoffice5/common/LocaleChangeListener;


# instance fields
.field private mAccountStrs:[Ljava/lang/String;

.field private mActivity:Landroid/app/Activity;

.field private mCallerView:Landroid/view/View;

.field private mChecks:[Z

.field mClickListener:Landroid/view/View$OnClickListener;

.field private mCount:I

.field private mEnables:[Z

.field private mLinearLayout:Landroid/widget/LinearLayout;

.field private mMenuIDs:[I

.field private mMenuType:I

.field private mOnKeyListener:Landroid/view/View$OnKeyListener;

.field private mResIDs:[I

.field private mRootFrame:Landroid/widget/FrameLayout;

.field private mScrollView:Landroid/widget/ScrollView;


# direct methods
.method public constructor <init>(Landroid/app/Activity;ILandroid/view/View;Landroid/view/View$OnClickListener;)V
    .locals 5
    .param p1, "act"    # Landroid/app/Activity;
    .param p2, "type"    # I
    .param p3, "caller"    # Landroid/view/View;
    .param p4, "ocl"    # Landroid/view/View$OnClickListener;

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    const/16 v1, 0x14

    const/4 v4, 0x1

    const/4 v3, -0x1

    .line 52
    invoke-direct {p0, p1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    .line 31
    iput v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mMenuType:I

    .line 32
    iput-object v2, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mScrollView:Landroid/widget/ScrollView;

    .line 33
    iput-object v2, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mActivity:Landroid/app/Activity;

    .line 37
    iput v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCount:I

    .line 38
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mMenuIDs:[I

    .line 39
    new-array v0, v1, [Z

    iput-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mChecks:[Z

    .line 40
    new-array v0, v1, [Z

    iput-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mEnables:[Z

    .line 41
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mResIDs:[I

    .line 42
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mAccountStrs:[Ljava/lang/String;

    .line 46
    iput-object v2, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCallerView:Landroid/view/View;

    .line 47
    iput-object v2, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mClickListener:Landroid/view/View$OnClickListener;

    .line 48
    iput-object v2, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mRootFrame:Landroid/widget/FrameLayout;

    .line 164
    new-instance v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu$2;

    invoke-direct {v0, p0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu$2;-><init>(Lcom/infraware/office/actionbar/ActionBarPopupMenu;)V

    iput-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mOnKeyListener:Landroid/view/View$OnKeyListener;

    .line 54
    iput-object p1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mActivity:Landroid/app/Activity;

    .line 55
    iput p2, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mMenuType:I

    .line 56
    iput-object p3, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCallerView:Landroid/view/View;

    .line 58
    new-instance v0, Landroid/widget/ScrollView;

    invoke-direct {v0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mScrollView:Landroid/widget/ScrollView;

    .line 60
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mScrollView:Landroid/widget/ScrollView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 62
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mLinearLayout:Landroid/widget/LinearLayout;

    .line 63
    invoke-virtual {p0, p2, p3}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->setMenuType(ILandroid/view/View;)V

    .line 64
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 65
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mLinearLayout:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v3, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 67
    iput-object p4, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mClickListener:Landroid/view/View$OnClickListener;

    .line 68
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mScrollView:Landroid/widget/ScrollView;

    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 70
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mRootFrame:Landroid/widget/FrameLayout;

    .line 71
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mRootFrame:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 72
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mRootFrame:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 73
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mRootFrame:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->setContentView(Landroid/view/View;)V

    .line 75
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mRootFrame:Landroid/widget/FrameLayout;

    new-instance v1, Lcom/infraware/office/actionbar/ActionBarPopupMenu$1;

    invoke-direct {v1, p0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu$1;-><init>(Lcom/infraware/office/actionbar/ActionBarPopupMenu;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 119
    invoke-virtual {p0, v4}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->setOutsideTouchable(Z)V

    .line 120
    invoke-virtual {p0, v4}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->setFocusable(Z)V

    .line 122
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mLinearLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mOnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 138
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mRootFrame:Landroid/widget/FrameLayout;

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 139
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mScrollView:Landroid/widget/ScrollView;

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 140
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mLinearLayout:Landroid/widget/LinearLayout;

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 142
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 143
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    .line 145
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/office/actionbar/ActionBarPopupMenu;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mLinearLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private getResString(I)Ljava/lang/String;
    .locals 4
    .param p1, "menu_id"    # I

    .prologue
    .line 544
    iget-object v3, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 545
    .local v0, "r":Landroid/content/res/Resources;
    const/4 v1, 0x0

    .line 546
    .local v1, "rid":I
    sparse-switch p1, :sswitch_data_0

    .line 750
    :goto_0
    if-eqz v1, :cond_3

    .line 752
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 756
    :goto_1
    return-object v2

    .line 549
    :sswitch_0
    const v1, 0x7f070065

    .line 550
    goto :goto_0

    .line 552
    :sswitch_1
    const v1, 0x7f070295

    .line 553
    goto :goto_0

    .line 555
    :sswitch_2
    const v1, 0x7f0701a8

    .line 556
    goto :goto_0

    .line 558
    :sswitch_3
    const v1, 0x7f070136

    .line 559
    goto :goto_0

    .line 561
    :sswitch_4
    const v1, 0x7f0701a2

    .line 562
    goto :goto_0

    .line 564
    :sswitch_5
    const v1, 0x7f0701e1

    .line 565
    goto :goto_0

    .line 567
    :sswitch_6
    const v1, 0x7f07015b

    .line 568
    goto :goto_0

    .line 570
    :sswitch_7
    const v1, 0x7f070254

    .line 571
    goto :goto_0

    .line 573
    :sswitch_8
    const v1, 0x7f0701cc

    .line 574
    goto :goto_0

    .line 576
    :sswitch_9
    const v1, 0x7f07018d

    .line 577
    goto :goto_0

    .line 579
    :sswitch_a
    const v1, 0x7f07023f

    .line 580
    goto :goto_0

    .line 582
    :sswitch_b
    const v1, 0x7f070231

    .line 583
    goto :goto_0

    .line 585
    :sswitch_c
    const v1, 0x7f07012e

    .line 586
    goto :goto_0

    .line 588
    :sswitch_d
    const v1, 0x7f070140

    .line 589
    goto :goto_0

    .line 591
    :sswitch_e
    const v1, 0x7f0701f1

    .line 592
    goto :goto_0

    .line 594
    :sswitch_f
    const v1, 0x7f0700e3

    .line 595
    goto :goto_0

    .line 597
    :sswitch_10
    const v1, 0x7f0701ad

    .line 598
    goto :goto_0

    .line 600
    :sswitch_11
    const v1, 0x7f070189

    .line 601
    goto :goto_0

    .line 603
    :sswitch_12
    iget-object v3, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mActivity:Landroid/app/Activity;

    check-cast v3, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsProtectSheet()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 604
    const v1, 0x7f070249

    goto :goto_0

    .line 606
    :cond_0
    const v1, 0x7f0701c6

    .line 607
    goto :goto_0

    .line 609
    :sswitch_13
    iget-object v3, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mActivity:Landroid/app/Activity;

    check-cast v3, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsFreezeSheet()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 610
    const v1, 0x7f070245

    goto :goto_0

    .line 612
    :cond_1
    const v1, 0x7f07015a

    .line 614
    goto :goto_0

    .line 616
    :sswitch_14
    const v1, 0x7f0701c7

    .line 617
    goto :goto_0

    .line 619
    :sswitch_15
    const v1, 0x7f070225

    .line 620
    goto :goto_0

    .line 622
    :sswitch_16
    const v1, 0x7f070134

    .line 623
    goto :goto_0

    .line 625
    :sswitch_17
    const v1, 0x7f070218

    .line 626
    goto/16 :goto_0

    .line 628
    :sswitch_18
    const v1, 0x7f07018a

    .line 629
    goto/16 :goto_0

    .line 631
    :sswitch_19
    const v1, 0x7f070210

    .line 632
    goto/16 :goto_0

    .line 634
    :sswitch_1a
    const v1, 0x7f07024f

    .line 635
    goto/16 :goto_0

    .line 637
    :sswitch_1b
    const v1, 0x7f0700d7

    .line 638
    goto/16 :goto_0

    .line 640
    :sswitch_1c
    const v1, 0x7f07017b

    .line 641
    goto/16 :goto_0

    .line 643
    :sswitch_1d
    const v1, 0x7f07024c

    .line 644
    goto/16 :goto_0

    .line 646
    :sswitch_1e
    const v1, 0x7f0700e7

    .line 647
    goto/16 :goto_0

    .line 649
    :sswitch_1f
    const v1, 0x7f0701ca

    .line 650
    goto/16 :goto_0

    .line 652
    :sswitch_20
    const v1, 0x7f070182

    .line 653
    goto/16 :goto_0

    .line 655
    :sswitch_21
    const v1, 0x7f070183

    .line 656
    goto/16 :goto_0

    .line 658
    :sswitch_22
    const v1, 0x7f070184

    .line 659
    goto/16 :goto_0

    .line 661
    :sswitch_23
    const v1, 0x7f07016e

    .line 662
    goto/16 :goto_0

    .line 664
    :sswitch_24
    const v1, 0x7f0701f5

    .line 665
    goto/16 :goto_0

    .line 667
    :sswitch_25
    const v1, 0x7f070158

    .line 668
    goto/16 :goto_0

    .line 670
    :sswitch_26
    const v1, 0x7f07023a

    .line 671
    goto/16 :goto_0

    .line 673
    :sswitch_27
    const v1, 0x7f070238

    .line 674
    goto/16 :goto_0

    .line 677
    :sswitch_28
    const v1, 0x7f070237

    .line 678
    goto/16 :goto_0

    .line 680
    :sswitch_29
    const v1, 0x7f0700df

    .line 681
    goto/16 :goto_0

    .line 683
    :sswitch_2a
    const v1, 0x7f0701f5

    .line 684
    goto/16 :goto_0

    .line 686
    :sswitch_2b
    const v1, 0x7f070186

    .line 687
    goto/16 :goto_0

    .line 689
    :sswitch_2c
    const v1, 0x7f070175

    .line 690
    goto/16 :goto_0

    .line 692
    :sswitch_2d
    const v1, 0x7f07017f

    .line 693
    goto/16 :goto_0

    .line 695
    :sswitch_2e
    const v1, 0x7f070178

    .line 696
    goto/16 :goto_0

    .line 698
    :sswitch_2f
    const v1, 0x7f0700f3

    .line 699
    goto/16 :goto_0

    .line 701
    :sswitch_30
    const v1, 0x7f0701a0

    .line 702
    goto/16 :goto_0

    .line 705
    :sswitch_31
    const v1, 0x7f0700cf

    .line 706
    goto/16 :goto_0

    .line 708
    :sswitch_32
    const v1, 0x7f0702f7

    .line 709
    goto/16 :goto_0

    .line 711
    :sswitch_33
    const v1, 0x7f0700cb

    .line 712
    goto/16 :goto_0

    .line 714
    :sswitch_34
    const v1, 0x7f0700ca

    .line 715
    goto/16 :goto_0

    .line 720
    :sswitch_35
    const v1, 0x7f07024e

    .line 721
    goto/16 :goto_0

    .line 723
    :sswitch_36
    const v1, 0x7f0700cd

    .line 724
    goto/16 :goto_0

    .line 726
    :sswitch_37
    const v1, 0x7f0700ce

    .line 727
    goto/16 :goto_0

    .line 729
    :sswitch_38
    const v1, 0x7f0700cc

    .line 730
    goto/16 :goto_0

    .line 732
    :sswitch_39
    const v1, 0x7f07022c

    .line 734
    :sswitch_3a
    iget-object v3, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mActivity:Landroid/app/Activity;

    check-cast v3, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->IsViewerMode()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 735
    const v1, 0x7f0700ed

    goto/16 :goto_0

    .line 737
    :cond_2
    const v1, 0x7f0700f2

    .line 738
    goto/16 :goto_0

    .line 740
    :sswitch_3b
    const v1, 0x7f0702ad

    .line 741
    goto/16 :goto_0

    .line 743
    :sswitch_3c
    const v1, 0x7f070126

    .line 744
    goto/16 :goto_0

    .line 746
    :sswitch_3d
    const v1, 0x7f070064

    goto/16 :goto_0

    .line 756
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 546
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_1
        0x4 -> :sswitch_3
        0x5 -> :sswitch_7
        0x6 -> :sswitch_6
        0x7 -> :sswitch_a
        0x8 -> :sswitch_9
        0x9 -> :sswitch_4
        0x10 -> :sswitch_23
        0x11 -> :sswitch_24
        0x12 -> :sswitch_28
        0x13 -> :sswitch_2f
        0x14 -> :sswitch_2e
        0x15 -> :sswitch_2d
        0x16 -> :sswitch_2c
        0x17 -> :sswitch_1c
        0x18 -> :sswitch_1e
        0x19 -> :sswitch_20
        0x21 -> :sswitch_11
        0x22 -> :sswitch_12
        0x23 -> :sswitch_13
        0x24 -> :sswitch_14
        0x25 -> :sswitch_15
        0x26 -> :sswitch_16
        0x27 -> :sswitch_17
        0x28 -> :sswitch_18
        0x29 -> :sswitch_19
        0x30 -> :sswitch_1a
        0x31 -> :sswitch_25
        0x34 -> :sswitch_29
        0x35 -> :sswitch_1b
        0x38 -> :sswitch_8
        0x49 -> :sswitch_2a
        0x51 -> :sswitch_10
        0x57 -> :sswitch_31
        0x58 -> :sswitch_e
        0x59 -> :sswitch_b
        0x60 -> :sswitch_d
        0x61 -> :sswitch_5
        0x62 -> :sswitch_32
        0x63 -> :sswitch_33
        0x65 -> :sswitch_1d
        0x67 -> :sswitch_34
        0x69 -> :sswitch_28
        0x70 -> :sswitch_27
        0x71 -> :sswitch_26
        0x72 -> :sswitch_30
        0x73 -> :sswitch_1f
        0x74 -> :sswitch_21
        0x75 -> :sswitch_22
        0x77 -> :sswitch_35
        0x79 -> :sswitch_2b
        0x80 -> :sswitch_f
        0x81 -> :sswitch_36
        0x83 -> :sswitch_c
        0x96 -> :sswitch_37
        0x97 -> :sswitch_38
        0x102 -> :sswitch_39
        0x117 -> :sswitch_3a
        0x119 -> :sswitch_3b
        0x120 -> :sswitch_3c
        0x121 -> :sswitch_3d
    .end sparse-switch
.end method


# virtual methods
.method public addMenu(IIZZ)V
    .locals 2
    .param p1, "menu_id"    # I
    .param p2, "res_id"    # I
    .param p3, "check"    # Z
    .param p4, "enable"    # Z

    .prologue
    .line 329
    iget v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCount:I

    const/16 v1, 0x14

    if-ge v0, v1, :cond_0

    .line 331
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mMenuIDs:[I

    iget v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCount:I

    aput p1, v0, v1

    .line 332
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mChecks:[Z

    iget v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCount:I

    aput-boolean p3, v0, v1

    .line 333
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mEnables:[Z

    iget v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCount:I

    aput-boolean p4, v0, v1

    .line 334
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mResIDs:[I

    iget v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCount:I

    aput p2, v0, v1

    .line 335
    iget v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCount:I

    .line 337
    :cond_0
    return-void
.end method

.method public addMenu(IIZZLjava/lang/String;)V
    .locals 2
    .param p1, "menu_id"    # I
    .param p2, "res_id"    # I
    .param p3, "check"    # Z
    .param p4, "enable"    # Z
    .param p5, "account"    # Ljava/lang/String;

    .prologue
    .line 341
    iget v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCount:I

    const/16 v1, 0x14

    if-ge v0, v1, :cond_0

    .line 343
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mMenuIDs:[I

    iget v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCount:I

    aput p1, v0, v1

    .line 344
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mResIDs:[I

    iget v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCount:I

    aput p2, v0, v1

    .line 345
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mChecks:[Z

    iget v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCount:I

    aput-boolean p3, v0, v1

    .line 346
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mEnables:[Z

    iget v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCount:I

    aput-boolean p4, v0, v1

    .line 347
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mAccountStrs:[Ljava/lang/String;

    iget v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCount:I

    aput-object p5, v0, v1

    .line 348
    iget v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCount:I

    .line 350
    :cond_0
    return-void
.end method

.method public deleteMenu(I)V
    .locals 5
    .param p1, "menu_id"    # I

    .prologue
    .line 354
    const/4 v1, -0x1

    .line 355
    .local v1, "index":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCount:I

    if-ge v0, v2, :cond_0

    .line 357
    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mMenuIDs:[I

    aget v2, v2, v0

    if-ne v2, p1, :cond_1

    .line 359
    move v1, v0

    .line 364
    :cond_0
    const/4 v2, -0x1

    if-le v1, v2, :cond_3

    .line 366
    move v0, v1

    :goto_1
    iget v2, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCount:I

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    .line 368
    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mMenuIDs:[I

    iget-object v3, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mMenuIDs:[I

    add-int/lit8 v4, v0, 0x1

    aget v3, v3, v4

    aput v3, v2, v0

    .line 369
    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mChecks:[Z

    iget-object v3, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mChecks:[Z

    add-int/lit8 v4, v0, 0x1

    aget-boolean v3, v3, v4

    aput-boolean v3, v2, v0

    .line 370
    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mEnables:[Z

    iget-object v3, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mEnables:[Z

    add-int/lit8 v4, v0, 0x1

    aget-boolean v3, v3, v4

    aput-boolean v3, v2, v0

    .line 371
    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mResIDs:[I

    iget-object v3, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mResIDs:[I

    add-int/lit8 v4, v0, 0x1

    aget v3, v3, v4

    aput v3, v2, v0

    .line 366
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 355
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 374
    :cond_2
    iget v2, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCount:I

    .line 376
    :cond_3
    return-void
.end method

.method public dismiss()V
    .locals 2

    .prologue
    .line 811
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCallerView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 813
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCallerView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 816
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->removeAllItems()V

    .line 818
    invoke-super {p0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 819
    return-void
.end method

.method public finalizeActionBarPopupMenu()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 149
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->removeAllViews()V

    .line 150
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mRootFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 151
    iput-object v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mLinearLayout:Landroid/widget/LinearLayout;

    .line 152
    iput-object v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mScrollView:Landroid/widget/ScrollView;

    .line 153
    iput-object v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mRootFrame:Landroid/widget/FrameLayout;

    .line 156
    iput-object v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mActivity:Landroid/app/Activity;

    .line 158
    iput-object v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCallerView:Landroid/view/View;

    .line 159
    iput-object v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mClickListener:Landroid/view/View$OnClickListener;

    .line 161
    iput-object v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mOnKeyListener:Landroid/view/View$OnKeyListener;

    .line 162
    return-void
.end method

.method public getCallerView()Landroid/view/View;
    .locals 1

    .prologue
    .line 772
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCallerView:Landroid/view/View;

    return-object v0
.end method

.method public isExistMenu(I)Z
    .locals 2
    .param p1, "menu_id"    # I

    .prologue
    .line 316
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCount:I

    if-ge v0, v1, :cond_1

    .line 318
    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mMenuIDs:[I

    aget v1, v1, v0

    if-ne v1, p1, :cond_0

    .line 320
    const/4 v1, 0x1

    .line 324
    :goto_1
    return v1

    .line 316
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 324
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public length()I
    .locals 1

    .prologue
    .line 762
    iget v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCount:I

    return v0
.end method

.method public onLocaleChanged()V
    .locals 1

    .prologue
    .line 535
    invoke-virtual {p0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 537
    invoke-virtual {p0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->dismiss()V

    .line 540
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->show()V

    .line 541
    return-void
.end method

.method public refresh()V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 824
    invoke-virtual {p0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 826
    invoke-virtual {p0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->getHeight()I

    move-result v7

    .line 827
    .local v7, "height":I
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    .line 828
    .local v8, "rect":Landroid/graphics/Rect;
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCallerView:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 829
    iget v11, v8, Landroid/graphics/Rect;->bottom:I

    .line 830
    .local v11, "y":I
    iget v10, v8, Landroid/graphics/Rect;->left:I

    .line 831
    .local v10, "x":I
    const/4 v8, 0x0

    .line 833
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    .line 835
    .local v6, "conf":Landroid/content/res/Configuration;
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mActivity:Landroid/app/Activity;

    iget v1, v6, Landroid/content/res/Configuration;->screenHeightDp:I

    int-to-float v1, v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v9

    .line 837
    .local v9, "screenSize":I
    sub-int/2addr v9, v11

    .line 839
    if-le v7, v9, :cond_0

    .line 840
    add-int/lit8 v0, v9, -0x1

    invoke-virtual {p0, v0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->setHeight(I)V

    .line 842
    :cond_0
    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCallerView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->getHeight()I

    move-result v5

    move-object v0, p0

    move v3, v2

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->update(Landroid/view/View;IIII)V

    .line 845
    .end local v6    # "conf":Landroid/content/res/Configuration;
    .end local v7    # "height":I
    .end local v8    # "rect":Landroid/graphics/Rect;
    .end local v9    # "screenSize":I
    .end local v10    # "x":I
    .end local v11    # "y":I
    :cond_1
    return-void
.end method

.method public removeAllItems()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 380
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCount:I

    if-ge v0, v1, :cond_0

    .line 382
    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mMenuIDs:[I

    aput v2, v1, v0

    .line 383
    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mChecks:[Z

    aput-boolean v2, v1, v0

    .line 384
    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mEnables:[Z

    aput-boolean v2, v1, v0

    .line 385
    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mResIDs:[I

    aput v2, v1, v0

    .line 380
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 387
    :cond_0
    iput v2, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCount:I

    .line 391
    return-void
.end method

.method public setClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "ocl"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 767
    iput-object p1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mClickListener:Landroid/view/View$OnClickListener;

    .line 768
    return-void
.end method

.method public setMenuType(ILandroid/view/View;)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "caller"    # Landroid/view/View;

    .prologue
    .line 306
    iput p1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mMenuType:I

    .line 307
    iput-object p2, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCallerView:Landroid/view/View;

    .line 309
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mScrollView:Landroid/widget/ScrollView;

    const v1, 0x7f0201a6

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setBackgroundResource(I)V

    .line 312
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 313
    return-void
.end method

.method public show()V
    .locals 23

    .prologue
    .line 396
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 398
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCallerView:Landroid/view/View;

    if-eqz v4, :cond_0

    .line 400
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCallerView:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setSelected(Z)V

    .line 403
    :cond_0
    invoke-super/range {p0 .. p0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 406
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mActivity:Landroid/app/Activity;

    instance-of v4, v4, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mActivity:Landroid/app/Activity;

    check-cast v4, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v4

    iget-object v4, v4, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    if-eqz v4, :cond_2

    .line 408
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mActivity:Landroid/app/Activity;

    check-cast v4, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v4

    iget-object v4, v4, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 409
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mActivity:Landroid/app/Activity;

    check-cast v4, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v4

    iget-object v4, v4, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismissAll()V

    .line 412
    :cond_2
    move-object/from16 v0, p0

    iget v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCount:I

    if-nez v4, :cond_3

    .line 530
    :goto_0
    return-void

    .line 415
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mActivity:Landroid/app/Activity;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static {v4, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v4

    int-to-float v0, v4

    move/from16 v16, v0

    .line 417
    .local v16, "px_per_dip":F
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 419
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCount:I

    if-ge v13, v4, :cond_8

    .line 422
    if-lez v13, :cond_5

    .line 424
    new-instance v14, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mActivity:Landroid/app/Activity;

    invoke-direct {v14, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 428
    .local v14, "iv":Landroid/widget/ImageView;
    invoke-static {}, Lcom/infraware/common/util/Utils;->isL()Z

    move-result v4

    if-eqz v4, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mEnables:[Z

    aget-boolean v4, v4, v13

    if-eqz v4, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mEnables:[Z

    add-int/lit8 v5, v13, -0x1

    aget-boolean v4, v4, v5

    if-nez v4, :cond_6

    .line 429
    :cond_4
    const v4, -0xf5b7a3

    invoke-virtual {v14, v4}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 433
    :goto_2
    new-instance v15, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    move/from16 v0, v16

    float-to-double v5, v0

    const-wide/high16 v7, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v5, v7

    double-to-int v5, v5

    invoke-direct {v15, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 435
    .local v15, "params1":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v14, v15}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 436
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v14}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 438
    .end local v14    # "iv":Landroid/widget/ImageView;
    .end local v15    # "params1":Landroid/widget/LinearLayout$LayoutParams;
    :cond_5
    const/4 v3, 0x0

    .line 440
    .local v3, "item":Lcom/infraware/office/actionbar/PopoverMenuItem;
    const/4 v4, 0x6

    move-object/from16 v0, p0

    iget v5, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mMenuType:I

    if-ne v4, v5, :cond_7

    .line 441
    new-instance v3, Lcom/infraware/office/actionbar/PopoverMenuItem;

    .end local v3    # "item":Lcom/infraware/office/actionbar/PopoverMenuItem;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mActivity:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mMenuIDs:[I

    aget v5, v5, v13

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mResIDs:[I

    aget v6, v6, v13

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mAccountStrs:[Ljava/lang/String;

    aget-object v7, v7, v13

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/infraware/office/actionbar/PopoverMenuItem;-><init>(Landroid/app/Activity;IILjava/lang/String;)V

    .line 445
    .restart local v3    # "item":Lcom/infraware/office/actionbar/PopoverMenuItem;
    :goto_3
    const v4, 0x7f020039

    invoke-virtual {v3, v4}, Lcom/infraware/office/actionbar/PopoverMenuItem;->setBackgroundResource(I)V

    .line 446
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mMenuIDs:[I

    aget v4, v4, v13

    invoke-virtual {v3, v4}, Lcom/infraware/office/actionbar/PopoverMenuItem;->setId(I)V

    .line 447
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Lcom/infraware/office/actionbar/PopoverMenuItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 450
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mEnables:[Z

    aget-boolean v4, v4, v13

    invoke-virtual {v3, v4}, Lcom/infraware/office/actionbar/PopoverMenuItem;->setFocusable(Z)V

    .line 452
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mOnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v3, v4}, Lcom/infraware/office/actionbar/PopoverMenuItem;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 454
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 419
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_1

    .line 431
    .end local v3    # "item":Lcom/infraware/office/actionbar/PopoverMenuItem;
    .restart local v14    # "iv":Landroid/widget/ImageView;
    :cond_6
    const v4, -0xdba492

    invoke-virtual {v14, v4}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_2

    .line 443
    .end local v14    # "iv":Landroid/widget/ImageView;
    .restart local v3    # "item":Lcom/infraware/office/actionbar/PopoverMenuItem;
    :cond_7
    new-instance v3, Lcom/infraware/office/actionbar/PopoverMenuItem;

    .end local v3    # "item":Lcom/infraware/office/actionbar/PopoverMenuItem;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mActivity:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mMenuIDs:[I

    aget v5, v5, v13

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mResIDs:[I

    aget v6, v6, v13

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mMenuIDs:[I

    aget v7, v7, v13

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->getResString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mChecks:[Z

    aget-boolean v8, v8, v13

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mEnables:[Z

    aget-boolean v9, v9, v13

    invoke-direct/range {v3 .. v9}, Lcom/infraware/office/actionbar/PopoverMenuItem;-><init>(Landroid/app/Activity;IILjava/lang/String;ZZ)V

    .restart local v3    # "item":Lcom/infraware/office/actionbar/PopoverMenuItem;
    goto :goto_3

    .line 458
    .end local v3    # "item":Lcom/infraware/office/actionbar/PopoverMenuItem;
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mLinearLayout:Landroid/widget/LinearLayout;

    const v5, 0x430d547b    # 141.33f

    mul-float v5, v5, v16

    float-to-double v5, v5

    const-wide/high16 v7, 0x3fe0000000000000L    # 0.5

    add-double/2addr v5, v7

    double-to-int v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setMinimumWidth(I)V

    .line 459
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mLinearLayout:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/widget/LinearLayout;->measure(II)V

    .line 461
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v20

    .line 462
    .local v20, "width":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v12

    .line 466
    .local v12, "height":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v4}, Landroid/widget/ScrollView;->getPaddingTop()I

    move-result v4

    add-int/2addr v12, v4

    .line 467
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v4}, Landroid/widget/ScrollView;->getPaddingBottom()I

    move-result v4

    add-int/2addr v12, v4

    .line 468
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v4}, Landroid/widget/ScrollView;->getPaddingLeft()I

    move-result v4

    add-int v20, v20, v4

    .line 469
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v4}, Landroid/widget/ScrollView;->getPaddingRight()I

    move-result v4

    add-int v20, v20, v4

    .line 471
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mActivity:Landroid/app/Activity;

    invoke-static {v4}, Lcom/infraware/common/util/Utils;->getScreenWidthPixels(Landroid/app/Activity;)I

    move-result v19

    .line 473
    .local v19, "screenWidth":I
    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_d

    .line 474
    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->setWidth(I)V

    .line 478
    :goto_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->setHeight(I)V

    .line 481
    new-instance v17, Landroid/graphics/Rect;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Rect;-><init>()V

    .line 482
    .local v17, "rect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCallerView:Landroid/view/View;

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 483
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v22, v0

    .line 484
    .local v22, "y":I
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    .line 487
    .local v21, "x":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v11

    .line 489
    .local v11, "decorView":Landroid/view/View;
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->getWidth()I

    move-result v4

    add-int v4, v4, v21

    invoke-virtual {v11}, Landroid/view/View;->getRight()I

    move-result v5

    if-le v4, v5, :cond_9

    .line 490
    invoke-virtual {v11}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->getWidth()I

    move-result v5

    sub-int v21, v4, v5

    .line 492
    :cond_9
    const/16 v17, 0x0

    .line 494
    invoke-virtual {v11}, Landroid/view/View;->getBottom()I

    move-result v4

    sub-int v4, v4, v22

    if-gt v4, v12, :cond_a

    .line 495
    invoke-virtual {v11}, Landroid/view/View;->getBottom()I

    move-result v4

    sub-int v4, v4, v22

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->setHeight(I)V

    .line 498
    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v10

    .line 500
    .local v10, "conf":Landroid/content/res/Configuration;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mActivity:Landroid/app/Activity;

    iget v5, v10, Landroid/content/res/Configuration;->screenHeightDp:I

    int-to-float v5, v5

    invoke-static {v4, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v18

    .line 503
    .local v18, "screenSize":I
    sub-int v18, v18, v22

    .line 505
    move/from16 v0, v18

    if-le v12, v0, :cond_b

    .line 506
    add-int/lit8 v4, v18, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->setHeight(I)V

    .line 519
    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCallerView:Landroid/view/View;

    const/16 v5, 0x33

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v4, v5, v1, v2}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->showAtLocation(Landroid/view/View;III)V

    .line 522
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCallerView:Landroid/view/View;

    if-eqz v4, :cond_c

    .line 525
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mCallerView:Landroid/view/View;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/view/View;->setSelected(Z)V

    .line 529
    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto/16 :goto_0

    .line 476
    .end local v10    # "conf":Landroid/content/res/Configuration;
    .end local v11    # "decorView":Landroid/view/View;
    .end local v17    # "rect":Landroid/graphics/Rect;
    .end local v18    # "screenSize":I
    .end local v21    # "x":I
    .end local v22    # "y":I
    :cond_d
    add-int/lit8 v4, v19, -0xa

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->setWidth(I)V

    goto/16 :goto_4
.end method
