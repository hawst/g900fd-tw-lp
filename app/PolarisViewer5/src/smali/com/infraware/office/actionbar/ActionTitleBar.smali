.class public Lcom/infraware/office/actionbar/ActionTitleBar;
.super Ljava/lang/Object;
.source "ActionTitleBar.java"

# interfaces
.implements Lcom/infraware/office/baseframe/EvBaseE$EV_ACTIONBAR_EVENT;
.implements Lcom/infraware/office/baseframe/EvBaseE$EV_ACTIONBAR_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_DOCEXTENSION_TYPE;
.implements Lcom/infraware/polarisoffice5/common/LocaleChangeListener;


# instance fields
.field private final LOG_CAT:Ljava/lang/String;

.field private mActionBarButton:Landroid/widget/Button;

.field mActionBarButtonListener:Landroid/view/View$OnClickListener;

.field private mActionBarImageButton:Landroid/widget/ImageButton;

.field private mActionBarImageButton2:Landroid/widget/ImageButton;

.field private mActionBarImageButtonStringId:I

.field mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mActionBarTitle:Landroid/widget/TextView;

.field private mActionBarType:I

.field private mActionbarView:Landroid/view/View;

.field private mActivity:Landroid/app/Activity;

.field private mButtonTextResId:I

.field private mClickEvent:I

.field private mClickEvent2:I

.field mKeyListener:Landroid/view/View$OnKeyListener;

.field private mMainLayout:Landroid/widget/LinearLayout;

.field private mRulerbarTopBubble:Landroid/widget/TextView;

.field private mTitlResId:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;I)V
    .locals 4
    .param p1, "act"    # Landroid/app/Activity;
    .param p2, "insertLayout"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const-string/jumbo v1, "ActionTitleBar"

    iput-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->LOG_CAT:Ljava/lang/String;

    .line 37
    iput-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionbarView:Landroid/view/View;

    .line 38
    iput-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarTitle:Landroid/widget/TextView;

    .line 39
    iput-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;

    .line 40
    iput-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;

    .line 41
    iput v3, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButtonStringId:I

    .line 42
    iput-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton2:Landroid/widget/ImageButton;

    .line 43
    iput-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mRulerbarTopBubble:Landroid/widget/TextView;

    .line 45
    iput-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    .line 46
    iput v3, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarType:I

    .line 47
    iput v3, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mClickEvent:I

    .line 48
    iput v3, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mClickEvent2:I

    .line 49
    iput-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mMainLayout:Landroid/widget/LinearLayout;

    .line 50
    iput v3, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mTitlResId:I

    .line 51
    iput v3, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mButtonTextResId:I

    .line 295
    new-instance v1, Lcom/infraware/office/actionbar/ActionTitleBar$1;

    invoke-direct {v1, p0}, Lcom/infraware/office/actionbar/ActionTitleBar$1;-><init>(Lcom/infraware/office/actionbar/ActionTitleBar;)V

    iput-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 328
    new-instance v1, Lcom/infraware/office/actionbar/ActionTitleBar$2;

    invoke-direct {v1, p0}, Lcom/infraware/office/actionbar/ActionTitleBar$2;-><init>(Lcom/infraware/office/actionbar/ActionTitleBar;)V

    iput-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButtonListener:Landroid/view/View$OnClickListener;

    .line 544
    new-instance v1, Lcom/infraware/office/actionbar/ActionTitleBar$3;

    invoke-direct {v1, p0}, Lcom/infraware/office/actionbar/ActionTitleBar$3;-><init>(Lcom/infraware/office/actionbar/ActionTitleBar;)V

    iput-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mKeyListener:Landroid/view/View$OnKeyListener;

    .line 55
    iput-object p1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    .line 56
    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, p2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 57
    .local v0, "layout":Landroid/widget/LinearLayout;
    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030001

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionbarView:Landroid/view/View;

    .line 59
    invoke-direct {p0}, Lcom/infraware/office/actionbar/ActionTitleBar;->resourcesInit()V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;III)V
    .locals 4
    .param p1, "act"    # Landroid/app/Activity;
    .param p2, "insertLayout"    # I
    .param p3, "ActionBarType"    # I
    .param p4, "ClickEvent"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const-string/jumbo v1, "ActionTitleBar"

    iput-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->LOG_CAT:Ljava/lang/String;

    .line 37
    iput-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionbarView:Landroid/view/View;

    .line 38
    iput-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarTitle:Landroid/widget/TextView;

    .line 39
    iput-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;

    .line 40
    iput-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;

    .line 41
    iput v3, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButtonStringId:I

    .line 42
    iput-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton2:Landroid/widget/ImageButton;

    .line 43
    iput-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mRulerbarTopBubble:Landroid/widget/TextView;

    .line 45
    iput-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    .line 46
    iput v3, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarType:I

    .line 47
    iput v3, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mClickEvent:I

    .line 48
    iput v3, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mClickEvent2:I

    .line 49
    iput-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mMainLayout:Landroid/widget/LinearLayout;

    .line 50
    iput v3, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mTitlResId:I

    .line 51
    iput v3, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mButtonTextResId:I

    .line 295
    new-instance v1, Lcom/infraware/office/actionbar/ActionTitleBar$1;

    invoke-direct {v1, p0}, Lcom/infraware/office/actionbar/ActionTitleBar$1;-><init>(Lcom/infraware/office/actionbar/ActionTitleBar;)V

    iput-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 328
    new-instance v1, Lcom/infraware/office/actionbar/ActionTitleBar$2;

    invoke-direct {v1, p0}, Lcom/infraware/office/actionbar/ActionTitleBar$2;-><init>(Lcom/infraware/office/actionbar/ActionTitleBar;)V

    iput-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButtonListener:Landroid/view/View$OnClickListener;

    .line 544
    new-instance v1, Lcom/infraware/office/actionbar/ActionTitleBar$3;

    invoke-direct {v1, p0}, Lcom/infraware/office/actionbar/ActionTitleBar$3;-><init>(Lcom/infraware/office/actionbar/ActionTitleBar;)V

    iput-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mKeyListener:Landroid/view/View$OnKeyListener;

    .line 64
    iput-object p1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    .line 65
    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, p2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 66
    .local v0, "layout":Landroid/widget/LinearLayout;
    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030001

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionbarView:Landroid/view/View;

    .line 67
    iput p3, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarType:I

    .line 68
    const/4 v1, 0x1

    iget v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarType:I

    if-ne v1, v2, :cond_0

    .line 69
    const v1, 0x7f070121

    iput v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mButtonTextResId:I

    .line 71
    :cond_0
    iput p4, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mClickEvent:I

    .line 73
    invoke-direct {p0}, Lcom/infraware/office/actionbar/ActionTitleBar;->resourcesInit()V

    .line 74
    invoke-virtual {p0}, Lcom/infraware/office/actionbar/ActionTitleBar;->setActionTitleBar()V

    .line 75
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/office/actionbar/ActionTitleBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/ActionTitleBar;

    .prologue
    .line 35
    iget v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButtonStringId:I

    return v0
.end method

.method static synthetic access$100(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/ActionTitleBar;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/ActionTitleBar;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionbarView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/ActionTitleBar;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$400(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/ActionTitleBar;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton2:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$500(Lcom/infraware/office/actionbar/ActionTitleBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/ActionTitleBar;

    .prologue
    .line 35
    iget v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mClickEvent2:I

    return v0
.end method

.method static synthetic access$600(Lcom/infraware/office/actionbar/ActionTitleBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/ActionTitleBar;

    .prologue
    .line 35
    iget v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mClickEvent:I

    return v0
.end method

.method static synthetic access$700(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/ActionTitleBar;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;

    return-object v0
.end method

.method private resourcesInit()V
    .locals 3

    .prologue
    .line 79
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0b0011

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mMainLayout:Landroid/widget/LinearLayout;

    .line 80
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionbarView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 81
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0b0012

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarTitle:Landroid/widget/TextView;

    .line 82
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0b0015

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;

    .line 83
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 85
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0b0014

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;

    .line 86
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 89
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    const v2, 0x7f070121

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0b0013

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton2:Landroid/widget/ImageButton;

    .line 92
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton2:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    return-void
.end method


# virtual methods
.method public RulerBubbleShowStatus()Z
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mRulerbarTopBubble:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 478
    const/4 v0, 0x1

    .line 479
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public RulerBubbleSize()I
    .locals 5

    .prologue
    .line 484
    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mRulerbarTopBubble:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 485
    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mRulerbarTopBubble:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    float-to-double v1, v1

    const-wide/high16 v3, 0x3fe0000000000000L    # 0.5

    add-double/2addr v1, v3

    double-to-int v0, v1

    .line 488
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public changeActionTitleBarHeight()V
    .locals 4

    .prologue
    .line 434
    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 435
    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mMainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 436
    .local v0, "landscape":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    const/high16 v3, 0x42200000    # 40.0f

    invoke-static {v2, v3}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 437
    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mMainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 438
    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mMainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->requestLayout()V

    .line 447
    .end local v0    # "landscape":Landroid/widget/LinearLayout$LayoutParams;
    :goto_0
    return-void

    .line 442
    :cond_0
    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mMainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 443
    .local v1, "portrait":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    const/high16 v3, 0x42400000    # 48.0f

    invoke-static {v2, v3}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 444
    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mMainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 445
    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mMainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->requestLayout()V

    goto :goto_0
.end method

.method public changeActionTitleBarHeight(I)V
    .locals 4
    .param p1, "nOrientation"    # I

    .prologue
    .line 452
    const/4 v2, 0x2

    if-ne p1, v2, :cond_0

    .line 453
    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mMainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 454
    .local v0, "landscape":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    const/high16 v3, 0x42200000    # 40.0f

    invoke-static {v2, v3}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 455
    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mMainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 456
    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mMainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->requestLayout()V

    .line 466
    .end local v0    # "landscape":Landroid/widget/LinearLayout$LayoutParams;
    :goto_0
    return-void

    .line 460
    :cond_0
    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mMainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 461
    .local v1, "portrait":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    const/high16 v3, 0x42400000    # 48.0f

    invoke-static {v2, v3}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 462
    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mMainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 463
    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mMainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->requestLayout()V

    goto :goto_0
.end method

.method public changeType(I)V
    .locals 3
    .param p1, "ActionBarType"    # I

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 128
    packed-switch p1, :pswitch_data_0

    .line 148
    :goto_0
    return-void

    .line 131
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 132
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 136
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 137
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 141
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 142
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 128
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public createRulerbarTopBubble()V
    .locals 2

    .prologue
    .line 469
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mRulerbarTopBubble:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 470
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0b0016

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mRulerbarTopBubble:Landroid/widget/TextView;

    .line 471
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mRulerbarTopBubble:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mRulerbarTopBubble:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMinimumWidth(I)V

    .line 473
    :cond_0
    return-void
.end method

.method public getButton()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;

    return-object v0
.end method

.method public getImageButton()Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method public getImageButton2()Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton2:Landroid/widget/ImageButton;

    return-object v0
.end method

.method public hide()V
    .locals 2

    .prologue
    .line 179
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionbarView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 180
    return-void
.end method

.method public hideRulerBubble()V
    .locals 2

    .prologue
    .line 503
    const-string/jumbo v0, "Action Bubble"

    const-string/jumbo v1, "hideRulerBubble "

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mRulerbarTopBubble:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 505
    return-void
.end method

.method public isFreeDrawMode()Z
    .locals 3

    .prologue
    .line 522
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionbarView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarTitle:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    const v2, 0x7f070159

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 523
    const/4 v0, 0x1

    .line 524
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShow()Z
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionbarView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTableDrawMode()Z
    .locals 3

    .prologue
    .line 529
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionbarView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarTitle:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    const v2, 0x7f070239

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 530
    const/4 v0, 0x1

    .line 531
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onLocaleChanged()V
    .locals 3

    .prologue
    .line 420
    iget v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mTitlResId:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarTitle:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 421
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarTitle:Landroid/widget/TextView;

    iget v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mTitlResId:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 423
    :cond_0
    iget v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mButtonTextResId:I

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;

    if-eqz v0, :cond_1

    .line 424
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;

    iget v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mButtonTextResId:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 427
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    .line 428
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    const v2, 0x7f070121

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 429
    :cond_2
    return-void
.end method

.method public setActionTitleBar()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 153
    iget v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarType:I

    packed-switch v0, :pswitch_data_0

    .line 167
    :goto_0
    :pswitch_0
    return-void

    .line 159
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 160
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 163
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 164
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 153
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setButton2Enabled(Z)V
    .locals 2
    .param p1, "change"    # Z

    .prologue
    .line 112
    if-nez p1, :cond_0

    .line 114
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton2:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 120
    :goto_0
    return-void

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton2:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0
.end method

.method public setButtonEnabled(Z)V
    .locals 7
    .param p1, "change"    # Z

    .prologue
    const v6, 0x7f050034

    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 97
    if-nez p1, :cond_0

    .line 99
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 100
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v4, v4, v4, v1}, Landroid/widget/Button;->setShadowLayer(FFFI)V

    .line 101
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 109
    :goto_0
    return-void

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 106
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;

    const/high16 v1, 0x3f800000    # 1.0f

    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080005

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/widget/Button;->setShadowLayer(FFFI)V

    .line 107
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0
.end method

.method public setButtonImage(I)V
    .locals 3
    .param p1, "imageResourceId"    # I

    .prologue
    .line 228
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 229
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    const v2, 0x7f07012f

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 230
    return-void
.end method

.method public setButtonImage(II)V
    .locals 7
    .param p1, "imageResourceIdNormal"    # I
    .param p2, "imageResourceIdPressed"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 232
    iget-object v3, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 233
    .local v0, "normalDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 235
    .local v1, "pressedDrawable":Landroid/graphics/drawable/Drawable;
    new-instance v2, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 236
    .local v2, "stateDrawable":Landroid/graphics/drawable/StateListDrawable;
    new-array v3, v6, [I

    const v4, 0x10100a7

    aput v4, v3, v5

    invoke-virtual {v2, v3, v1}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 237
    new-array v3, v6, [I

    const v4, 0x101009c

    aput v4, v3, v5

    invoke-virtual {v2, v3, v1}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 238
    new-array v3, v6, [I

    const v4, 0x10100a1

    aput v4, v3, v5

    invoke-virtual {v2, v3, v1}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 239
    new-array v3, v5, [I

    invoke-virtual {v2, v3, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 241
    iget-object v3, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 242
    return-void
.end method

.method public setButtonImage(III)V
    .locals 8
    .param p1, "imageResourceIdNormal"    # I
    .param p2, "imageResourceIdPressed"    # I
    .param p3, "imageResourceDisabled"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 244
    iget-object v4, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 245
    .local v1, "normalDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v4, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 246
    .local v2, "pressedDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v4, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 248
    .local v0, "disabledDrawable":Landroid/graphics/drawable/Drawable;
    new-instance v3, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v3}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 249
    .local v3, "stateDrawable":Landroid/graphics/drawable/StateListDrawable;
    new-array v4, v7, [I

    const v5, -0x101009e

    aput v5, v4, v6

    invoke-virtual {v3, v4, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 250
    new-array v4, v7, [I

    const v5, 0x10100a7

    aput v5, v4, v6

    invoke-virtual {v3, v4, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 251
    new-array v4, v7, [I

    const v5, 0x101009c

    aput v5, v4, v6

    invoke-virtual {v3, v4, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 252
    new-array v4, v7, [I

    const v5, 0x10100a1

    aput v5, v4, v6

    invoke-virtual {v3, v4, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 253
    new-array v4, v6, [I

    invoke-virtual {v3, v4, v1}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 255
    iget-object v4, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v3}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 256
    return-void
.end method

.method public setButtonImage2(I)V
    .locals 1
    .param p1, "imageResourceId"    # I

    .prologue
    .line 260
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton2:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 261
    return-void
.end method

.method public setButtonImage2(II)V
    .locals 7
    .param p1, "imageResourceIdNormal"    # I
    .param p2, "imageResourceIdPressed"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 263
    iget-object v3, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 264
    .local v0, "normalDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 266
    .local v1, "pressedDrawable":Landroid/graphics/drawable/Drawable;
    new-instance v2, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 267
    .local v2, "stateDrawable":Landroid/graphics/drawable/StateListDrawable;
    new-array v3, v6, [I

    const v4, 0x10100a7

    aput v4, v3, v5

    invoke-virtual {v2, v3, v1}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 268
    new-array v3, v6, [I

    const v4, 0x101009c

    aput v4, v3, v5

    invoke-virtual {v2, v3, v1}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 269
    new-array v3, v6, [I

    const v4, 0x10100a1

    aput v4, v3, v5

    invoke-virtual {v2, v3, v1}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 270
    new-array v3, v5, [I

    invoke-virtual {v2, v3, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 272
    iget-object v3, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton2:Landroid/widget/ImageButton;

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 273
    return-void
.end method

.method public setButtonImage2(III)V
    .locals 8
    .param p1, "imageResourceIdNormal"    # I
    .param p2, "imageResourceIdPressed"    # I
    .param p3, "imageResourceDisabled"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 275
    iget-object v4, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 276
    .local v1, "normalDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v4, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 277
    .local v2, "pressedDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v4, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 279
    .local v0, "disabledDrawable":Landroid/graphics/drawable/Drawable;
    new-instance v3, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v3}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 280
    .local v3, "stateDrawable":Landroid/graphics/drawable/StateListDrawable;
    new-array v4, v7, [I

    const v5, -0x101009e

    aput v5, v4, v6

    invoke-virtual {v3, v4, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 281
    new-array v4, v7, [I

    const v5, 0x10100a7

    aput v5, v4, v6

    invoke-virtual {v3, v4, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 282
    new-array v4, v7, [I

    const v5, 0x101009c

    aput v5, v4, v6

    invoke-virtual {v3, v4, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 283
    new-array v4, v7, [I

    const v5, 0x10100a1

    aput v5, v4, v6

    invoke-virtual {v3, v4, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 284
    new-array v4, v6, [I

    invoke-virtual {v3, v4, v1}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 286
    iget-object v4, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton2:Landroid/widget/ImageButton;

    invoke-virtual {v4, v3}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 287
    return-void
.end method

.method public setButtonImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 211
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 212
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 213
    return-void
.end method

.method public setButtonImage_ex(II)V
    .locals 3
    .param p1, "imageResourceId"    # I
    .param p2, "imageResourceStringId"    # I

    .prologue
    .line 216
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 217
    iput p2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButtonStringId:I

    .line 218
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 221
    const v0, 0x7f02000b

    if-ne p1, v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;

    const v2, 0x7f070065

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 224
    :cond_0
    return-void
.end method

.method public setButtonText(I)V
    .locals 2
    .param p1, "text"    # I

    .prologue
    .line 206
    iput p1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mButtonTextResId:I

    .line 207
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;

    iget v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mButtonTextResId:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 208
    return-void
.end method

.method public setEvent(I)V
    .locals 0
    .param p1, "event"    # I

    .prologue
    .line 123
    iput p1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mClickEvent:I

    .line 124
    return-void
.end method

.method public setFocus()V
    .locals 2

    .prologue
    .line 536
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 538
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    .line 539
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    .line 540
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocusFromTouch()Z

    .line 542
    :cond_0
    return-void
.end method

.method public setRulerBubblePos(I)V
    .locals 4
    .param p1, "xPosition"    # I

    .prologue
    .line 509
    const-string/jumbo v1, "Action Bubble"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "xPosition : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mRulerbarTopBubble:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 512
    .local v0, "param":Landroid/widget/FrameLayout$LayoutParams;
    const-string/jumbo v1, "Action Bubble"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "left Margin:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 516
    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mRulerbarTopBubble:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->requestLayout()V

    .line 517
    return-void
.end method

.method public setTextRulerbarBubble(Ljava/lang/String;)V
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 492
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mRulerbarTopBubble:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 493
    return-void
.end method

.method public setTitle(I)V
    .locals 2
    .param p1, "title"    # I

    .prologue
    .line 195
    iput p1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mTitlResId:I

    .line 196
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarTitle:Landroid/widget/TextView;

    iget v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mTitlResId:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 197
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "strTitle"    # Ljava/lang/String;

    .prologue
    .line 201
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    return-void
.end method

.method public setVisibilitySecondImageButton(ZI)V
    .locals 2
    .param p1, "bool"    # Z
    .param p2, "clickEvent"    # I

    .prologue
    .line 183
    iput p2, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mClickEvent2:I

    .line 185
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton2:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 191
    :goto_0
    return-void

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton2:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public show()V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mActionbarView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 172
    return-void
.end method

.method public showRulerBubble()V
    .locals 2

    .prologue
    .line 497
    const-string/jumbo v0, "Action Bubble"

    const-string/jumbo v1, "hideRulerBubble "

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar;->mRulerbarTopBubble:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 499
    return-void
.end method
