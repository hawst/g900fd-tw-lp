.class Lcom/infraware/office/actionbar/MainActionBar$5;
.super Ljava/lang/Object;
.source "MainActionBar.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/actionbar/MainActionBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/actionbar/MainActionBar;


# direct methods
.method constructor <init>(Lcom/infraware/office/actionbar/MainActionBar;)V
    .locals 0

    .prologue
    .line 946
    iput-object p1, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 950
    const-string/jumbo v0, "MainActionBar"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onLongClick : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 951
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$100(Lcom/infraware/office/actionbar/MainActionBar;)I

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_0

    .line 952
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onStopVideo()Z

    .line 954
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->IsViewerMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 955
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move v0, v2

    .line 998
    :goto_0
    return v0

    .line 958
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActionBarView:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$500(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/LinearLayout;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mViewSave:Landroid/widget/ImageButton;
    invoke-static {v3}, Lcom/infraware/office/actionbar/MainActionBar;->access$600(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;

    move-result-object v3

    const v4, 0x7f070131

    invoke-static {v0, v2, v3, v4}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    move v0, v1

    .line 959
    goto :goto_0

    .line 961
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActionBarView:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$500(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/LinearLayout;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mViewFind:Landroid/widget/ImageButton;
    invoke-static {v3}, Lcom/infraware/office/actionbar/MainActionBar;->access$700(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;

    move-result-object v3

    const v4, 0x7f07004f

    invoke-static {v0, v2, v3, v4}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    move v0, v1

    .line 962
    goto :goto_0

    .line 964
    :pswitch_3
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActionBarView:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$500(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/LinearLayout;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mViewUndo:Landroid/widget/ImageButton;
    invoke-static {v3}, Lcom/infraware/office/actionbar/MainActionBar;->access$800(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;

    move-result-object v3

    const v4, 0x7f070244

    invoke-static {v0, v2, v3, v4}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    move v0, v1

    .line 965
    goto :goto_0

    .line 967
    :pswitch_4
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActionBarView:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$500(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/LinearLayout;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mViewRedo:Landroid/widget/ImageButton;
    invoke-static {v3}, Lcom/infraware/office/actionbar/MainActionBar;->access$900(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;

    move-result-object v3

    const v4, 0x7f0701cb

    invoke-static {v0, v2, v3, v4}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    move v0, v1

    .line 968
    goto :goto_0

    .line 970
    :pswitch_5
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActionBarView:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$500(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/LinearLayout;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;
    invoke-static {v3}, Lcom/infraware/office/actionbar/MainActionBar;->access$1000(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;

    move-result-object v3

    const v4, 0x7f070051

    invoke-static {v0, v2, v3, v4}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    move v0, v1

    .line 971
    goto/16 :goto_0

    .line 973
    :pswitch_6
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActionBarView:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$500(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/LinearLayout;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mViewMenu:Landroid/widget/ImageButton;
    invoke-static {v3}, Lcom/infraware/office/actionbar/MainActionBar;->access$300(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;

    move-result-object v3

    const v4, 0x7f0702c4

    invoke-static {v0, v2, v3, v4}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    move v0, v1

    .line 974
    goto/16 :goto_0

    .line 980
    :pswitch_7
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActionBarView:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$500(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/LinearLayout;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPen:Landroid/widget/ImageButton;
    invoke-static {v3}, Lcom/infraware/office/actionbar/MainActionBar;->access$1100(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;

    move-result-object v3

    const v4, 0x7f070058

    invoke-static {v0, v2, v3, v4}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    move v0, v1

    .line 981
    goto/16 :goto_0

    .line 983
    :pswitch_8
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActionBarView:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$500(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/LinearLayout;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarLasso:Landroid/widget/ImageButton;
    invoke-static {v3}, Lcom/infraware/office/actionbar/MainActionBar;->access$1200(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;

    move-result-object v3

    const v4, 0x7f070057

    invoke-static {v0, v2, v3, v4}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    move v0, v1

    .line 984
    goto/16 :goto_0

    .line 986
    :pswitch_9
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActionBarView:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$500(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/LinearLayout;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarErase:Landroid/widget/ImageButton;
    invoke-static {v3}, Lcom/infraware/office/actionbar/MainActionBar;->access$1300(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;

    move-result-object v3

    const v4, 0x7f070055

    invoke-static {v0, v2, v3, v4}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    move v0, v1

    .line 987
    goto/16 :goto_0

    .line 989
    :pswitch_a
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActionBarView:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$500(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/LinearLayout;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarEraseAll:Landroid/widget/ImageButton;
    invoke-static {v3}, Lcom/infraware/office/actionbar/MainActionBar;->access$1400(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;

    move-result-object v3

    const v4, 0x7f070056

    invoke-static {v0, v2, v3, v4}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    move v0, v1

    .line 990
    goto/16 :goto_0

    .line 992
    :pswitch_b
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActionBarView:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$500(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/LinearLayout;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar$5;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPanning:Landroid/widget/ImageButton;
    invoke-static {v3}, Lcom/infraware/office/actionbar/MainActionBar;->access$1500(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;

    move-result-object v3

    const v4, 0x7f070054

    invoke-static {v0, v2, v3, v4}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    move v0, v1

    .line 993
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 998
    goto/16 :goto_0

    .line 955
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0002
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_2
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method
