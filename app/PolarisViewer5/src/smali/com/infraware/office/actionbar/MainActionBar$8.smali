.class Lcom/infraware/office/actionbar/MainActionBar$8;
.super Ljava/lang/Object;
.source "MainActionBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/actionbar/MainActionBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/actionbar/MainActionBar;


# direct methods
.method constructor <init>(Lcom/infraware/office/actionbar/MainActionBar;)V
    .locals 0

    .prologue
    .line 2361
    iput-object p1, p0, Lcom/infraware/office/actionbar/MainActionBar$8;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 2366
    const-string/jumbo v0, "MainActionBar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mToolbarListener:drawmode_clickLisner : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 2367
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$8;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$100(Lcom/infraware/office/actionbar/MainActionBar;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 2368
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$8;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onStopVideo()Z

    .line 2370
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$8;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->hideDictionaryPanel()V

    .line 2377
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$8;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    # setter for: Lcom/infraware/office/actionbar/MainActionBar;->m_TouchCmdID:I
    invoke-static {v0, v1}, Lcom/infraware/office/actionbar/MainActionBar;->access$1602(Lcom/infraware/office/actionbar/MainActionBar;I)I

    .line 2379
    const-string/jumbo v0, "MainActionBar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mToolbarListener:drawmode_Switch1 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$8;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->m_TouchCmdID:I
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$1600(Lcom/infraware/office/actionbar/MainActionBar;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 2380
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$8;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->m_TouchCmdID:I
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$1600(Lcom/infraware/office/actionbar/MainActionBar;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2389
    :goto_0
    return-void

    .line 2386
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$8;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # invokes: Lcom/infraware/office/actionbar/MainActionBar;->drawmode_runToolbar(Landroid/view/View;)V
    invoke-static {v0, p1}, Lcom/infraware/office/actionbar/MainActionBar;->access$1800(Lcom/infraware/office/actionbar/MainActionBar;Landroid/view/View;)V

    goto :goto_0

    .line 2380
    :pswitch_data_0
    .packed-switch 0x7f0b000c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
