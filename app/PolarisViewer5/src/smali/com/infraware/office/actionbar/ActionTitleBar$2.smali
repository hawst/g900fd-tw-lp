.class Lcom/infraware/office/actionbar/ActionTitleBar$2;
.super Ljava/lang/Object;
.source "ActionTitleBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/actionbar/ActionTitleBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/actionbar/ActionTitleBar;


# direct methods
.method constructor <init>(Lcom/infraware/office/actionbar/ActionTitleBar;)V
    .locals 0

    .prologue
    .line 329
    iput-object p1, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v2, 0x8

    .line 332
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton2:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$400(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 333
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mClickEvent2:I
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$500(Lcom/infraware/office/actionbar/ActionTitleBar;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 413
    :goto_0
    :sswitch_0
    return-void

    .line 336
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$100(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    const/16 v1, 0x91

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnActionBarEvent(I)V

    goto :goto_0

    .line 343
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mClickEvent:I
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$600(Lcom/infraware/office/actionbar/ActionTitleBar;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 375
    :sswitch_1
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$100(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/ExportToPdf;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->actionTitleBarButtonClick()V

    goto :goto_0

    .line 346
    :sswitch_2
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$100(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/print/PrintActivity;

    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mClickEvent:I
    invoke-static {v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$600(Lcom/infraware/office/actionbar/ActionTitleBar;)I

    move-result v1

    invoke-virtual {v0, v1, p1}, Lcom/infraware/polarisoffice5/print/PrintActivity;->actionTitleBarButtonClick(ILandroid/view/View;)V

    goto :goto_0

    .line 350
    :sswitch_3
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$100(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/print/PrintActivity;

    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mClickEvent:I
    invoke-static {v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$600(Lcom/infraware/office/actionbar/ActionTitleBar;)I

    move-result v1

    invoke-virtual {v0, v1, p1}, Lcom/infraware/polarisoffice5/print/PrintActivity;->actionTitleBarButtonClick(ILandroid/view/View;)V

    goto :goto_0

    .line 353
    :sswitch_4
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$100(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->actionTitleBarButtonClick()V

    goto :goto_0

    .line 356
    :sswitch_5
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$100(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->actionTitleBarButtonClick()V

    goto :goto_0

    .line 366
    :sswitch_6
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$100(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->actionTitleBarButtonClick()V

    goto :goto_0

    .line 372
    :sswitch_7
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$100(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/16 v1, 0x41

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    goto :goto_0

    .line 378
    :sswitch_8
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$100(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/PasswordActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PasswordActivity;->actionTitleBarButtonClick()V

    goto/16 :goto_0

    .line 381
    :sswitch_9
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$100(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/16 v1, 0x45

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    goto/16 :goto_0

    .line 384
    :sswitch_a
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$100(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->actionTitleBarButtonClick()V

    goto/16 :goto_0

    .line 387
    :sswitch_b
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$100(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/16 v1, 0x50

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    goto/16 :goto_0

    .line 390
    :sswitch_c
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$100(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/16 v1, 0x53

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    .line 391
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton2:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$400(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 394
    :sswitch_d
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$100(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/16 v1, 0x54

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    goto/16 :goto_0

    .line 397
    :sswitch_e
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$100(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/16 v1, 0x79

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    goto/16 :goto_0

    .line 400
    :sswitch_f
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$100(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/16 v1, 0x70

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    goto/16 :goto_0

    .line 403
    :sswitch_10
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$100(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/16 v1, 0x71

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    goto/16 :goto_0

    .line 406
    :sswitch_11
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$100(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->actionTitleBarButtonClick()V

    goto/16 :goto_0

    .line 409
    :sswitch_12
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$100(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/16 v1, 0x105

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    .line 410
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$2;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton2:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$400(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 333
    nop

    :pswitch_data_0
    .packed-switch 0x91
        :pswitch_0
    .end packed-switch

    .line 343
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x7 -> :sswitch_7
        0x9 -> :sswitch_0
        0x12 -> :sswitch_0
        0x21 -> :sswitch_0
        0x26 -> :sswitch_6
        0x31 -> :sswitch_d
        0x32 -> :sswitch_4
        0x33 -> :sswitch_5
        0x44 -> :sswitch_9
        0x46 -> :sswitch_8
        0x47 -> :sswitch_a
        0x49 -> :sswitch_b
        0x51 -> :sswitch_3
        0x52 -> :sswitch_c
        0x56 -> :sswitch_11
        0x68 -> :sswitch_2
        0x69 -> :sswitch_f
        0x71 -> :sswitch_10
        0x79 -> :sswitch_e
        0x104 -> :sswitch_12
    .end sparse-switch
.end method
