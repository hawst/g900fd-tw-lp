.class Lcom/infraware/office/actionbar/MainActionBar$2;
.super Ljava/lang/Object;
.source "MainActionBar.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/actionbar/MainActionBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/actionbar/MainActionBar;


# direct methods
.method constructor <init>(Lcom/infraware/office/actionbar/MainActionBar;)V
    .locals 0

    .prologue
    .line 482
    iput-object p1, p0, Lcom/infraware/office/actionbar/MainActionBar$2;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "event"    # I
    .param p3, "arg2"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v4, 0xe

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 485
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$2;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$100(Lcom/infraware/office/actionbar/MainActionBar;)I

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_0

    .line 486
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$2;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onStopVideo()Z

    .line 488
    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 550
    :goto_0
    return v0

    .line 491
    :cond_1
    sparse-switch p2, :sswitch_data_0

    move v0, v2

    .line 548
    goto :goto_0

    .line 494
    :sswitch_0
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$2;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mViewMenu:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$300(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->clearFocus()V

    .line 495
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$2;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setFocus()V

    :cond_2
    :goto_1
    :sswitch_1
    move v0, v1

    .line 550
    goto :goto_0

    .line 499
    :sswitch_2
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$2;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$400(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$2;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$400(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 500
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$2;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$400(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->dismiss()V

    goto :goto_1

    :sswitch_3
    move v0, v1

    .line 509
    goto :goto_0

    .line 514
    :sswitch_4
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$2;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mViewMenu:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$300(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->clearFocus()V

    .line 515
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$2;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setFocus()V

    move v0, v1

    .line 516
    goto :goto_0

    .line 527
    :sswitch_5
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v4, :cond_3

    .line 530
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$2;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setFocus()V

    .line 533
    :cond_3
    :sswitch_6
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v4, :cond_4

    .line 535
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$2;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mViewMenu:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$300(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->clearFocus()V

    .line 536
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$2;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setFocus()V

    .line 539
    :cond_4
    :sswitch_7
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$2;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$400(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$2;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$400(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 541
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$2;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$400(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->dismiss()V

    .line 542
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$2;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mViewMenu:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$300(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->clearFocus()V

    .line 543
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$2;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setFocus()V

    move v0, v2

    .line 544
    goto/16 :goto_0

    .line 491
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_6
        0x4 -> :sswitch_0
        0x13 -> :sswitch_3
        0x14 -> :sswitch_3
        0x15 -> :sswitch_2
        0x16 -> :sswitch_2
        0x17 -> :sswitch_1
        0x3d -> :sswitch_4
        0x42 -> :sswitch_1
        0x6f -> :sswitch_7
        0x84 -> :sswitch_5
    .end sparse-switch
.end method
