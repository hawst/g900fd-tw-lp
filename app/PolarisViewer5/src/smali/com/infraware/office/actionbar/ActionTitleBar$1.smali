.class Lcom/infraware/office/actionbar/ActionTitleBar$1;
.super Ljava/lang/Object;
.source "ActionTitleBar.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/actionbar/ActionTitleBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/actionbar/ActionTitleBar;


# direct methods
.method constructor <init>(Lcom/infraware/office/actionbar/ActionTitleBar;)V
    .locals 0

    .prologue
    .line 295
    iput-object p1, p0, Lcom/infraware/office/actionbar/ActionTitleBar$1;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 299
    const-string/jumbo v0, "ActionTitleBar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onLongClick : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 313
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 307
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$1;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButtonStringId:I
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$000(Lcom/infraware/office/actionbar/ActionTitleBar;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$1;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$100(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar$1;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActionbarView:Landroid/view/View;
    invoke-static {v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$200(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar$1;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;
    invoke-static {v2}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$300(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/widget/ImageButton;

    move-result-object v2

    const v3, 0x7f070065

    invoke-static {v0, v1, v2, v3}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    .line 311
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 310
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$1;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$100(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar$1;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActionbarView:Landroid/view/View;
    invoke-static {v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$200(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionTitleBar$1;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarImageButton:Landroid/widget/ImageButton;
    invoke-static {v2}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$300(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/widget/ImageButton;

    move-result-object v2

    const v3, 0x7f070061

    invoke-static {v0, v1, v2, v3}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_1

    .line 304
    :pswitch_data_0
    .packed-switch 0x7f0b0014
        :pswitch_0
    .end packed-switch
.end method
