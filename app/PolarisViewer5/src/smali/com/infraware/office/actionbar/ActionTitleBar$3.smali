.class Lcom/infraware/office/actionbar/ActionTitleBar$3;
.super Ljava/lang/Object;
.source "ActionTitleBar.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/actionbar/ActionTitleBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/actionbar/ActionTitleBar;


# direct methods
.method constructor <init>(Lcom/infraware/office/actionbar/ActionTitleBar;)V
    .locals 0

    .prologue
    .line 544
    iput-object p1, p0, Lcom/infraware/office/actionbar/ActionTitleBar$3;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v5, 0xe

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 549
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "[[[[[ onKey "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 550
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-eqz v2, :cond_0

    .line 600
    :goto_0
    :sswitch_0
    return v0

    .line 553
    :cond_0
    sparse-switch p2, :sswitch_data_0

    :goto_1
    move v0, v1

    .line 598
    goto :goto_0

    .line 556
    :sswitch_1
    iget-object v1, p0, Lcom/infraware/office/actionbar/ActionTitleBar$3;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;
    invoke-static {v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$700(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Button;->clearFocus()V

    goto :goto_0

    :sswitch_2
    move v0, v1

    .line 570
    goto :goto_0

    .line 574
    :sswitch_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v5, :cond_1

    .line 576
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v2, "[[[[[ f2"

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 578
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$3;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$700(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->clearFocus()V

    .line 579
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$3;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$100(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_1

    .line 581
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v2, "[[[[[ f3"

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 582
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$3;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$100(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setFocus()V

    .line 587
    :cond_1
    :sswitch_4
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v5, :cond_2

    .line 589
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$3;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$700(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->clearFocus()V

    .line 590
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$3;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$100(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_2

    .line 591
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$3;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$100(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setFocus()V

    .line 595
    :cond_2
    :sswitch_5
    iget-object v0, p0, Lcom/infraware/office/actionbar/ActionTitleBar$3;->this$0:Lcom/infraware/office/actionbar/ActionTitleBar;

    # getter for: Lcom/infraware/office/actionbar/ActionTitleBar;->mActionBarButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->access$700(Lcom/infraware/office/actionbar/ActionTitleBar;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->clearFocus()V

    goto :goto_1

    .line 553
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_4
        0x4 -> :sswitch_1
        0x14 -> :sswitch_2
        0x15 -> :sswitch_0
        0x16 -> :sswitch_0
        0x42 -> :sswitch_0
        0x6f -> :sswitch_5
        0x84 -> :sswitch_3
    .end sparse-switch
.end method
