.class Lcom/infraware/office/actionbar/ActionBarPopupMenu$2;
.super Ljava/lang/Object;
.source "ActionBarPopupMenu.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/actionbar/ActionBarPopupMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/actionbar/ActionBarPopupMenu;


# direct methods
.method constructor <init>(Lcom/infraware/office/actionbar/ActionBarPopupMenu;)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu$2;->this$0:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    .line 168
    const/4 v3, 0x4

    if-eq p2, v3, :cond_0

    const/16 v3, 0x6f

    if-ne p2, v3, :cond_2

    .line 170
    :cond_0
    iget-object v3, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu$2;->this$0:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v3}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->dismiss()V

    .line 298
    :cond_1
    :goto_0
    return v2

    .line 173
    :cond_2
    const/16 v3, 0x14

    if-eq p2, v3, :cond_3

    const/16 v3, 0x13

    if-ne p2, v3, :cond_5

    .line 175
    :cond_3
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_6

    .line 177
    iget-object v3, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu$2;->this$0:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    # getter for: Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mLinearLayout:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->access$000(Lcom/infraware/office/actionbar/ActionBarPopupMenu;)Landroid/widget/LinearLayout;

    move-result-object v3

    if-ne p1, v3, :cond_6

    .line 180
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu$2;->this$0:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    # getter for: Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mLinearLayout:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->access$000(Lcom/infraware/office/actionbar/ActionBarPopupMenu;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 181
    iget-object v3, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu$2;->this$0:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    # getter for: Lcom/infraware/office/actionbar/ActionBarPopupMenu;->mLinearLayout:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->access$000(Lcom/infraware/office/actionbar/ActionBarPopupMenu;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 183
    .local v0, "focusv":Landroid/view/View;
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->isFocusable()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 184
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    .line 180
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 193
    .end local v0    # "focusv":Landroid/view/View;
    .end local v1    # "i":I
    :cond_5
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-ne v3, v2, :cond_6

    const/16 v2, 0x52

    if-ne p2, v2, :cond_6

    .line 194
    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu$2;->this$0:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v2}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 195
    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu$2;->this$0:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v2}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->dismiss()V

    .line 298
    :cond_6
    const/4 v2, 0x0

    goto :goto_0
.end method
