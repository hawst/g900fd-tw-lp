.class public Lcom/infraware/office/actionbar/PopoverMenuItem;
.super Landroid/widget/LinearLayout;
.source "PopoverMenuItem.java"


# instance fields
.field mAfterCheck:Landroid/view/View;

.field mBeforeImage:Landroid/view/View;

.field mMenuID:I

.field mMiddleText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/app/Activity;IILjava/lang/String;)V
    .locals 5
    .param p1, "act"    # Landroid/app/Activity;
    .param p2, "menu_id"    # I
    .param p3, "image_res"    # I
    .param p4, "text"    # Ljava/lang/String;

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 78
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 15
    iput-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mMiddleText:Landroid/widget/TextView;

    .line 16
    iput-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mBeforeImage:Landroid/view/View;

    .line 17
    iput-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mAfterCheck:Landroid/view/View;

    .line 18
    iput v3, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mMenuID:I

    .line 80
    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03003c

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 81
    const v0, 0x7f0b01be

    invoke-virtual {p0, v0}, Lcom/infraware/office/actionbar/PopoverMenuItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mBeforeImage:Landroid/view/View;

    .line 82
    const v0, 0x7f0b01c0

    invoke-virtual {p0, v0}, Lcom/infraware/office/actionbar/PopoverMenuItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mAfterCheck:Landroid/view/View;

    .line 83
    const v0, 0x7f0b01bf

    invoke-virtual {p0, v0}, Lcom/infraware/office/actionbar/PopoverMenuItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mMiddleText:Landroid/widget/TextView;

    .line 84
    iput p2, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mMenuID:I

    .line 86
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mBeforeImage:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mBeforeImage:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mAfterCheck:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 91
    if-nez p3, :cond_4

    .line 92
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mAfterCheck:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 96
    :goto_0
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mMiddleText:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 97
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mMiddleText:Landroid/widget/TextView;

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mMiddleText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSelected(Z)V

    .line 100
    invoke-virtual {p0, v2}, Lcom/infraware/office/actionbar/PopoverMenuItem;->setClickable(Z)V

    .line 102
    invoke-static {}, Lcom/infraware/common/util/Utils;->isICS()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 103
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mMiddleText:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 104
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mMiddleText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mBeforeImage:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 105
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mMiddleText:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/HoveringManager;->setListSubtitleHoveringListener(Landroid/widget/TextView;)Z

    .line 108
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mBeforeImage:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 109
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mBeforeImage:Landroid/view/View;

    iget-object v1, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mMiddleText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 110
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mBeforeImage:Landroid/view/View;

    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/HoveringManager;->setHoveringListener(Landroid/view/View;)Z

    .line 114
    :cond_3
    invoke-virtual {p0, v2}, Lcom/infraware/office/actionbar/PopoverMenuItem;->setFocusable(Z)V

    .line 115
    return-void

    .line 94
    :cond_4
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mAfterCheck:Landroid/view/View;

    invoke-virtual {v0, p3}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public constructor <init>(Landroid/app/Activity;IILjava/lang/String;ZZ)V
    .locals 5
    .param p1, "act"    # Landroid/app/Activity;
    .param p2, "menu_id"    # I
    .param p3, "image_res"    # I
    .param p4, "text"    # Ljava/lang/String;
    .param p5, "check"    # Z
    .param p6, "enable"    # Z

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 23
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 15
    iput-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mMiddleText:Landroid/widget/TextView;

    .line 16
    iput-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mBeforeImage:Landroid/view/View;

    .line 17
    iput-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mAfterCheck:Landroid/view/View;

    .line 18
    iput v3, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mMenuID:I

    .line 25
    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03003c

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 26
    const v0, 0x7f0b01be

    invoke-virtual {p0, v0}, Lcom/infraware/office/actionbar/PopoverMenuItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mBeforeImage:Landroid/view/View;

    .line 27
    const v0, 0x7f0b01c0

    invoke-virtual {p0, v0}, Lcom/infraware/office/actionbar/PopoverMenuItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mAfterCheck:Landroid/view/View;

    .line 28
    const v0, 0x7f0b01bf

    invoke-virtual {p0, v0}, Lcom/infraware/office/actionbar/PopoverMenuItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mMiddleText:Landroid/widget/TextView;

    .line 29
    iput p2, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mMenuID:I

    .line 31
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mBeforeImage:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 32
    if-nez p3, :cond_4

    .line 33
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mBeforeImage:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 38
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mMiddleText:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 39
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mMiddleText:Landroid/widget/TextView;

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mMiddleText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSelected(Z)V

    .line 42
    invoke-virtual {p0, v2}, Lcom/infraware/office/actionbar/PopoverMenuItem;->setClickable(Z)V

    .line 44
    if-eqz p5, :cond_5

    .line 52
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mAfterCheck:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 59
    :goto_1
    invoke-virtual {p0, p6}, Lcom/infraware/office/actionbar/PopoverMenuItem;->setEnabled(Z)V

    .line 61
    invoke-static {}, Lcom/infraware/common/util/Utils;->isICS()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 62
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mMiddleText:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 63
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mMiddleText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mBeforeImage:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 64
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mMiddleText:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/HoveringManager;->setListSubtitleHoveringListener(Landroid/widget/TextView;)Z

    .line 67
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mBeforeImage:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 68
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mBeforeImage:Landroid/view/View;

    iget-object v1, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mMiddleText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 69
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mBeforeImage:Landroid/view/View;

    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/HoveringManager;->setHoveringListener(Landroid/view/View;)Z

    .line 73
    :cond_3
    invoke-virtual {p0, v2}, Lcom/infraware/office/actionbar/PopoverMenuItem;->setFocusable(Z)V

    .line 74
    return-void

    .line 35
    :cond_4
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mBeforeImage:Landroid/view/View;

    invoke-virtual {v0, p3}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    .line 56
    :cond_5
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mAfterCheck:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 130
    invoke-super {p0}, Landroid/widget/LinearLayout;->drawableStateChanged()V

    .line 132
    const/4 v4, 0x0

    .line 133
    .local v4, "isPressedState":Z
    const/4 v3, 0x0

    .line 135
    .local v3, "isFocusedState":Z
    invoke-virtual {p0}, Lcom/infraware/office/actionbar/PopoverMenuItem;->getDrawableState()[I

    move-result-object v6

    .line 136
    .local v6, "states":[I
    move-object v0, v6

    .local v0, "arr$":[I
    array-length v5, v0

    .local v5, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v5, :cond_2

    aget v1, v0, v2

    .line 137
    .local v1, "i":I
    const v7, 0x10100a7

    if-ne v1, v7, :cond_0

    .line 138
    const/4 v4, 0x1

    .line 140
    :cond_0
    const v7, 0x101009c

    if-ne v1, v7, :cond_1

    .line 141
    const/4 v3, 0x1

    .line 136
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 145
    .end local v1    # "i":I
    :cond_2
    iget-object v7, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mBeforeImage:Landroid/view/View;

    if-eqz v7, :cond_4

    .line 146
    iget-object v10, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mBeforeImage:Landroid/view/View;

    if-nez v4, :cond_3

    if-eqz v3, :cond_a

    :cond_3
    move v7, v9

    :goto_1
    invoke-virtual {v10, v7}, Landroid/view/View;->setPressed(Z)V

    .line 147
    :cond_4
    iget-object v7, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mAfterCheck:Landroid/view/View;

    if-eqz v7, :cond_6

    .line 148
    iget-object v10, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mAfterCheck:Landroid/view/View;

    if-nez v4, :cond_5

    if-eqz v3, :cond_b

    :cond_5
    move v7, v9

    :goto_2
    invoke-virtual {v10, v7}, Landroid/view/View;->setPressed(Z)V

    .line 149
    :cond_6
    iget-object v7, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mMiddleText:Landroid/widget/TextView;

    if-eqz v7, :cond_9

    .line 150
    iget-object v7, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mMiddleText:Landroid/widget/TextView;

    if-nez v4, :cond_7

    if-eqz v3, :cond_8

    :cond_7
    move v8, v9

    :cond_8
    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setPressed(Z)V

    .line 151
    :cond_9
    return-void

    :cond_a
    move v7, v8

    .line 146
    goto :goto_1

    :cond_b
    move v7, v8

    .line 148
    goto :goto_2
.end method

.method getMenuId()I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mMenuID:I

    return v0
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 121
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 123
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mBeforeImage:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 124
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mAfterCheck:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 125
    iget-object v0, p0, Lcom/infraware/office/actionbar/PopoverMenuItem;->mMiddleText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 126
    return-void
.end method
