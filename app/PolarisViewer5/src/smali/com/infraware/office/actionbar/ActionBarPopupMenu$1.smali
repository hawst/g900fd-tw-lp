.class Lcom/infraware/office/actionbar/ActionBarPopupMenu$1;
.super Ljava/lang/Object;
.source "ActionBarPopupMenu.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/office/actionbar/ActionBarPopupMenu;-><init>(Landroid/app/Activity;ILandroid/view/View;Landroid/view/View$OnClickListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/actionbar/ActionBarPopupMenu;


# direct methods
.method constructor <init>(Lcom/infraware/office/actionbar/ActionBarPopupMenu;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu$1;->this$0:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 81
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_2

    .line 83
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v0, v2

    .line 84
    .local v0, "x":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v1, v2

    .line 86
    .local v1, "y":I
    if-ltz v0, :cond_0

    if-ltz v1, :cond_0

    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu$1;->this$0:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v2}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->getWidth()I

    move-result v2

    if-gt v0, v2, :cond_0

    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu$1;->this$0:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v2}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->getHeight()I

    move-result v2

    if-le v1, v2, :cond_1

    .line 107
    :cond_0
    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu$1;->this$0:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v2}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->dismiss()V

    .line 115
    .end local v0    # "x":I
    .end local v1    # "y":I
    :cond_1
    :goto_0
    const/4 v2, 0x0

    return v2

    .line 110
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    .line 112
    iget-object v2, p0, Lcom/infraware/office/actionbar/ActionBarPopupMenu$1;->this$0:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v2}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->dismiss()V

    goto :goto_0
.end method
