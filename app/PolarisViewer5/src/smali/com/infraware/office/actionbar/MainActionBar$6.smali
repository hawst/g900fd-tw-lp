.class Lcom/infraware/office/actionbar/MainActionBar$6;
.super Ljava/lang/Object;
.source "MainActionBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/actionbar/MainActionBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/actionbar/MainActionBar;


# direct methods
.method constructor <init>(Lcom/infraware/office/actionbar/MainActionBar;)V
    .locals 0

    .prologue
    .line 1004
    iput-object p1, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 1007
    const-string/jumbo v0, "MainActionBar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "drawmode_clickLisner : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1009
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$100(Lcom/infraware/office/actionbar/MainActionBar;)I

    move-result v0

    if-ne v0, v5, :cond_3

    .line 1010
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onStopVideo()Z

    .line 1012
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getPopupmenu()Landroid/widget/PopupWindow;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1013
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getPopupmenu()Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1014
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getPopupmenu()Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 1021
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1022
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->hideDictionaryPanel()V

    .line 1035
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    # setter for: Lcom/infraware/office/actionbar/MainActionBar;->m_TouchCmdID:I
    invoke-static {v0, v1}, Lcom/infraware/office/actionbar/MainActionBar;->access$1602(Lcom/infraware/office/actionbar/MainActionBar;I)I

    .line 1036
    const-string/jumbo v0, "MainActionBar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "drawmode_Switch1 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->m_TouchCmdID:I
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$1600(Lcom/infraware/office/actionbar/MainActionBar;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1039
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->m_TouchCmdID:I
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$1600(Lcom/infraware/office/actionbar/MainActionBar;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1048
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->m_TouchCmdID:I
    invoke-static {v1}, Lcom/infraware/office/actionbar/MainActionBar;->access$1600(Lcom/infraware/office/actionbar/MainActionBar;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onClickActionBar(I)V

    .line 1050
    :pswitch_0
    const-string/jumbo v0, "MainActionBar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "drawmode_Switch2 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->m_TouchCmdID:I
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$1600(Lcom/infraware/office/actionbar/MainActionBar;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1053
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->m_TouchCmdID:I
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$1600(Lcom/infraware/office/actionbar/MainActionBar;)I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 1185
    :cond_2
    :goto_0
    :pswitch_1
    return-void

    .line 1016
    :cond_3
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$100(Lcom/infraware/office/actionbar/MainActionBar;)I

    move-result v0

    if-ne v0, v4, :cond_0

    .line 1017
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mbSheetLoadComplete:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 1056
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1057
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mHovermanager:Lcom/infraware/polarisoffice5/common/HoveringManager;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$1700(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/polarisoffice5/common/HoveringManager;

    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    const v1, 0x7f0b0002

    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/HoveringManager;->dismissImageButtonHover(Landroid/app/Activity;I)Z

    .line 1059
    :cond_4
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1060
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/infraware/office/baseframe/EvBaseView;->onShowIme(Z)V

    .line 1063
    :cond_5
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0, v3, p1}, Lcom/infraware/office/actionbar/MainActionBar;->actionBarPopupWindow(ILandroid/view/View;)Z

    .line 1066
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1068
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismissAll()V

    goto :goto_0

    .line 1072
    :pswitch_3
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$1000(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1073
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->hideAllToolbarDelay()Z

    .line 1074
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$100(Lcom/infraware/office/actionbar/MainActionBar;)I

    move-result v0

    if-ne v0, v5, :cond_6

    .line 1076
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v0, v3}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onHidePSlideManage(Z)Z

    .line 1079
    :cond_6
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1080
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mHovermanager:Lcom/infraware/polarisoffice5/common/HoveringManager;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$1700(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/polarisoffice5/common/HoveringManager;

    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    const v1, 0x7f0b0009

    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/HoveringManager;->dismissImageButtonHover(Landroid/app/Activity;I)Z

    .line 1081
    :cond_7
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mViewFind:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$700(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1083
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1085
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mViewMenu:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$300(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1086
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mViewSave:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$600(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1087
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    goto/16 :goto_0

    .line 1093
    :pswitch_4
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$100(Lcom/infraware/office/actionbar/MainActionBar;)I

    move-result v0

    if-ne v0, v4, :cond_8

    .line 1095
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v6}, Lcom/infraware/office/evengine/EvInterface;->IRedoUndo(I)V

    .line 1098
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->SupportUndoAutofilter()V

    goto/16 :goto_0

    .line 1105
    :cond_8
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v6}, Lcom/infraware/office/evengine/EvInterface;->IRedoUndo(I)V

    goto/16 :goto_0

    .line 1112
    :pswitch_5
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$100(Lcom/infraware/office/actionbar/MainActionBar;)I

    move-result v0

    if-ne v0, v4, :cond_9

    .line 1114
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v3}, Lcom/infraware/office/evengine/EvInterface;->IRedoUndo(I)V

    .line 1118
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->SupportUndoAutofilter()V

    goto/16 :goto_0

    .line 1125
    :cond_9
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v3}, Lcom/infraware/office/evengine/EvInterface;->IRedoUndo(I)V

    goto/16 :goto_0

    .line 1136
    :pswitch_6
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 1137
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mHovermanager:Lcom/infraware/polarisoffice5/common/HoveringManager;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$1700(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/polarisoffice5/common/HoveringManager;

    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    const v1, 0x7f0b0008

    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/HoveringManager;->dismissImageButtonHover(Landroid/app/Activity;I)Z

    .line 1139
    :cond_a
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/infraware/office/baseframe/EvBaseView;->onShowIme(Z)V

    .line 1140
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 1141
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$1000(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1143
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->hideAllToolbarDelay()Z

    .line 1151
    :goto_1
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$400(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$400(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1153
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->dismissMenuPopup()Z

    goto/16 :goto_0

    .line 1147
    :cond_b
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    .line 1148
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->drawMode_showToolbar()V

    goto :goto_1

    .line 1158
    :pswitch_7
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 1159
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mHovermanager:Lcom/infraware/polarisoffice5/common/HoveringManager;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$1700(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/polarisoffice5/common/HoveringManager;

    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    const v1, 0x7f0b000a

    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/HoveringManager;->dismissImageButtonHover(Landroid/app/Activity;I)Z

    .line 1160
    :cond_c
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 1161
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/infraware/office/baseframe/EvBaseView;->onShowIme(Z)V

    .line 1164
    :cond_d
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0, v5, p1}, Lcom/infraware/office/actionbar/MainActionBar;->actionBarPopupWindow(ILandroid/view/View;)Z

    .line 1169
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1171
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismiss()V

    goto/16 :goto_0

    .line 1180
    :pswitch_8
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    iget-object v1, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActionBarView:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/infraware/office/actionbar/MainActionBar;->access$500(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/LinearLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$6;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->m_TouchCmdID:I
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$1600(Lcom/infraware/office/actionbar/MainActionBar;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    # invokes: Lcom/infraware/office/actionbar/MainActionBar;->drawmode_runToolbar(Landroid/view/View;)V
    invoke-static {v0, v1}, Lcom/infraware/office/actionbar/MainActionBar;->access$1800(Lcom/infraware/office/actionbar/MainActionBar;Landroid/view/View;)V

    goto/16 :goto_0

    .line 1039
    :pswitch_data_0
    .packed-switch 0x7f0b0003
        :pswitch_0
    .end packed-switch

    .line 1053
    :pswitch_data_1
    .packed-switch 0x7f0b0002
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_6
        :pswitch_3
        :pswitch_7
        :pswitch_1
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method
