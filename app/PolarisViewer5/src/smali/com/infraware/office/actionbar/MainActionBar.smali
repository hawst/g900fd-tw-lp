.class public Lcom/infraware/office/actionbar/MainActionBar;
.super Ljava/lang/Object;
.source "MainActionBar.java"

# interfaces
.implements Landroid/view/textservice/SpellCheckerSession$SpellCheckerSessionListener;
.implements Lcom/infraware/office/baseframe/EvBaseE$EV_ACTIONBAR_EVENT;
.implements Lcom/infraware/office/evengine/E$EV_DOCEXTENSION_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_DOCTYPE;
.implements Lcom/infraware/office/evengine/E$EV_EDIT_CURSOR_MODE;
.implements Lcom/infraware/office/evengine/E$EV_EDIT_OBJECT_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_GUI_EVENT;
.implements Lcom/infraware/office/evengine/E$EV_KEY_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_PEN_MODE;
.implements Lcom/infraware/office/evengine/E$EV_REDO_UNDO;
.implements Lcom/infraware/office/evengine/E$EV_STATUS;
.implements Lcom/infraware/office/evengine/E$EV_VIEWMODE_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_ZOOM_TYPE;
.implements Lcom/infraware/polarisoffice5/common/LocaleChangeListener;


# static fields
.field public static final ACTIONBAR_DRAW_LASSO:I = 0x3

.field public static final ACTIONBAR_DRAW_NONE:I = 0x0

.field public static final ACTIONBAR_DRAW_SHAPE:I = 0x1

.field public static final ACTIONBAR_DRAW_TABLE:I = 0x2

.field public static final ACTIONBAR_MENU:I = 0x3

.field public static final ACTIONBAR_PRINT_MENU:I = 0x6

.field public static final ACTIONBAR_SAVE:I = 0x0

.field private static final INSERT_SHAPE:I = 0x1

.field private static final INSERT_SYMBOL:I = 0x2


# instance fields
.field ActionListItemNumber:F

.field private final LOG_CAT:Ljava/lang/String;

.field private delayButtonHandler:Landroid/os/Handler;

.field private delayButtonRunnable:Ljava/lang/Runnable;

.field mActionBarListener:Landroid/view/View$OnClickListener;

.field mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mActionBarView:Landroid/widget/LinearLayout;

.field private mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

.field private mBWPInfo:Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

.field private mDocExtType:I

.field private mDocOpenPath:Ljava/lang/String;

.field private mDocType:I

.field private mDrawMode:I

.field mFileName:Ljava/lang/String;

.field public mFileNameShow:Z

.field private mHeadTypePanelTemp:I

.field private mHovermanager:Lcom/infraware/polarisoffice5/common/HoveringManager;

.field mKeyListener:Landroid/view/View$OnKeyListener;

.field private mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

.field public mPassWordActivity:Z

.field private mPenCustomColor:Z

.field private mPenDrawToolbar:Landroid/widget/LinearLayout;

.field private mPenDrawToolbarErase:Landroid/widget/ImageButton;

.field private mPenDrawToolbarEraseAll:Landroid/widget/ImageButton;

.field private mPenDrawToolbarLasso:Landroid/widget/ImageButton;

.field private mPenDrawToolbarPanning:Landroid/widget/ImageButton;

.field private mPenDrawToolbarPen:Landroid/widget/ImageButton;

.field private mPopoverListener:Landroid/view/View$OnClickListener;

.field private mShowPanelHandler:Landroid/os/Handler;

.field private mShowPanelRunnable:Ljava/lang/Runnable;

.field private mStopUndoRedoCheck:Z

.field mToolbarListener:Landroid/view/View$OnClickListener;

.field private mTotalLoadComplete:Z

.field private mTypePanelTemp:I

.field private mViewActionBar:Landroid/widget/LinearLayout;

.field private mViewDraw:Landroid/widget/ImageButton;

.field private mViewFind:Landroid/widget/ImageButton;

.field private mViewMenu:Landroid/widget/ImageButton;

.field private mViewRedo:Landroid/widget/ImageButton;

.field private mViewRoot:Landroid/widget/LinearLayout;

.field private mViewSave:Landroid/widget/ImageButton;

.field private mViewTitle:Landroid/widget/TextView;

.field private mViewUndo:Landroid/widget/ImageButton;

.field private mViewUndoRedoLayout:Landroid/widget/LinearLayout;

.field private m_TouchCmdID:I

.field private m_bNoMarginView:Z

.field private m_nEditStatus:J

.field public mbSamsungPrint:Z

.field public mbUndoRedoStatus:Z


# direct methods
.method public constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;ILjava/lang/String;)V
    .locals 9
    .param p1, "activity"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .param p2, "docType"    # I
    .param p3, "openFilePath"    # Ljava/lang/String;

    .prologue
    const v8, 0x7f0202dd

    const/16 v7, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const-string/jumbo v2, "MainActionBar"

    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->LOG_CAT:Ljava/lang/String;

    .line 67
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 70
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mBWPInfo:Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    .line 72
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarView:Landroid/widget/LinearLayout;

    .line 73
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbar:Landroid/widget/LinearLayout;

    .line 74
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRoot:Landroid/widget/LinearLayout;

    .line 75
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewActionBar:Landroid/widget/LinearLayout;

    .line 77
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewSave:Landroid/widget/ImageButton;

    .line 78
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewTitle:Landroid/widget/TextView;

    .line 79
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndoRedoLayout:Landroid/widget/LinearLayout;

    .line 80
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndo:Landroid/widget/ImageButton;

    .line 81
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRedo:Landroid/widget/ImageButton;

    .line 83
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    .line 84
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewFind:Landroid/widget/ImageButton;

    .line 85
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewMenu:Landroid/widget/ImageButton;

    .line 87
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPen:Landroid/widget/ImageButton;

    .line 88
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarLasso:Landroid/widget/ImageButton;

    .line 89
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarErase:Landroid/widget/ImageButton;

    .line 90
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarEraseAll:Landroid/widget/ImageButton;

    .line 91
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPanning:Landroid/widget/ImageButton;

    .line 93
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mFileName:Ljava/lang/String;

    .line 107
    iput v5, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDrawMode:I

    .line 108
    iput v5, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I

    .line 109
    iput v5, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocExtType:I

    .line 110
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocOpenPath:Ljava/lang/String;

    .line 112
    const/4 v2, 0x0

    iput v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->ActionListItemNumber:F

    .line 113
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->m_nEditStatus:J

    .line 114
    iput v5, p0, Lcom/infraware/office/actionbar/MainActionBar;->m_TouchCmdID:I

    .line 116
    iput-boolean v5, p0, Lcom/infraware/office/actionbar/MainActionBar;->m_bNoMarginView:Z

    .line 118
    iput-boolean v5, p0, Lcom/infraware/office/actionbar/MainActionBar;->mTotalLoadComplete:Z

    .line 119
    iput-boolean v5, p0, Lcom/infraware/office/actionbar/MainActionBar;->mStopUndoRedoCheck:Z

    .line 121
    iput-boolean v5, p0, Lcom/infraware/office/actionbar/MainActionBar;->mFileNameShow:Z

    .line 122
    iput-boolean v5, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPassWordActivity:Z

    .line 123
    iput-boolean v5, p0, Lcom/infraware/office/actionbar/MainActionBar;->mbSamsungPrint:Z

    .line 125
    iput-boolean v5, p0, Lcom/infraware/office/actionbar/MainActionBar;->mbUndoRedoStatus:Z

    .line 127
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPopoverListener:Landroid/view/View$OnClickListener;

    .line 128
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    .line 131
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mShowPanelHandler:Landroid/os/Handler;

    .line 132
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mShowPanelRunnable:Ljava/lang/Runnable;

    .line 133
    iput-boolean v5, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenCustomColor:Z

    .line 134
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mHovermanager:Lcom/infraware/polarisoffice5/common/HoveringManager;

    .line 140
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->delayButtonHandler:Landroid/os/Handler;

    .line 141
    iput-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->delayButtonRunnable:Ljava/lang/Runnable;

    .line 482
    new-instance v2, Lcom/infraware/office/actionbar/MainActionBar$2;

    invoke-direct {v2, p0}, Lcom/infraware/office/actionbar/MainActionBar$2;-><init>(Lcom/infraware/office/actionbar/MainActionBar;)V

    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mKeyListener:Landroid/view/View$OnKeyListener;

    .line 946
    new-instance v2, Lcom/infraware/office/actionbar/MainActionBar$5;

    invoke-direct {v2, p0}, Lcom/infraware/office/actionbar/MainActionBar$5;-><init>(Lcom/infraware/office/actionbar/MainActionBar;)V

    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 1004
    new-instance v2, Lcom/infraware/office/actionbar/MainActionBar$6;

    invoke-direct {v2, p0}, Lcom/infraware/office/actionbar/MainActionBar$6;-><init>(Lcom/infraware/office/actionbar/MainActionBar;)V

    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarListener:Landroid/view/View$OnClickListener;

    .line 1224
    iput v5, p0, Lcom/infraware/office/actionbar/MainActionBar;->mTypePanelTemp:I

    .line 1225
    iput v5, p0, Lcom/infraware/office/actionbar/MainActionBar;->mHeadTypePanelTemp:I

    .line 2361
    new-instance v2, Lcom/infraware/office/actionbar/MainActionBar$8;

    invoke-direct {v2, p0}, Lcom/infraware/office/actionbar/MainActionBar$8;-><init>(Lcom/infraware/office/actionbar/MainActionBar;)V

    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mToolbarListener:Landroid/view/View$OnClickListener;

    .line 144
    iput-object p1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 145
    iput-object p3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocOpenPath:Ljava/lang/String;

    .line 146
    iput v5, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDrawMode:I

    .line 148
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const v3, 0x7f0b010f

    invoke-virtual {v2, v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 149
    .local v1, "layout":Landroid/widget/LinearLayout;
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 150
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const/high16 v2, 0x7f030000

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarView:Landroid/widget/LinearLayout;

    .line 153
    const/high16 v2, 0x7f0b0000

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRoot:Landroid/widget/LinearLayout;

    .line 154
    const v2, 0x7f0b0001

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewActionBar:Landroid/widget/LinearLayout;

    .line 155
    const v2, 0x7f0b000b

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbar:Landroid/widget/LinearLayout;

    .line 156
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbar:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 158
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRoot:Landroid/widget/LinearLayout;

    const v3, 0x7f0b0002

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewSave:Landroid/widget/ImageButton;

    .line 159
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRoot:Landroid/widget/LinearLayout;

    const v3, 0x7f0b0003

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewTitle:Landroid/widget/TextView;

    .line 160
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRoot:Landroid/widget/LinearLayout;

    const v3, 0x7f0b0004

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndoRedoLayout:Landroid/widget/LinearLayout;

    .line 161
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRoot:Landroid/widget/LinearLayout;

    const v3, 0x7f0b0005

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndo:Landroid/widget/ImageButton;

    .line 162
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRoot:Landroid/widget/LinearLayout;

    const v3, 0x7f0b0006

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRedo:Landroid/widget/ImageButton;

    .line 164
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRoot:Landroid/widget/LinearLayout;

    const v3, 0x7f0b0008

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    .line 165
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRoot:Landroid/widget/LinearLayout;

    const v3, 0x7f0b0009

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewFind:Landroid/widget/ImageButton;

    .line 166
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRoot:Landroid/widget/LinearLayout;

    const v3, 0x7f0b000a

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewMenu:Landroid/widget/ImageButton;

    .line 169
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRoot:Landroid/widget/LinearLayout;

    const v3, 0x7f0b000c

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPen:Landroid/widget/ImageButton;

    .line 170
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRoot:Landroid/widget/LinearLayout;

    const v3, 0x7f0b000d

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarLasso:Landroid/widget/ImageButton;

    .line 171
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRoot:Landroid/widget/LinearLayout;

    const v3, 0x7f0b000e

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarErase:Landroid/widget/ImageButton;

    .line 172
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRoot:Landroid/widget/LinearLayout;

    const v3, 0x7f0b000f

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarEraseAll:Landroid/widget/ImageButton;

    .line 173
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRoot:Landroid/widget/LinearLayout;

    const v3, 0x7f0b0010

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPanning:Landroid/widget/ImageButton;

    .line 174
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewSave:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewSave:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setLongClickable(Z)V

    .line 176
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewSave:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 179
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndoRedoLayout:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 181
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndo:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndo:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setLongClickable(Z)V

    .line 183
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndo:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 186
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRedo:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 187
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRedo:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setLongClickable(Z)V

    .line 188
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRedo:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 191
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 192
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setLongClickable(Z)V

    .line 193
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 196
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewFind:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewFind:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setLongClickable(Z)V

    .line 198
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewFind:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 200
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewMenu:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 201
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewMenu:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setLongClickable(Z)V

    .line 202
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewMenu:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 206
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPen:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mToolbarListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 207
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPen:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setLongClickable(Z)V

    .line 208
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPen:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 210
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarLasso:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mToolbarListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 211
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarLasso:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setLongClickable(Z)V

    .line 212
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarLasso:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 214
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarErase:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mToolbarListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 215
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarErase:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setLongClickable(Z)V

    .line 216
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarErase:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 218
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarEraseAll:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mToolbarListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 219
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarEraseAll:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setLongClickable(Z)V

    .line 220
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarEraseAll:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 222
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPanning:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mToolbarListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 223
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPanning:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setLongClickable(Z)V

    .line 224
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPanning:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 227
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndoRedoLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 228
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 230
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewSave:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 231
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewTitle:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 232
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndoRedoLayout:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 233
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndo:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 234
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRedo:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 235
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 236
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewFind:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 238
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewMenu:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 240
    iput p2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocExtType:I

    .line 241
    iget v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocExtType:I

    sparse-switch v2, :sswitch_data_0

    .line 276
    iput v5, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I

    .line 277
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    invoke-virtual {v2, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 278
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewFind:Landroid/widget/ImageButton;

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 284
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/office/actionbar/MainActionBar;->updateActionBarIcon()V

    .line 286
    :try_start_0
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string/jumbo v3, "com.sec.android.app.mobileprint"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 287
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mbSamsungPrint:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 303
    :goto_1
    new-instance v2, Lcom/infraware/office/actionbar/MainActionBar$1;

    invoke-direct {v2, p0}, Lcom/infraware/office/actionbar/MainActionBar$1;-><init>(Lcom/infraware/office/actionbar/MainActionBar;)V

    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPopoverListener:Landroid/view/View$OnClickListener;

    .line 479
    return-void

    .line 246
    :sswitch_0
    iput v6, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I

    .line 247
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewActionBar:Landroid/widget/LinearLayout;

    const v3, 0x7f0202dc

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 251
    :sswitch_1
    const/4 v2, 0x3

    iput v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I

    .line 252
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewActionBar:Landroid/widget/LinearLayout;

    const v3, 0x7f0202df

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 256
    :sswitch_2
    const/4 v2, 0x2

    iput v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I

    .line 257
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewActionBar:Landroid/widget/LinearLayout;

    const v3, 0x7f0202de

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 260
    :sswitch_3
    const/4 v2, 0x5

    iput v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I

    .line 262
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewActionBar:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v8}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 265
    :sswitch_4
    const/4 v2, 0x6

    iput v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I

    .line 267
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewActionBar:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v8}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 268
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    invoke-virtual {v2, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 271
    :sswitch_5
    const/16 v2, 0x26

    iput v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I

    .line 273
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewActionBar:Landroid/widget/LinearLayout;

    const v3, 0x7f0202de

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 289
    :catch_0
    move-exception v2

    goto :goto_1

    .line 241
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_0
        0x3 -> :sswitch_4
        0x5 -> :sswitch_2
        0x6 -> :sswitch_3
        0x12 -> :sswitch_0
        0x13 -> :sswitch_1
        0x14 -> :sswitch_2
        0x25 -> :sswitch_0
        0x26 -> :sswitch_5
    .end sparse-switch
.end method

.method static synthetic access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/MainActionBar;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/office/actionbar/MainActionBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/MainActionBar;

    .prologue
    .line 60
    iget v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I

    return v0
.end method

.method static synthetic access$1000(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/MainActionBar;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/MainActionBar;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPen:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/MainActionBar;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarLasso:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/MainActionBar;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarErase:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/MainActionBar;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarEraseAll:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/MainActionBar;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPanning:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/infraware/office/actionbar/MainActionBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/MainActionBar;

    .prologue
    .line 60
    iget v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->m_TouchCmdID:I

    return v0
.end method

.method static synthetic access$1602(Lcom/infraware/office/actionbar/MainActionBar;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/office/actionbar/MainActionBar;
    .param p1, "x1"    # I

    .prologue
    .line 60
    iput p1, p0, Lcom/infraware/office/actionbar/MainActionBar;->m_TouchCmdID:I

    return p1
.end method

.method static synthetic access$1700(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/polarisoffice5/common/HoveringManager;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/MainActionBar;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mHovermanager:Lcom/infraware/polarisoffice5/common/HoveringManager;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/infraware/office/actionbar/MainActionBar;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/office/actionbar/MainActionBar;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/infraware/office/actionbar/MainActionBar;->drawmode_runToolbar(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1900(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/MainActionBar;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbar:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$200(Lcom/infraware/office/actionbar/MainActionBar;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/MainActionBar;

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->m_bNoMarginView:Z

    return v0
.end method

.method static synthetic access$202(Lcom/infraware/office/actionbar/MainActionBar;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/office/actionbar/MainActionBar;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/infraware/office/actionbar/MainActionBar;->m_bNoMarginView:Z

    return p1
.end method

.method static synthetic access$300(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/MainActionBar;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewMenu:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$400(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/actionbar/ActionBarPopupMenu;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/MainActionBar;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    return-object v0
.end method

.method static synthetic access$500(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/MainActionBar;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarView:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$600(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/MainActionBar;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewSave:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$700(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/MainActionBar;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewFind:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$800(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/MainActionBar;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndo:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$900(Lcom/infraware/office/actionbar/MainActionBar;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/actionbar/MainActionBar;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRedo:Landroid/widget/ImageButton;

    return-object v0
.end method

.method private checkPopoverItemEnable(I)Z
    .locals 12
    .param p1, "event"    # I

    .prologue
    .line 1371
    iget-object v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v8, v8, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-nez v8, :cond_0

    .line 1372
    const/4 v8, 0x1

    .line 1613
    :goto_0
    return v8

    .line 1374
    :cond_0
    iget v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I

    sparse-switch v8, :sswitch_data_0

    .line 1613
    :cond_1
    :goto_1
    :sswitch_0
    const/4 v8, 0x1

    goto :goto_0

    .line 1378
    :sswitch_1
    sparse-switch p1, :sswitch_data_1

    goto :goto_1

    .line 1381
    :sswitch_2
    iget-object v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v8, v8, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v8}, Lcom/infraware/office/evengine/EvInterface;->IGetBWPOpInfo_Editor()Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    move-result-object v8

    iget v8, v8, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nStatusOP:I

    and-int/lit8 v8, v8, 0x10

    const/16 v9, 0x10

    if-eq v8, v9, :cond_1

    .line 1382
    const/4 v8, 0x0

    goto :goto_0

    .line 1387
    :sswitch_3
    iget-boolean v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPassWordActivity:Z

    if-eqz v8, :cond_2

    .line 1388
    const/4 v8, 0x0

    goto :goto_0

    .line 1393
    :cond_2
    iget-object v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v8, v8, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v8}, Lcom/infraware/office/evengine/EvInterface;->IGetBWPOpInfo_Editor()Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    move-result-object v8

    iget v8, v8, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nStatusOP:I

    and-int/lit8 v8, v8, 0x4

    const/4 v9, 0x4

    if-eq v8, v9, :cond_3

    iget-boolean v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mTotalLoadComplete:Z

    const/4 v9, 0x1

    if-ne v8, v9, :cond_4

    :cond_3
    invoke-virtual {p0}, Lcom/infraware/office/actionbar/MainActionBar;->getDocType()I

    move-result v8

    const/4 v9, 0x3

    if-ne v8, v9, :cond_5

    iget-object v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v8, v8, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v8}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v8

    iget v8, v8, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nTotalPages:I

    const/16 v9, 0x1f4

    if-le v8, v9, :cond_5

    .line 1396
    :cond_4
    const/4 v8, 0x0

    goto :goto_0

    .line 1398
    :cond_5
    iget-boolean v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mTotalLoadComplete:Z

    const/4 v9, 0x1

    if-eq v8, v9, :cond_1

    .line 1399
    const/4 v8, 0x0

    goto :goto_0

    .line 1403
    :sswitch_4
    iget-object v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v8, v8, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v8}, Lcom/infraware/office/evengine/EvInterface;->IGetBWPOpInfo_Editor()Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    move-result-object v8

    iget v8, v8, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nStatusOP:I

    and-int/lit8 v8, v8, 0x10

    const/16 v9, 0x10

    if-ne v8, v9, :cond_1

    .line 1404
    const/4 v8, 0x0

    goto :goto_0

    .line 1408
    :sswitch_5
    iget-object v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v8, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getReflowText()Z

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_1

    .line 1409
    const/4 v8, 0x0

    goto :goto_0

    .line 1413
    :sswitch_6
    iget-boolean v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPassWordActivity:Z

    if-eqz v8, :cond_1

    .line 1414
    const/4 v8, 0x0

    goto :goto_0

    .line 1417
    :sswitch_7
    iget-object v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v8, v8, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v8}, Lcom/infraware/office/evengine/EvInterface;->IsWebMode()I

    move-result v7

    .line 1418
    .local v7, "nWebMode":I
    const/4 v8, 0x1

    if-ne v7, v8, :cond_1

    .line 1420
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->m_bNoMarginView:Z

    .line 1421
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1425
    .end local v7    # "nWebMode":I
    :sswitch_8
    iget-object v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v8, v8, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v8}, Lcom/infraware/office/evengine/EvInterface;->IGetCaretInfo_Editor()Lcom/infraware/office/evengine/EV$CARET_INFO;

    move-result-object v8

    iget v5, v8, Lcom/infraware/office/evengine/EV$CARET_INFO;->bCaret:I

    .line 1427
    .local v5, "nCaret":I
    const/4 v8, 0x1

    if-eq v5, v8, :cond_6

    const/4 v8, 0x2

    if-ne v5, v8, :cond_7

    .line 1428
    :cond_6
    const/4 v8, 0x1

    goto/16 :goto_0

    .line 1430
    :cond_7
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1435
    .end local v5    # "nCaret":I
    :sswitch_9
    const/16 v8, 0x10

    if-ne p1, v8, :cond_8

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HYPERLINK()Z

    move-result v8

    if-nez v8, :cond_8

    .line 1437
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1445
    :cond_8
    iget-object v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v8, v8, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v8}, Lcom/infraware/office/evengine/EvInterface;->IGetHyperLinkInfo_Editor()Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-result-object v2

    .line 1447
    .local v2, "hyperlinkInfo":Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;
    iget-object v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v8, v8, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v8}, Lcom/infraware/office/evengine/EvInterface;->IGetCaretInfo_Editor()Lcom/infraware/office/evengine/EV$CARET_INFO;

    move-result-object v8

    iget v6, v8, Lcom/infraware/office/evengine/EV$CARET_INFO;->bCaret:I

    .line 1448
    .local v6, "nCaretMode":I
    iget-object v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v8}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/office/baseframe/EvBaseView;->GetObjCtrlType()I

    move-result v0

    .line 1450
    .local v0, "ObjType":I
    const/4 v8, 0x3

    if-ne v8, v0, :cond_9

    iget-object v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v8, v8, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v8}, Lcom/infraware/office/evengine/EvInterface;->IGetHyperLinkInfo_Enable()Z

    move-result v8

    if-nez v8, :cond_9

    .line 1451
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1454
    :cond_9
    const/4 v8, 0x4

    if-ne v8, v0, :cond_a

    .line 1455
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1457
    :cond_a
    const/4 v8, 0x5

    if-eq v8, v0, :cond_b

    const/4 v8, 0x7

    if-eq v8, v0, :cond_b

    const/4 v8, 0x6

    if-eq v8, v0, :cond_b

    const/16 v8, 0x12

    if-ne v8, v0, :cond_c

    .line 1458
    :cond_b
    const/4 v8, 0x1

    goto/16 :goto_0

    .line 1460
    :cond_c
    if-eqz v0, :cond_d

    const/4 v8, 0x3

    if-ne v8, v0, :cond_f

    :cond_d
    const/4 v8, 0x1

    if-eq v6, v8, :cond_e

    const/4 v8, 0x3

    if-eq v6, v8, :cond_e

    const/4 v8, 0x2

    if-ne v6, v8, :cond_f

    .line 1463
    :cond_e
    const/4 v8, 0x1

    goto/16 :goto_0

    .line 1467
    :cond_f
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1470
    .end local v0    # "ObjType":I
    .end local v2    # "hyperlinkInfo":Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;
    .end local v6    # "nCaretMode":I
    :sswitch_a
    iget-object v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v8}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget v8, v8, Landroid/content/res/Configuration;->orientation:I

    const/4 v9, 0x1

    if-eq v8, v9, :cond_1

    .line 1471
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1475
    :sswitch_b
    iget-object v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v8}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget v8, v8, Landroid/content/res/Configuration;->orientation:I

    const/4 v9, 0x1

    if-eq v8, v9, :cond_1

    .line 1476
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1485
    :sswitch_c
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v8

    iget v8, v8, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nTotalPages:I

    const/16 v9, 0x1f4

    if-gt v8, v9, :cond_10

    iget-boolean v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mTotalLoadComplete:Z

    if-nez v8, :cond_1

    .line 1486
    :cond_10
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1491
    :sswitch_d
    iget-object v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v8, v8, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v8}, Lcom/infraware/office/evengine/EvInterface;->IGetSummaryData()Lcom/infraware/office/evengine/EV$SUMMARY_DATA;

    move-result-object v1

    .line 1492
    .local v1, "data":Lcom/infraware/office/evengine/EV$SUMMARY_DATA;
    iget v8, v1, Lcom/infraware/office/evengine/EV$SUMMARY_DATA;->nWords:I

    if-nez v8, :cond_1

    .line 1493
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1500
    .end local v1    # "data":Lcom/infraware/office/evengine/EV$SUMMARY_DATA;
    :sswitch_e
    sparse-switch p1, :sswitch_data_2

    goto/16 :goto_1

    .line 1503
    :sswitch_f
    iget-object v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v8, v8, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v8}, Lcom/infraware/office/evengine/EvInterface;->IGetEditStauts_Editor()J

    move-result-wide v8

    const-wide/16 v10, 0x10

    and-long/2addr v8, v10

    const-wide/16 v10, 0x10

    cmp-long v8, v8, v10

    if-eqz v8, :cond_11

    .line 1504
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1506
    :cond_11
    iget-boolean v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPassWordActivity:Z

    if-eqz v8, :cond_1

    .line 1507
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1511
    :sswitch_10
    iget-boolean v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPassWordActivity:Z

    if-eqz v8, :cond_12

    .line 1512
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1514
    :cond_12
    iget-boolean v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mTotalLoadComplete:Z

    const/4 v9, 0x1

    if-eq v8, v9, :cond_1

    .line 1515
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1519
    :sswitch_11
    iget-object v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v8, v8, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v8}, Lcom/infraware/office/evengine/EvInterface;->IGetEditStauts_Editor()J

    move-result-wide v8

    const-wide/16 v10, 0x10

    and-long/2addr v8, v10

    const-wide/16 v10, 0x10

    cmp-long v8, v8, v10

    if-nez v8, :cond_1

    .line 1520
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1526
    :sswitch_12
    iget-object v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v8, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsCellSelected()Z

    move-result v8

    if-nez v8, :cond_13

    .line 1527
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1528
    :cond_13
    iget-object v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v8, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsProtectSheet()Z

    move-result v8

    if-nez v8, :cond_14

    iget-boolean v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPassWordActivity:Z

    if-eqz v8, :cond_1

    .line 1529
    :cond_14
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1532
    :sswitch_13
    iget-object v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v8, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsCanSort()Z

    move-result v8

    if-nez v8, :cond_1

    .line 1533
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1536
    :sswitch_14
    iget-boolean v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPassWordActivity:Z

    if-eqz v8, :cond_1

    .line 1537
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1540
    :sswitch_15
    iget-object v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v8, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsWholeCols()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1541
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1544
    :sswitch_16
    iget-object v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v8, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsWholeRows()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1545
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1549
    :sswitch_17
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HYPERLINK()Z

    move-result v8

    if-nez v8, :cond_15

    .line 1550
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1552
    :cond_15
    iget-object v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v8}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/office/baseframe/EvBaseView;->GetObjCtrlType()I

    move-result v0

    .line 1554
    .restart local v0    # "ObjType":I
    const/4 v8, 0x3

    if-ne v8, v0, :cond_16

    iget-object v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v8, v8, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v8}, Lcom/infraware/office/evengine/EvInterface;->IGetHyperLinkInfo_Enable()Z

    move-result v8

    if-nez v8, :cond_16

    .line 1555
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1557
    :cond_16
    const/4 v8, 0x5

    if-eq v8, v0, :cond_17

    const/16 v8, 0x8

    if-eq v8, v0, :cond_17

    const/4 v8, 0x1

    if-eq v8, v0, :cond_17

    const/4 v8, 0x7

    if-eq v8, v0, :cond_17

    const/4 v8, 0x6

    if-eq v8, v0, :cond_17

    const/16 v8, 0x12

    if-ne v8, v0, :cond_18

    .line 1558
    :cond_17
    const/4 v8, 0x1

    goto/16 :goto_0

    .line 1561
    :cond_18
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1567
    .end local v0    # "ObjType":I
    :sswitch_18
    const/4 v8, 0x1

    goto/16 :goto_0

    .line 1571
    :sswitch_19
    const/16 v8, 0x51

    if-ne p1, v8, :cond_19

    iget-boolean v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mTotalLoadComplete:Z

    if-nez v8, :cond_19

    .line 1572
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1573
    :cond_19
    invoke-virtual {p0}, Lcom/infraware/office/actionbar/MainActionBar;->getDocExtType()I

    move-result v8

    const/16 v9, 0x26

    if-ne v8, v9, :cond_1a

    .line 1574
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1576
    :cond_1a
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v8

    iget v8, v8, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nTotalPages:I

    const/16 v9, 0x1f4

    if-le v8, v9, :cond_1b

    .line 1577
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1579
    :cond_1b
    const/4 v8, 0x1

    goto/16 :goto_0

    .line 1582
    :sswitch_1a
    const/4 v8, 0x2

    if-ne p1, v8, :cond_1c

    iget-boolean v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mTotalLoadComplete:Z

    if-nez v8, :cond_1c

    .line 1583
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1585
    :cond_1c
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v8

    iget v8, v8, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nTotalPages:I

    const/16 v9, 0x1f4

    if-le v8, v9, :cond_1d

    .line 1586
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1587
    :cond_1d
    iget-boolean v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPassWordActivity:Z

    if-eqz v8, :cond_1

    .line 1588
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1594
    :sswitch_1b
    packed-switch p1, :pswitch_data_0

    goto/16 :goto_1

    .line 1597
    :pswitch_0
    iget-object v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v8, v8, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v8}, Lcom/infraware/office/evengine/EvInterface;->IPDFUpdated()Z

    move-result v8

    if-nez v8, :cond_1

    .line 1598
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1606
    :sswitch_1c
    iget-object v8, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const-string/jumbo v9, "textservices"

    invoke-virtual {v8, v9}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/textservice/TextServicesManager;

    .line 1607
    .local v4, "mTsm":Landroid/view/textservice/TextServicesManager;
    const/4 v8, 0x0

    sget-object v9, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const/4 v10, 0x0

    invoke-virtual {v4, v8, v9, p0, v10}, Landroid/view/textservice/TextServicesManager;->newSpellCheckerSession(Landroid/os/Bundle;Ljava/util/Locale;Landroid/view/textservice/SpellCheckerSession$SpellCheckerSessionListener;Z)Landroid/view/textservice/SpellCheckerSession;

    move-result-object v3

    .line 1608
    .local v3, "mScs":Landroid/view/textservice/SpellCheckerSession;
    if-nez v3, :cond_1

    .line 1609
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1374
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_e
        0x3 -> :sswitch_1
        0x5 -> :sswitch_1b
        0x26 -> :sswitch_e
        0x102 -> :sswitch_1c
    .end sparse-switch

    .line 1378
    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x3 -> :sswitch_3
        0x6 -> :sswitch_7
        0x7 -> :sswitch_d
        0x8 -> :sswitch_5
        0x9 -> :sswitch_6
        0x10 -> :sswitch_9
        0x28 -> :sswitch_b
        0x29 -> :sswitch_a
        0x34 -> :sswitch_9
        0x51 -> :sswitch_c
        0x59 -> :sswitch_8
        0x80 -> :sswitch_4
        0x117 -> :sswitch_0
    .end sparse-switch

    .line 1500
    :sswitch_data_2
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_1a
        0x3 -> :sswitch_10
        0x10 -> :sswitch_17
        0x14 -> :sswitch_16
        0x15 -> :sswitch_15
        0x22 -> :sswitch_14
        0x24 -> :sswitch_12
        0x25 -> :sswitch_13
        0x26 -> :sswitch_12
        0x51 -> :sswitch_19
        0x80 -> :sswitch_11
        0x117 -> :sswitch_18
    .end sparse-switch

    .line 1594
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private drawmode_runToolbar(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 2394
    const-string/jumbo v0, "MainActionBar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "drawmode_runToolbar : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 2396
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbar:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 2468
    :goto_0
    return-void

    .line 2400
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->delayButtonHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->delayButtonRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->delayButtonHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->delayButtonRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasCallbacks(Ljava/lang/Runnable;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b000f

    if-eq v0, v1, :cond_1

    .line 2405
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->delayButtonHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->delayButtonRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2408
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2410
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2411
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/16 v1, 0x107

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    .line 2414
    :goto_1
    invoke-virtual {p1, v4}, Landroid/view/View;->setSelected(Z)V

    .line 2415
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarLasso:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2416
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarErase:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2417
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarEraseAll:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2418
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPanning:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_0

    .line 2413
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/16 v1, 0x108

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    goto :goto_1

    .line 2421
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/16 v1, 0x109

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    .line 2422
    invoke-virtual {p1, v4}, Landroid/view/View;->setSelected(Z)V

    .line 2423
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPen:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2424
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarErase:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2425
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarEraseAll:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2426
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPanning:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_0

    .line 2429
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/16 v1, 0x110

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    .line 2430
    invoke-virtual {p1, v4}, Landroid/view/View;->setSelected(Z)V

    .line 2431
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPen:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2432
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarLasso:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2433
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarEraseAll:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2434
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPanning:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto/16 :goto_0

    .line 2437
    :pswitch_3
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/16 v1, 0x111

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    .line 2438
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPen:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v0

    if-ne v0, v4, :cond_4

    .line 2439
    iget-object p1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPen:Landroid/widget/ImageButton;

    .line 2448
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPen:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2449
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarLasso:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2450
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarErase:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2451
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarEraseAll:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2452
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPanning:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2453
    invoke-virtual {p0, p1}, Lcom/infraware/office/actionbar/MainActionBar;->delayButton(Landroid/view/View;)V

    goto/16 :goto_0

    .line 2440
    :cond_4
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarLasso:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v0

    if-ne v0, v4, :cond_5

    .line 2441
    iget-object p1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarLasso:Landroid/widget/ImageButton;

    goto :goto_2

    .line 2442
    :cond_5
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarErase:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v0

    if-ne v0, v4, :cond_6

    .line 2443
    iget-object p1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarErase:Landroid/widget/ImageButton;

    goto :goto_2

    .line 2444
    :cond_6
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarEraseAll:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v0

    if-ne v0, v4, :cond_7

    .line 2445
    iget-object p1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarEraseAll:Landroid/widget/ImageButton;

    goto :goto_2

    .line 2446
    :cond_7
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPanning:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v0

    if-ne v0, v4, :cond_3

    .line 2447
    iget-object p1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPanning:Landroid/widget/ImageButton;

    goto :goto_2

    .line 2456
    :pswitch_4
    invoke-virtual {p1, v4}, Landroid/view/View;->setSelected(Z)V

    .line 2457
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v2, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetPenMode(IZ)V

    .line 2458
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetInfraPenDrawMode(I)V

    .line 2459
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/16 v1, 0x34

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    .line 2461
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPen:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2462
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarLasso:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2463
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarErase:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2464
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarEraseAll:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2465
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->startPanningMode()V

    goto/16 :goto_0

    .line 2408
    :pswitch_data_0
    .packed-switch 0x7f0b000c
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private isSamsungEmailOpen(Ljava/lang/String;)Z
    .locals 1
    .param p1, "openPath"    # Ljava/lang/String;

    .prologue
    .line 2744
    const-string/jumbo v0, "data/com.android.email/cache"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2745
    const/4 v0, 0x1

    .line 2747
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setPopoverItem(I)V
    .locals 1
    .param p1, "parentCase"    # I

    .prologue
    .line 1994
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->removeAllItems()V

    .line 1995
    packed-switch p1, :pswitch_data_0

    .line 2010
    :goto_0
    :pswitch_0
    return-void

    .line 1998
    :pswitch_1
    invoke-direct {p0}, Lcom/infraware/office/actionbar/MainActionBar;->setPopoverItemSave()V

    goto :goto_0

    .line 2001
    :pswitch_2
    invoke-direct {p0}, Lcom/infraware/office/actionbar/MainActionBar;->setPopoverItemMenu()V

    goto :goto_0

    .line 1995
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private setPopoverItemInsertDraw()V
    .locals 0

    .prologue
    .line 1863
    return-void
.end method

.method private setPopoverItemMenu()V
    .locals 10

    .prologue
    const/16 v9, 0x57

    const/16 v8, 0x119

    const/16 v7, 0x62

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1873
    iget v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I

    sparse-switch v0, :sswitch_data_0

    .line 1982
    :goto_0
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_SETTING_MENU()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1983
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v0, v8}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->deleteMenu(I)V

    .line 1985
    :cond_0
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HELP_MENU()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1986
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v0, v7}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->deleteMenu(I)V

    .line 1988
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-static {v0}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_DOWNLOAD_FROM_SAUMSUNGAPPS(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1989
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const/16 v3, 0x121

    const v4, 0x7f020151

    invoke-virtual {v0, v3, v4, v2, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1990
    :cond_2
    return-void

    .line 1876
    :sswitch_0
    iget v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocExtType:I

    const/16 v3, 0x25

    if-eq v0, v3, :cond_3

    .line 1877
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const/16 v3, 0x34

    const v4, 0x7f020146

    invoke-virtual {v0, v3, v4, v2, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1879
    :cond_3
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const/16 v3, 0x77

    const v4, 0x7f02016f

    const/16 v5, 0x77

    invoke-direct {p0, v5}, Lcom/infraware/office/actionbar/MainActionBar;->checkPopoverItemEnable(I)Z

    move-result v5

    invoke-virtual {v0, v3, v4, v2, v5}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1893
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/infraware/common/util/Utils;->isKnoxMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/infraware/common/util/Utils;->isActiveTTS(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1894
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const/4 v3, 0x7

    const v4, 0x7f020164

    const/4 v5, 0x7

    invoke-direct {p0, v5}, Lcom/infraware/office/actionbar/MainActionBar;->checkPopoverItemEnable(I)Z

    move-result v5

    invoke-virtual {v0, v3, v4, v2, v5}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1895
    :cond_4
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const v3, 0x7f0201b5

    invoke-virtual {v0, v9, v3, v2, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1896
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const v3, 0x7f020159

    invoke-virtual {v0, v8, v3, v2, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1897
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const v3, 0x7f0201c8

    invoke-virtual {v0, v7, v3, v2, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    goto/16 :goto_0

    .line 1903
    :sswitch_1
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const/16 v3, 0x23

    const v4, 0x7f02014a

    invoke-virtual {v0, v3, v4, v2, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1904
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const v3, 0x7f0201b5

    invoke-virtual {v0, v9, v3, v2, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1905
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const v3, 0x7f020159

    invoke-virtual {v0, v8, v3, v2, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1906
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const v3, 0x7f0201c8

    invoke-virtual {v0, v7, v3, v2, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    goto/16 :goto_0

    .line 1909
    :sswitch_2
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const v3, 0x7f0201b5

    invoke-virtual {v0, v9, v3, v2, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1910
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const v3, 0x7f020159

    invoke-virtual {v0, v8, v3, v2, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1911
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const v3, 0x7f0201c8

    invoke-virtual {v0, v7, v3, v2, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    goto/16 :goto_0

    .line 1914
    :sswitch_3
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getOrientation()I

    move-result v0

    if-ne v0, v1, :cond_5

    .line 1915
    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const/16 v4, 0x29

    const v5, 0x7f020173

    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getContinuMode()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    :goto_1
    const/16 v6, 0x29

    invoke-direct {p0, v6}, Lcom/infraware/office/actionbar/MainActionBar;->checkPopoverItemEnable(I)Z

    move-result v6

    invoke-virtual {v3, v4, v5, v0, v6}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1917
    :cond_5
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const/16 v3, 0x30

    const v4, 0x7f02015c

    const/16 v5, 0x30

    invoke-direct {p0, v5}, Lcom/infraware/office/actionbar/MainActionBar;->checkPopoverItemEnable(I)Z

    move-result v5

    invoke-virtual {v0, v3, v4, v2, v5}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1918
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const v3, 0x7f0201b5

    invoke-virtual {v0, v9, v3, v2, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1919
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const v3, 0x7f020159

    invoke-virtual {v0, v8, v3, v2, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1920
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const v3, 0x7f0201c8

    invoke-virtual {v0, v7, v3, v2, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    goto/16 :goto_0

    :cond_6
    move v0, v2

    .line 1915
    goto :goto_1

    .line 1938
    :sswitch_4
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const/16 v3, 0x77

    const v4, 0x7f02016f

    const/16 v5, 0x77

    invoke-direct {p0, v5}, Lcom/infraware/office/actionbar/MainActionBar;->checkPopoverItemEnable(I)Z

    move-result v5

    invoke-virtual {v0, v3, v4, v2, v5}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1939
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const v3, 0x7f0201b5

    invoke-virtual {v0, v9, v3, v2, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1940
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const v3, 0x7f020159

    invoke-virtual {v0, v8, v3, v2, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1941
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const v3, 0x7f0201c8

    invoke-virtual {v0, v7, v3, v2, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    goto/16 :goto_0

    .line 1946
    :sswitch_5
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getPageMode()I

    move-result v0

    if-nez v0, :cond_8

    .line 1947
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const/16 v3, 0x96

    const v4, 0x7f02016b

    const/16 v5, 0x96

    invoke-direct {p0, v5}, Lcom/infraware/office/actionbar/MainActionBar;->checkPopoverItemEnable(I)Z

    move-result v5

    invoke-virtual {v0, v3, v4, v1, v5}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1951
    :goto_2
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getReflowText()Z

    move-result v0

    if-nez v0, :cond_7

    .line 1952
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getAnnotShow()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1953
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const/16 v3, 0x35

    const v4, 0x7f02013e

    const/16 v5, 0x35

    invoke-direct {p0, v5}, Lcom/infraware/office/actionbar/MainActionBar;->checkPopoverItemEnable(I)Z

    move-result v5

    invoke-virtual {v0, v3, v4, v1, v5}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1967
    :goto_3
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const/16 v3, 0x67

    const v4, 0x7f020142

    const/16 v5, 0x67

    invoke-direct {p0, v5}, Lcom/infraware/office/actionbar/MainActionBar;->checkPopoverItemEnable(I)Z

    move-result v5

    invoke-virtual {v0, v3, v4, v2, v5}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1970
    :cond_7
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getReflowText()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1971
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const/16 v3, 0x38

    const v4, 0x7f020155

    const/16 v5, 0x38

    invoke-direct {p0, v5}, Lcom/infraware/office/actionbar/MainActionBar;->checkPopoverItemEnable(I)Z

    move-result v5

    invoke-virtual {v0, v3, v4, v1, v5}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1974
    :goto_4
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const/16 v3, 0x63

    const v4, 0x7f020160

    const/16 v5, 0x63

    invoke-direct {p0, v5}, Lcom/infraware/office/actionbar/MainActionBar;->checkPopoverItemEnable(I)Z

    move-result v5

    invoke-virtual {v0, v3, v4, v2, v5}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1976
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const v3, 0x7f0201b5

    invoke-virtual {v0, v9, v3, v2, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1977
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const v3, 0x7f020159

    invoke-virtual {v0, v8, v3, v2, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1978
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const v3, 0x7f0201c8

    invoke-virtual {v0, v7, v3, v2, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    goto/16 :goto_0

    .line 1949
    :cond_8
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const/16 v3, 0x96

    const v4, 0x7f02016b

    const/16 v5, 0x96

    invoke-direct {p0, v5}, Lcom/infraware/office/actionbar/MainActionBar;->checkPopoverItemEnable(I)Z

    move-result v5

    invoke-virtual {v0, v3, v4, v2, v5}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    goto/16 :goto_2

    .line 1960
    :cond_9
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const/16 v3, 0x35

    const v4, 0x7f02013e

    const/16 v5, 0x35

    invoke-direct {p0, v5}, Lcom/infraware/office/actionbar/MainActionBar;->checkPopoverItemEnable(I)Z

    move-result v5

    invoke-virtual {v0, v3, v4, v2, v5}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    goto :goto_3

    .line 1973
    :cond_a
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const/16 v3, 0x38

    const v4, 0x7f020155

    const/16 v5, 0x38

    invoke-direct {p0, v5}, Lcom/infraware/office/actionbar/MainActionBar;->checkPopoverItemEnable(I)Z

    move-result v5

    invoke-virtual {v0, v3, v4, v2, v5}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    goto :goto_4

    .line 1873
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_3
        0x5 -> :sswitch_5
        0x6 -> :sswitch_4
        0x26 -> :sswitch_2
    .end sparse-switch
.end method

.method private setPopoverItemSave()V
    .locals 9

    .prologue
    const v8, 0x7f0201cc

    const v7, 0x7f0201ba

    const/16 v6, 0x51

    const/16 v5, 0x58

    const/4 v4, 0x0

    .line 1759
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-static {v0}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_DOWNLOAD_FROM_SAUMSUNGAPPS(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1761
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocOpenPath:Ljava/lang/String;

    invoke-static {v0}, Lcom/infraware/common/util/FileUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/infraware/common/util/FileUtils;->getTypeByFileName(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/infraware/common/util/FileUtils;->isSupportEditType(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1763
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-static {v0}, Lcom/infraware/common/util/Utils;->isInstalledPolarisOffice(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1765
    iget-boolean v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPassWordActivity:Z

    if-nez v0, :cond_2

    .line 1766
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetBWPOpInfo_Editor()Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nStatusOP:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mTotalLoadComplete:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    :cond_0
    invoke-virtual {p0}, Lcom/infraware/office/actionbar/MainActionBar;->getDocType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nTotalPages:I

    const/16 v1, 0x3e8

    if-gt v0, v1, :cond_2

    .line 1768
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const/16 v1, 0x120

    const v2, 0x7f0201b9

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1792
    :cond_2
    :goto_0
    iget v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I

    sparse-switch v0, :sswitch_data_0

    .line 1826
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-direct {p0, v5}, Lcom/infraware/office/actionbar/MainActionBar;->checkPopoverItemEnable(I)Z

    move-result v1

    invoke-virtual {v0, v5, v7, v4, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1828
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-direct {p0, v6}, Lcom/infraware/office/actionbar/MainActionBar;->checkPopoverItemEnable(I)Z

    move-result v1

    invoke-virtual {v0, v6, v8, v4, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1832
    :goto_1
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-static {v0}, Lcom/infraware/office/baseframe/porting/EvPrintHelper;->isSupportPrint(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/infraware/common/util/Utils;->isKnoxMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/infraware/office/actionbar/MainActionBar;->getDocExtType()I

    move-result v0

    const/16 v1, 0x26

    if-ne v0, v1, :cond_4

    .line 1835
    :cond_3
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v0, v6}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->deleteMenu(I)V

    .line 1844
    :cond_4
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "FT03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1845
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocOpenPath:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/infraware/office/actionbar/MainActionBar;->isSamsungEmailOpen(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1846
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v0, v5}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->deleteMenu(I)V

    .line 1848
    :cond_5
    return-void

    .line 1773
    :cond_6
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-static {v0}, Lcom/infraware/common/util/Utils;->isSupportSamsungApps(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1775
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const/16 v1, 0x120

    const v2, 0x7f0201b9

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    goto :goto_0

    .line 1797
    :sswitch_0
    iget v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocExtType:I

    const/16 v1, 0x25

    if-ne v0, v1, :cond_7

    .line 1799
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-direct {p0, v5}, Lcom/infraware/office/actionbar/MainActionBar;->checkPopoverItemEnable(I)Z

    move-result v1

    invoke-virtual {v0, v5, v7, v4, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    goto :goto_1

    .line 1804
    :cond_7
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const/4 v1, 0x2

    const v2, 0x7f0201c0

    const/4 v3, 0x2

    invoke-direct {p0, v3}, Lcom/infraware/office/actionbar/MainActionBar;->checkPopoverItemEnable(I)Z

    move-result v3

    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1805
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-direct {p0, v5}, Lcom/infraware/office/actionbar/MainActionBar;->checkPopoverItemEnable(I)Z

    move-result v1

    invoke-virtual {v0, v5, v7, v4, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1807
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-direct {p0, v6}, Lcom/infraware/office/actionbar/MainActionBar;->checkPopoverItemEnable(I)Z

    move-result v1

    invoke-virtual {v0, v6, v8, v4, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    goto :goto_1

    .line 1811
    :sswitch_1
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-direct {p0, v5}, Lcom/infraware/office/actionbar/MainActionBar;->checkPopoverItemEnable(I)Z

    move-result v1

    invoke-virtual {v0, v5, v7, v4, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1813
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-direct {p0, v6}, Lcom/infraware/office/actionbar/MainActionBar;->checkPopoverItemEnable(I)Z

    move-result v1

    invoke-virtual {v0, v6, v8, v4, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    goto/16 :goto_1

    .line 1816
    :sswitch_2
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-direct {p0, v5}, Lcom/infraware/office/actionbar/MainActionBar;->checkPopoverItemEnable(I)Z

    move-result v1

    invoke-virtual {v0, v5, v7, v4, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1818
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-direct {p0, v6}, Lcom/infraware/office/actionbar/MainActionBar;->checkPopoverItemEnable(I)Z

    move-result v1

    invoke-virtual {v0, v6, v8, v4, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    goto/16 :goto_1

    .line 1821
    :sswitch_3
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-direct {p0, v5}, Lcom/infraware/office/actionbar/MainActionBar;->checkPopoverItemEnable(I)Z

    move-result v1

    invoke-virtual {v0, v5, v7, v4, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    .line 1823
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-direct {p0, v6}, Lcom/infraware/office/actionbar/MainActionBar;->checkPopoverItemEnable(I)Z

    move-result v1

    invoke-virtual {v0, v6, v8, v4, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZ)V

    goto/16 :goto_1

    .line 1792
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0x5 -> :sswitch_1
        0x6 -> :sswitch_2
        0x26 -> :sswitch_3
    .end sparse-switch
.end method

.method private setUndoRedoCheck(Z)V
    .locals 1
    .param p1, "check"    # Z

    .prologue
    const/4 v0, 0x1

    .line 2292
    if-ne p1, v0, :cond_0

    .line 2294
    iput-boolean v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mStopUndoRedoCheck:Z

    .line 2295
    invoke-virtual {p0, v0}, Lcom/infraware/office/actionbar/MainActionBar;->showTitle(Z)V

    .line 2297
    :cond_0
    if-nez p1, :cond_1

    .line 2299
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mStopUndoRedoCheck:Z

    .line 2300
    invoke-virtual {p0}, Lcom/infraware/office/actionbar/MainActionBar;->updateMainActionBar()V

    .line 2302
    :cond_1
    return-void
.end method


# virtual methods
.method public IsPenCustomColor()Z
    .locals 1

    .prologue
    .line 2654
    iget-boolean v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenCustomColor:Z

    return v0
.end method

.method public IsSheetProtect()Z
    .locals 1

    .prologue
    .line 2265
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsProtectSheet()Z

    move-result v0

    return v0
.end method

.method public MainActionBarFinalize()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 576
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mShowPanelHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 578
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mShowPanelRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 579
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mShowPanelHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mShowPanelRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 580
    :cond_0
    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mShowPanelHandler:Landroid/os/Handler;

    .line 583
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mShowPanelRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_2

    .line 585
    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mShowPanelRunnable:Ljava/lang/Runnable;

    .line 588
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    if-eqz v0, :cond_3

    .line 590
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->finalizeActionBarPopupMenu()V

    .line 591
    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    .line 594
    :cond_3
    invoke-virtual {p0}, Lcom/infraware/office/actionbar/MainActionBar;->listenerClear()V

    .line 596
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_4

    .line 597
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarView:Landroid/widget/LinearLayout;

    invoke-static {v0}, Lcom/infraware/common/util/Utils;->unbindDrawables(Landroid/view/View;)V

    .line 599
    :cond_4
    return-void
.end method

.method public PropertyBtnSelected()V
    .locals 0

    .prologue
    .line 1216
    return-void
.end method

.method public PropertyBtnUnSelected()V
    .locals 0

    .prologue
    .line 1222
    return-void
.end method

.method public actionBarPopupWindow(ILandroid/view/View;)Z
    .locals 5
    .param p1, "parentCase"    # I
    .param p2, "caller"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x3

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2185
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->isFindReplaceMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2244
    :cond_0
    :goto_0
    return v0

    .line 2188
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/office/actionbar/MainActionBar;->hideAllToolbarDelay()Z

    .line 2190
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v2}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2192
    invoke-virtual {p0}, Lcom/infraware/office/actionbar/MainActionBar;->dismissMenuPopup()Z

    goto :goto_0

    .line 2196
    :cond_2
    if-eqz p2, :cond_0

    .line 2199
    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 2213
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    if-nez v0, :cond_5

    .line 2214
    new-instance v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPopoverListener:Landroid/view/View$OnClickListener;

    invoke-direct {v0, v2, v4, p2, v3}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;-><init>(Landroid/app/Activity;ILandroid/view/View;Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    .line 2224
    :goto_1
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->setOutsideTouchable(Z)V

    .line 2227
    invoke-direct {p0, p1}, Lcom/infraware/office/actionbar/MainActionBar;->setPopoverItem(I)V

    .line 2229
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    if-eqz v0, :cond_3

    .line 2232
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewActionBar:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/infraware/office/actionbar/MainActionBar$7;

    invoke-direct {v2, p0}, Lcom/infraware/office/actionbar/MainActionBar$7;-><init>(Lcom/infraware/office/actionbar/MainActionBar;)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    :cond_3
    move v0, v1

    .line 2244
    goto :goto_0

    .line 2203
    :pswitch_0
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    if-nez v2, :cond_4

    .line 2204
    new-instance v2, Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPopoverListener:Landroid/view/View$OnClickListener;

    invoke-direct {v2, v3, v0, p2, v4}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;-><init>(Landroid/app/Activity;ILandroid/view/View;Landroid/view/View$OnClickListener;)V

    iput-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    goto :goto_1

    .line 2206
    :cond_4
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v2, v0, p2}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->setMenuType(ILandroid/view/View;)V

    goto :goto_1

    .line 2216
    :cond_5
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v0, v4, p2}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->setMenuType(ILandroid/view/View;)V

    goto :goto_1

    .line 2199
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0002
        :pswitch_0
    .end packed-switch
.end method

.method public actionProperties(I)V
    .locals 1
    .param p1, "haed_type"    # I

    .prologue
    .line 1244
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getmView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 1366
    return-void
.end method

.method public changeDrawMode(I)Z
    .locals 1
    .param p1, "drawMode"    # I

    .prologue
    .line 792
    iget v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDrawMode:I

    if-ne v0, p1, :cond_0

    .line 793
    const/4 v0, 0x0

    .line 876
    :goto_0
    return v0

    .line 795
    :cond_0
    iput p1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDrawMode:I

    .line 876
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public changeToolbarHeight(Z)V
    .locals 6
    .param p1, "bLand"    # Z

    .prologue
    const/high16 v5, 0x422c0000    # 43.0f

    .line 2722
    if-eqz p1, :cond_0

    iget-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-static {v4, v5}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v4

    float-to-int v1, v4

    .line 2724
    .local v1, "lp_height":I
    :goto_0
    iget-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbar:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2725
    .local v0, "lp":Landroid/view/ViewGroup$LayoutParams;
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2727
    if-eqz p1, :cond_1

    iget-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/high16 v5, 0x42820000    # 65.0f

    invoke-static {v4, v5}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v4

    float-to-int v2, v4

    .line 2729
    .local v2, "lp_width":I
    :goto_1
    iget-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPen:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 2730
    .local v3, "params":Landroid/view/ViewGroup$LayoutParams;
    iput v2, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2731
    iget-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarLasso:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 2732
    iput v2, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2733
    iget-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarErase:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 2734
    iput v2, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2735
    iget-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarEraseAll:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 2736
    iput v2, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2737
    iget-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPanning:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 2738
    iput v2, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2740
    return-void

    .line 2722
    .end local v0    # "lp":Landroid/view/ViewGroup$LayoutParams;
    .end local v1    # "lp_height":I
    .end local v2    # "lp_width":I
    .end local v3    # "params":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    iget-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-static {v4, v5}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v4

    float-to-int v1, v4

    goto :goto_0

    .line 2727
    .restart local v0    # "lp":Landroid/view/ViewGroup$LayoutParams;
    .restart local v1    # "lp_height":I
    :cond_1
    iget-object v4, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const v5, 0x425551ec    # 53.33f

    invoke-static {v4, v5}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v4

    float-to-int v2, v4

    goto :goto_1
.end method

.method public changeUndoRedoLayout()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x2

    const-wide/16 v4, 0x1

    const/4 v1, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2046
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->IsViewerMode()Z

    move-result v0

    if-ne v0, v2, :cond_1

    .line 2048
    invoke-virtual {p0, v2}, Lcom/infraware/office/actionbar/MainActionBar;->showTitle(Z)V

    .line 2116
    :cond_0
    :goto_0
    return-void

    .line 2052
    :cond_1
    iget v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I

    if-ne v0, v1, :cond_4

    .line 2054
    invoke-virtual {p0, v3}, Lcom/infraware/office/actionbar/MainActionBar;->showTitle(Z)V

    .line 2055
    iget-wide v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->m_nEditStatus:J

    and-long/2addr v0, v6

    cmp-long v0, v0, v6

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->IsEditInTextboxMode()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2057
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndo:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2058
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndo:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 2066
    :goto_1
    iget-wide v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->m_nEditStatus:J

    and-long/2addr v0, v4

    cmp-long v0, v0, v4

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->IsEditInTextboxMode()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2068
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRedo:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2069
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRedo:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 2112
    :goto_2
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndo:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRedo:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2113
    invoke-virtual {p0, v2}, Lcom/infraware/office/actionbar/MainActionBar;->showTitle(Z)V

    goto :goto_0

    .line 2062
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndo:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2063
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndo:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setFocusable(Z)V

    goto :goto_1

    .line 2073
    :cond_3
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRedo:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2074
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRedo:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setFocusable(Z)V

    goto :goto_2

    .line 2080
    :cond_4
    invoke-virtual {p0, v3}, Lcom/infraware/office/actionbar/MainActionBar;->showTitle(Z)V

    .line 2081
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mBWPInfo:Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    iget v0, v0, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nStatusOP:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v1, :cond_5

    .line 2083
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndo:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2084
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndo:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 2093
    :goto_3
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mBWPInfo:Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    iget v0, v0, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nStatusOP:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_6

    .line 2095
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRedo:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2096
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRedo:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    goto :goto_2

    .line 2088
    :cond_5
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndo:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2089
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndo:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setFocusable(Z)V

    goto :goto_3

    .line 2100
    :cond_6
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRedo:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2101
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRedo:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setFocusable(Z)V

    goto :goto_2
.end method

.method public delayButton(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 899
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->delayButtonHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 900
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->delayButtonHandler:Landroid/os/Handler;

    .line 903
    :cond_0
    new-instance v0, Lcom/infraware/office/actionbar/MainActionBar$4;

    invoke-direct {v0, p0, p1}, Lcom/infraware/office/actionbar/MainActionBar$4;-><init>(Lcom/infraware/office/actionbar/MainActionBar;Landroid/view/View;)V

    iput-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->delayButtonRunnable:Ljava/lang/Runnable;

    .line 914
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->delayButtonHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->delayButtonRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 915
    return-void
.end method

.method public delayTitle()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 887
    invoke-virtual {p0, v0}, Lcom/infraware/office/actionbar/MainActionBar;->showTitle(Z)V

    .line 888
    iput-boolean v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mFileNameShow:Z

    .line 889
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/infraware/office/actionbar/MainActionBar$3;

    invoke-direct {v1, p0}, Lcom/infraware/office/actionbar/MainActionBar$3;-><init>(Lcom/infraware/office/actionbar/MainActionBar;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 895
    return-void
.end method

.method public dismissInlinePopupMenu()Z
    .locals 1

    .prologue
    .line 2349
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2351
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismissAll()V

    .line 2352
    const/4 v0, 0x1

    .line 2354
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dismissMenuPopup()Z
    .locals 1

    .prologue
    .line 2167
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    if-eqz v0, :cond_0

    .line 2169
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2171
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->dismiss()V

    .line 2174
    const/4 v0, 0x1

    .line 2178
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public drawMode_hideOption()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2568
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getPenDrawOption()Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2579
    :cond_0
    :goto_0
    return v0

    .line 2571
    :cond_1
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getPenDrawOption()Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getPenDrawOption()Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 2574
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onChangeFreeDrawMode(I)V

    move v0, v1

    .line 2575
    goto :goto_0
.end method

.method public drawMode_showToolbar()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2472
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    if-eqz v0, :cond_0

    .line 2474
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2475
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->dismiss()V

    .line 2478
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/infraware/office/actionbar/MainActionBar;->changeToolbarHeight(Z)V

    .line 2479
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbar:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2480
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2482
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_2

    .line 2483
    invoke-virtual {p0, v2}, Lcom/infraware/office/actionbar/MainActionBar;->setPenToolBarOfDirection(Z)V

    .line 2487
    :goto_1
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPen:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2489
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/infraware/office/actionbar/MainActionBar$9;

    invoke-direct {v1, p0}, Lcom/infraware/office/actionbar/MainActionBar$9;-><init>(Lcom/infraware/office/actionbar/MainActionBar;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2503
    return-void

    :cond_1
    move v0, v2

    .line 2478
    goto :goto_0

    .line 2485
    :cond_2
    invoke-virtual {p0, v1}, Lcom/infraware/office/actionbar/MainActionBar;->setPenToolBarOfDirection(Z)V

    goto :goto_1
.end method

.method public getActionBarDraw()Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 2260
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    return-object v0
.end method

.method public getActionBarMenu()Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 2255
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewMenu:Landroid/widget/ImageButton;

    return-object v0
.end method

.method public getActionBarSave()Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 2250
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewSave:Landroid/widget/ImageButton;

    return-object v0
.end method

.method public getActionBarView()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 2329
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarView:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public getDocExtType()I
    .locals 1

    .prologue
    .line 614
    iget v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocExtType:I

    return v0
.end method

.method public getDocType()I
    .locals 2

    .prologue
    .line 603
    iget-boolean v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPassWordActivity:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 604
    const/4 v0, 0x0

    .line 606
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I

    goto :goto_0
.end method

.method public getDrawMode()I
    .locals 1

    .prologue
    .line 787
    iget v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDrawMode:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 2334
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    return v0
.end method

.method public getPenDrawPanelPosition()Landroid/graphics/Point;
    .locals 3

    .prologue
    .line 2630
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 2632
    .local v0, "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 2634
    iget-object v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPen:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getRight()I

    move-result v1

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 2635
    iget-object v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbar:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getTop()I

    move-result v1

    iput v1, v0, Landroid/graphics/Point;->y:I

    .line 2644
    :goto_0
    return-object v0

    .line 2639
    :cond_0
    iget-object v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPen:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getLeft()I

    move-result v1

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 2640
    iget-object v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRoot:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getBottom()I

    move-result v1

    iput v1, v0, Landroid/graphics/Point;->y:I

    goto :goto_0
.end method

.method public getPopupMenu()Lcom/infraware/office/actionbar/ActionBarPopupMenu;
    .locals 1

    .prologue
    .line 566
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    return-object v0
.end method

.method public getTotalLoadState()Z
    .locals 1

    .prologue
    .line 2344
    iget-boolean v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mTotalLoadComplete:Z

    return v0
.end method

.method public getUndoRedoStatus()Z
    .locals 1

    .prologue
    .line 938
    iget-boolean v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mbUndoRedoStatus:Z

    return v0
.end method

.method public hide()V
    .locals 2

    .prologue
    .line 770
    invoke-virtual {p0}, Lcom/infraware/office/actionbar/MainActionBar;->dismissMenuPopup()Z

    .line 771
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarView:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 772
    return-void
.end method

.method public hideAllToolbar()Z
    .locals 2

    .prologue
    .line 2585
    const/4 v0, 0x0

    .line 2591
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getPenDrawOption()Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getPenDrawOption()Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 2595
    invoke-virtual {p0}, Lcom/infraware/office/actionbar/MainActionBar;->drawMode_hideOption()Z

    .line 2608
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/office/actionbar/MainActionBar;->hideToolbarDrawmodeDelay()Z

    move-result v0

    .line 2609
    return v0
.end method

.method public hideAllToolbarDelay()Z
    .locals 1

    .prologue
    .line 2615
    invoke-virtual {p0}, Lcom/infraware/office/actionbar/MainActionBar;->hideToolbarDrawmodeDelay()Z

    move-result v0

    return v0
.end method

.method public hideToolbarDrawmodeDelay()Z
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 2507
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getPenDrawOption()Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getPenDrawOption()Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 2510
    invoke-virtual {p0, v2}, Lcom/infraware/office/actionbar/MainActionBar;->setPenCustomColor(Z)V

    .line 2511
    invoke-virtual {p0}, Lcom/infraware/office/actionbar/MainActionBar;->drawMode_hideOption()Z

    .line 2563
    :cond_0
    :goto_0
    return v2

    .line 2515
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2516
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbar:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2518
    iget v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I

    if-ne v0, v3, :cond_5

    .line 2520
    const/4 v7, 0x0

    .line 2521
    .local v7, "bLandScape":I
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v3, :cond_2

    .line 2522
    const/4 v7, 0x1

    .line 2524
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getmView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->getBitmapWidth()I

    move-result v9

    .line 2525
    .local v9, "nWidth":I
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getmView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->getBitmapHeight()I

    move-result v8

    .line 2527
    .local v8, "nHeight":I
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbar:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    add-int/2addr v8, v0

    .line 2529
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_3

    .line 2530
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v7, v9, v8}, Lcom/infraware/office/evengine/EvInterface;->IChangeScreen(III)V

    .line 2549
    .end local v7    # "bLandScape":I
    .end local v8    # "nHeight":I
    .end local v9    # "nWidth":I
    :cond_3
    :goto_1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/infraware/office/actionbar/MainActionBar$10;

    invoke-direct {v1, p0}, Lcom/infraware/office/actionbar/MainActionBar$10;-><init>(Lcom/infraware/office/actionbar/MainActionBar;)V

    const-wide/16 v3, 0xfa

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2556
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/16 v1, 0x118

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    .line 2557
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_4

    .line 2558
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetInfraPenDrawMode(I)V

    .line 2559
    :cond_4
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/16 v1, 0x34

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    move v2, v10

    .line 2560
    goto/16 :goto_0

    .line 2531
    :cond_5
    iget v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I

    if-eq v0, v10, :cond_6

    iget v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 2533
    :cond_6
    const/4 v7, 0x0

    .line 2534
    .restart local v7    # "bLandScape":I
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v3, :cond_7

    .line 2535
    const/4 v7, 0x1

    .line 2537
    :cond_7
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getmView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->getBitmapWidth()I

    move-result v9

    .line 2538
    .restart local v9    # "nWidth":I
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getmView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->getBitmapHeight()I

    move-result v8

    .line 2540
    .restart local v8    # "nHeight":I
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbar:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    add-int/2addr v8, v0

    .line 2542
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_8

    .line 2543
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v7, v9, v8}, Lcom/infraware/office/evengine/EvInterface;->IChangeScreen(III)V

    .line 2545
    :cond_8
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    instance-of v0, v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    if-eqz v0, :cond_3

    .line 2546
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->prepareChangeScreen()V

    goto/16 :goto_1
.end method

.method public isEditMode()Z
    .locals 1

    .prologue
    .line 783
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->IsViewerMode()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 776
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 777
    const/4 v0, 0x1

    .line 779
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected listenerClear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2666
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewSave:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2667
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewSave:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2669
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndoRedoLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2671
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndo:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2672
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndo:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2674
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRedo:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2675
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRedo:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2677
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2678
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2680
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewFind:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2681
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewFind:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2685
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPen:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2686
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPen:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2688
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarLasso:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2689
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarLasso:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2691
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarErase:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2692
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarLasso:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2694
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarEraseAll:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2695
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarEraseAll:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2697
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPanning:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2698
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbarPanning:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2700
    iput-object v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 2701
    iput-object v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mToolbarListener:Landroid/view/View$OnClickListener;

    .line 2703
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewSave:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 2704
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 2705
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndoRedoLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 2706
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndo:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 2707
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewRedo:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 2708
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 2709
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewFind:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 2710
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewMenu:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 2712
    iput-object v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mKeyListener:Landroid/view/View$OnKeyListener;

    .line 2714
    iput-object v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarListener:Landroid/view/View$OnClickListener;

    .line 2717
    return-void
.end method

.method public onGetSentenceSuggestions([Landroid/view/textservice/SentenceSuggestionsInfo;)V
    .locals 0
    .param p1, "arg0"    # [Landroid/view/textservice/SentenceSuggestionsInfo;

    .prologue
    .line 2662
    return-void
.end method

.method public onGetSuggestions([Landroid/view/textservice/SuggestionsInfo;)V
    .locals 0
    .param p1, "arg0"    # [Landroid/view/textservice/SuggestionsInfo;

    .prologue
    .line 2359
    return-void
.end method

.method public onLocaleChanged()V
    .locals 0

    .prologue
    .line 2323
    return-void
.end method

.method public setFocus(Z)V
    .locals 2
    .param p1, "focus"    # Z

    .prologue
    .line 556
    if-eqz p1, :cond_0

    .line 558
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewMenu:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 559
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewMenu:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 560
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewMenu:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocusFromTouch()Z

    .line 562
    :cond_0
    return-void
.end method

.method public setOnTotalLoadComplete()V
    .locals 1

    .prologue
    .line 2287
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mTotalLoadComplete:Z

    .line 2288
    return-void
.end method

.method public setPassWordStyle()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2305
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPassWordActivity:Z

    .line 2307
    iget v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 2308
    invoke-virtual {p0}, Lcom/infraware/office/actionbar/MainActionBar;->sheetProtect()V

    .line 2317
    :cond_0
    :goto_0
    return-void

    .line 2309
    :cond_1
    iget v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 2314
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2315
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    goto :goto_0
.end method

.method public setPenCustomColor(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 2649
    iput-boolean p1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenCustomColor:Z

    .line 2650
    return-void
.end method

.method public setPenTitleBarOfDirection()I
    .locals 1

    .prologue
    .line 2339
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    return v0
.end method

.method public setPenToolBarOfDirection(Z)V
    .locals 3
    .param p1, "bLandscape"    # Z

    .prologue
    .line 2619
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbar:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2620
    if-nez p1, :cond_1

    .line 2621
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbar:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/high16 v2, 0x421c0000    # 39.0f

    invoke-static {v1, v2}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBottom(I)V

    .line 2625
    :cond_0
    :goto_0
    return-void

    .line 2623
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mPenDrawToolbar:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/high16 v2, 0x42020000    # 32.5f

    invoke-static {v1, v2}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBottom(I)V

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 882
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 884
    return-void
.end method

.method public setUndoRedoStatus(Z)V
    .locals 0
    .param p1, "bStatus"    # Z

    .prologue
    .line 943
    iput-boolean p1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mbUndoRedoStatus:Z

    .line 944
    return-void
.end method

.method public sheetProtect()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2269
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2270
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 2271
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->menuDisable()V

    .line 2273
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/infraware/office/actionbar/MainActionBar;->setUndoRedoCheck(Z)V

    .line 2274
    return-void
.end method

.method public sheetUnProtect()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2278
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2279
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 2281
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->menuEnable()V

    .line 2283
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/infraware/office/actionbar/MainActionBar;->setUndoRedoCheck(Z)V

    .line 2284
    return-void
.end method

.method public show()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 763
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewSave:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 764
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewMenu:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 765
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarView:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 766
    return-void
.end method

.method public showInsertPanel(I)V
    .locals 0
    .param p1, "headtype"    # I

    .prologue
    .line 1210
    return-void
.end method

.method public showTitle(Z)V
    .locals 3
    .param p1, "title"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 919
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 921
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndoRedoLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 923
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 933
    :goto_0
    return-void

    .line 928
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewUndoRedoLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 930
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public updateActionBarIcon()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 619
    iget v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I

    sparse-switch v0, :sswitch_data_0

    .line 758
    :goto_0
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mActionBarView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestLayout()V

    .line 759
    return-void

    .line 621
    :sswitch_0
    iget v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocExtType:I

    const/16 v1, 0x13

    if-ne v0, v1, :cond_0

    .line 622
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewSave:Landroid/widget/ImageButton;

    const v1, 0x7f0202bc

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 631
    :goto_1
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 632
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewFind:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 634
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewMenu:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 626
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewSave:Landroid/widget/ImageButton;

    const v1, 0x7f0202bb

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_1

    .line 648
    :sswitch_1
    iget v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocExtType:I

    const/16 v1, 0x14

    if-ne v0, v1, :cond_1

    .line 650
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewSave:Landroid/widget/ImageButton;

    const v1, 0x7f0202da

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 659
    :goto_2
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 660
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewFind:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 662
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewMenu:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 654
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewSave:Landroid/widget/ImageButton;

    const v1, 0x7f0202d9

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_2

    .line 675
    :sswitch_2
    iget v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocExtType:I

    const/16 v1, 0x12

    if-ne v0, v1, :cond_2

    .line 677
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewSave:Landroid/widget/ImageButton;

    const v1, 0x7f020296

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 691
    :goto_3
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 692
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewFind:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 694
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewMenu:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 680
    :cond_2
    iget v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocExtType:I

    const/16 v1, 0x25

    if-ne v0, v1, :cond_3

    .line 682
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewSave:Landroid/widget/ImageButton;

    const v1, 0x7f0202c9

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_3

    .line 686
    :cond_3
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewSave:Landroid/widget/ImageButton;

    const v1, 0x7f020295

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_3

    .line 708
    :sswitch_3
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewSave:Landroid/widget/ImageButton;

    const v1, 0x7f0202b7

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 709
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 710
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewFind:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 720
    :sswitch_4
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewSave:Landroid/widget/ImageButton;

    const v1, 0x7f0202a0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 723
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 724
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewFind:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 726
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewMenu:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 747
    :sswitch_5
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewSave:Landroid/widget/ImageButton;

    const v1, 0x7f020294

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 748
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewDraw:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 749
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewFind:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 751
    iget-object v0, p0, Lcom/infraware/office/actionbar/MainActionBar;->mViewMenu:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 619
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_1
        0x3 -> :sswitch_0
        0x5 -> :sswitch_3
        0x6 -> :sswitch_4
        0x26 -> :sswitch_5
    .end sparse-switch
.end method

.method public updateMainActionBar()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x1

    const/4 v4, 0x3

    const/4 v5, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2120
    iget-boolean v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mStopUndoRedoCheck:Z

    if-ne v1, v2, :cond_1

    .line 2161
    :cond_0
    :goto_0
    return-void

    .line 2123
    :cond_1
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    .line 2125
    .local v0, "evInterface":Lcom/infraware/office/evengine/EvInterface;
    iget-boolean v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mFileNameShow:Z

    if-ne v1, v2, :cond_4

    .line 2127
    iget v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I

    if-ne v1, v3, :cond_2

    .line 2129
    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetEditStauts_Editor()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->m_nEditStatus:J

    .line 2130
    iget-wide v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->m_nEditStatus:J

    const-wide/16 v3, 0x10

    and-long/2addr v1, v3

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    .line 2131
    iput-boolean v5, p0, Lcom/infraware/office/actionbar/MainActionBar;->mFileNameShow:Z

    goto :goto_0

    .line 2133
    :cond_2
    iget v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I

    if-eq v1, v2, :cond_3

    iget v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I

    if-ne v1, v4, :cond_0

    .line 2135
    :cond_3
    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetBWPOpInfo_Editor()Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mBWPInfo:Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    .line 2136
    iget-object v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mBWPInfo:Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    iget v1, v1, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nStatusOP:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_0

    .line 2137
    iput-boolean v5, p0, Lcom/infraware/office/actionbar/MainActionBar;->mFileNameShow:Z

    goto :goto_0

    .line 2142
    :cond_4
    iget v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I

    if-ne v1, v3, :cond_6

    .line 2144
    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetEditStauts_Editor()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->m_nEditStatus:J

    .line 2145
    iget-wide v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->m_nEditStatus:J

    const-wide/16 v3, 0x2

    and-long/2addr v1, v3

    const-wide/16 v3, 0x2

    cmp-long v1, v1, v3

    if-eqz v1, :cond_5

    iget-wide v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->m_nEditStatus:J

    and-long/2addr v1, v6

    cmp-long v1, v1, v6

    if-nez v1, :cond_0

    .line 2146
    :cond_5
    invoke-virtual {p0}, Lcom/infraware/office/actionbar/MainActionBar;->changeUndoRedoLayout()V

    goto :goto_0

    .line 2148
    :cond_6
    iget v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I

    if-eq v1, v2, :cond_7

    iget v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I

    if-ne v1, v4, :cond_0

    .line 2150
    :cond_7
    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetBWPOpInfo_Editor()Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mBWPInfo:Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    .line 2152
    iget-object v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mBWPInfo:Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    iget v1, v1, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nStatusOP:I

    and-int/lit8 v1, v1, 0x2

    if-eq v1, v3, :cond_8

    iget-object v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mBWPInfo:Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    iget v1, v1, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nStatusOP:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_9

    .line 2154
    :cond_8
    invoke-virtual {p0}, Lcom/infraware/office/actionbar/MainActionBar;->changeUndoRedoLayout()V

    .line 2157
    :cond_9
    iget-object v1, p0, Lcom/infraware/office/actionbar/MainActionBar;->mBWPInfo:Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    iget v1, v1, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nStatusOP:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_0

    .line 2158
    invoke-virtual {p0}, Lcom/infraware/office/actionbar/MainActionBar;->changeUndoRedoLayout()V

    goto/16 :goto_0
.end method
