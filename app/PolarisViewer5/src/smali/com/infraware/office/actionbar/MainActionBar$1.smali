.class Lcom/infraware/office/actionbar/MainActionBar$1;
.super Ljava/lang/Object;
.source "MainActionBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/office/actionbar/MainActionBar;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/actionbar/MainActionBar;


# direct methods
.method constructor <init>(Lcom/infraware/office/actionbar/MainActionBar;)V
    .locals 0

    .prologue
    .line 303
    iput-object p1, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 306
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 309
    .local v1, "id":I
    sparse-switch v1, :sswitch_data_0

    .line 475
    :cond_0
    :goto_0
    :sswitch_0
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v2}, Lcom/infraware/office/actionbar/MainActionBar;->dismissMenuPopup()Z

    .line 477
    return-void

    .line 320
    :sswitch_1
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    goto :goto_0

    .line 323
    :sswitch_2
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mDocType:I
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$100(Lcom/infraware/office/actionbar/MainActionBar;)I

    move-result v2

    if-ne v2, v3, :cond_2

    .line 324
    iget-object v5, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->m_bNoMarginView:Z
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$200(Lcom/infraware/office/actionbar/MainActionBar;)Z

    move-result v2

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    # setter for: Lcom/infraware/office/actionbar/MainActionBar;->m_bNoMarginView:Z
    invoke-static {v5, v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$202(Lcom/infraware/office/actionbar/MainActionBar;Z)Z

    .line 325
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->INoMarginView()V

    goto :goto_0

    :cond_1
    move v2, v4

    .line 324
    goto :goto_1

    .line 328
    :cond_2
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onReflow()V

    goto :goto_0

    .line 332
    :sswitch_3
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v2}, Lcom/infraware/office/actionbar/MainActionBar;->dismissMenuPopup()Z

    .line 333
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    goto :goto_0

    .line 336
    :sswitch_4
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    goto :goto_0

    .line 339
    :sswitch_5
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v2, v3}, Lcom/infraware/office/actionbar/MainActionBar;->showInsertPanel(I)V

    goto :goto_0

    .line 354
    :sswitch_6
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    goto :goto_0

    .line 357
    :sswitch_7
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    check-cast v2, Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnInsertTableActivity()V

    goto :goto_0

    .line 360
    :sswitch_8
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    check-cast v2, Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnInsertChart()V

    goto :goto_0

    .line 363
    :sswitch_9
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    const/16 v4, 0x30b

    invoke-virtual {v2, v4, v3, v3}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->OnEVSheetFuction(III)V

    goto/16 :goto_0

    .line 366
    :sswitch_a
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    const/16 v4, 0x30a

    invoke-virtual {v2, v4, v3, v3}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->OnEVSheetFuction(III)V

    goto/16 :goto_0

    .line 370
    :sswitch_b
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v2, v5}, Lcom/infraware/office/actionbar/MainActionBar;->showInsertPanel(I)V

    goto/16 :goto_0

    .line 377
    :sswitch_c
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    check-cast v2, Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    invoke-virtual {v2, v4}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->insertGallery(Z)V

    goto/16 :goto_0

    .line 386
    :sswitch_d
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    check-cast v2, Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    iput-boolean v4, v2, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mIsReplace:Z

    .line 387
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    check-cast v2, Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->insertCamera()V

    goto/16 :goto_0

    .line 390
    :sswitch_e
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    check-cast v2, Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->insertVideoFile()V

    goto/16 :goto_0

    .line 393
    :sswitch_f
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    check-cast v2, Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->insertVideoCapture()V

    goto/16 :goto_0

    .line 396
    :sswitch_10
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    const/16 v3, 0x305

    invoke-virtual {v2, v3, v4, v4}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->OnEVSheetFuction(III)V

    goto/16 :goto_0

    .line 402
    :sswitch_11
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    const/16 v3, 0x308

    invoke-virtual {v2, v3, v4, v4}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->OnEVSheetFuction(III)V

    .line 403
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    iget-boolean v2, v2, Lcom/infraware/office/actionbar/MainActionBar;->mPassWordActivity:Z

    if-nez v2, :cond_0

    .line 404
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsProtectSheet()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 405
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v2}, Lcom/infraware/office/actionbar/MainActionBar;->sheetProtect()V

    goto/16 :goto_0

    .line 407
    :cond_3
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v2}, Lcom/infraware/office/actionbar/MainActionBar;->sheetUnProtect()V

    goto/16 :goto_0

    .line 411
    :sswitch_12
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    const/16 v3, 0x306

    invoke-virtual {v2, v3, v4, v4}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->OnEVSheetFuction(III)V

    goto/16 :goto_0

    .line 414
    :sswitch_13
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    const/16 v3, 0x304

    invoke-virtual {v2, v3, v4, v4}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->OnEVSheetFuction(III)V

    goto/16 :goto_0

    .line 417
    :sswitch_14
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->sheetFilter()V

    goto/16 :goto_0

    .line 425
    :sswitch_15
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    .line 427
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 428
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v3}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/office/util/EvUtil;->hideIme(Landroid/content/Context;Landroid/os/IBinder;)V

    goto/16 :goto_0

    .line 431
    :sswitch_16
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->OnAnnotationEvent()V

    goto/16 :goto_0

    .line 434
    :sswitch_17
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->hideAnnot(Z)V

    .line 435
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->OnAnnotationListEvent()V

    goto/16 :goto_0

    .line 438
    :sswitch_18
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 439
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->onBookmarkMenu()V

    goto/16 :goto_0

    .line 442
    :sswitch_19
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 443
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getPageMode()I

    move-result v2

    if-nez v2, :cond_4

    .line 444
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->onContinuousLayout()V

    goto/16 :goto_0

    .line 446
    :cond_4
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->onSingleLayout()V

    goto/16 :goto_0

    .line 449
    :sswitch_1a
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 450
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->onContinuousLayout()V

    goto/16 :goto_0

    .line 453
    :sswitch_1b
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->fileOperationInfo()V

    goto/16 :goto_0

    .line 456
    :sswitch_1c
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onHelp()V

    goto/16 :goto_0

    .line 459
    :sswitch_1d
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    goto/16 :goto_0

    .line 464
    :sswitch_1e
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onSetting()V

    goto/16 :goto_0

    .line 467
    :sswitch_1f
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onRequestSamsungAppsPolarisOffice()V

    goto/16 :goto_0

    .line 470
    :sswitch_20
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    const-class v3, Lcom/infraware/polarisoffice5/OfficeDownloadFromSamsungApps;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 471
    .local v0, "i":Landroid/content/Intent;
    iget-object v2, p0, Lcom/infraware/office/actionbar/MainActionBar$1;->this$0:Lcom/infraware/office/actionbar/MainActionBar;

    # getter for: Lcom/infraware/office/actionbar/MainActionBar;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/actionbar/MainActionBar;->access$000(Lcom/infraware/office/actionbar/MainActionBar;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 309
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_1
        0x3 -> :sswitch_1
        0x4 -> :sswitch_1
        0x6 -> :sswitch_2
        0x7 -> :sswitch_3
        0x8 -> :sswitch_4
        0x9 -> :sswitch_6
        0x10 -> :sswitch_6
        0x11 -> :sswitch_5
        0x12 -> :sswitch_7
        0x13 -> :sswitch_8
        0x14 -> :sswitch_9
        0x15 -> :sswitch_a
        0x17 -> :sswitch_c
        0x18 -> :sswitch_d
        0x19 -> :sswitch_1
        0x20 -> :sswitch_10
        0x21 -> :sswitch_0
        0x22 -> :sswitch_11
        0x23 -> :sswitch_12
        0x24 -> :sswitch_13
        0x26 -> :sswitch_14
        0x27 -> :sswitch_15
        0x28 -> :sswitch_15
        0x29 -> :sswitch_15
        0x30 -> :sswitch_15
        0x31 -> :sswitch_6
        0x34 -> :sswitch_15
        0x35 -> :sswitch_16
        0x38 -> :sswitch_6
        0x49 -> :sswitch_6
        0x51 -> :sswitch_6
        0x57 -> :sswitch_1b
        0x58 -> :sswitch_1
        0x59 -> :sswitch_b
        0x61 -> :sswitch_3
        0x62 -> :sswitch_1c
        0x63 -> :sswitch_18
        0x65 -> :sswitch_e
        0x67 -> :sswitch_17
        0x69 -> :sswitch_6
        0x71 -> :sswitch_6
        0x72 -> :sswitch_15
        0x73 -> :sswitch_f
        0x74 -> :sswitch_1
        0x75 -> :sswitch_1
        0x77 -> :sswitch_6
        0x79 -> :sswitch_6
        0x80 -> :sswitch_1
        0x82 -> :sswitch_6
        0x83 -> :sswitch_6
        0x91 -> :sswitch_1d
        0x96 -> :sswitch_19
        0x97 -> :sswitch_1a
        0x102 -> :sswitch_6
        0x117 -> :sswitch_0
        0x119 -> :sswitch_1e
        0x120 -> :sswitch_1f
        0x121 -> :sswitch_20
    .end sparse-switch
.end method
