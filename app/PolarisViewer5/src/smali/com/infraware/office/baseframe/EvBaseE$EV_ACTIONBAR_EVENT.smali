.class public interface abstract Lcom/infraware/office/baseframe/EvBaseE$EV_ACTIONBAR_EVENT;
.super Ljava/lang/Object;
.source "EvBaseE.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/EvBaseE;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_ACTIONBAR_EVENT"
.end annotation


# static fields
.field public static final eEV_ACTIONBAR_ABOUT_EVENT:I = 0x57

.field public static final eEV_ACTIONBAR_ABOUT_PRODUCT_EVENT:I = 0x121

.field public static final eEV_ACTIONBAR_ADD_PRINT_ACCOUNT_EVENT:I = 0x92

.field public static final eEV_ACTIONBAR_ADD_SHEET_EVENT:I = 0x20

.field public static final eEV_ACTIONBAR_ALIGNMENT_EVENT:I = 0x106

.field public static final eEV_ACTIONBAR_ANNOTATION_EVENT:I = 0x35

.field public static final eEV_ACTIONBAR_ANNOTATION_LIST_EVENT:I = 0x67

.field public static final eEV_ACTIONBAR_ANNOTATION_LIST_TITLE_EVENT:I = 0x56

.field public static final eEV_ACTIONBAR_BOOKMARK_EVENT:I = 0x34

.field public static final eEV_ACTIONBAR_BROADCAST_EVENT:I = 0x80

.field public static final eEV_ACTIONBAR_CAMERA_EVENT:I = 0x18

.field public static final eEV_ACTIONBAR_CELL_EVENT:I = 0x16

.field public static final eEV_ACTIONBAR_CHANGE_VIEWMODE_EVENT:I = 0x117

.field public static final eEV_ACTIONBAR_CHART_DATA_RANGE_END_EVENT:I = 0x105

.field public static final eEV_ACTIONBAR_CHART_DATA_RANGE_EVENT:I = 0x104

.field public static final eEV_ACTIONBAR_CHART_EVENT:I = 0x13

.field public static final eEV_ACTIONBAR_COLOR_PINCKER_EVENT:I = 0x33

.field public static final eEV_ACTIONBAR_COLUMNS_EVENT:I = 0x14

.field public static final eEV_ACTIONBAR_COMMON_INPUT_EVENT:I = 0x32

.field public static final eEV_ACTIONBAR_COMMON_NUMBER_INPUT_EVENT:I = 0x47

.field public static final eEV_ACTIONBAR_CONDITIONAL_FORMAT:I = 0x64

.field public static final eEV_ACTIONBAR_CONTINUOUS_LAYOUT_EVENT:I = 0x97

.field public static final eEV_ACTIONBAR_DOUBLELAYOUT_EVENT:I = 0x81

.field public static final eEV_ACTIONBAR_EDIT_EVENT:I = 0x120

.field public static final eEV_ACTIONBAR_EDIT_PRINT_ACCOUNT_EVENT:I = 0x103

.field public static final eEV_ACTIONBAR_EMAIL_EVENT:I = 0x58

.field public static final eEV_ACTIONBAR_ENDPENDRAW_EVENT:I = 0x118

.field public static final eEV_ACTIONBAR_FILTER_EVENT:I = 0x26

.field public static final eEV_ACTIONBAR_FIND_REPLACE_EVENT:I = 0x4

.field public static final eEV_ACTIONBAR_FOOTNOTE_EVENT:I = 0x60

.field public static final eEV_ACTIONBAR_FREEDRAW_END_EVENT:I = 0x54

.field public static final eEV_ACTIONBAR_FREEDRAW_EVENT:I = 0x31

.field public static final eEV_ACTIONBAR_FREEDRAW_FREELINE_EVENT:I = 0x115

.field public static final eEV_ACTIONBAR_FREEDRAW_LASSO_EVENT:I = 0x116

.field public static final eEV_ACTIONBAR_FREEDRAW_SHAPE_EVENT:I = 0x114

.field public static final eEV_ACTIONBAR_FREEDRAW_STICKYNOTE_EVENT:I = 0x112

.field public static final eEV_ACTIONBAR_FREEDRAW_TEXT_EVENT:I = 0x113

.field public static final eEV_ACTIONBAR_FREEZE_FRAME_EVENT:I = 0x23

.field public static final eEV_ACTIONBAR_FULL_WIDTH_EVENT:I = 0x6

.field public static final eEV_ACTIONBAR_GALLERY_EVENT:I = 0x17

.field public static final eEV_ACTIONBAR_HELP_EVENT:I = 0x62

.field public static final eEV_ACTIONBAR_HYPERLINK_EVENT:I = 0x10

.field public static final eEV_ACTIONBAR_INSERT_SLIDE_EVENT:I = 0x55

.field public static final eEV_ACTIONBAR_LASSO_EVENT:I = 0x79

.field public static final eEV_ACTIONBAR_LAYOUT_EVENT:I = 0x83

.field public static final eEV_ACTIONBAR_MANAGE_SHEET_EVENT:I = 0x21

.field public static final eEV_ACTIONBAR_MANAGE_SLIDE_EVENT:I = 0x28

.field public static final eEV_ACTIONBAR_MASK_END_EVENT:I = 0x45

.field public static final eEV_ACTIONBAR_MASK_EVENT:I = 0x44

.field public static final eEV_ACTIONBAR_MEMO_EVENT:I = 0x8

.field public static final eEV_ACTIONBAR_MULTISELECT_GROUP_EVENT:I = 0x91

.field public static final eEV_ACTIONBAR_MULTI_SELECTION_END_EVENT:I = 0x53

.field public static final eEV_ACTIONBAR_MULTI_SELECTION_EVENT:I = 0x52

.field public static final eEV_ACTIONBAR_NUMBERS_LIST_CURRENCY_EVENT:I = 0x37

.field public static final eEV_ACTIONBAR_NUMBERS_LIST_DATE_EVENT:I = 0x48

.field public static final eEV_ACTIONBAR_NUMBERS_LIST_EVENT:I = 0x36

.field public static final eEV_ACTIONBAR_PAGE_ANIMATION_EVENT:I = 0x72

.field public static final eEV_ACTIONBAR_PAGE_LAYOUT_EVENT:I = 0x9

.field public static final eEV_ACTIONBAR_PAGE_NUMBER_EVENT:I = 0x93

.field public static final eEV_ACTIONBAR_PASSWORD_EVENT:I = 0x46

.field public static final eEV_ACTIONBAR_PDF_EVENT:I = 0x2

.field public static final eEV_ACTIONBAR_PENDRAW_ERASEALL_EVENT:I = 0x111

.field public static final eEV_ACTIONBAR_PENDRAW_ERASE_EVENT:I = 0x110

.field public static final eEV_ACTIONBAR_PENDRAW_EVENT:I = 0x82

.field public static final eEV_ACTIONBAR_PENDRAW_LASSO_EVENT:I = 0x109

.field public static final eEV_ACTIONBAR_PENDRAW_PEN_EVENT:I = 0x107

.field public static final eEV_ACTIONBAR_PENDRAW_PEN_OPTION_EVENT:I = 0x108

.field public static final eEV_ACTIONBAR_PRINTACCOUNT_EVENT:I = 0x68

.field public static final eEV_ACTIONBAR_PRINT_EVENT:I = 0x51

.field public static final eEV_ACTIONBAR_PROTECT_SHEET_EVENT:I = 0x22

.field public static final eEV_ACTIONBAR_RECALCULATION_EVENT:I = 0x24

.field public static final eEV_ACTIONBAR_RECORDING_EVENT:I = 0x73

.field public static final eEV_ACTIONBAR_REFLOW_EVENT:I = 0x38

.field public static final eEV_ACTIONBAR_RESIZE_IMAGE_EVENT:I = 0x40

.field public static final eEV_ACTIONBAR_ROTATE_IMAGE_EVENT:I = 0x43

.field public static final eEV_ACTIONBAR_ROW_EVENT:I = 0x15

.field public static final eEV_ACTIONBAR_RULER_END_EVENT:I = 0x78

.field public static final eEV_ACTIONBAR_RULER_EVENT:I = 0x61

.field public static final eEV_ACTIONBAR_SAVEAS_EVENT:I = 0x3

.field public static final eEV_ACTIONBAR_SAVE_EVENT:I = 0x1

.field public static final eEV_ACTIONBAR_SETTING_EVENT:I = 0x119

.field public static final eEV_ACTIONBAR_SHAPE_DRWAING_END_EVENT:I = 0x50

.field public static final eEV_ACTIONBAR_SHAPE_DRWAING_EVENT:I = 0x49

.field public static final eEV_ACTIONBAR_SHAPE_EVENT:I = 0x11

.field public static final eEV_ACTIONBAR_SHEET_INPUT_EVENT:I = 0x42

.field public static final eEV_ACTIONBAR_SINGLE_LAYOUT_EVENT:I = 0x96

.field public static final eEV_ACTIONBAR_SINGLE_SLIDE_NOTE_EVENT:I = 0x30

.field public static final eEV_ACTIONBAR_SINGLE_SLIDE_VIEW_OFF_EVENT:I = 0x29

.field public static final eEV_ACTIONBAR_SLIDESHOW_EVENT:I = 0x27

.field public static final eEV_ACTIONBAR_SLIDE_ANIMATION_ADD_EVENT:I = 0x100

.field public static final eEV_ACTIONBAR_SLIDE_ANIMATION_EFFECT_EVENT:I = 0x101

.field public static final eEV_ACTIONBAR_SLIDE_ANIMATION_EVENT:I = 0x98

.field public static final eEV_ACTIONBAR_SLIDE_TRANSITION_END_EVENT:I = 0x95

.field public static final eEV_ACTIONBAR_SLIDE_TRANSITION_EVENT:I = 0x94

.field public static final eEV_ACTIONBAR_SORT_EVENT:I = 0x25

.field public static final eEV_ACTIONBAR_SPELL_CHECK_EVENT:I = 0x102

.field public static final eEV_ACTIONBAR_SPLIT_EVENT:I = 0x39

.field public static final eEV_ACTIONBAR_SYMBOL_EVENT:I = 0x59

.field public static final eEV_ACTIONBAR_TABLEOFCONTENT_LIST_EVENT:I = 0x63

.field public static final eEV_ACTIONBAR_TABLE_DRAW_END_EVENT:I = 0x70

.field public static final eEV_ACTIONBAR_TABLE_DRAW_EVENT:I = 0x69

.field public static final eEV_ACTIONBAR_TABLE_EREASE_EVENT:I = 0x71

.field public static final eEV_ACTIONBAR_TABLE_EVENT:I = 0x12

.field public static final eEV_ACTIONBAR_TEXTBOX_EVENT:I = 0x19

.field public static final eEV_ACTIONBAR_TEXTBOX_HORI_EVENT:I = 0x74

.field public static final eEV_ACTIONBAR_TEXTBOX_VERTI_EVENT:I = 0x75

.field public static final eEV_ACTIONBAR_TTS_EVENT:I = 0x7

.field public static final eEV_ACTIONBAR_TTS_STOP_EVENT:I = 0x41

.field public static final eEV_ACTIONBAR_VIDEO_EVENT:I = 0x65

.field public static final eEV_ACTIONBAR_VIEWMODE_EVENT:I = 0x77

.field public static final eEV_ACTIONBAR_ZOOM_EVENT:I = 0x5
