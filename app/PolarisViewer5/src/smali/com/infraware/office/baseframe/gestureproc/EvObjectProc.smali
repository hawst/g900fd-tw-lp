.class public Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;
.super Ljava/lang/Object;
.source "EvObjectProc.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_CARET_DIRECTION_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_EDIT_OBJECT_BASE_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_EDIT_OBJECT_POINT_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_EDIT_OBJECT_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_TEXTMARK_DIRECTION_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_VIDEO_STATUS;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$1;,
        Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;,
        Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;,
        Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;,
        Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;,
        Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;,
        Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;,
        Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;,
        Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;
    }
.end annotation


# static fields
.field static final ACTION_DOWN_TEXT_ONLY:I = 0x3e8

.field static final BLANK_OBJECT_CAMERA:I = 0x2

.field static final BLANK_OBJECT_IMAGE:I = 0x1

.field static final BLANK_OBJECT_MOVIE:I = 0x3

.field static final BLANK_OBJECT_NONE:I = 0x0

.field static final BLANK_OBJECT_VEDIO:I = 0x4


# instance fields
.field bPressedFilterBtn:Z

.field mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

.field private mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

.field private mDocType:I

.field public mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

.field private mGrapAttInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;

.field private mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

.field private mListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

.field private mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

.field private mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

.field private mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

.field private mSheetAutofilterInfo:[I

.field private mSheetFormulaCursorRangeInfo:[I

.field private mSheetFormulaRangeInfo:[I

.field private mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

.field private mTouchDown:Landroid/graphics/Point;

.field private mView:Landroid/view/View;

.field private m_nSheetFormulaRangeCount:I

.field public rcVideoPlayBtn:Landroid/graphics/Rect;


# direct methods
.method constructor <init>(Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "listener"    # Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;
    .param p3, "bgp"    # Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    .prologue
    const/4 v5, 0x0

    const/4 v3, -0x1

    const/4 v4, 0x0

    .line 478
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    .line 54
    iput-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    .line 55
    iput-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    .line 56
    iput-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    .line 57
    iput-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    .line 58
    iput-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    .line 60
    iput-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mGrapAttInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;

    .line 62
    iput-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mView:Landroid/view/View;

    .line 63
    iput-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 64
    iput-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    .line 65
    iput-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    .line 66
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2, v3, v3}, Landroid/graphics/Point;-><init>(II)V

    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTouchDown:Landroid/graphics/Point;

    .line 68
    const/4 v2, 0x4

    iput v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mDocType:I

    .line 70
    iput-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->rcVideoPlayBtn:Landroid/graphics/Rect;

    .line 74
    iput-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mSheetAutofilterInfo:[I

    .line 75
    iput-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mSheetFormulaRangeInfo:[I

    .line 76
    iput-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mSheetFormulaCursorRangeInfo:[I

    .line 77
    iput v5, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->m_nSheetFormulaRangeCount:I

    .line 79
    iput-boolean v5, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->bPressedFilterBtn:Z

    .line 480
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mView:Landroid/view/View;

    .line 481
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 483
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 485
    .local v1, "resources":Landroid/content/res/Resources;
    new-instance v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    invoke-direct {v2, p0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;-><init>(Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;)V

    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    .line 486
    new-instance v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    invoke-direct {v2, p0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;-><init>(Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;)V

    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    .line 488
    const v2, 0x7f02031c

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 489
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iput v3, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->barWidth:I

    .line 490
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 492
    const v2, 0x7f020329

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 493
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->iconSize:Landroid/graphics/Point;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iput v3, v2, Landroid/graphics/Point;->x:I

    .line 494
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->iconSize:Landroid/graphics/Point;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    iput v3, v2, Landroid/graphics/Point;->y:I

    .line 495
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 497
    new-instance v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-direct {v2, p0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;-><init>(Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;)V

    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    .line 498
    const v2, 0x7f020314

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 499
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->iconSize:Landroid/graphics/Point;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iput v3, v2, Landroid/graphics/Point;->x:I

    .line 500
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->iconSize:Landroid/graphics/Point;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    iput v3, v2, Landroid/graphics/Point;->y:I

    .line 501
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 503
    new-instance v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    invoke-direct {v2, p0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;-><init>(Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;)V

    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    .line 504
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->iconSize:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iput v3, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->pointImageSize:I

    .line 505
    new-instance v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    invoke-direct {v2, p0, v4}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;-><init>(Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$1;)V

    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    .line 506
    const v2, 0x7f020318

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 507
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    iput v3, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfIconSize:I

    .line 508
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 510
    const v2, 0x7f0201ed

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 511
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    iput v3, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfSheetIconSize:I

    .line 512
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 514
    new-instance v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;

    invoke-direct {v2, p0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;-><init>(Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;)V

    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mGrapAttInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;

    .line 516
    new-instance v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    invoke-direct {v2, p0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;-><init>(Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;)V

    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    .line 524
    iput-object p2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    .line 525
    iput-object p3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    .line 527
    return-void
.end method

.method private GetObjectBaseType(I)I
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 3220
    const/4 v0, 0x0

    .line 3222
    .local v0, "nBaseType":I
    and-int/lit16 v1, p1, 0xfff

    sparse-switch v1, :sswitch_data_0

    .line 3258
    :goto_0
    return v0

    .line 3231
    :sswitch_0
    const/4 v0, 0x1

    .line 3232
    goto :goto_0

    .line 3245
    :sswitch_1
    const/4 v0, 0x2

    .line 3246
    goto :goto_0

    .line 3248
    :sswitch_2
    const/4 v0, 0x3

    .line 3249
    goto :goto_0

    .line 3251
    :sswitch_3
    const/4 v0, 0x4

    .line 3252
    goto :goto_0

    .line 3254
    :sswitch_4
    const/4 v0, 0x6

    goto :goto_0

    .line 3222
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x3 -> :sswitch_2
        0x4 -> :sswitch_1
        0x5 -> :sswitch_1
        0x6 -> :sswitch_1
        0x7 -> :sswitch_1
        0x8 -> :sswitch_1
        0x9 -> :sswitch_3
        0xa -> :sswitch_1
        0xb -> :sswitch_0
        0xc -> :sswitch_0
        0xd -> :sswitch_1
        0xe -> :sswitch_1
        0xf -> :sswitch_1
        0x10 -> :sswitch_1
        0x11 -> :sswitch_1
        0x12 -> :sswitch_1
        0x13 -> :sswitch_4
        0x20 -> :sswitch_0
        0x30 -> :sswitch_0
        0x40 -> :sswitch_0
        0x50 -> :sswitch_0
    .end sparse-switch
.end method

.method private drawBaseCell(Landroid/graphics/Canvas;Landroid/content/res/Resources;Lcom/infraware/office/baseframe/EvBaseView;)V
    .locals 36
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "resources"    # Landroid/content/res/Resources;
    .param p3, "ebv"    # Lcom/infraware/office/baseframe/EvBaseView;

    .prologue
    .line 2402
    const/16 v33, 0x0

    .line 2403
    .local v33, "sheetBitmap":Landroid/graphics/Bitmap;
    const/16 v22, 0x0

    .line 2404
    .local v22, "bitmapRight":Landroid/graphics/Bitmap;
    const/16 v20, 0x0

    .line 2405
    .local v20, "bitmapRegionEnd":Landroid/graphics/Bitmap;
    const/16 v21, 0x0

    .line 2406
    .local v21, "bitmapRegionStart":Landroid/graphics/Bitmap;
    const/16 v19, 0x0

    .line 2408
    .local v19, "bitmapBottom":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    sparse-switch v5, :sswitch_data_0

    .line 2699
    :cond_0
    :goto_0
    return-void

    .line 2433
    :sswitch_0
    const/4 v10, 0x0

    .line 2434
    .local v10, "cellMarkPaint":Landroid/graphics/Paint;
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_TEXTMARK_REVERSE()Z

    move-result v5

    if-nez v5, :cond_3

    .line 2435
    new-instance v10, Landroid/graphics/Paint;

    .end local v10    # "cellMarkPaint":Landroid/graphics/Paint;
    invoke-direct {v10}, Landroid/graphics/Paint;-><init>()V

    .line 2436
    .restart local v10    # "cellMarkPaint":Landroid/graphics/Paint;
    const/16 v5, 0x4d

    const/4 v6, 0x0

    const/16 v7, 0x8f

    const/16 v8, 0xe1

    invoke-virtual {v10, v5, v6, v7, v8}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 2437
    sget-object v5, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v10, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2451
    :goto_1
    const/16 v29, 0x0

    .local v29, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->nCellRectCount:I

    move/from16 v0, v29

    if-ge v0, v5, :cond_7

    .line 2453
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->mCellRectInfos:[S

    mul-int/lit8 v6, v29, 0x4

    aget-short v30, v5, v6

    .line 2454
    .local v30, "l":S
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->mCellRectInfos:[S

    mul-int/lit8 v6, v29, 0x4

    add-int/lit8 v6, v6, 0x1

    aget-short v35, v5, v6

    .line 2455
    .local v35, "t":S
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->mCellRectInfos:[S

    mul-int/lit8 v6, v29, 0x4

    add-int/lit8 v6, v6, 0x2

    aget-short v32, v5, v6

    .line 2456
    .local v32, "r":S
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->mCellRectInfos:[S

    mul-int/lit8 v6, v29, 0x4

    add-int/lit8 v6, v6, 0x3

    aget-short v17, v5, v6

    .line 2458
    .local v17, "b":S
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_TEXTMARK_REVERSE()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 2460
    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/infraware/office/baseframe/EvBaseView;->mSyncObject:Ljava/lang/Object;

    monitor-enter v6

    .line 2462
    :try_start_0
    invoke-virtual/range {p3 .. p3}, Lcom/infraware/office/baseframe/EvBaseView;->getPrivateBitmap()Landroid/graphics/Bitmap;

    move-result-object v24

    .line 2464
    .local v24, "content":Landroid/graphics/Bitmap;
    if-eqz v24, :cond_2

    .line 2466
    new-instance v26, Landroid/graphics/Rect;

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    move-object/from16 v0, v26

    invoke-direct {v0, v5, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2468
    .local v26, "drawRect":Landroid/graphics/Rect;
    move-object/from16 v0, v26

    move/from16 v1, v30

    move/from16 v2, v35

    move/from16 v3, v32

    move/from16 v4, v17

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->intersect(IIII)Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Rect;->width()I

    move-result v5

    if-lez v5, :cond_5

    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Rect;->height()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-lez v5, :cond_5

    .line 2471
    const/16 v25, 0x0

    .line 2474
    .local v25, "cursorBitmap":Landroid/graphics/Bitmap;
    :try_start_1
    move-object/from16 v0, v26

    iget v5, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, v26

    iget v7, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Rect;->height()I

    move-result v9

    move-object/from16 v0, v24

    invoke-static {v0, v5, v7, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v25

    .line 2475
    move-object/from16 v0, v25

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_4

    const/16 v18, 0x1

    .line 2476
    .local v18, "bSame":Z
    :goto_3
    const/16 v24, 0x0

    .line 2477
    if-eqz v25, :cond_2

    .line 2479
    move-object/from16 v0, v26

    iget v5, v0, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    move-object/from16 v0, v26

    iget v7, v0, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v5, v7, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2480
    if-nez v18, :cond_1

    .line 2481
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2482
    :cond_1
    const/16 v25, 0x0

    .line 2495
    .end local v18    # "bSame":Z
    .end local v25    # "cursorBitmap":Landroid/graphics/Bitmap;
    .end local v26    # "drawRect":Landroid/graphics/Rect;
    :cond_2
    :goto_4
    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2451
    .end local v24    # "content":Landroid/graphics/Bitmap;
    :goto_5
    add-int/lit8 v29, v29, 0x1

    goto/16 :goto_2

    .line 2440
    .end local v17    # "b":S
    .end local v29    # "i":I
    .end local v30    # "l":S
    .end local v32    # "r":S
    .end local v35    # "t":S
    :cond_3
    new-instance v10, Landroid/graphics/Paint;

    .end local v10    # "cellMarkPaint":Landroid/graphics/Paint;
    const/4 v5, 0x1

    invoke-direct {v10, v5}, Landroid/graphics/Paint;-><init>(I)V

    .line 2442
    .restart local v10    # "cellMarkPaint":Landroid/graphics/Paint;
    new-instance v23, Landroid/graphics/ColorMatrix;

    const/16 v5, 0x14

    new-array v5, v5, [F

    fill-array-data v5, :array_0

    move-object/from16 v0, v23

    invoke-direct {v0, v5}, Landroid/graphics/ColorMatrix;-><init>([F)V

    .line 2447
    .local v23, "colorMatrix":Landroid/graphics/ColorMatrix;
    new-instance v5, Landroid/graphics/ColorMatrixColorFilter;

    move-object/from16 v0, v23

    invoke-direct {v5, v0}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    invoke-virtual {v10, v5}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto/16 :goto_1

    .line 2475
    .end local v23    # "colorMatrix":Landroid/graphics/ColorMatrix;
    .restart local v17    # "b":S
    .restart local v24    # "content":Landroid/graphics/Bitmap;
    .restart local v25    # "cursorBitmap":Landroid/graphics/Bitmap;
    .restart local v26    # "drawRect":Landroid/graphics/Rect;
    .restart local v29    # "i":I
    .restart local v30    # "l":S
    .restart local v32    # "r":S
    .restart local v35    # "t":S
    :cond_4
    const/16 v18, 0x0

    goto :goto_3

    .line 2485
    :catch_0
    move-exception v28

    .line 2487
    .local v28, "ex":Ljava/lang/OutOfMemoryError;
    const/16 v24, 0x0

    goto :goto_4

    .line 2491
    .end local v25    # "cursorBitmap":Landroid/graphics/Bitmap;
    .end local v28    # "ex":Ljava/lang/OutOfMemoryError;
    :cond_5
    const/16 v24, 0x0

    goto :goto_4

    .line 2495
    .end local v24    # "content":Landroid/graphics/Bitmap;
    .end local v26    # "drawRect":Landroid/graphics/Rect;
    :catchall_0
    move-exception v5

    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v5

    .line 2498
    :cond_6
    move/from16 v0, v30

    int-to-float v6, v0

    move/from16 v0, v35

    int-to-float v7, v0

    move/from16 v0, v32

    int-to-float v8, v0

    move/from16 v0, v17

    int-to-float v9, v0

    move-object/from16 v5, p1

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_5

    .line 2502
    .end local v17    # "b":S
    .end local v30    # "l":S
    .end local v32    # "r":S
    .end local v35    # "t":S
    :cond_7
    const/4 v10, 0x0

    .line 2504
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_9

    .line 2505
    const v5, 0x7f020316

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v22

    .line 2506
    const v5, 0x7f020315

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v19

    .line 2507
    const v5, 0x7f020318

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v20

    .line 2508
    const/16 v21, 0x0

    .line 2537
    :goto_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v5}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->IsViewerMode()Z

    move-result v5

    if-nez v5, :cond_8

    .line 2539
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RightPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfIconSize:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RightPoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v7, v7, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfIconSize:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v5, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2541
    if-eqz v19, :cond_d

    .line 2543
    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Bitmap;->recycle()V

    .line 2544
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->BottomPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfIconSize:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->BottomPoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v7, v7, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfIconSize:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v5, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2545
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Bitmap;->recycle()V

    .line 2554
    :cond_8
    :goto_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->endSelectPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfIconSize:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->endSelectPoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v7, v7, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfIconSize:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v5, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2556
    if-eqz v21, :cond_e

    .line 2558
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Bitmap;->recycle()V

    .line 2559
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->startSelectPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfIconSize:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->startSelectPoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v7, v7, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfIconSize:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v5, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2560
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_0

    .line 2510
    :cond_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_a

    .line 2511
    const v5, 0x7f020315

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v22

    .line 2512
    const v5, 0x7f020316

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v19

    .line 2513
    const v5, 0x7f020318

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v20

    .line 2514
    const/16 v21, 0x0

    goto/16 :goto_6

    .line 2516
    :cond_a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    const/4 v6, 0x3

    if-ne v5, v6, :cond_b

    .line 2518
    const v5, 0x7f020315

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v22

    .line 2519
    const/16 v19, 0x0

    .line 2520
    const v5, 0x7f020319

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v20

    .line 2521
    const v5, 0x7f020318

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v21

    goto/16 :goto_6

    .line 2523
    :cond_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    const/4 v6, 0x4

    if-ne v5, v6, :cond_c

    .line 2525
    const v5, 0x7f020315

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v22

    .line 2526
    const/16 v19, 0x0

    .line 2527
    const v5, 0x7f020318

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v20

    .line 2528
    const v5, 0x7f020319

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v21

    goto/16 :goto_6

    .line 2531
    :cond_c
    const v5, 0x7f020315

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v22

    .line 2532
    const/16 v19, 0x0

    .line 2533
    const v5, 0x7f020318

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v20

    .line 2534
    const/16 v21, 0x0

    goto/16 :goto_6

    .line 2549
    :cond_d
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->BottomPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfIconSize:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->BottomPoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v7, v7, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfIconSize:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v5, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2550
    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_7

    .line 2564
    :cond_e
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->startSelectPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfIconSize:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->startSelectPoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v7, v7, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfIconSize:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v5, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2565
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_0

    .line 2572
    .end local v10    # "cellMarkPaint":Landroid/graphics/Paint;
    .end local v29    # "i":I
    :sswitch_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    const/4 v6, 0x3

    if-eq v5, v6, :cond_f

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    const/4 v6, 0x4

    if-ne v5, v6, :cond_12

    .line 2574
    :cond_f
    const v5, 0x7f020317

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v33

    .line 2581
    :goto_8
    if-eqz v33, :cond_0

    .line 2586
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->LTRegionPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    const/16 v6, 0x28

    if-le v5, v6, :cond_10

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->LTRegionPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    const/16 v6, 0x21

    if-le v5, v6, :cond_10

    .line 2587
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->LTRegionPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfIconSize:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->LTRegionPoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v7, v7, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfIconSize:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1, v5, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2589
    :cond_10
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RBRegionPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    const/16 v6, 0x28

    if-le v5, v6, :cond_11

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RBRegionPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    const/16 v6, 0x21

    if-le v5, v6, :cond_11

    .line 2590
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RBRegionPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfIconSize:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RBRegionPoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v7, v7, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfIconSize:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1, v5, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2592
    :cond_11
    invoke-virtual/range {v33 .. v33}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_0

    .line 2578
    :cond_12
    const v5, 0x7f020314

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v33

    goto/16 :goto_8

    .line 2596
    :sswitch_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RightPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    const/16 v6, 0x28

    if-le v5, v6, :cond_13

    .line 2597
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_14

    .line 2598
    const v5, 0x7f0201ee

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v33

    .line 2603
    :cond_13
    :goto_9
    move-object/from16 v0, p0

    iget v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mDocType:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_15

    .line 2604
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mView:Landroid/view/View;

    move-object/from16 v27, v0

    check-cast v27, Lcom/infraware/office/baseframe/EvBaseView;

    .line 2605
    .local v27, "eba":Lcom/infraware/office/baseframe/EvBaseView;
    invoke-virtual/range {v27 .. v27}, Lcom/infraware/office/baseframe/EvBaseView;->getContext()Landroid/content/Context;

    move-result-object v34

    check-cast v34, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .line 2607
    .local v34, "sheetEditorActivity":Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-virtual/range {v34 .. v34}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getCurrentSheetInfo()Lcom/infraware/office/evengine/EV$SHEET_INFO;

    move-result-object v31

    .line 2608
    .local v31, "m_sSheetInfo":Lcom/infraware/office/evengine/EV$SHEET_INFO;
    move-object/from16 v0, v31

    iget v5, v0, Lcom/infraware/office/evengine/EV$SHEET_INFO;->bProtectSheet:I

    if-lez v5, :cond_15

    .line 2609
    invoke-virtual/range {v33 .. v33}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_0

    .line 2600
    .end local v27    # "eba":Lcom/infraware/office/baseframe/EvBaseView;
    .end local v31    # "m_sSheetInfo":Lcom/infraware/office/evengine/EV$SHEET_INFO;
    .end local v34    # "sheetEditorActivity":Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    :cond_14
    const v5, 0x7f0201ed

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v33

    goto :goto_9

    .line 2613
    :cond_15
    if-eqz v33, :cond_0

    .line 2615
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RightPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfSheetIconSize:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RightPoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v7, v7, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfSheetIconSize:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1, v5, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2616
    invoke-virtual/range {v33 .. v33}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_0

    .line 2620
    :sswitch_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->BottomPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    const/16 v6, 0x21

    if-le v5, v6, :cond_16

    .line 2621
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_17

    .line 2622
    const v5, 0x7f0201de

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v33

    .line 2627
    :cond_16
    :goto_a
    move-object/from16 v0, p0

    iget v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mDocType:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_18

    .line 2628
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mView:Landroid/view/View;

    move-object/from16 v27, v0

    check-cast v27, Lcom/infraware/office/baseframe/EvBaseView;

    .line 2629
    .restart local v27    # "eba":Lcom/infraware/office/baseframe/EvBaseView;
    invoke-virtual/range {v27 .. v27}, Lcom/infraware/office/baseframe/EvBaseView;->getContext()Landroid/content/Context;

    move-result-object v34

    check-cast v34, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .line 2631
    .restart local v34    # "sheetEditorActivity":Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-virtual/range {v34 .. v34}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getCurrentSheetInfo()Lcom/infraware/office/evengine/EV$SHEET_INFO;

    move-result-object v31

    .line 2632
    .restart local v31    # "m_sSheetInfo":Lcom/infraware/office/evengine/EV$SHEET_INFO;
    move-object/from16 v0, v31

    iget v5, v0, Lcom/infraware/office/evengine/EV$SHEET_INFO;->bProtectSheet:I

    if-lez v5, :cond_18

    .line 2633
    invoke-virtual/range {v33 .. v33}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_0

    .line 2624
    .end local v27    # "eba":Lcom/infraware/office/baseframe/EvBaseView;
    .end local v31    # "m_sSheetInfo":Lcom/infraware/office/evengine/EV$SHEET_INFO;
    .end local v34    # "sheetEditorActivity":Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    :cond_17
    const v5, 0x7f0201dd

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v33

    goto :goto_a

    .line 2638
    :cond_18
    if-eqz v33, :cond_0

    .line 2640
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->BottomPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfSheetIconSize:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->BottomPoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v7, v7, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfSheetIconSize:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1, v5, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2641
    invoke-virtual/range {v33 .. v33}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_0

    .line 2646
    :sswitch_4
    new-instance v16, Landroid/graphics/Paint;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Paint;-><init>()V

    .line 2647
    .local v16, "cellTablePaint":Landroid/graphics/Paint;
    const/16 v5, 0x4d

    const/16 v6, 0x28

    const/16 v7, 0x82

    const/16 v8, 0xce

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 2648
    const/high16 v5, 0x40e00000    # 7.0f

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2649
    const/4 v5, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2650
    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2651
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->startSelectPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v12, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->startSelectPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v13, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->endSelectPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v14, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->endSelectPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v15, v5

    move-object/from16 v11, p1

    invoke-virtual/range {v11 .. v16}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 2658
    .end local v16    # "cellTablePaint":Landroid/graphics/Paint;
    :sswitch_5
    new-instance v16, Landroid/graphics/Paint;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Paint;-><init>()V

    .line 2659
    .restart local v16    # "cellTablePaint":Landroid/graphics/Paint;
    const/16 v5, 0xff

    const/16 v6, 0x80

    const/4 v7, 0x0

    const/16 v8, 0x80

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 2660
    sget-object v5, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2661
    const/high16 v5, 0x41200000    # 10.0f

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2662
    const/4 v5, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2663
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->startSelectPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v12, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->startSelectPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v13, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->endSelectPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v14, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->endSelectPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v15, v5

    move-object/from16 v11, p1

    invoke-virtual/range {v11 .. v16}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 2670
    .end local v16    # "cellTablePaint":Landroid/graphics/Paint;
    :sswitch_6
    new-instance v16, Landroid/graphics/Paint;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Paint;-><init>()V

    .line 2671
    .restart local v16    # "cellTablePaint":Landroid/graphics/Paint;
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_TEXTMARK_REVERSE()Z

    move-result v5

    if-nez v5, :cond_19

    .line 2672
    const/16 v5, 0x4d

    const/16 v6, 0x28

    const/16 v7, 0x82

    const/16 v8, 0xce

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 2675
    :goto_b
    sget-object v5, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2676
    const/high16 v5, 0x40a00000    # 5.0f

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2677
    const/4 v5, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2678
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->startSelectPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v12, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->startSelectPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v13, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->endSelectPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v14, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->endSelectPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v15, v5

    move-object/from16 v11, p1

    invoke-virtual/range {v11 .. v16}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 2674
    :cond_19
    const/16 v5, 0xff

    const/16 v6, 0x28

    const/16 v7, 0x82

    const/16 v8, 0xce

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto :goto_b

    .line 2685
    .end local v16    # "cellTablePaint":Landroid/graphics/Paint;
    :sswitch_7
    new-instance v16, Landroid/graphics/Paint;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Paint;-><init>()V

    .line 2686
    .restart local v16    # "cellTablePaint":Landroid/graphics/Paint;
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_TEXTMARK_REVERSE()Z

    move-result v5

    if-nez v5, :cond_1a

    .line 2687
    const/16 v5, 0x4d

    const/16 v6, 0xc0

    const/16 v7, 0xc0

    const/16 v8, 0xc0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 2690
    :goto_c
    sget-object v5, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2691
    const/high16 v5, 0x40a00000    # 5.0f

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2692
    const/4 v5, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2693
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->startSelectPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v12, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->startSelectPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v13, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->endSelectPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v14, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->endSelectPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v15, v5

    move-object/from16 v11, p1

    invoke-virtual/range {v11 .. v16}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 2689
    :cond_1a
    const/16 v5, 0xff

    const/16 v6, 0xc0

    const/16 v7, 0xc0

    const/16 v8, 0xc0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto :goto_c

    .line 2408
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_0
        0xb -> :sswitch_3
        0xc -> :sswitch_2
        0x20 -> :sswitch_6
        0x30 -> :sswitch_5
        0x40 -> :sswitch_7
        0x50 -> :sswitch_4
    .end sparse-switch

    .line 2442
    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        0x0
        0x0
        0x0
        0x437f0000    # 255.0f
        0x0
        -0x40800000    # -1.0f
        0x0
        0x0
        0x437f0000    # 255.0f
        0x0
        0x0
        -0x40800000    # -1.0f
        0x0
        0x437f0000    # 255.0f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private drawBaseInfraPen(Landroid/graphics/Canvas;Landroid/content/res/Resources;)V
    .locals 44
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 1995
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v0, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    move-object/from16 v37, v0

    .line 1997
    .local v37, "p":[Landroid/graphics/Point;
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_TEXTMARK_REVERSE()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1999
    new-instance v39, Landroid/graphics/Paint;

    invoke-direct/range {v39 .. v39}, Landroid/graphics/Paint;-><init>()V

    .line 2000
    .local v39, "paintFill":Landroid/graphics/Paint;
    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v39

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2002
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    const/4 v4, 0x5

    if-ne v3, v4, :cond_b

    .line 2003
    const/16 v3, 0x40

    const/16 v4, 0x77

    const/16 v5, 0xb5

    const/16 v6, 0xf0

    move-object/from16 v0, v39

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 2007
    :goto_0
    new-instance v23, Landroid/graphics/Path;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/Path;-><init>()V

    .line 2008
    .local v23, "fillPath":Landroid/graphics/Path;
    sget-object v3, Landroid/graphics/Path$FillType;->WINDING:Landroid/graphics/Path$FillType;

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 2009
    const/4 v3, 0x1

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    const/4 v4, 0x1

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 2010
    const/4 v3, 0x2

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    const/4 v4, 0x2

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2011
    const/4 v3, 0x3

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    const/4 v4, 0x3

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2012
    const/4 v3, 0x4

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    const/4 v4, 0x4

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2013
    const/4 v3, 0x1

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    const/4 v4, 0x1

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2014
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Path;->close()V

    .line 2016
    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v39

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 2018
    const/16 v39, 0x0

    .line 2025
    .end local v23    # "fillPath":Landroid/graphics/Path;
    .end local v39    # "paintFill":Landroid/graphics/Paint;
    :cond_0
    new-instance v8, Landroid/graphics/Paint;

    invoke-direct {v8}, Landroid/graphics/Paint;-><init>()V

    .line 2026
    .local v8, "paint1":Landroid/graphics/Paint;
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_TEXTMARK_REVERSE()Z

    move-result v3

    if-nez v3, :cond_c

    .line 2028
    const v3, -0xe47f39

    invoke-virtual {v8, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 2030
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v8, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2031
    const/4 v3, 0x1

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v4, v3

    const/4 v3, 0x1

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v5, v3

    const/4 v3, 0x2

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v6, v3

    const/4 v3, 0x2

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v7, v3

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2032
    const/4 v3, 0x3

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v4, v3

    const/4 v3, 0x3

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v5, v3

    const/4 v3, 0x4

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v6, v3

    const/4 v3, 0x4

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v7, v3

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2034
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v8, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2035
    const/4 v3, 0x2

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v4, v3

    const/4 v3, 0x2

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v5, v3

    const/4 v3, 0x3

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v6, v3

    const/4 v3, 0x3

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v7, v3

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2036
    const/4 v3, 0x4

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v4, v3

    const/4 v3, 0x4

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v5, v3

    const/4 v3, 0x1

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v6, v3

    const/4 v3, 0x1

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v7, v3

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2071
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->bRotationEnabled:I

    if-eqz v3, :cond_1

    .line 2072
    const v3, -0xa58984

    invoke-virtual {v8, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 2073
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v8, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2074
    const/4 v3, 0x5

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v4, v3

    const/4 v3, 0x5

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v5, v3

    const/16 v3, 0x9

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v6, v3

    const/16 v3, 0x9

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v7, v3

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2077
    :cond_1
    const/4 v8, 0x0

    .line 2081
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->iconSize:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    div-int/lit8 v24, v3, 0x2

    .line 2082
    .local v24, "half_x":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->iconSize:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    div-int/lit8 v26, v3, 0x2

    .line 2087
    .local v26, "half_y":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v3, 0x7f020314

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v19

    .line 2088
    .local v19, "ctrlBitmap1":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v3, 0x7f020317

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v20

    .line 2090
    .local v20, "ctrlBitmap2":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    const/4 v4, 0x1

    if-lt v3, v4, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    const/4 v4, 0x4

    if-le v3, v4, :cond_3

    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-boolean v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->mIsMove:Z

    if-eqz v3, :cond_d

    .line 2092
    :cond_3
    const/4 v3, 0x1

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int v3, v3, v24

    int-to-float v3, v3

    const/4 v4, 0x1

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int v4, v4, v26

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2093
    const/4 v3, 0x2

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int v3, v3, v24

    int-to-float v3, v3

    const/4 v4, 0x2

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int v4, v4, v26

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2094
    const/4 v3, 0x3

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int v3, v3, v24

    int-to-float v3, v3

    const/4 v4, 0x3

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int v4, v4, v26

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2095
    const/4 v3, 0x4

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int v3, v3, v24

    int-to-float v3, v3

    const/4 v4, 0x4

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int v4, v4, v26

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2104
    :goto_2
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Bitmap;->recycle()V

    .line 2105
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Bitmap;->recycle()V

    .line 2107
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v3, 0x7f020315

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v29

    .line 2108
    .local v29, "midBitmap1":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    if-eqz v3, :cond_4

    .line 2109
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    move-object/from16 v0, v29

    invoke-static {v0, v3}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v29

    .line 2110
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v3, 0x7f020316

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v30

    .line 2112
    .local v30, "midBitmap2":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    if-eqz v3, :cond_5

    .line 2113
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    move-object/from16 v0, v30

    invoke-static {v0, v3}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v30

    .line 2115
    :cond_5
    invoke-virtual/range {v29 .. v29}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    div-int/lit8 v25, v3, 0x2

    .line 2116
    .local v25, "half_x2":I
    invoke-virtual/range {v29 .. v29}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    div-int/lit8 v27, v3, 0x2

    .line 2118
    .local v27, "half_y2":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/high16 v4, 0x41cc0000    # 25.5f

    invoke-static {v3, v4}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v22

    .line 2120
    .local v22, "dip26":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    const/4 v4, 0x5

    if-lt v3, v4, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    const/16 v4, 0x8

    if-le v3, v4, :cond_7

    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-boolean v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->mIsMove:Z

    if-eqz v3, :cond_e

    .line 2122
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int/2addr v3, v4

    move/from16 v0, v22

    if-le v3, v0, :cond_8

    .line 2124
    const/4 v3, 0x5

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int v3, v3, v25

    int-to-float v3, v3

    const/4 v4, 0x5

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int v4, v4, v27

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2125
    const/4 v3, 0x7

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int v3, v3, v25

    int-to-float v3, v3

    const/4 v4, 0x7

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int v4, v4, v27

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2128
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int/2addr v3, v4

    move/from16 v0, v22

    if-le v3, v0, :cond_9

    .line 2130
    const/4 v3, 0x6

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int v3, v3, v25

    int-to-float v3, v3

    const/4 v4, 0x6

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int v4, v4, v27

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2131
    const/16 v3, 0x8

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int v3, v3, v25

    int-to-float v3, v3

    const/16 v4, 0x8

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int v4, v4, v27

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2149
    :cond_9
    :goto_3
    invoke-virtual/range {v29 .. v29}, Landroid/graphics/Bitmap;->recycle()V

    .line 2150
    invoke-virtual/range {v30 .. v30}, Landroid/graphics/Bitmap;->recycle()V

    .line 2152
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->bRotationEnabled:I

    if-eqz v3, :cond_a

    .line 2153
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    const/16 v4, 0x9

    if-ne v3, v4, :cond_10

    .line 2154
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v3, 0x7f020313

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v42

    .line 2155
    .local v42, "rotateBitmap2":Landroid/graphics/Bitmap;
    const/16 v3, 0x9

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int v3, v3, v24

    int-to-float v3, v3

    const/16 v4, 0x9

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int v4, v4, v26

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2156
    invoke-virtual/range {v42 .. v42}, Landroid/graphics/Bitmap;->recycle()V

    .line 2166
    .end local v42    # "rotateBitmap2":Landroid/graphics/Bitmap;
    :cond_a
    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nSmartGuidesCnt:I

    if-lez v3, :cond_11

    .line 2169
    new-instance v14, Landroid/graphics/Paint;

    invoke-direct {v14}, Landroid/graphics/Paint;-><init>()V

    .line 2171
    .local v14, "guidesPaint":Landroid/graphics/Paint;
    new-instance v21, Landroid/graphics/DashPathEffect;

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, v21

    invoke-direct {v0, v3, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    .line 2173
    .local v21, "dashPath":Landroid/graphics/DashPathEffect;
    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 2174
    const/16 v3, 0xff

    const/16 v4, 0xd0

    const/16 v5, 0x8a

    const/16 v6, 0x78

    invoke-virtual {v14, v3, v4, v5, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 2175
    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v14, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2176
    const/high16 v3, 0x40400000    # 3.0f

    invoke-virtual {v14, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2177
    const/4 v3, 0x1

    invoke-virtual {v14, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2178
    const/16 v28, 0x0

    .local v28, "i":I
    :goto_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nSmartGuidesCnt:I

    move/from16 v0, v28

    if-ge v0, v3, :cond_11

    .line 2180
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesStart:[Landroid/graphics/Point;

    aget-object v3, v3, v28

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v10, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesStart:[Landroid/graphics/Point;

    aget-object v3, v3, v28

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v11, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesEnd:[Landroid/graphics/Point;

    aget-object v3, v3, v28

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v12, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesEnd:[Landroid/graphics/Point;

    aget-object v3, v3, v28

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v13, v3

    move-object/from16 v9, p1

    invoke-virtual/range {v9 .. v14}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2178
    add-int/lit8 v28, v28, 0x1

    goto :goto_5

    .line 2005
    .end local v8    # "paint1":Landroid/graphics/Paint;
    .end local v14    # "guidesPaint":Landroid/graphics/Paint;
    .end local v19    # "ctrlBitmap1":Landroid/graphics/Bitmap;
    .end local v20    # "ctrlBitmap2":Landroid/graphics/Bitmap;
    .end local v21    # "dashPath":Landroid/graphics/DashPathEffect;
    .end local v22    # "dip26":I
    .end local v24    # "half_x":I
    .end local v25    # "half_x2":I
    .end local v26    # "half_y":I
    .end local v27    # "half_y2":I
    .end local v28    # "i":I
    .end local v29    # "midBitmap1":Landroid/graphics/Bitmap;
    .end local v30    # "midBitmap2":Landroid/graphics/Bitmap;
    .restart local v39    # "paintFill":Landroid/graphics/Paint;
    :cond_b
    const/16 v3, 0x30

    const/16 v4, 0x77

    const/16 v5, 0xb5

    const/16 v6, 0xf0

    move-object/from16 v0, v39

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_0

    .line 2043
    .end local v39    # "paintFill":Landroid/graphics/Paint;
    .restart local v8    # "paint1":Landroid/graphics/Paint;
    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v3, v4}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v36

    .line 2044
    .local v36, "oneDip":F
    const/4 v3, -0x1

    invoke-virtual {v8, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 2045
    const/high16 v3, 0x40400000    # 3.0f

    mul-float v3, v3, v36

    invoke-virtual {v8, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2046
    const/4 v3, 0x1

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    const/4 v4, 0x1

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    const/high16 v5, 0x3fc00000    # 1.5f

    mul-float v5, v5, v36

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2047
    const/4 v3, 0x2

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    const/4 v4, 0x2

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    const/high16 v5, 0x3fc00000    # 1.5f

    mul-float v5, v5, v36

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2048
    const/4 v3, 0x3

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    const/4 v4, 0x3

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    const/high16 v5, 0x3fc00000    # 1.5f

    mul-float v5, v5, v36

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2049
    const/4 v3, 0x4

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    const/4 v4, 0x4

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    const/high16 v5, 0x3fc00000    # 1.5f

    mul-float v5, v5, v36

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2051
    const/4 v3, 0x1

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v4, v3

    const/4 v3, 0x1

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v5, v3

    const/4 v3, 0x2

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v6, v3

    const/4 v3, 0x2

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v7, v3

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2052
    const/4 v3, 0x3

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v4, v3

    const/4 v3, 0x3

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v5, v3

    const/4 v3, 0x4

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v6, v3

    const/4 v3, 0x4

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v7, v3

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2053
    const/4 v3, 0x2

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v4, v3

    const/4 v3, 0x2

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v5, v3

    const/4 v3, 0x3

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v6, v3

    const/4 v3, 0x3

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v7, v3

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2054
    const/4 v3, 0x4

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v4, v3

    const/4 v3, 0x4

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v5, v3

    const/4 v3, 0x1

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v6, v3

    const/4 v3, 0x1

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v7, v3

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2056
    const v3, -0xce8d45

    invoke-virtual {v8, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 2057
    const/high16 v3, 0x40000000    # 2.0f

    mul-float v3, v3, v36

    invoke-virtual {v8, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2059
    const/4 v3, 0x1

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    const/4 v4, 0x1

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v3, v4, v1, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2060
    const/4 v3, 0x2

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    const/4 v4, 0x2

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v3, v4, v1, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2061
    const/4 v3, 0x3

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    const/4 v4, 0x3

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v3, v4, v1, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2062
    const/4 v3, 0x4

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    const/4 v4, 0x4

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v3, v4, v1, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2064
    const/4 v3, 0x1

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v4, v3

    const/4 v3, 0x1

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v5, v3

    const/4 v3, 0x2

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v6, v3

    const/4 v3, 0x2

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v7, v3

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2065
    const/4 v3, 0x3

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v4, v3

    const/4 v3, 0x3

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v5, v3

    const/4 v3, 0x4

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v6, v3

    const/4 v3, 0x4

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v7, v3

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2066
    const/4 v3, 0x2

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v4, v3

    const/4 v3, 0x2

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v5, v3

    const/4 v3, 0x3

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v6, v3

    const/4 v3, 0x3

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v7, v3

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2067
    const/4 v3, 0x4

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v4, v3

    const/4 v3, 0x4

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v5, v3

    const/4 v3, 0x1

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v6, v3

    const/4 v3, 0x1

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v7, v3

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 2099
    .end local v36    # "oneDip":F
    .restart local v19    # "ctrlBitmap1":Landroid/graphics/Bitmap;
    .restart local v20    # "ctrlBitmap2":Landroid/graphics/Bitmap;
    .restart local v24    # "half_x":I
    .restart local v26    # "half_y":I
    :cond_d
    const/4 v3, 0x1

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int v3, v3, v24

    int-to-float v3, v3

    const/4 v4, 0x1

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int v4, v4, v26

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2100
    const/4 v3, 0x2

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int v3, v3, v24

    int-to-float v3, v3

    const/4 v4, 0x2

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int v4, v4, v26

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2101
    const/4 v3, 0x3

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int v3, v3, v24

    int-to-float v3, v3

    const/4 v4, 0x3

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int v4, v4, v26

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2102
    const/4 v3, 0x4

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int v3, v3, v24

    int-to-float v3, v3

    const/4 v4, 0x4

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int v4, v4, v26

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 2136
    .restart local v22    # "dip26":I
    .restart local v25    # "half_x2":I
    .restart local v27    # "half_y2":I
    .restart local v29    # "midBitmap1":Landroid/graphics/Bitmap;
    .restart local v30    # "midBitmap2":Landroid/graphics/Bitmap;
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int/2addr v3, v4

    move/from16 v0, v22

    if-le v3, v0, :cond_f

    .line 2138
    const/4 v3, 0x5

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int v3, v3, v25

    int-to-float v3, v3

    const/4 v4, 0x5

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int v4, v4, v27

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2139
    const/4 v3, 0x7

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int v3, v3, v25

    int-to-float v3, v3

    const/4 v4, 0x7

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int v4, v4, v27

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2142
    :cond_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int/2addr v3, v4

    move/from16 v0, v22

    if-le v3, v0, :cond_9

    .line 2144
    const/4 v3, 0x6

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int v3, v3, v25

    int-to-float v3, v3

    const/4 v4, 0x6

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int v4, v4, v27

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2145
    const/16 v3, 0x8

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int v3, v3, v25

    int-to-float v3, v3

    const/16 v4, 0x8

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int v4, v4, v27

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 2160
    :cond_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v3, 0x7f020312

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v41

    .line 2161
    .local v41, "rotateBitmap1":Landroid/graphics/Bitmap;
    const/16 v3, 0x9

    aget-object v3, v37, v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int v3, v3, v24

    int-to-float v3, v3

    const/16 v4, 0x9

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int v4, v4, v26

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2162
    invoke-virtual/range {v41 .. v41}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_4

    .line 2193
    .end local v41    # "rotateBitmap1":Landroid/graphics/Bitmap;
    :cond_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nAdjustHandleCnt:I

    if-lez v3, :cond_15

    .line 2195
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v3, 0x7f02031a

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v17

    .line 2196
    .local v17, "adjustHandleNormalBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v3, 0x7f02031b

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v18

    .line 2198
    .local v18, "adjustHandlePresseBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    if-eqz v3, :cond_12

    .line 2199
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    move-object/from16 v0, v17

    invoke-static {v0, v3}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v17

    .line 2200
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    move-object/from16 v0, v18

    invoke-static {v0, v3}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v18

    .line 2203
    :cond_12
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    div-int/lit8 v24, v3, 0x2

    .line 2204
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    div-int/lit8 v26, v3, 0x2

    .line 2206
    const/16 v28, 0x0

    .restart local v28    # "i":I
    :goto_6
    const/16 v3, 0xa

    move/from16 v0, v28

    if-ge v0, v3, :cond_14

    .line 2207
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    const/16 v4, 0xa

    if-lt v3, v4, :cond_13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    const/16 v4, 0x14

    if-gt v3, v4, :cond_13

    .line 2209
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    aget-object v3, v3, v28

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int v3, v3, v24

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    aget-object v4, v4, v28

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int v4, v4, v26

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2206
    :goto_7
    add-int/lit8 v28, v28, 0x1

    goto :goto_6

    .line 2213
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    aget-object v3, v3, v28

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int v3, v3, v24

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    aget-object v4, v4, v28

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int v4, v4, v26

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_7

    .line 2218
    :cond_14
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Bitmap;->recycle()V

    .line 2220
    .end local v17    # "adjustHandleNormalBitmap":Landroid/graphics/Bitmap;
    .end local v18    # "adjustHandlePresseBitmap":Landroid/graphics/Bitmap;
    .end local v28    # "i":I
    :cond_15
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nAnimationOrderCnt:I

    if-lez v3, :cond_1b

    .line 2222
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v3, 0x7f020091

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 2223
    .local v16, "AnimtionNormalBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v3, 0x7f020092

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 2224
    .local v15, "AnimtionAddBitmap":Landroid/graphics/Bitmap;
    new-instance v38, Landroid/graphics/Paint;

    invoke-direct/range {v38 .. v38}, Landroid/graphics/Paint;-><init>()V

    .line 2225
    .local v38, "paint2":Landroid/graphics/Paint;
    const/16 v3, 0xa

    new-array v0, v3, [I

    move-object/from16 v35, v0

    .line 2226
    .local v35, "ntemp":[I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x41300000    # 11.0f

    invoke-static {v3, v4}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v3

    float-to-int v0, v3

    move/from16 v33, v0

    .line 2227
    .local v33, "nMargine":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const/high16 v5, 0x41480000    # 12.5f

    invoke-static {v4, v5}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v4

    float-to-int v4, v4

    sub-int/2addr v3, v4

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    sub-int v31, v3, v4

    .line 2228
    .local v31, "nBarX":I
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    add-int v32, v3, v33

    .line 2229
    .local v32, "nBarY":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x41400000    # 12.0f

    invoke-static {v3, v4}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v3

    float-to-int v0, v3

    move/from16 v34, v0

    .line 2230
    .local v34, "nText":I
    new-instance v43, Landroid/graphics/Rect;

    invoke-direct/range {v43 .. v43}, Landroid/graphics/Rect;-><init>()V

    .line 2231
    .local v43, "rt":Landroid/graphics/Rect;
    move/from16 v0, v34

    int-to-float v3, v0

    move-object/from16 v0, v38

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2232
    sget-object v3, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    move-object/from16 v0, v38

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 2233
    const v3, -0xd7d7d8

    move-object/from16 v0, v38

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 2234
    const-string/jumbo v3, "1234567890"

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object/from16 v0, v38

    move-object/from16 v1, v43

    invoke-virtual {v0, v3, v4, v5, v1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 2235
    move-object/from16 v0, v43

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, v43

    iget v4, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    div-int/lit8 v40, v3, 0x2

    .line 2237
    .local v40, "rMargine":I
    const/16 v28, 0x0

    .restart local v28    # "i":I
    :goto_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nAnimationOrderCnt:I

    move/from16 v0, v28

    if-ge v0, v3, :cond_16

    .line 2239
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAnimationOrder:[I

    aget v3, v3, v28

    aget v4, v35, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v35, v3

    .line 2237
    add-int/lit8 v28, v28, 0x1

    goto :goto_8

    .line 2241
    :cond_16
    const/16 v28, 0x0

    :goto_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nAnimationOrderCnt:I

    move/from16 v0, v28

    if-ge v0, v3, :cond_1a

    .line 2243
    add-int/lit8 v3, v28, 0x1

    mul-int v3, v3, v32

    sub-int v3, v3, v33

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int/2addr v4, v5

    if-gt v3, v4, :cond_19

    .line 2245
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAnimationOrder:[I

    aget v3, v3, v28

    aget v3, v35, v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_18

    .line 2246
    move/from16 v0, v31

    int-to-float v3, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    mul-int v5, v32, v28

    add-int/2addr v4, v5

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2247
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAnimationOrder:[I

    add-int/lit8 v4, v28, 0x1

    aget v3, v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int v4, v4, v31

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    mul-int v6, v32, v28

    add-int/2addr v5, v6

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    add-int v5, v5, v40

    int-to-float v5, v5

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v3, v4, v5, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 2241
    :cond_17
    :goto_a
    add-int/lit8 v28, v28, 0x1

    goto :goto_9

    .line 2249
    :cond_18
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAnimationOrder:[I

    aget v3, v3, v28

    aget v3, v35, v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_17

    .line 2250
    move/from16 v0, v31

    int-to-float v3, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    mul-int v5, v32, v28

    add-int/2addr v4, v5

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2251
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAnimationOrder:[I

    add-int/lit8 v4, v28, 0x1

    aget v3, v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int v4, v4, v31

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    mul-int v6, v32, v28

    add-int/2addr v5, v6

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    add-int v5, v5, v40

    int-to-float v5, v5

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v3, v4, v5, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_a

    .line 2255
    :cond_19
    move/from16 v0, v31

    int-to-float v3, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    add-int/lit8 v5, v28, -0x1

    mul-int v5, v5, v32

    add-int/2addr v4, v5

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2256
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAnimationOrder:[I

    aget v3, v3, v28

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int v4, v4, v31

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    add-int/lit8 v6, v28, -0x1

    mul-int v6, v6, v32

    add-int/2addr v5, v6

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    add-int v5, v5, v40

    int-to-float v5, v5

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v3, v4, v5, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 2260
    :cond_1a
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->recycle()V

    .line 2261
    invoke-virtual {v15}, Landroid/graphics/Bitmap;->recycle()V

    .line 2267
    .end local v15    # "AnimtionAddBitmap":Landroid/graphics/Bitmap;
    .end local v16    # "AnimtionNormalBitmap":Landroid/graphics/Bitmap;
    .end local v28    # "i":I
    .end local v31    # "nBarX":I
    .end local v32    # "nBarY":I
    .end local v33    # "nMargine":I
    .end local v34    # "nText":I
    .end local v35    # "ntemp":[I
    .end local v38    # "paint2":Landroid/graphics/Paint;
    .end local v40    # "rMargine":I
    .end local v43    # "rt":Landroid/graphics/Rect;
    :cond_1b
    return-void

    .line 2171
    nop

    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40000000    # 2.0f
    .end array-data
.end method

.method private drawBaseLine(Landroid/graphics/Canvas;Landroid/content/res/Resources;)V
    .locals 28
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 1578
    const v24, 0x7f020314

    move-object/from16 v0, p2

    move/from16 v1, v24

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 1579
    .local v16, "normalBitmap":Landroid/graphics/Bitmap;
    const v24, 0x7f020317

    move-object/from16 v0, p2

    move/from16 v1, v24

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v23

    .line 1581
    .local v23, "selectBitmap":Landroid/graphics/Bitmap;
    new-instance v20, Landroid/graphics/Point;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/Point;-><init>()V

    .line 1582
    .local v20, "pt_sta":Landroid/graphics/Point;
    new-instance v19, Landroid/graphics/Point;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Point;-><init>()V

    .line 1584
    .local v19, "pt_end":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->eEditing:I

    move/from16 v24, v0

    if-nez v24, :cond_1

    .line 1585
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->startPoint:Landroid/graphics/Point;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->startPoint:Landroid/graphics/Point;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    move-object/from16 v0, v20

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 1586
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->endPoint:Landroid/graphics/Point;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->endPoint:Landroid/graphics/Point;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    move-object/from16 v0, v19

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 1603
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    move/from16 v24, v0

    const/16 v25, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_2

    .line 1604
    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->pointImageSize:I

    move/from16 v25, v0

    div-int/lit8 v25, v25, 0x2

    sub-int v24, v24, v25

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->pointImageSize:I

    move/from16 v26, v0

    div-int/lit8 v26, v26, 0x2

    sub-int v25, v25, v26

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    move-object/from16 v4, v26

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1608
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    move/from16 v24, v0

    const/16 v25, 0x2

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_3

    .line 1609
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->pointImageSize:I

    move/from16 v25, v0

    div-int/lit8 v25, v25, 0x2

    sub-int v24, v24, v25

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->pointImageSize:I

    move/from16 v26, v0

    div-int/lit8 v26, v26, 0x2

    sub-int v25, v25, v26

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    move-object/from16 v4, v26

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1613
    :goto_2
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->recycle()V

    .line 1614
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->recycle()V

    .line 1618
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->nAdjustHandleCnt:I

    move/from16 v24, v0

    if-lez v24, :cond_6

    .line 1620
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v24, 0x7f02031a

    move-object/from16 v0, p2

    move/from16 v1, v24

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 1621
    .local v7, "adjustHandleNormalBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v24, 0x7f02031b

    move-object/from16 v0, p2

    move/from16 v1, v24

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 1623
    .local v8, "adjustHandlePresseBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->rotateAngle:I

    move/from16 v24, v0

    if-eqz v24, :cond_0

    .line 1624
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    move/from16 v24, v0

    move/from16 v0, v24

    invoke-static {v7, v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 1625
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    move/from16 v24, v0

    move/from16 v0, v24

    invoke-static {v8, v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 1628
    :cond_0
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v24

    div-int/lit8 v9, v24, 0x2

    .line 1629
    .local v9, "half_x":I
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v24

    div-int/lit8 v10, v24, 0x2

    .line 1631
    .local v10, "half_y":I
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->nAdjustHandleCnt:I

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v11, v0, :cond_5

    .line 1632
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v25, v0

    const/16 v25, 0xa

    move/from16 v0, v24

    move/from16 v1, v25

    if-lt v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v25, v0

    const/16 v25, 0x14

    move/from16 v0, v24

    move/from16 v1, v25

    if-gt v0, v1, :cond_4

    .line 1634
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    move-object/from16 v24, v0

    aget-object v24, v24, v11

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v24, v0

    sub-int v24, v24, v9

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    move-object/from16 v25, v0

    aget-object v25, v25, v11

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    sub-int v25, v25, v10

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v25

    move-object/from16 v3, v26

    invoke-virtual {v0, v8, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1631
    :goto_4
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_3

    .line 1589
    .end local v7    # "adjustHandleNormalBitmap":Landroid/graphics/Bitmap;
    .end local v8    # "adjustHandlePresseBitmap":Landroid/graphics/Bitmap;
    .end local v9    # "half_x":I
    .end local v10    # "half_y":I
    .end local v11    # "i":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->startEditingPoint:Landroid/graphics/Point;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->startEditingPoint:Landroid/graphics/Point;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    move-object/from16 v0, v20

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 1590
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->endEditingPoint:Landroid/graphics/Point;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->endEditingPoint:Landroid/graphics/Point;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    move-object/from16 v0, v19

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_0

    .line 1606
    :cond_2
    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->pointImageSize:I

    move/from16 v25, v0

    div-int/lit8 v25, v25, 0x2

    sub-int v24, v24, v25

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->pointImageSize:I

    move/from16 v26, v0

    div-int/lit8 v26, v26, 0x2

    sub-int v25, v25, v26

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v24

    move/from16 v3, v25

    move-object/from16 v4, v26

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 1611
    :cond_3
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->pointImageSize:I

    move/from16 v25, v0

    div-int/lit8 v25, v25, 0x2

    sub-int v24, v24, v25

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->pointImageSize:I

    move/from16 v26, v0

    div-int/lit8 v26, v26, 0x2

    sub-int v25, v25, v26

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v24

    move/from16 v3, v25

    move-object/from16 v4, v26

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 1637
    .restart local v7    # "adjustHandleNormalBitmap":Landroid/graphics/Bitmap;
    .restart local v8    # "adjustHandlePresseBitmap":Landroid/graphics/Bitmap;
    .restart local v9    # "half_x":I
    .restart local v10    # "half_y":I
    .restart local v11    # "i":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    move-object/from16 v24, v0

    aget-object v24, v24, v11

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v24, v0

    sub-int v24, v24, v9

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    move-object/from16 v25, v0

    aget-object v25, v25, v11

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    sub-int v25, v25, v10

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v25

    move-object/from16 v3, v26

    invoke-virtual {v0, v7, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_4

    .line 1641
    :cond_5
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    .line 1642
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    .line 1644
    .end local v7    # "adjustHandleNormalBitmap":Landroid/graphics/Bitmap;
    .end local v8    # "adjustHandlePresseBitmap":Landroid/graphics/Bitmap;
    .end local v9    # "half_x":I
    .end local v10    # "half_y":I
    .end local v11    # "i":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nAnimationOrderCnt:I

    move/from16 v24, v0

    if-lez v24, :cond_c

    .line 1646
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v24, 0x7f020091

    move-object/from16 v0, p2

    move/from16 v1, v24

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 1647
    .local v6, "AnimtionNormalBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v24, 0x7f020092

    move-object/from16 v0, p2

    move/from16 v1, v24

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 1648
    .local v5, "AnimtionAddBitmap":Landroid/graphics/Bitmap;
    new-instance v18, Landroid/graphics/Paint;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/Paint;-><init>()V

    .line 1649
    .local v18, "paint2":Landroid/graphics/Paint;
    const/16 v24, 0xa

    move/from16 v0, v24

    new-array v0, v0, [I

    move-object/from16 v17, v0

    .line 1650
    .local v17, "ntemp":[I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mView:Landroid/view/View;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v24

    const/high16 v25, 0x41300000    # 11.0f

    invoke-static/range {v24 .. v25}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v24

    move/from16 v0, v24

    float-to-int v14, v0

    .line 1651
    .local v14, "nMargine":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mView:Landroid/view/View;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v25

    const/high16 v26, 0x41480000    # 12.5f

    invoke-static/range {v25 .. v26}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    sub-int v24, v24, v25

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v25

    sub-int v12, v24, v25

    .line 1652
    .local v12, "nBarX":I
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v24

    add-int v13, v24, v14

    .line 1653
    .local v13, "nBarY":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mView:Landroid/view/View;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v24

    const/high16 v25, 0x41400000    # 12.0f

    invoke-static/range {v24 .. v25}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v24

    move/from16 v0, v24

    float-to-int v15, v0

    .line 1654
    .local v15, "nText":I
    new-instance v22, Landroid/graphics/Rect;

    invoke-direct/range {v22 .. v22}, Landroid/graphics/Rect;-><init>()V

    .line 1655
    .local v22, "rt":Landroid/graphics/Rect;
    int-to-float v0, v15

    move/from16 v24, v0

    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1656
    sget-object v24, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    move-object/from16 v0, v18

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 1657
    const v24, -0xd7d7d8

    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1658
    const-string/jumbo v24, "1234567890"

    const/16 v25, 0x0

    const/16 v26, 0x1

    move-object/from16 v0, v18

    move-object/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    move-object/from16 v4, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1659
    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v25, v0

    sub-int v24, v24, v25

    div-int/lit8 v21, v24, 0x2

    .line 1661
    .local v21, "rMargine":I
    const/4 v11, 0x0

    .restart local v11    # "i":I
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nAnimationOrderCnt:I

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v11, v0, :cond_7

    .line 1663
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAnimationOrder:[I

    move-object/from16 v24, v0

    aget v24, v24, v11

    aget v25, v17, v24

    add-int/lit8 v25, v25, 0x1

    aput v25, v17, v24

    .line 1661
    add-int/lit8 v11, v11, 0x1

    goto :goto_5

    .line 1665
    :cond_7
    const/4 v11, 0x0

    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nAnimationOrderCnt:I

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v11, v0, :cond_b

    .line 1667
    add-int/lit8 v24, v11, 0x1

    mul-int v24, v24, v13

    sub-int v24, v24, v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v26, v0

    sub-int v25, v25, v26

    move/from16 v0, v24

    move/from16 v1, v25

    if-gt v0, v1, :cond_a

    .line 1669
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAnimationOrder:[I

    move-object/from16 v24, v0

    aget v24, v24, v11

    aget v24, v17, v24

    const/16 v25, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_9

    .line 1670
    int-to-float v0, v12

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    mul-int v26, v13, v11

    add-int v25, v25, v26

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v25

    move-object/from16 v3, v26

    invoke-virtual {v0, v5, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1671
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAnimationOrder:[I

    move-object/from16 v24, v0

    add-int/lit8 v25, v11, 0x1

    aget v24, v24, v25

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v25

    div-int/lit8 v25, v25, 0x2

    add-int v25, v25, v12

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v26, v0

    mul-int v27, v13, v11

    add-int v26, v26, v27

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v27

    div-int/lit8 v27, v27, 0x2

    add-int v26, v26, v27

    add-int v26, v26, v21

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    move-object/from16 v4, v18

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1665
    :cond_8
    :goto_7
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_6

    .line 1673
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAnimationOrder:[I

    move-object/from16 v24, v0

    aget v24, v24, v11

    aget v24, v17, v24

    const/16 v25, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_8

    .line 1674
    int-to-float v0, v12

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    mul-int v26, v13, v11

    add-int v25, v25, v26

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v25

    move-object/from16 v3, v26

    invoke-virtual {v0, v6, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1675
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAnimationOrder:[I

    move-object/from16 v24, v0

    add-int/lit8 v25, v11, 0x1

    aget v24, v24, v25

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v25

    div-int/lit8 v25, v25, 0x2

    add-int v25, v25, v12

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v26, v0

    mul-int v27, v13, v11

    add-int v26, v26, v27

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v27

    div-int/lit8 v27, v27, 0x2

    add-int v26, v26, v27

    add-int v26, v26, v21

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    move-object/from16 v4, v18

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_7

    .line 1679
    :cond_a
    int-to-float v0, v12

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    add-int/lit8 v26, v11, -0x1

    mul-int v26, v26, v13

    add-int v25, v25, v26

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v25

    move-object/from16 v3, v26

    invoke-virtual {v0, v5, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1680
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAnimationOrder:[I

    move-object/from16 v24, v0

    aget v24, v24, v11

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v25

    div-int/lit8 v25, v25, 0x2

    add-int v25, v25, v12

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v26, v0

    add-int/lit8 v27, v11, -0x1

    mul-int v27, v27, v13

    add-int v26, v26, v27

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v27

    div-int/lit8 v27, v27, 0x2

    add-int v26, v26, v27

    add-int v26, v26, v21

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    move-object/from16 v4, v18

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1684
    :cond_b
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    .line 1685
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    .line 1689
    .end local v5    # "AnimtionAddBitmap":Landroid/graphics/Bitmap;
    .end local v6    # "AnimtionNormalBitmap":Landroid/graphics/Bitmap;
    .end local v11    # "i":I
    .end local v12    # "nBarX":I
    .end local v13    # "nBarY":I
    .end local v14    # "nMargine":I
    .end local v15    # "nText":I
    .end local v17    # "ntemp":[I
    .end local v18    # "paint2":Landroid/graphics/Paint;
    .end local v21    # "rMargine":I
    .end local v22    # "rt":Landroid/graphics/Rect;
    :cond_c
    return-void
.end method

.method private drawBaseMulti(Landroid/graphics/Canvas;Landroid/content/res/Resources;)V
    .locals 40
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 1345
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->nObjectCount:I

    move/from16 v0, v17

    if-ge v0, v3, :cond_2

    .line 1348
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v3, v3, v17

    iget v0, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->mObjectType:I

    move/from16 v20, v0

    .line 1349
    .local v20, "nObjectType":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->eEditing:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 1351
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v3, v3, v17

    iget-object v0, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->editStartPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    .line 1352
    .local v39, "startPoint":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v3, v3, v17

    iget-object v11, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->editEndPoint:Landroid/graphics/Point;

    .line 1360
    .local v11, "endPoint":Landroid/graphics/Point;
    :goto_1
    const/16 v21, 0x0

    .line 1361
    .local v21, "nRotate":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->eEditing:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_4

    .line 1362
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v3, v3, v17

    iget v0, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->editRotateAngle:I

    move/from16 v21, v0

    .line 1366
    :goto_2
    new-instance v23, Landroid/graphics/PointF;

    move-object/from16 v0, v39

    iget v3, v0, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    move-object/from16 v0, v39

    iget v4, v0, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    move-object/from16 v0, v23

    invoke-direct {v0, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1367
    .local v23, "p0":Landroid/graphics/PointF;
    new-instance v24, Landroid/graphics/PointF;

    iget v3, v11, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    move-object/from16 v0, v39

    iget v4, v0, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    move-object/from16 v0, v24

    invoke-direct {v0, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1368
    .local v24, "p1":Landroid/graphics/PointF;
    new-instance v34, Landroid/graphics/PointF;

    iget v3, v11, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    iget v4, v11, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    move-object/from16 v0, v34

    invoke-direct {v0, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1369
    .local v34, "p2":Landroid/graphics/PointF;
    new-instance v35, Landroid/graphics/PointF;

    move-object/from16 v0, v39

    iget v3, v0, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    iget v4, v11, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    move-object/from16 v0, v35

    invoke-direct {v0, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1371
    .local v35, "p3":Landroid/graphics/PointF;
    new-instance v37, Landroid/graphics/PointF;

    move-object/from16 v0, v23

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v24

    iget v4, v0, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, v23

    iget v4, v0, Landroid/graphics/PointF;->y:F

    const/high16 v5, 0x425c0000    # 55.0f

    sub-float/2addr v4, v5

    move-object/from16 v0, v37

    invoke-direct {v0, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1372
    .local v37, "pR":Landroid/graphics/PointF;
    if-eqz v21, :cond_0

    .line 1374
    new-instance v36, Landroid/graphics/PointF;

    move-object/from16 v0, v23

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v24

    iget v4, v0, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, v24

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v34

    iget v5, v0, Landroid/graphics/PointF;->y:F

    add-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    move-object/from16 v0, v36

    invoke-direct {v0, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1375
    .local v36, "pC":Landroid/graphics/PointF;
    move-object/from16 v0, v23

    move-object/from16 v1, v36

    move/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRotatePoint(Landroid/graphics/PointF;Landroid/graphics/PointF;I)Landroid/graphics/PointF;

    move-result-object v26

    .line 1376
    .local v26, "p100":Landroid/graphics/PointF;
    move-object/from16 v0, v24

    move-object/from16 v1, v36

    move/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRotatePoint(Landroid/graphics/PointF;Landroid/graphics/PointF;I)Landroid/graphics/PointF;

    move-result-object v27

    .line 1377
    .local v27, "p101":Landroid/graphics/PointF;
    move-object/from16 v0, v34

    move-object/from16 v1, v36

    move/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRotatePoint(Landroid/graphics/PointF;Landroid/graphics/PointF;I)Landroid/graphics/PointF;

    move-result-object v28

    .line 1378
    .local v28, "p102":Landroid/graphics/PointF;
    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRotatePoint(Landroid/graphics/PointF;Landroid/graphics/PointF;I)Landroid/graphics/PointF;

    move-result-object v29

    .line 1379
    .local v29, "p103":Landroid/graphics/PointF;
    move-object/from16 v0, v37

    move-object/from16 v1, v36

    move/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRotatePoint(Landroid/graphics/PointF;Landroid/graphics/PointF;I)Landroid/graphics/PointF;

    move-result-object v30

    .line 1381
    .local v30, "p109":Landroid/graphics/PointF;
    move-object/from16 v23, v26

    .line 1382
    move-object/from16 v24, v27

    .line 1383
    move-object/from16 v34, v28

    .line 1384
    move-object/from16 v35, v29

    .line 1385
    move-object/from16 v37, v30

    .line 1388
    .end local v26    # "p100":Landroid/graphics/PointF;
    .end local v27    # "p101":Landroid/graphics/PointF;
    .end local v28    # "p102":Landroid/graphics/PointF;
    .end local v29    # "p103":Landroid/graphics/PointF;
    .end local v30    # "p109":Landroid/graphics/PointF;
    .end local v36    # "pC":Landroid/graphics/PointF;
    :cond_0
    new-instance v25, Landroid/graphics/PointF;

    move-object/from16 v0, v23

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v24

    iget v4, v0, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, v23

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v24

    iget v5, v0, Landroid/graphics/PointF;->y:F

    add-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-direct {v0, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1389
    .local v25, "p10":Landroid/graphics/PointF;
    new-instance v31, Landroid/graphics/PointF;

    move-object/from16 v0, v24

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v34

    iget v4, v0, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, v24

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v34

    iget v5, v0, Landroid/graphics/PointF;->y:F

    add-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    move-object/from16 v0, v31

    invoke-direct {v0, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1390
    .local v31, "p11":Landroid/graphics/PointF;
    new-instance v32, Landroid/graphics/PointF;

    move-object/from16 v0, v34

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v35

    iget v4, v0, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, v34

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v35

    iget v5, v0, Landroid/graphics/PointF;->y:F

    add-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    move-object/from16 v0, v32

    invoke-direct {v0, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1391
    .local v32, "p12":Landroid/graphics/PointF;
    new-instance v33, Landroid/graphics/PointF;

    move-object/from16 v0, v35

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v23

    iget v4, v0, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, v35

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v23

    iget v5, v0, Landroid/graphics/PointF;->y:F

    add-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    move-object/from16 v0, v33

    invoke-direct {v0, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1395
    .local v33, "p13":Landroid/graphics/PointF;
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_TEXTMARK_REVERSE()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1398
    new-instance v38, Landroid/graphics/Paint;

    invoke-direct/range {v38 .. v38}, Landroid/graphics/Paint;-><init>()V

    .line 1399
    .local v38, "paintFill":Landroid/graphics/Paint;
    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v38

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1401
    const/4 v3, 0x5

    move/from16 v0, v20

    if-ne v0, v3, :cond_5

    .line 1402
    const/16 v3, 0x40

    const/16 v4, 0x77

    const/16 v5, 0xb5

    const/16 v6, 0xf0

    move-object/from16 v0, v38

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 1407
    :goto_3
    new-instance v12, Landroid/graphics/Path;

    invoke-direct {v12}, Landroid/graphics/Path;-><init>()V

    .line 1408
    .local v12, "fillPath":Landroid/graphics/Path;
    sget-object v3, Landroid/graphics/Path$FillType;->WINDING:Landroid/graphics/Path$FillType;

    invoke-virtual {v12, v3}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 1409
    move-object/from16 v0, v23

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v23

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v12, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1410
    move-object/from16 v0, v24

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v24

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v12, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1411
    move-object/from16 v0, v34

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v34

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v12, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1412
    move-object/from16 v0, v35

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v35

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v12, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1413
    move-object/from16 v0, v23

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v23

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v12, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1414
    invoke-virtual {v12}, Landroid/graphics/Path;->close()V

    .line 1416
    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v12, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1418
    const/16 v38, 0x0

    .line 1424
    .end local v12    # "fillPath":Landroid/graphics/Path;
    .end local v38    # "paintFill":Landroid/graphics/Paint;
    :cond_1
    new-instance v8, Landroid/graphics/Paint;

    invoke-direct {v8}, Landroid/graphics/Paint;-><init>()V

    .line 1426
    .local v8, "paint1":Landroid/graphics/Paint;
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_TEXTMARK_REVERSE()Z

    move-result v3

    if-nez v3, :cond_6

    .line 1428
    const v3, -0x787879

    invoke-virtual {v8, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1429
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v8, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1431
    move-object/from16 v0, v23

    iget v4, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v23

    iget v5, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v24

    iget v6, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v24

    iget v7, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1432
    move-object/from16 v0, v24

    iget v4, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v24

    iget v5, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v34

    iget v6, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v34

    iget v7, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1433
    move-object/from16 v0, v34

    iget v4, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v34

    iget v5, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v35

    iget v6, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v35

    iget v7, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1434
    move-object/from16 v0, v35

    iget v4, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v35

    iget v5, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v23

    iget v6, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v23

    iget v7, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1466
    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->IsViewerMode()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1468
    const/4 v8, 0x0

    .line 1573
    .end local v8    # "paint1":Landroid/graphics/Paint;
    .end local v11    # "endPoint":Landroid/graphics/Point;
    .end local v20    # "nObjectType":I
    .end local v21    # "nRotate":I
    .end local v23    # "p0":Landroid/graphics/PointF;
    .end local v24    # "p1":Landroid/graphics/PointF;
    .end local v25    # "p10":Landroid/graphics/PointF;
    .end local v31    # "p11":Landroid/graphics/PointF;
    .end local v32    # "p12":Landroid/graphics/PointF;
    .end local v33    # "p13":Landroid/graphics/PointF;
    .end local v34    # "p2":Landroid/graphics/PointF;
    .end local v35    # "p3":Landroid/graphics/PointF;
    .end local v37    # "pR":Landroid/graphics/PointF;
    .end local v39    # "startPoint":Landroid/graphics/Point;
    :cond_2
    return-void

    .line 1356
    .restart local v20    # "nObjectType":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v3, v3, v17

    iget-object v0, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    .line 1357
    .restart local v39    # "startPoint":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v3, v3, v17

    iget-object v11, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->endRangePoint:Landroid/graphics/Point;

    .restart local v11    # "endPoint":Landroid/graphics/Point;
    goto/16 :goto_1

    .line 1364
    .restart local v21    # "nRotate":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v3, v3, v17

    iget v0, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->rotateAngle:I

    move/from16 v21, v0

    goto/16 :goto_2

    .line 1405
    .restart local v23    # "p0":Landroid/graphics/PointF;
    .restart local v24    # "p1":Landroid/graphics/PointF;
    .restart local v25    # "p10":Landroid/graphics/PointF;
    .restart local v31    # "p11":Landroid/graphics/PointF;
    .restart local v32    # "p12":Landroid/graphics/PointF;
    .restart local v33    # "p13":Landroid/graphics/PointF;
    .restart local v34    # "p2":Landroid/graphics/PointF;
    .restart local v35    # "p3":Landroid/graphics/PointF;
    .restart local v37    # "pR":Landroid/graphics/PointF;
    .restart local v38    # "paintFill":Landroid/graphics/Paint;
    :cond_5
    const/16 v3, 0x30

    const/16 v4, 0x77

    const/16 v5, 0xb5

    const/16 v6, 0xf0

    move-object/from16 v0, v38

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_3

    .line 1439
    .end local v38    # "paintFill":Landroid/graphics/Paint;
    .restart local v8    # "paint1":Landroid/graphics/Paint;
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v3, v4}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v22

    .line 1440
    .local v22, "oneDip":F
    const/4 v3, -0x1

    invoke-virtual {v8, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1441
    const/high16 v3, 0x40400000    # 3.0f

    mul-float v3, v3, v22

    invoke-virtual {v8, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1442
    move-object/from16 v0, v23

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v23

    iget v4, v0, Landroid/graphics/PointF;->y:F

    const/high16 v5, 0x3fc00000    # 1.5f

    mul-float v5, v5, v22

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1443
    move-object/from16 v0, v24

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v24

    iget v4, v0, Landroid/graphics/PointF;->y:F

    const/high16 v5, 0x3fc00000    # 1.5f

    mul-float v5, v5, v22

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1444
    move-object/from16 v0, v34

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v34

    iget v4, v0, Landroid/graphics/PointF;->y:F

    const/high16 v5, 0x3fc00000    # 1.5f

    mul-float v5, v5, v22

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1445
    move-object/from16 v0, v35

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v35

    iget v4, v0, Landroid/graphics/PointF;->y:F

    const/high16 v5, 0x3fc00000    # 1.5f

    mul-float v5, v5, v22

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1447
    move-object/from16 v0, v23

    iget v4, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v23

    iget v5, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v24

    iget v6, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v24

    iget v7, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1448
    move-object/from16 v0, v34

    iget v4, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v34

    iget v5, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v35

    iget v6, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v35

    iget v7, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1449
    move-object/from16 v0, v24

    iget v4, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v24

    iget v5, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v34

    iget v6, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v34

    iget v7, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1450
    move-object/from16 v0, v35

    iget v4, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v35

    iget v5, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v23

    iget v6, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v23

    iget v7, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1452
    const v3, -0xce8d45

    invoke-virtual {v8, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1453
    const/high16 v3, 0x40000000    # 2.0f

    mul-float v3, v3, v22

    invoke-virtual {v8, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1455
    move-object/from16 v0, v23

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v23

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v3, v4, v1, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1456
    move-object/from16 v0, v24

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v24

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v3, v4, v1, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1457
    move-object/from16 v0, v34

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v34

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v3, v4, v1, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1458
    move-object/from16 v0, v35

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v35

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v3, v4, v1, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1460
    move-object/from16 v0, v23

    iget v4, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v23

    iget v5, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v24

    iget v6, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v24

    iget v7, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1461
    move-object/from16 v0, v34

    iget v4, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v34

    iget v5, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v35

    iget v6, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v35

    iget v7, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1462
    move-object/from16 v0, v24

    iget v4, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v24

    iget v5, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v34

    iget v6, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v34

    iget v7, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1463
    move-object/from16 v0, v35

    iget v4, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v35

    iget v5, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v23

    iget v6, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v23

    iget v7, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_4

    .line 1475
    .end local v22    # "oneDip":F
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v3, v3, v17

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->bRotationEnabled:I

    if-eqz v3, :cond_8

    .line 1477
    const/16 v3, 0x8

    move/from16 v0, v20

    if-eq v0, v3, :cond_8

    const/4 v3, 0x4

    move/from16 v0, v20

    if-eq v0, v3, :cond_8

    .line 1479
    const v3, -0x787879

    invoke-virtual {v8, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1480
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v8, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1481
    move-object/from16 v0, v25

    iget v4, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v25

    iget v5, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v37

    iget v6, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v37

    iget v7, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1486
    :cond_8
    const/4 v8, 0x0

    .line 1490
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->iconSize:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    div-int/lit8 v13, v3, 0x2

    .line 1491
    .local v13, "half_x":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->iconSize:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    div-int/lit8 v15, v3, 0x2

    .line 1492
    .local v15, "half_y":I
    const/4 v9, 0x0

    .line 1493
    .local v9, "ctrlBitmap1":Landroid/graphics/Bitmap;
    const/4 v10, 0x0

    .line 1495
    .local v10, "ctrlBitmap2":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v3, 0x7f020314

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 1496
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v3, 0x7f020317

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 1498
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->eEditing:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_c

    .line 1500
    move-object/from16 v0, v23

    iget v3, v0, Landroid/graphics/PointF;->x:F

    int-to-float v4, v13

    sub-float/2addr v3, v4

    move-object/from16 v0, v23

    iget v4, v0, Landroid/graphics/PointF;->y:F

    int-to-float v5, v15

    sub-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1501
    move-object/from16 v0, v24

    iget v3, v0, Landroid/graphics/PointF;->x:F

    int-to-float v4, v13

    sub-float/2addr v3, v4

    move-object/from16 v0, v24

    iget v4, v0, Landroid/graphics/PointF;->y:F

    int-to-float v5, v15

    sub-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1502
    move-object/from16 v0, v34

    iget v3, v0, Landroid/graphics/PointF;->x:F

    int-to-float v4, v13

    sub-float/2addr v3, v4

    move-object/from16 v0, v34

    iget v4, v0, Landroid/graphics/PointF;->y:F

    int-to-float v5, v15

    sub-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1503
    move-object/from16 v0, v35

    iget v3, v0, Landroid/graphics/PointF;->x:F

    int-to-float v4, v13

    sub-float/2addr v3, v4

    move-object/from16 v0, v35

    iget v4, v0, Landroid/graphics/PointF;->y:F

    int-to-float v5, v15

    sub-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1513
    :goto_5
    invoke-virtual {v9}, Landroid/graphics/Bitmap;->recycle()V

    .line 1514
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->recycle()V

    .line 1519
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v3, 0x7f020315

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v18

    .line 1520
    .local v18, "midBitmap1":Landroid/graphics/Bitmap;
    if-eqz v21, :cond_9

    .line 1521
    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-static {v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v18

    .line 1522
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v3, 0x7f020316

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v19

    .line 1524
    .local v19, "midBitmap2":Landroid/graphics/Bitmap;
    if-eqz v21, :cond_a

    .line 1525
    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-static {v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v19

    .line 1527
    :cond_a
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    div-int/lit8 v14, v3, 0x2

    .line 1528
    .local v14, "half_x2":I
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    div-int/lit8 v16, v3, 0x2

    .line 1530
    .local v16, "half_y2":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->eEditing:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_d

    .line 1532
    move-object/from16 v0, v25

    iget v3, v0, Landroid/graphics/PointF;->x:F

    int-to-float v4, v14

    sub-float/2addr v3, v4

    move-object/from16 v0, v25

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move/from16 v0, v16

    int-to-float v5, v0

    sub-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1533
    move-object/from16 v0, v31

    iget v3, v0, Landroid/graphics/PointF;->x:F

    int-to-float v4, v14

    sub-float/2addr v3, v4

    move-object/from16 v0, v31

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move/from16 v0, v16

    int-to-float v5, v0

    sub-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1534
    move-object/from16 v0, v32

    iget v3, v0, Landroid/graphics/PointF;->x:F

    int-to-float v4, v14

    sub-float/2addr v3, v4

    move-object/from16 v0, v32

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move/from16 v0, v16

    int-to-float v5, v0

    sub-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1535
    move-object/from16 v0, v33

    iget v3, v0, Landroid/graphics/PointF;->x:F

    int-to-float v4, v14

    sub-float/2addr v3, v4

    move-object/from16 v0, v33

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move/from16 v0, v16

    int-to-float v5, v0

    sub-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1545
    :goto_6
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->recycle()V

    .line 1546
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Bitmap;->recycle()V

    .line 1553
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v3, v3, v17

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->bRotationEnabled:I

    if-eqz v3, :cond_b

    .line 1554
    const/16 v3, 0x8

    move/from16 v0, v20

    if-eq v0, v3, :cond_b

    const/4 v3, 0x4

    move/from16 v0, v20

    if-eq v0, v3, :cond_b

    .line 1556
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->eEditing:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_e

    .line 1558
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v3, 0x7f020313

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 1559
    move-object/from16 v0, v37

    iget v3, v0, Landroid/graphics/PointF;->x:F

    int-to-float v4, v13

    sub-float/2addr v3, v4

    move-object/from16 v0, v37

    iget v4, v0, Landroid/graphics/PointF;->y:F

    int-to-float v5, v15

    sub-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1560
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->recycle()V

    .line 1345
    :cond_b
    :goto_7
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_0

    .line 1507
    .end local v14    # "half_x2":I
    .end local v16    # "half_y2":I
    .end local v18    # "midBitmap1":Landroid/graphics/Bitmap;
    .end local v19    # "midBitmap2":Landroid/graphics/Bitmap;
    :cond_c
    move-object/from16 v0, v23

    iget v3, v0, Landroid/graphics/PointF;->x:F

    int-to-float v4, v13

    sub-float/2addr v3, v4

    move-object/from16 v0, v23

    iget v4, v0, Landroid/graphics/PointF;->y:F

    int-to-float v5, v15

    sub-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1508
    move-object/from16 v0, v24

    iget v3, v0, Landroid/graphics/PointF;->x:F

    int-to-float v4, v13

    sub-float/2addr v3, v4

    move-object/from16 v0, v24

    iget v4, v0, Landroid/graphics/PointF;->y:F

    int-to-float v5, v15

    sub-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1509
    move-object/from16 v0, v34

    iget v3, v0, Landroid/graphics/PointF;->x:F

    int-to-float v4, v13

    sub-float/2addr v3, v4

    move-object/from16 v0, v34

    iget v4, v0, Landroid/graphics/PointF;->y:F

    int-to-float v5, v15

    sub-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1510
    move-object/from16 v0, v35

    iget v3, v0, Landroid/graphics/PointF;->x:F

    int-to-float v4, v13

    sub-float/2addr v3, v4

    move-object/from16 v0, v35

    iget v4, v0, Landroid/graphics/PointF;->y:F

    int-to-float v5, v15

    sub-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_5

    .line 1539
    .restart local v14    # "half_x2":I
    .restart local v16    # "half_y2":I
    .restart local v18    # "midBitmap1":Landroid/graphics/Bitmap;
    .restart local v19    # "midBitmap2":Landroid/graphics/Bitmap;
    :cond_d
    move-object/from16 v0, v25

    iget v3, v0, Landroid/graphics/PointF;->x:F

    int-to-float v4, v14

    sub-float/2addr v3, v4

    move-object/from16 v0, v25

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move/from16 v0, v16

    int-to-float v5, v0

    sub-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1540
    move-object/from16 v0, v31

    iget v3, v0, Landroid/graphics/PointF;->x:F

    int-to-float v4, v14

    sub-float/2addr v3, v4

    move-object/from16 v0, v31

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move/from16 v0, v16

    int-to-float v5, v0

    sub-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1541
    move-object/from16 v0, v32

    iget v3, v0, Landroid/graphics/PointF;->x:F

    int-to-float v4, v14

    sub-float/2addr v3, v4

    move-object/from16 v0, v32

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move/from16 v0, v16

    int-to-float v5, v0

    sub-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1542
    move-object/from16 v0, v33

    iget v3, v0, Landroid/graphics/PointF;->x:F

    int-to-float v4, v14

    sub-float/2addr v3, v4

    move-object/from16 v0, v33

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move/from16 v0, v16

    int-to-float v5, v0

    sub-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_6

    .line 1564
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v3, 0x7f020312

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 1565
    move-object/from16 v0, v37

    iget v3, v0, Landroid/graphics/PointF;->x:F

    int-to-float v4, v13

    sub-float/2addr v3, v4

    move-object/from16 v0, v37

    iget v4, v0, Landroid/graphics/PointF;->y:F

    int-to-float v5, v15

    sub-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1566
    invoke-virtual {v9}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_7
.end method

.method private drawBaseRectAngle(Landroid/graphics/Canvas;Landroid/content/res/Resources;)V
    .locals 46
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 1695
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v0, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    move-object/from16 v37, v0

    .line 1697
    .local v37, "p":[Landroid/graphics/Point;
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_TEXTMARK_REVERSE()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1699
    new-instance v39, Landroid/graphics/Paint;

    invoke-direct/range {v39 .. v39}, Landroid/graphics/Paint;-><init>()V

    .line 1700
    .local v39, "paintFill":Landroid/graphics/Paint;
    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1702
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    const/4 v5, 0x5

    if-ne v4, v5, :cond_7

    .line 1703
    const/16 v4, 0x40

    const/16 v5, 0x77

    const/16 v6, 0xb5

    const/16 v7, 0xf0

    move-object/from16 v0, v39

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 1707
    :goto_0
    new-instance v23, Landroid/graphics/Path;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/Path;-><init>()V

    .line 1708
    .local v23, "fillPath":Landroid/graphics/Path;
    sget-object v4, Landroid/graphics/Path$FillType;->WINDING:Landroid/graphics/Path$FillType;

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 1709
    const/4 v4, 0x1

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    const/4 v5, 0x1

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1710
    const/4 v4, 0x2

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    const/4 v5, 0x2

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1711
    const/4 v4, 0x3

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    const/4 v5, 0x3

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1712
    const/4 v4, 0x4

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    const/4 v5, 0x4

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1713
    const/4 v4, 0x1

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    const/4 v5, 0x1

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1714
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Path;->close()V

    .line 1716
    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v39

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1718
    const/16 v39, 0x0

    .line 1725
    .end local v23    # "fillPath":Landroid/graphics/Path;
    .end local v39    # "paintFill":Landroid/graphics/Paint;
    :cond_0
    new-instance v9, Landroid/graphics/Paint;

    invoke-direct {v9}, Landroid/graphics/Paint;-><init>()V

    .line 1727
    .local v9, "paint1":Landroid/graphics/Paint;
    const/16 v44, 0x1

    .line 1728
    .local v44, "strokeHori":I
    const/16 v45, 0x1

    .line 1730
    .local v45, "strokeVert":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v5, 0x159

    if-le v4, v5, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v5, 0x168

    if-lt v4, v5, :cond_4

    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    if-lez v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v5, 0xf

    if-lt v4, v5, :cond_4

    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v5, 0xa5

    if-le v4, v5, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v5, 0xb4

    if-lt v4, v5, :cond_4

    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v5, 0xb4

    if-le v4, v5, :cond_8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v5, 0xc3

    if-ge v4, v5, :cond_8

    .line 1734
    :cond_4
    const/16 v44, 0x2

    .line 1741
    :cond_5
    :goto_1
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_TEXTMARK_REVERSE()Z

    move-result v4

    if-nez v4, :cond_d

    .line 1743
    const v4, -0xe47f39

    invoke-virtual {v9, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 1745
    move/from16 v0, v44

    int-to-float v4, v0

    invoke-virtual {v9, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1746
    const/4 v4, 0x1

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v5, v4

    const/4 v4, 0x1

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v6, v4

    const/4 v4, 0x2

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v7, v4

    const/4 v4, 0x2

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v8, v4

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1747
    const/4 v4, 0x3

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v5, v4

    const/4 v4, 0x3

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v6, v4

    const/4 v4, 0x4

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v7, v4

    const/4 v4, 0x4

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v8, v4

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1749
    move/from16 v0, v45

    int-to-float v4, v0

    invoke-virtual {v9, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1750
    const/4 v4, 0x2

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v5, v4

    const/4 v4, 0x2

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v6, v4

    const/4 v4, 0x3

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v7, v4

    const/4 v4, 0x3

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v8, v4

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1751
    const/4 v4, 0x4

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v5, v4

    const/4 v4, 0x4

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v6, v4

    const/4 v4, 0x1

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v7, v4

    const/4 v4, 0x1

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v8, v4

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1789
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->IsViewerMode()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 1791
    const/4 v9, 0x0

    .line 1793
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    const/16 v5, 0x10

    if-ne v4, v5, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getVideoStatus()I

    move-result v4

    if-nez v4, :cond_6

    .line 1796
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, v37

    invoke-direct {v0, v1, v2, v3}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->drawVideoBtn(Landroid/graphics/Canvas;Landroid/content/res/Resources;[Landroid/graphics/Point;)V

    .line 1989
    :cond_6
    :goto_3
    return-void

    .line 1705
    .end local v9    # "paint1":Landroid/graphics/Paint;
    .end local v44    # "strokeHori":I
    .end local v45    # "strokeVert":I
    .restart local v39    # "paintFill":Landroid/graphics/Paint;
    :cond_7
    const/16 v4, 0x30

    const/16 v5, 0x77

    const/16 v6, 0xb5

    const/16 v7, 0xf0

    move-object/from16 v0, v39

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_0

    .line 1735
    .end local v39    # "paintFill":Landroid/graphics/Paint;
    .restart local v9    # "paint1":Landroid/graphics/Paint;
    .restart local v44    # "strokeHori":I
    .restart local v45    # "strokeVert":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v5, 0x4b

    if-le v4, v5, :cond_9

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v5, 0x5a

    if-lt v4, v5, :cond_c

    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v5, 0x5a

    if-le v4, v5, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v5, 0x69

    if-lt v4, v5, :cond_c

    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v5, 0xff

    if-le v4, v5, :cond_b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v5, 0x10e

    if-lt v4, v5, :cond_c

    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v5, 0x10e

    if-le v4, v5, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v5, 0x11d

    if-ge v4, v5, :cond_5

    .line 1739
    :cond_c
    const/16 v45, 0x2

    goto/16 :goto_1

    .line 1756
    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static {v4, v5}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v36

    .line 1773
    .local v36, "oneDip":F
    const v4, -0xce8d45

    invoke-virtual {v9, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 1774
    const/high16 v4, 0x40000000    # 2.0f

    mul-float v4, v4, v36

    invoke-virtual {v9, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1776
    const/4 v4, 0x1

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    const/4 v5, 0x1

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v4, v5, v1, v9}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1777
    const/4 v4, 0x2

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    const/4 v5, 0x2

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v4, v5, v1, v9}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1778
    const/4 v4, 0x3

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    const/4 v5, 0x3

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v4, v5, v1, v9}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1779
    const/4 v4, 0x4

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    const/4 v5, 0x4

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v4, v5, v1, v9}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1781
    const/4 v4, 0x1

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v5, v4

    const/4 v4, 0x1

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v6, v4

    const/4 v4, 0x2

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v7, v4

    const/4 v4, 0x2

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v8, v4

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1782
    const/4 v4, 0x3

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v5, v4

    const/4 v4, 0x3

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v6, v4

    const/4 v4, 0x4

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v7, v4

    const/4 v4, 0x4

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v8, v4

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1783
    const/4 v4, 0x2

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v5, v4

    const/4 v4, 0x2

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v6, v4

    const/4 v4, 0x3

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v7, v4

    const/4 v4, 0x3

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v8, v4

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1784
    const/4 v4, 0x4

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v5, v4

    const/4 v4, 0x4

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v6, v4

    const/4 v4, 0x1

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v7, v4

    const/4 v4, 0x1

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v8, v4

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 1801
    .end local v36    # "oneDip":F
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->bRotationEnabled:I

    if-eqz v4, :cond_f

    .line 1803
    const v4, -0x787879

    invoke-virtual {v9, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 1804
    move/from16 v0, v45

    int-to-float v4, v0

    invoke-virtual {v9, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1805
    const/4 v4, 0x5

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v5, v4

    const/4 v4, 0x5

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v6, v4

    const/16 v4, 0x9

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v7, v4

    const/16 v4, 0x9

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v8, v4

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1808
    :cond_f
    const/4 v9, 0x0

    .line 1815
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->iconSize:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    div-int/lit8 v24, v4, 0x2

    .line 1816
    .local v24, "half_x":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->iconSize:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    div-int/lit8 v26, v4, 0x2

    .line 1821
    .local v26, "half_y":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v4, 0x7f020314

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v20

    .line 1822
    .local v20, "ctrlBitmap1":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v4, 0x7f020317

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 1824
    .local v21, "ctrlBitmap2":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    const/4 v5, 0x1

    if-lt v4, v5, :cond_10

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    const/4 v5, 0x4

    if-le v4, v5, :cond_11

    :cond_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-boolean v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->mIsMove:Z

    if-eqz v4, :cond_17

    .line 1826
    :cond_11
    const/4 v4, 0x1

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int v4, v4, v24

    int-to-float v4, v4

    const/4 v5, 0x1

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int v5, v5, v26

    int-to-float v5, v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1827
    const/4 v4, 0x2

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int v4, v4, v24

    int-to-float v4, v4

    const/4 v5, 0x2

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int v5, v5, v26

    int-to-float v5, v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1828
    const/4 v4, 0x3

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int v4, v4, v24

    int-to-float v4, v4

    const/4 v5, 0x3

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int v5, v5, v26

    int-to-float v5, v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1829
    const/4 v4, 0x4

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int v4, v4, v24

    int-to-float v4, v4

    const/4 v5, 0x4

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int v5, v5, v26

    int-to-float v5, v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1839
    :goto_4
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Bitmap;->recycle()V

    .line 1840
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->recycle()V

    .line 1844
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v4, 0x7f020315

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v29

    .line 1845
    .local v29, "midBitmap1":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    if-eqz v4, :cond_12

    .line 1846
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    move-object/from16 v0, v29

    invoke-static {v0, v4}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v29

    .line 1847
    :cond_12
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v4, 0x7f020316

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v30

    .line 1848
    .local v30, "midBitmap2":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    if-eqz v4, :cond_13

    .line 1849
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    move-object/from16 v0, v30

    invoke-static {v0, v4}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v30

    .line 1851
    :cond_13
    invoke-virtual/range {v29 .. v29}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v25, v4, 0x2

    .line 1852
    .local v25, "half_x2":I
    invoke-virtual/range {v29 .. v29}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    div-int/lit8 v27, v4, 0x2

    .line 1855
    .local v27, "half_y2":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    const/4 v5, 0x5

    if-lt v4, v5, :cond_14

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    const/16 v5, 0x8

    if-le v4, v5, :cond_15

    :cond_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-boolean v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->mIsMove:Z

    if-eqz v4, :cond_18

    .line 1857
    :cond_15
    const/4 v4, 0x5

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int v4, v4, v25

    int-to-float v4, v4

    const/4 v5, 0x5

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int v5, v5, v27

    int-to-float v5, v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1858
    const/4 v4, 0x6

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int v4, v4, v25

    int-to-float v4, v4

    const/4 v5, 0x6

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int v5, v5, v27

    int-to-float v5, v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1859
    const/4 v4, 0x7

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int v4, v4, v25

    int-to-float v4, v4

    const/4 v5, 0x7

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int v5, v5, v27

    int-to-float v5, v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1860
    const/16 v4, 0x8

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int v4, v4, v25

    int-to-float v4, v4

    const/16 v5, 0x8

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int v5, v5, v27

    int-to-float v5, v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1870
    :goto_5
    invoke-virtual/range {v29 .. v29}, Landroid/graphics/Bitmap;->recycle()V

    .line 1871
    invoke-virtual/range {v30 .. v30}, Landroid/graphics/Bitmap;->recycle()V

    .line 1876
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->bRotationEnabled:I

    if-eqz v4, :cond_16

    .line 1877
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    const/16 v5, 0x9

    if-ne v4, v5, :cond_19

    .line 1878
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v4, 0x7f020313

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v42

    .line 1879
    .local v42, "rotateBitmap2":Landroid/graphics/Bitmap;
    const/16 v4, 0x9

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int v4, v4, v24

    int-to-float v4, v4

    const/16 v5, 0x9

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int v5, v5, v26

    int-to-float v5, v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1880
    invoke-virtual/range {v42 .. v42}, Landroid/graphics/Bitmap;->recycle()V

    .line 1891
    .end local v42    # "rotateBitmap2":Landroid/graphics/Bitmap;
    :cond_16
    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nSmartGuidesCnt:I

    if-lez v4, :cond_1a

    .line 1894
    new-instance v15, Landroid/graphics/Paint;

    invoke-direct {v15}, Landroid/graphics/Paint;-><init>()V

    .line 1896
    .local v15, "guidesPaint":Landroid/graphics/Paint;
    new-instance v22, Landroid/graphics/DashPathEffect;

    const/4 v4, 0x2

    new-array v4, v4, [F

    fill-array-data v4, :array_0

    const/high16 v5, 0x3f800000    # 1.0f

    move-object/from16 v0, v22

    invoke-direct {v0, v4, v5}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    .line 1898
    .local v22, "dashPath":Landroid/graphics/DashPathEffect;
    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 1899
    const/16 v4, 0xff

    const/16 v5, 0xd0

    const/16 v6, 0x8a

    const/16 v7, 0x78

    invoke-virtual {v15, v4, v5, v6, v7}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 1900
    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v15, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1901
    const/high16 v4, 0x40400000    # 3.0f

    invoke-virtual {v15, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1902
    const/4 v4, 0x1

    invoke-virtual {v15, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1903
    const/16 v28, 0x0

    .local v28, "i":I
    :goto_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nSmartGuidesCnt:I

    move/from16 v0, v28

    if-ge v0, v4, :cond_1a

    .line 1905
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesStart:[Landroid/graphics/Point;

    aget-object v4, v4, v28

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v11, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesStart:[Landroid/graphics/Point;

    aget-object v4, v4, v28

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v12, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesEnd:[Landroid/graphics/Point;

    aget-object v4, v4, v28

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v13, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesEnd:[Landroid/graphics/Point;

    aget-object v4, v4, v28

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v14, v4

    move-object/from16 v10, p1

    invoke-virtual/range {v10 .. v15}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1903
    add-int/lit8 v28, v28, 0x1

    goto :goto_7

    .line 1833
    .end local v15    # "guidesPaint":Landroid/graphics/Paint;
    .end local v22    # "dashPath":Landroid/graphics/DashPathEffect;
    .end local v25    # "half_x2":I
    .end local v27    # "half_y2":I
    .end local v28    # "i":I
    .end local v29    # "midBitmap1":Landroid/graphics/Bitmap;
    .end local v30    # "midBitmap2":Landroid/graphics/Bitmap;
    :cond_17
    const/4 v4, 0x1

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int v4, v4, v24

    int-to-float v4, v4

    const/4 v5, 0x1

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int v5, v5, v26

    int-to-float v5, v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1834
    const/4 v4, 0x2

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int v4, v4, v24

    int-to-float v4, v4

    const/4 v5, 0x2

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int v5, v5, v26

    int-to-float v5, v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1835
    const/4 v4, 0x3

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int v4, v4, v24

    int-to-float v4, v4

    const/4 v5, 0x3

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int v5, v5, v26

    int-to-float v5, v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1836
    const/4 v4, 0x4

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int v4, v4, v24

    int-to-float v4, v4

    const/4 v5, 0x4

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int v5, v5, v26

    int-to-float v5, v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_4

    .line 1864
    .restart local v25    # "half_x2":I
    .restart local v27    # "half_y2":I
    .restart local v29    # "midBitmap1":Landroid/graphics/Bitmap;
    .restart local v30    # "midBitmap2":Landroid/graphics/Bitmap;
    :cond_18
    const/4 v4, 0x5

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int v4, v4, v25

    int-to-float v4, v4

    const/4 v5, 0x5

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int v5, v5, v27

    int-to-float v5, v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1865
    const/4 v4, 0x6

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int v4, v4, v25

    int-to-float v4, v4

    const/4 v5, 0x6

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int v5, v5, v27

    int-to-float v5, v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1866
    const/4 v4, 0x7

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int v4, v4, v25

    int-to-float v4, v4

    const/4 v5, 0x7

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int v5, v5, v27

    int-to-float v5, v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1867
    const/16 v4, 0x8

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int v4, v4, v25

    int-to-float v4, v4

    const/16 v5, 0x8

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int v5, v5, v27

    int-to-float v5, v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_5

    .line 1884
    :cond_19
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v4, 0x7f020312

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v41

    .line 1885
    .local v41, "rotateBitmap1":Landroid/graphics/Bitmap;
    const/16 v4, 0x9

    aget-object v4, v37, v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int v4, v4, v24

    int-to-float v4, v4

    const/16 v5, 0x9

    aget-object v5, v37, v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int v5, v5, v26

    int-to-float v5, v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1886
    invoke-virtual/range {v41 .. v41}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_6

    .line 1916
    .end local v41    # "rotateBitmap1":Landroid/graphics/Bitmap;
    :cond_1a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nAdjustHandleCnt:I

    if-lez v4, :cond_1e

    .line 1918
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v4, 0x7f02031a

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v18

    .line 1919
    .local v18, "adjustHandleNormalBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v4, 0x7f02031b

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v19

    .line 1921
    .local v19, "adjustHandlePresseBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    if-eqz v4, :cond_1b

    .line 1922
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    move-object/from16 v0, v18

    invoke-static {v0, v4}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v18

    .line 1923
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    move-object/from16 v0, v19

    invoke-static {v0, v4}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v19

    .line 1926
    :cond_1b
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v24, v4, 0x2

    .line 1927
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    div-int/lit8 v26, v4, 0x2

    .line 1929
    const/16 v28, 0x0

    .restart local v28    # "i":I
    :goto_8
    const/16 v4, 0xa

    move/from16 v0, v28

    if-ge v0, v4, :cond_1d

    .line 1930
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    const/16 v5, 0xa

    if-lt v4, v5, :cond_1c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    const/16 v5, 0x14

    if-gt v4, v5, :cond_1c

    .line 1932
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    aget-object v4, v4, v28

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int v4, v4, v24

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    aget-object v5, v5, v28

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int v5, v5, v26

    int-to-float v5, v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1929
    :goto_9
    add-int/lit8 v28, v28, 0x1

    goto :goto_8

    .line 1936
    :cond_1c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    aget-object v4, v4, v28

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int v4, v4, v24

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    aget-object v5, v5, v28

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int v5, v5, v26

    int-to-float v5, v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_9

    .line 1941
    :cond_1d
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->recycle()V

    .line 1942
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Bitmap;->recycle()V

    .line 1944
    .end local v18    # "adjustHandleNormalBitmap":Landroid/graphics/Bitmap;
    .end local v19    # "adjustHandlePresseBitmap":Landroid/graphics/Bitmap;
    .end local v28    # "i":I
    :cond_1e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nAnimationOrderCnt:I

    if-lez v4, :cond_6

    .line 1946
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v4, 0x7f020091

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v17

    .line 1947
    .local v17, "AnimtionNormalBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v4, 0x7f020092

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 1948
    .local v16, "AnimtionAddBitmap":Landroid/graphics/Bitmap;
    new-instance v38, Landroid/graphics/Paint;

    invoke-direct/range {v38 .. v38}, Landroid/graphics/Paint;-><init>()V

    .line 1949
    .local v38, "paint2":Landroid/graphics/Paint;
    const/16 v4, 0xa

    new-array v0, v4, [I

    move-object/from16 v35, v0

    .line 1950
    .local v35, "ntemp":[I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const/high16 v5, 0x41300000    # 11.0f

    invoke-static {v4, v5}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v4

    float-to-int v0, v4

    move/from16 v33, v0

    .line 1951
    .local v33, "nMargine":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    const/high16 v6, 0x41480000    # 12.5f

    invoke-static {v5, v6}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v5

    float-to-int v5, v5

    sub-int/2addr v4, v5

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    sub-int v31, v4, v5

    .line 1952
    .local v31, "nBarX":I
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    add-int v32, v4, v33

    .line 1953
    .local v32, "nBarY":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const/high16 v5, 0x41400000    # 12.0f

    invoke-static {v4, v5}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v4

    float-to-int v0, v4

    move/from16 v34, v0

    .line 1954
    .local v34, "nText":I
    new-instance v43, Landroid/graphics/Rect;

    invoke-direct/range {v43 .. v43}, Landroid/graphics/Rect;-><init>()V

    .line 1955
    .local v43, "rt":Landroid/graphics/Rect;
    move/from16 v0, v34

    int-to-float v4, v0

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1956
    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 1957
    const v4, -0xd7d7d8

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 1958
    const-string/jumbo v4, "1234567890"

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object/from16 v0, v38

    move-object/from16 v1, v43

    invoke-virtual {v0, v4, v5, v6, v1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1959
    move-object/from16 v0, v43

    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, v43

    iget v5, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v5

    div-int/lit8 v40, v4, 0x2

    .line 1961
    .local v40, "rMargine":I
    const/16 v28, 0x0

    .restart local v28    # "i":I
    :goto_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nAnimationOrderCnt:I

    move/from16 v0, v28

    if-ge v0, v4, :cond_1f

    .line 1963
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAnimationOrder:[I

    aget v4, v4, v28

    aget v5, v35, v4

    add-int/lit8 v5, v5, 0x1

    aput v5, v35, v4

    .line 1961
    add-int/lit8 v28, v28, 0x1

    goto :goto_a

    .line 1965
    :cond_1f
    const/16 v28, 0x0

    :goto_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nAnimationOrderCnt:I

    move/from16 v0, v28

    if-ge v0, v4, :cond_23

    .line 1967
    add-int/lit8 v4, v28, 0x1

    mul-int v4, v4, v32

    sub-int v4, v4, v33

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    sub-int/2addr v5, v6

    if-gt v4, v5, :cond_22

    .line 1969
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAnimationOrder:[I

    aget v4, v4, v28

    aget v4, v35, v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_21

    .line 1970
    move/from16 v0, v31

    int-to-float v4, v0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    mul-int v6, v32, v28

    add-int/2addr v5, v6

    int-to-float v5, v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1971
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAnimationOrder:[I

    add-int/lit8 v5, v28, 0x1

    aget v4, v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int v5, v5, v31

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    mul-int v7, v32, v28

    add-int/2addr v6, v7

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v6, v7

    add-int v6, v6, v40

    int-to-float v6, v6

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v4, v5, v6, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1965
    :cond_20
    :goto_c
    add-int/lit8 v28, v28, 0x1

    goto :goto_b

    .line 1973
    :cond_21
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAnimationOrder:[I

    aget v4, v4, v28

    aget v4, v35, v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_20

    .line 1974
    move/from16 v0, v31

    int-to-float v4, v0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    mul-int v6, v32, v28

    add-int/2addr v5, v6

    int-to-float v5, v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1975
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAnimationOrder:[I

    add-int/lit8 v5, v28, 0x1

    aget v4, v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int v5, v5, v31

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    mul-int v7, v32, v28

    add-int/2addr v6, v7

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v6, v7

    add-int v6, v6, v40

    int-to-float v6, v6

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v4, v5, v6, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_c

    .line 1979
    :cond_22
    move/from16 v0, v31

    int-to-float v4, v0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    add-int/lit8 v6, v28, -0x1

    mul-int v6, v6, v32

    add-int/2addr v5, v6

    int-to-float v5, v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1980
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAnimationOrder:[I

    aget v4, v4, v28

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int v5, v5, v31

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    add-int/lit8 v7, v28, -0x1

    mul-int v7, v7, v32

    add-int/2addr v6, v7

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v6, v7

    add-int v6, v6, v40

    int-to-float v6, v6

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v4, v5, v6, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1984
    :cond_23
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Bitmap;->recycle()V

    .line 1985
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_3

    .line 1896
    nop

    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40000000    # 2.0f
    .end array-data
.end method

.method private drawBaseTextMark(Landroid/graphics/Canvas;Landroid/content/res/Resources;Lcom/infraware/office/baseframe/EvBaseView;)V
    .locals 24
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "resources"    # Landroid/content/res/Resources;
    .param p3, "ebv"    # Lcom/infraware/office/baseframe/EvBaseView;

    .prologue
    .line 2275
    const/4 v8, 0x0

    .line 2277
    .local v8, "textMarkPaint":Landroid/graphics/Paint;
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_TEXTMARK_REVERSE()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2278
    new-instance v8, Landroid/graphics/Paint;

    .end local v8    # "textMarkPaint":Landroid/graphics/Paint;
    const/4 v3, 0x1

    invoke-direct {v8, v3}, Landroid/graphics/Paint;-><init>(I)V

    .line 2279
    .restart local v8    # "textMarkPaint":Landroid/graphics/Paint;
    new-instance v11, Landroid/graphics/ColorMatrix;

    const/16 v3, 0x14

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-direct {v11, v3}, Landroid/graphics/ColorMatrix;-><init>([F)V

    .line 2284
    .local v11, "colorMatrix":Landroid/graphics/ColorMatrix;
    new-instance v3, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v3, v11}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    invoke-virtual {v8, v3}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 2298
    .end local v11    # "colorMatrix":Landroid/graphics/ColorMatrix;
    :goto_0
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->nTextRectCount:I

    move/from16 v0, v16

    if-ge v0, v3, :cond_7

    .line 2300
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->mTextRectInfos:[S

    mul-int/lit8 v4, v16, 0x4

    aget-short v17, v3, v4

    .line 2301
    .local v17, "l":S
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->mTextRectInfos:[S

    mul-int/lit8 v4, v16, 0x4

    add-int/lit8 v4, v4, 0x1

    aget-short v23, v3, v4

    .line 2302
    .local v23, "t":S
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->mTextRectInfos:[S

    mul-int/lit8 v4, v16, 0x4

    add-int/lit8 v4, v4, 0x2

    aget-short v20, v3, v4

    .line 2303
    .local v20, "r":S
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->mTextRectInfos:[S

    mul-int/lit8 v4, v16, 0x4

    add-int/lit8 v4, v4, 0x3

    aget-short v9, v3, v4

    .line 2304
    .local v9, "b":S
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_TEXTMARK_REVERSE()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2306
    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/infraware/office/baseframe/EvBaseView;->mSyncObject:Ljava/lang/Object;

    monitor-enter v4

    .line 2308
    :try_start_0
    invoke-virtual/range {p3 .. p3}, Lcom/infraware/office/baseframe/EvBaseView;->getPrivateBitmap()Landroid/graphics/Bitmap;

    move-result-object v12

    .line 2310
    .local v12, "content":Landroid/graphics/Bitmap;
    if-eqz v12, :cond_1

    .line 2312
    new-instance v14, Landroid/graphics/Rect;

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-direct {v14, v3, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2314
    .local v14, "drawRect":Landroid/graphics/Rect;
    move/from16 v0, v17

    move/from16 v1, v23

    move/from16 v2, v20

    invoke-virtual {v14, v0, v1, v2, v9}, Landroid/graphics/Rect;->intersect(IIII)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v14}, Landroid/graphics/Rect;->width()I

    move-result v3

    if-lez v3, :cond_5

    invoke-virtual {v14}, Landroid/graphics/Rect;->height()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-lez v3, :cond_5

    .line 2317
    const/4 v13, 0x0

    .line 2320
    .local v13, "cursorBitmap":Landroid/graphics/Bitmap;
    :try_start_1
    iget v3, v14, Landroid/graphics/Rect;->left:I

    iget v5, v14, Landroid/graphics/Rect;->top:I

    invoke-virtual {v14}, Landroid/graphics/Rect;->width()I

    move-result v6

    invoke-virtual {v14}, Landroid/graphics/Rect;->height()I

    move-result v7

    invoke-static {v12, v3, v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 2321
    if-ne v13, v12, :cond_4

    const/4 v10, 0x1

    .line 2322
    .local v10, "bSame":Z
    :goto_2
    const/4 v12, 0x0

    .line 2323
    if-eqz v13, :cond_1

    .line 2325
    iget v3, v14, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    iget v5, v14, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v3, v5, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2326
    if-nez v10, :cond_0

    .line 2327
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2328
    :cond_0
    const/4 v13, 0x0

    .line 2341
    .end local v10    # "bSame":Z
    .end local v13    # "cursorBitmap":Landroid/graphics/Bitmap;
    .end local v14    # "drawRect":Landroid/graphics/Rect;
    :cond_1
    :goto_3
    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2298
    .end local v12    # "content":Landroid/graphics/Bitmap;
    :goto_4
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_1

    .line 2288
    .end local v9    # "b":S
    .end local v16    # "i":I
    .end local v17    # "l":S
    .end local v20    # "r":S
    .end local v23    # "t":S
    :cond_2
    new-instance v8, Landroid/graphics/Paint;

    .end local v8    # "textMarkPaint":Landroid/graphics/Paint;
    invoke-direct {v8}, Landroid/graphics/Paint;-><init>()V

    .line 2289
    .restart local v8    # "textMarkPaint":Landroid/graphics/Paint;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget-boolean v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->mIsDrawBar:Z

    if-eqz v3, :cond_3

    .line 2290
    const/16 v3, 0x4d

    const/4 v4, 0x0

    const/16 v5, 0x8f

    const/16 v6, 0xe1

    invoke-virtual {v8, v3, v4, v5, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 2294
    :goto_5
    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v8, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    goto/16 :goto_0

    .line 2292
    :cond_3
    const/16 v3, 0x7f

    const/4 v4, 0x0

    const/16 v5, 0x8f

    const/16 v6, 0xe1

    invoke-virtual {v8, v3, v4, v5, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto :goto_5

    .line 2321
    .restart local v9    # "b":S
    .restart local v12    # "content":Landroid/graphics/Bitmap;
    .restart local v13    # "cursorBitmap":Landroid/graphics/Bitmap;
    .restart local v14    # "drawRect":Landroid/graphics/Rect;
    .restart local v16    # "i":I
    .restart local v17    # "l":S
    .restart local v20    # "r":S
    .restart local v23    # "t":S
    :cond_4
    const/4 v10, 0x0

    goto :goto_2

    .line 2331
    :catch_0
    move-exception v15

    .line 2333
    .local v15, "ex":Ljava/lang/OutOfMemoryError;
    const/4 v12, 0x0

    goto :goto_3

    .line 2337
    .end local v13    # "cursorBitmap":Landroid/graphics/Bitmap;
    .end local v15    # "ex":Ljava/lang/OutOfMemoryError;
    :cond_5
    const/4 v12, 0x0

    goto :goto_3

    .line 2341
    .end local v12    # "content":Landroid/graphics/Bitmap;
    .end local v14    # "drawRect":Landroid/graphics/Rect;
    :catchall_0
    move-exception v3

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v3

    .line 2346
    :cond_6
    move/from16 v0, v17

    int-to-float v4, v0

    move/from16 v0, v23

    int-to-float v5, v0

    move/from16 v0, v20

    int-to-float v6, v0

    int-to-float v7, v9

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_4

    .line 2349
    .end local v9    # "b":S
    .end local v17    # "l":S
    .end local v20    # "r":S
    .end local v23    # "t":S
    :cond_7
    const/4 v8, 0x0

    .line 2351
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget-boolean v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->mIsDrawBar:Z

    if-eqz v3, :cond_9

    .line 2375
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_a

    .line 2376
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startPressedId:I

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v22

    .line 2377
    .local v22, "selecttopBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endPressedId:I

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 2378
    .local v21, "selectbtmBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startImageRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startImageRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2379
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endImageRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endImageRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2380
    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Bitmap;->recycle()V

    .line 2381
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->recycle()V

    .line 2394
    .end local v21    # "selectbtmBitmap":Landroid/graphics/Bitmap;
    .end local v22    # "selecttopBitmap":Landroid/graphics/Bitmap;
    :cond_9
    :goto_6
    return-void

    .line 2384
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startNormalId:I

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v19

    .line 2385
    .local v19, "normaltopBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endNormalId:I

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v18

    .line 2386
    .local v18, "normalbtmBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startImageRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startImageRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2387
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endImageRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endImageRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2388
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Bitmap;->recycle()V

    .line 2389
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_6

    .line 2279
    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        0x0
        0x0
        0x0
        0x437f0000    # 255.0f
        0x0
        -0x40800000    # -1.0f
        0x0
        0x0
        0x437f0000    # 255.0f
        0x0
        0x0
        -0x40800000    # -1.0f
        0x0
        0x437f0000    # 255.0f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private drawInfraPenMulti(Landroid/graphics/Canvas;Landroid/content/res/Resources;)V
    .locals 23
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 1225
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->nObjectCount:I

    if-ge v14, v2, :cond_4

    .line 1228
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v2, v2, v14

    iget v15, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->mObjectType:I

    .line 1229
    .local v15, "nObjectType":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->eEditing:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 1231
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v2, v2, v14

    iget-object v0, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->editStartPoint:Landroid/graphics/Point;

    move-object/from16 v22, v0

    .line 1232
    .local v22, "startPoint":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v2, v2, v14

    iget-object v10, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->editEndPoint:Landroid/graphics/Point;

    .line 1240
    .local v10, "endPoint":Landroid/graphics/Point;
    :goto_1
    new-instance v17, Landroid/graphics/PointF;

    move-object/from16 v0, v22

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    move-object/from16 v0, v22

    iget v3, v0, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    move-object/from16 v0, v17

    invoke-direct {v0, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1241
    .local v17, "p0":Landroid/graphics/PointF;
    new-instance v18, Landroid/graphics/PointF;

    iget v2, v10, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    move-object/from16 v0, v22

    iget v3, v0, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    move-object/from16 v0, v18

    invoke-direct {v0, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1242
    .local v18, "p1":Landroid/graphics/PointF;
    new-instance v19, Landroid/graphics/PointF;

    iget v2, v10, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget v3, v10, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    move-object/from16 v0, v19

    invoke-direct {v0, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1243
    .local v19, "p2":Landroid/graphics/PointF;
    new-instance v20, Landroid/graphics/PointF;

    move-object/from16 v0, v22

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget v3, v10, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    move-object/from16 v0, v20

    invoke-direct {v0, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1249
    .local v20, "p3":Landroid/graphics/PointF;
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_TEXTMARK_REVERSE()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1251
    new-instance v21, Landroid/graphics/Paint;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/Paint;-><init>()V

    .line 1252
    .local v21, "paintFill":Landroid/graphics/Paint;
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1254
    const/4 v2, 0x5

    if-ne v15, v2, :cond_2

    .line 1255
    const/16 v2, 0x40

    const/16 v3, 0x77

    const/16 v4, 0xb5

    const/16 v5, 0xf0

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 1260
    :goto_2
    new-instance v11, Landroid/graphics/Path;

    invoke-direct {v11}, Landroid/graphics/Path;-><init>()V

    .line 1261
    .local v11, "fillPath":Landroid/graphics/Path;
    sget-object v2, Landroid/graphics/Path$FillType;->WINDING:Landroid/graphics/Path$FillType;

    invoke-virtual {v11, v2}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 1262
    move-object/from16 v0, v17

    iget v2, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v17

    iget v3, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v11, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1263
    move-object/from16 v0, v18

    iget v2, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v18

    iget v3, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v11, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1264
    move-object/from16 v0, v19

    iget v2, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v11, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1265
    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v20

    iget v3, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v11, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1266
    move-object/from16 v0, v17

    iget v2, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v17

    iget v3, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v11, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1267
    invoke-virtual {v11}, Landroid/graphics/Path;->close()V

    .line 1269
    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v11, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1271
    const/16 v21, 0x0

    .line 1276
    .end local v11    # "fillPath":Landroid/graphics/Path;
    .end local v21    # "paintFill":Landroid/graphics/Paint;
    :cond_0
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 1278
    .local v7, "paint1":Landroid/graphics/Paint;
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_TEXTMARK_REVERSE()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1280
    const v2, -0xe47f39

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1282
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1283
    move-object/from16 v0, v17

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v17

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v18

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v18

    iget v6, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1284
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v20

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v20

    iget v6, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1285
    move-object/from16 v0, v18

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v18

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v6, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1286
    move-object/from16 v0, v20

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v20

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v17

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v17

    iget v6, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1317
    :goto_3
    const/4 v7, 0x0

    .line 1321
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->iconSize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    div-int/lit8 v12, v2, 0x2

    .line 1322
    .local v12, "half_x":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->iconSize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    div-int/lit8 v13, v2, 0x2

    .line 1323
    .local v13, "half_y":I
    const/4 v8, 0x0

    .line 1324
    .local v8, "ctrlBitmap1":Landroid/graphics/Bitmap;
    const/4 v9, 0x0

    .line 1326
    .local v9, "ctrlBitmap2":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v2, 0x7f020314

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 1327
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v2, 0x7f020317

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 1329
    move-object/from16 v0, v17

    iget v2, v0, Landroid/graphics/PointF;->x:F

    int-to-float v3, v12

    sub-float/2addr v2, v3

    move-object/from16 v0, v17

    iget v3, v0, Landroid/graphics/PointF;->y:F

    int-to-float v4, v13

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1330
    move-object/from16 v0, v18

    iget v2, v0, Landroid/graphics/PointF;->x:F

    int-to-float v3, v12

    sub-float/2addr v2, v3

    move-object/from16 v0, v18

    iget v3, v0, Landroid/graphics/PointF;->y:F

    int-to-float v4, v13

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1331
    move-object/from16 v0, v19

    iget v2, v0, Landroid/graphics/PointF;->x:F

    int-to-float v3, v12

    sub-float/2addr v2, v3

    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->y:F

    int-to-float v4, v13

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1332
    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/PointF;->x:F

    int-to-float v3, v12

    sub-float/2addr v2, v3

    move-object/from16 v0, v20

    iget v3, v0, Landroid/graphics/PointF;->y:F

    int-to-float v4, v13

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1334
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    .line 1335
    invoke-virtual {v9}, Landroid/graphics/Bitmap;->recycle()V

    .line 1225
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_0

    .line 1236
    .end local v7    # "paint1":Landroid/graphics/Paint;
    .end local v8    # "ctrlBitmap1":Landroid/graphics/Bitmap;
    .end local v9    # "ctrlBitmap2":Landroid/graphics/Bitmap;
    .end local v10    # "endPoint":Landroid/graphics/Point;
    .end local v12    # "half_x":I
    .end local v13    # "half_y":I
    .end local v17    # "p0":Landroid/graphics/PointF;
    .end local v18    # "p1":Landroid/graphics/PointF;
    .end local v19    # "p2":Landroid/graphics/PointF;
    .end local v20    # "p3":Landroid/graphics/PointF;
    .end local v22    # "startPoint":Landroid/graphics/Point;
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v2, v2, v14

    iget-object v0, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v22, v0

    .line 1237
    .restart local v22    # "startPoint":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v2, v2, v14

    iget-object v10, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->endRangePoint:Landroid/graphics/Point;

    .restart local v10    # "endPoint":Landroid/graphics/Point;
    goto/16 :goto_1

    .line 1258
    .restart local v17    # "p0":Landroid/graphics/PointF;
    .restart local v18    # "p1":Landroid/graphics/PointF;
    .restart local v19    # "p2":Landroid/graphics/PointF;
    .restart local v20    # "p3":Landroid/graphics/PointF;
    .restart local v21    # "paintFill":Landroid/graphics/Paint;
    :cond_2
    const/16 v2, 0x30

    const/16 v3, 0x77

    const/16 v4, 0xb5

    const/16 v5, 0xf0

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_2

    .line 1291
    .end local v21    # "paintFill":Landroid/graphics/Paint;
    .restart local v7    # "paint1":Landroid/graphics/Paint;
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v2, v3}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v16

    .line 1292
    .local v16, "oneDip":F
    const/4 v2, -0x1

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1293
    const/high16 v2, 0x40400000    # 3.0f

    mul-float v2, v2, v16

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1294
    move-object/from16 v0, v17

    iget v2, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v17

    iget v3, v0, Landroid/graphics/PointF;->y:F

    const/high16 v4, 0x3fc00000    # 1.5f

    mul-float v4, v4, v16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v7}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1295
    move-object/from16 v0, v18

    iget v2, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v18

    iget v3, v0, Landroid/graphics/PointF;->y:F

    const/high16 v4, 0x3fc00000    # 1.5f

    mul-float v4, v4, v16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v7}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1296
    move-object/from16 v0, v19

    iget v2, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->y:F

    const/high16 v4, 0x3fc00000    # 1.5f

    mul-float v4, v4, v16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v7}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1297
    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v20

    iget v3, v0, Landroid/graphics/PointF;->y:F

    const/high16 v4, 0x3fc00000    # 1.5f

    mul-float v4, v4, v16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v7}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1299
    move-object/from16 v0, v17

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v17

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v18

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v18

    iget v6, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1300
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v20

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v20

    iget v6, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1301
    move-object/from16 v0, v18

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v18

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v6, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1302
    move-object/from16 v0, v20

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v20

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v17

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v17

    iget v6, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1304
    const v2, -0xce8d45

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1305
    const/high16 v2, 0x40000000    # 2.0f

    mul-float v2, v2, v16

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1307
    move-object/from16 v0, v17

    iget v2, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v17

    iget v3, v0, Landroid/graphics/PointF;->y:F

    const/high16 v4, 0x3fc00000    # 1.5f

    mul-float v4, v4, v16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v7}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1308
    move-object/from16 v0, v18

    iget v2, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v18

    iget v3, v0, Landroid/graphics/PointF;->y:F

    const/high16 v4, 0x3fc00000    # 1.5f

    mul-float v4, v4, v16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v7}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1309
    move-object/from16 v0, v19

    iget v2, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->y:F

    const/high16 v4, 0x3fc00000    # 1.5f

    mul-float v4, v4, v16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v7}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1310
    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v20

    iget v3, v0, Landroid/graphics/PointF;->y:F

    const/high16 v4, 0x3fc00000    # 1.5f

    mul-float v4, v4, v16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v7}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1312
    move-object/from16 v0, v17

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v17

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v18

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v18

    iget v6, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1313
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v20

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v20

    iget v6, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1314
    move-object/from16 v0, v18

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v18

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v6, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1315
    move-object/from16 v0, v20

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v20

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v17

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v17

    iget v6, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 1341
    .end local v7    # "paint1":Landroid/graphics/Paint;
    .end local v10    # "endPoint":Landroid/graphics/Point;
    .end local v15    # "nObjectType":I
    .end local v16    # "oneDip":F
    .end local v17    # "p0":Landroid/graphics/PointF;
    .end local v18    # "p1":Landroid/graphics/PointF;
    .end local v19    # "p2":Landroid/graphics/PointF;
    .end local v20    # "p3":Landroid/graphics/PointF;
    .end local v22    # "startPoint":Landroid/graphics/Point;
    :cond_4
    return-void
.end method

.method private drawSheetAutoFilter(Landroid/graphics/Canvas;Landroid/content/res/Resources;)V
    .locals 21
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 2730
    const v15, 0x7f0201da

    .line 2731
    .local v15, "sheetFilterNormal":I
    const v16, 0x7f0201db

    .line 2734
    .local v16, "sheetFilterPressed":I
    const/4 v12, 0x0

    .line 2735
    .local v12, "nNativeArrIndex":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mSheetAutofilterInfo:[I

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v18, v0

    div-int/lit8 v18, v18, 0x4

    move/from16 v0, v18

    if-ge v8, v0, :cond_3

    .line 2736
    const/4 v5, 0x0

    .line 2737
    .local v5, "ctrlBitmap1":Landroid/graphics/Bitmap;
    const/4 v6, 0x0

    .line 2739
    .local v6, "ctrlBitmap2":Landroid/graphics/Bitmap;
    move-object/from16 v0, p2

    invoke-static {v0, v15}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 2740
    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 2742
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mSheetAutofilterInfo:[I

    move-object/from16 v18, v0

    add-int/lit8 v13, v12, 0x1

    .end local v12    # "nNativeArrIndex":I
    .local v13, "nNativeArrIndex":I
    aget v10, v18, v12

    .line 2743
    .local v10, "left":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mSheetAutofilterInfo:[I

    move-object/from16 v18, v0

    add-int/lit8 v12, v13, 0x1

    .end local v13    # "nNativeArrIndex":I
    .restart local v12    # "nNativeArrIndex":I
    aget v17, v18, v13

    .line 2744
    .local v17, "top":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mSheetAutofilterInfo:[I

    move-object/from16 v18, v0

    add-int/lit8 v13, v12, 0x1

    .end local v12    # "nNativeArrIndex":I
    .restart local v13    # "nNativeArrIndex":I
    aget v14, v18, v12

    .line 2745
    .local v14, "right":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mSheetAutofilterInfo:[I

    move-object/from16 v18, v0

    add-int/lit8 v12, v13, 0x1

    .end local v13    # "nNativeArrIndex":I
    .restart local v12    # "nNativeArrIndex":I
    aget v4, v18, v13

    .line 2747
    .local v4, "bottom":I
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    .line 2748
    .local v9, "imageHeight":I
    const/4 v11, 0x0

    .line 2749
    .local v11, "margin":I
    sub-int v7, v4, v17

    .line 2751
    .local v7, "height":I
    if-ge v7, v9, :cond_1

    .line 2752
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->bPressedFilterBtn:Z

    move/from16 v18, v0

    if-eqz v18, :cond_0

    .line 2753
    const/16 v18, 0x0

    new-instance v19, Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v20

    sub-int v20, v14, v20

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v17

    invoke-direct {v0, v1, v2, v14, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v6, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2764
    :goto_1
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    .line 2765
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    .line 2735
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    .line 2755
    :cond_0
    const/16 v18, 0x0

    new-instance v19, Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v20

    sub-int v20, v14, v20

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v17

    invoke-direct {v0, v1, v2, v14, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v5, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 2758
    :cond_1
    sub-int v18, v7, v9

    div-int/lit8 v11, v18, 0x2

    .line 2759
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->bPressedFilterBtn:Z

    move/from16 v18, v0

    if-eqz v18, :cond_2

    .line 2760
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v18

    sub-int v18, v14, v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    add-int v19, v17, v11

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v6, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 2762
    :cond_2
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v18

    sub-int v18, v14, v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    add-int v19, v17, v11

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v5, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 2770
    .end local v4    # "bottom":I
    .end local v5    # "ctrlBitmap1":Landroid/graphics/Bitmap;
    .end local v6    # "ctrlBitmap2":Landroid/graphics/Bitmap;
    .end local v7    # "height":I
    .end local v9    # "imageHeight":I
    .end local v10    # "left":I
    .end local v11    # "margin":I
    .end local v14    # "right":I
    .end local v17    # "top":I
    :cond_3
    return-void
.end method

.method private drawSheetFormulaCursorRange(Landroid/graphics/Canvas;Landroid/content/res/Resources;)V
    .locals 16
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 2773
    const/4 v8, 0x0

    .line 2774
    .local v8, "nNativeArrIndex":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mSheetFormulaCursorRangeInfo:[I

    array-length v13, v13

    div-int/lit8 v13, v13, 0x4

    if-ge v5, v13, :cond_0

    .line 2775
    const/4 v2, 0x0

    .line 2776
    .local v2, "ctrlBitmap1":Landroid/graphics/Bitmap;
    const/4 v3, 0x0

    .line 2778
    .local v3, "ctrlBitmap2":Landroid/graphics/Bitmap;
    div-int/lit8 v13, v8, 0x4

    rem-int/lit8 v7, v13, 0x7

    .line 2779
    .local v7, "nFormulaIndex":I
    packed-switch v7, :pswitch_data_0

    .line 2827
    .end local v2    # "ctrlBitmap1":Landroid/graphics/Bitmap;
    .end local v3    # "ctrlBitmap2":Landroid/graphics/Bitmap;
    .end local v7    # "nFormulaIndex":I
    :cond_0
    return-void

    .line 2781
    .restart local v2    # "ctrlBitmap1":Landroid/graphics/Bitmap;
    .restart local v3    # "ctrlBitmap2":Landroid/graphics/Bitmap;
    .restart local v7    # "nFormulaIndex":I
    :pswitch_0
    const v13, 0x7f020304

    move-object/from16 v0, p2

    invoke-static {v0, v13}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 2782
    const v13, 0x7f020305

    move-object/from16 v0, p2

    invoke-static {v0, v13}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 2812
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mSheetFormulaCursorRangeInfo:[I

    add-int/lit8 v9, v8, 0x1

    .end local v8    # "nNativeArrIndex":I
    .local v9, "nNativeArrIndex":I
    aget v6, v13, v8

    .line 2813
    .local v6, "left":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mSheetFormulaCursorRangeInfo:[I

    add-int/lit8 v8, v9, 0x1

    .end local v9    # "nNativeArrIndex":I
    .restart local v8    # "nNativeArrIndex":I
    aget v11, v13, v9

    .line 2814
    .local v11, "top":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mSheetFormulaCursorRangeInfo:[I

    add-int/lit8 v9, v8, 0x1

    .end local v8    # "nNativeArrIndex":I
    .restart local v9    # "nNativeArrIndex":I
    aget v10, v13, v8

    .line 2815
    .local v10, "right":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mSheetFormulaCursorRangeInfo:[I

    add-int/lit8 v8, v9, 0x1

    .end local v9    # "nNativeArrIndex":I
    .restart local v8    # "nNativeArrIndex":I
    aget v1, v13, v9

    .line 2817
    .local v1, "bottom":I
    sub-int v12, v10, v6

    .line 2818
    .local v12, "width":I
    sub-int v4, v1, v11

    .line 2821
    .local v4, "height":I
    div-int/lit8 v13, v12, 0x4

    add-int/2addr v13, v6

    const/16 v14, 0x28

    if-le v13, v14, :cond_1

    div-int/lit8 v13, v4, 0x4

    add-int/2addr v13, v11

    const/16 v14, 0x21

    if-le v13, v14, :cond_1

    .line 2822
    div-int/lit8 v13, v12, 0x4

    sub-int v13, v6, v13

    int-to-float v13, v13

    div-int/lit8 v14, v4, 0x4

    sub-int v14, v11, v14

    int-to-float v14, v14

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13, v14, v15}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2824
    :cond_1
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 2825
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 2774
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 2785
    .end local v1    # "bottom":I
    .end local v4    # "height":I
    .end local v6    # "left":I
    .end local v10    # "right":I
    .end local v11    # "top":I
    .end local v12    # "width":I
    :pswitch_1
    const v13, 0x7f020306

    move-object/from16 v0, p2

    invoke-static {v0, v13}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 2786
    const v13, 0x7f020307

    move-object/from16 v0, p2

    invoke-static {v0, v13}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 2787
    goto :goto_1

    .line 2789
    :pswitch_2
    const v13, 0x7f020308

    move-object/from16 v0, p2

    invoke-static {v0, v13}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 2790
    const v13, 0x7f020309

    move-object/from16 v0, p2

    invoke-static {v0, v13}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 2791
    goto :goto_1

    .line 2793
    :pswitch_3
    const v13, 0x7f02030a

    move-object/from16 v0, p2

    invoke-static {v0, v13}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 2794
    const v13, 0x7f02030b

    move-object/from16 v0, p2

    invoke-static {v0, v13}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 2795
    goto/16 :goto_1

    .line 2797
    :pswitch_4
    const v13, 0x7f02030c

    move-object/from16 v0, p2

    invoke-static {v0, v13}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 2798
    const v13, 0x7f02030d

    move-object/from16 v0, p2

    invoke-static {v0, v13}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 2799
    goto/16 :goto_1

    .line 2801
    :pswitch_5
    const v13, 0x7f02030e

    move-object/from16 v0, p2

    invoke-static {v0, v13}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 2802
    const v13, 0x7f02030f

    move-object/from16 v0, p2

    invoke-static {v0, v13}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 2803
    goto/16 :goto_1

    .line 2805
    :pswitch_6
    const v13, 0x7f020310

    move-object/from16 v0, p2

    invoke-static {v0, v13}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 2806
    const v13, 0x7f020311

    move-object/from16 v0, p2

    invoke-static {v0, v13}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 2807
    goto/16 :goto_1

    .line 2779
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private drawVideoBtn(Landroid/graphics/Canvas;Landroid/content/res/Resources;[Landroid/graphics/Point;)V
    .locals 30
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "resources"    # Landroid/content/res/Resources;
    .param p3, "p"    # [Landroid/graphics/Point;

    .prologue
    .line 3824
    const/16 v18, 0x0

    .line 3825
    .local v18, "oBitmap":Landroid/graphics/Bitmap;
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getVideoStatus()I

    move-result v24

    packed-switch v24, :pswitch_data_0

    .line 3841
    :goto_0
    :pswitch_0
    if-nez v18, :cond_1

    .line 3920
    :cond_0
    :goto_1
    :pswitch_1
    return-void

    .line 3828
    :pswitch_2
    const v24, 0x7f020026

    move-object/from16 v0, p2

    move/from16 v1, v24

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v18

    .line 3829
    goto :goto_0

    .line 3845
    :cond_1
    new-instance v19, Landroid/graphics/Rect;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Rect;-><init>()V

    .line 3848
    .local v19, "rcVideo":Landroid/graphics/Rect;
    const/16 v24, 0x1

    aget-object v24, p3, v24

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v24, v0

    const/16 v25, 0x3

    aget-object v25, p3, v25

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v25, v0

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_2

    .line 3850
    const/16 v24, 0x3

    aget-object v24, p3, v24

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v20, v0

    .line 3851
    .local v20, "x1":I
    const/16 v24, 0x1

    aget-object v24, p3, v24

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v21, v0

    .line 3857
    .local v21, "x2":I
    :goto_2
    const/16 v24, 0x1

    aget-object v24, p3, v24

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v24, v0

    const/16 v25, 0x3

    aget-object v25, p3, v25

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_3

    .line 3859
    const/16 v24, 0x3

    aget-object v24, p3, v24

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v22, v0

    .line 3860
    .local v22, "y1":I
    const/16 v24, 0x1

    aget-object v24, p3, v24

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v23, v0

    .line 3866
    .local v23, "y2":I
    :goto_3
    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v22

    move/from16 v3, v21

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 3867
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v24

    move/from16 v0, v24

    int-to-double v10, v0

    .line 3868
    .local v10, "nBtnWidth":D
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v24

    move/from16 v0, v24

    int-to-double v8, v0

    .line 3871
    .local v8, "nBtnHeight":D
    const/16 v24, 0x9

    :try_start_0
    aget-object v24, p3, v24

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v24, v0

    const/16 v25, 0x5

    aget-object v25, p3, v25

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    sub-int v24, v24, v25

    invoke-static/range {v24 .. v24}, Ljava/lang/Math;->abs(I)I

    move-result v24

    const/16 v25, 0x9

    aget-object v25, p3, v25

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v25, v0

    const/16 v26, 0x5

    aget-object v26, p3, v26

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v26, v0

    sub-int v25, v25, v26

    invoke-static/range {v25 .. v25}, Ljava/lang/Math;->abs(I)I

    move-result v25

    div-int v24, v24, v25

    move/from16 v0, v24

    int-to-double v5, v0

    .line 3873
    .local v5, "a":D
    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    cmpl-double v24, v5, v24

    if-lez v24, :cond_4

    .line 3875
    const/16 v24, 0x5

    aget-object v24, p3, v24

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v24, v0

    const/16 v25, 0x7

    aget-object v25, p3, v25

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v25, v0

    sub-int v24, v24, v25

    invoke-static/range {v24 .. v24}, Ljava/lang/Math;->abs(I)I

    move-result v24

    move/from16 v0, v24

    int-to-double v0, v0

    move-wide/from16 v24, v0

    const-wide/high16 v26, 0x4000000000000000L    # 2.0

    invoke-static/range {v24 .. v27}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v24

    const/16 v26, 0x5

    aget-object v26, p3, v26

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v26, v0

    const/16 v27, 0x7

    aget-object v27, p3, v27

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v27, v0

    sub-int v26, v26, v27

    invoke-static/range {v26 .. v26}, Ljava/lang/Math;->abs(I)I

    move-result v26

    move/from16 v0, v26

    int-to-double v0, v0

    move-wide/from16 v26, v0

    const-wide/high16 v28, 0x4000000000000000L    # 2.0

    invoke-static/range {v26 .. v29}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v26

    add-double v24, v24, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v16

    .line 3876
    .local v16, "nVideoWidth":D
    const/16 v24, 0x8

    aget-object v24, p3, v24

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v24, v0

    const/16 v25, 0x6

    aget-object v25, p3, v25

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v25, v0

    sub-int v24, v24, v25

    invoke-static/range {v24 .. v24}, Ljava/lang/Math;->abs(I)I

    move-result v24

    move/from16 v0, v24

    int-to-double v0, v0

    move-wide/from16 v24, v0

    const-wide/high16 v26, 0x4000000000000000L    # 2.0

    invoke-static/range {v24 .. v27}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v24

    const/16 v26, 0x8

    aget-object v26, p3, v26

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v26, v0

    const/16 v27, 0x6

    aget-object v27, p3, v27

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v27, v0

    sub-int v26, v26, v27

    invoke-static/range {v26 .. v26}, Ljava/lang/Math;->abs(I)I

    move-result v26

    move/from16 v0, v26

    int-to-double v0, v0

    move-wide/from16 v26, v0

    const-wide/high16 v28, 0x4000000000000000L    # 2.0

    invoke-static/range {v26 .. v29}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v26

    add-double v24, v24, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v14

    .line 3885
    .local v14, "nVideoHeight":D
    :goto_4
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v24

    move/from16 v0, v24

    int-to-double v0, v0

    move-wide/from16 v24, v0

    const-wide/high16 v26, 0x4000000000000000L    # 2.0

    invoke-static/range {v24 .. v27}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v24

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v26

    move/from16 v0, v26

    int-to-double v0, v0

    move-wide/from16 v26, v0

    const-wide/high16 v28, 0x4000000000000000L    # 2.0

    invoke-static/range {v26 .. v29}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v26

    add-double v24, v24, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sqrt(D)D
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v10

    .line 3886
    move-wide v8, v10

    .line 3897
    .end local v5    # "a":D
    :goto_5
    cmpg-double v24, v16, v10

    if-gez v24, :cond_5

    .line 3898
    const/16 v24, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->rcVideoPlayBtn:Landroid/graphics/Rect;

    goto/16 :goto_1

    .line 3854
    .end local v8    # "nBtnHeight":D
    .end local v10    # "nBtnWidth":D
    .end local v14    # "nVideoHeight":D
    .end local v16    # "nVideoWidth":D
    .end local v20    # "x1":I
    .end local v21    # "x2":I
    .end local v22    # "y1":I
    .end local v23    # "y2":I
    :cond_2
    const/16 v24, 0x3

    aget-object v24, p3, v24

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v21, v0

    .line 3855
    .restart local v21    # "x2":I
    const/16 v24, 0x1

    aget-object v24, p3, v24

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v20, v0

    .restart local v20    # "x1":I
    goto/16 :goto_2

    .line 3863
    :cond_3
    const/16 v24, 0x3

    aget-object v24, p3, v24

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v23, v0

    .line 3864
    .restart local v23    # "y2":I
    const/16 v24, 0x1

    aget-object v24, p3, v24

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v22, v0

    .restart local v22    # "y1":I
    goto/16 :goto_3

    .line 3880
    .restart local v5    # "a":D
    .restart local v8    # "nBtnHeight":D
    .restart local v10    # "nBtnWidth":D
    :cond_4
    const/16 v24, 0x5

    :try_start_1
    aget-object v24, p3, v24

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v24, v0

    const/16 v25, 0x7

    aget-object v25, p3, v25

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v25, v0

    sub-int v24, v24, v25

    invoke-static/range {v24 .. v24}, Ljava/lang/Math;->abs(I)I

    move-result v24

    move/from16 v0, v24

    int-to-double v0, v0

    move-wide/from16 v24, v0

    const-wide/high16 v26, 0x4000000000000000L    # 2.0

    invoke-static/range {v24 .. v27}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v24

    const/16 v26, 0x5

    aget-object v26, p3, v26

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v26, v0

    const/16 v27, 0x7

    aget-object v27, p3, v27

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v27, v0

    sub-int v26, v26, v27

    invoke-static/range {v26 .. v26}, Ljava/lang/Math;->abs(I)I

    move-result v26

    move/from16 v0, v26

    int-to-double v0, v0

    move-wide/from16 v26, v0

    const-wide/high16 v28, 0x4000000000000000L    # 2.0

    invoke-static/range {v26 .. v29}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v26

    add-double v24, v24, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v14

    .line 3881
    .restart local v14    # "nVideoHeight":D
    const/16 v24, 0x8

    aget-object v24, p3, v24

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v24, v0

    const/16 v25, 0x6

    aget-object v25, p3, v25

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v25, v0

    sub-int v24, v24, v25

    invoke-static/range {v24 .. v24}, Ljava/lang/Math;->abs(I)I

    move-result v24

    move/from16 v0, v24

    int-to-double v0, v0

    move-wide/from16 v24, v0

    const-wide/high16 v26, 0x4000000000000000L    # 2.0

    invoke-static/range {v24 .. v27}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v24

    const/16 v26, 0x8

    aget-object v26, p3, v26

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v26, v0

    const/16 v27, 0x6

    aget-object v27, p3, v27

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v27, v0

    sub-int v26, v26, v27

    invoke-static/range {v26 .. v26}, Ljava/lang/Math;->abs(I)I

    move-result v26

    move/from16 v0, v26

    int-to-double v0, v0

    move-wide/from16 v26, v0

    const-wide/high16 v28, 0x4000000000000000L    # 2.0

    invoke-static/range {v26 .. v29}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v26

    add-double v24, v24, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sqrt(D)D
    :try_end_1
    .catch Ljava/lang/ArithmeticException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v16

    .restart local v16    # "nVideoWidth":D
    goto/16 :goto_4

    .line 3889
    .end local v5    # "a":D
    .end local v14    # "nVideoHeight":D
    .end local v16    # "nVideoWidth":D
    :catch_0
    move-exception v7

    .line 3890
    .local v7, "e":Ljava/lang/ArithmeticException;
    const/16 v24, 0x5

    aget-object v24, p3, v24

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v24, v0

    const/16 v25, 0x7

    aget-object v25, p3, v25

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v25, v0

    sub-int v24, v24, v25

    invoke-static/range {v24 .. v24}, Ljava/lang/Math;->abs(I)I

    move-result v24

    move/from16 v0, v24

    int-to-double v0, v0

    move-wide/from16 v24, v0

    const-wide/high16 v26, 0x4000000000000000L    # 2.0

    invoke-static/range {v24 .. v27}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v24

    const/16 v26, 0x5

    aget-object v26, p3, v26

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v26, v0

    const/16 v27, 0x7

    aget-object v27, p3, v27

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v27, v0

    sub-int v26, v26, v27

    invoke-static/range {v26 .. v26}, Ljava/lang/Math;->abs(I)I

    move-result v26

    move/from16 v0, v26

    int-to-double v0, v0

    move-wide/from16 v26, v0

    const-wide/high16 v28, 0x4000000000000000L    # 2.0

    invoke-static/range {v26 .. v29}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v26

    add-double v24, v24, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v16

    .line 3891
    .restart local v16    # "nVideoWidth":D
    const/16 v24, 0x8

    aget-object v24, p3, v24

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v24, v0

    const/16 v25, 0x6

    aget-object v25, p3, v25

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v25, v0

    sub-int v24, v24, v25

    invoke-static/range {v24 .. v24}, Ljava/lang/Math;->abs(I)I

    move-result v24

    move/from16 v0, v24

    int-to-double v0, v0

    move-wide/from16 v24, v0

    const-wide/high16 v26, 0x4000000000000000L    # 2.0

    invoke-static/range {v24 .. v27}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v24

    const/16 v26, 0x8

    aget-object v26, p3, v26

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v26, v0

    const/16 v27, 0x6

    aget-object v27, p3, v27

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v27, v0

    sub-int v26, v26, v27

    invoke-static/range {v26 .. v26}, Ljava/lang/Math;->abs(I)I

    move-result v26

    move/from16 v0, v26

    int-to-double v0, v0

    move-wide/from16 v26, v0

    const-wide/high16 v28, 0x4000000000000000L    # 2.0

    invoke-static/range {v26 .. v29}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v26

    add-double v24, v24, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v14

    .line 3893
    .restart local v14    # "nVideoHeight":D
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v24

    move/from16 v0, v24

    int-to-double v10, v0

    .line 3894
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v24

    move/from16 v0, v24

    int-to-double v8, v0

    goto/16 :goto_5

    .line 3902
    .end local v7    # "e":Ljava/lang/ArithmeticException;
    :cond_5
    cmpg-double v24, v14, v8

    if-gez v24, :cond_6

    .line 3903
    const/16 v24, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->rcVideoPlayBtn:Landroid/graphics/Rect;

    goto/16 :goto_1

    .line 3907
    :cond_6
    if-eqz v18, :cond_0

    .line 3909
    sub-int v24, v21, v20

    div-int/lit8 v24, v24, 0x2

    add-int v24, v24, v20

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v25

    div-int/lit8 v25, v25, 0x2

    sub-int v12, v24, v25

    .line 3910
    .local v12, "nPosX":I
    sub-int v24, v23, v22

    div-int/lit8 v24, v24, 0x2

    add-int v24, v24, v22

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v25

    div-int/lit8 v25, v25, 0x2

    sub-int v13, v24, v25

    .line 3912
    .local v13, "nPosY":I
    int-to-float v0, v12

    move/from16 v24, v0

    int-to-float v0, v13

    move/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v24

    move/from16 v3, v25

    move-object/from16 v4, v26

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 3914
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->rcVideoPlayBtn:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    if-nez v24, :cond_7

    .line 3915
    new-instance v24, Landroid/graphics/Rect;

    invoke-direct/range {v24 .. v24}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->rcVideoPlayBtn:Landroid/graphics/Rect;

    .line 3916
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->rcVideoPlayBtn:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v25

    add-int v25, v25, v12

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v26

    add-int v26, v26, v13

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v12, v13, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 3918
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_1

    .line 3825
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getRotatePoint(Landroid/graphics/PointF;Landroid/graphics/PointF;I)Landroid/graphics/PointF;
    .locals 13
    .param p0, "pt"    # Landroid/graphics/PointF;
    .param p1, "center"    # Landroid/graphics/PointF;
    .param p2, "angle"    # I

    .prologue
    .line 3303
    iget v5, p0, Landroid/graphics/PointF;->x:F

    iget v6, p1, Landroid/graphics/PointF;->x:F

    sub-float v0, v5, v6

    .line 3304
    .local v0, "dx":F
    iget v5, p0, Landroid/graphics/PointF;->y:F

    iget v6, p1, Landroid/graphics/PointF;->y:F

    sub-float v1, v5, v6

    .line 3305
    .local v1, "dy":F
    const-wide v5, 0x400921fb54442d18L    # Math.PI

    int-to-double v7, p2

    mul-double/2addr v5, v7

    const-wide v7, 0x4066800000000000L    # 180.0

    div-double v2, v5, v7

    .line 3306
    .local v2, "nRadian":D
    new-instance v4, Landroid/graphics/PointF;

    invoke-direct {v4}, Landroid/graphics/PointF;-><init>()V

    .line 3308
    .local v4, "p":Landroid/graphics/PointF;
    iget v5, p1, Landroid/graphics/PointF;->x:F

    float-to-double v5, v5

    float-to-double v7, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v9

    mul-double/2addr v7, v9

    float-to-double v9, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v11

    mul-double/2addr v9, v11

    sub-double/2addr v7, v9

    add-double/2addr v5, v7

    double-to-float v5, v5

    iput v5, v4, Landroid/graphics/PointF;->x:F

    .line 3309
    iget v5, p1, Landroid/graphics/PointF;->y:F

    float-to-double v5, v5

    float-to-double v7, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v9

    mul-double/2addr v7, v9

    float-to-double v9, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v11

    mul-double/2addr v9, v11

    add-double/2addr v7, v9

    add-double/2addr v5, v7

    double-to-float v5, v5

    iput v5, v4, Landroid/graphics/PointF;->y:F

    .line 3311
    return-object v4
.end method

.method public static rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "b"    # Landroid/graphics/Bitmap;
    .param p1, "degrees"    # I

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 3715
    if-eqz p1, :cond_0

    if-eqz p0, :cond_0

    .line 3717
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 3718
    .local v5, "m":Landroid/graphics/Matrix;
    int-to-float v0, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-virtual {v5, v0, v1, v2}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 3721
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 3722
    .local v7, "b2":Landroid/graphics/Bitmap;
    if-eq p0, v7, :cond_0

    .line 3724
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 3725
    const/4 p0, 0x0

    .line 3726
    move-object p0, v7

    .line 3735
    .end local v5    # "m":Landroid/graphics/Matrix;
    .end local v7    # "b2":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-object p0

    .line 3730
    .restart local v5    # "m":Landroid/graphics/Matrix;
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public EvObjectProcfinalize()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 466
    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    .line 467
    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    .line 468
    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    .line 469
    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    .line 470
    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    .line 471
    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    .line 472
    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mView:Landroid/view/View;

    .line 473
    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    .line 474
    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mSheetAutofilterInfo:[I

    .line 475
    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 476
    return-void
.end method

.method public FlingObjCtrl()Ljava/lang/Boolean;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 3197
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    if-eqz v1, :cond_1

    .line 3199
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget-object v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 3202
    .local v0, "rect":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTouchDown:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTouchDown:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3203
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 3215
    .end local v0    # "rect":Landroid/graphics/Rect;
    :goto_0
    return-object v1

    .line 3207
    .restart local v0    # "rect":Landroid/graphics/Rect;
    :cond_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    if-lez v1, :cond_2

    .line 3208
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    .line 3212
    .end local v0    # "rect":Landroid/graphics/Rect;
    :cond_1
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    if-lez v1, :cond_2

    .line 3213
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    .line 3215
    :cond_2
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0
.end method

.method public IsInFilterRect(Landroid/graphics/Rect;II)Z
    .locals 1
    .param p1, "a_FilterRect"    # Landroid/graphics/Rect;
    .param p2, "nX"    # I
    .param p3, "nY"    # I

    .prologue
    .line 3156
    iget v0, p1, Landroid/graphics/Rect;->left:I

    if-gt v0, p2, :cond_0

    iget v0, p1, Landroid/graphics/Rect;->right:I

    if-gt p2, v0, :cond_0

    iget v0, p1, Landroid/graphics/Rect;->top:I

    if-gt v0, p3, :cond_0

    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    if-gt p3, v0, :cond_0

    .line 3158
    const/4 v0, 0x1

    .line 3159
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public LongPressObjCtrl(II)Ljava/lang/Boolean;
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 3186
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public checkFilterRect()Landroid/graphics/Rect;
    .locals 23

    .prologue
    .line 3116
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mView:Landroid/view/View;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    .line 3117
    .local v16, "resources":Landroid/content/res/Resources;
    const v18, 0x7f0201da

    .line 3118
    .local v18, "sheetFilterNormal":I
    const v19, 0x7f0201db

    .line 3119
    .local v19, "sheetFilterPressed":I
    const/4 v14, 0x0

    .line 3120
    .local v14, "nNativeArrIndex":I
    const/4 v6, 0x0

    .line 3121
    .local v6, "filterRect":Landroid/graphics/Rect;
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mSheetAutofilterInfo:[I

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v21, v0

    div-int/lit8 v21, v21, 0x4

    move/from16 v0, v21

    if-ge v9, v0, :cond_2

    .line 3122
    const/4 v4, 0x0

    .line 3123
    .local v4, "ctrlBitmap1":Landroid/graphics/Bitmap;
    const/4 v5, 0x0

    .line 3125
    .local v5, "ctrlBitmap2":Landroid/graphics/Bitmap;
    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 3126
    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 3128
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mSheetAutofilterInfo:[I

    move-object/from16 v21, v0

    add-int/lit8 v15, v14, 0x1

    .end local v14    # "nNativeArrIndex":I
    .local v15, "nNativeArrIndex":I
    aget v11, v21, v14

    .line 3129
    .local v11, "left":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mSheetAutofilterInfo:[I

    move-object/from16 v21, v0

    add-int/lit8 v14, v15, 0x1

    .end local v15    # "nNativeArrIndex":I
    .restart local v14    # "nNativeArrIndex":I
    aget v20, v21, v15

    .line 3130
    .local v20, "top":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mSheetAutofilterInfo:[I

    move-object/from16 v21, v0

    add-int/lit8 v15, v14, 0x1

    .end local v14    # "nNativeArrIndex":I
    .restart local v15    # "nNativeArrIndex":I
    aget v17, v21, v14

    .line 3131
    .local v17, "right":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mSheetAutofilterInfo:[I

    move-object/from16 v21, v0

    add-int/lit8 v14, v15, 0x1

    .end local v15    # "nNativeArrIndex":I
    .restart local v14    # "nNativeArrIndex":I
    aget v3, v21, v15

    .line 3133
    .local v3, "bottom":I
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    .line 3134
    .local v10, "imageHeight":I
    const/4 v13, 0x0

    .line 3135
    .local v13, "margin":I
    sub-int v8, v3, v20

    .line 3137
    .local v8, "height":I
    if-ge v8, v10, :cond_0

    .line 3138
    new-instance v6, Landroid/graphics/Rect;

    .end local v6    # "filterRect":Landroid/graphics/Rect;
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v21

    sub-int v21, v17, v21

    move/from16 v0, v21

    move/from16 v1, v20

    move/from16 v2, v17

    invoke-direct {v6, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 3145
    .restart local v6    # "filterRect":Landroid/graphics/Rect;
    :goto_1
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 3146
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    .line 3148
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v21

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getXforPopup()I

    move-result v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v22

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getYforPopup()I

    move-result v22

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v6, v1, v2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->IsInFilterRect(Landroid/graphics/Rect;II)Z

    move-result v21

    if-eqz v21, :cond_1

    move-object v7, v6

    .line 3151
    .end local v3    # "bottom":I
    .end local v4    # "ctrlBitmap1":Landroid/graphics/Bitmap;
    .end local v5    # "ctrlBitmap2":Landroid/graphics/Bitmap;
    .end local v6    # "filterRect":Landroid/graphics/Rect;
    .end local v8    # "height":I
    .end local v10    # "imageHeight":I
    .end local v11    # "left":I
    .end local v13    # "margin":I
    .end local v17    # "right":I
    .end local v20    # "top":I
    .local v7, "filterRect":Landroid/graphics/Rect;
    :goto_2
    return-object v7

    .line 3141
    .end local v7    # "filterRect":Landroid/graphics/Rect;
    .restart local v3    # "bottom":I
    .restart local v4    # "ctrlBitmap1":Landroid/graphics/Bitmap;
    .restart local v5    # "ctrlBitmap2":Landroid/graphics/Bitmap;
    .restart local v6    # "filterRect":Landroid/graphics/Rect;
    .restart local v8    # "height":I
    .restart local v10    # "imageHeight":I
    .restart local v11    # "left":I
    .restart local v13    # "margin":I
    .restart local v17    # "right":I
    .restart local v20    # "top":I
    :cond_0
    sub-int v21, v8, v10

    div-int/lit8 v13, v21, 0x2

    .line 3142
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v21

    sub-int v12, v17, v21

    .line 3143
    .local v12, "leftPoint":I
    new-instance v6, Landroid/graphics/Rect;

    .end local v6    # "filterRect":Landroid/graphics/Rect;
    add-int v21, v20, v13

    sub-int v22, v3, v13

    move/from16 v0, v21

    move/from16 v1, v17

    move/from16 v2, v22

    invoke-direct {v6, v12, v0, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .restart local v6    # "filterRect":Landroid/graphics/Rect;
    goto :goto_1

    .line 3121
    .end local v12    # "leftPoint":I
    :cond_1
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .end local v3    # "bottom":I
    .end local v4    # "ctrlBitmap1":Landroid/graphics/Bitmap;
    .end local v5    # "ctrlBitmap2":Landroid/graphics/Bitmap;
    .end local v8    # "height":I
    .end local v10    # "imageHeight":I
    .end local v11    # "left":I
    .end local v13    # "margin":I
    .end local v17    # "right":I
    .end local v20    # "top":I
    :cond_2
    move-object v7, v6

    .line 3151
    .end local v6    # "filterRect":Landroid/graphics/Rect;
    .restart local v7    # "filterRect":Landroid/graphics/Rect;
    goto :goto_2
.end method

.method public checkObjectPoint(IILjava/lang/Boolean;)I
    .locals 21
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "bDown"    # Ljava/lang/Boolean;

    .prologue
    .line 2837
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2838
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTouchDown:Landroid/graphics/Point;

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 2842
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    packed-switch v3, :pswitch_data_0

    .line 2859
    :goto_1
    :pswitch_0
    new-instance v9, Landroid/graphics/Rect;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v9, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2860
    .local v9, "sDrawRect":Landroid/graphics/Rect;
    const/4 v15, 0x0

    .line 2861
    .local v15, "nSensitive":I
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    .line 2863
    .local v10, "bDraw":Ljava/lang/Boolean;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    packed-switch v3, :pswitch_data_1

    .line 3105
    :cond_0
    :goto_2
    :pswitch_1
    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3106
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/4 v4, 0x2

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-interface/range {v3 .. v9}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    .line 3111
    :cond_1
    const/4 v3, 0x0

    return v3

    .line 2840
    .end local v9    # "sDrawRect":Landroid/graphics/Rect;
    .end local v10    # "bDraw":Ljava/lang/Boolean;
    .end local v15    # "nSensitive":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTouchDown:Landroid/graphics/Point;

    const/4 v4, -0x1

    const/4 v5, -0x1

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 2854
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    const/4 v4, 0x0

    iput v4, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    goto :goto_1

    .line 2866
    .restart local v9    # "sDrawRect":Landroid/graphics/Rect;
    .restart local v10    # "bDraw":Ljava/lang/Boolean;
    .restart local v15    # "nSensitive":I
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    const/4 v4, 0x0

    iput v4, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    .line 2867
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2880
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    if-nez v3, :cond_0

    .line 2881
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    goto :goto_2

    .line 2885
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    const/4 v4, 0x1

    if-ge v3, v4, :cond_4

    .line 2887
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    goto :goto_2

    .line 2892
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    const/4 v4, 0x0

    iput v4, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    goto :goto_2

    .line 2899
    :pswitch_4
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2902
    new-instance v20, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startImageRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->barWidth:I

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x23

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startImageRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v4, v4, -0x37

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startImageRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->barWidth:I

    sub-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startImageRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, v20

    invoke-direct {v0, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2905
    .local v20, "sSRect":Landroid/graphics/Rect;
    new-instance v19, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endImageRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->barWidth:I

    add-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endImageRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endImageRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->barWidth:I

    add-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x23

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endImageRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    add-int/lit8 v6, v6, 0x37

    move-object/from16 v0, v19

    invoke-direct {v0, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2908
    .local v19, "sERect":Landroid/graphics/Rect;
    move-object/from16 v0, v20

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2909
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    const/4 v4, 0x1

    iput v4, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    .line 2910
    move-object/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto/16 :goto_2

    .line 2912
    :cond_5
    move-object/from16 v0, v19

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2913
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    const/4 v4, 0x2

    iput v4, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    .line 2914
    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto/16 :goto_2

    .line 2917
    :cond_6
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    goto/16 :goto_2

    .line 2921
    .end local v19    # "sERect":Landroid/graphics/Rect;
    .end local v20    # "sSRect":Landroid/graphics/Rect;
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    const/4 v4, 0x1

    if-ge v3, v4, :cond_8

    .line 2923
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    goto/16 :goto_2

    .line 2928
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    const/4 v4, 0x0

    iput v4, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    goto/16 :goto_2

    .line 2934
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->pointImageSize:I

    div-int/lit8 v3, v3, 0x2

    add-int/lit8 v15, v3, 0x5

    .line 2935
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 2938
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->nAdjustHandleCnt:I

    if-ge v13, v3, :cond_a

    .line 2939
    new-instance v17, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    aget-object v3, v3, v13

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int/2addr v3, v15

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    aget-object v4, v4, v13

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int/2addr v4, v15

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    aget-object v5, v5, v13

    iget v5, v5, Landroid/graphics/Point;->x:I

    add-int/2addr v5, v15

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    aget-object v6, v6, v13

    iget v6, v6, Landroid/graphics/Point;->y:I

    add-int/2addr v6, v15

    move-object/from16 v0, v17

    invoke-direct {v0, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2942
    .local v17, "rect":Landroid/graphics/Rect;
    move-object/from16 v0, v17

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 2944
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    add-int/lit8 v4, v13, 0xa

    iput v4, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    .line 2938
    :cond_9
    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    .line 2948
    .end local v17    # "rect":Landroid/graphics/Rect;
    :cond_a
    new-instance v20, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->startPoint:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int/2addr v3, v15

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->startPoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int/2addr v4, v15

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->startPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    add-int/2addr v5, v15

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->startPoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    add-int/2addr v6, v15

    move-object/from16 v0, v20

    invoke-direct {v0, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2950
    .restart local v20    # "sSRect":Landroid/graphics/Rect;
    new-instance v19, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->endPoint:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int/2addr v3, v15

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->endPoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int/2addr v4, v15

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->endPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    add-int/2addr v5, v15

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->endPoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    add-int/2addr v6, v15

    move-object/from16 v0, v19

    invoke-direct {v0, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2953
    .restart local v19    # "sERect":Landroid/graphics/Rect;
    move-object/from16 v0, v20

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 2954
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    const/4 v4, 0x1

    iput v4, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    .line 2955
    move-object/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto/16 :goto_2

    .line 2957
    :cond_b
    move-object/from16 v0, v19

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 2958
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    const/4 v4, 0x2

    iput v4, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    .line 2959
    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto/16 :goto_2

    .line 2962
    :cond_c
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    goto/16 :goto_2

    .line 2966
    .end local v13    # "i":I
    .end local v19    # "sERect":Landroid/graphics/Rect;
    .end local v20    # "sSRect":Landroid/graphics/Rect;
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    const/4 v4, 0x1

    if-ge v3, v4, :cond_e

    .line 2968
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    goto/16 :goto_2

    .line 2973
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    const/4 v4, 0x0

    iput v4, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    goto/16 :goto_2

    .line 2979
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_f

    .line 2980
    const/16 v15, 0x19

    .line 2984
    :goto_4
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_14

    .line 2985
    new-instance v18, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RightPoint:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int/2addr v3, v15

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RightPoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int/2addr v4, v15

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RightPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    add-int/2addr v5, v15

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RightPoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    add-int/2addr v6, v15

    move-object/from16 v0, v18

    invoke-direct {v0, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2987
    .local v18, "rightRect":Landroid/graphics/Rect;
    new-instance v11, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->BottomPoint:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int/2addr v3, v15

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->BottomPoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int/2addr v4, v15

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->BottomPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    add-int/2addr v5, v15

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->BottomPoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    add-int/2addr v6, v15

    invoke-direct {v11, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2989
    .local v11, "bottomRect":Landroid/graphics/Rect;
    new-instance v14, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->LTRegionPoint:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int/2addr v3, v15

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->LTRegionPoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int/2addr v4, v15

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->LTRegionPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    add-int/2addr v5, v15

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->LTRegionPoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    add-int/2addr v6, v15

    invoke-direct {v14, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2991
    .local v14, "ltRegionRect":Landroid/graphics/Rect;
    new-instance v16, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RBRegionPoint:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int/2addr v3, v15

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RBRegionPoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int/2addr v4, v15

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RBRegionPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    add-int/2addr v5, v15

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RBRegionPoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    add-int/2addr v6, v15

    move-object/from16 v0, v16

    invoke-direct {v0, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2994
    .local v16, "rbRegionRect":Landroid/graphics/Rect;
    move-object/from16 v0, v18

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 2995
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    const/4 v4, 0x1

    iput v4, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    .line 2996
    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto/16 :goto_2

    .line 2982
    .end local v11    # "bottomRect":Landroid/graphics/Rect;
    .end local v14    # "ltRegionRect":Landroid/graphics/Rect;
    .end local v16    # "rbRegionRect":Landroid/graphics/Rect;
    .end local v18    # "rightRect":Landroid/graphics/Rect;
    :cond_f
    const/16 v15, 0x14

    goto/16 :goto_4

    .line 2998
    .restart local v11    # "bottomRect":Landroid/graphics/Rect;
    .restart local v14    # "ltRegionRect":Landroid/graphics/Rect;
    .restart local v16    # "rbRegionRect":Landroid/graphics/Rect;
    .restart local v18    # "rightRect":Landroid/graphics/Rect;
    :cond_10
    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 2999
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    const/4 v4, 0x2

    iput v4, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    .line 3000
    invoke-virtual {v9, v11}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto/16 :goto_2

    .line 3002
    :cond_11
    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v14, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 3003
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    const/4 v4, 0x3

    iput v4, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    .line 3004
    invoke-virtual {v9, v14}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto/16 :goto_2

    .line 3006
    :cond_12
    move-object/from16 v0, v16

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 3007
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    const/4 v4, 0x4

    iput v4, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    .line 3008
    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto/16 :goto_2

    .line 3021
    :cond_13
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    goto/16 :goto_2

    .line 3025
    .end local v11    # "bottomRect":Landroid/graphics/Rect;
    .end local v14    # "ltRegionRect":Landroid/graphics/Rect;
    .end local v16    # "rbRegionRect":Landroid/graphics/Rect;
    .end local v18    # "rightRect":Landroid/graphics/Rect;
    :cond_14
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    const/4 v4, 0x1

    if-ge v3, v4, :cond_15

    .line 3026
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    goto/16 :goto_2

    .line 3028
    :cond_15
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    const/4 v4, 0x0

    iput v4, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    goto/16 :goto_2

    .line 3048
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->iconSize:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    div-int/lit8 v3, v3, 0x2

    add-int/lit8 v15, v3, 0x5

    .line 3049
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1d

    .line 3051
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    const/4 v4, 0x0

    iput v4, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    .line 3054
    const/4 v13, 0x0

    .restart local v13    # "i":I
    :goto_5
    const/16 v3, 0xa

    if-ge v13, v3, :cond_17

    .line 3055
    new-instance v17, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    aget-object v3, v3, v13

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int/2addr v3, v15

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    aget-object v4, v4, v13

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int/2addr v4, v15

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    aget-object v5, v5, v13

    iget v5, v5, Landroid/graphics/Point;->x:I

    add-int/2addr v5, v15

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    aget-object v6, v6, v13

    iget v6, v6, Landroid/graphics/Point;->y:I

    add-int/2addr v6, v15

    move-object/from16 v0, v17

    invoke-direct {v0, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 3058
    .restart local v17    # "rect":Landroid/graphics/Rect;
    move-object/from16 v0, v17

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_16

    .line 3060
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    add-int/lit8 v4, v13, 0xa

    iput v4, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    .line 3054
    :cond_16
    add-int/lit8 v13, v13, 0x1

    goto :goto_5

    .line 3064
    .end local v17    # "rect":Landroid/graphics/Rect;
    :cond_17
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    if-nez v3, :cond_19

    .line 3066
    const/4 v13, 0x1

    :goto_6
    const/16 v3, 0xa

    if-ge v13, v3, :cond_19

    .line 3068
    new-instance v17, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    aget-object v3, v3, v13

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int/2addr v3, v15

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    aget-object v4, v4, v13

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int/2addr v4, v15

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    aget-object v5, v5, v13

    iget v5, v5, Landroid/graphics/Point;->x:I

    add-int/2addr v5, v15

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    aget-object v6, v6, v13

    iget v6, v6, Landroid/graphics/Point;->y:I

    add-int/2addr v6, v15

    move-object/from16 v0, v17

    invoke-direct {v0, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 3071
    .restart local v17    # "rect":Landroid/graphics/Rect;
    move-object/from16 v0, v17

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 3073
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iput v13, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    .line 3066
    :cond_18
    add-int/lit8 v13, v13, 0x1

    goto :goto_6

    .line 3078
    .end local v17    # "rect":Landroid/graphics/Rect;
    :cond_19
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->bRotationEnabled:I

    if-nez v3, :cond_1a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    const/16 v4, 0x9

    if-ne v3, v4, :cond_1a

    .line 3079
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    const/4 v4, 0x0

    iput v4, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    .line 3082
    :cond_1a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    if-nez v3, :cond_1c

    .line 3084
    new-instance v12, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    invoke-direct {v12, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 3087
    .local v12, "centerRect":Landroid/graphics/Rect;
    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v12, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 3088
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->mIsMove:Z

    .line 3089
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    goto/16 :goto_2

    .line 3092
    :cond_1b
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    goto/16 :goto_2

    .line 3095
    .end local v12    # "centerRect":Landroid/graphics/Rect;
    :cond_1c
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    goto/16 :goto_2

    .line 3099
    .end local v13    # "i":I
    :cond_1d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->mIsMove:Z

    .line 3100
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    const/4 v4, 0x0

    iput v4, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    goto/16 :goto_2

    .line 2842
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 2863
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_6
        :pswitch_7
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_7
    .end packed-switch
.end method

.method public checkPopupShow(IIZ)Z
    .locals 10
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "bMultiSelectionMode"    # Z

    .prologue
    .line 3325
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mView:Landroid/view/View;

    check-cast v2, Lcom/infraware/office/baseframe/EvBaseView;

    .line 3327
    .local v2, "eba":Lcom/infraware/office/baseframe/EvBaseView;
    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    const/16 v7, 0x1f

    if-ne v6, v7, :cond_1

    .line 3329
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getMultiSelectRect()Landroid/graphics/Rect;

    move-result-object v4

    .line 3330
    .local v4, "rect":Landroid/graphics/Rect;
    if-eqz v4, :cond_0

    .line 3332
    invoke-virtual {v4, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 3333
    const/4 v6, 0x1

    .line 3421
    .end local v4    # "rect":Landroid/graphics/Rect;
    :goto_0
    return v6

    .line 3335
    .restart local v4    # "rect":Landroid/graphics/Rect;
    :cond_0
    const/4 v6, 0x0

    goto :goto_0

    .line 3339
    .end local v4    # "rect":Landroid/graphics/Rect;
    :cond_1
    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    if-eqz v6, :cond_3

    .line 3342
    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_2

    .line 3343
    const/4 v6, 0x0

    goto :goto_0

    .line 3344
    :cond_2
    const/4 v6, 0x1

    goto :goto_0

    .line 3346
    :cond_3
    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    const/16 v7, 0x11

    if-ne v6, v7, :cond_5

    .line 3349
    new-instance v1, Landroid/graphics/Point;

    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    iget-object v7, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v7, v7, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->x:I

    add-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x2

    iget-object v7, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v7, v7, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->y:I

    iget-object v8, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v8, v8, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->y:I

    add-int/2addr v7, v8

    div-int/lit8 v7, v7, 0x2

    invoke-direct {v1, v6, v7}, Landroid/graphics/Point;-><init>(II)V

    .line 3352
    .local v1, "center":Landroid/graphics/Point;
    new-instance v4, Landroid/graphics/Rect;

    iget v6, v1, Landroid/graphics/Point;->x:I

    add-int/lit8 v6, v6, -0x28

    iget v7, v1, Landroid/graphics/Point;->y:I

    add-int/lit8 v7, v7, -0x28

    iget v8, v1, Landroid/graphics/Point;->x:I

    add-int/lit8 v8, v8, 0x28

    iget v9, v1, Landroid/graphics/Point;->y:I

    add-int/lit8 v9, v9, 0x28

    invoke-direct {v4, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 3353
    .restart local v4    # "rect":Landroid/graphics/Rect;
    invoke-virtual {v4, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 3354
    const/4 v6, 0x0

    goto :goto_0

    .line 3356
    :cond_4
    const/4 v6, 0x1

    goto :goto_0

    .line 3358
    .end local v1    # "center":Landroid/graphics/Point;
    .end local v4    # "rect":Landroid/graphics/Rect;
    :cond_5
    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    const/4 v7, 0x2

    if-eq v6, v7, :cond_6

    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    const/4 v7, 0x2

    if-eq v6, v7, :cond_6

    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    const/4 v7, 0x4

    if-ne v6, v7, :cond_e

    .line 3361
    :cond_6
    new-instance v4, Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    add-int/lit8 v6, v6, -0x14

    iget-object v7, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget-object v7, v7, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->y:I

    add-int/lit8 v7, v7, -0x14

    iget-object v8, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget-object v8, v8, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->x:I

    add-int/lit8 v8, v8, 0x14

    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget-object v9, v9, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v9, v9, Landroid/graphics/Point;->y:I

    add-int/lit8 v9, v9, 0x14

    invoke-direct {v4, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 3364
    .restart local v4    # "rect":Landroid/graphics/Rect;
    invoke-virtual {v4, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 3366
    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_7

    .line 3368
    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTouchDown:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    add-int/lit8 v6, v6, -0xa

    iget-object v7, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTouchDown:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->y:I

    add-int/lit8 v7, v7, -0xa

    iget-object v8, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTouchDown:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->x:I

    add-int/lit8 v8, v8, 0xa

    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTouchDown:Landroid/graphics/Point;

    iget v9, v9, Landroid/graphics/Point;->y:I

    add-int/lit8 v9, v9, 0xa

    invoke-virtual {v4, v6, v7, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 3369
    invoke-virtual {v4, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-nez v6, :cond_7

    .line 3370
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 3373
    :cond_7
    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    const/4 v7, 0x4

    if-eq v6, v7, :cond_d

    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    const/16 v7, 0x8

    if-eq v6, v7, :cond_d

    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    const/4 v7, 0x5

    if-eq v6, v7, :cond_d

    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    const/16 v7, 0x11

    if-eq v6, v7, :cond_d

    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    const/16 v7, 0x12

    if-eq v6, v7, :cond_d

    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    const/16 v7, 0xd

    if-eq v6, v7, :cond_d

    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    const/16 v7, 0xa

    if-eq v6, v7, :cond_d

    iget v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mDocType:I

    const/4 v7, 0x2

    if-eq v6, v7, :cond_d

    .line 3382
    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_a

    .line 3384
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v6

    invoke-virtual {v6}, Lcom/infraware/office/evengine/EvInterface;->IGetBWPGrapAttrInfo_Editor()Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;

    move-result-object v0

    .line 3385
    .local v0, "attr_info":Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;
    iget v6, v0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->nShapeType:I

    const/16 v7, 0x70

    if-ne v6, v7, :cond_8

    .line 3386
    const/4 v6, 0x1

    goto/16 :goto_0

    .line 3390
    :cond_8
    iget v6, v0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->nShapeType:I

    const/16 v7, 0xc9

    if-ne v6, v7, :cond_9

    iget v6, v0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->nShapeStyle:I

    if-nez v6, :cond_9

    .line 3391
    const/4 v6, 0x1

    goto/16 :goto_0

    .line 3393
    :cond_9
    iget v6, v0, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->nShapeType:I

    if-nez v6, :cond_a

    .line 3394
    const/4 v6, 0x1

    goto/16 :goto_0

    .line 3397
    .end local v0    # "attr_info":Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;
    :cond_a
    if-nez p3, :cond_d

    .line 3400
    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseView;->getContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 3401
    .local v3, "ebva":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    const/4 v5, 0x1

    .line 3402
    .local v5, "showTosatMessage":Z
    invoke-virtual {v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->IsViewerMode()Z

    move-result v6

    if-eqz v6, :cond_b

    .line 3403
    const/4 v5, 0x0

    .line 3405
    :cond_b
    invoke-virtual {v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getIsEncryptDoc()Z

    move-result v6

    if-eqz v6, :cond_c

    .line 3406
    const/4 v5, 0x0

    .line 3408
    :cond_c
    const/4 v6, 0x1

    if-ne v5, v6, :cond_d

    .line 3409
    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f070319

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 3414
    .end local v3    # "ebva":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .end local v5    # "showTosatMessage":Z
    :cond_d
    const/4 v6, 0x1

    goto/16 :goto_0

    .line 3418
    .end local v4    # "rect":Landroid/graphics/Rect;
    :cond_e
    iget v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mDocType:I

    const/4 v7, 0x3

    if-ne v6, v7, :cond_f

    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    if-nez v6, :cond_f

    .line 3419
    const/4 v6, 0x1

    goto/16 :goto_0

    .line 3421
    :cond_f
    const/4 v6, 0x0

    goto/16 :goto_0
.end method

.method public drawBaseRectAngleVideo(Landroid/graphics/Canvas;Z)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "bMultiSelectionMode"    # Z

    .prologue
    .line 3924
    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 3926
    .local v2, "resources":Landroid/content/res/Resources;
    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v1, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    .line 3927
    .local v1, "p":[Landroid/graphics/Point;
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getVideoStatus()I

    move-result v3

    if-nez v3, :cond_1

    .line 3929
    invoke-virtual {p0, p1, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->drawTransBlueFillRect(Landroid/graphics/Canvas;[Landroid/graphics/Point;)V

    .line 3930
    invoke-virtual {p0, p1, v2, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->drawTransBlueEdgeLine(Landroid/graphics/Canvas;Landroid/content/res/Resources;[Landroid/graphics/Point;)Z

    move-result v0

    .line 3931
    .local v0, "bPass":Z
    if-eqz v0, :cond_1

    .line 3938
    .end local v0    # "bPass":Z
    :cond_0
    :goto_0
    return-void

    .line 3936
    :cond_1
    if-nez p2, :cond_0

    .line 3937
    invoke-direct {p0, p1, v2, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->drawVideoBtn(Landroid/graphics/Canvas;Landroid/content/res/Resources;[Landroid/graphics/Point;)V

    goto :goto_0
.end method

.method public drawObjectProc(Landroid/graphics/Canvas;Lcom/infraware/office/baseframe/EvBaseView;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "ebv"    # Lcom/infraware/office/baseframe/EvBaseView;

    .prologue
    .line 1151
    iget v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mDocType:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 1152
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->initVideoInfo(I)V

    .line 1154
    :cond_0
    const/4 v2, 0x0

    .line 1155
    .local v2, "saveClipRect":Landroid/graphics/Rect;
    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget-boolean v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->bClipEnable:Z

    if-eqz v3, :cond_1

    .line 1156
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v2

    .line 1157
    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->clipStartPoint:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->clipStartPoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    iget-object v5, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->clipEndPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->clipEndPoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    invoke-virtual {p1, v3, v4, v5, v6}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 1161
    :cond_1
    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1164
    .local v1, "resources":Landroid/content/res/Resources;
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 1165
    .local v0, "ebva":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getActionMode()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    .line 1166
    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mSheetAutofilterInfo:[I

    if-eqz v3, :cond_2

    .line 1212
    :cond_2
    :goto_0
    return-void

    .line 1171
    :cond_3
    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    packed-switch v3, :pswitch_data_0

    .line 1206
    :goto_1
    if-eqz v2, :cond_4

    .line 1207
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 1209
    :cond_4
    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mSheetAutofilterInfo:[I

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v3, v3, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->ISheetFilterIsRunning()Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .line 1174
    :pswitch_0
    invoke-direct {p0, p1, v1, p2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->drawBaseCell(Landroid/graphics/Canvas;Landroid/content/res/Resources;Lcom/infraware/office/baseframe/EvBaseView;)V

    goto :goto_1

    .line 1177
    :pswitch_1
    invoke-direct {p0, p1, v1, p2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->drawBaseTextMark(Landroid/graphics/Canvas;Landroid/content/res/Resources;Lcom/infraware/office/baseframe/EvBaseView;)V

    goto :goto_1

    .line 1180
    :pswitch_2
    invoke-direct {p0, p1, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->drawBaseRectAngle(Landroid/graphics/Canvas;Landroid/content/res/Resources;)V

    goto :goto_1

    .line 1184
    :pswitch_3
    invoke-direct {p0, p1, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->drawBaseLine(Landroid/graphics/Canvas;Landroid/content/res/Resources;)V

    goto :goto_1

    .line 1188
    :pswitch_4
    invoke-direct {p0, p1, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->drawBaseMulti(Landroid/graphics/Canvas;Landroid/content/res/Resources;)V

    goto :goto_1

    .line 1191
    :pswitch_5
    invoke-direct {p0, p1, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->drawInfraPenMulti(Landroid/graphics/Canvas;Landroid/content/res/Resources;)V

    goto :goto_1

    .line 1194
    :pswitch_6
    invoke-direct {p0, p1, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->drawBaseInfraPen(Landroid/graphics/Canvas;Landroid/content/res/Resources;)V

    goto :goto_1

    .line 1171
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method public drawSheetViewModeAutofilter(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 1216
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1218
    .local v0, "resources":Landroid/content/res/Resources;
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mSheetAutofilterInfo:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->ISheetFilterIsRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1221
    :cond_0
    return-void
.end method

.method public drawTransBlueEdgeLine(Landroid/graphics/Canvas;Landroid/content/res/Resources;[Landroid/graphics/Point;)Z
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "resources"    # Landroid/content/res/Resources;
    .param p3, "p"    # [Landroid/graphics/Point;

    .prologue
    .line 3970
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 3972
    .local v5, "paint1":Landroid/graphics/Paint;
    const/4 v7, 0x1

    .line 3973
    .local v7, "strokeHori":I
    const/4 v8, 0x1

    .line 3975
    .local v8, "strokeVert":I
    const v0, -0xe47f39

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 3977
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v1, 0x159

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v1, 0x168

    if-lt v0, v1, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v1, 0xf

    if-lt v0, v1, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v1, 0xa5

    if-le v0, v1, :cond_2

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v1, 0xb4

    if-lt v0, v1, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v1, 0xb4

    if-le v0, v1, :cond_6

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v1, 0xc3

    if-ge v0, v1, :cond_6

    .line 3981
    :cond_3
    const/4 v7, 0x2

    .line 3988
    :cond_4
    :goto_0
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_TEXTMARK_REVERSE()Z

    move-result v0

    if-nez v0, :cond_b

    .line 3990
    const v0, -0xe47f39

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 3992
    int-to-float v0, v7

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 3993
    const/4 v0, 0x1

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v1, v0

    const/4 v0, 0x1

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v2, v0

    const/4 v0, 0x2

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v3, v0

    const/4 v0, 0x2

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 3994
    const/4 v0, 0x3

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v1, v0

    const/4 v0, 0x3

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v2, v0

    const/4 v0, 0x4

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v3, v0

    const/4 v0, 0x4

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 3996
    int-to-float v0, v8

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 3997
    const/4 v0, 0x2

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v1, v0

    const/4 v0, 0x2

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v2, v0

    const/4 v0, 0x3

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v3, v0

    const/4 v0, 0x3

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 3998
    const/4 v0, 0x4

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v1, v0

    const/4 v0, 0x4

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v2, v0

    const/4 v0, 0x1

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v3, v0

    const/4 v0, 0x1

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 4030
    :goto_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->IsViewerMode()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 4031
    const/4 v5, 0x0

    .line 4032
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getVideoStatus()I

    move-result v0

    if-nez v0, :cond_5

    .line 4036
    invoke-direct {p0, p1, p2, p3}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->drawVideoBtn(Landroid/graphics/Canvas;Landroid/content/res/Resources;[Landroid/graphics/Point;)V

    .line 4038
    :cond_5
    const/4 v0, 0x1

    .line 4040
    :goto_2
    return v0

    .line 3982
    :cond_6
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v1, 0x4b

    if-le v0, v1, :cond_7

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v1, 0x5a

    if-lt v0, v1, :cond_a

    :cond_7
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v1, 0x5a

    if-le v0, v1, :cond_8

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v1, 0x69

    if-lt v0, v1, :cond_a

    :cond_8
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v1, 0xff

    if-le v0, v1, :cond_9

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v1, 0x10e

    if-lt v0, v1, :cond_a

    :cond_9
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v1, 0x10e

    if-le v0, v1, :cond_4

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    const/16 v1, 0x11d

    if-ge v0, v1, :cond_4

    .line 3986
    :cond_a
    const/4 v8, 0x2

    goto/16 :goto_0

    .line 4003
    :cond_b
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v6

    .line 4004
    .local v6, "oneDip":F
    const/4 v0, -0x1

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 4005
    const/high16 v0, 0x40400000    # 3.0f

    mul-float/2addr v0, v6

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 4006
    const/4 v0, 0x1

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    const/4 v1, 0x1

    aget-object v1, p3, v1

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    const/high16 v2, 0x3fc00000    # 1.5f

    mul-float/2addr v2, v6

    invoke-virtual {p1, v0, v1, v2, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 4007
    const/4 v0, 0x2

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    const/4 v1, 0x2

    aget-object v1, p3, v1

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    const/high16 v2, 0x3fc00000    # 1.5f

    mul-float/2addr v2, v6

    invoke-virtual {p1, v0, v1, v2, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 4008
    const/4 v0, 0x3

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    const/4 v1, 0x3

    aget-object v1, p3, v1

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    const/high16 v2, 0x3fc00000    # 1.5f

    mul-float/2addr v2, v6

    invoke-virtual {p1, v0, v1, v2, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 4009
    const/4 v0, 0x4

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    const/4 v1, 0x4

    aget-object v1, p3, v1

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    const/high16 v2, 0x3fc00000    # 1.5f

    mul-float/2addr v2, v6

    invoke-virtual {p1, v0, v1, v2, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 4011
    const/4 v0, 0x1

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v1, v0

    const/4 v0, 0x1

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v2, v0

    const/4 v0, 0x2

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v3, v0

    const/4 v0, 0x2

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 4012
    const/4 v0, 0x3

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v1, v0

    const/4 v0, 0x3

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v2, v0

    const/4 v0, 0x4

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v3, v0

    const/4 v0, 0x4

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 4013
    const/4 v0, 0x2

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v1, v0

    const/4 v0, 0x2

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v2, v0

    const/4 v0, 0x3

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v3, v0

    const/4 v0, 0x3

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 4014
    const/4 v0, 0x4

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v1, v0

    const/4 v0, 0x4

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v2, v0

    const/4 v0, 0x1

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v3, v0

    const/4 v0, 0x1

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 4016
    const v0, -0xce8d45

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 4017
    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr v0, v6

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 4019
    const/4 v0, 0x1

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    const/4 v1, 0x1

    aget-object v1, p3, v1

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1, v6, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 4020
    const/4 v0, 0x2

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    const/4 v1, 0x2

    aget-object v1, p3, v1

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1, v6, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 4021
    const/4 v0, 0x3

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    const/4 v1, 0x3

    aget-object v1, p3, v1

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1, v6, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 4022
    const/4 v0, 0x4

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    const/4 v1, 0x4

    aget-object v1, p3, v1

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1, v6, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 4024
    const/4 v0, 0x1

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v1, v0

    const/4 v0, 0x1

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v2, v0

    const/4 v0, 0x2

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v3, v0

    const/4 v0, 0x2

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 4025
    const/4 v0, 0x3

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v1, v0

    const/4 v0, 0x3

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v2, v0

    const/4 v0, 0x4

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v3, v0

    const/4 v0, 0x4

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 4026
    const/4 v0, 0x2

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v1, v0

    const/4 v0, 0x2

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v2, v0

    const/4 v0, 0x3

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v3, v0

    const/4 v0, 0x3

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 4027
    const/4 v0, 0x4

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v1, v0

    const/4 v0, 0x4

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v2, v0

    const/4 v0, 0x1

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v3, v0

    const/4 v0, 0x1

    aget-object v0, p3, v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 4040
    .end local v6    # "oneDip":F
    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_2
.end method

.method public drawTransBlueFillRect(Landroid/graphics/Canvas;[Landroid/graphics/Point;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "p"    # [Landroid/graphics/Point;

    .prologue
    const/16 v9, 0x77

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 3942
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_TEXTMARK_REVERSE()Z

    move-result v2

    if-nez v2, :cond_0

    .line 3944
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 3945
    .local v1, "paintFill":Landroid/graphics/Paint;
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 3947
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_1

    .line 3948
    const/16 v2, 0x40

    const/16 v3, 0xb5

    const/16 v4, 0xf0

    invoke-virtual {v1, v2, v9, v3, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 3952
    :goto_0
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 3953
    .local v0, "fillPath":Landroid/graphics/Path;
    sget-object v2, Landroid/graphics/Path$FillType;->WINDING:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, v2}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 3954
    aget-object v2, p2, v5

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    aget-object v3, p2, v5

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 3955
    aget-object v2, p2, v6

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    aget-object v3, p2, v6

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 3956
    aget-object v2, p2, v7

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    aget-object v3, p2, v7

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 3957
    aget-object v2, p2, v8

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    aget-object v3, p2, v8

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 3958
    aget-object v2, p2, v5

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    aget-object v3, p2, v5

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 3959
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 3961
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 3963
    const/4 v1, 0x0

    .line 3966
    .end local v0    # "fillPath":Landroid/graphics/Path;
    .end local v1    # "paintFill":Landroid/graphics/Paint;
    :cond_0
    return-void

    .line 3950
    .restart local v1    # "paintFill":Landroid/graphics/Paint;
    :cond_1
    const/16 v2, 0x30

    const/16 v3, 0xb5

    const/16 v4, 0xf0

    invoke-virtual {v1, v2, v9, v3, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto :goto_0
.end method

.method public drawVideo(Landroid/graphics/Canvas;Z)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "bMultiSelectionMode"    # Z

    .prologue
    .line 1137
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    packed-switch v0, :pswitch_data_0

    .line 1148
    :goto_0
    return-void

    .line 1142
    :pswitch_0
    invoke-virtual {p0, p1, p2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->drawBaseRectAngleVideo(Landroid/graphics/Canvas;Z)V

    goto :goto_0

    .line 1137
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public getGrapAttInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;
    .locals 1

    .prologue
    .line 3283
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mGrapAttInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;

    return-object v0
.end method

.method public getMultiInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;
    .locals 1

    .prologue
    .line 3432
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    return-object v0
.end method

.method public getMultiSelectRect()Landroid/graphics/Rect;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 3452
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v2, v2, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 3454
    .local v1, "rect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_5

    .line 3456
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->nObjectCount:I

    if-ge v0, v2, :cond_5

    .line 3458
    if-nez v0, :cond_1

    .line 3460
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    iget-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v4, v4, v0

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    iget-object v5, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v5, v5, v0

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 3456
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3465
    :cond_1
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget v3, v1, Landroid/graphics/Rect;->left:I

    if-ge v2, v3, :cond_2

    .line 3466
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 3467
    :cond_2
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget v3, v1, Landroid/graphics/Rect;->top:I

    if-ge v2, v3, :cond_3

    .line 3468
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 3470
    :cond_3
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget v3, v1, Landroid/graphics/Rect;->right:I

    if-le v2, v3, :cond_4

    .line 3471
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 3472
    :cond_4
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    if-le v2, v3, :cond_0

    .line 3473
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    goto :goto_1

    .line 3477
    .end local v0    # "i":I
    :cond_5
    return-object v1
.end method

.method public getObecjtCenter()Landroid/graphics/Point;
    .locals 4

    .prologue
    .line 3295
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 3296
    .local v0, "centerPoint":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->startSelectPoint:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->endSelectPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->startSelectPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->endSelectPoint:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 3298
    return-object v0
.end method

.method public getObjectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    .locals 1

    .prologue
    .line 3315
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    return-object v0
.end method

.method public getObjectSize()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 3290
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->sObjectSize:Landroid/graphics/Point;

    return-object v0
.end method

.method public getObjectType()I
    .locals 1

    .prologue
    .line 3286
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    return v0
.end method

.method public getPlaceHolderIcon(II)I
    .locals 7
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v2, 0x1

    .line 3739
    new-instance v0, Landroid/graphics/Point;

    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    add-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    iget-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    iget-object v5, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v5, v5, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    add-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    invoke-direct {v0, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    .line 3742
    .local v0, "center":Landroid/graphics/Point;
    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    const/16 v4, 0x11

    if-ne v3, v4, :cond_1

    .line 3743
    new-instance v1, Landroid/graphics/Rect;

    iget v3, v0, Landroid/graphics/Point;->x:I

    add-int/lit8 v3, v3, -0x28

    iget v4, v0, Landroid/graphics/Point;->y:I

    add-int/lit8 v4, v4, -0x28

    iget v5, v0, Landroid/graphics/Point;->x:I

    add-int/lit8 v5, v5, 0x28

    iget v6, v0, Landroid/graphics/Point;->y:I

    add-int/lit8 v6, v6, 0x28

    invoke-direct {v1, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 3744
    .local v1, "rect":Landroid/graphics/Rect;
    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 3762
    .end local v1    # "rect":Landroid/graphics/Rect;
    :cond_0
    :goto_0
    return v2

    .line 3747
    :cond_1
    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    const/16 v4, 0x12

    if-ne v3, v4, :cond_4

    .line 3748
    new-instance v1, Landroid/graphics/Rect;

    iget v3, v0, Landroid/graphics/Point;->x:I

    add-int/lit8 v3, v3, -0x50

    iget v4, v0, Landroid/graphics/Point;->y:I

    add-int/lit8 v4, v4, -0x50

    iget v5, v0, Landroid/graphics/Point;->x:I

    iget v6, v0, Landroid/graphics/Point;->y:I

    invoke-direct {v1, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 3749
    .restart local v1    # "rect":Landroid/graphics/Rect;
    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-nez v3, :cond_0

    .line 3751
    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v3, v0, Landroid/graphics/Point;->y:I

    add-int/lit8 v3, v3, -0x50

    iget v4, v0, Landroid/graphics/Point;->x:I

    add-int/lit8 v4, v4, 0x50

    iget v5, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 3752
    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3753
    const/4 v2, 0x2

    goto :goto_0

    .line 3754
    :cond_2
    iget v2, v0, Landroid/graphics/Point;->x:I

    add-int/lit8 v2, v2, -0x50

    iget v3, v0, Landroid/graphics/Point;->y:I

    iget v4, v0, Landroid/graphics/Point;->x:I

    iget v5, v0, Landroid/graphics/Point;->y:I

    add-int/lit8 v5, v5, 0x50

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 3755
    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3756
    const/4 v2, 0x3

    goto :goto_0

    .line 3757
    :cond_3
    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v3, v0, Landroid/graphics/Point;->y:I

    iget v4, v0, Landroid/graphics/Point;->x:I

    add-int/lit8 v4, v4, 0x50

    iget v5, v0, Landroid/graphics/Point;->y:I

    add-int/lit8 v5, v5, 0x50

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 3758
    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 3759
    const/4 v2, 0x4

    goto :goto_0

    .line 3762
    .end local v1    # "rect":Landroid/graphics/Rect;
    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getRectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;
    .locals 1

    .prologue
    .line 3426
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    return-object v0
.end method

.method public getRotateAngle()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3483
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 3484
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    .line 3490
    :cond_0
    :goto_0
    return v0

    .line 3485
    :cond_1
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    .line 3486
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->rotateAngle:I

    goto :goto_0

    .line 3487
    :cond_2
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    .line 3488
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v0, v1, v0

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->rotateAngle:I

    goto :goto_0
.end method

.method public getSheetFormulaRangeCount()I
    .locals 1

    .prologue
    .line 3711
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->m_nSheetFormulaRangeCount:I

    return v0
.end method

.method public getStatusBarHeight()I
    .locals 3

    .prologue
    .line 2830
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 2831
    .local v0, "rect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 2832
    .local v1, "wnd":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 2833
    iget v2, v0, Landroid/graphics/Rect;->top:I

    return v2
.end method

.method public getTextMarkInfo_for_popup()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;
    .locals 1

    .prologue
    .line 3767
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    return-object v0
.end method

.method public getVideoPlayBtnRc()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 3276
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->rcVideoPlayBtn:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    .line 3277
    const/4 v0, 0x0

    .line 3279
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->rcVideoPlayBtn:Landroid/graphics/Rect;

    goto :goto_0
.end method

.method public getVideoStatus()I
    .locals 1

    .prologue
    .line 3780
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getVideoManager()Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3781
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getVideoManager()Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->getVideoStatus()I

    move-result v0

    .line 3783
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initVideoInfo(I)V
    .locals 1
    .param p1, "nStatus"    # I

    .prologue
    .line 3788
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->rcVideoPlayBtn:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    .line 3790
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onStopVideo()Z

    .line 3792
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->rcVideoPlayBtn:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 3793
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->rcVideoPlayBtn:Landroid/graphics/Rect;

    .line 3795
    :cond_0
    return-void
.end method

.method public isCellObjMarkArea(II)Z
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 3262
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    packed-switch v1, :pswitch_data_0

    .line 3272
    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 3266
    :pswitch_0
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->startSelectPoint:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->startSelectPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->endSelectPoint:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v4, v4, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->endSelectPoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 3268
    .local v0, "sRect":Landroid/graphics/Rect;
    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3269
    const/4 v1, 0x1

    goto :goto_0

    .line 3262
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public isGroup()Z
    .locals 2

    .prologue
    .line 3627
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 3628
    const/4 v0, 0x1

    .line 3630
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isGroupEnable()Z
    .locals 1

    .prologue
    .line 3634
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->isGroupEnable()Z

    move-result v0

    return v0
.end method

.method public isGroupInMulti()Z
    .locals 3

    .prologue
    .line 3613
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    const/16 v2, 0x1f

    if-ne v1, v2, :cond_1

    .line 3615
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->nObjectCount:I

    if-ge v0, v1, :cond_1

    .line 3617
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->mObjectType:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_0

    .line 3618
    const/4 v1, 0x1

    .line 3622
    .end local v0    # "i":I
    :goto_1
    return v1

    .line 3615
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3622
    .end local v0    # "i":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public isImageInMulti()Z
    .locals 3

    .prologue
    .line 3637
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    const/16 v2, 0x1f

    if-ne v1, v2, :cond_1

    .line 3639
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->nObjectCount:I

    if-ge v0, v1, :cond_1

    .line 3641
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->mObjectType:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    .line 3642
    const/4 v1, 0x1

    .line 3646
    .end local v0    # "i":I
    :goto_1
    return v1

    .line 3639
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3646
    .end local v0    # "i":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public isMultiLine()Z
    .locals 3

    .prologue
    .line 3602
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->nObjectCount:I

    if-ge v0, v1, :cond_1

    .line 3604
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->mObjectType:I

    const/16 v2, 0x9

    if-eq v1, v2, :cond_0

    .line 3605
    const/4 v1, 0x0

    .line 3608
    :goto_1
    return v1

    .line 3602
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3608
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public isMultiShape()Z
    .locals 3

    .prologue
    .line 3591
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->nObjectCount:I

    if-ge v0, v1, :cond_1

    .line 3593
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->mObjectType:I

    const/4 v2, 0x6

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->mObjectType:I

    const/4 v2, 0x7

    if-eq v1, v2, :cond_0

    .line 3595
    const/4 v1, 0x0

    .line 3598
    :goto_1
    return v1

    .line 3591
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3598
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public isPointInObject(II)Z
    .locals 11
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 3500
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 3501
    .local v1, "rect":Landroid/graphics/Rect;
    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    packed-switch v6, :pswitch_data_0

    :cond_0
    :goto_0
    move v4, v5

    .line 3578
    :cond_1
    :goto_1
    return v4

    .line 3505
    :pswitch_0
    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    packed-switch v6, :pswitch_data_1

    goto :goto_0

    .line 3516
    :pswitch_1
    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->startSelectPoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    iget-object v7, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v7, v7, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfIconSize:I

    sub-int/2addr v6, v7

    iget-object v7, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v7, v7, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->startSelectPoint:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->y:I

    iget-object v8, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v8, v8, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfIconSize:I

    sub-int/2addr v7, v8

    iget-object v8, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v8, v8, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->endSelectPoint:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->x:I

    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v9, v9, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfIconSize:I

    add-int/2addr v8, v9

    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v9, v9, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->endSelectPoint:Landroid/graphics/Point;

    iget v9, v9, Landroid/graphics/Point;->y:I

    iget-object v10, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v10, v10, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfIconSize:I

    add-int/2addr v9, v10

    invoke-virtual {v1, v6, v7, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 3518
    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-nez v6, :cond_1

    move v4, v5

    .line 3520
    goto :goto_1

    .line 3508
    :pswitch_2
    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->startSelectPoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    iget-object v7, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v7, v7, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfIconSize:I

    sub-int/2addr v6, v7

    iget-object v7, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v7, v7, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->startSelectPoint:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->y:I

    iget-object v8, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v8, v8, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfIconSize:I

    sub-int/2addr v7, v8

    iget-object v8, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v8, v8, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->endSelectPoint:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->x:I

    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v9, v9, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfIconSize:I

    add-int/2addr v8, v9

    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget-object v9, v9, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->endSelectPoint:Landroid/graphics/Point;

    iget v9, v9, Landroid/graphics/Point;->y:I

    iget-object v10, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    iget v10, v10, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfIconSize:I

    add-int/2addr v9, v10

    invoke-virtual {v1, v6, v7, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 3510
    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-nez v6, :cond_1

    move v4, v5

    .line 3513
    goto :goto_1

    .line 3532
    :pswitch_3
    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v3, v6, Landroid/graphics/Point;->y:I

    .line 3533
    .local v3, "selTop":I
    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v2, v6, Landroid/graphics/Point;->y:I

    .line 3535
    .local v2, "selBottom":I
    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    iget-object v7, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget-object v7, v7, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->x:I

    invoke-virtual {v1, v6, v3, v7, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 3542
    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-nez v6, :cond_1

    move v4, v5

    .line 3544
    goto/16 :goto_1

    .line 3548
    .end local v2    # "selBottom":I
    .end local v3    # "selTop":I
    :pswitch_4
    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    iget-object v7, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v7, v7, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->y:I

    iget-object v8, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v8, v8, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->x:I

    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iget-object v9, v9, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v9, v9, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v6, v7, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 3550
    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-nez v6, :cond_1

    move v4, v5

    .line 3552
    goto/16 :goto_1

    .line 3556
    :pswitch_5
    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    iget-object v7, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget-object v7, v7, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->y:I

    iget-object v8, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget-object v8, v8, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->x:I

    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget-object v9, v9, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v9, v9, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v6, v7, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 3558
    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-nez v6, :cond_1

    move v4, v5

    .line 3560
    goto/16 :goto_1

    .line 3563
    :pswitch_6
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->nObjectCount:I

    if-ge v0, v6, :cond_0

    .line 3565
    if-nez v0, :cond_2

    .line 3567
    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v6, v6, v0

    iget-object v6, v6, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    iget-object v7, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v7, v7, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->startRangePoint:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->y:I

    iget-object v8, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v8, v8, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v8, v8, v0

    iget-object v8, v8, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->x:I

    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    iget-object v9, v9, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v9, v9, v0

    iget-object v9, v9, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->endRangePoint:Landroid/graphics/Point;

    iget v9, v9, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v6, v7, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 3570
    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-nez v6, :cond_1

    .line 3563
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 3501
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 3505
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public isPointInObject(Landroid/graphics/Point;)Z
    .locals 2
    .param p1, "pt"    # Landroid/graphics/Point;

    .prologue
    .line 3494
    iget v0, p1, Landroid/graphics/Point;->x:I

    iget v1, p1, Landroid/graphics/Point;->y:I

    invoke-virtual {p0, v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->isPointInObject(II)Z

    move-result v0

    return v0
.end method

.method public moveCaret(Lcom/infraware/office/evengine/EvInterface;II)V
    .locals 9
    .param p1, "eif"    # Lcom/infraware/office/evengine/EvInterface;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    const/4 v1, 0x0

    .line 3690
    add-int/lit8 v3, p3, -0x3c

    const/16 v4, 0x3e8

    move-object v0, p1

    move v2, p2

    move v5, v1

    move v6, v1

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 3691
    const/4 v3, 0x2

    add-int/lit8 v5, p3, -0x3c

    move-object v2, p1

    move v4, p2

    move v6, v1

    move v7, v1

    move v8, v1

    invoke-virtual/range {v2 .. v8}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 3692
    return-void
.end method

.method public setDocType(I)V
    .locals 0
    .param p1, "editModeType"    # I

    .prologue
    .line 3320
    iput p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mDocType:I

    .line 3322
    return-void
.end method

.method public setFastMoveAction()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3584
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iput v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    .line 3585
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    iput-boolean v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->mIsMove:Z

    .line 3586
    return-void
.end method

.method public setObjectInfo(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V
    .locals 44
    .param p1, "object"    # Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;

    .prologue
    .line 556
    if-nez p1, :cond_1

    .line 557
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->unsetObjetInfo()V

    .line 1133
    :cond_0
    :goto_0
    return-void

    .line 562
    :cond_1
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nObjectType:I

    move/from16 v39, v0

    move/from16 v0, v39

    and-int/lit16 v15, v0, 0xfff

    .line 563
    .local v15, "obj_type":I
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->GetObjectBaseType(I)I

    move-result v6

    .line 565
    .local v6, "base_type":I
    move-object/from16 v0, p1

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->nMultiObj:I

    move/from16 v39, v0

    const/16 v40, 0x1

    move/from16 v0, v39

    move/from16 v1, v40

    if-le v0, v1, :cond_2

    .line 567
    const/16 v39, 0x6

    move/from16 v0, v39

    if-ne v6, v0, :cond_3

    .line 569
    const/4 v6, 0x7

    .line 570
    const/16 v15, 0x1f

    .line 579
    :cond_2
    :goto_1
    if-nez v6, :cond_4

    .line 581
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    .line 585
    .local v5, "backuptype":I
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->unsetObjetInfo()V

    goto :goto_0

    .line 574
    .end local v5    # "backuptype":I
    :cond_3
    const/4 v6, 0x5

    .line 575
    const/16 v15, 0x1f

    goto :goto_1

    .line 606
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    move/from16 v39, v0

    move/from16 v0, v39

    if-eq v0, v6, :cond_5

    .line 608
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->unsetObjetInfo()V

    .line 611
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iput v15, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    .line 612
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iput v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    .line 614
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startPoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startPoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 615
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->endPoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->endPoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 616
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->sObjectSize:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->sObjectSize:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->sObjectSize:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 618
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->eEditing:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->eEditing:I

    .line 619
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->editStartPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingStartPoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingStartPoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 620
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->editEndPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingEndPoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingEndPoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 622
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->clipStartPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->clipStart:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->clipStart:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 623
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->clipEndPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->clipEnd:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->clipEnd:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 624
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->bClipEnable:I

    move/from16 v39, v0

    if-nez v39, :cond_9

    const/16 v39, 0x0

    :goto_2
    move/from16 v0, v39

    move-object/from16 v1, v40

    iput-boolean v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->bClipEnable:Z

    .line 625
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->bGroupEnabled:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->bGroupEnabled:I

    .line 627
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mGrapAttInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-boolean v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->bDML:Z

    move/from16 v40, v0

    invoke-virtual/range {v39 .. v40}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->setDML(Z)V

    .line 628
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mGrapAttInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-boolean v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->bSupport3D:Z

    move/from16 v40, v0

    invoke-virtual/range {v39 .. v40}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->setSupport3D(Z)V

    .line 629
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mGrapAttInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-boolean v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->b2007DocxVML:Z

    move/from16 v40, v0

    invoke-virtual/range {v39 .. v40}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->set2007DocxVML(Z)V

    .line 630
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mGrapAttInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-boolean v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->bSupport3DBevel:Z

    move/from16 v40, v0

    invoke-virtual/range {v39 .. v40}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->setSupport3DBevel(Z)V

    .line 631
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mGrapAttInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-boolean v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->b3D:Z

    move/from16 v40, v0

    invoke-virtual/range {v39 .. v40}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->set3D(Z)V

    .line 633
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    move/from16 v39, v0

    packed-switch v39, :pswitch_data_0

    goto/16 :goto_0

    .line 637
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->startSelectPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 638
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->endSelectPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 643
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    move/from16 v39, v0

    sparse-switch v39, :sswitch_data_0

    goto/16 :goto_0

    .line 685
    :sswitch_0
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    aget-object v39, v39, v40

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->nObjectType:I

    move/from16 v39, v0

    packed-switch v39, :pswitch_data_1

    .line 700
    :pswitch_1
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v39, v0

    const/16 v40, 0x1

    aget-object v39, v39, v40

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->nObjectType:I

    move/from16 v39, v0

    const/16 v40, 0x6

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_f

    .line 701
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RBRegionPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    const/16 v41, 0x0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 707
    :cond_6
    :goto_3
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v39, v0

    const/16 v40, 0x1

    aget-object v39, v39, v40

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->nObjectType:I

    move/from16 v39, v0

    packed-switch v39, :pswitch_data_2

    .line 722
    :pswitch_2
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    aget-object v39, v39, v40

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->nObjectType:I

    move/from16 v39, v0

    const/16 v40, 0x6

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_10

    .line 723
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RBRegionPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    const/16 v41, 0x0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 730
    :cond_7
    :goto_4
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    aget-object v39, v39, v40

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->nObjectType:I

    move/from16 v39, v0

    const/16 v40, 0x4

    move/from16 v0, v39

    move/from16 v1, v40

    if-eq v0, v1, :cond_8

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v39, v0

    const/16 v40, 0x1

    aget-object v39, v39, v40

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->nObjectType:I

    move/from16 v39, v0

    const/16 v40, 0x4

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_11

    .line 731
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    const/16 v40, 0x1

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput-boolean v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->bLTAutoFill:Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    const/16 v40, 0x1

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput-boolean v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->bRBAutoFill:Z

    goto/16 :goto_0

    .line 624
    :cond_9
    const/16 v39, 0x1

    goto/16 :goto_2

    .line 647
    :sswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->LTRegionPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->endSelectPoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->endSelectPoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 649
    move-object/from16 v0, p1

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->nObjPointCnt:I

    move/from16 v39, v0

    if-eqz v39, :cond_c

    .line 652
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RightPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    aget-object v40, v40, v41

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v41, v0

    const/16 v42, 0x0

    aget-object v41, v41, v42

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 658
    :goto_5
    move-object/from16 v0, p1

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->nObjPointCnt:I

    move/from16 v39, v0

    if-eqz v39, :cond_d

    .line 661
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->BottomPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v40, v0

    const/16 v41, 0x1

    aget-object v40, v40, v41

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v41, v0

    const/16 v42, 0x1

    aget-object v41, v41, v42

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 669
    :goto_6
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nObjectType:I

    move/from16 v39, v0

    move/from16 v0, v39

    and-int/lit16 v0, v0, 0x1000

    move/from16 v39, v0

    if-eqz v39, :cond_a

    .line 670
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    const/16 v40, 0x3

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    .line 672
    :cond_a
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nMarkingRectCount:I

    move/from16 v32, v0

    .line 673
    .local v32, "rect_count":I
    if-lez v32, :cond_e

    .line 674
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v39, 0x258

    move/from16 v0, v32

    move/from16 v1, v39

    if-le v0, v1, :cond_b

    .line 675
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v32, 0x258

    .line 676
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v40

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->mCellRectInfos:[S

    move-object/from16 v41, v0

    mul-int/lit8 v42, v32, 0x4

    invoke-virtual/range {v40 .. v42}, Lcom/infraware/office/evengine/EvInterface;->IGetCellMarkRectInfo([SI)I

    move-result v40

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->nCellRectCount:I

    goto/16 :goto_0

    .line 655
    .end local v32    # "rect_count":I
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RightPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, -0x64

    const/16 v41, -0x64

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_5

    .line 664
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->BottomPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, -0x64

    const/16 v41, -0x64

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_6

    .line 679
    .restart local v32    # "rect_count":I
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->nCellRectCount:I

    goto/16 :goto_0

    .line 687
    .end local v32    # "rect_count":I
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->LTRegionPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    const/16 v41, 0x0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 688
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RBRegionPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    const/16 v41, 0x0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_3

    .line 691
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RBRegionPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    aget-object v40, v40, v41

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v41, v0

    const/16 v42, 0x0

    aget-object v41, v41, v42

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_3

    .line 694
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->LTRegionPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    aget-object v40, v40, v41

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v41, v0

    const/16 v42, 0x0

    aget-object v41, v41, v42

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_3

    .line 697
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RBRegionPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    aget-object v40, v40, v41

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v41, v0

    const/16 v42, 0x0

    aget-object v41, v41, v42

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_3

    .line 702
    :cond_f
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v39, v0

    const/16 v40, 0x1

    aget-object v39, v39, v40

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->nObjectType:I

    move/from16 v39, v0

    const/16 v40, 0x7

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_6

    .line 703
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->LTRegionPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    const/16 v41, 0x0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_3

    .line 709
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->LTRegionPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    const/16 v41, 0x0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 710
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RBRegionPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    const/16 v41, 0x0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_4

    .line 713
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RBRegionPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v40, v0

    const/16 v41, 0x1

    aget-object v40, v40, v41

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v41, v0

    const/16 v42, 0x1

    aget-object v41, v41, v42

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_4

    .line 716
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->LTRegionPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v40, v0

    const/16 v41, 0x1

    aget-object v40, v40, v41

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v41, v0

    const/16 v42, 0x1

    aget-object v41, v41, v42

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_4

    .line 719
    :pswitch_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RBRegionPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v40, v0

    const/16 v41, 0x1

    aget-object v40, v40, v41

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v41, v0

    const/16 v42, 0x1

    aget-object v41, v41, v42

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_4

    .line 724
    :cond_10
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    aget-object v39, v39, v40

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->nObjectType:I

    move/from16 v39, v0

    const/16 v40, 0x7

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_7

    .line 725
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->LTRegionPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    const/16 v41, 0x0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_4

    .line 733
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput-boolean v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->bLTAutoFill:Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput-boolean v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->bRBAutoFill:Z

    goto/16 :goto_0

    .line 738
    :sswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->BottomPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    aget-object v40, v40, v41

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v41, v0

    const/16 v42, 0x0

    aget-object v41, v41, v42

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_0

    .line 741
    :sswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RightPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    aget-object v40, v40, v41

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v41, v0

    const/16 v42, 0x0

    aget-object v41, v41, v42

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_0

    .line 746
    :sswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_12

    .line 748
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    add-int/lit8 v40, v40, -0x2

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Landroid/graphics/Point;->x:I

    .line 749
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    add-int/lit8 v40, v40, -0x2

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Landroid/graphics/Point;->x:I

    .line 752
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_13

    .line 754
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    add-int/lit8 v40, v40, -0x2

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Landroid/graphics/Point;->y:I

    .line 755
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    add-int/lit8 v40, v40, -0x2

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Landroid/graphics/Point;->y:I

    .line 758
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->startSelectPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 759
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->endSelectPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_0

    .line 764
    :sswitch_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->startSelectPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 765
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->endSelectPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_0

    .line 774
    :pswitch_b
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v36, v0

    .line 775
    .local v36, "startX":I
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v37, v0

    .line 776
    .local v37, "startY":I
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startMarkDirection:I

    move/from16 v34, v0

    .line 777
    .local v34, "startDirection":I
    const/16 v39, 0x3

    move/from16 v0, v34

    move/from16 v1, v39

    if-ne v0, v1, :cond_16

    .line 778
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startImageRect:Landroid/graphics/Rect;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->iconSize:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    sub-int v40, v37, v40

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->barWidth:I

    move/from16 v41, v0

    add-int v40, v40, v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->iconSize:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    add-int v41, v41, v36

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->barWidth:I

    move/from16 v42, v0

    add-int v42, v42, v37

    move-object/from16 v0, v39

    move/from16 v1, v36

    move/from16 v2, v40

    move/from16 v3, v41

    move/from16 v4, v42

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 780
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    const v40, 0x7f020327

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startNormalId:I

    .line 781
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    const v40, 0x7f020328

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startPressedId:I

    .line 802
    :goto_7
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->endPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v11, v0, Landroid/graphics/Point;->x:I

    .line 803
    .local v11, "endX":I
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->endPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v12, v0, Landroid/graphics/Point;->y:I

    .line 804
    .local v12, "endY":I
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v9, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->endMarkDirection:I

    .line 805
    .local v9, "endDirection":I
    const/16 v39, 0x2

    move/from16 v0, v39

    if-ne v9, v0, :cond_19

    .line 806
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endImageRect:Landroid/graphics/Rect;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->iconSize:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    sub-int v40, v11, v40

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->barWidth:I

    move/from16 v41, v0

    sub-int v41, v12, v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->barWidth:I

    move/from16 v42, v0

    sub-int v42, v12, v42

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->iconSize:Landroid/graphics/Point;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v43, v0

    add-int v42, v42, v43

    move-object/from16 v0, v39

    move/from16 v1, v40

    move/from16 v2, v41

    move/from16 v3, v42

    invoke-virtual {v0, v1, v2, v11, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 808
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    const v40, 0x7f020321

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endNormalId:I

    .line 809
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    const v40, 0x7f020322

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endPressedId:I

    .line 848
    :goto_8
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjPoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    aget-object v39, v39, v40

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->nObjectType:I

    move/from16 v39, v0

    if-nez v39, :cond_14

    .line 849
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput-boolean v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->mIsDrawBar:Z

    .line 851
    :cond_14
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nMarkingRectCount:I

    move/from16 v32, v0

    .line 852
    .restart local v32    # "rect_count":I
    if-lez v32, :cond_1c

    .line 853
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v39, 0x12c

    move/from16 v0, v32

    move/from16 v1, v39

    if-le v0, v1, :cond_15

    .line 854
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v32, 0x12c

    .line 855
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v40

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->mTextRectInfos:[S

    move-object/from16 v41, v0

    mul-int/lit8 v42, v32, 0x4

    invoke-virtual/range {v40 .. v42}, Lcom/infraware/office/evengine/EvInterface;->IGetTextMarkRectInfo([SI)I

    move-result v40

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->nTextRectCount:I

    .line 861
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMWDnD()Lcom/infraware/common/multiwindow/MWDnDOperator;

    move-result-object v39

    if-eqz v39, :cond_0

    .line 862
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMWDnD()Lcom/infraware/common/multiwindow/MWDnDOperator;

    move-result-object v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->nTextRectCount:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->mTextRectInfos:[S

    move-object/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Lcom/infraware/common/multiwindow/MWDnDOperator;->setLastTextMarkArea(I[S)V

    goto/16 :goto_0

    .line 783
    .end local v9    # "endDirection":I
    .end local v11    # "endX":I
    .end local v12    # "endY":I
    .end local v32    # "rect_count":I
    :cond_16
    const/16 v39, 0x2

    move/from16 v0, v34

    move/from16 v1, v39

    if-ne v0, v1, :cond_17

    .line 784
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startImageRect:Landroid/graphics/Rect;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->iconSize:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    sub-int v40, v36, v40

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->barWidth:I

    move/from16 v41, v0

    sub-int v41, v37, v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->barWidth:I

    move/from16 v42, v0

    sub-int v42, v37, v42

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->iconSize:Landroid/graphics/Point;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v43, v0

    add-int v42, v42, v43

    move-object/from16 v0, v39

    move/from16 v1, v40

    move/from16 v2, v41

    move/from16 v3, v36

    move/from16 v4, v42

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 786
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    const v40, 0x7f020321

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startNormalId:I

    .line 787
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    const v40, 0x7f020322

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startPressedId:I

    goto/16 :goto_7

    .line 789
    :cond_17
    const/16 v39, 0x4

    move/from16 v0, v34

    move/from16 v1, v39

    if-ne v0, v1, :cond_18

    .line 790
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startImageRect:Landroid/graphics/Rect;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->iconSize:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    sub-int v40, v36, v40

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->iconSize:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v41, v0

    sub-int v41, v37, v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->barWidth:I

    move/from16 v42, v0

    add-int v41, v41, v42

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->barWidth:I

    move/from16 v42, v0

    add-int v42, v42, v37

    move-object/from16 v0, v39

    move/from16 v1, v40

    move/from16 v2, v41

    move/from16 v3, v36

    move/from16 v4, v42

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 792
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    const v40, 0x7f020323

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startNormalId:I

    .line 793
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    const v40, 0x7f020324

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startPressedId:I

    goto/16 :goto_7

    .line 796
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startImageRect:Landroid/graphics/Rect;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->iconSize:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    sub-int v40, v36, v40

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->barWidth:I

    move/from16 v41, v0

    add-int v40, v40, v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->iconSize:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    sub-int v41, v37, v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->barWidth:I

    move/from16 v42, v0

    add-int v42, v42, v36

    move-object/from16 v0, v39

    move/from16 v1, v40

    move/from16 v2, v41

    move/from16 v3, v42

    move/from16 v4, v37

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 798
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    const v40, 0x7f020329

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startNormalId:I

    .line 799
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    const v40, 0x7f02032a

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startPressedId:I

    goto/16 :goto_7

    .line 811
    .restart local v9    # "endDirection":I
    .restart local v11    # "endX":I
    .restart local v12    # "endY":I
    :cond_19
    const/16 v39, 0x3

    move/from16 v0, v39

    if-ne v9, v0, :cond_1a

    .line 812
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endImageRect:Landroid/graphics/Rect;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->iconSize:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    sub-int v40, v12, v40

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->barWidth:I

    move/from16 v41, v0

    add-int v40, v40, v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->iconSize:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    add-int v41, v41, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->barWidth:I

    move/from16 v42, v0

    add-int v42, v42, v12

    move-object/from16 v0, v39

    move/from16 v1, v40

    move/from16 v2, v41

    move/from16 v3, v42

    invoke-virtual {v0, v11, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 814
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    const v40, 0x7f020327

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endNormalId:I

    .line 815
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    const v40, 0x7f020328

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endPressedId:I

    goto/16 :goto_8

    .line 817
    :cond_1a
    const/16 v39, 0x5

    move/from16 v0, v39

    if-ne v9, v0, :cond_1b

    .line 818
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endImageRect:Landroid/graphics/Rect;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->barWidth:I

    move/from16 v40, v0

    sub-int v40, v12, v40

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->iconSize:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    add-int v41, v41, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->barWidth:I

    move/from16 v42, v0

    sub-int v42, v12, v42

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->iconSize:Landroid/graphics/Point;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v43, v0

    add-int v42, v42, v43

    move-object/from16 v0, v39

    move/from16 v1, v40

    move/from16 v2, v41

    move/from16 v3, v42

    invoke-virtual {v0, v11, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 820
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    const v40, 0x7f020325

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endNormalId:I

    .line 821
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    const v40, 0x7f020326

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endPressedId:I

    goto/16 :goto_8

    .line 824
    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endImageRect:Landroid/graphics/Rect;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->barWidth:I

    move/from16 v40, v0

    sub-int v40, v11, v40

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->barWidth:I

    move/from16 v41, v0

    sub-int v41, v11, v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->iconSize:Landroid/graphics/Point;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v42, v0

    add-int v41, v41, v42

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->iconSize:Landroid/graphics/Point;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v42, v0

    add-int v42, v42, v12

    move-object/from16 v0, v39

    move/from16 v1, v40

    move/from16 v2, v41

    move/from16 v3, v42

    invoke-virtual {v0, v1, v12, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 826
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    const v40, 0x7f02031d

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endNormalId:I

    .line 827
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    const v40, 0x7f02031e

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endPressedId:I

    goto/16 :goto_8

    .line 858
    .restart local v32    # "rect_count":I
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->nTextRectCount:I

    goto/16 :goto_9

    .line 873
    .end local v9    # "endDirection":I
    .end local v11    # "endX":I
    .end local v12    # "endY":I
    .end local v32    # "rect_count":I
    .end local v34    # "startDirection":I
    .end local v36    # "startX":I
    .end local v37    # "startY":I
    :pswitch_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->bRotationEnabled:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->bRotationEnabled:I

    .line 874
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->bPureImage:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->bPureImage:I

    .line 879
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->eEditing:I

    move/from16 v39, v0

    if-nez v39, :cond_20

    .line 881
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 882
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 883
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nRotateAngle:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    .line 904
    :cond_1d
    :goto_a
    new-instance v16, Landroid/graphics/PointF;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-float v0, v0

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    move/from16 v0, v40

    int-to-float v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v16

    move/from16 v1, v39

    move/from16 v2, v40

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 905
    .local v16, "p0":Landroid/graphics/PointF;
    new-instance v17, Landroid/graphics/PointF;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-float v0, v0

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    move/from16 v0, v40

    int-to-float v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v17

    move/from16 v1, v39

    move/from16 v2, v40

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 906
    .local v17, "p1":Landroid/graphics/PointF;
    new-instance v27, Landroid/graphics/PointF;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-float v0, v0

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    move/from16 v0, v40

    int-to-float v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v27

    move/from16 v1, v39

    move/from16 v2, v40

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 907
    .local v27, "p2":Landroid/graphics/PointF;
    new-instance v28, Landroid/graphics/PointF;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-float v0, v0

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    move/from16 v0, v40

    int-to-float v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v28

    move/from16 v1, v39

    move/from16 v2, v40

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 910
    .local v28, "p3":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMWDnD()Lcom/infraware/common/multiwindow/MWDnDOperator;

    move-result-object v39

    if-eqz v39, :cond_1e

    .line 911
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMWDnD()Lcom/infraware/common/multiwindow/MWDnDOperator;

    move-result-object v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v43, v0

    invoke-virtual/range {v39 .. v43}, Lcom/infraware/common/multiwindow/MWDnDOperator;->setLastSelectedRect(IIII)V

    .line 914
    :cond_1e
    new-instance v30, Landroid/graphics/PointF;

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v39, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    add-float v39, v39, v40

    const/high16 v40, 0x40000000    # 2.0f

    div-float v39, v39, v40

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v40, v0

    const/high16 v41, 0x425c0000    # 55.0f

    sub-float v40, v40, v41

    move-object/from16 v0, v30

    move/from16 v1, v39

    move/from16 v2, v40

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 915
    .local v30, "pR":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    move/from16 v39, v0

    if-eqz v39, :cond_1f

    .line 917
    new-instance v29, Landroid/graphics/PointF;

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v39, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    add-float v39, v39, v40

    const/high16 v40, 0x40000000    # 2.0f

    div-float v39, v39, v40

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v40, v0

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    add-float v40, v40, v41

    const/high16 v41, 0x40000000    # 2.0f

    div-float v40, v40, v41

    move-object/from16 v0, v29

    move/from16 v1, v39

    move/from16 v2, v40

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 918
    .local v29, "pC":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    move/from16 v39, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v29

    move/from16 v2, v39

    invoke-static {v0, v1, v2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRotatePoint(Landroid/graphics/PointF;Landroid/graphics/PointF;I)Landroid/graphics/PointF;

    move-result-object v19

    .line 919
    .local v19, "p100":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    move/from16 v39, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v29

    move/from16 v2, v39

    invoke-static {v0, v1, v2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRotatePoint(Landroid/graphics/PointF;Landroid/graphics/PointF;I)Landroid/graphics/PointF;

    move-result-object v20

    .line 920
    .local v20, "p101":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    move/from16 v39, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    move/from16 v2, v39

    invoke-static {v0, v1, v2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRotatePoint(Landroid/graphics/PointF;Landroid/graphics/PointF;I)Landroid/graphics/PointF;

    move-result-object v21

    .line 921
    .local v21, "p102":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    move/from16 v39, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    move/from16 v2, v39

    invoke-static {v0, v1, v2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRotatePoint(Landroid/graphics/PointF;Landroid/graphics/PointF;I)Landroid/graphics/PointF;

    move-result-object v22

    .line 922
    .local v22, "p103":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    move/from16 v39, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    move/from16 v2, v39

    invoke-static {v0, v1, v2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRotatePoint(Landroid/graphics/PointF;Landroid/graphics/PointF;I)Landroid/graphics/PointF;

    move-result-object v23

    .line 924
    .local v23, "p109":Landroid/graphics/PointF;
    move-object/from16 v16, v19

    .line 925
    move-object/from16 v17, v20

    .line 926
    move-object/from16 v27, v21

    .line 927
    move-object/from16 v28, v22

    .line 928
    move-object/from16 v30, v23

    .line 931
    .end local v19    # "p100":Landroid/graphics/PointF;
    .end local v20    # "p101":Landroid/graphics/PointF;
    .end local v21    # "p102":Landroid/graphics/PointF;
    .end local v22    # "p103":Landroid/graphics/PointF;
    .end local v23    # "p109":Landroid/graphics/PointF;
    .end local v29    # "pC":Landroid/graphics/PointF;
    :cond_1f
    new-instance v18, Landroid/graphics/PointF;

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v39, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    add-float v39, v39, v40

    const/high16 v40, 0x40000000    # 2.0f

    div-float v39, v39, v40

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v40, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    add-float v40, v40, v41

    const/high16 v41, 0x40000000    # 2.0f

    div-float v40, v40, v41

    move-object/from16 v0, v18

    move/from16 v1, v39

    move/from16 v2, v40

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 932
    .local v18, "p10":Landroid/graphics/PointF;
    new-instance v24, Landroid/graphics/PointF;

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v39, v0

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    add-float v39, v39, v40

    const/high16 v40, 0x40000000    # 2.0f

    div-float v39, v39, v40

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v40, v0

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    add-float v40, v40, v41

    const/high16 v41, 0x40000000    # 2.0f

    div-float v40, v40, v41

    move-object/from16 v0, v24

    move/from16 v1, v39

    move/from16 v2, v40

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 933
    .local v24, "p11":Landroid/graphics/PointF;
    new-instance v25, Landroid/graphics/PointF;

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v39, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    add-float v39, v39, v40

    const/high16 v40, 0x40000000    # 2.0f

    div-float v39, v39, v40

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v40, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    add-float v40, v40, v41

    const/high16 v41, 0x40000000    # 2.0f

    div-float v40, v40, v41

    move-object/from16 v0, v25

    move/from16 v1, v39

    move/from16 v2, v40

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 934
    .local v25, "p12":Landroid/graphics/PointF;
    new-instance v26, Landroid/graphics/PointF;

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v39, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    add-float v39, v39, v40

    const/high16 v40, 0x40000000    # 2.0f

    div-float v39, v39, v40

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v40, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    add-float v40, v40, v41

    const/high16 v41, 0x40000000    # 2.0f

    div-float v40, v40, v41

    move-object/from16 v0, v26

    move/from16 v1, v39

    move/from16 v2, v40

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 936
    .local v26, "p13":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, 0x1

    aget-object v39, v39, v40

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    const/high16 v41, 0x3f000000    # 0.5f

    add-float v40, v40, v41

    move/from16 v0, v40

    float-to-int v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    const/high16 v42, 0x3f000000    # 0.5f

    add-float v41, v41, v42

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 937
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, 0x2

    aget-object v39, v39, v40

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    const/high16 v41, 0x3f000000    # 0.5f

    add-float v40, v40, v41

    move/from16 v0, v40

    float-to-int v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    const/high16 v42, 0x3f000000    # 0.5f

    add-float v41, v41, v42

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 938
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, 0x3

    aget-object v39, v39, v40

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    const/high16 v41, 0x3f000000    # 0.5f

    add-float v40, v40, v41

    move/from16 v0, v40

    float-to-int v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    const/high16 v42, 0x3f000000    # 0.5f

    add-float v41, v41, v42

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 939
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, 0x4

    aget-object v39, v39, v40

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    const/high16 v41, 0x3f000000    # 0.5f

    add-float v40, v40, v41

    move/from16 v0, v40

    float-to-int v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    const/high16 v42, 0x3f000000    # 0.5f

    add-float v41, v41, v42

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 940
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, 0x5

    aget-object v39, v39, v40

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    const/high16 v41, 0x3f000000    # 0.5f

    add-float v40, v40, v41

    move/from16 v0, v40

    float-to-int v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    const/high16 v42, 0x3f000000    # 0.5f

    add-float v41, v41, v42

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 941
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, 0x6

    aget-object v39, v39, v40

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    const/high16 v41, 0x3f000000    # 0.5f

    add-float v40, v40, v41

    move/from16 v0, v40

    float-to-int v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    const/high16 v42, 0x3f000000    # 0.5f

    add-float v41, v41, v42

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 942
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, 0x7

    aget-object v39, v39, v40

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    const/high16 v41, 0x3f000000    # 0.5f

    add-float v40, v40, v41

    move/from16 v0, v40

    float-to-int v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    const/high16 v42, 0x3f000000    # 0.5f

    add-float v41, v41, v42

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 943
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, 0x8

    aget-object v39, v39, v40

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    const/high16 v41, 0x3f000000    # 0.5f

    add-float v40, v40, v41

    move/from16 v0, v40

    float-to-int v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    const/high16 v42, 0x3f000000    # 0.5f

    add-float v41, v41, v42

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 944
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, 0x9

    aget-object v39, v39, v40

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    const/high16 v41, 0x3f000000    # 0.5f

    add-float v40, v40, v41

    move/from16 v0, v40

    float-to-int v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    const/high16 v42, 0x3f000000    # 0.5f

    add-float v41, v41, v42

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 946
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->nSmartGuidesCnt:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nSmartGuidesCnt:I

    .line 947
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_b
    const/16 v39, 0x6

    move/from16 v0, v39

    if-ge v13, v0, :cond_23

    .line 948
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesType:[I

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptSmartGuidesType:[I

    move-object/from16 v40, v0

    aget v40, v40, v13

    aput v40, v39, v13

    .line 949
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesStart:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    aget-object v39, v39, v13

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptSmartGuidesStart:[Landroid/graphics/Point;

    move-object/from16 v40, v0

    aget-object v40, v40, v13

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Landroid/graphics/Point;->x:I

    .line 950
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesStart:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    aget-object v39, v39, v13

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptSmartGuidesStart:[Landroid/graphics/Point;

    move-object/from16 v40, v0

    aget-object v40, v40, v13

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Landroid/graphics/Point;->y:I

    .line 951
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesEnd:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    aget-object v39, v39, v13

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptSmartGuidesEnd:[Landroid/graphics/Point;

    move-object/from16 v40, v0

    aget-object v40, v40, v13

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Landroid/graphics/Point;->x:I

    .line 952
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesEnd:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    aget-object v39, v39, v13

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptSmartGuidesEnd:[Landroid/graphics/Point;

    move-object/from16 v40, v0

    aget-object v40, v40, v13

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Landroid/graphics/Point;->y:I

    .line 947
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_b

    .line 885
    .end local v13    # "i":I
    .end local v16    # "p0":Landroid/graphics/PointF;
    .end local v17    # "p1":Landroid/graphics/PointF;
    .end local v18    # "p10":Landroid/graphics/PointF;
    .end local v24    # "p11":Landroid/graphics/PointF;
    .end local v25    # "p12":Landroid/graphics/PointF;
    .end local v26    # "p13":Landroid/graphics/PointF;
    .end local v27    # "p2":Landroid/graphics/PointF;
    .end local v28    # "p3":Landroid/graphics/PointF;
    .end local v30    # "pR":Landroid/graphics/PointF;
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->eEditing:I

    move/from16 v39, v0

    const/16 v40, 0x1

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_21

    .line 887
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->editStartPoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->editStartPoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 888
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->editEndPoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->editEndPoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 889
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nRotateAngle:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    goto/16 :goto_a

    .line 891
    :cond_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->eEditing:I

    move/from16 v39, v0

    const/16 v40, 0x2

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_22

    .line 893
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->editStartPoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->editStartPoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 894
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->editEndPoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->editEndPoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 895
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nRotateAngle:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    goto/16 :goto_a

    .line 897
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->eEditing:I

    move/from16 v39, v0

    const/16 v40, 0x3

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_1d

    .line 899
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 900
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 901
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nEditingAngle:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    goto/16 :goto_a

    .line 955
    .restart local v13    # "i":I
    .restart local v16    # "p0":Landroid/graphics/PointF;
    .restart local v17    # "p1":Landroid/graphics/PointF;
    .restart local v18    # "p10":Landroid/graphics/PointF;
    .restart local v24    # "p11":Landroid/graphics/PointF;
    .restart local v25    # "p12":Landroid/graphics/PointF;
    .restart local v26    # "p13":Landroid/graphics/PointF;
    .restart local v27    # "p2":Landroid/graphics/PointF;
    .restart local v28    # "p3":Landroid/graphics/PointF;
    .restart local v30    # "pR":Landroid/graphics/PointF;
    :cond_23
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->nAdjustHandleCnt:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nAdjustHandleCnt:I

    .line 956
    const/4 v13, 0x0

    :goto_c
    const/16 v39, 0xa

    move/from16 v0, v39

    if-ge v13, v0, :cond_24

    .line 957
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    aget-object v39, v39, v13

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptAdjustHandlePoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v40, v0

    aget-object v40, v40, v13

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Landroid/graphics/Point;->x:I

    .line 958
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    aget-object v39, v39, v13

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptAdjustHandlePoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v40, v0

    aget-object v40, v40, v13

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Landroid/graphics/Point;->y:I

    .line 956
    add-int/lit8 v13, v13, 0x1

    goto :goto_c

    .line 960
    :cond_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->nAnimationOrderCnt:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nAnimationOrderCnt:I

    .line 961
    const/4 v13, 0x0

    :goto_d
    const/16 v39, 0xa

    move/from16 v0, v39

    if-ge v13, v0, :cond_25

    .line 962
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAnimationOrder:[I

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptAnimationOrder:[I

    move-object/from16 v40, v0

    aget v40, v40, v13

    aput v40, v39, v13

    .line 961
    add-int/lit8 v13, v13, 0x1

    goto :goto_d

    .line 965
    :cond_25
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->eEditing:I

    move/from16 v39, v0

    if-eqz v39, :cond_0

    .line 966
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    const/16 v40, -0x1

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nAdjustHandleCnt:I

    .line 967
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    const/16 v40, -0x1

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nAnimationOrderCnt:I

    goto/16 :goto_0

    .line 975
    .end local v13    # "i":I
    .end local v16    # "p0":Landroid/graphics/PointF;
    .end local v17    # "p1":Landroid/graphics/PointF;
    .end local v18    # "p10":Landroid/graphics/PointF;
    .end local v24    # "p11":Landroid/graphics/PointF;
    .end local v25    # "p12":Landroid/graphics/PointF;
    .end local v26    # "p13":Landroid/graphics/PointF;
    .end local v27    # "p2":Landroid/graphics/PointF;
    .end local v28    # "p3":Landroid/graphics/PointF;
    .end local v30    # "pR":Landroid/graphics/PointF;
    :pswitch_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nRotateAngle:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->rotateAngle:I

    .line 978
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->endPoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    invoke-static/range {v39 .. v40}, Ljava/lang/Math;->min(II)I

    move-result v39

    move/from16 v0, v39

    int-to-float v14, v0

    .line 979
    .local v14, "left":F
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->endPoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    invoke-static/range {v39 .. v40}, Ljava/lang/Math;->max(II)I

    move-result v39

    move/from16 v0, v39

    int-to-float v0, v0

    move/from16 v33, v0

    .line 980
    .local v33, "right":F
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->endPoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    invoke-static/range {v39 .. v40}, Ljava/lang/Math;->min(II)I

    move-result v39

    move/from16 v0, v39

    int-to-float v0, v0

    move/from16 v38, v0

    .line 981
    .local v38, "top":F
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->endPoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    invoke-static/range {v39 .. v40}, Ljava/lang/Math;->max(II)I

    move-result v39

    move/from16 v0, v39

    int-to-float v7, v0

    .line 983
    .local v7, "bottom":F
    new-instance v35, Landroid/graphics/PointF;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-float v0, v0

    move/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startPoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    move/from16 v0, v40

    int-to-float v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v35

    move/from16 v1, v39

    move/from16 v2, v40

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 984
    .local v35, "startPt":Landroid/graphics/PointF;
    new-instance v10, Landroid/graphics/PointF;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->endPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-float v0, v0

    move/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->endPoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    move/from16 v0, v40

    int-to-float v0, v0

    move/from16 v40, v0

    move/from16 v0, v39

    move/from16 v1, v40

    invoke-direct {v10, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 986
    .local v10, "endPt":Landroid/graphics/PointF;
    new-instance v8, Landroid/graphics/PointF;

    add-float v39, v14, v33

    const/high16 v40, 0x40000000    # 2.0f

    div-float v39, v39, v40

    add-float v40, v38, v7

    const/high16 v41, 0x40000000    # 2.0f

    div-float v40, v40, v41

    move/from16 v0, v39

    move/from16 v1, v40

    invoke-direct {v8, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 989
    .local v8, "center":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->rotateAngle:I

    move/from16 v39, v0

    move-object/from16 v0, v35

    move/from16 v1, v39

    invoke-static {v0, v8, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRotatePoint(Landroid/graphics/PointF;Landroid/graphics/PointF;I)Landroid/graphics/PointF;

    move-result-object v35

    .line 990
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->rotateAngle:I

    move/from16 v39, v0

    move/from16 v0, v39

    invoke-static {v10, v8, v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRotatePoint(Landroid/graphics/PointF;Landroid/graphics/PointF;I)Landroid/graphics/PointF;

    move-result-object v10

    .line 992
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->startPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v35

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    move/from16 v0, v40

    float-to-int v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v35

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 993
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->endPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    iget v0, v10, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    move/from16 v0, v40

    float-to-int v0, v0

    move/from16 v40, v0

    iget v0, v10, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 995
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    float-to-int v0, v14

    move/from16 v40, v0

    move/from16 v0, v38

    float-to-int v0, v0

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 996
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move/from16 v0, v33

    float-to-int v0, v0

    move/from16 v40, v0

    float-to-int v0, v7

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 999
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingStartPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingEndPoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    invoke-static/range {v39 .. v40}, Ljava/lang/Math;->min(II)I

    move-result v39

    move/from16 v0, v39

    int-to-float v14, v0

    .line 1000
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingStartPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingEndPoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    invoke-static/range {v39 .. v40}, Ljava/lang/Math;->max(II)I

    move-result v39

    move/from16 v0, v39

    int-to-float v0, v0

    move/from16 v33, v0

    .line 1001
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingStartPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingEndPoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    invoke-static/range {v39 .. v40}, Ljava/lang/Math;->min(II)I

    move-result v39

    move/from16 v0, v39

    int-to-float v0, v0

    move/from16 v38, v0

    .line 1002
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingStartPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingEndPoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    invoke-static/range {v39 .. v40}, Ljava/lang/Math;->max(II)I

    move-result v39

    move/from16 v0, v39

    int-to-float v7, v0

    .line 1004
    new-instance v35, Landroid/graphics/PointF;

    .end local v35    # "startPt":Landroid/graphics/PointF;
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingStartPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-float v0, v0

    move/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingStartPoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    move/from16 v0, v40

    int-to-float v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v35

    move/from16 v1, v39

    move/from16 v2, v40

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1005
    .restart local v35    # "startPt":Landroid/graphics/PointF;
    new-instance v10, Landroid/graphics/PointF;

    .end local v10    # "endPt":Landroid/graphics/PointF;
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingEndPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-float v0, v0

    move/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingEndPoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    move/from16 v0, v40

    int-to-float v0, v0

    move/from16 v40, v0

    move/from16 v0, v39

    move/from16 v1, v40

    invoke-direct {v10, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1007
    .restart local v10    # "endPt":Landroid/graphics/PointF;
    new-instance v8, Landroid/graphics/PointF;

    .end local v8    # "center":Landroid/graphics/PointF;
    add-float v39, v14, v33

    const/high16 v40, 0x40000000    # 2.0f

    div-float v39, v39, v40

    add-float v40, v38, v7

    const/high16 v41, 0x40000000    # 2.0f

    div-float v40, v40, v41

    move/from16 v0, v39

    move/from16 v1, v40

    invoke-direct {v8, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1010
    .restart local v8    # "center":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->rotateAngle:I

    move/from16 v39, v0

    move-object/from16 v0, v35

    move/from16 v1, v39

    invoke-static {v0, v8, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRotatePoint(Landroid/graphics/PointF;Landroid/graphics/PointF;I)Landroid/graphics/PointF;

    move-result-object v35

    .line 1011
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->rotateAngle:I

    move/from16 v39, v0

    move/from16 v0, v39

    invoke-static {v10, v8, v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRotatePoint(Landroid/graphics/PointF;Landroid/graphics/PointF;I)Landroid/graphics/PointF;

    move-result-object v10

    .line 1013
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->startEditingPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v35

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    move/from16 v0, v40

    float-to-int v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v35

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 1014
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->endEditingPoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    iget v0, v10, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    move/from16 v0, v40

    float-to-int v0, v0

    move/from16 v40, v0

    iget v0, v10, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 1023
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->nSmartGuidesCnt:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nSmartGuidesCnt:I

    .line 1024
    const/4 v13, 0x0

    .restart local v13    # "i":I
    :goto_e
    const/16 v39, 0x6

    move/from16 v0, v39

    if-ge v13, v0, :cond_26

    .line 1025
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesType:[I

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptSmartGuidesType:[I

    move-object/from16 v40, v0

    aget v40, v40, v13

    aput v40, v39, v13

    .line 1026
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesStart:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    aget-object v39, v39, v13

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptSmartGuidesStart:[Landroid/graphics/Point;

    move-object/from16 v40, v0

    aget-object v40, v40, v13

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Landroid/graphics/Point;->x:I

    .line 1027
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesStart:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    aget-object v39, v39, v13

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptSmartGuidesStart:[Landroid/graphics/Point;

    move-object/from16 v40, v0

    aget-object v40, v40, v13

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Landroid/graphics/Point;->y:I

    .line 1028
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesEnd:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    aget-object v39, v39, v13

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptSmartGuidesEnd:[Landroid/graphics/Point;

    move-object/from16 v40, v0

    aget-object v40, v40, v13

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Landroid/graphics/Point;->x:I

    .line 1029
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesEnd:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    aget-object v39, v39, v13

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptSmartGuidesEnd:[Landroid/graphics/Point;

    move-object/from16 v40, v0

    aget-object v40, v40, v13

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Landroid/graphics/Point;->y:I

    .line 1024
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_e

    .line 1032
    :cond_26
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->nAdjustHandleCnt:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->nAdjustHandleCnt:I

    .line 1033
    const/4 v13, 0x0

    :goto_f
    const/16 v39, 0xa

    move/from16 v0, v39

    if-ge v13, v0, :cond_27

    .line 1034
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    aget-object v39, v39, v13

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptAdjustHandlePoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v40, v0

    aget-object v40, v40, v13

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Landroid/graphics/Point;->x:I

    .line 1035
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    aget-object v39, v39, v13

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptAdjustHandlePoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v40, v0

    aget-object v40, v40, v13

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Landroid/graphics/Point;->y:I

    .line 1033
    add-int/lit8 v13, v13, 0x1

    goto :goto_f

    .line 1037
    :cond_27
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->nAnimationOrderCnt:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nAnimationOrderCnt:I

    .line 1038
    const/4 v13, 0x0

    :goto_10
    const/16 v39, 0xa

    move/from16 v0, v39

    if-ge v13, v0, :cond_0

    .line 1039
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAnimationOrder:[I

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptAnimationOrder:[I

    move-object/from16 v40, v0

    aget v40, v40, v13

    aput v40, v39, v13

    .line 1038
    add-int/lit8 v13, v13, 0x1

    goto :goto_10

    .line 1050
    .end local v7    # "bottom":F
    .end local v8    # "center":Landroid/graphics/PointF;
    .end local v10    # "endPt":Landroid/graphics/PointF;
    .end local v13    # "i":I
    .end local v14    # "left":F
    .end local v33    # "right":F
    .end local v35    # "startPt":Landroid/graphics/PointF;
    .end local v38    # "top":F
    :pswitch_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->nMultiObj:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->nObjectCount:I

    .line 1051
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->eEditing:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->eEditing:I

    .line 1054
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->nObjectCount:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v40, 0xa

    move/from16 v0, v39

    move/from16 v1, v40

    if-le v0, v1, :cond_28

    .line 1055
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v40, 0xa

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->nObjectCount:I

    .line 1057
    :cond_28
    const/16 v39, 0xd

    move/from16 v0, v39

    new-array v0, v0, [I

    move-object/from16 v31, v0

    .line 1058
    .local v31, "pointarray":[I
    const/4 v13, 0x0

    .restart local v13    # "i":I
    :goto_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->nObjectCount:I

    move/from16 v39, v0

    move/from16 v0, v39

    if-ge v13, v0, :cond_0

    .line 1060
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, v31

    invoke-virtual {v0, v13, v1}, Lcom/infraware/office/evengine/EvInterface;->IGetMultiSelectPointInfo(I[I)V

    .line 1061
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-object/from16 v1, v31

    invoke-virtual {v0, v13, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->setData(I[I)V

    .line 1058
    add-int/lit8 v13, v13, 0x1

    goto :goto_11

    .line 1066
    .end local v13    # "i":I
    .end local v31    # "pointarray":[I
    :pswitch_f
    const-string/jumbo v39, "HYOHYUN"

    const-string/jumbo v40, "eEV_OBJECT_INFRA_PEN "

    invoke-static/range {v39 .. v40}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1067
    const-string/jumbo v39, "HYOHYUN"

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v41, "X = "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v41, v0

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string/jumbo v41, "   Y = "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1068
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->bRotationEnabled:I

    .line 1072
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->eEditing:I

    move/from16 v39, v0

    if-nez v39, :cond_2a

    .line 1074
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 1075
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 1076
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nRotateAngle:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    .line 1093
    :cond_29
    :goto_12
    new-instance v16, Landroid/graphics/PointF;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-float v0, v0

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    move/from16 v0, v40

    int-to-float v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v16

    move/from16 v1, v39

    move/from16 v2, v40

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1094
    .restart local v16    # "p0":Landroid/graphics/PointF;
    new-instance v17, Landroid/graphics/PointF;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-float v0, v0

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    move/from16 v0, v40

    int-to-float v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v17

    move/from16 v1, v39

    move/from16 v2, v40

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1095
    .restart local v17    # "p1":Landroid/graphics/PointF;
    new-instance v27, Landroid/graphics/PointF;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-float v0, v0

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    move/from16 v0, v40

    int-to-float v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v27

    move/from16 v1, v39

    move/from16 v2, v40

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1096
    .restart local v27    # "p2":Landroid/graphics/PointF;
    new-instance v28, Landroid/graphics/PointF;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-float v0, v0

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    move/from16 v0, v40

    int-to-float v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v28

    move/from16 v1, v39

    move/from16 v2, v40

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1098
    .restart local v28    # "p3":Landroid/graphics/PointF;
    new-instance v18, Landroid/graphics/PointF;

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v39, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    add-float v39, v39, v40

    const/high16 v40, 0x40000000    # 2.0f

    div-float v39, v39, v40

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v40, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    add-float v40, v40, v41

    const/high16 v41, 0x40000000    # 2.0f

    div-float v40, v40, v41

    move-object/from16 v0, v18

    move/from16 v1, v39

    move/from16 v2, v40

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1099
    .restart local v18    # "p10":Landroid/graphics/PointF;
    new-instance v24, Landroid/graphics/PointF;

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v39, v0

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    add-float v39, v39, v40

    const/high16 v40, 0x40000000    # 2.0f

    div-float v39, v39, v40

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v40, v0

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    add-float v40, v40, v41

    const/high16 v41, 0x40000000    # 2.0f

    div-float v40, v40, v41

    move-object/from16 v0, v24

    move/from16 v1, v39

    move/from16 v2, v40

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1100
    .restart local v24    # "p11":Landroid/graphics/PointF;
    new-instance v25, Landroid/graphics/PointF;

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v39, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    add-float v39, v39, v40

    const/high16 v40, 0x40000000    # 2.0f

    div-float v39, v39, v40

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v40, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    add-float v40, v40, v41

    const/high16 v41, 0x40000000    # 2.0f

    div-float v40, v40, v41

    move-object/from16 v0, v25

    move/from16 v1, v39

    move/from16 v2, v40

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1101
    .restart local v25    # "p12":Landroid/graphics/PointF;
    new-instance v26, Landroid/graphics/PointF;

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v39, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    add-float v39, v39, v40

    const/high16 v40, 0x40000000    # 2.0f

    div-float v39, v39, v40

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v40, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    add-float v40, v40, v41

    const/high16 v41, 0x40000000    # 2.0f

    div-float v40, v40, v41

    move-object/from16 v0, v26

    move/from16 v1, v39

    move/from16 v2, v40

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1103
    .restart local v26    # "p13":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, 0x1

    aget-object v39, v39, v40

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    const/high16 v41, 0x3f000000    # 0.5f

    add-float v40, v40, v41

    move/from16 v0, v40

    float-to-int v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    const/high16 v42, 0x3f000000    # 0.5f

    add-float v41, v41, v42

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 1104
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, 0x2

    aget-object v39, v39, v40

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    const/high16 v41, 0x3f000000    # 0.5f

    add-float v40, v40, v41

    move/from16 v0, v40

    float-to-int v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    const/high16 v42, 0x3f000000    # 0.5f

    add-float v41, v41, v42

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 1105
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, 0x3

    aget-object v39, v39, v40

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    const/high16 v41, 0x3f000000    # 0.5f

    add-float v40, v40, v41

    move/from16 v0, v40

    float-to-int v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    const/high16 v42, 0x3f000000    # 0.5f

    add-float v41, v41, v42

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 1106
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, 0x4

    aget-object v39, v39, v40

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    const/high16 v41, 0x3f000000    # 0.5f

    add-float v40, v40, v41

    move/from16 v0, v40

    float-to-int v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    const/high16 v42, 0x3f000000    # 0.5f

    add-float v41, v41, v42

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 1107
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, 0x5

    aget-object v39, v39, v40

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    const/high16 v41, 0x3f000000    # 0.5f

    add-float v40, v40, v41

    move/from16 v0, v40

    float-to-int v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    const/high16 v42, 0x3f000000    # 0.5f

    add-float v41, v41, v42

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 1108
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, 0x6

    aget-object v39, v39, v40

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    const/high16 v41, 0x3f000000    # 0.5f

    add-float v40, v40, v41

    move/from16 v0, v40

    float-to-int v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    const/high16 v42, 0x3f000000    # 0.5f

    add-float v41, v41, v42

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 1109
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, 0x7

    aget-object v39, v39, v40

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    const/high16 v41, 0x3f000000    # 0.5f

    add-float v40, v40, v41

    move/from16 v0, v40

    float-to-int v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    const/high16 v42, 0x3f000000    # 0.5f

    add-float v41, v41, v42

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 1110
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    const/16 v40, 0x8

    aget-object v39, v39, v40

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v40, v0

    const/high16 v41, 0x3f000000    # 0.5f

    add-float v40, v40, v41

    move/from16 v0, v40

    float-to-int v0, v0

    move/from16 v40, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v41, v0

    const/high16 v42, 0x3f000000    # 0.5f

    add-float v41, v41, v42

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 1112
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->nSmartGuidesCnt:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nSmartGuidesCnt:I

    .line 1113
    const/4 v13, 0x0

    .restart local v13    # "i":I
    :goto_13
    const/16 v39, 0x6

    move/from16 v0, v39

    if-ge v13, v0, :cond_2c

    .line 1114
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesType:[I

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptSmartGuidesType:[I

    move-object/from16 v40, v0

    aget v40, v40, v13

    aput v40, v39, v13

    .line 1115
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesStart:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    aget-object v39, v39, v13

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptSmartGuidesStart:[Landroid/graphics/Point;

    move-object/from16 v40, v0

    aget-object v40, v40, v13

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Landroid/graphics/Point;->x:I

    .line 1116
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesStart:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    aget-object v39, v39, v13

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptSmartGuidesStart:[Landroid/graphics/Point;

    move-object/from16 v40, v0

    aget-object v40, v40, v13

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Landroid/graphics/Point;->y:I

    .line 1117
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesEnd:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    aget-object v39, v39, v13

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptSmartGuidesEnd:[Landroid/graphics/Point;

    move-object/from16 v40, v0

    aget-object v40, v40, v13

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Landroid/graphics/Point;->x:I

    .line 1118
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesEnd:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    aget-object v39, v39, v13

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptSmartGuidesEnd:[Landroid/graphics/Point;

    move-object/from16 v40, v0

    aget-object v40, v40, v13

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Landroid/graphics/Point;->y:I

    .line 1113
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_13

    .line 1078
    .end local v13    # "i":I
    .end local v16    # "p0":Landroid/graphics/PointF;
    .end local v17    # "p1":Landroid/graphics/PointF;
    .end local v18    # "p10":Landroid/graphics/PointF;
    .end local v24    # "p11":Landroid/graphics/PointF;
    .end local v25    # "p12":Landroid/graphics/PointF;
    .end local v26    # "p13":Landroid/graphics/PointF;
    .end local v27    # "p2":Landroid/graphics/PointF;
    .end local v28    # "p3":Landroid/graphics/PointF;
    :cond_2a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->eEditing:I

    move/from16 v39, v0

    const/16 v40, 0x1

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_2b

    .line 1080
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->editStartPoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->editStartPoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 1081
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->editEndPoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->editEndPoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 1082
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nRotateAngle:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    goto/16 :goto_12

    .line 1085
    :cond_2b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->eEditing:I

    move/from16 v39, v0

    const/16 v40, 0x2

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_29

    .line 1087
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->editStartPoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->editStartPoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 1088
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->editEndPoint:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->editEndPoint:Landroid/graphics/Point;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Landroid/graphics/Point;->set(II)V

    .line 1089
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nRotateAngle:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    goto/16 :goto_12

    .line 1121
    .restart local v13    # "i":I
    .restart local v16    # "p0":Landroid/graphics/PointF;
    .restart local v17    # "p1":Landroid/graphics/PointF;
    .restart local v18    # "p10":Landroid/graphics/PointF;
    .restart local v24    # "p11":Landroid/graphics/PointF;
    .restart local v25    # "p12":Landroid/graphics/PointF;
    .restart local v26    # "p13":Landroid/graphics/PointF;
    .restart local v27    # "p2":Landroid/graphics/PointF;
    .restart local v28    # "p3":Landroid/graphics/PointF;
    :cond_2c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->nAdjustHandleCnt:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nAdjustHandleCnt:I

    .line 1122
    const/4 v13, 0x0

    :goto_14
    const/16 v39, 0xa

    move/from16 v0, v39

    if-ge v13, v0, :cond_2d

    .line 1123
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    aget-object v39, v39, v13

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptAdjustHandlePoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v40, v0

    aget-object v40, v40, v13

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Landroid/graphics/Point;->x:I

    .line 1124
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    move-object/from16 v39, v0

    aget-object v39, v39, v13

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptAdjustHandlePoint:[Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;

    move-object/from16 v40, v0

    aget-object v40, v40, v13

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_POINT;->point:Landroid/graphics/Point;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Landroid/graphics/Point;->y:I

    .line 1122
    add-int/lit8 v13, v13, 0x1

    goto :goto_14

    .line 1126
    :cond_2d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->nAnimationOrderCnt:I

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nAnimationOrderCnt:I

    .line 1127
    const/4 v13, 0x0

    :goto_15
    const/16 v39, 0xa

    move/from16 v0, v39

    if-ge v13, v0, :cond_0

    .line 1128
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAnimationOrder:[I

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptAnimationOrder:[I

    move-object/from16 v40, v0

    aget v40, v40, v13

    aput v40, v39, v13

    .line 1127
    add-int/lit8 v13, v13, 0x1

    goto :goto_15

    .line 633
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_c
        :pswitch_b
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_e
    .end packed-switch

    .line 643
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0xb -> :sswitch_2
        0xc -> :sswitch_3
        0x20 -> :sswitch_5
        0x30 -> :sswitch_4
        0x40 -> :sswitch_4
        0x50 -> :sswitch_5
    .end sparse-switch

    .line 685
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_4
        :pswitch_1
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 707
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_7
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_8
        :pswitch_2
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public setSheetAutoFilterInfo([I)V
    .locals 0
    .param p1, "info"    # [I

    .prologue
    .line 3695
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mSheetAutofilterInfo:[I

    .line 3696
    return-void
.end method

.method public setSheetFormulaCursorRangeRect([I)V
    .locals 0
    .param p1, "info"    # [I

    .prologue
    .line 3703
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mSheetFormulaCursorRangeInfo:[I

    .line 3704
    return-void
.end method

.method public setSheetFormulaRangeCount(I)V
    .locals 0
    .param p1, "a_nCount"    # I

    .prologue
    .line 3707
    iput p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->m_nSheetFormulaRangeCount:I

    .line 3708
    return-void
.end method

.method public setSheetFormulaRangeRect([I)V
    .locals 0
    .param p1, "info"    # [I

    .prologue
    .line 3699
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mSheetFormulaRangeInfo:[I

    .line 3700
    return-void
.end method

.method public unsetObjetInfo()V
    .locals 1

    .prologue
    .line 530
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    packed-switch v0, :pswitch_data_0

    .line 549
    :goto_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mObjectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->SetInit()V

    .line 550
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mGrapAttInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->SetInit()V

    .line 551
    return-void

    .line 532
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mCellInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->SetInit()V

    goto :goto_0

    .line 535
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mTextInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->SetInit()V

    goto :goto_0

    .line 538
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mRectInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->SetInit()V

    goto :goto_0

    .line 541
    :pswitch_3
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mLineInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->SetInit()V

    goto :goto_0

    .line 544
    :pswitch_4
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mMultiInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->SetInit()V

    goto :goto_0

    .line 530
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
