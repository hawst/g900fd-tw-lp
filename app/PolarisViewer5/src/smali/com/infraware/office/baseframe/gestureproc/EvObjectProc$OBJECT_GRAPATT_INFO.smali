.class public Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;
.super Ljava/lang/Object;
.source "EvObjectProc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "OBJECT_GRAPATT_INFO"
.end annotation


# instance fields
.field public b2007DocxVML:Z

.field public b3D:Z

.field public bDML:Z

.field public bSupport3D:Z

.field public bSupport3DBevel:Z

.field final synthetic this$0:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;


# direct methods
.method public constructor <init>(Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public SetInit()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 99
    iput-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->bDML:Z

    .line 100
    iput-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->b2007DocxVML:Z

    .line 101
    iput-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->bSupport3D:Z

    .line 102
    iput-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->bSupport3DBevel:Z

    .line 103
    iput-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->b3D:Z

    .line 104
    return-void
.end method

.method public is2007DocxVML()Z
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->b2007DocxVML:Z

    return v0
.end method

.method public is3D()Z
    .locals 1

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->b3D:Z

    return v0
.end method

.method public isDML()Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->bDML:Z

    return v0
.end method

.method public isSupport3D()Z
    .locals 1

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->bSupport3D:Z

    return v0
.end method

.method public isSupport3DBevel()Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->bSupport3DBevel:Z

    return v0
.end method

.method public set2007DocxVML(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 108
    iput-boolean p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->b2007DocxVML:Z

    return-void
.end method

.method public set3D(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->b3D:Z

    return-void
.end method

.method public setDML(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 106
    iput-boolean p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->bDML:Z

    return-void
.end method

.method public setSupport3D(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 107
    iput-boolean p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->bSupport3D:Z

    return-void
.end method

.method public setSupport3DBevel(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 109
    iput-boolean p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->bSupport3DBevel:Z

    return-void
.end method
