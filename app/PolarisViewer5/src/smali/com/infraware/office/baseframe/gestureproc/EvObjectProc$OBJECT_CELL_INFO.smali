.class Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;
.super Ljava/lang/Object;
.source "EvObjectProc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OBJECT_CELL_INFO"
.end annotation


# instance fields
.field public BottomPoint:Landroid/graphics/Point;

.field public LTRegionPoint:Landroid/graphics/Point;

.field public final MAX_OBJECT_CELLRECT_COUNT:I

.field public RBRegionPoint:Landroid/graphics/Point;

.field public RightPoint:Landroid/graphics/Point;

.field public bLTAutoFill:Z

.field public bRBAutoFill:Z

.field public endSelectPoint:Landroid/graphics/Point;

.field public halfIconSize:I

.field public halfSheetIconSize:I

.field public mCellRectInfos:[S

.field public nCellRectCount:I

.field public startSelectPoint:Landroid/graphics/Point;

.field final synthetic this$0:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;


# direct methods
.method private constructor <init>(Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 166
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->startSelectPoint:Landroid/graphics/Point;

    .line 169
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->endSelectPoint:Landroid/graphics/Point;

    .line 170
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->BottomPoint:Landroid/graphics/Point;

    .line 171
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RightPoint:Landroid/graphics/Point;

    .line 172
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->LTRegionPoint:Landroid/graphics/Point;

    .line 173
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->bLTAutoFill:Z

    .line 174
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RBRegionPoint:Landroid/graphics/Point;

    .line 175
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->bRBAutoFill:Z

    .line 176
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfIconSize:I

    .line 177
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->halfSheetIconSize:I

    .line 182
    const/16 v0, 0x258

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->MAX_OBJECT_CELLRECT_COUNT:I

    .line 183
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->nCellRectCount:I

    .line 184
    const/16 v0, 0x960

    new-array v0, v0, [S

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->mCellRectInfos:[S

    return-void
.end method

.method synthetic constructor <init>(Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;
    .param p2, "x1"    # Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$1;

    .prologue
    .line 166
    invoke-direct {p0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;-><init>(Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;)V

    return-void
.end method


# virtual methods
.method public SetInit()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 189
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->startSelectPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 190
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->endSelectPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 191
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RightPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 192
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->BottomPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 193
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->LTRegionPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 194
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->bLTAutoFill:Z

    .line 195
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->RBRegionPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 196
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->bRBAutoFill:Z

    .line 198
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->nCellRectCount:I

    .line 199
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_CELL_INFO;->mCellRectInfos:[S

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([SS)V

    .line 200
    return-void
.end method
