.class Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchAdapter;
.super Landroid/widget/ArrayAdapter;
.source "EvBaseViewerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/EvBaseViewerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PopupSearchAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final resId:I

.field final synthetic this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;


# direct methods
.method public constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "textViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4766
    .local p4, "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchAdapter;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 4768
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 4770
    iput-object p2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchAdapter;->mContext:Landroid/content/Context;

    .line 4771
    iput-object p4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchAdapter;->mDataList:Ljava/util/List;

    .line 4772
    iput p3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchAdapter;->resId:I

    .line 4773
    return-void
.end method


# virtual methods
.method public getItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4776
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchAdapter;->mDataList:Ljava/util/List;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 4782
    move-object v2, p2

    .line 4784
    .local v2, "row":Landroid/view/View;
    if-nez v2, :cond_1

    .line 4786
    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchAdapter;->mContext:Landroid/content/Context;

    const-string/jumbo v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 4787
    .local v1, "inflator":Landroid/view/LayoutInflater;
    iget v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchAdapter;->resId:I

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 4788
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchViewHolder;

    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchAdapter;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-direct {v0, v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchViewHolder;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    .line 4789
    .local v0, "holder":Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchViewHolder;
    const v4, 0x7f0b01c1

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchViewHolder;->tvMenuItem:Landroid/widget/TextView;

    .line 4790
    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 4794
    .end local v1    # "inflator":Landroid/view/LayoutInflater;
    :goto_0
    iget-object v5, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchAdapter;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchAdapter;->mDataList:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v5, v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 4796
    .local v3, "to":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 4797
    iget-object v4, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchViewHolder;->tvMenuItem:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4799
    :cond_0
    return-object v2

    .line 4792
    .end local v0    # "holder":Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchViewHolder;
    .end local v3    # "to":Ljava/lang/String;
    :cond_1
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchViewHolder;

    .restart local v0    # "holder":Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchViewHolder;
    goto :goto_0
.end method
