.class public interface abstract Lcom/infraware/office/baseframe/EvBaseE$BaseActivityEventType;
.super Ljava/lang/Object;
.source "EvBaseE.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/EvBaseE;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "BaseActivityEventType"
.end annotation


# static fields
.field public static final MSG_REQUEST_THUMBNAIL_ACTIVITY:I = 0x6

.field public static final MSG_REQUEST_VIEWER_BOOKCLIP:I = 0x7

.field public static final MSG_RESULT_BOOKMARK_ACTIVITY:I = 0x8

.field public static final MSG_RESULT_CAMERA_ACTIVITY:I = 0xe

.field public static final MSG_RESULT_CHART_DATA_ACTIVITY:I = 0x18

.field public static final MSG_RESULT_COLORPICKER_ACTIVITY:I = 0x15

.field public static final MSG_RESULT_COMMON_INPUTFIELD_ACTIVITY:I = 0x16

.field public static final MSG_RESULT_CONDITIONAL_FORMAT_ACTIVITY:I = 0x23

.field public static final MSG_RESULT_CREATE_ROOM_ACTIVITY:I = 0x29

.field public static final MSG_RESULT_FULLSCREEN_VIDEO_ACTIVITY:I = 0x28

.field public static final MSG_RESULT_HEADERFOOTER_ACTIVITY:I = 0x27

.field public static final MSG_RESULT_INSERTHYPERLINK_ACTIVITY:I = 0x14

.field public static final MSG_RESULT_INSERTIMAGE_ACTIVITY:I = 0x4

.field public static final MSG_RESULT_INSERTTABLE_ACTIVITY:I = 0x5

.field public static final MSG_RESULT_INSERT_CAMERA_ACTIVITY:I = 0x12

.field public static final MSG_RESULT_INSERT_CHART_ACTIVITY:I = 0x17

.field public static final MSG_RESULT_INSERT_GALLERY_ACTIVITY:I = 0x11

.field public static final MSG_RESULT_INSERT_GOOGLE_IMAGE_SEARCH_ACTIVITY:I = 0x13

.field public static final MSG_RESULT_INSERT_SYMBOL_ACTIVITY:I = 0x24

.field public static final MSG_RESULT_MANUAL_RESIZE:I = 0x20

.field public static final MSG_RESULT_PAGE_LAYOUT_ACTIVITY:I = 0x21

.field public static final MSG_RESULT_PASSWORD_ACTIVITY:I = 0x1e

.field public static final MSG_RESULT_PDFBOOKMARK_ACTIVITY:I = 0x1a

.field public static final MSG_RESULT_PDF_EXPORT_ACTIVITY:I = 0x1d

.field public static final MSG_RESULT_PRINT_ACTIVITY:I = 0xf

.field public static final MSG_RESULT_RECORDING_ACTIVITY:I = 0x26

.field public static final MSG_RESULT_RESIZE_OBJECT:I = 0x1b

.field public static final MSG_RESULT_ROTATE_OBJECT:I = 0x1c

.field public static final MSG_RESULT_SAVEAS_ACTIVITY:I = 0xc

.field public static final MSG_RESULT_SEND_EMAIL_ACTIVITY:I = 0x34

.field public static final MSG_RESULT_SETTING_ACTIVITY:I = 0xd

.field public static final MSG_RESULT_SHEET_CELLFORMAT_ACTIVITY:I = 0x2f

.field public static final MSG_RESULT_SHEET_MANAGE_ACTIVITY:I = 0x22

.field public static final MSG_RESULT_SLIDE_ANIMATION_ACTIVITY:I = 0x2b

.field public static final MSG_RESULT_SLIDE_ANIMATION_ADD_ACTIVITY:I = 0x2c

.field public static final MSG_RESULT_SLIDE_ANIMATION_EFFECT_ACTIVITY:I = 0x2d

.field public static final MSG_RESULT_SLIDE_LAYOUT_ACTIVITY:I = 0x9

.field public static final MSG_RESULT_SLIDE_LIST_ACTIVITY:I = 0xa

.field public static final MSG_RESULT_SLIDE_PREVIEW_ACTIVITY:I = 0x2e

.field public static final MSG_RESULT_SLIDE_SHOW_ACTIVITY:I = 0xb

.field public static final MSG_RESULT_SLIDE_TRANSITION_ACTIVITY:I = 0x2a

.field public static final MSG_RESULT_SPLIT_CELL_ACTIVITY:I = 0x19

.field public static final MSG_RESULT_TTS_ACTIVITY:I = 0x10

.field public static final MSG_RESULT_UPLOAD_WEBSTORAGE:I = 0x32

.field public static final MSG_RESULT_VIDEO_ACTIVITY:I = 0x25

.field public static final MSG_RESULT_WHEEL_BUTTON_ACTIVITY:I = 0x1f

.field public static final POPUP_DIALOG_PDF_EXPORT:I = 0x33
