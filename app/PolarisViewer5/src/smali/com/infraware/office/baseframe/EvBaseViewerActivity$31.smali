.class Lcom/infraware/office/baseframe/EvBaseViewerActivity$31;
.super Ljava/lang/Object;
.source "EvBaseViewerActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onEmailSendDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 0

    .prologue
    .line 3583
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$31;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v4, 0x1

    .line 3588
    const/4 v0, 0x0

    .line 3590
    .local v0, "sendEmailFinish":Z
    packed-switch p2, :pswitch_data_0

    .line 3600
    :goto_0
    if-nez v0, :cond_0

    .line 3601
    if-nez p2, :cond_1

    .line 3602
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$31;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->showDialog(I)V

    .line 3609
    :cond_0
    :goto_1
    return-void

    .line 3592
    :pswitch_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$31;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$31;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onSendEMail(ZI)Z

    move-result v0

    .line 3593
    goto :goto_0

    .line 3596
    :pswitch_1
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$31;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$31;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v2

    invoke-virtual {v1, v4, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onSendEMail(ZI)Z

    move-result v0

    goto :goto_0

    .line 3604
    :cond_1
    if-ne p2, v4, :cond_0

    .line 3605
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$31;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrPdfExportFilePath:Ljava/lang/String;

    .line 3606
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$31;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/16 v2, 0x33

    invoke-virtual {v1, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->showDialog(I)V

    goto :goto_1

    .line 3590
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
