.class Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;
.super Ljava/lang/Object;
.source "EvObjectProc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OBJECT_LINE_INFO"
.end annotation


# static fields
.field public static final MAX_ADJUSTHANDLE_ARRSIZE:I = 0xa


# instance fields
.field public endEditingPoint:Landroid/graphics/Point;

.field public endPoint:Landroid/graphics/Point;

.field public nAdjustHandleCnt:I

.field public nSmartGuidesCnt:I

.field public pointImageSize:I

.field public ptAdjustControls:[Landroid/graphics/Point;

.field public rotateAngle:I

.field public startEditingPoint:Landroid/graphics/Point;

.field public startPoint:Landroid/graphics/Point;

.field final synthetic this$0:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;


# direct methods
.method public constructor <init>(Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;)V
    .locals 5

    .prologue
    const/16 v4, 0xa

    const/4 v2, -0x1

    const/4 v3, 0x0

    .line 434
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 421
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, v3, v3}, Landroid/graphics/Point;-><init>(II)V

    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->startPoint:Landroid/graphics/Point;

    .line 422
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, v3, v3}, Landroid/graphics/Point;-><init>(II)V

    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->endPoint:Landroid/graphics/Point;

    .line 423
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, v3, v3}, Landroid/graphics/Point;-><init>(II)V

    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->startEditingPoint:Landroid/graphics/Point;

    .line 424
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, v3, v3}, Landroid/graphics/Point;-><init>(II)V

    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->endEditingPoint:Landroid/graphics/Point;

    .line 425
    iput v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->rotateAngle:I

    .line 427
    const/16 v1, 0x23

    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->pointImageSize:I

    .line 429
    iput v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->nAdjustHandleCnt:I

    .line 430
    iput v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->nSmartGuidesCnt:I

    .line 432
    new-array v1, v4, [Landroid/graphics/Point;

    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    .line 435
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_0

    .line 436
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2, v3, v3}, Landroid/graphics/Point;-><init>(II)V

    aput-object v2, v1, v0

    .line 435
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 438
    :cond_0
    return-void
.end method


# virtual methods
.method public SetInit()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 440
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->startPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 441
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->endPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 442
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->startEditingPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 443
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->endEditingPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 444
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->rotateAngle:I

    .line 445
    iput v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->nAdjustHandleCnt:I

    .line 446
    iput v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_LINE_INFO;->nSmartGuidesCnt:I

    .line 447
    return-void
.end method
