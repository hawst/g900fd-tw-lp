.class Lcom/infraware/office/baseframe/porting/EvClipboardExManager$1;
.super Landroid/os/Handler;
.source "EvClipboardExManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/porting/EvClipboardExManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$1;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 39
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 40
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v3, "type"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 41
    .local v1, "nDataType":I
    const-string/jumbo v3, "data"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 42
    .local v2, "strData":Ljava/lang/String;
    const-string/jumbo v3, "native"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 44
    const-string/jumbo v3, "value"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 45
    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$1;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    # getter for: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v3}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$000(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v3

    invoke-virtual {v3, v6, v1, v4}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    .line 60
    :goto_0
    return-void

    .line 46
    :cond_0
    const-string/jumbo v3, "format"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 47
    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$1;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    # getter for: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v3}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$000(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v3

    invoke-virtual {v3, v7, v1, v4}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    goto :goto_0

    .line 49
    :cond_1
    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$1;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    # getter for: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v3}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$000(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v3

    invoke-virtual {v3, v5, v1, v4}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    goto :goto_0

    .line 53
    :cond_2
    const-string/jumbo v3, "value"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 54
    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$1;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    # getter for: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v3}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$000(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v3

    invoke-virtual {v3, v6, v1, v2}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    goto :goto_0

    .line 55
    :cond_3
    const-string/jumbo v3, "format"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 56
    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$1;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    # getter for: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v3}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$000(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v3

    invoke-virtual {v3, v7, v1, v2}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    goto :goto_0

    .line 58
    :cond_4
    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$1;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    # getter for: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v3}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$000(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v3

    invoke-virtual {v3, v5, v1, v2}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    goto :goto_0
.end method
