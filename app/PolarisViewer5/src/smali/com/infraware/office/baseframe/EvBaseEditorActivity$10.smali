.class Lcom/infraware/office/baseframe/EvBaseEditorActivity$10;
.super Ljava/lang/Object;
.source "EvBaseEditorActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onCreateMsgPopup(I)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)V
    .locals 0

    .prologue
    .line 2437
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$10;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2439
    const-string/jumbo v0, "EvBaseEditorActivity"

    const-string/jumbo v1, "OnSave and Finish : set to call getPageThumbnail()"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 2442
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$10;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    iget v0, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_nPopupType:I

    sparse-switch v0, :sswitch_data_0

    .line 2481
    :cond_0
    :goto_0
    return-void

    .line 2444
    :sswitch_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$10;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    iput-boolean v2, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbSaveAndFinish:Z

    .line 2445
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$10;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    iget v0, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mDocType:I

    if-ne v0, v3, :cond_1

    .line 2448
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$10;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$10;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    iget v1, v1, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mnSaveThumbOption:I

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getPageThumbnail(I)V

    .line 2450
    :cond_1
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->SAVE_2007()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2451
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$10;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onCheckTypeSave()V

    .line 2452
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$10;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    iget-boolean v0, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mIsDifType:Z

    if-eqz v0, :cond_0

    .line 2453
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$10;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMsgPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$10;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMsgPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2454
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$10;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMsgPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 2455
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$10;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    const/16 v1, 0x19

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->showDialog(I)V

    goto :goto_0

    .line 2463
    :sswitch_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$10;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    iput-boolean v2, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbSaveAndFinish:Z

    .line 2464
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$10;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    iget v0, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mDocType:I

    if-ne v0, v3, :cond_3

    .line 2467
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$10;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getPageThumbnail()V

    .line 2469
    :cond_3
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$10;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->GetOpenType()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$10;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    iget-boolean v0, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mIsDifType:Z

    if-eq v0, v2, :cond_0

    .line 2475
    :sswitch_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$10;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbSaveAs:Z
    invoke-static {v0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->access$400(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)Z

    move-result v0

    if-ne v0, v2, :cond_0

    goto :goto_0

    .line 2442
    :sswitch_data_0
    .sparse-switch
        0x18 -> :sswitch_0
        0x19 -> :sswitch_1
        0x1f -> :sswitch_2
    .end sparse-switch
.end method
