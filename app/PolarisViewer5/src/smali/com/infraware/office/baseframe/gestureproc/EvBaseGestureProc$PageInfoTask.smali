.class Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;
.super Ljava/util/TimerTask;
.source "EvBaseGestureProc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PageInfoTask"
.end annotation


# instance fields
.field protected final mAnimInterval:J

.field protected final mInfoInterval:J

.field protected final mMinusRatio:F

.field protected final mScrollBarInterval:J

.field mStartTime:J

.field final synthetic this$0:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;J)V
    .locals 2
    .param p2, "startTime"    # J

    .prologue
    .line 656
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    .line 657
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 650
    const-wide/16 v0, 0x5dc

    iput-wide v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;->mInfoInterval:J

    .line 651
    const-wide/16 v0, 0x28a

    iput-wide v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;->mScrollBarInterval:J

    .line 652
    const-wide/16 v0, 0xc8

    iput-wide v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;->mAnimInterval:J

    .line 653
    const v0, 0x3f933333    # 1.15f

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;->mMinusRatio:F

    .line 658
    iput-wide p2, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;->mStartTime:J

    .line 659
    const/16 v0, 0xe6

    iput v0, p1, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mScrollBarAlpha:I

    .line 660
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 663
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoHandler:Landroid/os/Handler;

    new-instance v1, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask$1;

    invoke-direct {v1, p0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask$1;-><init>(Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 704
    return-void
.end method
