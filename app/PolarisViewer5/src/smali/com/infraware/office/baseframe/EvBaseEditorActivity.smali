.class public abstract Lcom/infraware/office/baseframe/EvBaseEditorActivity;
.super Lcom/infraware/office/baseframe/EvBaseViewerActivity;
.source "EvBaseEditorActivity.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_BORDER_STYLE;
.implements Lcom/infraware/office/evengine/E$EV_BORDER_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_BULLETNUMBER_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_BULLET_NUMBERING;
.implements Lcom/infraware/office/evengine/E$EV_CARET_MARK;
.implements Lcom/infraware/office/evengine/E$EV_CELL_DELETE_MODE;
.implements Lcom/infraware/office/evengine/E$EV_CELL_INSERT_DELETE;
.implements Lcom/infraware/office/evengine/E$EV_CELL_INSERT_MODE;
.implements Lcom/infraware/office/evengine/E$EV_CHART_BAR_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_CLIPBOARD_MODE;
.implements Lcom/infraware/office/evengine/E$EV_DOC_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_EDIT_CURSOR_MODE;
.implements Lcom/infraware/office/evengine/E$EV_EDIT_OBJECT_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_ERROR_CODE;
.implements Lcom/infraware/office/evengine/E$EV_IMAGE_MASK;
.implements Lcom/infraware/office/evengine/E$EV_INDENTATION;
.implements Lcom/infraware/office/evengine/E$EV_MASK_ATT;
.implements Lcom/infraware/office/evengine/E$EV_PARAGRAPH_ALIGN;
.implements Lcom/infraware/office/evengine/E$EV_PARAGRAPH_MASK;
.implements Lcom/infraware/office/evengine/E$EV_PEN_MODE;
.implements Lcom/infraware/office/evengine/E$EV_REDO_UNDO;
.implements Lcom/infraware/office/evengine/E$EV_REPLACE_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_ROTATE_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_SAVE_OPTION;
.implements Lcom/infraware/office/evengine/E$EV_SELECT_CELL_MODE;
.implements Lcom/infraware/office/evengine/E$EV_SHEET_CHART_AXIS;
.implements Lcom/infraware/office/evengine/E$EV_SHEET_CHART_LEGEND;
.implements Lcom/infraware/office/evengine/E$EV_SHEET_CHART_SERIES;
.implements Lcom/infraware/office/evengine/E$EV_SHEET_CHART_STYLE;
.implements Lcom/infraware/office/evengine/E$EV_SHEET_CHART_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_STATUS;
.implements Lcom/infraware/office/evengine/E$EV_WORD_CELL_EQUAL_WIDTH_HEIGHT;
.implements Lcom/infraware/office/evengine/E$EV_WORD_CELL_MERGE_SEP;
.implements Lcom/infraware/office/evengine/EvListener$EditorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/office/baseframe/EvBaseEditorActivity$ImageInsertViewHolder;
    }
.end annotation


# static fields
.field private static final MAX_IMAGESIZE:I = 0x400

.field private static final MAX_OBJECTSIZE:I = 0x22e

.field private static final MIN_IMAGESIZE:I = 0x1

.field private static final MIN_OBJECTSIZE:I = 0x2


# instance fields
.field public final COLOR_PICKER_ACTIVITY_SLIDE_BACKGROUND:I

.field private final LOG_CAT:Ljava/lang/String;

.field protected final METAKEY_CTRL_ON:I

.field public MediaDirectory:Ljava/lang/String;

.field docView:Landroid/widget/RelativeLayout;

.field final handler:Landroid/os/Handler;

.field protected mBShowIMEAfterScreenChanged:Z

.field protected mBWPInfo:Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mCameraImageUri:Landroid/net/Uri;

.field protected mCellStatus:I

.field private mCurrentLocale:Ljava/util/Locale;

.field private mDownloadImageFilename:Ljava/lang/String;

.field mEventHandler:Landroid/os/Handler;

.field private mFileSaveAs:Ljava/lang/String;

.field private mFilename:Ljava/lang/String;

.field private mImageDataIntent:Landroid/content/Intent;

.field private mImageOrientation:I

.field private mImageSize:Landroid/graphics/Point;

.field mIsBackPressSave:Z

.field mIsDifType:Z

.field public mIsReplace:Z

.field mOriginalHeight:I

.field mOriginalWidth:I

.field private mPaperMask:I

.field private mProgressDialog:Landroid/app/AlertDialog;

.field protected mSaveAsWebStorage:Z

.field private mSaveMenu:I

.field protected mSaveWebStorage:Z

.field private mSpinedImageDegree:I

.field private mStyle_num:I

.field public mVideoFrameLayout:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

.field private m_ToastMsg:Landroid/widget/Toast;

.field private m_nErrMsgId:I

.field public m_oHandler:Landroid/os/Handler;

.field public mbCroppingMode:Z

.field protected mbFreedrawActionReady:Z

.field private mbPdfExport:Z

.field private mbSaveAs:Z

.field public mbSpellCheck:Z

.field private mpPaper:Lcom/infraware/office/evengine/EV$PAPER_INFO;

.field setFieldValue:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 81
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;-><init>()V

    .line 87
    const-string/jumbo v0, "EvBaseEditorActivity"

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->LOG_CAT:Ljava/lang/String;

    .line 89
    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mBWPInfo:Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    .line 90
    iput v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mCellStatus:I

    .line 91
    iput v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_nErrMsgId:I

    .line 92
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mIsDifType:Z

    iput-boolean v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mIsBackPressSave:Z

    .line 93
    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_ToastMsg:Landroid/widget/Toast;

    .line 94
    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mCameraImageUri:Landroid/net/Uri;

    .line 95
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mBShowIMEAfterScreenChanged:Z

    .line 96
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbSaveAs:Z

    .line 97
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbPdfExport:Z

    .line 98
    iput v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mSaveMenu:I

    .line 100
    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mFileSaveAs:Ljava/lang/String;

    .line 104
    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->docView:Landroid/widget/RelativeLayout;

    .line 105
    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mpPaper:Lcom/infraware/office/evengine/EV$PAPER_INFO;

    .line 106
    iput v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mPaperMask:I

    .line 108
    const/4 v0, 0x2

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->COLOR_PICKER_ACTIVITY_SLIDE_BACKGROUND:I

    .line 110
    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mVideoFrameLayout:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    .line 113
    iput v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mOriginalWidth:I

    .line 114
    iput v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mOriginalHeight:I

    .line 115
    iput v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mSpinedImageDegree:I

    .line 125
    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->setFieldValue:Ljava/lang/String;

    .line 126
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbCroppingMode:Z

    .line 127
    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_oHandler:Landroid/os/Handler;

    .line 131
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mStyle_num:I

    .line 135
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mSaveWebStorage:Z

    .line 136
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mSaveAsWebStorage:Z

    .line 138
    iput v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mImageOrientation:I

    .line 140
    const/16 v0, 0x1000

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->METAKEY_CTRL_ON:I

    .line 142
    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mDownloadImageFilename:Ljava/lang/String;

    .line 143
    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mProgressDialog:Landroid/app/AlertDialog;

    .line 144
    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mImageDataIntent:Landroid/content/Intent;

    .line 146
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbSpellCheck:Z

    .line 148
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbFreedrawActionReady:Z

    .line 683
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$2;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity$2;-><init>(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEventHandler:Landroid/os/Handler;

    .line 1061
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$6;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity$6;-><init>(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->handler:Landroid/os/Handler;

    .line 1070
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mImageSize:Landroid/graphics/Point;

    .line 1273
    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mBitmap:Landroid/graphics/Bitmap;

    .line 1275
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mIsReplace:Z

    return-void
.end method

.method private ConvertMmToPx(I)I
    .locals 4
    .param p1, "mm"    # I

    .prologue
    .line 1816
    mul-int/lit16 v0, p1, 0xc8

    int-to-double v0, v0

    const-wide v2, 0x4039666666666666L    # 25.4

    div-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method private ConvertPxToMm(I)I
    .locals 4
    .param p1, "px"    # I

    .prologue
    .line 1809
    int-to-double v0, p1

    const-wide v2, 0x4039666666666666L    # 25.4

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4069000000000000L    # 200.0

    div-double/2addr v0, v2

    double-to-int v0, v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public static declared-synchronized GetExifOrientation(Ljava/lang/String;)I
    .locals 9
    .param p0, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v8, -0x1

    .line 1278
    const-class v6, Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    monitor-enter v6

    const/4 v0, 0x0

    .line 1279
    .local v0, "degree":I
    const/4 v2, 0x0

    .line 1282
    .local v2, "exif":Landroid/media/ExifInterface;
    :try_start_0
    new-instance v3, Landroid/media/ExifInterface;

    invoke-direct {v3, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .end local v2    # "exif":Landroid/media/ExifInterface;
    .local v3, "exif":Landroid/media/ExifInterface;
    move-object v2, v3

    .line 1289
    .end local v3    # "exif":Landroid/media/ExifInterface;
    .restart local v2    # "exif":Landroid/media/ExifInterface;
    :goto_0
    if-eqz v2, :cond_0

    .line 1291
    :try_start_1
    const-string/jumbo v5, "Orientation"

    const/4 v7, -0x1

    invoke-virtual {v2, v5, v7}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    .line 1292
    .local v4, "orientation":I
    if-eq v4, v8, :cond_0

    .line 1295
    packed-switch v4, :pswitch_data_0

    .line 1309
    .end local v4    # "orientation":I
    :cond_0
    :goto_1
    :pswitch_0
    monitor-exit v6

    return v0

    .line 1284
    :catch_0
    move-exception v1

    .line 1286
    .local v1, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1278
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    monitor-exit v6

    throw v5

    .line 1298
    .restart local v4    # "orientation":I
    :pswitch_1
    const/16 v0, 0x5a

    .line 1299
    goto :goto_1

    .line 1301
    :pswitch_2
    const/16 v0, 0xb4

    .line 1302
    goto :goto_1

    .line 1304
    :pswitch_3
    const/16 v0, 0x10e

    goto :goto_1

    .line 1295
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private InsertShapeDrawing()V
    .locals 0

    .prologue
    .line 2850
    return-void
.end method

.method private OnMaskImage()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2351
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->hide()V

    .line 2352
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const/16 v1, 0x44

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->setEvent(I)V

    .line 2353
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const v1, 0x7f07018b

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->setTitle(I)V

    .line 2354
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->show()V

    .line 2355
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/infraware/office/evengine/EvInterface;->ISetCroppingMode(II)V

    .line 2356
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbCroppingMode:Z

    .line 2358
    const v0, 0x7f0b002c

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->docView:Landroid/widget/RelativeLayout;

    .line 2360
    return-void
.end method

.method private OnMultiSelection()V
    .locals 0

    .prologue
    .line 2348
    return-void
.end method

.method private OnResultInsertImageActivity(Landroid/content/Intent;)V
    .locals 12
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 988
    if-eqz p1, :cond_0

    .line 989
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 990
    .local v1, "uri":Landroid/net/Uri;
    if-eqz v1, :cond_0

    .line 991
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v9

    .line 992
    .local v9, "scheme":Ljava/lang/String;
    if-nez v9, :cond_1

    .line 1058
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v9    # "scheme":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 995
    .restart local v1    # "uri":Landroid/net/Uri;
    .restart local v9    # "scheme":Ljava/lang/String;
    :cond_1
    const-string/jumbo v8, ""

    .line 996
    .local v8, "filename":Ljava/lang/String;
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "file"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 997
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 998
    const/4 v0, 0x7

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 1023
    :cond_2
    :goto_1
    if-eqz v8, :cond_7

    const-string/jumbo v0, "https://"

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string/jumbo v0, "http://"

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1024
    invoke-static {v8}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->GetExifOrientation(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mSpinedImageDegree:I

    .line 1026
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v8, v0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnResizeImage(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0

    .line 999
    :cond_3
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "content"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1000
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string/jumbo v0, "_data"

    aput-object v0, v2, v11

    .line 1001
    .local v2, "proj":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 1004
    .local v7, "cur":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 1005
    if-nez v7, :cond_4

    .line 1016
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1017
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1008
    :cond_4
    :try_start_1
    const-string/jumbo v0, "_data"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 1009
    .local v6, "colum_index":I
    const/4 v0, -0x1

    if-eq v6, v0, :cond_5

    .line 1010
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1011
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1013
    :cond_5
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1016
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1017
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 1014
    .end local v6    # "colum_index":I
    :catch_0
    move-exception v0

    .line 1016
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1017
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 1016
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_6

    invoke-interface {v7}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_6

    .line 1017
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    .line 1027
    .end local v2    # "proj":[Ljava/lang/String;
    .end local v7    # "cur":Landroid/database/Cursor;
    :cond_7
    invoke-static {p0}, Lcom/infraware/common/util/Utils;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1031
    iput-object v10, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mImageDataIntent:Landroid/content/Intent;

    .line 1032
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mImageDataIntent:Landroid/content/Intent;

    .line 1033
    const/16 v0, 0x2d

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->showDialog(I)V

    .line 1035
    new-instance v0, Ljava/lang/Thread;

    new-instance v3, Lcom/infraware/office/baseframe/EvBaseEditorActivity$5;

    invoke-direct {v3, p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity$5;-><init>(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)V

    invoke-direct {v0, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 1053
    :cond_8
    invoke-static {p0}, Lcom/infraware/common/util/Utils;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1054
    const v0, 0x7f070092

    invoke-static {p0, v0, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0
.end method

.method private OnResultInsertSymbol(Landroid/content/Intent;)V
    .locals 4
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 924
    iget v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mDocType:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 925
    const-string/jumbo v1, "wCode"

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 927
    .local v0, "wCod":I
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1, v3, v0, v3}, Lcom/infraware/office/evengine/EvInterface;->ICharInsert(III)V

    .line 929
    .end local v0    # "wCod":I
    :cond_0
    return-void
.end method

.method private OnResultPageLayout(Landroid/content/Intent;)V
    .locals 0
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 950
    return-void
.end method

.method private OnResultRecordingActivity(Landroid/content/Intent;)V
    .locals 19
    .param p1, "oData"    # Landroid/content/Intent;

    .prologue
    .line 1520
    if-nez p1, :cond_1

    .line 1659
    :cond_0
    :goto_0
    return-void

    .line 1523
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 1525
    .local v3, "oUri":Landroid/net/Uri;
    if-eqz v3, :cond_0

    .line 1528
    const/16 v17, 0x0

    .line 1529
    .local v17, "oCursor":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v7, "_id"

    aput-object v7, v4, v5

    const/4 v5, 0x1

    const-string/jumbo v7, "_data"

    aput-object v7, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 1534
    if-eqz v17, :cond_0

    .line 1537
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1538
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_2

    .line 1540
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1544
    :cond_2
    const-string/jumbo v2, "_id"

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 1545
    .local v14, "nId":J
    const-string/jumbo v2, "_data"

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1546
    .local v10, "szFilePath":Ljava/lang/String;
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 1548
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    .line 1549
    .local v13, "contentResolver":Landroid/content/ContentResolver;
    const/4 v2, 0x1

    const/4 v4, 0x0

    invoke-static {v13, v14, v15, v2, v4}, Landroid/provider/MediaStore$Video$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 1556
    .local v6, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-lez v2, :cond_3

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-gtz v2, :cond_4

    .line 1558
    :cond_3
    if-eqz v6, :cond_0

    .line 1559
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 1650
    :cond_4
    const/16 v2, 0x2f

    invoke-virtual {v10, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v16

    .line 1651
    .local v16, "nIdx":I
    const/4 v2, 0x0

    move/from16 v0, v16

    invoke-virtual {v10, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v18

    .line 1652
    .local v18, "videoDirectoryPath":Ljava/lang/String;
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->MediaDirectory:Ljava/lang/String;

    .line 1653
    add-int/lit8 v2, v16, 0x1

    invoke-virtual {v10, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    .line 1654
    .local v12, "VideoFileName":Ljava/lang/String;
    const-string/jumbo v2, "EvBaseEditorActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "VideoPath "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1655
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mIsReplace:Z

    const/4 v11, 0x0

    invoke-virtual/range {v4 .. v11}, Lcom/infraware/office/evengine/EvInterface;->IVideoInsert(Ljava/lang/String;Landroid/graphics/Bitmap;IIZLjava/lang/String;Z)V

    .line 1657
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    .line 1658
    const/4 v6, 0x0

    .line 1659
    goto/16 :goto_0
.end method

.method private OnResultResizeObjActivity(Landroid/content/Intent;)V
    .locals 9
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    .line 628
    const-string/jumbo v6, "width"

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 629
    .local v5, "width":I
    const-string/jumbo v6, "height"

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 631
    .local v0, "height":I
    const-string/jumbo v6, "type"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 633
    .local v4, "type":Ljava/lang/String;
    const-string/jumbo v6, "####"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "OnResultResizeObjActivity : result type : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    const-string/jumbo v6, "table"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 637
    iget-object v6, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v6, v5, v0}, Lcom/infraware/office/evengine/EvInterface;->ISetObjResize(II)V

    .line 655
    :cond_0
    :goto_0
    return-void

    .line 641
    :cond_1
    iget v6, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mOriginalWidth:I

    if-ne v6, v5, :cond_2

    iget v6, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mOriginalHeight:I

    if-eq v6, v0, :cond_0

    .line 642
    :cond_2
    const/16 v1, 0x4000

    .line 643
    .local v1, "nSelector":I
    iget-object v6, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-static {v6, v1}, Lcom/infraware/office/evengine/EvShapeInterfaceUtil;->getShapeInfo(Lcom/infraware/office/evengine/EvInterface;I)Ljava/util/Hashtable;

    move-result-object v2

    .line 645
    .local v2, "nSizeInfo":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Object;>;"
    const/16 v6, 0x4000

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;

    .line 646
    .local v3, "pSize":Lcom/infraware/office/evengine/EV$SHAPE_SIZE;
    if-nez v3, :cond_3

    .line 647
    new-instance v6, Ljava/lang/NullPointerException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Size Info is NULL, request Selector = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 649
    :cond_3
    int-to-float v6, v5

    iput v6, v3, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;->nWidth:F

    .line 650
    int-to-float v6, v0

    iput v6, v3, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;->nHeight:F

    .line 652
    iget-object v6, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-static {v6, v2}, Lcom/infraware/office/evengine/EvShapeInterfaceUtil;->setShapeInfo(Lcom/infraware/office/evengine/EvInterface;Ljava/util/Hashtable;)V

    goto :goto_0
.end method

.method private OnResultRotateObjeActivity(Landroid/content/Intent;)V
    .locals 10
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 953
    const-string/jumbo v9, "angle"

    invoke-virtual {p1, v9, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 955
    .local v0, "degree":I
    const-string/jumbo v9, "flip"

    invoke-virtual {p1, v9, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 956
    .local v1, "flip":Ljava/lang/Boolean;
    const-string/jumbo v9, "mirror"

    invoke-virtual {p1, v9, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 958
    .local v2, "mirror":Ljava/lang/Boolean;
    const-string/jumbo v9, "preset"

    invoke-virtual {p1, v9, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 961
    .local v6, "rotationPreset":I
    const/16 v3, 0x4000

    .line 962
    .local v3, "nSelector":I
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-static {v9, v3}, Lcom/infraware/office/evengine/EvShapeInterfaceUtil;->getShapeInfo(Lcom/infraware/office/evengine/EvInterface;I)Ljava/util/Hashtable;

    move-result-object v4

    .line 964
    .local v4, "nSizeInfo":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Object;>;"
    const/16 v9, 0x4000

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;

    .line 965
    .local v5, "pSize":Lcom/infraware/office/evengine/EV$SHAPE_SIZE;
    if-nez v5, :cond_0

    .line 966
    new-instance v7, Ljava/lang/NullPointerException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Size Info is NULL, request Selector = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 968
    :cond_0
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-ne v9, v7, :cond_3

    .line 969
    iget-boolean v9, v5, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;->bFlip:Z

    if-nez v9, :cond_2

    :goto_0
    iput-boolean v7, v5, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;->bFlip:Z

    .line 975
    :cond_1
    :goto_1
    int-to-float v7, v0

    iput v7, v5, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;->nRotation:F

    .line 976
    iput v6, v5, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;->nRotationPreset:I

    .line 978
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-static {v7, v4}, Lcom/infraware/office/evengine/EvShapeInterfaceUtil;->setShapeInfo(Lcom/infraware/office/evengine/EvInterface;Ljava/util/Hashtable;)V

    .line 979
    return-void

    :cond_2
    move v7, v8

    .line 969
    goto :goto_0

    .line 971
    :cond_3
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-ne v9, v7, :cond_1

    .line 972
    iget-boolean v9, v5, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;->bMirror:Z

    if-nez v9, :cond_4

    :goto_2
    iput-boolean v7, v5, Lcom/infraware/office/evengine/EV$SHAPE_SIZE;->bMirror:Z

    goto :goto_1

    :cond_4
    move v7, v8

    goto :goto_2
.end method

.method private OnResultSplitCellActivity(Landroid/content/Intent;)V
    .locals 6
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 982
    const-string/jumbo v2, "rows"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 983
    .local v1, "rowString":Ljava/lang/String;
    const-string/jumbo v2, "cols"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 984
    .local v0, "colString":Ljava/lang/String;
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v3, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/infraware/office/evengine/EvInterface;->ICellMergeSeparate(III)V

    .line 985
    return-void
.end method

.method private TitleRename()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 555
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mFileSaveAs:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 556
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mFileSaveAs:Ljava/lang/String;

    invoke-static {v1}, Lcom/infraware/common/util/FileUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/MainActionBar;->setTitle(Ljava/lang/String;)V

    .line 557
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mFileSaveAs:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mstrOpenFilePath:Ljava/lang/String;

    .line 558
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->getDocFileExtentionType(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mDocExtensionType:I

    .line 559
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0, v2}, Lcom/infraware/office/actionbar/MainActionBar;->showTitle(Z)V

    .line 560
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mFileSaveAs:Ljava/lang/String;

    .line 565
    :cond_0
    :goto_0
    return-void

    .line 562
    :cond_1
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_STANDARD_UI()Z

    move-result v0

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IDocumentModified_Editor()Z

    move-result v0

    if-nez v0, :cond_0

    .line 563
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0, v2}, Lcom/infraware/office/actionbar/MainActionBar;->showTitle(Z)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mDownloadImageFilename:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/infraware/office/baseframe/EvBaseEditorActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseEditorActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mDownloadImageFilename:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mImageDataIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$202(Lcom/infraware/office/baseframe/EvBaseEditorActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseEditorActivity;
    .param p1, "x1"    # I

    .prologue
    .line 81
    iput p1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mSpinedImageDegree:I

    return p1
.end method

.method static synthetic access$300(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mProgressDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$400(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbSaveAs:Z

    return v0
.end method

.method private copyFile(Ljava/io/File;Ljava/io/File;)Z
    .locals 26
    .param p1, "sourceFile"    # Ljava/io/File;
    .param p2, "targetFile"    # Ljava/io/File;

    .prologue
    .line 487
    const/4 v14, 0x0

    .line 488
    .local v14, "fis":Ljava/io/FileInputStream;
    const/16 v16, 0x0

    .line 489
    .local v16, "fos":Ljava/io/FileOutputStream;
    const/4 v2, 0x0

    .line 490
    .local v2, "fcIn":Ljava/nio/channels/FileChannel;
    const/4 v7, 0x0

    .line 492
    .local v7, "fcOut":Ljava/nio/channels/FileChannel;
    const/high16 v8, 0x10000

    .line 493
    .local v8, "CHANNEL_BUFFER_SIZE":I
    const/high16 v9, 0x500000

    .line 495
    .local v9, "HUGE_FILE_SIZE":I
    const/16 v18, 0x0

    .line 496
    .local v18, "freeBlock":I
    const/4 v10, 0x0

    .line 498
    .local v10, "blockSize":I
    :try_start_0
    new-instance v21, Landroid/os/StatFs;

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-direct {v0, v5}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 499
    .local v21, "sf":Landroid/os/StatFs;
    invoke-virtual/range {v21 .. v21}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v18

    .line 500
    invoke-virtual/range {v21 .. v21}, Landroid/os/StatFs;->getBlockSize()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v10

    .line 507
    :try_start_1
    new-instance v15, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v15, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 508
    .end local v14    # "fis":Ljava/io/FileInputStream;
    .local v15, "fis":Ljava/io/FileInputStream;
    :try_start_2
    invoke-virtual {v15}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    .line 509
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v22

    .line 511
    .local v22, "size":J
    new-instance v17, Ljava/io/FileOutputStream;

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 512
    .end local v16    # "fos":Ljava/io/FileOutputStream;
    .local v17, "fos":Ljava/io/FileOutputStream;
    :try_start_3
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v7

    .line 514
    int-to-long v5, v9

    cmp-long v5, v22, v5

    if-lez v5, :cond_5

    const/high16 v5, 0x280000

    int-to-long v5, v5

    add-long v5, v5, v22

    :goto_0
    int-to-long v0, v10

    move-wide/from16 v24, v0

    div-long v19, v5, v24

    .line 515
    .local v19, "minFree":J
    move/from16 v0, v18

    int-to-long v5, v0

    cmp-long v5, v5, v19

    if-gez v5, :cond_6

    .line 516
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->delete()Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 517
    const/4 v5, 0x0

    .line 531
    if-eqz v2, :cond_0

    .line 532
    :try_start_4
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 533
    :cond_0
    if-eqz v15, :cond_1

    .line 534
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V

    .line 535
    :cond_1
    if-eqz v7, :cond_2

    .line 536
    invoke-virtual {v7}, Ljava/nio/channels/FileChannel;->close()V

    .line 537
    :cond_2
    if-eqz v17, :cond_3

    .line 538
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_3
    move-object/from16 v16, v17

    .end local v17    # "fos":Ljava/io/FileOutputStream;
    .restart local v16    # "fos":Ljava/io/FileOutputStream;
    move-object v14, v15

    .line 551
    .end local v15    # "fis":Ljava/io/FileInputStream;
    .end local v19    # "minFree":J
    .end local v21    # "sf":Landroid/os/StatFs;
    .end local v22    # "size":J
    .restart local v14    # "fis":Ljava/io/FileInputStream;
    :cond_4
    :goto_1
    return v5

    .line 501
    :catch_0
    move-exception v13

    .line 502
    .local v13, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->delete()Z

    .line 503
    const/4 v5, 0x0

    goto :goto_1

    .line 514
    .end local v13    # "e":Ljava/lang/IllegalArgumentException;
    .end local v14    # "fis":Ljava/io/FileInputStream;
    .end local v16    # "fos":Ljava/io/FileOutputStream;
    .restart local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "fos":Ljava/io/FileOutputStream;
    .restart local v21    # "sf":Landroid/os/StatFs;
    .restart local v22    # "size":J
    :cond_5
    const-wide/16 v5, 0x2

    :try_start_5
    div-long v5, v22, v5

    add-long v5, v5, v22

    goto :goto_0

    .line 539
    .restart local v19    # "minFree":J
    :catch_1
    move-exception v13

    .line 540
    .local v13, "e":Ljava/io/IOException;
    const/4 v5, 0x0

    move-object/from16 v16, v17

    .end local v17    # "fos":Ljava/io/FileOutputStream;
    .restart local v16    # "fos":Ljava/io/FileOutputStream;
    move-object v14, v15

    .end local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "fis":Ljava/io/FileInputStream;
    goto :goto_1

    .line 520
    .end local v13    # "e":Ljava/io/IOException;
    .end local v14    # "fis":Ljava/io/FileInputStream;
    .end local v16    # "fos":Ljava/io/FileOutputStream;
    .restart local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "fos":Ljava/io/FileOutputStream;
    :cond_6
    const-wide/16 v3, 0x0

    .line 521
    .local v3, "position":J
    const-wide/16 v11, 0x0

    .line 522
    .local v11, "count":J
    :goto_2
    cmp-long v5, v3, v22

    if-gez v5, :cond_7

    .line 523
    int-to-long v5, v8

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->transferTo(JJLjava/nio/channels/WritableByteChannel;)J
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-result-wide v5

    add-long/2addr v3, v5

    .line 524
    const-wide/16 v5, 0x1

    add-long/2addr v11, v5

    goto :goto_2

    .line 531
    :cond_7
    if-eqz v2, :cond_8

    .line 532
    :try_start_6
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 533
    :cond_8
    if-eqz v15, :cond_9

    .line 534
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V

    .line 535
    :cond_9
    if-eqz v7, :cond_a

    .line 536
    invoke-virtual {v7}, Ljava/nio/channels/FileChannel;->close()V

    .line 537
    :cond_a
    if-eqz v17, :cond_b

    .line 538
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 546
    :cond_b
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xb

    if-lt v5, v6, :cond_c

    .line 548
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onMediaDBBroadCast(Ljava/lang/String;)V

    .line 551
    :cond_c
    const/4 v5, 0x1

    move-object/from16 v16, v17

    .end local v17    # "fos":Ljava/io/FileOutputStream;
    .restart local v16    # "fos":Ljava/io/FileOutputStream;
    move-object v14, v15

    .end local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "fis":Ljava/io/FileInputStream;
    goto :goto_1

    .line 539
    .end local v14    # "fis":Ljava/io/FileInputStream;
    .end local v16    # "fos":Ljava/io/FileOutputStream;
    .restart local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "fos":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v13

    .line 540
    .restart local v13    # "e":Ljava/io/IOException;
    const/4 v5, 0x0

    move-object/from16 v16, v17

    .end local v17    # "fos":Ljava/io/FileOutputStream;
    .restart local v16    # "fos":Ljava/io/FileOutputStream;
    move-object v14, v15

    .end local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "fis":Ljava/io/FileInputStream;
    goto :goto_1

    .line 526
    .end local v3    # "position":J
    .end local v11    # "count":J
    .end local v13    # "e":Ljava/io/IOException;
    .end local v19    # "minFree":J
    .end local v22    # "size":J
    :catch_3
    move-exception v13

    .line 527
    .local v13, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_7
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 528
    const/4 v5, 0x0

    .line 531
    if-eqz v2, :cond_d

    .line 532
    :try_start_8
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 533
    :cond_d
    if-eqz v14, :cond_e

    .line 534
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V

    .line 535
    :cond_e
    if-eqz v7, :cond_f

    .line 536
    invoke-virtual {v7}, Ljava/nio/channels/FileChannel;->close()V

    .line 537
    :cond_f
    if-eqz v16, :cond_4

    .line 538
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto :goto_1

    .line 539
    :catch_4
    move-exception v13

    .line 540
    .local v13, "e":Ljava/io/IOException;
    const/4 v5, 0x0

    goto :goto_1

    .line 530
    .end local v13    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 531
    :goto_4
    if-eqz v2, :cond_10

    .line 532
    :try_start_9
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 533
    :cond_10
    if-eqz v14, :cond_11

    .line 534
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V

    .line 535
    :cond_11
    if-eqz v7, :cond_12

    .line 536
    invoke-virtual {v7}, Ljava/nio/channels/FileChannel;->close()V

    .line 537
    :cond_12
    if-eqz v16, :cond_13

    .line 538
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    .line 540
    :cond_13
    throw v5

    .line 539
    :catch_5
    move-exception v13

    .line 540
    .restart local v13    # "e":Ljava/io/IOException;
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 530
    .end local v13    # "e":Ljava/io/IOException;
    .end local v14    # "fis":Ljava/io/FileInputStream;
    .restart local v15    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v5

    move-object v14, v15

    .end local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .end local v14    # "fis":Ljava/io/FileInputStream;
    .end local v16    # "fos":Ljava/io/FileOutputStream;
    .restart local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "fos":Ljava/io/FileOutputStream;
    .restart local v22    # "size":J
    :catchall_2
    move-exception v5

    move-object/from16 v16, v17

    .end local v17    # "fos":Ljava/io/FileOutputStream;
    .restart local v16    # "fos":Ljava/io/FileOutputStream;
    move-object v14, v15

    .end local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .line 526
    .end local v14    # "fis":Ljava/io/FileInputStream;
    .end local v22    # "size":J
    .restart local v15    # "fis":Ljava/io/FileInputStream;
    :catch_6
    move-exception v13

    move-object v14, v15

    .end local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "fis":Ljava/io/FileInputStream;
    goto :goto_3

    .end local v14    # "fis":Ljava/io/FileInputStream;
    .end local v16    # "fos":Ljava/io/FileOutputStream;
    .restart local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "fos":Ljava/io/FileOutputStream;
    .restart local v22    # "size":J
    :catch_7
    move-exception v13

    move-object/from16 v16, v17

    .end local v17    # "fos":Ljava/io/FileOutputStream;
    .restart local v16    # "fos":Ljava/io/FileOutputStream;
    move-object v14, v15

    .end local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "fis":Ljava/io/FileInputStream;
    goto :goto_3
.end method

.method private getSampleSize(I)I
    .locals 2
    .param p1, "nValue"    # I

    .prologue
    .line 1075
    const/4 v0, 0x1

    .line 1076
    .local v0, "nSize":I
    :goto_0
    const/16 v1, 0x400

    if-le p1, v1, :cond_0

    .line 1077
    div-int/lit8 p1, p1, 0x2

    .line 1078
    mul-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 1080
    :cond_0
    return v0
.end method

.method private onCreateMsgPopup(I)Landroid/app/AlertDialog;
    .locals 4
    .param p1, "nType"    # I

    .prologue
    .line 2423
    iput p1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_nPopupType:I

    .line 2424
    const/4 v0, 0x0

    .line 2425
    .local v0, "nMsgId":I
    sparse-switch p1, :sswitch_data_0

    .line 2437
    :goto_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v2

    invoke-direct {v1, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f070065

    new-instance v3, Lcom/infraware/office/baseframe/EvBaseEditorActivity$10;

    invoke-direct {v3, p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity$10;-><init>(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f07011d

    new-instance v3, Lcom/infraware/office/baseframe/EvBaseEditorActivity$9;

    invoke-direct {v3, p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity$9;-><init>(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f07005f

    new-instance v3, Lcom/infraware/office/baseframe/EvBaseEditorActivity$8;

    invoke-direct {v3, p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity$8;-><init>(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMsgPopup:Landroid/app/AlertDialog;

    .line 2493
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMsgPopup:Landroid/app/AlertDialog;

    const v2, 0x7f0700c9

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 2494
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMsgPopup:Landroid/app/AlertDialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 2495
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMsgPopup:Landroid/app/AlertDialog;

    return-object v1

    .line 2427
    :sswitch_0
    const v0, 0x7f0701e4

    .line 2428
    goto :goto_0

    .line 2430
    :sswitch_1
    const v0, 0x7f0701e5

    .line 2431
    goto :goto_0

    .line 2433
    :sswitch_2
    const v0, 0x7f070188

    goto :goto_0

    .line 2425
    :sswitch_data_0
    .sparse-switch
        0x18 -> :sswitch_0
        0x19 -> :sswitch_1
        0x1f -> :sswitch_2
    .end sparse-switch
.end method

.method private onLocaleChanged()V
    .locals 2

    .prologue
    .line 455
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMsgPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMsgPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 456
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_nPopupType:I

    sparse-switch v0, :sswitch_data_0

    .line 470
    :goto_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMsgPopup:Landroid/app/AlertDialog;

    const v1, 0x7f070065

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 471
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMsgPopup:Landroid/app/AlertDialog;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f07005f

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 472
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMsgPopup:Landroid/app/AlertDialog;

    const/4 v1, -0x3

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f07011d

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 473
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMsgPopup:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f070063

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 475
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_nPopupType:I

    const/16 v1, 0x32

    if-ne v0, v1, :cond_0

    .line 476
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMsgPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 477
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMsgPopup:Landroid/app/AlertDialog;

    .line 478
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_nPopupType:I

    .line 480
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onEmailSendDialog()V

    .line 483
    :cond_0
    return-void

    .line 459
    :sswitch_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMsgPopup:Landroid/app/AlertDialog;

    const v1, 0x7f0701e4

    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 462
    :sswitch_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMsgPopup:Landroid/app/AlertDialog;

    const v1, 0x7f0701e5

    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 465
    :sswitch_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMsgPopup:Landroid/app/AlertDialog;

    const v1, 0x7f070188

    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 456
    nop

    :sswitch_data_0
    .sparse-switch
        0x18 -> :sswitch_0
        0x19 -> :sswitch_1
        0x1f -> :sswitch_2
    .end sparse-switch
.end method

.method private onResultWheelButtonActivity(Landroid/content/Intent;)V
    .locals 5
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 2910
    const-string/jumbo v2, "key_wheel_type"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 2911
    .local v0, "wheel_type":I
    const-string/jumbo v2, "key_value"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 2913
    .local v1, "wheel_value":I
    const-string/jumbo v2, "#####"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onResultWheelButtonActivity: wheel_type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "wheel_value: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2916
    return-void
.end method

.method private onToastMessage(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 2521
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_ToastMsg:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 2522
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_ToastMsg:Landroid/widget/Toast;

    .line 2525
    :goto_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_ToastMsg:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2526
    return-void

    .line 2524
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_ToastMsg:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V

    goto :goto_0
.end method

.method private setTableDrawMode(Z)V
    .locals 1
    .param p1, "set"    # Z

    .prologue
    .line 2950
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0, p1}, Lcom/infraware/office/baseframe/EvBaseView;->setDisableTextInput(Z)V

    .line 2951
    return-void
.end method


# virtual methods
.method public ActivityMsgProc(IIIIILjava/lang/Object;)I
    .locals 8
    .param p1, "msg"    # I
    .param p2, "p1"    # I
    .param p3, "p2"    # I
    .param p4, "p3"    # I
    .param p5, "p4"    # I
    .param p6, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2189
    packed-switch p1, :pswitch_data_0

    .line 2326
    :pswitch_0
    invoke-super/range {p0 .. p6}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    move-result v2

    .line 2328
    .end local p6    # "obj":Ljava/lang/Object;
    :goto_0
    return v2

    .line 2191
    .restart local p6    # "obj":Ljava/lang/Object;
    :pswitch_1
    const v2, 0x7f070318

    invoke-direct {p0, v2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onToastMessage(I)V

    .end local p6    # "obj":Ljava/lang/Object;
    :cond_0
    :goto_1
    :pswitch_2
    move v2, v3

    .line 2328
    goto :goto_0

    .line 2194
    .restart local p6    # "obj":Ljava/lang/Object;
    :pswitch_3
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseView;->requestFocus()Z

    .line 2196
    :pswitch_4
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onEndInsertFreeformShape()V

    .line 2197
    invoke-super/range {p0 .. p6}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto :goto_1

    .line 2209
    :pswitch_5
    invoke-static {p0}, Lcom/infraware/common/define/CMModelDefine$Diotek;->USE_DICTIONARY_SEARCH(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-nez v4, :cond_2

    .line 2211
    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-nez v4, :cond_1

    .line 2212
    new-instance v4, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-direct {v4, p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v4, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    .line 2214
    :cond_1
    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v4, :cond_2

    .line 2215
    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-object v5, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v5}, Lcom/infraware/office/actionbar/MainActionBar;->getHeight()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setActionbarHeight(I)V

    .line 2218
    :cond_2
    iget-boolean v4, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mBShowIMEAfterScreenChanged:Z

    if-ne v4, v3, :cond_0

    .line 2219
    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v4}, Lcom/infraware/office/evengine/EvInterface;->IGetCaretInfo_Editor()Lcom/infraware/office/evengine/EV$CARET_INFO;

    move-result-object v4

    iget v0, v4, Lcom/infraware/office/evengine/EV$CARET_INFO;->bCaret:I

    .line 2220
    .local v0, "nCaretMode":I
    if-eq v0, v3, :cond_3

    if-eq v0, v7, :cond_3

    if-ne v0, v6, :cond_4

    .line 2222
    :cond_3
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ShowIme()V

    .line 2225
    :cond_4
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mBShowIMEAfterScreenChanged:Z

    goto :goto_0

    .line 2232
    .end local v0    # "nCaretMode":I
    :pswitch_6
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->NewDocumentMode()V

    goto :goto_1

    .line 2235
    :pswitch_7
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->TemplateDocumetMode()V

    goto :goto_1

    .line 2248
    :pswitch_8
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v2

    iget-object v2, v2, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v2

    iget-object v2, v2, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2249
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v2

    iget-object v2, v2, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismissAll()V

    .line 2250
    :cond_5
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v2, :cond_0

    .line 2251
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->hidePanel()V

    goto :goto_1

    .line 2255
    :pswitch_9
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2, v6}, Lcom/infraware/office/evengine/EvInterface;->ICellEqualWidthHeight(I)V

    .line 2256
    invoke-super/range {p0 .. p6}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_1

    .line 2259
    :pswitch_a
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2, v3}, Lcom/infraware/office/evengine/EvInterface;->ICellEqualWidthHeight(I)V

    .line 2260
    invoke-super/range {p0 .. p6}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_1

    .line 2263
    :pswitch_b
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v4, 0x4

    invoke-virtual {v2, v7, v4}, Lcom/infraware/office/evengine/EvInterface;->ICaretMark(II)V

    .line 2264
    invoke-super/range {p0 .. p6}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_1

    .line 2267
    :pswitch_c
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2, v7, v3}, Lcom/infraware/office/evengine/EvInterface;->ICaretMark(II)V

    .line 2268
    invoke-super/range {p0 .. p6}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_1

    .line 2271
    :pswitch_d
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2, v7, v6}, Lcom/infraware/office/evengine/EvInterface;->ICaretMark(II)V

    .line 2272
    invoke-super/range {p0 .. p6}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_1

    .line 2275
    :pswitch_e
    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v4, v2, v2, v2}, Lcom/infraware/office/evengine/EvInterface;->ICellMergeSeparate(III)V

    .line 2276
    invoke-super/range {p0 .. p6}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_1

    .line 2279
    :pswitch_f
    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v4, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetFormCopyPaste(I)V

    .line 2282
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700c6

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 2284
    .local v1, "popup":Landroid/widget/Toast;
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 2288
    .end local v1    # "popup":Landroid/widget/Toast;
    :pswitch_10
    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v4, v3}, Lcom/infraware/office/evengine/EvInterface;->ISetFormCopyPaste(I)V

    .line 2290
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f07029b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 2292
    .restart local v1    # "popup":Landroid/widget/Toast;
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 2296
    .end local v1    # "popup":Landroid/widget/Toast;
    :pswitch_11
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2, v3}, Lcom/infraware/office/evengine/EvInterface;->ISetFrameGroup(I)V

    goto/16 :goto_1

    .line 2299
    :pswitch_12
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2, v6}, Lcom/infraware/office/evengine/EvInterface;->ISetFrameGroup(I)V

    goto/16 :goto_1

    .line 2302
    :pswitch_13
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnMaskImage()V

    .line 2303
    invoke-super/range {p0 .. p6}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_1

    .line 2306
    :pswitch_14
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnMultiSelection()V

    .line 2307
    invoke-super/range {p0 .. p6}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_1

    .line 2311
    :pswitch_15
    check-cast p6, Landroid/view/KeyEvent;

    .end local p6    # "obj":Ljava/lang/Object;
    invoke-virtual {p0, p6}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->processShortCut(Landroid/view/KeyEvent;)V

    goto/16 :goto_1

    .line 2314
    .restart local p6    # "obj":Ljava/lang/Object;
    :pswitch_16
    const-string/jumbo v2, "HYOHYUN"

    const-string/jumbo v4, "EVBaseEditorActivity eOnInfraPenDrawing"

    invoke-static {v2, v4}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2317
    :pswitch_17
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbFreedrawActionReady:Z

    .line 2318
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onEndInsertFreeformShape()V

    goto/16 :goto_1

    .line 2321
    :pswitch_18
    const/16 v2, 0x6e

    if-eq p2, v2, :cond_6

    const/16 v2, 0x6f

    if-eq p2, v2, :cond_6

    const/16 v2, 0x70

    if-ne p2, v2, :cond_0

    .line 2322
    :cond_6
    iput-boolean v3, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbFreedrawActionReady:Z

    goto/16 :goto_1

    .line 2189
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_3
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_8
        :pswitch_7
        :pswitch_2
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_f
        :pswitch_10
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_e
        :pswitch_a
        :pswitch_9
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_d
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_11
        :pswitch_12
        :pswitch_0
        :pswitch_13
        :pswitch_0
        :pswitch_14
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_15
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_16
        :pswitch_0
        :pswitch_0
        :pswitch_17
        :pswitch_0
        :pswitch_18
    .end packed-switch
.end method

.method public CheckSaveDocType()I
    .locals 2

    .prologue
    .line 2115
    const/4 v0, 0x0

    .line 2116
    .local v0, "contentType":I
    iget v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mDocExtensionType:I

    sparse-switch v1, :sswitch_data_0

    .line 2145
    :goto_0
    return v0

    .line 2118
    :sswitch_0
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->SAVE_2007()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2119
    const/4 v0, 0x5

    .line 2120
    goto :goto_0

    .line 2123
    :cond_0
    :sswitch_1
    const/4 v0, 0x2

    .line 2124
    goto :goto_0

    .line 2126
    :sswitch_2
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->SAVE_2007()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2127
    const/4 v0, 0x4

    .line 2128
    goto :goto_0

    .line 2131
    :cond_1
    :sswitch_3
    const/4 v0, 0x1

    .line 2132
    goto :goto_0

    .line 2134
    :sswitch_4
    const/16 v0, 0x9

    .line 2135
    goto :goto_0

    .line 2137
    :sswitch_5
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->SAVE_2007()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2138
    const/4 v0, 0x6

    .line 2139
    goto :goto_0

    .line 2142
    :cond_2
    :sswitch_6
    const/4 v0, 0x3

    goto :goto_0

    .line 2116
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_3
        0x3 -> :sswitch_4
        0x5 -> :sswitch_6
        0x12 -> :sswitch_2
        0x13 -> :sswitch_0
        0x14 -> :sswitch_5
    .end sparse-switch
.end method

.method public DefalutBWPChartInsert(IIII)V
    .locals 18
    .param p1, "nChartType"    # I
    .param p2, "nDimention"    # I
    .param p3, "nBarType"    # I
    .param p4, "nStyleID"    # I

    .prologue
    .line 1906
    const-string/jumbo v1, "EvBaseEditorActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "nStyleID = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1930
    const/16 v1, 0xc

    new-array v4, v1, [Ljava/lang/String;

    .line 1931
    .local v4, "serialDataArray":[Ljava/lang/String;
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_0
    const/16 v1, 0xc

    move/from16 v0, v17

    if-ge v0, v1, :cond_0

    .line 1932
    add-int/lit8 v1, v17, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v17

    .line 1931
    add-int/lit8 v17, v17, 0x1

    goto :goto_0

    .line 1934
    :cond_0
    const/4 v1, 0x3

    new-array v5, v1, [Ljava/lang/String;

    .line 1935
    .local v5, "serialNameArray":[Ljava/lang/String;
    const/4 v1, 0x0

    const-string/jumbo v2, "Col1"

    aput-object v2, v5, v1

    .line 1936
    const/4 v1, 0x1

    const-string/jumbo v2, "Col2"

    aput-object v2, v5, v1

    .line 1937
    const/4 v1, 0x2

    const-string/jumbo v2, "Col3"

    aput-object v2, v5, v1

    .line 1939
    const/4 v1, 0x4

    new-array v6, v1, [Ljava/lang/String;

    .line 1940
    .local v6, "itemNameArray":[Ljava/lang/String;
    const/4 v1, 0x0

    const-string/jumbo v2, "Item1"

    aput-object v2, v6, v1

    .line 1941
    const/4 v1, 0x1

    const-string/jumbo v2, "Item2"

    aput-object v2, v6, v1

    .line 1942
    const/4 v1, 0x2

    const-string/jumbo v2, "Item3"

    aput-object v2, v6, v1

    .line 1943
    const/4 v1, 0x3

    const-string/jumbo v2, "Item4"

    aput-object v2, v6, v1

    .line 1945
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x3

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x3

    move/from16 v3, p1

    move/from16 v14, p2

    move/from16 v15, p3

    move/from16 v16, p4

    invoke-virtual/range {v1 .. v16}, Lcom/infraware/office/evengine/EvInterface;->ISetBwpChart(II[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)V

    .line 1950
    return-void
.end method

.method public GetMsgPopup()Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 2529
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMsgPopup:Landroid/app/AlertDialog;

    return-object v0
.end method

.method public IsDifType()Z
    .locals 1

    .prologue
    .line 2533
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mIsDifType:Z

    return v0
.end method

.method protected NewDocumentMode()V
    .locals 0

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->setDeviceResolution()V

    .line 195
    return-void
.end method

.method public OnActionBarEvent(I)V
    .locals 9
    .param p1, "eEventType"    # I

    .prologue
    const-wide/16 v4, 0x12c

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 2701
    sparse-switch p1, :sswitch_data_0

    .line 2823
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    .line 2826
    :cond_0
    :goto_0
    return-void

    .line 2703
    :sswitch_0
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnExportToPdf()V

    goto :goto_0

    .line 2706
    :sswitch_1
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    goto :goto_0

    .line 2709
    :sswitch_2
    invoke-virtual {p0, v7}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onStartTableDraw(Z)V

    goto :goto_0

    .line 2712
    :sswitch_3
    invoke-virtual {p0, v7}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onStartTableErease(Z)V

    goto :goto_0

    .line 2715
    :sswitch_4
    invoke-virtual {p0, v7}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onEndTableDraw(Z)V

    goto :goto_0

    .line 2718
    :sswitch_5
    invoke-virtual {p0, v7}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onStartLASSO(Z)V

    goto :goto_0

    .line 2722
    :sswitch_6
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v3}, Lcom/infraware/office/evengine/EvInterface;->IInsertTextBox(Z)V

    .line 2723
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/infraware/office/baseframe/EvBaseEditorActivity$11;

    invoke-direct {v1, p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity$11;-><init>(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2730
    :sswitch_7
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v7}, Lcom/infraware/office/evengine/EvInterface;->IInsertTextBox(Z)V

    .line 2731
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/infraware/office/baseframe/EvBaseEditorActivity$12;

    invoke-direct {v1, p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity$12;-><init>(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 2739
    :sswitch_8
    const/16 v1, 0x29

    const/16 v2, 0x45

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object v0, p0

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    .line 2741
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v3, v7}, Lcom/infraware/office/evengine/EvInterface;->ISetCroppingMode(II)V

    .line 2742
    iput-boolean v3, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbCroppingMode:Z

    .line 2743
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->hide()V

    .line 2744
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v7, :cond_1

    .line 2745
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v0, :cond_0

    .line 2746
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->show()V

    goto :goto_0

    .line 2748
    :cond_1
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mDocType:I

    if-ne v0, v8, :cond_2

    .line 2749
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v0, :cond_0

    .line 2750
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->show()V

    goto :goto_0

    .line 2752
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v0, :cond_0

    .line 2753
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->hide()V

    goto/16 :goto_0

    .line 2774
    :sswitch_9
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IsLassoViewMode_Editor()Z

    move-result v0

    if-ne v0, v7, :cond_3

    .line 2775
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v3, v7}, Lcom/infraware/office/evengine/EvInterface;->ISetPenMode(IZ)V

    .line 2777
    :cond_3
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->InsertShapeDrawing()V

    .line 2778
    const v0, 0x7f07031c

    invoke-direct {p0, v0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onToastMessage(I)V

    goto/16 :goto_0

    .line 2781
    :sswitch_a
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->hide()V

    .line 2782
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v7, :cond_5

    .line 2783
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v0, :cond_4

    .line 2784
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->show()V

    .line 2795
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v3}, Lcom/infraware/office/evengine/EvInterface;->ISetMultiSelect(I)V

    .line 2796
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    check-cast v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;

    iput-boolean v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsMultiSelectionMode:Z

    goto/16 :goto_0

    .line 2786
    :cond_5
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mDocType:I

    if-ne v0, v8, :cond_6

    .line 2787
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v0, :cond_4

    .line 2788
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->show()V

    goto :goto_1

    .line 2790
    :cond_6
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v0, :cond_4

    .line 2791
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->show()V

    goto :goto_1

    .line 2810
    :sswitch_b
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->GetObjCtrlType()I

    move-result v0

    const/16 v1, 0x1f

    if-ne v0, v1, :cond_7

    .line 2812
    const/16 v2, 0x26

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object v1, p0

    move v4, v3

    move v5, v3

    move v6, v3

    invoke-virtual/range {v1 .. v7}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_0

    .line 2814
    :cond_7
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->GetObjCtrlType()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 2816
    const/16 v2, 0x27

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object v1, p0

    move v4, v3

    move v5, v3

    move v6, v3

    invoke-virtual/range {v1 .. v7}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_0

    .line 2701
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x19 -> :sswitch_6
        0x45 -> :sswitch_8
        0x49 -> :sswitch_9
        0x51 -> :sswitch_1
        0x53 -> :sswitch_a
        0x69 -> :sswitch_2
        0x70 -> :sswitch_4
        0x71 -> :sswitch_3
        0x74 -> :sswitch_6
        0x75 -> :sswitch_7
        0x79 -> :sswitch_5
        0x91 -> :sswitch_b
    .end sparse-switch
.end method

.method public OnBookMarkEditorMode()V
    .locals 0

    .prologue
    .line 338
    return-void
.end method

.method public OnColorPickerActivity(II)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "color"    # I

    .prologue
    .line 674
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 676
    .local v0, "colorPicker":Landroid/content/Intent;
    const-string/jumbo v1, "color_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 677
    const-string/jumbo v1, "color_value"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 679
    const/16 v1, 0x15

    invoke-virtual {p0, v0, v1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 681
    return-void
.end method

.method public OnCoreNotify(I)V
    .locals 2
    .param p1, "nNotifyCode"    # I

    .prologue
    const/4 v1, 0x0

    .line 387
    packed-switch p1, :pswitch_data_0

    .line 406
    :goto_0
    :pswitch_0
    return-void

    .line 389
    :pswitch_1
    const v0, 0x7f07029a

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 394
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->setPassWordStyle()V

    goto :goto_0

    .line 399
    :pswitch_3
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    check-cast v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;

    iput-boolean v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsMultiSelectionMode:Z

    goto :goto_0

    .line 403
    :pswitch_4
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    check-cast v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsMultiSelectionMode:Z

    goto :goto_0

    .line 387
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public OnCoreNotify2(II)V
    .locals 0
    .param p1, "nNotifyCode2"    # I
    .param p2, "nData"    # I

    .prologue
    .line 411
    return-void
.end method

.method public OnDeleteCells()V
    .locals 4

    .prologue
    .line 768
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v2

    invoke-direct {v1, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 769
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f070110

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 770
    const v2, 0x7f040009

    new-instance v3, Lcom/infraware/office/baseframe/EvBaseEditorActivity$3;

    invoke-direct {v3, p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity$3;-><init>(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 785
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 786
    .local v0, "alertdialog":Landroid/app/AlertDialog;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 787
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 788
    return-void
.end method

.method public OnDrawBitmap(II)V
    .locals 0
    .param p1, "nCallId"    # I
    .param p2, "bShowAutomap"    # I

    .prologue
    .line 2972
    invoke-super {p0, p1, p2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnDrawBitmap(II)V

    .line 2973
    return-void
.end method

.method public OnEditOrViewMode(II)V
    .locals 2
    .param p1, "bEditMode"    # I
    .param p2, "mode_type"    # I

    .prologue
    .line 342
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getDocExtensionType()I

    move-result v1

    invoke-virtual {v0, p0, p0, v1, p2}, Lcom/infraware/office/baseframe/EvBaseView;->setModeType(Landroid/app/Activity;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;II)V

    .line 343
    return-void
.end method

.method public OnExportToPdf()V
    .locals 7

    .prologue
    .line 568
    iget-object v5, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v5}, Lcom/infraware/office/evengine/EvInterface;->IsWebMode()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 570
    const v5, 0x7f070315

    invoke-direct {p0, v5}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onToastMessage(I)V

    .line 586
    :goto_0
    return-void

    .line 574
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->CheckSaveDocType()I

    move-result v1

    .line 575
    .local v1, "contentType":I
    add-int/lit8 v3, v1, 0x0

    .line 576
    .local v3, "mode":I
    new-instance v2, Landroid/content/Intent;

    const-class v5, Lcom/infraware/polarisoffice5/common/ExportToPdf;

    invoke-direct {v2, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 577
    .local v2, "exportToPdf":Landroid/content/Intent;
    new-instance v4, Ljava/lang/String;

    iget-object v5, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 578
    .local v4, "savePath":Ljava/lang/String;
    iget-object v5, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-static {p0, v5}, Lcom/infraware/common/util/FileUtils;->isSavableDirectory(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-static {}, Lcom/infraware/common/util/FileUtils;->isSdcard()Z

    move-result v5

    if-nez v5, :cond_1

    .line 579
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    .line 580
    .local v0, "LOCAL_ROOT_PATH":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-static {v6}, Lcom/infraware/common/util/FileUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 582
    .end local v0    # "LOCAL_ROOT_PATH":Ljava/lang/String;
    :cond_1
    const-string/jumbo v5, "key_filename"

    iget v6, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mDocExtensionType:I

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 583
    const-string/jumbo v5, "key_current_file"

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 584
    const-string/jumbo v5, "key_content_mode"

    invoke-virtual {v2, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 585
    const/16 v5, 0x1d

    invoke-virtual {p0, v2, v5}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public OnFlick(I)V
    .locals 0
    .param p1, "nRetData"    # I

    .prologue
    .line 2967
    return-void
.end method

.method public OnHidAction(I)V
    .locals 0
    .param p1, "eev_hid_action"    # I

    .prologue
    .line 370
    return-void
.end method

.method public OnIMEInsertMode()V
    .locals 0

    .prologue
    .line 378
    return-void
.end method

.method public OnInsertChart()V
    .locals 0

    .prologue
    .line 2559
    return-void
.end method

.method public OnInsertFreeformShapes()V
    .locals 2

    .prologue
    .line 2960
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->getActionMode()I

    move-result v0

    const/16 v1, 0xf

    if-ne v0, v1, :cond_0

    .line 2961
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->setActionMode(I)V

    .line 2963
    :cond_0
    return-void
.end method

.method public OnInsertSymbol()V
    .locals 0

    .prologue
    .line 2548
    return-void
.end method

.method public OnInsertTableActivity()V
    .locals 0

    .prologue
    .line 671
    return-void
.end method

.method public OnInsertTableMode()V
    .locals 0

    .prologue
    .line 374
    return-void
.end method

.method public OnMergeCells()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 807
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v1, v1, v1}, Lcom/infraware/office/evengine/EvInterface;->ICellMergeSeparate(III)V

    .line 808
    return-void
.end method

.method public OnNewDoc(I)V
    .locals 1
    .param p1, "bOk"    # I

    .prologue
    .line 347
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnLoadComplete(I)V

    .line 348
    return-void
.end method

.method public OnObjectPoints(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V
    .locals 4
    .param p1, "param"    # Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 352
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/infraware/office/baseframe/EvBaseView;->OnObjectPoints(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V

    .line 353
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbCroppingMode:Z

    if-ne v0, v3, :cond_1

    .line 355
    const/4 v0, 0x2

    iget v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mDocType:I

    if-eq v0, v1, :cond_1

    .line 356
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->isShow()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->GetObjCtrlType()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    .line 357
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->hide()V

    .line 358
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbCroppingMode:Z

    .line 359
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v3, :cond_0

    .line 360
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->show()V

    .line 362
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v2, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetCroppingMode(II)V

    .line 366
    :cond_1
    return-void
.end method

.method protected OnPassWordActivity(Z)V
    .locals 0
    .param p1, "bReFail"    # Z

    .prologue
    .line 2889
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnPassWordActivity(Z)V

    .line 2890
    return-void
.end method

.method public OnPrintActivity()V
    .locals 6

    .prologue
    .line 589
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 591
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 593
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v4

    invoke-direct {v3, p0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v4, 0x7f0700a8

    invoke-virtual {p0, v4}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0701c1

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f070063

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 598
    .local v1, "m_oMsgDialog":Landroid/app/AlertDialog;
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 599
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 607
    .end local v1    # "m_oMsgDialog":Landroid/app/AlertDialog;
    :goto_0
    return-void

    .line 603
    :cond_0
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/infraware/polarisoffice5/print/PrintActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 604
    .local v2, "printIntent":Landroid/content/Intent;
    const-string/jumbo v3, "key_filename"

    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 606
    const/16 v3, 0xf

    invoke-virtual {p0, v2, v3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public OnResizeImage(Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 23
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "deleteFile"    # Ljava/lang/Boolean;

    .prologue
    .line 1084
    new-instance v16, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1085
    .local v16, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v2, 0x1

    move-object/from16 v0, v16

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1086
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 1088
    const/16 v21, 0x0

    .line 1089
    .local v21, "width":I
    const/4 v12, 0x0

    .line 1091
    .local v12, "height":I
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_EXIF_INFO()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1092
    invoke-static/range {p1 .. p1}, Lcom/infraware/common/util/Utils;->getExifOrientation(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mImageOrientation:I

    .line 1094
    :cond_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mImageOrientation:I

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mImageOrientation:I

    const/16 v3, 0xb4

    if-ne v2, v3, :cond_3

    .line 1095
    :cond_1
    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move/from16 v21, v0

    .line 1096
    move-object/from16 v0, v16

    iget v12, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 1102
    :goto_0
    move-object/from16 v0, v16

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    const/4 v3, 0x1

    if-lt v2, v3, :cond_2

    move-object/from16 v0, v16

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    const/4 v3, 0x1

    if-ge v2, v3, :cond_4

    .line 1271
    :cond_2
    :goto_1
    return-void

    .line 1098
    :cond_3
    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    move/from16 v21, v0

    .line 1099
    move-object/from16 v0, v16

    iget v12, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    goto :goto_0

    .line 1105
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mImageSize:Landroid/graphics/Point;

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ConvertPxToMm(I)I

    move-result v3

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ConvertPxToMm(I)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Point;->set(II)V

    .line 1107
    new-instance v19, Landroid/graphics/Point;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mImageSize:Landroid/graphics/Point;

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Landroid/graphics/Point;-><init>(Landroid/graphics/Point;)V

    .line 1109
    .local v19, "resizeImageSize":Landroid/graphics/Point;
    move-object/from16 v0, v16

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    const/16 v3, 0x400

    if-gt v2, v3, :cond_5

    move-object/from16 v0, v16

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    const/16 v3, 0x400

    if-le v2, v3, :cond_6

    .line 1110
    :cond_5
    move-object/from16 v0, v19

    iget v2, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/Point;->y:I

    if-lt v2, v3, :cond_8

    .line 1111
    const/16 v2, 0x400

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ConvertPxToMm(I)I

    move-result v2

    move-object/from16 v0, v16

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    mul-int/lit16 v3, v3, 0x400

    move-object/from16 v0, v16

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    div-int/2addr v3, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ConvertPxToMm(I)I

    move-result v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Point;->set(II)V

    .line 1116
    :cond_6
    :goto_2
    const-string/jumbo v22, "640"

    .line 1117
    .local v22, "widthString":Ljava/lang/String;
    const-string/jumbo v13, "640"

    .line 1118
    .local v13, "heightString":Ljava/lang/String;
    new-instance v11, Landroid/graphics/Point;

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ConvertMmToPx(I)I

    move-result v2

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ConvertMmToPx(I)I

    move-result v3

    invoke-direct {v11, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    .line 1120
    .local v11, "frameSize":Landroid/graphics/Point;
    new-instance v14, Landroid/graphics/Point;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mImageSize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ConvertMmToPx(I)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mImageSize:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ConvertMmToPx(I)I

    move-result v3

    invoke-direct {v14, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    .line 1121
    .local v14, "imageSize":Landroid/graphics/Point;
    const/4 v8, 0x0

    .line 1123
    .local v8, "bitmap":Landroid/graphics/Bitmap;
    new-instance v16, Landroid/graphics/BitmapFactory$Options;

    .end local v16    # "options":Landroid/graphics/BitmapFactory$Options;
    invoke-direct/range {v16 .. v16}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1124
    .restart local v16    # "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v2, 0x1

    move-object/from16 v0, v16

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 1125
    const/4 v2, 0x1

    move-object/from16 v0, v16

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 1129
    iget v2, v14, Landroid/graphics/Point;->x:I

    const/16 v3, 0x400

    if-gt v2, v3, :cond_9

    iget v2, v14, Landroid/graphics/Point;->y:I

    const/16 v3, 0x400

    if-gt v2, v3, :cond_9

    .line 1130
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 1132
    if-eqz v8, :cond_7

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    if-eq v2, v3, :cond_7

    .line 1134
    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    const/4 v3, 0x1

    invoke-virtual {v8, v2, v3}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v18

    .line 1135
    .local v18, "resizeBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    .line 1136
    move-object/from16 v8, v18

    .line 1228
    .end local v18    # "resizeBitmap":Landroid/graphics/Bitmap;
    :cond_7
    :goto_3
    if-nez v8, :cond_c

    .line 1229
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700a7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 1113
    .end local v8    # "bitmap":Landroid/graphics/Bitmap;
    .end local v11    # "frameSize":Landroid/graphics/Point;
    .end local v13    # "heightString":Ljava/lang/String;
    .end local v14    # "imageSize":Landroid/graphics/Point;
    .end local v22    # "widthString":Ljava/lang/String;
    :cond_8
    move-object/from16 v0, v16

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    mul-int/lit16 v2, v2, 0x400

    move-object/from16 v0, v16

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    div-int/2addr v2, v3

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ConvertPxToMm(I)I

    move-result v2

    const/16 v3, 0x400

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ConvertPxToMm(I)I

    move-result v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_2

    .line 1139
    .restart local v8    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v11    # "frameSize":Landroid/graphics/Point;
    .restart local v13    # "heightString":Ljava/lang/String;
    .restart local v14    # "imageSize":Landroid/graphics/Point;
    .restart local v22    # "widthString":Ljava/lang/String;
    :cond_9
    const/16 v20, 0x1

    .line 1141
    .local v20, "size":I
    const/4 v2, 0x0

    move-object/from16 v0, v16

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1143
    new-instance v17, Landroid/graphics/Point;

    move-object/from16 v0, v17

    invoke-direct {v0, v14}, Landroid/graphics/Point;-><init>(Landroid/graphics/Point;)V

    .line 1150
    .local v17, "orgImageSize":Landroid/graphics/Point;
    move-object/from16 v0, v17

    iget v2, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, v17

    iget v3, v0, Landroid/graphics/Point;->y:I

    if-lt v2, v3, :cond_b

    .line 1151
    const-string/jumbo v2, "camera"

    const-string/jumbo v3, "x: %d > y: %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, v17

    iget v6, v0, Landroid/graphics/Point;->x:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    move-object/from16 v0, v17

    iget v6, v0, Landroid/graphics/Point;->y:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1159
    move-object/from16 v0, v17

    iget v2, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getSampleSize(I)I

    move-result v20

    .line 1162
    const/16 v2, 0x400

    move-object/from16 v0, v17

    iget v3, v0, Landroid/graphics/Point;->y:I

    mul-int/lit16 v3, v3, 0x400

    move-object/from16 v0, v17

    iget v4, v0, Landroid/graphics/Point;->x:I

    div-int/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Point;->set(II)V

    .line 1175
    :goto_4
    move/from16 v0, v20

    move-object/from16 v1, v16

    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1176
    const-string/jumbo v2, "size"

    const-string/jumbo v3, "%d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1178
    :try_start_0
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 1185
    :goto_5
    if-eqz v8, :cond_7

    .line 1186
    const/4 v15, 0x0

    .line 1189
    .local v15, "newBitmap":Landroid/graphics/Bitmap;
    :try_start_1
    move-object/from16 v0, v17

    iget v2, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, v17

    iget v3, v0, Landroid/graphics/Point;->y:I

    const/4 v4, 0x1

    invoke-static {v8, v2, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v15

    .line 1196
    :goto_6
    invoke-virtual {v8}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v15}, Ljava/lang/Object;->hashCode()I

    move-result v3

    if-eq v2, v3, :cond_a

    .line 1200
    :try_start_2
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 1208
    :cond_a
    :goto_7
    move-object v8, v15

    .line 1209
    if-eqz v8, :cond_7

    .line 1211
    const/16 v18, 0x0

    .line 1214
    .restart local v18    # "resizeBitmap":Landroid/graphics/Bitmap;
    :try_start_3
    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    const/4 v3, 0x1

    invoke-virtual {v8, v2, v3}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v18

    .line 1215
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_3

    .line 1216
    move-object/from16 v8, v18

    goto/16 :goto_3

    .line 1164
    .end local v15    # "newBitmap":Landroid/graphics/Bitmap;
    .end local v18    # "resizeBitmap":Landroid/graphics/Bitmap;
    :cond_b
    const-string/jumbo v2, "camera"

    const-string/jumbo v3, "x: %d < y: %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, v17

    iget v6, v0, Landroid/graphics/Point;->x:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    move-object/from16 v0, v17

    iget v6, v0, Landroid/graphics/Point;->y:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1171
    move-object/from16 v0, v17

    iget v2, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getSampleSize(I)I

    move-result v20

    .line 1173
    move-object/from16 v0, v17

    iget v2, v0, Landroid/graphics/Point;->x:I

    mul-int/lit16 v2, v2, 0x400

    move-object/from16 v0, v17

    iget v3, v0, Landroid/graphics/Point;->y:I

    div-int/2addr v2, v3

    const/16 v3, 0x400

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_4

    .line 1180
    :catch_0
    move-exception v9

    .line 1182
    .local v9, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700a7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_5

    .line 1191
    .end local v9    # "e":Ljava/lang/OutOfMemoryError;
    .restart local v15    # "newBitmap":Landroid/graphics/Bitmap;
    :catch_1
    move-exception v9

    .line 1193
    .restart local v9    # "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700a7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_6

    .line 1202
    .end local v9    # "e":Ljava/lang/OutOfMemoryError;
    :catch_2
    move-exception v9

    .line 1204
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    goto/16 :goto_7

    .line 1217
    .end local v9    # "e":Ljava/lang/Exception;
    .restart local v18    # "resizeBitmap":Landroid/graphics/Bitmap;
    :catch_3
    move-exception v9

    .line 1218
    .local v9, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    .line 1219
    const/4 v8, 0x0

    goto/16 :goto_3

    .line 1232
    .end local v9    # "e":Ljava/lang/OutOfMemoryError;
    .end local v15    # "newBitmap":Landroid/graphics/Bitmap;
    .end local v17    # "orgImageSize":Landroid/graphics/Point;
    .end local v18    # "resizeBitmap":Landroid/graphics/Bitmap;
    .end local v20    # "size":I
    :cond_c
    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mImageOrientation:I

    invoke-static {v8, v2}, Lcom/infraware/common/util/Utils;->getRotatedBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mBitmap:Landroid/graphics/Bitmap;

    .line 1233
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mIsReplace:Z

    if-nez v2, :cond_d

    .line 1236
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mFilename:Ljava/lang/String;

    .line 1243
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 1244
    new-instance v10, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1245
    .local v10, "file":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    goto/16 :goto_1

    .line 1250
    .end local v10    # "file":Ljava/io/File;
    :cond_d
    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mDocType:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_e

    .line 1251
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mBitmap:Landroid/graphics/Bitmap;

    iget v5, v14, Landroid/graphics/Point;->x:I

    iget v6, v14, Landroid/graphics/Point;->y:I

    const/4 v7, 0x1

    invoke-virtual/range {v2 .. v7}, Lcom/infraware/office/evengine/EvInterface;->IImageInsert(Ljava/lang/String;Landroid/graphics/Bitmap;IIZ)V

    .line 1256
    :goto_8
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    .line 1258
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 1259
    new-instance v10, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1260
    .restart local v10    # "file":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    goto/16 :goto_1

    .line 1253
    .end local v10    # "file":Ljava/io/File;
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mBitmap:Landroid/graphics/Bitmap;

    iget v5, v11, Landroid/graphics/Point;->x:I

    iget v6, v11, Landroid/graphics/Point;->y:I

    const/4 v7, 0x1

    invoke-virtual/range {v2 .. v7}, Lcom/infraware/office/evengine/EvInterface;->IImageInsert(Ljava/lang/String;Landroid/graphics/Bitmap;IIZ)V

    goto :goto_8
.end method

.method public OnResultCameraActivity(Landroid/content/Intent;)V
    .locals 12
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    const v11, 0x7f0700a7

    const/16 v5, 0x400

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1338
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mCameraImageUri:Landroid/net/Uri;

    if-eqz v0, :cond_1

    .line 1339
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mCameraImageUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v6

    .line 1342
    .local v6, "filename":Ljava/lang/String;
    invoke-static {v6}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->GetExifOrientation(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mSpinedImageDegree:I

    .line 1344
    if-eqz v6, :cond_1

    .line 1345
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v6, v0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnResizeImage(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1379
    .end local v6    # "filename":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 1350
    :cond_1
    if-eqz p1, :cond_0

    .line 1351
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/Bitmap;

    .line 1352
    .local v9, "orgBitmap":Landroid/graphics/Bitmap;
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v9, v0, v4}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1353
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    if-nez v2, :cond_2

    .line 1354
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1357
    :cond_2
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-gt v0, v5, :cond_3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-le v0, v5, :cond_5

    .line 1358
    :cond_3
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    .line 1359
    .local v10, "width":I
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 1360
    .local v7, "height":I
    if-lt v10, v7, :cond_4

    .line 1361
    const/16 v10, 0x400

    .line 1362
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    mul-int/lit16 v0, v0, 0x400

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int v7, v0, v1

    .line 1368
    :goto_1
    invoke-static {v2, v10, v7, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 1369
    .local v8, "newBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 1370
    move-object v2, v8

    .line 1371
    if-nez v2, :cond_5

    .line 1372
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1364
    .end local v8    # "newBitmap":Landroid/graphics/Bitmap;
    :cond_4
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    mul-int/lit16 v0, v0, 0x400

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    div-int v10, v0, v1

    .line 1365
    const/16 v7, 0x400

    goto :goto_1

    .line 1376
    .end local v7    # "height":I
    .end local v10    # "width":I
    :cond_5
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x0

    iget-boolean v5, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mIsReplace:Z

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvInterface;->IImageInsert(Ljava/lang/String;Landroid/graphics/Bitmap;IIZ)V

    .line 1377
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_0
.end method

.method public OnResultChartDataActivity(Landroid/content/Intent;)V
    .locals 17
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 1867
    const-string/jumbo v1, "Serial Name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 1868
    .local v5, "SerialName":[Ljava/lang/String;
    const-string/jumbo v1, "Item Name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 1869
    .local v6, "ItemName":[Ljava/lang/String;
    const-string/jumbo v1, "Serial Data"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1884
    .local v4, "SerialData":[Ljava/lang/String;
    const/16 v2, 0x380

    .line 1887
    .local v2, "nMask":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v3, 0x0

    array-length v7, v6

    array-length v8, v5

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v1 .. v16}, Lcom/infraware/office/evengine/EvInterface;->ISetBwpChart(II[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)V

    .line 1889
    return-void
.end method

.method public OnResultColorPickerActivity(Landroid/content/Intent;)V
    .locals 4
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 1953
    const-string/jumbo v2, "color_type"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 1954
    .local v0, "color_type":I
    const-string/jumbo v2, "color_value"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1975
    .local v1, "color_value":I
    iget v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mFreeDrawMode:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 1977
    iget-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbPenDraw:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 1978
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    invoke-virtual {v2, v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->setOtherColor(I)V

    .line 1984
    :cond_0
    :goto_0
    return-void

    .line 1980
    :cond_1
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mFreeDrawOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v2, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->setOtherColor(I)V

    goto :goto_0
.end method

.method public OnResultCommonInputFieldActivity(Landroid/content/Intent;)V
    .locals 5
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 1850
    const-string/jumbo v3, "key_filed_type"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 1851
    .local v2, "nFieldType":I
    const-string/jumbo v3, "key_filed_set_value"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->setFieldValue:Ljava/lang/String;

    .line 1853
    iget v3, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mDocExtensionType:I

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    iget v3, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mDocExtensionType:I

    const/16 v4, 0x14

    if-ne v3, v4, :cond_1

    .line 1854
    :cond_0
    const/4 v1, 0x0

    .line 1855
    .local v1, "m_sChartInfo":Lcom/infraware/office/evengine/EV$GUI_SHEET_ALL_CHART_EVENT;
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->IGetAllChartInfo()Lcom/infraware/office/evengine/EV$GUI_SHEET_ALL_CHART_EVENT;

    move-result-object v1

    .line 1856
    const/4 v0, 0x0

    .line 1857
    .local v0, "m_sCellInfo":Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->IGetCellInfo()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    move-result-object v0

    .line 1864
    .end local v0    # "m_sCellInfo":Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;
    .end local v1    # "m_sChartInfo":Lcom/infraware/office/evengine/EV$GUI_SHEET_ALL_CHART_EVENT;
    :cond_1
    return-void
.end method

.method public OnResultInsertChartActivitry(Landroid/content/Intent;)V
    .locals 7
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x0

    .line 1893
    const-string/jumbo v5, "PPT_CHART"

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 1895
    .local v0, "bPPT":Z
    if-eqz v0, :cond_0

    .line 1896
    const-string/jumbo v5, "ChartType"

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 1897
    .local v2, "nChartType":I
    const-string/jumbo v5, "nDimention"

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 1898
    .local v3, "nDimention":I
    const-string/jumbo v5, "barType"

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1899
    .local v1, "nBarType":I
    const-string/jumbo v5, "StyleID"

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 1901
    .local v4, "nStyleID":I
    invoke-virtual {p0, v2, v3, v1, v4}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->DefalutBWPChartInsert(IIII)V

    .line 1903
    .end local v1    # "nBarType":I
    .end local v2    # "nChartType":I
    .end local v3    # "nDimention":I
    .end local v4    # "nStyleID":I
    :cond_0
    return-void
.end method

.method public OnResultInsertHyperlinkActivity(Landroid/content/Intent;)V
    .locals 5
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 1831
    const-string/jumbo v3, "hyper_url"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1832
    .local v2, "hyper_url":Ljava/lang/String;
    const-string/jumbo v3, "hyper_text"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1835
    .local v1, "hyper_text":Ljava/lang/String;
    iget v3, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mDocType:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 1836
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->IGetSheetHyperLinkInfo()Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    move-result-object v0

    .line 1837
    .local v0, "a_hyperlinkInfo":Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;
    const/4 v3, 0x3

    iput v3, v0, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;->nType:I

    .line 1838
    iput-object v2, v0, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    .line 1839
    iput-object v1, v0, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;->szHyperText:Ljava/lang/String;

    .line 1840
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3, v0}, Lcom/infraware/office/evengine/EvInterface;->ISetSheetHyperLinkInfo(Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;)V

    .line 1847
    .end local v0    # "a_hyperlinkInfo":Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;
    :goto_0
    return-void

    .line 1842
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 1843
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3, v1, v2}, Lcom/infraware/office/evengine/EvInterface;->IHyperlinkEditor(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1845
    :cond_1
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v2}, Lcom/infraware/office/evengine/EvInterface;->IHyperlinkEditor(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public OnResultInsertInsertTableActivity(Landroid/content/Intent;)V
    .locals 5
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 1820
    const-string/jumbo v3, "id_inserttable_row"

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1821
    .local v1, "nRow":I
    const-string/jumbo v3, "id_inserttable_col"

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 1822
    .local v0, "nCol":I
    const-string/jumbo v3, "id_inserttable_style"

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 1824
    .local v2, "nStlyeNum":I
    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 1828
    :cond_0
    :goto_0
    return-void

    .line 1827
    :cond_1
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/high16 v4, -0x1000000

    invoke-virtual {v3, v1, v0, v4, v2}, Lcom/infraware/office/evengine/EvInterface;->ICreateTable(IIII)V

    goto :goto_0
.end method

.method public OnResultVideoActivity(Landroid/content/Intent;)V
    .locals 22
    .param p1, "oData"    # Landroid/content/Intent;

    .prologue
    .line 1383
    if-nez p1, :cond_1

    .line 1517
    :cond_0
    :goto_0
    return-void

    .line 1386
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 1388
    .local v3, "oUri":Landroid/net/Uri;
    if-eqz v3, :cond_0

    .line 1391
    const/4 v10, 0x0

    .line 1392
    .local v10, "szFilePath":Ljava/lang/String;
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v21

    .line 1393
    .local v21, "strUri":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    .line 1394
    .local v14, "contentResolver":Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 1395
    .local v6, "bitmap":Landroid/graphics/Bitmap;
    const-string/jumbo v2, "content://"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1396
    const/16 v19, 0x0

    .line 1397
    .local v19, "oCursor":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v7, "_id"

    aput-object v7, v4, v5

    const/4 v5, 0x1

    const-string/jumbo v7, "_data"

    aput-object v7, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .end local v6    # "bitmap":Landroid/graphics/Bitmap;
    move-result-object v19

    .line 1402
    if-eqz v19, :cond_0

    .line 1405
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1406
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_2

    .line 1408
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1412
    :cond_2
    const-string/jumbo v2, "_id"

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 1413
    .local v16, "nId":J
    const-string/jumbo v2, "_data"

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1414
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 1416
    const/4 v2, 0x1

    const/4 v4, 0x0

    move-wide/from16 v0, v16

    invoke-static {v14, v0, v1, v2, v4}, Landroid/provider/MediaStore$Video$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 1429
    .end local v16    # "nId":J
    .end local v19    # "oCursor":Landroid/database/Cursor;
    .restart local v6    # "bitmap":Landroid/graphics/Bitmap;
    :cond_3
    :goto_1
    if-nez v10, :cond_4

    .line 1430
    new-instance v10, Ljava/lang/String;

    .end local v10    # "szFilePath":Ljava/lang/String;
    invoke-direct {v10}, Ljava/lang/String;-><init>()V

    .line 1433
    .restart local v10    # "szFilePath":Ljava/lang/String;
    :cond_4
    const/16 v2, 0x2f

    invoke-virtual {v10, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v18

    .line 1434
    .local v18, "nIdx":I
    const/4 v2, 0x0

    move/from16 v0, v18

    invoke-virtual {v10, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 1435
    .local v12, "VideoDirectoryPath":Ljava/lang/String;
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->MediaDirectory:Ljava/lang/String;

    .line 1436
    add-int/lit8 v2, v18, 0x1

    invoke-virtual {v10, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    .line 1437
    .local v13, "VideoFileName":Ljava/lang/String;
    const-string/jumbo v2, "EvBaseEditorActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "VideoPath "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1438
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mIsReplace:Z

    const/4 v11, 0x0

    invoke-virtual/range {v4 .. v11}, Lcom/infraware/office/evengine/EvInterface;->IVideoInsert(Ljava/lang/String;Landroid/graphics/Bitmap;IIZLjava/lang/String;Z)V

    goto/16 :goto_0

    .line 1418
    .end local v12    # "VideoDirectoryPath":Ljava/lang/String;
    .end local v13    # "VideoFileName":Ljava/lang/String;
    .end local v18    # "nIdx":I
    :cond_5
    const-string/jumbo v2, "file://"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1420
    :try_start_0
    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v10

    .line 1421
    new-instance v20, Landroid/media/MediaMetadataRetriever;

    invoke-direct/range {v20 .. v20}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 1422
    .local v20, "retriever":Landroid/media/MediaMetadataRetriever;
    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 1423
    invoke-virtual/range {v20 .. v20}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime()Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    goto :goto_1

    .line 1424
    .end local v20    # "retriever":Landroid/media/MediaMetadataRetriever;
    :catch_0
    move-exception v15

    .line 1425
    .local v15, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v15}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1
.end method

.method public OnSaveConform()Ljava/lang/Boolean;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2107
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IDocumentModified_Editor()Z

    move-result v0

    if-ne v0, v1, :cond_0

    .line 2108
    const/16 v0, 0x18

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->showDialog(I)V

    .line 2109
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2111
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public OnSaveDoc(I)V
    .locals 10
    .param p1, "bOk"    # I

    .prologue
    .line 208
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbSaveing:Z

    .line 210
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mOfficePrintManager:Lcom/infraware/polarisoffice5/print/OfficePrintManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->isPossibleMakePdf()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 211
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->messageHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    sget-object v2, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->dialogId:Ljava/lang/String;

    const/16 v3, 0xb

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->fillMsg(ILjava/lang/String;ILjava/lang/String;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 212
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->messageHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    sget-object v2, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->dialogId:Ljava/lang/String;

    const/16 v3, 0x33

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->fillMsg(ILjava/lang/String;ILjava/lang/String;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 222
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbSendingEMail:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 223
    sparse-switch p1, :sswitch_data_0

    .line 229
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onSendEMailEnd()V

    .line 334
    :goto_0
    return-void

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mOfficePrintManager:Lcom/infraware/polarisoffice5/print/OfficePrintManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->makePdf()V

    goto :goto_0

    .line 226
    :sswitch_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onSendEMail(ZI)Z

    goto :goto_0

    .line 237
    :cond_1
    sparse-switch p1, :sswitch_data_1

    .line 314
    const-string/jumbo v0, "EvBaseEditorActivity"

    const-string/jumbo v1, "EV_INTERNAL_ERROR = "

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    const v0, 0x7f070265

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_nErrMsgId:I

    .line 319
    :cond_2
    :goto_1
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_nErrMsgId:I

    if-lez v0, :cond_3

    .line 320
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_nErrMsgId:I

    invoke-direct {p0, v0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onToastMessage(I)V

    .line 322
    :cond_3
    new-instance v7, Landroid/os/Handler;

    invoke-direct {v7}, Landroid/os/Handler;-><init>()V

    .line 323
    .local v7, "h_test":Landroid/os/Handler;
    new-instance v6, Lcom/infraware/office/baseframe/EvBaseEditorActivity$1;

    invoke-direct {v6, p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity$1;-><init>(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)V

    .line 333
    .local v6, "h_run":Ljava/lang/Runnable;
    const-wide/16 v0, 0x1f4

    invoke-virtual {v7, v6, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 240
    .end local v6    # "h_run":Ljava/lang/Runnable;
    .end local v7    # "h_test":Landroid/os/Handler;
    :sswitch_1
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mInternalCmdType:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mInternalCmdType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    .line 242
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mInternalCmdType:I

    .line 244
    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mCheckOriginalKeep:Z

    .line 245
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mstrOpenFilePath:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mstrRequestThumbnailPath:Ljava/lang/String;

    .line 246
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbIsSavedAs:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_6

    .line 247
    const-string/jumbo v0, "EvBaseEditorActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mFileSaveAs = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mFileSaveAs:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mFileSaveAs:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mstrRequestThumbnailPath:Ljava/lang/String;

    .line 250
    :cond_6
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbSaveAndFinish:Z

    if-nez v0, :cond_7

    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbPdfExport:Z

    if-nez v0, :cond_7

    .line 251
    const-string/jumbo v0, "EvBaseEditorActivity"

    const-string/jumbo v1, "OnSaveDoc : set to call getPageThumbnail()"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mRequestThumbnailOnSave:Z

    .line 256
    :cond_7
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_8

    .line 258
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbPdfExport:Z

    if-eqz v0, :cond_a

    .line 259
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mstrPdfExportFilePath:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onMediaDBBroadCast(Ljava/lang/String;)V

    .line 266
    :cond_8
    :goto_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbIsSavedAs:Z

    .line 267
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbPdfExport:Z

    .line 269
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/infraware/office/actionbar/MainActionBar;->mFileNameShow:Z

    .line 270
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->TitleRename()V

    .line 272
    invoke-static {}, Lcom/infraware/filemanager/database/recent/RecentFileManager;->getInstance()Lcom/infraware/filemanager/database/recent/RecentFileManager;

    move-result-object v8

    .line 273
    .local v8, "recent":Lcom/infraware/filemanager/database/recent/RecentFileManager;
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-virtual {v8, p0, v0}, Lcom/infraware/filemanager/database/recent/RecentFileManager;->InsertFileInfoToDB(Landroid/content/Context;Ljava/lang/String;)V

    .line 275
    const-string/jumbo v0, "EvBaseEditorActivity"

    const-string/jumbo v1, "EV_PROCESS_SUCCESS | EV_FILE_NOT_MODEFIED = "

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseView;->SetOpenType(I)V

    .line 277
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_nErrMsgId:I

    .line 279
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_9

    .line 280
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISetResetUndoData()I

    .line 282
    :cond_9
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mIsBackPressSave:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 283
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mIsBackPressSave:Z

    .line 284
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->finish()V

    goto/16 :goto_1

    .line 260
    .end local v8    # "recent":Lcom/infraware/filemanager/database/recent/RecentFileManager;
    :cond_a
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbIsSavedAs:Z

    if-eqz v0, :cond_b

    .line 261
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mFileSaveAs:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onMediaDBBroadCast(Ljava/lang/String;)V

    goto :goto_2

    .line 263
    :cond_b
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onMediaDBBroadCast(Ljava/lang/String;)V

    goto :goto_2

    .line 289
    :sswitch_2
    const-string/jumbo v0, "EvBaseEditorActivity"

    const-string/jumbo v1, "EV_DOCUMENT_TRUNCATED = "

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    const v0, 0x7f07019e

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_nErrMsgId:I

    goto/16 :goto_1

    .line 293
    :sswitch_3
    const-string/jumbo v0, "EvBaseEditorActivity"

    const-string/jumbo v1, "EV_REPAIRED_PAGE = "

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_nErrMsgId:I

    goto/16 :goto_1

    .line 297
    :sswitch_4
    const-string/jumbo v0, "EvBaseEditorActivity"

    const-string/jumbo v1, "EV_MEMORY_ERROR = "

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    const v0, 0x7f0700a7

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_nErrMsgId:I

    goto/16 :goto_1

    .line 301
    :sswitch_5
    const-string/jumbo v0, "EvBaseEditorActivity"

    const-string/jumbo v1, "EV_FILE_CREATE_ERROR = "

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    const v0, 0x7f070230

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_nErrMsgId:I

    goto/16 :goto_1

    .line 305
    :sswitch_6
    const-string/jumbo v0, "EvBaseEditorActivity"

    const-string/jumbo v1, "EV_FILE_WRITE_ERROR = "

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    const v0, 0x7f070090

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_nErrMsgId:I

    goto/16 :goto_1

    .line 309
    :sswitch_7
    const-string/jumbo v0, "EvBaseEditorActivity"

    const-string/jumbo v1, "EV_FILE_STORAGE_ERROR = "

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    const v0, 0x7f070230

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_nErrMsgId:I

    goto/16 :goto_1

    .line 223
    :sswitch_data_0
    .sparse-switch
        -0x13 -> :sswitch_0
        0x1 -> :sswitch_0
    .end sparse-switch

    .line 237
    :sswitch_data_1
    .sparse-switch
        -0x13 -> :sswitch_1
        -0x12 -> :sswitch_7
        -0x11 -> :sswitch_6
        -0x10 -> :sswitch_5
        -0x1 -> :sswitch_4
        0x1 -> :sswitch_1
        0x20 -> :sswitch_2
        0x30 -> :sswitch_3
    .end sparse-switch
.end method

.method public OnSelectCells()V
    .locals 4

    .prologue
    .line 791
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v2

    invoke-direct {v1, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 792
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0701ee

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 793
    const v2, 0x7f04000a

    new-instance v3, Lcom/infraware/office/baseframe/EvBaseEditorActivity$4;

    invoke-direct {v3, p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity$4;-><init>(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 801
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 802
    .local v0, "alertdialog":Landroid/app/AlertDialog;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 803
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 804
    return-void
.end method

.method public OnTotalLoadComplete()V
    .locals 0

    .prologue
    .line 2979
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnTotalLoadComplete()V

    .line 2981
    return-void
.end method

.method public OnUndoOrRedo(ZIII)V
    .locals 0
    .param p1, "bUndo"    # Z
    .param p2, "nAction"    # I
    .param p3, "nPage1"    # I
    .param p4, "nPage2"    # I

    .prologue
    .line 382
    return-void
.end method

.method public OpenColorPicker(II)V
    .locals 0
    .param p1, "type"    # I
    .param p2, "fontColor"    # I

    .prologue
    .line 2869
    invoke-virtual {p0, p1, p2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnColorPickerActivity(II)V

    .line 2870
    return-void
.end method

.method public OpenCommonTextInput(IILjava/lang/String;)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "aTitle"    # I
    .param p3, "aContent"    # Ljava/lang/String;

    .prologue
    .line 2875
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2876
    .local v0, "InputField":Landroid/content/Intent;
    const-string/jumbo v1, "key_filed_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2877
    const-string/jumbo v1, "key_title_text"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2878
    const-string/jumbo v1, "key_filed_get_value"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2879
    const-string/jumbo v1, "docType"

    iget v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mDocType:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2880
    const/16 v1, 0x16

    invoke-virtual {p0, v0, v1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2881
    return-void
.end method

.method protected TemplateDocumetMode()V
    .locals 0

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->setDeviceResolution()V

    .line 191
    return-void
.end method

.method public checkDocType()Z
    .locals 5

    .prologue
    const/16 v4, 0x12

    const/4 v3, 0x2

    const/4 v1, 0x0

    .line 415
    const/4 v0, 0x0

    .line 416
    .local v0, "docType":I
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetDocType()I

    move-result v0

    .line 418
    sparse-switch v0, :sswitch_data_0

    .line 436
    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 421
    :sswitch_0
    iget v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mDocExtensionType:I

    if-eq v2, v3, :cond_0

    .line 423
    iput v3, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mDocExtensionType:I

    goto :goto_0

    .line 428
    :sswitch_1
    iget v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mDocExtensionType:I

    if-eq v2, v4, :cond_0

    .line 430
    iput v4, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mDocExtensionType:I

    goto :goto_0

    .line 418
    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0xf -> :sswitch_1
    .end sparse-switch
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    .line 2676
    iget-boolean v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbFreedrawActionReady:Z

    if-ne v1, v3, :cond_0

    .line 2677
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbFreedrawActionReady:Z

    .line 2679
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 2680
    .local v0, "rect":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1, v0}, Lcom/infraware/office/baseframe/EvBaseView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 2682
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-ne v1, v3, :cond_0

    .line 2683
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onStartInsertFreeformShape()V

    .line 2687
    .end local v0    # "rect":Landroid/graphics/Rect;
    :cond_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->getActionMode()I

    move-result v1

    const/16 v2, 0xf

    if-ne v1, v2, :cond_1

    .line 2688
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 2689
    .restart local v0    # "rect":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1, v0}, Lcom/infraware/office/baseframe/EvBaseView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 2691
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2692
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onEndInsertFreeformShape()V

    .line 2696
    .end local v0    # "rect":Landroid/graphics/Rect;
    :cond_1
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    return v1
.end method

.method protected downloadImageFile(Landroid/net/Uri;Ljava/io/File;)Z
    .locals 11
    .param p1, "aUri"    # Landroid/net/Uri;
    .param p2, "attachment_name"    # Ljava/io/File;

    .prologue
    const/4 v7, 0x0

    .line 1744
    const/4 v6, 0x0

    .line 1745
    .local v6, "stream":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 1746
    .local v4, "out":Ljava/io/FileOutputStream;
    const/4 v0, 0x0

    .line 1748
    .local v0, "buf":[B
    :try_start_0
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-virtual {v8, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v6

    .line 1749
    if-nez v6, :cond_0

    .line 1750
    const-string/jumbo v8, "EvBaseEditorActivity"

    const-string/jumbo v9, "[downloadImageFile()] stream is NULL!"

    invoke-static {v8, v9}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1803
    :goto_0
    return v7

    .line 1754
    :cond_0
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 1755
    .end local v4    # "out":Ljava/io/FileOutputStream;
    .local v5, "out":Ljava/io/FileOutputStream;
    const/16 v8, 0x400

    :try_start_1
    new-array v0, v8, [B

    .line 1757
    :goto_1
    invoke-virtual {v6, v0}, Ljava/io/InputStream;->read([B)I

    move-result v3

    .line 1758
    .local v3, "numread":I
    if-gtz v3, :cond_1

    .line 1764
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 1765
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    .line 1800
    const/4 v4, 0x0

    .line 1801
    .end local v5    # "out":Ljava/io/FileOutputStream;
    .restart local v4    # "out":Ljava/io/FileOutputStream;
    const/4 v0, 0x0

    .line 1802
    const/4 v6, 0x0

    .line 1803
    const/4 v7, 0x1

    goto :goto_0

    .line 1761
    .end local v4    # "out":Ljava/io/FileOutputStream;
    .restart local v5    # "out":Ljava/io/FileOutputStream;
    :cond_1
    const/4 v8, 0x0

    invoke-virtual {v5, v0, v8, v3}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1766
    .end local v3    # "numread":I
    :catch_0
    move-exception v1

    move-object v4, v5

    .line 1767
    .end local v5    # "out":Ljava/io/FileOutputStream;
    .local v1, "e":Ljava/io/IOException;
    .restart local v4    # "out":Ljava/io/FileOutputStream;
    :goto_2
    const-string/jumbo v8, "EvBaseEditorActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "[downloadImageFile()] IOException error! Status:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1768
    if-eqz v4, :cond_2

    .line 1772
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1780
    :cond_2
    :goto_3
    if-eqz v6, :cond_3

    .line 1784
    :try_start_3
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 1792
    :cond_3
    :goto_4
    const/4 v4, 0x0

    .line 1793
    const/4 v0, 0x0

    .line 1794
    const/4 v6, 0x0

    .line 1797
    goto :goto_0

    .line 1774
    :catch_1
    move-exception v2

    .line 1777
    .local v2, "e1":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 1786
    .end local v2    # "e1":Ljava/io/IOException;
    :catch_2
    move-exception v2

    .line 1789
    .restart local v2    # "e1":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 1766
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "e1":Ljava/io/IOException;
    :catch_3
    move-exception v1

    goto :goto_2
.end method

.method public endShapeDrawing(Z)V
    .locals 2
    .param p1, "updateMode"    # Z

    .prologue
    const/4 v1, 0x0

    .line 2857
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseView;->setActionMode(I)V

    .line 2859
    if-eqz p1, :cond_0

    .line 2861
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->drawAllContents()V

    .line 2864
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/MainActionBar;->changeDrawMode(I)Z

    .line 2866
    return-void
.end method

.method public getActionTitlebar()Lcom/infraware/office/actionbar/ActionTitleBar;
    .locals 1

    .prologue
    .line 2954
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    return-object v0
.end method

.method public getCurrentShapeStyleNum()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mStyle_num:I

    return v0
.end method

.method protected getImageFileFromWeb(Landroid/content/Intent;)Ljava/lang/String;
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x0

    .line 1691
    const/4 v0, 0x0

    .line 1693
    .local v0, "fileName":Ljava/lang/String;
    if-nez p1, :cond_1

    .line 1716
    :cond_0
    :goto_0
    return-object v5

    .line 1696
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    .line 1698
    .local v4, "uri":Landroid/net/Uri;
    if-eqz v4, :cond_0

    .line 1699
    invoke-virtual {p0, p1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getImageFileName(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 1700
    if-eqz v0, :cond_0

    .line 1705
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 1707
    .local v2, "szDownloadFolder":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1710
    .local v3, "tempFilePath":Ljava/io/File;
    invoke-virtual {p0, v4, v3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->downloadImageFile(Landroid/net/Uri;Ljava/io/File;)Z

    move-result v1

    .line 1712
    .local v1, "isSuccess":Z
    if-eqz v1, :cond_0

    .line 1716
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method protected getImageFileName(Landroid/content/Intent;)Ljava/lang/String;
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 1722
    const/4 v6, 0x0

    .line 1723
    .local v6, "attachment_name":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1724
    .local v7, "c":Landroid/database/Cursor;
    if-nez v7, :cond_0

    .line 1725
    const-string/jumbo v0, "EvBaseEditorActivity"

    const-string/jumbo v1, "[getImageFileName] cursor from ContentResolver is NULL!!!"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1737
    :goto_0
    return-object v2

    .line 1728
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1729
    const-string/jumbo v0, "_display_name"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 1730
    .local v8, "fileNameColumnId":I
    if-ltz v8, :cond_1

    .line 1731
    invoke-interface {v7, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1732
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-object v2, v6

    .line 1733
    goto :goto_0

    .line 1735
    :cond_1
    const-string/jumbo v0, "EvBaseEditorActivity"

    const-string/jumbo v1, "[getImageFileName] Attachment name cannot be found!!!"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1736
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public getVideoManager()Lcom/infraware/polarisoffice5/common/VideoFrameLayout;
    .locals 1

    .prologue
    .line 1667
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mVideoFrameLayout:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    return-object v0
.end method

.method public insertCamera()V
    .locals 5

    .prologue
    .line 754
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 755
    .local v0, "cameraIntent":Landroid/content/Intent;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "tmp_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 756
    .local v1, "url":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mCameraImageUri:Landroid/net/Uri;

    .line 757
    const-string/jumbo v2, "output"

    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mCameraImageUri:Landroid/net/Uri;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 758
    const/16 v2, 0x12

    invoke-virtual {p0, v0, v2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 759
    return-void
.end method

.method public insertGallery(Z)V
    .locals 4
    .param p1, "isReplace"    # Z

    .prologue
    .line 711
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.GET_CONTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 712
    .local v0, "insertimageIntent":Landroid/content/Intent;
    const-string/jumbo v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 714
    if-nez p1, :cond_1

    .line 716
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mIsReplace:Z

    .line 718
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getDocType()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 719
    const-string/jumbo v1, "image/* video/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 722
    :cond_0
    const-string/jumbo v1, "android.intent.extra.TITLE"

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07017c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 730
    :goto_0
    const/16 v1, 0x11

    invoke-virtual {p0, v0, v1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 733
    return-void

    .line 726
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mIsReplace:Z

    .line 728
    const-string/jumbo v1, "android.intent.extra.TITLE"

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0701cf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public insertVideoCapture()V
    .locals 4

    .prologue
    .line 736
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "android.media.action.VIDEO_CAPTURE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 737
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/high16 v3, 0x10000

    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 739
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 740
    const/16 v2, 0x25

    invoke-virtual {p0, v0, v2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 743
    :cond_0
    return-void
.end method

.method public insertVideoFile()V
    .locals 3

    .prologue
    .line 747
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 748
    .local v0, "oIntent":Landroid/content/Intent;
    const-string/jumbo v1, "android.intent.extra.LOCAL_ONLY"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 749
    const-string/jumbo v1, "video/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 750
    const/16 v1, 0x26

    invoke-virtual {p0, v0, v1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 751
    return-void
.end method

.method public isFreeDrawMode()Z
    .locals 1

    .prologue
    .line 2936
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->isFreeDrawMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2937
    const/4 v0, 0x1

    .line 2938
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTableDrawMode()Z
    .locals 1

    .prologue
    .line 2943
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->isTableDrawMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2944
    const/4 v0, 0x1

    .line 2945
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 819
    invoke-super {p0, p1, p2, p3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 820
    const/4 v2, -0x1

    if-ne p2, v2, :cond_2

    .line 821
    packed-switch p1, :pswitch_data_0

    .line 921
    :goto_0
    :pswitch_0
    return-void

    .line 823
    :pswitch_1
    iput-boolean v3, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbSaveing:Z

    .line 824
    iput-boolean v3, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbPdfExport:Z

    .line 829
    const-string/jumbo v2, "key_new_file"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mstrPdfExportFilePath:Ljava/lang/String;

    .line 830
    const/16 v2, 0x33

    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->showDialog(I)V

    .line 832
    const-string/jumbo v2, "key_save_include_pendraw"

    invoke-virtual {p3, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 833
    .local v1, "savePendraw":Ljava/lang/Boolean;
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-ne v2, v3, :cond_0

    .line 834
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mstrPdfExportFilePath:Ljava/lang/String;

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Lcom/infraware/office/evengine/EvInterface;->ISaveDocument(Ljava/lang/String;I)V

    goto :goto_0

    .line 836
    :cond_0
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mstrPdfExportFilePath:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/infraware/office/evengine/EvInterface;->ISaveDocument(Ljava/lang/String;I)V

    goto :goto_0

    .line 845
    .end local v1    # "savePendraw":Ljava/lang/Boolean;
    :pswitch_2
    invoke-direct {p0, p3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnResultInsertImageActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 848
    :pswitch_3
    invoke-virtual {p0, p3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnResultCameraActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 851
    :pswitch_4
    invoke-virtual {p0, p3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnResultInsertInsertTableActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 854
    :pswitch_5
    invoke-virtual {p0, p3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnResultInsertHyperlinkActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 857
    :pswitch_6
    invoke-virtual {p0, p3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnResultColorPickerActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 860
    :pswitch_7
    invoke-virtual {p0, p3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnResultCommonInputFieldActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 863
    :pswitch_8
    invoke-virtual {p0, p3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnResultInsertChartActivitry(Landroid/content/Intent;)V

    goto :goto_0

    .line 866
    :pswitch_9
    invoke-virtual {p0, p3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnResultChartDataActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 870
    :pswitch_a
    invoke-direct {p0, p3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnResultSplitCellActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 874
    :pswitch_b
    invoke-direct {p0, p3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnResultResizeObjActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 877
    :pswitch_c
    invoke-direct {p0, p3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnResultRotateObjeActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 883
    :pswitch_d
    invoke-direct {p0, p3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onResultWheelButtonActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 886
    :pswitch_e
    invoke-virtual {p3, p0}, Landroid/content/Intent;->resolveType(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 887
    .local v0, "dataType":Ljava/lang/String;
    const-string/jumbo v2, "video"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 888
    invoke-virtual {p0, p3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnResultVideoActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 891
    :cond_1
    invoke-direct {p0, p3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnResultInsertImageActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 895
    .end local v0    # "dataType":Ljava/lang/String;
    :pswitch_f
    invoke-virtual {p0, p3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnResultCameraActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 898
    :pswitch_10
    invoke-virtual {p0, p3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnResultVideoActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 901
    :pswitch_11
    invoke-direct {p0, p3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnResultRecordingActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 907
    :pswitch_12
    invoke-direct {p0, p3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnResultInsertSymbol(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 912
    :cond_2
    packed-switch p1, :pswitch_data_1

    goto/16 :goto_0

    .line 914
    :pswitch_13
    iput-boolean v4, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mIsBackPressSave:Z

    goto/16 :goto_0

    .line 821
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_e
        :pswitch_f
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_1
        :pswitch_0
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_12
        :pswitch_10
        :pswitch_11
    .end packed-switch

    .line 912
    :pswitch_data_1
    .packed-switch 0xc
        :pswitch_13
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 1998
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->drawMode_hideOption()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2104
    :cond_0
    :goto_0
    return-void

    .line 2001
    :cond_1
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v1}, Lcom/infraware/office/actionbar/MainActionBar;->getDrawMode()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 2003
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->hideAllToolbar()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2014
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->isShow()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2017
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    instance-of v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    check-cast v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;

    iget-boolean v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsMultiSelectionMode:Z

    if-nez v0, :cond_5

    .line 2020
    const/16 v1, 0x29

    const/16 v2, 0x45

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object v0, p0

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    .line 2022
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v3, v3}, Lcom/infraware/office/evengine/EvInterface;->ISetCroppingMode(II)V

    .line 2024
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->hide()V

    .line 2025
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v0, :cond_3

    .line 2026
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->show()V

    .line 2027
    :cond_3
    iput-boolean v3, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbCroppingMode:Z

    goto :goto_0

    .line 2008
    :cond_4
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->hideAllToolbarDelay()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 2033
    :cond_5
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    instance-of v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    check-cast v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;

    iget-boolean v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsMultiSelectionMode:Z

    if-eqz v0, :cond_6

    .line 2036
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IClearFrameSet()V

    .line 2037
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v3}, Lcom/infraware/office/evengine/EvInterface;->ISetMultiSelect(I)V

    .line 2038
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    check-cast v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;

    iput-boolean v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsMultiSelectionMode:Z

    goto :goto_0

    .line 2053
    :cond_6
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v7

    .line 2054
    .local v7, "ebv":Lcom/infraware/office/baseframe/EvBaseView;
    if-eqz v7, :cond_7

    iget-object v0, v7, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    if-eqz v0, :cond_7

    iget-object v0, v7, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2055
    iget-object v0, v7, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismissAll()V

    .line 2078
    :cond_7
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isShown()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2080
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->hidePanel()V

    goto/16 :goto_0

    .line 2084
    :cond_8
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v0, :cond_9

    .line 2085
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->dismissMenuPopup()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2103
    :cond_9
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onBackPressed()V

    goto/16 :goto_0
.end method

.method public onCameraActivity()V
    .locals 5

    .prologue
    .line 620
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 621
    .local v0, "cameraIntent":Landroid/content/Intent;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "tmp_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 622
    .local v1, "url":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mCameraImageUri:Landroid/net/Uri;

    .line 623
    const-string/jumbo v2, "output"

    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mCameraImageUri:Landroid/net/Uri;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 624
    const/16 v2, 0xe

    invoke-virtual {p0, v0, v2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 625
    return-void
.end method

.method public onChangeScreen(I)V
    .locals 9
    .param p1, "nType"    # I

    .prologue
    const/4 v5, 0x0

    .line 176
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onChangeScreen(I)V

    .line 177
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnInitFreeDrawOption()V

    .line 178
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnInitPenDrawOption()V

    .line 179
    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v4}, Lcom/infraware/office/baseframe/EvBaseView;->getActionMode()I

    move-result v4

    const/4 v6, 0x5

    if-eq v4, v6, :cond_0

    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v4}, Lcom/infraware/office/baseframe/EvBaseView;->getActionMode()I

    move-result v4

    const/16 v6, 0x8

    if-eq v4, v6, :cond_0

    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v4}, Lcom/infraware/office/baseframe/EvBaseView;->getActionMode()I

    move-result v4

    const/16 v6, 0xb

    if-ne v4, v6, :cond_1

    .line 182
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 183
    .local v0, "downTime":J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 184
    .local v2, "eventTime":J
    const/4 v4, 0x1

    const/4 v7, 0x0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    .line 185
    .local v8, "up_event":Landroid/view/MotionEvent;
    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v4, v8}, Lcom/infraware/office/baseframe/EvBaseView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 187
    .end local v0    # "downTime":J
    .end local v2    # "eventTime":J
    .end local v8    # "up_event":Landroid/view/MotionEvent;
    :cond_1
    return-void
.end method

.method protected onCheckTypeSave()V
    .locals 1

    .prologue
    .line 2499
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mIsDifType:Z

    .line 2500
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getDocExtensionType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2507
    :goto_0
    return-void

    .line 2504
    :pswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mIsDifType:Z

    goto :goto_0

    .line 2500
    nop

    :pswitch_data_0
    .packed-switch 0x12
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onClickColorPicker(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 2657
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2658
    .local v0, "intent":Landroid/content/Intent;
    iget-boolean v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbPenDraw:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 2659
    const-string/jumbo v1, "color_value"

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getColor()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2662
    :goto_0
    const/16 v1, 0x15

    invoke-virtual {p0, v0, v1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2663
    return-void

    .line 2661
    :cond_0
    const-string/jumbo v1, "color_value"

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mFreeDrawOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getColor()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/res/Configuration;

    .prologue
    .line 446
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mCurrentLocale:Ljava/util/Locale;

    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    if-eq v0, v1, :cond_0

    .line 447
    iget-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mCurrentLocale:Ljava/util/Locale;

    .line 448
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onLocaleChanged()V

    .line 451
    :cond_0
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 452
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 158
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mCurrentLocale:Ljava/util/Locale;

    .line 160
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEV_VIEW_MODE:I

    .line 163
    invoke-static {}, Lcom/infraware/common/util/Utils;->isL()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    const v0, 0x103012c

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->setTheme(I)V

    .line 166
    :cond_0
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onCreate(Landroid/os/Bundle;)V

    .line 167
    return-void
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;

    .prologue
    .line 2363
    sparse-switch p1, :sswitch_data_0

    .line 2419
    invoke-super {p0, p1, p2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v3

    :goto_0
    return-object v3

    .line 2365
    :sswitch_0
    invoke-static {}, Lcom/infraware/polarisoffice5/common/DlgFactory;->getInstance()Lcom/infraware/polarisoffice5/common/DlgFactory;

    move-result-object v6

    invoke-virtual {v6, p0}, Lcom/infraware/polarisoffice5/common/DlgFactory;->createSaveProgressDlg(Landroid/app/Activity;)Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;

    move-result-object v3

    goto :goto_0

    .line 2367
    :sswitch_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    .line 2369
    .local v0, "dlgText":Ljava/lang/String;
    const/4 v1, 0x0

    .line 2370
    .local v1, "fileName":Ljava/lang/String;
    iget-object v6, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mstrPdfExportFilePath:Ljava/lang/String;

    if-eqz v6, :cond_0

    .line 2371
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mstrPdfExportFilePath:Ljava/lang/String;

    .line 2377
    :goto_1
    invoke-static {v1}, Lcom/infraware/common/util/FileUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2378
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2379
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0701c0

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2381
    invoke-static {}, Lcom/infraware/polarisoffice5/common/DlgFactory;->getInstance()Lcom/infraware/polarisoffice5/common/DlgFactory;

    move-result-object v6

    invoke-virtual {v6, p0}, Lcom/infraware/polarisoffice5/common/DlgFactory;->createExportToPdfDlg(Landroid/app/Activity;)Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;

    move-result-object v3

    .line 2382
    .local v3, "pd":Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f07012f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 2383
    invoke-virtual {v3, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2374
    .end local v3    # "pd":Landroid/app/ProgressDialog;
    :cond_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mstrOpenFilePath:Ljava/lang/String;

    goto :goto_1

    .line 2388
    .end local v0    # "dlgText":Ljava/lang/String;
    .end local v1    # "fileName":Ljava/lang/String;
    :sswitch_2
    invoke-direct {p0, p1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onCreateMsgPopup(I)Landroid/app/AlertDialog;

    move-result-object v3

    goto :goto_0

    .line 2393
    :sswitch_3
    const-string/jumbo v6, "layout_inflater"

    invoke-virtual {p0, v6}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    .line 2394
    .local v5, "vi":Landroid/view/LayoutInflater;
    const v6, 0x7f030041

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 2395
    .local v4, "v":Landroid/view/View;
    const v6, 0x7f0b01d5

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 2396
    .local v2, "msg":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070271

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2398
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v7

    invoke-direct {v6, p0, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f07017c

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/infraware/office/baseframe/EvBaseEditorActivity$7;

    invoke-direct {v7, p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity$7;-><init>(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mProgressDialog:Landroid/app/AlertDialog;

    .line 2415
    iget-object v6, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mProgressDialog:Landroid/app/AlertDialog;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 2416
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mProgressDialog:Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 2363
    nop

    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0x18 -> :sswitch_2
        0x19 -> :sswitch_2
        0x1f -> :sswitch_2
        0x2b -> :sswitch_3
        0x2d -> :sswitch_3
        0x33 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 171
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onDestroy()V

    .line 172
    return-void
.end method

.method public onEndInsertFreeformShape()V
    .locals 0

    .prologue
    .line 2654
    return-void
.end method

.method public onEndTableDraw(Z)V
    .locals 2
    .param p1, "updateMode"    # Z

    .prologue
    const/4 v1, 0x0

    .line 2612
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/MainActionBar;->changeDrawMode(I)Z

    .line 2614
    if-eqz p1, :cond_0

    .line 2616
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseView;->setActionMode(I)V

    .line 2619
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISetTableCancleMode()V

    .line 2620
    invoke-direct {p0, v1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->setTableDrawMode(Z)V

    .line 2621
    return-void
.end method

.method public onLocaleChange(I)V
    .locals 2
    .param p1, "nLocale"    # I

    .prologue
    .line 2563
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onLocaleChange(I)V

    .line 2565
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v1}, Lcom/infraware/office/actionbar/MainActionBar;->isVisible()Z

    move-result v1

    if-ne v0, v1, :cond_0

    .line 2566
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->onLocaleChanged()V

    .line 2568
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mProgressDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mProgressDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2569
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mProgressDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 2570
    const/16 v0, 0x2d

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->showDialog(I)V

    .line 2572
    :cond_1
    return-void
.end method

.method public onMediaDBBroadCast(Ljava/lang/String;)V
    .locals 5
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 2930
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 2931
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "file://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2932
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 2510
    invoke-super {p0, p1, p2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 2511
    sparse-switch p1, :sswitch_data_0

    .line 2518
    :goto_0
    return-void

    .line 2515
    :sswitch_0
    invoke-virtual {p0, p1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->removeDialog(I)V

    goto :goto_0

    .line 2511
    nop

    :sswitch_data_0
    .sparse-switch
        0x18 -> :sswitch_0
        0x19 -> :sswitch_0
        0x1f -> :sswitch_0
    .end sparse-switch
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 2923
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onResume()V

    .line 2925
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->drawAllContents()V

    .line 2926
    return-void
.end method

.method public onSettingActivity()V
    .locals 3

    .prologue
    .line 811
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/infraware/polarisoffice5/setting/SettingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 812
    .local v0, "settingIntent":Landroid/content/Intent;
    const-string/jumbo v1, "key_version"

    const v2, 0x7f070322

    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 813
    const-string/jumbo v1, "key_interanl_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 814
    const/16 v1, 0xd

    invoke-virtual {p0, v0, v1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 815
    return-void
.end method

.method public onStartInsertFreeformShape()V
    .locals 0

    .prologue
    .line 2635
    return-void
.end method

.method public onStartLASSO(Z)V
    .locals 3
    .param p1, "bStart"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2587
    if-eqz p1, :cond_0

    .line 2588
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseView;->setActionMode(I)V

    .line 2589
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/MainActionBar;->changeDrawMode(I)Z

    .line 2590
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/16 v1, 0x8

    invoke-virtual {v0, v1, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetPenMode(IZ)V

    .line 2596
    :goto_0
    return-void

    .line 2592
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v1, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetPenMode(IZ)V

    .line 2593
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/MainActionBar;->changeDrawMode(I)Z

    .line 2594
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseView;->setActionMode(I)V

    goto :goto_0
.end method

.method public onStartTableDraw(Z)V
    .locals 2
    .param p1, "updateMode"    # Z

    .prologue
    .line 2575
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/MainActionBar;->changeDrawMode(I)Z

    .line 2576
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseView;->setActionMode(I)V

    .line 2577
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISetTableDrawMode()V

    .line 2578
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->setTableDrawMode(Z)V

    .line 2580
    if-eqz p1, :cond_0

    .line 2582
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->drawAllContents()V

    .line 2584
    :cond_0
    return-void
.end method

.method public onStartTableErease(Z)V
    .locals 2
    .param p1, "updateMode"    # Z

    .prologue
    .line 2599
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseView;->setActionMode(I)V

    .line 2600
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISetTableEreaseMode()V

    .line 2601
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->setTableDrawMode(Z)V

    .line 2604
    if-eqz p1, :cond_0

    .line 2606
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->drawAllContents()V

    .line 2608
    :cond_0
    return-void
.end method

.method protected processShortCut(Landroid/view/KeyEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2150
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v4

    and-int/lit16 v4, v4, 0x1000

    if-eqz v4, :cond_1

    move v1, v2

    .line 2151
    .local v1, "isCtrl":Z
    :goto_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v4

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_2

    move v0, v2

    .line 2153
    .local v0, "bShift":Z
    :goto_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    .line 2185
    :cond_0
    :goto_2
    return-void

    .end local v0    # "bShift":Z
    .end local v1    # "isCtrl":Z
    :cond_1
    move v1, v3

    .line 2150
    goto :goto_0

    .restart local v1    # "isCtrl":Z
    :cond_2
    move v0, v3

    .line 2151
    goto :goto_1

    .line 2156
    .restart local v0    # "bShift":Z
    :sswitch_0
    if-ne v1, v2, :cond_0

    .line 2157
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2, v3, v3, v5}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    goto :goto_2

    .line 2161
    :sswitch_1
    if-ne v1, v2, :cond_0

    .line 2162
    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v4, v2, v3, v5}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    goto :goto_2

    .line 2166
    :sswitch_2
    if-ne v1, v2, :cond_0

    .line 2167
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mClipBoard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getDocType()I

    move-result v5

    invoke-virtual {v2, v4, v3, v3, v5}, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->doPaste(Lcom/infraware/office/evengine/EvInterface;ZZI)Z

    goto :goto_2

    .line 2171
    :sswitch_3
    if-ne v1, v2, :cond_0

    .line 2172
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->ISelectAll()V

    goto :goto_2

    .line 2176
    :sswitch_4
    if-ne v1, v2, :cond_0

    .line 2177
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3, v2}, Lcom/infraware/office/evengine/EvInterface;->IRedoUndo(I)V

    goto :goto_2

    .line 2181
    :sswitch_5
    if-ne v1, v2, :cond_0

    .line 2182
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IRedoUndo(I)V

    goto :goto_2

    .line 2153
    nop

    :sswitch_data_0
    .sparse-switch
        0x1d -> :sswitch_3
        0x1f -> :sswitch_1
        0x32 -> :sswitch_2
        0x34 -> :sswitch_0
        0x35 -> :sswitch_5
        0x36 -> :sswitch_4
    .end sparse-switch
.end method

.method public setEvListener()V
    .locals 0

    .prologue
    .line 2895
    return-void
.end method

.method protected setScreenLock(Z)V
    .locals 1
    .param p1, "lock"    # Z

    .prologue
    const/4 v0, 0x1

    .line 199
    if-ne p1, v0, :cond_1

    .line 200
    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->setRequestedOrientation(I)V

    .line 203
    :cond_0
    :goto_0
    return-void

    .line 201
    :cond_1
    if-nez p1, :cond_0

    .line 202
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->setRequestedOrientation(I)V

    goto :goto_0
.end method
