.class public abstract Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;
.super Ljava/lang/Object;
.source "EvBaseGestureProc.java"

# interfaces
.implements Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;
.implements Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback$msg;
.implements Lcom/infraware/office/baseframe/gestureproc/porting/EvAdvanceMotionDetector$OnEvMotionListener;
.implements Lcom/infraware/office/evengine/E$EDVA_PAGE_INFO_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_CHAR_INPUT;
.implements Lcom/infraware/office/evengine/E$EV_EDIT_OBJECT_POINT_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_EDIT_OBJECT_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_GUI_EVENT;
.implements Lcom/infraware/office/evengine/E$EV_HID_ACTION;
.implements Lcom/infraware/office/evengine/E$EV_KEY_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_SCROLL_COMMAND_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_SCROLL_FACTOR_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_VKEYS;
.implements Lcom/infraware/office/evengine/E$EV_ZOOM_TYPE;
.implements Lcom/infraware/office/evengine/E;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;,
        Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;,
        Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;
    }
.end annotation


# static fields
.field public static final FIND_MODE:I = 0x1

.field public static final FREEDRAW_ERASER_MODE:I = 0x8

.field public static final FREEDRAW_MARK_MODE:I = 0x5

.field public static final FREEDRAW_MARK_OPTION_MODE:I = 0x6

.field public static final FREEDRAW_PANNING_MODE:I = 0x7

.field public static final FREE_SHAPE_DRAWING_MODE:I = 0x4

.field public static final INFRAPENDRAW_ERASE_MODE:I = 0xe

.field public static final INFRAPENDRAW_LASSO_MODE:I = 0xd

.field public static final INFRAPENDRAW_PEN_MODE:I = 0xc

.field public static final INSERT_FREEFORM_SHAPES_MODE:I = 0xf

.field public static final LASSO_MODE:I = 0xb

.field public static final NORMAL_MODE:I = 0x0

.field public static final PANNING_MODE:I = 0x11

.field public static final POINTER_DRAW_MODE:I = 0x10

.field public static final RULERBAR_MODE:I = 0xa

.field protected static final STATE_DOUBLE_TAP:I = 0x5

.field protected static final STATE_DOWN:I = 0x1

.field protected static final STATE_DOWN_MOVE:I = 0x2

.field protected static final STATE_DOWN_UP:I = 0xa

.field protected static final STATE_DOWN_UP_DOWN:I = 0xb

.field protected static final STATE_DOWN_UP_DOWN_MOVE:I = 0xc

.field protected static final STATE_DOWN_UP_DOWN_UP:I = 0x14

.field protected static final STATE_DOWN_WAITMOVE:I = 0x3

.field protected static final STATE_LONG_PRESS:I = 0x6

.field protected static final STATE_NONE:I = 0x0

.field public static final TABLE_DRAW_MODE:I = 0x9

.field public static final WORD_MEMO_MODE:I = 0x2

.field public static final WORD_TTS_MODE:I = 0x3


# instance fields
.field public final GESTURE_CHANGE_SCALE:I

.field public final GESTURE_CHANGE_SCALE_DOWN:I

.field public final GESTURE_CHANGE_SCALE_END:I

.field public final GESTURE_DOUBLE_TAP:I

.field public final GESTURE_DOWN:I

.field public final GESTURE_DRAG:I

.field public final GESTURE_FLING:I

.field public final GESTURE_LATE_DRAG:I

.field public final GESTURE_LONG_PRESS:I

.field public final GESTURE_MULTI_TOUCH_MOVE:I

.field public final GESTURE_NONE:I

.field public final GESTURE_PEN_PANNING:I

.field public final GESTURE_PEN_SCALE:I

.field public final GESTURE_PINCH_ZOOM:I

.field protected final GYRO_TILT_THRESHOLD:I

.field protected mActionMode:I

.field protected mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

.field protected mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

.field protected mAdvMotionDetector:Lcom/infraware/office/baseframe/gestureproc/porting/EvAdvanceMotionDetector;

.field protected mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

.field protected mEvInterface:Lcom/infraware/office/evengine/EvInterface;

.field protected mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

.field protected mGyroPreviousTime:J

.field protected mGyroTiltCurrentValue:I

.field protected final mInitialScrollBarAlpha:I

.field protected mPageInfo:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;

.field mPageInfoHandler:Landroid/os/Handler;

.field protected mPageInfoTimer:Ljava/util/Timer;

.field protected mPageInfoType:I

.field protected mPointerView:Lcom/infraware/polarisoffice5/common/PointerDrawView;

.field public mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

.field protected mScaleTouchBeginScale:I

.field protected mScaleTouchBeginSpace:F

.field protected mScaleTouchPreCenter:Landroid/graphics/PointF;

.field protected mScaleTouchPreSpace:F

.field protected mScrollBarAlpha:I

.field protected final mScrollBarMargin:F

.field protected final mScrollBarMinSize:F

.field protected final mScrollBarThickness:F

.field protected mTouchStatus:I

.field protected mTransformInfo:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;

.field protected mView:Landroid/view/View;

.field protected mbFlickEnable:Z

.field protected mbScaleTouchEnable:Z

.field protected mbScrollbar:Z


# direct methods
.method constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;)V
    .locals 6
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "listener"    # Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 59
    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    .line 60
    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mView:Landroid/view/View;

    .line 61
    iput v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoType:I

    .line 62
    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    .line 63
    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mAdvMotionDetector:Lcom/infraware/office/baseframe/gestureproc/porting/EvAdvanceMotionDetector;

    .line 64
    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    .line 65
    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfo:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;

    .line 66
    const/16 v0, 0xe6

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mInitialScrollBarAlpha:I

    .line 67
    const/16 v0, 0xe6

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mScrollBarAlpha:I

    .line 68
    iput-boolean v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mbScrollbar:Z

    .line 69
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mScrollBarThickness:F

    .line 70
    const/high16 v0, 0x41f00000    # 30.0f

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mScrollBarMinSize:F

    .line 71
    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mScrollBarMargin:F

    .line 72
    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mTransformInfo:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;

    .line 74
    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    .line 75
    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    .line 78
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v1, v1}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mScaleTouchPreCenter:Landroid/graphics/PointF;

    .line 79
    iput v5, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mScaleTouchPreSpace:F

    .line 80
    iput v5, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mScaleTouchBeginSpace:F

    .line 81
    iput v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mScaleTouchBeginScale:I

    .line 82
    iput v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mGyroTiltCurrentValue:I

    .line 83
    const/4 v0, 0x5

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->GYRO_TILT_THRESHOLD:I

    .line 84
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mGyroPreviousTime:J

    .line 86
    iput v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->GESTURE_NONE:I

    .line 87
    iput v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->GESTURE_DOWN:I

    .line 88
    const/4 v0, 0x2

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->GESTURE_DRAG:I

    .line 89
    const/4 v0, 0x3

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->GESTURE_PINCH_ZOOM:I

    .line 90
    const/4 v0, 0x4

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->GESTURE_FLING:I

    .line 91
    const/4 v0, 0x5

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->GESTURE_LATE_DRAG:I

    .line 92
    const/4 v0, 0x6

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->GESTURE_DOUBLE_TAP:I

    .line 93
    const/4 v0, 0x7

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->GESTURE_LONG_PRESS:I

    .line 94
    const/16 v0, 0x8

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->GESTURE_CHANGE_SCALE_DOWN:I

    .line 95
    const/16 v0, 0x9

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->GESTURE_MULTI_TOUCH_MOVE:I

    .line 96
    const/16 v0, 0xa

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->GESTURE_PEN_PANNING:I

    .line 97
    const/16 v0, 0xb

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->GESTURE_PEN_SCALE:I

    .line 98
    const/16 v0, 0xc

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->GESTURE_CHANGE_SCALE:I

    .line 99
    const/16 v0, 0xd

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->GESTURE_CHANGE_SCALE_END:I

    .line 120
    iput v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mTouchStatus:I

    .line 121
    iput v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mActionMode:I

    .line 123
    iput-boolean v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mbFlickEnable:Z

    .line 124
    iput-boolean v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mbScaleTouchEnable:Z

    .line 141
    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPointerView:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    .line 648
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoHandler:Landroid/os/Handler;

    .line 144
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 145
    iput-object p3, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    .line 146
    iput-object p2, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mView:Landroid/view/View;

    .line 147
    new-instance v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;

    invoke-direct {v0, p0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;-><init>(Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfo:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;

    .line 148
    new-instance v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;

    invoke-direct {v0, p0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;-><init>(Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mTransformInfo:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;

    .line 152
    new-instance v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    invoke-direct {v0, p1, p2, p0}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    .line 153
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 155
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "FT03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    new-instance v0, Lcom/infraware/office/baseframe/gestureproc/porting/EvAdvanceMotionDetector;

    invoke-direct {v0, p1, p0}, Lcom/infraware/office/baseframe/gestureproc/porting/EvAdvanceMotionDetector;-><init>(Landroid/content/Context;Lcom/infraware/office/baseframe/gestureproc/porting/EvAdvanceMotionDetector$OnEvMotionListener;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mAdvMotionDetector:Lcom/infraware/office/baseframe/gestureproc/porting/EvAdvanceMotionDetector;

    .line 158
    :cond_0
    new-instance v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-direct {v0, p2, p3, p0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;-><init>(Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object v0, p1

    .line 160
    check-cast v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 162
    instance-of v0, p1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_1

    .line 163
    check-cast p1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 165
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_2

    .line 167
    new-instance v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-direct {v0, v1, p2, v2}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    .line 168
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->setDocType(I)V

    .line 170
    :cond_2
    return-void
.end method


# virtual methods
.method DrawCaret(Landroid/graphics/Canvas;Lcom/infraware/office/baseframe/EvBaseView;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "ebv"    # Lcom/infraware/office/baseframe/EvBaseView;

    .prologue
    .line 443
    return-void
.end method

.method DrawCaretOnNative(Landroid/graphics/Canvas;Lcom/infraware/office/baseframe/EvBaseView;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "ebv"    # Lcom/infraware/office/baseframe/EvBaseView;

    .prologue
    .line 444
    return-void
.end method

.method public Gesturefinalize()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 185
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 186
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 188
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mAdvMotionDetector:Lcom/infraware/office/baseframe/gestureproc/porting/EvAdvanceMotionDetector;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mAdvMotionDetector:Lcom/infraware/office/baseframe/gestureproc/porting/EvAdvanceMotionDetector;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/porting/EvAdvanceMotionDetector;->Motionfinalize()V

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    if-eqz v0, :cond_1

    .line 192
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->AdvanceGesturefinalize()V

    .line 194
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfo:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;

    if-eqz v0, :cond_2

    .line 195
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfo:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->PageInfofinalize()V

    .line 197
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mTransformInfo:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;

    if-eqz v0, :cond_3

    .line 198
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mTransformInfo:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->TransformInfofinalize()V

    .line 203
    :cond_3
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    if-eqz v0, :cond_4

    .line 204
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->EvObjectProcfinalize()V

    .line 205
    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    .line 208
    :cond_4
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    if-eqz v0, :cond_5

    .line 210
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->finalizePopupMenuWindow()V

    .line 211
    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    .line 214
    :cond_5
    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mAdvMotionDetector:Lcom/infraware/office/baseframe/gestureproc/porting/EvAdvanceMotionDetector;

    .line 215
    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    .line 216
    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 217
    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    .line 218
    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mView:Landroid/view/View;

    .line 220
    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfo:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;

    .line 221
    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mTransformInfo:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;

    .line 223
    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 225
    return-void
.end method

.method public GetMultiInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;
    .locals 1

    .prologue
    .line 449
    const/4 v0, 0x0

    return-object v0
.end method

.method public GetObjCtrlSelIndex()I
    .locals 1

    .prologue
    .line 452
    const/4 v0, 0x0

    return v0
.end method

.method public GetObjCtrlSize()Landroid/graphics/Point;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 451
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    return-object v0
.end method

.method public GetObjCtrlType()I
    .locals 1

    .prologue
    .line 447
    const/4 v0, 0x0

    return v0
.end method

.method public GetObjectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-result-object v0

    return-object v0
.end method

.method public GetRectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;
    .locals 1

    .prologue
    .line 448
    const/4 v0, 0x0

    return-object v0
.end method

.method public OnLoadComplete()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 439
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0x2c

    const/4 v6, 0x0

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    .line 440
    return-void
.end method

.method public OnNewDocument()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 431
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0x8

    const/4 v6, 0x0

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    .line 432
    return-void
.end method

.method public OnObjectPoints(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V
    .locals 0
    .param p1, "param"    # Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;

    .prologue
    .line 446
    return-void
.end method

.method public OnSheetAutoFilterCellPos([I)V
    .locals 0
    .param p1, "nCellPos"    # [I

    .prologue
    .line 453
    return-void
.end method

.method public OnSheetFormulaRangeRect(I[I[I)V
    .locals 0
    .param p1, "a_nCount"    # I
    .param p2, "a_aRangeRect"    # [I
    .param p3, "a_aCurRect"    # [I

    .prologue
    .line 454
    return-void
.end method

.method public OnSurfaceChanged(IIII)V
    .locals 8
    .param p1, "oldWidth"    # I
    .param p2, "oldHeight"    # I
    .param p3, "newWidth"    # I
    .param p4, "newHeight"    # I

    .prologue
    .line 406
    const/4 v7, 0x0

    .line 407
    .local v7, "bLandScape":I
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 408
    const/4 v7, 0x1

    .line 411
    :cond_0
    const/4 v0, 0x1

    if-le p4, v0, :cond_4

    .line 412
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getActionMode()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 413
    const/4 v0, 0x1

    if-ne v7, v0, :cond_1

    if-ge p3, p4, :cond_1

    .line 414
    const/4 v7, 0x0

    .line 415
    :cond_1
    if-nez v7, :cond_2

    if-le p3, p4, :cond_2

    .line 416
    const/4 v7, 0x1

    .line 418
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0xc

    const/4 v6, 0x0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    .line 420
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mActionMode:I

    const/16 v1, 0xc

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mTouchStatus:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->isPenDrawing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 421
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->getLastMotionX()F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->getLastMotionY()F

    move-result v3

    float-to-int v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 422
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mTouchStatus:I

    .line 423
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->cancel()V

    .line 425
    :cond_3
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v7, p3, p4}, Lcom/infraware/office/evengine/EvInterface;->IChangeScreen(III)V

    .line 427
    :cond_4
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/4 v1, 0x5

    const/4 v6, 0x0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    .line 428
    return-void
.end method

.method public OnTemplateDocument()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 435
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0xa

    const/4 v6, 0x0

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    .line 436
    return-void
.end method

.method public beginBatchEdit()Z
    .locals 1

    .prologue
    .line 495
    const/4 v0, 0x0

    return v0
.end method

.method public cancelGesture()V
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->cancel()V

    .line 357
    :cond_0
    return-void
.end method

.method public dismissPageInfoTimer(I)V
    .locals 7
    .param p1, "nCallBackID"    # I

    .prologue
    .line 602
    invoke-virtual {p0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getPageInfoType(I)I

    move-result v6

    .line 603
    .local v6, "infoId":I
    if-nez v6, :cond_1

    .line 605
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 606
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 607
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    .line 610
    :cond_0
    invoke-virtual {p0, v6}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->setPageInfoType(I)V

    .line 611
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    if-nez v0, :cond_1

    .line 612
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    .line 613
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    new-instance v1, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v1, p0, v2, v3}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;-><init>(Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;J)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x32

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 617
    :cond_1
    return-void
.end method

.method protected drawScrollBar(Landroid/graphics/Canvas;Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "hor"    # Landroid/graphics/RectF;
    .param p3, "ver"    # Landroid/graphics/RectF;

    .prologue
    const/high16 v2, 0x40a00000    # 5.0f

    .line 243
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 244
    .local v0, "paint":Landroid/graphics/Paint;
    const v1, -0xbbbbbc

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 249
    invoke-virtual {p1, p2, v2, v2, v0}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 250
    invoke-virtual {p1, p3, v2, v2, v0}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 251
    return-void
.end method

.method public endBatchEdit()Z
    .locals 1

    .prologue
    .line 496
    const/4 v0, 0x0

    return v0
.end method

.method public finishComposingText()Z
    .locals 1

    .prologue
    .line 491
    const/4 v0, 0x0

    return v0
.end method

.method public getActionMode()I
    .locals 1

    .prologue
    .line 587
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mActionMode:I

    return v0
.end method

.method public getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->EV()Lcom/infraware/office/evengine/EV;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    return-object v0
.end method

.method public getLastMotionPoint()Landroid/graphics/PointF;
    .locals 2

    .prologue
    .line 392
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 393
    .local v0, "pointF":Landroid/graphics/PointF;
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->getLastMotionX()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 394
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->getLastMotionY()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 396
    return-object v0
.end method

.method public getObjectProc()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    return-object v0
.end method

.method protected getPageInfoType(I)I
    .locals 1
    .param p1, "nCallBackID"    # I

    .prologue
    .line 533
    const/4 v0, 0x0

    .line 535
    .local v0, "infoId":I
    sparse-switch p1, :sswitch_data_0

    .line 558
    :goto_0
    return v0

    .line 547
    :sswitch_0
    const/4 v0, 0x1

    .line 548
    goto :goto_0

    .line 554
    :sswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 535
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x5 -> :sswitch_0
        0x6 -> :sswitch_0
        0x7 -> :sswitch_0
        0x8 -> :sswitch_0
        0x9 -> :sswitch_0
        0xa -> :sswitch_0
        0xb -> :sswitch_0
        0xe -> :sswitch_1
        0x10 -> :sswitch_1
        0x11 -> :sswitch_1
        0x12 -> :sswitch_1
        0x13 -> :sswitch_1
        0x1b -> :sswitch_0
        0x3d -> :sswitch_0
        0x11c -> :sswitch_0
    .end sparse-switch
.end method

.method protected abstract getScrollBarRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
.end method

.method protected final getScrollBarRect(Landroid/graphics/RectF;Landroid/graphics/RectF;FFFF)V
    .locals 16
    .param p1, "h"    # Landroid/graphics/RectF;
    .param p2, "v"    # Landroid/graphics/RectF;
    .param p3, "x"    # F
    .param p4, "y"    # F
    .param p5, "docWidth"    # F
    .param p6, "docHeight"    # F

    .prologue
    .line 254
    new-instance v5, Landroid/graphics/RectF;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getView()Landroid/view/View;

    move-result-object v13

    invoke-virtual {v13}, Landroid/view/View;->getWidth()I

    move-result v13

    int-to-float v13, v13

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getView()Landroid/view/View;

    move-result-object v14

    invoke-virtual {v14}, Landroid/view/View;->getHeight()I

    move-result v14

    int-to-float v14, v14

    invoke-direct {v5, v11, v12, v13, v14}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 256
    .local v5, "screenRect":Landroid/graphics/RectF;
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v11

    invoke-virtual {v11}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v4

    .line 257
    .local v4, "info":Lcom/infraware/office/evengine/EV$CONFIG_INFO;
    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v11

    cmpl-float v11, p5, v11

    if-lez v11, :cond_4

    iget v11, v4, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nReflowState:I

    if-nez v11, :cond_4

    .line 258
    new-instance v6, Landroid/graphics/RectF;

    const/high16 v11, 0x40800000    # 4.0f

    const/high16 v12, 0x40800000    # 4.0f

    iget v13, v5, Landroid/graphics/RectF;->right:F

    const/high16 v14, 0x40800000    # 4.0f

    sub-float/2addr v13, v14

    const/high16 v14, 0x41200000    # 10.0f

    sub-float/2addr v13, v14

    iget v14, v5, Landroid/graphics/RectF;->bottom:F

    const/high16 v15, 0x40800000    # 4.0f

    sub-float/2addr v14, v15

    invoke-direct {v6, v11, v12, v13, v14}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 263
    .local v6, "scrollBarHOutline":Landroid/graphics/RectF;
    iget v11, v6, Landroid/graphics/RectF;->left:F

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v12

    mul-float v12, v12, p3

    div-float v12, v12, p5

    add-float v1, v11, v12

    .line 264
    .local v1, "horizentalSx":F
    iget v11, v6, Landroid/graphics/RectF;->bottom:F

    const/high16 v12, 0x41200000    # 10.0f

    sub-float v2, v11, v12

    .line 265
    .local v2, "horizentalSy":F
    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v11

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v12

    mul-float/2addr v11, v12

    div-float v3, v11, p5

    .line 267
    .local v3, "horizentalWidth":F
    const/high16 v11, 0x41f00000    # 30.0f

    cmpg-float v11, v3, v11

    if-gez v11, :cond_0

    .line 268
    const/high16 v3, 0x41f00000    # 30.0f

    .line 270
    :cond_0
    const/high16 v11, 0x41f00000    # 30.0f

    add-float/2addr v11, v1

    iget v12, v5, Landroid/graphics/RectF;->right:F

    cmpl-float v11, v11, v12

    if-lez v11, :cond_1

    .line 271
    iget v11, v6, Landroid/graphics/RectF;->right:F

    const/high16 v12, 0x41f00000    # 30.0f

    sub-float v1, v11, v12

    .line 273
    :cond_1
    add-float v11, v1, v3

    const/high16 v12, 0x41200000    # 10.0f

    add-float/2addr v12, v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v11, v12}, Landroid/graphics/RectF;->set(FFFF)V

    .line 278
    .end local v1    # "horizentalSx":F
    .end local v2    # "horizentalSy":F
    .end local v3    # "horizentalWidth":F
    .end local v6    # "scrollBarHOutline":Landroid/graphics/RectF;
    :goto_0
    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v11

    cmpl-float v11, p6, v11

    if-lez v11, :cond_5

    .line 279
    new-instance v7, Landroid/graphics/RectF;

    const/high16 v11, 0x40800000    # 4.0f

    const/high16 v12, 0x40800000    # 4.0f

    iget v13, v5, Landroid/graphics/RectF;->right:F

    const/high16 v14, 0x40800000    # 4.0f

    sub-float/2addr v13, v14

    iget v14, v5, Landroid/graphics/RectF;->bottom:F

    const/high16 v15, 0x40800000    # 4.0f

    sub-float/2addr v14, v15

    const/high16 v15, 0x41200000    # 10.0f

    sub-float/2addr v14, v15

    invoke-direct {v7, v11, v12, v13, v14}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 284
    .local v7, "scrollBarVOutline":Landroid/graphics/RectF;
    iget v11, v7, Landroid/graphics/RectF;->right:F

    const/high16 v12, 0x41200000    # 10.0f

    sub-float v9, v11, v12

    .line 285
    .local v9, "verticalSx":F
    iget v11, v7, Landroid/graphics/RectF;->top:F

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v12

    mul-float v12, v12, p4

    div-float v12, v12, p6

    add-float v10, v11, v12

    .line 286
    .local v10, "verticalSy":F
    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v11

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v12

    mul-float/2addr v11, v12

    div-float v8, v11, p6

    .line 288
    .local v8, "verticalHeight":F
    const/high16 v11, 0x41f00000    # 30.0f

    cmpg-float v11, v8, v11

    if-gez v11, :cond_2

    .line 289
    const/high16 v8, 0x41f00000    # 30.0f

    .line 291
    :cond_2
    const/high16 v11, 0x41f00000    # 30.0f

    add-float/2addr v11, v10

    iget v12, v7, Landroid/graphics/RectF;->bottom:F

    cmpl-float v11, v11, v12

    if-lez v11, :cond_3

    .line 292
    iget v11, v7, Landroid/graphics/RectF;->bottom:F

    const/high16 v12, 0x41f00000    # 30.0f

    sub-float v10, v11, v12

    .line 294
    :cond_3
    const/high16 v11, 0x41200000    # 10.0f

    add-float/2addr v11, v9

    add-float v12, v10, v8

    move-object/from16 v0, p2

    invoke-virtual {v0, v9, v10, v11, v12}, Landroid/graphics/RectF;->set(FFFF)V

    .line 298
    .end local v7    # "scrollBarVOutline":Landroid/graphics/RectF;
    .end local v8    # "verticalHeight":F
    .end local v9    # "verticalSx":F
    .end local v10    # "verticalSy":F
    :goto_1
    return-void

    .line 276
    :cond_4
    const/high16 v11, -0x40800000    # -1.0f

    const/high16 v12, -0x40800000    # -1.0f

    const/high16 v13, -0x40800000    # -1.0f

    const/high16 v14, -0x40800000    # -1.0f

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12, v13, v14}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0

    .line 297
    :cond_5
    const/high16 v11, -0x40800000    # -1.0f

    const/high16 v12, -0x40800000    # -1.0f

    const/high16 v13, -0x40800000    # -1.0f

    const/high16 v14, -0x40800000    # -1.0f

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12, v13, v14}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_1
.end method

.method public getTouchStatus()I
    .locals 1

    .prologue
    .line 178
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mTouchStatus:I

    return v0
.end method

.method protected getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mView:Landroid/view/View;

    return-object v0
.end method

.method public getXforPopup()I
    .locals 1

    .prologue
    .line 1162
    const/4 v0, 0x0

    return v0
.end method

.method public getYforPopup()I
    .locals 1

    .prologue
    .line 1163
    const/4 v0, 0x0

    return v0
.end method

.method public killPageInfoTimer()V
    .locals 1

    .prologue
    .line 526
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 527
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 528
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    .line 530
    :cond_0
    return-void
.end method

.method protected midPoint(Landroid/view/MotionEvent;)Landroid/graphics/PointF;
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    .line 508
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    add-float v0, v2, v3

    .line 509
    .local v0, "x":F
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    add-float v1, v2, v3

    .line 510
    .local v1, "y":F
    new-instance v2, Landroid/graphics/PointF;

    div-float v3, v0, v4

    div-float v4, v1, v4

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v2
.end method

.method protected minMax(III)I
    .locals 0
    .param p1, "value"    # I
    .param p2, "min"    # I
    .param p3, "max"    # I

    .prologue
    .line 514
    if-ge p1, p2, :cond_0

    .line 519
    .end local p2    # "min":I
    :goto_0
    return p2

    .line 516
    .restart local p2    # "min":I
    :cond_0
    if-le p1, p3, :cond_1

    move p2, p3

    .line 517
    goto :goto_0

    :cond_1
    move p2, p1

    .line 519
    goto :goto_0
.end method

.method public onBackPressed()Z
    .locals 1

    .prologue
    .line 360
    const/4 v0, 0x0

    return v0
.end method

.method public abstract onCancel(Landroid/view/MotionEvent;)V
.end method

.method public abstract onDoubleTap(Landroid/view/MotionEvent;)Z
.end method

.method public abstract onDoubleTapConfirmed(Landroid/view/MotionEvent;)Z
.end method

.method public onDraw(Landroid/graphics/Canvas;Lcom/infraware/office/baseframe/EvBaseView;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "ebv"    # Lcom/infraware/office/baseframe/EvBaseView;

    .prologue
    const/4 v4, 0x2

    .line 304
    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfo:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 305
    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfo:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;

    iget-object v3, v3, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 307
    const/4 v0, 0x0

    .line 308
    .local v0, "bQuickShown":Z
    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getQuickScroll()Lcom/infraware/polarisoffice5/common/QuickScrollView;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 310
    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getQuickScroll()Lcom/infraware/polarisoffice5/common/QuickScrollView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->isShown()Z

    move-result v0

    .line 313
    :cond_0
    iget-boolean v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mbScrollbar:Z

    if-eqz v3, :cond_1

    if-nez v0, :cond_1

    .line 314
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 315
    .local v1, "hor":Landroid/graphics/RectF;
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 316
    .local v2, "ver":Landroid/graphics/RectF;
    invoke-virtual {p0, v1, v2}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getScrollBarRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 317
    invoke-virtual {p0, p1, v1, v2}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->drawScrollBar(Landroid/graphics/Canvas;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 319
    .end local v1    # "hor":Landroid/graphics/RectF;
    .end local v2    # "ver":Landroid/graphics/RectF;
    :cond_1
    return-void
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v5, 0x1

    const/high16 v4, 0x41b00000    # 22.0f

    const/high16 v3, -0x40800000    # -1.0f

    .line 341
    const/4 v2, 0x4

    iput v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mTouchStatus:I

    .line 343
    mul-float/2addr p3, v3

    .line 344
    mul-float/2addr p4, v3

    .line 346
    div-float v2, p3, v4

    float-to-int v0, v2

    .line 347
    .local v0, "x":I
    div-float v2, p4, v4

    float-to-int v1, v2

    .line 349
    .local v1, "y":I
    iget-boolean v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mbFlickEnable:Z

    if-ne v2, v5, :cond_0

    .line 350
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2, v0, v1}, Lcom/infraware/office/evengine/EvInterface;->IFlick(II)V

    .line 352
    :cond_0
    return v5
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 10
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, -0x2

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 461
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    .line 463
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 465
    packed-switch p1, :pswitch_data_0

    .line 473
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 487
    :cond_1
    :goto_0
    return v1

    .line 468
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/infraware/office/actionbar/MainActionBar;->setFocus(Z)V

    move v1, v9

    .line 469
    goto :goto_0

    .line 475
    :sswitch_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move v3, v1

    move v4, v1

    move v5, v1

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvInterface;->IScroll(IIIII)V

    move v1, v9

    .line 476
    goto :goto_0

    .line 479
    :sswitch_1
    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move v4, v9

    move v5, v2

    move v6, v1

    move v7, v1

    move v8, v1

    invoke-virtual/range {v3 .. v8}, Lcom/infraware/office/evengine/EvInterface;->IScroll(IIIII)V

    move v1, v9

    .line 480
    goto :goto_0

    .line 483
    :sswitch_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/infraware/office/actionbar/MainActionBar;->setFocus(Z)V

    move v1, v9

    .line 484
    goto :goto_0

    .line 465
    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
    .end packed-switch

    .line 473
    :sswitch_data_0
    .sparse-switch
        0x39 -> :sswitch_2
        0x3a -> :sswitch_2
        0x5c -> :sswitch_0
        0x5d -> :sswitch_1
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 489
    const/4 v0, 0x0

    return v0
.end method

.method public abstract onLongPress(Landroid/view/MotionEvent;)V
.end method

.method public onMotionPanningGyro(II)V
    .locals 0
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    .line 367
    return-void
.end method

.method public onMotionTilt(I)V
    .locals 17
    .param p1, "tilt"    # I

    .prologue
    .line 369
    move-object/from16 v0, p0

    iget v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mTouchStatus:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 370
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    .line 371
    .local v13, "gyroCurrentTime":J
    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mGyroPreviousTime:J

    sub-long v1, v13, v1

    const-wide/16 v4, 0xf

    cmp-long v1, v1, v4

    if-gez v1, :cond_1

    .line 389
    .end local v13    # "gyroCurrentTime":J
    :cond_0
    :goto_0
    return-void

    .line 373
    .restart local v13    # "gyroCurrentTime":J
    :cond_1
    invoke-static/range {p1 .. p1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const/4 v2, 0x5

    if-le v1, v2, :cond_0

    .line 374
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v15

    .line 375
    .local v15, "info":Lcom/infraware/office/evengine/EV$CONFIG_INFO;
    move/from16 v0, p1

    int-to-float v1, v0

    const v2, 0x3fb66666    # 1.425f

    mul-float/2addr v1, v2

    float-to-int v0, v1

    move/from16 v16, v0

    .line 376
    .local v16, "newTilt":I
    iget v1, v15, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    add-int v3, v1, v16

    .line 377
    .local v3, "nScale":I
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v1

    iget v1, v1, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nMinZoom:I

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v2

    iget v2, v2, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nMaxZoom:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v1, v2}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->minMax(III)I

    move-result v3

    .line 378
    const-string/jumbo v1, "EvBaseGestureProc"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "tilt = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, " nScale = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v1

    iget v1, v1, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    if-eq v1, v3, :cond_2

    .line 381
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x1

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mScaleTouchPreCenter:Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->x:F

    float-to-int v11, v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mScaleTouchPreCenter:Landroid/graphics/PointF;

    iget v12, v12, Landroid/graphics/PointF;->y:F

    float-to-int v12, v12

    invoke-virtual/range {v1 .. v12}, Lcom/infraware/office/evengine/EvInterface;->ISetZoom(IIIIIIIIIII)V

    .line 383
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mGyroPreviousTime:J

    .line 385
    move-object/from16 v0, p0

    iget v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mScaleTouchPreSpace:F

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mScaleTouchBeginSpace:F

    .line 386
    move-object/from16 v0, p0

    iput v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mScaleTouchBeginScale:I

    goto/16 :goto_0
.end method

.method public onMotionTwoTapping()V
    .locals 0

    .prologue
    .line 366
    return-void
.end method

.method public onShowIme(Z)Z
    .locals 1
    .param p1, "bShow"    # Z

    .prologue
    .line 492
    const/4 v0, 0x1

    return v0
.end method

.method public abstract onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
.end method

.method public onTextChanged(ZLjava/lang/CharSequence;III)V
    .locals 0
    .param p1, "bComposing"    # Z
    .param p2, "text"    # Ljava/lang/CharSequence;
    .param p3, "start"    # I
    .param p4, "before"    # I
    .param p5, "after"    # I

    .prologue
    .line 459
    return-void
.end method

.method public onTouchDown(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 325
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getActionMode()I

    move-result v0

    if-ne v0, v7, :cond_0

    .line 326
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/ImmManager;->hideDisposableIme(Landroid/app/Activity;)V

    .line 328
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/4 v1, 0x3

    const/4 v6, 0x0

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    return v7
.end method

.method public abstract onTouchDrag(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
.end method

.method public abstract onTouchUp(Landroid/view/MotionEvent;)Z
.end method

.method public onUpdateSelection(I)V
    .locals 0
    .param p1, "nPos"    # I

    .prologue
    .line 494
    return-void
.end method

.method public setActionMode(I)V
    .locals 4
    .param p1, "nMode"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 566
    iput p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mActionMode:I

    .line 567
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    invoke-virtual {v0, v3}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->SetPenMode(Z)V

    .line 569
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mbFlickEnable:Z

    .line 570
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mbScaleTouchEnable:Z

    .line 572
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mActionMode:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 573
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    invoke-virtual {v0, v2}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->SetAlwaysInMoveRegion(Z)V

    .line 584
    :goto_0
    return-void

    .line 574
    :cond_0
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mActionMode:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_1

    .line 575
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    invoke-virtual {v0, v2}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->SetAlwaysInMoveRegion(Z)V

    goto :goto_0

    .line 576
    :cond_1
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mActionMode:I

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mActionMode:I

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mActionMode:I

    const/16 v1, 0xe

    if-ne v0, v1, :cond_3

    .line 577
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    invoke-virtual {v0, v2}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->SetPenMode(Z)V

    goto :goto_0

    .line 578
    :cond_3
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mActionMode:I

    const/16 v1, 0xf

    if-ne v0, v1, :cond_4

    .line 579
    iput-boolean v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mbFlickEnable:Z

    .line 580
    iput-boolean v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mbScaleTouchEnable:Z

    goto :goto_0

    .line 583
    :cond_4
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    invoke-virtual {v0, v3}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->SetAlwaysInMoveRegion(Z)V

    goto :goto_0
.end method

.method public setEnableGesture(Z)V
    .locals 2
    .param p1, "isEnable"    # Z

    .prologue
    .line 595
    if-eqz p1, :cond_0

    .line 596
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mView:Landroid/view/View;

    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 599
    :goto_0
    return-void

    .line 598
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0
.end method

.method public setPageInfoText()V
    .locals 1

    .prologue
    .line 645
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfo:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;

    if-eqz v0, :cond_0

    .line 646
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfo:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;

    # invokes: Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->setText()V
    invoke-static {v0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->access$000(Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;)V

    .line 647
    :cond_0
    return-void
.end method

.method public setPageInfoTimer(I)V
    .locals 7
    .param p1, "nCallBackID"    # I

    .prologue
    .line 620
    invoke-virtual {p0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getPageInfoType(I)I

    move-result v6

    .line 622
    .local v6, "infoId":I
    if-eqz v6, :cond_1

    .line 624
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 625
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 626
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    .line 629
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mbScrollbar:Z

    .line 631
    invoke-virtual {p0, v6}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->setPageInfoType(I)V

    .line 632
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    if-nez v0, :cond_1

    .line 634
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    .line 636
    :try_start_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    new-instance v1, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v1, p0, v2, v3}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;-><init>(Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;J)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x32

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 642
    :cond_1
    :goto_0
    return-void

    .line 638
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected setPageInfoType(I)V
    .locals 2
    .param p1, "EDVA_PAGE_INFO_TYPE"    # I

    .prologue
    .line 228
    iput p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoType:I

    .line 231
    :try_start_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfo:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 232
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfo:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 233
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoType:I

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfo:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 240
    :goto_0
    return-void

    .line 236
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfo:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 237
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setPointerView(Lcom/infraware/polarisoffice5/common/PointerDrawView;)V
    .locals 0
    .param p1, "pointerView"    # Lcom/infraware/polarisoffice5/common/PointerDrawView;

    .prologue
    .line 591
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPointerView:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    .line 592
    return-void
.end method

.method public setPrevText(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 493
    return-void
.end method

.method protected spacing(Landroid/view/MotionEvent;)F
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 502
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    sub-float v0, v2, v3

    .line 503
    .local v0, "x":F
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    sub-float v1, v2, v3

    .line 504
    .local v1, "y":F
    mul-float v2, v0, v0

    mul-float v3, v1, v1

    add-float/2addr v2, v3

    invoke-static {v2}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v2

    return v2
.end method

.method public updateCaretPos(ZZ)V
    .locals 0
    .param p1, "bSoftCommit"    # Z
    .param p2, "bHardCommit"    # Z

    .prologue
    .line 490
    return-void
.end method
