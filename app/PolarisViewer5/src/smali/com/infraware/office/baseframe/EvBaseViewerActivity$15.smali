.class Lcom/infraware/office/baseframe/EvBaseViewerActivity$15;
.super Ljava/lang/Object;
.source "EvBaseViewerActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/office/baseframe/EvBaseViewerActivity;->SearchSetPopupWindow(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

.field final synthetic val$SrcListItem:Ljava/util/ArrayList;

.field final synthetic val$adapter:Lcom/infraware/polarisoffice5/common/MultiAdapter;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Ljava/util/ArrayList;Lcom/infraware/polarisoffice5/common/MultiAdapter;)V
    .locals 0

    .prologue
    .line 1158
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$15;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iput-object p2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$15;->val$SrcListItem:Ljava/util/ArrayList;

    iput-object p3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$15;->val$adapter:Lcom/infraware/polarisoffice5/common/MultiAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1161
    const/4 v0, 0x0

    .line 1163
    .local v0, "bChecked":I
    invoke-static {p2}, Lcom/infraware/common/util/Utils;->setCheckBoxAccessibility(Landroid/view/View;)V

    .line 1165
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$15;->val$SrcListItem:Ljava/util/ArrayList;

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/MultiListItem;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/MultiListItem;->getRightBtnState()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 1166
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$15;->val$SrcListItem:Ljava/util/ArrayList;

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/MultiListItem;

    invoke-virtual {v1, v3}, Lcom/infraware/polarisoffice5/common/MultiListItem;->setRightBtnState(I)V

    .line 1167
    const/4 v0, 0x0

    .line 1173
    :goto_0
    if-ne v0, v2, :cond_2

    .line 1174
    if-nez p3, :cond_1

    .line 1175
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$15;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # setter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbMatchCase:I
    invoke-static {v1, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$1402(Lcom/infraware/office/baseframe/EvBaseViewerActivity;I)I

    .line 1184
    :goto_1
    new-instance v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity$15$1;

    invoke-direct {v1, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$15$1;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity$15;)V

    invoke-virtual {p2, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 1190
    return-void

    .line 1169
    :cond_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$15;->val$SrcListItem:Ljava/util/ArrayList;

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/MultiListItem;

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/MultiListItem;->setRightBtnState(I)V

    .line 1170
    const/4 v0, 0x1

    goto :goto_0

    .line 1177
    :cond_1
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$15;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # setter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbMatchWhole:I
    invoke-static {v1, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$1502(Lcom/infraware/office/baseframe/EvBaseViewerActivity;I)I

    goto :goto_1

    .line 1179
    :cond_2
    if-nez p3, :cond_3

    .line 1180
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$15;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # setter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbMatchCase:I
    invoke-static {v1, v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$1402(Lcom/infraware/office/baseframe/EvBaseViewerActivity;I)I

    goto :goto_1

    .line 1182
    :cond_3
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$15;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # setter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbMatchWhole:I
    invoke-static {v1, v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$1502(Lcom/infraware/office/baseframe/EvBaseViewerActivity;I)I

    goto :goto_1
.end method
