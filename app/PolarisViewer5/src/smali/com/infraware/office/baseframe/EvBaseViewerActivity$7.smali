.class Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;
.super Ljava/lang/Object;
.source "EvBaseViewerActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/EvBaseViewerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 0

    .prologue
    .line 654
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 656
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 658
    .local v0, "viewId":I
    sparse-switch v0, :sswitch_data_0

    .line 740
    :cond_0
    :goto_0
    return-void

    .line 661
    :sswitch_0
    const-string/jumbo v1, "EvBaseViewerActivity"

    const-string/jumbo v2, "find_function_button"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 663
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isShown()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 664
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->hidePanel()V

    .line 666
    :cond_1
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/ImmManager;->hideDisposableIme(Landroid/app/Activity;)V

    .line 667
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindSetBtnIsLatestClicked:Z
    invoke-static {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$300(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Z

    move-result v1

    if-ne v1, v3, :cond_2

    .line 669
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # setter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindSetBtnIsLatestClicked:Z
    invoke-static {v1, v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$302(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Z)Z

    goto :goto_0

    .line 673
    :cond_2
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # setter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindSetBtnIsLatestClicked:Z
    invoke-static {v1, v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$302(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Z)Z

    .line 676
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->dismissPopupWindow()Z

    move-result v1

    if-eq v1, v3, :cond_0

    .line 679
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->SearchSetPopupWindow(I)V

    goto :goto_0

    .line 682
    :sswitch_1
    const-string/jumbo v1, "EvBaseViewerActivity"

    const-string/jumbo v2, "find_prev_button"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isShown()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 684
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->hidePanel()V

    .line 685
    :cond_3
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # invokes: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->hideSoftInputInFindMode()V
    invoke-static {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$400(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    .line 687
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$500(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    if-eqz v1, :cond_4

    .line 688
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1, v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->delayFindText(I)V

    goto :goto_0

    .line 690
    :cond_4
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1, v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->FindText(I)V

    goto/16 :goto_0

    .line 693
    :sswitch_2
    const-string/jumbo v1, "EvBaseViewerActivity"

    const-string/jumbo v2, "find_next_button"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isShown()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 695
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->hidePanel()V

    .line 696
    :cond_5
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # invokes: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->hideSoftInputInFindMode()V
    invoke-static {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$400(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    .line 698
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$500(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    if-eqz v1, :cond_6

    .line 699
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1, v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->delayFindText(I)V

    goto/16 :goto_0

    .line 701
    :cond_6
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1, v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->FindText(I)V

    goto/16 :goto_0

    .line 704
    :sswitch_3
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1, v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ReplaceText(I)V

    .line 705
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # invokes: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->hideSoftInputInFindReplaceMode()V
    invoke-static {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$600(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    goto/16 :goto_0

    .line 708
    :sswitch_4
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1, v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ReplaceText(I)V

    .line 709
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # invokes: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->hideSoftInputInFindReplaceMode()V
    invoke-static {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$600(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    goto/16 :goto_0

    .line 712
    :sswitch_5
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ReplaceText(I)V

    .line 713
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # invokes: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->hideSoftInputInFindReplaceMode()V
    invoke-static {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$600(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    goto/16 :goto_0

    .line 716
    :sswitch_6
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindBar:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindBar:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_7

    .line 718
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->initReplaceBar()V

    .line 719
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1, v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ReplaceBarShow(Z)V

    .line 720
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindReplaceText:Ljava/lang/String;
    invoke-static {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$700(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 721
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$800(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/EditText;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindReplaceText:Ljava/lang/String;
    invoke-static {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$700(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 722
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$800(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/EditText;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$800(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelection(I)V

    goto/16 :goto_0

    .line 727
    :cond_7
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBar:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBar:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 729
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->InitFindBar()V

    .line 730
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1, v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->FindBarShow(Z)V

    .line 731
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindReplaceText:Ljava/lang/String;
    invoke-static {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$700(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 732
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$900(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/EditText;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindReplaceText:Ljava/lang/String;
    invoke-static {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$700(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 733
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$900(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/EditText;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$900(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelection(I)V

    goto/16 :goto_0

    .line 658
    :sswitch_data_0
    .sparse-switch
        0x7f0b0064 -> :sswitch_0
        0x7f0b0065 -> :sswitch_2
        0x7f0b0066 -> :sswitch_1
        0x7f0b006a -> :sswitch_0
        0x7f0b006b -> :sswitch_3
        0x7f0b006e -> :sswitch_4
        0x7f0b006f -> :sswitch_5
        0x7f0b01d7 -> :sswitch_6
    .end sparse-switch
.end method
