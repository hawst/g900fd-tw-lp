.class Lcom/infraware/office/baseframe/EvBaseViewerActivity$56;
.super Ljava/lang/Object;
.source "EvBaseViewerActivity.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/EvBaseViewerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 0

    .prologue
    .line 5568
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$56;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "e"    # Landroid/view/DragEvent;

    .prologue
    const/4 v1, 0x0

    .line 5571
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$56;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v2, v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    if-eq p1, v2, :cond_1

    .line 5586
    :cond_0
    :goto_0
    return v1

    .line 5572
    :cond_1
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$56;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMWDnD()Lcom/infraware/common/multiwindow/MWDnDOperator;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 5574
    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v0

    .line 5575
    .local v0, "action":I
    const/4 v1, 0x1

    .line 5576
    .local v1, "result":Z
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 5578
    :pswitch_0
    const/4 v1, 0x0

    .line 5579
    goto :goto_0

    .line 5581
    :pswitch_1
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$56;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMWDnD()Lcom/infraware/common/multiwindow/MWDnDOperator;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lcom/infraware/common/multiwindow/MWDnDOperator;->onDragEnded(Landroid/view/View;Landroid/view/DragEvent;)Z

    move-result v1

    .line 5582
    goto :goto_0

    .line 5576
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
