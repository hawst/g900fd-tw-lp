.class Lcom/infraware/office/baseframe/EvBaseView$Renderer;
.super Ljava/lang/Thread;
.source "EvBaseView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/EvBaseView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Renderer"
.end annotation


# instance fields
.field private _run:Z

.field private _surfaceHolder:Landroid/view/SurfaceHolder;

.field private _view:Lcom/infraware/office/baseframe/EvBaseView;

.field final synthetic this$0:Lcom/infraware/office/baseframe/EvBaseView;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/EvBaseView;Landroid/view/SurfaceHolder;Lcom/infraware/office/baseframe/EvBaseView;)V
    .locals 1
    .param p2, "surfaceHolder"    # Landroid/view/SurfaceHolder;
    .param p3, "view"    # Lcom/infraware/office/baseframe/EvBaseView;

    .prologue
    .line 216
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseView$Renderer;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 214
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseView$Renderer;->_run:Z

    .line 217
    iput-object p2, p0, Lcom/infraware/office/baseframe/EvBaseView$Renderer;->_surfaceHolder:Landroid/view/SurfaceHolder;

    .line 218
    iput-object p3, p0, Lcom/infraware/office/baseframe/EvBaseView$Renderer;->_view:Lcom/infraware/office/baseframe/EvBaseView;

    .line 219
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 228
    :goto_0
    iget-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseView$Renderer;->_run:Z

    if-eqz v2, :cond_2

    .line 229
    const/4 v0, 0x0

    .line 231
    .local v0, "c":Landroid/graphics/Canvas;
    :try_start_0
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView$Renderer;->_surfaceHolder:Landroid/view/SurfaceHolder;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/view/SurfaceHolder;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    move-result-object v0

    .line 232
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseView$Renderer;->_surfaceHolder:Landroid/view/SurfaceHolder;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 233
    :try_start_1
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView$Renderer;->_view:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v2, v0}, Lcom/infraware/office/baseframe/EvBaseView;->onDraw(Landroid/graphics/Canvas;)V

    .line 234
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 236
    if-eqz v0, :cond_0

    .line 237
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView$Renderer;->_surfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v2, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 241
    :cond_0
    const-wide/16 v2, 0xa

    :try_start_2
    invoke-static {v2, v3}, Lcom/infraware/office/baseframe/EvBaseView$Renderer;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 242
    :catch_0
    move-exception v1

    .line 243
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 234
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 236
    :catchall_1
    move-exception v2

    if-eqz v0, :cond_1

    .line 237
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseView$Renderer;->_surfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v3, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    :cond_1
    throw v2

    .line 246
    .end local v0    # "c":Landroid/graphics/Canvas;
    :cond_2
    return-void
.end method

.method public setRunning(Z)V
    .locals 0
    .param p1, "run"    # Z

    .prologue
    .line 222
    iput-boolean p1, p0, Lcom/infraware/office/baseframe/EvBaseView$Renderer;->_run:Z

    .line 223
    return-void
.end method
