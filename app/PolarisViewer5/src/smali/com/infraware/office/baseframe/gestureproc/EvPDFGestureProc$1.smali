.class Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc$1;
.super Ljava/lang/Object;
.source "EvPDFGestureProc.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;)V
    .locals 0

    .prologue
    .line 523
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc$1;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 9
    .param p1, "a_view"    # Landroid/view/View;
    .param p2, "a_keyCode"    # I
    .param p3, "a_keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    .line 527
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v6, 0x1

    .line 528
    .local v6, "isDown":Z
    :goto_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc$1;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc$1;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mView:Landroid/view/View;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v8, 0x1

    .line 529
    .local v8, "isVaildView":Z
    :goto_1
    if-eqz v6, :cond_0

    if-nez v8, :cond_3

    .line 530
    :cond_0
    const/4 v0, 0x0

    .line 564
    :goto_2
    return v0

    .line 527
    .end local v6    # "isDown":Z
    .end local v8    # "isVaildView":Z
    :cond_1
    const/4 v6, 0x0

    goto :goto_0

    .line 528
    .restart local v6    # "isDown":Z
    :cond_2
    const/4 v8, 0x0

    goto :goto_1

    .line 532
    .restart local v8    # "isVaildView":Z
    :cond_3
    packed-switch p2, :pswitch_data_0

    .line 564
    const/4 v0, 0x0

    goto :goto_2

    .line 535
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc$1;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x6

    const/16 v2, 0x28

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc$1;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;

    # getter for: Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mOffset:I
    invoke-static {v4}, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->access$000(Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;)I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvInterface;->IScroll(IIIII)V

    .line 536
    const/4 v0, 0x1

    goto :goto_2

    .line 540
    :pswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getDeviceId()I

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p3}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    const/4 v7, 0x1

    .line 541
    .local v7, "isInnerLongKey":Z
    :goto_3
    const/4 v0, 0x1

    if-ne v7, v0, :cond_5

    .line 542
    const/4 v0, 0x0

    goto :goto_2

    .line 540
    .end local v7    # "isInnerLongKey":Z
    :cond_4
    const/4 v7, 0x0

    goto :goto_3

    .line 545
    .restart local v7    # "isInnerLongKey":Z
    :cond_5
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_6

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc$1;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetScrollInfo_Editor()Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;->nCurPosY:I

    if-gtz v0, :cond_7

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc$1;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetCaretInfo_Editor()Lcom/infraware/office/evengine/EV$CARET_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CARET_INFO;->bCaret:I

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc$1;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_7

    .line 546
    :cond_6
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc$1;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 547
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc$1;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 548
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc$1;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ICancel()V

    .line 549
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc$1;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;

    # getter for: Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->access$100(Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/MainActionBar;->setFocus(Z)V

    .line 550
    const/4 v0, 0x1

    goto/16 :goto_2

    .line 552
    :cond_7
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc$1;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x6

    const/16 v2, 0x28

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc$1;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;

    # getter for: Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mOffset:I
    invoke-static {v4}, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->access$000(Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;)I

    move-result v4

    neg-int v4, v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvInterface;->IScroll(IIIII)V

    .line 553
    const/4 v0, 0x1

    goto/16 :goto_2

    .line 557
    .end local v7    # "isInnerLongKey":Z
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc$1;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x6

    const/16 v2, 0x28

    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc$1;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;

    # getter for: Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mOffset:I
    invoke-static {v3}, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->access$000(Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;)I

    move-result v3

    neg-int v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvInterface;->IScroll(IIIII)V

    .line 558
    const/4 v0, 0x1

    goto/16 :goto_2

    .line 561
    :pswitch_3
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc$1;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x6

    const/16 v2, 0x28

    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc$1;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;

    # getter for: Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mOffset:I
    invoke-static {v3}, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->access$000(Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;)I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvInterface;->IScroll(IIIII)V

    .line 562
    const/4 v0, 0x1

    goto/16 :goto_2

    .line 532
    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
