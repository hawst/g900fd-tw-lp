.class public Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;
.super Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;
.source "EvSheetGestureProc.java"


# instance fields
.field private mSheetActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

.field private mViewerActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

.field private nOldCol1:I

.field private nOldRow1:I

.field private nXforPopup:I

.field private nYforPopup:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "listener"    # Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 23
    invoke-direct {p0, p1, p2, p3}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;)V

    .line 16
    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mSheetActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .line 17
    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mViewerActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 18
    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->nOldRow1:I

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->nOldCol1:I

    .line 19
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->nXforPopup:I

    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->nYforPopup:I

    .line 25
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->setIsLongpressEnabled(Z)V

    move-object v0, p1

    .line 26
    check-cast v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mViewerActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 27
    check-cast p1, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mSheetActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .line 28
    return-void
.end method


# virtual methods
.method protected getPageInfoType(I)I
    .locals 1
    .param p1, "nCallBackID"    # I

    .prologue
    .line 180
    const/4 v0, 0x0

    .line 182
    .local v0, "infoId":I
    packed-switch p1, :pswitch_data_0

    .line 192
    :goto_0
    :pswitch_0
    return v0

    .line 188
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 182
    nop

    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public getXforPopup()I
    .locals 1

    .prologue
    .line 206
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->nXforPopup:I

    return v0
.end method

.method public getYforPopup()I
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->nYforPopup:I

    return v0
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 41
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mActionMode:I

    const/16 v1, 0xf

    if-ne v0, v1, :cond_0

    .line 42
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 45
    :goto_0
    return v0

    .line 44
    :cond_0
    const/4 v0, 0x6

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mTouchStatus:I

    .line 45
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onDoubleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 50
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mActionMode:I

    const/16 v1, 0xf

    if-ne v0, v1, :cond_0

    .line 52
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onDoubleTapConfirmed(Landroid/view/MotionEvent;)Z

    .line 54
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mSheetActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->onEndInsertFreeformShape()V

    .line 55
    const/4 v0, 0x1

    .line 58
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onDoubleTapConfirmed(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 197
    invoke-super {p0, p1, p2}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 203
    invoke-super {p0, p1, p2}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 7
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, -0x1

    .line 32
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->GetObjCtrlType()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->GetObjCtrlType()I

    move-result v0

    const/16 v1, 0xb

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->GetObjCtrlType()I

    move-result v0

    const/16 v1, 0xc

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->GetObjCtrlType()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->GetObjCtrlSelIndex()I

    move-result v0

    if-ne v0, v6, :cond_1

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/4 v1, 0x4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    long-to-int v4, v4

    invoke-static {p1, v6}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v5

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    .line 37
    :goto_0
    return-void

    .line 36
    :cond_1
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onLongPress(Landroid/view/MotionEvent;)V

    goto :goto_0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v1, 0xf

    const/4 v4, 0x2

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 103
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mSheetActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->isChangeKeypad()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    :goto_0
    return v2

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mViewerActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    if-ne v0, v4, :cond_1

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mSheetActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getActionMode()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v2, v10

    .line 108
    goto :goto_0

    .line 110
    :cond_1
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mActionMode:I

    if-ne v0, v10, :cond_2

    .line 111
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    move-result v2

    goto :goto_0

    .line 113
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mViewerActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mViewerActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mActionMode:I

    const/16 v3, 0xc

    if-eq v0, v3, :cond_6

    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mActionMode:I

    const/16 v3, 0xd

    if-eq v0, v3, :cond_6

    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mActionMode:I

    const/16 v3, 0xe

    if-eq v0, v3, :cond_6

    .line 116
    const/4 v8, 0x0

    .line 117
    .local v8, "bMemo":Z
    const/4 v7, 0x0

    .line 119
    .local v7, "bHyperLINK":Z
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mViewerActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    if-ne v0, v4, :cond_4

    .line 121
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetCommentText()Ljava/lang/String;

    move-result-object v9

    .line 122
    .local v9, "memo_str":Ljava/lang/String;
    if-eqz v9, :cond_3

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 123
    const/4 v8, 0x1

    .line 128
    :cond_3
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mSheetActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getFilterProcessing()Z

    move-result v0

    if-nez v0, :cond_4

    .line 130
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mSheetActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->dismissFilterPopup()V

    .line 132
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mSheetActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetSheetHyperLinkInfo()Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    .line 133
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    iget v0, v0, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;->nType:I

    if-eqz v0, :cond_4

    .line 135
    const/4 v7, 0x1

    .line 149
    .end local v9    # "memo_str":Ljava/lang/String;
    :cond_4
    if-eqz v8, :cond_6

    .line 154
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mSheetActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsSingleCell()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mSheetActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsSingleMergeCell()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mSheetActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getCellInfo()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tSelectedRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v0, v0, Lcom/infraware/office/evengine/EV$RANGE;->nRow1:I

    iget v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->nOldRow1:I

    if-ne v0, v3, :cond_6

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mSheetActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getCellInfo()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tSelectedRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v0, v0, Lcom/infraware/office/evengine/EV$RANGE;->nCol1:I

    iget v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->nOldCol1:I

    if-ne v0, v3, :cond_6

    iget-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->m_bSheetPopupMenu:Z

    if-nez v0, :cond_6

    .line 157
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    .end local v7    # "bHyperLINK":Z
    .end local v8    # "bMemo":Z
    :cond_6
    move v2, v10

    .line 175
    goto/16 :goto_0
.end method

.method public onTouchDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mSheetActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->isChangeKeypad()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const/4 v0, 0x0

    .line 80
    :goto_0
    return v0

    .line 66
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->nXforPopup:I

    .line 67
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->nYforPopup:I

    .line 70
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mViewerActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->isShowMemo()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mViewerActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->HideMemo()V

    .line 74
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mViewerActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->IsEditInTextboxMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 76
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mViewerActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->SheetInputTextboxText()V

    .line 77
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mViewerActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->SheetTextboxHide()V

    .line 80
    :cond_2
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onTouchDown(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTouchUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mSheetActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mSheetActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getCellInfo()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mSheetActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getCellInfo()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tActiveRange:Lcom/infraware/office/evengine/EV$RANGE;

    if-nez v0, :cond_1

    .line 88
    :cond_0
    const/4 v0, 0x1

    .line 98
    :goto_0
    return v0

    .line 92
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mSheetActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getCellInfo()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tActiveRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v0, v0, Lcom/infraware/office/evengine/EV$RANGE;->nRow1:I

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->nOldRow1:I

    .line 93
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->mSheetActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getCellInfo()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tActiveRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v0, v0, Lcom/infraware/office/evengine/EV$RANGE;->nCol1:I

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->nOldCol1:I

    .line 95
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->nXforPopup:I

    .line 96
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;->nYforPopup:I

    .line 98
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onTouchUp(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method
