.class Lcom/infraware/office/baseframe/EvBaseViewerActivity$53;
.super Ljava/lang/Object;
.source "EvBaseViewerActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/EvBaseViewerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 0

    .prologue
    .line 4868
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$53;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 4871
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$53;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v3, v3, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMsgPopup:Landroid/app/AlertDialog;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$53;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v3, v3, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMsgPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$53;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v3, v3, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMsgPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    instance-of v3, v3, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchAdapter;

    if-nez v3, :cond_1

    .line 4888
    :cond_0
    :goto_0
    return-void

    .line 4878
    :cond_1
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$53;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v3, v3, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMsgPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchAdapter;

    .line 4879
    .local v0, "adapter":Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchAdapter;
    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchAdapter;->getItems()Ljava/util/List;

    move-result-object v1

    .line 4881
    .local v1, "alItems":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 4883
    .local v2, "id":I
    if-eqz p1, :cond_2

    .line 4884
    const-string/jumbo v3, "jaesun"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "arg0 : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4887
    :cond_2
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$53;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # invokes: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onClickSearchItem(I)V
    invoke-static {v3, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$3200(Lcom/infraware/office/baseframe/EvBaseViewerActivity;I)V

    goto :goto_0
.end method
