.class public Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;
.super Ljava/lang/Object;
.source "EvObjectProc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "OBJECT_RECT_INFO"
.end annotation


# static fields
.field public static final MAX_ADJUSTHANDLE_ARRSIZE:I = 0xa

.field public static final MAX_SMART_GUIDES:I = 0x6


# instance fields
.field public final AnimtionOrderAdd:I

.field public final AnimtionOrderNomal:I

.field public final PT_BottomSide:I

.field public final PT_LeftBottom:I

.field public final PT_LeftSide:I

.field public final PT_LeftTop:I

.field public final PT_RightBottom:I

.field public final PT_RightSide:I

.field public final PT_RightTop:I

.field public final PT_Rotate:I

.field public final PT_TopSide:I

.field public final adjustImageNormal:I

.field public final adjustImagePress:I

.field public bPureImage:I

.field public bRotationEnabled:I

.field public final edgeImageNormal:I

.field public final edgeImagePress:I

.field public endRangePoint:Landroid/graphics/Point;

.field public iconSize:Landroid/graphics/Point;

.field public mIsMove:Z

.field public final midImageNormal:I

.field public final midImagePress:I

.field public nAdjustHandleCnt:I

.field public nAnimationOrderCnt:I

.field public nSmartGuidesCnt:I

.field public ptAdjustControls:[Landroid/graphics/Point;

.field public ptAnimationOrder:[I

.field public ptControls:[Landroid/graphics/Point;

.field public ptSmartGuidesEnd:[Landroid/graphics/Point;

.field public ptSmartGuidesStart:[Landroid/graphics/Point;

.field public ptSmartGuidesType:[I

.field public rotateAngle:I

.field public final rotateImageNormal:I

.field public final rotateImagePress:I

.field public startRangePoint:Landroid/graphics/Point;

.field final synthetic this$0:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;


# direct methods
.method public constructor <init>(Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;)V
    .locals 6

    .prologue
    const/4 v2, -0x1

    const/16 v5, 0xa

    const/4 v4, 0x6

    const/4 v3, 0x0

    .line 292
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, v3, v3}, Landroid/graphics/Point;-><init>(II)V

    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    .line 245
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, v3, v3}, Landroid/graphics/Point;-><init>(II)V

    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    .line 246
    iput v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    .line 250
    new-array v1, v5, [Landroid/graphics/Point;

    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    .line 252
    iput v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nSmartGuidesCnt:I

    .line 254
    new-array v1, v4, [I

    fill-array-data v1, :array_0

    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesType:[I

    .line 255
    new-array v1, v4, [Landroid/graphics/Point;

    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesStart:[Landroid/graphics/Point;

    .line 256
    new-array v1, v4, [Landroid/graphics/Point;

    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesEnd:[Landroid/graphics/Point;

    .line 258
    iput v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nAdjustHandleCnt:I

    .line 259
    iput v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nAnimationOrderCnt:I

    .line 261
    new-array v1, v5, [Landroid/graphics/Point;

    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    .line 262
    new-array v1, v5, [I

    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAnimationOrder:[I

    .line 263
    const/4 v1, 0x1

    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->PT_LeftTop:I

    .line 264
    const/4 v1, 0x2

    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->PT_RightTop:I

    .line 265
    const/4 v1, 0x3

    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->PT_RightBottom:I

    .line 266
    const/4 v1, 0x4

    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->PT_LeftBottom:I

    .line 267
    const/4 v1, 0x5

    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->PT_TopSide:I

    .line 268
    iput v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->PT_RightSide:I

    .line 269
    const/4 v1, 0x7

    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->PT_BottomSide:I

    .line 270
    const/16 v1, 0x8

    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->PT_LeftSide:I

    .line 271
    const/16 v1, 0x9

    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->PT_Rotate:I

    .line 273
    iput v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->bPureImage:I

    .line 274
    iput v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->bRotationEnabled:I

    .line 275
    iput-boolean v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->mIsMove:Z

    .line 277
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, v3, v3}, Landroid/graphics/Point;-><init>(II)V

    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->iconSize:Landroid/graphics/Point;

    .line 278
    const v1, 0x7f020314

    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->edgeImageNormal:I

    .line 279
    const v1, 0x7f020317

    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->edgeImagePress:I

    .line 280
    const v1, 0x7f020315

    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->midImageNormal:I

    .line 281
    const v1, 0x7f020316

    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->midImagePress:I

    .line 282
    const v1, 0x7f020312

    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateImageNormal:I

    .line 283
    const v1, 0x7f020313

    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateImagePress:I

    .line 284
    const v1, 0x7f02031a

    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->adjustImageNormal:I

    .line 285
    const v1, 0x7f02031b

    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->adjustImagePress:I

    .line 286
    const v1, 0x7f020091

    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->AnimtionOrderNomal:I

    .line 287
    const v1, 0x7f020092

    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->AnimtionOrderAdd:I

    .line 293
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v5, :cond_0

    .line 294
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2, v3, v3}, Landroid/graphics/Point;-><init>(II)V

    aput-object v2, v1, v0

    .line 293
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 296
    :cond_0
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v5, :cond_1

    .line 297
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptAdjustControls:[Landroid/graphics/Point;

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2, v3, v3}, Landroid/graphics/Point;-><init>(II)V

    aput-object v2, v1, v0

    .line 296
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 300
    :cond_1
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v4, :cond_2

    .line 301
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesStart:[Landroid/graphics/Point;

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2, v3, v3}, Landroid/graphics/Point;-><init>(II)V

    aput-object v2, v1, v0

    .line 302
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptSmartGuidesEnd:[Landroid/graphics/Point;

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2, v3, v3}, Landroid/graphics/Point;-><init>(II)V

    aput-object v2, v1, v0

    .line 300
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 304
    :cond_2
    return-void

    .line 254
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method public SetInit()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 307
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    invoke-virtual {v1, v2, v2}, Landroid/graphics/Point;->set(II)V

    .line 308
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    invoke-virtual {v1, v2, v2}, Landroid/graphics/Point;->set(II)V

    .line 310
    iput v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    .line 314
    iput v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->bPureImage:I

    .line 315
    iput v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->bRotationEnabled:I

    .line 316
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->mIsMove:Z

    .line 318
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    .line 319
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->ptControls:[Landroid/graphics/Point;

    aget-object v1, v1, v0

    invoke-virtual {v1, v2, v2}, Landroid/graphics/Point;->set(II)V

    .line 318
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 323
    :cond_0
    iput v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nAnimationOrderCnt:I

    .line 324
    iput v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nAdjustHandleCnt:I

    .line 325
    iput v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->nSmartGuidesCnt:I

    .line 326
    return-void
.end method
