.class Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout$2;
.super Ljava/lang/Object;
.source "EvBaseGestureProc.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->setText()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;)V
    .locals 0

    .prologue
    .line 774
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout$2;->this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 781
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout$2;->this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mPageInfoCurrent:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "%d"

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout$2;->this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;

    # getter for: Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->nCurPage:I
    invoke-static {v4}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->access$200(Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 782
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout$2;->this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mPageInfoTotal:Landroid/widget/TextView;

    const-string/jumbo v1, "%d"

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout$2;->this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;

    # getter for: Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->nTotalPage:I
    invoke-static {v3}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->access$300(Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 783
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout$2;->this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mPageInfoZoom:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 784
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout$2;->this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mPageInfoPage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 785
    return-void
.end method
