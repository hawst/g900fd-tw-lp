.class Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;
.super Landroid/os/Handler;
.source "EvBaseViewerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/EvBaseViewerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 0

    .prologue
    .line 425
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v12, 0xa

    const/4 v11, 0x1

    .line 428
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 429
    .local v0, "bundle":Landroid/os/Bundle;
    iget v9, p1, Landroid/os/Message;->what:I

    sparse-switch v9, :sswitch_data_0

    .line 575
    :cond_0
    :goto_0
    return-void

    .line 431
    :sswitch_0
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # setter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mViewSearchMode:I
    invoke-static {v9, v11}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$002(Lcom/infraware/office/baseframe/EvBaseViewerActivity;I)I

    .line 432
    const-string/jumbo v9, "EvBaseViewerActivity"

    const-string/jumbo v10, "messageHandler CloseDialog"

    invoke-static {v9, v10}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    invoke-static {}, Lcom/infraware/polarisoffice5/common/DlgFactory;->getInstance()Lcom/infraware/polarisoffice5/common/DlgFactory;

    move-result-object v9

    iget-object v10, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v9, v10, v12}, Lcom/infraware/polarisoffice5/common/DlgFactory;->dismisProgress(Landroid/app/Activity;I)V

    .line 437
    sget-object v9, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->searchResultCode:Ljava/lang/String;

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 438
    .local v7, "resultCode":I
    if-ne v7, v11, :cond_1

    .line 439
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v9, v11}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->FindReplaceEnabled(Z)V

    goto :goto_0

    .line 444
    :cond_1
    const/4 v1, 0x0

    .line 446
    .local v1, "content":Ljava/lang/String;
    const/16 v9, 0x33

    if-ne v7, v9, :cond_2

    .line 447
    :try_start_0
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->FindReplaceEnabled(Z)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 530
    :catch_0
    move-exception v2

    .line 531
    .local v2, "e":Landroid/content/res/Resources$NotFoundException;
    const-string/jumbo v9, "EvBaseViewerActivity"

    const-string/jumbo v10, "Hey, check string resources"

    invoke-static {v9, v10}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 451
    .end local v2    # "e":Landroid/content/res/Resources$NotFoundException;
    :cond_2
    :try_start_1
    sget-object v9, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->searchContent:Ljava/lang/String;

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 452
    .local v4, "nTotalCnt":I
    const/16 v9, 0x31

    if-ne v7, v9, :cond_4

    .line 453
    const/16 v9, 0x64

    if-lt v4, v9, :cond_3

    .line 454
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v9}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0701d4

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 455
    .local v5, "replaceAllLimitString":Ljava/lang/String;
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v9}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const/4 v10, 0x0

    invoke-static {v9, v5, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/Toast;->show()V

    .line 461
    .end local v5    # "replaceAllLimitString":Ljava/lang/String;
    :goto_1
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->FindReplaceEnabled(Z)V

    goto :goto_0

    .line 458
    :cond_3
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v9}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f0701d9

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 459
    .local v6, "replaceAllString":Ljava/lang/String;
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v9}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const/4 v10, 0x0

    invoke-static {v9, v6, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 477
    .end local v6    # "replaceAllString":Ljava/lang/String;
    :cond_4
    const/4 v9, 0x2

    if-ne v7, v9, :cond_5

    .line 479
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->FindReplaceEnabled(Z)V

    .line 481
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v9, v9, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v9, :cond_0

    .line 482
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v9, v9, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/infraware/office/evengine/EvInterface;->ISetFindModeChange(I)V

    goto/16 :goto_0

    .line 486
    :cond_5
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v9}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget-object v10, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->searchContent:Ljava/lang/String;

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 487
    .local v8, "searchResultString":Ljava/lang/String;
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v9}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const/4 v10, 0x0

    invoke-static {v9, v8, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/Toast;->show()V

    .line 488
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->FindReplaceEnabled(Z)V
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 535
    .end local v1    # "content":Ljava/lang/String;
    .end local v4    # "nTotalCnt":I
    .end local v7    # "resultCode":I
    .end local v8    # "searchResultString":Ljava/lang/String;
    :sswitch_1
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mViewSearchMode:I
    invoke-static {v9}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$000(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)I

    move-result v9

    const/4 v10, 0x3

    if-ne v9, v10, :cond_0

    .line 536
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # invokes: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->showDialogAfterCheck(I)V
    invoke-static {v9, v12}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$100(Lcom/infraware/office/baseframe/EvBaseViewerActivity;I)V

    goto/16 :goto_0

    .line 539
    :sswitch_2
    invoke-static {}, Lcom/infraware/polarisoffice5/common/DlgFactory;->getInstance()Lcom/infraware/polarisoffice5/common/DlgFactory;

    move-result-object v9

    iget-object v10, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    sget-object v11, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->dialogId:Ljava/lang/String;

    invoke-virtual {v0, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {v9, v10, v11}, Lcom/infraware/polarisoffice5/common/DlgFactory;->dismisProgress(Landroid/app/Activity;I)V

    goto/16 :goto_0

    .line 545
    :sswitch_3
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v9}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->RequestViewerPrint()V

    goto/16 :goto_0

    .line 548
    :sswitch_4
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    .line 549
    .local v3, "key":Ljava/lang/String;
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-static {v9}, Lcom/infraware/common/define/CMModelDefine$Diotek;->USE_DICTIONARY_SEARCH(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 551
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v9, v9, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-nez v9, :cond_7

    .line 552
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    new-instance v10, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-object v11, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-direct {v10, v11}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v10, v9, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    .line 556
    :goto_2
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v9, v9, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v9, :cond_6

    .line 557
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v9, v9, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-object v10, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v10, v10, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v10}, Lcom/infraware/office/actionbar/MainActionBar;->getHeight()I

    move-result v10

    invoke-virtual {v9, v10}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setActionbarHeight(I)V

    .line 559
    :cond_6
    if-eqz v3, :cond_8

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_8

    .line 560
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v9, v9, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v9, v3}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setSearchKeyword(Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 554
    :cond_7
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v9, v9, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v9}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setPanelDefaultSize()V

    goto :goto_2

    .line 562
    :cond_8
    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v9, v9, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setSearchKeyword(Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 429
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0xb -> :sswitch_3
        0x378 -> :sswitch_4
    .end sparse-switch
.end method
