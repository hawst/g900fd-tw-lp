.class public Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;
.super Ljava/lang/Object;
.source "EvObjectProc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "OBJECT_MULTI_INFO"
.end annotation


# instance fields
.field public final MAX_OBJECT_MULTIITEM_INFO:I

.field public eEditing:I

.field public mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

.field public nObjectCount:I

.field final synthetic this$0:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;


# direct methods
.method public constructor <init>(Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/16 v3, 0xa

    .line 342
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 333
    iput v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->MAX_OBJECT_MULTIITEM_INFO:I

    .line 335
    iput v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->nObjectCount:I

    .line 336
    new-array v1, v3, [Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    .line 338
    iput v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->eEditing:I

    .line 343
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 344
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    new-instance v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    invoke-direct {v2, p1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;-><init>(Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;)V

    aput-object v2, v1, v0

    .line 343
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 346
    :cond_0
    return-void
.end method


# virtual methods
.method public SetInit()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 349
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->nObjectCount:I

    .line 350
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->eEditing:I

    .line 352
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    .line 353
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->SetInit()V

    .line 352
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 355
    :cond_0
    return-void
.end method

.method public isGroupEnable()Z
    .locals 2

    .prologue
    .line 372
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->nObjectCount:I

    if-ge v0, v1, :cond_1

    .line 373
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->bGroupEnabled:I

    if-eqz v1, :cond_0

    .line 374
    const/4 v1, 0x1

    .line 376
    :goto_1
    return v1

    .line 372
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 376
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public isRotatable()Z
    .locals 2

    .prologue
    .line 365
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->nObjectCount:I

    if-ge v0, v1, :cond_1

    .line 366
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->bRotationEnabled:I

    if-eqz v1, :cond_0

    .line 367
    const/4 v1, 0x1

    .line 369
    :goto_1
    return v1

    .line 365
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 369
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public setData(I[I)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "pointarry"    # [I

    .prologue
    .line 358
    if-ltz p1, :cond_0

    const/16 v0, 0xa

    if-ge p1, v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->setData([I)V

    .line 361
    :cond_0
    return-void
.end method
