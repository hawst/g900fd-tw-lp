.class Lcom/infraware/office/baseframe/EvBaseViewerActivity$50;
.super Ljava/lang/Object;
.source "EvBaseViewerActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/EvBaseViewerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 0

    .prologue
    .line 4327
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$50;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 4347
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 4343
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 4331
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$50;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$900(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 4333
    .local v0, "strOrgTxt":Ljava/lang/String;
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$50;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->FindReplaceEnabled(Z)V

    .line 4334
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_0

    .line 4335
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$50;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->FindReplaceEnabled(Z)V

    .line 4338
    :cond_0
    return-void
.end method
