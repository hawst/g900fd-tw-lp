.class public interface abstract Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;
.super Ljava/lang/Object;
.source "EvAdvanceGestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnEvGestureListener"
.end annotation


# virtual methods
.method public abstract onCancel(Landroid/view/MotionEvent;)V
.end method

.method public abstract onDoubleTap(Landroid/view/MotionEvent;)Z
.end method

.method public abstract onDoubleTapConfirmed(Landroid/view/MotionEvent;)Z
.end method

.method public abstract onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
.end method

.method public abstract onLongPress(Landroid/view/MotionEvent;)V
.end method

.method public abstract onScale(Landroid/view/ScaleGestureDetector;)Z
.end method

.method public abstract onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
.end method

.method public abstract onScaleEnd(Landroid/view/ScaleGestureDetector;)V
.end method

.method public abstract onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
.end method

.method public abstract onTouchDown(Landroid/view/MotionEvent;)Z
.end method

.method public abstract onTouchDrag(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
.end method

.method public abstract onTouchUp(Landroid/view/MotionEvent;)Z
.end method
