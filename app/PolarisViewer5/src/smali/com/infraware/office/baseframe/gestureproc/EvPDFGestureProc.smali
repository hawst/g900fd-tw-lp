.class public Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;
.super Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;
.source "EvPDFGestureProc.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_PDF_SMOOTH_DEBUG;
.implements Lcom/infraware/office/evengine/E$EV_PEN_MODE;


# instance fields
.field private final LOG_CAT:Ljava/lang/String;

.field private isSelect:Z

.field private lastMotion:Landroid/view/MotionEvent;

.field private mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

.field private mHyperLinkMode:Z

.field private mOffset:I

.field private mScaleDownSpanX:F

.field private mScaleDownSpanY:F

.field private mScaleDownTime:J

.field public m_nXScroll:I

.field public m_nYScroll:I

.field private onKeyListener:Landroid/view/View$OnKeyListener;

.field private onTouchDownX:I

.field private onTouchDownY:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;)V
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "listener"    # Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 57
    invoke-direct {p0, p1, p2, p3}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;)V

    .line 31
    const-string/jumbo v0, "EvPDFGestureProc"

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->LOG_CAT:Ljava/lang/String;

    .line 33
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mHyperLinkMode:Z

    .line 34
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->isSelect:Z

    .line 40
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mScaleDownTime:J

    .line 41
    iput v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mScaleDownSpanX:F

    .line 42
    iput v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mScaleDownSpanY:F

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->lastMotion:Landroid/view/MotionEvent;

    .line 51
    iput v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->onTouchDownX:I

    .line 52
    iput v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->onTouchDownY:I

    .line 53
    iput v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->m_nXScroll:I

    .line 54
    iput v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->m_nYScroll:I

    .line 522
    new-instance v0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc$1;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc$1;-><init>(Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->onKeyListener:Landroid/view/View$OnKeyListener;

    .line 58
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->setIsLongpressEnabled(Z)V

    .line 60
    const-string/jumbo v0, "EvPDFGestureProc"

    const-string/jumbo v1, "EvEditGestureProc(EvGestureCallback listener, View view)"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    check-cast p1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 65
    invoke-direct {p0}, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->getOffset()I

    move-result v0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mOffset:I

    .line 66
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 67
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;

    .prologue
    .line 30
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mOffset:I

    return v0
.end method

.method static synthetic access$100(Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    return-object v0
.end method

.method private getOffset()I
    .locals 2

    .prologue
    .line 569
    const/high16 v0, 0x42200000    # 40.0f

    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-static {v1}, Lcom/infraware/common/util/Utils;->getDensityDpi(Landroid/app/Activity;)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    const/high16 v1, 0x43700000    # 240.0f

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method private onPointerEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 574
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mPointerView:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->onTouch(Landroid/view/MotionEvent;)V

    .line 575
    return-void
.end method


# virtual methods
.method public Gesturefinalize()V
    .locals 0

    .prologue
    .line 75
    invoke-super {p0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->Gesturefinalize()V

    .line 76
    return-void
.end method

.method public GetObjCtrlType()I
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v0

    return v0
.end method

.method public OnObjectPoints(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V
    .locals 1
    .param p1, "param"    # Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;

    .prologue
    .line 395
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->setObjectInfo(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V

    .line 398
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTransformInfo:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;

    if-eqz v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTransformInfo:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;

    invoke-virtual {v0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->setPDFParam(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V

    .line 401
    :cond_0
    return-void
.end method

.method public OnSurfaceChanged(IIII)V
    .locals 8
    .param p1, "oldWidth"    # I
    .param p2, "oldHeight"    # I
    .param p3, "newWidth"    # I
    .param p4, "newHeight"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x2

    .line 120
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTouchStatus:I

    if-ne v0, v1, :cond_0

    .line 121
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->lastMotion:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->lastMotion:Landroid/view/MotionEvent;

    invoke-virtual {v3}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->lastMotion:Landroid/view/MotionEvent;

    invoke-virtual {v4}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    long-to-int v5, v4

    iget-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->lastMotion:Landroid/view/MotionEvent;

    const/4 v6, -0x1

    invoke-static {v4, v6}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    move v4, v1

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 124
    :cond_0
    iput-boolean v7, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->isSelect:Z

    .line 125
    iput v7, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTouchStatus:I

    .line 127
    invoke-super {p0, p1, p2, p3, p4}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->OnSurfaceChanged(IIII)V

    .line 128
    return-void
.end method

.method protected getPageEdgeChecker()Z
    .locals 2

    .prologue
    .line 242
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mView:Landroid/view/View;

    check-cast v1, Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->onScreenOffset()[I

    move-result-object v0

    .line 270
    .local v0, "offset":[I
    const/4 v1, 0x1

    return v1
.end method

.method protected getScrollBarRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 8
    .param p1, "hor"    # Landroid/graphics/RectF;
    .param p2, "ver"    # Landroid/graphics/RectF;

    .prologue
    .line 498
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetScrollInfo_Editor()Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;

    move-result-object v7

    .line 499
    .local v7, "screenInfo":Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;
    iget v0, v7, Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;->nCurPosX:I

    int-to-float v3, v0

    iget v0, v7, Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;->nCurPosY:I

    int-to-float v4, v0

    iget v0, v7, Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;->nWidth:I

    int-to-float v5, v0

    iget v0, v7, Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;->nHeight:I

    int-to-float v6, v0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->getScrollBarRect(Landroid/graphics/RectF;Landroid/graphics/RectF;FFFF)V

    .line 500
    return-void
.end method

.method public onCancel(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 494
    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 370
    const-string/jumbo v0, "EvPDFGestureProc"

    const-string/jumbo v1, "onDoubleTap"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mActionMode:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mActionMode:I

    const/16 v1, 0xf

    if-eq v0, v1, :cond_0

    .line 373
    invoke-virtual {p0, v4}, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->onShowIme(Z)Z

    move v4, v7

    .line 385
    :goto_0
    return v4

    .line 377
    :cond_0
    const-string/jumbo v0, "EvEditGestureProc"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " mEvObjectProc.GetObjCtrlType() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    const-string/jumbo v0, "EvEditGestureProc"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " mEvObjectProc.GetObjInfo () x: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-result-object v2

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->clipEndPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " y: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-result-object v2

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->clipEndPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    const-string/jumbo v0, "EvEditGestureProc"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " mEvObjectProc.GetObjInfo () x: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-result-object v2

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->editEndPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " y: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-result-object v2

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->editEndPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    long-to-int v5, v5

    const/4 v6, -0x1

    invoke-static {p1, v6}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 382
    const/4 v0, 0x6

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTouchStatus:I

    .line 383
    iput-boolean v7, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->isSelect:Z

    goto/16 :goto_0
.end method

.method public onDoubleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x1

    .line 406
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mActionMode:I

    if-ne v0, v1, :cond_1

    .line 408
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 410
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->show()V

    .line 425
    :cond_0
    :goto_0
    return v1

    .line 414
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v0

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mActionMode:I

    if-eq v0, v1, :cond_0

    .line 417
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->sendDictionaryMessage()V

    .line 419
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->getPopupMenu()Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->getPopupMenu()Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 421
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->show()V

    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;Lcom/infraware/office/baseframe/EvBaseView;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "ebv"    # Lcom/infraware/office/baseframe/EvBaseView;

    .prologue
    .line 505
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mActionMode:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 511
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->getPopupMenu()Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->getPopupMenu()Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 513
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->getPopupMore()Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 514
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->show()V

    .line 518
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->drawObjectProc(Landroid/graphics/Canvas;Lcom/infraware/office/baseframe/EvBaseView;)V

    .line 519
    invoke-super {p0, p1, p2}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->onDraw(Landroid/graphics/Canvas;Lcom/infraware/office/baseframe/EvBaseView;)V

    .line 520
    return-void
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 113
    const/4 v0, 0x2

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTouchStatus:I

    .line 114
    const/4 v0, 0x0

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 9
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v1, 0x30

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 675
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    .line 676
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v7

    .line 685
    .local v7, "nMetaState":I
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 686
    sparse-switch p1, :sswitch_data_0

    .line 718
    :cond_0
    sparse-switch p1, :sswitch_data_1

    .line 754
    .end local v7    # "nMetaState":I
    :cond_1
    :goto_0
    return v2

    .line 708
    .restart local v7    # "nMetaState":I
    :sswitch_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    move v3, v2

    move v4, v2

    move v5, v2

    move-object v6, p2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    move v2, v8

    .line 711
    goto :goto_0

    .line 713
    :sswitch_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/infraware/office/actionbar/MainActionBar;->setFocus(Z)V

    move v2, v8

    .line 714
    goto :goto_0

    .line 744
    :sswitch_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/infraware/office/actionbar/MainActionBar;->setFocus(Z)V

    move v2, v8

    .line 745
    goto :goto_0

    .line 748
    :sswitch_3
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    move v3, v2

    move v4, v2

    move v5, v2

    move-object v6, p2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    move v2, v8

    .line 750
    goto :goto_0

    .line 686
    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_1
        0x1d -> :sswitch_0
        0x1f -> :sswitch_0
        0x22 -> :sswitch_0
        0x2c -> :sswitch_0
        0x2f -> :sswitch_0
    .end sparse-switch

    .line 718
    :sswitch_data_1
    .sparse-switch
        0x39 -> :sswitch_2
        0x3a -> :sswitch_2
        0x83 -> :sswitch_3
        0x8e -> :sswitch_3
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 759
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 770
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 9
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 461
    const-string/jumbo v0, "EvPDFGestureProc"

    const-string/jumbo v1, "onDoubleTap"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    const-string/jumbo v0, "EvEditGestureProc"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " mEvObjectProc.GetObjCtrlType() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    const-string/jumbo v0, "EvEditGestureProc"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " mEvObjectProc.GetObjInfo () x: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-result-object v2

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->clipEndPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " y: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-result-object v2

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->clipEndPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    const-string/jumbo v0, "EvEditGestureProc"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " mEvObjectProc.GetObjInfo () x: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-result-object v2

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->editEndPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " y: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-result-object v2

    iget-object v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->editEndPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    invoke-static {}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getInstance()Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0, v1}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->isMultiWindowSupported(Landroid/app/Activity;)Z

    move-result v7

    .line 469
    .local v7, "isMW":Z
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMWDnD()Lcom/infraware/common/multiwindow/MWDnDOperator;

    move-result-object v8

    .line 470
    .local v8, "oMWDnD":Lcom/infraware/common/multiwindow/MWDnDOperator;
    if-eqz v8, :cond_0

    if-eqz v7, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v8, v0, v1}, Lcom/infraware/common/multiwindow/MWDnDOperator;->isTextMarkArea(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 471
    invoke-virtual {v8}, Lcom/infraware/common/multiwindow/MWDnDOperator;->preoccupyOnEditCopy()V

    .line 472
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    .line 473
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v8, v0, v1}, Lcom/infraware/common/multiwindow/MWDnDOperator;->startDragTextOnPDF(Landroid/view/View;Landroid/graphics/Bitmap;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 474
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTouchStatus:I

    .line 488
    :goto_0
    return-void

    .line 480
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    const/4 v4, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    long-to-int v5, v5

    const/4 v6, -0x1

    invoke-static {p1, v6}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 481
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    const/4 v4, 0x2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    long-to-int v5, v5

    const/4 v6, -0x1

    invoke-static {p1, v6}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 482
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    long-to-int v5, v5

    const/4 v6, -0x1

    invoke-static {p1, v6}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 483
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    const/4 v4, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    long-to-int v5, v5

    const/4 v6, -0x1

    invoke-static {p1, v6}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 484
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    const/4 v4, 0x2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    long-to-int v5, v5

    const/4 v6, -0x1

    invoke-static {p1, v6}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 485
    const/4 v0, 0x6

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTouchStatus:I

    .line 486
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->isSelect:Z

    goto/16 :goto_0
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 15
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 579
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTouchStatus:I

    const/16 v1, 0xc

    if-ne v0, v1, :cond_2

    .line 580
    invoke-virtual/range {p1 .. p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v14

    .line 581
    .local v14, "newDist":F
    const/high16 v0, 0x41700000    # 15.0f

    cmpl-float v0, v14, v0

    if-lez v0, :cond_1

    .line 583
    new-instance v12, Landroid/graphics/PointF;

    invoke-virtual/range {p1 .. p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v1

    invoke-direct {v12, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 584
    .local v12, "center":Landroid/graphics/PointF;
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mScaleTouchBeginScale:I

    int-to-float v0, v0

    iget v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mScaleTouchBeginSpace:F

    div-float v1, v14, v1

    mul-float/2addr v0, v1

    float-to-int v2, v0

    .line 585
    .local v2, "nScale":I
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nMinZoom:I

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v1

    iget v1, v1, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nMaxZoom:I

    invoke-virtual {p0, v2, v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->minMax(III)I

    move-result v2

    .line 587
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    if-eq v0, v2, :cond_0

    .line 588
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x1

    iget v10, v12, Landroid/graphics/PointF;->x:F

    float-to-int v10, v10

    iget v11, v12, Landroid/graphics/PointF;->y:F

    float-to-int v11, v11

    invoke-virtual/range {v0 .. v11}, Lcom/infraware/office/evengine/EvInterface;->ISetZoom(IIIIIIIIIII)V

    .line 590
    :cond_0
    const-string/jumbo v0, "EvPDFGestureProc"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onScale nScale = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    iput v14, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mScaleTouchPreSpace:F

    .line 595
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mGyroTiltCurrentValue:I

    .line 597
    .end local v2    # "nScale":I
    .end local v12    # "center":Landroid/graphics/PointF;
    :cond_1
    const/4 v0, 0x1

    .line 621
    .end local v14    # "newDist":F
    :goto_0
    return v0

    .line 598
    :cond_2
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTouchStatus:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_5

    .line 600
    invoke-virtual/range {p1 .. p1}, Landroid/view/ScaleGestureDetector;->getEventTime()J

    move-result-wide v0

    iget-wide v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mScaleDownTime:J

    sub-long/2addr v0, v3

    const-wide/16 v3, 0xc8

    cmp-long v0, v0, v3

    if-lez v0, :cond_3

    .line 602
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mScaleDownSpanX:F

    invoke-virtual/range {p1 .. p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v1

    div-float v13, v0, v1

    .line 603
    .local v13, "gap":F
    float-to-double v0, v13

    const-wide v3, 0x3ff0be0ded288ce7L    # 1.0464

    cmpg-double v0, v0, v3

    if-gez v0, :cond_4

    float-to-double v0, v13

    const-wide v3, 0x3fefeeb702602c91L    # 0.99789

    cmpl-double v0, v0, v3

    if-lez v0, :cond_4

    .line 611
    const/16 v0, 0xa

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTouchStatus:I

    .line 615
    .end local v13    # "gap":F
    :cond_3
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 613
    .restart local v13    # "gap":F
    :cond_4
    const/16 v0, 0xc

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTouchStatus:I

    goto :goto_1

    .line 616
    .end local v13    # "gap":F
    :cond_5
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTouchStatus:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_6

    .line 617
    new-instance v12, Landroid/graphics/PointF;

    invoke-virtual/range {p1 .. p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v1

    invoke-direct {v12, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 618
    .restart local v12    # "center":Landroid/graphics/PointF;
    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v4, 0x1

    iget v0, v12, Landroid/graphics/PointF;->x:F

    float-to-int v5, v0

    iget v0, v12, Landroid/graphics/PointF;->y:F

    float-to-int v6, v0

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/view/ScaleGestureDetector;->getEventTime()J

    move-result-wide v0

    long-to-int v8, v0

    const/4 v9, 0x0

    invoke-virtual/range {v3 .. v9}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 619
    const/4 v0, 0x1

    goto :goto_0

    .line 621
    .end local v12    # "center":Landroid/graphics/PointF;
    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 3
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 627
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTouchStatus:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 628
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mScaleTouchPreCenter:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 629
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mScaleTouchBeginSpace:F

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mScaleTouchPreSpace:F

    .line 630
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mScaleTouchBeginScale:I

    .line 631
    const/16 v0, 0xc

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTouchStatus:I

    .line 642
    const/4 v0, 0x1

    .line 644
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 13
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 651
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mScaleTouchBeginSpace:F

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mScaleTouchPreSpace:F

    .line 652
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mScaleTouchBeginScale:I

    .line 654
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mGyroTiltCurrentValue:I

    .line 655
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mScaleDownSpanX:F

    .line 657
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTouchStatus:I

    const/16 v1, 0xc

    if-ne v0, v1, :cond_1

    .line 658
    const/4 v0, 0x2

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTouchStatus:I

    .line 659
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v2

    iget v2, v2, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x2

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v0 .. v11}, Lcom/infraware/office/evengine/EvInterface;->ISetZoom(IIIIIIIIIII)V

    .line 661
    const/16 v0, 0xd

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTouchStatus:I

    .line 667
    :cond_0
    :goto_0
    const/16 v0, 0xd

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTouchStatus:I

    .line 668
    return-void

    .line 662
    :cond_1
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTouchStatus:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 663
    new-instance v12, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v1

    invoke-direct {v12, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 664
    .local v12, "center":Landroid/graphics/PointF;
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x2

    iget v2, v12, Landroid/graphics/PointF;->x:F

    float-to-int v2, v2

    iget v3, v12, Landroid/graphics/PointF;->y:F

    float-to-int v3, v3

    const/4 v4, 0x2

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getEventTime()J

    move-result-wide v5

    long-to-int v5, v5

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    goto :goto_0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x1

    .line 431
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mActionMode:I

    if-ne v0, v7, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 433
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->show()V

    .line 456
    :goto_0
    return v7

    .line 454
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/4 v1, 0x7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    const/4 v6, 0x0

    move v5, v4

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto :goto_0
.end method

.method public onTouchDown(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    .line 132
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->onTouchDown(Landroid/view/MotionEvent;)Z

    .line 134
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mActionMode:I

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    .line 136
    invoke-direct {p0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->onPointerEvent(Landroid/view/MotionEvent;)V

    .line 152
    :goto_0
    return v4

    .line 140
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->onTouchDownX:I

    .line 141
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->onTouchDownY:I

    .line 143
    const/4 v0, 0x2

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTouchStatus:I

    .line 144
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->checkObjectPoint(IILjava/lang/Boolean;)I

    move-result v0

    if-eq v0, v4, :cond_1

    .line 145
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    long-to-int v5, v5

    const/4 v6, -0x1

    invoke-static {p1, v6}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 151
    :cond_1
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->lastMotion:Landroid/view/MotionEvent;

    goto :goto_0
.end method

.method public onTouchDrag(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 9
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v6, -0x1

    const/4 v8, 0x1

    .line 199
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mActionMode:I

    const/16 v2, 0x10

    if-ne v0, v2, :cond_0

    .line 201
    invoke-direct {p0, p2}, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->onPointerEvent(Landroid/view/MotionEvent;)V

    .line 236
    :goto_0
    return v8

    .line 206
    :cond_0
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->onTouchDownX:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->m_nXScroll:I

    .line 207
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->onTouchDownY:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->m_nYScroll:I

    .line 208
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->getPageEdgeChecker()Z

    move-result v7

    .line 211
    .local v7, "pageEdgeFlag":Z
    const-string/jumbo v0, "EvPDFGestureProc"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onSingleTouchDrag mTouchState ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTouchStatus:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTouchStatus:I

    const/4 v2, 0x5

    if-ne v0, v2, :cond_2

    .line 213
    iput v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTouchStatus:I

    .line 214
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->checkObjectPoint(IILjava/lang/Boolean;)I

    move-result v0

    if-eq v0, v8, :cond_1

    .line 216
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->checkObjectPoint(IILjava/lang/Boolean;)I

    .line 217
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    long-to-int v5, v4

    invoke-static {p2, v6}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    move v4, v1

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 235
    :cond_1
    :goto_1
    iput-object p2, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->lastMotion:Landroid/view/MotionEvent;

    goto :goto_0

    .line 229
    :cond_2
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTouchStatus:I

    if-ne v0, v4, :cond_1

    iget-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->isSelect:Z

    if-nez v0, :cond_1

    .line 230
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v2, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v3, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    long-to-int v5, v4

    invoke-static {p2, v6}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    move v1, v8

    move v4, v8

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    goto :goto_1
.end method

.method public onTouchUp(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 157
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mActionMode:I

    const/16 v2, 0x10

    if-ne v0, v2, :cond_0

    .line 159
    invoke-direct {p0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->onPointerEvent(Landroid/view/MotionEvent;)V

    .line 194
    :goto_0
    return v8

    .line 164
    :cond_0
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->onTouchDownX:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->m_nXScroll:I

    .line 165
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->onTouchDownY:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->m_nYScroll:I

    .line 168
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTouchStatus:I

    if-ne v0, v1, :cond_1

    .line 169
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->checkObjectPoint(IILjava/lang/Boolean;)I

    .line 170
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    long-to-int v5, v4

    const/4 v4, -0x1

    invoke-static {p1, v4}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    move v4, v1

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 180
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->isShowing()Z

    move-result v0

    if-nez v0, :cond_3

    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mActionMode:I

    if-eq v0, v8, :cond_3

    .line 186
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->getPopupMenu()Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->getPopupMenu()Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 188
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->show()V

    .line 192
    :cond_3
    iput-boolean v7, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->isSelect:Z

    .line 193
    iput v7, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mTouchStatus:I

    goto :goto_0
.end method

.method public setPageInfoTimer(I)V
    .locals 7
    .param p1, "nCallBackID"    # I

    .prologue
    const/4 v1, 0x1

    .line 80
    invoke-virtual {p0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->getPageInfoType(I)I

    move-result v6

    .line 82
    .local v6, "infoId":I
    if-eqz v6, :cond_1

    .line 84
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    .line 90
    :cond_0
    if-ne v6, v1, :cond_2

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nTotalPages:I

    if-ne v0, v1, :cond_2

    .line 98
    :goto_0
    invoke-virtual {p0, v6}, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->setPageInfoType(I)V

    .line 99
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    if-nez v0, :cond_1

    .line 101
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    .line 103
    :try_start_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    new-instance v1, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v1, p0, v2, v3}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;-><init>(Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;J)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x32

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    :cond_1
    :goto_1
    return-void

    .line 94
    :cond_2
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;->mbScrollbar:Z

    goto :goto_0

    .line 105
    :catch_0
    move-exception v0

    goto :goto_1
.end method
