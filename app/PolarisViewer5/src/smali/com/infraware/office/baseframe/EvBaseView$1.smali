.class Lcom/infraware/office/baseframe/EvBaseView$1;
.super Landroid/os/Handler;
.source "EvBaseView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/EvBaseView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/baseframe/EvBaseView;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/EvBaseView;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseView$1;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 125
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 137
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 138
    return-void

    .line 127
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView$1;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    iget v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mWidth:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView$1;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    iget v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mHeight:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView$1;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView$1;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    iget v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mWidth:I

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView$1;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView$1;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    iget v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mHeight:I

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView$1;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 132
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView$1;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView$1;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView$1;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    iget v1, v1, Lcom/infraware/office/baseframe/EvBaseView;->mWidth:I

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView$1;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    iget v2, v2, Lcom/infraware/office/baseframe/EvBaseView;->mHeight:I

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->OnSurfaceChanged(IIII)V

    goto :goto_0

    .line 125
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
