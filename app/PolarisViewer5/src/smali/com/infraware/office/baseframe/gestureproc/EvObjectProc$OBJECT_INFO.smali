.class public Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
.super Ljava/lang/Object;
.source "EvObjectProc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "OBJECT_INFO"
.end annotation


# static fields
.field public static final mSelectedImageAdjustHandle0:I = 0xa


# instance fields
.field public bClipEnable:Z

.field public bGroupEnabled:I

.field public clipEndPoint:Landroid/graphics/Point;

.field public clipStartPoint:Landroid/graphics/Point;

.field public eEditing:I

.field public editEndPoint:Landroid/graphics/Point;

.field public editStartPoint:Landroid/graphics/Point;

.field public endRangePoint:Landroid/graphics/Point;

.field public mBaseType:I

.field public mObjectType:I

.field public mSelectedImage:I

.field public sObjectSize:Landroid/graphics/Point;

.field public startRangePoint:Landroid/graphics/Point;

.field final synthetic this$0:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;


# direct methods
.method public constructor <init>(Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 119
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    .line 121
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    .line 123
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    .line 127
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    .line 128
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    .line 129
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->sObjectSize:Landroid/graphics/Point;

    .line 132
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->editStartPoint:Landroid/graphics/Point;

    .line 133
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->editEndPoint:Landroid/graphics/Point;

    .line 135
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->clipStartPoint:Landroid/graphics/Point;

    .line 136
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->clipEndPoint:Landroid/graphics/Point;

    .line 137
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->bClipEnable:Z

    .line 138
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->bGroupEnabled:I

    .line 146
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->eEditing:I

    return-void
.end method


# virtual methods
.method public SetInit()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 148
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    .line 149
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    .line 150
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mSelectedImage:I

    .line 153
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 154
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 155
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->sObjectSize:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 158
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->editStartPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 159
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->editEndPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 161
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->eEditing:I

    .line 162
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->bGroupEnabled:I

    .line 163
    return-void
.end method
