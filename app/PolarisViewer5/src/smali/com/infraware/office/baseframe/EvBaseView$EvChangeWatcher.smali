.class Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;
.super Ljava/lang/Object;
.source "EvBaseView.java"

# interfaces
.implements Landroid/text/SpanWatcher;
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/EvBaseView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EvChangeWatcher"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/baseframe/EvBaseView;


# direct methods
.method private constructor <init>(Lcom/infraware/office/baseframe/EvBaseView;)V
    .locals 0

    .prologue
    .line 1031
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/infraware/office/baseframe/EvBaseView;Lcom/infraware/office/baseframe/EvBaseView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/infraware/office/baseframe/EvBaseView;
    .param p2, "x1"    # Lcom/infraware/office/baseframe/EvBaseView$1;

    .prologue
    .line 1031
    invoke-direct {p0, p1}, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;-><init>(Lcom/infraware/office/baseframe/EvBaseView;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "buffer"    # Landroid/text/Editable;

    .prologue
    .line 1125
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 27
    .param p1, "buffer"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "after"    # I

    .prologue
    .line 1035
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v2

    .line 1036
    .local v2, "evInterface":Lcom/infraware/office/evengine/EvInterface;
    invoke-static/range {p1 .. p1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v26

    .line 1037
    .local v26, "t":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    iget-boolean v3, v3, Lcom/infraware/office/baseframe/EvBaseView;->mbRemoveBColor:Z

    if-eqz v3, :cond_4

    .line 1038
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/infraware/office/baseframe/EvBaseView;->mbRemoveBColor:Z

    .line 1039
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    iget v3, v3, Lcom/infraware/office/baseframe/EvBaseView;->mRemoveBColorE:I

    add-int/lit8 v13, v3, -0x1

    .line 1041
    .local v13, "tt":I
    if-gtz v26, :cond_0

    if-lez v13, :cond_1

    .line 1042
    :cond_0
    const/4 v13, -0x1

    .line 1044
    :cond_1
    if-nez v26, :cond_2

    if-nez v13, :cond_2

    move/from16 v0, p3

    move/from16 v1, p4

    if-le v0, v1, :cond_2

    .line 1045
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/infraware/office/baseframe/EvBaseView;->mbDeletedFirstPos:Z

    .line 1048
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    iget v3, v3, Lcom/infraware/office/baseframe/EvBaseView;->mRemoveBColorS:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    iget v4, v4, Lcom/infraware/office/baseframe/EvBaseView;->mRemoveBColorE:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-wide/16 v7, -0x1

    const-wide/16 v9, -0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v2 .. v13}, Lcom/infraware/office/evengine/EvInterface;->ISetCompBackColor(IIIIJJIII)V

    .line 1070
    .end local v13    # "tt":I
    :cond_3
    :goto_0
    return-void

    .line 1052
    :cond_4
    if-lez v26, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    iget-boolean v3, v3, Lcom/infraware/office/baseframe/EvBaseView;->mbDeletedFirstPos:Z

    if-eqz v3, :cond_5

    .line 1055
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/infraware/office/baseframe/EvBaseView;->mbDeletedFirstPos:Z

    .line 1056
    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const-wide/16 v19, -0x1

    const-wide/16 v21, -0x1

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, -0x1

    move-object v14, v2

    invoke-virtual/range {v14 .. v25}, Lcom/infraware/office/evengine/EvInterface;->ISetCompBackColor(IIIIJJIII)V

    goto :goto_0

    .line 1060
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    iget-boolean v3, v3, Lcom/infraware/office/baseframe/EvBaseView;->mbDeletedFirstPos:Z

    if-nez v3, :cond_3

    .line 1061
    if-nez v26, :cond_6

    if-nez p2, :cond_6

    const/4 v3, 0x1

    move/from16 v0, p3

    if-ne v0, v3, :cond_6

    const/4 v3, 0x1

    move/from16 v0, p4

    if-ne v0, v3, :cond_6

    .line 1062
    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const-wide/16 v19, -0x1

    const-wide/16 v21, -0x1

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    move-object v14, v2

    invoke-virtual/range {v14 .. v25}, Lcom/infraware/office/evengine/EvInterface;->ISetCompBackColor(IIIIJJIII)V

    goto :goto_0

    .line 1064
    :cond_6
    const/4 v3, 0x1

    move/from16 v0, v26

    if-ne v0, v3, :cond_3

    if-nez p2, :cond_3

    const/4 v3, 0x1

    move/from16 v0, p3

    if-ne v0, v3, :cond_3

    const/4 v3, 0x1

    move/from16 v0, p4

    if-ne v0, v3, :cond_3

    .line 1065
    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const-wide/16 v19, -0x1

    const-wide/16 v21, -0x1

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, -0x1

    move-object v14, v2

    invoke-virtual/range {v14 .. v25}, Lcom/infraware/office/evengine/EvInterface;->ISetCompBackColor(IIIIJJIII)V

    goto :goto_0
.end method

.method public onSpanAdded(Landroid/text/Spannable;Ljava/lang/Object;II)V
    .locals 3
    .param p1, "buf"    # Landroid/text/Spannable;
    .param p2, "what"    # Ljava/lang/Object;
    .param p3, "s"    # I
    .param p4, "e"    # I

    .prologue
    .line 1135
    const-string/jumbo v0, "EvBaseView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onSpanAdded s="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " e="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " what="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1137
    return-void
.end method

.method public onSpanChanged(Landroid/text/Spannable;Ljava/lang/Object;IIII)V
    .locals 3
    .param p1, "buf"    # Landroid/text/Spannable;
    .param p2, "what"    # Ljava/lang/Object;
    .param p3, "s"    # I
    .param p4, "e"    # I
    .param p5, "st"    # I
    .param p6, "en"    # I

    .prologue
    .line 1129
    const-string/jumbo v0, "EvBaseView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onSpanChanged s="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " e="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " st="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " en="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " what="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1131
    return-void
.end method

.method public onSpanRemoved(Landroid/text/Spannable;Ljava/lang/Object;II)V
    .locals 3
    .param p1, "buf"    # Landroid/text/Spannable;
    .param p2, "what"    # Ljava/lang/Object;
    .param p3, "s"    # I
    .param p4, "e"    # I

    .prologue
    .line 1141
    const-string/jumbo v0, "EvBaseView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onSpanRemoved s="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " e="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " what="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1143
    instance-of v0, p2, Landroid/text/style/BackgroundColorSpan;

    if-eqz v0, :cond_0

    .line 1144
    const-string/jumbo v0, "EvBaseView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "remove bspan start="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " end="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " color="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    check-cast p2, Landroid/text/style/BackgroundColorSpan;

    .end local p2    # "what":Ljava/lang/Object;
    invoke-virtual {p2}, Landroid/text/style/BackgroundColorSpan;->getBackgroundColor()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1145
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/infraware/office/baseframe/EvBaseView;->mbRemoveBColor:Z

    .line 1146
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    iput p3, v0, Lcom/infraware/office/baseframe/EvBaseView;->mRemoveBColorS:I

    .line 1147
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    iput p4, v0, Lcom/infraware/office/baseframe/EvBaseView;->mRemoveBColorE:I

    .line 1149
    :cond_0
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 20
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "after"    # I

    .prologue
    .line 1074
    const-string/jumbo v2, "EvBaseView"

    const-string/jumbo v3, "============================================="

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1076
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    # getter for: Lcom/infraware/office/baseframe/EvBaseView;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/office/baseframe/EvBaseView;->access$100(Lcom/infraware/office/baseframe/EvBaseView;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 1120
    :cond_0
    :goto_0
    return-void

    .line 1078
    :cond_1
    const-string/jumbo v2, "EvBaseView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onTextChanged start="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " before="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " after="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/infraware/office/util/EvUtil;->stringToHexa(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1081
    move-object/from16 v0, p1

    instance-of v2, v0, Landroid/text/Spannable;

    if-eqz v2, :cond_3

    move-object/from16 v16, p1

    .line 1082
    check-cast v16, Landroid/text/Spannable;

    .line 1083
    .local v16, "sp":Landroid/text/Spannable;
    const/4 v2, 0x0

    invoke-interface/range {v16 .. v16}, Landroid/text/Spannable;->length()I

    move-result v3

    const-class v4, Landroid/text/style/BackgroundColorSpan;

    move-object/from16 v0, v16

    invoke-interface {v0, v2, v3, v4}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Landroid/text/style/BackgroundColorSpan;

    .line 1084
    .local v13, "bspans":[Landroid/text/style/BackgroundColorSpan;
    array-length v14, v13

    .line 1085
    .local v14, "count":I
    new-array v0, v14, [I

    move-object/from16 v19, v0

    .line 1086
    .local v19, "vs":[I
    new-array v0, v14, [I

    move-object/from16 v18, v0

    .line 1087
    .local v18, "ve":[I
    new-array v0, v14, [I

    move-object/from16 v17, v0

    .line 1088
    .local v17, "vc":[I
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_1
    if-ge v15, v14, :cond_2

    .line 1089
    aget-object v2, v13, v15

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v2

    aput v2, v19, v15

    .line 1090
    aget-object v2, v13, v15

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v2

    aput v2, v18, v15

    .line 1091
    aget-object v2, v13, v15

    invoke-virtual {v2}, Landroid/text/style/BackgroundColorSpan;->getBackgroundColor()I

    move-result v2

    aput v2, v17, v15

    .line 1092
    const-string/jumbo v2, "EvBaseView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "add bspan start="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, v19, v15

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " end="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, v18, v15

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " color="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v13, v15

    invoke-virtual {v4}, Landroid/text/style/BackgroundColorSpan;->getBackgroundColor()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1088
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    .line 1095
    :cond_2
    invoke-static/range {v16 .. v16}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v12

    .line 1096
    .local v12, "t":I
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v1

    .line 1097
    .local v1, "evInterface":Lcom/infraware/office/evengine/EvInterface;
    const/4 v2, 0x2

    if-ne v14, v2, :cond_7

    .line 1098
    const/4 v2, 0x0

    aget v2, v19, v2

    const/4 v3, 0x0

    aget v3, v18, v3

    const/4 v4, 0x1

    aget v4, v19, v4

    const/4 v5, 0x1

    aget v5, v18, v5

    const/4 v6, 0x0

    aget v6, v17, v6

    int-to-long v6, v6

    const/4 v8, 0x1

    aget v8, v17, v8

    int-to-long v8, v8

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-virtual/range {v1 .. v12}, Lcom/infraware/office/evengine/EvInterface;->ISetCompBackColor(IIIIJJIII)V

    .line 1103
    .end local v1    # "evInterface":Lcom/infraware/office/evengine/EvInterface;
    .end local v12    # "t":I
    .end local v13    # "bspans":[Landroid/text/style/BackgroundColorSpan;
    .end local v14    # "count":I
    .end local v15    # "i":I
    .end local v16    # "sp":Landroid/text/Spannable;
    .end local v17    # "vc":[I
    .end local v18    # "ve":[I
    .end local v19    # "vs":[I
    :cond_3
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/EvBaseView;->checkComposing()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/office/baseframe/EvBaseView;->setComposing(Z)V

    .line 1104
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseView;->isEditableClear()Z

    move-result v2

    if-nez v2, :cond_8

    .line 1105
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v2, v2, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v2, :cond_4

    .line 1106
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v2, v2, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    iget-boolean v3, v3, Lcom/infraware/office/baseframe/EvBaseView;->mbComposing:Z

    move-object/from16 v4, p1

    move/from16 v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    invoke-virtual/range {v2 .. v7}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->onTextChanged(ZLjava/lang/CharSequence;III)V

    .line 1108
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseView;->isComposing()Z

    move-result v2

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseView;->isKeyProc()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseView;->isForceClear()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1109
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseView;->getEditableText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v12

    .line 1110
    .restart local v12    # "t":I
    if-eqz v12, :cond_6

    const/16 v2, 0x1f4

    if-le v12, v2, :cond_0

    .line 1111
    :cond_6
    const-string/jumbo v2, "EvBaseView"

    const-string/jumbo v3, "mEditable clear"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1112
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseView;->clearEditable()V

    goto/16 :goto_0

    .line 1099
    .restart local v1    # "evInterface":Lcom/infraware/office/evengine/EvInterface;
    .restart local v13    # "bspans":[Landroid/text/style/BackgroundColorSpan;
    .restart local v14    # "count":I
    .restart local v15    # "i":I
    .restart local v16    # "sp":Landroid/text/Spannable;
    .restart local v17    # "vc":[I
    .restart local v18    # "ve":[I
    .restart local v19    # "vs":[I
    :cond_7
    const/4 v2, 0x1

    if-ne v14, v2, :cond_3

    .line 1100
    const/4 v2, 0x0

    aget v2, v19, v2

    const/4 v3, 0x0

    aget v3, v18, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    aget v6, v17, v6

    int-to-long v6, v6

    const-wide/16 v8, -0x1

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-virtual/range {v1 .. v12}, Lcom/infraware/office/evengine/EvInterface;->ISetCompBackColor(IIIIJJIII)V

    goto/16 :goto_2

    .line 1117
    .end local v1    # "evInterface":Lcom/infraware/office/evengine/EvInterface;
    .end local v12    # "t":I
    .end local v13    # "bspans":[Landroid/text/style/BackgroundColorSpan;
    .end local v14    # "count":I
    .end local v15    # "i":I
    .end local v16    # "sp":Landroid/text/Spannable;
    .end local v17    # "vc":[I
    .end local v18    # "ve":[I
    .end local v19    # "vs":[I
    :cond_8
    const-string/jumbo v2, "EvBaseView"

    const-string/jumbo v3, "mEditable clear reset"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1118
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/infraware/office/baseframe/EvBaseView;->setEditableClear(Z)V

    goto/16 :goto_0
.end method
