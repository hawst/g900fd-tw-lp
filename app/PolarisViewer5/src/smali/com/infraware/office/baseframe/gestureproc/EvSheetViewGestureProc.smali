.class public Lcom/infraware/office/baseframe/gestureproc/EvSheetViewGestureProc;
.super Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;
.source "EvSheetViewGestureProc.java"


# instance fields
.field private final LOG_CAT:Ljava/lang/String;

.field private m_activity:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "listener"    # Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    .prologue
    .line 18
    invoke-direct {p0, p1, p2, p3}, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;)V

    .line 14
    const-string/jumbo v0, "EvSheetViewGestureProc"

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetViewGestureProc;->LOG_CAT:Ljava/lang/String;

    .line 19
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetViewGestureProc;->m_activity:Landroid/app/Activity;

    .line 20
    return-void
.end method


# virtual methods
.method protected getPageInfoType(I)I
    .locals 1
    .param p1, "nCallBackID"    # I

    .prologue
    .line 132
    const/4 v0, 0x0

    .line 134
    .local v0, "infoId":I
    packed-switch p1, :pswitch_data_0

    .line 144
    :goto_0
    :pswitch_0
    return v0

    .line 140
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 134
    nop

    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onTouchDown(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 46
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetViewGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/4 v6, 0x0

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    .line 48
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetViewGestureProc;->mTouchStatus:I

    .line 49
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v3, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v4, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    long-to-int v6, v5

    const/4 v0, -0x1

    invoke-static {p1, v0}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v7

    move v5, v2

    invoke-virtual/range {v1 .. v7}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 50
    const/4 v0, 0x1

    return v0
.end method

.method public onTouchDrag(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 10
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    const/4 v9, 0x1

    const/4 v1, 0x0

    const/4 v8, -0x1

    .line 55
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetViewGestureProc;->mTouchStatus:I

    const/4 v2, 0x5

    if-ne v0, v2, :cond_0

    .line 56
    const/4 v0, 0x2

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetViewGestureProc;->mTouchStatus:I

    .line 57
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    const/16 v4, 0x1b

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    long-to-int v5, v5

    invoke-static {p2, v8}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 61
    :goto_0
    return v9

    .line 60
    :cond_0
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v4, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v5, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    long-to-int v7, v6

    invoke-static {p2, v8}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v8

    move v3, v9

    move v6, v1

    invoke-virtual/range {v2 .. v8}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    goto :goto_0
.end method

.method public onTouchUp(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x2

    const/4 v4, 0x0

    .line 67
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetViewGestureProc;->mTouchStatus:I

    if-ne v0, v1, :cond_0

    .line 68
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    long-to-int v5, v5

    const/4 v6, -0x1

    invoke-static {p1, v6}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 69
    iput v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvSheetViewGestureProc;->mTouchStatus:I

    .line 71
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
