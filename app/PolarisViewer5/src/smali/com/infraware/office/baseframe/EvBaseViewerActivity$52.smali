.class Lcom/infraware/office/baseframe/EvBaseViewerActivity$52;
.super Ljava/lang/Object;
.source "EvBaseViewerActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ShowContentSearch(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 0

    .prologue
    .line 4537
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$52;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 4540
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$52;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-boolean v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mIsContentSearch:Z

    if-nez v0, :cond_0

    .line 4556
    :goto_0
    return-void

    .line 4541
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$52;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->InitFindBar()V

    .line 4542
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$52;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->hide()V

    .line 4543
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$52;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onContentSearchStart()V

    .line 4544
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$52;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->FindBarShow(Z)V

    .line 4545
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$52;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$900(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$52;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mContentSearchWord:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 4546
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$52;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$900(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/EditText;

    move-result-object v0

    new-instance v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity$52$1;

    invoke-direct {v1, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$52$1;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity$52;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
