.class public Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;
.super Ljava/lang/Object;
.source "EvTextToSpeechHelper.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;
.implements Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;
.implements Lcom/infraware/office/baseframe/EvBaseE$BaseActivityEventType;
.implements Lcom/infraware/office/baseframe/EvBaseE$EV_ACTIONBAR_EVENT;
.implements Lcom/infraware/office/baseframe/EvBaseE;
.implements Lcom/infraware/office/evengine/E$EV_DOCEXTENSION_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_TTS_HELPER_STATUS;
.implements Lcom/infraware/office/evengine/E$EV_TTS_REQUEST_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_TTS_START_MODE;


# static fields
.field private static final AFTER_GUIDE_FOR_CANE_HANDLER_MSG:I = 0x2

.field private static final FIRST_PLAY_FOR_CANE_HANDELER_MSG:I = 0x4

.field private static final FIRST_PLAY_HANDELER_MSG:I = 0x3

.field private static final INIT_FOR_CANE_HANDELER_MSG:I = 0x5

.field private static final INIT_HANDLER_MSG:I = 0x1

.field private static final PLAY_HANDLER_MSG:I = 0x0

.field private static final SPEAK_GUIDE_FOR_CANE_HANDELER_MSG:I = 0x6


# instance fields
.field private DUMMY_PARAMS:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final HEADSET_BRAODCAST_MESSAGE:Ljava/lang/String;

.field private final MINIMUM_TIME_FOR_A_LINE:J

.field private hasAudioFocus:Z

.field private mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

.field private mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mEvInterface:Lcom/infraware/office/evengine/EvInterface;

.field private mPrevStartTime:J

.field private mReceiver:Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;

.field private final mStopHandler:Landroid/os/Handler;

.field private m_TtsEngine:Landroid/speech/tts/TextToSpeech;

.field private m_bClose:Z

.field private m_bGuideRequestFlag:Z

.field private m_bGuideSpoken:Z

.field private m_bReqGuideSpeak:Z

.field private m_bReqStrAfterGuide:Z

.field private m_bStartModeSpeak:Z

.field private m_bStrSpokenAfterGuide:Z

.field private m_bTTSInit:Z

.field private m_nHelperStatus:I

.field private m_nStartMode:I

.field private m_strReadTTS:Ljava/lang/String;

.field protected final messageHandler:Landroid/os/Handler;

.field private strHashMap:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 4
    .param p1, "activity"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 35
    iput-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->DUMMY_PARAMS:Ljava/util/HashMap;

    .line 39
    const-string/jumbo v0, "officeTTS"

    iput-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->strHashMap:Ljava/lang/String;

    .line 40
    iput-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_strReadTTS:Ljava/lang/String;

    .line 42
    iput-boolean v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bTTSInit:Z

    iput-boolean v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bClose:Z

    .line 43
    iput-boolean v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bGuideRequestFlag:Z

    iput-boolean v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bStartModeSpeak:Z

    .line 46
    iput-boolean v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bReqGuideSpeak:Z

    iput-boolean v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bGuideSpoken:Z

    .line 47
    iput-boolean v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bReqStrAfterGuide:Z

    iput-boolean v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bStrSpokenAfterGuide:Z

    .line 49
    iput v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nHelperStatus:I

    .line 50
    iput v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nStartMode:I

    .line 61
    const-string/jumbo v0, "android.intent.action.HEADSET_PLUG"

    iput-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->HEADSET_BRAODCAST_MESSAGE:Ljava/lang/String;

    .line 62
    iput-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mReceiver:Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;

    .line 64
    iput-boolean v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->hasAudioFocus:Z

    .line 484
    new-instance v0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$2;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$2;-><init>(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->messageHandler:Landroid/os/Handler;

    .line 577
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mPrevStartTime:J

    .line 578
    const-wide/16 v0, 0x1f4

    iput-wide v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->MINIMUM_TIME_FOR_A_LINE:J

    .line 762
    new-instance v0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$3;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$3;-><init>(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 905
    new-instance v0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$4;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$4;-><init>(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mStopHandler:Landroid/os/Handler;

    .line 67
    iput-object p1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 68
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 70
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->DUMMY_PARAMS:Ljava/util/HashMap;

    const-string/jumbo v1, "utteranceId"

    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->strHashMap:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const-string/jumbo v1, "audio"

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mAudioManager:Landroid/media/AudioManager;

    .line 72
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->BGMPause()V

    .line 73
    invoke-direct {p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->registerReceiver()V

    .line 75
    new-instance v0, Landroid/speech/tts/TextToSpeech;

    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-direct {v0, v1, p0}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    .line 76
    iput-boolean v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bTTSInit:Z

    .line 77
    iput-boolean v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bGuideRequestFlag:Z

    .line 79
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->initManageGuideFlag()V

    .line 80
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bClose:Z

    return v0
.end method

.method static synthetic access$200(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    .prologue
    .line 29
    iget v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nStartMode:I

    return v0
.end method

.method static synthetic access$300(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)Lcom/infraware/office/evengine/EvInterface;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    return-object v0
.end method

.method static synthetic access$400(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_strReadTTS:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->DUMMY_PARAMS:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$600(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)Landroid/speech/tts/TextToSpeech;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    return-object v0
.end method

.method public static isSupportTTS(I)Z
    .locals 1
    .param p0, "EV_DOCEXTENSION_TYPE"    # I

    .prologue
    .line 83
    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/16 v0, 0x12

    if-eq p0, v0, :cond_0

    const/16 v0, 0xc

    if-ne p0, v0, :cond_1

    .line 84
    :cond_0
    const/4 v0, 0x1

    .line 86
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onTTSSettingFail()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 947
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07030c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 950
    .local v0, "toast":Landroid/widget/Toast;
    const/16 v1, 0x11

    invoke-virtual {v0, v1, v4, v4}, Landroid/widget/Toast;->setGravity(III)V

    .line 951
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 966
    return-void
.end method

.method private registerReceiver()V
    .locals 3

    .prologue
    .line 788
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mReceiver:Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;

    if-eqz v1, :cond_0

    .line 797
    :goto_0
    return-void

    .line 791
    :cond_0
    new-instance v1, Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;

    invoke-direct {v1}, Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;-><init>()V

    iput-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mReceiver:Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;

    .line 792
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mReceiver:Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;

    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mStopHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;->setHandler(Landroid/os/Handler;)V

    .line 794
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 795
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string/jumbo v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 796
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mReceiver:Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0
.end method


# virtual methods
.method public BGMPause()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 756
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->hasAudioFocus:Z

    if-nez v0, :cond_0

    .line 757
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 758
    iput-boolean v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->hasAudioFocus:Z

    .line 760
    :cond_0
    return-void
.end method

.method public FlagLog(Ljava/lang/String;)V
    .locals 3
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 914
    const-string/jumbo v0, "FlagLog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " callFunc = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 915
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bStartModeSpeak:Z

    if-eqz v0, :cond_0

    .line 916
    const-string/jumbo v0, "FlagLog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " StartModeSpeak "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bStartModeSpeak:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 918
    :cond_0
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bGuideRequestFlag:Z

    if-eqz v0, :cond_1

    .line 919
    const-string/jumbo v0, "FlagLog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " GuideRequestFlag "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bGuideRequestFlag:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 921
    :cond_1
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bReqGuideSpeak:Z

    if-eqz v0, :cond_2

    .line 922
    const-string/jumbo v0, "FlagLog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " ReqPgNum "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bReqGuideSpeak:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 924
    :cond_2
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bGuideSpoken:Z

    if-eqz v0, :cond_3

    .line 925
    const-string/jumbo v0, "FlagLog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " SpokenPgNum "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bGuideSpoken:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 927
    :cond_3
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bReqStrAfterGuide:Z

    if-eqz v0, :cond_4

    .line 928
    const-string/jumbo v0, "FlagLog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " ReqAfterString "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bReqStrAfterGuide:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    :cond_4
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bStrSpokenAfterGuide:Z

    if-eqz v0, :cond_5

    .line 931
    const-string/jumbo v0, "FlagLog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " SpokenAfterString "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bStrSpokenAfterGuide:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 933
    :cond_5
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getTTSToolbarPlayFlag()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 934
    const-string/jumbo v1, "FlagLog"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "WordEditorPlayFlag "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getTTSToolbarPlayFlag()Z

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 936
    :cond_6
    const-string/jumbo v0, "FlagLog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "HelperCurStatus = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nHelperStatus:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 937
    const-string/jumbo v0, "FlagLog"

    const-string/jumbo v1, ""

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 941
    return-void
.end method

.method public changeHelperStatus(I)V
    .locals 3
    .param p1, "nBtnId"    # I

    .prologue
    const/4 v2, 0x3

    .line 716
    iget v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nHelperStatus:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    .line 743
    :cond_0
    :goto_0
    return-void

    .line 719
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getTTSToolbarPlayFlag()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 725
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->isSpeaking()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 726
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I

    .line 729
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->initManageGuideFlag()V

    .line 731
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 737
    :pswitch_0
    iput v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nHelperStatus:I

    goto :goto_0

    .line 734
    :pswitch_1
    const/4 v0, 0x2

    iput v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nHelperStatus:I

    goto :goto_0

    .line 740
    :pswitch_2
    iput v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nHelperStatus:I

    goto :goto_0

    .line 731
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b012a
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public checkLanguage()I
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 828
    const/4 v0, 0x0

    .line 830
    .local v0, "nAvailableCount":I
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    if-nez v1, :cond_0

    .line 831
    const/4 v1, 0x0

    .line 856
    :goto_0
    return v1

    .line 833
    :cond_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v1

    if-ne v1, v5, :cond_1

    .line 834
    add-int/lit8 v0, v0, 0x1

    .line 835
    :cond_1
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    sget-object v2, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v1

    if-ne v1, v5, :cond_2

    .line 836
    add-int/lit8 v0, v0, 0x1

    .line 837
    :cond_2
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    sget-object v2, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v1

    if-ne v1, v5, :cond_3

    .line 838
    add-int/lit8 v0, v0, 0x1

    .line 839
    :cond_3
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    sget-object v2, Ljava/util/Locale;->GERMAN:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v1

    if-ne v1, v5, :cond_4

    .line 840
    add-int/lit8 v0, v0, 0x1

    .line 841
    :cond_4
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    sget-object v2, Ljava/util/Locale;->FRANCE:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v1

    if-ne v1, v5, :cond_5

    .line 842
    add-int/lit8 v0, v0, 0x1

    .line 843
    :cond_5
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    sget-object v2, Ljava/util/Locale;->ITALY:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v1

    if-ne v1, v5, :cond_6

    .line 844
    add-int/lit8 v0, v0, 0x1

    .line 845
    :cond_6
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    new-instance v2, Ljava/util/Locale;

    const-string/jumbo v3, "spa"

    const-string/jumbo v4, "ESP"

    invoke-direct {v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v1

    if-ne v1, v5, :cond_7

    .line 846
    add-int/lit8 v0, v0, 0x1

    .line 847
    :cond_7
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    sget-object v2, Ljava/util/Locale;->SIMPLIFIED_CHINESE:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v1

    if-ne v1, v5, :cond_8

    .line 848
    add-int/lit8 v0, v0, 0x1

    .line 849
    :cond_8
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    sget-object v2, Ljava/util/Locale;->TRADITIONAL_CHINESE:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v1

    if-ne v1, v5, :cond_9

    .line 850
    add-int/lit8 v0, v0, 0x1

    .line 851
    :cond_9
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    sget-object v2, Ljava/util/Locale;->TAIWAN:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v1

    if-ne v1, v5, :cond_a

    .line 852
    add-int/lit8 v0, v0, 0x1

    .line 853
    :cond_a
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    sget-object v2, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v1

    if-ne v1, v5, :cond_b

    .line 854
    add-int/lit8 v0, v0, 0x1

    :cond_b
    move v1, v0

    .line 856
    goto/16 :goto_0
.end method

.method public checkLanguageForSamsung()[Ljava/util/Locale;
    .locals 10

    .prologue
    const/16 v5, 0xb

    const/4 v9, 0x1

    .line 808
    const/4 v3, 0x0

    .line 809
    .local v3, "nCount":I
    new-array v0, v5, [Ljava/util/Locale;

    .line 810
    .local v0, "arrAvailable":[Ljava/util/Locale;
    new-array v1, v5, [Ljava/util/Locale;

    const/4 v5, 0x0

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    aput-object v6, v1, v5

    sget-object v5, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    aput-object v5, v1, v9

    const/4 v5, 0x2

    sget-object v6, Ljava/util/Locale;->UK:Ljava/util/Locale;

    aput-object v6, v1, v5

    const/4 v5, 0x3

    sget-object v6, Ljava/util/Locale;->GERMAN:Ljava/util/Locale;

    aput-object v6, v1, v5

    const/4 v5, 0x4

    sget-object v6, Ljava/util/Locale;->FRANCE:Ljava/util/Locale;

    aput-object v6, v1, v5

    const/4 v5, 0x5

    sget-object v6, Ljava/util/Locale;->ITALY:Ljava/util/Locale;

    aput-object v6, v1, v5

    const/4 v5, 0x6

    new-instance v6, Ljava/util/Locale;

    const-string/jumbo v7, "spa"

    const-string/jumbo v8, "ESP"

    invoke-direct {v6, v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v1, v5

    const/4 v5, 0x7

    sget-object v6, Ljava/util/Locale;->SIMPLIFIED_CHINESE:Ljava/util/Locale;

    aput-object v6, v1, v5

    const/16 v5, 0x8

    sget-object v6, Ljava/util/Locale;->TRADITIONAL_CHINESE:Ljava/util/Locale;

    aput-object v6, v1, v5

    const/16 v5, 0x9

    sget-object v6, Ljava/util/Locale;->TAIWAN:Ljava/util/Locale;

    aput-object v6, v1, v5

    const/16 v5, 0xa

    sget-object v6, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    aput-object v6, v1, v5

    .line 815
    .local v1, "arrLocaleList":[Ljava/util/Locale;
    iget-object v5, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    if-nez v5, :cond_1

    .line 824
    :cond_0
    return-object v0

    .line 818
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v5, v1

    if-ge v2, v5, :cond_0

    .line 819
    iget-object v5, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    aget-object v6, v1, v2

    invoke-virtual {v5, v6}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v5

    if-ne v5, v9, :cond_2

    .line 820
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "nCount":I
    .local v4, "nCount":I
    aget-object v5, v1, v2

    aput-object v5, v0, v3

    move v3, v4

    .line 818
    .end local v4    # "nCount":I
    .restart local v3    # "nCount":I
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public doCompletedForGuide()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 533
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bGuideRequestFlag:Z

    if-eqz v0, :cond_1

    .line 534
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->onStopForGuide()V

    .line 575
    :cond_0
    :goto_0
    return-void

    .line 538
    :cond_1
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bStartModeSpeak:Z

    if-eqz v0, :cond_2

    .line 540
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bStartModeSpeak:Z

    .line 541
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bReqGuideSpeak:Z

    .line 543
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->messageHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 548
    :cond_2
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bReqGuideSpeak:Z

    if-eqz v0, :cond_3

    .line 550
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bGuideSpoken:Z

    .line 551
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bReqGuideSpeak:Z

    .line 554
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->messageHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 556
    :cond_3
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bGuideSpoken:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bReqStrAfterGuide:Z

    if-eqz v0, :cond_0

    .line 560
    :cond_4
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bReqStrAfterGuide:Z

    if-eqz v0, :cond_5

    .line 562
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bReqStrAfterGuide:Z

    .line 563
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bStrSpokenAfterGuide:Z

    .line 564
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->initManageGuideFlag()V

    .line 565
    const/4 v0, 0x3

    iput v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nHelperStatus:I

    .line 568
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->messageHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 573
    :cond_5
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->messageHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public doFirstPlayHandlerForCane()V
    .locals 2

    .prologue
    const/16 v1, 0x100

    .line 465
    const/4 v0, 0x3

    iput v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nHelperStatus:I

    .line 467
    iget v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nStartMode:I

    packed-switch v0, :pswitch_data_0

    .line 481
    :goto_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->enableTTSToolbar(Z)V

    .line 482
    return-void

    .line 470
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->IGetTextToSpeachString(I)V

    goto :goto_0

    .line 474
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->IGetTextToSpeachString(I)V

    goto :goto_0

    .line 478
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->IGetTextToSpeachString(I)V

    goto :goto_0

    .line 467
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public doPlayHandler()V
    .locals 2

    .prologue
    .line 447
    iget v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nHelperStatus:I

    packed-switch v0, :pswitch_data_0

    .line 461
    :goto_0
    return-void

    .line 450
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->IGetTextToSpeachString(I)V

    goto :goto_0

    .line 455
    :pswitch_1
    const/4 v0, 0x3

    iput v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nHelperStatus:I

    .line 456
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_strReadTTS:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->speechString(Ljava/lang/String;)V

    goto :goto_0

    .line 447
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public finalizeSpeech()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 90
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bClose:Z

    .line 92
    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->hasAudioFocus:Z

    if-eqz v3, :cond_0

    .line 93
    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mAudioManager:Landroid/media/AudioManager;

    iget-object v4, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v3, v4}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 94
    iput-boolean v5, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->hasAudioFocus:Z

    .line 96
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->initManageGuideFlag()V

    .line 97
    iput-boolean v5, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bGuideRequestFlag:Z

    .line 98
    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const v4, 0xffff

    invoke-virtual {v3, v4}, Lcom/infraware/office/evengine/EvInterface;->IGetTextToSpeachString(I)V

    .line 100
    iget-boolean v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bTTSInit:Z

    if-eqz v3, :cond_1

    .line 101
    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v3}, Landroid/speech/tts/TextToSpeech;->stop()I

    move-result v0

    .line 103
    .local v0, "nRetValue":I
    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v3}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    .line 107
    .end local v0    # "nRetValue":I
    :cond_1
    const/4 v1, 0x0

    .line 108
    .local v1, "nStrMsg":I
    iget v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nStartMode:I

    if-nez v3, :cond_2

    .line 109
    const v1, 0x7f070312

    .line 113
    :goto_0
    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v4, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    .line 115
    .local v2, "toast":Landroid/widget/Toast;
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 116
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->unregisterReceiver()V

    .line 118
    invoke-virtual {p0, v5}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->setFocusMode(Z)V

    .line 119
    const/4 v3, 0x3

    iput v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nHelperStatus:I

    .line 120
    return-void

    .line 111
    .end local v2    # "toast":Landroid/widget/Toast;
    :cond_2
    const v1, 0x7f07030f

    goto :goto_0
.end method

.method public getGuideRequestFlag()Z
    .locals 1

    .prologue
    .line 877
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bGuideRequestFlag:Z

    return v0
.end method

.method public getGuideSpokenFlag()Z
    .locals 1

    .prologue
    .line 885
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bGuideSpoken:Z

    return v0
.end method

.method public getManageTTSStatus()I
    .locals 1

    .prologue
    .line 897
    iget v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nHelperStatus:I

    return v0
.end method

.method public getReqGuideSpeakFlag()Z
    .locals 1

    .prologue
    .line 881
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bReqGuideSpeak:Z

    return v0
.end method

.method public getReqStrAfterGuideFlag()Z
    .locals 1

    .prologue
    .line 889
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bReqStrAfterGuide:Z

    return v0
.end method

.method public getStartModeSpeak()Z
    .locals 1

    .prologue
    .line 901
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bStartModeSpeak:Z

    return v0
.end method

.method public getStrSpokenAfterGuide()Z
    .locals 1

    .prologue
    .line 893
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bStrSpokenAfterGuide:Z

    return v0
.end method

.method public initManageGuideFlag()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 862
    iput-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bReqGuideSpeak:Z

    .line 863
    iput-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bGuideSpoken:Z

    .line 864
    iput-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bReqStrAfterGuide:Z

    .line 865
    iput-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bStrSpokenAfterGuide:Z

    .line 866
    return-void
.end method

.method public initializeFocusTTS(Z)V
    .locals 4
    .param p1, "bMarking"    # Z

    .prologue
    .line 394
    if-eqz p1, :cond_1

    .line 395
    const/4 v0, 0x2

    iput v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nStartMode:I

    .line 396
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/16 v1, 0x100

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->IGetTextToSpeachString(I)V

    .line 401
    :cond_0
    :goto_0
    return-void

    .line 398
    :cond_1
    iget v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nStartMode:I

    if-eqz v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->messageHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public initializeWholeTTS(Z)V
    .locals 2
    .param p1, "bFocus"    # Z

    .prologue
    .line 404
    if-nez p1, :cond_0

    .line 405
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->IGetTextToSpeachString(I)V

    .line 406
    :cond_0
    return-void
.end method

.method public nextLine()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 626
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->BGMPause()V

    .line 629
    const/4 v0, 0x3

    iput v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nHelperStatus:I

    .line 630
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->messageHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 631
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->IGetTextToSpeachString(I)V

    .line 632
    return-void
.end method

.method public onGuideStart()V
    .locals 4

    .prologue
    .line 172
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->removeAllMessages()V

    .line 176
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->messageHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 177
    return-void
.end method

.method public onInit(I)V
    .locals 2
    .param p1, "status"    # I

    .prologue
    .line 281
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "FT03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282
    invoke-virtual {p0, p1}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->onInitForSamsung(I)V

    .line 285
    :goto_0
    return-void

    .line 284
    :cond_0
    invoke-virtual {p0, p1}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->onInitNormal(I)V

    goto :goto_0
.end method

.method public onInitForSamsung(I)V
    .locals 13
    .param p1, "status"    # I

    .prologue
    .line 288
    if-nez p1, :cond_11

    .line 290
    iget-object v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    if-nez v9, :cond_0

    .line 389
    :goto_0
    return-void

    .line 293
    :cond_0
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bTTSInit:Z

    .line 295
    iget-object v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    iget-object v10, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v10}, Landroid/speech/tts/TextToSpeech;->getDefaultEngine()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/speech/tts/TextToSpeech;->setEngineByPackageName(Ljava/lang/String;)I

    .line 297
    iget-object v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v9, p0}, Landroid/speech/tts/TextToSpeech;->setOnUtteranceCompletedListener(Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;)I

    move-result v7

    .line 301
    .local v7, "nRtn":I
    :try_start_0
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->checkLanguageForSamsung()[Ljava/util/Locale;

    move-result-object v0

    .line 302
    .local v0, "arrAvailableLanguage":[Ljava/util/Locale;
    const/4 v9, 0x0

    aget-object v9, v0, v9

    if-eqz v9, :cond_3

    const/4 v2, 0x1

    .line 304
    .local v2, "hasAvailableLanguage":Z
    :goto_1
    iget-object v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v9}, Landroid/speech/tts/TextToSpeech;->getLanguage()Ljava/util/Locale;

    move-result-object v9

    if-nez v9, :cond_4

    .line 306
    if-eqz v2, :cond_1

    .line 307
    iget-object v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    const/4 v10, 0x0

    aget-object v10, v0, v10

    invoke-virtual {v9, v10}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    .line 352
    :cond_1
    :goto_2
    if-eqz v2, :cond_2

    iget-object v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    iget-object v10, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v10}, Landroid/speech/tts/TextToSpeech;->getLanguage()Ljava/util/Locale;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v9

    const/4 v10, -0x2

    if-ne v9, v10, :cond_c

    .line 354
    :cond_2
    invoke-direct {p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->onTTSSettingFail()V

    .line 355
    iget-object v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/16 v10, 0x41

    invoke-virtual {v9, v10}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 374
    .end local v0    # "arrAvailableLanguage":[Ljava/util/Locale;
    .end local v2    # "hasAvailableLanguage":Z
    :catch_0
    move-exception v1

    .line 376
    .local v1, "e":Ljava/lang/Exception;
    invoke-direct {p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->onTTSSettingFail()V

    .line 377
    iget-object v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/16 v10, 0x41

    invoke-virtual {v9, v10}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    goto :goto_0

    .line 302
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "arrAvailableLanguage":[Ljava/util/Locale;
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 309
    .restart local v2    # "hasAvailableLanguage":Z
    :cond_4
    :try_start_1
    iget-object v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v9}, Landroid/speech/tts/TextToSpeech;->getLanguage()Ljava/util/Locale;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 311
    if-eqz v2, :cond_1

    .line 312
    iget-object v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    const/4 v10, 0x0

    aget-object v10, v0, v10

    invoke-virtual {v9, v10}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    goto :goto_2

    .line 314
    :cond_5
    invoke-static {}, Lcom/infraware/common/util/Utils;->isL()Z

    move-result v9

    if-eqz v9, :cond_7

    .line 315
    iget-object v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    iget-object v10, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v10}, Landroid/speech/tts/TextToSpeech;->getLanguage()Ljava/util/Locale;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    move-result v3

    .line 316
    .local v3, "languageResult":I
    const/4 v9, -0x2

    if-eq v3, v9, :cond_6

    const/4 v9, -0x1

    if-ne v3, v9, :cond_1

    .line 318
    :cond_6
    iget-object v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v9, v10}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    goto :goto_2

    .line 328
    .end local v3    # "languageResult":I
    :cond_7
    iget-object v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v9}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string/jumbo v10, "tts_default_synth"

    invoke-static {v9, v10}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 329
    .local v6, "mDefaultSynth":Ljava/lang/String;
    iget-object v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v9}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string/jumbo v10, "tts_default_locale"

    invoke-static {v9, v10}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 330
    .local v5, "mDefaultLocale":Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/infraware/common/util/Utils;->parseEnginePrefFromList(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 331
    .local v8, "strLocale":Ljava/lang/String;
    if-eqz v8, :cond_a

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_a

    .line 332
    new-instance v4, Ljava/util/Locale;

    invoke-direct {v4, v8, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    .local v4, "locale":Ljava/util/Locale;
    :goto_3
    iget-object v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v9, v4}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v9

    if-eqz v9, :cond_8

    iget-object v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v9, v4}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v9

    const/4 v10, 0x1

    if-eq v9, v10, :cond_8

    iget-object v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v9, v4}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v9

    const/4 v10, 0x2

    if-ne v9, v10, :cond_b

    .line 340
    :cond_8
    iget-object v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v9, v4}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    move-result v3

    .line 341
    .restart local v3    # "languageResult":I
    const/4 v9, -0x2

    if-eq v3, v9, :cond_9

    const/4 v9, -0x1

    if-ne v3, v9, :cond_1

    .line 343
    :cond_9
    iget-object v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v9, v10}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    goto/16 :goto_2

    .line 334
    .end local v3    # "languageResult":I
    .end local v4    # "locale":Ljava/util/Locale;
    :cond_a
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    .restart local v4    # "locale":Ljava/util/Locale;
    goto :goto_3

    .line 348
    :cond_b
    iget-object v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v9, v10}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    goto/16 :goto_2

    .line 359
    .end local v4    # "locale":Ljava/util/Locale;
    .end local v5    # "mDefaultLocale":Ljava/lang/String;
    .end local v6    # "mDefaultSynth":Ljava/lang/String;
    .end local v8    # "strLocale":Ljava/lang/String;
    :cond_c
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v9

    if-eqz v9, :cond_d

    .line 360
    iget-object v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->messageHandler:Landroid/os/Handler;

    const/4 v10, 0x5

    const-wide/16 v11, 0xc8

    invoke-virtual {v9, v10, v11, v12}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 363
    :cond_d
    iget-object v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_strReadTTS:Ljava/lang/String;

    if-eqz v9, :cond_10

    .line 364
    iget v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nStartMode:I

    if-nez v9, :cond_f

    .line 365
    iget-object v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->messageHandler:Landroid/os/Handler;

    const/4 v10, 0x1

    const-wide/16 v11, 0x12c

    invoke-virtual {v9, v10, v11, v12}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 371
    :cond_e
    :goto_4
    const/4 v9, 0x3

    iput v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nHelperStatus:I

    goto/16 :goto_0

    .line 366
    :cond_f
    iget v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nStartMode:I

    const/4 v10, 0x2

    if-ne v9, v10, :cond_e

    .line 367
    iget-object v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->messageHandler:Landroid/os/Handler;

    const/4 v10, 0x1

    const-wide/16 v11, 0x12c

    invoke-virtual {v9, v10, v11, v12}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_4

    .line 368
    :cond_10
    iget v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nStartMode:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_e

    .line 369
    const/4 v9, 0x0

    invoke-virtual {p0, v9}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->initializeFocusTTS(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    .line 386
    .end local v0    # "arrAvailableLanguage":[Ljava/util/Locale;
    .end local v2    # "hasAvailableLanguage":Z
    .end local v7    # "nRtn":I
    :cond_11
    iget-object v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v10, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v10}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f07031a

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v10

    const/4 v11, 0x0

    invoke-static {v9, v10, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/Toast;->show()V

    .line 387
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bTTSInit:Z

    goto/16 :goto_0
.end method

.method public onInitNormal(I)V
    .locals 7
    .param p1, "status"    # I

    .prologue
    const/4 v6, 0x3

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 410
    if-nez p1, :cond_4

    .line 411
    iput-boolean v4, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bTTSInit:Z

    .line 413
    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v3}, Landroid/speech/tts/TextToSpeech;->getDefaultEngine()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/speech/tts/TextToSpeech;->setEngineByPackageName(Ljava/lang/String;)I

    .line 415
    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v2, p0}, Landroid/speech/tts/TextToSpeech;->setOnUtteranceCompletedListener(Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;)I

    move-result v0

    .line 417
    .local v0, "nRtn":I
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->checkLanguage()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    move-result v2

    const/4 v3, -0x2

    if-ne v2, v3, :cond_2

    .line 420
    :cond_0
    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07030c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 423
    .local v1, "toast":Landroid/widget/Toast;
    const/16 v2, 0x11

    invoke-virtual {v1, v2, v5, v5}, Landroid/widget/Toast;->setGravity(III)V

    .line 424
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 425
    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/16 v3, 0x41

    invoke-virtual {v2, v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    .line 442
    .end local v0    # "nRtn":I
    .end local v1    # "toast":Landroid/widget/Toast;
    :cond_1
    :goto_0
    return-void

    .line 429
    .restart local v0    # "nRtn":I
    :cond_2
    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_strReadTTS:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 431
    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_strReadTTS:Ljava/lang/String;

    iget-object v4, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->DUMMY_PARAMS:Ljava/util/HashMap;

    invoke-virtual {v2, v3, v5, v4}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    .line 432
    iput v6, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nHelperStatus:I

    goto :goto_0

    .line 433
    :cond_3
    iget v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nStartMode:I

    if-ne v2, v4, :cond_1

    .line 434
    iput v6, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nHelperStatus:I

    .line 435
    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/16 v3, 0x100

    invoke-virtual {v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IGetTextToSpeachString(I)V

    goto :goto_0

    .line 439
    .end local v0    # "nRtn":I
    :cond_4
    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07031a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 440
    iput-boolean v5, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bTTSInit:Z

    goto :goto_0
.end method

.method public onStopForGuide()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 686
    iget-boolean v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bGuideRequestFlag:Z

    if-eqz v1, :cond_0

    .line 687
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->removeAllMessages()V

    .line 689
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bStartModeSpeak:Z

    .line 690
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->initManageGuideFlag()V

    .line 692
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v1}, Landroid/speech/tts/TextToSpeech;->stop()I

    move-result v0

    .line 693
    .local v0, "nRetValue":I
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_strReadTTS:Ljava/lang/String;

    .line 694
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v1, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->enableTTSToolbar(Z)V

    .line 696
    const/4 v1, 0x5

    iput v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nHelperStatus:I

    .line 699
    .end local v0    # "nRetValue":I
    :cond_0
    return-void
.end method

.method public onUtteranceCompleted(Ljava/lang/String;)V
    .locals 7
    .param p1, "utteranceId"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const-wide/16 v4, 0x1f4

    .line 586
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->strHashMap:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 588
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->removeAllMessages()V

    .line 590
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mPrevStartTime:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-gez v0, :cond_2

    .line 593
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 594
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->doCompletedForGuide()V

    .line 612
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mPrevStartTime:J

    .line 614
    :cond_0
    return-void

    .line 598
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->messageHandler:Landroid/os/Handler;

    invoke-virtual {v0, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 603
    :cond_2
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 604
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->doCompletedForGuide()V

    goto :goto_0

    .line 608
    :cond_3
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->messageHandler:Landroid/os/Handler;

    invoke-virtual {v0, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 647
    const/4 v0, 0x2

    iput v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nHelperStatus:I

    .line 648
    return-void
.end method

.method public play()V
    .locals 1

    .prologue
    .line 618
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->BGMPause()V

    .line 620
    const/4 v0, 0x3

    iput v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nHelperStatus:I

    .line 621
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_strReadTTS:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->speechString(Ljava/lang/String;)V

    .line 622
    return-void
.end method

.method public prevLine()V
    .locals 2

    .prologue
    .line 636
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->BGMPause()V

    .line 639
    const/4 v0, 0x3

    iput v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nHelperStatus:I

    .line 640
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->messageHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 641
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/16 v1, 0x3000

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->IGetTextToSpeachString(I)V

    .line 642
    return-void
.end method

.method public removeAllMessages()V
    .locals 2

    .prologue
    .line 702
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->messageHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 703
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->messageHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 705
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 707
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->messageHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 708
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->messageHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 709
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->messageHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 710
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->messageHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 712
    :cond_0
    return-void
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 652
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->BGMPause()V

    .line 654
    const/4 v0, 0x3

    iput v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nHelperStatus:I

    .line 656
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_strReadTTS:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 657
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_strReadTTS:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->speechString(Ljava/lang/String;)V

    .line 658
    :cond_0
    return-void
.end method

.method public resumeForGuide()V
    .locals 2

    .prologue
    .line 662
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->BGMPause()V

    .line 664
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->messageHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 666
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_strReadTTS:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 667
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_strReadTTS:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->setSpeakStringAfterScroll(Ljava/lang/String;)V

    .line 668
    :cond_0
    return-void
.end method

.method public setFocusMode(Z)V
    .locals 1
    .param p1, "bFocus"    # Z

    .prologue
    .line 869
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nStartMode:I

    .line 870
    return-void
.end method

.method public setGuideRequestFlag(Z)V
    .locals 0
    .param p1, "bRestartRequest"    # Z

    .prologue
    .line 873
    iput-boolean p1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bGuideRequestFlag:Z

    .line 874
    return-void
.end method

.method public setReadString(Ljava/lang/String;)V
    .locals 1
    .param p1, "readStr"    # Ljava/lang/String;

    .prologue
    .line 251
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bClose:Z

    if-eqz v0, :cond_0

    .line 257
    :goto_0
    return-void

    .line 254
    :cond_0
    iput-object p1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_strReadTTS:Ljava/lang/String;

    goto :goto_0
.end method

.method public setSpeakStringAfterScroll(Ljava/lang/String;)V
    .locals 3
    .param p1, "reStartStr"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 230
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bClose:Z

    if-eqz v0, :cond_1

    .line 247
    :cond_0
    :goto_0
    return-void

    .line 233
    :cond_1
    invoke-virtual {p0, p1}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->setReadString(Ljava/lang/String;)V

    .line 234
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bGuideSpoken:Z

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getTTSToolbarPlayFlag()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bReqStrAfterGuide:Z

    .line 245
    const-string/jumbo v0, "setSpeakStringAfterScroll"

    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_strReadTTS:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->speakString(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->enableTTSToolbar(Z)V

    goto :goto_0
.end method

.method public speakGuide(Z)V
    .locals 12
    .param p1, "bStartMode"    # Z

    .prologue
    const v9, 0x7f070242

    const/4 v8, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 184
    iget-boolean v6, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bClose:Z

    if-eqz v6, :cond_1

    .line 224
    :cond_0
    :goto_0
    return-void

    .line 187
    :cond_1
    iget-boolean v6, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bGuideRequestFlag:Z

    if-nez v6, :cond_0

    .line 190
    iget-object v6, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v6}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    .line 194
    .local v0, "configInfo":Lcom/infraware/office/evengine/EV$CONFIG_INFO;
    const/4 v2, 0x0

    .line 195
    .local v2, "nCurPgNum":I
    iget-object v6, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v6}, Lcom/infraware/office/evengine/EvInterface;->IGetPageDisplayInfo()[Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;

    move-result-object v1

    .line 196
    .local v1, "info":[Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;
    aget-object v6, v1, v10

    invoke-virtual {v6}, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->isValidInfo()Z

    move-result v6

    if-ne v6, v11, :cond_2

    .line 197
    aget-object v6, v1, v10

    iget v2, v6, Lcom/infraware/office/evengine/EV$PAGE_DISPLAY_INFO;->nPageNum:I

    .line 201
    :goto_1
    if-eqz p1, :cond_4

    .line 202
    iget v6, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nStartMode:I

    if-nez v6, :cond_3

    .line 204
    iget-object v6, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v6}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070243

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    .line 205
    .local v5, "text":Ljava/lang/CharSequence;
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 207
    .local v4, "strSpeakStartMode":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v7}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v8, v8, [Ljava/lang/Object;

    iget v9, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nTotalPages:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 211
    .end local v4    # "strSpeakStartMode":Ljava/lang/String;
    .end local v5    # "text":Ljava/lang/CharSequence;
    .local v3, "strSpeakPageMsg":Ljava/lang/String;
    :goto_2
    iput-boolean v11, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bStartModeSpeak:Z

    .line 219
    :goto_3
    iput-boolean v11, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bReqGuideSpeak:Z

    .line 220
    const-string/jumbo v6, "speakGuide"

    invoke-virtual {p0, v6, v3}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->speakString(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    iget-boolean v6, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bStartModeSpeak:Z

    if-eqz v6, :cond_0

    .line 223
    iget-object v6, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v6, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v6, v10}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->enableTTSToolbar(Z)V

    goto :goto_0

    .line 199
    .end local v3    # "strSpeakPageMsg":Ljava/lang/String;
    :cond_2
    iget v2, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    goto :goto_1

    .line 210
    :cond_3
    iget-object v6, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v6}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v8, [Ljava/lang/Object;

    iget v8, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nTotalPages:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v11

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "strSpeakPageMsg":Ljava/lang/String;
    goto :goto_2

    .line 213
    .end local v3    # "strSpeakPageMsg":Ljava/lang/String;
    :cond_4
    iget-object v6, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v6, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getTTSToolbarPlayFlag()Z

    move-result v6

    if-nez v6, :cond_5

    .line 214
    iget-object v6, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v6}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070241

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v8, [Ljava/lang/Object;

    iget v8, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nTotalPages:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v11

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "strSpeakPageMsg":Ljava/lang/String;
    goto :goto_3

    .line 216
    .end local v3    # "strSpeakPageMsg":Ljava/lang/String;
    :cond_5
    iget-object v6, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v6}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070240

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v8, [Ljava/lang/Object;

    iget v8, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nTotalPages:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v11

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "strSpeakPageMsg":Ljava/lang/String;
    goto/16 :goto_3
.end method

.method public speakString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "strCallFunc"    # Ljava/lang/String;
    .param p2, "speakString"    # Ljava/lang/String;

    .prologue
    .line 262
    if-nez p2, :cond_1

    .line 276
    :cond_0
    :goto_0
    return-void

    .line 264
    :cond_1
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bGuideRequestFlag:Z

    if-nez v0, :cond_0

    .line 267
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bTTSInit:Z

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->isSpeaking()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 271
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I

    .line 273
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->DUMMY_PARAMS:Ljava/util/HashMap;

    invoke-virtual {v0, p2, v1, v2}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    goto :goto_0
.end method

.method public speechString(Ljava/lang/String;)V
    .locals 5
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 129
    iget-boolean v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bClose:Z

    if-eqz v1, :cond_1

    .line 168
    :cond_0
    :goto_0
    return-void

    .line 132
    :cond_1
    iget-boolean v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bGuideRequestFlag:Z

    if-eqz v1, :cond_2

    .line 133
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->onStopForGuide()V

    goto :goto_0

    .line 137
    :cond_2
    invoke-virtual {p0, p1}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->setReadString(Ljava/lang/String;)V

    .line 139
    iget-boolean v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bTTSInit:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_strReadTTS:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nHelperStatus:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 141
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v1}, Landroid/speech/tts/TextToSpeech;->isSpeaking()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 146
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v1}, Landroid/speech/tts/TextToSpeech;->stop()I

    move-result v0

    .line 147
    .local v0, "nRetValue":I
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->messageHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 148
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->initManageGuideFlag()V

    .line 149
    const/4 v1, 0x4

    iput v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nHelperStatus:I

    .line 150
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->messageHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 155
    .end local v0    # "nRetValue":I
    :cond_3
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    new-instance v2, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$1;

    invoke-direct {v2, p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$1;-><init>(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)V

    invoke-virtual {v1, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 164
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_strReadTTS:Ljava/lang/String;

    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->DUMMY_PARAMS:Ljava/util/HashMap;

    invoke-virtual {v1, v2, v4, v3}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    goto :goto_0
.end method

.method public stop(Z)I
    .locals 2
    .param p1, "bTTSToolbar"    # Z

    .prologue
    .line 672
    const/4 v1, 0x0

    iput v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nHelperStatus:I

    .line 673
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v1}, Landroid/speech/tts/TextToSpeech;->stop()I

    move-result v0

    .line 675
    .local v0, "nRetValue":I
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 676
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->initManageGuideFlag()V

    .line 678
    :cond_0
    if-nez p1, :cond_1

    .line 679
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v1, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onEndTextToSpeech()V

    .line 681
    :cond_1
    return v0
.end method

.method public unregisterReceiver()V
    .locals 2

    .prologue
    .line 801
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mReceiver:Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;

    if-eqz v0, :cond_0

    .line 803
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mReceiver:Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 805
    :cond_0
    return-void
.end method
