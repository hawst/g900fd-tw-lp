.class Lcom/infraware/office/baseframe/EvBaseEditorActivity$5;
.super Ljava/lang/Object;
.source "EvBaseEditorActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnResultInsertImageActivity(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)V
    .locals 0

    .prologue
    .line 1036
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$5;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1039
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$5;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$5;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$5;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mImageDataIntent:Landroid/content/Intent;
    invoke-static {v3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->access$100(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getImageFileFromWeb(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mDownloadImageFilename:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->access$002(Lcom/infraware/office/baseframe/EvBaseEditorActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 1041
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$5;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mDownloadImageFilename:Ljava/lang/String;
    invoke-static {v1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->access$000(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1042
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$5;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$5;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mDownloadImageFilename:Ljava/lang/String;
    invoke-static {v2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->access$000(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->GetExifOrientation(Ljava/lang/String;)I

    move-result v2

    # setter for: Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mSpinedImageDegree:I
    invoke-static {v1, v2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->access$202(Lcom/infraware/office/baseframe/EvBaseEditorActivity;I)I

    .line 1044
    :cond_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$5;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mProgressDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->access$300(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)Landroid/app/AlertDialog;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$5;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mProgressDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->access$300(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1045
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$5;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mProgressDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->access$300(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 1048
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$5;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 1049
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity$5;->this$0:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1051
    .end local v0    # "msg":Landroid/os/Message;
    :cond_1
    return-void
.end method
