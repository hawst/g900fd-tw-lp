.class Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;
.super Ljava/lang/Object;
.source "EvBaseView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnStylusButtonEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/EvBaseView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StylusEventListener"
.end annotation


# instance fields
.field private mEvInterface:Lcom/infraware/office/evengine/EvInterface;

.field private mPressed:Z

.field private mStartX:F

.field private mStartY:F

.field final synthetic this$0:Lcom/infraware/office/baseframe/EvBaseView;


# direct methods
.method private constructor <init>(Lcom/infraware/office/baseframe/EvBaseView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1417
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1419
    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mStartX:F

    .line 1420
    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mStartY:F

    .line 1421
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mPressed:Z

    .line 1422
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    return-void
.end method

.method synthetic constructor <init>(Lcom/infraware/office/baseframe/EvBaseView;Lcom/infraware/office/baseframe/EvBaseView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/infraware/office/baseframe/EvBaseView;
    .param p2, "x1"    # Lcom/infraware/office/baseframe/EvBaseView$1;

    .prologue
    .line 1417
    invoke-direct {p0, p1}, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;-><init>(Lcom/infraware/office/baseframe/EvBaseView;)V

    return-void
.end method


# virtual methods
.method public onStylusButtonEvent(Landroid/view/MotionEvent;I)V
    .locals 17
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "clipboardId"    # I

    .prologue
    .line 1427
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v8

    .line 1429
    .local v8, "action":I
    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    .line 1499
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1433
    :cond_1
    packed-switch v8, :pswitch_data_0

    goto :goto_0

    .line 1439
    :pswitch_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mStartX:F

    .line 1440
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mStartY:F

    .line 1442
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mStartX:F

    float-to-int v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mStartY:F

    float-to-int v4, v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v7}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 1443
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mStartX:F

    float-to-int v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mStartY:F

    float-to-int v4, v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v7}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 1444
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v1, :cond_2

    .line 1445
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->updateCaretPos(ZZ)V

    .line 1447
    :cond_2
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mPressed:Z

    goto :goto_0

    .line 1452
    :pswitch_2
    const/4 v11, 0x0

    .line 1453
    .local v11, "endX":F
    const/4 v12, 0x0

    .line 1455
    .local v12, "endY":F
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mPressed:Z

    if-eqz v1, :cond_0

    .line 1458
    new-instance v14, Landroid/graphics/Rect;

    invoke-direct {v14}, Landroid/graphics/Rect;-><init>()V

    .line 1459
    .local v14, "rect":Landroid/graphics/Rect;
    new-instance v13, Landroid/graphics/Point;

    invoke-direct {v13}, Landroid/graphics/Point;-><init>()V

    .line 1460
    .local v13, "offset":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->this$0:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1, v14, v13}, Lcom/infraware/office/baseframe/EvBaseView;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    .line 1461
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v11

    .line 1462
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v12

    .line 1463
    move-object/from16 v0, p0

    iget v1, v0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mStartX:F

    iget v2, v13, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mStartX:F

    .line 1464
    move-object/from16 v0, p0

    iget v1, v0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mStartY:F

    iget v2, v13, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mStartY:F

    .line 1465
    move-object/from16 v0, p0

    iget v1, v0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mStartX:F

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gez v1, :cond_3

    .line 1466
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mStartX:F

    .line 1468
    :cond_3
    move-object/from16 v0, p0

    iget v1, v0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mStartY:F

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gez v1, :cond_4

    .line 1469
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mStartY:F

    .line 1471
    :cond_4
    iget v1, v13, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    sub-float/2addr v11, v1

    .line 1472
    iget v1, v13, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    sub-float/2addr v12, v1

    .line 1473
    const/4 v1, 0x0

    cmpg-float v1, v11, v1

    if-gez v1, :cond_5

    .line 1474
    const/4 v11, 0x0

    .line 1476
    :cond_5
    const/4 v1, 0x0

    cmpg-float v1, v12, v1

    if-gez v1, :cond_6

    .line 1477
    const/4 v12, 0x0

    .line 1480
    :cond_6
    const/4 v15, 0x0

    .line 1481
    .local v15, "sX":I
    const/16 v16, 0x0

    .line 1482
    .local v16, "sY":I
    const/4 v9, 0x0

    .line 1483
    .local v9, "eX":I
    const/4 v10, 0x0

    .line 1484
    .local v10, "eY":I
    move-object/from16 v0, p0

    iget v1, v0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mStartX:F

    float-to-int v15, v1

    .line 1485
    move-object/from16 v0, p0

    iget v1, v0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mStartY:F

    float-to-int v0, v1

    move/from16 v16, v0

    .line 1486
    float-to-int v9, v11

    .line 1487
    float-to-int v10, v12

    .line 1489
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move/from16 v0, v16

    invoke-virtual {v1, v15, v0, v9, v10}, Lcom/infraware/office/evengine/EvInterface;->ISetMarkingByPen(IIII)V

    .line 1491
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mPressed:Z

    goto/16 :goto_0

    .line 1495
    .end local v9    # "eX":I
    .end local v10    # "eY":I
    .end local v11    # "endX":F
    .end local v12    # "endY":F
    .end local v13    # "offset":Landroid/graphics/Point;
    .end local v14    # "rect":Landroid/graphics/Rect;
    .end local v15    # "sX":I
    .end local v16    # "sY":I
    :pswitch_3
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;->mPressed:Z

    goto/16 :goto_0

    .line 1433
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
