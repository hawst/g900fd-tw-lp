.class public Lcom/infraware/office/baseframe/porting/EvPrintHelper;
.super Ljava/lang/Object;
.source "EvPrintHelper.java"


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    return-void
.end method

.method public static isSupportMobilePrint(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 23
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string/jumbo v3, "com.sec.android.app.mobileprint"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 24
    const/4 v1, 0x1

    .line 27
    :goto_0
    return v1

    .line 25
    :catch_0
    move-exception v0

    .line 27
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0
.end method

.method public static isSupportPrint(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 15
    invoke-static {p0}, Lcom/infraware/common/define/CMModelDefine$B;->USE_GOOGLE_PRINT(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_POLARIS_PRINT()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 16
    :cond_0
    const/4 v0, 0x1

    .line 18
    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, Lcom/infraware/office/baseframe/porting/EvPrintHelper;->isSupportMobilePrint(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method
