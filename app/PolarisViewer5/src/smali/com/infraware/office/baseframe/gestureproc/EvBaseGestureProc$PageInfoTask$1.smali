.class Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask$1;
.super Ljava/lang/Object;
.source "EvBaseGestureProc.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;)V
    .locals 0

    .prologue
    .line 663
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask$1;->this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const-wide/16 v5, 0x28a

    const/4 v10, 0x0

    const/4 v4, 0x0

    .line 667
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask$1;->this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;

    iget-wide v2, v2, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;->mStartTime:J

    sub-long v7, v0, v2

    .line 668
    .local v7, "diffTime":J
    cmp-long v0, v7, v5

    if-ltz v0, :cond_1

    .line 669
    const-wide/16 v0, 0x5dc

    cmp-long v0, v7, v0

    if-ltz v0, :cond_2

    .line 670
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask$1;->this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iput v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mScrollBarAlpha:I

    .line 671
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask$1;->this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 672
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask$1;->this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 673
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask$1;->this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iput-object v10, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    .line 675
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask$1;->this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iput-boolean v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mbScrollbar:Z

    .line 677
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask$1;->this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0, v4}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->setPageInfoType(I)V

    .line 702
    :cond_1
    :goto_0
    return-void

    .line 679
    :cond_2
    cmp-long v0, v7, v5

    if-lez v0, :cond_3

    const-wide/16 v0, 0x352

    cmp-long v0, v7, v0

    if-gez v0, :cond_3

    .line 680
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask$1;->this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    const/high16 v1, 0x43660000    # 230.0f

    sub-long v2, v7, v5

    long-to-float v2, v2

    const v3, 0x3f933333    # 1.15f

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mScrollBarAlpha:I

    .line 682
    :try_start_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask$1;->this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 683
    :catch_0
    move-exception v9

    .line 684
    .local v9, "e":Ljava/lang/NullPointerException;
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask$1;->this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 685
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask$1;->this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 686
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask$1;->this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iput-object v10, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    goto :goto_0

    .line 691
    .end local v9    # "e":Ljava/lang/NullPointerException;
    :cond_3
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask$1;->this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iput v4, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mScrollBarAlpha:I

    .line 693
    :try_start_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask$1;->this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 694
    :catch_1
    move-exception v9

    .line 695
    .restart local v9    # "e":Ljava/lang/NullPointerException;
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask$1;->this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 696
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask$1;->this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 697
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask$1;->this$1:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoTask;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iput-object v10, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoTimer:Ljava/util/Timer;

    goto :goto_0
.end method
