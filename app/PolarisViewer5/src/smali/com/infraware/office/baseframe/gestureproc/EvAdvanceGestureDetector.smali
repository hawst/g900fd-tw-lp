.class public Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;
.super Ljava/lang/Object;
.source "EvAdvanceGestureDetector.java"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;
.implements Landroid/view/GestureDetector$OnGestureListener;
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;,
        Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$GestureHandler;
    }
.end annotation


# static fields
.field private static final DOUBLE_TAP_TIMEOUT:I

.field protected static final LOG_CAT:Ljava/lang/String; = "EvAdvanceGestureDetector"

.field protected static final STATUS_DOUBLETAP:I = 0x1

.field protected static final STATUS_FLING:I = 0x2

.field protected static final STATUS_LONGPRESS:I = 0x3

.field protected static final STATUS_NONE:I = 0x0

.field protected static final STATUS_SCALE:I = 0x4

.field protected static final STATUS_SCALE_DOWN:I = 0x5

.field private static final TAP:I = 0x3


# instance fields
.field private mAlwaysInBiggerTapRegion:Z

.field private mAlwaysInMoveRegion:Z

.field private mAlwaysInTapRegion:Z

.field private mBiggerTouchSlopSquare:I

.field private mCurrentDownEvent:Landroid/view/MotionEvent;

.field protected mDetector:Landroid/view/GestureDetector;

.field protected mDoubleTapBeginTime:J

.field private mDoubleTapSlopSquare:I

.field private final mHandler:Landroid/os/Handler;

.field private mLastMotionX:F

.field private mLastMotionY:F

.field protected mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

.field private mPenMode:Z

.field private mPreviousUpEvent:Landroid/view/MotionEvent;

.field protected mScaleDetector:Landroid/view/ScaleGestureDetector;

.field private mStillDown:Z

.field private mTouchSlopSquare:I

.field protected mTouchState:I

.field protected mView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v0

    sput v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->DOUBLE_TAP_TIMEOUT:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "listener"    # Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    .prologue
    const/4 v5, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput v5, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    .line 29
    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mDoubleTapBeginTime:J

    .line 31
    const/16 v3, 0x190

    iput v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mBiggerTouchSlopSquare:I

    .line 38
    iput-boolean v5, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mAlwaysInMoveRegion:Z

    .line 39
    iput-boolean v5, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mPenMode:Z

    .line 52
    new-instance v3, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$GestureHandler;

    invoke-direct {v3, p0}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$GestureHandler;-><init>(Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;)V

    iput-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mHandler:Landroid/os/Handler;

    .line 57
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    .line 83
    if-nez p3, :cond_0

    .line 84
    new-instance v3, Ljava/lang/NullPointerException;

    const-string/jumbo v4, "EvAdvanceGestureDetector must not be null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 87
    :cond_0
    iput-object p2, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mView:Landroid/view/View;

    .line 88
    new-instance v3, Landroid/view/GestureDetector;

    invoke-direct {v3, p1, p0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mDetector:Landroid/view/GestureDetector;

    .line 89
    new-instance v3, Landroid/view/ScaleGestureDetector;

    invoke-direct {v3, p1, p0}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    .line 90
    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mDetector:Landroid/view/GestureDetector;

    invoke-virtual {v3, p0}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 91
    iput-object p3, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    .line 94
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 95
    .local v0, "configuration":Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    .line 96
    .local v2, "touchSlop":I
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledDoubleTapSlop()I

    move-result v1

    .line 97
    .local v1, "doubleTapSlop":I
    mul-int v3, v2, v2

    iput v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchSlopSquare:I

    .line 98
    mul-int v3, v1, v1

    iput v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mDoubleTapSlopSquare:I

    .line 99
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mStillDown:Z

    return v0
.end method

.method static synthetic access$100(Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;)Landroid/view/MotionEvent;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    return-object v0
.end method


# virtual methods
.method public AdvanceGesturefinalize()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 103
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mDetector:Landroid/view/GestureDetector;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 106
    :cond_0
    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mView:Landroid/view/View;

    .line 107
    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mDetector:Landroid/view/GestureDetector;

    .line 108
    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    .line 109
    return-void
.end method

.method public SetAlwaysInMoveRegion(Z)V
    .locals 0
    .param p1, "alwaysInMoveRegion"    # Z

    .prologue
    .line 117
    iput-boolean p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mAlwaysInMoveRegion:Z

    .line 118
    return-void
.end method

.method public SetPenMode(Z)V
    .locals 0
    .param p1, "bPenMode"    # Z

    .prologue
    .line 121
    iput-boolean p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mPenMode:Z

    .line 122
    return-void
.end method

.method public cancel()V
    .locals 2

    .prologue
    .line 422
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 423
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mStillDown:Z

    .line 424
    return-void
.end method

.method protected dumpEvent(Landroid/view/MotionEvent;)V
    .locals 9
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x6

    const/4 v7, 0x5

    .line 517
    const/16 v5, 0xa

    new-array v3, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "DOWN"

    aput-object v6, v3, v5

    const/4 v5, 0x1

    const-string/jumbo v6, "UP"

    aput-object v6, v3, v5

    const/4 v5, 0x2

    const-string/jumbo v6, "MOVE"

    aput-object v6, v3, v5

    const/4 v5, 0x3

    const-string/jumbo v6, "CANCEL"

    aput-object v6, v3, v5

    const/4 v5, 0x4

    const-string/jumbo v6, "OUTSIDE"

    aput-object v6, v3, v5

    const-string/jumbo v5, "POINTER_DOWN"

    aput-object v5, v3, v7

    const-string/jumbo v5, "POINTER_UP"

    aput-object v5, v3, v8

    const/4 v5, 0x7

    const-string/jumbo v6, "7?"

    aput-object v6, v3, v5

    const/16 v5, 0x8

    const-string/jumbo v6, "8?"

    aput-object v6, v3, v5

    const/16 v5, 0x9

    const-string/jumbo v6, "9?"

    aput-object v6, v3, v5

    .line 520
    .local v3, "names":[Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 521
    .local v4, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 522
    .local v0, "action":I
    and-int/lit16 v1, v0, 0xff

    .line 523
    .local v1, "actionCode":I
    const-string/jumbo v5, "event ACTION_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v3, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 525
    if-eq v1, v7, :cond_0

    if-ne v1, v8, :cond_1

    .line 526
    :cond_0
    const-string/jumbo v5, "(pid "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    shr-int/lit8 v6, v0, 0x8

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 527
    const-string/jumbo v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 530
    :cond_1
    const-string/jumbo v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 531
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v5

    if-ge v2, v5, :cond_3

    .line 532
    const-string/jumbo v5, "#"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 533
    const-string/jumbo v5, "(pid "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 534
    const-string/jumbo v5, ")="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 535
    const-string/jumbo v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 536
    add-int/lit8 v5, v2, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v6

    if-ge v5, v6, :cond_2

    .line 537
    const-string/jumbo v5, ";"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 531
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 539
    :cond_3
    const-string/jumbo v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 540
    const-string/jumbo v5, "Event dump"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    return-void
.end method

.method public getLastMotionX()F
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mLastMotionX:F

    return v0
.end method

.method public getLastMotionY()F
    .locals 1

    .prologue
    .line 131
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mLastMotionY:F

    return v0
.end method

.method protected isConsideredDoubleTap(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "firstDown"    # Landroid/view/MotionEvent;
    .param p2, "firstUp"    # Landroid/view/MotionEvent;
    .param p3, "secondDown"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 428
    iget-boolean v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mAlwaysInBiggerTapRegion:Z

    if-nez v3, :cond_1

    .line 438
    :cond_0
    :goto_0
    return v2

    .line 432
    :cond_1
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    sub-long/2addr v3, v5

    sget v5, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->DOUBLE_TAP_TIMEOUT:I

    int-to-long v5, v5

    cmp-long v3, v3, v5

    if-gtz v3, :cond_0

    .line 436
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    sub-int v0, v3, v4

    .line 437
    .local v0, "deltaX":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    sub-int v1, v3, v4

    .line 438
    .local v1, "deltaY":I
    mul-int v3, v0, v0

    mul-int v4, v1, v1

    add-int/2addr v3, v4

    iget v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mDoubleTapSlopSquare:I

    if-ge v3, v4, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public isPenDrawing()Z
    .locals 1

    .prologue
    .line 573
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->isPenMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mStillDown:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPenMode()Z
    .locals 1

    .prologue
    .line 124
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mPenMode:Z

    return v0
.end method

.method public final onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 446
    const-string/jumbo v1, "EvAdvanceGestureDetector"

    const-string/jumbo v2, "mTouchState = STATUS_DOUBLETAP"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    iget-boolean v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mPenMode:Z

    if-ne v1, v3, :cond_1

    .line 456
    :cond_0
    :goto_0
    return v0

    .line 450
    :cond_1
    iput v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    .line 451
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mDoubleTapBeginTime:J

    .line 453
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    if-eqz v1, :cond_0

    .line 454
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 442
    const/4 v0, 0x0

    return v0
.end method

.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 471
    const/4 v0, 0x0

    return v0
.end method

.method public final onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v0, 0x0

    .line 475
    iget-boolean v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mPenMode:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 482
    :cond_0
    :goto_0
    return v0

    .line 478
    :cond_1
    const/4 v1, 0x2

    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    .line 479
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    if-eqz v1, :cond_0

    .line 480
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    goto :goto_0
.end method

.method public final onLongPress(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x1

    .line 487
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mPenMode:Z

    if-ne v0, v1, :cond_1

    .line 495
    :cond_0
    :goto_0
    return-void

    .line 490
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 491
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    if-eq v0, v1, :cond_0

    .line 492
    iput v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    .line 493
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;->onLongPress(Landroid/view/MotionEvent;)V

    goto :goto_0
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 2
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 544
    const-string/jumbo v0, "HYOHYUN"

    const-string/jumbo v1, "onScale"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    const/4 v0, 0x4

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    .line 546
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    if-eqz v0, :cond_0

    .line 547
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;->onScale(Landroid/view/ScaleGestureDetector;)Z

    move-result v0

    .line 549
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 2
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 554
    const-string/jumbo v0, "HYOHYUN"

    const-string/jumbo v1, "onScaleBegin"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    const/4 v0, 0x5

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    .line 556
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    if-eqz v0, :cond_0

    .line 557
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;->onScaleBegin(Landroid/view/ScaleGestureDetector;)Z

    move-result v0

    .line 559
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 2
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 565
    const-string/jumbo v0, "HYOHYUN"

    const-string/jumbo v1, "onScaleEnd"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    .line 567
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    if-eqz v0, :cond_0

    .line 568
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;->onScaleEnd(Landroid/view/ScaleGestureDetector;)V

    .line 569
    :cond_0
    return-void
.end method

.method public final onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    const/4 v0, 0x0

    .line 499
    iget-boolean v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mPenMode:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 505
    :cond_0
    :goto_0
    return v0

    .line 502
    :cond_1
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    if-eqz v1, :cond_0

    .line 503
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;->onTouchDrag(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    goto :goto_0
.end method

.method public final onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 509
    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 467
    const/4 v0, 0x0

    return v0
.end method

.method public final onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 512
    const/4 v0, 0x0

    return v0
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 23
    .param p1, "v"    # Landroid/view/View;
    .param p2, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 259
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mView:Landroid/view/View;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    if-ne v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mDetector:Landroid/view/GestureDetector;

    move-object/from16 v17, v0

    if-eqz v17, :cond_4

    .line 260
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mAlwaysInMoveRegion:Z

    move/from16 v17, v0

    if-nez v17, :cond_0

    .line 261
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mDetector:Landroid/view/GestureDetector;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 262
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 265
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mPenMode:Z

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_1

    .line 266
    invoke-virtual/range {p0 .. p2}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->onTouchForPenMode(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v12

    .line 418
    :goto_0
    return v12

    .line 271
    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v16

    .line 272
    .local v16, "y":F
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v15

    .line 273
    .local v15, "x":F
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v17

    move/from16 v0, v17

    and-int/lit16 v4, v0, 0xff

    .line 275
    .local v4, "action":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    move/from16 v17, v0

    const/16 v18, 0x3

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_3

    .line 276
    :cond_2
    packed-switch v4, :pswitch_data_0

    .line 282
    const-string/jumbo v17, "EvAdvanceGestureDetector"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "Oops!! mTouchState = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    const-string/jumbo v17, "HYOHYUN"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "Oops!! mTouchState = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    .line 291
    :cond_3
    :pswitch_0
    packed-switch v4, :pswitch_data_1

    .line 418
    .end local v4    # "action":I
    .end local v15    # "x":F
    .end local v16    # "y":F
    :cond_4
    :goto_1
    :pswitch_1
    const/4 v12, 0x0

    goto :goto_0

    .line 294
    .restart local v4    # "action":I
    .restart local v15    # "x":F
    .restart local v16    # "y":F
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    const/16 v18, 0x3

    invoke-virtual/range {v17 .. v18}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v11

    .line 295
    .local v11, "hadTapMessage":Z
    if-eqz v11, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    const/16 v18, 0x3

    invoke-virtual/range {v17 .. v18}, Landroid/os/Handler;->removeMessages(I)V

    .line 297
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v17, v0

    if-eqz v17, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v17, v0

    if-eqz v17, :cond_7

    if-eqz v11, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    move-object/from16 v3, p2

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->isConsideredDoubleTap(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 304
    :goto_2
    move-object/from16 v0, p0

    iput v15, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mLastMotionX:F

    .line 305
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mLastMotionY:F

    .line 306
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v17, v0

    if-eqz v17, :cond_6

    .line 307
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/MotionEvent;->recycle()V

    .line 309
    :cond_6
    invoke-static/range {p2 .. p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    .line 310
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mAlwaysInTapRegion:Z

    .line 311
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mAlwaysInBiggerTapRegion:Z

    .line 312
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mStillDown:Z

    .line 314
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    move-object/from16 v17, v0

    if-eqz v17, :cond_4

    .line 315
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-interface {v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;->onTouchDown(Landroid/view/MotionEvent;)Z

    move-result v12

    goto/16 :goto_0

    .line 301
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    const/16 v18, 0x3

    sget v19, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->DOUBLE_TAP_TIMEOUT:I

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v19, v0

    invoke-virtual/range {v17 .. v20}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_2

    .line 321
    .end local v11    # "hadTapMessage":Z
    :pswitch_3
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mStillDown:Z

    .line 322
    invoke-static/range {p2 .. p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v5

    .line 323
    .local v5, "currentUpEvent":Landroid/view/MotionEvent;
    const/4 v12, 0x0

    .line 324
    .local v12, "result":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    move-object/from16 v17, v0

    if-eqz v17, :cond_8

    .line 325
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-interface {v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;->onTouchUp(Landroid/view/MotionEvent;)Z

    move-result v12

    .line 326
    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_c

    .line 327
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    const/16 v18, 0x3

    invoke-virtual/range {v17 .. v18}, Landroid/os/Handler;->removeMessages(I)V

    .line 328
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v17

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mDoubleTapBeginTime:J

    move-wide/from16 v19, v0

    sub-long v17, v17, v19

    const-wide/16 v19, 0x15e

    cmp-long v17, v17, v19

    if-gez v17, :cond_b

    .line 329
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    move-object/from16 v17, v0

    if-eqz v17, :cond_9

    .line 330
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-interface {v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;->onDoubleTapConfirmed(Landroid/view/MotionEvent;)Z

    .line 340
    :cond_9
    :goto_3
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    .line 342
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v17, v0

    if-eqz v17, :cond_a

    .line 343
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/MotionEvent;->recycle()V

    .line 346
    :cond_a
    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    goto/16 :goto_0

    .line 333
    :cond_b
    const-string/jumbo v17, "EvAdvanceGestureDetector"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "Double tap canceled, endTime - startTime = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v19

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mDoubleTapBeginTime:J

    move-wide/from16 v21, v0

    sub-long v19, v19, v21

    invoke-virtual/range {v18 .. v20}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 335
    :cond_c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    move/from16 v17, v0

    const/16 v18, 0x3

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_9

    .line 337
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    move-object/from16 v17, v0

    if-eqz v17, :cond_9

    .line 338
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-interface {v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;->onDoubleTapConfirmed(Landroid/view/MotionEvent;)Z

    goto :goto_3

    .line 351
    .end local v5    # "currentUpEvent":Landroid/view/MotionEvent;
    .end local v12    # "result":Z
    :pswitch_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mLastMotionX:F

    move/from16 v17, v0

    sub-float v13, v17, v15

    .line 352
    .local v13, "scrollX":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mLastMotionY:F

    move/from16 v17, v0

    sub-float v14, v17, v16

    .line 353
    .local v14, "scrollY":F
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mAlwaysInMoveRegion:Z

    move/from16 v17, v0

    if-eqz v17, :cond_d

    .line 354
    move-object/from16 v0, p0

    iput v15, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mLastMotionX:F

    .line 355
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mLastMotionY:F

    .line 356
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, p2

    invoke-interface {v0, v1, v2, v13, v14}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;->onTouchDrag(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v12

    goto/16 :goto_0

    .line 358
    :cond_d
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v17

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_e

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_e

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    move/from16 v17, v0

    const/16 v18, 0x3

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_12

    .line 361
    :cond_e
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mAlwaysInTapRegion:Z

    move/from16 v17, v0

    if-eqz v17, :cond_10

    .line 362
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/MotionEvent;->getX()F

    move-result v17

    sub-float v17, v15, v17

    move/from16 v0, v17

    float-to-int v6, v0

    .line 363
    .local v6, "deltaX":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/MotionEvent;->getY()F

    move-result v17

    sub-float v17, v16, v17

    move/from16 v0, v17

    float-to-int v7, v0

    .line 364
    .local v7, "deltaY":I
    mul-int v17, v6, v6

    mul-int v18, v7, v7

    add-int v8, v17, v18

    .line 365
    .local v8, "distance":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchSlopSquare:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-le v8, v0, :cond_f

    .line 366
    move-object/from16 v0, p0

    iput v15, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mLastMotionX:F

    .line 367
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mLastMotionY:F

    .line 368
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mAlwaysInTapRegion:Z

    .line 369
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    const/16 v18, 0x3

    invoke-virtual/range {v17 .. v18}, Landroid/os/Handler;->removeMessages(I)V

    .line 371
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    move-object/from16 v17, v0

    if-eqz v17, :cond_f

    .line 372
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, p2

    invoke-interface {v0, v1, v2, v13, v14}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;->onTouchDrag(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v12

    goto/16 :goto_0

    .line 375
    :cond_f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mBiggerTouchSlopSquare:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-le v8, v0, :cond_4

    .line 376
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mAlwaysInBiggerTapRegion:Z

    goto/16 :goto_1

    .line 378
    .end local v6    # "deltaX":I
    .end local v7    # "deltaY":I
    .end local v8    # "distance":I
    :cond_10
    invoke-static {v13}, Ljava/lang/Math;->abs(F)F

    move-result v17

    const/high16 v18, 0x3f800000    # 1.0f

    cmpl-float v17, v17, v18

    if-gez v17, :cond_11

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v17

    const/high16 v18, 0x3f800000    # 1.0f

    cmpl-float v17, v17, v18

    if-ltz v17, :cond_4

    .line 379
    :cond_11
    move-object/from16 v0, p0

    iput v15, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mLastMotionX:F

    .line 380
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mLastMotionY:F

    .line 382
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    move-object/from16 v17, v0

    if-eqz v17, :cond_4

    .line 383
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, p2

    invoke-interface {v0, v1, v2, v13, v14}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;->onTouchDrag(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v12

    goto/16 :goto_0

    .line 389
    :cond_12
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mAlwaysInTapRegion:Z

    move/from16 v17, v0

    if-eqz v17, :cond_13

    .line 391
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/MotionEvent;->getX()F

    move-result v17

    sub-float v17, v15, v17

    move/from16 v0, v17

    float-to-int v9, v0

    .line 392
    .local v9, "dx":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/MotionEvent;->getY()F

    move-result v17

    sub-float v17, v16, v17

    move/from16 v0, v17

    float-to-int v10, v0

    .line 393
    .local v10, "dy":I
    mul-int v17, v9, v9

    mul-int v18, v10, v10

    add-int v8, v17, v18

    .line 394
    .restart local v8    # "distance":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchSlopSquare:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-le v8, v0, :cond_13

    .line 395
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mAlwaysInTapRegion:Z

    .line 396
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->cancel()V

    .line 400
    .end local v8    # "distance":I
    .end local v9    # "dx":I
    .end local v10    # "dy":I
    :cond_13
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 405
    .end local v13    # "scrollX":F
    .end local v14    # "scrollY":F
    :pswitch_5
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->cancel()V

    goto/16 :goto_1

    .line 411
    :pswitch_6
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->cancel()V

    .line 412
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    move-object/from16 v17, v0

    if-eqz v17, :cond_4

    .line 413
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-interface {v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;->onCancel(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    .line 276
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 291
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_1
        :pswitch_5
        :pswitch_1
    .end packed-switch
.end method

.method public final onTouchForPenMode(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 15
    .param p1, "v"    # Landroid/view/View;
    .param p2, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 153
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    .line 154
    .local v8, "y":F
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    .line 155
    .local v7, "x":F
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    and-int/lit16 v1, v9, 0xff

    .line 160
    .local v1, "action":I
    iget v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    const/4 v10, 0x4

    if-eq v9, v10, :cond_0

    .line 161
    const-string/jumbo v9, "HYOHYUN"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "onTouchForPenMode = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    :cond_0
    iget v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    const/4 v10, 0x4

    if-ne v9, v10, :cond_1

    .line 164
    const/4 v4, 0x0

    .line 254
    :goto_0
    return v4

    .line 166
    :cond_1
    iget v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    const/4 v10, 0x1

    if-eq v9, v10, :cond_2

    iget v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    const/4 v10, 0x3

    if-ne v9, v10, :cond_3

    .line 167
    :cond_2
    packed-switch v1, :pswitch_data_0

    .line 173
    const-string/jumbo v9, "EvAdvanceGestureDetector"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Oops!! mTouchState = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    const-string/jumbo v9, "HYOHYUN"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Oops!! mTouchState = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    const/4 v9, 0x0

    iput v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    .line 181
    :cond_3
    :pswitch_0
    packed-switch v1, :pswitch_data_1

    .line 254
    :cond_4
    :goto_1
    :pswitch_1
    const/4 v4, 0x0

    goto :goto_0

    .line 184
    :pswitch_2
    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v10, 0x3

    invoke-virtual {v9, v10}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v3

    .line 185
    .local v3, "hadTapMessage":Z
    if-eqz v3, :cond_5

    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v10, 0x3

    invoke-virtual {v9, v10}, Landroid/os/Handler;->removeMessages(I)V

    .line 187
    :cond_5
    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    if-eqz v9, :cond_7

    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    if-eqz v9, :cond_7

    if-eqz v3, :cond_7

    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    iget-object v10, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v0, p2

    invoke-virtual {p0, v9, v10, v0}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->isConsideredDoubleTap(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 194
    :goto_2
    iput v7, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mLastMotionX:F

    .line 195
    iput v8, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mLastMotionY:F

    .line 196
    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    if-eqz v9, :cond_6

    .line 197
    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    invoke-virtual {v9}, Landroid/view/MotionEvent;->recycle()V

    .line 199
    :cond_6
    invoke-static/range {p2 .. p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v9

    iput-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    .line 200
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mAlwaysInTapRegion:Z

    .line 201
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mAlwaysInBiggerTapRegion:Z

    .line 202
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mStillDown:Z

    .line 204
    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    if-eqz v9, :cond_4

    .line 205
    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    move-object/from16 v0, p2

    invoke-interface {v9, v0}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;->onTouchDown(Landroid/view/MotionEvent;)Z

    move-result v4

    goto/16 :goto_0

    .line 191
    :cond_7
    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v10, 0x3

    sget v11, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->DOUBLE_TAP_TIMEOUT:I

    int-to-long v11, v11

    invoke-virtual {v9, v10, v11, v12}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_2

    .line 210
    .end local v3    # "hadTapMessage":Z
    :pswitch_3
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mStillDown:Z

    .line 211
    invoke-static/range {p2 .. p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v2

    .line 212
    .local v2, "currentUpEvent":Landroid/view/MotionEvent;
    const/4 v4, 0x0

    .line 213
    .local v4, "result":Z
    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    if-eqz v9, :cond_8

    .line 214
    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    move-object/from16 v0, p2

    invoke-interface {v9, v0}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;->onTouchUp(Landroid/view/MotionEvent;)Z

    move-result v4

    .line 215
    :cond_8
    iget v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_9

    .line 216
    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v10, 0x3

    invoke-virtual {v9, v10}, Landroid/os/Handler;->removeMessages(I)V

    .line 217
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v9

    iget-wide v11, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mDoubleTapBeginTime:J

    sub-long/2addr v9, v11

    const-wide/16 v11, 0x15e

    cmp-long v9, v9, v11

    if-gez v9, :cond_b

    .line 218
    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    if-eqz v9, :cond_9

    .line 219
    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    move-object/from16 v0, p2

    invoke-interface {v9, v0}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;->onDoubleTapConfirmed(Landroid/view/MotionEvent;)Z

    .line 224
    :cond_9
    :goto_3
    const/4 v9, 0x0

    iput v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mTouchState:I

    .line 226
    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    if-eqz v9, :cond_a

    .line 227
    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    invoke-virtual {v9}, Landroid/view/MotionEvent;->recycle()V

    .line 230
    :cond_a
    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    goto/16 :goto_0

    .line 222
    :cond_b
    const-string/jumbo v9, "EvAdvanceGestureDetector"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Double tap canceled, endTime - startTime = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v11

    iget-wide v13, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mDoubleTapBeginTime:J

    sub-long/2addr v11, v13

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 235
    .end local v2    # "currentUpEvent":Landroid/view/MotionEvent;
    .end local v4    # "result":Z
    :pswitch_4
    iget v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mLastMotionX:F

    sub-float v5, v9, v7

    .line 236
    .local v5, "scrollX":F
    iget v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mLastMotionY:F

    sub-float v6, v9, v8

    .line 237
    .local v6, "scrollY":F
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_c

    .line 238
    iput v7, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mLastMotionX:F

    .line 239
    iput v8, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mLastMotionY:F

    .line 240
    iget-object v9, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    iget-object v10, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v0, p2

    invoke-interface {v9, v10, v0, v5, v6}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;->onTouchDrag(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v4

    goto/16 :goto_0

    .line 243
    :cond_c
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 246
    .end local v5    # "scrollX":F
    .end local v6    # "scrollY":F
    :pswitch_5
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->cancel()V

    goto/16 :goto_1

    .line 251
    :pswitch_6
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->cancel()V

    goto/16 :goto_1

    .line 167
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 181
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_1
        :pswitch_5
        :pswitch_1
    .end packed-switch
.end method

.method public setIsLongpressEnabled(Z)V
    .locals 1
    .param p1, "isLongpressEnabled"    # Z

    .prologue
    .line 112
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mDetector:Landroid/view/GestureDetector;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 114
    :cond_0
    return-void
.end method
