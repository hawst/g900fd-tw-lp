.class public Lcom/infraware/office/baseframe/gestureproc/EvWordGestureProc;
.super Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;
.source "EvWordGestureProc.java"


# instance fields
.field private final LOG_CAT:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "listener"    # Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    .prologue
    .line 11
    invoke-direct {p0, p1, p2, p3}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;)V

    .line 8
    const-string/jumbo v0, "EvWordGestureProc"

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvWordGestureProc;->LOG_CAT:Ljava/lang/String;

    .line 12
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvWordGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->setIsLongpressEnabled(Z)V

    .line 13
    return-void
.end method


# virtual methods
.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 19
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvWordGestureProc;->mActionMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 22
    :goto_0
    return-void

    .line 21
    :cond_0
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onLongPress(Landroid/view/MotionEvent;)V

    goto :goto_0
.end method
