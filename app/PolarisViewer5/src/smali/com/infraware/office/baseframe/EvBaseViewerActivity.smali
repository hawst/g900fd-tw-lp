.class public abstract Lcom/infraware/office/baseframe/EvBaseViewerActivity;
.super Lcom/infraware/common/baseactivity/BaseActivity;
.source "EvBaseViewerActivity.java"

# interfaces
.implements Lcom/infraware/common/event/SdCardListener;
.implements Lcom/infraware/office/baseframe/EvBaseE$BaseActivityEventType;
.implements Lcom/infraware/office/baseframe/EvBaseE$EV_ACTIONBAR_EVENT;
.implements Lcom/infraware/office/baseframe/EvBaseE$PopupDialogEventType;
.implements Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback$msg;
.implements Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;
.implements Lcom/infraware/office/evengine/E$EV_DOCEXTENSION_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_DOCTYPE;
.implements Lcom/infraware/office/evengine/E$EV_ERROR_CODE;
.implements Lcom/infraware/office/evengine/E$EV_EVENT_SET_ERROR_VALUE;
.implements Lcom/infraware/office/evengine/E$EV_GUI_EVENT;
.implements Lcom/infraware/office/evengine/E$EV_KEY_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_PROTECT_INFO;
.implements Lcom/infraware/office/evengine/E$EV_REPLACE_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_RES_STRING_ID;
.implements Lcom/infraware/office/evengine/E$EV_SCROLL_COMMAND_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_SCROLL_FACTOR_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_SEARCH_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_SHEET_FIND_REPLACE;
.implements Lcom/infraware/office/evengine/E$EV_THUMBNAIL_OPTION_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_VIEWMODE_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_VIEW_MODE;
.implements Lcom/infraware/office/evengine/E$EV_ZOOM_TYPE;
.implements Lcom/infraware/office/evengine/E;
.implements Lcom/infraware/office/evengine/EvListener$ViewerListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchViewHolder;,
        Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchAdapter;,
        Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupWindowDissmissListener;,
        Lcom/infraware/office/baseframe/EvBaseViewerActivity$CloseActionReceiver;,
        Lcom/infraware/office/baseframe/EvBaseViewerActivity$FreeDrawMode;,
        Lcom/infraware/office/baseframe/EvBaseViewerActivity$PrintHandle;
    }
.end annotation


# static fields
.field protected static final FALSE:I = 0x0

.field protected static final HANDLE_MODE_CLOSE_DIALOG:I = 0x3

.field public static final HANDLE_MODE_DICTIONARY_SEARCH:I = 0x378

.field protected static final HANDLE_MODE_PASSWORD_OPEN:I = 0x6

.field protected static final HANDLE_MODE_PRINT_OPEN:I = 0x4

.field protected static final HANDLE_MODE_PRINT_OPEN_ERROR:I = 0x5

.field protected static final HANDLE_MODE_SEARCH:I = 0x1

.field protected static final HANDLE_MODE_SEARCH_PROGRESS:I = 0x2

.field private static final RUN_SEARCH_PROGRESS_TIME:J = 0x320L

.field protected static final TRUE:I = 0x1

.field protected static final dialogId:Ljava/lang/String;

.field protected static mErrorAndClosePopup:Landroid/app/AlertDialog;

.field protected static final searchContent:Ljava/lang/String;

.field protected static final searchResultCode:Ljava/lang/String;


# instance fields
.field private final LOG_CAT:Ljava/lang/String;

.field RunnableUpdateMainActionBar:Ljava/lang/Runnable;

.field protected bEncryptDoc:Z

.field findWatcher:Landroid/text/TextWatcher;

.field private isErrDismiss:Z

.field private mActivity:Landroid/app/Activity;

.field public mAutoSaveTest:I

.field private mBookMarkPath:Ljava/lang/String;

.field protected mCheckBox:Landroid/widget/CheckBox;

.field protected mCheckOriginalKeep:Z

.field mClickListener:Landroid/view/View$OnClickListener;

.field protected mClipBoard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

.field private mCloseReceiver:Lcom/infraware/office/baseframe/EvBaseViewerActivity$CloseActionReceiver;

.field public mConfigurationChangeNotifier:Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;

.field protected mContentSearchWord:Ljava/lang/String;

.field mDismissListener:Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupWindowDissmissListener;

.field private mDlgMsg:Landroid/app/AlertDialog;

.field public mDocExtensionType:I

.field public mDocType:I

.field protected mEV_VIEW_MODE:I

.field mEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

.field private mEvBaseEditorActivity:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

.field public mEvInterface:Lcom/infraware/office/evengine/EvInterface;

.field public mEvPrintHelper:Lcom/infraware/office/baseframe/porting/EvPrintHelper;

.field protected mFindBar:Landroid/widget/LinearLayout;

.field private mFindDelete:Lcom/infraware/common/control/custom/DeleteImageButton;

.field mFindKeyListener:Landroid/view/View$OnKeyListener;

.field private mFindNextBtn:Landroid/widget/ImageButton;

.field private mFindPrevBtn:Landroid/widget/ImageButton;

.field private mFindReplaceModeBtn:Landroid/widget/Button;

.field private mFindReplaceText:Ljava/lang/String;

.field private mFindSetBtn:Landroid/widget/ImageButton;

.field private mFindSetBtnIsLatestClicked:Z

.field private mFindText:Landroid/widget/EditText;

.field protected mFreeDrawBar:Landroid/view/View;

.field private mFreeDrawClickListener:Landroid/view/View$OnClickListener;

.field protected mFreeDrawMode:I

.field protected mFreeDrawOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

.field private mHandAnimationView:Landroid/widget/LinearLayout;

.field protected mInternalCmdType:I

.field public mIsContentSearch:Z

.field mIsMultiWindow:Z

.field protected mIsViewerMode:Z

.field private mLoadFailStrID:I

.field mMWDnDOperator:Lcom/infraware/common/multiwindow/MWDnDOperator;

.field public mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

.field protected mMainLinearLayout:Landroid/view/View;

.field private mMimeType:Ljava/lang/String;

.field protected mMsgPopup:Landroid/app/AlertDialog;

.field protected mOfficePrintManager:Lcom/infraware/polarisoffice5/print/OfficePrintManager;

.field mOnClickListener:Landroid/content/DialogInterface$OnClickListener;

.field mOnDragListener:Landroid/view/View$OnDragListener;

.field mOnLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

.field private mPdfViwerActivity:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

.field private mPenColor:I

.field protected mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

.field private mPenSize:I

.field private mPenType:I

.field private mPopup:Landroid/widget/PopupWindow;

.field private mPowerSaveOption:Lcom/infraware/office/baseframe/porting/EvPowerSaveOption;

.field private mPrintAll:Landroid/widget/RadioButton;

.field private mPrintCount:I

.field private mPrintCurPage:I

.field private mPrintCurrent:Landroid/widget/RadioButton;

.field mPrintEditFocusChange:Landroid/view/View$OnFocusChangeListener;

.field private mPrintEditLayout:Landroid/widget/LinearLayout;

.field private mPrintEndEdit:Landroid/widget/EditText;

.field private mPrintFilePath:Ljava/lang/String;

.field private mPrintPage:Landroid/widget/RadioButton;

.field private mPrintPageCount:I

.field private mPrintPagePopup:Landroid/app/AlertDialog;

.field private mPrintPath:Ljava/lang/String;

.field private mPrintPrevDlg:Landroid/app/AlertDialog;

.field private mPrintProgressPopup:Landroid/app/AlertDialog;

.field private mPrintProgressText:Landroid/widget/TextView;

.field private mPrintStartEdit:Landroid/widget/EditText;

.field private mProgress:Landroid/widget/ProgressBar;

.field protected mQuickScroll:Lcom/infraware/polarisoffice5/common/QuickScrollView;

.field private mRect:Landroid/graphics/Rect;

.field private mReplaceAllBtn:Landroid/widget/ImageButton;

.field protected mReplaceBar:Landroid/widget/LinearLayout;

.field private mReplaceBtn:Landroid/widget/ImageButton;

.field mReplaceKeyListener:Landroid/view/View$OnKeyListener;

.field private mReplaceNextBtn:Landroid/widget/ImageButton;

.field private mReplaceSetBtn:Landroid/widget/ImageButton;

.field private mReplace_DstTxt:Landroid/widget/EditText;

.field private mReplace_OrgTxt:Landroid/widget/EditText;

.field protected mRequestThumbnailOnSave:Z

.field private mRootPath:Ljava/lang/String;

.field mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

.field protected mTempPath:Ljava/lang/String;

.field mTempRefreshHandler:Landroid/os/Handler;

.field mTempRefreshRun:Ljava/lang/Runnable;

.field private mThumbnail:Landroid/graphics/Bitmap;

.field public mThumbnailChart:[Landroid/graphics/Bitmap;

.field private mThumbnailFileName:Ljava/lang/String;

.field private mThumbnailPath:Ljava/lang/String;

.field protected mTitleBar:Landroid/widget/LinearLayout;

.field mToast:Landroid/widget/Toast;

.field mTouchListener:Landroid/view/View$OnTouchListener;

.field private mUri:Landroid/net/Uri;

.field protected mView:Lcom/infraware/office/baseframe/EvBaseView;

.field private mViewSearchMode:I

.field private mZoomMenu:Landroid/app/AlertDialog;

.field protected m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

.field m_bByMultipleLauncher:Z

.field protected m_nPopupType:I

.field private m_nPrintEndPage:I

.field private m_nPrintStartPage:I

.field private m_oInputFilter:Lcom/infraware/polarisoffice5/common/EditInputFilter;

.field private m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

.field protected m_strStorageId:Ljava/lang/String;

.field private mbFinishCalled:Z

.field protected mbIsSavedAs:Z

.field protected mbLoadComplete:Z

.field protected mbLoadFail:Z

.field private mbMatchCase:I

.field private mbMatchWhole:I

.field protected mbPenDraw:Z

.field private mbReflowText:Z

.field protected mbSaveAndFinish:Z

.field protected mbSaveing:Z

.field private mbScroll:Z

.field protected mbSendingEMail:Z

.field private mbShowSearchBar:Z

.field public final messageHandler:Landroid/os/Handler;

.field protected mnSaveThumbOption:I

.field protected mstrEmailFolderPath:Ljava/lang/String;

.field protected mstrOpenFilePath:Ljava/lang/String;

.field protected mstrPdfExportFilePath:Ljava/lang/String;

.field protected mstrRequestThumbnailPath:Ljava/lang/String;

.field protected mstrTempFilePathForEMail:Ljava/lang/String;

.field onGoolgeSearchClickListener:Landroid/content/DialogInterface$OnClickListener;

.field onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field onNetWorkUseAlarmClickListener:Landroid/content/DialogInterface$OnClickListener;

.field onRunShareClickListener:Landroid/content/DialogInterface$OnClickListener;

.field onSamsungAppsPolairsOfficeClickListener:Landroid/content/DialogInterface$OnClickListener;

.field onSearchDlgCancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field onSearchDlgClickListener:Landroid/content/DialogInterface$OnClickListener;

.field onSearchDlgKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field onSendEmailAlarmClickListener:Landroid/content/DialogInterface$OnClickListener;

.field replaceWatcher:Landroid/text/TextWatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 191
    new-instance v0, Ljava/lang/String;

    const-string/jumbo v1, "SEARCH_RESULT_CODE"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->searchResultCode:Ljava/lang/String;

    .line 192
    new-instance v0, Ljava/lang/String;

    const-string/jumbo v1, "SEARCH_CONTENT"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->searchContent:Ljava/lang/String;

    .line 197
    new-instance v0, Ljava/lang/String;

    const-string/jumbo v1, "DIALOG_ID"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->dialogId:Ljava/lang/String;

    .line 203
    const/4 v0, 0x0

    sput-object v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mErrorAndClosePopup:Landroid/app/AlertDialog;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/16 v5, 0xff

    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 744
    invoke-direct {p0}, Lcom/infraware/common/baseactivity/BaseActivity;-><init>()V

    .line 158
    const-string/jumbo v0, "EvBaseViewerActivity"

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->LOG_CAT:Ljava/lang/String;

    .line 162
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 163
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    .line 164
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrPdfExportFilePath:Ljava/lang/String;

    .line 165
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrRequestThumbnailPath:Ljava/lang/String;

    .line 166
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    .line 168
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainLinearLayout:Landroid/view/View;

    .line 170
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mIsMultiWindow:Z

    .line 171
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 177
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbLoadComplete:Z

    .line 178
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbLoadFail:Z

    .line 181
    iput v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPopupType:I

    .line 182
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMsgPopup:Landroid/app/AlertDialog;

    .line 184
    iput v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEV_VIEW_MODE:I

    .line 186
    iput-boolean v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mIsViewerMode:Z

    .line 188
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbReflowText:Z

    .line 204
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDlgMsg:Landroid/app/AlertDialog;

    .line 213
    const/16 v0, 0x9

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mnSaveThumbOption:I

    .line 220
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mTitleBar:Landroid/widget/LinearLayout;

    .line 222
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindBar:Landroid/widget/LinearLayout;

    .line 223
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindPrevBtn:Landroid/widget/ImageButton;

    .line 224
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindNextBtn:Landroid/widget/ImageButton;

    .line 226
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindSetBtn:Landroid/widget/ImageButton;

    .line 227
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindSetBtnIsLatestClicked:Z

    .line 229
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBar:Landroid/widget/LinearLayout;

    .line 230
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceNextBtn:Landroid/widget/ImageButton;

    .line 231
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBtn:Landroid/widget/ImageButton;

    .line 232
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceAllBtn:Landroid/widget/ImageButton;

    .line 233
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindReplaceModeBtn:Landroid/widget/Button;

    .line 234
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;

    .line 235
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_DstTxt:Landroid/widget/EditText;

    .line 236
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceSetBtn:Landroid/widget/ImageButton;

    .line 238
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_oInputFilter:Lcom/infraware/polarisoffice5/common/EditInputFilter;

    .line 241
    iput v5, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mViewSearchMode:I

    .line 243
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPopup:Landroid/widget/PopupWindow;

    .line 244
    iput v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbMatchCase:I

    .line 245
    iput v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbMatchWhole:I

    .line 246
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    .line 247
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindDelete:Lcom/infraware/common/control/custom/DeleteImageButton;

    .line 248
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbShowSearchBar:Z

    .line 249
    iput v5, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocExtensionType:I

    .line 250
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbSaveing:Z

    .line 252
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbIsSavedAs:Z

    .line 253
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbSaveAndFinish:Z

    .line 254
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbFinishCalled:Z

    .line 255
    iput v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mInternalCmdType:I

    .line 257
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbSendingEMail:Z

    .line 258
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrEmailFolderPath:Ljava/lang/String;

    .line 259
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrTempFilePathForEMail:Ljava/lang/String;

    .line 261
    iput-boolean v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbScroll:Z

    .line 264
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    .line 265
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mCloseReceiver:Lcom/infraware/office/baseframe/EvBaseViewerActivity$CloseActionReceiver;

    .line 267
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mZoomMenu:Landroid/app/AlertDialog;

    .line 268
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFreeDrawBar:Landroid/view/View;

    .line 270
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbPenDraw:Z

    .line 272
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFreeDrawOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    .line 273
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .line 275
    iput v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenType:I

    .line 276
    iput v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenSize:I

    .line 277
    iput v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenColor:I

    .line 279
    iput v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFreeDrawMode:I

    .line 292
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPrevDlg:Landroid/app/AlertDialog;

    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPagePopup:Landroid/app/AlertDialog;

    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintProgressPopup:Landroid/app/AlertDialog;

    .line 293
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvPrintHelper:Lcom/infraware/office/baseframe/porting/EvPrintHelper;

    .line 294
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintProgressText:Landroid/widget/TextView;

    .line 295
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mProgress:Landroid/widget/ProgressBar;

    .line 299
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEditLayout:Landroid/widget/LinearLayout;

    .line 300
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintAll:Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintCurrent:Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPage:Landroid/widget/RadioButton;

    .line 302
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintStartEdit:Landroid/widget/EditText;

    .line 303
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEndEdit:Landroid/widget/EditText;

    .line 305
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    .line 306
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mCheckOriginalKeep:Z

    .line 308
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPowerSaveOption:Lcom/infraware/office/baseframe/porting/EvPowerSaveOption;

    .line 309
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPath:Ljava/lang/String;

    .line 310
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mTempPath:Ljava/lang/String;

    .line 311
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mBookMarkPath:Ljava/lang/String;

    .line 313
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mThumbnailPath:Ljava/lang/String;

    .line 314
    const-string/jumbo v0, "thumb_img001.png"

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mThumbnailFileName:Ljava/lang/String;

    .line 315
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mRequestThumbnailOnSave:Z

    .line 317
    iput v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    .line 319
    iput v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPrintStartPage:I

    .line 320
    iput v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPrintEndPage:I

    .line 323
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mContentSearchWord:Ljava/lang/String;

    .line 324
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mIsContentSearch:Z

    .line 331
    iput v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mAutoSaveTest:I

    .line 333
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mQuickScroll:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    .line 335
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindReplaceText:Ljava/lang/String;

    .line 337
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mThumbnail:Landroid/graphics/Bitmap;

    .line 338
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->bEncryptDoc:Z

    .line 340
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    .line 342
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mClipBoard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    .line 344
    const/16 v0, 0x18

    new-array v0, v0, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mThumbnailChart:[Landroid/graphics/Bitmap;

    .line 347
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintFilePath:Ljava/lang/String;

    .line 348
    iput v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPageCount:I

    .line 349
    iput v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintCurPage:I

    .line 350
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mRootPath:Ljava/lang/String;

    .line 354
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mConfigurationChangeNotifier:Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;

    .line 356
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_bByMultipleLauncher:Z

    .line 359
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    .line 360
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mUri:Landroid/net/Uri;

    .line 361
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMimeType:Ljava/lang/String;

    .line 364
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMWDnDOperator:Lcom/infraware/common/multiwindow/MWDnDOperator;

    .line 376
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$1;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$1;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->RunnableUpdateMainActionBar:Ljava/lang/Runnable;

    .line 425
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$2;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->messageHandler:Landroid/os/Handler;

    .line 583
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$3;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$3;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onSearchDlgCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 589
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$4;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$4;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onSearchDlgClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 596
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$5;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$5;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onSearchDlgKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 614
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$6;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$6;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 622
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupWindowDissmissListener;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupWindowDissmissListener;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDismissListener:Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupWindowDissmissListener;

    .line 654
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$7;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mClickListener:Landroid/view/View$OnClickListener;

    .line 854
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$12;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$12;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    .line 878
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$13;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$13;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindKeyListener:Landroid/view/View$OnKeyListener;

    .line 910
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$14;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$14;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceKeyListener:Landroid/view/View$OnKeyListener;

    .line 1443
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$17;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$17;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mOnLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    .line 1566
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->isErrDismiss:Z

    .line 2075
    iput v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mLoadFailStrID:I

    .line 2409
    iput v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintCount:I

    .line 2844
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$23;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$23;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 2850
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$24;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$24;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onSendEmailAlarmClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 2867
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$25;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$25;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onNetWorkUseAlarmClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 2883
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$26;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$26;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onSamsungAppsPolairsOfficeClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 2899
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$27;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$27;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onRunShareClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 2915
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$28;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$28;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onGoolgeSearchClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 3617
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$32;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$32;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEditFocusChange:Landroid/view/View$OnFocusChangeListener;

    .line 4310
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$49;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$49;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->replaceWatcher:Landroid/text/TextWatcher;

    .line 4327
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$50;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$50;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findWatcher:Landroid/text/TextWatcher;

    .line 4868
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$53;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$53;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mOnClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 5184
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mTempRefreshHandler:Landroid/os/Handler;

    .line 5185
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$54;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$54;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mTempRefreshRun:Ljava/lang/Runnable;

    .line 5302
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$55;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$55;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFreeDrawClickListener:Landroid/view/View$OnClickListener;

    .line 5367
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mToast:Landroid/widget/Toast;

    .line 5568
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$56;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$56;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mOnDragListener:Landroid/view/View$OnDragListener;

    .line 745
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 751
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;

    invoke-direct {v0}, Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mConfigurationChangeNotifier:Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;

    .line 752
    return-void
.end method

.method private FindbarSoftInputManager()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 5401
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->isFocused()Z

    move-result v2

    if-ne v2, v1, :cond_1

    move v0, v1

    .line 5405
    .local v0, "isFindTextFocused":Z
    :goto_0
    if-ne v0, v1, :cond_0

    .line 5408
    invoke-static {p0}, Lcom/infraware/polarisoffice5/common/ImmManager;->showDisposableIme(Landroid/app/Activity;)V

    .line 5410
    :cond_0
    return-void

    .line 5401
    .end local v0    # "isFindTextFocused":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private OnInitFreeDraw()V
    .locals 5

    .prologue
    const v4, 0x7f05005a

    const/4 v3, 0x0

    .line 5093
    const v0, 0x7f0b0110

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFreeDrawBar:Landroid/view/View;

    .line 5094
    const v0, 0x7f0b013b

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFreeDrawOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    .line 5095
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFreeDrawOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v3, v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->initOptions(III)V

    .line 5096
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnInitFreeDrawOption()V

    .line 5098
    const v0, 0x7f0b013c

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .line 5099
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v3, v1, v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->initOptions(III)V

    .line 5100
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnInitPenDrawOption()V

    .line 5102
    return-void
.end method

.method private OnStartFreeDraw()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 5148
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFreeDrawBar:Landroid/view/View;

    if-nez v0, :cond_0

    .line 5149
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnInitFreeDraw()V

    .line 5150
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFreeDrawBar:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 5151
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->hide()V

    .line 5152
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFreeDrawBar:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 5153
    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onChangeFreeDrawMode(I)V

    .line 5156
    :cond_1
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbPenDraw:Z

    if-eqz v0, :cond_2

    .line 5157
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetInfraPenDrawMode(I)V

    .line 5159
    :cond_2
    invoke-direct {p0, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setFreeDrawMode(Z)V

    .line 5160
    return-void
.end method

.method private ReplaceEditTextEnable()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4280
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 4281
    .local v1, "strOrgTxt":Ljava/lang/String;
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_DstTxt:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 4283
    .local v0, "strDstTxt":Ljava/lang/String;
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 4284
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 4285
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceNextBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 4286
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceNextBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 4287
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceAllBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 4288
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceAllBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 4290
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_1

    .line 4291
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 4292
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 4293
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceNextBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 4294
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceNextBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 4295
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceAllBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 4296
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceAllBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 4308
    :cond_0
    :goto_0
    return-void

    .line 4300
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_0

    .line 4301
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceNextBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 4302
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceNextBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 4303
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 4304
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 4305
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceAllBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 4306
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceAllBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setFocusable(Z)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mViewSearchMode:I

    return v0
.end method

.method static synthetic access$002(Lcom/infraware/office/baseframe/EvBaseViewerActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .param p1, "x1"    # I

    .prologue
    .line 148
    iput p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mViewSearchMode:I

    return p1
.end method

.method static synthetic access$100(Lcom/infraware/office/baseframe/EvBaseViewerActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .param p1, "x1"    # I

    .prologue
    .line 148
    invoke-direct {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->showDialogAfterCheck(I)V

    return-void
.end method

.method static synthetic access$1000(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindSetBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindPrevBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindNextBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_DstTxt:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/infraware/office/baseframe/EvBaseViewerActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .param p1, "x1"    # I

    .prologue
    .line 148
    iput p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbMatchCase:I

    return p1
.end method

.method static synthetic access$1502(Lcom/infraware/office/baseframe/EvBaseViewerActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .param p1, "x1"    # I

    .prologue
    .line 148
    iput p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbMatchWhole:I

    return p1
.end method

.method static synthetic access$1700(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintCount:I

    return v0
.end method

.method static synthetic access$1708(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)I
    .locals 2
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintCount:I

    return v0
.end method

.method static synthetic access$1800(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->connectHelpPage()V

    return-void
.end method

.method static synthetic access$202(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Landroid/widget/PopupWindow;)Landroid/widget/PopupWindow;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .param p1, "x1"    # Landroid/widget/PopupWindow;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPopup:Landroid/widget/PopupWindow;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->connectUriSamungAppsPolarisOffice()V

    return-void
.end method

.method static synthetic access$2200(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->connectRunShare()V

    return-void
.end method

.method static synthetic access$2300(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintStartEdit:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEndEdit:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintProgressPopup:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/RadioButton;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintCurrent:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/RadioButton;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPage:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPagePopup:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/RadioButton;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintAll:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindSetBtnIsLatestClicked:Z

    return v0
.end method

.method static synthetic access$3000(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 148
    invoke-direct {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onToastMessage(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$302(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 148
    iput-boolean p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindSetBtnIsLatestClicked:Z

    return p1
.end method

.method static synthetic access$3100(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ReplaceEditTextEnable()V

    return-void
.end method

.method static synthetic access$3200(Lcom/infraware/office/baseframe/EvBaseViewerActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .param p1, "x1"    # I

    .prologue
    .line 148
    invoke-direct {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onClickSearchItem(I)V

    return-void
.end method

.method static synthetic access$3300(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 148
    invoke-direct {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onMultiWindowStatusChanged(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->hideSoftInputInFindMode()V

    return-void
.end method

.method static synthetic access$500(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$600(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->hideSoftInputInFindReplaceMode()V

    return-void
.end method

.method static synthetic access$700(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindReplaceText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$900(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    return-object v0
.end method

.method private checkOrigianlKeep(Ljava/lang/String;)V
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 1259
    const/16 v2, 0x2e

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 1260
    .local v0, "nIndex":I
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 1261
    .local v1, "strExt":Ljava/lang/String;
    const-string/jumbo v2, ".dot"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, ".dotx"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, ".pot"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, ".potx"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, ".xlt"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, ".xltx"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    .line 1266
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mCheckOriginalKeep:Z

    .line 1267
    :cond_1
    return-void
.end method

.method private checkProtectDocument()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1464
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->IGetBWPProtectStatusInfo()I

    move-result v0

    .line 1465
    .local v0, "aProtectMask":I
    if-nez v0, :cond_1

    .line 1467
    new-instance v1, Ljava/io/File;

    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1468
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->canWrite()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1469
    iput-boolean v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->bEncryptDoc:Z

    .line 1470
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v3}, Lcom/infraware/office/actionbar/MainActionBar;->setPassWordStyle()V

    .line 1471
    const/16 v3, 0x28

    invoke-virtual {p0, v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->showDialog(I)V

    .line 1485
    .end local v1    # "f":Ljava/io/File;
    :cond_0
    :goto_0
    return-void

    .line 1475
    :cond_1
    iput-boolean v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->bEncryptDoc:Z

    .line 1476
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v3}, Lcom/infraware/office/actionbar/MainActionBar;->setPassWordStyle()V

    .line 1477
    const/4 v2, 0x0

    .line 1478
    .local v2, "nMenuID":I
    and-int/lit8 v3, v0, 0x2

    if-eqz v3, :cond_2

    .line 1479
    const/16 v2, 0x28

    .line 1483
    :goto_1
    if-eqz v2, :cond_0

    .line 1484
    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->showDialog(I)V

    goto :goto_0

    .line 1481
    :cond_2
    const/16 v2, 0x27

    goto :goto_1
.end method

.method private connectHelpPage()V
    .locals 5

    .prologue
    .line 2968
    invoke-static {p0}, Lcom/infraware/common/notice/NoticeNotifyManager;->getNoticeUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 2969
    .local v1, "strUrl":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 2970
    const v3, 0x7f070031

    invoke-virtual {p0, v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2977
    :cond_0
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_WEB_VIEW()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2978
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/infraware/polarisoffice5/OfficeWebView;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2980
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v3, "key_web_title"

    const v4, 0x7f0702be

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2982
    const-string/jumbo v3, "key_web_url"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2983
    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->startActivity(Landroid/content/Intent;)V

    .line 2997
    :goto_0
    return-void

    .line 2986
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.VIEW"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2987
    .restart local v0    # "intent":Landroid/content/Intent;
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2988
    .local v2, "uri":Landroid/net/Uri;
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2989
    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private connectRunShare()V
    .locals 5

    .prologue
    .line 5726
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.SEND"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 5727
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v3, "text/plain"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 5729
    const-string/jumbo v1, ""

    .line 5730
    .local v1, "select":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 5731
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    check-cast v2, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .line 5732
    .local v2, "sheetActivity":Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getCellText()Ljava/lang/String;

    move-result-object v1

    .line 5737
    .end local v2    # "sheetActivity":Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 5738
    const-string/jumbo v3, "android.intent.extra.TEXT"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5740
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0702c9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->startActivity(Landroid/content/Intent;)V

    .line 5741
    return-void

    .line 5734
    :cond_1
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->IGetMarkString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private connectUriSamungAppsPolarisOffice()V
    .locals 6

    .prologue
    .line 3001
    :try_start_0
    const-string/jumbo v2, "samsungapps://ProductDetail/com.infraware.polarisoffice5"

    .line 3003
    .local v2, "string_of_uri":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 3004
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 3005
    const v3, 0x14000020

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 3008
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3013
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "string_of_uri":Ljava/lang/String;
    :goto_0
    return-void

    .line 3009
    :catch_0
    move-exception v0

    .line 3010
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0702e8

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private hideSoftInputInFindMode()V
    .locals 3

    .prologue
    .line 5379
    const-string/jumbo v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 5380
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->isFocused()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 5381
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 5383
    :cond_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->isFocused()Z

    move-result v1

    if-nez v1, :cond_1

    .line 5384
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->requestFocus()Z

    .line 5386
    :cond_1
    return-void
.end method

.method private hideSoftInputInFindReplaceMode()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5389
    const-string/jumbo v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 5390
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->isFocused()Z

    move-result v1

    if-ne v1, v3, :cond_2

    .line 5391
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 5395
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->isFocused()Z

    move-result v1

    if-nez v1, :cond_1

    .line 5396
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->requestFocus()Z

    .line 5398
    :cond_1
    return-void

    .line 5392
    :cond_2
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_DstTxt:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_DstTxt:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->isFocused()Z

    move-result v1

    if-ne v1, v3, :cond_0

    .line 5393
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_DstTxt:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0
.end method

.method private mouseWheelZoomDown()V
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 5520
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v12

    .line 5521
    .local v12, "ci":Lcom/infraware/office/evengine/EV$CONFIG_INFO;
    const/4 v2, -0x1

    .line 5523
    .local v2, "nZoom":I
    iget v0, v12, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    const/16 v3, 0x7530

    if-le v0, v3, :cond_2

    .line 5524
    const/16 v2, 0x7530

    .line 5540
    :goto_0
    iget v0, v12, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nMinZoom:I

    if-ge v2, v0, :cond_0

    .line 5542
    iget v2, v12, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nMinZoom:I

    .line 5545
    :cond_0
    iget v0, v12, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    if-eq v2, v0, :cond_1

    .line 5547
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    move v11, v1

    invoke-virtual/range {v0 .. v11}, Lcom/infraware/office/evengine/EvInterface;->ISetZoom(IIIIIIIIIII)V

    .line 5549
    :cond_1
    return-void

    .line 5525
    :cond_2
    iget v0, v12, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    const/16 v3, 0x4e20

    if-le v0, v3, :cond_3

    .line 5526
    const/16 v2, 0x4e20

    goto :goto_0

    .line 5527
    :cond_3
    iget v0, v12, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    const/16 v3, 0x30d4

    if-le v0, v3, :cond_4

    .line 5528
    const/16 v2, 0x30d4

    goto :goto_0

    .line 5529
    :cond_4
    iget v0, v12, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    const/16 v3, 0x2710

    if-le v0, v3, :cond_5

    .line 5530
    const/16 v2, 0x2710

    goto :goto_0

    .line 5531
    :cond_5
    iget v0, v12, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    const/16 v3, 0x1d4c

    if-le v0, v3, :cond_6

    .line 5532
    const/16 v2, 0x1d4c

    goto :goto_0

    .line 5533
    :cond_6
    iget v0, v12, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    const/16 v3, 0x1388

    if-le v0, v3, :cond_7

    .line 5534
    const/16 v2, 0x1388

    goto :goto_0

    .line 5535
    :cond_7
    iget v0, v12, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    const/16 v3, 0x9c4

    if-le v0, v3, :cond_8

    .line 5536
    const/16 v2, 0x9c4

    goto :goto_0

    .line 5538
    :cond_8
    iget v2, v12, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nMinZoom:I

    goto :goto_0
.end method

.method private mouseWheelZoomUp()V
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 5485
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v12

    .line 5486
    .local v12, "ci":Lcom/infraware/office/evengine/EV$CONFIG_INFO;
    const/4 v2, -0x1

    .line 5488
    .local v2, "nZoom":I
    iget v0, v12, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    const/16 v3, 0x9c4

    if-ge v0, v3, :cond_2

    .line 5489
    const/16 v2, 0x9c4

    .line 5508
    :goto_0
    iget v0, v12, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nMaxZoom:I

    if-le v2, v0, :cond_0

    .line 5510
    iget v2, v12, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nMaxZoom:I

    .line 5513
    :cond_0
    iget v0, v12, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    if-eq v2, v0, :cond_1

    .line 5515
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    move v11, v1

    invoke-virtual/range {v0 .. v11}, Lcom/infraware/office/evengine/EvInterface;->ISetZoom(IIIIIIIIIII)V

    .line 5517
    :cond_1
    return-void

    .line 5490
    :cond_2
    iget v0, v12, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    const/16 v3, 0x1388

    if-ge v0, v3, :cond_3

    .line 5491
    const/16 v2, 0x1388

    goto :goto_0

    .line 5492
    :cond_3
    iget v0, v12, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    const/16 v3, 0x1d4c

    if-ge v0, v3, :cond_4

    .line 5493
    const/16 v2, 0x1d4c

    goto :goto_0

    .line 5494
    :cond_4
    iget v0, v12, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    const/16 v3, 0x2710

    if-ge v0, v3, :cond_5

    .line 5495
    const/16 v2, 0x2710

    goto :goto_0

    .line 5496
    :cond_5
    iget v0, v12, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    const/16 v3, 0x30d4

    if-ge v0, v3, :cond_6

    .line 5497
    const/16 v2, 0x30d4

    goto :goto_0

    .line 5498
    :cond_6
    iget v0, v12, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    const/16 v3, 0x4e20

    if-ge v0, v3, :cond_7

    .line 5499
    const/16 v2, 0x4e20

    goto :goto_0

    .line 5500
    :cond_7
    iget v0, v12, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    const/16 v3, 0x7530

    if-ge v0, v3, :cond_8

    .line 5501
    const/16 v2, 0x7530

    goto :goto_0

    .line 5502
    :cond_8
    iget v0, v12, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    const v3, 0x9c40

    if-ge v0, v3, :cond_9

    .line 5503
    const v2, 0x9c40

    goto :goto_0

    .line 5505
    :cond_9
    iget v2, v12, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nMaxZoom:I

    goto :goto_0
.end method

.method private onChangePenDrawMode(I)V
    .locals 6
    .param p1, "mode"    # I

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 5194
    packed-switch p1, :pswitch_data_0

    .line 5241
    :goto_0
    :pswitch_0
    iput p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFreeDrawMode:I

    .line 5242
    return-void

    .line 5197
    :pswitch_1
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1, v4, v3}, Lcom/infraware/office/evengine/EvInterface;->ISetPenMode(IZ)V

    .line 5198
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1, v4}, Lcom/infraware/office/baseframe/EvBaseView;->setActionMode(I)V

    goto :goto_0

    .line 5201
    :pswitch_2
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getThickness()I

    move-result v1

    mul-int/lit8 v1, v1, 0x48

    div-int/lit8 v0, v1, 0xa

    .line 5202
    .local v0, "thickness":I
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getType()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    .line 5217
    :goto_1
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1, v0}, Lcom/infraware/office/evengine/EvInterface;->ISetPenSize(I)V

    .line 5218
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getColor()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetPenColor(I)V

    .line 5219
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getTransparency()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetPenTransparency(I)V

    .line 5220
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Lcom/infraware/office/baseframe/EvBaseView;->setActionMode(I)V

    .line 5221
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    invoke-virtual {v1, v5}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->setVisibility(I)V

    goto :goto_0

    .line 5205
    :pswitch_3
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->ISetPenMode(IZ)V

    goto :goto_1

    .line 5208
    :pswitch_4
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x4

    invoke-virtual {v1, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->ISetPenMode(IZ)V

    goto :goto_1

    .line 5211
    :pswitch_5
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1, v3, v3}, Lcom/infraware/office/evengine/EvInterface;->ISetPenMode(IZ)V

    goto :goto_1

    .line 5214
    :pswitch_6
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x7

    invoke-virtual {v1, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->ISetPenMode(IZ)V

    goto :goto_1

    .line 5224
    .end local v0    # "thickness":I
    :pswitch_7
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/infraware/office/baseframe/EvBaseView;->setActionMode(I)V

    .line 5225
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    invoke-virtual {v1, v4}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->setVisibility(I)V

    .line 5226
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    invoke-virtual {v1, v4}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->setTrackingMode(Z)V

    goto :goto_0

    .line 5229
    :pswitch_8
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Lcom/infraware/office/baseframe/EvBaseView;->setActionMode(I)V

    .line 5230
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/16 v2, 0x9

    invoke-virtual {v1, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->ISetPenMode(IZ)V

    .line 5232
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mTempRefreshHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mTempRefreshRun:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 5233
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mTempRefreshHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mTempRefreshRun:Ljava/lang/Runnable;

    const-wide/16 v3, 0x1f4

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 5236
    :pswitch_9
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Lcom/infraware/office/baseframe/EvBaseView;->setActionMode(I)V

    .line 5237
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1, v5, v3}, Lcom/infraware/office/evengine/EvInterface;->ISetPenMode(IZ)V

    .line 5238
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1, v3}, Lcom/infraware/office/evengine/EvInterface;->ISetInfraPenDrawMode(I)V

    goto/16 :goto_0

    .line 5194
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 5202
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private onClickSearchItem(I)V
    .locals 5
    .param p1, "itemId"    # I

    .prologue
    .line 4893
    move-object v0, p0

    .line 4895
    .local v0, "evBaseViewerActivity":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    sparse-switch p1, :sswitch_data_0

    .line 4933
    .end local v0    # "evBaseViewerActivity":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    :goto_0
    return-void

    .line 4898
    .restart local v0    # "evBaseViewerActivity":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    :sswitch_0
    instance-of v4, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    if-eqz v4, :cond_0

    .line 4899
    check-cast v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    .end local v0    # "evBaseViewerActivity":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->runGoogleSearch()V

    goto :goto_0

    .line 4902
    .restart local v0    # "evBaseViewerActivity":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->runGoogleSearch()V

    goto :goto_0

    .line 4906
    :sswitch_1
    instance-of v4, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    if-eqz v4, :cond_1

    .line 4907
    check-cast v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    .end local v0    # "evBaseViewerActivity":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->runWikipediaSearch()V

    goto :goto_0

    .line 4910
    .restart local v0    # "evBaseViewerActivity":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    :cond_1
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/ExternalCallManager;->runWikipediaSearch(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    goto :goto_0

    .line 4914
    :sswitch_2
    instance-of v4, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    if-eqz v4, :cond_2

    .line 4915
    check-cast v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    .end local v0    # "evBaseViewerActivity":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->runDictionarySearch()V

    goto :goto_0

    .line 4918
    .restart local v0    # "evBaseViewerActivity":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    :cond_2
    const-string/jumbo v2, ""

    .line 4919
    .local v2, "select":Ljava/lang/String;
    instance-of v4, v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    if-eqz v4, :cond_3

    move-object v3, v0

    .line 4920
    check-cast v3, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .line 4921
    .local v3, "sheetActivity":Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getCellText()Ljava/lang/String;

    move-result-object v2

    .line 4926
    .end local v3    # "sheetActivity":Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    :goto_1
    iget-object v4, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->messageHandler:Landroid/os/Handler;

    invoke-virtual {v4}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 4927
    .local v1, "msg":Landroid/os/Message;
    const/16 v4, 0x378

    iput v4, v1, Landroid/os/Message;->what:I

    .line 4928
    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 4929
    iget-object v4, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->messageHandler:Landroid/os/Handler;

    invoke-virtual {v4, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 4923
    .end local v1    # "msg":Landroid/os/Message;
    :cond_3
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v4

    invoke-virtual {v4}, Lcom/infraware/office/evengine/EvInterface;->IGetMarkString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 4895
    :sswitch_data_0
    .sparse-switch
        0x7f07008a -> :sswitch_2
        0x7f070166 -> :sswitch_0
        0x7f070251 -> :sswitch_1
    .end sparse-switch
.end method

.method private onCreateOnKKOver()V
    .locals 2

    .prologue
    .line 5598
    new-instance v0, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 5600
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    new-instance v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity$57;

    invoke-direct {v1, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$57;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->setStateChangeListener(Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;)Z

    .line 5615
    return-void
.end method

.method private onMultiWindowStatusChanged(Z)V
    .locals 4
    .param p1, "aIsSplit"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 5622
    invoke-static {}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getInstance()Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->isNormalWindow(Landroid/app/Activity;)Z

    move-result v3

    if-nez v3, :cond_1

    move v0, v1

    .line 5623
    .local v0, "isMW":Z
    :goto_0
    iget-boolean v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mIsMultiWindow:Z

    if-ne v3, v0, :cond_2

    .line 5624
    const-string/jumbo v1, "onMultiWindowStatusChanged"

    const-string/jumbo v2, "Even"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 5636
    :cond_0
    :goto_1
    return-void

    .end local v0    # "isMW":Z
    :cond_1
    move v0, v2

    .line 5622
    goto :goto_0

    .line 5627
    .restart local v0    # "isMW":Z
    :cond_2
    iget-boolean v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mIsMultiWindow:Z

    if-nez v3, :cond_3

    if-eqz v0, :cond_3

    .line 5628
    const-string/jumbo v2, "onMultiWindowStatusChanged"

    const-string/jumbo v3, "Normal -> Multi"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 5629
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onChangedFromNormalWindowToMultiWindow()V

    .line 5630
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mIsMultiWindow:Z

    goto :goto_1

    .line 5631
    :cond_3
    iget-boolean v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mIsMultiWindow:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 5632
    const-string/jumbo v1, "onMultiWindowStatusChanged"

    const-string/jumbo v3, "Multi -> Normal"

    invoke-static {v1, v3}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 5633
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onChangedFromMultiwindowToNormalWindow()V

    .line 5634
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mIsMultiWindow:Z

    goto :goto_1
.end method

.method private onReflowText()V
    .locals 1

    .prologue
    .line 401
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbReflowText:Z

    if-eqz v0, :cond_0

    .line 402
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbReflowText:Z

    .line 403
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISetPrintMode()V

    .line 409
    :goto_0
    return-void

    .line 406
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbReflowText:Z

    .line 407
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISetWebMode()V

    goto :goto_0
.end method

.method private onResultPassWordActivity(Landroid/content/Intent;)V
    .locals 4
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 2737
    const-string/jumbo v1, "key_new_file"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2738
    .local v0, "pass":Ljava/lang/String;
    const-string/jumbo v1, "exit"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 2739
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->finish()V

    .line 2744
    :goto_0
    return-void

    .line 2742
    :cond_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mTempPath:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Lcom/infraware/office/evengine/EvInterface;->IOpenEx(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onStartPenDraw()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 5123
    const v0, 0x7f0b013c

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .line 5124
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenType:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 5125
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    const/4 v1, 0x0

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f05005a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->initOptions(III)V

    .line 5130
    :goto_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v5}, Lcom/infraware/office/evengine/EvInterface;->ISetInfraPenDrawMode(I)V

    .line 5132
    invoke-virtual {p0, v5}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ChangeViewMode(I)V

    .line 5133
    invoke-direct {p0, v5}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setFreeDrawMode(Z)V

    .line 5134
    return-void

    .line 5127
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    iget v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenType:I

    iget v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenSize:I

    iget v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenColor:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->initOptions(III)V

    goto :goto_0
.end method

.method private onToastMessage(Ljava/lang/String;)V
    .locals 3
    .param p1, "strText"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 5370
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mToast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 5371
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mToast:Landroid/widget/Toast;

    .line 5374
    :goto_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mToast:Landroid/widget/Toast;

    const/16 v1, 0x11

    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 5375
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 5376
    return-void

    .line 5373
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private renameTemplateFile(I)V
    .locals 2
    .param p1, "nNewDoc"    # I

    .prologue
    .line 1488
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 1489
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "key_new_file"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    .line 1492
    :cond_0
    return-void
.end method

.method private setFreeDrawMode(Z)V
    .locals 1
    .param p1, "set"    # Z

    .prologue
    .line 5327
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0, p1}, Lcom/infraware/office/baseframe/EvBaseView;->setDisableTextInput(Z)V

    .line 5328
    return-void
.end method

.method private showDialogAfterCheck(I)V
    .locals 1
    .param p1, "dialog"    # I

    .prologue
    .line 579
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 580
    invoke-virtual {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->showDialog(I)V

    .line 581
    :cond_0
    return-void
.end method

.method private showErrorDlg()V
    .locals 2

    .prologue
    const/16 v1, 0x34

    .line 1555
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDlgMsg:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 1557
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDlgMsg:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1559
    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->showDialog(I)V

    .line 1563
    :cond_0
    :goto_0
    return-void

    .line 1562
    :cond_1
    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->showDialog(I)V

    goto :goto_0
.end method


# virtual methods
.method public ActivityMsgProc(IIIIILjava/lang/Object;)I
    .locals 7
    .param p1, "msg"    # I
    .param p2, "p1"    # I
    .param p3, "p2"    # I
    .param p4, "p3"    # I
    .param p5, "p4"    # I
    .param p6, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4354
    sparse-switch p1, :sswitch_data_0

    .line 4386
    :goto_0
    return v3

    .line 4356
    :sswitch_0
    const-string/jumbo v3, "HYOHYUN"

    const-string/jumbo v5, "EVBaseViewerActivity eOnInfraPenCustomColor"

    invoke-static {v3, v5}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4357
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v3, v4}, Lcom/infraware/office/actionbar/MainActionBar;->setPenCustomColor(Z)V

    :goto_1
    move v3, v4

    .line 4386
    goto :goto_0

    .line 4360
    :sswitch_1
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/EvBaseView;->drawAllContents()V

    goto :goto_1

    :sswitch_2
    move-object v1, p6

    .line 4363
    check-cast v1, Ljava/lang/String;

    .line 4365
    .local v1, "hyperLink":Ljava/lang/String;
    :try_start_0
    const-string/jumbo v5, "http://"

    invoke-virtual {v1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-gez v5, :cond_0

    .line 4366
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "http://"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 4367
    :cond_0
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v5, "android.intent.action.VIEW"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-direct {v2, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 4368
    .local v2, "i":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 4369
    .end local v2    # "i":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 4370
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f07016f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 4354
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x6 -> :sswitch_2
        0x3a -> :sswitch_0
    .end sparse-switch
.end method

.method protected AlertErrorAndCloseDlg(I)V
    .locals 4
    .param p1, "contentStringId"    # I

    .prologue
    .line 2128
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2130
    .local v1, "content":Ljava/lang/String;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v2

    invoke-direct {v0, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 2131
    .local v0, "ab":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 2132
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 2133
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070063

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/infraware/office/baseframe/EvBaseViewerActivity$18;

    invoke-direct {v3, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$18;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2140
    new-instance v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity$19;

    invoke-direct {v2, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$19;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 2147
    new-instance v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity$20;

    invoke-direct {v2, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$20;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 2158
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    sput-object v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mErrorAndClosePopup:Landroid/app/AlertDialog;

    .line 2159
    sget-object v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mErrorAndClosePopup:Landroid/app/AlertDialog;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 2160
    sget-object v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mErrorAndClosePopup:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 2161
    return-void
.end method

.method public CancelFind()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 4200
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindBar:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4201
    const/16 v0, 0xff

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mViewSearchMode:I

    .line 4202
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindPrevBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 4203
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindPrevBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 4204
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindNextBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 4205
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindNextBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 4206
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindPrevBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 4207
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindNextBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 4209
    :cond_0
    return-void
.end method

.method protected ChangeViewMode(I)V
    .locals 8
    .param p1, "ev_view_mode"    # I

    .prologue
    const/4 v4, 0x1

    .line 2396
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2398
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    invoke-static {v0, v1}, Lcom/infraware/common/util/Utils;->setScreenMode(Landroid/view/Window;I)V

    .line 2401
    :cond_0
    const v0, 0x7f0b002c

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    .line 2403
    .local v7, "docViewLayout":Landroid/widget/RelativeLayout;
    if-ne p1, v4, :cond_1

    .line 2404
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v2

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    move v1, p1

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IChangeViewMode(IIIIII)V

    .line 2407
    :goto_0
    return-void

    .line 2406
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v2

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    move v1, p1

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IChangeViewMode(IIIIII)V

    goto :goto_0
.end method

.method public FindBarClose()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1022
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->dismissPopupWindow()Z

    .line 1023
    const/16 v2, 0xa

    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->removeDialog(I)V

    .line 1024
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindBar:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindBar:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 1025
    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->FindBarShow(Z)V

    .line 1026
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->ISearchStop()V

    .line 1027
    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mViewSearchMode:I

    .line 1028
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindReplaceText:Ljava/lang/String;

    .line 1029
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1032
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public FindBarShow(Z)V
    .locals 12
    .param p1, "bShow"    # Z

    .prologue
    const/4 v11, 0x3

    const/16 v2, -0x7d0

    const/4 v10, 0x1

    const/4 v1, 0x0

    .line 961
    if-eqz p1, :cond_8

    .line 963
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    .line 964
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IClearFrameSet()V

    .line 965
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnFindDialog()V

    .line 966
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->dismissPopupWindow()Z

    .line 967
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ReplaceBarClose()Z

    .line 968
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 969
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0, v10}, Lcom/infraware/office/baseframe/EvBaseView;->setActionMode(I)V

    .line 971
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    if-eq v0, v10, :cond_1

    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    if-eq v0, v11, :cond_1

    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    const/4 v3, 0x6

    if-ne v0, v3, :cond_2

    .line 974
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_2

    .line 975
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move v3, v2

    move v4, v1

    move v5, v1

    move v6, v1

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 976
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v4, 0x2

    move v5, v2

    move v6, v2

    move v7, v1

    move v8, v1

    move v9, v1

    invoke-virtual/range {v3 .. v9}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 980
    :cond_2
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    if-ne v0, v11, :cond_3

    .line 982
    instance-of v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    if-eqz v0, :cond_3

    move-object v0, p0

    .line 984
    check-cast v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->resetSlideManageButtonEnable()V

    .line 989
    :cond_3
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_4

    .line 990
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v10}, Lcom/infraware/office/evengine/EvInterface;->ISetFindModeChange(I)V

    .line 992
    :cond_4
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-ne v0, v10, :cond_7

    .line 993
    .local v10, "isFindTextFocused":Z
    :goto_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    if-eqz v0, :cond_5

    if-nez v10, :cond_5

    .line 994
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 998
    :cond_5
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mIsContentSearch:Z

    if-nez v0, :cond_6

    .line 1000
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->FindbarSoftInputManager()V

    .line 1018
    .end local v10    # "isFindTextFocused":Z
    :cond_6
    :goto_1
    invoke-virtual {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setShowSearchBar(Z)V

    .line 1019
    return-void

    :cond_7
    move v10, v1

    .line 992
    goto :goto_0

    .line 1005
    :cond_8
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindBar:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1006
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseView;->setActionMode(I)V

    .line 1007
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->hideSoftInputInFindMode()V

    .line 1009
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    if-ne v0, v11, :cond_6

    .line 1011
    instance-of v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    if-eqz v0, :cond_6

    move-object v0, p0

    .line 1013
    check-cast v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->resetSlideManageButtonEnable()V

    goto :goto_1
.end method

.method public FindReplaceEnabled(Z)V
    .locals 2
    .param p1, "b"    # Z

    .prologue
    const/4 v1, 0x1

    .line 4182
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindBar:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4183
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindPrevBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 4184
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindPrevBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 4185
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindNextBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 4186
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindNextBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 4187
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindPrevBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 4188
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindNextBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 4191
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBar:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4192
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ReplaceEditTextEnable()V

    .line 4193
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceNextBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 4194
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 4195
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceAllBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 4197
    :cond_1
    return-void
.end method

.method public FindText(I)V
    .locals 8
    .param p1, "bPrev"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x1

    .line 1036
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->dismissPopupWindow()Z

    .line 1037
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1042
    .local v1, "strFind":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1066
    :cond_0
    :goto_0
    return-void

    .line 1045
    :cond_1
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mViewSearchMode:I

    if-eq v0, v5, :cond_2

    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mViewSearchMode:I

    const/16 v2, 0xff

    if-ne v0, v2, :cond_0

    .line 1047
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocExtensionType()I

    move-result v0

    const/4 v2, 0x5

    if-eq v0, v2, :cond_3

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocExtensionType()I

    move-result v0

    const/16 v2, 0x14

    if-eq v0, v2, :cond_3

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocExtensionType()I

    move-result v0

    const/16 v2, 0x26

    if-ne v0, v2, :cond_7

    .line 1050
    :cond_3
    const/4 v6, 0x0

    .line 1051
    .local v6, "nFlag":I
    if-ne p1, v5, :cond_4

    .line 1052
    or-int/lit8 v6, v6, 0x8

    .line 1053
    :cond_4
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbMatchCase:I

    if-ne v0, v5, :cond_5

    .line 1054
    or-int/lit8 v6, v6, 0x2

    .line 1055
    :cond_5
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbMatchWhole:I

    if-ne v0, v5, :cond_6

    .line 1056
    or-int/lit8 v6, v6, 0x4

    .line 1058
    :cond_6
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v6}, Lcom/infraware/office/evengine/EvInterface;->ISheetFindReplace(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1063
    .end local v6    # "nFlag":I
    :goto_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindPrevBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 1064
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindNextBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setClickable(Z)V

    goto :goto_0

    .line 1060
    :cond_7
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbMatchWhole:I

    iget v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbMatchCase:I

    move v4, p1

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvInterface;->ISearchStart(Ljava/lang/String;IIII)V

    goto :goto_1
.end method

.method public GetBitmap(II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 2310
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->processLoadComplete()V

    .line 2311
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/baseframe/EvBaseView;->getBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public GetChartThumbnailBitmap(III)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "index"    # I
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    .line 1884
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 1885
    :cond_0
    const/4 v0, 0x0

    .line 1888
    :goto_0
    return-object v0

    .line 1887
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mThumbnailChart:[Landroid/graphics/Bitmap;

    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, p1

    .line 1888
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mThumbnailChart:[Landroid/graphics/Bitmap;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public GetInterfaceHandle()I
    .locals 1

    .prologue
    .line 4664
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->getInterfaceHandleAddress()I

    move-result v0

    if-eqz v0, :cond_0

    .line 4665
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->getInterfaceHandleAddress()I

    move-result v0

    .line 4667
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->getNativeInterfaceHandle()I

    move-result v0

    goto :goto_0
.end method

.method public GetPageThumbnailBitmap(III)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "nPageNum"    # I
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    .line 1770
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 1771
    :cond_0
    const/4 v0, 0x0

    .line 1776
    :goto_0
    return-object v0

    .line 1773
    :cond_1
    const-string/jumbo v0, "EvBaseViewerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "GetPageThumbnailBitmap Page = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " Width = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " Height = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1775
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mThumbnail:Landroid/graphics/Bitmap;

    .line 1776
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mThumbnail:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public GetPrintBitmap(II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 2316
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/baseframe/EvBaseView;->getPrintBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public GetThumbnailBitmap(III)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "nPageNum"    # I
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    .line 1759
    const-string/jumbo v0, "EvBaseViewerActivity"

    const-string/jumbo v1, "GetThumbnailBitmap()"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1760
    const/4 v0, 0x0

    return-object v0
.end method

.method public HideMemo()V
    .locals 0

    .prologue
    .line 4942
    return-void
.end method

.method public InitFindBar()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 780
    const v0, 0x7f0b0063

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindBar:Landroid/widget/LinearLayout;

    .line 781
    const v0, 0x7f0b0064

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindSetBtn:Landroid/widget/ImageButton;

    .line 782
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindSetBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 783
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindSetBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 785
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindSetBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setLongClickable(Z)V

    .line 786
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindSetBtn:Landroid/widget/ImageButton;

    new-instance v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity$8;

    invoke-direct {v1, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$8;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 795
    const v0, 0x7f0b0066

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindPrevBtn:Landroid/widget/ImageButton;

    .line 796
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindPrevBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 797
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindPrevBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 798
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindPrevBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 799
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindPrevBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 801
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindPrevBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setLongClickable(Z)V

    .line 802
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindPrevBtn:Landroid/widget/ImageButton;

    new-instance v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity$9;

    invoke-direct {v1, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$9;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 810
    const v0, 0x7f0b0065

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindNextBtn:Landroid/widget/ImageButton;

    .line 811
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindNextBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 812
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindNextBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 813
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindNextBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 814
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindNextBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 815
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindNextBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setLongClickable(Z)V

    .line 816
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindNextBtn:Landroid/widget/ImageButton;

    new-instance v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity$10;

    invoke-direct {v1, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$10;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 825
    const v0, 0x7f0b0067

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    .line 826
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 827
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 828
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    const v1, 0x7f0701d6

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 829
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->DISABLE_EMOJI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 830
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    const-string/jumbo v1, "inputType=DisableEmoji"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 832
    :cond_0
    new-instance v0, Lcom/infraware/polarisoffice5/common/EditInputFilter;

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/EditInputFilter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_oInputFilter:Lcom/infraware/polarisoffice5/common/EditInputFilter;

    .line 833
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_oInputFilter:Lcom/infraware/polarisoffice5/common/EditInputFilter;

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/EditInputFilter;->setMaxLength(I)V

    .line 834
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_oInputFilter:Lcom/infraware/polarisoffice5/common/EditInputFilter;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/EditInputFilter;->getFilters()[Landroid/text/InputFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 836
    const v0, 0x7f0b0068

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/infraware/common/control/custom/DeleteImageButton;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindDelete:Lcom/infraware/common/control/custom/DeleteImageButton;

    .line 837
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindDelete:Lcom/infraware/common/control/custom/DeleteImageButton;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Lcom/infraware/common/control/custom/DeleteImageButton;->init(Landroid/widget/EditText;)V

    .line 840
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    new-instance v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity$11;

    invoke-direct {v1, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$11;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 850
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 851
    return-void
.end method

.method public IsEditInTextboxMode()Z
    .locals 1

    .prologue
    .line 4946
    const/4 v0, 0x0

    return v0
.end method

.method public IsViewerMode()Z
    .locals 1

    .prologue
    .line 4995
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mIsViewerMode:Z

    return v0
.end method

.method public OnActionBarEvent(I)V
    .locals 7
    .param p1, "eEventType"    # I

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 4566
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-nez v0, :cond_1

    .line 4632
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 4569
    :cond_1
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 4577
    :sswitch_1
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->InitFindBar()V

    .line 4578
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->hide()V

    .line 4579
    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->FindBarShow(Z)V

    goto :goto_0

    .line 4571
    :sswitch_2
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onReflowText()V

    goto :goto_0

    .line 4584
    :sswitch_3
    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onChangeFreeDrawMode(I)V

    goto :goto_0

    .line 4587
    :sswitch_4
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnInitPenDrawOption()V

    .line 4588
    invoke-virtual {p0, v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onChangeFreeDrawMode(I)V

    goto :goto_0

    .line 4591
    :sswitch_5
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onChangeFreeDrawMode(I)V

    goto :goto_0

    .line 4594
    :sswitch_6
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onChangeFreeDrawMode(I)V

    goto :goto_0

    .line 4597
    :sswitch_7
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IInfraPenAllErase()V

    goto :goto_0

    .line 4600
    :sswitch_8
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnEndPenDraw()V

    goto :goto_0

    .line 4603
    :sswitch_9
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbPenDraw:Z

    .line 4604
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onStartPenDraw()V

    .line 4605
    const/16 v1, 0x34

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object v0, p0

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto :goto_0

    .line 4608
    :sswitch_a
    iput-boolean v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbPenDraw:Z

    .line 4609
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnStartFreeDraw()V

    goto :goto_0

    .line 4612
    :sswitch_b
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnEndFreeDraw()V

    goto :goto_0

    .line 4616
    :sswitch_c
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    if-eq v0, v2, :cond_3

    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nTotalPages:I

    const/16 v1, 0x3e8

    if-le v0, v1, :cond_3

    :cond_2
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    if-ne v0, v4, :cond_4

    .line 4618
    :cond_3
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onEmailSendDialog()V

    goto :goto_0

    .line 4622
    :cond_4
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    invoke-virtual {p0, v3, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onSendEMail(ZI)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4623
    const/16 v0, 0xb

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->showDialog(I)V

    goto :goto_0

    .line 4627
    :sswitch_d
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mOfficePrintManager:Lcom/infraware/polarisoffice5/print/OfficePrintManager;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->print(Landroid/app/Activity;Lcom/infraware/office/baseframe/EvBaseView;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 4569
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x31 -> :sswitch_a
        0x38 -> :sswitch_2
        0x51 -> :sswitch_d
        0x54 -> :sswitch_b
        0x58 -> :sswitch_c
        0x80 -> :sswitch_0
        0x82 -> :sswitch_9
        0x107 -> :sswitch_3
        0x108 -> :sswitch_4
        0x109 -> :sswitch_5
        0x110 -> :sswitch_6
        0x111 -> :sswitch_7
        0x118 -> :sswitch_8
    .end sparse-switch
.end method

.method public OnBFinalDrawBitmap(I)V
    .locals 0
    .param p1, "nReserved"    # I

    .prologue
    .line 2337
    return-void
.end method

.method public OnCloseDoc()V
    .locals 1

    .prologue
    .line 1754
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbLoadComplete:Z

    .line 1755
    return-void
.end method

.method public OnDrawBitmap(II)V
    .locals 6
    .param p1, "nCallId"    # I
    .param p2, "bShowAutomap"    # I

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 2342
    const/16 v1, 0xe

    if-ne p1, v1, :cond_0

    .line 2343
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mQuickScroll:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->hide()V

    .line 2346
    :cond_0
    if-ne p1, v4, :cond_1

    iget v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    const/16 v2, 0x26

    if-ne v1, v2, :cond_1

    .line 2347
    invoke-virtual {p0, v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ChangeViewMode(I)V

    .line 2349
    :cond_1
    if-ne p1, v4, :cond_3

    iget v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    if-eq v1, v3, :cond_2

    iget v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    if-eq v1, v5, :cond_2

    iget v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    if-ne v1, v4, :cond_3

    .line 2351
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->IsViewerMode()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2352
    invoke-virtual {p0, v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ChangeViewMode(I)V

    .line 2355
    :cond_3
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->RunnableUpdateMainActionBar:Ljava/lang/Runnable;

    if-eqz v1, :cond_4

    .line 2356
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->RunnableUpdateMainActionBar:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2359
    :cond_4
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iput p1, v1, Lcom/infraware/office/baseframe/EvBaseView;->mCallBackID:I

    .line 2360
    const-string/jumbo v1, "EvBaseViewerActivity"

    const-string/jumbo v2, "Called OnDrawBitmap"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2361
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->drawAllContents()V

    .line 2363
    const/16 v1, 0x2c

    if-ne p1, v1, :cond_6

    .line 2364
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IEditPageRedrawBitmap()V

    .line 2392
    :cond_5
    :goto_0
    return-void

    .line 2368
    :cond_6
    const/16 v1, 0x1b

    if-eq p1, v1, :cond_7

    const/16 v1, 0x3d

    if-ne p1, v1, :cond_5

    .line 2370
    :cond_7
    iget-boolean v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbScroll:Z

    if-ne v1, v3, :cond_5

    .line 2372
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mQuickScroll:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseView;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->show(I)V

    .line 2373
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2374
    iget v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    if-ne v1, v3, :cond_5

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->getActionMode()I

    move-result v1

    if-ne v1, v5, :cond_5

    .line 2376
    instance-of v1, p0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    if-eqz v1, :cond_5

    move-object v0, p0

    .line 2377
    check-cast v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    .line 2378
    .local v0, "wordEditorActivity":Lcom/infraware/polarisoffice5/word/WordEditorActivity;
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mQuickScroll:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->getUserMode()Z

    move-result v1

    if-nez v1, :cond_5

    .line 2379
    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onGuideStartDelayTextToSpeech()V

    goto :goto_0
.end method

.method public OnDrawGetChartThumbnail(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 1893
    return-void
.end method

.method public OnDrawGetPageThumbnail(I)V
    .locals 28
    .param p1, "nPageNum"    # I

    .prologue
    .line 1781
    const-string/jumbo v2, "EvBaseViewerActivity"

    const-string/jumbo v9, "OnDrawGetPageThumbnail()"

    invoke-static {v2, v9}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1782
    const-string/jumbo v2, "EvBaseViewerActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "mstrOpenFilePath = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1783
    const-string/jumbo v2, "EvBaseViewerActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "mstrRequestThumbnailPath = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrRequestThumbnailPath:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1786
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrRequestThumbnailPath:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {p0 .. p0}, Lcom/infraware/common/util/Utils;->getDataCacheDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "/template"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1787
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrRequestThumbnailPath:Ljava/lang/String;

    .line 1789
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->makeThumbnailDir()V

    .line 1790
    const-string/jumbo v2, "EvBaseViewerActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "mThumbnailPath = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mThumbnailPath:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1792
    const/16 v25, 0x0

    .line 1793
    .local v25, "fileInputStream":Ljava/io/FileInputStream;
    const/16 v27, 0x0

    .line 1795
    .local v27, "numberBytes":I
    :try_start_0
    new-instance v26, Ljava/io/FileInputStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mThumbnailPath:Ljava/lang/String;

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mThumbnailFileName:Ljava/lang/String;

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v26

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1802
    .end local v25    # "fileInputStream":Ljava/io/FileInputStream;
    .local v26, "fileInputStream":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual/range {v26 .. v26}, Ljava/io/FileInputStream;->available()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v27

    .line 1817
    move/from16 v0, v27

    new-array v3, v0, [B

    .line 1819
    .local v3, "byteArray":[B
    :try_start_2
    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/io/FileInputStream;->read([B)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 1838
    :try_start_3
    invoke-virtual/range {v26 .. v26}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5

    .line 1845
    :goto_0
    const/16 v25, 0x0

    .line 1847
    .end local v26    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v25    # "fileInputStream":Ljava/io/FileInputStream;
    const-string/jumbo v4, ""

    .line 1848
    .local v4, "title":Ljava/lang/String;
    const-string/jumbo v5, ""

    .line 1849
    .local v5, "author":Ljava/lang/String;
    const-string/jumbo v6, ""

    .line 1850
    .local v6, "last_editor":Ljava/lang/String;
    const/4 v7, 0x0

    .line 1851
    .local v7, "page":I
    const/4 v8, 0x0

    .line 1853
    .local v8, "word":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocExtensionType:I

    const/4 v9, 0x6

    if-eq v2, v9, :cond_2

    .line 1855
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetSummaryData()Lcom/infraware/office/evengine/EV$SUMMARY_DATA;

    move-result-object v22

    .line 1856
    .local v22, "data":Lcom/infraware/office/evengine/EV$SUMMARY_DATA;
    move-object/from16 v0, v22

    iget-object v4, v0, Lcom/infraware/office/evengine/EV$SUMMARY_DATA;->szTitle:Ljava/lang/String;

    .line 1857
    move-object/from16 v0, v22

    iget-object v5, v0, Lcom/infraware/office/evengine/EV$SUMMARY_DATA;->szAuthor:Ljava/lang/String;

    .line 1858
    move-object/from16 v0, v22

    iget-object v6, v0, Lcom/infraware/office/evengine/EV$SUMMARY_DATA;->szModifiedBy:Ljava/lang/String;

    .line 1859
    move-object/from16 v0, v22

    iget v7, v0, Lcom/infraware/office/evengine/EV$SUMMARY_DATA;->nPage:I

    .line 1860
    move-object/from16 v0, v22

    iget v8, v0, Lcom/infraware/office/evengine/EV$SUMMARY_DATA;->nWords:I

    .line 1868
    .end local v22    # "data":Lcom/infraware/office/evengine/EV$SUMMARY_DATA;
    :goto_1
    invoke-static/range {p0 .. p0}, Lcom/infraware/common/database/ThumbnailManager;->getInstance(Landroid/content/Context;)Lcom/infraware/common/database/ThumbnailManager;

    move-result-object v1

    .line 1870
    .local v1, "thumbnailMgr":Lcom/infraware/common/database/ThumbnailManager;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrRequestThumbnailPath:Ljava/lang/String;

    invoke-virtual/range {v1 .. v8}, Lcom/infraware/common/database/ThumbnailManager;->InsertFileInfoToDB(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1873
    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    const/4 v9, 0x2

    if-ne v2, v9, :cond_1

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mRequestThumbnailOnSave:Z

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbSaveAndFinish:Z

    if-nez v2, :cond_1

    .line 1874
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mRequestThumbnailOnSave:Z

    .line 1875
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v21

    .line 1876
    .local v21, "ci":Lcom/infraware/office/evengine/EV$CONFIG_INFO;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v10, 0x0

    move-object/from16 v0, v21

    iget v11, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    invoke-virtual/range {v9 .. v20}, Lcom/infraware/office/evengine/EvInterface;->ISetZoom(IIIIIIIIIII)V

    .line 1880
    .end local v21    # "ci":Lcom/infraware/office/evengine/EV$CONFIG_INFO;
    :cond_1
    const/4 v3, 0x0

    .line 1881
    .end local v1    # "thumbnailMgr":Lcom/infraware/common/database/ThumbnailManager;
    .end local v3    # "byteArray":[B
    .end local v4    # "title":Ljava/lang/String;
    .end local v5    # "author":Ljava/lang/String;
    .end local v6    # "last_editor":Ljava/lang/String;
    .end local v7    # "page":I
    .end local v8    # "word":I
    :goto_2
    return-void

    .line 1796
    :catch_0
    move-exception v23

    .line 1797
    .local v23, "e":Ljava/io/FileNotFoundException;
    invoke-virtual/range {v23 .. v23}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 1798
    const-string/jumbo v2, "EvBaseViewerActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Tried to get a thumbnail from \""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mThumbnailPath:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Lcom/infraware/common/util/CMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1803
    .end local v23    # "e":Ljava/io/FileNotFoundException;
    .end local v25    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v26    # "fileInputStream":Ljava/io/FileInputStream;
    :catch_1
    move-exception v23

    .line 1804
    .local v23, "e":Ljava/io/IOException;
    invoke-virtual/range {v23 .. v23}, Ljava/io/IOException;->printStackTrace()V

    .line 1807
    :try_start_4
    invoke-virtual/range {v26 .. v26}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 1814
    :goto_3
    const/16 v25, 0x0

    .line 1815
    .end local v26    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v25    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_2

    .line 1809
    .end local v25    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v26    # "fileInputStream":Ljava/io/FileInputStream;
    :catch_2
    move-exception v24

    .line 1812
    .local v24, "e1":Ljava/io/IOException;
    invoke-virtual/range {v24 .. v24}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 1820
    .end local v23    # "e":Ljava/io/IOException;
    .end local v24    # "e1":Ljava/io/IOException;
    .restart local v3    # "byteArray":[B
    :catch_3
    move-exception v23

    .line 1821
    .restart local v23    # "e":Ljava/io/IOException;
    invoke-virtual/range {v23 .. v23}, Ljava/io/IOException;->printStackTrace()V

    .line 1824
    :try_start_5
    invoke-virtual/range {v26 .. v26}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 1831
    :goto_4
    const/16 v25, 0x0

    .line 1832
    .end local v26    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v25    # "fileInputStream":Ljava/io/FileInputStream;
    const/4 v3, 0x0

    .line 1833
    goto :goto_2

    .line 1826
    .end local v25    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v26    # "fileInputStream":Ljava/io/FileInputStream;
    :catch_4
    move-exception v24

    .line 1829
    .restart local v24    # "e1":Ljava/io/IOException;
    invoke-virtual/range {v24 .. v24}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 1840
    .end local v23    # "e":Ljava/io/IOException;
    .end local v24    # "e1":Ljava/io/IOException;
    :catch_5
    move-exception v24

    .line 1843
    .restart local v24    # "e1":Ljava/io/IOException;
    invoke-virtual/range {v24 .. v24}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 1864
    .end local v24    # "e1":Ljava/io/IOException;
    .end local v26    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "title":Ljava/lang/String;
    .restart local v5    # "author":Ljava/lang/String;
    .restart local v6    # "last_editor":Ljava/lang/String;
    .restart local v7    # "page":I
    .restart local v8    # "word":I
    .restart local v25    # "fileInputStream":Ljava/io/FileInputStream;
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetPDFTitle()Ljava/lang/String;

    move-result-object v4

    .line 1865
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetPDFAuthor()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1
.end method

.method public OnDrawThumbnailBitmap(I)V
    .locals 2
    .param p1, "nPageNum"    # I

    .prologue
    .line 1765
    const-string/jumbo v0, "EvBaseViewerActivity"

    const-string/jumbo v1, "OnDrawThumbnailBitmap()"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1766
    return-void
.end method

.method public OnEditCopyCut(IIILjava/lang/String;Ljava/lang/String;I)V
    .locals 9
    .param p1, "caller"    # I
    .param p2, "nType"    # I
    .param p3, "result"    # I
    .param p4, "text"    # Ljava/lang/String;
    .param p5, "data"    # Ljava/lang/String;
    .param p6, "nMode"    # I

    .prologue
    const/4 v8, 0x0

    .line 4705
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMWDnD()Lcom/infraware/common/multiwindow/MWDnDOperator;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMWDnD()Lcom/infraware/common/multiwindow/MWDnDOperator;

    move-result-object v0

    move v1, p1

    move v2, p2

    move-object v3, p4

    move-object v4, p5

    move v5, p6

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/common/multiwindow/MWDnDOperator;->onEditCopy(IILjava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4745
    :cond_0
    :goto_0
    return-void

    .line 4710
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mClipBoard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    if-eqz v0, :cond_0

    .line 4714
    const/4 v7, 0x0

    .line 4715
    .local v7, "popup":Landroid/widget/Toast;
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "FT03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 4718
    .local v6, "isSamsung":Z
    packed-switch p6, :pswitch_data_0

    .line 4744
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mClipBoard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    invoke-virtual {v0, p1, p2, p4, p5}, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->OnEditCopyCut(IILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 4720
    :pswitch_0
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700c7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    .line 4723
    if-nez v6, :cond_2

    .line 4724
    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 4727
    :pswitch_1
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700c6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    .line 4730
    if-nez v6, :cond_2

    .line 4731
    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 4734
    :pswitch_2
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07029b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    .line 4737
    if-nez v6, :cond_0

    .line 4738
    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 4718
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public OnEndFreeDraw()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 5163
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFreeDrawBar:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFreeDrawBar:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 5164
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->hide()V

    .line 5165
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 5166
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v0, :cond_0

    .line 5167
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->show()V

    .line 5179
    :cond_0
    :goto_0
    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onChangeFreeDrawMode(I)V

    .line 5180
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFreeDrawBar:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 5182
    :cond_1
    invoke-direct {p0, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setFreeDrawMode(Z)V

    .line 5183
    return-void

    .line 5170
    :cond_2
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 5171
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v0, :cond_0

    .line 5172
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->show()V

    goto :goto_0

    .line 5174
    :cond_3
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v0, :cond_0

    .line 5175
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->hide()V

    goto :goto_0
.end method

.method public OnEndPenDraw()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 5137
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getType()I

    move-result v0

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenType:I

    .line 5138
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getThickness()I

    move-result v0

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenSize:I

    .line 5139
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getColor()I

    move-result v0

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenColor:I

    .line 5141
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->setVisibility(I)V

    .line 5142
    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onChangeFreeDrawMode(I)V

    .line 5143
    invoke-direct {p0, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setFreeDrawMode(Z)V

    .line 5144
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbPenDraw:Z

    .line 5145
    return-void
.end method

.method protected OnFindDialog()V
    .locals 0

    .prologue
    .line 4657
    return-void
.end method

.method public OnGetResID(I)Ljava/lang/String;
    .locals 2
    .param p1, "nStrID"    # I

    .prologue
    .line 2179
    packed-switch p1, :pswitch_data_0

    .line 2187
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2181
    :pswitch_0
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0702bb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2183
    :pswitch_1
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0702b9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2185
    :pswitch_2
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0702ba

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2179
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public OnInitFreeDrawOption()V
    .locals 2

    .prologue
    .line 5105
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFreeDrawOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    if-eqz v1, :cond_0

    .line 5106
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFreeDrawOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 5107
    .local v0, "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getFreeDrawOptionMargin()I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 5110
    .end local v0    # "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    :cond_0
    return-void
.end method

.method public OnInitPenDrawOption()V
    .locals 3

    .prologue
    .line 5113
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    if-eqz v2, :cond_0

    .line 5114
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 5116
    .local v0, "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v2}, Lcom/infraware/office/actionbar/MainActionBar;->getPenDrawPanelPosition()Landroid/graphics/Point;

    move-result-object v1

    .line 5117
    .local v1, "pt":Landroid/graphics/Point;
    iget v2, v1, Landroid/graphics/Point;->x:I

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 5118
    iget v2, v1, Landroid/graphics/Point;->y:I

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 5121
    .end local v0    # "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    .end local v1    # "pt":Landroid/graphics/Point;
    :cond_0
    return-void
.end method

.method public OnLoadFail(I)V
    .locals 9
    .param p1, "nErrorCode"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 2079
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseView;->fillcolorsurfaceview(I)V

    .line 2080
    iput-boolean v8, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbLoadFail:Z

    .line 2083
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->isPrintMode()Z

    move-result v0

    if-ne v0, v8, :cond_0

    .line 2084
    iput-object v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvPrintHelper:Lcom/infraware/office/baseframe/porting/EvPrintHelper;

    .line 2085
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetInitialHeapSize()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->ISetHeapSize(I)V

    .line 2086
    const/16 v0, 0x1b

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->removeDialog(I)V

    .line 2087
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0, v5}, Lcom/infraware/office/baseframe/EvBaseView;->setPrintMode(Z)V

    .line 2092
    :goto_0
    const v6, 0x7f070132

    .line 2093
    .local v6, "nStrID":I
    sparse-switch p1, :sswitch_data_0

    .line 2123
    :goto_1
    iput v6, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mLoadFailStrID:I

    .line 2124
    invoke-virtual {p0, v6}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->AlertErrorAndCloseDlg(I)V

    .line 2125
    :goto_2
    return-void

    .line 2090
    .end local v6    # "nStrID":I
    :cond_0
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->messageHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    sget-object v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->dialogId:Ljava/lang/String;

    const/16 v3, 0x9

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->fillMsg(ILjava/lang/String;ILjava/lang/String;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 2097
    .restart local v6    # "nStrID":I
    :sswitch_0
    const v6, 0x7f070132

    .line 2098
    goto :goto_1

    .line 2101
    :sswitch_1
    const v6, 0x7f070133

    .line 2102
    goto :goto_1

    .line 2104
    :sswitch_2
    const v6, 0x7f070132

    .line 2105
    goto :goto_1

    .line 2108
    :sswitch_3
    const v6, 0x7f07019f

    .line 2109
    const/16 v0, -0x16

    if-ne v0, p1, :cond_1

    .line 2110
    invoke-virtual {p0, v8}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnPassWordActivity(Z)V

    goto :goto_2

    .line 2112
    :cond_1
    invoke-virtual {p0, v5}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnPassWordActivity(Z)V

    goto :goto_2

    .line 2115
    :sswitch_4
    const v6, 0x7f070120

    .line 2116
    goto :goto_1

    .line 2118
    :sswitch_5
    const v6, 0x7f07029c

    goto :goto_1

    .line 2093
    :sswitch_data_0
    .sparse-switch
        -0x21 -> :sswitch_5
        -0x16 -> :sswitch_3
        -0x7 -> :sswitch_4
        -0x5 -> :sswitch_3
        -0x4 -> :sswitch_0
        -0x3 -> :sswitch_1
        -0x2 -> :sswitch_2
        -0x1 -> :sswitch_0
        0x0 -> :sswitch_0
    .end sparse-switch
.end method

.method public OnObjectPoints(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V
    .locals 1
    .param p1, "param"    # Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;

    .prologue
    .line 4660
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/infraware/office/baseframe/EvBaseView;->OnObjectPoints(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V

    .line 4661
    return-void
.end method

.method public OnPageMove(III)V
    .locals 2
    .param p1, "nCurrentPage"    # I
    .param p2, "nTotalPage"    # I
    .param p3, "nErrorCode"    # I

    .prologue
    .line 2165
    const/4 v0, 0x6

    if-ne p3, v0, :cond_0

    .line 2166
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0701bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2170
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    const/16 v1, 0xb

    iput v1, v0, Lcom/infraware/office/baseframe/EvBaseView;->mCallBackID:I

    .line 2172
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->setPageInfoTimer()V

    .line 2174
    invoke-virtual {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setSheetTitle(I)V

    .line 2175
    return-void
.end method

.method protected OnPassWordActivity(Z)V
    .locals 2
    .param p1, "bReFail"    # Z

    .prologue
    .line 609
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/infraware/polarisoffice5/common/PasswordActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 610
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "REFAIL"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 611
    const/16 v1, 0x1e

    invoke-virtual {p0, v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 612
    return-void
.end method

.method public OnPrintCompleted(I)V
    .locals 5
    .param p1, "zoomScale"    # I

    .prologue
    .line 2321
    const-string/jumbo v2, "EvBaseViewerActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "print complete - zoom : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 2324
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->SDROOT_PATH:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "/print/resultPrint.PNG"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 2326
    .local v1, "out":Ljava/io/OutputStream;
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v2, v2, Lcom/infraware/office/baseframe/EvBaseView;->mPrintResultBitmap:Landroid/graphics/Bitmap;

    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    invoke-virtual {v2, v3, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 2327
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2333
    .end local v1    # "out":Ljava/io/OutputStream;
    :goto_0
    return-void

    .line 2328
    :catch_0
    move-exception v0

    .line 2329
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 2330
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 2331
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public OnPrintMode(Ljava/lang/String;)V
    .locals 7
    .param p1, "strPrintFile"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 2414
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvPrintHelper:Lcom/infraware/office/baseframe/porting/EvPrintHelper;

    if-eqz v0, :cond_0

    .line 2415
    if-nez p1, :cond_2

    .line 2417
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2418
    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvPrintHelper:Lcom/infraware/office/baseframe/porting/EvPrintHelper;

    .line 2420
    iget-object v6, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->messageHandler:Landroid/os/Handler;

    const/16 v1, 0xb

    move-object v0, p0

    move-object v4, v2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->fillMsg(ILjava/lang/String;ILjava/lang/String;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2422
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ICancel()V

    .line 2423
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->RequestViewerPrint()V

    .line 2424
    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintFilePath:Ljava/lang/String;

    .line 2425
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0, v3}, Lcom/infraware/office/baseframe/EvBaseView;->setPrintMode(Z)V

    .line 2454
    :cond_0
    :goto_0
    return-void

    .line 2428
    :cond_1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity$22;

    invoke-direct {v1, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$22;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 2441
    :cond_2
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPageCount:I

    iget v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintCurPage:I

    if-le v0, v1, :cond_0

    .line 2442
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintCurPage:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintCurPage:I

    .line 2444
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintProgressPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_3

    .line 2446
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintProgressPopup:Landroid/app/AlertDialog;

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701ad

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 2447
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintProgressPopup:Landroid/app/AlertDialog;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f07005f

    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2449
    :cond_3
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintProgressText:Landroid/widget/TextView;

    const-string/jumbo v1, "%s (%d/%d)"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const v4, 0x7f0701c3

    invoke-virtual {p0, v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintCurPage:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPageCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2450
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mProgress:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintCurPage:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0
.end method

.method public OnPrintedCount(I)V
    .locals 19
    .param p1, "nTotalCount"    # I

    .prologue
    .line 2458
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine$B;->USE_GOOGLE_PRINT(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2548
    :cond_0
    :goto_0
    return-void

    .line 2462
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvPrintHelper:Lcom/infraware/office/baseframe/porting/EvPrintHelper;

    if-eqz v1, :cond_0

    .line 2463
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvPrintHelper:Lcom/infraware/office/baseframe/porting/EvPrintHelper;

    .line 2465
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintProgressPopup:Landroid/app/AlertDialog;

    if-eqz v1, :cond_2

    .line 2466
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintProgressPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 2467
    :cond_2
    const/16 v1, 0x1a

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->removeDialog(I)V

    .line 2468
    if-nez p1, :cond_3

    .line 2469
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->messageHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    const/16 v2, 0xb

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v1, p0

    invoke-virtual/range {v1 .. v6}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->fillMsg(ILjava/lang/String;ILjava/lang/String;I)Landroid/os/Message;

    move-result-object v1

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2472
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintFilePath:Ljava/lang/String;

    .line 2473
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v1

    sget-object v2, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->PRINT_NO_CONTENT:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0, v2, v3}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto :goto_0

    .line 2478
    :cond_3
    move-object/from16 v0, p0

    iget v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintCurPage:I

    move/from16 v0, p1

    if-ne v0, v1, :cond_4

    .line 2479
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/infraware/office/baseframe/EvBaseView;->setPrintMode(Z)V

    .line 2484
    :cond_4
    :try_start_0
    invoke-static/range {p0 .. p0}, Lcom/infraware/common/define/CMModelDefine;->isEpsonPrinter(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2485
    new-instance v10, Landroid/content/Intent;

    const-string/jumbo v1, "com.epson.mobilephone.samsungprint.photo"

    invoke-direct {v10, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2487
    .local v10, "epsonPrintIntent":Landroid/content/Intent;
    const/4 v11, 0x3

    .line 2488
    .local v11, "file_type":I
    const-string/jumbo v1, "FILE_TYPE"

    invoke-virtual {v10, v1, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2491
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintFilePath:Ljava/lang/String;

    .line 2492
    .local v12, "folder_name":Ljava/lang/String;
    const-string/jumbo v1, "FOLDER_NAME"

    invoke-virtual {v10, v1, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2495
    invoke-static {v12}, Lcom/infraware/common/util/FileUtils;->getArrayChildFileName(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 2496
    .local v7, "arrayFilePath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string/jumbo v1, "FILE_NAME"

    invoke-virtual {v10, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2499
    const/16 v16, 0x0

    .line 2500
    .local v16, "media_type":I
    const-string/jumbo v1, "MEDIA_TYPE"

    move/from16 v0, v16

    invoke-virtual {v10, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2503
    const/4 v15, 0x0

    .line 2504
    .local v15, "media_size":I
    const-string/jumbo v1, "MEDIA_SIZE"

    invoke-virtual {v10, v1, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2507
    const/4 v8, 0x0

    .line 2508
    .local v8, "color_mode":I
    const-string/jumbo v1, "COLOR_MODE"

    invoke-virtual {v10, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2511
    const/4 v14, 0x1

    .line 2512
    .local v14, "layout_type":I
    const-string/jumbo v1, "LAYOUT_TYPE"

    invoke-virtual {v10, v1, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2515
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getPackageName()Ljava/lang/String;

    move-result-object v17

    .line 2516
    .local v17, "package_name":Ljava/lang/String;
    const-string/jumbo v1, "PACKAGE_NAME"

    move-object/from16 v0, v17

    invoke-virtual {v10, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2518
    const/16 v1, 0xf

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2545
    .end local v7    # "arrayFilePath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v8    # "color_mode":I
    .end local v10    # "epsonPrintIntent":Landroid/content/Intent;
    .end local v11    # "file_type":I
    .end local v12    # "folder_name":Ljava/lang/String;
    .end local v14    # "layout_type":I
    .end local v15    # "media_size":I
    .end local v16    # "media_type":I
    .end local v17    # "package_name":Ljava/lang/String;
    :goto_1
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintFilePath:Ljava/lang/String;

    goto/16 :goto_0

    .line 2521
    :cond_5
    :try_start_1
    new-instance v13, Landroid/content/Intent;

    const-string/jumbo v1, "com.sec.android.app.mobileprint.PRINT"

    invoke-direct {v13, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2522
    .local v13, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "android.intent.extra.STREAM"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintFilePath:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v13, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2523
    const-string/jumbo v1, "android.intent.extra.TITLE"

    const-string/jumbo v2, "Polaris Office Viewer 5"

    invoke-virtual {v13, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2525
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "FT03"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2526
    const-string/jumbo v1, "android.intent.extra.SUBJECT"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-static {v2}, Lcom/infraware/common/util/FileUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2528
    const-string/jumbo v1, "android.intent.extra.shortcut.NAME"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-virtual {v13, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2529
    const-string/jumbo v1, "android.intent.extra.shortcut.ICON"

    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPrintStartPage:I

    invoke-virtual {v13, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2530
    const-string/jumbo v1, "android.intent.extra.shortcut.ICON_RESOURCE"

    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPrintEndPage:I

    invoke-virtual {v13, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2531
    const-string/jumbo v1, "android.intent.extra.shortcut.INTENT"

    const/4 v2, 0x1

    invoke-virtual {v13, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2532
    const/high16 v1, 0x10000000

    invoke-virtual {v13, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2533
    const-string/jumbo v1, "large_document"

    const/4 v2, 0x1

    invoke-virtual {v13, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2535
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPrintStartPage:I

    .line 2536
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPrintEndPage:I

    .line 2538
    :cond_6
    const/16 v1, 0xf

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 2540
    .end local v13    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v9

    .line 2542
    .local v9, "e":Landroid/content/ActivityNotFoundException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->messageHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    const/16 v2, 0xb

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v1, p0

    invoke-virtual/range {v1 .. v6}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->fillMsg(ILjava/lang/String;ILjava/lang/String;I)Landroid/os/Message;

    move-result-object v1

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1
.end method

.method public OnProgress(II)V
    .locals 0
    .param p1, "EV_PROGRESS_TYPE"    # I
    .param p2, "nProgressValue"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2192
    return-void
.end method

.method public OnProgressStart(I)V
    .locals 0
    .param p1, "EV_PROGRESS_TYPE"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2196
    return-void
.end method

.method public OnSearchMenu()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/high16 v9, 0x41c80000    # 25.0f

    const/4 v10, 0x0

    .line 4808
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 4809
    .local v1, "alItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchAdapter;

    const v7, 0x7f03003d

    invoke-direct {v0, p0, p0, v7, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchAdapter;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Landroid/content/Context;ILjava/util/List;)V

    .line 4813
    .local v0, "adapter":Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchAdapter;
    const v7, 0x7f070166

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4815
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine;->isChina()Z

    move-result v7

    if-nez v7, :cond_0

    .line 4816
    const v7, 0x7f070251

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4819
    :cond_0
    invoke-static {p0}, Lcom/infraware/common/define/CMModelDefine$Diotek;->USE_DICTIONARY_SEARCH(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_1

    iget v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    const/4 v8, 0x2

    if-eq v7, v8, :cond_1

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$Diotek;->isLoadComplete()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 4821
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v7

    invoke-virtual {v7}, Lcom/infraware/office/evengine/EvInterface;->IGetMarkString()Ljava/lang/String;

    move-result-object v6

    .line 4822
    .local v6, "str":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    const/16 v8, 0x64

    if-ge v7, v8, :cond_1

    .line 4823
    const v7, 0x7f07008a

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4827
    .end local v6    # "str":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ne v7, v11, :cond_2

    .line 4828
    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-direct {p0, v7}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onClickSearchItem(I)V

    .line 4866
    :goto_0
    return-void

    .line 4832
    :cond_2
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0xe

    if-lt v7, v8, :cond_4

    .line 4833
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v7

    invoke-direct {v2, p0, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 4837
    .local v2, "builder":Landroid/app/AlertDialog$Builder;
    :goto_1
    const v7, 0x7f0702c7

    invoke-virtual {v2, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 4838
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mOnClickListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v0, v7}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 4840
    instance-of v7, p0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    if-eqz v7, :cond_3

    move-object v4, p0

    .line 4842
    check-cast v4, Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    .line 4843
    .local v4, "evBaseEditorActivity":Lcom/infraware/office/baseframe/EvBaseEditorActivity;
    iput-boolean v11, v4, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mIsReplace:Z

    .line 4846
    .end local v4    # "evBaseEditorActivity":Lcom/infraware/office/baseframe/EvBaseEditorActivity;
    :cond_3
    const/16 v7, 0x33

    iput v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPopupType:I

    .line 4847
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    iput-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMsgPopup:Landroid/app/AlertDialog;

    .line 4850
    invoke-static {}, Lcom/infraware/common/util/Utils;->isL()Z

    move-result v7

    if-nez v7, :cond_5

    .line 4851
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMsgPopup:Landroid/app/AlertDialog;

    invoke-virtual {v7}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v5

    .line 4853
    .local v5, "lv":Landroid/widget/ListView;
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f05002e

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 4854
    .local v3, "colorValue":Ljava/lang/String;
    new-instance v7, Landroid/graphics/drawable/ColorDrawable;

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v8

    invoke-direct {v7, v8}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v5, v7}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 4855
    const/high16 v7, 0x3f000000    # 0.5f

    invoke-static {p0, v7}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v7

    invoke-virtual {v5, v7}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 4856
    const v7, 0x7f020130

    invoke-virtual {v5, v7}, Landroid/widget/ListView;->setSelector(I)V

    .line 4864
    .end local v3    # "colorValue":Ljava/lang/String;
    :goto_2
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMsgPopup:Landroid/app/AlertDialog;

    invoke-virtual {v7, v10}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 4865
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMsgPopup:Landroid/app/AlertDialog;

    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 4835
    .end local v2    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v5    # "lv":Landroid/widget/ListView;
    :cond_4
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .restart local v2    # "builder":Landroid/app/AlertDialog$Builder;
    goto :goto_1

    .line 4860
    :cond_5
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMsgPopup:Landroid/app/AlertDialog;

    invoke-virtual {v7}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v5

    .line 4861
    .restart local v5    # "lv":Landroid/widget/ListView;
    invoke-static {p0, v9}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v7

    const/high16 v8, 0x41200000    # 10.0f

    invoke-static {p0, v8}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v8

    invoke-static {p0, v9}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v9

    invoke-virtual {v5, v7, v8, v9, v10}, Landroid/widget/ListView;->setPadding(IIII)V

    goto :goto_2
.end method

.method public OnSearchMode(IIII)V
    .locals 10
    .param p1, "nResult"    # I
    .param p2, "nCurrentPage"    # I
    .param p3, "nTotalPage"    # I
    .param p4, "nReplaceAllCount"    # I

    .prologue
    const/16 v9, 0x10

    const v5, 0x7f0701dc

    const/4 v8, 0x0

    const/4 v3, 0x2

    const/4 v1, 0x1

    .line 2215
    const-string/jumbo v0, "EvBaseViewerActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "OnSearchMode nReuslt = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2217
    sparse-switch p1, :sswitch_data_0

    .line 2272
    :goto_0
    return-void

    .line 2219
    :sswitch_0
    invoke-static {p0, v5, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2220
    iput v8, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mViewSearchMode:I

    .line 2221
    sget-object v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->searchResultCode:Ljava/lang/String;

    sget-object v4, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->searchContent:Ljava/lang/String;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->fillMsg(ILjava/lang/String;ILjava/lang/String;I)Landroid/os/Message;

    move-result-object v6

    .line 2223
    .local v6, "msg":Landroid/os/Message;
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->messageHandler:Landroid/os/Handler;

    invoke-virtual {v0, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 2226
    .end local v6    # "msg":Landroid/os/Message;
    :sswitch_1
    invoke-static {p0, v5, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2227
    iput v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mViewSearchMode:I

    .line 2228
    sget-object v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->searchResultCode:Ljava/lang/String;

    sget-object v4, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->searchContent:Ljava/lang/String;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->fillMsg(ILjava/lang/String;ILjava/lang/String;I)Landroid/os/Message;

    move-result-object v6

    .line 2230
    .restart local v6    # "msg":Landroid/os/Message;
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->messageHandler:Landroid/os/Handler;

    invoke-virtual {v0, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 2233
    .end local v6    # "msg":Landroid/os/Message;
    :sswitch_2
    const/16 v0, 0x50

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mViewSearchMode:I

    .line 2234
    sget-object v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->searchResultCode:Ljava/lang/String;

    sget-object v4, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->searchContent:Ljava/lang/String;

    const v5, 0x7f0701d3

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->fillMsg(ILjava/lang/String;ILjava/lang/String;I)Landroid/os/Message;

    move-result-object v6

    .line 2236
    .restart local v6    # "msg":Landroid/os/Message;
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->messageHandler:Landroid/os/Handler;

    invoke-virtual {v0, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 2239
    .end local v6    # "msg":Landroid/os/Message;
    :sswitch_3
    iput v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mViewSearchMode:I

    .line 2240
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->messageHandler:Landroid/os/Handler;

    sget-object v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->searchResultCode:Ljava/lang/String;

    const/4 v4, 0x0

    move-object v0, p0

    move v3, v1

    move v5, v8

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->fillMsg(ILjava/lang/String;ILjava/lang/String;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2242
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/infraware/office/util/EvUtil;->hideIme(Landroid/content/Context;Landroid/os/IBinder;)V

    goto :goto_0

    .line 2245
    :sswitch_4
    const/4 v0, 0x3

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mViewSearchMode:I

    .line 2246
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->messageHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x320

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 2250
    :sswitch_5
    const-string/jumbo v0, "EvBaseViewerActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "nReplaceAllCount = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2251
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->messageHandler:Landroid/os/Handler;

    sget-object v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->searchResultCode:Ljava/lang/String;

    const/16 v3, 0x31

    sget-object v4, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->searchContent:Ljava/lang/String;

    move-object v0, p0

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->fillMsg(ILjava/lang/String;ILjava/lang/String;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 2257
    :sswitch_6
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->messageHandler:Landroid/os/Handler;

    sget-object v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->searchResultCode:Ljava/lang/String;

    const/16 v3, 0x32

    sget-object v4, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->searchContent:Ljava/lang/String;

    move-object v0, p0

    move v5, v1

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->fillMsg(ILjava/lang/String;ILjava/lang/String;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 2261
    :sswitch_7
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->messageHandler:Landroid/os/Handler;

    sget-object v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->searchResultCode:Ljava/lang/String;

    const/16 v3, 0x33

    sget-object v4, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->searchContent:Ljava/lang/String;

    move-object v0, p0

    move v5, v1

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->fillMsg(ILjava/lang/String;ILjava/lang/String;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 2265
    :sswitch_8
    iput v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mViewSearchMode:I

    .line 2266
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->messageHandler:Landroid/os/Handler;

    sget-object v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->searchResultCode:Ljava/lang/String;

    sget-object v4, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->searchContent:Ljava/lang/String;

    const v5, 0x7f0701db

    move-object v0, p0

    move v3, v9

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->fillMsg(ILjava/lang/String;ILjava/lang/String;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 2217
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_3
        0x2 -> :sswitch_1
        0x3 -> :sswitch_4
        0x10 -> :sswitch_8
        0x31 -> :sswitch_5
        0x32 -> :sswitch_6
        0x33 -> :sswitch_7
        0x50 -> :sswitch_2
    .end sparse-switch
.end method

.method public OnTerminate()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 2277
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->isPrintMode()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2278
    iput-object v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvPrintHelper:Lcom/infraware/office/baseframe/porting/EvPrintHelper;

    .line 2279
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetInitialHeapSize()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->ISetHeapSize(I)V

    .line 2281
    const/16 v0, 0x1b

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->removeDialog(I)V

    .line 2282
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0, v5}, Lcom/infraware/office/baseframe/EvBaseView;->setPrintMode(Z)V

    .line 2288
    :goto_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity$21;

    invoke-direct {v1, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$21;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2306
    return-void

    .line 2286
    :cond_0
    iget-object v6, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->messageHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    sget-object v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->dialogId:Ljava/lang/String;

    const/16 v3, 0x9

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->fillMsg(ILjava/lang/String;ILjava/lang/String;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public OnTextToSpeachString(Ljava/lang/String;)V
    .locals 0
    .param p1, "strTTS"    # Ljava/lang/String;

    .prologue
    .line 2552
    return-void
.end method

.method public OnTotalLoadComplete()V
    .locals 5

    .prologue
    .line 2049
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v2}, Lcom/infraware/office/actionbar/MainActionBar;->setOnTotalLoadComplete()V

    .line 2057
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 2058
    .local v0, "start":J
    const-string/jumbo v2, "***"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setOnTotalLoadComplete:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 2061
    iget-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mIsContentSearch:Z

    if-eqz v2, :cond_0

    .line 2062
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 2067
    :pswitch_0
    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v2, v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ShowContentSearch(J)V

    .line 2072
    :cond_0
    :goto_0
    return-void

    .line 2065
    :pswitch_1
    const-wide/16 v2, 0x3e8

    invoke-virtual {p0, v2, v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ShowContentSearch(J)V

    goto :goto_0

    .line 2062
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public ReplaceBarClose()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4156
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->FindBarClose()Z

    move-result v2

    if-ne v2, v0, :cond_1

    .line 4177
    :cond_0
    :goto_0
    return v0

    .line 4159
    :cond_1
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBar:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBar:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v2

    if-nez v2, :cond_4

    .line 4160
    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ReplaceBarShow(Z)V

    .line 4161
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->ISearchStop()V

    .line 4162
    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setMatchCase(I)V

    .line 4163
    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setMatchWhole(I)V

    .line 4164
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;

    if-eqz v1, :cond_3

    .line 4165
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindReplaceText:Ljava/lang/String;

    .line 4166
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;

    if-eqz v1, :cond_2

    .line 4167
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 4172
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_DstTxt:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    .line 4173
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_DstTxt:Landroid/widget/EditText;

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 4170
    :cond_3
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindReplaceText:Ljava/lang/String;

    goto :goto_1

    :cond_4
    move v0, v1

    .line 4177
    goto :goto_0
.end method

.method protected ReplaceBarShow(Z)V
    .locals 3
    .param p1, "bShow"    # Z

    .prologue
    const/4 v2, 0x0

    .line 4139
    if-eqz p1, :cond_0

    .line 4140
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->dismissPopupWindow()Z

    .line 4141
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->FindBarClose()Z

    .line 4142
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 4143
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseView;->setActionMode(I)V

    .line 4144
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 4145
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->FindbarSoftInputManager()V

    .line 4152
    :goto_0
    invoke-virtual {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setShowSearchBar(Z)V

    .line 4153
    return-void

    .line 4147
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBar:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 4148
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0, v2}, Lcom/infraware/office/baseframe/EvBaseView;->setActionMode(I)V

    .line 4149
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->hideSoftInputInFindReplaceMode()V

    goto :goto_0
.end method

.method public ReplaceText(I)V
    .locals 11
    .param p1, "nMode"    # I

    .prologue
    const/4 v6, 0x1

    const/16 v10, 0x26

    const/16 v9, 0x14

    const/4 v8, 0x5

    const/4 v4, 0x0

    .line 4083
    const/4 v7, 0x0

    .line 4084
    .local v7, "nFlag":I
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 4085
    .local v1, "OrgText":Ljava/lang/String;
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_DstTxt:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 4086
    .local v5, "DstText":Ljava/lang/String;
    const-string/jumbo v0, "EvBaseViewerActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "orgText = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "DstText = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4088
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocExtensionType()I

    move-result v0

    if-eq v0, v8, :cond_0

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocExtensionType()I

    move-result v0

    if-eq v0, v9, :cond_0

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocExtensionType()I

    move-result v0

    if-ne v0, v10, :cond_2

    .line 4091
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMatchCase()I

    move-result v0

    if-ne v0, v6, :cond_1

    .line 4092
    or-int/lit8 v7, v7, 0x2

    .line 4093
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMatchWhole()I

    move-result v0

    if-ne v0, v6, :cond_2

    .line 4094
    or-int/lit8 v7, v7, 0x4

    .line 4097
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceNextBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 4098
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 4099
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceAllBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 4101
    packed-switch p1, :pswitch_data_0

    .line 4136
    .end local v5    # "DstText":Ljava/lang/String;
    :goto_0
    return-void

    .line 4103
    .restart local v5    # "DstText":Ljava/lang/String;
    :pswitch_0
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocExtensionType()I

    move-result v0

    if-eq v0, v8, :cond_3

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocExtensionType()I

    move-result v0

    if-eq v0, v9, :cond_3

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocExtensionType()I

    move-result v0

    if-ne v0, v10, :cond_4

    .line 4106
    :cond_3
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v7}, Lcom/infraware/office/evengine/EvInterface;->ISheetFindReplace(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 4108
    :cond_4
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMatchWhole()I

    move-result v2

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMatchCase()I

    move-result v3

    const/4 v5, 0x0

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->ISetReplace(Ljava/lang/String;IIILjava/lang/String;I)V

    goto :goto_0

    .line 4112
    :pswitch_1
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocExtensionType()I

    move-result v0

    if-eq v0, v8, :cond_5

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocExtensionType()I

    move-result v0

    if-eq v0, v9, :cond_5

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocExtensionType()I

    move-result v0

    if-ne v0, v10, :cond_6

    .line 4115
    :cond_5
    or-int/lit8 v7, v7, 0x20

    .line 4116
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v1, v5, v7}, Lcom/infraware/office/evengine/EvInterface;->ISheetFindReplace(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 4118
    :cond_6
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMatchWhole()I

    move-result v2

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMatchCase()I

    move-result v3

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->ISetReplace(Ljava/lang/String;IIILjava/lang/String;I)V

    goto :goto_0

    .line 4122
    :pswitch_2
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocExtensionType()I

    move-result v0

    if-eq v0, v8, :cond_7

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocExtensionType()I

    move-result v0

    if-eq v0, v9, :cond_7

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocExtensionType()I

    move-result v0

    if-ne v0, v10, :cond_8

    .line 4125
    :cond_7
    or-int/lit8 v7, v7, 0x10

    .line 4126
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v1, v5, v7}, Lcom/infraware/office/evengine/EvInterface;->ISheetFindReplace(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 4128
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->showDialog(I)V

    .line 4131
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMatchWhole()I

    move-result v2

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMatchCase()I

    move-result v3

    const/4 v6, 0x2

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->ISetReplace(Ljava/lang/String;IIILjava/lang/String;I)V

    goto :goto_0

    .line 4101
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public ReqeustViewerBookClip(Landroid/content/Intent;)V
    .locals 4
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 2753
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->EV()Lcom/infraware/office/evengine/EV;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getBookClip()Lcom/infraware/office/evengine/EV$BOOK_CLIP;

    move-result-object v0

    .line 2754
    .local v0, "a_Clip":Lcom/infraware/office/evengine/EV$BOOK_CLIP;
    const-string/jumbo v1, "FILEPATH"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/infraware/office/evengine/EV$BOOK_CLIP;->szFilePath:Ljava/lang/String;

    .line 2755
    const-string/jumbo v1, "CLIPNAME"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/infraware/office/evengine/EV$BOOK_CLIP;->szClipName:Ljava/lang/String;

    .line 2756
    const-string/jumbo v1, "EvBaseViewerActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "a_Clip.szFilePath  = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/infraware/office/evengine/EV$BOOK_CLIP;->szFilePath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " a_Clip.szClipName = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/infraware/office/evengine/EV$BOOK_CLIP;->szClipName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2757
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1, v0}, Lcom/infraware/office/evengine/EvInterface;->IApplyBookClip(Lcom/infraware/office/evengine/EV$BOOK_CLIP;)V

    .line 2758
    return-void
.end method

.method public RequestThumbnail(Landroid/content/Intent;)V
    .locals 8
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 2747
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object v1, p0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->ISetListener(Lcom/infraware/office/evengine/EvListener$ViewerListener;Lcom/infraware/office/evengine/EvListener$EditorListener;Lcom/infraware/office/evengine/EvListener$WordEditorListener;Lcom/infraware/office/evengine/EvListener$PptEditorListener;Lcom/infraware/office/evengine/EvListener$SheetEditorListener;Lcom/infraware/office/evengine/EvListener$PdfViewerListener;)V

    .line 2748
    const-string/jumbo v0, "THUMBNAIL_GOTO"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 2749
    .local v7, "nPageNum":I
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, v7}, Lcom/infraware/office/evengine/EvInterface;->IMovePage(II)V

    .line 2750
    return-void
.end method

.method public RequestViewerPrint()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 2763
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-nez v1, :cond_1

    .line 2791
    .end local p0    # "this":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    :cond_0
    :goto_0
    return-void

    .line 2766
    .restart local p0    # "this":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    :cond_1
    const/4 v0, 0x0

    .line 2767
    .local v0, "bLandScape":I
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 2768
    const/4 v0, 0x1

    .line 2770
    :cond_2
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/infraware/office/baseframe/EvBaseView;->setPrintMode(Z)V

    .line 2777
    iget v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_3

    iget v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    if-ne v1, v4, :cond_4

    .line 2778
    :cond_3
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IEditPageRedrawBitmap()V

    .line 2787
    :goto_1
    iget v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    if-ne v1, v4, :cond_0

    if-nez v0, :cond_0

    move-object v1, p0

    .line 2788
    check-cast v1, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onShowPSlideManage()V

    .line 2789
    check-cast p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .end local p0    # "this":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onShowSlideManageOpenButton()Z

    goto :goto_0

    .line 2781
    .restart local p0    # "this":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    :cond_4
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseView;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/EvBaseView;->getHeight()I

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IChangeScreen(III)V

    .line 2784
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IReDraw()V

    goto :goto_1
.end method

.method public SearchSetPopupWindow(I)V
    .locals 21
    .param p1, "parentID"    # I

    .prologue
    .line 1087
    const-string/jumbo v2, "layout_inflater"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/view/LayoutInflater;

    .line 1088
    .local v16, "inflater":Landroid/view/LayoutInflater;
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 1089
    .local v14, "SrcListItem":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/infraware/polarisoffice5/common/MultiListItem;>;"
    const v18, 0x7f050133

    .line 1090
    .local v18, "nColor":I
    const v5, 0x7f020016

    .line 1092
    .local v5, "nCheckBoxDrawable":I
    new-instance v1, Lcom/infraware/polarisoffice5/common/MultiListItem;

    const/4 v2, 0x7

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0701e9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v6, -0x1

    const/4 v7, -0x1

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-direct/range {v1 .. v8}, Lcom/infraware/polarisoffice5/common/MultiListItem;-><init>(ILjava/lang/String;Ljava/lang/String;IIII)V

    .line 1096
    .local v1, "item":Lcom/infraware/polarisoffice5/common/MultiListItem;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbMatchCase:I

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/MultiListItem;->setRightBtnState(I)V

    .line 1098
    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    const/16 v3, 0x26

    if-ne v2, v3, :cond_2

    .line 1099
    :cond_0
    new-instance v6, Lcom/infraware/polarisoffice5/common/MultiListItem;

    const/4 v7, 0x7

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0701ea

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v11, -0x1

    const/4 v12, -0x1

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    move v10, v5

    invoke-direct/range {v6 .. v13}, Lcom/infraware/polarisoffice5/common/MultiListItem;-><init>(ILjava/lang/String;Ljava/lang/String;IIII)V

    .line 1110
    .local v6, "item1":Lcom/infraware/polarisoffice5/common/MultiListItem;
    :goto_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbMatchWhole:I

    invoke-virtual {v6, v2}, Lcom/infraware/polarisoffice5/common/MultiListItem;->setRightBtnState(I)V

    .line 1111
    invoke-virtual {v14, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1112
    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1114
    new-instance v15, Lcom/infraware/polarisoffice5/common/MultiAdapter;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->messageHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v14, v2}, Lcom/infraware/polarisoffice5/common/MultiAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Landroid/os/Handler;)V

    .line 1116
    .local v15, "adapter":Lcom/infraware/polarisoffice5/common/MultiAdapter;
    const v2, 0x7f030042

    const/4 v3, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v19

    .line 1117
    .local v19, "popupView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mTouchListener:Landroid/view/View$OnTouchListener;

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1119
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v4, v4, Lcom/infraware/office/baseframe/EvBaseView;->mFilePath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/infraware/office/evengine/EvInterface;->getDocFileExtentionType(Ljava/lang/String;)I

    move-result v3

    if-ne v2, v3, :cond_3

    .line 1120
    new-instance v2, Landroid/widget/PopupWindow;

    const/high16 v3, 0x43330000    # 179.0f

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->dipToPx(F)F

    move-result v3

    float-to-int v3, v3

    const/high16 v4, 0x42af0000    # 87.5f

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->dipToPx(F)F

    move-result v4

    float-to-int v4, v4

    move-object/from16 v0, v19

    invoke-direct {v2, v0, v3, v4}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPopup:Landroid/widget/PopupWindow;

    .line 1122
    const v2, 0x7f0b01d7

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindReplaceModeBtn:Landroid/widget/Button;

    .line 1123
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindReplaceModeBtn:Landroid/widget/Button;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 1125
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPopup:Landroid/widget/PopupWindow;

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0201a6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1154
    :cond_1
    :goto_1
    const v2, 0x7f0b01d6

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/ListView;

    .line 1155
    .local v17, "listView":Landroid/widget/ListView;
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1158
    new-instance v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity$15;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v14, v15}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$15;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Ljava/util/ArrayList;Lcom/infraware/polarisoffice5/common/MultiAdapter;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1192
    new-instance v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity$16;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$16;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1207
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPopup:Landroid/widget/PopupWindow;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 1208
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPopup:Landroid/widget/PopupWindow;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    .line 1209
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPopup:Landroid/widget/PopupWindow;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 1210
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPopup:Landroid/widget/PopupWindow;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDismissListener:Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupWindowDissmissListener;

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 1213
    invoke-virtual/range {p0 .. p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/ImageButton;

    .line 1214
    .local v20, "searchOptionButton":Landroid/widget/ImageButton;
    const/4 v2, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1217
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDismissListener:Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupWindowDissmissListener;

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupWindowDissmissListener;->setCallerButton(Landroid/widget/ImageButton;)V

    .line 1219
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPopup:Landroid/widget/PopupWindow;

    invoke-virtual/range {p0 .. p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v3, v4, v7}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    .line 1220
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v2}, Landroid/widget/PopupWindow;->update()V

    .line 1221
    return-void

    .line 1105
    .end local v6    # "item1":Lcom/infraware/polarisoffice5/common/MultiListItem;
    .end local v15    # "adapter":Lcom/infraware/polarisoffice5/common/MultiAdapter;
    .end local v17    # "listView":Landroid/widget/ListView;
    .end local v19    # "popupView":Landroid/view/View;
    .end local v20    # "searchOptionButton":Landroid/widget/ImageButton;
    :cond_2
    new-instance v6, Lcom/infraware/polarisoffice5/common/MultiListItem;

    const/4 v7, 0x7

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0701eb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v11, -0x1

    const/4 v12, -0x1

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    move v10, v5

    invoke-direct/range {v6 .. v13}, Lcom/infraware/polarisoffice5/common/MultiListItem;-><init>(ILjava/lang/String;Ljava/lang/String;IIII)V

    .restart local v6    # "item1":Lcom/infraware/polarisoffice5/common/MultiListItem;
    goto/16 :goto_0

    .line 1127
    .restart local v15    # "adapter":Lcom/infraware/polarisoffice5/common/MultiAdapter;
    .restart local v19    # "popupView":Landroid/view/View;
    :cond_3
    new-instance v2, Landroid/widget/PopupWindow;

    const/high16 v3, 0x43330000    # 179.0f

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->dipToPx(F)F

    move-result v3

    float-to-int v3, v3

    const v4, 0x43068000    # 134.5f

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->dipToPx(F)F

    move-result v4

    float-to-int v4, v4

    move-object/from16 v0, v19

    invoke-direct {v2, v0, v3, v4}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPopup:Landroid/widget/PopupWindow;

    .line 1129
    const v2, 0x7f0b01d7

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindReplaceModeBtn:Landroid/widget/Button;

    .line 1130
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindReplaceModeBtn:Landroid/widget/Button;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 1131
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindReplaceModeBtn:Landroid/widget/Button;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1133
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v2}, Lcom/infraware/office/actionbar/MainActionBar;->getDocType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v2}, Lcom/infraware/office/actionbar/MainActionBar;->IsSheetProtect()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1134
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindReplaceModeBtn:Landroid/widget/Button;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1135
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindReplaceModeBtn:Landroid/widget/Button;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setFocusable(Z)V

    .line 1138
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->IsViewerMode()Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_5

    .line 1139
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindReplaceModeBtn:Landroid/widget/Button;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 1140
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPopup:Landroid/widget/PopupWindow;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->getHeight()I

    move-result v3

    const/high16 v4, 0x423c0000    # 47.0f

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->dipToPx(F)F

    move-result v4

    float-to-int v4, v4

    sub-int/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 1143
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindBar:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindBar:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v2

    if-nez v2, :cond_6

    .line 1144
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPopup:Landroid/widget/PopupWindow;

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0201a6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1145
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindReplaceModeBtn:Landroid/widget/Button;

    const v3, 0x7f0701d0

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(I)V

    .line 1148
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBar:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBar:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    .line 1149
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPopup:Landroid/widget/PopupWindow;

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0201a6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1150
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindReplaceModeBtn:Landroid/widget/Button;

    const v3, 0x7f070135

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(I)V

    goto/16 :goto_1
.end method

.method public SheetInputTextboxText()V
    .locals 0

    .prologue
    .line 4954
    return-void
.end method

.method public SheetTextboxHide()V
    .locals 0

    .prologue
    .line 4950
    return-void
.end method

.method public ShowContentSearch(J)V
    .locals 2
    .param p1, "aDelay"    # J

    .prologue
    .line 4536
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mIsContentSearch:Z

    if-nez v0, :cond_0

    .line 4558
    :goto_0
    return-void

    .line 4537
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity$52;

    invoke-direct {v1, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$52;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public ShowIme()V
    .locals 2

    .prologue
    .line 4962
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->bEncryptDoc:Z

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 4963
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-static {v0}, Lcom/infraware/office/util/EvUtil;->showIme(Landroid/view/View;)V

    .line 4964
    :cond_0
    return-void
.end method

.method protected checkPrintStatus()V
    .locals 18

    .prologue
    .line 3966
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvPrintHelper:Lcom/infraware/office/baseframe/porting/EvPrintHelper;

    if-nez v1, :cond_0

    .line 3967
    new-instance v1, Lcom/infraware/office/baseframe/porting/EvPrintHelper;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lcom/infraware/office/baseframe/porting/EvPrintHelper;-><init>(Landroid/app/Activity;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvPrintHelper:Lcom/infraware/office/baseframe/porting/EvPrintHelper;

    .line 3969
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->makeRootDir()V

    .line 3970
    sget-object v1, Lcom/infraware/common/define/CMModelDefine;->CUSTOM_PRINT_PATH:Ljava/lang/String;

    const-string/jumbo v2, "polarisPrint"

    sget-object v3, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->PRINT_PATH:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v1, v2, v3, v0}, Lcom/infraware/common/util/FileUtils;->createCustomPath(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPath:Ljava/lang/String;

    .line 3972
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintFilePath:Ljava/lang/String;

    .line 3973
    new-instance v16, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintFilePath:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3974
    .local v16, "newFolder":Ljava/io/File;
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_4

    .line 3976
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->mkdir()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 3977
    const/4 v1, 0x1

    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v1, v2}, Ljava/io/File;->setExecutable(ZZ)Z

    move-result v13

    .line 3978
    .local v13, "b":Z
    const/4 v1, 0x1

    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v1, v2}, Ljava/io/File;->setReadable(ZZ)Z

    move-result v13

    .line 3979
    const/4 v1, 0x1

    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v1, v2}, Ljava/io/File;->setWritable(ZZ)Z

    move-result v13

    .line 3981
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPrintStartPage:I

    .line 3982
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPrintEndPage:I

    .line 3984
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintAll:Landroid/widget/RadioButton;

    invoke-virtual {v1}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 3985
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPrintStartPage:I

    .line 3986
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v1

    iget v1, v1, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nTotalPages:I

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPrintEndPage:I

    .line 4005
    :cond_1
    :goto_0
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintCurPage:I

    .line 4006
    move-object/from16 v0, p0

    iget v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPrintEndPage:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPrintStartPage:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPageCount:I

    .line 4009
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity$46;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$46;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    const-wide/16 v3, 0x12c

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 4016
    const/16 v6, 0x12

    .line 4017
    .local v6, "nRetType":I
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocExtensionType()I

    move-result v1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocExtensionType()I

    move-result v1

    const/16 v2, 0x14

    if-ne v1, v2, :cond_3

    .line 4020
    :cond_2
    or-int/lit16 v6, v6, 0x100

    .line 4022
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x4

    move-object/from16 v0, p0

    iget v3, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPrintStartPage:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPrintEndPage:I

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintFilePath:Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v1 .. v12}, Lcom/infraware/office/evengine/EvInterface;->ISetPrintEx(IIILjava/lang/String;ILjava/lang/String;Ljava/lang/String;IIII)V

    .line 4026
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/infraware/office/baseframe/EvBaseView;->setPrintMode(Z)V

    .line 4029
    .end local v6    # "nRetType":I
    .end local v13    # "b":Z
    :cond_4
    return-void

    .line 3988
    .restart local v13    # "b":Z
    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintCurrent:Landroid/widget/RadioButton;

    invoke-virtual {v1}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 3989
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v1

    iget v1, v1, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPrintStartPage:I

    .line 3990
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v1

    iget v1, v1, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPrintEndPage:I

    goto/16 :goto_0

    .line 3991
    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEditLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPage:Landroid/widget/RadioButton;

    invoke-virtual {v1}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintStartEdit:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintStartEdit:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-lez v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEndEdit:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEndEdit:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 3994
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintStartEdit:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v17

    .line 3995
    .local v17, "startValue":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEndEdit:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v14

    .line 3996
    .local v14, "endValue":Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPrintStartPage:I

    .line 3997
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPrintEndPage:I

    .line 3999
    move-object/from16 v0, p0

    iget v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPrintStartPage:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPrintEndPage:I

    if-le v1, v2, :cond_1

    .line 4000
    move-object/from16 v0, p0

    iget v15, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPrintStartPage:I

    .line 4001
    .local v15, "nValue":I
    move-object/from16 v0, p0

    iget v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPrintEndPage:I

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPrintStartPage:I

    .line 4002
    move-object/from16 v0, p0

    iput v15, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPrintEndPage:I

    goto/16 :goto_0
.end method

.method public clearDictionaryPanel()Z
    .locals 1

    .prologue
    .line 5552
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5553
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->hidePanel()V

    .line 5554
    const/4 v0, 0x1

    .line 5557
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public connectGooleSearch()V
    .locals 6

    .prologue
    .line 5643
    const-string/jumbo v2, ""

    .line 5644
    .local v2, "select":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 5645
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    check-cast v3, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .line 5646
    .local v3, "sheetActivity":Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getCellText()Ljava/lang/String;

    move-result-object v2

    .line 5651
    .end local v3    # "sheetActivity":Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    :goto_0
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 5652
    :cond_0
    const-string/jumbo v2, " "

    .line 5654
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v4, "android.intent.action.WEB_SEARCH"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 5655
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v4, "query"

    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5657
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 5661
    :goto_1
    return-void

    .line 5648
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_2
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v4

    invoke-virtual {v4}, Lcom/infraware/office/evengine/EvInterface;->IGetMarkString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 5658
    .restart local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 5659
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public delayFindText(I)V
    .locals 4
    .param p1, "bPrev"    # I

    .prologue
    .line 5757
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity$58;

    invoke-direct {v1, p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$58;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;I)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 5765
    return-void
.end method

.method public dipToPx(F)F
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 1081
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v0, p1, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    return v0
.end method

.method public dismissPopupWindow()Z
    .locals 1

    .prologue
    .line 1069
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMsgPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 1070
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMsgPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 1071
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPopup:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_1

    .line 1072
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 1073
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPopup:Landroid/widget/PopupWindow;

    .line 1074
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 1075
    const/4 v0, 0x1

    .line 1077
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    .line 5057
    iget v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFreeDrawMode:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_4

    .line 5058
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 5059
    .local v0, "rect":Landroid/graphics/Rect;
    const/4 v1, 0x0

    .line 5061
    .local v1, "view":Landroid/view/View;
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_2

    .line 5064
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .line 5072
    :cond_0
    :goto_0
    if-eqz v1, :cond_4

    .line 5074
    invoke-virtual {v1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 5075
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-nez v3, :cond_4

    .line 5076
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getTrackingMode()Z

    move-result v3

    if-nez v3, :cond_3

    .line 5077
    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onChangeFreeDrawMode(I)V

    .line 5089
    .end local v0    # "rect":Landroid/graphics/Rect;
    .end local v1    # "view":Landroid/view/View;
    :cond_1
    :goto_1
    return v2

    .line 5066
    .restart local v0    # "rect":Landroid/graphics/Rect;
    .restart local v1    # "view":Landroid/view/View;
    :cond_2
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFreeDrawOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFreeDrawOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    .line 5069
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFreeDrawOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    goto :goto_0

    .line 5078
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-ne v3, v2, :cond_1

    .line 5079
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->setTrackingMode(Z)V

    .line 5081
    invoke-super {p0, p1}, Lcom/infraware/common/baseactivity/BaseActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    goto :goto_1

    .line 5089
    .end local v0    # "rect":Landroid/graphics/Rect;
    .end local v1    # "view":Landroid/view/View;
    :cond_4
    invoke-super {p0, p1}, Lcom/infraware/common/baseactivity/BaseActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    goto :goto_1
.end method

.method public endPanningMode()V
    .locals 2

    .prologue
    .line 5364
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseView;->setActionMode(I)V

    .line 5365
    return-void
.end method

.method public exportEmail(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 12
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "mimeType"    # Ljava/lang/String;

    .prologue
    const/16 v11, 0x34

    const/4 v10, 0x0

    .line 3490
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "FT03"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 3491
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 3493
    .local v7, "targetShareIntents":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v8, "android.intent.action.SENDTO"

    invoke-direct {v2, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3494
    .local v2, "intent":Landroid/content/Intent;
    const-string/jumbo v8, "message/rfc822"

    invoke-virtual {v2, v8}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 3495
    const-string/jumbo v8, "android.intent.extra.STREAM"

    invoke-virtual {v2, v8, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 3496
    const-string/jumbo v8, "mailto:"

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 3498
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/high16 v9, 0x10000

    invoke-virtual {v8, v2, v9}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    .line 3499
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_2

    .line 3500
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    .line 3501
    .local v5, "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v8, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v8, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 3503
    .local v4, "packageName":Ljava/lang/String;
    const-string/jumbo v8, "com.glympse.android.glympse"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 3504
    new-instance v6, Landroid/content/Intent;

    const-string/jumbo v8, "android.intent.action.SEND"

    invoke-direct {v6, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3505
    .local v6, "targetIntent":Landroid/content/Intent;
    invoke-virtual {v6, p2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 3506
    const/high16 v8, 0x10000000

    invoke-virtual {v6, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 3507
    const-string/jumbo v8, "android.intent.extra.STREAM"

    invoke-virtual {v6, v8, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 3508
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 3509
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3512
    .end local v4    # "packageName":Ljava/lang/String;
    .end local v5    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    .end local v6    # "targetIntent":Landroid/content/Intent;
    :cond_1
    invoke-interface {v7, v10}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Intent;

    const/4 v9, 0x0

    invoke-static {v8, v9}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 3513
    .local v0, "chooserIntent":Landroid/content/Intent;
    const-string/jumbo v9, "android.intent.extra.INITIAL_INTENTS"

    new-array v8, v10, [Landroid/os/Parcelable;

    invoke-interface {v7, v8}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Landroid/os/Parcelable;

    invoke-virtual {v0, v9, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 3515
    invoke-virtual {p0, v0, v11}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 3528
    .end local v0    # "chooserIntent":Landroid/content/Intent;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v7    # "targetShareIntents":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    :cond_2
    :goto_1
    return-void

    .line 3519
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_3
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v8, "android.intent.action.SEND"

    invoke-direct {v2, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3520
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string/jumbo v8, "message/rfc822"

    invoke-virtual {v2, v8}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 3521
    const-string/jumbo v8, "android.intent.extra.STREAM"

    invoke-virtual {v2, v8, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 3526
    invoke-virtual {p0, v2, v11}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1
.end method

.method public fileOperationInfo()V
    .locals 4

    .prologue
    .line 2816
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/infraware/filemanager/FileInfoActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2817
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "key_current_file"

    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2818
    const-string/jumbo v2, "INTCMD"

    iget v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mInternalCmdType:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2820
    iget v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_1

    .line 2822
    :cond_0
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetSummaryData()Lcom/infraware/office/evengine/EV$SUMMARY_DATA;

    move-result-object v0

    .line 2824
    .local v0, "data":Lcom/infraware/office/evengine/EV$SUMMARY_DATA;
    const-string/jumbo v2, "key_current_file_doc_type"

    iget v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2825
    const-string/jumbo v2, "key_current_file_doc_page"

    iget v3, v0, Lcom/infraware/office/evengine/EV$SUMMARY_DATA;->nPage:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2826
    const-string/jumbo v2, "key_current_file_doc_words"

    iget v3, v0, Lcom/infraware/office/evengine/EV$SUMMARY_DATA;->nWords:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2829
    .end local v0    # "data":Lcom/infraware/office/evengine/EV$SUMMARY_DATA;
    :cond_1
    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->startActivity(Landroid/content/Intent;)V

    .line 2830
    return-void
.end method

.method protected fillMsg(ILjava/lang/String;ILjava/lang/String;I)Landroid/os/Message;
    .locals 3
    .param p1, "what"    # I
    .param p2, "key1"    # Ljava/lang/String;
    .param p3, "value1"    # I
    .param p4, "key2"    # Ljava/lang/String;
    .param p5, "value2"    # I

    .prologue
    .line 2199
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->messageHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 2200
    .local v1, "message":Landroid/os/Message;
    iput p1, v1, Landroid/os/Message;->what:I

    .line 2201
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2203
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz p2, :cond_0

    .line 2204
    invoke-virtual {v0, p2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2205
    :cond_0
    if-eqz p4, :cond_1

    .line 2206
    invoke-virtual {v0, p4, p5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2208
    :cond_1
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 2210
    return-object v1
.end method

.method public finish()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2556
    const-string/jumbo v0, "EvBaseViewerActivity"

    const-string/jumbo v1, "Called finish"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2557
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbFinishCalled:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_6

    .line 2560
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mIsContentSearch:Z

    .line 2563
    iput-boolean v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbFinishCalled:Z

    .line 2565
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbSaveAndFinish:Z

    if-eqz v0, :cond_1

    .line 2566
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbLoadFail:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbLoadComplete:Z

    if-eqz v0, :cond_0

    .line 2567
    const-string/jumbo v0, "EvBaseViewerActivity"

    const-string/jumbo v1, "finish() :: Call getPageThumbnail()"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 2568
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mnSaveThumbOption:I

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getPageThumbnail(I)V

    .line 2570
    :cond_0
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbLoadFail:Z

    .line 2571
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbSaveAndFinish:Z

    .line 2574
    :cond_1
    sput-object v3, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mErrorAndClosePopup:Landroid/app/AlertDialog;

    .line 2576
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_3

    .line 2577
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IClose()V

    .line 2580
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->DeleteOpenedFileList(Ljava/lang/String;)Z

    .line 2582
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->getInterfaceHandleAddress()I

    move-result v0

    if-eqz v0, :cond_2

    .line 2583
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->getInterfaceHandleAddress()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->deleteInterfaceHandle(I)V

    .line 2585
    :cond_2
    iput-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 2588
    :cond_3
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mTempPath:Ljava/lang/String;

    sget-object v1, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->OFFICE_ROOT_PATH:Ljava/lang/String;

    if-eq v0, v1, :cond_4

    .line 2589
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mTempPath:Ljava/lang/String;

    invoke-static {v0}, Lcom/infraware/common/util/FileUtils;->deleteAllFilesInDirectory(Ljava/lang/String;)V

    .line 2592
    :cond_4
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_bByMultipleLauncher:Z

    if-nez v0, :cond_5

    .line 2593
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/infraware/common/util/FileUtils;->deleteDirectory(Ljava/io/File;Z)V

    .line 2594
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrEmailFolderPath:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/infraware/common/util/FileUtils;->deleteDirectory(Ljava/lang/String;Z)V

    .line 2597
    :cond_5
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPath:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/infraware/common/util/FileUtils;->deleteDirectory(Ljava/lang/String;Z)V

    .line 2602
    :cond_6
    invoke-static {}, Lcom/infraware/polarisoffice5/MyApplication;->getInstance()Lcom/infraware/polarisoffice5/MyApplication;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/infraware/polarisoffice5/MyApplication;->setCurrentViewer(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    .line 2603
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->finish()V

    .line 2604
    return-void
.end method

.method public finishEx()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2618
    const-string/jumbo v0, "EvBaseViewerActivity"

    const-string/jumbo v1, "Called finishEx"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2619
    sput-object v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mErrorAndClosePopup:Landroid/app/AlertDialog;

    .line 2622
    invoke-static {}, Lcom/infraware/polarisoffice5/MyApplication;->getInstance()Lcom/infraware/polarisoffice5/MyApplication;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/MyApplication;->setCurrentViewer(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    .line 2623
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->finish()V

    .line 2624
    return-void
.end method

.method public fisnishbyMultipleLauncher()V
    .locals 2

    .prologue
    .line 2609
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2610
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.infraware.polarisviewer5.CLOSE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2611
    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 2613
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_bByMultipleLauncher:Z

    .line 2614
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->finish()V

    .line 2615
    return-void
.end method

.method public getCheckOriginalKeep()Z
    .locals 1

    .prologue
    .line 385
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mCheckOriginalKeep:Z

    return v0
.end method

.method public getClipBoardData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4692
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mClipBoard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getClipboardhelper()Lcom/infraware/office/baseframe/porting/EvClipboardHelper;
    .locals 1

    .prologue
    .line 4681
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mClipBoard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    return-object v0
.end method

.method public getDictionaryPanelMain()Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    return-object v0
.end method

.method public getDocExtensionType()I
    .locals 1

    .prologue
    .line 763
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocExtensionType:I

    return v0
.end method

.method public getDocType()I
    .locals 1

    .prologue
    .line 775
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    return v0
.end method

.method public getFreeDrawOptionMargin()I
    .locals 2

    .prologue
    .line 5331
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 5332
    const/high16 v0, 0x42400000    # 48.0f

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->dipToPx(F)F

    move-result v0

    float-to-int v0, v0

    .line 5334
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x42200000    # 40.0f

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->dipToPx(F)F

    move-result v0

    float-to-int v0, v0

    goto :goto_0
.end method

.method public getIsEncryptDoc()Z
    .locals 1

    .prologue
    .line 4958
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->bEncryptDoc:Z

    return v0
.end method

.method public getMWDnD()Lcom/infraware/common/multiwindow/MWDnDOperator;
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMWDnDOperator:Lcom/infraware/common/multiwindow/MWDnDOperator;

    return-object v0
.end method

.method public getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    return-object v0
.end method

.method public getMatchCase()I
    .locals 1

    .prologue
    .line 755
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbMatchCase:I

    return v0
.end method

.method public getMatchWhole()I
    .locals 1

    .prologue
    .line 759
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbMatchWhole:I

    return v0
.end method

.method public getMotionEventRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 5052
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getMulticount()I
    .locals 1

    .prologue
    .line 5342
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->GetMultiInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->nObjectCount:I

    return v0
.end method

.method public getOpenFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5593
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public getOpenFilePath(Landroid/content/Intent;)Ljava/lang/String;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1598
    const/4 v1, 0x0

    .line 1599
    .local v1, "path":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 1600
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1601
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 1602
    const-string/jumbo v2, "key_filename"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1604
    .end local v0    # "extras":Landroid/os/Bundle;
    :cond_0
    return-object v1
.end method

.method public getPageThumbnail()V
    .locals 7

    .prologue
    const/high16 v4, 0x43540000    # 212.0f

    const/4 v1, 0x1

    .line 4648
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->makeThumbnailDir()V

    .line 4649
    const-string/jumbo v0, "EvBaseViewerActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "getPageThumbnail() :: mThumbnailPath = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mThumbnailPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4651
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-static {p0, v4}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v3

    invoke-static {p0, v4}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v4

    iget-object v5, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mThumbnailPath:Ljava/lang/String;

    move v2, v1

    move v6, v1

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IGetPageThumbnail(IIIILjava/lang/String;I)V

    .line 4654
    return-void
.end method

.method public getPageThumbnail(I)V
    .locals 7
    .param p1, "nOption"    # I

    .prologue
    const/4 v1, 0x1

    const/high16 v4, 0x43540000    # 212.0f

    .line 4635
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->makeThumbnailDir()V

    .line 4636
    const-string/jumbo v0, "EvBaseViewerActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "getPageThumbnail() :: mThumbnailPath = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mThumbnailPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4639
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-nez v0, :cond_0

    .line 4640
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 4642
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-static {p0, v4}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v3

    invoke-static {p0, v4}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v4

    iget-object v5, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mThumbnailPath:Ljava/lang/String;

    move v2, v1

    move v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IGetPageThumbnail(IIIILjava/lang/String;I)V

    .line 4645
    return-void
.end method

.method public getPenDrawOption()Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPenDrawOption:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    return-object v0
.end method

.method public getQuickScroll()Lcom/infraware/polarisoffice5/common/QuickScrollView;
    .locals 1

    .prologue
    .line 1456
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mQuickScroll:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    return-object v0
.end method

.method public getReflowText()Z
    .locals 1

    .prologue
    .line 397
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbReflowText:Z

    return v0
.end method

.method public getScreenView()Lcom/infraware/office/baseframe/EvBaseView;
    .locals 1

    .prologue
    .line 1507
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    return-object v0
.end method

.method public getSdcardIntentFilter()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 1495
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1496
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1497
    const-string/jumbo v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 1498
    return-object v0
.end method

.method protected getZoomValueArray()[I
    .locals 7

    .prologue
    .line 3204
    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    .line 3205
    .local v4, "zoomValueVector":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Integer;>;"
    const/16 v5, 0x8

    new-array v2, v5, [I

    fill-array-data v2, :array_0

    .line 3207
    .local v2, "zoomList":[I
    iget-object v5, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v5}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v1

    .line 3208
    .local v1, "info":Lcom/infraware/office/evengine/EV$CONFIG_INFO;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v5, v2

    if-ge v0, v5, :cond_1

    .line 3209
    aget v5, v2, v0

    iget v6, v1, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nMinZoom:I

    if-lt v5, v6, :cond_0

    aget v5, v2, v0

    iget v6, v1, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nMaxZoom:I

    if-gt v5, v6, :cond_0

    .line 3210
    aget v5, v2, v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 3208
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3213
    :cond_1
    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v5

    new-array v3, v5, [I

    .line 3214
    .local v3, "zoomValueArray":[I
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v5

    if-ge v0, v5, :cond_2

    .line 3215
    invoke-virtual {v4, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    aput v5, v3, v0

    .line 3214
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3217
    :cond_2
    return-object v3

    .line 3205
    nop

    :array_0
    .array-data 4
        0x19
        0x32
        0x4b
        0x64
        0x7d
        0xc8
        0x12c
        0x190
    .end array-data
.end method

.method public getmView()Landroid/view/View;
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    return-object v0
.end method

.method public hasClipBoardData()I
    .locals 1

    .prologue
    .line 4685
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mClipBoard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->hasText()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4686
    const/4 v0, 0x1

    .line 4688
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hideDictionaryPanel()V
    .locals 1

    .prologue
    .line 4671
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v0, :cond_0

    .line 4672
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->hidePanel()V

    .line 4674
    :cond_0
    return-void
.end method

.method protected initReplaceBar()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 4212
    const v0, 0x7f0b0069

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBar:Landroid/widget/LinearLayout;

    .line 4214
    const v0, 0x7f0b006b

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceNextBtn:Landroid/widget/ImageButton;

    .line 4215
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceNextBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4216
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceNextBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 4217
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceNextBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 4219
    const v0, 0x7f0b006e

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBtn:Landroid/widget/ImageButton;

    .line 4220
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4221
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 4222
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 4223
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 4225
    const v0, 0x7f0b006f

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceAllBtn:Landroid/widget/ImageButton;

    .line 4226
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceAllBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4227
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceAllBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 4228
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceAllBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 4229
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceAllBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 4231
    const v0, 0x7f0b006a

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceSetBtn:Landroid/widget/ImageButton;

    .line 4232
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceSetBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4234
    const v0, 0x7f0b006c

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;

    .line 4235
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->replaceWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 4236
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;

    const v1, 0x7f0701d6

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 4237
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->DISABLE_EMOJI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4238
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;

    const-string/jumbo v1, "inputType=DisableEmoji"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 4240
    :cond_0
    const v0, 0x7f0b0070

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_DstTxt:Landroid/widget/EditText;

    .line 4241
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_DstTxt:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->replaceWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 4242
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_DstTxt:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 4243
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_DstTxt:Landroid/widget/EditText;

    const v1, 0x7f0701d7

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 4244
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->DISABLE_EMOJI()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4245
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_DstTxt:Landroid/widget/EditText;

    const-string/jumbo v1, "inputType=DisableEmoji"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 4247
    :cond_1
    new-instance v0, Lcom/infraware/polarisoffice5/common/EditInputFilter;

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/EditInputFilter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_oInputFilter:Lcom/infraware/polarisoffice5/common/EditInputFilter;

    .line 4248
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_oInputFilter:Lcom/infraware/polarisoffice5/common/EditInputFilter;

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/EditInputFilter;->setMaxLength(I)V

    .line 4249
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_oInputFilter:Lcom/infraware/polarisoffice5/common/EditInputFilter;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/EditInputFilter;->getFilters()[Landroid/text/InputFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 4250
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_DstTxt:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_oInputFilter:Lcom/infraware/polarisoffice5/common/EditInputFilter;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/EditInputFilter;->getFilters()[Landroid/text/InputFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 4253
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;

    new-instance v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity$47;

    invoke-direct {v1, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$47;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 4264
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_DstTxt:Landroid/widget/EditText;

    new-instance v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity$48;

    invoke-direct {v1, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$48;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 4276
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 4277
    return-void
.end method

.method public isFindReplaceMode()Z
    .locals 1

    .prologue
    .line 4985
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindBar:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBar:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 4987
    :cond_1
    const/4 v0, 0x1

    .line 4988
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMultiWindow()Z
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mIsMultiWindow:Z

    return v0
.end method

.method public isScroll(Z)V
    .locals 0
    .param p1, "mbScroll"    # Z

    .prologue
    .line 4992
    iput-boolean p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbScroll:Z

    .line 4993
    return-void
.end method

.method public declared-synchronized isSdCardAction(Ljava/lang/String;)V
    .locals 1
    .param p1, "nowAction"    # Ljava/lang/String;

    .prologue
    .line 4469
    monitor-enter p0

    :try_start_0
    const-string/jumbo v0, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4476
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-static {v0}, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->isSdCardFile(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4478
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IClose()V

    .line 4479
    const v0, 0x7f0700a5

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->AlertErrorAndCloseDlg(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4496
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 4481
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->isMountedPath(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4483
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IClose()V

    .line 4484
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->showErrorDlg()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 4469
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isShowMemo()Z
    .locals 1

    .prologue
    .line 4937
    const/4 v0, 0x0

    return v0
.end method

.method public isShowSearchBar()Z
    .locals 1

    .prologue
    .line 1594
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbShowSearchBar:Z

    return v0
.end method

.method public isShownDictionaryPanel()Z
    .locals 1

    .prologue
    .line 4753
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v0, :cond_0

    .line 4754
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4755
    const/4 v0, 0x1

    .line 4757
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isbPenDraw()Z
    .locals 1

    .prologue
    .line 5338
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbPenDraw:Z

    return v0
.end method

.method protected makeOfficeDir()V
    .locals 3

    .prologue
    .line 1231
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPath:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mBookMarkPath:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mTempPath:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mRootPath:Ljava/lang/String;

    .line 1233
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->makeRootDir()V

    .line 1235
    sget-object v0, Lcom/infraware/common/define/CMModelDefine;->CUSTOM_TEMP_PATH:Ljava/lang/String;

    const-string/jumbo v1, "polarisTemp"

    sget-object v2, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->TEMP_PATH:Ljava/lang/String;

    invoke-static {v0, v1, v2, p0}, Lcom/infraware/common/util/FileUtils;->createCustomPath(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mTempPath:Ljava/lang/String;

    .line 1237
    const-string/jumbo v0, ""

    const-string/jumbo v1, "polarisBookmark"

    sget-object v2, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->BOOKMARK_PATH:Ljava/lang/String;

    invoke-static {v0, v1, v2, p0}, Lcom/infraware/common/util/FileUtils;->createCustomPath(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mBookMarkPath:Ljava/lang/String;

    .line 1239
    sget-object v0, Lcom/infraware/common/define/CMModelDefine;->CUSTOM_PRINT_PATH:Ljava/lang/String;

    const-string/jumbo v1, "polarisPrint"

    sget-object v2, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->PRINT_PATH:Ljava/lang/String;

    invoke-static {v0, v1, v2, p0}, Lcom/infraware/common/util/FileUtils;->createCustomPath(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPath:Ljava/lang/String;

    .line 1241
    return-void
.end method

.method protected makeRootDir()V
    .locals 2

    .prologue
    .line 1225
    sget-object v0, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->OFFICE_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 1226
    sget-object v0, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->OFFICE_ROOT_PATH:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {v0, v1, p0}, Lcom/infraware/common/util/FileUtils;->makeDirectory(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mRootPath:Ljava/lang/String;

    .line 1227
    :cond_0
    return-void
.end method

.method protected makeThumbnailDir()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1244
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mThumbnailPath:Ljava/lang/String;

    .line 1246
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mThumbnailPath:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1247
    const-string/jumbo v0, "polarisTemp"

    const/4 v1, 0x0

    invoke-static {v0, v1, p0}, Lcom/infraware/common/util/FileUtils;->makeDirectory(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mThumbnailPath:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1249
    sget-object v0, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->TEMP_PATH:Ljava/lang/String;

    invoke-static {v0, v2, p0}, Lcom/infraware/common/util/FileUtils;->makeDirectory(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mThumbnailPath:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1251
    sget-object v0, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->SDROOT_PATH:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mThumbnailPath:Ljava/lang/String;

    .line 1253
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mThumbnailPath:Ljava/lang/String;

    invoke-static {v0}, Lcom/infraware/common/util/FileUtils;->addPathDelemeter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mThumbnailPath:Ljava/lang/String;

    .line 1254
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mThumbnailPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->GetInterfaceHandle()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mThumbnailPath:Ljava/lang/String;

    .line 1255
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mThumbnailPath:Ljava/lang/String;

    invoke-static {v0, v2, p0}, Lcom/infraware/common/util/FileUtils;->makeDirectory(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;

    .line 1256
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x1

    .line 2694
    invoke-super {p0, p1, p2, p3}, Lcom/infraware/common/baseactivity/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2697
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    .line 2698
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->GetInterfaceHandle()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->SetInterfaceHandleAddress(I)V

    .line 2699
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setEvListener()V

    .line 2701
    if-eqz p3, :cond_1

    .line 2702
    const-string/jumbo v0, "SdCardUnMounted"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-ne v0, v2, :cond_1

    .line 2703
    const-string/jumbo v0, "EvBaseViewerActivity"

    const-string/jumbo v1, "OnActivityReult SdCardUnMounted"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2704
    const-string/jumbo v0, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->isSdCardAction(Ljava/lang/String;)V

    .line 2708
    :cond_1
    const/16 v0, 0xf

    if-ne p1, v0, :cond_4

    .line 2709
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->RequestViewerPrint()V

    .line 2715
    :cond_2
    :goto_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_3

    .line 2716
    sparse-switch p1, :sswitch_data_0

    .line 2734
    :cond_3
    :goto_1
    return-void

    .line 2711
    :cond_4
    const/16 v0, 0x34

    if-ne p1, v0, :cond_2

    .line 2712
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onSendEMailEnd()V

    goto :goto_0

    .line 2718
    :sswitch_0
    invoke-virtual {p0, p3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ReqeustViewerBookClip(Landroid/content/Intent;)V

    goto :goto_1

    .line 2721
    :sswitch_1
    invoke-virtual {p0, p3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->RequestThumbnail(Landroid/content/Intent;)V

    goto :goto_1

    .line 2724
    :sswitch_2
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setResultFromSettingActivity()V

    goto :goto_1

    .line 2727
    :sswitch_3
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->bEncryptDoc:Z

    .line 2728
    invoke-direct {p0, p3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onResultPassWordActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 2716
    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_1
        0x7 -> :sswitch_0
        0xd -> :sswitch_2
        0x1e -> :sswitch_3
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4034
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4035
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->hidePanel()V

    .line 4079
    :cond_0
    :goto_0
    return-void

    .line 4039
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->FindBarClose()Z

    move-result v0

    if-ne v0, v3, :cond_2

    .line 4040
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->hideAllToolbarDelay()Z

    .line 4041
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->show()V

    goto :goto_0

    .line 4045
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->hideAllToolbarDelay()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4049
    :cond_3
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFreeDrawMode:I

    if-eqz v0, :cond_6

    .line 4050
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFreeDrawMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 4051
    invoke-virtual {p0, v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onChangeFreeDrawMode(I)V

    goto :goto_0

    .line 4054
    :cond_4
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbPenDraw:Z

    if-eqz v0, :cond_5

    .line 4056
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetInfraPenDrawMode(I)V

    .line 4057
    const/16 v1, 0x34

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object v0, p0

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    .line 4059
    :cond_5
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnEndFreeDraw()V

    goto :goto_0

    .line 4064
    :cond_6
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v0, :cond_7

    .line 4065
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->dismissMenuPopup()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4071
    :cond_7
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvPrintHelper:Lcom/infraware/office/baseframe/porting/EvPrintHelper;

    if-eqz v0, :cond_8

    .line 4072
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvPrintHelper:Lcom/infraware/office/baseframe/porting/EvPrintHelper;

    .line 4073
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetInitialHeapSize()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->ISetHeapSize(I)V

    goto :goto_0

    .line 4077
    :cond_8
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->finish()V

    .line 4078
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onChangeFreeDrawMode(I)V
    .locals 1
    .param p1, "mode"    # I

    .prologue
    .line 5244
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbPenDraw:Z

    if-eqz v0, :cond_0

    .line 5245
    invoke-direct {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onChangePenDrawMode(I)V

    .line 5300
    :cond_0
    return-void
.end method

.method public onChangeScreen(I)V
    .locals 2
    .param p1, "nType"    # I

    .prologue
    const/4 v1, 0x1

    .line 1726
    if-ne p1, v1, :cond_2

    .line 1728
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v0, :cond_0

    .line 1729
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/MainActionBar;->setPenToolBarOfDirection(Z)V

    .line 1730
    :cond_0
    const-string/jumbo v0, "EvBaseViewerActivity"

    const-string/jumbo v1, "onChangeScreen ORIENTATION_PORTRAIT"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1738
    :goto_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v0, :cond_1

    .line 1739
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->dismissMenuPopup()Z

    .line 1750
    :cond_1
    return-void

    .line 1733
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v0, :cond_3

    .line 1734
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/MainActionBar;->setPenToolBarOfDirection(Z)V

    .line 1735
    :cond_3
    const-string/jumbo v0, "Plasma Activity"

    const-string/jumbo v1, "onChangeScreen ORIENTATION_LANDSCAPE"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onChangedFromMultiwindowToNormalWindow()V
    .locals 0

    .prologue
    .line 5639
    return-void
.end method

.method protected onChangedFromNormalWindowToMultiWindow()V
    .locals 0

    .prologue
    .line 5638
    return-void
.end method

.method public onCheckboxClicked(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 2834
    move-object v1, p1

    check-cast v1, Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 2837
    .local v0, "checked":Z
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 2842
    :goto_0
    return-void

    .line 2839
    :pswitch_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 2837
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b003c
        :pswitch_0
    .end packed-switch
.end method

.method public onClickActionBar(I)V
    .locals 4
    .param p1, "nViewId"    # I

    .prologue
    .line 4513
    const v0, 0x7f0b0009

    if-ne p1, v0, :cond_0

    .line 4532
    :goto_0
    return-void

    .line 4516
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->FindBarClose()Z

    .line 4517
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ReplaceBarClose()Z

    .line 4520
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity$51;

    invoke-direct {v1, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$51;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 10
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1624
    invoke-super {p0, p1}, Lcom/infraware/common/baseactivity/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1625
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->dismissPopupWindow()Z

    .line 1629
    const-string/jumbo v7, "[onConfigueChanged]"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "mViewSearchMode == "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mViewSearchMode:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1630
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindBar:Landroid/widget/LinearLayout;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindBar:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v7

    if-nez v7, :cond_0

    iget v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mViewSearchMode:I

    const/4 v8, 0x3

    if-ne v7, v8, :cond_0

    .line 1631
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v7}, Lcom/infraware/office/evengine/EvInterface;->ISearchStop()V

    .line 1633
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->CancelFind()V

    .line 1635
    invoke-static {}, Lcom/infraware/polarisoffice5/common/DlgFactory;->getInstance()Lcom/infraware/polarisoffice5/common/DlgFactory;

    move-result-object v7

    const/16 v8, 0xa

    invoke-virtual {v7, p0, v8}, Lcom/infraware/polarisoffice5/common/DlgFactory;->dismisProgress(Landroid/app/Activity;I)V

    .line 1641
    :cond_0
    invoke-static {}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->configurationChanged()V

    .line 1643
    iget v7, p1, Landroid/content/res/Configuration;->fontScale:F

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget v8, v8, Landroid/content/res/Configuration;->fontScale:F

    cmpl-float v7, v7, v8

    if-nez v7, :cond_2

    .line 1644
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindBar:Landroid/widget/LinearLayout;

    if-eqz v7, :cond_9

    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindBar:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v7

    if-nez v7, :cond_9

    move v0, v5

    .line 1645
    .local v0, "isFindMode":Z
    :goto_0
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBar:Landroid/widget/LinearLayout;

    if-eqz v7, :cond_a

    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplaceBar:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v7

    if-nez v7, :cond_a

    move v1, v5

    .line 1647
    .local v1, "isReplaceMode":Z
    :goto_1
    if-eq v0, v5, :cond_1

    if-ne v1, v5, :cond_2

    .line 1648
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    .line 1649
    .local v3, "theme":Landroid/content/res/Resources$Theme;
    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    .line 1650
    .local v4, "typedValue":Landroid/util/TypedValue;
    const v7, 0x101039f

    invoke-virtual {v3, v7, v4, v5}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1677
    .end local v0    # "isFindMode":Z
    .end local v1    # "isReplaceMode":Z
    .end local v3    # "theme":Landroid/content/res/Resources$Theme;
    .end local v4    # "typedValue":Landroid/util/TypedValue;
    :cond_2
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v7, v7, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v7, v7, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v7, v7, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    if-eqz v7, :cond_3

    .line 1678
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v7, v7, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v7, v7, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismissAll()V

    .line 1681
    :cond_3
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v7}, Lcom/infraware/office/actionbar/MainActionBar;->isVisible()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1683
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v7}, Lcom/infraware/office/actionbar/MainActionBar;->dismissInlinePopupMenu()Z

    .line 1686
    :cond_4
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v7

    if-nez v7, :cond_5

    .line 1688
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getWindow()Landroid/view/Window;

    move-result-object v7

    iget v8, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-static {v7, v8}, Lcom/infraware/common/util/Utils;->setScreenMode(Landroid/view/Window;I)V

    .line 1691
    :cond_5
    iget v7, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v7, v5, :cond_b

    move v2, v5

    .line 1713
    .local v2, "portrait":Z
    :goto_2
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v7, :cond_6

    .line 1714
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v7, p1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1716
    :cond_6
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mConfigurationChangeNotifier:Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;

    if-eqz v7, :cond_7

    .line 1717
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mConfigurationChangeNotifier:Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;

    invoke-virtual {v7, p1}, Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;->notifyObserver(Landroid/content/res/Configuration;)V

    .line 1719
    :cond_7
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v7, :cond_8

    .line 1720
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    iget v8, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_c

    :goto_3
    invoke-virtual {v7, v5}, Lcom/infraware/office/actionbar/MainActionBar;->changeToolbarHeight(Z)V

    .line 1722
    :cond_8
    return-void

    .end local v2    # "portrait":Z
    :cond_9
    move v0, v6

    .line 1644
    goto :goto_0

    .restart local v0    # "isFindMode":Z
    :cond_a
    move v1, v6

    .line 1645
    goto :goto_1

    .end local v0    # "isFindMode":Z
    :cond_b
    move v2, v6

    .line 1691
    goto :goto_2

    .restart local v2    # "portrait":Z
    :cond_c
    move v5, v6

    .line 1720
    goto :goto_3
.end method

.method protected onContentSearchStart()V
    .locals 0

    .prologue
    .line 4560
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 1271
    invoke-static {}, Lcom/infraware/polarisoffice5/MyApplication;->getInstance()Lcom/infraware/polarisoffice5/MyApplication;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/infraware/polarisoffice5/MyApplication;->setCurrentViewer(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    .line 1272
    invoke-static {}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getInstance()Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->isNormalWindow(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_b

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mIsMultiWindow:Z

    .line 1274
    const-string/jumbo v0, "window"

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v10

    .line 1276
    .local v10, "display":Landroid/view/Display;
    const-string/jumbo v0, "EvBaseViewerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "getDefaultDisplay mWidth = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v10}, Landroid/view/Display;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "mHeight = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v10}, Landroid/view/Display;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1277
    const-string/jumbo v0, "EvBaseViewerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "PolarisOffcie Version = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f070322

    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1280
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "INTCMD"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mInternalCmdType:I

    .line 1281
    const/4 v5, 0x0

    .line 1282
    .local v5, "openType":I
    const/4 v6, -0x1

    .line 1283
    .local v6, "aNewTemplatePPT":I
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v12

    .line 1284
    .local v12, "uri":Landroid/net/Uri;
    if-eqz v12, :cond_0

    .line 1285
    invoke-virtual {v12}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1290
    :cond_0
    new-instance v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mClipBoard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    .line 1292
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "key_auto_save_test"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mAutoSaveTest:I

    .line 1294
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getOpenFilePath(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    .line 1295
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrRequestThumbnailPath:Ljava/lang/String;

    .line 1298
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1299
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->getDocFileExtentionType(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocExtensionType:I

    .line 1301
    :cond_1
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocExtensionType:I

    sparse-switch v0, :sswitch_data_0

    .line 1328
    :goto_1
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mInternalCmdType:I

    if-eqz v0, :cond_3

    .line 1329
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 1330
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "key_ppt_template"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 1332
    :cond_2
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mInternalCmdType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_c

    .line 1333
    const/4 v5, 0x1

    .line 1341
    :cond_3
    :goto_2
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v11

    .line 1342
    .local v11, "path":Ljava/lang/String;
    const-string/jumbo v0, "EvBaseViewerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "ExternalStorage Path = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1344
    const v0, 0x7f0b0119

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseView;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    .line 1347
    invoke-static {}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getInstance()Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->isMultiWindowSupported(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1348
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mOnDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 1349
    new-instance v0, Lcom/infraware/common/multiwindow/MWDnDOperator;

    invoke-direct {v0, p0}, Lcom/infraware/common/multiwindow/MWDnDOperator;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMWDnDOperator:Lcom/infraware/common/multiwindow/MWDnDOperator;

    .line 1352
    :cond_4
    invoke-static {}, Lcom/infraware/common/util/Utils;->isKKOver()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1353
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onCreateOnKKOver()V

    .line 1361
    :cond_5
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_6

    .line 1362
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mInternalCmdType:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_d

    .line 1365
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IInitInterfaceHandleAddress()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseView;->setInterfaceHandleAddress(I)V

    .line 1366
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseView;->setIsinit(Z)V

    .line 1375
    :cond_6
    :goto_3
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->makeOfficeDir()V

    .line 1376
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    iget v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocExtensionType:I

    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mTempPath:Ljava/lang/String;

    iget-object v8, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mBookMarkPath:Ljava/lang/String;

    move-object v1, p0

    invoke-virtual/range {v0 .. v8}, Lcom/infraware/office/baseframe/EvBaseView;->setInit(Landroid/app/Activity;ILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;)V

    .line 1378
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->checkOrigianlKeep(Ljava/lang/String;)V

    .line 1380
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setEvListener()V

    .line 1382
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mInternalCmdType:I

    invoke-direct {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->renameTemplateFile(I)V

    .line 1387
    new-instance v0, Lcom/infraware/office/actionbar/MainActionBar;

    iget v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocExtensionType:I

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2}, Lcom/infraware/office/actionbar/MainActionBar;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    .line 1388
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-static {v1}, Lcom/infraware/common/util/FileUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/MainActionBar;->setTitle(Ljava/lang/String;)V

    .line 1389
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->show()V

    .line 1391
    new-instance v0, Lcom/infraware/office/actionbar/ActionTitleBar;

    const v1, 0x7f0b001d

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/infraware/office/actionbar/ActionTitleBar;-><init>(Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    .line 1394
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->hide()V

    .line 1397
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mInternalCmdType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_7

    .line 1398
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->setOnTotalLoadComplete()V

    .line 1402
    :cond_7
    invoke-super {p0, p1}, Lcom/infraware/common/baseactivity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 1404
    const-string/jumbo v0, "EvBaseViewerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Oncreate docExtension = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocExtensionType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1405
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->showDialog(I)V

    .line 1407
    new-instance v0, Lcom/infraware/common/event/SdCardEvent;

    invoke-direct {v0}, Lcom/infraware/common/event/SdCardEvent;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    .line 1408
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    invoke-virtual {v0, p0}, Lcom/infraware/common/event/SdCardEvent;->setSdListener(Lcom/infraware/common/event/SdCardListener;)V

    .line 1409
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getSdcardIntentFilter()Landroid/content/IntentFilter;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1411
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$CloseActionReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$CloseActionReceiver;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Lcom/infraware/office/baseframe/EvBaseViewerActivity$1;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mCloseReceiver:Lcom/infraware/office/baseframe/EvBaseViewerActivity$CloseActionReceiver;

    .line 1412
    new-instance v9, Landroid/content/IntentFilter;

    invoke-direct {v9}, Landroid/content/IntentFilter;-><init>()V

    .line 1413
    .local v9, "closeFilter":Landroid/content/IntentFilter;
    const-string/jumbo v0, "com.infraware.polarisviewer5.DEACTIVE"

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1414
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mCloseReceiver:Lcom/infraware/office/baseframe/EvBaseViewerActivity$CloseActionReceiver;

    invoke-virtual {p0, v0, v9}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1417
    new-instance v0, Lcom/infraware/polarisoffice5/common/QuickScrollView;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/QuickScrollView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mQuickScroll:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    .line 1418
    new-instance v0, Lcom/infraware/office/baseframe/porting/EvPowerSaveOption;

    invoke-direct {v0}, Lcom/infraware/office/baseframe/porting/EvPowerSaveOption;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPowerSaveOption:Lcom/infraware/office/baseframe/porting/EvPowerSaveOption;

    .line 1420
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-nez v0, :cond_8

    .line 1421
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->finish()V

    .line 1424
    :cond_8
    invoke-static {p0}, Lcom/infraware/common/define/CMModelDefine$Diotek;->USE_DICTIONARY_SEARCH(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-nez v0, :cond_9

    .line 1425
    new-instance v0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    .line 1428
    :cond_9
    const v0, 0x7f0b0025

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainLinearLayout:Landroid/view/View;

    .line 1429
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainLinearLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mOnLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1433
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "search-key"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mContentSearchWord:Ljava/lang/String;

    .line 1434
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mContentSearchWord:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 1435
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mIsContentSearch:Z

    .line 1438
    :cond_a
    iput-object p0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    .line 1439
    invoke-static {}, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->getInsTance()Lcom/infraware/polarisoffice5/print/OfficePrintManager;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mOfficePrintManager:Lcom/infraware/polarisoffice5/print/OfficePrintManager;

    .line 1440
    return-void

    .line 1272
    .end local v5    # "openType":I
    .end local v6    # "aNewTemplatePPT":I
    .end local v9    # "closeFilter":Landroid/content/IntentFilter;
    .end local v10    # "display":Landroid/view/Display;
    .end local v11    # "path":Ljava/lang/String;
    .end local v12    # "uri":Landroid/net/Uri;
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1304
    .restart local v5    # "openType":I
    .restart local v6    # "aNewTemplatePPT":I
    .restart local v10    # "display":Landroid/view/Display;
    .restart local v12    # "uri":Landroid/net/Uri;
    :sswitch_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    goto/16 :goto_1

    .line 1308
    :sswitch_1
    const/4 v0, 0x3

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    goto/16 :goto_1

    .line 1312
    :sswitch_2
    const/4 v0, 0x2

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    goto/16 :goto_1

    .line 1315
    :sswitch_3
    const/16 v0, 0x26

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    goto/16 :goto_1

    .line 1318
    :sswitch_4
    const/4 v0, 0x5

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    goto/16 :goto_1

    .line 1321
    :sswitch_5
    const/4 v0, 0x6

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    goto/16 :goto_1

    .line 1324
    :sswitch_6
    const/16 v0, 0x25

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    goto/16 :goto_1

    .line 1334
    :cond_c
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mInternalCmdType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 1335
    const/4 v5, 0x2

    .line 1336
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_3

    .line 1337
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->getDocFileExtentionType(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocExtensionType:I

    goto/16 :goto_2

    .line 1367
    .restart local v11    # "path":Ljava/lang/String;
    :cond_d
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mInternalCmdType:I

    if-ltz v0, :cond_6

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->getNativeInterfaceHandle()I

    move-result v0

    if-nez v0, :cond_6

    .line 1371
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IInitInterfaceHandleAddress()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->setNativeInterfaceHandle(I)V

    .line 1372
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseView;->setIsinit(Z)V

    goto/16 :goto_3

    .line 1301
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_0
        0x3 -> :sswitch_5
        0x5 -> :sswitch_2
        0x6 -> :sswitch_4
        0x12 -> :sswitch_0
        0x13 -> :sswitch_1
        0x14 -> :sswitch_2
        0x25 -> :sswitch_6
        0x26 -> :sswitch_3
    .end sparse-switch
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 12
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;

    .prologue
    .line 3640
    const-string/jumbo v7, "EvBaseViewerActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "onCreateDialog id = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3641
    sparse-switch p1, :sswitch_data_0

    .line 3962
    invoke-super {p0, p1, p2}, Lcom/infraware/common/baseactivity/BaseActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 3643
    :sswitch_0
    invoke-static {}, Lcom/infraware/polarisoffice5/common/DlgFactory;->getInstance()Lcom/infraware/polarisoffice5/common/DlgFactory;

    move-result-object v7

    invoke-virtual {v7, p0}, Lcom/infraware/polarisoffice5/common/DlgFactory;->createProtectWritePasswordDlg(Landroid/app/Activity;)Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 3645
    :sswitch_1
    invoke-static {}, Lcom/infraware/polarisoffice5/common/DlgFactory;->getInstance()Lcom/infraware/polarisoffice5/common/DlgFactory;

    move-result-object v7

    invoke-virtual {v7, p0}, Lcom/infraware/polarisoffice5/common/DlgFactory;->createProtectReadPasswordDlg(Landroid/app/Activity;)Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 3647
    :sswitch_2
    invoke-static {}, Lcom/infraware/polarisoffice5/common/DlgFactory;->getInstance()Lcom/infraware/polarisoffice5/common/DlgFactory;

    move-result-object v7

    iget-object v8, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-static {v8}, Lcom/infraware/common/util/FileUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, p0, v8}, Lcom/infraware/polarisoffice5/common/DlgFactory;->createOpenProgressDlg(Landroid/app/Activity;Ljava/lang/String;)Lcom/infraware/polarisoffice5/common/DlgFactory$OpenProgressDlg;

    move-result-object v0

    goto :goto_0

    .line 3651
    :sswitch_3
    invoke-static {}, Lcom/infraware/polarisoffice5/common/DlgFactory;->getInstance()Lcom/infraware/polarisoffice5/common/DlgFactory;

    move-result-object v7

    invoke-virtual {v7, p0}, Lcom/infraware/polarisoffice5/common/DlgFactory;->createSearchProgressDlg(Landroid/app/Activity;)Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg;

    move-result-object v0

    goto :goto_0

    .line 3655
    :sswitch_4
    const-string/jumbo v7, "layout_inflater"

    invoke-virtual {p0, v7}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    .line 3656
    .local v6, "vi":Landroid/view/LayoutInflater;
    const v7, 0x7f030041

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 3657
    .local v5, "v":Landroid/view/View;
    const v7, 0x7f0b01d5

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintProgressText:Landroid/widget/TextView;

    .line 3658
    const v7, 0x102000d

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ProgressBar;

    iput-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mProgress:Landroid/widget/ProgressBar;

    .line 3659
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mProgress:Landroid/widget/ProgressBar;

    iget v8, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPageCount:I

    invoke-virtual {v7, v8}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 3660
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mProgress:Landroid/widget/ProgressBar;

    iget v8, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintCurPage:I

    invoke-virtual {v7, v8}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 3661
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintProgressText:Landroid/widget/TextView;

    const-string/jumbo v8, "%s (%d/%d)"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const v11, 0x7f0701c3

    invoke-virtual {p0, v11}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    iget v11, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintCurPage:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    iget v11, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPageCount:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3662
    new-instance v7, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v8

    invoke-direct {v7, p0, v8}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0701ad

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    const v8, 0x7f07005f

    invoke-virtual {p0, v8}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    new-instance v9, Lcom/infraware/office/baseframe/EvBaseViewerActivity$34;

    invoke-direct {v9, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$34;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    new-instance v8, Lcom/infraware/office/baseframe/EvBaseViewerActivity$33;

    invoke-direct {v8, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$33;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    iput-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintProgressPopup:Landroid/app/AlertDialog;

    .line 3690
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintProgressPopup:Landroid/app/AlertDialog;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 3691
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintProgressPopup:Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 3694
    .end local v5    # "v":Landroid/view/View;
    .end local v6    # "vi":Landroid/view/LayoutInflater;
    :sswitch_5
    const-string/jumbo v7, "layout_inflater"

    invoke-virtual {p0, v7}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    .line 3695
    .restart local v6    # "vi":Landroid/view/LayoutInflater;
    const v7, 0x7f030041

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 3696
    .restart local v5    # "v":Landroid/view/View;
    const v7, 0x7f0b01d5

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 3697
    .local v2, "msg":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0701c2

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3700
    new-instance v7, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v8

    invoke-direct {v7, p0, v8}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0701ad

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    iput-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPrevDlg:Landroid/app/AlertDialog;

    .line 3703
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPrevDlg:Landroid/app/AlertDialog;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 3704
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPrevDlg:Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 3707
    .end local v2    # "msg":Landroid/widget/TextView;
    .end local v5    # "v":Landroid/view/View;
    .end local v6    # "vi":Landroid/view/LayoutInflater;
    :sswitch_6
    new-instance v7, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v8

    invoke-direct {v7, p0, v8}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0701ad

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    const v8, 0x7f0701c1

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    const v8, 0x7f070063

    invoke-virtual {p0, v8}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    new-instance v9, Lcom/infraware/office/baseframe/EvBaseViewerActivity$35;

    invoke-direct {v9, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$35;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 3716
    .local v0, "builder":Landroid/app/AlertDialog;
    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    goto/16 :goto_0

    .line 3721
    .end local v0    # "builder":Landroid/app/AlertDialog;
    :sswitch_7
    new-instance v7, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v8

    invoke-direct {v7, p0, v8}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0701f1

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    const v8, 0x7f0701e8

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    const v8, 0x7f070063

    invoke-virtual {p0, v8}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    new-instance v9, Lcom/infraware/office/baseframe/EvBaseViewerActivity$36;

    invoke-direct {v9, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$36;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 3730
    .restart local v0    # "builder":Landroid/app/AlertDialog;
    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    goto/16 :goto_0

    .line 3734
    .end local v0    # "builder":Landroid/app/AlertDialog;
    :sswitch_8
    const-string/jumbo v7, "layout_inflater"

    invoke-virtual {p0, v7}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    .line 3735
    .restart local v6    # "vi":Landroid/view/LayoutInflater;
    const v7, 0x7f030040

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 3737
    .restart local v5    # "v":Landroid/view/View;
    const v7, 0x7f0b01cf

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioButton;

    iput-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintAll:Landroid/widget/RadioButton;

    .line 3738
    const v7, 0x7f0b01d0

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioButton;

    iput-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintCurrent:Landroid/widget/RadioButton;

    .line 3739
    const v7, 0x7f0b01d2

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioButton;

    iput-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPage:Landroid/widget/RadioButton;

    .line 3740
    const v7, 0x7f0b01d1

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    iput-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEditLayout:Landroid/widget/LinearLayout;

    .line 3741
    const v7, 0x7f0b01d3

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/EditText;

    iput-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintStartEdit:Landroid/widget/EditText;

    .line 3742
    const v7, 0x7f0b01d4

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/EditText;

    iput-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEndEdit:Landroid/widget/EditText;

    .line 3744
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintStartEdit:Landroid/widget/EditText;

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setRawInputType(I)V

    .line 3745
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintStartEdit:Landroid/widget/EditText;

    iget-object v8, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v8}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v8

    iget v8, v8, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 3746
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintStartEdit:Landroid/widget/EditText;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 3747
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintStartEdit:Landroid/widget/EditText;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 3748
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintStartEdit:Landroid/widget/EditText;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 3750
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEndEdit:Landroid/widget/EditText;

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setRawInputType(I)V

    .line 3751
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEndEdit:Landroid/widget/EditText;

    iget-object v8, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v8}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v8

    iget v8, v8, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 3752
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEndEdit:Landroid/widget/EditText;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 3753
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEndEdit:Landroid/widget/EditText;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 3754
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEndEdit:Landroid/widget/EditText;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 3756
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v7}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v7

    iget v7, v7, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nTotalPages:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v3, v7, 0x1

    .line 3757
    .local v3, "nLength":I
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v7}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v7

    iget v4, v7, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nTotalPages:I

    .line 3758
    .local v4, "nMaxValue":I
    new-instance v1, Lcom/infraware/office/util/InputValueFilter;

    invoke-direct {v1, p0, v3, v4}, Lcom/infraware/office/util/InputValueFilter;-><init>(Landroid/content/Context;II)V

    .line 3759
    .local v1, "filter":Lcom/infraware/office/util/InputValueFilter;
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintStartEdit:Landroid/widget/EditText;

    invoke-virtual {v1}, Lcom/infraware/office/util/InputValueFilter;->getFilters()[Landroid/text/InputFilter;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 3760
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEndEdit:Landroid/widget/EditText;

    invoke-virtual {v1}, Lcom/infraware/office/util/InputValueFilter;->getFilters()[Landroid/text/InputFilter;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 3761
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintStartEdit:Landroid/widget/EditText;

    iget-object v8, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEditFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 3762
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEndEdit:Landroid/widget/EditText;

    iget-object v8, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEditFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 3764
    iget v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocExtensionType:I

    sparse-switch v7, :sswitch_data_1

    .line 3784
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintCurrent:Landroid/widget/RadioButton;

    const v8, 0x7f0701b6

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setText(I)V

    .line 3785
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPage:Landroid/widget/RadioButton;

    const v8, 0x7f0701b9

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setText(I)V

    .line 3786
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEditLayout:Landroid/widget/LinearLayout;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3789
    :goto_1
    iget v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocExtensionType:I

    const/4 v8, 0x5

    if-eq v7, v8, :cond_0

    iget v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocExtensionType:I

    const/16 v8, 0x14

    if-eq v7, v8, :cond_0

    iget v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocExtensionType:I

    const/16 v8, 0x26

    if-eq v7, v8, :cond_0

    iget v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocExtensionType:I

    const/4 v8, 0x1

    if-eq v7, v8, :cond_0

    iget v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocExtensionType:I

    const/16 v8, 0x13

    if-eq v7, v8, :cond_0

    .line 3797
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintAll:Landroid/widget/RadioButton;

    const v8, 0x7f0701b2

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setText(I)V

    .line 3803
    :cond_0
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintAll:Landroid/widget/RadioButton;

    new-instance v8, Lcom/infraware/office/baseframe/EvBaseViewerActivity$37;

    invoke-direct {v8, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$37;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3818
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintCurrent:Landroid/widget/RadioButton;

    new-instance v8, Lcom/infraware/office/baseframe/EvBaseViewerActivity$38;

    invoke-direct {v8, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$38;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3833
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPage:Landroid/widget/RadioButton;

    new-instance v8, Lcom/infraware/office/baseframe/EvBaseViewerActivity$39;

    invoke-direct {v8, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$39;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3851
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintStartEdit:Landroid/widget/EditText;

    new-instance v8, Lcom/infraware/office/baseframe/EvBaseViewerActivity$40;

    invoke-direct {v8, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$40;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 3879
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEndEdit:Landroid/widget/EditText;

    new-instance v8, Lcom/infraware/office/baseframe/EvBaseViewerActivity$41;

    invoke-direct {v8, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$41;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 3906
    new-instance v7, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v8

    invoke-direct {v7, p0, v8}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v8, 0x7f0701be

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    const v8, 0x7f070063

    new-instance v9, Lcom/infraware/office/baseframe/EvBaseViewerActivity$43;

    invoke-direct {v9, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$43;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    const v8, 0x7f07005f

    new-instance v9, Lcom/infraware/office/baseframe/EvBaseViewerActivity$42;

    invoke-direct {v9, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$42;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    iput-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPagePopup:Landroid/app/AlertDialog;

    .line 3932
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintAll:Landroid/widget/RadioButton;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 3933
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintCurrent:Landroid/widget/RadioButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 3934
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPage:Landroid/widget/RadioButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 3935
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPagePopup:Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 3769
    :sswitch_9
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintAll:Landroid/widget/RadioButton;

    const v8, 0x7f0701b4

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setText(I)V

    .line 3770
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintCurrent:Landroid/widget/RadioButton;

    const v8, 0x7f0701b8

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setText(I)V

    .line 3771
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPage:Landroid/widget/RadioButton;

    const v8, 0x7f0701b9

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setText(I)V

    .line 3772
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEditLayout:Landroid/widget/LinearLayout;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 3777
    :sswitch_a
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintAll:Landroid/widget/RadioButton;

    const v8, 0x7f0701b3

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setText(I)V

    .line 3778
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintCurrent:Landroid/widget/RadioButton;

    const v8, 0x7f0701b7

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setText(I)V

    .line 3779
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEditLayout:Landroid/widget/LinearLayout;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 3939
    .end local v1    # "filter":Lcom/infraware/office/util/InputValueFilter;
    .end local v3    # "nLength":I
    .end local v4    # "nMaxValue":I
    .end local v5    # "v":Landroid/view/View;
    .end local v6    # "vi":Landroid/view/LayoutInflater;
    :sswitch_b
    new-instance v7, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v8

    invoke-direct {v7, p0, v8}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const/high16 v8, 0x7f070000

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    const v8, 0x7f070263

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f070063

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/infraware/office/baseframe/EvBaseViewerActivity$45;

    invoke-direct {v9, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$45;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    new-instance v8, Lcom/infraware/office/baseframe/EvBaseViewerActivity$44;

    invoke-direct {v8, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$44;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    iput-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDlgMsg:Landroid/app/AlertDialog;

    .line 3958
    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDlgMsg:Landroid/app/AlertDialog;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 3959
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDlgMsg:Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 3641
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_3
        0x1a -> :sswitch_4
        0x1b -> :sswitch_5
        0x1c -> :sswitch_6
        0x1e -> :sswitch_8
        0x20 -> :sswitch_7
        0x27 -> :sswitch_0
        0x28 -> :sswitch_1
        0x34 -> :sswitch_b
    .end sparse-switch

    .line 3764
    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_9
        0x5 -> :sswitch_a
        0x13 -> :sswitch_9
        0x14 -> :sswitch_a
        0x26 -> :sswitch_a
    .end sparse-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 2812
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2629
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainLinearLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mOnLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 2630
    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainLinearLayout:Landroid/view/View;

    .line 2632
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onDestroy()V

    .line 2633
    const-string/jumbo v0, "EvBaseViewerActivity"

    const-string/jumbo v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2634
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->dismissPopupWindow()Z

    .line 2635
    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPowerSaveOption:Lcom/infraware/office/baseframe/porting/EvPowerSaveOption;

    .line 2637
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mQuickScroll:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    if-eqz v0, :cond_0

    .line 2638
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mQuickScroll:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->QuickScrollFinalize()V

    .line 2639
    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mQuickScroll:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    .line 2642
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v0, :cond_1

    .line 2643
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->MainActionBarFinalize()V

    .line 2644
    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    .line 2647
    :cond_1
    invoke-static {}, Lcom/infraware/polarisoffice5/common/DlgFactory;->getInstance()Lcom/infraware/polarisoffice5/common/DlgFactory;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, p0, v1}, Lcom/infraware/polarisoffice5/common/DlgFactory;->dismisProgress(Landroid/app/Activity;I)V

    .line 2650
    invoke-static {p0}, Lcom/infraware/common/define/CMModelDefine$Diotek;->USE_DICTIONARY_SEARCH(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v0, :cond_2

    .line 2651
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->stopDictionaryService()V

    .line 2652
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$Diotek;->initialize()V

    .line 2655
    :cond_2
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbFinishCalled:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_4

    .line 2656
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IClose()V

    .line 2658
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->DeleteOpenedFileList(Ljava/lang/String;)Z

    .line 2660
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->getInterfaceHandleAddress()I

    move-result v0

    if-eqz v0, :cond_3

    .line 2661
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->getInterfaceHandleAddress()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->deleteInterfaceHandle(I)V

    .line 2663
    :cond_3
    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 2667
    :cond_4
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->isFinishing()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    .line 2668
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2669
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mCloseReceiver:Lcom/infraware/office/baseframe/EvBaseViewerActivity$CloseActionReceiver;

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2670
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->destory()V

    .line 2673
    :cond_5
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mClipBoard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    if-eqz v0, :cond_6

    .line 2674
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mClipBoard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->thisFinalize()V

    .line 2675
    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mClipBoard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    .line 2679
    :cond_6
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbFinishCalled:Z

    if-nez v0, :cond_7

    .line 2680
    invoke-static {}, Lcom/infraware/polarisoffice5/MyApplication;->getInstance()Lcom/infraware/polarisoffice5/MyApplication;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/MyApplication;->setCurrentViewer(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    .line 2681
    :cond_7
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 2682
    return-void
.end method

.method public onEmailSendDialog()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 3557
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v3}, Lcom/infraware/office/actionbar/MainActionBar;->getTotalLoadState()Z

    move-result v3

    if-nez v3, :cond_1

    .line 3558
    const/4 v2, 0x0

    .line 3559
    .local v2, "sendEmailFinish":Z
    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v4

    invoke-virtual {p0, v3, v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onSendEMail(ZI)Z

    move-result v2

    .line 3561
    if-nez v2, :cond_0

    .line 3562
    const/16 v3, 0xb

    invoke-virtual {p0, v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->showDialog(I)V

    .line 3615
    .end local v2    # "sendEmailFinish":Z
    :cond_0
    :goto_0
    return-void

    .line 3567
    :cond_1
    const/16 v3, 0x32

    iput v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPopupType:I

    .line 3569
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v3

    invoke-direct {v0, p0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 3570
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f070309

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 3572
    const/4 v1, 0x0

    .line 3574
    .local v1, "itemArrayResID":I
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getIsEncryptDoc()Z

    move-result v3

    if-eq v3, v5, :cond_2

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocExtensionType()I

    move-result v3

    const/16 v4, 0x25

    if-eq v3, v4, :cond_2

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v3

    iget v3, v3, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nTotalPages:I

    const/16 v4, 0x3e8

    if-le v3, v4, :cond_3

    .line 3577
    :cond_2
    const v1, 0x7f040002

    .line 3583
    :goto_1
    new-instance v3, Lcom/infraware/office/baseframe/EvBaseViewerActivity$31;

    invoke-direct {v3, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$31;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 3612
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMsgPopup:Landroid/app/AlertDialog;

    .line 3613
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMsgPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3, v5}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 3614
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMsgPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 3580
    :cond_3
    const v1, 0x7f040001

    goto :goto_1
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 16
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 5435
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v15

    .line 5436
    .local v15, "nMetaState":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 5480
    :goto_0
    invoke-super/range {p0 .. p1}, Lcom/infraware/common/baseactivity/BaseActivity;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    return v1

    .line 5439
    :pswitch_0
    move-object/from16 v0, p0

    instance-of v14, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    .line 5440
    .local v14, "isPDFViewerActivity":Z
    const/16 v1, 0x3000

    if-ne v15, v1, :cond_1

    const/4 v12, 0x1

    .line 5441
    .local v12, "isCtrlLeftOn":Z
    :goto_1
    const/16 v1, 0x5000

    if-ne v15, v1, :cond_2

    const/4 v13, 0x1

    .line 5443
    .local v13, "isCtrlRightOn":Z
    :goto_2
    if-nez v12, :cond_0

    if-eqz v13, :cond_4

    .line 5444
    :cond_0
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    .line 5445
    invoke-direct/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mouseWheelZoomUp()V

    goto :goto_0

    .line 5440
    .end local v12    # "isCtrlLeftOn":Z
    .end local v13    # "isCtrlRightOn":Z
    :cond_1
    const/4 v12, 0x0

    goto :goto_1

    .line 5441
    .restart local v12    # "isCtrlLeftOn":Z
    :cond_2
    const/4 v13, 0x0

    goto :goto_2

    .line 5448
    .restart local v13    # "isCtrlRightOn":Z
    :cond_3
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v1

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gez v1, :cond_4

    .line 5449
    invoke-direct/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mouseWheelZoomDown()V

    goto :goto_0

    .line 5454
    :cond_4
    if-nez v14, :cond_6

    .line 5455
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v1

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gez v1, :cond_5

    .line 5457
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v15}, Lcom/infraware/office/evengine/EvInterface;->ICaretMove(II)V

    goto :goto_0

    .line 5462
    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v15}, Lcom/infraware/office/evengine/EvInterface;->ICaretMove(II)V

    goto :goto_0

    .line 5467
    :cond_6
    const/high16 v1, 0x42200000    # 40.0f

    invoke-static/range {p0 .. p0}, Lcom/infraware/common/util/Utils;->getDensityDpi(Landroid/app/Activity;)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x43700000    # 240.0f

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 5468
    .local v5, "nOffset":I
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v1

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gez v1, :cond_7

    .line 5470
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x6

    const/16 v3, 0x28

    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IScroll(IIIII)V

    goto/16 :goto_0

    .line 5475
    :cond_7
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v7, 0x6

    const/16 v8, 0x28

    const/4 v9, 0x0

    neg-int v10, v5

    const/4 v11, 0x0

    invoke-virtual/range {v6 .. v11}, Lcom/infraware/office/evengine/EvInterface;->IScroll(IIIII)V

    goto/16 :goto_0

    .line 5436
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method public onHelp()V
    .locals 11

    .prologue
    const/4 v6, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 2934
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    const/16 v4, 0x130

    invoke-static {v3, v4}, Lcom/infraware/common/define/CMModelDefine;->isShowConnetNetWorkPopup(Landroid/content/Context;I)Z

    move-result v3

    if-ne v3, v8, :cond_2

    .line 2937
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 2938
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030006

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2940
    .local v1, "networkView":Landroid/view/View;
    const v3, 0x7f0b003b

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 2941
    .local v2, "tv":Landroid/widget/TextView;
    const v3, 0x7f0b003c

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mCheckBox:Landroid/widget/CheckBox;

    .line 2942
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/infraware/common/util/Utils;->getIsConnectWlan(Landroid/app/Activity;)Z

    move-result v3

    if-ne v3, v8, :cond_0

    .line 2943
    const v3, 0x7f07005d

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 2944
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_WLAN_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onNetWorkUseAlarmClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v7, v6, v8

    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v7, v6, v10

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    .line 2961
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    .end local v1    # "networkView":Landroid/view/View;
    .end local v2    # "tv":Landroid/widget/TextView;
    :goto_0
    return-void

    .line 2949
    .restart local v0    # "inflater":Landroid/view/LayoutInflater;
    .restart local v1    # "networkView":Landroid/view/View;
    .restart local v2    # "tv":Landroid/widget/TextView;
    :cond_0
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/infraware/common/util/Utils;->getIsConnectMobileNetwork(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2950
    const v3, 0x7f07005b

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 2951
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_MOBILE_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onNetWorkUseAlarmClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v7, v6, v8

    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v7, v6, v10

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto :goto_0

    .line 2955
    :cond_1
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_NETWORK_NOT_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto :goto_0

    .line 2959
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    .end local v1    # "networkView":Landroid/view/View;
    .end local v2    # "tv":Landroid/widget/TextView;
    :cond_2
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->connectHelpPage()V

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 947
    packed-switch p1, :pswitch_data_0

    .line 957
    invoke-super {p0, p1, p2}, Lcom/infraware/common/baseactivity/BaseActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 949
    :pswitch_0
    iget v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->getActionMode()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 950
    const/4 v0, 0x0

    goto :goto_0

    .line 951
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->InitFindBar()V

    .line 952
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v1, :cond_1

    .line 953
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v1}, Lcom/infraware/office/actionbar/MainActionBar;->hide()V

    .line 954
    :cond_1
    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->FindBarShow(Z)V

    goto :goto_0

    .line 947
    :pswitch_data_0
    .packed-switch 0x54
        :pswitch_0
    .end packed-switch
.end method

.method public onLocaleChange(I)V
    .locals 6
    .param p1, "nLocale"    # I

    .prologue
    const v5, 0x7f0701d6

    const/16 v4, 0xcc

    .line 4392
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4393
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->onLocaleChanged()V

    .line 4396
    :cond_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v1, :cond_1

    .line 4397
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v1}, Lcom/infraware/office/actionbar/MainActionBar;->onLocaleChanged()V

    .line 4399
    :cond_1
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;

    if-eqz v1, :cond_2

    .line 4400
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 4401
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->invalidate()V

    .line 4402
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;

    invoke-static {v4, v4, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHintTextColor(I)V

    .line 4405
    :cond_2
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_DstTxt:Landroid/widget/EditText;

    if-eqz v1, :cond_3

    .line 4406
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_DstTxt:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0701d7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 4407
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_DstTxt:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->invalidate()V

    .line 4408
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_DstTxt:Landroid/widget/EditText;

    invoke-static {v4, v4, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHintTextColor(I)V

    .line 4411
    :cond_3
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    if-eqz v1, :cond_4

    .line 4413
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 4414
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindText:Landroid/widget/EditText;

    invoke-static {v4, v4, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHintTextColor(I)V

    .line 4417
    :cond_4
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mZoomMenu:Landroid/app/AlertDialog;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mZoomMenu:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 4418
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mZoomMenu:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 4419
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onZoomMenu(Z)V

    .line 4422
    :cond_5
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v1, :cond_7

    .line 4423
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isShown()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    .line 4424
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->hidePanel()V

    .line 4425
    :cond_6
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->onLocaleChange()V

    .line 4428
    :cond_7
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFreeDrawBar:Landroid/view/View;

    if-eqz v1, :cond_8

    .line 4431
    :cond_8
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v1

    if-eqz v1, :cond_9

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v1

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v1, :cond_9

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v1

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    if-eqz v1, :cond_9

    .line 4435
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v1

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 4437
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v1

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v1, v1, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismissAll()V

    .line 4442
    :cond_9
    invoke-static {}, Lcom/infraware/common/util/Utils;->isICS()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-static {p0}, Lcom/infraware/polarisoffice5/common/HoveringManager;->setHoveringButton(Landroid/app/Activity;)Z

    .line 4444
    :cond_a
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPagePopup:Landroid/app/AlertDialog;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPagePopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 4445
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPagePopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 4447
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPagePopup:Landroid/app/AlertDialog;

    .line 4448
    const/16 v1, 0x1e

    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->showDialog(I)V

    .line 4451
    :cond_b
    iget v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_nPopupType:I

    const/16 v2, 0x33

    if-ne v1, v2, :cond_c

    .line 4452
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMsgPopup:Landroid/app/AlertDialog;

    const v2, 0x7f0702c7

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 4454
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMsgPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 4456
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMsgPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    instance-of v1, v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchAdapter;

    if-eqz v1, :cond_c

    .line 4458
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMsgPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchAdapter;

    .line 4459
    .local v0, "adapter":Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchAdapter;
    if-eqz v0, :cond_c

    .line 4461
    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchAdapter;->notifyDataSetChanged()V

    .line 4466
    .end local v0    # "adapter":Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupSearchAdapter;
    :cond_c
    return-void
.end method

.method public onMenuOpened(ILandroid/view/Menu;)Z
    .locals 1
    .param p1, "featureId"    # I
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 3097
    invoke-super {p0, p1, p2}, Lcom/infraware/common/baseactivity/BaseActivity;->onMenuOpened(ILandroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 3118
    invoke-super {p0, p1}, Lcom/infraware/common/baseactivity/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 1572
    :try_start_0
    sget-object v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mErrorAndClosePopup:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mErrorAndClosePopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1573
    sget-object v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mErrorAndClosePopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 1574
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->isErrDismiss:Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1581
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->dismissPopupWindow()Z

    .line 1582
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v1}, Lcom/infraware/office/actionbar/MainActionBar;->dismissMenuPopup()Z

    .line 1583
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v1, :cond_1

    .line 1584
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->onPause()V

    .line 1586
    :cond_1
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onPause()V

    .line 1587
    return-void

    .line 1577
    :catch_0
    move-exception v0

    .line 1578
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const/4 v1, 0x0

    sput-object v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mErrorAndClosePopup:Landroid/app/AlertDialog;

    goto :goto_0
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 3194
    invoke-super {p0, p1, p2}, Lcom/infraware/common/baseactivity/BaseActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 3195
    sparse-switch p1, :sswitch_data_0

    .line 3201
    :goto_0
    return-void

    .line 3198
    :sswitch_0
    invoke-virtual {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->removeDialog(I)V

    goto :goto_0

    .line 3195
    nop

    :sswitch_data_0
    .sparse-switch
        0x1a -> :sswitch_0
        0x1e -> :sswitch_0
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 3084
    invoke-super {p0, p1}, Lcom/infraware/common/baseactivity/BaseActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onReflow()V
    .locals 2

    .prologue
    .line 3088
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->ISetViewMode(I)V

    .line 3089
    return-void
.end method

.method public onRequestSamsungAppsPolarisOffice()V
    .locals 13

    .prologue
    const/4 v8, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 3017
    iget-object v5, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    const-string/jumbo v6, "com.infraware.polarisoffice5"

    invoke-static {v5, v6}, Lcom/infraware/common/util/Utils;->isInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 3019
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 3020
    .local v1, "it":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string/jumbo v6, "key_filename"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3021
    .local v2, "key_filename":Ljava/lang/String;
    const-string/jumbo v5, "com.infraware.polarisoffice5"

    const-string/jumbo v6, "com.infraware.polarisoffice5.OfficeLauncherActivity"

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3022
    const-string/jumbo v5, "key_filename"

    invoke-virtual {v1, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3023
    const-string/jumbo v5, "previe"

    invoke-virtual {v1, v5, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3024
    const-string/jumbo v5, "sort_order"

    invoke-virtual {v1, v5, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3026
    const-string/jumbo v5, "android.intent.extra.STREAM"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "file://"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "/Sanity%20test.docx"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3027
    const-string/jumbo v5, "from-myfiles"

    invoke-virtual {v1, v5, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3028
    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->startActivity(Landroid/content/Intent;)V

    .line 3029
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->finish()V

    .line 3059
    .end local v1    # "it":Landroid/content/Intent;
    .end local v2    # "key_filename":Ljava/lang/String;
    :goto_0
    return-void

    .line 3033
    :cond_0
    iget-object v5, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    const/16 v6, 0x132

    invoke-static {v5, v6}, Lcom/infraware/common/define/CMModelDefine;->isShowConnetNetWorkPopup(Landroid/content/Context;I)Z

    move-result v5

    if-ne v5, v11, :cond_3

    .line 3035
    iget-object v5, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    const-string/jumbo v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 3036
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f030006

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 3038
    .local v3, "networkView":Landroid/view/View;
    const v5, 0x7f0b003b

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 3039
    .local v4, "tv":Landroid/widget/TextView;
    const v5, 0x7f0b003c

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    iput-object v5, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mCheckBox:Landroid/widget/CheckBox;

    .line 3040
    iget-object v5, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->getIsConnectWlan(Landroid/app/Activity;)Z

    move-result v5

    if-ne v5, v11, :cond_1

    .line 3041
    const v5, 0x7f07005d

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 3042
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    sget-object v7, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_WLAN_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v8, v8, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    aput-object v9, v8, v10

    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onSamsungAppsPolairsOfficeClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v9, v8, v11

    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v9, v8, v12

    invoke-virtual {v5, v6, v7, v8}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto :goto_0

    .line 3047
    :cond_1
    iget-object v5, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->getIsConnectMobileNetwork(Landroid/app/Activity;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 3048
    const v5, 0x7f07005b

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 3049
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    sget-object v7, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_MOBILE_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v8, v8, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    aput-object v9, v8, v10

    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onSamsungAppsPolairsOfficeClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v9, v8, v11

    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v9, v8, v12

    invoke-virtual {v5, v6, v7, v8}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 3053
    :cond_2
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    sget-object v7, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_NETWORK_NOT_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v8, v10, [Ljava/lang/Object;

    invoke-virtual {v5, v6, v7, v8}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 3057
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    .end local v3    # "networkView":Landroid/view/View;
    .end local v4    # "tv":Landroid/widget/TextView;
    :cond_3
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->connectUriSamungAppsPolarisOffice()V

    goto/16 :goto_0
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1512
    const-string/jumbo v1, "EvBaseViewerActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "OnResume mView mWidth = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/EvBaseView;->getWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "mHeight = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/EvBaseView;->getHeight()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1516
    :try_start_0
    sget-object v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mErrorAndClosePopup:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mLoadFailStrID:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->isErrDismiss:Z

    if-eqz v1, :cond_0

    .line 1518
    iget v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mLoadFailStrID:I

    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->AlertErrorAndCloseDlg(I)V

    .line 1519
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->isErrDismiss:Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1529
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v1, :cond_1

    .line 1531
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->GetInterfaceHandle()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/office/evengine/EvInterface;->SetInterfaceHandleAddress(I)V

    .line 1533
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setEvListener()V

    .line 1534
    invoke-virtual {p0, v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setSheetTitle(I)V

    .line 1535
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->dismissPageInfoTimer()V

    .line 1537
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onResume()V

    .line 1539
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->FindbarSoftInputManager()V

    .line 1541
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-static {v1}, Lcom/infraware/common/util/FileUtils;->isDirectory(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1542
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->showErrorDlg()V

    .line 1545
    :cond_2
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPagePopup:Landroid/app/AlertDialog;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPagePopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1546
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEndEdit:Landroid/widget/EditText;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEndEdit:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1547
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEndEdit:Landroid/widget/EditText;

    invoke-static {v1}, Lcom/infraware/office/util/EvUtil;->showIme(Landroid/view/View;)V

    .line 1548
    :cond_3
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintStartEdit:Landroid/widget/EditText;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintStartEdit:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1549
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintStartEdit:Landroid/widget/EditText;

    invoke-static {v1}, Lcom/infraware/office/util/EvUtil;->showIme(Landroid/view/View;)V

    .line 1551
    :cond_4
    return-void

    .line 1522
    :catch_0
    move-exception v0

    .line 1523
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const/4 v1, 0x0

    sput-object v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mErrorAndClosePopup:Landroid/app/AlertDialog;

    goto :goto_0
.end method

.method public onSendEMail(ZI)Z
    .locals 18
    .param p1, "bPdfExport"    # Z
    .param p2, "type"    # I

    .prologue
    .line 3364
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    if-nez v12, :cond_0

    .line 3365
    const/4 v12, 0x1

    .line 3485
    :goto_0
    return v12

    .line 3367
    :cond_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    sget-object v13, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    .line 3368
    .local v6, "nIndex":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    add-int/lit8 v13, v6, 0x1

    invoke-virtual {v12, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 3370
    .local v2, "fileName":Ljava/lang/String;
    if-eqz p1, :cond_2

    .line 3372
    move-object/from16 v0, p0

    instance-of v12, v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    if-eqz v12, :cond_1

    move-object/from16 v11, p0

    .line 3373
    check-cast v11, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    .line 3374
    .local v11, "wordEditorActivity":Lcom/infraware/polarisoffice5/word/WordEditorActivity;
    invoke-virtual {v11}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getReflowText()Z

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_1

    .line 3375
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    const v13, 0x7f070315

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mToast:Landroid/widget/Toast;

    .line 3376
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v12}, Landroid/widget/Toast;->show()V

    .line 3377
    const/4 v12, 0x1

    goto :goto_0

    .line 3381
    .end local v11    # "wordEditorActivity":Lcom/infraware/polarisoffice5/word/WordEditorActivity;
    :cond_1
    const/4 v12, 0x1

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbSendingEMail:Z

    .line 3383
    const/16 v12, 0x2e

    invoke-virtual {v2, v12}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v6

    .line 3384
    const/4 v12, 0x0

    invoke-virtual {v2, v12, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 3385
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, ".pdf"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3387
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->SEND_EMAIL_PATH:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->GetInterfaceHandle()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrEmailFolderPath:Ljava/lang/String;

    .line 3388
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrEmailFolderPath:Ljava/lang/String;

    invoke-static {v12}, Lcom/infraware/common/util/FileUtils;->makeDirectories(Ljava/lang/String;)Ljava/lang/String;

    .line 3389
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrEmailFolderPath:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrTempFilePathForEMail:Ljava/lang/String;

    .line 3391
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrTempFilePathForEMail:Ljava/lang/String;

    invoke-virtual {v12, v13}, Lcom/infraware/office/evengine/EvInterface;->ISaveDocument(Ljava/lang/String;)V

    .line 3393
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 3397
    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v12}, Lcom/infraware/office/evengine/EvInterface;->IDocumentModified_Editor()Z

    move-result v12

    if-nez v12, :cond_4

    const/4 v12, 0x7

    move/from16 v0, p2

    if-ne v0, v12, :cond_3

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v12}, Lcom/infraware/office/evengine/EvInterface;->IPDFUpdated()Z

    move-result v12

    const/4 v13, 0x1

    if-eq v12, v13, :cond_4

    :cond_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v12}, Lcom/infraware/office/baseframe/EvBaseView;->GetOpenType()I

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_5

    .line 3401
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbSendingEMail:Z

    if-nez v12, :cond_5

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrTempFilePathForEMail:Ljava/lang/String;

    if-nez v12, :cond_5

    .line 3403
    const/4 v12, 0x1

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbSendingEMail:Z

    .line 3405
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->SEND_EMAIL_PATH:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->GetInterfaceHandle()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrEmailFolderPath:Ljava/lang/String;

    .line 3406
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrEmailFolderPath:Ljava/lang/String;

    invoke-static {v12}, Lcom/infraware/common/util/FileUtils;->makeDirectories(Ljava/lang/String;)Ljava/lang/String;

    .line 3407
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrEmailFolderPath:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrTempFilePathForEMail:Ljava/lang/String;

    .line 3409
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrTempFilePathForEMail:Ljava/lang/String;

    invoke-virtual {v12, v13}, Lcom/infraware/office/evengine/EvInterface;->ITempSaveDocument(Ljava/lang/String;)V

    .line 3411
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 3416
    :cond_5
    const/4 v5, 0x0

    .line 3417
    .local v5, "mimeType":Ljava/lang/String;
    const/4 v3, -0x1

    .line 3418
    .local v3, "idx_exe":I
    const/4 v8, 0x0

    .line 3420
    .local v8, "temp":Ljava/lang/String;
    const/4 v1, 0x0

    .line 3422
    .local v1, "f":Ljava/io/File;
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbSendingEMail:Z

    const/4 v13, 0x1

    if-ne v12, v13, :cond_6

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrTempFilePathForEMail:Ljava/lang/String;

    if-eqz v12, :cond_6

    .line 3424
    new-instance v1, Ljava/io/File;

    .end local v1    # "f":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrTempFilePathForEMail:Ljava/lang/String;

    invoke-direct {v1, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3425
    .restart local v1    # "f":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrTempFilePathForEMail:Ljava/lang/String;

    const/16 v13, 0x2e

    invoke-virtual {v12, v13}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    .line 3426
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrTempFilePathForEMail:Ljava/lang/String;

    add-int/lit8 v13, v3, 0x1

    invoke-virtual {v12, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 3429
    :cond_6
    if-eqz v1, :cond_7

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_8

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    if-eqz v12, :cond_8

    .line 3431
    :cond_7
    new-instance v1, Ljava/io/File;

    .end local v1    # "f":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-direct {v1, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3432
    .restart local v1    # "f":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    const/16 v13, 0x2e

    invoke-virtual {v12, v13}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    .line 3433
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    add-int/lit8 v13, v3, 0x1

    invoke-virtual {v12, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 3436
    :cond_8
    if-eqz v1, :cond_9

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_a

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v12, v12, Lcom/infraware/office/baseframe/EvBaseView;->mFilePath:Ljava/lang/String;

    if-eqz v12, :cond_a

    .line 3438
    :cond_9
    new-instance v1, Ljava/io/File;

    .end local v1    # "f":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v12, v12, Lcom/infraware/office/baseframe/EvBaseView;->mFilePath:Ljava/lang/String;

    invoke-direct {v1, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3439
    .restart local v1    # "f":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v12, v12, Lcom/infraware/office/baseframe/EvBaseView;->mFilePath:Ljava/lang/String;

    const/16 v13, 0x2e

    invoke-virtual {v12, v13}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    .line 3440
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v12, v12, Lcom/infraware/office/baseframe/EvBaseView;->mFilePath:Ljava/lang/String;

    add-int/lit8 v13, v3, 0x1

    invoke-virtual {v12, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 3443
    :cond_a
    if-nez v1, :cond_b

    .line 3444
    const/4 v12, 0x1

    goto/16 :goto_0

    .line 3447
    :cond_b
    invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/infraware/common/util/FileUtils;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 3449
    if-nez v5, :cond_c

    invoke-static {}, Lcom/infraware/common/util/Utils;->isKKOver()Z

    move-result v12

    if-eqz v12, :cond_c

    .line 3450
    const-string/jumbo v12, "hwp"

    invoke-virtual {v8, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_c

    .line 3451
    const-string/jumbo v5, "application/x-hwp"

    .line 3454
    :cond_c
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v10

    .line 3456
    .local v10, "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    const/16 v13, 0x131

    invoke-static {v12, v13}, Lcom/infraware/common/define/CMModelDefine;->isShowConnetNetWorkPopup(Landroid/content/Context;I)Z

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_f

    .line 3458
    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mUri:Landroid/net/Uri;

    .line 3459
    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMimeType:Ljava/lang/String;

    .line 3460
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    const-string/jumbo v13, "layout_inflater"

    invoke-virtual {v12, v13}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 3461
    .local v4, "inflater":Landroid/view/LayoutInflater;
    const v12, 0x7f030006

    const/4 v13, 0x0

    invoke-virtual {v4, v12, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 3463
    .local v7, "networkView":Landroid/view/View;
    const v12, 0x7f0b003b

    invoke-virtual {v7, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 3464
    .local v9, "tv":Landroid/widget/TextView;
    const v12, 0x7f0b003c

    invoke-virtual {v7, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mCheckBox:Landroid/widget/CheckBox;

    .line 3465
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v12}, Lcom/infraware/common/util/Utils;->getIsConnectWlan(Landroid/app/Activity;)Z

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_d

    .line 3466
    const v12, 0x7f07005d

    invoke-virtual {v9, v12}, Landroid/widget/TextView;->setText(I)V

    .line 3467
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    sget-object v14, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_WLAN_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    const/4 v15, 0x3

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    move-object/from16 v17, v0

    aput-object v17, v15, v16

    const/16 v16, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onSendEmailAlarmClickListener:Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v17, v0

    aput-object v17, v15, v16

    const/16 v16, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    move-object/from16 v17, v0

    aput-object v17, v15, v16

    invoke-virtual {v12, v13, v14, v15}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    .line 3485
    .end local v4    # "inflater":Landroid/view/LayoutInflater;
    .end local v7    # "networkView":Landroid/view/View;
    .end local v9    # "tv":Landroid/widget/TextView;
    :goto_1
    const/4 v12, 0x1

    goto/16 :goto_0

    .line 3472
    .restart local v4    # "inflater":Landroid/view/LayoutInflater;
    .restart local v7    # "networkView":Landroid/view/View;
    .restart local v9    # "tv":Landroid/widget/TextView;
    :cond_d
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v12}, Lcom/infraware/common/util/Utils;->getIsConnectMobileNetwork(Landroid/app/Activity;)Z

    move-result v12

    if-eqz v12, :cond_e

    .line 3473
    const v12, 0x7f07005b

    invoke-virtual {v9, v12}, Landroid/widget/TextView;->setText(I)V

    .line 3474
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    sget-object v14, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_MOBILE_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    const/4 v15, 0x3

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    move-object/from16 v17, v0

    aput-object v17, v15, v16

    const/16 v16, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onSendEmailAlarmClickListener:Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v17, v0

    aput-object v17, v15, v16

    const/16 v16, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    move-object/from16 v17, v0

    aput-object v17, v15, v16

    invoke-virtual {v12, v13, v14, v15}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto :goto_1

    .line 3478
    :cond_e
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    sget-object v14, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_NETWORK_NOT_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    const/4 v15, 0x0

    new-array v15, v15, [Ljava/lang/Object;

    invoke-virtual {v12, v13, v14, v15}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto :goto_1

    .line 3482
    .end local v4    # "inflater":Landroid/view/LayoutInflater;
    .end local v7    # "networkView":Landroid/view/View;
    .end local v9    # "tv":Landroid/widget/TextView;
    :cond_f
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v5}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->exportEmail(Landroid/net/Uri;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onSendEMailEnd()V
    .locals 4

    .prologue
    .line 3543
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrTempFilePathForEMail:Ljava/lang/String;

    .line 3544
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbSendingEMail:Z

    .line 3546
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity$30;

    invoke-direct {v1, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$30;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 3554
    return-void
.end method

.method public onSendFilePath(ZI)V
    .locals 18
    .param p1, "bPdf"    # Z
    .param p2, "type"    # I

    .prologue
    .line 3305
    if-eqz p1, :cond_1

    .line 3307
    move/from16 v2, p2

    .line 3308
    .local v2, "contentType":I
    add-int/lit8 v8, v2, 0x0

    .line 3309
    .local v8, "mode":I
    new-instance v3, Landroid/content/Intent;

    const-class v15, Lcom/infraware/polarisoffice5/common/ExportToPdf;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v15}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3310
    .local v3, "exportToPdf":Landroid/content/Intent;
    new-instance v9, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-direct {v9, v15}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 3311
    .local v9, "savePath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lcom/infraware/common/util/FileUtils;->isSavableDirectory(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_0

    .line 3312
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v15

    invoke-virtual {v15}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3313
    .local v1, "LOCAL_ROOT_PATH":Ljava/lang/String;
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/infraware/common/util/FileUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 3315
    .end local v1    # "LOCAL_ROOT_PATH":Ljava/lang/String;
    :cond_0
    const-string/jumbo v15, "key_filename"

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocExtensionType:I

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v3, v15, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3316
    const-string/jumbo v15, "key_current_file"

    invoke-virtual {v3, v15, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3317
    const-string/jumbo v15, "key_content_mode"

    invoke-virtual {v3, v15, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3318
    const-string/jumbo v15, "PDFSaveToEmail"

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v3, v15, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3319
    const/16 v15, 0x1d

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v15}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 3361
    .end local v2    # "contentType":I
    .end local v3    # "exportToPdf":Landroid/content/Intent;
    .end local v8    # "mode":I
    .end local v9    # "savePath":Ljava/lang/String;
    :goto_0
    return-void

    .line 3324
    :cond_1
    const/4 v7, 0x0

    .line 3325
    .local v7, "mimeType":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-direct {v4, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3326
    .local v4, "f":Ljava/io/File;
    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v13

    .line 3329
    .local v13, "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    const/16 v16, 0x2e

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    .line 3330
    .local v5, "idx_exe":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    add-int/lit8 v16, v5, 0x1

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 3332
    .local v10, "temp":Ljava/lang/String;
    new-instance v6, Landroid/content/Intent;

    const-string/jumbo v15, "android.intent.action.SEND"

    invoke-direct {v6, v15}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3334
    .local v6, "intent":Landroid/content/Intent;
    invoke-virtual {v10}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/infraware/common/util/FileUtils;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 3336
    if-nez v7, :cond_2

    .line 3337
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "application/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v10}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 3339
    :cond_2
    invoke-virtual {v6, v7}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 3340
    const-string/jumbo v15, "text/plain"

    invoke-virtual {v7, v15}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v15

    if-nez v15, :cond_3

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v15

    const-string/jumbo v16, "FT03"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 3343
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v17, 0x7d0

    invoke-static/range {v15 .. v17}, Lcom/infraware/common/util/FileUtils;->getTextFromFile(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v11

    .line 3345
    .local v11, "text":Ljava/lang/String;
    const-string/jumbo v15, "sms_body"

    invoke-virtual {v6, v15, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3346
    const-string/jumbo v15, "PolarisOfficeMode"

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3349
    .end local v11    # "text":Ljava/lang/String;
    :cond_3
    const-string/jumbo v15, "android.intent.extra.STREAM"

    invoke-virtual {v6, v15, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 3353
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v15

    const-string/jumbo v16, "FV09"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 3355
    const v15, 0x7f0702ce

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 3359
    .local v12, "title":Ljava/lang/CharSequence;
    :goto_1
    invoke-static {v6, v12}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v14

    .line 3360
    .local v14, "wrapperIntent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 3357
    .end local v12    # "title":Ljava/lang/CharSequence;
    .end local v14    # "wrapperIntent":Landroid/content/Intent;
    :cond_4
    const v15, 0x7f0702ae

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    .restart local v12    # "title":Ljava/lang/CharSequence;
    goto :goto_1
.end method

.method public onSetting()V
    .locals 3

    .prologue
    .line 3062
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/infraware/polarisoffice5/setting/SettingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3063
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "key_version"

    const v2, 0x7f070322

    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3065
    const-string/jumbo v1, "key_interanl_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3066
    const/16 v1, 0x1015

    invoke-virtual {p0, v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 3067
    return-void
.end method

.method public onSettingActivity()V
    .locals 3

    .prologue
    .line 2685
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/infraware/polarisoffice5/setting/SettingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2686
    .local v0, "settingIntent":Landroid/content/Intent;
    const-string/jumbo v1, "key_version"

    const v2, 0x7f070322

    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2688
    const-string/jumbo v1, "key_interanl_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2689
    const/16 v1, 0xd

    invoke-virtual {p0, v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2690
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 4968
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPowerSaveOption:Lcom/infraware/office/baseframe/porting/EvPowerSaveOption;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->getDocFileExtentionType(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 4971
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPowerSaveOption:Lcom/infraware/office/baseframe/porting/EvPowerSaveOption;

    invoke-virtual {v0, p0}, Lcom/infraware/office/baseframe/porting/EvPowerSaveOption;->setPowerSaveOnResume(Landroid/content/Context;)V

    .line 4972
    :cond_0
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onStart()V

    .line 4973
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 4977
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbFinishCalled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPowerSaveOption:Lcom/infraware/office/baseframe/porting/EvPowerSaveOption;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->getDocFileExtentionType(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 4980
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPowerSaveOption:Lcom/infraware/office/baseframe/porting/EvPowerSaveOption;

    invoke-virtual {v0, p0}, Lcom/infraware/office/baseframe/porting/EvPowerSaveOption;->setPowerRestoreOnPause(Landroid/content/Context;)V

    .line 4981
    :cond_0
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onStop()V

    .line 4982
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 4
    .param p1, "hasFocus"    # Z

    .prologue
    .line 5348
    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v1, :cond_1

    .line 5349
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->GetInterfaceHandle()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/office/evengine/EvInterface;->SetInterfaceHandleAddress(I)V

    .line 5350
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setEvListener()V

    .line 5352
    const/4 v0, 0x0

    .line 5353
    .local v0, "bLandScape":I
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 5354
    const/4 v0, 0x1

    .line 5355
    :cond_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseView;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/EvBaseView;->getHeight()I

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IChangeScreen(III)V

    .line 5358
    .end local v0    # "bLandScape":I
    :cond_1
    invoke-super {p0, p1}, Lcom/infraware/common/baseactivity/BaseActivity;->onWindowFocusChanged(Z)V

    .line 5359
    return-void
.end method

.method public onWindowStatusChanged(ZZZ)V
    .locals 1
    .param p1, "isMaximized"    # Z
    .param p2, "isMinimized"    # Z
    .param p3, "isPinup"    # Z

    .prologue
    .line 5618
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onMultiWindowStatusChanged(Z)V

    .line 5619
    return-void

    .line 5618
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onZoomMenu(Z)V
    .locals 14
    .param p1, "isToolbar"    # Z

    .prologue
    .line 3221
    const/4 v0, 0x0

    .line 3222
    .local v0, "ZOOM_MENU_FIT_PAGE":I
    const/4 v1, 0x1

    .line 3224
    .local v1, "ZOOM_MENU_FIT_WIDTH":I
    new-instance v8, Ljava/util/Vector;

    invoke-direct {v8}, Ljava/util/Vector;-><init>()V

    .line 3225
    .local v8, "zoomStringVector":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    new-instance v11, Ljava/util/Vector;

    invoke-direct {v11}, Ljava/util/Vector;-><init>()V

    .line 3227
    .local v11, "zoomValueVector":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Integer;>;"
    if-nez p1, :cond_0

    .line 3228
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocExtensionType()I

    move-result v12

    sparse-switch v12, :sswitch_data_0

    .line 3243
    const v12, 0x7f070137

    invoke-virtual {p0, v12}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v12}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 3244
    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 3248
    :goto_0
    :sswitch_0
    iget v12, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocExtensionType:I

    const/16 v13, 0xc

    if-eq v12, v13, :cond_0

    .line 3249
    const v12, 0x7f07013a

    invoke-virtual {p0, v12}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v12}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 3250
    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 3254
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getZoomValueArray()[I

    move-result-object v5

    .line 3255
    .local v5, "validZoomValueArray":[I
    const/16 v12, 0x8

    new-array v6, v12, [I

    fill-array-data v6, :array_0

    .line 3257
    .local v6, "zoomList":[I
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_1
    array-length v12, v6

    if-ge v2, v12, :cond_2

    .line 3258
    aget v12, v6, v2

    invoke-static {v5, v12}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v4

    .line 3260
    .local v4, "validZoomIndex":I
    if-ltz v4, :cond_1

    .line 3261
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    aget v13, v5, v4

    invoke-static {v13}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "%"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v12}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 3263
    aget v12, v5, v4

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 3257
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3231
    .end local v2    # "index":I
    .end local v4    # "validZoomIndex":I
    .end local v5    # "validZoomValueArray":[I
    .end local v6    # "zoomList":[I
    :sswitch_1
    const v12, 0x7f070138

    invoke-virtual {p0, v12}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v12}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 3232
    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3237
    :sswitch_2
    const v12, 0x7f070139

    invoke-virtual {p0, v12}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v12}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 3238
    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3267
    .restart local v2    # "index":I
    .restart local v5    # "validZoomValueArray":[I
    .restart local v6    # "zoomList":[I
    :cond_2
    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v12

    new-array v7, v12, [Ljava/lang/String;

    .line 3268
    .local v7, "zoomMenuString":[Ljava/lang/String;
    invoke-virtual {v8, v7}, Ljava/util/Vector;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 3269
    invoke-virtual {v11}, Ljava/util/Vector;->size()I

    move-result v12

    new-array v10, v12, [Ljava/lang/Integer;

    .line 3270
    .local v10, "zoomValue":[Ljava/lang/Integer;
    invoke-virtual {v11, v10}, Ljava/util/Vector;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 3272
    iget-object v12, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v12}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v3

    .line 3273
    .local v3, "info":Lcom/infraware/office/evengine/EV$CONFIG_INFO;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const v13, 0x7f070254

    invoke-virtual {p0, v13}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, " - "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, v3, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "%"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 3275
    .local v9, "zoomTitle":Ljava/lang/String;
    new-instance v12, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v13

    invoke-direct {v12, p0, v13}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v12, v9}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v12

    new-instance v13, Lcom/infraware/office/baseframe/EvBaseViewerActivity$29;

    invoke-direct {v13, p0, v10}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$29;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;[Ljava/lang/Integer;)V

    invoke-virtual {v12, v7, v13}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v12

    invoke-virtual {v12}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v12

    iput-object v12, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mZoomMenu:Landroid/app/AlertDialog;

    .line 3299
    iget-object v12, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mZoomMenu:Landroid/app/AlertDialog;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 3300
    iget-object v12, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mZoomMenu:Landroid/app/AlertDialog;

    invoke-virtual {v12}, Landroid/app/AlertDialog;->show()V

    .line 3301
    return-void

    .line 3228
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x5 -> :sswitch_2
        0xc -> :sswitch_0
        0x13 -> :sswitch_1
        0x14 -> :sswitch_2
        0x26 -> :sswitch_2
    .end sparse-switch

    .line 3255
    :array_0
    .array-data 4
        0x19
        0x32
        0x4b
        0x64
        0x7d
        0xc8
        0x12c
        0x190
    .end array-data
.end method

.method public processLoadComplete()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1896
    const/4 v0, 0x0

    .line 1897
    .local v0, "bShowAnimation":Z
    iget-boolean v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbLoadComplete:Z

    if-nez v1, :cond_2

    .line 1899
    const/4 v0, 0x1

    .line 1901
    iput-boolean v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbLoadComplete:Z

    .line 1903
    invoke-virtual {p0, v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setSheetTitle(I)V

    .line 1904
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->checkProtectDocument()V

    .line 1906
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setDeviceResolution()V

    .line 1909
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPrevDlg:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    .line 1910
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPrevDlg:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1911
    const/16 v1, 0x1b

    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->removeDialog(I)V

    .line 1917
    :cond_0
    iget-boolean v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mIsContentSearch:Z

    if-nez v1, :cond_1

    .line 1919
    invoke-static {}, Lcom/infraware/polarisoffice5/common/DlgFactory;->getInstance()Lcom/infraware/polarisoffice5/common/DlgFactory;

    move-result-object v1

    const/16 v2, 0x9

    invoke-virtual {v1, p0, v2}, Lcom/infraware/polarisoffice5/common/DlgFactory;->dismisProgress(Landroid/app/Activity;I)V

    .line 1924
    :cond_1
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->OnLoadComplete()V

    .line 1932
    iget v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mInternalCmdType:I

    if-ne v1, v4, :cond_2

    .line 1933
    iget v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    .line 1934
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetViewMode(I)V

    .line 1955
    :cond_2
    iget-boolean v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mRequestThumbnailOnSave:Z

    if-eqz v1, :cond_4

    .line 1956
    const-string/jumbo v1, "EvBaseViewerActivity"

    const-string/jumbo v2, "prcessLoadComplete : Saved, Call getPageThumbnail()"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1957
    iget v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocType:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_3

    .line 1958
    iput-boolean v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mRequestThumbnailOnSave:Z

    .line 1960
    :cond_3
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getPageThumbnail()V

    .line 1976
    :cond_4
    return-void
.end method

.method public runGoogleSearch()V
    .locals 11

    .prologue
    const/4 v6, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 5665
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    const/16 v4, 0x135

    invoke-static {v3, v4}, Lcom/infraware/common/define/CMModelDefine;->isShowConnetNetWorkPopup(Landroid/content/Context;I)Z

    move-result v3

    if-ne v3, v8, :cond_2

    .line 5668
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 5669
    .local v0, "inflater1":Landroid/view/LayoutInflater;
    const v3, 0x7f030006

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 5671
    .local v1, "networkView":Landroid/view/View;
    const v3, 0x7f0b003b

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 5672
    .local v2, "tv":Landroid/widget/TextView;
    const v3, 0x7f0b003c

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mCheckBox:Landroid/widget/CheckBox;

    .line 5673
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/infraware/common/util/Utils;->getIsConnectWlan(Landroid/app/Activity;)Z

    move-result v3

    if-ne v3, v8, :cond_0

    .line 5674
    const v3, 0x7f07005d

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 5675
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_WLAN_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onGoolgeSearchClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v7, v6, v8

    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v7, v6, v10

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    .line 5692
    .end local v0    # "inflater1":Landroid/view/LayoutInflater;
    .end local v1    # "networkView":Landroid/view/View;
    .end local v2    # "tv":Landroid/widget/TextView;
    :goto_0
    return-void

    .line 5680
    .restart local v0    # "inflater1":Landroid/view/LayoutInflater;
    .restart local v1    # "networkView":Landroid/view/View;
    .restart local v2    # "tv":Landroid/widget/TextView;
    :cond_0
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/infraware/common/util/Utils;->getIsConnectMobileNetwork(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 5681
    const v3, 0x7f07005b

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 5682
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_MOBILE_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onGoolgeSearchClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v7, v6, v8

    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v7, v6, v10

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto :goto_0

    .line 5686
    :cond_1
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_NETWORK_NOT_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto :goto_0

    .line 5690
    .end local v0    # "inflater1":Landroid/view/LayoutInflater;
    .end local v1    # "networkView":Landroid/view/View;
    .end local v2    # "tv":Landroid/widget/TextView;
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->connectGooleSearch()V

    goto :goto_0
.end method

.method public runShare()V
    .locals 11

    .prologue
    const/4 v6, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 5696
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    const/16 v4, 0x134

    invoke-static {v3, v4}, Lcom/infraware/common/define/CMModelDefine;->isShowConnetNetWorkPopup(Landroid/content/Context;I)Z

    move-result v3

    if-ne v3, v8, :cond_2

    .line 5699
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 5700
    .local v0, "inflater1":Landroid/view/LayoutInflater;
    const v3, 0x7f030006

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 5702
    .local v1, "networkView":Landroid/view/View;
    const v3, 0x7f0b003b

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 5703
    .local v2, "tv":Landroid/widget/TextView;
    const v3, 0x7f0b003c

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mCheckBox:Landroid/widget/CheckBox;

    .line 5704
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/infraware/common/util/Utils;->getIsConnectWlan(Landroid/app/Activity;)Z

    move-result v3

    if-ne v3, v8, :cond_0

    .line 5705
    const v3, 0x7f07005d

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 5706
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_WLAN_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onRunShareClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v7, v6, v8

    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v7, v6, v10

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    .line 5723
    .end local v0    # "inflater1":Landroid/view/LayoutInflater;
    .end local v1    # "networkView":Landroid/view/View;
    .end local v2    # "tv":Landroid/widget/TextView;
    :goto_0
    return-void

    .line 5711
    .restart local v0    # "inflater1":Landroid/view/LayoutInflater;
    .restart local v1    # "networkView":Landroid/view/View;
    .restart local v2    # "tv":Landroid/widget/TextView;
    :cond_0
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/infraware/common/util/Utils;->getIsConnectMobileNetwork(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 5712
    const v3, 0x7f07005b

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 5713
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_MOBILE_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onRunShareClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v7, v6, v8

    iget-object v7, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v7, v6, v10

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto :goto_0

    .line 5717
    :cond_1
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_NETWORK_NOT_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto :goto_0

    .line 5721
    .end local v0    # "inflater1":Landroid/view/LayoutInflater;
    .end local v1    # "networkView":Landroid/view/View;
    .end local v2    # "tv":Landroid/widget/TextView;
    :cond_2
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->connectRunShare()V

    goto :goto_0
.end method

.method public sendDictionaryMessage()V
    .locals 2

    .prologue
    .line 4748
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4749
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setSearchKeyword(Ljava/lang/String;)Z

    .line 4750
    :cond_0
    return-void
.end method

.method public setClipBoardData(Ljava/lang/String;)V
    .locals 1
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 4696
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mClipBoard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    if-nez v0, :cond_0

    .line 4699
    :goto_0
    return-void

    .line 4698
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mClipBoard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    invoke-virtual {v0, p1}, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected setDeviceResolution()V
    .locals 8

    .prologue
    .line 5415
    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v4}, Lcom/infraware/office/evengine/EvInterface;->IGetFrameDetectionArea()Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;

    move-result-object v0

    .line 5417
    .local v0, "frameDetectionArea":Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {p0, v4}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v4

    float-to-double v4, v4

    iput-wide v4, v0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_dDeviceDIP:D

    .line 5418
    iget-wide v4, v0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_dDeviceDIP:D

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double v2, v4, v6

    .line 5420
    .local v2, "ratio":D
    iget v4, v0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_nCtrlBoxMargin:I

    int-to-double v4, v4

    mul-double/2addr v4, v2

    double-to-int v4, v4

    iput v4, v0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_nCtrlBoxMargin:I

    .line 5421
    iget v4, v0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_nFrameDetectionMargin:I

    int-to-double v4, v4

    mul-double/2addr v4, v2

    double-to-int v4, v4

    iput v4, v0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_nFrameDetectionMargin:I

    .line 5422
    iget v4, v0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_nRotCtrlBoxHeight:I

    int-to-double v4, v4

    mul-double/2addr v4, v2

    double-to-int v4, v4

    iput v4, v0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_nRotCtrlBoxHeight:I

    .line 5424
    const/high16 v4, 0x41a00000    # 20.0f

    invoke-virtual {p0, v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->dipToPx(F)F

    move-result v1

    .line 5425
    .local v1, "nPx":F
    float-to-int v4, v1

    iput v4, v0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_nSheetDetectionMargin:I

    .line 5426
    iget v4, v0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_nVMLShapeHandlePosMargin:I

    int-to-double v4, v4

    mul-double/2addr v4, v2

    double-to-int v4, v4

    iput v4, v0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_nVMLShapeHandlePosMargin:I

    .line 5427
    iget v4, v0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_nAdjustHandleDetectionMargin:I

    int-to-double v4, v4

    mul-double/2addr v4, v2

    double-to-int v4, v4

    iput v4, v0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_nAdjustHandleDetectionMargin:I

    .line 5429
    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v4, v0}, Lcom/infraware/office/evengine/EvInterface;->ISetFrameDetectionArea(Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;)V

    .line 5430
    return-void
.end method

.method public abstract setEvListener()V
.end method

.method public setFocus()V
    .locals 1

    .prologue
    .line 1460
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->requestFocus()Z

    .line 1461
    return-void
.end method

.method public setFocusFindButton(Ljava/lang/Boolean;)V
    .locals 1
    .param p1, "isFoucsNextButton"    # Ljava/lang/Boolean;

    .prologue
    .line 5744
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindBar:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5745
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5746
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindNextBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 5753
    :cond_0
    :goto_0
    return-void

    .line 5749
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindPrevBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    goto :goto_0
.end method

.method public setMatchCase(I)V
    .locals 0
    .param p1, "b"    # I

    .prologue
    .line 767
    iput p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbMatchCase:I

    .line 768
    return-void
.end method

.method public setMatchWhole(I)V
    .locals 0
    .param p1, "b"    # I

    .prologue
    .line 771
    iput p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbMatchWhole:I

    .line 772
    return-void
.end method

.method public setMotionEventRect(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "spellRect"    # Landroid/graphics/Rect;

    .prologue
    .line 5048
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mRect:Landroid/graphics/Rect;

    .line 5049
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mRect:Landroid/graphics/Rect;

    .line 5050
    return-void
.end method

.method public setSendingEMailCancel()V
    .locals 2

    .prologue
    .line 5563
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbSendingEMail:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 5564
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbSendingEMail:Z

    .line 5565
    :cond_0
    return-void
.end method

.method protected setSheetTitle(I)V
    .locals 3
    .param p1, "nCurrentPage"    # I

    .prologue
    .line 4499
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_SHEET_TITLE_NAME()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 4500
    iget v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocExtensionType:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mDocExtensionType:I

    const/16 v2, 0x14

    if-ne v1, v2, :cond_2

    .line 4502
    :cond_0
    if-nez p1, :cond_1

    .line 4503
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetCurrentSheetIndex()I

    move-result v1

    add-int/lit8 p1, v1, 0x1

    .line 4504
    :cond_1
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->EV()Lcom/infraware/office/evengine/EV;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getSheetInfo()Lcom/infraware/office/evengine/EV$SHEET_INFO;

    move-result-object v0

    .line 4505
    .local v0, "sSheetInfo":Lcom/infraware/office/evengine/EV$SHEET_INFO;
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    add-int/lit8 v2, p1, -0x1

    invoke-virtual {v1, v0, v2}, Lcom/infraware/office/evengine/EvInterface;->IGetSheetInfo(Lcom/infraware/office/evengine/EV$SHEET_INFO;I)V

    .line 4510
    .end local v0    # "sSheetInfo":Lcom/infraware/office/evengine/EV$SHEET_INFO;
    :cond_2
    return-void
.end method

.method public setShowSearchBar(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 1590
    iput-boolean p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbShowSearchBar:Z

    .line 1591
    return-void
.end method

.method public setViewerMode(Z)V
    .locals 6
    .param p1, "isViewerMode"    # Z

    .prologue
    const/4 v5, 0x6

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 4997
    iput-boolean p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mIsViewerMode:Z

    .line 5008
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mIsViewerMode:Z

    if-eqz v0, :cond_6

    .line 5011
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5012
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/infraware/office/util/EvUtil;->hideIme(Landroid/content/Context;Landroid/os/IBinder;)V

    .line 5014
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    if-eq v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    if-eq v0, v3, :cond_1

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    if-ne v0, v4, :cond_5

    .line 5015
    :cond_1
    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ChangeViewMode(I)V

    .line 5027
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    if-ne v0, v1, :cond_3

    move-object v0, p0

    .line 5029
    check-cast v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    iget-object v3, v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->mMemo_view:Lcom/infraware/polarisoffice5/common/MemoView;

    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mIsViewerMode:Z

    if-nez v0, :cond_9

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/infraware/polarisoffice5/common/MemoView;->setTextEnabled(Z)V

    .line 5032
    :cond_3
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    if-ne v0, v4, :cond_4

    .line 5034
    check-cast p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .end local p0    # "this":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getSheetBar()Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->constructSheetbar()V

    .line 5037
    :cond_4
    return-void

    .line 5016
    .restart local p0    # "this":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    :cond_5
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HWP_EDIT_SUPPORT()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    if-ne v0, v5, :cond_2

    .line 5017
    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ChangeViewMode(I)V

    goto :goto_0

    .line 5021
    :cond_6
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    if-eq v0, v1, :cond_7

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    if-eq v0, v3, :cond_7

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    if-ne v0, v4, :cond_8

    .line 5022
    :cond_7
    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ChangeViewMode(I)V

    goto :goto_0

    .line 5023
    :cond_8
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HWP_EDIT_SUPPORT()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    if-ne v0, v5, :cond_2

    .line 5024
    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ChangeViewMode(I)V

    goto :goto_0

    :cond_9
    move v0, v2

    .line 5029
    goto :goto_1
.end method

.method public showSpellPopup(Landroid/graphics/Rect;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p1, "mRect"    # Landroid/graphics/Rect;
    .param p2, "worngSpell"    # Ljava/lang/String;
    .param p3, "mCandidateWordList"    # [Ljava/lang/String;

    .prologue
    .line 5040
    return-void
.end method

.method public startPanningMode()V
    .locals 2

    .prologue
    .line 5361
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseView;->setActionMode(I)V

    .line 5362
    return-void
.end method
