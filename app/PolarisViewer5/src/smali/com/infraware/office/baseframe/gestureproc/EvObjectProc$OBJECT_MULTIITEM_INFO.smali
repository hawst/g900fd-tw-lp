.class public Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;
.super Ljava/lang/Object;
.source "EvObjectProc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "OBJECT_MULTIITEM_INFO"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field public bGroupEnabled:I

.field public bRotationEnabled:I

.field public editEndPoint:Landroid/graphics/Point;

.field public editRotateAngle:I

.field public editStartPoint:Landroid/graphics/Point;

.field public endRangePoint:Landroid/graphics/Point;

.field public mObjectType:I

.field public rotateAngle:I

.field public startRangePoint:Landroid/graphics/Point;

.field final synthetic this$0:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 380
    const-class v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 380
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 381
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->mObjectType:I

    .line 382
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->startRangePoint:Landroid/graphics/Point;

    .line 383
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->endRangePoint:Landroid/graphics/Point;

    .line 384
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->editStartPoint:Landroid/graphics/Point;

    .line 385
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->editEndPoint:Landroid/graphics/Point;

    .line 386
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->rotateAngle:I

    .line 387
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->editRotateAngle:I

    .line 388
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->bRotationEnabled:I

    .line 389
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->bGroupEnabled:I

    return-void
.end method


# virtual methods
.method public SetInit()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 391
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->mObjectType:I

    .line 392
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->startRangePoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 393
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->endRangePoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 394
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->editStartPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 395
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->editEndPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 397
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->rotateAngle:I

    .line 398
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->editRotateAngle:I

    .line 399
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->bRotationEnabled:I

    .line 400
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->bGroupEnabled:I

    .line 401
    return-void
.end method

.method public setData([I)V
    .locals 3
    .param p1, "pointarray"    # [I

    .prologue
    .line 404
    sget-boolean v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    array-length v0, p1

    const/16 v1, 0xd

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 405
    :cond_0
    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->mObjectType:I

    .line 406
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->startRangePoint:Landroid/graphics/Point;

    const/4 v1, 0x1

    aget v1, p1, v1

    const/4 v2, 0x2

    aget v2, p1, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 407
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->endRangePoint:Landroid/graphics/Point;

    const/4 v1, 0x3

    aget v1, p1, v1

    const/4 v2, 0x4

    aget v2, p1, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 408
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->editStartPoint:Landroid/graphics/Point;

    const/4 v1, 0x5

    aget v1, p1, v1

    const/4 v2, 0x6

    aget v2, p1, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 409
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->editEndPoint:Landroid/graphics/Point;

    const/4 v1, 0x7

    aget v1, p1, v1

    const/16 v2, 0x8

    aget v2, p1, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 411
    const/16 v0, 0x9

    aget v0, p1, v0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->rotateAngle:I

    .line 412
    const/16 v0, 0xa

    aget v0, p1, v0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->editRotateAngle:I

    .line 413
    const/16 v0, 0xb

    aget v0, p1, v0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->bRotationEnabled:I

    .line 414
    const/16 v0, 0xc

    aget v0, p1, v0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->bGroupEnabled:I

    .line 415
    return-void
.end method
