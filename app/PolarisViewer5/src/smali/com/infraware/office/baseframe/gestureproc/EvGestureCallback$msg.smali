.class public interface abstract Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback$msg;
.super Ljava/lang/Object;
.source "EvGestureCallback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "msg"
.end annotation


# static fields
.field public static final eOnAniMation:I = 0x35

.field public static final eOnBeforeInsertShapes:I = 0x37

.field public static final eOnBeforeSurfaceChanged:I = 0xc

.field public static final eOnCommitText:I = 0xb

.field public static final eOnContextMenu:I = 0x0

.field public static final eOnDictionary:I = 0x7

.field public static final eOnDoubleDataChart:I = 0xe

.field public static final eOnFling:I = 0x2d

.field public static final eOnHyperLink:I = 0x6

.field public static final eOnInfraPenCustomColor:I = 0x3a

.field public static final eOnInfraPenDrawing:I = 0x34

.field public static final eOnInsertShapes:I = 0x39

.field public static final eOnLoadComplete:I = 0x2c

.field public static final eOnLongPress:I = 0x4

.field public static final eOnMainViewTouched:I = 0x3

.field public static final eOnMoreAutoHeight:I = 0x16

.field public static final eOnMoreAutoWidth:I = 0x15

.field public static final eOnMoreChartDataRange:I = 0x38

.field public static final eOnMoreClear:I = 0x1e

.field public static final eOnMoreColumWidth:I = 0x14

.field public static final eOnMoreConditionalFormat:I = 0x32

.field public static final eOnMoreDeleteCell:I = 0x12

.field public static final eOnMoreDistributeCols:I = 0x1b

.field public static final eOnMoreDistributeRows:I = 0x1a

.field public static final eOnMoreFormatCopy:I = 0x10

.field public static final eOnMoreFormatPaste:I = 0x11

.field public static final eOnMoreGroup:I = 0x26

.field public static final eOnMoreHideColum:I = 0x17

.field public static final eOnMoreHideMemo:I = 0x36

.field public static final eOnMoreHideRow:I = 0x18

.field public static final eOnMoreInsertCells:I = 0x28

.field public static final eOnMoreMask:I = 0x29

.field public static final eOnMoreMemo:I = 0xf

.field public static final eOnMoreMerge:I = 0x19

.field public static final eOnMoreMultiSelection:I = 0x2b

.field public static final eOnMoreReplaceImage:I = 0x21

.field public static final eOnMoreResizeImage:I = 0x22

.field public static final eOnMoreRotateImage:I = 0x25

.field public static final eOnMoreRowHeight:I = 0x13

.field public static final eOnMoreSelectAll:I = 0x1c

.field public static final eOnMoreSelectColumns:I = 0x1f

.field public static final eOnMoreSelectRows:I = 0x20

.field public static final eOnMoreSheetHyperlink:I = 0x2e

.field public static final eOnMoreSplit:I = 0x1d

.field public static final eOnMoreTTS:I = 0x2a

.field public static final eOnMoreUnGroup:I = 0x27

.field public static final eOnMoreUnHideColum:I = 0x23

.field public static final eOnMoreUnHideRow:I = 0x24

.field public static final eOnMoveFocusToActionbar:I = 0x31

.field public static final eOnNeedToRedraw:I = 0x2

.field public static final eOnNewDocument:I = 0x8

.field public static final eOnProcessShortCutKey:I = 0x30

.field public static final eOnSheetEditFocus:I = 0x1

.field public static final eOnSheetEditTextBox:I = 0x33

.field public static final eOnSheetKey:I = 0x2f

.field public static final eOnShowIme:I = 0x9

.field public static final eOnSingleDataChart:I = 0xd

.field public static final eOnSurfaceChanged:I = 0x5

.field public static final eOnTemplateDocument:I = 0xa
