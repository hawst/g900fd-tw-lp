.class public Lcom/infraware/office/baseframe/EvInputConnection;
.super Landroid/view/inputmethod/BaseInputConnection;
.source "EvInputConnection.java"


# static fields
.field public static final CAP_MODE_CHARACTERS:I = 0x1000

.field public static final CAP_MODE_SENTENCES:I = 0x4000

.field public static final CAP_MODE_WORDS:I = 0x2000

.field private static final DEBUG:Z = true

.field private static final TAG:Ljava/lang/String; = "EvInputConnection"


# instance fields
.field private mDisableInputTextMode:Z

.field private final mTextView:Lcom/infraware/office/baseframe/EvBaseView;


# direct methods
.method public constructor <init>(Lcom/infraware/office/baseframe/EvBaseView;)V
    .locals 1
    .param p1, "targetView"    # Lcom/infraware/office/baseframe/EvBaseView;

    .prologue
    .line 34
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Landroid/view/inputmethod/BaseInputConnection;-><init>(Landroid/view/View;Z)V

    .line 448
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/EvInputConnection;->mDisableInputTextMode:Z

    .line 35
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvInputConnection;->mTextView:Lcom/infraware/office/baseframe/EvBaseView;

    .line 36
    return-void
.end method


# virtual methods
.method public beginBatchEdit()Z
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvInputConnection;->mTextView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->beginBatchEdit()Z

    move-result v0

    return v0
.end method

.method public calcComposingLen()I
    .locals 8

    .prologue
    const/4 v7, -0x1

    .line 363
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvInputConnection;->getEditable()Landroid/text/Editable;

    move-result-object v2

    .line 364
    .local v2, "content":Landroid/text/Editable;
    if-nez v2, :cond_0

    .line 365
    const/4 v4, 0x0

    .line 393
    :goto_0
    return v4

    .line 368
    :cond_0
    invoke-static {v2}, Lcom/infraware/office/baseframe/EvInputConnection;->getComposingSpanStart(Landroid/text/Spannable;)I

    move-result v0

    .line 369
    .local v0, "a":I
    invoke-static {v2}, Lcom/infraware/office/baseframe/EvInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    move-result v1

    .line 371
    .local v1, "b":I
    const-string/jumbo v4, "EvInputConnection"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Composing span: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    if-ge v1, v0, :cond_1

    .line 374
    move v3, v0

    .line 375
    .local v3, "tmp":I
    move v0, v1

    .line 376
    move v1, v3

    .line 379
    .end local v3    # "tmp":I
    :cond_1
    if-eq v0, v7, :cond_3

    if-eq v1, v7, :cond_3

    .line 393
    :cond_2
    :goto_1
    sub-int v4, v1, v0

    goto :goto_0

    .line 382
    :cond_3
    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    .line 383
    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    .line 384
    if-gez v0, :cond_4

    const/4 v0, 0x0

    .line 385
    :cond_4
    if-gez v1, :cond_5

    const/4 v1, 0x0

    .line 386
    :cond_5
    if-ge v1, v0, :cond_2

    .line 387
    move v3, v0

    .line 388
    .restart local v3    # "tmp":I
    move v0, v1

    .line 389
    move v1, v3

    goto :goto_1
.end method

.method public clearMetaKeyStates(I)Z
    .locals 3
    .param p1, "states"    # I

    .prologue
    .line 426
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvInputConnection;->getEditable()Landroid/text/Editable;

    move-result-object v0

    .line 427
    .local v0, "content":Landroid/text/Editable;
    if-nez v0, :cond_0

    const/4 v2, 0x0

    .line 437
    :goto_0
    return v2

    .line 428
    :cond_0
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvInputConnection;->mTextView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseView;->getKeyListener()Landroid/text/method/KeyListener;

    move-result-object v1

    .line 429
    .local v1, "kl":Landroid/text/method/KeyListener;
    if-eqz v1, :cond_1

    .line 431
    :try_start_0
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvInputConnection;->mTextView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-interface {v1, v2, v0, p1}, Landroid/text/method/KeyListener;->clearMetaKeyState(Landroid/view/View;Landroid/text/Editable;I)V
    :try_end_0
    .catch Ljava/lang/AbstractMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 437
    :cond_1
    :goto_1
    const/4 v2, 0x1

    goto :goto_0

    .line 432
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method public commitCompletion(Landroid/view/inputmethod/CompletionInfo;)Z
    .locals 2
    .param p1, "text"    # Landroid/view/inputmethod/CompletionInfo;

    .prologue
    .line 298
    const-string/jumbo v0, "EvInputConnection"

    const-string/jumbo v1, "commitCompletion"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    const/4 v0, 0x0

    return v0
.end method

.method public commitText(Ljava/lang/CharSequence;I)Z
    .locals 6
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "newCursorPosition"    # I

    .prologue
    .line 412
    iget-boolean v3, p0, Lcom/infraware/office/baseframe/EvInputConnection;->mDisableInputTextMode:Z

    if-eqz v3, :cond_0

    .line 413
    const/4 v3, 0x1

    .line 421
    :goto_0
    return v3

    .line 415
    :cond_0
    const-string/jumbo v3, "EvInputConnection"

    const-string/jumbo v4, "============================================="

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvInputConnection;->mTextView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/EvBaseView;->getEditableText()Landroid/text/Editable;

    move-result-object v1

    .line 417
    .local v1, "content":Landroid/text/Editable;
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 418
    .local v2, "str":Ljava/lang/String;
    const-string/jumbo v3, "EvInputConnection"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "commitText newCursorPosition "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " text: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/infraware/office/util/EvUtil;->stringToHexa(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " content: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvInputConnection;->calcComposingLen()I

    move-result v0

    .line 420
    .local v0, "compLength":I
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvInputConnection;->mTextView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v3, p1, v0}, Lcom/infraware/office/baseframe/EvBaseView;->commitText(Ljava/lang/CharSequence;I)V

    .line 421
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    move-result v3

    goto :goto_0
.end method

.method public deleteSurroundingText(II)Z
    .locals 16
    .param p1, "leftLength"    # I
    .param p2, "rightLength"    # I

    .prologue
    .line 88
    const-string/jumbo v13, "EvInputConnection"

    const-string/jumbo v14, "============================================="

    invoke-static {v13, v14}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string/jumbo v13, "EvInputConnection"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "deleteSurroundingText "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " / "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvInputConnection;->beginBatchEdit()Z

    .line 93
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvInputConnection;->getEditable()Landroid/text/Editable;

    move-result-object v6

    .line 94
    .local v6, "content":Landroid/text/Editable;
    if-nez v6, :cond_0

    const/4 v13, 0x0

    .line 191
    :goto_0
    return v13

    .line 95
    :cond_0
    invoke-static {v6}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    .line 96
    .local v1, "a":I
    invoke-static {v6}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v2

    .line 97
    .local v2, "b":I
    if-le v1, v2, :cond_1

    .line 98
    move v12, v1

    .line 99
    .local v12, "tmp":I
    move v1, v2

    .line 100
    move v2, v12

    .line 104
    .end local v12    # "tmp":I
    :cond_1
    invoke-static {v6}, Lcom/infraware/office/baseframe/EvInputConnection;->getComposingSpanStart(Landroid/text/Spannable;)I

    move-result v3

    .line 105
    .local v3, "ca":I
    invoke-static {v6}, Lcom/infraware/office/baseframe/EvInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    move-result v4

    .line 106
    .local v4, "cb":I
    if-ge v4, v3, :cond_2

    .line 107
    move v12, v3

    .line 108
    .restart local v12    # "tmp":I
    move v3, v4

    .line 109
    move v4, v12

    .line 112
    .end local v12    # "tmp":I
    :cond_2
    const/4 v13, -0x1

    if-eq v3, v13, :cond_4

    const/4 v13, -0x1

    if-eq v4, v13, :cond_4

    .line 113
    if-ge v3, v1, :cond_3

    move v1, v3

    .line 114
    :cond_3
    if-le v4, v2, :cond_4

    move v2, v4

    .line 118
    :cond_4
    const-string/jumbo v5, ""

    .line 119
    .local v5, "compStr":Ljava/lang/String;
    if-nez p2, :cond_5

    .line 120
    const/4 v13, -0x1

    if-eq v3, v13, :cond_5

    const/4 v13, -0x1

    if-eq v4, v13, :cond_5

    .line 121
    invoke-interface {v6, v3, v4}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 122
    const/4 v13, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v13}, Lcom/infraware/office/baseframe/EvInputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    .line 127
    :cond_5
    const/4 v7, 0x0

    .line 128
    .local v7, "deleted":I
    if-lez p1, :cond_8

    .line 129
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v9

    .line 130
    .local v9, "evInterface":Lcom/infraware/office/evengine/EvInterface;
    invoke-interface {v6}, Landroid/text/Editable;->length()I

    move-result v13

    if-lez v13, :cond_c

    .line 143
    sub-int v10, v1, p1

    .line 144
    .local v10, "start":I
    move v11, v10

    .line 145
    .local v11, "t":I
    if-gez v10, :cond_6

    const/4 v10, 0x0

    .line 146
    :cond_6
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v13

    if-lez v13, :cond_b

    .line 147
    invoke-interface {v6, v10, v2}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 154
    :goto_1
    if-gez v11, :cond_7

    .line 155
    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v11

    .line 156
    const/4 v13, 0x2

    const/16 v14, 0x8

    invoke-virtual {v9, v13, v14, v11}, Lcom/infraware/office/evengine/EvInterface;->ICharInsert(III)V

    .line 160
    :cond_7
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v13

    if-lez v13, :cond_8

    .line 161
    const-string/jumbo v13, "EvInputConnection"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "length = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " text = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    const/4 v13, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v13}, Lcom/infraware/office/baseframe/EvInputConnection;->setComposingText(Ljava/lang/CharSequence;I)Z

    .line 170
    .end local v9    # "evInterface":Lcom/infraware/office/evengine/EvInterface;
    .end local v10    # "start":I
    .end local v11    # "t":I
    :cond_8
    :goto_2
    if-lez p2, :cond_a

    .line 171
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v9

    .line 172
    .restart local v9    # "evInterface":Lcom/infraware/office/evengine/EvInterface;
    invoke-interface {v6}, Landroid/text/Editable;->length()I

    move-result v13

    if-lez v13, :cond_d

    .line 173
    sub-int/2addr v2, v7

    .line 175
    add-int v8, v2, p2

    .line 176
    .local v8, "end":I
    const/4 v11, 0x0

    .line 177
    .restart local v11    # "t":I
    invoke-interface {v6}, Landroid/text/Editable;->length()I

    move-result v13

    if-le v8, v13, :cond_9

    .line 178
    invoke-interface {v6}, Landroid/text/Editable;->length()I

    move-result v13

    sub-int v11, v8, v13

    .line 179
    invoke-interface {v6}, Landroid/text/Editable;->length()I

    move-result v8

    .line 181
    :cond_9
    invoke-interface {v6, v2, v8}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 183
    if-lez v11, :cond_a

    .line 184
    const/4 v13, 0x2

    const/16 v14, 0x2e

    invoke-virtual {v9, v13, v14, v11}, Lcom/infraware/office/evengine/EvInterface;->ICharInsert(III)V

    .line 189
    .end local v8    # "end":I
    .end local v9    # "evInterface":Lcom/infraware/office/evengine/EvInterface;
    .end local v11    # "t":I
    :cond_a
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/EvInputConnection;->endBatchEdit()Z

    .line 191
    const/4 v13, 0x1

    goto/16 :goto_0

    .line 149
    .restart local v9    # "evInterface":Lcom/infraware/office/evengine/EvInterface;
    .restart local v10    # "start":I
    .restart local v11    # "t":I
    :cond_b
    invoke-interface {v6, v10, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 150
    sub-int v7, v1, v10

    goto :goto_1

    .line 167
    .end local v10    # "start":I
    .end local v11    # "t":I
    :cond_c
    const/4 v13, 0x2

    const/16 v14, 0x8

    move/from16 v0, p1

    invoke-virtual {v9, v13, v14, v0}, Lcom/infraware/office/evengine/EvInterface;->ICharInsert(III)V

    goto :goto_2

    .line 187
    :cond_d
    const/4 v13, 0x2

    const/16 v14, 0x2e

    move/from16 v0, p2

    invoke-virtual {v9, v13, v14, v0}, Lcom/infraware/office/evengine/EvInterface;->ICharInsert(III)V

    goto :goto_3
.end method

.method public endBatchEdit()Z
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvInputConnection;->mTextView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->endBatchEdit()Z

    move-result v0

    return v0
.end method

.method public finishComposingText()Z
    .locals 3

    .prologue
    .line 290
    const-string/jumbo v1, "EvInputConnection"

    const-string/jumbo v2, "finishComposingText"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    invoke-super {p0}, Landroid/view/inputmethod/BaseInputConnection;->finishComposingText()Z

    move-result v0

    .line 292
    .local v0, "bRet":Z
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvInputConnection;->mTextView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->finishComposingText()Z

    .line 293
    return v0
.end method

.method public getCursorCapsMode(I)I
    .locals 6
    .param p1, "reqModes"    # I

    .prologue
    .line 265
    const-string/jumbo v3, "EvInputConnection"

    const-string/jumbo v4, "============================================="

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    const-string/jumbo v3, "EvInputConnection"

    const-string/jumbo v4, "getCursorCapsMode"

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    const/4 v1, 0x0

    .line 268
    .local v1, "mode":I
    and-int/lit16 v3, p1, 0x1000

    if-eqz v3, :cond_0

    .line 269
    or-int/lit16 v1, v1, 0x1000

    .line 270
    const-string/jumbo v3, "EvInputConnection"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "getCursorCapsMode set mode "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    :cond_0
    and-int/lit16 v3, p1, 0x6000

    if-nez v3, :cond_1

    .line 273
    const-string/jumbo v3, "EvInputConnection"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "getCursorCapsMode reutn mode "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    .end local v1    # "mode":I
    :goto_0
    return v1

    .line 277
    .restart local v1    # "mode":I
    :cond_1
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    .line 278
    .local v0, "evInterface":Lcom/infraware/office/evengine/EvInterface;
    const-string/jumbo v3, "EvInputConnection"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "getCursorCapsMode "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IsStartOfSentence_Editor()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IsStartOfSentence_Editor()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 280
    or-int/lit16 v2, v1, 0x2000

    .line 281
    .local v2, "test":I
    const-string/jumbo v3, "EvInputConnection"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "getCursorCapsMode return mode "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    or-int/lit16 v1, v1, 0x2000

    goto :goto_0

    .line 284
    .end local v2    # "test":I
    :cond_2
    const-string/jumbo v3, "EvInputConnection"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "getCursorCapsMode reutn mode "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getDisableTextInput()Z
    .locals 1

    .prologue
    .line 457
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvInputConnection;->mDisableInputTextMode:Z

    return v0
.end method

.method public getEditable()Landroid/text/Editable;
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvInputConnection;->mTextView:Lcom/infraware/office/baseframe/EvBaseView;

    .line 41
    .local v0, "ev":Lcom/infraware/office/baseframe/EvBaseView;
    if-eqz v0, :cond_0

    .line 42
    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->getEditableText()Landroid/text/Editable;

    move-result-object v1

    .line 44
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getExtractedText(Landroid/view/inputmethod/ExtractedTextRequest;I)Landroid/view/inputmethod/ExtractedText;
    .locals 9
    .param p1, "request"    # Landroid/view/inputmethod/ExtractedTextRequest;
    .param p2, "flags"    # I

    .prologue
    const/4 v8, -0x1

    const/4 v7, 0x0

    .line 206
    const-string/jumbo v4, "EvInputConnection"

    const-string/jumbo v5, "============================================="

    invoke-static {v4, v5}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    const-string/jumbo v4, "EvInputConnection"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "getExtractedText flags = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    iget-object v4, p0, Lcom/infraware/office/baseframe/EvInputConnection;->mTextView:Lcom/infraware/office/baseframe/EvBaseView;

    if-eqz v4, :cond_2

    .line 210
    new-instance v1, Landroid/view/inputmethod/ExtractedText;

    invoke-direct {v1}, Landroid/view/inputmethod/ExtractedText;-><init>()V

    .line 211
    .local v1, "et":Landroid/view/inputmethod/ExtractedText;
    iput v7, v1, Landroid/view/inputmethod/ExtractedText;->flags:I

    .line 212
    iput v8, v1, Landroid/view/inputmethod/ExtractedText;->partialStartOffset:I

    .line 213
    iput v8, v1, Landroid/view/inputmethod/ExtractedText;->partialEndOffset:I

    .line 214
    iput v7, v1, Landroid/view/inputmethod/ExtractedText;->startOffset:I

    .line 215
    const-string/jumbo v4, ""

    iput-object v4, v1, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    .line 216
    iput v7, v1, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    .line 217
    iput v7, v1, Landroid/view/inputmethod/ExtractedText;->selectionEnd:I

    .line 219
    const/4 v2, 0x0

    .line 220
    .local v2, "selLen":I
    iget-object v4, p0, Lcom/infraware/office/baseframe/EvInputConnection;->mTextView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v4}, Lcom/infraware/office/baseframe/EvBaseView;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    .line 221
    .local v0, "content":Landroid/text/Editable;
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 235
    .local v3, "str":Ljava/lang/String;
    const/4 v2, 0x0

    .line 237
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    .line 238
    iput-object v3, v1, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    .line 239
    if-lez v2, :cond_1

    .line 240
    iput v7, v1, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    .line 241
    iget v4, v1, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    add-int/2addr v4, v2

    iput v4, v1, Landroid/view/inputmethod/ExtractedText;->selectionEnd:I

    .line 249
    :cond_0
    :goto_0
    const-string/jumbo v4, "EvInputConnection"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "getExtractedText length = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " text = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    .end local v0    # "content":Landroid/text/Editable;
    .end local v1    # "et":Landroid/view/inputmethod/ExtractedText;
    .end local v2    # "selLen":I
    .end local v3    # "str":Ljava/lang/String;
    :goto_1
    return-object v1

    .line 244
    .restart local v0    # "content":Landroid/text/Editable;
    .restart local v1    # "et":Landroid/view/inputmethod/ExtractedText;
    .restart local v2    # "selLen":I
    .restart local v3    # "str":Ljava/lang/String;
    :cond_1
    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v4

    iput v4, v1, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    .line 245
    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v4

    iput v4, v1, Landroid/view/inputmethod/ExtractedText;->selectionEnd:I

    goto :goto_0

    .line 252
    .end local v0    # "content":Landroid/text/Editable;
    .end local v1    # "et":Landroid/view/inputmethod/ExtractedText;
    .end local v2    # "selLen":I
    .end local v3    # "str":Ljava/lang/String;
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getTextAfterCursor(II)Ljava/lang/CharSequence;
    .locals 3
    .param p1, "length"    # I
    .param p2, "flags"    # I

    .prologue
    .line 334
    const-string/jumbo v0, "EvInputConnection"

    const-string/jumbo v1, "============================================="

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    const-string/jumbo v0, "EvInputConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "getTextAfterCursor "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->getTextAfterCursor(II)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getTextBeforeCursor(II)Ljava/lang/CharSequence;
    .locals 3
    .param p1, "length"    # I
    .param p2, "flags"    # I

    .prologue
    .line 304
    const-string/jumbo v0, "EvInputConnection"

    const-string/jumbo v1, "============================================="

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    const-string/jumbo v0, "EvInputConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "getTextBeforeCursor "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->getTextBeforeCursor(II)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public performContextMenuAction(I)Z
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 442
    const-string/jumbo v0, "EvInputConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "performContextMenuAction "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvInputConnection;->mTextView:Lcom/infraware/office/baseframe/EvBaseView;

    if-eqz v0, :cond_0

    .line 444
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvInputConnection;->mTextView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0, p1}, Lcom/infraware/office/baseframe/EvBaseView;->onTextContextMenuItem(I)Z

    .line 445
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public performEditorAction(I)Z
    .locals 1
    .param p1, "actionCode"    # I

    .prologue
    .line 50
    invoke-super {p0, p1}, Landroid/view/inputmethod/BaseInputConnection;->performEditorAction(I)Z

    move-result v0

    return v0
.end method

.method public reportFullscreenMode(Z)Z
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 56
    invoke-super {p0, p1}, Landroid/view/inputmethod/BaseInputConnection;->reportFullscreenMode(Z)Z

    move-result v0

    return v0
.end method

.method public setComposingRegion(II)Z
    .locals 9
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 61
    const-string/jumbo v6, "EvInputConnection"

    const-string/jumbo v7, "============================================="

    invoke-static {v6, v7}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    const-string/jumbo v6, "EvInputConnection"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "setComposingRegion "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " / "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvInputConnection;->getEditable()Landroid/text/Editable;

    move-result-object v2

    .line 65
    .local v2, "content":Landroid/text/Editable;
    if-eqz v2, :cond_5

    .line 66
    move v0, p1

    .line 67
    .local v0, "a":I
    move v1, p2

    .line 68
    .local v1, "b":I
    if-le v0, v1, :cond_0

    .line 69
    move v5, v0

    .line 70
    .local v5, "tmp":I
    move v0, v1

    .line 71
    move v1, v5

    .line 74
    .end local v5    # "tmp":I
    :cond_0
    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v3

    .line 75
    .local v3, "length":I
    if-gez v0, :cond_1

    const/4 v0, 0x0

    .line 76
    :cond_1
    if-gez v1, :cond_2

    const/4 v1, 0x0

    .line 77
    :cond_2
    if-le v0, v3, :cond_3

    move v0, v3

    .line 78
    :cond_3
    if-le v1, v3, :cond_4

    move v1, v3

    .line 79
    :cond_4
    invoke-interface {v2, v0, v1}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v4

    .line 80
    .local v4, "text":Ljava/lang/CharSequence;
    const-string/jumbo v6, "EvInputConnection"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "setPrevText "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    iget-object v6, p0, Lcom/infraware/office/baseframe/EvInputConnection;->mTextView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v6, v4}, Lcom/infraware/office/baseframe/EvBaseView;->setPrevText(Ljava/lang/CharSequence;)V

    .line 83
    .end local v0    # "a":I
    .end local v1    # "b":I
    .end local v3    # "length":I
    .end local v4    # "text":Ljava/lang/CharSequence;
    :cond_5
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->setComposingRegion(II)Z

    move-result v6

    return v6
.end method

.method public setComposingText(Ljava/lang/CharSequence;I)Z
    .locals 6
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "newCursorPosition"    # I

    .prologue
    .line 398
    iget-boolean v3, p0, Lcom/infraware/office/baseframe/EvInputConnection;->mDisableInputTextMode:Z

    if-eqz v3, :cond_0

    .line 399
    const/4 v3, 0x1

    .line 407
    :goto_0
    return v3

    .line 401
    :cond_0
    const-string/jumbo v3, "EvInputConnection"

    const-string/jumbo v4, "============================================="

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvInputConnection;->mTextView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/EvBaseView;->getEditableText()Landroid/text/Editable;

    move-result-object v1

    .line 403
    .local v1, "content":Landroid/text/Editable;
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 404
    .local v2, "str":Ljava/lang/String;
    const-string/jumbo v3, "EvInputConnection"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setComposingText newCursorPosition "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " text: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/infraware/office/util/EvUtil;->stringToHexa(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " content: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvInputConnection;->calcComposingLen()I

    move-result v0

    .line 406
    .local v0, "compLength":I
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvInputConnection;->mTextView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v3, p1, v0}, Lcom/infraware/office/baseframe/EvBaseView;->setComposingText(Ljava/lang/CharSequence;I)V

    .line 407
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->setComposingText(Ljava/lang/CharSequence;I)Z

    move-result v3

    goto :goto_0
.end method

.method public setDisableTextInput(Z)V
    .locals 0
    .param p1, "set"    # Z

    .prologue
    .line 452
    iput-boolean p1, p0, Lcom/infraware/office/baseframe/EvInputConnection;->mDisableInputTextMode:Z

    .line 453
    return-void
.end method

.method public setSelection(II)Z
    .locals 3
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 256
    const-string/jumbo v0, "EvInputConnection"

    const-string/jumbo v1, "============================================="

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    const-string/jumbo v0, "EvInputConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setSelection "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    if-ltz p1, :cond_0

    if-gez p2, :cond_1

    .line 259
    :cond_0
    const/4 v0, 0x1

    .line 260
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->setSelection(II)Z

    move-result v0

    goto :goto_0
.end method
