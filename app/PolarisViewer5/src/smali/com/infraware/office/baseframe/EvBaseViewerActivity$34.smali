.class Lcom/infraware/office/baseframe/EvBaseViewerActivity$34;
.super Ljava/lang/Object;
.source "EvBaseViewerActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 0

    .prologue
    .line 3666
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$34;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 3669
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$34;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintProgressPopup:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$2500(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 3670
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$34;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/16 v1, 0x1a

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->removeDialog(I)V

    .line 3671
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$34;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvPrintHelper:Lcom/infraware/office/baseframe/porting/EvPrintHelper;

    .line 3672
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ICancel()V

    .line 3673
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$34;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->RequestViewerPrint()V

    .line 3674
    return-void
.end method
