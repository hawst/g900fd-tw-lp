.class public Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;
.super Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;
.source "EvViewGestureProc.java"


# static fields
.field public static final FASTMOVE_EXTERNAL_POINT:I = 0xbb8


# instance fields
.field private final LOG_CAT:Ljava/lang/String;

.field protected mFastMovePoint:Landroid/graphics/Point;

.field protected mIsFastMove:Z

.field protected mIsFastMoveFlag:Z

.field prev_time:J


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "listener"    # Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0, p1, p2, p3}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;)V

    .line 24
    const-string/jumbo v0, "EvViewGestureProc"

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->LOG_CAT:Ljava/lang/String;

    .line 31
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mIsFastMove:Z

    .line 32
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mIsFastMoveFlag:Z

    .line 33
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mFastMovePoint:Landroid/graphics/Point;

    .line 34
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->prev_time:J

    .line 38
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->setIsLongpressEnabled(Z)V

    .line 41
    return-void
.end method


# virtual methods
.method public OnObjectPoints(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V
    .locals 1
    .param p1, "param"    # Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;

    .prologue
    .line 310
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->setObjectInfo(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V

    .line 312
    return-void
.end method

.method protected getScrollBarRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 8
    .param p1, "hor"    # Landroid/graphics/RectF;
    .param p2, "ver"    # Landroid/graphics/RectF;

    .prologue
    .line 280
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetScrollInfo_Editor()Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;

    move-result-object v7

    .line 281
    .local v7, "screenInfo":Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;
    iget v0, v7, Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;->nCurPosX:I

    int-to-float v3, v0

    iget v0, v7, Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;->nCurPosY:I

    int-to-float v4, v0

    iget v0, v7, Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;->nWidth:I

    int-to-float v5, v0

    iget v0, v7, Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;->nHeight:I

    int-to-float v6, v0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->getScrollBarRect(Landroid/graphics/RectF;Landroid/graphics/RectF;FFFF)V

    .line 282
    return-void
.end method

.method public onCancel(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 273
    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 186
    const/4 v0, 0x1

    return v0
.end method

.method public onDoubleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 15
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 192
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v12

    .line 193
    .local v12, "doc_type":I
    const/4 v0, 0x1

    if-eq v12, v0, :cond_0

    const/4 v0, 0x6

    if-ne v12, v0, :cond_1

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_1

    .line 197
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->getMemoCmdData()Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;

    move-result-object v14

    .line 198
    .local v14, "memo":Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    const/4 v0, -0x1

    iput v0, v14, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->nMemoId:I

    .line 199
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/16 v1, 0x9

    invoke-virtual {v0, v1, v14}, Lcom/infraware/office/evengine/EvInterface;->IMemoCommand(ILcom/infraware/office/evengine/EV$MEMO_CMD_DATA;)Z

    .line 200
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0x36

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    .line 205
    .end local v14    # "memo":Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v13

    .line 206
    .local v13, "info":Lcom/infraware/office/evengine/EV$CONFIG_INFO;
    iget v0, v13, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    iget v1, v13, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nFitToWidthZoomValue:I

    if-le v0, v1, :cond_3

    .line 207
    iget v2, v13, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nFitToWidthZoomValue:I

    .line 211
    .local v2, "nZoomRatio":I
    :goto_0
    const-string/jumbo v0, "EvViewGestureProc"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onDoubleTap ScreenZoomRatio ZoomRatio = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    iget v0, v13, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    if-eq v0, v2, :cond_2

    .line 213
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v10

    float-to-int v10, v10

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v11

    float-to-int v11, v11

    invoke-virtual/range {v0 .. v11}, Lcom/infraware/office/evengine/EvInterface;->ISetZoom(IIIIIIIIIII)V

    .line 215
    :cond_2
    const/4 v0, 0x1

    return v0

    .line 209
    .end local v2    # "nZoomRatio":I
    :cond_3
    iget v0, v13, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nFitToWidthZoomValue:I

    mul-int/lit8 v2, v0, 0x3

    .restart local v2    # "nZoomRatio":I
    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;Lcom/infraware/office/baseframe/EvBaseView;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "ebv"    # Lcom/infraware/office/baseframe/EvBaseView;

    .prologue
    .line 287
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mActionMode:I

    packed-switch v0, :pswitch_data_0

    .line 303
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1, p2}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->onDraw(Landroid/graphics/Canvas;Lcom/infraware/office/baseframe/EvBaseView;)V

    .line 304
    return-void

    .line 294
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 295
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->drawObjectProc(Landroid/graphics/Canvas;Lcom/infraware/office/baseframe/EvBaseView;)V

    goto :goto_0

    .line 287
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 8
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    const/4 v7, -0x1

    .line 262
    const/4 v0, 0x7

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mTouchStatus:I

    .line 264
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    long-to-int v5, v5

    invoke-static {p1, v7}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 265
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    long-to-int v5, v5

    invoke-static {p1, v7}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 266
    const/4 v0, 0x6

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mTouchStatus:I

    .line 268
    return-void
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 14
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 318
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mTouchStatus:I

    const/16 v3, 0xc

    if-ne v0, v3, :cond_2

    .line 319
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v13

    .line 320
    .local v13, "newDist":F
    const/high16 v0, 0x41700000    # 15.0f

    cmpl-float v0, v13, v0

    if-lez v0, :cond_1

    .line 321
    new-instance v12, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v3

    invoke-direct {v12, v0, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 322
    .local v12, "center":Landroid/graphics/PointF;
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mScaleTouchBeginScale:I

    int-to-float v0, v0

    iget v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mScaleTouchBeginSpace:F

    div-float v3, v13, v3

    mul-float/2addr v0, v3

    float-to-int v2, v0

    .line 323
    .local v2, "nScale":I
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nMinZoom:I

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v3

    iget v3, v3, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nMaxZoom:I

    invoke-virtual {p0, v2, v0, v3}, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->minMax(III)I

    move-result v2

    .line 325
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    if-eq v0, v2, :cond_0

    .line 326
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget v3, v12, Landroid/graphics/PointF;->x:F

    float-to-int v10, v3

    iget v3, v12, Landroid/graphics/PointF;->y:F

    float-to-int v11, v3

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v8, v1

    move v9, v7

    invoke-virtual/range {v0 .. v11}, Lcom/infraware/office/evengine/EvInterface;->ISetZoom(IIIIIIIIIII)V

    .line 328
    :cond_0
    const-string/jumbo v0, "EvViewGestureProc"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onScale nScale = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    iput v13, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mScaleTouchPreSpace:F

    .line 333
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mGyroTiltCurrentValue:I

    .line 337
    .end local v2    # "nScale":I
    .end local v12    # "center":Landroid/graphics/PointF;
    .end local v13    # "newDist":F
    :cond_1
    :goto_0
    return v7

    :cond_2
    move v7, v1

    goto :goto_0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 4
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    .line 343
    iget v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mTouchStatus:I

    if-ne v1, v0, :cond_0

    .line 344
    iput v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mTouchStatus:I

    .line 345
    :cond_0
    iget v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mTouchStatus:I

    if-ne v1, v2, :cond_1

    .line 346
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mScaleTouchPreCenter:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 347
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v1

    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mScaleTouchBeginSpace:F

    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mScaleTouchPreSpace:F

    .line 348
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v1

    iget v1, v1, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mScaleTouchBeginScale:I

    .line 349
    const/16 v1, 0xc

    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mTouchStatus:I

    .line 352
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 12
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v7, 0x2

    const/4 v1, 0x0

    .line 358
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mTouchStatus:I

    const/16 v2, 0xc

    if-ne v0, v2, :cond_0

    .line 360
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mScaleTouchBeginSpace:F

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mScaleTouchPreSpace:F

    .line 361
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mScaleTouchBeginScale:I

    .line 363
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mGyroTiltCurrentValue:I

    .line 365
    iput v7, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mTouchStatus:I

    .line 366
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v2

    iget v2, v2, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v8, v1

    move v9, v1

    move v10, v1

    move v11, v1

    invoke-virtual/range {v0 .. v11}, Lcom/infraware/office/evengine/EvInterface;->ISetZoom(IIIIIIIIIII)V

    .line 372
    const/4 v0, 0x5

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mTouchStatus:I

    .line 374
    :cond_0
    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 221
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v8

    .line 223
    .local v8, "doc_type":I
    const/4 v0, 0x1

    if-eq v8, v0, :cond_0

    const/4 v0, 0x6

    if-ne v8, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->IsViewerMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 225
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_1

    .line 227
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->getMemoCmdData()Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;

    move-result-object v10

    .line 228
    .local v10, "memo":Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v10}, Lcom/infraware/office/evengine/EvInterface;->IMemoCommand(ILcom/infraware/office/evengine/EV$MEMO_CMD_DATA;)Z

    .line 229
    iget-boolean v0, v10, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->bShow:Z

    if-eqz v0, :cond_1

    .line 231
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x7

    invoke-virtual {v0, v1, v10}, Lcom/infraware/office/evengine/EvInterface;->IMemoCommand(ILcom/infraware/office/evengine/EV$MEMO_CMD_DATA;)Z

    .line 232
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0xf

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    .line 233
    const/4 v0, 0x1

    .line 256
    .end local v10    # "memo":Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    :goto_0
    return v0

    .line 238
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/infraware/office/evengine/EvInterface;->IGetHyperLinkInfoEx(II)Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-result-object v9

    .line 239
    .local v9, "hyperlinkInfo":Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;
    const/4 v7, 0x0

    .line 240
    .local v7, "bHyperLinkMode":Z
    iget v0, v9, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->bUse:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 242
    iget-object v0, v9, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 243
    iget-object v0, v9, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 244
    const/4 v7, 0x1

    .line 247
    :cond_2
    const/4 v0, 0x1

    if-ne v7, v0, :cond_3

    .line 250
    const-string/jumbo v0, "EvViewGestureProc"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "hyperlinkInfo.szHyperLink = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v9, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    const-string/jumbo v0, "EvViewGestureProc"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "hyperlinkInfo.szHyperText = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v9, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/4 v1, 0x6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v6, v9, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    .line 256
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 255
    :cond_3
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/4 v1, 0x7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto :goto_1
.end method

.method public onTouchDown(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 47
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mTouchStatus:I

    const/4 v4, 0x6

    if-ne v0, v4, :cond_1

    .line 73
    :cond_0
    :goto_0
    return v8

    .line 50
    :cond_1
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->onTouchDown(Landroid/view/MotionEvent;)Z

    .line 52
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v2, v0

    .line 53
    .local v2, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v3, v0

    .line 55
    .local v3, "y":I
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mIsFastMove:Z

    .line 56
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mIsFastMoveFlag:Z

    .line 57
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mFastMovePoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 59
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mTouchStatus:I

    if-nez v0, :cond_0

    .line 60
    iput v8, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mTouchStatus:I

    .line 62
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mFastMovePoint:Landroid/graphics/Point;

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Point;->set(II)V

    .line 64
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_2

    .line 65
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    long-to-int v5, v4

    const/4 v4, -0x1

    invoke-static {p1, v4}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    move v4, v1

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 67
    :cond_2
    iput-boolean v8, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mIsFastMoveFlag:Z

    .line 68
    new-instance v7, Ljava/util/GregorianCalendar;

    invoke-direct {v7}, Ljava/util/GregorianCalendar;-><init>()V

    .line 69
    .local v7, "cal":Ljava/util/GregorianCalendar;
    invoke-virtual {v7}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->prev_time:J

    goto :goto_0
.end method

.method public onTouchDrag(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 21
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 126
    const-string/jumbo v1, "EvViewGestureProc"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "onSingleTouchDrag mTouchState ="

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mTouchStatus:I

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    move-object/from16 v0, p0

    iget v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mTouchStatus:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 129
    const/4 v1, 0x2

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mTouchStatus:I

    .line 131
    :cond_0
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v3, v1

    .line 132
    .local v3, "x":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v4, v1

    .line 134
    .local v4, "y":I
    move-object/from16 v0, p0

    iget v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mTouchStatus:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_2

    .line 135
    const/4 v1, 0x2

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mTouchStatus:I

    .line 136
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x0

    const/16 v5, 0x1b

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    long-to-int v6, v6

    const/4 v7, -0x1

    move-object/from16 v0, p2

    invoke-static {v0, v7}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v7

    invoke-virtual/range {v1 .. v7}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 180
    .end local p3    # "distanceX":F
    .end local p4    # "distanceY":F
    :cond_1
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 138
    .restart local p3    # "distanceX":F
    .restart local p4    # "distanceY":F
    :cond_2
    move-object/from16 v0, p0

    iget v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mTouchStatus:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_3

    move-object/from16 v0, p0

    iget v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mTouchStatus:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_1

    .line 140
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->isPenMode()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 172
    .end local p3    # "distanceX":F
    .end local p4    # "distanceY":F
    :cond_4
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mIsFastMove:Z

    if-eqz v1, :cond_c

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/4 v2, 0x2

    if-eq v1, v2, :cond_c

    .line 174
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v6, 0x1

    add-int/lit16 v7, v3, -0xbb8

    add-int/lit16 v8, v4, -0xbb8

    const/4 v9, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v1

    long-to-int v10, v1

    const/4 v1, -0x1

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v11

    invoke-virtual/range {v5 .. v11}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    goto :goto_0

    .line 142
    .restart local p3    # "distanceX":F
    .restart local p4    # "distanceY":F
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mIsFastMoveFlag:Z

    if-eqz v1, :cond_4

    .line 144
    new-instance v12, Ljava/util/GregorianCalendar;

    invoke-direct {v12}, Ljava/util/GregorianCalendar;-><init>()V

    .line 145
    .local v12, "cal":Ljava/util/GregorianCalendar;
    invoke-virtual {v12}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v13

    .line 147
    .local v13, "now_time":J
    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->prev_time:J

    sub-long v1, v13, v1

    long-to-double v15, v1

    .line 149
    .local v15, "time_diff":D
    const-wide/16 v17, 0x0

    .line 150
    .local v17, "vx":D
    invoke-static/range {p3 .. p3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/high16 v2, 0x40a00000    # 5.0f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_7

    .line 152
    const/4 v1, 0x0

    cmpg-float v1, p3, v1

    if-gez v1, :cond_6

    move/from16 v0, p3

    neg-float v0, v0

    move/from16 p3, v0

    .end local p3    # "distanceX":F
    :cond_6
    move/from16 v0, p3

    float-to-double v1, v0

    div-double v17, v1, v15

    .line 154
    :cond_7
    const-wide/16 v19, 0x0

    .line 155
    .local v19, "vy":D
    invoke-static/range {p4 .. p4}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/high16 v2, 0x40a00000    # 5.0f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_9

    .line 157
    const/4 v1, 0x0

    cmpg-float v1, p4, v1

    if-gez v1, :cond_8

    move/from16 v0, p4

    neg-float v0, v0

    move/from16 p4, v0

    .end local p4    # "distanceY":F
    :cond_8
    move/from16 v0, p4

    float-to-double v1, v0

    div-double v19, v1, v15

    .line 160
    :cond_9
    const-wide v1, 0x3fe3333333333333L    # 0.6

    cmpl-double v1, v17, v1

    if-gtz v1, :cond_a

    const-wide v1, 0x3fe3333333333333L    # 0.6

    cmpl-double v1, v19, v1

    if-lez v1, :cond_b

    .line 162
    :cond_a
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mIsFastMove:Z

    .line 163
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v6, 0x2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mFastMovePoint:Landroid/graphics/Point;

    iget v7, v1, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mFastMovePoint:Landroid/graphics/Point;

    iget v8, v1, Landroid/graphics/Point;->y:I

    const/4 v9, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v1

    long-to-int v10, v1

    const/4 v1, -0x1

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v11

    invoke-virtual/range {v5 .. v11}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 164
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mFastMovePoint:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    add-int/lit16 v7, v1, -0xbb8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mFastMovePoint:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    add-int/lit16 v8, v1, -0xbb8

    const/4 v9, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v1

    long-to-int v10, v1

    const/4 v1, -0x1

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v11

    invoke-virtual/range {v5 .. v11}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 169
    :goto_2
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mIsFastMoveFlag:Z

    goto/16 :goto_1

    .line 167
    :cond_b
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mIsFastMove:Z

    goto :goto_2

    .line 177
    .end local v12    # "cal":Ljava/util/GregorianCalendar;
    .end local v13    # "now_time":J
    .end local v15    # "time_diff":D
    .end local v17    # "vx":D
    .end local v19    # "vy":D
    :cond_c
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x1

    const/4 v5, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    long-to-int v6, v6

    const/4 v7, -0x1

    move-object/from16 v0, p2

    invoke-static {v0, v7}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v7

    invoke-virtual/range {v1 .. v7}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    goto/16 :goto_0
.end method

.method public onTouchUp(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v6, 0xa

    const/4 v8, -0x1

    const/4 v7, 0x1

    const/4 v1, 0x2

    const/4 v4, 0x0

    .line 78
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mActionMode:I

    const/16 v5, 0x9

    if-ne v0, v5, :cond_0

    .line 121
    :goto_0
    return v4

    .line 82
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v2, v0

    .line 83
    .local v2, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v3, v0

    .line 85
    .local v3, "y":I
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mTouchStatus:I

    if-ne v0, v7, :cond_3

    .line 87
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-ne v0, v7, :cond_2

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mFastMovePoint:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-gt v0, v6, :cond_1

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mFastMovePoint:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-le v0, v6, :cond_2

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mFastMovePoint:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v1, v2

    iget-object v5, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mFastMovePoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int/2addr v5, v3

    invoke-virtual {v0, v1, v5}, Lcom/infraware/office/evengine/EvInterface;->IFlick(II)V

    .line 90
    iput v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mTouchStatus:I

    move v4, v7

    .line 91
    goto :goto_0

    .line 93
    :cond_2
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mTouchStatus:I

    .line 96
    :cond_3
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mTouchStatus:I

    if-ne v0, v1, :cond_6

    .line 98
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mIsFastMove:Z

    if-eqz v0, :cond_5

    .line 100
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_4

    .line 101
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    add-int/lit16 v2, v2, -0xbb8

    add-int/lit16 v3, v3, -0xbb8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    .end local v2    # "x":I
    .end local v3    # "y":I
    move-result-wide v5

    long-to-int v5, v5

    invoke-static {p1, v8}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 118
    :cond_4
    :goto_1
    iput-boolean v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mIsFastMove:Z

    .line 120
    iput v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mTouchStatus:I

    move v4, v7

    .line 121
    goto :goto_0

    .line 105
    .restart local v2    # "x":I
    .restart local v3    # "y":I
    :cond_5
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_4

    .line 106
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    long-to-int v5, v5

    invoke-static {p1, v8}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    goto :goto_1

    .line 110
    :cond_6
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mTouchStatus:I

    const/4 v5, 0x6

    if-ne v0, v5, :cond_4

    .line 112
    iput-boolean v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mIsFastMove:Z

    .line 114
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_4

    .line 115
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    long-to-int v5, v5

    invoke-static {p1, v8}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    goto :goto_1
.end method
