.class public interface abstract Lcom/infraware/office/baseframe/EvBaseE$EV_WHEEL_BUTTON_TYPE;
.super Ljava/lang/Object;
.source "EvBaseE.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/EvBaseE;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EV_WHEEL_BUTTON_TYPE"
.end annotation


# static fields
.field public static final eEV_CUSTOM_BOTTOM:I = 0x3

.field public static final eEV_CUSTOM_LEFT:I = 0x4

.field public static final eEV_CUSTOM_RIGHT:I = 0x5

.field public static final eEV_CUSTOM_TOP:I = 0x2

.field public static final eEV_PANEL_FONT_SIZE:I = 0x7

.field public static final eEV_SHEET_NUMBER_DECIMAL_PLACES:I = 0x6

.field public static final eEV_TABLE_COL:I = 0x0

.field public static final eEV_TABLE_ROW:I = 0x1

.field public static final eEV_WHEEL_3DFORMAT_DEPTH:I = 0x20

.field public static final eEV_WHEEL_CELL_LINE_WIDTH:I = 0x19

.field public static final eEV_WHEEL_IMAGE_BRIGHT:I = 0xe

.field public static final eEV_WHEEL_IMAGE_CONTRAST:I = 0xf

.field public static final eEV_WHEEL_IMAGE_LINE_WIDTH:I = 0x1c

.field public static final eEV_WHEEL_LINE_LINE_WIDTH:I = 0x16

.field public static final eEV_WHEEL_LINE_SPACING:I = 0x8

.field public static final eEV_WHEEL_MARKER_THICKNESS:I = 0x1e

.field public static final eEV_WHEEL_NEON_SIZE:I = 0x12

.field public static final eEV_WHEEL_PARA_AFTER:I = 0xa

.field public static final eEV_WHEEL_PARA_BEFORE:I = 0x9

.field public static final eEV_WHEEL_PARA_FIRST_LINE:I = 0xd

.field public static final eEV_WHEEL_PARA_LEFT:I = 0xb

.field public static final eEV_WHEEL_PARA_RIGHT:I = 0xc

.field public static final eEV_WHEEL_PRINT_COPY_COUNT:I = 0x1d

.field public static final eEV_WHEEL_REFLECT_SIZE:I = 0x11

.field public static final eEV_WHEEL_REFLECT_TRANS:I = 0x10

.field public static final eEV_WHEEL_SHAPE_LINE_WIDTH:I = 0x17

.field public static final eEV_WHEEL_SHAPE_OPACITY:I = 0x1f

.field public static final eEV_WHEEL_SHEET_CELL_LINE_WIDTH:I = 0x1a

.field public static final eEV_WHEEL_TABLE_LINE_WIDTH:I = 0x1b

.field public static final eEV_WHEEL_VIDEO_LINE_WIDTH:I = 0x18
