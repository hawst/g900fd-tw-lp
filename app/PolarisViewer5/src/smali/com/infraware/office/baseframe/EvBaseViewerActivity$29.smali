.class Lcom/infraware/office/baseframe/EvBaseViewerActivity$29;
.super Ljava/lang/Object;
.source "EvBaseViewerActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onZoomMenu(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

.field final synthetic val$zoomValue:[Ljava/lang/Integer;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;[Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 3278
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$29;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iput-object p2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$29;->val$zoomValue:[Ljava/lang/Integer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 12
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v1, 0x0

    .line 3282
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$29;->val$zoomValue:[Ljava/lang/Integer;

    array-length v0, v0

    if-lt p2, v0, :cond_0

    .line 3297
    :goto_0
    return-void

    .line 3285
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$29;->val$zoomValue:[Ljava/lang/Integer;

    aget-object v0, v0, p2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 3286
    .local v2, "zoomLevel":I
    packed-switch v2, :pswitch_data_0

    .line 3294
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$29;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    move v11, v1

    invoke-virtual/range {v0 .. v11}, Lcom/infraware/office/evengine/EvInterface;->ISetZoom(IIIIIIIIIII)V

    goto :goto_0

    .line 3288
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$29;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->ISetViewMode(I)V

    goto :goto_0

    .line 3291
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$29;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->ISetViewMode(I)V

    goto :goto_0

    .line 3286
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
