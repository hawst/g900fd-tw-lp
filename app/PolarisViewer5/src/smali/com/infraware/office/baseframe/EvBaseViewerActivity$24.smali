.class Lcom/infraware/office/baseframe/EvBaseViewerActivity$24;
.super Ljava/lang/Object;
.source "EvBaseViewerActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/EvBaseViewerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 0

    .prologue
    .line 2850
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$24;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 2853
    packed-switch p2, :pswitch_data_0

    .line 2864
    :goto_0
    return-void

    .line 2856
    :pswitch_0
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$24;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$500(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/app/Activity;

    move-result-object v1

    const/16 v2, 0x131

    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$24;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v3, v3, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/common/config/RuntimeConfig;->setBooleanPreference(Landroid/content/Context;IZ)V

    .line 2857
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$24;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$24;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mUri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$1800(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$24;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMimeType:Ljava/lang/String;
    invoke-static {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$1900(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->exportEmail(Landroid/net/Uri;Ljava/lang/String;)V

    goto :goto_0

    .line 2861
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$24;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mbSendingEMail:Z

    goto :goto_0

    .line 2853
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
