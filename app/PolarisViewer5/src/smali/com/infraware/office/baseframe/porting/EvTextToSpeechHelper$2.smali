.class Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$2;
.super Landroid/os/Handler;
.source "EvTextToSpeechHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)V
    .locals 0

    .prologue
    .line 485
    iput-object p1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$2;->this$0:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 489
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$2;->this$0:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    # getter for: Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_bClose:Z
    invoke-static {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->access$100(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 526
    :cond_0
    :goto_0
    return-void

    .line 492
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 495
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$2;->this$0:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->doPlayHandler()V

    goto :goto_0

    .line 499
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$2;->this$0:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->doFirstPlayHandlerForCane()V

    goto :goto_0

    .line 503
    :pswitch_3
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$2;->this$0:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->speakGuide(Z)V

    goto :goto_0

    .line 508
    :pswitch_4
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$2;->this$0:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    # getter for: Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_nStartMode:I
    invoke-static {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->access$200(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)I

    move-result v0

    if-ne v0, v1, :cond_2

    .line 509
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$2;->this$0:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    # getter for: Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->access$300(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    const/16 v1, 0x100

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->IGetTextToSpeachString(I)V

    goto :goto_0

    .line 510
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$2;->this$0:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    # getter for: Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_strReadTTS:Ljava/lang/String;
    invoke-static {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->access$400(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 511
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$2;->this$0:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    # getter for: Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_TtsEngine:Landroid/speech/tts/TextToSpeech;
    invoke-static {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->access$600(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)Landroid/speech/tts/TextToSpeech;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$2;->this$0:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    # getter for: Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->m_strReadTTS:Ljava/lang/String;
    invoke-static {v1}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->access$400(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$2;->this$0:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    # getter for: Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->DUMMY_PARAMS:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->access$500(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    goto :goto_0

    .line 516
    :pswitch_5
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$2;->this$0:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    # getter for: Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->access$000(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getTTSToolbarPlayFlag()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 517
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$2;->this$0:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    # getter for: Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->access$300(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    const/16 v1, 0x4000

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->IGetTextToSpeachString(I)V

    goto :goto_0

    .line 519
    :cond_3
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$2;->this$0:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    # getter for: Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->access$000(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->enableTTSToolbar(Z)V

    goto :goto_0

    .line 523
    :pswitch_6
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$2;->this$0:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-virtual {v0, v3}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->speakGuide(Z)V

    goto :goto_0

    .line 492
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_6
    .end packed-switch
.end method
