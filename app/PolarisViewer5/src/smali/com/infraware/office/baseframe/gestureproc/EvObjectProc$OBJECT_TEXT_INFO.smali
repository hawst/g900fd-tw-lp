.class public Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;
.super Ljava/lang/Object;
.source "EvObjectProc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "OBJECT_TEXT_INFO"
.end annotation


# instance fields
.field public final MAX_OBJECT_TEXTRECT_COUNT:I

.field public barWidth:I

.field public endImageRect:Landroid/graphics/Rect;

.field public endNormalId:I

.field public endPressedId:I

.field public iconSize:Landroid/graphics/Point;

.field public mIsDrawBar:Z

.field public mTextRectInfos:[S

.field public nTextRectCount:I

.field public startImageRect:Landroid/graphics/Rect;

.field public startNormalId:I

.field public startPressedId:I

.field final synthetic this$0:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;


# direct methods
.method public constructor <init>(Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 203
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 205
    const/16 v0, 0x12c

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->MAX_OBJECT_TEXTRECT_COUNT:I

    .line 207
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->nTextRectCount:I

    .line 208
    const/16 v0, 0x4b0

    new-array v0, v0, [S

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->mTextRectInfos:[S

    .line 210
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startImageRect:Landroid/graphics/Rect;

    .line 211
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endImageRect:Landroid/graphics/Rect;

    .line 216
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->mIsDrawBar:Z

    .line 217
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->barWidth:I

    .line 218
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->iconSize:Landroid/graphics/Point;

    .line 219
    const v0, 0x7f020329

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startNormalId:I

    .line 220
    const v0, 0x7f02032a

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startPressedId:I

    .line 221
    const v0, 0x7f02031d

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endNormalId:I

    .line 222
    const v0, 0x7f02031e

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endPressedId:I

    return-void
.end method


# virtual methods
.method public SetInit()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 225
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startImageRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 226
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endImageRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 232
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->nTextRectCount:I

    .line 233
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->mTextRectInfos:[S

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([SS)V

    .line 235
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->mIsDrawBar:Z

    .line 236
    const v0, 0x7f020329

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startNormalId:I

    .line 237
    const v0, 0x7f02032a

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startPressedId:I

    .line 238
    const v0, 0x7f02031d

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endNormalId:I

    .line 239
    const v0, 0x7f02031e

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->endPressedId:I

    .line 240
    return-void
.end method
