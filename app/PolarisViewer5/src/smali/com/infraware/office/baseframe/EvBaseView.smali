.class public Lcom/infraware/office/baseframe/EvBaseView;
.super Landroid/view/SurfaceView;
.source "EvBaseView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements Lcom/infraware/office/evengine/E$EDVA_PAGE_INFO_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_DOCEXTENSION_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_DOCTYPE;
.implements Lcom/infraware/office/evengine/E$EV_GUI_EVENT;
.implements Lcom/infraware/office/evengine/E$EV_PDF_SMOOTH_DEBUG;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;,
        Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;,
        Lcom/infraware/office/baseframe/EvBaseView$Renderer;,
        Lcom/infraware/office/baseframe/EvBaseView$InputMethodState;
    }
.end annotation


# static fields
.field static final DEBUG_EXTRACT:Z = true

.field protected static final SCREEN_SIZE_MISMATCH:I

.field static final USE_RENDER_THREAD:Z


# instance fields
.field private final BUFFER_SIZE:I

.field final CMD_IME_ACTION_INIT_COMPOSING:Ljava/lang/String;

.field private final LOG_CAT:Ljava/lang/String;

.field protected bSurfaceCreated:Z

.field protected isInit:Z

.field private mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

.field protected mBitmap:Landroid/graphics/Bitmap;

.field protected mBookMarkPath:Ljava/lang/String;

.field public mCallBackID:I

.field protected mCanvas:Landroid/graphics/Canvas;

.field protected mChangeWatcher:Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;

.field protected mCompLen:I

.field protected mCompText:Ljava/lang/CharSequence;

.field protected mEditable:Landroid/text/Editable;

.field protected mEvEditModeType:I

.field protected mFilePath:Ljava/lang/String;

.field public mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

.field protected final mHandler:Landroid/os/Handler;

.field protected mHeight:I

.field protected mIc:Lcom/infraware/office/baseframe/EvInputConnection;

.field protected mInput:Landroid/text/method/KeyListener;

.field mInputMethodState:Lcom/infraware/office/baseframe/EvBaseView$InputMethodState;

.field protected mInterfaceHandleAddress:I

.field protected mOldHeight:I

.field protected mOldWidth:I

.field protected mOpenType:I

.field protected mPrintResultBitmap:Landroid/graphics/Bitmap;

.field protected mRemoveBColorE:I

.field protected mRemoveBColorS:I

.field protected mRenderThread:Lcom/infraware/office/baseframe/EvBaseView$Renderer;

.field private mRulerGuideLinePosition:I

.field private mStylusEventListener:Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;

.field protected mSurfaceHolder:Landroid/view/SurfaceHolder;

.field public mSyncObject:Ljava/lang/Object;

.field protected mTempPath:Ljava/lang/String;

.field protected mWidth:I

.field protected m_eNewTemplatePPT:I

.field mbCallOnSurfaceChanged:Z

.field protected mbChangekeyboardtoPanel:Z

.field protected mbComposing:Z

.field protected mbDeletedFirstPos:Z

.field protected mbEditableClear:Z

.field protected mbForceClear:Z

.field public mbKeyProc:Z

.field protected mbPrint:Z

.field protected mbRemoveBColor:Z

.field protected offset:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 146
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/infraware/office/baseframe/EvBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 147
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 150
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/infraware/office/baseframe/EvBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 151
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 154
    invoke-direct {p0, p1, p2, p3}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    const-string/jumbo v0, "EvBaseView"

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->LOG_CAT:Ljava/lang/String;

    .line 66
    const/16 v0, 0x64

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->BUFFER_SIZE:I

    .line 68
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 70
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->bSurfaceCreated:Z

    .line 75
    iput v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mEvEditModeType:I

    .line 76
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mFilePath:Ljava/lang/String;

    .line 77
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mTempPath:Ljava/lang/String;

    .line 78
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBookMarkPath:Ljava/lang/String;

    .line 79
    iput v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mOpenType:I

    .line 80
    iput v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->m_eNewTemplatePPT:I

    .line 81
    iput v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mOldWidth:I

    .line 82
    iput v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mOldHeight:I

    .line 83
    iput v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mWidth:I

    .line 84
    iput v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mHeight:I

    .line 85
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;

    .line 86
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mPrintResultBitmap:Landroid/graphics/Bitmap;

    .line 87
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    .line 88
    const v0, 0xffff

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mCallBackID:I

    .line 90
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mEditable:Landroid/text/Editable;

    .line 91
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mIc:Lcom/infraware/office/baseframe/EvInputConnection;

    .line 92
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInput:Landroid/text/method/KeyListener;

    .line 94
    iput-boolean v3, p0, Lcom/infraware/office/baseframe/EvBaseView;->mbComposing:Z

    .line 95
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mCompText:Ljava/lang/CharSequence;

    .line 96
    iput v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mCompLen:I

    .line 97
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mbEditableClear:Z

    .line 98
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mbKeyProc:Z

    .line 99
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mbForceClear:Z

    .line 109
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mbPrint:Z

    .line 110
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mbChangekeyboardtoPanel:Z

    .line 111
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mbRemoveBColor:Z

    .line 112
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mbDeletedFirstPos:Z

    .line 115
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->offset:[I

    .line 117
    iput v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInterfaceHandleAddress:I

    .line 118
    iput-boolean v3, p0, Lcom/infraware/office/baseframe/EvBaseView;->isInit:Z

    .line 120
    const-string/jumbo v0, "imeAction:initComposing"

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->CMD_IME_ACTION_INIT_COMPOSING:Ljava/lang/String;

    .line 122
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseView$1;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseView$1;-><init>(Lcom/infraware/office/baseframe/EvBaseView;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mHandler:Landroid/os/Handler;

    .line 140
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mRulerGuideLinePosition:I

    .line 1284
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mbCallOnSurfaceChanged:Z

    .line 1526
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mSyncObject:Ljava/lang/Object;

    .line 156
    invoke-static {v1}, Lcom/infraware/office/util/EvUtil;->setInputMethodManager(Landroid/view/inputmethod/InputMethodManager;)V

    .line 157
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 158
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 159
    iput-boolean v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->bSurfaceCreated:Z

    .line 161
    invoke-virtual {p0, v3}, Lcom/infraware/office/baseframe/EvBaseView;->setFocusable(Z)V

    .line 162
    invoke-virtual {p0, v3}, Lcom/infraware/office/baseframe/EvBaseView;->setFocusableInTouchMode(Z)V

    .line 163
    invoke-virtual {p0, v3}, Lcom/infraware/office/baseframe/EvBaseView;->setClickable(Z)V

    .line 164
    invoke-virtual {p0, v3}, Lcom/infraware/office/baseframe/EvBaseView;->setLongClickable(Z)V

    .line 166
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mEditable:Landroid/text/Editable;

    if-nez v0, :cond_0

    .line 167
    invoke-static {}, Landroid/text/Editable$Factory;->getInstance()Landroid/text/Editable$Factory;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/text/Editable$Factory;->newEditable(Ljava/lang/CharSequence;)Landroid/text/Editable;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mEditable:Landroid/text/Editable;

    .line 168
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mEditable:Landroid/text/Editable;

    invoke-static {v0, v2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 171
    :cond_0
    sget-object v0, Landroid/text/method/TextKeyListener$Capitalize;->SENTENCES:Landroid/text/method/TextKeyListener$Capitalize;

    invoke-static {v2, v0}, Landroid/text/method/TextKeyListener;->getInstance(ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/TextKeyListener;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInput:Landroid/text/method/KeyListener;

    .line 172
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->setChangeWatcher()V

    .line 173
    return-void
.end method

.method static synthetic access$100(Lcom/infraware/office/baseframe/EvBaseView;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/EvBaseView;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    return-object v0
.end method

.method private drawRulerGuideLine(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 592
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getHeight()I

    move-result v2

    .line 594
    .local v2, "height":I
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f02011c

    invoke-static {v5, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 596
    .local v0, "bmpGuideDot":Landroid/graphics/Bitmap;
    iget v5, p0, Lcom/infraware/office/baseframe/EvBaseView;->mRulerGuideLinePosition:I

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int v3, v5, v6

    .line 598
    .local v3, "xpos":I
    const/4 v4, 0x0

    .line 599
    .local v4, "ypos":I
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 600
    .local v1, "guide_height":I
    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_0

    .line 602
    int-to-float v5, v3

    int-to-float v6, v4

    const/4 v7, 0x0

    invoke-virtual {p1, v0, v5, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 600
    add-int/2addr v4, v1

    goto :goto_0

    .line 605
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 607
    return-void
.end method

.method private killPageInfo()V
    .locals 1

    .prologue
    .line 1352
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v0, :cond_0

    .line 1353
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->killPageInfoTimer()V

    .line 1354
    :cond_0
    return-void
.end method

.method private registerForStylusPenEvent()V
    .locals 3

    .prologue
    .line 1504
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1505
    .local v0, "observer":Landroid/view/ViewTreeObserver;
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mStylusEventListener:Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;

    if-eqz v1, :cond_0

    .line 1510
    :goto_0
    return-void

    .line 1508
    :cond_0
    new-instance v1, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;-><init>(Lcom/infraware/office/baseframe/EvBaseView;Lcom/infraware/office/baseframe/EvBaseView$1;)V

    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mStylusEventListener:Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;

    .line 1509
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mStylusEventListener:Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewTreeObserver;->addOnStylusButtonEventListener(Landroid/content/Context;Landroid/view/ViewTreeObserver$OnStylusButtonEventListener;)V

    goto :goto_0
.end method

.method private removeForStylusPenEvent()V
    .locals 2

    .prologue
    .line 1513
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1514
    .local v0, "observer":Landroid/view/ViewTreeObserver;
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mStylusEventListener:Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;

    if-nez v1, :cond_0

    .line 1519
    :goto_0
    return-void

    .line 1517
    :cond_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mStylusEventListener:Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnStylusButtonEventListener(Landroid/view/ViewTreeObserver$OnStylusButtonEventListener;)V

    .line 1518
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mStylusEventListener:Lcom/infraware/office/baseframe/EvBaseView$StylusEventListener;

    goto :goto_0
.end method


# virtual methods
.method public GetMultiInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;
    .locals 1

    .prologue
    .line 504
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->GetMultiInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    move-result-object v0

    return-object v0
.end method

.method public GetObjCtrlSelIndex()I
    .locals 1

    .prologue
    .line 514
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v0, :cond_0

    .line 515
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->GetObjCtrlSelIndex()I

    move-result v0

    .line 517
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public GetObjCtrlSize()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 508
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v0, :cond_0

    .line 509
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->GetObjCtrlSize()Landroid/graphics/Point;

    move-result-object v0

    .line 510
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public GetObjCtrlType()I
    .locals 1

    .prologue
    .line 490
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v0, :cond_0

    .line 491
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->GetObjCtrlType()I

    move-result v0

    .line 493
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public GetObjectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->GetObjectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-result-object v0

    return-object v0
.end method

.method public GetOpenType()I
    .locals 1

    .prologue
    .line 521
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mOpenType:I

    return v0
.end method

.method public GetRectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;
    .locals 1

    .prologue
    .line 500
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->GetRectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-result-object v0

    return-object v0
.end method

.method public OnLoadComplete()V
    .locals 1

    .prologue
    .line 1334
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v0, :cond_0

    .line 1335
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->OnLoadComplete()V

    .line 1336
    :cond_0
    return-void
.end method

.method public OnObjectPoints(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V
    .locals 1
    .param p1, "param"    # Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;

    .prologue
    .line 485
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v0, :cond_0

    .line 486
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->OnObjectPoints(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V

    .line 487
    :cond_0
    return-void
.end method

.method public OnSheetAutoFilterCellPos([I)V
    .locals 1
    .param p1, "nCellPos"    # [I

    .prologue
    .line 529
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v0, :cond_0

    .line 530
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->OnSheetAutoFilterCellPos([I)V

    .line 531
    :cond_0
    return-void
.end method

.method public OnSheetFormulaRangeRect(I[I[I)V
    .locals 1
    .param p1, "a_nCount"    # I
    .param p2, "a_aRangeRect"    # [I
    .param p3, "a_aCurRect"    # [I

    .prologue
    .line 534
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v0, :cond_0

    .line 535
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0, p1, p2, p3}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->OnSheetFormulaRangeRect(I[I[I)V

    .line 536
    :cond_0
    return-void
.end method

.method public SetOpenType(I)V
    .locals 0
    .param p1, "openType"    # I

    .prologue
    .line 525
    iput p1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mOpenType:I

    .line 526
    return-void
.end method

.method public beginBatchEdit()Z
    .locals 3

    .prologue
    .line 790
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInputMethodState:Lcom/infraware/office/baseframe/EvBaseView$InputMethodState;

    if-eqz v0, :cond_0

    .line 791
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInputMethodState:Lcom/infraware/office/baseframe/EvBaseView$InputMethodState;

    iget v1, v0, Lcom/infraware/office/baseframe/EvBaseView$InputMethodState;->mBatchEditNesting:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/infraware/office/baseframe/EvBaseView$InputMethodState;->mBatchEditNesting:I

    .line 792
    const-string/jumbo v0, "EvBaseView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "beginBatchEdit "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInputMethodState:Lcom/infraware/office/baseframe/EvBaseView$InputMethodState;

    iget v2, v2, Lcom/infraware/office/baseframe/EvBaseView$InputMethodState;->mBatchEditNesting:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 794
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public cancelGesture()V
    .locals 1

    .prologue
    .line 1154
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v0, :cond_0

    .line 1155
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->cancelGesture()V

    .line 1156
    :cond_0
    return-void
.end method

.method public checkComposing()Z
    .locals 3

    .prologue
    .line 1025
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getEditableText()Landroid/text/Editable;

    move-result-object v1

    .line 1026
    .local v1, "editable":Landroid/text/Editable;
    invoke-static {v1}, Lcom/infraware/office/baseframe/EvInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    move-result v0

    .line 1027
    .local v0, "candEnd":I
    const/4 v2, -0x1

    if-le v0, v2, :cond_0

    const/4 v2, 0x1

    .line 1028
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public clearEditable()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 996
    const-string/jumbo v2, "EvBaseView"

    const-string/jumbo v3, "============================================="

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 997
    const-string/jumbo v2, "EvBaseView"

    const-string/jumbo v3, "clearEditable"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    .line 1003
    .local v0, "evInterface":Lcom/infraware/office/evengine/EvInterface;
    const/4 v1, 0x0

    .line 1004
    .local v1, "strBefore":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1005
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/infraware/office/evengine/EvInterface;->IGetCaretBeforeString(I)Ljava/lang/String;

    move-result-object v1

    .line 1007
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/EvBaseView;->setEditableClear(Z)V

    .line 1008
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/method/TextKeyListener;->clear(Landroid/text/Editable;)V

    .line 1009
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_2

    .line 1010
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0xa

    if-eq v2, v3, :cond_1

    .line 1011
    sget-object v2, Landroid/text/method/TextKeyListener$Capitalize;->NONE:Landroid/text/method/TextKeyListener$Capitalize;

    invoke-static {v4, v2}, Landroid/text/method/TextKeyListener;->getInstance(ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/TextKeyListener;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInput:Landroid/text/method/KeyListener;

    .line 1018
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->setChangeWatcher()V

    .line 1019
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->initEditable()V

    .line 1021
    const-string/jumbo v2, "EvBaseView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "clearEditalbe selection= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseView;->mEditable:Landroid/text/Editable;

    invoke-static {v4}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " content= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseView;->mEditable:Landroid/text/Editable;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " hexa= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseView;->mEditable:Landroid/text/Editable;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/infraware/office/util/EvUtil;->stringToHexa(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1022
    return-void

    .line 1013
    :cond_1
    sget-object v2, Landroid/text/method/TextKeyListener$Capitalize;->SENTENCES:Landroid/text/method/TextKeyListener$Capitalize;

    invoke-static {v4, v2}, Landroid/text/method/TextKeyListener;->getInstance(ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/TextKeyListener;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInput:Landroid/text/method/KeyListener;

    goto :goto_0

    .line 1016
    :cond_2
    sget-object v2, Landroid/text/method/TextKeyListener$Capitalize;->SENTENCES:Landroid/text/method/TextKeyListener$Capitalize;

    invoke-static {v4, v2}, Landroid/text/method/TextKeyListener;->getInstance(ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/TextKeyListener;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInput:Landroid/text/method/KeyListener;

    goto :goto_0
.end method

.method public commitText(Ljava/lang/CharSequence;I)V
    .locals 4
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "nlen"    # I

    .prologue
    const/4 v3, 0x0

    .line 828
    const-string/jumbo v0, "EvBaseView"

    const-string/jumbo v1, "============================================="

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 829
    const-string/jumbo v0, "EvBaseView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "commitText : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " nlen : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 830
    invoke-virtual {p0, v3}, Lcom/infraware/office/baseframe/EvBaseView;->setKeyProc(Z)V

    .line 831
    invoke-virtual {p0, v3}, Lcom/infraware/office/baseframe/EvBaseView;->setForceClear(Z)V

    .line 832
    return-void
.end method

.method public destory()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 352
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseView;->killPageInfo()V

    .line 354
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mSyncObject:Ljava/lang/Object;

    monitor-enter v1

    .line 356
    :try_start_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 358
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;

    .line 360
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 363
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mPrintResultBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 364
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mPrintResultBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 365
    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mPrintResultBitmap:Landroid/graphics/Bitmap;

    .line 367
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v0, :cond_2

    .line 369
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->Gesturefinalize()V

    .line 370
    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    .line 372
    :cond_2
    return-void

    .line 360
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public dismissPageInfoTimer()V
    .locals 2

    .prologue
    .line 1340
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v0, :cond_0

    .line 1341
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mCallBackID:I

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->dismissPageInfoTimer(I)V

    .line 1342
    :cond_0
    const v0, 0xffff

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mCallBackID:I

    .line 1343
    return-void
.end method

.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1172
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1173
    const/4 v0, 0x1

    .line 1175
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/view/SurfaceView;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public drawAllContents()V
    .locals 6

    .prologue
    .line 447
    const/4 v0, 0x0

    .line 449
    .local v0, "canvas":Landroid/graphics/Canvas;
    :try_start_0
    iget-object v5, p0, Lcom/infraware/office/baseframe/EvBaseView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    monitor-enter v5
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 450
    :try_start_1
    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v4}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    .line 451
    if-eqz v0, :cond_0

    .line 452
    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseView;->onDraw(Landroid/graphics/Canvas;)V

    .line 453
    :cond_0
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 462
    if-eqz v0, :cond_1

    .line 465
    :try_start_2
    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v4, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalMonitorStateException; {:try_start_2 .. :try_end_2} :catch_3

    .line 477
    :cond_1
    :goto_0
    return-void

    .line 453
    :catchall_0
    move-exception v4

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v4
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 455
    :catch_0
    move-exception v1

    .line 456
    .local v1, "e":Ljava/lang/NullPointerException;
    :try_start_5
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 462
    if-eqz v0, :cond_1

    .line 465
    :try_start_6
    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v4, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V
    :try_end_6
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/IllegalMonitorStateException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 466
    :catch_1
    move-exception v2

    .line 468
    .local v2, "e2":Ljava/lang/IllegalStateException;
    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 466
    .end local v1    # "e":Ljava/lang/NullPointerException;
    .end local v2    # "e2":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v2

    .line 468
    .restart local v2    # "e2":Ljava/lang/IllegalStateException;
    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 471
    .end local v2    # "e2":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v3

    .line 472
    .local v3, "imse":Ljava/lang/IllegalMonitorStateException;
    invoke-virtual {v3}, Ljava/lang/IllegalMonitorStateException;->printStackTrace()V

    goto :goto_0

    .line 471
    .end local v3    # "imse":Ljava/lang/IllegalMonitorStateException;
    .restart local v1    # "e":Ljava/lang/NullPointerException;
    :catch_4
    move-exception v3

    .line 472
    .restart local v3    # "imse":Ljava/lang/IllegalMonitorStateException;
    invoke-virtual {v3}, Ljava/lang/IllegalMonitorStateException;->printStackTrace()V

    goto :goto_0

    .line 457
    .end local v1    # "e":Ljava/lang/NullPointerException;
    .end local v3    # "imse":Ljava/lang/IllegalMonitorStateException;
    :catch_5
    move-exception v1

    .line 459
    .local v1, "e":Ljava/lang/IllegalStateException;
    :try_start_7
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 462
    if-eqz v0, :cond_1

    .line 465
    :try_start_8
    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v4, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V
    :try_end_8
    .catch Ljava/lang/IllegalStateException; {:try_start_8 .. :try_end_8} :catch_6
    .catch Ljava/lang/IllegalMonitorStateException; {:try_start_8 .. :try_end_8} :catch_7

    goto :goto_0

    .line 466
    :catch_6
    move-exception v2

    .line 468
    .restart local v2    # "e2":Ljava/lang/IllegalStateException;
    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 471
    .end local v2    # "e2":Ljava/lang/IllegalStateException;
    :catch_7
    move-exception v3

    .line 472
    .restart local v3    # "imse":Ljava/lang/IllegalMonitorStateException;
    invoke-virtual {v3}, Ljava/lang/IllegalMonitorStateException;->printStackTrace()V

    goto :goto_0

    .line 462
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    .end local v3    # "imse":Ljava/lang/IllegalMonitorStateException;
    :catchall_1
    move-exception v4

    if-eqz v0, :cond_2

    .line 465
    :try_start_9
    iget-object v5, p0, Lcom/infraware/office/baseframe/EvBaseView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v5, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V
    :try_end_9
    .catch Ljava/lang/IllegalStateException; {:try_start_9 .. :try_end_9} :catch_8
    .catch Ljava/lang/IllegalMonitorStateException; {:try_start_9 .. :try_end_9} :catch_9

    .line 473
    :cond_2
    :goto_1
    throw v4

    .line 466
    :catch_8
    move-exception v2

    .line 468
    .restart local v2    # "e2":Ljava/lang/IllegalStateException;
    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1

    .line 471
    .end local v2    # "e2":Ljava/lang/IllegalStateException;
    :catch_9
    move-exception v3

    .line 472
    .restart local v3    # "imse":Ljava/lang/IllegalMonitorStateException;
    invoke-virtual {v3}, Ljava/lang/IllegalMonitorStateException;->printStackTrace()V

    goto :goto_1
.end method

.method public endBatchEdit()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 798
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInputMethodState:Lcom/infraware/office/baseframe/EvBaseView$InputMethodState;

    if-eqz v0, :cond_0

    .line 799
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInputMethodState:Lcom/infraware/office/baseframe/EvBaseView$InputMethodState;

    iget v1, v0, Lcom/infraware/office/baseframe/EvBaseView$InputMethodState;->mBatchEditNesting:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/infraware/office/baseframe/EvBaseView$InputMethodState;->mBatchEditNesting:I

    .line 800
    const-string/jumbo v0, "EvBaseView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "endBatchEdit "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInputMethodState:Lcom/infraware/office/baseframe/EvBaseView$InputMethodState;

    iget v2, v2, Lcom/infraware/office/baseframe/EvBaseView$InputMethodState;->mBatchEditNesting:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 802
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0, v3, v3}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->updateCaretPos(ZZ)V

    .line 803
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public ensureEndedBatchEdit()V
    .locals 2

    .prologue
    .line 954
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInputMethodState:Lcom/infraware/office/baseframe/EvBaseView$InputMethodState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInputMethodState:Lcom/infraware/office/baseframe/EvBaseView$InputMethodState;

    const/4 v1, 0x0

    iput v1, v0, Lcom/infraware/office/baseframe/EvBaseView$InputMethodState;->mBatchEditNesting:I

    .line 955
    :cond_0
    return-void
.end method

.method public fillcolorsurfaceview(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 480
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, p1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 482
    :cond_0
    return-void
.end method

.method public finishComposingText()Z
    .locals 2

    .prologue
    .line 807
    const-string/jumbo v0, "EvBaseView"

    const-string/jumbo v1, "============================================="

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 808
    const-string/jumbo v0, "EvBaseView"

    const-string/jumbo v1, "finishComposingText"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 810
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v0, :cond_0

    .line 811
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->finishComposingText()Z

    move-result v0

    .line 812
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getANormalizeString(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 10
    .param p1, "str"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v9, 0x0

    .line 897
    const-string/jumbo v6, "EvBaseView"

    const-string/jumbo v7, "============================================="

    invoke-static {v6, v7}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 898
    const-string/jumbo v6, "EvBaseView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "getANormalizeString str : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/infraware/office/util/EvUtil;->stringToHexa(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 899
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 901
    .local v4, "tstr1":Ljava/lang/String;
    move-object v5, v4

    .line 902
    .local v5, "tstr2":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    .line 903
    .local v2, "len":I
    if-nez v2, :cond_0

    const-string/jumbo v6, ""

    .line 930
    :goto_0
    return-object v6

    .line 905
    :cond_0
    add-int/lit8 v3, v2, -0x1

    .line 906
    .local v3, "t":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-gt v1, v3, :cond_2

    .line 907
    invoke-virtual {v4, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    .line 909
    .local v0, "code":I
    const/16 v6, 0x9

    if-ne v0, v6, :cond_6

    .line 910
    if-ne v1, v3, :cond_4

    const-string/jumbo v5, ""

    .line 918
    :cond_1
    :goto_2
    const-string/jumbo v6, "EvBaseView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "getANormalizeString t: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " tstr2: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v5}, Lcom/infraware/office/util/EvUtil;->stringToHexa(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 923
    .end local v0    # "code":I
    :cond_2
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_3

    .line 924
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    .line 925
    .restart local v0    # "code":I
    const/16 v6, 0xa

    if-ne v0, v6, :cond_3

    .line 926
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v9, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .end local v0    # "code":I
    :cond_3
    move-object v6, v5

    .line 930
    goto :goto_0

    .line 912
    .restart local v0    # "code":I
    :cond_4
    if-lez v1, :cond_1

    .line 913
    add-int/lit8 v6, v1, -0x1

    invoke-virtual {v4, v6}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    .line 914
    const/16 v6, 0x2e

    if-ne v0, v6, :cond_5

    const/4 v6, 0x1

    if-le v1, v6, :cond_5

    add-int/lit8 v6, v1, -0x2

    invoke-virtual {v4, v9, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    .line 915
    :cond_5
    add-int/lit8 v6, v1, -0x1

    invoke-virtual {v4, v9, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    .line 906
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public getActionMode()I
    .locals 1

    .prologue
    .line 1328
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v0, :cond_0

    .line 1329
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getActionMode()I

    move-result v0

    .line 1330
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBNormalizeString(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 9
    .param p1, "str"    # Ljava/lang/CharSequence;

    .prologue
    .line 873
    const-string/jumbo v6, "EvBaseView"

    const-string/jumbo v7, "============================================="

    invoke-static {v6, v7}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 874
    const-string/jumbo v6, "EvBaseView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "getBNormalizeString str : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/infraware/office/util/EvUtil;->stringToHexa(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 875
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 877
    .local v4, "tstr1":Ljava/lang/String;
    move-object v5, v4

    .line 878
    .local v5, "tstr2":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    .line 879
    .local v2, "len":I
    if-nez v2, :cond_0

    const-string/jumbo v6, ""

    .line 893
    :goto_0
    return-object v6

    .line 881
    :cond_0
    add-int/lit8 v3, v2, -0x1

    .line 882
    .local v3, "t":I
    move v1, v3

    .local v1, "i":I
    :goto_1
    if-ltz v1, :cond_1

    .line 883
    invoke-virtual {v4, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    .line 885
    .local v0, "code":I
    const/16 v6, 0x9

    if-ne v0, v6, :cond_3

    .line 886
    if-ne v1, v3, :cond_2

    const-string/jumbo v5, ""

    .line 888
    :goto_2
    const-string/jumbo v6, "EvBaseView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "getBNormalizeString t: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " tstr2: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v5}, Lcom/infraware/office/util/EvUtil;->stringToHexa(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .end local v0    # "code":I
    :cond_1
    move-object v6, v5

    .line 893
    goto :goto_0

    .line 887
    .restart local v0    # "code":I
    :cond_2
    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    .line 882
    :cond_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_1
.end method

.method public getBatchEditNesting()I
    .locals 1

    .prologue
    .line 949
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInputMethodState:Lcom/infraware/office/baseframe/EvBaseView$InputMethodState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInputMethodState:Lcom/infraware/office/baseframe/EvBaseView$InputMethodState;

    iget v0, v0, Lcom/infraware/office/baseframe/EvBaseView$InputMethodState;->mBatchEditNesting:I

    .line 950
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBitmap(II)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "aScreenWidth"    # I
    .param p2, "aScreenHeight"    # I

    .prologue
    .line 376
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseView;->mSyncObject:Ljava/lang/Object;

    monitor-enter v3

    .line 378
    :try_start_0
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-ne v2, p1, :cond_0

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-eq v2, p2, :cond_4

    .line 380
    :cond_0
    iget v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mWidth:I

    if-ne v2, p1, :cond_1

    iget v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mHeight:I

    if-eq v2, p2, :cond_2

    .line 382
    :cond_1
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 383
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x0

    iput v2, v1, Landroid/os/Message;->what:I

    .line 384
    iput p1, v1, Landroid/os/Message;->arg1:I

    iput p1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mWidth:I

    .line 385
    iput p2, v1, Landroid/os/Message;->arg2:I

    iput p2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mHeight:I

    .line 386
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 389
    .end local v1    # "msg":Landroid/os/Message;
    :cond_2
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_3

    .line 390
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 391
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 395
    :cond_3
    :try_start_1
    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 405
    :try_start_2
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v2, :cond_4

    .line 406
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700a7

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v2, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 411
    :cond_4
    :goto_0
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 412
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;

    :goto_1
    return-object v2

    .line 397
    :catch_0
    move-exception v0

    .line 398
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    const/4 v2, 0x0

    :try_start_3
    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 405
    :try_start_4
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v2, :cond_4

    .line 406
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700a7

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v2, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 411
    .end local v0    # "e":Ljava/lang/OutOfMemoryError;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v2

    .line 400
    :catch_1
    move-exception v0

    .line 401
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const/4 v2, 0x0

    :try_start_5
    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;

    .line 402
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 405
    :try_start_6
    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v4, :cond_5

    .line 406
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0700a7

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    :cond_5
    monitor-exit v3

    goto :goto_1

    .line 405
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_1
    move-exception v2

    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v4, :cond_6

    .line 406
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0700a7

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    :cond_6
    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method public getBitmapHeight()I
    .locals 1

    .prologue
    .line 1533
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mHeight:I

    return v0
.end method

.method public getBitmapWidth()I
    .locals 1

    .prologue
    .line 1529
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mWidth:I

    return v0
.end method

.method public getDisableTextInput()Z
    .locals 1

    .prologue
    .line 1391
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mIc:Lcom/infraware/office/baseframe/EvInputConnection;

    if-eqz v0, :cond_0

    .line 1392
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mIc:Lcom/infraware/office/baseframe/EvInputConnection;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvInputConnection;->getDisableTextInput()Z

    move-result v0

    .line 1393
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getEditableText()Landroid/text/Editable;
    .locals 1

    .prologue
    .line 851
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mEditable:Landroid/text/Editable;

    return-object v0
.end method

.method public getInitBufferSize()I
    .locals 1

    .prologue
    .line 855
    const/16 v0, 0x64

    return v0
.end method

.method public getInputConnection()Landroid/view/inputmethod/InputConnection;
    .locals 1

    .prologue
    .line 768
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mIc:Lcom/infraware/office/baseframe/EvInputConnection;

    return-object v0
.end method

.method public getInterfaceHandleAddress()I
    .locals 1

    .prologue
    .line 1368
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInterfaceHandleAddress:I

    return v0
.end method

.method public final getKeyListener()Landroid/text/method/KeyListener;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInput:Landroid/text/method/KeyListener;

    return-object v0
.end method

.method public getPrintBitmap(II)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "aScreenWidth"    # I
    .param p2, "aScreenHeight"    # I

    .prologue
    const/4 v2, 0x0

    const v5, 0x7f0700a7

    const/4 v4, 0x0

    .line 418
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mPrintResultBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mPrintResultBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-ne v1, p1, :cond_0

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mPrintResultBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-eq v1, p2, :cond_2

    .line 420
    :cond_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mPrintResultBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 421
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mPrintResultBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 422
    iput-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mPrintResultBitmap:Landroid/graphics/Bitmap;

    .line 427
    :cond_1
    :try_start_0
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mPrintResultBitmap:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 433
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mPrintResultBitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_2

    .line 434
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 439
    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mPrintResultBitmap:Landroid/graphics/Bitmap;

    return-object v1

    .line 429
    :catch_0
    move-exception v0

    .line 430
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    const/4 v1, 0x0

    :try_start_1
    iput-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mPrintResultBitmap:Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 433
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mPrintResultBitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_2

    .line 434
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 433
    .end local v0    # "e":Ljava/lang/OutOfMemoryError;
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mPrintResultBitmap:Landroid/graphics/Bitmap;

    if-nez v2, :cond_3

    .line 434
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :cond_3
    throw v1
.end method

.method public getPrivateBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 1523
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public initEditable()V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 963
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    .line 964
    .local v0, "evInterface":Lcom/infraware/office/evengine/EvInterface;
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getInitBufferSize()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/infraware/office/evengine/EvInterface;->IGetCaretBeforeString(I)Ljava/lang/String;

    move-result-object v2

    .line 965
    .local v2, "strBefore":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getInitBufferSize()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/infraware/office/evengine/EvInterface;->IGetCaretAfterString(I)Ljava/lang/String;

    move-result-object v1

    .line 967
    .local v1, "strAfter":Ljava/lang/String;
    const-string/jumbo v3, ""

    .line 968
    .local v3, "strContent":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 969
    const-string/jumbo v4, "EvBaseView"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "before= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 970
    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/EvBaseView;->getBNormalizeString(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 971
    const-string/jumbo v4, "EvBaseView"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "before= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 972
    move-object v3, v2

    .line 974
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_1

    .line 975
    const-string/jumbo v4, "EvBaseView"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "after= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 976
    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/EvBaseView;->getANormalizeString(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 977
    const-string/jumbo v4, "EvBaseView"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "after= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 978
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 981
    :cond_1
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/infraware/office/baseframe/EvBaseView;->setEditableClear(Z)V

    .line 982
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_4

    .line 983
    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseView;->mEditable:Landroid/text/Editable;

    iget-object v6, p0, Lcom/infraware/office/baseframe/EvBaseView;->mEditable:Landroid/text/Editable;

    invoke-interface {v6}, Landroid/text/Editable;->length()I

    move-result v6

    invoke-interface {v4, v5, v6, v3}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 984
    iget-object v6, p0, Lcom/infraware/office/baseframe/EvBaseView;->mEditable:Landroid/text/Editable;

    if-nez v2, :cond_3

    move v4, v5

    :goto_0
    invoke-static {v6, v4}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 990
    :goto_1
    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v4, :cond_2

    .line 991
    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v6, p0, Lcom/infraware/office/baseframe/EvBaseView;->mEditable:Landroid/text/Editable;

    invoke-static {v6}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v6

    invoke-virtual {v4, v6}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->onUpdateSelection(I)V

    .line 992
    :cond_2
    invoke-virtual {p0, v5}, Lcom/infraware/office/baseframe/EvBaseView;->setEditableClear(Z)V

    .line 993
    return-void

    .line 984
    :cond_3
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    goto :goto_0

    .line 987
    :cond_4
    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseView;->mEditable:Landroid/text/Editable;

    invoke-interface {v4}, Landroid/text/Editable;->clear()V

    .line 988
    iget-object v4, p0, Lcom/infraware/office/baseframe/EvBaseView;->mEditable:Landroid/text/Editable;

    invoke-static {v4, v5}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    goto :goto_1
.end method

.method public isComposing()Z
    .locals 1

    .prologue
    .line 843
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mbComposing:Z

    return v0
.end method

.method public isEditableClear()Z
    .locals 1

    .prologue
    .line 847
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mbEditableClear:Z

    return v0
.end method

.method public isForceClear()Z
    .locals 1

    .prologue
    .line 839
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mbForceClear:Z

    return v0
.end method

.method public isKeyProc()Z
    .locals 1

    .prologue
    .line 835
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mbKeyProc:Z

    return v0
.end method

.method public isPrintMode()Z
    .locals 1

    .prologue
    .line 265
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mbPrint:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 1397
    invoke-super {p0}, Landroid/view/SurfaceView;->onAttachedToWindow()V

    .line 1398
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string/jumbo v1, "com.sec.feature.hovering_ui"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1399
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    instance-of v0, v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    instance-of v0, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    if-eqz v0, :cond_1

    .line 1403
    :cond_0
    :goto_0
    return-void

    .line 1401
    :cond_1
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseView;->registerForStylusPenEvent()V

    goto :goto_0
.end method

.method public onBackPressed()Z
    .locals 1

    .prologue
    .line 1376
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v0, :cond_0

    .line 1377
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->onBackPressed()Z

    move-result v0

    .line 1379
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCheckIsTextEditor()Z
    .locals 1

    .prologue
    .line 742
    const/4 v0, 0x1

    return v0
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 2
    .param p1, "outAttrs"    # Landroid/view/inputmethod/EditorInfo;

    .prologue
    .line 747
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mIc:Lcom/infraware/office/baseframe/EvInputConnection;

    if-nez v0, :cond_0

    .line 748
    new-instance v0, Lcom/infraware/office/baseframe/EvInputConnection;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvInputConnection;-><init>(Lcom/infraware/office/baseframe/EvBaseView;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mIc:Lcom/infraware/office/baseframe/EvInputConnection;

    .line 751
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInputMethodState:Lcom/infraware/office/baseframe/EvBaseView$InputMethodState;

    if-nez v0, :cond_1

    .line 752
    new-instance v0, Lcom/infraware/office/baseframe/EvBaseView$InputMethodState;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvBaseView$InputMethodState;-><init>(Lcom/infraware/office/baseframe/EvBaseView;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInputMethodState:Lcom/infraware/office/baseframe/EvBaseView$InputMethodState;

    .line 755
    :cond_1
    invoke-static {}, Lcom/infraware/common/util/Utils;->isICS()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 756
    iget v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const v1, 0x52000001

    or-int/2addr v0, v1

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 760
    :goto_0
    const/16 v0, 0x4001

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    .line 761
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->initialSelStart:I

    .line 762
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->initialSelEnd:I

    .line 764
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mIc:Lcom/infraware/office/baseframe/EvInputConnection;

    return-object v0

    .line 758
    :cond_2
    iget v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const v1, -0x2fffffff

    or-int/2addr v0, v1

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 1407
    invoke-super {p0}, Landroid/view/SurfaceView;->onDetachedFromWindow()V

    .line 1408
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string/jumbo v1, "com.sec.feature.hovering_ui"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1409
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    instance-of v0, v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    instance-of v0, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    if-eqz v0, :cond_1

    .line 1413
    :cond_0
    :goto_0
    return-void

    .line 1411
    :cond_1
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseView;->removeForStylusPenEvent()V

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 541
    :try_start_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mSyncObject:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 544
    :try_start_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 552
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mEvEditModeType:I

    const/4 v2, 0x5

    if-ne v0, v2, :cond_0

    .line 572
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 574
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 576
    :try_start_2
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->setPageInfoTimer()V

    .line 577
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v0, :cond_2

    .line 578
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0, p1, p0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->onDraw(Landroid/graphics/Canvas;Lcom/infraware/office/baseframe/EvBaseView;)V

    .line 580
    :cond_2
    iget v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mRulerGuideLinePosition:I

    if-lez v0, :cond_3

    .line 582
    invoke-direct {p0, p1}, Lcom/infraware/office/baseframe/EvBaseView;->drawRulerGuideLine(Landroid/graphics/Canvas;)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0

    .line 589
    :cond_3
    :goto_0
    return-void

    .line 574
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_0

    .line 586
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 612
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 613
    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 670
    :cond_0
    :goto_0
    return v1

    .line 616
    :cond_1
    const-string/jumbo v2, "EvBaseView"

    const-string/jumbo v3, "============================================="

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 617
    const-string/jumbo v2, "EvBaseView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onKeyDown keyCode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/EvBaseView;->setKeyProc(Z)V

    .line 619
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/EvBaseView;->setForceClear(Z)V

    .line 621
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_2

    .line 622
    sparse-switch p1, :sswitch_data_0

    .line 650
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInput:Landroid/text/method/KeyListener;

    if-eqz v2, :cond_5

    .line 651
    const-string/jumbo v2, "EvBaseView"

    const-string/jumbo v3, "mInput.onKeyDown4"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 652
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInput:Landroid/text/method/KeyListener;

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getEditableText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v2, p0, v3, p1, p2}, Landroid/text/method/KeyListener;->onKeyDown(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 653
    const-string/jumbo v2, "EvBaseView"

    const-string/jumbo v3, "mInput.onKeyDown5"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 654
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v2, :cond_0

    .line 655
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 658
    :pswitch_1
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v2, p1, p2}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->onKeyDown(ILandroid/view/KeyEvent;)Z

    goto :goto_0

    .line 624
    :sswitch_0
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getEditableText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    .line 625
    .local v0, "selEnd":I
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getEditableText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    if-lez v2, :cond_3

    if-lez v0, :cond_3

    .line 626
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInput:Landroid/text/method/KeyListener;

    if-eqz v2, :cond_4

    .line 627
    const-string/jumbo v2, "EvBaseView"

    const-string/jumbo v3, "mInput.onKeyDown1"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInput:Landroid/text/method/KeyListener;

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getEditableText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v2, p0, v3, p1, p2}, Landroid/text/method/KeyListener;->onKeyDown(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 629
    const-string/jumbo v2, "EvBaseView"

    const-string/jumbo v3, "mInput.onKeyDown2"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 635
    :cond_3
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v1, :cond_4

    .line 636
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v1, p1, p2}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto/16 :goto_0

    .line 638
    :cond_4
    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto/16 :goto_0

    .line 642
    .end local v0    # "selEnd":I
    :sswitch_1
    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/EvBaseView;->setForceClear(Z)V

    goto :goto_1

    .line 666
    :cond_5
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v1, :cond_6

    .line 667
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v1, p1, p2}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto/16 :goto_0

    .line 670
    :cond_6
    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto/16 :goto_0

    .line 622
    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_1
        0x14 -> :sswitch_1
        0x42 -> :sswitch_1
        0x43 -> :sswitch_0
    .end sparse-switch

    .line 655
    :pswitch_data_0
    .packed-switch 0x39
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x1

    .line 675
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 676
    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v2

    .line 737
    :goto_0
    return v2

    .line 679
    :cond_0
    const-string/jumbo v2, "EvBaseView"

    const-string/jumbo v4, "============================================="

    invoke-static {v2, v4}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    const-string/jumbo v2, "EvBaseView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "onKeyUp keyCode: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 681
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v3, :cond_1

    .line 682
    sparse-switch p1, :sswitch_data_0

    .line 711
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInput:Landroid/text/method/KeyListener;

    if-eqz v2, :cond_5

    .line 712
    const-string/jumbo v2, "EvBaseView"

    const-string/jumbo v4, "mInput.onKeyUp3"

    invoke-static {v2, v4}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 713
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInput:Landroid/text/method/KeyListener;

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getEditableText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v2, p0, v4, p1, p2}, Landroid/text/method/KeyListener;->onKeyUp(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 714
    const-string/jumbo v2, "EvBaseView"

    const-string/jumbo v4, "mInput.onKeyUp4"

    invoke-static {v2, v4}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 715
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v2, :cond_2

    .line 716
    packed-switch p1, :pswitch_data_0

    :cond_2
    :goto_2
    :pswitch_0
    move v2, v3

    .line 722
    goto :goto_0

    .line 685
    :sswitch_0
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    .line 686
    .local v0, "mab":Lcom/infraware/office/actionbar/MainActionBar;
    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->getActionBarMenu()Landroid/widget/ImageButton;

    move-result-object v1

    .line 687
    .local v1, "v":Landroid/view/View;
    if-eqz v1, :cond_1

    .line 688
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, Lcom/infraware/office/actionbar/MainActionBar;->actionBarPopupWindow(ILandroid/view/View;)Z

    goto :goto_1

    .line 692
    .end local v0    # "mab":Lcom/infraware/office/actionbar/MainActionBar;
    .end local v1    # "v":Landroid/view/View;
    :sswitch_1
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getEditableText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    if-lez v2, :cond_3

    .line 693
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInput:Landroid/text/method/KeyListener;

    if-eqz v2, :cond_4

    .line 694
    const-string/jumbo v2, "EvBaseView"

    const-string/jumbo v4, "mInput.onKeyUp1"

    invoke-static {v2, v4}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 695
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInput:Landroid/text/method/KeyListener;

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getEditableText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v2, p0, v4, p1, p2}, Landroid/text/method/KeyListener;->onKeyUp(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 696
    const-string/jumbo v2, "EvBaseView"

    const-string/jumbo v4, "mInput.onKeyUp2"

    invoke-static {v2, v4}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v3

    .line 697
    goto/16 :goto_0

    .line 702
    :cond_3
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v2, :cond_4

    .line 703
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v2, p1, p2}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->onKeyUp(ILandroid/view/KeyEvent;)Z

    .line 705
    :cond_4
    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v2

    goto/16 :goto_0

    .line 719
    :pswitch_1
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v2, p1, p2}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->onKeyUp(ILandroid/view/KeyEvent;)Z

    goto :goto_2

    .line 726
    :cond_5
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->isForceClear()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 727
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->clearEditable()V

    .line 730
    :cond_6
    invoke-virtual {p0, v6}, Lcom/infraware/office/baseframe/EvBaseView;->setKeyProc(Z)V

    .line 731
    invoke-virtual {p0, v6}, Lcom/infraware/office/baseframe/EvBaseView;->setForceClear(Z)V

    .line 733
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v2, :cond_7

    .line 734
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v2, p1, p2}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_7

    move v2, v3

    .line 735
    goto/16 :goto_0

    .line 737
    :cond_7
    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v2

    goto/16 :goto_0

    .line 682
    nop

    :sswitch_data_0
    .sparse-switch
        0x43 -> :sswitch_1
        0x52 -> :sswitch_0
    .end sparse-switch

    .line 716
    :pswitch_data_0
    .packed-switch 0x39
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onScreenOffset()[I
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->offset:[I

    return-object v0
.end method

.method public onShowIme(Z)V
    .locals 1
    .param p1, "bShow"    # Z

    .prologue
    .line 772
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v0, :cond_0

    .line 773
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->onShowIme(Z)Z

    .line 774
    :cond_0
    return-void
.end method

.method public onTextContextMenuItem(I)Z
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 297
    const/4 v0, 0x0

    return v0
.end method

.method public openDocEx(Ljava/lang/String;)V
    .locals 0
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 1167
    return-void
.end method

.method public restartInput()V
    .locals 3

    .prologue
    .line 816
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 817
    .local v0, "Imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    .line 818
    :cond_0
    return-void
.end method

.method public setActionMode(I)V
    .locals 1
    .param p1, "aMode"    # I

    .prologue
    .line 1323
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v0, :cond_0

    .line 1324
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->setActionMode(I)V

    .line 1325
    :cond_0
    return-void
.end method

.method public setChangeWatcher()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 189
    const/16 v0, 0x64

    .line 190
    .local v0, "PRIORITY":I
    iget-object v6, p0, Lcom/infraware/office/baseframe/EvBaseView;->mEditable:Landroid/text/Editable;

    invoke-interface {v6}, Landroid/text/Editable;->length()I

    move-result v4

    .line 191
    .local v4, "textLength":I
    iget-object v6, p0, Lcom/infraware/office/baseframe/EvBaseView;->mEditable:Landroid/text/Editable;

    instance-of v6, v6, Landroid/text/Spannable;

    if-eqz v6, :cond_2

    .line 192
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseView;->mEditable:Landroid/text/Editable;

    .line 194
    .local v3, "sp":Landroid/text/Spannable;
    invoke-interface {v3}, Landroid/text/Spannable;->length()I

    move-result v6

    const-class v7, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;

    invoke-interface {v3, v8, v6, v7}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;

    .line 195
    .local v5, "watchers":[Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;
    array-length v1, v5

    .line 196
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 197
    aget-object v6, v5, v2

    invoke-interface {v3, v6}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 196
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 199
    :cond_0
    iget-object v6, p0, Lcom/infraware/office/baseframe/EvBaseView;->mChangeWatcher:Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;

    if-nez v6, :cond_1

    .line 200
    new-instance v6, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;

    const/4 v7, 0x0

    invoke-direct {v6, p0, v7}, Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;-><init>(Lcom/infraware/office/baseframe/EvBaseView;Lcom/infraware/office/baseframe/EvBaseView$1;)V

    iput-object v6, p0, Lcom/infraware/office/baseframe/EvBaseView;->mChangeWatcher:Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;

    .line 202
    :cond_1
    iget-object v6, p0, Lcom/infraware/office/baseframe/EvBaseView;->mChangeWatcher:Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;

    const v7, 0x640012

    invoke-interface {v3, v6, v8, v4, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 205
    iget-object v6, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInput:Landroid/text/method/KeyListener;

    if-eqz v6, :cond_2

    .line 206
    iget-object v6, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInput:Landroid/text/method/KeyListener;

    const/16 v7, 0x12

    invoke-interface {v3, v6, v8, v4, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 209
    .end local v1    # "count":I
    .end local v2    # "i":I
    .end local v3    # "sp":Landroid/text/Spannable;
    .end local v5    # "watchers":[Lcom/infraware/office/baseframe/EvBaseView$EvChangeWatcher;
    :cond_2
    return-void
.end method

.method public setChangekeyboradtoPanel(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 268
    iput-boolean p1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mbChangekeyboardtoPanel:Z

    .line 269
    return-void
.end method

.method public setComposing(Z)V
    .locals 3
    .param p1, "bComposing"    # Z

    .prologue
    .line 944
    const-string/jumbo v0, "EvBaseView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setComposing : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 945
    iput-boolean p1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mbComposing:Z

    .line 946
    return-void
.end method

.method public setComposingText(Ljava/lang/CharSequence;I)V
    .locals 4
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "nlen"    # I

    .prologue
    const/4 v3, 0x0

    .line 821
    const-string/jumbo v0, "EvBaseView"

    const-string/jumbo v1, "============================================="

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 822
    const-string/jumbo v0, "EvBaseView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setComposingText : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " nlen : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 823
    invoke-virtual {p0, v3}, Lcom/infraware/office/baseframe/EvBaseView;->setKeyProc(Z)V

    .line 824
    invoke-virtual {p0, v3}, Lcom/infraware/office/baseframe/EvBaseView;->setForceClear(Z)V

    .line 825
    return-void
.end method

.method public setDisableTextInput(Z)V
    .locals 1
    .param p1, "set"    # Z

    .prologue
    .line 1384
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mIc:Lcom/infraware/office/baseframe/EvInputConnection;

    if-nez v0, :cond_0

    .line 1385
    new-instance v0, Lcom/infraware/office/baseframe/EvInputConnection;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/EvInputConnection;-><init>(Lcom/infraware/office/baseframe/EvBaseView;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mIc:Lcom/infraware/office/baseframe/EvInputConnection;

    .line 1386
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mIc:Lcom/infraware/office/baseframe/EvInputConnection;

    invoke-virtual {v0, p1}, Lcom/infraware/office/baseframe/EvInputConnection;->setDisableTextInput(Z)V

    .line 1387
    return-void
.end method

.method public setEditableClear(Z)V
    .locals 3
    .param p1, "bClear"    # Z

    .prologue
    .line 958
    const-string/jumbo v0, "EvBaseView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setEditableClear : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 959
    iput-boolean p1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mbEditableClear:Z

    .line 960
    return-void
.end method

.method public setEditableText(Ljava/lang/CharSequence;)V
    .locals 4
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v3, 0x0

    .line 865
    const-string/jumbo v0, "EvBaseView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setEditableText : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 866
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseView;->setEditableClear(Z)V

    .line 867
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mEditable:Landroid/text/Editable;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mEditable:Landroid/text/Editable;

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-interface {v0, v3, v1, p1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 869
    invoke-virtual {p0, v3}, Lcom/infraware/office/baseframe/EvBaseView;->setEditableClear(Z)V

    .line 870
    return-void
.end method

.method public setForceClear(Z)V
    .locals 3
    .param p1, "bForceClear"    # Z

    .prologue
    .line 939
    const-string/jumbo v0, "EvBaseView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setForceClear : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 940
    iput-boolean p1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mbForceClear:Z

    .line 941
    return-void
.end method

.method public setInit(Landroid/app/Activity;ILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "mode_type"    # I
    .param p3, "openFilePath"    # Ljava/lang/String;
    .param p4, "docExtType"    # I
    .param p5, "OpenType"    # I
    .param p6, "aNewTemplatePPT"    # I
    .param p7, "tempPath"    # Ljava/lang/String;
    .param p8, "bookMarkPath"    # Ljava/lang/String;

    .prologue
    .line 251
    iput-object p3, p0, Lcom/infraware/office/baseframe/EvBaseView;->mFilePath:Ljava/lang/String;

    .line 252
    iput-object p7, p0, Lcom/infraware/office/baseframe/EvBaseView;->mTempPath:Ljava/lang/String;

    .line 253
    iput-object p8, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBookMarkPath:Ljava/lang/String;

    .line 254
    iput p5, p0, Lcom/infraware/office/baseframe/EvBaseView;->mOpenType:I

    .line 255
    iput p6, p0, Lcom/infraware/office/baseframe/EvBaseView;->m_eNewTemplatePPT:I

    move-object v0, p1

    .line 256
    check-cast v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object v0, p1

    .line 257
    check-cast v0, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    invoke-virtual {p0, p1, v0, p4, p2}, Lcom/infraware/office/baseframe/EvBaseView;->setModeType(Landroid/app/Activity;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;II)V

    .line 258
    return-void
.end method

.method public setInterfaceHandleAddress(I)V
    .locals 0
    .param p1, "address"    # I

    .prologue
    .line 1364
    iput p1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mInterfaceHandleAddress:I

    .line 1365
    return-void
.end method

.method public setIsinit(Z)V
    .locals 0
    .param p1, "init"    # Z

    .prologue
    .line 1372
    iput-boolean p1, p0, Lcom/infraware/office/baseframe/EvBaseView;->isInit:Z

    .line 1373
    return-void
.end method

.method public setKeyProc(Z)V
    .locals 3
    .param p1, "bKeyProc"    # Z

    .prologue
    .line 934
    const-string/jumbo v0, "EvBaseView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setKeyProc : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 935
    iput-boolean p1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mbKeyProc:Z

    .line 936
    return-void
.end method

.method public setModeType(Landroid/app/Activity;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;II)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "callbackListener"    # Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;
    .param p3, "docExtType"    # I
    .param p4, "mode_type"    # I

    .prologue
    .line 301
    iput p4, p0, Lcom/infraware/office/baseframe/EvBaseView;->mEvEditModeType:I

    .line 302
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->Gesturefinalize()V

    .line 304
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->clearEditable()V

    .line 307
    :cond_0
    sparse-switch p4, :sswitch_data_0

    .line 335
    const/4 v0, 0x5

    if-eq p3, v0, :cond_1

    const/16 v0, 0x14

    if-eq p3, v0, :cond_1

    const/16 v0, 0x26

    if-ne p3, v0, :cond_3

    .line 336
    :cond_1
    const-string/jumbo v0, "EvBaseView"

    const-string/jumbo v1, "setModeTypeChanged = eEV_EDITOR_SHEET_VIEWMODE"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    new-instance v0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;

    invoke-direct {v0, p1, p0, p2}, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    .line 349
    :goto_0
    return-void

    .line 311
    :sswitch_0
    const-string/jumbo v0, "EvBaseView"

    const-string/jumbo v1, "setModeTypeChanged = eEV_EDITOR_WORD"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    new-instance v0, Lcom/infraware/office/baseframe/gestureproc/EvWordGestureProc;

    invoke-direct {v0, p1, p0, p2}, Lcom/infraware/office/baseframe/gestureproc/EvWordGestureProc;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    goto :goto_0

    .line 315
    :sswitch_1
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HWP_EDIT_SUPPORT()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 317
    const-string/jumbo v0, "EvBaseView"

    const-string/jumbo v1, "setModeTypeChanged = eEV_EDITOR_WORD HWP"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    new-instance v0, Lcom/infraware/office/baseframe/gestureproc/EvWordGestureProc;

    invoke-direct {v0, p1, p0, p2}, Lcom/infraware/office/baseframe/gestureproc/EvWordGestureProc;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    goto :goto_0

    .line 322
    :cond_2
    const-string/jumbo v0, "EvBaseView"

    const-string/jumbo v1, "setModeTypeChanged = eEV_EDITOR_VIEWMODE"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    new-instance v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;

    invoke-direct {v0, p1, p0, p2}, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    goto :goto_0

    .line 327
    :sswitch_2
    const-string/jumbo v0, "EvBaseView"

    const-string/jumbo v1, "setModeTypeChanged = eEV_EDITOR_SHEET"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    new-instance v0, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;

    invoke-direct {v0, p1, p0, p2}, Lcom/infraware/office/baseframe/gestureproc/EvSheetGestureProc;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    goto :goto_0

    .line 331
    :sswitch_3
    const-string/jumbo v0, "EvBaseView"

    const-string/jumbo v1, "setModeTypeChanged = eEV_EDITOR_PPT"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    new-instance v0, Lcom/infraware/office/baseframe/gestureproc/EvPptGestureProc;

    invoke-direct {v0, p1, p0, p2}, Lcom/infraware/office/baseframe/gestureproc/EvPptGestureProc;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    goto :goto_0

    .line 339
    :cond_3
    const/4 v0, 0x6

    if-ne p3, v0, :cond_4

    .line 340
    const-string/jumbo v0, "EvBaseView"

    const-string/jumbo v1, "setModeTypeChanged = eEV_EDITOR_PDF_VIEWMODE"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    new-instance v0, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;

    invoke-direct {v0, p1, p0, p2}, Lcom/infraware/office/baseframe/gestureproc/EvPDFGestureProc;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    goto :goto_0

    .line 344
    :cond_4
    const-string/jumbo v0, "EvBaseView"

    const-string/jumbo v1, "setModeTypeChanged = eEV_EDITOR_VIEWMODE"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    new-instance v0, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;

    invoke-direct {v0, p1, p0, p2}, Lcom/infraware/office/baseframe/gestureproc/EvViewGestureProc;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    goto :goto_0

    .line 307
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x6 -> :sswitch_1
        0x25 -> :sswitch_0
    .end sparse-switch
.end method

.method public setPageInfoText()V
    .locals 1

    .prologue
    .line 1357
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v0, :cond_0

    .line 1358
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->setPageInfoText()V

    .line 1359
    :cond_0
    return-void
.end method

.method public setPageInfoTimer()V
    .locals 2

    .prologue
    .line 1346
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v0, :cond_0

    .line 1347
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mCallBackID:I

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->setPageInfoTimer(I)V

    .line 1348
    :cond_0
    const v0, 0xffff

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mCallBackID:I

    .line 1349
    return-void
.end method

.method public setPrevText(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 859
    const-string/jumbo v0, "EvBaseView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setPrevText : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 860
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v0, :cond_0

    .line 861
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->setPrevText(Ljava/lang/CharSequence;)V

    .line 862
    :cond_0
    return-void
.end method

.method public setPrintMode(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 261
    iput-boolean p1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mbPrint:Z

    .line 262
    return-void
.end method

.method public setRulerGuideLinePosition(I)V
    .locals 0
    .param p1, "x"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mRulerGuideLinePosition:I

    .line 143
    return-void
.end method

.method public setScreenOffset(II)V
    .locals 2
    .param p1, "offsetX"    # I
    .param p2, "offsetY"    # I

    .prologue
    .line 184
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->offset:[I

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 185
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->offset:[I

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 186
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 15
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 1181
    iget-boolean v3, p0, Lcom/infraware/office/baseframe/EvBaseView;->bSurfaceCreated:Z

    if-nez v3, :cond_6

    .line 1182
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/infraware/office/baseframe/EvBaseView;->bSurfaceCreated:Z

    .line 1183
    const-string/jumbo v3, "EvBaseView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "surfaceChanged width="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " height="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1185
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v2

    .line 1186
    .local v2, "evInterface":Lcom/infraware/office/evengine/EvInterface;
    iget-boolean v3, p0, Lcom/infraware/office/baseframe/EvBaseView;->isInit:Z

    if-nez v3, :cond_0

    .line 1188
    move/from16 v0, p3

    move/from16 v1, p4

    invoke-virtual {v2, v0, v1}, Lcom/infraware/office/evengine/EvInterface;->IInitialize(II)V

    .line 1189
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/infraware/office/baseframe/EvBaseView;->isInit:Z

    .line 1193
    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetProperties()Lcom/infraware/office/evengine/EV$PROPERTIES;

    move-result-object v3

    if-nez v3, :cond_2

    .line 1282
    .end local v2    # "evInterface":Lcom/infraware/office/evengine/EvInterface;
    :cond_1
    :goto_0
    return-void

    .line 1197
    .restart local v2    # "evInterface":Lcom/infraware/office/evengine/EvInterface;
    :cond_2
    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetProperties()Lcom/infraware/office/evengine/EV$PROPERTIES;

    move-result-object v13

    .line 1198
    .local v13, "properties":Lcom/infraware/office/evengine/EV$PROPERTIES;
    const/4 v3, 0x0

    iput v3, v13, Lcom/infraware/office/evengine/EV$PROPERTIES;->byPageEdgeWidth:I

    .line 1200
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    .line 1201
    .local v14, "resources":Landroid/content/res/Resources;
    const v3, 0x7f050028

    invoke-virtual {v14, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    iput v3, v13, Lcom/infraware/office/evengine/EV$PROPERTIES;->dwBgColor:I

    .line 1202
    const v3, 0x7f050029

    invoke-virtual {v14, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    iput v3, v13, Lcom/infraware/office/evengine/EV$PROPERTIES;->dwEdgeColor:I

    .line 1203
    const v3, 0x7f05002a

    invoke-virtual {v14, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    iput v3, v13, Lcom/infraware/office/evengine/EV$PROPERTIES;->dwOutlineColor:I

    .line 1205
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseView;->mFilePath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/infraware/office/evengine/EvInterface;->getDocFileExtentionType(Ljava/lang/String;)I

    move-result v11

    .line 1206
    .local v11, "fileExtType":I
    const/4 v3, 0x6

    if-ne v3, v11, :cond_5

    .line 1207
    const/16 v3, 0x14

    iput v3, v13, Lcom/infraware/office/evengine/EV$PROPERTIES;->nMakeThumbnailPages:I

    .line 1211
    :goto_1
    invoke-virtual {v2, v13}, Lcom/infraware/office/evengine/EvInterface;->ISetProperties(Lcom/infraware/office/evengine/EV$PROPERTIES;)V

    .line 1213
    const/4 v8, 0x0

    .line 1214
    .local v8, "bLandScape":I
    invoke-virtual {v14}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    .line 1215
    const/4 v8, 0x1

    .line 1217
    :cond_3
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3}, Lcom/infraware/common/util/Utils;->getCurrentLocaleType(Landroid/content/res/Resources;)I

    move-result v7

    .line 1218
    .local v7, "locale":I
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v12

    .line 1262
    .local v12, "mab":Lcom/infraware/office/actionbar/MainActionBar;
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseView;->mFilePath:Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v9, p0, Lcom/infraware/office/baseframe/EvBaseView;->mTempPath:Ljava/lang/String;

    iget-object v10, p0, Lcom/infraware/office/baseframe/EvBaseView;->mBookMarkPath:Ljava/lang/String;

    move/from16 v4, p3

    move/from16 v5, p4

    invoke-virtual/range {v2 .. v10}, Lcom/infraware/office/evengine/EvInterface;->IOpen(Ljava/lang/String;IIIIILjava/lang/String;Ljava/lang/String;)V

    .line 1281
    .end local v2    # "evInterface":Lcom/infraware/office/evengine/EvInterface;
    .end local v7    # "locale":I
    .end local v8    # "bLandScape":I
    .end local v11    # "fileExtType":I
    .end local v12    # "mab":Lcom/infraware/office/actionbar/MainActionBar;
    .end local v13    # "properties":Lcom/infraware/office/evengine/EV$PROPERTIES;
    .end local v14    # "resources":Landroid/content/res/Resources;
    :cond_4
    :goto_2
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->drawAllContents()V

    goto :goto_0

    .line 1209
    .restart local v2    # "evInterface":Lcom/infraware/office/evengine/EvInterface;
    .restart local v11    # "fileExtType":I
    .restart local v13    # "properties":Lcom/infraware/office/evengine/EV$PROPERTIES;
    .restart local v14    # "resources":Landroid/content/res/Resources;
    :cond_5
    const/16 v3, 0x64

    iput v3, v13, Lcom/infraware/office/evengine/EV$PROPERTIES;->nMakeThumbnailPages:I

    goto :goto_1

    .line 1266
    .end local v2    # "evInterface":Lcom/infraware/office/evengine/EvInterface;
    .end local v11    # "fileExtType":I
    .end local v13    # "properties":Lcom/infraware/office/evengine/EV$PROPERTIES;
    .end local v14    # "resources":Landroid/content/res/Resources;
    :cond_6
    const-string/jumbo v3, "EvBaseView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "surfaceChanged width="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " height="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1267
    iget v3, p0, Lcom/infraware/office/baseframe/EvBaseView;->mWidth:I

    move/from16 v0, p3

    if-ne v3, v0, :cond_7

    iget v3, p0, Lcom/infraware/office/baseframe/EvBaseView;->mHeight:I

    move/from16 v0, p4

    if-ne v3, v0, :cond_7

    iget-boolean v3, p0, Lcom/infraware/office/baseframe/EvBaseView;->mbCallOnSurfaceChanged:Z

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    .line 1268
    :cond_7
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/infraware/office/baseframe/EvBaseView;->mbCallOnSurfaceChanged:Z

    .line 1269
    iget v3, p0, Lcom/infraware/office/baseframe/EvBaseView;->mWidth:I

    iput v3, p0, Lcom/infraware/office/baseframe/EvBaseView;->mOldWidth:I

    .line 1270
    iget v3, p0, Lcom/infraware/office/baseframe/EvBaseView;->mHeight:I

    iput v3, p0, Lcom/infraware/office/baseframe/EvBaseView;->mOldHeight:I

    .line 1272
    move/from16 v0, p3

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mWidth:I

    move/from16 v0, p4

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mHeight:I

    .line 1274
    iget-boolean v3, p0, Lcom/infraware/office/baseframe/EvBaseView;->mbPrint:Z

    if-nez v3, :cond_4

    .line 1275
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v3, :cond_4

    .line 1276
    iget-object v3, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget v4, p0, Lcom/infraware/office/baseframe/EvBaseView;->mOldWidth:I

    iget v5, p0, Lcom/infraware/office/baseframe/EvBaseView;->mOldHeight:I

    iget v6, p0, Lcom/infraware/office/baseframe/EvBaseView;->mWidth:I

    iget v9, p0, Lcom/infraware/office/baseframe/EvBaseView;->mHeight:I

    invoke-virtual {v3, v4, v5, v6, v9}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->OnSurfaceChanged(IIII)V

    goto :goto_2
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 1287
    const-string/jumbo v0, "EvBaseView"

    const-string/jumbo v1, "surfaceCreated"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1288
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mWidth:I

    .line 1289
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mHeight:I

    .line 1291
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1292
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/EvBaseView;->mbCallOnSurfaceChanged:Z

    .line 1299
    :cond_0
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 1303
    const-string/jumbo v0, "EvBaseView"

    const-string/jumbo v1, "surfaceDestroyed"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1305
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseView;->killPageInfo()V

    .line 1320
    return-void
.end method

.method public updateCaretPos()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 777
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->ensureEndedBatchEdit()V

    .line 778
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eqz v1, :cond_0

    .line 779
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v1, v2, v2}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->updateCaretPos(ZZ)V

    .line 781
    :cond_0
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "FT03"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 782
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 783
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_1

    .line 784
    const-string/jumbo v1, "imeAction:initComposing"

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->sendAppPrivateCommand(Landroid/view/View;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 787
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_1
    return-void
.end method
