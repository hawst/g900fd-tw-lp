.class public Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;
.super Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;
.source "EvEditGestureProc.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_VIDEO_STATUS;


# instance fields
.field private final BUFFER_SIZE:I

.field private final LOG_CAT:Ljava/lang/String;

.field private bSpellWrong:Z

.field hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

.field index:I

.field private mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

.field protected mFastMovePoint:Landroid/graphics/Point;

.field protected mIsFastMove:Z

.field protected mIsFastMoveFlag:Z

.field public mIsMultiSelectionMode:Z

.field private mPenDragTime:[I

.field private mPenDragX:[I

.field private mPenDragY:[I

.field private mPenPressure:[I

.field protected mPrevText:Ljava/lang/String;

.field mSavedCaretPos:I

.field private mScaleDownSpanX:F

.field private mScaleDownTime:J

.field private mWrongSpell:Ljava/lang/String;

.field private mWrongSpellList:[Ljava/lang/String;

.field private m_bAltPressed:Z

.field public m_bSheetPopupMenu:Z

.field private m_bShiftPressed:Z

.field public m_oHandler:Landroid/os/Handler;

.field protected mbPrevComposing:Z

.field protected mbPrevComposingOrg:Z

.field prev_time:J

.field sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;)V
    .locals 6
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "listener"    # Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    .prologue
    const-wide/16 v4, 0x0

    const/16 v3, 0x100

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 101
    invoke-direct {p0, p1, p2, p3}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;)V

    .line 45
    const-string/jumbo v0, "EvEditGestureProc"

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->LOG_CAT:Ljava/lang/String;

    .line 48
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->index:I

    .line 49
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mSavedCaretPos:I

    .line 51
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mbPrevComposing:Z

    .line 52
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mbPrevComposingOrg:Z

    .line 54
    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPrevText:Ljava/lang/String;

    .line 55
    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mWrongSpell:Ljava/lang/String;

    .line 56
    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mWrongSpellList:[Ljava/lang/String;

    .line 58
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->bSpellWrong:Z

    .line 61
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsFastMove:Z

    .line 62
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsFastMoveFlag:Z

    .line 63
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mFastMovePoint:Landroid/graphics/Point;

    .line 64
    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->m_oHandler:Landroid/os/Handler;

    .line 66
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsMultiSelectionMode:Z

    .line 68
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenDragX:[I

    .line 69
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenDragY:[I

    .line 71
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenDragTime:[I

    .line 72
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenPressure:[I

    .line 74
    iput-wide v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mScaleDownTime:J

    .line 75
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mScaleDownSpanX:F

    .line 78
    iput-wide v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->prev_time:J

    .line 80
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->m_bShiftPressed:Z

    .line 82
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->m_bAltPressed:Z

    .line 84
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->m_bSheetPopupMenu:Z

    .line 86
    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    .line 87
    iput-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    .line 1103
    iput v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->BUFFER_SIZE:I

    .line 102
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->setIsLongpressEnabled(Z)V

    .line 104
    const-string/jumbo v0, "EvEditGestureProc"

    const-string/jumbo v1, "EvEditGestureProc(EvGestureCallback listener, View view)"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    check-cast p1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 111
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->setDocType(I)V

    .line 112
    return-void
.end method

.method private onHIDAction(IIIIII)V
    .locals 7
    .param p1, "action"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "modifiers"    # I
    .param p5, "time"    # I
    .param p6, "pressLevel"    # I

    .prologue
    .line 1534
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    .line 1535
    return-void
.end method

.method private onPointerEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1529
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPointerView:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->onTouch(Landroid/view/MotionEvent;)V

    .line 1530
    return-void
.end method

.method private onSetPenPosition([I[I[I[II)V
    .locals 6
    .param p1, "x"    # [I
    .param p2, "y"    # [I
    .param p3, "time"    # [I
    .param p4, "pressLevel"    # [I
    .param p5, "count"    # I

    .prologue
    .line 1539
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvInterface;->ISetPenPosition([I[I[I[II)V

    .line 1540
    return-void
.end method


# virtual methods
.method public Gesturefinalize()V
    .locals 0

    .prologue
    .line 97
    invoke-super {p0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->Gesturefinalize()V

    .line 98
    return-void
.end method

.method public GetMultiInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getMultiInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    move-result-object v0

    return-object v0
.end method

.method public GetObjCtrlSize()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectSize()Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method public GetObjCtrlType()I
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v0

    return v0
.end method

.method public GetRectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-result-object v0

    return-object v0
.end method

.method public OnObjectPoints(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V
    .locals 1
    .param p1, "param"    # Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->setObjectInfo(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V

    .line 120
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTransformInfo:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTransformInfo:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;

    invoke-virtual {v0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->setParam(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V

    .line 123
    :cond_0
    return-void
.end method

.method public OnSheetAutoFilterCellPos([I)V
    .locals 1
    .param p1, "nCellPos"    # [I

    .prologue
    .line 219
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->setSheetAutoFilterInfo([I)V

    .line 221
    :cond_0
    return-void
.end method

.method public OnSheetFormulaRangeRect(I[I[I)V
    .locals 1
    .param p1, "a_nCount"    # I
    .param p2, "a_aRangeRect"    # [I
    .param p3, "a_aCurRect"    # [I

    .prologue
    .line 225
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->setSheetFormulaRangeCount(I)V

    .line 226
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0, p2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->setSheetFormulaRangeRect([I)V

    .line 227
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0, p3}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->setSheetFormulaCursorRangeRect([I)V

    .line 229
    return-void
.end method

.method public ResetObjectPoints()V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->setObjectInfo(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V

    .line 116
    return-void
.end method

.method public deleteString(II)V
    .locals 5
    .param p1, "start"    # I
    .param p2, "before"    # I

    .prologue
    .line 1187
    const-string/jumbo v2, "EvEditGestureProc"

    const-string/jumbo v3, "============================================="

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1188
    if-lez p2, :cond_1

    .line 1189
    add-int v2, p1, p2

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getSavedCaretPos()I

    move-result v3

    sub-int v0, v2, v3

    .line 1190
    .local v0, "right":I
    const-string/jumbo v2, "EvEditGestureProc"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "deleteString oldsel : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getSavedCaretPos()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " right : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " before : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1191
    if-lez v0, :cond_0

    .line 1192
    move v1, v0

    .line 1193
    .local v1, "t":I
    :goto_0
    if-lez v1, :cond_0

    .line 1194
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/infraware/office/evengine/EvInterface;->ICaretMove(II)V

    .line 1195
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 1198
    .end local v1    # "t":I
    :cond_0
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v3, 0x2

    const/16 v4, 0x8

    invoke-virtual {v2, v3, v4, p2}, Lcom/infraware/office/evengine/EvInterface;->ICharInsert(III)V

    .line 1200
    .end local v0    # "right":I
    :cond_1
    return-void
.end method

.method protected doHyperLink()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 630
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 631
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    iget v0, v0, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;->nType:I

    sparse-switch v0, :sswitch_data_0

    .line 656
    :goto_0
    return-void

    .line 633
    :sswitch_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    iget-object v1, v1, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/ExternalCallManager;->runEmail(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Ljava/lang/String;)V

    goto :goto_0

    .line 636
    :sswitch_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    iget-object v1, v1, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/ExternalCallManager;->runHyperlink(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Ljava/lang/String;)V

    goto :goto_0

    .line 640
    :sswitch_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    iget-object v1, v1, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/ExternalCallManager;->runSMS(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Ljava/lang/String;)V

    goto :goto_0

    .line 643
    :sswitch_3
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0x2e

    iget-object v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto :goto_0

    .line 648
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    iget-object v7, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    .line 649
    .local v7, "hyperLink":Ljava/lang/String;
    const-string/jumbo v0, "HTTP"

    invoke-virtual {v7, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "Http"

    invoke-virtual {v7, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 651
    :cond_1
    const-string/jumbo v0, "H(TTP|ttp)"

    const-string/jumbo v1, "http"

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 654
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-static {v0, v7}, Lcom/infraware/polarisoffice5/common/ExternalCallManager;->runHyperlink(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Ljava/lang/String;)V

    goto :goto_0

    .line 631
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0xf -> :sswitch_0
        0x1f -> :sswitch_2
        0x20 -> :sswitch_3
    .end sparse-switch
.end method

.method public finishComposingText()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1344
    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->sendEmptyCommit(Z)V

    .line 1345
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->setPrevComposingOrg(Z)V

    .line 1346
    return v1
.end method

.method public getPrevTextLen()I
    .locals 1

    .prologue
    .line 1150
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPrevText:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPrevText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRotateAngle()I
    .locals 1

    .prologue
    .line 1524
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRotateAngle()I

    move-result v0

    return v0
.end method

.method public getSavedCaretPos()I
    .locals 1

    .prologue
    .line 1177
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mSavedCaretPos:I

    return v0
.end method

.method protected getScrollBarRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 8
    .param p1, "hor"    # Landroid/graphics/RectF;
    .param p2, "ver"    # Landroid/graphics/RectF;

    .prologue
    .line 1518
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetScrollInfo_Editor()Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;

    move-result-object v7

    .line 1519
    .local v7, "screenInfo":Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;
    iget v0, v7, Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;->nCurPosX:I

    int-to-float v3, v0

    iget v0, v7, Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;->nCurPosY:I

    int-to-float v4, v0

    iget v0, v7, Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;->nWidth:I

    int-to-float v5, v0

    iget v0, v7, Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;->nHeight:I

    int-to-float v6, v0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getScrollBarRect(Landroid/graphics/RectF;Landroid/graphics/RectF;FFFF)V

    .line 1520
    return-void
.end method

.method public getVideoPlayBtnRc()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getVideoPlayBtnRc()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public getVideoStatus()I
    .locals 2

    .prologue
    .line 147
    const/4 v0, 0x0

    .line 150
    .local v0, "nVideoStatus":I
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    instance-of v1, v1, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v1, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getVideoManager()Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 152
    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v1, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getVideoManager()Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->getVideoStatus()I

    move-result v0

    .line 154
    :cond_0
    return v0
.end method

.method public insertString(Ljava/lang/String;II)V
    .locals 7
    .param p1, "strText"    # Ljava/lang/String;
    .param p2, "compType"    # I
    .param p3, "compPos"    # I

    .prologue
    const/16 v6, 0x100

    .line 1105
    const/4 v1, 0x0

    .line 1106
    .local v1, "s":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 1108
    .local v2, "t":I
    if-nez v2, :cond_1

    .line 1109
    const-string/jumbo v3, "EvEditGestureProc"

    const-string/jumbo v4, "insertString commit or delete"

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1110
    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3, p1, p2, p3, v2}, Lcom/infraware/office/evengine/EvInterface;->IInsertString(Ljava/lang/String;III)V

    .line 1130
    :cond_0
    return-void

    .line 1114
    :cond_1
    const-string/jumbo v0, ""

    .line 1115
    .local v0, "in_str":Ljava/lang/String;
    :goto_0
    if-lez v2, :cond_0

    .line 1116
    if-le v2, v6, :cond_2

    .line 1117
    add-int/lit16 v3, v1, 0x100

    invoke-virtual {p1, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1118
    const-string/jumbo v3, "EvEditGestureProc"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "insertString1 comptype="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " comppos="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " len="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "text="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1119
    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    add-int v4, p3, v1

    invoke-virtual {v3, v0, p2, v4, v6}, Lcom/infraware/office/evengine/EvInterface;->IInsertString(Ljava/lang/String;III)V

    .line 1127
    :goto_1
    add-int/lit16 v2, v2, -0x100

    .line 1128
    add-int/lit16 v1, v1, 0x100

    goto :goto_0

    .line 1122
    :cond_2
    add-int v3, v1, v2

    invoke-virtual {p1, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1123
    const-string/jumbo v3, "EvEditGestureProc"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "insertString2 comptype="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " comppos="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " len="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "text="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1124
    iget-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    add-int v4, p3, v1

    invoke-virtual {v3, v0, p2, v4, v2}, Lcom/infraware/office/evengine/EvInterface;->IInsertString(Ljava/lang/String;III)V

    goto :goto_1
.end method

.method public is2007DocxVML()Z
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getGrapAttInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->is2007DocxVML()Z

    move-result v0

    return v0
.end method

.method public is3D()Z
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getGrapAttInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->is3D()Z

    move-result v0

    return v0
.end method

.method public isDML()Z
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getGrapAttInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->isDML()Z

    move-result v0

    return v0
.end method

.method public isPrevComposing()Z
    .locals 1

    .prologue
    .line 1154
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mbPrevComposing:Z

    return v0
.end method

.method public isPrevComposingOrg()Z
    .locals 1

    .prologue
    .line 1158
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mbPrevComposingOrg:Z

    return v0
.end method

.method public isSupport3D()Z
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getGrapAttInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->isSupport3D()Z

    move-result v0

    return v0
.end method

.method public isSupport3DBevel()Z
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getGrapAttInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_GRAPATT_INFO;->isSupport3DBevel()Z

    move-result v0

    return v0
.end method

.method public onCancel(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1090
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    packed-switch v0, :pswitch_data_0

    .line 1100
    :cond_0
    :goto_0
    return-void

    .line 1093
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getEvTextToSpeechHelper()Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1095
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1096
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onGuideStartTextToSpeech()V

    goto :goto_0

    .line 1090
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x3

    const/4 v12, 0x1

    const/4 v2, 0x0

    .line 662
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    if-ne v0, v4, :cond_1

    .line 712
    :cond_0
    :goto_0
    return v2

    .line 665
    :cond_1
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    const/16 v1, 0xf

    if-eq v0, v1, :cond_2

    .line 667
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    const/16 v1, 0x9

    if-eq v0, v1, :cond_0

    .line 670
    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onShowIme(Z)Z

    move v2, v12

    .line 671
    goto :goto_0

    .line 675
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 677
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0xe

    const/4 v6, 0x0

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    move v2, v12

    .line 678
    goto :goto_0

    .line 681
    :cond_3
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    if-ne v0, v4, :cond_5

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v0

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 683
    const/4 v11, 0x0

    .line 684
    .local v11, "nVideoStatus":I
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getVideoManager()Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 685
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getVideoManager()Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->getVideoStatus()I

    move-result v11

    .line 687
    :cond_4
    if-nez v11, :cond_5

    .line 688
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->show()V

    .line 691
    .end local v11    # "nVideoStatus":I
    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v5, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v6, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    long-to-int v8, v0

    const/4 v0, -0x1

    invoke-static {p1, v0}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v9

    move-object v3, p0

    move v7, v2

    invoke-direct/range {v3 .. v9}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onHIDAction(IIIIII)V

    .line 692
    const/4 v0, 0x6

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    .line 694
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v0

    if-ne v0, v4, :cond_7

    .line 696
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->sendDictionaryMessage()V

    .line 697
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->isShownDictionaryPanel()Z

    move-result v0

    if-nez v0, :cond_6

    .line 698
    invoke-virtual {p0, v12}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onShowIme(Z)Z

    .line 699
    :cond_6
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->show()V

    goto/16 :goto_0

    .line 701
    :cond_7
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v0

    if-nez v0, :cond_0

    .line 703
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetCaretInfo_Editor()Lcom/infraware/office/evengine/EV$CARET_INFO;

    move-result-object v10

    .line 705
    .local v10, "ci":Lcom/infraware/office/evengine/EV$CARET_INFO;
    iget v0, v10, Lcom/infraware/office/evengine/EV$CARET_INFO;->bCaret:I

    if-ne v0, v12, :cond_0

    .line 708
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->show()V

    goto/16 :goto_0
.end method

.method public onDoubleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 718
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    const/16 v1, 0xc

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    const/16 v1, 0xd

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    const/16 v1, 0xe

    if-ne v0, v1, :cond_1

    .line 763
    :cond_0
    :goto_0
    return v10

    .line 723
    :cond_1
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    if-ne v0, v10, :cond_3

    .line 725
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 726
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->show()V

    goto :goto_0

    .line 728
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsSingleCell()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 729
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->show()V

    goto :goto_0

    .line 734
    :cond_3
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v7

    .line 735
    .local v7, "doc_type":I
    if-eq v7, v10, :cond_4

    const/4 v0, 0x6

    if-ne v7, v0, :cond_5

    .line 737
    :cond_4
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_5

    .line 739
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->getMemoCmdData()Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;

    move-result-object v8

    .line 740
    .local v8, "memo":Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    const/4 v0, -0x1

    iput v0, v8, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->nMemoId:I

    .line 741
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/16 v1, 0x9

    invoke-virtual {v0, v1, v8}, Lcom/infraware/office/evengine/EvInterface;->IMemoCommand(ILcom/infraware/office/evengine/EV$MEMO_CMD_DATA;)Z

    .line 742
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0x36

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    .line 760
    .end local v8    # "memo":Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    :cond_5
    new-instance v9, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-direct {v9, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 761
    .local v9, "spellRect":Landroid/graphics/Rect;
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0, v9}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setMotionEventRect(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;Lcom/infraware/office/baseframe/EvBaseView;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "ebv"    # Lcom/infraware/office/baseframe/EvBaseView;

    .prologue
    const/4 v1, 0x1

    .line 240
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    sparse-switch v0, :sswitch_data_0

    .line 270
    :cond_0
    :goto_0
    :sswitch_0
    invoke-super {p0, p1, p2}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->onDraw(Landroid/graphics/Canvas;Lcom/infraware/office/baseframe/EvBaseView;)V

    .line 271
    return-void

    .line 244
    :sswitch_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    if-eq v0, v1, :cond_0

    .line 253
    :cond_1
    :sswitch_2
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 256
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v0

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 263
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    iget-boolean v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsMultiSelectionMode:Z

    invoke-virtual {v0, p1, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->drawVideo(Landroid/graphics/Canvas;Z)V

    goto :goto_0

    .line 265
    :cond_3
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->drawObjectProc(Landroid/graphics/Canvas;Lcom/infraware/office/baseframe/EvBaseView;)V

    goto :goto_0

    .line 240
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_2
        0x3 -> :sswitch_2
        0x9 -> :sswitch_1
        0xb -> :sswitch_2
        0xd -> :sswitch_2
        0x11 -> :sswitch_0
    .end sparse-switch
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 2
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v1, 0x1

    .line 1053
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    packed-switch v0, :pswitch_data_0

    .line 1079
    :goto_0
    :pswitch_0
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsFastMove:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->FlingObjCtrl()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    .line 1085
    :goto_1
    return v0

    :pswitch_1
    move v0, v1

    .line 1064
    goto :goto_1

    .line 1067
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getEvTextToSpeechHelper()Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1069
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1070
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->initGuideTextToSpeech()V

    goto :goto_0

    :cond_0
    move v0, v1

    .line 1072
    goto :goto_1

    :cond_1
    move v0, v1

    .line 1074
    goto :goto_1

    .line 1082
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 1083
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onStopVideo()Z

    .line 1085
    :cond_3
    invoke-super {p0, p1, p2, p3, p4}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    goto :goto_1

    .line 1053
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1368
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_3

    .line 1369
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v1

    .line 1370
    .local v1, "nMetaState":I
    iget-boolean v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->m_bShiftPressed:Z

    if-eqz v2, :cond_0

    .line 1371
    or-int/lit8 v1, v1, 0x1

    .line 1373
    :cond_0
    iget-boolean v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->m_bAltPressed:Z

    if-eqz v2, :cond_1

    .line 1374
    or-int/lit8 v1, v1, 0x2

    .line 1377
    :cond_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1379
    packed-switch p1, :pswitch_data_0

    .line 1391
    :cond_2
    sparse-switch p1, :sswitch_data_0

    .end local v1    # "nMetaState":I
    :cond_3
    move v2, v4

    .line 1497
    :goto_0
    return v2

    .line 1381
    .restart local v1    # "nMetaState":I
    :pswitch_0
    iget v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    if-ne v2, v3, :cond_4

    .line 1383
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setFocusFindButton(Ljava/lang/Boolean;)V

    move v2, v3

    .line 1384
    goto :goto_0

    .line 1386
    :cond_4
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/infraware/office/actionbar/MainActionBar;->setFocus(Z)V

    move v2, v3

    .line 1387
    goto :goto_0

    .line 1394
    :sswitch_0
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/16 v5, 0xa

    invoke-virtual {v2, v4, v5, v4}, Lcom/infraware/office/evengine/EvInterface;->ICharInsert(III)V

    .line 1395
    invoke-virtual {p0, v3, v3}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->updateCaretPos(ZZ)V

    move v2, v3

    .line 1396
    goto :goto_0

    .line 1398
    :sswitch_1
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/16 v5, 0x8

    invoke-virtual {v2, v6, v5, v4}, Lcom/infraware/office/evengine/EvInterface;->ICharInsert(III)V

    .line 1399
    invoke-virtual {p0, v3, v4}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->updateCaretPos(ZZ)V

    move v2, v3

    .line 1400
    goto :goto_0

    .line 1403
    :sswitch_2
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v2

    if-eq v2, v5, :cond_5

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v2

    if-ne v2, v3, :cond_7

    :cond_5
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetCaretInfo_Editor()Lcom/infraware/office/evengine/EV$CARET_INFO;

    move-result-object v2

    iget v2, v2, Lcom/infraware/office/evengine/EV$CARET_INFO;->bCaret:I

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetBWPGrapAttrInfo_Editor()Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;

    move-result-object v2

    iget v2, v2, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->eObjectType:I

    if-nez v2, :cond_7

    .line 1404
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    instance-of v2, v2, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    if-eqz v2, :cond_7

    .line 1405
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v2, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getContinuMode()Z

    move-result v2

    if-nez v2, :cond_6

    .line 1406
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2, v5, v4}, Lcom/infraware/office/evengine/EvInterface;->IMovePage(II)V

    :goto_1
    move v2, v3

    .line 1410
    goto :goto_0

    .line 1408
    :cond_6
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/16 v5, 0x1e

    invoke-virtual {v2, v4, v5}, Lcom/infraware/office/evengine/EvInterface;->IFlick(II)V

    goto :goto_1

    .line 1413
    :cond_7
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2, v3, v1}, Lcom/infraware/office/evengine/EvInterface;->ICaretMove(II)V

    .line 1414
    invoke-virtual {p0, v3, v3}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->updateCaretPos(ZZ)V

    move v2, v3

    .line 1415
    goto :goto_0

    .line 1418
    :sswitch_3
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDeviceId()I

    move-result v2

    if-nez v2, :cond_8

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v2

    if-ne v2, v3, :cond_8

    move v0, v3

    .line 1419
    .local v0, "isInnerLongKey":Z
    :goto_2
    if-ne v0, v3, :cond_9

    .line 1420
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/infraware/office/actionbar/MainActionBar;->setFocus(Z)V

    move v2, v3

    .line 1421
    goto/16 :goto_0

    .end local v0    # "isInnerLongKey":Z
    :cond_8
    move v0, v4

    .line 1418
    goto :goto_2

    .line 1425
    .restart local v0    # "isInnerLongKey":Z
    :cond_9
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetScrollInfo_Editor()Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;

    move-result-object v2

    iget v2, v2, Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;->nCurPosY:I

    if-gtz v2, :cond_d

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetCaretInfo_Editor()Lcom/infraware/office/evengine/EV$CARET_INFO;

    move-result-object v2

    iget v2, v2, Lcom/infraware/office/evengine/EV$CARET_INFO;->bCaret:I

    if-nez v2, :cond_d

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetBWPGrapAttrInfo_Editor()Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;

    move-result-object v2

    iget v2, v2, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->eObjectType:I

    if-nez v2, :cond_d

    .line 1426
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v2

    if-ne v2, v6, :cond_c

    .line 1427
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->IsViewerMode()Z

    move-result v2

    if-nez v2, :cond_b

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetCellInfo()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    move-result-object v2

    iget-object v2, v2, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tActiveRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v2, v2, Lcom/infraware/office/evengine/EV$RANGE;->nRow1:I

    if-nez v2, :cond_a

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetCellInfo()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    move-result-object v2

    iget-object v2, v2, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tActiveRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v2, v2, Lcom/infraware/office/evengine/EV$RANGE;->nRow2:I

    if-eqz v2, :cond_b

    .line 1428
    :cond_a
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2, v4, v1}, Lcom/infraware/office/evengine/EvInterface;->ICaretMove(II)V

    move v2, v3

    .line 1429
    goto/16 :goto_0

    .line 1431
    :cond_b
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->requestLayout()V

    .line 1432
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->clearFocus()V

    .line 1433
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->ICancel()V

    .line 1434
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/infraware/office/actionbar/MainActionBar;->setFocus(Z)V

    move v2, v3

    .line 1435
    goto/16 :goto_0

    .line 1439
    :cond_c
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v2

    iget v2, v2, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    if-ne v2, v3, :cond_d

    .line 1440
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->requestLayout()V

    .line 1441
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->clearFocus()V

    .line 1442
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->ICancel()V

    .line 1443
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/infraware/office/actionbar/MainActionBar;->setFocus(Z)V

    move v2, v3

    .line 1444
    goto/16 :goto_0

    .line 1448
    :cond_d
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v2

    if-eq v2, v5, :cond_e

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v2

    if-ne v2, v3, :cond_10

    :cond_e
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetCaretInfo_Editor()Lcom/infraware/office/evengine/EV$CARET_INFO;

    move-result-object v2

    iget v2, v2, Lcom/infraware/office/evengine/EV$CARET_INFO;->bCaret:I

    if-nez v2, :cond_10

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetBWPGrapAttrInfo_Editor()Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;

    move-result-object v2

    iget v2, v2, Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;->eObjectType:I

    if-nez v2, :cond_10

    .line 1449
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    instance-of v2, v2, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    if-eqz v2, :cond_10

    .line 1450
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v2, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getContinuMode()Z

    move-result v2

    if-nez v2, :cond_f

    .line 1451
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2, v3, v4}, Lcom/infraware/office/evengine/EvInterface;->IMovePage(II)V

    :goto_3
    move v2, v3

    .line 1454
    goto/16 :goto_0

    .line 1453
    :cond_f
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/16 v5, -0x1e

    invoke-virtual {v2, v4, v5}, Lcom/infraware/office/evengine/EvInterface;->IFlick(II)V

    goto :goto_3

    .line 1458
    :cond_10
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetCaretPos()Lcom/infraware/office/evengine/EV$CARET_POS;

    move-result-object v2

    iget v2, v2, Lcom/infraware/office/evengine/EV$CARET_POS;->nColPos:I

    if-nez v2, :cond_11

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetCaretPos()Lcom/infraware/office/evengine/EV$CARET_POS;

    move-result-object v2

    iget v2, v2, Lcom/infraware/office/evengine/EV$CARET_POS;->nParaPos:I

    if-nez v2, :cond_11

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v2

    if-ne v2, v3, :cond_11

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v2

    iget v2, v2, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    if-ne v2, v3, :cond_11

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetCaretInfo_Editor()Lcom/infraware/office/evengine/EV$CARET_INFO;

    move-result-object v2

    iget v2, v2, Lcom/infraware/office/evengine/EV$CARET_INFO;->bCaret:I

    if-ne v2, v3, :cond_11

    .line 1460
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->clearFocus()V

    .line 1461
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/infraware/office/actionbar/MainActionBar;->setFocus(Z)V

    move v2, v3

    .line 1462
    goto/16 :goto_0

    .line 1464
    :cond_11
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2, v4, v1}, Lcom/infraware/office/evengine/EvInterface;->ICaretMove(II)V

    .line 1465
    invoke-virtual {p0, v3, v3}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->updateCaretPos(ZZ)V

    move v2, v3

    .line 1466
    goto/16 :goto_0

    .line 1470
    .end local v0    # "isInnerLongKey":Z
    :sswitch_4
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2, v6, v1}, Lcom/infraware/office/evengine/EvInterface;->ICaretMove(II)V

    .line 1471
    invoke-virtual {p0, v3, v4}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->updateCaretPos(ZZ)V

    move v2, v3

    .line 1472
    goto/16 :goto_0

    .line 1474
    :sswitch_5
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2, v5, v1}, Lcom/infraware/office/evengine/EvInterface;->ICaretMove(II)V

    .line 1475
    invoke-virtual {p0, v3, v4}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->updateCaretPos(ZZ)V

    move v2, v3

    .line 1476
    goto/16 :goto_0

    .line 1479
    :sswitch_6
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getVideoStatus()I

    move-result v2

    if-ne v2, v6, :cond_13

    .line 1480
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v2, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onStopVideo()Z

    :cond_12
    move v2, v3

    .line 1485
    goto/16 :goto_0

    .line 1481
    :cond_13
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v2

    if-ne v2, v5, :cond_12

    iget-boolean v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsMultiSelectionMode:Z

    if-nez v2, :cond_12

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v2

    const/16 v4, 0x10

    if-ne v2, v4, :cond_12

    .line 1483
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onPlayVideoByKeypad()Z

    move-result v2

    goto/16 :goto_0

    .line 1488
    :sswitch_7
    iget v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    if-ne v2, v3, :cond_14

    .line 1490
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->setFocusFindButton(Ljava/lang/Boolean;)V

    move v2, v3

    .line 1491
    goto/16 :goto_0

    .line 1493
    :cond_14
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/infraware/office/actionbar/MainActionBar;->setFocus(Z)V

    move v2, v3

    .line 1494
    goto/16 :goto_0

    .line 1379
    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
    .end packed-switch

    .line 1391
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_3
        0x14 -> :sswitch_2
        0x15 -> :sswitch_4
        0x16 -> :sswitch_5
        0x17 -> :sswitch_6
        0x39 -> :sswitch_7
        0x3a -> :sswitch_7
        0x42 -> :sswitch_0
        0x43 -> :sswitch_1
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1502
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1513
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 17
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 966
    const-string/jumbo v1, "EvEditGestureProc"

    const-string/jumbo v2, "onLongPress"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 967
    move-object/from16 v0, p0

    iget v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 1049
    :cond_0
    :goto_0
    return-void

    .line 988
    :cond_1
    invoke-static {}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getInstance()Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1, v2}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->isNormalWindow(Landroid/app/Activity;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v13, 0x1

    .line 990
    .local v13, "isMW":Z
    :goto_1
    if-nez v13, :cond_3

    .line 992
    const/4 v2, 0x2

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v3, v1

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v4, v1

    const/4 v5, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    long-to-int v6, v6

    const/4 v1, -0x1

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v7

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onHIDAction(IIIIII)V

    .line 993
    invoke-virtual/range {p0 .. p1}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onDoubleTap(Landroid/view/MotionEvent;)Z

    .line 994
    const/4 v1, 0x6

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    goto :goto_0

    .line 988
    .end local v13    # "isMW":Z
    :cond_2
    const/4 v13, 0x0

    goto :goto_1

    .line 997
    .restart local v13    # "isMW":Z
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v15

    .line 998
    .local v15, "type":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v3, v1

    .line 999
    .local v3, "x":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v4, v1

    .line 1000
    .local v4, "y":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getmView()Landroid/view/View;

    move-result-object v16

    .line 1001
    .local v16, "v":Landroid/view/View;
    sget-boolean v1, Lcom/infraware/common/multiwindow/MWDnDOperator;->LOG:Z

    if-eqz v1, :cond_4

    const-string/jumbo v1, "MWDnD"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "type : "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMWDnD()Lcom/infraware/common/multiwindow/MWDnDOperator;

    move-result-object v14

    .line 1003
    .local v14, "oMWDnD":Lcom/infraware/common/multiwindow/MWDnDOperator;
    const/4 v12, 0x1

    .line 1004
    .local v12, "doDefault":Z
    packed-switch v15, :pswitch_data_0

    .line 1042
    :cond_5
    :goto_2
    :pswitch_0
    if-eqz v12, :cond_0

    .line 1043
    const/4 v6, 0x2

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v7, v1

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v8, v1

    const/4 v9, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v1

    long-to-int v10, v1

    const/4 v1, -0x1

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v11

    move-object/from16 v5, p0

    invoke-direct/range {v5 .. v11}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onHIDAction(IIIIII)V

    .line 1044
    invoke-virtual/range {p0 .. p1}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onDoubleTap(Landroid/view/MotionEvent;)Z

    .line 1045
    const/4 v1, 0x6

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    goto/16 :goto_0

    .line 1006
    :pswitch_1
    invoke-virtual {v14, v3, v4}, Lcom/infraware/common/multiwindow/MWDnDOperator;->isTextMarkArea(II)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    .line 1007
    invoke-virtual {v14}, Lcom/infraware/common/multiwindow/MWDnDOperator;->preoccupyOnEditCopy()V

    .line 1008
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1, v2, v5, v6}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    .line 1009
    const/4 v1, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Lcom/infraware/common/multiwindow/MWDnDOperator;->startDragTextMark(Landroid/view/View;Landroid/graphics/Bitmap;)V

    .line 1010
    const/4 v12, 0x0

    goto :goto_2

    .line 1014
    :pswitch_2
    invoke-virtual {v14, v3, v4}, Lcom/infraware/common/multiwindow/MWDnDOperator;->isSelectedArea(II)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    .line 1015
    invoke-virtual {v14}, Lcom/infraware/common/multiwindow/MWDnDOperator;->preoccupyOnEditCopy()V

    .line 1016
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1, v2, v5, v6}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    .line 1017
    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v14, v0, v1, v2}, Lcom/infraware/common/multiwindow/MWDnDOperator;->startDragImage(Landroid/view/View;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 1018
    const/4 v2, 0x2

    const/4 v5, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    long-to-int v6, v6

    const/4 v1, -0x1

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v7

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onHIDAction(IIIIII)V

    .line 1019
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    .line 1020
    const/4 v12, 0x0

    goto :goto_2

    .line 1026
    :pswitch_3
    invoke-virtual {v14, v3, v4}, Lcom/infraware/common/multiwindow/MWDnDOperator;->isSelectedArea(II)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    .line 1027
    invoke-virtual {v14}, Lcom/infraware/common/multiwindow/MWDnDOperator;->preoccupyOnEditCopy()V

    .line 1028
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1, v2, v5, v6}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    .line 1029
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_6

    .line 1030
    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v14, v0, v1, v2}, Lcom/infraware/common/multiwindow/MWDnDOperator;->startDragImage(Landroid/view/View;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 1034
    :goto_3
    const/4 v2, 0x2

    const/4 v5, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    long-to-int v6, v6

    const/4 v1, -0x1

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v7

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onHIDAction(IIIIII)V

    .line 1035
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    .line 1036
    const/4 v12, 0x0

    goto/16 :goto_2

    .line 1032
    :cond_6
    const/4 v1, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Lcom/infraware/common/multiwindow/MWDnDOperator;->startDragHtmlWithImage(Landroid/view/View;Landroid/graphics/Bitmap;)V

    goto :goto_3

    .line 1004
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public onPlayVideo(II)Z
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v4, 0x1

    .line 159
    const/4 v0, 0x0

    .line 161
    .local v0, "bRet":Z
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getVideoPlayBtnRc()Landroid/graphics/Rect;

    move-result-object v1

    .line 167
    .local v1, "rcPlayBtn":Landroid/graphics/Rect;
    if-nez v1, :cond_1

    .line 178
    :cond_0
    :goto_0
    return v4

    .line 169
    :cond_1
    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-ne v2, v4, :cond_0

    .line 171
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getVideoStatus()I

    move-result v2

    if-nez v2, :cond_0

    .line 172
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v2, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v2, p1, p2}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onPlayVideo(II)Z

    move-result v0

    .line 173
    if-nez v0, :cond_0

    .line 174
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v2, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getVideoManager()Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->setVideoStatus(I)V

    goto :goto_0
.end method

.method public onPlayVideoByKeypad()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 183
    const/4 v0, 0x0

    .line 184
    .local v0, "bRet":Z
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getVideoPlayBtnRc()Landroid/graphics/Rect;

    move-result-object v1

    .line 186
    .local v1, "rcPlayBtn":Landroid/graphics/Rect;
    if-nez v1, :cond_1

    .line 194
    :cond_0
    :goto_0
    return v4

    .line 188
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getVideoStatus()I

    move-result v2

    if-nez v2, :cond_0

    .line 189
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v2, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v2, v3, v3}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onPlayVideo(II)Z

    move-result v0

    .line 190
    if-nez v0, :cond_0

    .line 191
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v2, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getVideoManager()Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->setVideoStatus(I)V

    goto :goto_0
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 15
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 1546
    const-string/jumbo v0, "HYOHYUN"

    const-string/jumbo v1, "onScale"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1547
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mbScaleTouchEnable:Z

    if-nez v0, :cond_1

    .line 1548
    :cond_0
    const/4 v0, 0x0

    .line 1595
    :goto_0
    return v0

    .line 1551
    :cond_1
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    const/16 v1, 0xc

    if-ne v0, v1, :cond_4

    .line 1553
    invoke-virtual/range {p1 .. p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v14

    .line 1554
    .local v14, "newDist":F
    const/high16 v0, 0x41200000    # 10.0f

    cmpl-float v0, v14, v0

    if-lez v0, :cond_3

    .line 1555
    new-instance v12, Landroid/graphics/PointF;

    invoke-virtual/range {p1 .. p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v1

    invoke-direct {v12, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1556
    .local v12, "center":Landroid/graphics/PointF;
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mScaleTouchBeginScale:I

    int-to-float v0, v0

    iget v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mScaleTouchBeginSpace:F

    div-float v1, v14, v1

    mul-float/2addr v0, v1

    float-to-int v2, v0

    .line 1557
    .local v2, "nScale":I
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nMinZoom:I

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v1

    iget v1, v1, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nMaxZoom:I

    invoke-virtual {p0, v2, v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->minMax(III)I

    move-result v2

    .line 1559
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    if-eq v0, v2, :cond_2

    .line 1560
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x1

    iget v10, v12, Landroid/graphics/PointF;->x:F

    float-to-int v10, v10

    iget v11, v12, Landroid/graphics/PointF;->y:F

    float-to-int v11, v11

    invoke-virtual/range {v0 .. v11}, Lcom/infraware/office/evengine/EvInterface;->ISetZoom(IIIIIIIIIII)V

    .line 1562
    :cond_2
    const-string/jumbo v0, "EvEditGestureProc"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onScale nScale = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1565
    iput v14, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mScaleTouchPreSpace:F

    .line 1567
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mGyroTiltCurrentValue:I

    .line 1569
    .end local v2    # "nScale":I
    .end local v12    # "center":Landroid/graphics/PointF;
    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    .line 1571
    .end local v14    # "newDist":F
    :cond_4
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_7

    .line 1573
    invoke-virtual/range {p1 .. p1}, Landroid/view/ScaleGestureDetector;->getEventTime()J

    move-result-wide v0

    iget-wide v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mScaleDownTime:J

    sub-long/2addr v0, v3

    const-wide/16 v3, 0xc8

    cmp-long v0, v0, v3

    if-lez v0, :cond_5

    .line 1575
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mScaleDownSpanX:F

    invoke-virtual/range {p1 .. p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v1

    div-float v13, v0, v1

    .line 1576
    .local v13, "gap":F
    float-to-double v0, v13

    const-wide v3, 0x3ff0be0ded288ce7L    # 1.0464

    cmpg-double v0, v0, v3

    if-gez v0, :cond_6

    float-to-double v0, v13

    const-wide v3, 0x3fefeeb702602c91L    # 0.99789

    cmpl-double v0, v0, v3

    if-lez v0, :cond_6

    .line 1584
    const/16 v0, 0xa

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    .line 1588
    .end local v13    # "gap":F
    :cond_5
    :goto_1
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1586
    .restart local v13    # "gap":F
    :cond_6
    const/16 v0, 0xc

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    goto :goto_1

    .line 1589
    .end local v13    # "gap":F
    :cond_7
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_8

    .line 1590
    new-instance v12, Landroid/graphics/PointF;

    invoke-virtual/range {p1 .. p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v1

    invoke-direct {v12, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1591
    .restart local v12    # "center":Landroid/graphics/PointF;
    const/4 v4, 0x1

    iget v0, v12, Landroid/graphics/PointF;->x:F

    float-to-int v5, v0

    iget v0, v12, Landroid/graphics/PointF;->y:F

    float-to-int v6, v0

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/view/ScaleGestureDetector;->getEventTime()J

    move-result-wide v0

    long-to-int v8, v0

    const/4 v9, 0x0

    move-object v3, p0

    invoke-direct/range {v3 .. v9}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onHIDAction(IIIIII)V

    .line 1592
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1595
    .end local v12    # "center":Landroid/graphics/PointF;
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 9
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x3

    const/4 v8, 0x1

    const/4 v1, 0x0

    const/16 v4, 0xc

    .line 1601
    const-string/jumbo v0, "HYOHYUN"

    const-string/jumbo v2, "onScaleBegin"

    invoke-static {v0, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1602
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    if-eq v0, v5, :cond_0

    iget-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mbScaleTouchEnable:Z

    if-nez v0, :cond_1

    .line 1651
    :cond_0
    :goto_0
    return v1

    .line 1605
    :cond_1
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    if-ne v0, v8, :cond_2

    .line 1606
    iput v6, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    .line 1608
    :cond_2
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    if-ne v0, v6, :cond_0

    .line 1609
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mScaleTouchPreCenter:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 1610
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mScaleTouchBeginSpace:F

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mScaleTouchPreSpace:F

    .line 1611
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mScaleTouchBeginScale:I

    .line 1613
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    if-ne v0, v8, :cond_7

    .line 1616
    iget-object v7, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v7, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    .line 1617
    .local v7, "wordEditorActivity":Lcom/infraware/polarisoffice5/word/WordEditorActivity;
    if-eqz v7, :cond_3

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getEvTextToSpeechHelper()Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getReflowText()Z

    move-result v0

    if-eq v0, v8, :cond_4

    .line 1618
    :cond_3
    iput v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    .line 1623
    .end local v7    # "wordEditorActivity":Lcom/infraware/polarisoffice5/word/WordEditorActivity;
    :cond_4
    :goto_1
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    if-eq v0, v4, :cond_5

    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    const/16 v2, 0xd

    if-eq v0, v2, :cond_5

    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    const/16 v2, 0xe

    if-ne v0, v2, :cond_9

    .line 1627
    :cond_5
    iput v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    .line 1628
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getEventTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mScaleDownTime:J

    .line 1629
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mScaleDownSpanX:F

    .line 1631
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-ne v0, v6, :cond_8

    .line 1634
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v5}, Lcom/infraware/office/evengine/EvInterface;->ISetInfraPenDrawMode(I)V

    .line 1635
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v0

    float-to-int v2, v0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v0

    float-to-int v3, v0

    iget-wide v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mScaleDownTime:J

    long-to-int v5, v4

    const/16 v6, 0xff

    move-object v0, p0

    move v4, v1

    invoke-direct/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onHIDAction(IIIIII)V

    :cond_6
    :goto_2
    move v1, v8

    .line 1649
    goto/16 :goto_0

    .line 1621
    :cond_7
    iput v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    goto :goto_1

    .line 1638
    :cond_8
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v5}, Lcom/infraware/office/evengine/EvInterface;->ISetInfraPenDrawMode(I)V

    goto :goto_2

    .line 1640
    :cond_9
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_6

    .line 1642
    iput v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    .line 1643
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getEventTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mScaleDownTime:J

    .line 1644
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mScaleDownSpanX:F

    .line 1645
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v5}, Lcom/infraware/office/evengine/EvInterface;->ISetInfraPenDrawMode(I)V

    .line 1646
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISetTableCancleMode()V

    goto :goto_2
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 13
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/16 v3, 0xc

    const/4 v7, 0x2

    const/16 v12, 0xd

    const/4 v1, 0x0

    .line 1658
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mScaleTouchBeginSpace:F

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mScaleTouchPreSpace:F

    .line 1659
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mScaleTouchBeginScale:I

    .line 1660
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mGyroTiltCurrentValue:I

    .line 1661
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mScaleDownSpanX:F

    .line 1663
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    if-eq v0, v3, :cond_0

    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    if-eq v0, v12, :cond_0

    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    const/16 v2, 0xe

    if-ne v0, v2, :cond_1

    .line 1667
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetInfraPenDrawMode(I)V

    .line 1670
    :cond_1
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    if-ne v0, v3, :cond_2

    .line 1671
    iput v7, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    .line 1672
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v2

    iget v2, v2, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v8, v1

    move v9, v1

    move v10, v1

    move v11, v1

    invoke-virtual/range {v0 .. v11}, Lcom/infraware/office/evengine/EvInterface;->ISetZoom(IIIIIIIIIII)V

    .line 1674
    iput v12, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    .line 1676
    :cond_2
    iput v12, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    .line 1677
    return-void
.end method

.method public onShowIme(Z)Z
    .locals 7
    .param p1, "bShow"    # Z

    .prologue
    const/4 v2, 0x0

    .line 1351
    if-eqz p1, :cond_1

    .line 1353
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->IsViewerMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1354
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/infraware/office/util/EvUtil;->showIme(Landroid/view/View;)V

    .line 1355
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0x9

    const/4 v6, 0x0

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    .line 1361
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1359
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/office/util/EvUtil;->hideIme(Landroid/content/Context;Landroid/os/IBinder;)V

    goto :goto_0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 15
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 769
    const-string/jumbo v0, "EvEditGestureProc"

    const-string/jumbo v1, "onSingleTapConfirmed"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-nez v0, :cond_0

    .line 772
    const/4 v0, 0x1

    .line 961
    :goto_0
    return v0

    .line 774
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-nez v0, :cond_2

    .line 775
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 777
    :cond_2
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    .line 779
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 780
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->show()V

    .line 785
    :cond_3
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 782
    :cond_4
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsSingleCell()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 783
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->show()V

    goto :goto_1

    .line 788
    :cond_5
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_6

    .line 789
    const/4 v0, 0x1

    goto :goto_0

    .line 791
    :cond_6
    const/4 v8, 0x0

    .line 792
    .local v8, "bMemo":Z
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v11

    .line 794
    .local v11, "docType":I
    const/4 v0, 0x1

    if-eq v11, v0, :cond_7

    const/4 v0, 0x6

    if-ne v11, v0, :cond_a

    .line 796
    :cond_7
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_8

    .line 798
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->getMemoCmdData()Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;

    move-result-object v12

    .line 799
    .local v12, "memo":Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v12}, Lcom/infraware/office/evengine/EvInterface;->IMemoCommand(ILcom/infraware/office/evengine/EV$MEMO_CMD_DATA;)Z

    .line 800
    iget-boolean v0, v12, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->bShow:Z

    if-eqz v0, :cond_8

    .line 802
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x7

    invoke-virtual {v0, v1, v12}, Lcom/infraware/office/evengine/EvInterface;->IMemoCommand(ILcom/infraware/office/evengine/EV$MEMO_CMD_DATA;)Z

    .line 803
    iget v0, v12, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->nMemoId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_9

    .line 805
    const/4 v8, 0x1

    .line 808
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onShowIme(Z)Z

    .line 825
    .end local v12    # "memo":Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    :cond_8
    :goto_2
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    const/16 v1, 0xf

    if-ne v0, v1, :cond_b

    .line 827
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 812
    .restart local v12    # "memo":Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    :cond_9
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0xf

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto :goto_2

    .line 817
    .end local v12    # "memo":Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    :cond_a
    const/4 v0, 0x2

    if-ne v11, v0, :cond_8

    .line 819
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetCommentText()Ljava/lang/String;

    move-result-object v13

    .line 820
    .local v13, "memo_str":Ljava/lang/String;
    if-eqz v13, :cond_8

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_8

    .line 821
    const/4 v8, 0x1

    goto :goto_2

    .line 830
    .end local v13    # "memo_str":Ljava/lang/String;
    :cond_b
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-nez v0, :cond_d

    .line 831
    :cond_c
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 833
    :cond_d
    const/4 v7, 0x0

    .line 835
    .local v7, "bHyperLINK":Z
    const/4 v9, 0x0

    .line 837
    .local v9, "bShowPopupMenu":Z
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_e

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v0

    const/16 v1, 0x10

    if-ne v0, v1, :cond_e

    .line 839
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p0, v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onPlayVideo(II)Z

    move-result v0

    goto/16 :goto_0

    .line 842
    :cond_e
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 889
    :cond_f
    :goto_3
    :pswitch_0
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    if-nez v0, :cond_10

    .line 891
    const/4 v0, 0x2

    if-ne v11, v0, :cond_14

    .line 893
    iget-object v14, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v14, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .line 896
    .local v14, "sheetEditorActivity":Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-virtual {v14}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getFilterProcessing()Z

    move-result v0

    if-nez v0, :cond_10

    .line 898
    invoke-virtual {v14}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->dismissFilterPopup()V

    .line 900
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetSheetHyperLinkInfo()Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    .line 901
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    iget v0, v0, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;->nType:I

    if-eqz v0, :cond_10

    .line 903
    const/4 v7, 0x1

    .line 921
    .end local v14    # "sheetEditorActivity":Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    :cond_10
    :goto_4
    if-eqz v8, :cond_15

    if-eqz v7, :cond_15

    .line 923
    const/4 v9, 0x1

    .line 924
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mIsMemoAndHyperlink:Z

    .line 942
    :cond_11
    :goto_5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->m_bSheetPopupMenu:Z

    .line 944
    if-eqz v9, :cond_13

    iget-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsMultiSelectionMode:Z

    if-nez v0, :cond_13

    .line 946
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->isTouchUpShowFlag()Z

    move-result v0

    if-nez v0, :cond_12

    .line 947
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->show()V

    .line 948
    :cond_12
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setTouchUpShowFlag(Z)V

    .line 949
    const/4 v0, 0x2

    if-ne v11, v0, :cond_13

    .line 950
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->m_bSheetPopupMenu:Z

    .line 961
    :cond_13
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 848
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetCaretInfo_Editor()Lcom/infraware/office/evengine/EV$CARET_INFO;

    move-result-object v10

    .line 852
    .local v10, "ci":Lcom/infraware/office/evengine/EV$CARET_INFO;
    iget v0, v10, Lcom/infraware/office/evengine/EV$CARET_INFO;->bCaret:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_f

    iget v0, v10, Lcom/infraware/office/evengine/EV$CARET_INFO;->nHeight:I

    if-lez v0, :cond_f

    .line 854
    const/4 v9, 0x1

    goto :goto_3

    .line 862
    .end local v10    # "ci":Lcom/infraware/office/evengine/EV$CARET_INFO;
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->isCellObjMarkArea(II)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 863
    const/4 v9, 0x1

    goto :goto_3

    .line 909
    :cond_14
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_10

    .line 911
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetHyperLinkInfo_Editor()Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    .line 912
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    iget v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->bUse:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_10

    .line 914
    const/4 v7, 0x1

    goto :goto_4

    .line 926
    :cond_15
    if-eqz v8, :cond_19

    .line 928
    const/4 v0, 0x1

    if-eq v11, v0, :cond_16

    const/4 v0, 0x6

    if-ne v11, v0, :cond_18

    .line 929
    :cond_16
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0xf

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    .line 934
    :cond_17
    :goto_6
    const/4 v9, 0x0

    goto/16 :goto_5

    .line 930
    :cond_18
    const/4 v0, 0x2

    if-ne v11, v0, :cond_17

    .line 932
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0xf

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto :goto_6

    .line 936
    :cond_19
    if-eqz v7, :cond_11

    .line 938
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->doHyperLink()V

    .line 939
    const/4 v9, 0x0

    goto/16 :goto_5

    .line 842
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onTextChanged(ZLjava/lang/CharSequence;III)V
    .locals 15
    .param p1, "bComposing"    # Z
    .param p2, "text"    # Ljava/lang/CharSequence;
    .param p3, "start"    # I
    .param p4, "before"    # I
    .param p5, "after"    # I

    .prologue
    .line 1204
    iget v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    if-eqz v2, :cond_0

    .line 1205
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->sendEmptyCommit(Z)V

    .line 1297
    :goto_0
    return-void

    .line 1219
    :cond_0
    add-int v2, p3, p5

    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-interface {v0, v1, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    .line 1220
    .local v13, "strText":Ljava/lang/String;
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v11

    .line 1221
    .local v11, "endPos":I
    if-nez v11, :cond_1

    if-nez p4, :cond_1

    .line 1222
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->sendEmptyCommit(Z)V

    goto :goto_0

    .line 1226
    :cond_1
    const-string/jumbo v2, "EvEditGestureProc"

    const-string/jumbo v3, "============================================="

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1227
    if-nez p1, :cond_6

    .line 1228
    if-nez p4, :cond_4

    .line 1229
    const-string/jumbo v2, "EvEditGestureProc"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "commmit onTextChanged compPos=0 Len="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v13}, Lcom/infraware/office/util/EvUtil;->stringToHexa(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1232
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->isPrevComposingOrg()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1233
    const-string/jumbo v2, ""

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0, v2, v3, v4}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->insertString(Ljava/lang/String;II)V

    .line 1250
    :goto_1
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->updateCaretPos(ZZ)V

    .line 1251
    const-string/jumbo v2, ""

    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->setPrevText(Ljava/lang/CharSequence;)V

    .line 1295
    :cond_2
    :goto_2
    invoke-virtual/range {p0 .. p1}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->setPrevComposing(Z)V

    .line 1296
    invoke-virtual/range {p0 .. p1}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->setPrevComposingOrg(Z)V

    goto :goto_0

    .line 1235
    :cond_3
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0, v13, v2, v3}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->insertString(Ljava/lang/String;II)V

    goto :goto_1

    .line 1237
    :cond_4
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v3, 0xb

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-interface/range {v2 .. v8}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    .line 1238
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->isPrevComposing()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1239
    const-string/jumbo v2, "EvEditGestureProc"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "comp onTextChanged compPos=0 Len="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v13}, Lcom/infraware/office/util/EvUtil;->stringToHexa(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1240
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0, v13, v2, v3}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->insertString(Ljava/lang/String;II)V

    .line 1241
    const-string/jumbo v2, "EvEditGestureProc"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "commmit onTextChanged compPos=0 Len=0 :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v13}, Lcom/infraware/office/util/EvUtil;->stringToHexa(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1242
    const-string/jumbo v2, ""

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0, v2, v3, v4}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->insertString(Ljava/lang/String;II)V

    .line 1247
    :goto_3
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v3, 0xb

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-interface/range {v2 .. v8}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_1

    .line 1244
    :cond_5
    move/from16 v0, p3

    move/from16 v1, p4

    invoke-virtual {p0, v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->deleteString(II)V

    .line 1245
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0, v13, v2, v3}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->insertString(Ljava/lang/String;II)V

    goto :goto_3

    .line 1253
    :cond_6
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getPrevTextLen()I

    move-result v12

    .line 1254
    .local v12, "prevPos":I
    sub-int v10, v12, p4

    .line 1255
    .local v10, "compPos":I
    sub-int v9, p5, p4

    .line 1256
    .local v9, "compLen":I
    const-string/jumbo v2, "EvEditGestureProc"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onTextChanged0-0  prevPos : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " compPos : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1257
    const-string/jumbo v2, "EvEditGestureProc"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onTextChanged0-1  mbPrevComposing : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->isPrevComposing()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " endPos : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1259
    if-nez v9, :cond_9

    .line 1260
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->isPrevComposing()Z

    move-result v2

    if-nez v2, :cond_7

    .line 1261
    move/from16 v0, p3

    move/from16 v1, p4

    invoke-virtual {p0, v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->deleteString(II)V

    .line 1262
    const/4 v10, 0x0

    .line 1264
    :cond_7
    const-string/jumbo v2, "EvEditGestureProc"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onTextChanged1 compPos="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " compLen="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v13}, Lcom/infraware/office/util/EvUtil;->stringToHexa(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1278
    :cond_8
    :goto_4
    const-string/jumbo v2, "EvEditGestureProc"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "comp onTextChanged0-3 compPos="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " Len="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v13}, Lcom/infraware/office/util/EvUtil;->stringToHexa(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1280
    const/4 v2, 0x0

    invoke-virtual {p0, v13, v2, v10}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->insertString(Ljava/lang/String;II)V

    .line 1281
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->updateCaretPos(ZZ)V

    .line 1282
    if-lez p5, :cond_c

    .line 1283
    add-int v2, p3, p5

    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-interface {v0, v1, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->setPrevText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1266
    :cond_9
    if-lez v9, :cond_a

    .line 1267
    const-string/jumbo v2, "EvEditGestureProc"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onTextChanged2 compPos="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " compLen="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v13}, Lcom/infraware/office/util/EvUtil;->stringToHexa(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 1269
    :cond_a
    if-gez v9, :cond_8

    .line 1272
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->isPrevComposing()Z

    move-result v2

    if-nez v2, :cond_b

    .line 1273
    move/from16 v0, p3

    move/from16 v1, p4

    invoke-virtual {p0, v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->deleteString(II)V

    .line 1274
    const/4 v10, 0x0

    .line 1276
    :cond_b
    const-string/jumbo v2, "EvEditGestureProc"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onTextChanged3 compPos="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " compLen="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v13}, Lcom/infraware/office/util/EvUtil;->stringToHexa(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 1286
    :cond_c
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPrevText:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 1287
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPrevText:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v14

    .line 1288
    .local v14, "t":I
    if-lez v14, :cond_d

    move/from16 v0, p4

    if-le v14, v0, :cond_d

    .line 1289
    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPrevText:Ljava/lang/String;

    const/4 v3, 0x0

    sub-int v4, v14, p4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->setPrevText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1291
    :cond_d
    const-string/jumbo v2, ""

    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->setPrevText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2
.end method

.method public onTouchDown(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 276
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    const/4 v4, 0x3

    if-ne v0, v4, :cond_0

    .line 278
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getEvTextToSpeechHelper()Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 279
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 280
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->initGuideTextToSpeech()V

    .line 287
    :cond_0
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    const/16 v4, 0x10

    if-ne v0, v4, :cond_2

    .line 289
    invoke-direct {p0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onPointerEvent(Landroid/view/MotionEvent;)V

    move v1, v8

    .line 327
    :cond_1
    :goto_0
    return v1

    .line 294
    :cond_2
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    const/4 v4, 0x6

    if-ne v0, v4, :cond_3

    move v1, v8

    .line 295
    goto :goto_0

    .line 297
    :cond_3
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->onTouchDown(Landroid/view/MotionEvent;)Z

    .line 299
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v2, v0

    .line 300
    .local v2, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v3, v0

    .line 302
    .local v3, "y":I
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsFastMove:Z

    .line 303
    iput-boolean v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsFastMoveFlag:Z

    .line 304
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mFastMovePoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 307
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    if-nez v0, :cond_4

    .line 308
    iput v8, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    .line 310
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mFastMovePoint:Landroid/graphics/Point;

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Point;->set(II)V

    .line 311
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->checkObjectPoint(IILjava/lang/Boolean;)I

    move-result v0

    if-ne v0, v8, :cond_5

    :cond_4
    :goto_1
    move v1, v8

    .line 327
    goto :goto_0

    .line 315
    :cond_5
    const-string/jumbo v0, "HYOHYUN"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "onSingleTouchDOWN mTouchStatus = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " x = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " Y = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    long-to-int v5, v4

    const/4 v0, -0x1

    invoke-static {p1, v0}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    move-object v0, p0

    move v4, v1

    invoke-direct/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onHIDAction(IIIIII)V

    .line 318
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0, v2, v3}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->isPointInObject(II)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 320
    iput-boolean v8, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsFastMoveFlag:Z

    .line 321
    new-instance v7, Ljava/util/GregorianCalendar;

    invoke-direct {v7}, Ljava/util/GregorianCalendar;-><init>()V

    .line 322
    .local v7, "cal":Ljava/util/GregorianCalendar;
    invoke-virtual {v7}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->prev_time:J

    goto :goto_1
.end method

.method public onTouchDrag(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 29
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 332
    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    .line 333
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v2

    if-nez v2, :cond_0

    .line 334
    const/4 v2, 0x0

    .line 460
    .end local p3    # "distanceX":F
    .end local p4    # "distanceY":F
    :goto_0
    return v2

    .line 337
    .restart local p3    # "distanceX":F
    .restart local p4    # "distanceY":F
    :cond_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    const/16 v3, 0x10

    if-ne v2, v3, :cond_1

    .line 339
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onPointerEvent(Landroid/view/MotionEvent;)V

    .line 340
    const/4 v2, 0x1

    goto :goto_0

    .line 343
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 344
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    .line 346
    :cond_2
    const-string/jumbo v2, "HYOHYUN"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "onSingleTouchDrag mTouchStatus = "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v6, " mIsFastMoveFlag = "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsFastMoveFlag:Z

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v4, v2

    .line 348
    .local v4, "x":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v5, v2

    .line 350
    .local v5, "y":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    const/4 v3, 0x5

    if-eq v2, v3, :cond_3

    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_3

    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_17

    .line 352
    :cond_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_7

    .line 353
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    .line 354
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v4, v5, v3}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->checkObjectPoint(IILjava/lang/Boolean;)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_6

    .line 457
    .end local p3    # "distanceX":F
    .end local p4    # "distanceY":F
    :cond_4
    :goto_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    const/16 v3, 0x9

    if-eq v2, v3, :cond_5

    .line 458
    const/4 v2, 0x1

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->updateCaretPos(ZZ)V

    .line 460
    :cond_5
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 357
    .restart local p3    # "distanceX":F
    .restart local p4    # "distanceY":F
    :cond_6
    const/4 v3, 0x0

    const/16 v6, 0x1b

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v7

    long-to-int v7, v7

    const/4 v2, -0x1

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v8

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onHIDAction(IIIIII)V

    goto :goto_1

    .line 359
    :cond_7
    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_8

    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_4

    .line 361
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/4 v3, 0x2

    if-ne v2, v3, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->isPenMode()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 394
    .end local p3    # "distanceX":F
    .end local p4    # "distanceY":F
    :cond_9
    :goto_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_14

    .line 395
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v11

    .line 396
    .local v11, "N":I
    if-lez v11, :cond_12

    const/16 v2, 0x64

    if-ge v11, v2, :cond_12

    .line 397
    const/16 v20, 0x0

    .local v20, "j":I
    :goto_3
    move/from16 v0, v20

    if-ge v0, v11, :cond_11

    .line 398
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenDragX:[I

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v3

    float-to-int v3, v3

    aput v3, v2, v20

    .line 399
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenDragY:[I

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v3

    float-to-int v3, v3

    aput v3, v2, v20

    .line 401
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenDragTime:[I

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    move-result-wide v6

    long-to-int v3, v6

    aput v3, v2, v20

    .line 402
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenPressure:[I

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-static {v0, v1}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v3

    aput v3, v2, v20

    .line 397
    add-int/lit8 v20, v20, 0x1

    goto :goto_3

    .line 363
    .end local v11    # "N":I
    .end local v20    # "j":I
    .restart local p3    # "distanceX":F
    .restart local p4    # "distanceY":F
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsFastMoveFlag:Z

    if-eqz v2, :cond_9

    .line 365
    new-instance v19, Ljava/util/GregorianCalendar;

    invoke-direct/range {v19 .. v19}, Ljava/util/GregorianCalendar;-><init>()V

    .line 366
    .local v19, "cal":Ljava/util/GregorianCalendar;
    invoke-virtual/range {v19 .. v19}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v21

    .line 368
    .local v21, "now_time":J
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->prev_time:J

    sub-long v2, v21, v2

    long-to-double v0, v2

    move-wide/from16 v23, v0

    .line 370
    .local v23, "time_diff":D
    const-wide/16 v25, 0x0

    .line 371
    .local v25, "vx":D
    invoke-static/range {p3 .. p3}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x40a00000    # 5.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_c

    .line 372
    const/4 v2, 0x0

    cmpg-float v2, p3, v2

    if-gez v2, :cond_b

    move/from16 v0, p3

    neg-float v0, v0

    move/from16 p3, v0

    .end local p3    # "distanceX":F
    :cond_b
    move/from16 v0, p3

    float-to-double v2, v0

    div-double v25, v2, v23

    .line 374
    :cond_c
    const-wide/16 v27, 0x0

    .line 375
    .local v27, "vy":D
    invoke-static/range {p4 .. p4}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x40a00000    # 5.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_e

    .line 376
    const/4 v2, 0x0

    cmpg-float v2, p4, v2

    if-gez v2, :cond_d

    move/from16 v0, p4

    neg-float v0, v0

    move/from16 p4, v0

    .end local p4    # "distanceY":F
    :cond_d
    move/from16 v0, p4

    float-to-double v2, v0

    div-double v27, v2, v23

    .line 378
    :cond_e
    const-wide v2, 0x3fe3333333333333L    # 0.6

    cmpl-double v2, v25, v2

    if-gtz v2, :cond_f

    const-wide v2, 0x3fe3333333333333L    # 0.6

    cmpl-double v2, v27, v2

    if-lez v2, :cond_10

    .line 380
    :cond_f
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsFastMove:Z

    .line 381
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->setFastMoveAction()V

    .line 382
    const/4 v7, 0x2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mFastMovePoint:Landroid/graphics/Point;

    iget v8, v2, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mFastMovePoint:Landroid/graphics/Point;

    iget v9, v2, Landroid/graphics/Point;->y:I

    const/4 v10, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    long-to-int v11, v2

    const/4 v2, -0x1

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v12

    move-object/from16 v6, p0

    invoke-direct/range {v6 .. v12}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onHIDAction(IIIIII)V

    .line 383
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mFastMovePoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    add-int/lit16 v8, v2, -0xbb8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mFastMovePoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    add-int/lit16 v9, v2, -0xbb8

    const/4 v10, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    long-to-int v11, v2

    const/4 v2, -0x1

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v12

    move-object/from16 v6, p0

    invoke-direct/range {v6 .. v12}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onHIDAction(IIIIII)V

    .line 389
    :goto_4
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsFastMoveFlag:Z

    goto/16 :goto_2

    .line 387
    :cond_10
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsFastMove:Z

    goto :goto_4

    .line 404
    .end local v19    # "cal":Ljava/util/GregorianCalendar;
    .end local v21    # "now_time":J
    .end local v23    # "time_diff":D
    .end local v25    # "vx":D
    .end local v27    # "vy":D
    .restart local v11    # "N":I
    .restart local v20    # "j":I
    :cond_11
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenDragX:[I

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenDragY:[I

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenDragTime:[I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenPressure:[I

    move-object/from16 v6, p0

    invoke-direct/range {v6 .. v11}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onSetPenPosition([I[I[I[II)V

    goto/16 :goto_1

    .line 406
    .end local v20    # "j":I
    :cond_12
    if-nez v11, :cond_13

    .line 407
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenDragX:[I

    const/4 v3, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v6, v6

    aput v6, v2, v3

    .line 408
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenDragY:[I

    const/4 v3, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v6, v6

    aput v6, v2, v3

    .line 410
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenDragTime:[I

    const/4 v3, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    long-to-int v6, v6

    aput v6, v2, v3

    .line 411
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenPressure:[I

    const/4 v3, 0x0

    const/4 v6, -0x1

    move-object/from16 v0, p2

    invoke-static {v0, v6}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    aput v6, v2, v3

    .line 413
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenDragX:[I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenDragY:[I

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenDragTime:[I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenPressure:[I

    move-object/from16 v16, v0

    const/16 v17, 0x1

    move-object/from16 v12, p0

    invoke-direct/range {v12 .. v17}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onSetPenPosition([I[I[I[II)V

    goto/16 :goto_1

    .line 416
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenDragX:[I

    const/4 v3, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v6, v6

    aput v6, v2, v3

    .line 417
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenDragY:[I

    const/4 v3, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v6, v6

    aput v6, v2, v3

    .line 419
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenDragTime:[I

    const/4 v3, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    long-to-int v6, v6

    aput v6, v2, v3

    .line 420
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenPressure:[I

    const/4 v3, 0x0

    const/4 v6, -0x1

    move-object/from16 v0, p2

    invoke-static {v0, v6}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    aput v6, v2, v3

    .line 422
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenDragX:[I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenDragY:[I

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenDragTime:[I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPenPressure:[I

    move-object/from16 v16, v0

    const/16 v17, 0x1

    move-object/from16 v12, p0

    invoke-direct/range {v12 .. v17}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onSetPenPosition([I[I[I[II)V

    .line 423
    const-string/jumbo v2, "HYOHYUN"

    const-string/jumbo v3, "ISetPenPosition default "

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 426
    .end local v11    # "N":I
    :cond_14
    move-object/from16 v0, p0

    iget v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    const/4 v3, 0x6

    if-eq v2, v3, :cond_4

    .line 430
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/4 v3, 0x2

    if-ne v2, v3, :cond_15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->isPenMode()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 432
    const/4 v3, 0x1

    const/4 v6, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v7

    long-to-int v7, v7

    const/4 v2, -0x1

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v8

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onHIDAction(IIIIII)V

    .line 433
    const-string/jumbo v2, "HYOHYUN"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "IHIDACTION MOVE x =  "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v6, " y="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 435
    :cond_15
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsFastMove:Z

    if-eqz v2, :cond_16

    .line 437
    const/4 v13, 0x1

    add-int/lit16 v14, v4, -0xbb8

    add-int/lit16 v15, v5, -0xbb8

    const/16 v16, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    long-to-int v0, v2

    move/from16 v17, v0

    const/4 v2, -0x1

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v18

    move-object/from16 v12, p0

    invoke-direct/range {v12 .. v18}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onHIDAction(IIIIII)V

    goto/16 :goto_1

    .line 442
    :cond_16
    const/4 v3, 0x1

    const/4 v6, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v7

    long-to-int v7, v7

    const/4 v2, -0x1

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v8

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onHIDAction(IIIIII)V

    .line 443
    const-string/jumbo v2, "HYOHYUN"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "IHIDACTION MOVE x =  "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v6, " y="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 452
    .restart local p3    # "distanceX":F
    .restart local p4    # "distanceY":F
    :cond_17
    const/16 v2, 0xd

    move-object/from16 v0, p0

    iget v3, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    if-ne v2, v3, :cond_4

    .line 453
    const-string/jumbo v2, "HYOHYUN"

    const-string/jumbo v3, "GESTURE_CHANGE_SCALE_END"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    goto/16 :goto_1
.end method

.method public onTouchUp(Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 466
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-nez v0, :cond_0

    .line 467
    const/4 v0, 0x1

    .line 625
    :goto_0
    return v0

    .line 469
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-nez v0, :cond_2

    .line 470
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 472
    :cond_2
    const-string/jumbo v0, "EvEditGestureProc"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onSingleTouchUp mTouchState ="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 476
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getEvTextToSpeechHelper()Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 477
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 478
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onGuideStartTextToSpeech()V

    .line 485
    :cond_3
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    const/16 v1, 0x10

    if-ne v0, v1, :cond_6

    .line 487
    invoke-direct {p0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onPointerEvent(Landroid/view/MotionEvent;)V

    .line 488
    const/4 v0, 0x1

    goto :goto_0

    .line 480
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 482
    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .line 491
    :cond_6
    const/4 v11, 0x0

    .line 493
    .local v11, "bShowPopup":Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v2, v0

    .line 494
    .local v2, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v3, v0

    .line 496
    .local v3, "y":I
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    const/16 v1, 0xb

    if-eq v0, v1, :cond_7

    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    const/16 v1, 0xd

    if-ne v0, v1, :cond_8

    .line 497
    :cond_7
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsFastMove:Z

    .line 498
    const/4 v1, 0x2

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    long-to-int v5, v5

    const/4 v0, -0x1

    invoke-static {p1, v0}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onHIDAction(IIIIII)V

    .line 499
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    .line 500
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->checkObjectPoint(IILjava/lang/Boolean;)I

    .line 501
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 508
    :cond_8
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_b

    .line 510
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mbFlickEnable:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_a

    .line 511
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_a

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mFastMovePoint:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v1, 0xa

    if-gt v0, v1, :cond_9

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mFastMovePoint:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v1, 0xa

    if-le v0, v1, :cond_a

    .line 513
    :cond_9
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->checkObjectPoint(IILjava/lang/Boolean;)I

    .line 514
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mFastMovePoint:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v1, v2

    iget-object v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mFastMovePoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int/2addr v4, v3

    invoke-virtual {v0, v1, v4}, Lcom/infraware/office/evengine/EvInterface;->IFlick(II)V

    .line 515
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    .line 516
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 520
    :cond_a
    const/4 v0, 0x2

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    .line 523
    :cond_b
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_13

    .line 525
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsFastMove:Z

    if-eqz v0, :cond_12

    .line 526
    const/4 v5, 0x2

    add-int/lit16 v6, v2, -0xbb8

    add-int/lit16 v7, v3, -0xbb8

    const/4 v8, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    long-to-int v9, v0

    const/4 v0, -0x1

    invoke-static {p1, v0}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v10

    move-object v4, p0

    invoke-direct/range {v4 .. v10}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onHIDAction(IIIIII)V

    .line 537
    :cond_c
    :goto_1
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    if-nez v0, :cond_d

    .line 538
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    iget-boolean v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsMultiSelectionMode:Z

    invoke-virtual {v0, v2, v3, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->checkPopupShow(IIZ)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 540
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    iget-boolean v0, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mbCroppingMode:Z

    if-nez v0, :cond_d

    .line 541
    const/4 v11, 0x1

    .line 544
    :cond_d
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->checkObjectPoint(IILjava/lang/Boolean;)I

    .line 563
    :cond_e
    :goto_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsFastMove:Z

    .line 566
    const/4 v0, 0x1

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->updateCaretPos(ZZ)V

    .line 568
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    if-nez v0, :cond_11

    .line 570
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_15

    .line 571
    iget-object v12, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v12, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .line 574
    .local v12, "sheetEditorActivity":Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    if-eqz v11, :cond_14

    .line 576
    invoke-virtual {v12}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getFilterProcessing()Z

    move-result v0

    if-nez v0, :cond_10

    .line 577
    invoke-virtual {v12}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->dismissFilterPopup()V

    .line 579
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetSheetHyperLinkInfo()Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    .line 580
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    iget v0, v0, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;->nType:I

    if-nez v0, :cond_10

    .line 581
    :cond_f
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->show()V

    .line 582
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setTouchUpShowFlag(Z)V

    .line 583
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->m_bSheetPopupMenu:Z

    .line 593
    :cond_10
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->show()V

    .line 594
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setTouchUpShowFlag(Z)V

    .line 595
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->m_bSheetPopupMenu:Z

    .line 624
    .end local v12    # "sheetEditorActivity":Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    :cond_11
    :goto_3
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    .line 625
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 530
    :cond_12
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActionMode:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_c

    .line 534
    const/4 v1, 0x2

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    long-to-int v5, v5

    const/4 v0, -0x1

    invoke-static {p1, v0}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onHIDAction(IIIIII)V

    goto/16 :goto_1

    .line 546
    :cond_13
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_e

    .line 548
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsFastMove:Z

    .line 551
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->checkObjectPoint(IILjava/lang/Boolean;)I

    .line 553
    const/4 v11, 0x1

    .line 555
    const/4 v1, 0x2

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    long-to-int v5, v5

    const/4 v0, -0x1

    invoke-static {p1, v0}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onHIDAction(IIIIII)V

    goto/16 :goto_2

    .line 599
    .restart local v12    # "sheetEditorActivity":Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    :cond_14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->m_bSheetPopupMenu:Z

    goto :goto_3

    .line 606
    .end local v12    # "sheetEditorActivity":Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    :cond_15
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v0

    const/16 v1, 0x10

    if-ne v0, v1, :cond_16

    .line 608
    const/4 v11, 0x0

    goto :goto_3

    .line 612
    :cond_16
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetHyperLinkInfo_Editor()Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    .line 614
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    iget v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->bUse:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_17

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_11

    .line 616
    :cond_17
    if-eqz v11, :cond_11

    .line 618
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->show()V

    .line 619
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setTouchUpShowFlag(Z)V

    goto :goto_3
.end method

.method public onUpdateSelection(I)V
    .locals 3
    .param p1, "nPos"    # I

    .prologue
    .line 1181
    const-string/jumbo v0, "EvEditGestureProc"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onUpdateSelection : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1182
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getSavedCaretPos()I

    move-result v0

    if-eq p1, v0, :cond_0

    .line 1183
    invoke-virtual {p0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->setSavedCaretPos(I)V

    .line 1184
    :cond_0
    return-void
.end method

.method public sendEmptyCommit(Z)V
    .locals 4
    .param p1, "bPrevClear"    # Z

    .prologue
    const/4 v3, 0x0

    .line 1133
    const-string/jumbo v0, "EvEditGestureProc"

    const-string/jumbo v1, "sendEmptyCommit"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1134
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPrevText:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPrevText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 1135
    if-eqz p1, :cond_0

    .line 1136
    const-string/jumbo v0, ""

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->setPrevText(Ljava/lang/CharSequence;)V

    .line 1137
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const-string/jumbo v1, ""

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, v3, v3}, Lcom/infraware/office/evengine/EvInterface;->IInsertString(Ljava/lang/String;III)V

    .line 1139
    :cond_1
    invoke-virtual {p0, v3}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->setPrevComposing(Z)V

    .line 1140
    return-void
.end method

.method public setPrevComposing(Z)V
    .locals 3
    .param p1, "bComposing"    # Z

    .prologue
    .line 1162
    const-string/jumbo v0, "EvEditGestureProc"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setPrevComposing : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1163
    iput-boolean p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mbPrevComposing:Z

    .line 1164
    return-void
.end method

.method public setPrevComposingOrg(Z)V
    .locals 3
    .param p1, "bComposing"    # Z

    .prologue
    .line 1167
    const-string/jumbo v0, "EvEditGestureProc"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setPrevComposingOrg : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1168
    iput-boolean p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mbPrevComposingOrg:Z

    .line 1169
    return-void
.end method

.method public setPrevText(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 1143
    const-string/jumbo v0, "EvEditGestureProc"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setPrevText : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1144
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mPrevText:Ljava/lang/String;

    .line 1147
    return-void
.end method

.method public setSavedCaretPos(I)V
    .locals 3
    .param p1, "nPos"    # I

    .prologue
    .line 1172
    const-string/jumbo v0, "EvEditGestureProc"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setSavedCaretPos : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1173
    iput p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mSavedCaretPos:I

    .line 1174
    return-void
.end method

.method public updateCaretPos(ZZ)V
    .locals 16
    .param p1, "bSoftCommit"    # Z
    .param p2, "bHardCommit"    # Z

    .prologue
    .line 1301
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getView()Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1340
    :cond_0
    :goto_0
    return-void

    .line 1302
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/infraware/office/util/EvUtil;->getInputMethodManager(Landroid/content/Context;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v15

    .line 1303
    .local v15, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v15, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v15, v1}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1304
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->getBatchEditNesting()I

    move-result v13

    .line 1305
    .local v13, "batchEdit":I
    const-string/jumbo v1, "EvEditGestureProc"

    const-string/jumbo v2, "============================================="

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1306
    const-string/jumbo v1, "EvEditGestureProc"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "updateCaretPos bSoftCommit="

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v7, " bHardCommit="

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v7, " batchEdit="

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1307
    if-nez v13, :cond_0

    .line 1309
    const/4 v1, 0x1

    move/from16 v0, p1

    if-ne v0, v1, :cond_3

    const/4 v1, 0x1

    move/from16 v0, p2

    if-ne v0, v1, :cond_3

    .line 1310
    move-object/from16 v0, p0

    iget v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mTouchStatus:I

    if-nez v1, :cond_2

    .line 1311
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, 0x1

    const/4 v12, -0x1

    invoke-virtual/range {v1 .. v12}, Lcom/infraware/office/evengine/EvInterface;->ISetCompBackColor(IIIIJJIII)V

    .line 1313
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->isPrevComposing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1314
    const-string/jumbo v1, "EvEditGestureProc"

    const-string/jumbo v2, "updateCaretPos restartInput"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1316
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getView()Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, -0x1

    const/4 v6, -0x1

    move-object v1, v15

    invoke-virtual/range {v1 .. v6}, Landroid/view/inputmethod/InputMethodManager;->updateSelection(Landroid/view/View;IIII)V

    .line 1317
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v15, v1}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    .line 1321
    :cond_3
    const/4 v1, 0x1

    move/from16 v0, p2

    if-ne v0, v1, :cond_4

    .line 1322
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->clearEditable()V

    .line 1323
    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->sendEmptyCommit(Z)V

    .line 1326
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->getEditableText()Landroid/text/Editable;

    move-result-object v14

    .line 1327
    .local v14, "editable":Landroid/text/Editable;
    invoke-static {v14}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v3

    .line 1328
    .local v3, "selStart":I
    invoke-static {v14}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v4

    .line 1329
    .local v4, "selEnd":I
    const/4 v5, -0x1

    .line 1330
    .local v5, "candStart":I
    const/4 v6, -0x1

    .line 1331
    .local v6, "candEnd":I
    invoke-interface {v14}, Landroid/text/Editable;->length()I

    move-result v1

    if-lez v1, :cond_5

    if-nez p1, :cond_5

    .line 1332
    invoke-static {v14}, Lcom/infraware/office/baseframe/EvInputConnection;->getComposingSpanStart(Landroid/text/Spannable;)I

    move-result v5

    .line 1333
    invoke-static {v14}, Lcom/infraware/office/baseframe/EvInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    move-result v6

    .line 1336
    :cond_5
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->setSavedCaretPos(I)V

    .line 1337
    const-string/jumbo v1, "EvEditGestureProc"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "updateCaretPos selStart="

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v7, " sedEnd="

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v7, " candStart="

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v7, " candEnd="

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1338
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->getView()Landroid/view/View;

    move-result-object v2

    move-object v1, v15

    invoke-virtual/range {v1 .. v6}, Landroid/view/inputmethod/InputMethodManager;->updateSelection(Landroid/view/View;IIII)V

    goto/16 :goto_0
.end method
