.class public Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;
.super Ljava/lang/Object;
.source "EvBaseGestureProc.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_OBJECT_EDITING_TYPE;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TransformInfoLayout"
.end annotation


# static fields
.field public static final TRANSFORMINFO_HEIGHT:I = 0x2

.field protected static final TRANSFORMINFO_INVISIBLE:I = 0x0

.field public static final TRANSFORMINFO_ROTATE:I = 0x4

.field protected static final TRANSFORMINFO_SETTEXT:I = 0x2

.field protected static final TRANSFORMINFO_VISIBLE:I = 0x1

.field public static final TRANSFORMINFO_WIDTH:I = 0x1

.field public static final TRANSFORMINFO_WIDTHHEIGHT:I = 0x3


# instance fields
.field protected mActivity:Landroid/app/Activity;

.field protected mHandler:Landroid/os/Handler;

.field private mString1:Ljava/lang/String;

.field private mString2:Ljava/lang/String;

.field protected mTransformInfoLayout:Landroid/widget/FrameLayout;

.field protected mTransformInfoType:I

.field protected mTransformInfomMid:Landroid/widget/TextView;

.field protected mTransformInfomText1:Landroid/widget/TextView;

.field protected mTransformInfomText2:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;


# direct methods
.method public constructor <init>(Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;Landroid/app/Activity;)V
    .locals 3
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v0, 0x0

    .line 889
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 849
    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mActivity:Landroid/app/Activity;

    .line 850
    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfoLayout:Landroid/widget/FrameLayout;

    .line 851
    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfomText1:Landroid/widget/TextView;

    .line 852
    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfomMid:Landroid/widget/TextView;

    .line 853
    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfomText2:Landroid/widget/TextView;

    .line 864
    const/4 v0, 0x3

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfoType:I

    .line 869
    new-instance v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout$1;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout$1;-><init>(Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mHandler:Landroid/os/Handler;

    .line 890
    iput-object p2, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mActivity:Landroid/app/Activity;

    .line 897
    const v0, 0x7f0b0124

    invoke-virtual {p2, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfoLayout:Landroid/widget/FrameLayout;

    .line 899
    invoke-virtual {p2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030059

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfoLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 901
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfoLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b0275

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfomText1:Landroid/widget/TextView;

    .line 902
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfoLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b0276

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfomMid:Landroid/widget/TextView;

    .line 903
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfoLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b0277

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfomText2:Landroid/widget/TextView;

    .line 904
    return-void
.end method

.method static synthetic access$500(Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;I)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;
    .param p1, "x1"    # I

    .prologue
    .line 847
    invoke-direct {p0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->setVisibility(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;

    .prologue
    .line 847
    invoke-direct {p0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->setText()V

    return-void
.end method

.method private setText()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 914
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfoType:I

    packed-switch v0, :pswitch_data_0

    .line 943
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfomText1:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 944
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfomMid:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 945
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfomText2:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 949
    :goto_0
    return-void

    .line 918
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfomText1:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mString1:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 919
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfomText1:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 920
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfomMid:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 921
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfomText2:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 924
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfomText2:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mString2:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 925
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfomText1:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 926
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfomMid:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 927
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfomText2:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 930
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfomText1:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mString1:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 931
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfomText2:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mString2:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 932
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfomText1:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 933
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfomMid:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 934
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfomText2:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 937
    :pswitch_3
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfomText1:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mString1:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 938
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfomText1:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 939
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfomMid:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 940
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfomText2:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 914
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private setVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 952
    if-nez p1, :cond_0

    .line 953
    invoke-direct {p0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->setText()V

    .line 955
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfoLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_1

    .line 956
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfoLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 957
    :cond_1
    return-void
.end method


# virtual methods
.method public TransformInfofinalize()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 907
    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mActivity:Landroid/app/Activity;

    .line 908
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfoLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 909
    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfoLayout:Landroid/widget/FrameLayout;

    .line 910
    return-void
.end method

.method public setPDFParam(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V
    .locals 6
    .param p1, "param"    # Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;

    .prologue
    const/4 v4, 0x3

    const/high16 v5, 0x41200000    # 10.0f

    .line 960
    iget-object v0, p1, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    .line 964
    .local v0, "a":Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;
    iget v3, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->eEditing:I

    packed-switch v3, :pswitch_data_0

    .line 977
    iput v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfoType:I

    .line 979
    iget-object v3, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startPointFromPage:Landroid/graphics/Point;

    iget v1, v3, Landroid/graphics/Point;->x:I

    .line 980
    .local v1, "temp_i":I
    int-to-float v3, v1

    div-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    .line 981
    .local v2, "temp_s":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "W : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "cm"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mString1:Ljava/lang/String;

    .line 983
    iget-object v3, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startPointFromPage:Landroid/graphics/Point;

    iget v1, v3, Landroid/graphics/Point;->y:I

    .line 984
    int-to-float v3, v1

    div-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    .line 985
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "H : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "cm"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mString2:Ljava/lang/String;

    .line 988
    :goto_0
    iget v3, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->eEditing:I

    if-nez v3, :cond_0

    .line 989
    const/16 v3, 0x8

    invoke-direct {p0, v3}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->setVisibility(I)V

    .line 992
    :goto_1
    return-void

    .line 966
    .end local v1    # "temp_i":I
    .end local v2    # "temp_s":Ljava/lang/String;
    :pswitch_0
    iput v4, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfoType:I

    .line 967
    iget-object v3, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startPointFromPage:Landroid/graphics/Point;

    iget v1, v3, Landroid/graphics/Point;->x:I

    .line 969
    .restart local v1    # "temp_i":I
    int-to-float v3, v1

    div-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    .line 970
    .restart local v2    # "temp_s":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "X : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "cm"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mString1:Ljava/lang/String;

    .line 972
    iget-object v3, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startPointFromPage:Landroid/graphics/Point;

    iget v1, v3, Landroid/graphics/Point;->y:I

    .line 973
    int-to-float v3, v1

    div-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    .line 974
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Y : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "cm"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mString2:Ljava/lang/String;

    goto :goto_0

    .line 991
    :cond_0
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->setVisibility(I)V

    goto :goto_1

    .line 964
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public setParam(Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;)V
    .locals 25
    .param p1, "param"    # Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;

    .prologue
    .line 996
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    .line 997
    .local v2, "a":Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;
    iget-object v6, v2, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->endPoint:Landroid/graphics/Point;

    .line 998
    .local v6, "endPoint":Landroid/graphics/Point;
    iget-object v12, v2, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startPoint:Landroid/graphics/Point;

    .line 999
    .local v12, "staPoint":Landroid/graphics/Point;
    iget v15, v2, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nObjectType:I

    .line 1000
    .local v15, "type":I
    iget v3, v2, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nObjectEditInfo:I

    .line 1001
    .local v3, "editInfo":I
    iget-object v10, v2, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->sObjectSize:Landroid/graphics/Point;

    .line 1002
    .local v10, "oSize":Landroid/graphics/Point;
    iget-object v7, v2, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->startPointFromPage:Landroid/graphics/Point;

    .line 1003
    .local v7, "fromPagePoint":Landroid/graphics/Point;
    iget-object v5, v2, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingStartPoint:Landroid/graphics/Point;

    .line 1004
    .local v5, "editingStart":Landroid/graphics/Point;
    iget-object v4, v2, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->editingEndPoint:Landroid/graphics/Point;

    .line 1006
    .local v4, "editingEnd":Landroid/graphics/Point;
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY;->ptObjRange:Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v11, v0, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->nEditingAngle:I

    .line 1012
    .local v11, "rotate":I
    iget v0, v6, Landroid/graphics/Point;->x:I

    move/from16 v19, v0

    iget v0, v12, Landroid/graphics/Point;->x:I

    move/from16 v20, v0

    sub-int v19, v19, v20

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    iget v0, v10, Landroid/graphics/Point;->x:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    div-float v16, v19, v20

    .line 1013
    .local v16, "unit":F
    iget v0, v2, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->eEditing:I

    move/from16 v19, v0

    packed-switch v19, :pswitch_data_0

    .line 1089
    :goto_0
    iget v0, v2, Lcom/infraware/office/evengine/EV$EDITOR_OBJECT_POINTARRAY$EDIT_OBJECT_RANGE;->eEditing:I

    move/from16 v19, v0

    if-nez v19, :cond_0

    .line 1090
    const/16 v19, 0x8

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->setVisibility(I)V

    .line 1093
    :goto_1
    return-void

    .line 1016
    :pswitch_0
    const/16 v19, 0x3

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfoType:I

    .line 1017
    iget v0, v7, Landroid/graphics/Point;->x:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    mul-float v19, v19, v16

    iget v0, v12, Landroid/graphics/Point;->x:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    sub-float v19, v19, v20

    iget v0, v5, Landroid/graphics/Point;->x:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    add-float v19, v19, v20

    div-float v19, v19, v16

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v19, v0

    const-wide/high16 v21, 0x3fe0000000000000L    # 0.5

    add-double v19, v19, v21

    move-wide/from16 v0, v19

    double-to-int v13, v0

    .line 1019
    .local v13, "temp_i":I
    int-to-float v0, v13

    move/from16 v19, v0

    const/high16 v20, 0x41200000    # 10.0f

    div-float v19, v19, v20

    invoke-static/range {v19 .. v19}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v14

    .line 1020
    .local v14, "temp_s":Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "X : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, "cm"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mString1:Ljava/lang/String;

    .line 1022
    iget v0, v7, Landroid/graphics/Point;->y:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    mul-float v19, v19, v16

    iget v0, v12, Landroid/graphics/Point;->y:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    sub-float v19, v19, v20

    iget v0, v5, Landroid/graphics/Point;->y:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    add-float v19, v19, v20

    div-float v19, v19, v16

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v19, v0

    const-wide/high16 v21, 0x3fe0000000000000L    # 0.5

    add-double v19, v19, v21

    move-wide/from16 v0, v19

    double-to-int v13, v0

    .line 1023
    int-to-float v0, v13

    move/from16 v19, v0

    const/high16 v20, 0x41200000    # 10.0f

    div-float v19, v19, v20

    invoke-static/range {v19 .. v19}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v14

    .line 1024
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "Y : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, "cm"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mString2:Ljava/lang/String;

    goto/16 :goto_0

    .line 1028
    .end local v13    # "temp_i":I
    .end local v14    # "temp_s":Ljava/lang/String;
    :pswitch_1
    packed-switch v15, :pswitch_data_1

    .line 1068
    :pswitch_2
    const/16 v19, 0x3

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfoType:I

    .line 1070
    iget v0, v4, Landroid/graphics/Point;->x:I

    move/from16 v19, v0

    iget v0, v5, Landroid/graphics/Point;->x:I

    move/from16 v20, v0

    sub-int v19, v19, v20

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    div-float v19, v19, v16

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v19, v0

    const-wide/high16 v21, 0x3fe0000000000000L    # 0.5

    add-double v19, v19, v21

    move-wide/from16 v0, v19

    double-to-int v13, v0

    .line 1071
    .restart local v13    # "temp_i":I
    int-to-float v0, v13

    move/from16 v19, v0

    const/high16 v20, 0x41200000    # 10.0f

    div-float v19, v19, v20

    invoke-static/range {v19 .. v19}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v14

    .line 1072
    .restart local v14    # "temp_s":Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "W : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, "cm"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mString1:Ljava/lang/String;

    .line 1074
    iget v0, v4, Landroid/graphics/Point;->y:I

    move/from16 v19, v0

    iget v0, v5, Landroid/graphics/Point;->y:I

    move/from16 v20, v0

    sub-int v19, v19, v20

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    div-float v19, v19, v16

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v19, v0

    const-wide/high16 v21, 0x3fe0000000000000L    # 0.5

    add-double v19, v19, v21

    move-wide/from16 v0, v19

    double-to-int v13, v0

    .line 1075
    int-to-float v0, v13

    move/from16 v19, v0

    const/high16 v20, 0x41200000    # 10.0f

    div-float v19, v19, v20

    invoke-static/range {v19 .. v19}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v14

    .line 1076
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "H : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, "cm"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mString2:Ljava/lang/String;

    goto/16 :goto_0

    .line 1032
    .end local v13    # "temp_i":I
    .end local v14    # "temp_s":Ljava/lang/String;
    :pswitch_3
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfoType:I

    .line 1033
    iget v0, v4, Landroid/graphics/Point;->x:I

    move/from16 v19, v0

    iget v0, v5, Landroid/graphics/Point;->x:I

    move/from16 v20, v0

    sub-int v17, v19, v20

    .line 1034
    .local v17, "x_len":I
    iget v0, v4, Landroid/graphics/Point;->y:I

    move/from16 v19, v0

    iget v0, v5, Landroid/graphics/Point;->y:I

    move/from16 v20, v0

    sub-int v18, v19, v20

    .line 1036
    .local v18, "y_len":I
    move/from16 v0, v17

    int-to-double v0, v0

    move-wide/from16 v19, v0

    const-wide/high16 v21, 0x4000000000000000L    # 2.0

    invoke-static/range {v19 .. v22}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v19

    move/from16 v0, v18

    int-to-double v0, v0

    move-wide/from16 v21, v0

    const-wide/high16 v23, 0x4000000000000000L    # 2.0

    invoke-static/range {v21 .. v24}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v21

    add-double v19, v19, v21

    const-wide/high16 v21, 0x3fe0000000000000L    # 0.5

    invoke-static/range {v19 .. v22}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    .line 1037
    .local v8, "line_len":D
    move/from16 v0, v16

    float-to-double v0, v0

    move-wide/from16 v19, v0

    div-double v19, v8, v19

    const-wide/high16 v21, 0x3fe0000000000000L    # 0.5

    add-double v19, v19, v21

    move-wide/from16 v0, v19

    double-to-int v13, v0

    .line 1038
    .restart local v13    # "temp_i":I
    int-to-float v0, v13

    move/from16 v19, v0

    const/high16 v20, 0x41200000    # 10.0f

    div-float v19, v19, v20

    invoke-static/range {v19 .. v19}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v14

    .line 1039
    .restart local v14    # "temp_s":Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "L : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, "cm"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mString1:Ljava/lang/String;

    goto/16 :goto_0

    .line 1045
    .end local v8    # "line_len":D
    .end local v13    # "temp_i":I
    .end local v14    # "temp_s":Ljava/lang/String;
    .end local v17    # "x_len":I
    .end local v18    # "y_len":I
    :pswitch_4
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfoType:I

    .line 1047
    iget v0, v4, Landroid/graphics/Point;->y:I

    move/from16 v19, v0

    iget v0, v5, Landroid/graphics/Point;->y:I

    move/from16 v20, v0

    sub-int v18, v19, v20

    .line 1049
    .restart local v18    # "y_len":I
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v19, v0

    div-float v19, v19, v16

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v19, v0

    const-wide/high16 v21, 0x3fe0000000000000L    # 0.5

    add-double v19, v19, v21

    move-wide/from16 v0, v19

    double-to-int v13, v0

    .line 1050
    .restart local v13    # "temp_i":I
    int-to-float v0, v13

    move/from16 v19, v0

    const/high16 v20, 0x41200000    # 10.0f

    div-float v19, v19, v20

    invoke-static/range {v19 .. v19}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v14

    .line 1051
    .restart local v14    # "temp_s":Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "H : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, "cm"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mString1:Ljava/lang/String;

    goto/16 :goto_0

    .line 1056
    .end local v13    # "temp_i":I
    .end local v14    # "temp_s":Ljava/lang/String;
    .end local v18    # "y_len":I
    :pswitch_5
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfoType:I

    .line 1057
    iget v0, v4, Landroid/graphics/Point;->x:I

    move/from16 v19, v0

    iget v0, v5, Landroid/graphics/Point;->x:I

    move/from16 v20, v0

    sub-int v17, v19, v20

    .line 1060
    .restart local v17    # "x_len":I
    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v19, v0

    div-float v19, v19, v16

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v19, v0

    const-wide/high16 v21, 0x3fe0000000000000L    # 0.5

    add-double v19, v19, v21

    move-wide/from16 v0, v19

    double-to-int v13, v0

    .line 1061
    .restart local v13    # "temp_i":I
    int-to-float v0, v13

    move/from16 v19, v0

    const/high16 v20, 0x41200000    # 10.0f

    div-float v19, v19, v20

    invoke-static/range {v19 .. v19}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v14

    .line 1062
    .restart local v14    # "temp_s":Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "W : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, "cm"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mString1:Ljava/lang/String;

    goto/16 :goto_0

    .line 1083
    .end local v13    # "temp_i":I
    .end local v14    # "temp_s":Ljava/lang/String;
    .end local v17    # "x_len":I
    :pswitch_6
    const/16 v19, 0x4

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mTransformInfoType:I

    .line 1085
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const/16 v20, 0xba

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->mString1:Ljava/lang/String;

    goto/16 :goto_0

    .line 1092
    :cond_0
    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$TransformInfoLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 1013
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_6
    .end packed-switch

    .line 1028
    :pswitch_data_1
    .packed-switch 0x9
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
