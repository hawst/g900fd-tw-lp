.class Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;
.super Ljava/lang/Object;
.source "EvClipboardExManager.java"

# interfaces
.implements Landroid/sec/clipboard/IClipboardDataPasteEvent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/porting/EvClipboardExManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "IClipboardDataPasteEventImpl"
.end annotation


# instance fields
.field private final mBinder:Landroid/sec/clipboard/IClipboardDataPasteEvent$Stub;

.field final synthetic this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)V
    .locals 1

    .prologue
    .line 80
    iput-object p1, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    new-instance v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl$1;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl$1;-><init>(Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->mBinder:Landroid/sec/clipboard/IClipboardDataPasteEvent$Stub;

    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->mBinder:Landroid/sec/clipboard/IClipboardDataPasteEvent$Stub;

    return-object v0
.end method

.method public onClipboardDataPaste(Landroid/sec/clipboard/data/ClipboardData;)V
    .locals 23
    .param p1, "data"    # Landroid/sec/clipboard/data/ClipboardData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 107
    const/16 v20, 0x0

    .line 108
    .local v20, "textCS":Ljava/lang/CharSequence;
    const/4 v3, 0x0

    .line 110
    .local v3, "strText":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    # getter for: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mDocType:I
    invoke-static {v1}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$100(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)I

    move-result v15

    .line 112
    .local v15, "cur_doctype":I
    invoke-virtual/range {p1 .. p1}, Landroid/sec/clipboard/data/ClipboardData;->GetFomat()I

    move-result v16

    .line 114
    .local v16, "format":I
    packed-switch v16, :pswitch_data_0

    .line 222
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    # getter for: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {v1}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$900(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/sec/clipboard/ClipboardExManager;->dismissUIDataDialog()V

    .line 223
    return-void

    :pswitch_0
    move-object/from16 v21, p1

    .line 117
    check-cast v21, Landroid/sec/clipboard/data/list/ClipboardDataText;

    .line 118
    .local v21, "txt":Landroid/sec/clipboard/data/list/ClipboardDataText;
    invoke-virtual/range {v21 .. v21}, Landroid/sec/clipboard/data/list/ClipboardDataText;->GetText()Ljava/lang/CharSequence;

    move-result-object v20

    .line 121
    invoke-interface/range {v20 .. v20}, Ljava/lang/CharSequence;->length()I

    move-result v19

    .line 122
    .local v19, "length":I
    if-lez v19, :cond_1

    add-int/lit8 v1, v19, -0x1

    move-object/from16 v0, v20

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isSurrogate(C)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 123
    const/4 v1, 0x1

    move/from16 v0, v19

    if-ne v0, v1, :cond_2

    const/16 v20, 0x0

    .line 128
    :cond_1
    :goto_1
    if-eqz v20, :cond_0

    .line 130
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 131
    if-eqz v3, :cond_0

    .line 135
    const/4 v1, 0x2

    if-ne v15, v1, :cond_4

    .line 137
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    # getter for: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mBackupCaller:I
    invoke-static {v1}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$200(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)I

    move-result v1

    if-ne v1, v15, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    # getter for: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mBackupText:Ljava/lang/String;
    invoke-static {v1}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$300(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    # getter for: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mBackupText:Ljava/lang/String;
    invoke-static {v1}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$300(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    # getter for: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mBackupType:I
    invoke-static {v1}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$400(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)I

    move-result v1

    if-nez v1, :cond_3

    .line 142
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    iget-object v7, v1, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->messageHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    const/4 v2, 0x0

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    # getter for: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mIsValue:Z
    invoke-static {v5}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$500(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)Z

    move-result v5

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    # getter for: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mIsFormat:Z
    invoke-static {v8}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$600(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)Z

    move-result v6

    # invokes: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->pasteMsg(ILjava/lang/String;ZZZ)Landroid/os/Message;
    invoke-static/range {v1 .. v6}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$700(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;ILjava/lang/String;ZZZ)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 124
    :cond_2
    add-int/lit8 v1, v19, -0x2

    move-object/from16 v0, v20

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    add-int/lit8 v2, v19, -0x1

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    move-result v1

    if-nez v1, :cond_1

    .line 125
    const/4 v1, 0x0

    add-int/lit8 v2, v19, -0x1

    move-object/from16 v0, v20

    invoke-interface {v0, v1, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v20

    goto :goto_1

    .line 146
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    iget-object v7, v1, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->messageHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    # getter for: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mIsValue:Z
    invoke-static {v5}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$500(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)Z

    move-result v5

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    # getter for: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mIsFormat:Z
    invoke-static {v8}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$600(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)Z

    move-result v6

    # invokes: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->pasteMsg(ILjava/lang/String;ZZZ)Landroid/os/Message;
    invoke-static/range {v1 .. v6}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$700(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;ILjava/lang/String;ZZZ)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 152
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    iget-object v7, v1, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->messageHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    # invokes: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->pasteMsg(ILjava/lang/String;ZZZ)Landroid/os/Message;
    invoke-static/range {v1 .. v6}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$700(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;ILjava/lang/String;ZZZ)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .end local v19    # "length":I
    .end local v21    # "txt":Landroid/sec/clipboard/data/list/ClipboardDataText;
    :pswitch_1
    move-object/from16 v17, p1

    .line 157
    check-cast v17, Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;

    .line 158
    .local v17, "html":Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;
    invoke-virtual/range {v17 .. v17}, Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;->GetHTMLFragment()Ljava/lang/CharSequence;

    move-result-object v18

    .line 160
    .local v18, "htmlCS":Ljava/lang/CharSequence;
    if-eqz v18, :cond_0

    .line 162
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 163
    .local v6, "strHtml":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 165
    const/4 v1, 0x2

    if-ne v15, v1, :cond_5

    .line 167
    const/4 v1, 0x2

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;->GetAlternateFormat(I)Landroid/sec/clipboard/data/ClipboardData;

    move-result-object v22

    check-cast v22, Landroid/sec/clipboard/data/list/ClipboardDataText;

    .line 169
    .local v22, "txtfromhtml":Landroid/sec/clipboard/data/list/ClipboardDataText;
    if-eqz v22, :cond_0

    .line 171
    invoke-virtual/range {v22 .. v22}, Landroid/sec/clipboard/data/list/ClipboardDataText;->GetText()Ljava/lang/CharSequence;

    move-result-object v20

    .line 172
    if-eqz v20, :cond_0

    .line 174
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 175
    if-eqz v3, :cond_0

    .line 177
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    iget-object v7, v1, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->messageHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    # invokes: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->pasteMsg(ILjava/lang/String;ZZZ)Landroid/os/Message;
    invoke-static/range {v1 .. v6}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$700(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;ILjava/lang/String;ZZZ)Landroid/os/Message;

    .end local v6    # "strHtml":Ljava/lang/String;
    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 181
    .end local v22    # "txtfromhtml":Landroid/sec/clipboard/data/list/ClipboardDataText;
    .restart local v6    # "strHtml":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    # getter for: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v1}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$000(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetBWPOpInfo_Editor()Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    move-result-object v13

    .line 182
    .local v13, "aInfo":Lcom/infraware/office/evengine/EV$BWP_OP_INFO;
    iget v1, v13, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nStatusOP:I

    const/high16 v2, 0x1000000

    and-int/2addr v1, v2

    if-eqz v1, :cond_6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    # getter for: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mBackupData:Ljava/lang/String;
    invoke-static {v1}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$800(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    # getter for: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mBackupData:Ljava/lang/String;
    invoke-static {v1}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$800(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    # getter for: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mBackupType:I
    invoke-static {v1}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$400(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    # getter for: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mBackupCaller:I
    invoke-static {v1}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$200(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)I

    move-result v1

    if-ne v1, v15, :cond_6

    .line 187
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    iget-object v1, v1, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->messageHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    const/4 v5, 0x1

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    # invokes: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->pasteMsg(ILjava/lang/String;ZZZ)Landroid/os/Message;
    invoke-static/range {v4 .. v9}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$700(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;ILjava/lang/String;ZZZ)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 191
    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    iget-object v1, v1, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->messageHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    const/4 v5, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    # invokes: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->pasteMsg(ILjava/lang/String;ZZZ)Landroid/os/Message;
    invoke-static/range {v4 .. v9}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$700(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;ILjava/lang/String;ZZZ)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .end local v6    # "strHtml":Ljava/lang/String;
    .end local v13    # "aInfo":Lcom/infraware/office/evengine/EV$BWP_OP_INFO;
    .end local v17    # "html":Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;
    .end local v18    # "htmlCS":Ljava/lang/CharSequence;
    :pswitch_2
    move-object/from16 v14, p1

    .line 196
    check-cast v14, Landroid/sec/clipboard/data/list/ClipboardDataBitmap;

    .line 197
    .local v14, "bmp":Landroid/sec/clipboard/data/list/ClipboardDataBitmap;
    invoke-virtual {v14}, Landroid/sec/clipboard/data/list/ClipboardDataBitmap;->GetBitmapPath()Ljava/lang/String;

    move-result-object v9

    .line 199
    .local v9, "img_path":Ljava/lang/String;
    const/4 v1, 0x2

    if-ne v15, v1, :cond_7

    .line 201
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    iget-object v1, v1, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->messageHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    const/4 v8, 0x2

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    # invokes: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->pasteMsg(ILjava/lang/String;ZZZ)Landroid/os/Message;
    invoke-static/range {v7 .. v12}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$700(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;ILjava/lang/String;ZZZ)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 205
    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    # getter for: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v1}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$000(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetBWPOpInfo_Editor()Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    move-result-object v13

    .line 206
    .restart local v13    # "aInfo":Lcom/infraware/office/evengine/EV$BWP_OP_INFO;
    iget v1, v13, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nStatusOP:I

    const/high16 v2, 0x1000000

    and-int/2addr v1, v2

    if-eqz v1, :cond_8

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    # getter for: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mBackupData:Ljava/lang/String;
    invoke-static {v1}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$800(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    # getter for: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mBackupData:Ljava/lang/String;
    invoke-static {v1}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$800(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    # getter for: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mBackupType:I
    invoke-static {v1}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$400(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    # getter for: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mBackupCaller:I
    invoke-static {v1}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$200(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)I

    move-result v1

    if-ne v1, v15, :cond_8

    .line 211
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    iget-object v1, v1, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->messageHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    const/4 v8, 0x2

    const/4 v10, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    # invokes: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->pasteMsg(ILjava/lang/String;ZZZ)Landroid/os/Message;
    invoke-static/range {v7 .. v12}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$700(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;ILjava/lang/String;ZZZ)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 215
    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    iget-object v1, v1, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->messageHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;->this$0:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    const/4 v8, 0x2

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    # invokes: Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->pasteMsg(ILjava/lang/String;ZZZ)Landroid/os/Message;
    invoke-static/range {v7 .. v12}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->access$700(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;ILjava/lang/String;ZZZ)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 114
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
