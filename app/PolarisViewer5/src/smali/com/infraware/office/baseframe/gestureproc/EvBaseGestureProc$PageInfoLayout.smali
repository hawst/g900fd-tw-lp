.class public Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;
.super Ljava/lang/Object;
.source "EvBaseGestureProc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PageInfoLayout"
.end annotation


# static fields
.field protected static final PAGEINFO_INVISIBLE:I = 0x0

.field protected static final PAGEINFO_SETTEXT:I = 0x2

.field protected static final PAGEINFO_VISIBLE:I = 0x1


# instance fields
.field private configInfo:Lcom/infraware/office/evengine/EV$CONFIG_INFO;

.field protected mActivity:Landroid/app/Activity;

.field protected mHandler:Landroid/os/Handler;

.field protected mPageInfoCurrent:Landroid/widget/TextView;

.field protected mPageInfoLayout:Landroid/widget/FrameLayout;

.field protected mPageInfoPage:Landroid/widget/LinearLayout;

.field protected mPageInfoTotal:Landroid/widget/TextView;

.field protected mPageInfoZoom:Landroid/widget/TextView;

.field private nCurPage:I

.field private nTotalPage:I

.field final synthetic this$0:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;


# direct methods
.method public constructor <init>(Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;Landroid/app/Activity;)V
    .locals 3
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 746
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 708
    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mActivity:Landroid/app/Activity;

    .line 709
    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mPageInfoLayout:Landroid/widget/FrameLayout;

    .line 710
    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mPageInfoCurrent:Landroid/widget/TextView;

    .line 711
    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mPageInfoTotal:Landroid/widget/TextView;

    .line 712
    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mPageInfoZoom:Landroid/widget/TextView;

    .line 713
    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mPageInfoPage:Landroid/widget/LinearLayout;

    .line 715
    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->configInfo:Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    .line 716
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->nCurPage:I

    .line 717
    iput v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->nTotalPage:I

    .line 722
    new-instance v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout$1;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout$1;-><init>(Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mHandler:Landroid/os/Handler;

    .line 747
    iput-object p2, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mActivity:Landroid/app/Activity;

    .line 748
    const v0, 0x7f0b0123

    invoke-virtual {p2, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mPageInfoLayout:Landroid/widget/FrameLayout;

    .line 749
    invoke-virtual {p2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03002e

    iget-object v2, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mPageInfoLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 751
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mPageInfoLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b0163

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mPageInfoPage:Landroid/widget/LinearLayout;

    .line 752
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mPageInfoLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b0164

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mPageInfoCurrent:Landroid/widget/TextView;

    .line 753
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mPageInfoLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b0165

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mPageInfoTotal:Landroid/widget/TextView;

    .line 754
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mPageInfoLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b0166

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mPageInfoZoom:Landroid/widget/TextView;

    .line 755
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;

    .prologue
    .line 707
    invoke-direct {p0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->setText()V

    return-void
.end method

.method static synthetic access$100(Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;I)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;
    .param p1, "x1"    # I

    .prologue
    .line 707
    invoke-direct {p0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->setVisibility(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;

    .prologue
    .line 707
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->nCurPage:I

    return v0
.end method

.method static synthetic access$300(Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;

    .prologue
    .line 707
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->nTotalPage:I

    return v0
.end method

.method static synthetic access$400(Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;)Lcom/infraware/office/evengine/EV$CONFIG_INFO;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;

    .prologue
    .line 707
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->configInfo:Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    return-object v0
.end method

.method private setText()V
    .locals 2

    .prologue
    .line 765
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-nez v0, :cond_1

    .line 817
    :cond_0
    :goto_0
    return-void

    .line 768
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->configInfo:Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    .line 769
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->configInfo:Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->nCurPage:I

    .line 770
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->configInfo:Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nTotalPages:I

    iput v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->nTotalPage:I

    .line 771
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->nCurPage:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->nTotalPage:I

    if-lez v0, :cond_0

    .line 773
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPageInfoType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 774
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout$2;

    invoke-direct {v1, p0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout$2;-><init>(Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 790
    :cond_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout$3;

    invoke-direct {v1, p0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout$3;-><init>(Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private setVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 820
    if-nez p1, :cond_0

    .line 821
    invoke-direct {p0}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->setText()V

    .line 823
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mPageInfoLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_1

    .line 824
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mPageInfoLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 844
    :cond_1
    return-void
.end method


# virtual methods
.method public PageInfofinalize()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 758
    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mActivity:Landroid/app/Activity;

    .line 759
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mPageInfoLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 760
    iput-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc$PageInfoLayout;->mPageInfoLayout:Landroid/widget/FrameLayout;

    .line 761
    return-void
.end method
