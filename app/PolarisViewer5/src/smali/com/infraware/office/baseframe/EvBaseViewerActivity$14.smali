.class Lcom/infraware/office/baseframe/EvBaseViewerActivity$14;
.super Ljava/lang/Object;
.source "EvBaseViewerActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/EvBaseViewerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 0

    .prologue
    .line 910
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$14;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x0

    const v4, 0x7f0b0070

    const/4 v0, 0x1

    .line 913
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_2

    .line 914
    packed-switch p2, :pswitch_data_0

    :cond_0
    move v0, v1

    .line 938
    :cond_1
    :goto_0
    :sswitch_0
    return v0

    .line 916
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v2, v4, :cond_0

    goto :goto_0

    .line 922
    :cond_2
    sparse-switch p2, :sswitch_data_0

    :cond_3
    move v0, v1

    .line 938
    goto :goto_0

    .line 926
    :sswitch_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v2, v4, :cond_3

    goto :goto_0

    .line 930
    :sswitch_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f0b006f

    if-eq v2, v3, :cond_1

    .line 932
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v2, v4, :cond_3

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$14;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_DstTxt:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$1300(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$14;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_OrgTxt:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$800(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    .line 914
    nop

    :pswitch_data_0
    .packed-switch 0x42
        :pswitch_0
    .end packed-switch

    .line 922
    :sswitch_data_0
    .sparse-switch
        0x14 -> :sswitch_0
        0x3d -> :sswitch_2
        0x42 -> :sswitch_1
    .end sparse-switch
.end method
