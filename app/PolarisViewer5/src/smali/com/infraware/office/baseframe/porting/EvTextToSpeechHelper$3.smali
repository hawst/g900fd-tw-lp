.class Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$3;
.super Ljava/lang/Object;
.source "EvTextToSpeechHelper.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)V
    .locals 0

    .prologue
    .line 762
    iput-object p1, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$3;->this$0:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 2
    .param p1, "focusChange"    # I

    .prologue
    .line 765
    packed-switch p1, :pswitch_data_0

    .line 783
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 768
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$3;->this$0:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->stop(Z)I

    goto :goto_0

    .line 772
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$3;->this$0:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    # getter for: Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->access$000(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$3;->this$0:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    # getter for: Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->access$000(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    if-eqz v0, :cond_0

    .line 773
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$3;->this$0:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    # getter for: Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->access$000(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onPauseTTSButton()V

    goto :goto_0

    .line 776
    :pswitch_3
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$3;->this$0:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    # getter for: Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->access$000(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$3;->this$0:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    # getter for: Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->access$000(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    if-eqz v0, :cond_0

    .line 777
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper$3;->this$0:Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;

    # getter for: Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;->access$000(Lcom/infraware/office/baseframe/porting/EvTextToSpeechHelper;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onPlayTTSButton()V

    goto :goto_0

    .line 765
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
