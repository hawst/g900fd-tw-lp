.class public Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupWindowDissmissListener;
.super Ljava/lang/Object;
.source "EvBaseViewerActivity.java"

# interfaces
.implements Landroid/widget/PopupWindow$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/EvBaseViewerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PopupWindowDissmissListener"
.end annotation


# instance fields
.field button:Landroid/widget/ImageButton;

.field final synthetic this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;


# direct methods
.method public constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 1

    .prologue
    .line 624
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupWindowDissmissListener;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 626
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupWindowDissmissListener;->button:Landroid/widget/ImageButton;

    return-void
.end method


# virtual methods
.method public onDismiss()V
    .locals 4

    .prologue
    .line 635
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupWindowDissmissListener;->button:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 636
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupWindowDissmissListener;->button:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 639
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupWindowDissmissListener;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/4 v1, 0x0

    # setter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPopup:Landroid/widget/PopupWindow;
    invoke-static {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$202(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Landroid/widget/PopupWindow;)Landroid/widget/PopupWindow;

    .line 641
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupWindowDissmissListener;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFindSetBtnIsLatestClicked:Z
    invoke-static {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$300(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 642
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupWindowDissmissListener;->button:Landroid/widget/ImageButton;

    new-instance v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupWindowDissmissListener$1;

    invoke-direct {v1, p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupWindowDissmissListener$1;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupWindowDissmissListener;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ImageButton;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 650
    :cond_1
    return-void
.end method

.method public setCallerButton(Landroid/widget/ImageButton;)V
    .locals 0
    .param p1, "button"    # Landroid/widget/ImageButton;

    .prologue
    .line 629
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$PopupWindowDissmissListener;->button:Landroid/widget/ImageButton;

    .line 630
    return-void
.end method
