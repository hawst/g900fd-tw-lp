.class Lcom/infraware/office/baseframe/EvBaseViewerActivity$17;
.super Ljava/lang/Object;
.source "EvBaseViewerActivity.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/EvBaseViewerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 0

    .prologue
    .line 1443
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$17;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I
    .param p6, "oldLeft"    # I
    .param p7, "oldTop"    # I
    .param p8, "oldRight"    # I
    .param p9, "oldBottom"    # I

    .prologue
    .line 1446
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$17;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainLinearLayout:Landroid/view/View;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$17;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainLinearLayout:Landroid/view/View;

    if-nez v0, :cond_1

    .line 1452
    :cond_0
    :goto_0
    return-void

    .line 1447
    :cond_1
    if-ne p2, p6, :cond_2

    if-ne p3, p7, :cond_2

    if-ne p4, p8, :cond_2

    if-eq p5, p9, :cond_0

    .line 1448
    :cond_2
    invoke-static {}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getInstance()Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$17;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0, v1}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->isNormalWindow(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1450
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$17;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v0, :cond_0

    .line 1451
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$17;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->dismissMenuPopup()Z

    goto :goto_0
.end method
