.class Lcom/infraware/office/baseframe/EvBaseViewerActivity$55;
.super Ljava/lang/Object;
.source "EvBaseViewerActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/EvBaseViewerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 0

    .prologue
    .line 5302
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$55;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    .line 5305
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 5322
    :goto_0
    return-void

    .line 5307
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$55;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mFreeDrawMode:I

    if-ne v0, v1, :cond_0

    .line 5308
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$55;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onChangeFreeDrawMode(I)V

    goto :goto_0

    .line 5310
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$55;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onChangeFreeDrawMode(I)V

    goto :goto_0

    .line 5313
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$55;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onChangeFreeDrawMode(I)V

    goto :goto_0

    .line 5316
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$55;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IInfraPenAllErase()V

    goto :goto_0

    .line 5319
    :pswitch_3
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$55;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onChangeFreeDrawMode(I)V

    goto :goto_0

    .line 5305
    :pswitch_data_0
    .packed-switch 0x7f0b0111
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
