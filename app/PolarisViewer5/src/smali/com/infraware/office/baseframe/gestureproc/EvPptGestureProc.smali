.class public Lcom/infraware/office/baseframe/gestureproc/EvPptGestureProc;
.super Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;
.source "EvPptGestureProc.java"


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "listener"    # Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    .prologue
    .line 11
    invoke-direct {p0, p1, p2, p3}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;)V

    .line 12
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPptGestureProc;->mAdvGestureDetector:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->setIsLongpressEnabled(Z)V

    .line 13
    const-string/jumbo v0, "EvPptGestureProc"

    const-string/jumbo v1, "EvPptGestureProc"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 8
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 18
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPptGestureProc;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0x2d

    const/4 v6, 0x0

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    move-result v0

    if-ne v0, v7, :cond_0

    .line 19
    invoke-super {p0, p1, p2, p3, p4}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    .line 20
    :goto_0
    return v0

    :cond_0
    move v0, v7

    goto :goto_0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 27
    iget v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvPptGestureProc;->mActionMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 30
    :goto_0
    return-void

    .line 29
    :cond_0
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->onLongPress(Landroid/view/MotionEvent;)V

    goto :goto_0
.end method
