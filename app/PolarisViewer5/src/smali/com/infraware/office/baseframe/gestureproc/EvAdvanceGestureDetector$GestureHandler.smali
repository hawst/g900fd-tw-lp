.class Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$GestureHandler;
.super Landroid/os/Handler;
.source "EvAdvanceGestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GestureHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$GestureHandler;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    .line 63
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 64
    return-void
.end method

.method constructor <init>(Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;Landroid/os/Handler;)V
    .locals 1
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$GestureHandler;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    .line 67
    invoke-virtual {p2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 68
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 72
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 79
    :cond_0
    :goto_0
    return-void

    .line 75
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$GestureHandler;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$GestureHandler;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    # getter for: Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mStillDown:Z
    invoke-static {v0}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->access$000(Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$GestureHandler;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mListener:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;

    iget-object v1, p0, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$GestureHandler;->this$0:Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;

    # getter for: Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;
    invoke-static {v1}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;->access$100(Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector;)Landroid/view/MotionEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/infraware/office/baseframe/gestureproc/EvAdvanceGestureDetector$OnEvGestureListener;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 72
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method
