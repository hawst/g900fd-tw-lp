.class public Lcom/infraware/office/baseframe/porting/EvClipboardExManager;
.super Ljava/lang/Object;
.source "EvClipboardExManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;
    }
.end annotation


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mBackupCaller:I

.field private mBackupData:Ljava/lang/String;

.field private mBackupText:Ljava/lang/String;

.field private mBackupType:I

.field private mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;

.field private mDocType:I

.field private mEvInterface:Lcom/infraware/office/evengine/EvInterface;

.field private mIsFormat:Z

.field private mIsValue:Z

.field mPasteEvent:Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;

.field protected final messageHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/sec/clipboard/ClipboardExManager;)V
    .locals 2
    .param p1, "act"    # Landroid/app/Activity;
    .param p2, "clipboardExManager"    # Landroid/sec/clipboard/ClipboardExManager;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object v1, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;

    .line 31
    iput-object v1, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mActivity:Landroid/app/Activity;

    .line 32
    iput-object v1, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 33
    iput-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mIsValue:Z

    .line 34
    iput-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mIsFormat:Z

    .line 36
    new-instance v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$1;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$1;-><init>(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->messageHandler:Landroid/os/Handler;

    .line 227
    iput-object v1, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mPasteEvent:Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;

    .line 239
    iput-object p1, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mActivity:Landroid/app/Activity;

    .line 240
    iput-object p2, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;

    .line 242
    new-instance v0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;

    invoke-direct {v0, p0}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;-><init>(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mPasteEvent:Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;

    .line 243
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)Lcom/infraware/office/evengine/EvInterface;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    .prologue
    .line 27
    iget v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mDocType:I

    return v0
.end method

.method static synthetic access$200(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    .prologue
    .line 27
    iget v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mBackupCaller:I

    return v0
.end method

.method static synthetic access$300(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mBackupText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    .prologue
    .line 27
    iget v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mBackupType:I

    return v0
.end method

.method static synthetic access$500(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mIsValue:Z

    return v0
.end method

.method static synthetic access$600(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mIsFormat:Z

    return v0
.end method

.method static synthetic access$700(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;ILjava/lang/String;ZZZ)Landroid/os/Message;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/porting/EvClipboardExManager;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Z
    .param p4, "x4"    # Z
    .param p5, "x5"    # Z

    .prologue
    .line 27
    invoke-direct/range {p0 .. p5}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->pasteMsg(ILjava/lang/String;ZZZ)Landroid/os/Message;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mBackupData:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/infraware/office/baseframe/porting/EvClipboardExManager;)Landroid/sec/clipboard/ClipboardExManager;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;

    return-object v0
.end method

.method private pasteMsg(ILjava/lang/String;ZZZ)Landroid/os/Message;
    .locals 3
    .param p1, "type"    # I
    .param p2, "data"    # Ljava/lang/String;
    .param p3, "isnative"    # Z
    .param p4, "isvalue"    # Z
    .param p5, "isformat"    # Z

    .prologue
    .line 65
    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->messageHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 66
    .local v1, "message":Landroid/os/Message;
    const/4 v2, 0x0

    iput v2, v1, Landroid/os/Message;->what:I

    .line 67
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 69
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v2, "type"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 70
    const-string/jumbo v2, "data"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string/jumbo v2, "native"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 72
    const-string/jumbo v2, "value"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 73
    const-string/jumbo v2, "format"

    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 74
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 76
    return-object v1
.end method


# virtual methods
.method public OnEditCopyCut(IILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "caller"    # I
    .param p2, "nType"    # I
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "data"    # Ljava/lang/String;

    .prologue
    .line 417
    iput p2, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mBackupType:I

    .line 418
    iput p1, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mBackupCaller:I

    .line 419
    iput-object p3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mBackupText:Ljava/lang/String;

    .line 420
    iput-object p4, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mBackupData:Ljava/lang/String;

    .line 422
    if-nez p2, :cond_1

    .line 423
    invoke-virtual {p0, p3}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->setText(Ljava/lang/String;)V

    .line 431
    :cond_0
    :goto_0
    return-void

    .line 425
    :cond_1
    const/4 v0, 0x1

    if-ne p2, v0, :cond_2

    .line 426
    invoke-virtual {p0, p4}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->setHtml(Ljava/lang/String;)V

    goto :goto_0

    .line 428
    :cond_2
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 429
    invoke-virtual {p0, p4}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->setImgPath(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public doPaste(Lcom/infraware/office/evengine/EvInterface;ZZI)Z
    .locals 6
    .param p1, "evi"    # Lcom/infraware/office/evengine/EvInterface;
    .param p2, "bValue"    # Z
    .param p3, "bFormat"    # Z
    .param p4, "docType"    # I

    .prologue
    const/4 v1, 0x0

    .line 394
    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mPasteEvent:Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;

    if-eqz v2, :cond_0

    .line 395
    iput-object p1, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 396
    iput-boolean p2, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mIsValue:Z

    .line 397
    iput-boolean p3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mIsFormat:Z

    .line 398
    iput p4, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mDocType:I

    .line 404
    :try_start_0
    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;

    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mActivity:Landroid/app/Activity;

    const/4 v4, 0x4

    iget-object v5, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mPasteEvent:Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;

    invoke-virtual {v2, v3, v4, v5}, Landroid/sec/clipboard/ClipboardExManager;->getData(Landroid/content/Context;ILandroid/sec/clipboard/IClipboardDataPasteEvent;)Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 409
    const/4 v1, 0x1

    .line 412
    :cond_0
    :goto_0
    return v1

    .line 406
    :catch_0
    move-exception v0

    .line 407
    .local v0, "e2":Ljava/lang/NoSuchMethodError;
    goto :goto_0
.end method

.method public getDataListSize()I
    .locals 2

    .prologue
    .line 361
    :try_start_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;

    invoke-virtual {v1}, Landroid/sec/clipboard/ClipboardExManager;->getDataListSize()I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 368
    :goto_0
    return v0

    .line 364
    :catch_0
    move-exception v1

    .line 368
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getText()Ljava/lang/String;
    .locals 5

    .prologue
    .line 346
    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;

    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mActivity:Landroid/app/Activity;

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Landroid/sec/clipboard/ClipboardExManager;->getData(Landroid/content/Context;I)Landroid/sec/clipboard/data/ClipboardData;

    move-result-object v0

    .line 348
    .local v0, "data":Landroid/sec/clipboard/data/ClipboardData;
    if-eqz v0, :cond_0

    move-object v1, v0

    .line 350
    check-cast v1, Landroid/sec/clipboard/data/list/ClipboardDataText;

    .line 351
    .local v1, "text":Landroid/sec/clipboard/data/list/ClipboardDataText;
    invoke-virtual {v1}, Landroid/sec/clipboard/data/list/ClipboardDataText;->GetText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 354
    .end local v1    # "text":Landroid/sec/clipboard/data/list/ClipboardDataText;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isSamsungClipboard()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 374
    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;

    if-nez v2, :cond_1

    .line 387
    :cond_0
    :goto_0
    return v1

    .line 377
    :cond_1
    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mPasteEvent:Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;

    if-eqz v2, :cond_0

    .line 381
    :try_start_0
    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;

    invoke-virtual {v2}, Landroid/sec/clipboard/ClipboardExManager;->getDataListSize()I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 387
    const/4 v1, 0x1

    goto :goto_0

    .line 383
    :catch_0
    move-exception v0

    .line 384
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    goto :goto_0
.end method

.method public setHtml(Ljava/lang/String;)V
    .locals 6
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 277
    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 278
    .local v3, "sdk":Ljava/lang/String;
    const-string/jumbo v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 279
    .local v1, "p1":I
    const-string/jumbo v4, "."

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    .line 280
    .local v2, "p2":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    .line 281
    sget-object v4, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v4, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 296
    :cond_0
    :try_start_0
    new-instance v0, Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;

    invoke-direct {v0}, Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;-><init>()V

    .line 297
    .local v0, "html":Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;
    invoke-virtual {v0, p1}, Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;->SetHTMLFragment(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 299
    :try_start_1
    iget-object v4, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;

    iget-object v5, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4, v5, v0}, Landroid/sec/clipboard/ClipboardExManager;->setData(Landroid/content/Context;Landroid/sec/clipboard/data/ClipboardData;)Z
    :try_end_1
    .catch Ljava/lang/NoSuchMethodError; {:try_start_1 .. :try_end_1} :catch_1

    .line 309
    .end local v0    # "html":Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;
    :goto_0
    return-void

    .line 306
    :catch_0
    move-exception v4

    goto :goto_0

    .line 301
    .restart local v0    # "html":Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;
    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public setImgPath(Ljava/lang/String;)V
    .locals 6
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 315
    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 316
    .local v3, "sdk":Ljava/lang/String;
    const-string/jumbo v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 317
    .local v1, "p1":I
    const-string/jumbo v4, "."

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    .line 318
    .local v2, "p2":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    .line 319
    sget-object v4, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v4, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 332
    :cond_0
    :try_start_0
    new-instance v0, Landroid/sec/clipboard/data/list/ClipboardDataBitmap;

    invoke-direct {v0}, Landroid/sec/clipboard/data/list/ClipboardDataBitmap;-><init>()V

    .line 333
    .local v0, "bm":Landroid/sec/clipboard/data/list/ClipboardDataBitmap;
    invoke-virtual {v0, p1}, Landroid/sec/clipboard/data/list/ClipboardDataBitmap;->SetBitmapPath(Ljava/lang/String;)Z

    .line 334
    iget-object v4, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;

    iget-object v5, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4, v5, v0}, Landroid/sec/clipboard/ClipboardExManager;->setData(Landroid/content/Context;Landroid/sec/clipboard/data/ClipboardData;)Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 341
    .end local v0    # "bm":Landroid/sec/clipboard/data/list/ClipboardDataBitmap;
    :goto_0
    return-void

    .line 337
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method public setText(Ljava/lang/String;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 261
    new-instance v0, Landroid/sec/clipboard/data/list/ClipboardDataText;

    invoke-direct {v0}, Landroid/sec/clipboard/data/list/ClipboardDataText;-><init>()V

    .line 263
    .local v0, "Text":Landroid/sec/clipboard/data/list/ClipboardDataText;
    invoke-virtual {v0, p1}, Landroid/sec/clipboard/data/list/ClipboardDataText;->SetText(Ljava/lang/CharSequence;)Z

    .line 266
    :try_start_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;

    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v2, v0}, Landroid/sec/clipboard/ClipboardExManager;->setData(Landroid/content/Context;Landroid/sec/clipboard/data/ClipboardData;)Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 271
    :goto_0
    return-void

    .line 268
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public thisFinalize()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 249
    iput-object v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;

    .line 250
    iput-object v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mActivity:Landroid/app/Activity;

    .line 251
    iput-object v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mBackupText:Ljava/lang/String;

    .line 252
    iput-object v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mBackupData:Ljava/lang/String;

    .line 254
    iput-object v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mPasteEvent:Lcom/infraware/office/baseframe/porting/EvClipboardExManager$IClipboardDataPasteEventImpl;

    .line 255
    iput-object v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 256
    return-void
.end method
