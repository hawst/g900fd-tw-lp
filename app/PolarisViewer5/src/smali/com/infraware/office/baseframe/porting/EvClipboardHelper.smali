.class public Lcom/infraware/office/baseframe/porting/EvClipboardHelper;
.super Ljava/lang/Object;
.source "EvClipboardHelper.java"


# static fields
.field public static final CLIPBOARD_DATA_TYPE_HTML:I = 0x1

.field public static final CLIPBOARD_DATA_TYPE_IMGPATH:I = 0x2

.field public static final CLIPBOARD_DATA_TYPE_TEXT:I

.field public static mClipboardRootPath:Ljava/lang/String;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mBackupData:Ljava/lang/String;

.field private mBackupText:Ljava/lang/String;

.field private mBackupType:I

.field private mClipboardManager:Landroid/text/ClipboardManager;

.field private mEvInterface:Lcom/infraware/office/evengine/EvInterface;

.field private mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-string/jumbo v0, "/sdcard/.clipboard"

    sput-object v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mClipboardRootPath:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 6
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v5, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3}, Ljava/lang/String;-><init>()V

    iput-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mBackupData:Ljava/lang/String;

    .line 28
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3}, Ljava/lang/String;-><init>()V

    iput-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mBackupText:Ljava/lang/String;

    .line 29
    const/4 v3, -0x1

    iput v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mBackupType:I

    .line 30
    iput-object v5, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 31
    iput-object v5, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    .line 42
    iput-object p1, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mActivity:Landroid/app/Activity;

    .line 43
    const-string/jumbo v3, "clipboard"

    invoke-virtual {p1, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/text/ClipboardManager;

    iput-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mClipboardManager:Landroid/text/ClipboardManager;

    .line 48
    :try_start_0
    const-string/jumbo v3, "clipboardEx"

    invoke-virtual {p1, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/sec/clipboard/ClipboardExManager;

    .line 49
    .local v2, "samsung":Landroid/sec/clipboard/ClipboardExManager;
    if-eqz v2, :cond_0

    .line 51
    new-instance v3, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    invoke-direct {v3, p1, v2}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;-><init>(Landroid/app/Activity;Landroid/sec/clipboard/ClipboardExManager;)V

    iput-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    .line 53
    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->isSamsungClipboard()Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 55
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/app/Activity;->getFilesDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/.clipboard"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mClipboardRootPath:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    .line 74
    .end local v2    # "samsung":Landroid/sec/clipboard/ClipboardExManager;
    :cond_0
    :goto_0
    invoke-static {}, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->createClipboardDirectoty()Z

    .line 75
    return-void

    .line 59
    .restart local v2    # "samsung":Landroid/sec/clipboard/ClipboardExManager;
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->thisFinalize()V

    .line 60
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;
    :try_end_1
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 63
    .end local v2    # "samsung":Landroid/sec/clipboard/ClipboardExManager;
    :catch_0
    move-exception v0

    .line 64
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    if-eqz v3, :cond_2

    .line 65
    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->thisFinalize()V

    .line 66
    :cond_2
    iput-object v5, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    goto :goto_0

    .line 67
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v1

    .line 69
    .local v1, "e2":Ljava/lang/NoSuchMethodError;
    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    if-eqz v3, :cond_3

    .line 70
    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->thisFinalize()V

    .line 71
    :cond_3
    iput-object v5, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    goto :goto_0
.end method

.method public static bitmap2File(Landroid/graphics/Bitmap;Ljava/lang/String;)Z
    .locals 7
    .param p0, "bm"    # Landroid/graphics/Bitmap;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 348
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 349
    .local v3, "file":Ljava/io/File;
    const/4 v1, 0x0

    .line 351
    .local v1, "fOut":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 356
    .end local v1    # "fOut":Ljava/io/FileOutputStream;
    .local v2, "fOut":Ljava/io/FileOutputStream;
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x55

    invoke-virtual {p0, v5, v6, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 359
    :try_start_1
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V

    .line 360
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 361
    const/4 v4, 0x1

    move-object v1, v2

    .line 365
    .end local v2    # "fOut":Ljava/io/FileOutputStream;
    .restart local v1    # "fOut":Ljava/io/FileOutputStream;
    :goto_0
    return v4

    .line 352
    :catch_0
    move-exception v0

    .line 353
    .local v0, "e":Ljava/io/FileNotFoundException;
    goto :goto_0

    .line 363
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    .end local v1    # "fOut":Ljava/io/FileOutputStream;
    .restart local v2    # "fOut":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v0

    .local v0, "e":Ljava/io/IOException;
    move-object v1, v2

    .line 365
    .end local v2    # "fOut":Ljava/io/FileOutputStream;
    .restart local v1    # "fOut":Ljava/io/FileOutputStream;
    goto :goto_0
.end method

.method public static createClipboardDirectoty()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 457
    new-instance v0, Ljava/io/File;

    sget-object v4, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mClipboardRootPath:Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 461
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 463
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 466
    const/16 v3, 0x1ff

    const/4 v4, 0x0

    :try_start_0
    invoke-static {v0, v3, v4}, Lcom/infraware/common/util/FileUtils;->changePermissons(Ljava/io/File;IZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 508
    :goto_0
    return v2

    .line 467
    :catch_0
    move-exception v1

    .line 469
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 474
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 476
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 479
    const/16 v3, 0x1ff

    const/4 v4, 0x0

    :try_start_1
    invoke-static {v0, v3, v4}, Lcom/infraware/common/util/FileUtils;->changePermissons(Ljava/io/File;IZ)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 480
    :catch_1
    move-exception v1

    .line 482
    .restart local v1    # "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    move v2, v3

    .line 487
    goto :goto_0

    :cond_2
    move v2, v3

    .line 491
    goto :goto_0

    .line 497
    :cond_3
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 500
    const/16 v3, 0x1ff

    const/4 v4, 0x0

    :try_start_2
    invoke-static {v0, v3, v4}, Lcom/infraware/common/util/FileUtils;->changePermissons(Ljava/io/File;IZ)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 501
    :catch_2
    move-exception v1

    .line 503
    .restart local v1    # "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .end local v1    # "e":Ljava/lang/Exception;
    :cond_4
    move v2, v3

    .line 508
    goto :goto_0
.end method

.method public static file2Bitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 370
    const/4 v0, 0x0

    .line 372
    .local v0, "bmp":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-static {p0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v2, v0

    .line 377
    :goto_0
    return-object v2

    .line 373
    :catch_0
    move-exception v1

    .line 374
    .local v1, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public OnEditCopyCut(IILjava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "caller"    # I
    .param p2, "nType"    # I
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "data"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 383
    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    if-eqz v3, :cond_1

    .line 386
    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    invoke-virtual {v3, p1, p2, p3, p4}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->OnEditCopyCut(IILjava/lang/String;Ljava/lang/String;)V

    .line 450
    :cond_0
    :goto_0
    return-void

    .line 390
    :cond_1
    iput p2, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mBackupType:I

    .line 392
    const-string/jumbo v0, ""

    .line 393
    .local v0, "call":Ljava/lang/String;
    if-ne p1, v5, :cond_6

    .line 394
    const-string/jumbo v0, "po_word"

    .line 400
    :cond_2
    :goto_1
    new-instance v1, Lcom/infraware/polarisoffice5/common/BrClipboardManager;

    invoke-direct {v1}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;-><init>()V

    .line 401
    .local v1, "manager":Lcom/infraware/polarisoffice5/common/BrClipboardManager;
    const-string/jumbo v3, "set"

    invoke-virtual {v1, v3, v0}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->Open(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    const-string/jumbo v2, ""

    .line 404
    .local v2, "toSystem":Ljava/lang/String;
    if-eqz p3, :cond_8

    .line 406
    iput-object p3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mBackupText:Ljava/lang/String;

    .line 407
    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mBackupText:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_3

    .line 409
    const-string/jumbo v3, "text"

    iget-object v4, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mBackupText:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->SetData(Ljava/lang/String;[B)Z

    .line 410
    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mBackupText:Ljava/lang/String;

    .line 417
    :cond_3
    :goto_2
    if-eqz p4, :cond_b

    .line 419
    iput-object p4, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mBackupData:Ljava/lang/String;

    .line 421
    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mBackupData:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_5

    .line 423
    if-nez p2, :cond_9

    .line 425
    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mBackupText:Ljava/lang/String;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mBackupText:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_5

    .line 427
    :cond_4
    const-string/jumbo v3, "text"

    iget-object v4, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mBackupData:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->SetData(Ljava/lang/String;[B)Z

    .line 428
    iget-object v2, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mBackupData:Ljava/lang/String;

    .line 440
    :cond_5
    :goto_3
    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->Close()Z

    .line 441
    const/4 v1, 0x0

    .line 443
    iput p2, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mBackupType:I

    .line 445
    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mClipboardManager:Landroid/text/ClipboardManager;

    if-eqz v3, :cond_0

    .line 447
    iget-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mClipboardManager:Landroid/text/ClipboardManager;

    invoke-virtual {v3, v2}, Landroid/text/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 395
    .end local v1    # "manager":Lcom/infraware/polarisoffice5/common/BrClipboardManager;
    .end local v2    # "toSystem":Ljava/lang/String;
    :cond_6
    if-ne p1, v6, :cond_7

    .line 396
    const-string/jumbo v0, "po_sheet"

    goto :goto_1

    .line 397
    :cond_7
    const/4 v3, 0x3

    if-ne p1, v3, :cond_2

    .line 398
    const-string/jumbo v0, "po_slide"

    goto :goto_1

    .line 414
    .restart local v1    # "manager":Lcom/infraware/polarisoffice5/common/BrClipboardManager;
    .restart local v2    # "toSystem":Ljava/lang/String;
    :cond_8
    const-string/jumbo v3, ""

    iput-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mBackupText:Ljava/lang/String;

    goto :goto_2

    .line 431
    :cond_9
    if-ne p2, v5, :cond_a

    .line 432
    const-string/jumbo v3, "html"

    iget-object v4, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mBackupData:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->SetData(Ljava/lang/String;[B)Z

    goto :goto_3

    .line 433
    :cond_a
    if-ne p2, v6, :cond_5

    .line 434
    const-string/jumbo v3, "imgpath"

    iget-object v4, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mBackupData:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->SetData(Ljava/lang/String;[B)Z

    goto :goto_3

    .line 438
    :cond_b
    const-string/jumbo v3, ""

    iput-object v3, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mBackupData:Ljava/lang/String;

    goto :goto_3
.end method

.method public doPaste(Lcom/infraware/office/evengine/EvInterface;ZZI)Z
    .locals 25
    .param p1, "evi"    # Lcom/infraware/office/evengine/EvInterface;
    .param p2, "bValue"    # Z
    .param p3, "bFormat"    # Z
    .param p4, "docType"    # I

    .prologue
    .line 146
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    move-object/from16 v21, v0

    if-eqz v21, :cond_0

    .line 148
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->doPaste(Lcom/infraware/office/evengine/EvInterface;ZZI)Z

    move-result v21

    .line 343
    :goto_0
    return v21

    .line 151
    :cond_0
    new-instance v14, Lcom/infraware/polarisoffice5/common/BrClipboardManager;

    invoke-direct {v14}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;-><init>()V

    .line 152
    .local v14, "mgr":Lcom/infraware/polarisoffice5/common/BrClipboardManager;
    const-string/jumbo v21, "get"

    const-string/jumbo v22, ""

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v14, v0, v1}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->Open(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const-string/jumbo v21, "html"

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->GetData(Ljava/lang/String;)[B

    move-result-object v7

    .line 155
    .local v7, "bhtml":[B
    const-string/jumbo v21, "imgpath"

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->GetData(Ljava/lang/String;)[B

    move-result-object v8

    .line 156
    .local v8, "bimg":[B
    const-string/jumbo v21, "text"

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->GetData(Ljava/lang/String;)[B

    move-result-object v9

    .line 157
    .local v9, "btext":[B
    invoke-virtual {v14}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->GetCaller()Ljava/lang/String;

    move-result-object v10

    .line 158
    .local v10, "caller":Ljava/lang/String;
    invoke-virtual {v14}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->Close()Z

    .line 159
    const/4 v14, 0x0

    .line 161
    const-string/jumbo v12, ""

    .line 162
    .local v12, "html":Ljava/lang/String;
    if-eqz v7, :cond_1

    .line 163
    new-instance v12, Ljava/lang/String;

    .end local v12    # "html":Ljava/lang/String;
    invoke-direct {v12, v7}, Ljava/lang/String;-><init>([B)V

    .line 164
    .restart local v12    # "html":Ljava/lang/String;
    :cond_1
    const-string/jumbo v13, ""

    .line 165
    .local v13, "img":Ljava/lang/String;
    if-eqz v8, :cond_2

    .line 166
    new-instance v13, Ljava/lang/String;

    .end local v13    # "img":Ljava/lang/String;
    invoke-direct {v13, v8}, Ljava/lang/String;-><init>([B)V

    .line 167
    .restart local v13    # "img":Ljava/lang/String;
    :cond_2
    const-string/jumbo v20, ""

    .line 168
    .local v20, "text":Ljava/lang/String;
    if-eqz v9, :cond_3

    .line 169
    new-instance v20, Ljava/lang/String;

    .end local v20    # "text":Ljava/lang/String;
    move-object/from16 v0, v20

    invoke-direct {v0, v9}, Ljava/lang/String;-><init>([B)V

    .line 172
    .restart local v20    # "text":Ljava/lang/String;
    :cond_3
    const/16 v18, 0x0

    .line 173
    .local v18, "program":I
    const-string/jumbo v21, "po_word"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v21

    if-nez v21, :cond_5

    .line 174
    const/16 v18, 0x1

    .line 180
    :cond_4
    :goto_1
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 181
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v21, v0

    if-nez v21, :cond_7

    .line 182
    const/16 v21, 0x0

    goto :goto_0

    .line 175
    :cond_5
    const-string/jumbo v21, "po_sheet"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v21

    if-nez v21, :cond_6

    .line 176
    const/16 v18, 0x2

    goto :goto_1

    .line 177
    :cond_6
    const-string/jumbo v21, "po_slide"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v21

    if-nez v21, :cond_4

    .line 178
    const/16 v18, 0x3

    goto :goto_1

    .line 185
    :cond_7
    const-string/jumbo v19, ""

    .line 186
    .local v19, "strSystem":Ljava/lang/String;
    const/16 v16, 0x0

    .line 187
    .local v16, "notAvailable":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mClipboardManager:Landroid/text/ClipboardManager;

    move-object/from16 v21, v0

    if-eqz v21, :cond_8

    .line 189
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mClipboardManager:Landroid/text/ClipboardManager;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/text/ClipboardManager;->hasText()Z

    move-result v21

    if-eqz v21, :cond_c

    .line 191
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mClipboardManager:Landroid/text/ClipboardManager;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/text/ClipboardManager;->getText()Ljava/lang/CharSequence;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v19

    .line 192
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v21

    if-lez v21, :cond_b

    .line 194
    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v21

    if-eqz v21, :cond_8

    .line 197
    const/16 v21, 0x1

    move/from16 v0, p2

    move/from16 v1, v21

    if-ne v0, v1, :cond_9

    .line 198
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v21, v0

    const/16 v22, 0x3

    const/16 v23, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    .line 203
    :goto_2
    const/16 v16, 0x1

    .line 215
    :cond_8
    :goto_3
    if-eqz v16, :cond_d

    .line 218
    new-instance v15, Lcom/infraware/polarisoffice5/common/BrClipboardManager;

    invoke-direct {v15}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;-><init>()V

    .line 219
    .local v15, "mgr2":Lcom/infraware/polarisoffice5/common/BrClipboardManager;
    const-string/jumbo v21, "set"

    const-string/jumbo v22, ""

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v15, v0, v1}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->Open(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    invoke-virtual {v15}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->Empty()V

    .line 221
    invoke-virtual {v15}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->Close()Z

    .line 222
    const/4 v15, 0x0

    .line 223
    const/16 v21, 0x1

    goto/16 :goto_0

    .line 199
    .end local v15    # "mgr2":Lcom/infraware/polarisoffice5/common/BrClipboardManager;
    :cond_9
    const/16 v21, 0x1

    move/from16 v0, p3

    move/from16 v1, v21

    if-ne v0, v1, :cond_a

    .line 200
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v21, v0

    const/16 v22, 0x4

    const/16 v23, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    goto :goto_2

    .line 202
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v21, v0

    const/16 v22, 0x2

    const/16 v23, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    goto :goto_2

    .line 207
    :cond_b
    const/16 v16, 0x1

    goto :goto_3

    .line 211
    :cond_c
    const/16 v16, 0x1

    goto :goto_3

    .line 226
    :cond_d
    const/4 v5, 0x0

    .line 230
    .local v5, "bMatch":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mActivity:Landroid/app/Activity;

    move-object/from16 v21, v0

    check-cast v21, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual/range {v21 .. v21}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v11

    .line 232
    .local v11, "cur_prog":I
    const/16 v21, 0x2

    move/from16 v0, v21

    if-ne v11, v0, :cond_11

    .line 234
    const/16 v21, 0x2

    move/from16 v0, v18

    move/from16 v1, v21

    if-ne v0, v1, :cond_e

    .line 235
    const/4 v5, 0x1

    .line 251
    :cond_e
    :goto_4
    if-eqz v5, :cond_f

    .line 254
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v21

    if-lez v21, :cond_15

    .line 256
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mBackupType:I

    move/from16 v21, v0

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_14

    .line 258
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mBackupData:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v21

    if-eqz v21, :cond_f

    .line 259
    const/4 v5, 0x0

    .line 281
    :cond_f
    :goto_5
    const/4 v6, 0x0

    .line 282
    .local v6, "bNative":Z
    if-eqz v5, :cond_10

    .line 284
    const/16 v21, 0x2

    move/from16 v0, v21

    if-eq v11, v0, :cond_18

    .line 286
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/infraware/office/evengine/EvInterface;->IGetBWPOpInfo_Editor()Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    move-result-object v17

    .line 287
    .local v17, "opInfo":Lcom/infraware/office/evengine/EV$BWP_OP_INFO;
    if-eqz v17, :cond_10

    .line 289
    move-object/from16 v0, v17

    iget v0, v0, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nStatusOP:I

    move/from16 v21, v0

    const/high16 v22, 0x1000000

    and-int v21, v21, v22

    if-eqz v21, :cond_10

    .line 290
    const/4 v6, 0x1

    .line 301
    .end local v17    # "opInfo":Lcom/infraware/office/evengine/EV$BWP_OP_INFO;
    :cond_10
    :goto_6
    if-eqz v5, :cond_1b

    .line 304
    const/16 v21, 0x1

    move/from16 v0, p2

    move/from16 v1, v21

    if-ne v0, v1, :cond_19

    .line 305
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v21, v0

    const/16 v22, 0x3

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v21 .. v24}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    .line 343
    :goto_7
    const/16 v21, 0x1

    goto/16 :goto_0

    .line 237
    .end local v6    # "bNative":Z
    :cond_11
    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v11, v0, :cond_12

    .line 239
    const/16 v21, 0x1

    move/from16 v0, v18

    move/from16 v1, v21

    if-ne v0, v1, :cond_e

    .line 240
    const/4 v5, 0x1

    goto :goto_4

    .line 242
    :cond_12
    const/16 v21, 0x3

    move/from16 v0, v21

    if-ne v11, v0, :cond_13

    .line 244
    const/16 v21, 0x3

    move/from16 v0, v18

    move/from16 v1, v21

    if-ne v0, v1, :cond_e

    .line 245
    const/4 v5, 0x1

    goto/16 :goto_4

    .line 248
    :cond_13
    const/4 v5, 0x0

    goto/16 :goto_4

    .line 262
    :cond_14
    const/4 v5, 0x0

    goto :goto_5

    .line 264
    :cond_15
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v21

    if-lez v21, :cond_17

    .line 266
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mBackupType:I

    move/from16 v21, v0

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_16

    .line 268
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mBackupData:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v21

    if-eqz v21, :cond_f

    .line 269
    const/4 v5, 0x0

    goto/16 :goto_5

    .line 272
    :cond_16
    const/4 v5, 0x0

    goto/16 :goto_5

    .line 274
    :cond_17
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v21

    if-lez v21, :cond_f

    .line 276
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mBackupType:I

    move/from16 v21, v0

    if-eqz v21, :cond_f

    .line 277
    const/4 v5, 0x0

    goto/16 :goto_5

    .line 296
    .restart local v6    # "bNative":Z
    :cond_18
    const/4 v6, 0x1

    goto :goto_6

    .line 306
    :cond_19
    const/16 v21, 0x1

    move/from16 v0, p3

    move/from16 v1, v21

    if-ne v0, v1, :cond_1a

    .line 307
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v21, v0

    const/16 v22, 0x4

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v21 .. v24}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    goto :goto_7

    .line 309
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v21, v0

    const/16 v22, 0x2

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v21 .. v24}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    goto/16 :goto_7

    .line 314
    :cond_1b
    const/16 v21, 0x2

    move/from16 v0, v21

    if-ne v11, v0, :cond_1f

    .line 317
    const/16 v21, 0x1

    move/from16 v0, p2

    move/from16 v1, v21

    if-ne v0, v1, :cond_1c

    .line 318
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v21, v0

    const/16 v22, 0x3

    const/16 v23, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    goto/16 :goto_7

    .line 319
    :cond_1c
    const/16 v21, 0x1

    move/from16 v0, p3

    move/from16 v1, v21

    if-ne v0, v1, :cond_1d

    .line 320
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v21, v0

    const/16 v22, 0x4

    const/16 v23, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    goto/16 :goto_7

    .line 324
    :cond_1d
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v21

    if-lez v21, :cond_1e

    .line 325
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v21, v0

    const/16 v22, 0x2

    const/16 v23, 0x2

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2, v13}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    goto/16 :goto_7

    .line 329
    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v21, v0

    const/16 v22, 0x2

    const/16 v23, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    goto/16 :goto_7

    .line 334
    :cond_1f
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v21

    if-lez v21, :cond_20

    .line 335
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v21, v0

    const/16 v22, 0x2

    const/16 v23, 0x2

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2, v13}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    goto/16 :goto_7

    .line 336
    :cond_20
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v21

    if-lez v21, :cond_21

    .line 337
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v21, v0

    const/16 v22, 0x2

    const/16 v23, 0x1

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2, v12}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    goto/16 :goto_7

    .line 339
    :cond_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v21, v0

    const/16 v22, 0x2

    const/16 v23, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    goto/16 :goto_7
.end method

.method public getText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 114
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    if-nez v1, :cond_0

    .line 116
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mClipboardManager:Landroid/text/ClipboardManager;

    if-eqz v1, :cond_1

    .line 117
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mClipboardManager:Landroid/text/ClipboardManager;

    invoke-virtual {v1}, Landroid/text/ClipboardManager;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 127
    :goto_0
    return-object v0

    .line 123
    :cond_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->getText()Ljava/lang/String;

    move-result-object v0

    .line 124
    .local v0, "text":Ljava/lang/String;
    goto :goto_0

    .line 127
    .end local v0    # "text":Ljava/lang/String;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasText()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 98
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    if-nez v1, :cond_1

    .line 100
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mClipboardManager:Landroid/text/ClipboardManager;

    if-eqz v1, :cond_2

    .line 101
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mClipboardManager:Landroid/text/ClipboardManager;

    invoke-virtual {v1}, Landroid/text/ClipboardManager;->hasText()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 110
    :cond_0
    :goto_0
    return v0

    .line 107
    :cond_1
    iget-object v1, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->getDataListSize()I

    move-result v1

    if-gtz v1, :cond_0

    .line 110
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onChangeScreen()V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->thisFinalize()V

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    .line 84
    :cond_0
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    if-nez v0, :cond_1

    .line 132
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mClipboardManager:Landroid/text/ClipboardManager;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mClipboardManager:Landroid/text/ClipboardManager;

    invoke-virtual {v0, p1}, Landroid/text/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->setText(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public thisFinalize()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 87
    iput-object v1, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mClipboardManager:Landroid/text/ClipboardManager;

    .line 88
    iput-object v1, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mActivity:Landroid/app/Activity;

    .line 89
    iput-object v1, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mClipboardManager:Landroid/text/ClipboardManager;

    .line 90
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvClipboardExManager;->thisFinalize()V

    .line 93
    iput-object v1, p0, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mSamsungClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardExManager;

    .line 95
    :cond_0
    return-void
.end method
