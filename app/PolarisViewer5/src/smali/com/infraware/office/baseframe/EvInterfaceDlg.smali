.class public Lcom/infraware/office/baseframe/EvInterfaceDlg;
.super Ljava/lang/Thread;
.source "EvInterfaceDlg.java"


# instance fields
.field mContext:Landroid/content/Context;

.field mLoop:Landroid/os/Looper;

.field mMessageId:I

.field mProgress:Landroid/app/ProgressDialog;

.field mTitleId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "titleId"    # I
    .param p3, "messageId"    # I

    .prologue
    .line 18
    const-string/jumbo v0, "EvInterfaceDlg"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 20
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvInterfaceDlg;->mContext:Landroid/content/Context;

    .line 21
    iput p2, p0, Lcom/infraware/office/baseframe/EvInterfaceDlg;->mTitleId:I

    .line 22
    iput p3, p0, Lcom/infraware/office/baseframe/EvInterfaceDlg;->mMessageId:I

    .line 23
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvInterfaceDlg;->setDaemon(Z)V

    .line 24
    return-void
.end method

.method static stop(Lcom/infraware/office/baseframe/EvInterfaceDlg;)V
    .locals 2
    .param p0, "dlg"    # Lcom/infraware/office/baseframe/EvInterfaceDlg;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvInterfaceDlg;->mProgress:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvInterfaceDlg;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 47
    :cond_0
    const-wide/16 v0, 0x64

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    :goto_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvInterfaceDlg;->mLoop:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 54
    return-void

    .line 48
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 29
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 31
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvInterfaceDlg;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvInterfaceDlg;->mProgress:Landroid/app/ProgressDialog;

    .line 32
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvInterfaceDlg;->mProgress:Landroid/app/ProgressDialog;

    iget v1, p0, Lcom/infraware/office/baseframe/EvInterfaceDlg;->mTitleId:I

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 33
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvInterfaceDlg;->mProgress:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/infraware/office/baseframe/EvInterfaceDlg;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lcom/infraware/office/baseframe/EvInterfaceDlg;->mMessageId:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 34
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvInterfaceDlg;->mProgress:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 35
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvInterfaceDlg;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 37
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/baseframe/EvInterfaceDlg;->mLoop:Landroid/os/Looper;

    .line 38
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 40
    return-void
.end method
