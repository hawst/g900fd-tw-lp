.class Lcom/infraware/office/baseframe/EvBaseViewerActivity$12;
.super Ljava/lang/Object;
.source "EvBaseViewerActivity.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/EvBaseViewerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 0

    .prologue
    .line 854
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$12;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "arg0"    # Landroid/widget/TextView;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 859
    const/4 v2, 0x3

    if-ne p2, v2, :cond_1

    .line 861
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$12;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v2, v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$12;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v2, v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isShown()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 862
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$12;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v2, v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->hidePanel()V

    .line 863
    :cond_0
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$12;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # invokes: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->hideSoftInputInFindMode()V
    invoke-static {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$400(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    .line 864
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$12;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->FindText(I)V

    .line 874
    :goto_0
    return v0

    .line 867
    :cond_1
    const/4 v2, 0x5

    if-ne p2, v2, :cond_2

    .line 869
    iget-object v2, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$12;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_DstTxt:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$1300(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/EditText;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 870
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$12;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mReplace_DstTxt:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$1300(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    :cond_2
    move v0, v1

    .line 874
    goto :goto_0
.end method
