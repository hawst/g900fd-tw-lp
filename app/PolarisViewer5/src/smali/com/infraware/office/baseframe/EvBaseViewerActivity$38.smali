.class Lcom/infraware/office/baseframe/EvBaseViewerActivity$38;
.super Ljava/lang/Object;
.source "EvBaseViewerActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 0

    .prologue
    .line 3818
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$38;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 3821
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$38;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintAll:Landroid/widget/RadioButton;
    invoke-static {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$2900(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 3822
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$38;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPage:Landroid/widget/RadioButton;
    invoke-static {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$2700(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 3823
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$38;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPage:Landroid/widget/RadioButton;
    invoke-static {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$2700(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setSelected(Z)V

    .line 3824
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$38;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintStartEdit:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$2300(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 3825
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$38;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintStartEdit:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$2300(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 3826
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$38;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintStartEdit:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$2300(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 3827
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$38;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEndEdit:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$2400(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 3828
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$38;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEndEdit:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$2400(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 3829
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$38;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintEndEdit:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$2400(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 3830
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$38;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # getter for: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mPrintPagePopup:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$2800(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 3831
    return-void
.end method
