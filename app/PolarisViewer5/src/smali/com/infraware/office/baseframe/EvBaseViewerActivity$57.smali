.class Lcom/infraware/office/baseframe/EvBaseViewerActivity$57;
.super Ljava/lang/Object;
.source "EvBaseViewerActivity.java"

# interfaces
.implements Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onCreateOnKKOver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 0

    .prologue
    .line 5600
    iput-object p1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$57;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onModeChanged(Z)V
    .locals 2
    .param p1, "toMulti"    # Z

    .prologue
    .line 5611
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$57;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isNormalWindow()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 5612
    .local v0, "bSplit":Z
    :goto_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$57;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # invokes: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onMultiWindowStatusChanged(Z)V
    invoke-static {v1, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$3300(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Z)V

    .line 5613
    return-void

    .line 5611
    .end local v0    # "bSplit":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSizeChanged(Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "arg0"    # Landroid/graphics/Rect;

    .prologue
    .line 5606
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$57;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isNormalWindow()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 5607
    .local v0, "bSplit":Z
    :goto_0
    iget-object v1, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity$57;->this$0:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    # invokes: Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onMultiWindowStatusChanged(Z)V
    invoke-static {v1, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->access$3300(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Z)V

    .line 5608
    return-void

    .line 5606
    .end local v0    # "bSplit":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onZoneChanged(I)V
    .locals 0
    .param p1, "arg0"    # I

    .prologue
    .line 5603
    return-void
.end method
