.class public interface abstract Lcom/infraware/office/baseframe/EvBaseE$PopupDialogEventType;
.super Ljava/lang/Object;
.source "EvBaseE.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/baseframe/EvBaseE;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PopupDialogEventType"
.end annotation


# static fields
.field public static final POPUP_2003_SAVE_CONFIRM:I = 0x19

.field public static final POPUP_CHART_LEGEND:I = 0x17

.field public static final POPUP_CLEAR_LIST:I = 0x12

.field public static final POPUP_COLUMN_WIDTH:I = 0x6

.field public static final POPUP_DATE_LIST:I = 0xf

.field public static final POPUP_DECIMAL_LIST:I = 0x14

.field public static final POPUP_DELETE_CELL_LIST:I = 0x13

.field public static final POPUP_DIALOG_ERROR:I = 0x34

.field public static final POPUP_DIALOG_OPEN_PROGRESS:I = 0x9

.field public static final POPUP_DIALOG_PRINT_NOTICE:I = 0x1c

.field public static final POPUP_DIALOG_PRINT_PREV_PROGRESS:I = 0x1b

.field public static final POPUP_DIALOG_PRINT_PROGRESS:I = 0x1a

.field public static final POPUP_DIALOG_SAVE_PROGRESS:I = 0xb

.field public static final POPUP_DIALOG_SEARCH:I = 0x2c

.field public static final POPUP_DIALOG_SEARCH_PROGRESS:I = 0xa

.field public static final POPUP_DIALOG_SENDFILE:I = 0x20

.field public static final POPUP_DOWNLOAD_PROGRESS:I = 0x2d

.field public static final POPUP_FONT_SIZE:I = 0x7

.field public static final POPUP_FRACTION_LIST:I = 0x11

.field public static final POPUP_INSERT_CELL_LIST:I = 0xe

.field public static final POPUP_INSERT_IMAGE:I = 0x21

.field public static final POPUP_LAYOUT:I = 0x30

.field public static final POPUP_LOSE_COLUMN_INFO:I = 0x1f

.field public static final POPUP_MORE_SEARCH_MENU:I = 0x33

.field public static final POPUP_NFC_NOT_SAVED:I = 0x2a

.field public static final POPUP_NFC_PARTITIAL_SAVED:I = 0x29

.field public static final POPUP_NONE:I = 0x0

.field public static final POPUP_NUMBERS_LIST:I = 0xd

.field public static final POPUP_NUMBER_CURRENCY:I = 0x2

.field public static final POPUP_NUMBER_NUMBERS:I = 0x1

.field public static final POPUP_NUMBER_PERCENT:I = 0x3

.field public static final POPUP_NUMBER_SCIENTIFIC:I = 0x4

.field public static final POPUP_OTHERS_FONT_SIZE:I = 0x8

.field public static final POPUP_PAGE_LAYOUT_PROGRESS:I = 0x2b

.field public static final POPUP_PRINT_RANGE:I = 0x1e

.field public static final POPUP_PROTECT_READ_PASSWORD_ONLY:I = 0x28

.field public static final POPUP_PROTECT_WRITE_PASSWORD_ONLY:I = 0x27

.field public static final POPUP_ROW_HEIGHT:I = 0x5

.field public static final POPUP_SAVE_CONFIRM:I = 0x18

.field public static final POPUP_SEND_EMAIL:I = 0x32

.field public static final POPUP_SHEET_LOAD_COMPLETE:I = 0x24

.field public static final POPUP_SHEET_MERGE_CELL:I = 0x23

.field public static final POPUP_SHEET_RENAME:I = 0x22

.field public static final POPUP_SLIDE_SHOW_DRAW_CONFORM:I = 0x25

.field public static final POPUP_SLIDE_SHOW_SETTINGS:I = 0x26

.field public static final POPUP_SLIDE_VIEWMODE:I = 0x2f

.field public static final POPUP_SORT_DIRECTION_LIST:I = 0x16

.field public static final POPUP_SORT_TYPE_LIST_3:I = 0x15

.field public static final POPUP_TABLE_INSERT_CELL:I = 0x31

.field public static final POPUP_TIME_LIST:I = 0x10

.field public static final POPUP_TTS_SELECT_LANGUAGE:I = 0x1d

.field public static final POPUP_WORD_VIEWMODE:I = 0x2e

.field public static final POPUP_ZOOM_LIST:I = 0xc
