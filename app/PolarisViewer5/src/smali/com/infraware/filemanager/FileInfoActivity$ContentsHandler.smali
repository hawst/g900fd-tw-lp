.class Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;
.super Landroid/os/Handler;
.source "FileInfoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/filemanager/FileInfoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContentsHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/filemanager/FileInfoActivity;


# direct methods
.method private constructor <init>(Lcom/infraware/filemanager/FileInfoActivity;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;->this$0:Lcom/infraware/filemanager/FileInfoActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/infraware/filemanager/FileInfoActivity;Lcom/infraware/filemanager/FileInfoActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/infraware/filemanager/FileInfoActivity;
    .param p2, "x1"    # Lcom/infraware/filemanager/FileInfoActivity$1;

    .prologue
    .line 193
    invoke-direct {p0, p1}, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;-><init>(Lcom/infraware/filemanager/FileInfoActivity;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 197
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;->this$0:Lcom/infraware/filemanager/FileInfoActivity;

    invoke-virtual {v2}, Lcom/infraware/filemanager/FileInfoActivity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 248
    :cond_0
    :goto_0
    return-void

    .line 200
    :cond_1
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 203
    :pswitch_0
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;->this$0:Lcom/infraware/filemanager/FileInfoActivity;

    # invokes: Lcom/infraware/filemanager/FileInfoActivity;->onChangeSize()V
    invoke-static {v2}, Lcom/infraware/filemanager/FileInfoActivity;->access$100(Lcom/infraware/filemanager/FileInfoActivity;)V

    .line 204
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;->this$0:Lcom/infraware/filemanager/FileInfoActivity;

    # invokes: Lcom/infraware/filemanager/FileInfoActivity;->onChangeCount()V
    invoke-static {v2}, Lcom/infraware/filemanager/FileInfoActivity;->access$200(Lcom/infraware/filemanager/FileInfoActivity;)V

    goto :goto_0

    .line 207
    :pswitch_1
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;->this$0:Lcom/infraware/filemanager/FileInfoActivity;

    # getter for: Lcom/infraware/filemanager/FileInfoActivity;->m_pbPortProgressSize:Landroid/widget/ProgressBar;
    invoke-static {v2}, Lcom/infraware/filemanager/FileInfoActivity;->access$300(Lcom/infraware/filemanager/FileInfoActivity;)Landroid/widget/ProgressBar;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 208
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;->this$0:Lcom/infraware/filemanager/FileInfoActivity;

    # getter for: Lcom/infraware/filemanager/FileInfoActivity;->m_pbLandProgressSize:Landroid/widget/ProgressBar;
    invoke-static {v2}, Lcom/infraware/filemanager/FileInfoActivity;->access$400(Lcom/infraware/filemanager/FileInfoActivity;)Landroid/widget/ProgressBar;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 209
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;->this$0:Lcom/infraware/filemanager/FileInfoActivity;

    # getter for: Lcom/infraware/filemanager/FileInfoActivity;->m_pbPortProgressContents:Landroid/widget/ProgressBar;
    invoke-static {v2}, Lcom/infraware/filemanager/FileInfoActivity;->access$500(Lcom/infraware/filemanager/FileInfoActivity;)Landroid/widget/ProgressBar;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 210
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;->this$0:Lcom/infraware/filemanager/FileInfoActivity;

    # getter for: Lcom/infraware/filemanager/FileInfoActivity;->m_pbLandProgressContents:Landroid/widget/ProgressBar;
    invoke-static {v2}, Lcom/infraware/filemanager/FileInfoActivity;->access$600(Lcom/infraware/filemanager/FileInfoActivity;)Landroid/widget/ProgressBar;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 213
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/infraware/filemanager/file/FileListItem;

    .line 214
    .local v0, "localItem":Lcom/infraware/filemanager/file/FileListItem;
    if-eqz v0, :cond_0

    .line 217
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;->this$0:Lcom/infraware/filemanager/FileInfoActivity;

    # getter for: Lcom/infraware/filemanager/FileInfoActivity;->m_pbPortProgressSize:Landroid/widget/ProgressBar;
    invoke-static {v2}, Lcom/infraware/filemanager/FileInfoActivity;->access$300(Lcom/infraware/filemanager/FileInfoActivity;)Landroid/widget/ProgressBar;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 218
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;->this$0:Lcom/infraware/filemanager/FileInfoActivity;

    # getter for: Lcom/infraware/filemanager/FileInfoActivity;->m_pbLandProgressSize:Landroid/widget/ProgressBar;
    invoke-static {v2}, Lcom/infraware/filemanager/FileInfoActivity;->access$400(Lcom/infraware/filemanager/FileInfoActivity;)Landroid/widget/ProgressBar;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 219
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;->this$0:Lcom/infraware/filemanager/FileInfoActivity;

    # getter for: Lcom/infraware/filemanager/FileInfoActivity;->m_pbPortProgressContents:Landroid/widget/ProgressBar;
    invoke-static {v2}, Lcom/infraware/filemanager/FileInfoActivity;->access$500(Lcom/infraware/filemanager/FileInfoActivity;)Landroid/widget/ProgressBar;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 220
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;->this$0:Lcom/infraware/filemanager/FileInfoActivity;

    # getter for: Lcom/infraware/filemanager/FileInfoActivity;->m_pbLandProgressContents:Landroid/widget/ProgressBar;
    invoke-static {v2}, Lcom/infraware/filemanager/FileInfoActivity;->access$600(Lcom/infraware/filemanager/FileInfoActivity;)Landroid/widget/ProgressBar;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 222
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler$1;

    invoke-direct {v3, p0, v0}, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler$1;-><init>(Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;Lcom/infraware/filemanager/file/FileListItem;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 232
    .end local v0    # "localItem":Lcom/infraware/filemanager/file/FileListItem;
    :pswitch_3
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;->this$0:Lcom/infraware/filemanager/FileInfoActivity;

    # invokes: Lcom/infraware/filemanager/FileInfoActivity;->onChangeThumbnail()V
    invoke-static {v2}, Lcom/infraware/filemanager/FileInfoActivity;->access$900(Lcom/infraware/filemanager/FileInfoActivity;)V

    goto :goto_0

    .line 235
    :pswitch_4
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/infraware/filemanager/file/FileListItem;

    .line 236
    .local v1, "thumbnailItem":Lcom/infraware/filemanager/file/FileListItem;
    if-eqz v1, :cond_0

    .line 239
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler$2;

    invoke-direct {v3, p0, v1}, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler$2;-><init>(Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;Lcom/infraware/filemanager/file/FileListItem;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 200
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
