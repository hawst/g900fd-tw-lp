.class public Lcom/infraware/filemanager/FileInfoActivity;
.super Landroid/app/Activity;
.source "FileInfoActivity.java"

# interfaces
.implements Lcom/infraware/common/event/SdCardListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/filemanager/FileInfoActivity$CloseActionReceiver;,
        Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;
    }
.end annotation


# static fields
.field static final INFO_ACTION_CHANGE_CONTENTS:I = 0x0

.field static final INFO_ACTION_CHANGE_THUMBNAIL:I = 0x3

.field static final INFO_ACTION_END_CONTENTS:I = 0x1

.field static final INFO_ACTION_GET_CONTENTS_LOCAL:I = 0x2

.field static final INFO_ACTION_GET_THUMBNAIL:I = 0x4

.field static final INFO_TITLE_TEXT_MIN_SIZE_DP:F = 78.5f


# instance fields
.field private isDoc:Z

.field private isFolder:Z

.field private isNewDoc:Z

.field private isNoSize:Z

.field private isPdf:Z

.field private m_arryTvLandTitle:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private m_arryTvPortTitle:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private m_bitmapIcon:Landroid/graphics/Bitmap;

.field private m_bitmapThumb:Landroid/graphics/Bitmap;

.field private m_ivLandIcon:Landroid/widget/ImageView;

.field private m_ivLandThumb:Landroid/widget/ImageView;

.field private m_ivPortIcon:Landroid/widget/ImageView;

.field private m_ivPortThumb:Landroid/widget/ImageView;

.field private m_layoutLand:Landroid/widget/LinearLayout;

.field private m_layoutLandAttributes:Landroid/widget/LinearLayout;

.field private m_layoutLandAuthor:Landroid/widget/LinearLayout;

.field private m_layoutLandContents:Landroid/widget/LinearLayout;

.field private m_layoutLandModifiedBy:Landroid/widget/LinearLayout;

.field private m_layoutLandPage:Landroid/widget/LinearLayout;

.field private m_layoutLandSize:Landroid/widget/LinearLayout;

.field private m_layoutLandThumb:Landroid/widget/LinearLayout;

.field private m_layoutLandTitle:Landroid/widget/LinearLayout;

.field private m_layoutLandWords:Landroid/widget/LinearLayout;

.field private m_layoutPort:Landroid/widget/LinearLayout;

.field private m_layoutPortAttributes:Landroid/widget/LinearLayout;

.field private m_layoutPortAuthor:Landroid/widget/LinearLayout;

.field private m_layoutPortContents:Landroid/widget/LinearLayout;

.field private m_layoutPortModifiedBy:Landroid/widget/LinearLayout;

.field private m_layoutPortPage:Landroid/widget/LinearLayout;

.field private m_layoutPortSize:Landroid/widget/LinearLayout;

.field private m_layoutPortThumb:Landroid/widget/LinearLayout;

.field private m_layoutPortTitle:Landroid/widget/LinearLayout;

.field private m_layoutPortWords:Landroid/widget/LinearLayout;

.field private m_layoutTitle:Landroid/widget/LinearLayout;

.field private m_nAttributesId:I

.field private m_nContentsFile:I

.field private m_nContentsFolder:I

.field private m_nContentsSize:J

.field private m_nDocPage:I

.field private m_nDocType:I

.field private m_nDocWord:I

.field private m_nInternalCmdType:I

.field private m_nLastModified:J

.field private m_nLocaleType:I

.field private m_nOrientation:I

.field private m_nTypeId:I

.field private m_oCloseFilter:Landroid/content/IntentFilter;

.field private m_oCloseReceiver:Lcom/infraware/filemanager/FileInfoActivity$CloseActionReceiver;

.field private m_oFileItem:Ljava/lang/Object;

.field private m_oInfoHandler:Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;

.field private m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

.field private m_oThumbItem:Lcom/infraware/common/database/ThumbnailItem;

.field private m_pbLandProgressContents:Landroid/widget/ProgressBar;

.field private m_pbLandProgressSize:Landroid/widget/ProgressBar;

.field private m_pbPortProgressContents:Landroid/widget/ProgressBar;

.field private m_pbPortProgressSize:Landroid/widget/ProgressBar;

.field private m_strDocAuthor:Ljava/lang/String;

.field private m_strDocLastEditor:Ljava/lang/String;

.field private m_strDocTitle:Ljava/lang/String;

.field private m_strFileName:Ljava/lang/String;

.field private m_strLocation:Ljava/lang/String;

.field private m_tvLandAttributes:Landroid/widget/TextView;

.field private m_tvLandContents:Landroid/widget/TextView;

.field private m_tvLandDocAuthor:Landroid/widget/TextView;

.field private m_tvLandDocModifiedBy:Landroid/widget/TextView;

.field private m_tvLandDocPage:Landroid/widget/TextView;

.field private m_tvLandDocTitle:Landroid/widget/TextView;

.field private m_tvLandDocWords:Landroid/widget/TextView;

.field private m_tvLandLocation:Landroid/widget/TextView;

.field private m_tvLandModified:Landroid/widget/TextView;

.field private m_tvLandName:Landroid/widget/TextView;

.field private m_tvLandSize:Landroid/widget/TextView;

.field private m_tvLandTitleAttributes:Landroid/widget/TextView;

.field private m_tvLandTitleContents:Landroid/widget/TextView;

.field private m_tvLandTitleDocAuthor:Landroid/widget/TextView;

.field private m_tvLandTitleDocModifiedBy:Landroid/widget/TextView;

.field private m_tvLandTitleDocPage:Landroid/widget/TextView;

.field private m_tvLandTitleDocTitle:Landroid/widget/TextView;

.field private m_tvLandTitleDocWords:Landroid/widget/TextView;

.field private m_tvLandTitleLocation:Landroid/widget/TextView;

.field private m_tvLandTitleModified:Landroid/widget/TextView;

.field private m_tvLandTitleSize:Landroid/widget/TextView;

.field private m_tvLandTitleType:Landroid/widget/TextView;

.field private m_tvLandType:Landroid/widget/TextView;

.field private m_tvPortAttributes:Landroid/widget/TextView;

.field private m_tvPortContents:Landroid/widget/TextView;

.field private m_tvPortDocAuthor:Landroid/widget/TextView;

.field private m_tvPortDocModifiedBy:Landroid/widget/TextView;

.field private m_tvPortDocPage:Landroid/widget/TextView;

.field private m_tvPortDocTitle:Landroid/widget/TextView;

.field private m_tvPortDocWords:Landroid/widget/TextView;

.field private m_tvPortLocation:Landroid/widget/TextView;

.field private m_tvPortModified:Landroid/widget/TextView;

.field private m_tvPortName:Landroid/widget/TextView;

.field private m_tvPortSize:Landroid/widget/TextView;

.field private m_tvPortTitleAttributes:Landroid/widget/TextView;

.field private m_tvPortTitleContents:Landroid/widget/TextView;

.field private m_tvPortTitleDocAuthor:Landroid/widget/TextView;

.field private m_tvPortTitleDocModifiedBy:Landroid/widget/TextView;

.field private m_tvPortTitleDocPage:Landroid/widget/TextView;

.field private m_tvPortTitleDocTitle:Landroid/widget/TextView;

.field private m_tvPortTitleDocWords:Landroid/widget/TextView;

.field private m_tvPortTitleLocation:Landroid/widget/TextView;

.field private m_tvPortTitleModified:Landroid/widget/TextView;

.field private m_tvPortTitleSize:Landroid/widget/TextView;

.field private m_tvPortTitleType:Landroid/widget/TextView;

.field private m_tvPortType:Landroid/widget/TextView;

.field private m_tvTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 46
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 55
    iput-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    .line 60
    iput v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nInternalCmdType:I

    .line 61
    iput-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oFileItem:Ljava/lang/Object;

    .line 153
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_arryTvLandTitle:Ljava/util/ArrayList;

    .line 154
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_arryTvPortTitle:Ljava/util/ArrayList;

    .line 162
    iput-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_bitmapThumb:Landroid/graphics/Bitmap;

    .line 163
    iput-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_bitmapIcon:Landroid/graphics/Bitmap;

    .line 164
    iput-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strFileName:Ljava/lang/String;

    .line 165
    iput v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nTypeId:I

    .line 166
    iput-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strLocation:Ljava/lang/String;

    .line 167
    iput-wide v3, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nContentsSize:J

    .line 168
    iput v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nContentsFolder:I

    .line 169
    iput v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nContentsFile:I

    .line 170
    iput-wide v3, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nLastModified:J

    .line 171
    iput v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nAttributesId:I

    .line 173
    iput-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strDocTitle:Ljava/lang/String;

    .line 174
    iput-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strDocAuthor:Ljava/lang/String;

    .line 175
    iput-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strDocLastEditor:Ljava/lang/String;

    .line 176
    iput v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nDocPage:I

    .line 177
    iput v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nDocWord:I

    .line 178
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nDocType:I

    .line 180
    iput-boolean v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->isNewDoc:Z

    .line 181
    iput-boolean v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->isDoc:Z

    .line 182
    iput-boolean v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->isPdf:Z

    .line 183
    iput-boolean v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->isFolder:Z

    .line 184
    iput-boolean v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->isNoSize:Z

    .line 186
    iput-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oThumbItem:Lcom/infraware/common/database/ThumbnailItem;

    .line 189
    iput-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oCloseReceiver:Lcom/infraware/filemanager/FileInfoActivity$CloseActionReceiver;

    .line 190
    iput-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    .line 192
    new-instance v0, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;

    invoke-direct {v0, p0, v1}, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;-><init>(Lcom/infraware/filemanager/FileInfoActivity;Lcom/infraware/filemanager/FileInfoActivity$1;)V

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oInfoHandler:Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;

    .line 1295
    return-void
.end method

.method static synthetic access$100(Lcom/infraware/filemanager/FileInfoActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/filemanager/FileInfoActivity;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/infraware/filemanager/FileInfoActivity;->onChangeSize()V

    return-void
.end method

.method static synthetic access$1000(Lcom/infraware/filemanager/FileInfoActivity;Lcom/infraware/filemanager/file/FileListItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/filemanager/FileInfoActivity;
    .param p1, "x1"    # Lcom/infraware/filemanager/file/FileListItem;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/infraware/filemanager/FileInfoActivity;->getThumbnail(Lcom/infraware/filemanager/file/FileListItem;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/infraware/filemanager/FileInfoActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/FileInfoActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strLocation:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/infraware/filemanager/FileInfoActivity;Ljava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/FileInfoActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/infraware/filemanager/FileInfoActivity;->getTextWidth(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/infraware/filemanager/FileInfoActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/FileInfoActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortLocation:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/infraware/filemanager/FileInfoActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/FileInfoActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandLocation:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/infraware/filemanager/FileInfoActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/filemanager/FileInfoActivity;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/infraware/filemanager/FileInfoActivity;->onChangeCount()V

    return-void
.end method

.method static synthetic access$300(Lcom/infraware/filemanager/FileInfoActivity;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/FileInfoActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_pbPortProgressSize:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$400(Lcom/infraware/filemanager/FileInfoActivity;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/FileInfoActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_pbLandProgressSize:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/infraware/filemanager/FileInfoActivity;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/FileInfoActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_pbPortProgressContents:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$600(Lcom/infraware/filemanager/FileInfoActivity;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/FileInfoActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_pbLandProgressContents:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$700(Lcom/infraware/filemanager/FileInfoActivity;Ljava/io/File;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/filemanager/FileInfoActivity;
    .param p1, "x1"    # Ljava/io/File;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/infraware/filemanager/FileInfoActivity;->getLocalSubfolders(Ljava/io/File;)V

    return-void
.end method

.method static synthetic access$800(Lcom/infraware/filemanager/FileInfoActivity;)Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/FileInfoActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oInfoHandler:Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/infraware/filemanager/FileInfoActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/filemanager/FileInfoActivity;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/infraware/filemanager/FileInfoActivity;->onChangeThumbnail()V

    return-void
.end method

.method private getLocalSubfolders(Ljava/io/File;)V
    .locals 9
    .param p1, "folder"    # Ljava/io/File;

    .prologue
    const/4 v8, 0x0

    .line 1234
    new-instance v4, Lcom/infraware/filemanager/FileInfoActivity$4;

    invoke-direct {v4, p0}, Lcom/infraware/filemanager/FileInfoActivity$4;-><init>(Lcom/infraware/filemanager/FileInfoActivity;)V

    invoke-virtual {p1, v4}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v3

    .line 1243
    .local v3, "subFolders":[Ljava/io/File;
    new-instance v4, Lcom/infraware/filemanager/FileInfoActivity$5;

    invoke-direct {v4, p0}, Lcom/infraware/filemanager/FileInfoActivity$5;-><init>(Lcom/infraware/filemanager/FileInfoActivity;)V

    invoke-virtual {p1, v4}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v2

    .line 1252
    .local v2, "subFiles":[Ljava/io/File;
    const/4 v1, 0x0

    .line 1253
    .local v1, "nContentsFolder":I
    const/4 v0, 0x0

    .line 1255
    .local v0, "nContentsFile":I
    if-eqz v3, :cond_0

    .line 1256
    array-length v1, v3

    .line 1257
    :cond_0
    if-eqz v2, :cond_1

    .line 1258
    array-length v0, v2

    .line 1260
    :cond_1
    iget v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nContentsFolder:I

    add-int/2addr v4, v1

    iput v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nContentsFolder:I

    .line 1261
    iget v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nContentsFile:I

    add-int/2addr v4, v0

    iput v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nContentsFile:I

    .line 1262
    iget-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oInfoHandler:Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;

    invoke-virtual {v4, v8}, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;->sendEmptyMessage(I)Z

    .line 1264
    :goto_0
    if-lez v0, :cond_2

    .line 1265
    iget-wide v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nContentsSize:J

    add-int/lit8 v6, v0, -0x1

    aget-object v6, v2, v6

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v6

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nContentsSize:J

    .line 1266
    iget-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oInfoHandler:Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;

    invoke-virtual {v4, v8}, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;->sendEmptyMessage(I)Z

    .line 1267
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1270
    :cond_2
    :goto_1
    if-lez v1, :cond_3

    .line 1271
    add-int/lit8 v4, v1, -0x1

    aget-object v4, v3, v4

    invoke-direct {p0, v4}, Lcom/infraware/filemanager/FileInfoActivity;->getLocalSubfolders(Ljava/io/File;)V

    .line 1272
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 1274
    :cond_3
    return-void
.end method

.method private getSdcardIntentFilter()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 1278
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1279
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1280
    const-string/jumbo v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 1281
    return-object v0
.end method

.method private getTextWidth(I)I
    .locals 3
    .param p1, "text"    # I

    .prologue
    .line 1224
    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandType:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/infraware/filemanager/FileInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v1

    float-to-int v0, v1

    .line 1226
    .local v0, "textWidth":I
    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortLocation:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setWidth(I)V

    .line 1227
    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandLocation:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setWidth(I)V

    .line 1229
    add-int/lit8 v1, v0, 0x3

    return v1
.end method

.method private getTextWidth(Ljava/lang/String;)I
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 1217
    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandType:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v1

    float-to-int v0, v1

    .line 1219
    .local v0, "textWidth":I
    add-int/lit8 v1, v0, 0x3

    return v1
.end method

.method private getThumbnail(Lcom/infraware/filemanager/file/FileListItem;)V
    .locals 8
    .param p1, "item"    # Lcom/infraware/filemanager/file/FileListItem;

    .prologue
    const/16 v7, 0x7d0

    .line 760
    iget-object v5, p1, Lcom/infraware/filemanager/file/FileListItem;->webExt:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    .line 761
    iget-object v1, p1, Lcom/infraware/filemanager/file/FileListItem;->webExt:Ljava/lang/String;

    .line 765
    .local v1, "extension":Ljava/lang/String;
    :goto_0
    invoke-static {v1}, Lcom/infraware/common/util/FileUtils;->isSupported(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 767
    new-instance v2, Ljava/io/File;

    invoke-virtual {p1}, Lcom/infraware/filemanager/file/FileListItem;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 768
    .local v2, "file":Ljava/io/File;
    invoke-static {p0}, Lcom/infraware/common/database/ThumbnailManager;->getInstance(Landroid/content/Context;)Lcom/infraware/common/database/ThumbnailManager;

    move-result-object v5

    invoke-virtual {v5, p0, v2}, Lcom/infraware/common/database/ThumbnailManager;->getThumbnail(Landroid/content/Context;Ljava/io/File;)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_bitmapThumb:Landroid/graphics/Bitmap;

    .line 770
    iget-object v5, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_bitmapThumb:Landroid/graphics/Bitmap;

    if-nez v5, :cond_0

    .line 772
    const/4 v5, 0x2

    invoke-static {v5, v1}, Lcom/infraware/common/util/FileUtils;->isDocumentType(ILjava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 773
    invoke-virtual {p0}, Lcom/infraware/filemanager/FileInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f02008e

    invoke-static {v5, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_bitmapThumb:Landroid/graphics/Bitmap;

    .line 808
    .end local v2    # "file":Ljava/io/File;
    :cond_0
    :goto_1
    return-void

    .line 763
    .end local v1    # "extension":Ljava/lang/String;
    :cond_1
    iget-object v1, p1, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    .restart local v1    # "extension":Ljava/lang/String;
    goto :goto_0

    .line 774
    .restart local v2    # "file":Ljava/io/File;
    :cond_2
    const/4 v5, 0x3

    invoke-static {v5, v1}, Lcom/infraware/common/util/FileUtils;->isDocumentType(ILjava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 775
    invoke-virtual {p0}, Lcom/infraware/filemanager/FileInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f02008d

    invoke-static {v5, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_bitmapThumb:Landroid/graphics/Bitmap;

    goto :goto_1

    .line 776
    :cond_3
    const-string/jumbo v5, "txt"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 777
    invoke-virtual {p0}, Lcom/infraware/filemanager/FileInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f02008f

    invoke-static {v5, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_bitmapThumb:Landroid/graphics/Bitmap;

    goto :goto_1

    .line 779
    :cond_4
    invoke-virtual {p0}, Lcom/infraware/filemanager/FileInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020090

    invoke-static {v5, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_bitmapThumb:Landroid/graphics/Bitmap;

    goto :goto_1

    .line 785
    .end local v2    # "file":Ljava/io/File;
    :cond_5
    invoke-static {p0}, Lcom/infraware/filemanager/manager/IconManager;->getInstance(Landroid/content/Context;)Lcom/infraware/filemanager/manager/IconManager;

    move-result-object v5

    iget-object v6, p1, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/infraware/filemanager/manager/IconManager;->isImageExt(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 787
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 789
    .local v3, "options":Landroid/graphics/BitmapFactory$Options;
    invoke-virtual {p1}, Lcom/infraware/filemanager/file/FileListItem;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->getBitmapOfWidth(Ljava/lang/String;)I

    move-result v5

    if-gt v5, v7, :cond_6

    invoke-virtual {p1}, Lcom/infraware/filemanager/file/FileListItem;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->getBitmapOfHeight(Ljava/lang/String;)I

    move-result v5

    if-le v5, v7, :cond_7

    .line 791
    :cond_6
    invoke-virtual {p1}, Lcom/infraware/filemanager/file/FileListItem;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->getBitmapOfWidth(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p1}, Lcom/infraware/filemanager/file/FileListItem;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/infraware/common/util/Utils;->getBitmapOfHeight(Ljava/lang/String;)I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    div-int/lit16 v4, v5, 0x7d0

    .line 792
    .local v4, "resizeRate":I
    mul-int/lit8 v5, v4, 0x2

    iput v5, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 793
    const/4 v5, 0x1

    iput-boolean v5, v3, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 795
    .end local v4    # "resizeRate":I
    :cond_7
    const/16 v5, 0x4000

    new-array v5, v5, [B

    iput-object v5, v3, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 797
    :try_start_0
    invoke-virtual {p1}, Lcom/infraware/filemanager/file/FileListItem;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_bitmapThumb:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 798
    :catch_0
    move-exception v0

    .line 799
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    new-instance v5, Lcom/infraware/filemanager/FileInfoActivity$1;

    invoke-direct {v5, p0}, Lcom/infraware/filemanager/FileInfoActivity$1;-><init>(Lcom/infraware/filemanager/FileInfoActivity;)V

    invoke-virtual {p0, v5}, Lcom/infraware/filemanager/FileInfoActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_1
.end method

.method private initFromExtra()V
    .locals 13

    .prologue
    .line 507
    invoke-virtual {p0}, Lcom/infraware/filemanager/FileInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 509
    .local v4, "intent":Landroid/content/Intent;
    const-string/jumbo v11, "INTCMD"

    const/4 v12, 0x0

    invoke-virtual {v4, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    iput v11, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nInternalCmdType:I

    .line 511
    iget v11, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nInternalCmdType:I

    const/4 v12, 0x2

    if-eq v11, v12, :cond_0

    iget v11, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nInternalCmdType:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_1

    .line 513
    :cond_0
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/infraware/filemanager/FileInfoActivity;->isNewDoc:Z

    .line 515
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/filemanager/FileInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v11

    const-string/jumbo v12, "key_file_item"

    invoke-virtual {v11, v12}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v2

    .line 517
    .local v2, "extraStream":[B
    if-eqz v2, :cond_2

    .line 519
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 522
    .local v0, "bis":Ljava/io/ByteArrayInputStream;
    :try_start_0
    new-instance v7, Ljava/io/ObjectInputStream;

    invoke-direct {v7, v0}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 523
    .local v7, "ois":Ljava/io/ObjectInputStream;
    invoke-virtual {v7}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/infraware/filemanager/file/FileListItem;

    iput-object v11, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oFileItem:Ljava/lang/Object;

    .line 525
    invoke-virtual {v7}, Ljava/io/ObjectInputStream;->close()V
    :try_end_0
    .catch Ljava/io/StreamCorruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    .line 535
    .end local v0    # "bis":Ljava/io/ByteArrayInputStream;
    .end local v7    # "ois":Ljava/io/ObjectInputStream;
    :cond_2
    :goto_0
    iget-object v11, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oFileItem:Ljava/lang/Object;

    if-nez v11, :cond_5

    .line 537
    const-string/jumbo v11, "key_current_file"

    invoke-virtual {v4, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 538
    .local v10, "strFileName":Ljava/lang/String;
    if-nez v10, :cond_4

    .line 602
    .end local v10    # "strFileName":Ljava/lang/String;
    :cond_3
    :goto_1
    return-void

    .line 526
    .restart local v0    # "bis":Ljava/io/ByteArrayInputStream;
    :catch_0
    move-exception v1

    .line 527
    .local v1, "e":Ljava/io/StreamCorruptedException;
    const/4 v11, 0x0

    iput-object v11, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oFileItem:Ljava/lang/Object;

    goto :goto_0

    .line 528
    .end local v1    # "e":Ljava/io/StreamCorruptedException;
    :catch_1
    move-exception v1

    .line 529
    .local v1, "e":Ljava/io/IOException;
    const/4 v11, 0x0

    iput-object v11, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oFileItem:Ljava/lang/Object;

    goto :goto_0

    .line 530
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 531
    .local v1, "e":Ljava/lang/ClassNotFoundException;
    const/4 v11, 0x0

    iput-object v11, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oFileItem:Ljava/lang/Object;

    goto :goto_0

    .line 541
    .end local v0    # "bis":Ljava/io/ByteArrayInputStream;
    .end local v1    # "e":Ljava/lang/ClassNotFoundException;
    .restart local v10    # "strFileName":Ljava/lang/String;
    :cond_4
    iget-boolean v11, p0, Lcom/infraware/filemanager/FileInfoActivity;->isNewDoc:Z

    if-eqz v11, :cond_9

    .line 543
    new-instance v5, Lcom/infraware/filemanager/file/FileListItem;

    invoke-direct {v5}, Lcom/infraware/filemanager/file/FileListItem;-><init>()V

    .line 544
    .local v5, "item":Lcom/infraware/filemanager/file/FileListItem;
    const/4 v11, 0x0

    iput-boolean v11, v5, Lcom/infraware/filemanager/file/FileListItem;->isFolder:Z

    .line 545
    const/4 v11, 0x1

    iput v11, v5, Lcom/infraware/filemanager/file/FileListItem;->type:I

    .line 547
    const/4 v8, 0x0

    .line 548
    .local v8, "path":Ljava/lang/String;
    const/4 v6, 0x0

    .line 550
    .local v6, "name":Ljava/lang/String;
    const-string/jumbo v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    .line 551
    .local v9, "slashIdx":I
    if-gez v9, :cond_7

    .line 552
    const-string/jumbo v8, ""

    .line 558
    :goto_2
    add-int/lit8 v11, v9, 0x1

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v12

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 560
    const v11, 0x7f070284

    invoke-virtual {p0, v11}, Lcom/infraware/filemanager/FileInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v5, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    .line 561
    invoke-virtual {v5, v6}, Lcom/infraware/filemanager/file/FileListItem;->setName(Ljava/lang/String;)V

    .line 563
    const v11, 0x7f070284

    invoke-virtual {p0, v11}, Lcom/infraware/filemanager/FileInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v5, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    .line 565
    iput-object v5, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oFileItem:Ljava/lang/Object;

    .line 567
    const-string/jumbo v11, "User"

    iput-object v11, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strDocAuthor:Ljava/lang/String;

    .line 568
    const v11, 0x7f070284

    invoke-virtual {p0, v11}, Lcom/infraware/filemanager/FileInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strDocLastEditor:Ljava/lang/String;

    .line 569
    const v11, 0x7f070278

    invoke-virtual {p0, v11}, Lcom/infraware/filemanager/FileInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strDocTitle:Ljava/lang/String;

    .line 589
    .end local v6    # "name":Ljava/lang/String;
    .end local v8    # "path":Ljava/lang/String;
    .end local v9    # "slashIdx":I
    :goto_3
    invoke-direct {p0, v4}, Lcom/infraware/filemanager/FileInfoActivity;->updateDocInfo(Landroid/content/Intent;)V

    .line 592
    .end local v5    # "item":Lcom/infraware/filemanager/file/FileListItem;
    .end local v10    # "strFileName":Ljava/lang/String;
    :cond_5
    iget-object v11, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oFileItem:Ljava/lang/Object;

    check-cast v11, Lcom/infraware/filemanager/file/FileListItem;

    iget-object v11, v11, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    const-string/jumbo v12, "doc"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_6

    iget-object v11, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oFileItem:Ljava/lang/Object;

    check-cast v11, Lcom/infraware/filemanager/file/FileListItem;

    iget-object v11, v11, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    const-string/jumbo v12, "docx"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_6

    iget-object v11, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oFileItem:Ljava/lang/Object;

    check-cast v11, Lcom/infraware/filemanager/file/FileListItem;

    iget-object v11, v11, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    const-string/jumbo v12, "hwp"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 596
    :cond_6
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/infraware/filemanager/FileInfoActivity;->isDoc:Z

    goto/16 :goto_1

    .line 553
    .restart local v5    # "item":Lcom/infraware/filemanager/file/FileListItem;
    .restart local v6    # "name":Ljava/lang/String;
    .restart local v8    # "path":Ljava/lang/String;
    .restart local v9    # "slashIdx":I
    .restart local v10    # "strFileName":Ljava/lang/String;
    :cond_7
    if-nez v9, :cond_8

    .line 554
    const-string/jumbo v8, "/"

    goto :goto_2

    .line 556
    :cond_8
    const/4 v11, 0x0

    invoke-virtual {v10, v11, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_2

    .line 573
    .end local v5    # "item":Lcom/infraware/filemanager/file/FileListItem;
    .end local v6    # "name":Ljava/lang/String;
    .end local v8    # "path":Ljava/lang/String;
    .end local v9    # "slashIdx":I
    :cond_9
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 574
    .local v3, "file":Ljava/io/File;
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 578
    new-instance v5, Lcom/infraware/filemanager/file/FileListItem;

    invoke-direct {v5}, Lcom/infraware/filemanager/file/FileListItem;-><init>()V

    .line 579
    .restart local v5    # "item":Lcom/infraware/filemanager/file/FileListItem;
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v11

    iput-boolean v11, v5, Lcom/infraware/filemanager/file/FileListItem;->isFolder:Z

    .line 580
    const/4 v11, 0x1

    iput v11, v5, Lcom/infraware/filemanager/file/FileListItem;->type:I

    .line 581
    invoke-virtual {v3}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v11

    iput-object v11, v5, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    .line 582
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Lcom/infraware/filemanager/file/FileListItem;->setName(Ljava/lang/String;)V

    .line 583
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v11

    iput-wide v11, v5, Lcom/infraware/filemanager/file/FileListItem;->size:J

    .line 584
    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v11

    iput-wide v11, v5, Lcom/infraware/filemanager/file/FileListItem;->updateTime:J

    .line 586
    iput-object v5, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oFileItem:Ljava/lang/Object;

    goto/16 :goto_3

    .line 598
    .end local v3    # "file":Ljava/io/File;
    .end local v5    # "item":Lcom/infraware/filemanager/file/FileListItem;
    .end local v10    # "strFileName":Ljava/lang/String;
    :cond_a
    iget-object v11, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oFileItem:Ljava/lang/Object;

    check-cast v11, Lcom/infraware/filemanager/file/FileListItem;

    iget-object v11, v11, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    const-string/jumbo v12, "pdf"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 600
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/infraware/filemanager/FileInfoActivity;->isPdf:Z

    goto/16 :goto_1
.end method

.method private initView()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 824
    const v0, 0x7f0b0031

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutTitle:Landroid/widget/LinearLayout;

    .line 825
    const v0, 0x7f0b0034

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvTitle:Landroid/widget/TextView;

    .line 826
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvTitle:Landroid/widget/TextView;

    const v1, 0x7f070297

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 828
    const v0, 0x7f0b008a

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutPort:Landroid/widget/LinearLayout;

    .line 829
    const v0, 0x7f0b00af

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutLand:Landroid/widget/LinearLayout;

    .line 831
    const v0, 0x7f0b0093

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutPortSize:Landroid/widget/LinearLayout;

    .line 832
    const v0, 0x7f0b009d

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutPortContents:Landroid/widget/LinearLayout;

    .line 833
    const v0, 0x7f0b00ac

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutPortAttributes:Landroid/widget/LinearLayout;

    .line 835
    const v0, 0x7f0b009a

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutPortAuthor:Landroid/widget/LinearLayout;

    .line 836
    const v0, 0x7f0b00a3

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutPortModifiedBy:Landroid/widget/LinearLayout;

    .line 837
    const v0, 0x7f0b0097

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutPortTitle:Landroid/widget/LinearLayout;

    .line 838
    const v0, 0x7f0b00a6

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutPortPage:Landroid/widget/LinearLayout;

    .line 840
    const v0, 0x7f0b00a9

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutPortWords:Landroid/widget/LinearLayout;

    .line 842
    const v0, 0x7f0b00b8

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutLandSize:Landroid/widget/LinearLayout;

    .line 843
    const v0, 0x7f0b00c2

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutLandContents:Landroid/widget/LinearLayout;

    .line 844
    const v0, 0x7f0b00d1

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutLandAttributes:Landroid/widget/LinearLayout;

    .line 847
    const v0, 0x7f0b00bf

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutLandAuthor:Landroid/widget/LinearLayout;

    .line 848
    const v0, 0x7f0b00c8

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutLandModifiedBy:Landroid/widget/LinearLayout;

    .line 849
    const v0, 0x7f0b00bc

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutLandTitle:Landroid/widget/LinearLayout;

    .line 850
    const v0, 0x7f0b00cb

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutLandPage:Landroid/widget/LinearLayout;

    .line 852
    const v0, 0x7f0b00ce

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutLandWords:Landroid/widget/LinearLayout;

    .line 854
    const v0, 0x7f0b008b

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutPortThumb:Landroid/widget/LinearLayout;

    .line 855
    const v0, 0x7f0b008c

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_ivPortThumb:Landroid/widget/ImageView;

    .line 856
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutPortThumb:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 857
    const v0, 0x7f0b008d

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_ivPortIcon:Landroid/widget/ImageView;

    .line 858
    const v0, 0x7f0b008e

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortName:Landroid/widget/TextView;

    .line 860
    const v0, 0x7f0b008f

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleType:Landroid/widget/TextView;

    .line 861
    const v0, 0x7f0b0091

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleLocation:Landroid/widget/TextView;

    .line 862
    const v0, 0x7f0b0094

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleSize:Landroid/widget/TextView;

    .line 863
    const v0, 0x7f0b009e

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleContents:Landroid/widget/TextView;

    .line 864
    const v0, 0x7f0b00a1

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleModified:Landroid/widget/TextView;

    .line 865
    const v0, 0x7f0b00ad

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleAttributes:Landroid/widget/TextView;

    .line 867
    const v0, 0x7f0b0090

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortType:Landroid/widget/TextView;

    .line 868
    const v0, 0x7f0b0092

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortLocation:Landroid/widget/TextView;

    .line 869
    const v0, 0x7f0b0095

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortSize:Landroid/widget/TextView;

    .line 870
    const v0, 0x7f0b009f

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortContents:Landroid/widget/TextView;

    .line 871
    const v0, 0x7f0b00a2

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortModified:Landroid/widget/TextView;

    .line 872
    const v0, 0x7f0b00ae

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortAttributes:Landroid/widget/TextView;

    .line 874
    const v0, 0x7f0b00b0

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutLandThumb:Landroid/widget/LinearLayout;

    .line 875
    const v0, 0x7f0b00b1

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_ivLandThumb:Landroid/widget/ImageView;

    .line 876
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutLandThumb:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 877
    const v0, 0x7f0b00b2

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_ivLandIcon:Landroid/widget/ImageView;

    .line 878
    const v0, 0x7f0b00b3

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandName:Landroid/widget/TextView;

    .line 880
    const v0, 0x7f0b00b4

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleType:Landroid/widget/TextView;

    .line 881
    const v0, 0x7f0b00b6

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleLocation:Landroid/widget/TextView;

    .line 882
    const v0, 0x7f0b00b9

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleSize:Landroid/widget/TextView;

    .line 883
    const v0, 0x7f0b00c3

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleContents:Landroid/widget/TextView;

    .line 884
    const v0, 0x7f0b00c6

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleModified:Landroid/widget/TextView;

    .line 885
    const v0, 0x7f0b00d2

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleAttributes:Landroid/widget/TextView;

    .line 887
    const v0, 0x7f0b00b5

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandType:Landroid/widget/TextView;

    .line 888
    const v0, 0x7f0b00b7

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandLocation:Landroid/widget/TextView;

    .line 889
    const v0, 0x7f0b00ba

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandSize:Landroid/widget/TextView;

    .line 890
    const v0, 0x7f0b00c4

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandContents:Landroid/widget/TextView;

    .line 891
    const v0, 0x7f0b00c7

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandModified:Landroid/widget/TextView;

    .line 892
    const v0, 0x7f0b00d3

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandAttributes:Landroid/widget/TextView;

    .line 894
    const v0, 0x7f0b0096

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_pbPortProgressSize:Landroid/widget/ProgressBar;

    .line 895
    const v0, 0x7f0b00bb

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_pbLandProgressSize:Landroid/widget/ProgressBar;

    .line 896
    const v0, 0x7f0b00a0

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_pbPortProgressContents:Landroid/widget/ProgressBar;

    .line 897
    const v0, 0x7f0b00c5

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_pbLandProgressContents:Landroid/widget/ProgressBar;

    .line 899
    const v0, 0x7f0b0098

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleDocTitle:Landroid/widget/TextView;

    .line 900
    const v0, 0x7f0b009b

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleDocAuthor:Landroid/widget/TextView;

    .line 901
    const v0, 0x7f0b00a4

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleDocModifiedBy:Landroid/widget/TextView;

    .line 902
    const v0, 0x7f0b00a7

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleDocPage:Landroid/widget/TextView;

    .line 904
    const v0, 0x7f0b00aa

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleDocWords:Landroid/widget/TextView;

    .line 906
    const v0, 0x7f0b00bd

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleDocTitle:Landroid/widget/TextView;

    .line 907
    const v0, 0x7f0b00c0

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleDocAuthor:Landroid/widget/TextView;

    .line 908
    const v0, 0x7f0b00c9

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleDocModifiedBy:Landroid/widget/TextView;

    .line 909
    const v0, 0x7f0b00cc

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleDocPage:Landroid/widget/TextView;

    .line 911
    const v0, 0x7f0b00cf

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleDocWords:Landroid/widget/TextView;

    .line 913
    const v0, 0x7f0b0099

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortDocTitle:Landroid/widget/TextView;

    .line 914
    const v0, 0x7f0b009c

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortDocAuthor:Landroid/widget/TextView;

    .line 915
    const v0, 0x7f0b00a5

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortDocModifiedBy:Landroid/widget/TextView;

    .line 916
    const v0, 0x7f0b00a8

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortDocPage:Landroid/widget/TextView;

    .line 918
    const v0, 0x7f0b00ab

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortDocWords:Landroid/widget/TextView;

    .line 920
    const v0, 0x7f0b00be

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandDocTitle:Landroid/widget/TextView;

    .line 921
    const v0, 0x7f0b00c1

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandDocAuthor:Landroid/widget/TextView;

    .line 922
    const v0, 0x7f0b00ca

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandDocModifiedBy:Landroid/widget/TextView;

    .line 923
    const v0, 0x7f0b00cd

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandDocPage:Landroid/widget/TextView;

    .line 925
    const v0, 0x7f0b00d0

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandDocWords:Landroid/widget/TextView;

    .line 927
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_arryTvPortTitle:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleType:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 928
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_arryTvPortTitle:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleLocation:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 929
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_arryTvPortTitle:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleModified:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 931
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_arryTvLandTitle:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleType:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 932
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_arryTvLandTitle:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleLocation:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 933
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_arryTvLandTitle:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleModified:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 935
    iget-boolean v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->isFolder:Z

    if-nez v0, :cond_5

    .line 937
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutPortContents:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 938
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutLandContents:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 945
    :goto_0
    iget-boolean v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->isNoSize:Z

    if-eqz v0, :cond_6

    .line 947
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutPortSize:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 948
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutLandSize:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 957
    :goto_1
    iget-boolean v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->isDoc:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oThumbItem:Lcom/infraware/common/database/ThumbnailItem;

    if-nez v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nDocType:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nDocType:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_3

    .line 960
    :cond_1
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutLandPage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 961
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutPortPage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 964
    invoke-direct {p0}, Lcom/infraware/filemanager/FileInfoActivity;->isWordsNumSupport()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 966
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutLandWords:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 967
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutPortWords:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 970
    :cond_2
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_arryTvLandTitle:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleDocPage:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 971
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_arryTvPortTitle:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleDocPage:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 973
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_arryTvPortTitle:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleDocWords:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 974
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_arryTvLandTitle:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleDocWords:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 977
    :cond_3
    iget-boolean v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->isNewDoc:Z

    if-eqz v0, :cond_4

    .line 979
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutPortAttributes:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 980
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutLandAttributes:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 982
    :cond_4
    return-void

    .line 941
    :cond_5
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_arryTvLandTitle:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleContents:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 942
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_arryTvPortTitle:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleContents:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 951
    :cond_6
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_arryTvPortTitle:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleSize:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 952
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_arryTvLandTitle:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleSize:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private isWordsNumSupport()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 812
    invoke-virtual {p0}, Lcom/infraware/filemanager/FileInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 813
    .local v0, "systemLocale":Ljava/lang/String;
    sget-object v2, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "th"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine;->isChina()Z

    move-result v2

    if-ne v2, v1, :cond_1

    .line 817
    :cond_0
    const/4 v1, 0x0

    .line 819
    :cond_1
    return v1
.end method

.method private onChangeCount()V
    .locals 4

    .prologue
    .line 489
    const v2, 0x7f070277

    invoke-virtual {p0, v2}, Lcom/infraware/filemanager/FileInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 490
    .local v0, "strCount":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 491
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nContentsFolder:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 492
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 493
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f070276

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/FileInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 494
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 495
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nContentsFile:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 497
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortContents:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 498
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandContents:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 500
    invoke-direct {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->getTextWidth(Ljava/lang/String;)I

    move-result v1

    .line 501
    .local v1, "textWidth":I
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortContents:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setWidth(I)V

    .line 502
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandContents:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setWidth(I)V

    .line 503
    return-void
.end method

.method private onChangeSize()V
    .locals 4

    .prologue
    const v2, 0x7f070284

    .line 465
    iget-boolean v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->isNewDoc:Z

    if-eqz v1, :cond_0

    .line 467
    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortSize:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 468
    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandSize:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 470
    invoke-direct {p0, v2}, Lcom/infraware/filemanager/FileInfoActivity;->getTextWidth(I)I

    move-result v0

    .line 471
    .local v0, "textWidth":I
    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortSize:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setWidth(I)V

    .line 472
    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandSize:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setWidth(I)V

    .line 483
    :goto_0
    return-void

    .line 476
    .end local v0    # "textWidth":I
    :cond_0
    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortSize:Landroid/widget/TextView;

    iget-wide v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nContentsSize:J

    invoke-static {v2, v3}, Lcom/infraware/common/util/FileUtils;->getSizeString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 477
    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandSize:Landroid/widget/TextView;

    iget-wide v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nContentsSize:J

    invoke-static {v2, v3}, Lcom/infraware/common/util/FileUtils;->getSizeString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 479
    iget-wide v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nContentsSize:J

    invoke-static {v1, v2}, Lcom/infraware/common/util/FileUtils;->getSizeString(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/infraware/filemanager/FileInfoActivity;->getTextWidth(Ljava/lang/String;)I

    move-result v0

    .line 480
    .restart local v0    # "textWidth":I
    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortSize:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setWidth(I)V

    .line 481
    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandSize:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setWidth(I)V

    goto :goto_0
.end method

.method private onChangeThumbnail()V
    .locals 9

    .prologue
    const v8, 0x7f02005e

    const v7, 0x7f02005d

    .line 416
    iget-object v5, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_bitmapThumb:Landroid/graphics/Bitmap;

    if-nez v5, :cond_0

    .line 461
    :goto_0
    return-void

    .line 420
    :cond_0
    iget-object v5, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_bitmapThumb:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    iget-object v6, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_bitmapThumb:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    if-ge v5, v6, :cond_1

    .line 422
    const/high16 v5, 0x426a0000    # 58.5f

    invoke-static {p0, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v3

    .line 423
    .local v3, "nX":I
    const/high16 v5, 0x41780000    # 15.5f

    invoke-static {p0, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v4

    .line 424
    .local v4, "nY":I
    const/high16 v5, 0x43370000    # 183.0f

    invoke-static {p0, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v1

    .line 425
    .local v1, "nCX":I
    const v5, 0x43848000    # 265.0f

    invoke-static {p0, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v2

    .line 427
    .local v2, "nCY":I
    iget-object v5, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutPortThumb:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v8}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 428
    iget-object v5, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutLandThumb:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v8}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 443
    :goto_1
    iget-object v5, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_ivPortThumb:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 444
    .local v0, "layoutParam":Landroid/widget/LinearLayout$LayoutParams;
    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 445
    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 446
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 447
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 449
    iget-object v5, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_ivPortThumb:Landroid/widget/ImageView;

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 451
    iget-object v5, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_ivLandThumb:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "layoutParam":Landroid/widget/LinearLayout$LayoutParams;
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 452
    .restart local v0    # "layoutParam":Landroid/widget/LinearLayout$LayoutParams;
    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 453
    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 454
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 455
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 457
    iget-object v5, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_ivLandThumb:Landroid/widget/ImageView;

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 459
    iget-object v5, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_ivPortThumb:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_bitmapThumb:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 460
    iget-object v5, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_ivLandThumb:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_bitmapThumb:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 432
    .end local v0    # "layoutParam":Landroid/widget/LinearLayout$LayoutParams;
    .end local v1    # "nCX":I
    .end local v2    # "nCY":I
    .end local v3    # "nX":I
    .end local v4    # "nY":I
    :cond_1
    const/high16 v5, 0x41580000    # 13.5f

    invoke-static {p0, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v3

    .line 433
    .restart local v3    # "nX":I
    const/high16 v5, 0x42360000    # 45.5f

    invoke-static {p0, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v4

    .line 434
    .restart local v4    # "nY":I
    const v5, 0x43888000    # 273.0f

    invoke-static {p0, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v1

    .line 435
    .restart local v1    # "nCX":I
    const/high16 v5, 0x434d0000    # 205.0f

    invoke-static {p0, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v2

    .line 437
    .restart local v2    # "nCY":I
    iget-object v5, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutPortThumb:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v7}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 438
    iget-object v5, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutLandThumb:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v7}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_1
.end method

.method private onLocaleChanged(I)V
    .locals 7
    .param p1, "nLocale"    # I

    .prologue
    const v6, 0x7f07027d

    const v5, 0x7f07027c

    const v4, 0x7f07027b

    const v3, 0x7f07027a

    const v2, 0x7f070279

    .line 378
    iput p1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nLocaleType:I

    .line 380
    const v0, 0x7f0b0034

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvTitle:Landroid/widget/TextView;

    .line 381
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvTitle:Landroid/widget/TextView;

    const v1, 0x7f070297

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 383
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleType:Landroid/widget/TextView;

    const v1, 0x7f070282

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 384
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleLocation:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 385
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleSize:Landroid/widget/TextView;

    const v1, 0x7f070280

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 386
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleContents:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 387
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleModified:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(I)V

    .line 388
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleAttributes:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 390
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleType:Landroid/widget/TextView;

    const v1, 0x7f070282

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 391
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleLocation:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 392
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleSize:Landroid/widget/TextView;

    const v1, 0x7f070280

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 393
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleContents:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 394
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleModified:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(I)V

    .line 395
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleAttributes:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 397
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleDocTitle:Landroid/widget/TextView;

    const v1, 0x7f070281

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 398
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleDocAuthor:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 399
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleDocModifiedBy:Landroid/widget/TextView;

    const v1, 0x7f07027e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 400
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleDocPage:Landroid/widget/TextView;

    const v1, 0x7f07027f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 404
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleDocTitle:Landroid/widget/TextView;

    const v1, 0x7f070281

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 405
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleDocAuthor:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 406
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleDocModifiedBy:Landroid/widget/TextView;

    const v1, 0x7f07027e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 407
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleDocPage:Landroid/widget/TextView;

    const v1, 0x7f07027f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 411
    invoke-direct {p0}, Lcom/infraware/filemanager/FileInfoActivity;->setTextTitleSize()V

    .line 412
    return-void
.end method

.method private onOrientationChanged()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 346
    invoke-virtual {p0}, Lcom/infraware/filemanager/FileInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    iput v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nOrientation:I

    .line 347
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v2

    if-nez v2, :cond_0

    .line 349
    invoke-virtual {p0}, Lcom/infraware/filemanager/FileInfoActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    iget v3, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nOrientation:I

    invoke-static {v2, v3}, Lcom/infraware/common/util/Utils;->setScreenMode(Landroid/view/Window;I)V

    .line 352
    :cond_0
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvTitle:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 353
    .local v1, "layoutParamTitleText":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v2, 0x41500000    # 13.0f

    invoke-static {p0, v2}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 354
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 356
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 358
    .local v0, "layoutParam":Landroid/widget/LinearLayout$LayoutParams;
    iget v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nOrientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 360
    const/high16 v2, 0x42400000    # 48.0f

    invoke-static {p0, v2}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 361
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 363
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutPort:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 364
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutLand:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 374
    :goto_0
    return-void

    .line 368
    :cond_1
    const/high16 v2, 0x42200000    # 40.0f

    invoke-static {p0, v2}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 369
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 371
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutPort:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 372
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutLand:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private setLocalInfo(Lcom/infraware/filemanager/file/FileListItem;)V
    .locals 11
    .param p1, "item"    # Lcom/infraware/filemanager/file/FileListItem;

    .prologue
    const-wide/16 v9, 0x1f4

    const/4 v8, 0x1

    const v7, 0x7f070278

    const/4 v6, -0x1

    .line 668
    iget-boolean v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->isNewDoc:Z

    if-eqz v4, :cond_2

    .line 670
    iget-object v4, p1, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    iput-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strFileName:Ljava/lang/String;

    .line 671
    iget-object v4, p1, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    iput-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strLocation:Ljava/lang/String;

    .line 679
    :goto_0
    new-instance v1, Lcom/infraware/filemanager/file/FileListIcon;

    invoke-virtual {p0}, Lcom/infraware/filemanager/FileInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/infraware/filemanager/file/FileListIcon;-><init>(Landroid/content/Context;)V

    .line 680
    .local v1, "fileIcon":Lcom/infraware/filemanager/file/FileListIcon;
    iget-boolean v4, p1, Lcom/infraware/filemanager/file/FileListItem;->isFolder:Z

    if-eqz v4, :cond_3

    .line 682
    iput-boolean v8, p0, Lcom/infraware/filemanager/FileInfoActivity;->isFolder:Z

    .line 684
    iget-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strFileName:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/infraware/filemanager/file/FileListIcon;->getFolderIcon(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_bitmapIcon:Landroid/graphics/Bitmap;

    .line 685
    const v4, 0x7f070288

    iput v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nTypeId:I

    .line 687
    iget-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oInfoHandler:Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;

    const/4 v5, 0x2

    invoke-virtual {v4, v5, p1}, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    .line 688
    .local v3, "message":Landroid/os/Message;
    iget-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oInfoHandler:Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;

    invoke-virtual {v4, v3, v9, v10}, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 744
    :goto_1
    iget-wide v4, p1, Lcom/infraware/filemanager/file/FileListItem;->updateTime:J

    iput-wide v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nLastModified:J

    .line 746
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Lcom/infraware/filemanager/file/FileListItem;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 747
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->canWrite()Z

    move-result v4

    if-nez v4, :cond_0

    .line 748
    const v4, 0x7f070274

    iput v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nAttributesId:I

    .line 750
    :cond_0
    iget-boolean v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->isNewDoc:Z

    if-eqz v4, :cond_1

    .line 751
    const/4 v4, 0x0

    iput v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nAttributesId:I

    .line 754
    :cond_1
    return-void

    .line 675
    .end local v0    # "file":Ljava/io/File;
    .end local v1    # "fileIcon":Lcom/infraware/filemanager/file/FileListIcon;
    .end local v3    # "message":Landroid/os/Message;
    :cond_2
    invoke-virtual {p1}, Lcom/infraware/filemanager/file/FileListItem;->getFullFileName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strFileName:Ljava/lang/String;

    .line 676
    invoke-virtual {p1}, Lcom/infraware/filemanager/file/FileListItem;->getPath()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strLocation:Ljava/lang/String;

    goto :goto_0

    .line 692
    .restart local v1    # "fileIcon":Lcom/infraware/filemanager/file/FileListIcon;
    :cond_3
    iget-object v4, p1, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    invoke-static {v4}, Lcom/infraware/filemanager/porting/DRM;->isDRMExt(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-static {}, Lcom/infraware/filemanager/porting/DRM;->getInstance()Lcom/infraware/filemanager/porting/DRM;

    move-result-object v4

    invoke-virtual {p1}, Lcom/infraware/filemanager/file/FileListItem;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, p0}, Lcom/infraware/filemanager/porting/DRM;->isDRM(Ljava/lang/String;Landroid/content/Context;)I

    move-result v4

    if-lez v4, :cond_7

    .line 694
    invoke-static {}, Lcom/infraware/filemanager/porting/DRM;->getInstance()Lcom/infraware/filemanager/porting/DRM;

    move-result-object v4

    invoke-virtual {v1, v4, p1}, Lcom/infraware/filemanager/file/FileListIcon;->getDRMFileIcon(Lcom/infraware/filemanager/porting/DRM;Lcom/infraware/filemanager/file/FileListItem;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_bitmapIcon:Landroid/graphics/Bitmap;

    .line 695
    invoke-static {}, Lcom/infraware/filemanager/porting/DRM;->getInstance()Lcom/infraware/filemanager/porting/DRM;

    move-result-object v4

    invoke-virtual {v1, v4, p1}, Lcom/infraware/filemanager/file/FileListIcon;->getDRMFileType(Lcom/infraware/filemanager/porting/DRM;Lcom/infraware/filemanager/file/FileListItem;)I

    move-result v4

    iput v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nTypeId:I

    .line 703
    :goto_2
    iget-wide v4, p1, Lcom/infraware/filemanager/file/FileListItem;->size:J

    iput-wide v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nContentsSize:J

    .line 705
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Lcom/infraware/filemanager/file/FileListItem;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 706
    .restart local v0    # "file":Ljava/io/File;
    invoke-static {p0}, Lcom/infraware/common/database/ThumbnailManager;->getInstance(Landroid/content/Context;)Lcom/infraware/common/database/ThumbnailManager;

    move-result-object v4

    invoke-virtual {v4, p0, v0}, Lcom/infraware/common/database/ThumbnailManager;->getThumbnailItem(Landroid/content/Context;Ljava/io/File;)Lcom/infraware/common/database/ThumbnailItem;

    move-result-object v4

    iput-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oThumbItem:Lcom/infraware/common/database/ThumbnailItem;

    .line 708
    iget-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oThumbItem:Lcom/infraware/common/database/ThumbnailItem;

    if-eqz v4, :cond_6

    .line 710
    iget-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oThumbItem:Lcom/infraware/common/database/ThumbnailItem;

    iget-object v4, v4, Lcom/infraware/common/database/ThumbnailItem;->docAuthor:Ljava/lang/String;

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oThumbItem:Lcom/infraware/common/database/ThumbnailItem;

    iget-object v4, v4, Lcom/infraware/common/database/ThumbnailItem;->docAuthor:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_8

    .line 711
    iget-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oThumbItem:Lcom/infraware/common/database/ThumbnailItem;

    iget-object v4, v4, Lcom/infraware/common/database/ThumbnailItem;->docAuthor:Ljava/lang/String;

    iput-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strDocAuthor:Ljava/lang/String;

    .line 715
    :goto_3
    iget-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oThumbItem:Lcom/infraware/common/database/ThumbnailItem;

    iget-object v4, v4, Lcom/infraware/common/database/ThumbnailItem;->docTitle:Ljava/lang/String;

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oThumbItem:Lcom/infraware/common/database/ThumbnailItem;

    iget-object v4, v4, Lcom/infraware/common/database/ThumbnailItem;->docTitle:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_9

    .line 716
    iget-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oThumbItem:Lcom/infraware/common/database/ThumbnailItem;

    iget-object v4, v4, Lcom/infraware/common/database/ThumbnailItem;->docTitle:Ljava/lang/String;

    iput-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strDocTitle:Ljava/lang/String;

    .line 720
    :goto_4
    iget-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oThumbItem:Lcom/infraware/common/database/ThumbnailItem;

    iget-object v4, v4, Lcom/infraware/common/database/ThumbnailItem;->docLastEditor:Ljava/lang/String;

    if-eqz v4, :cond_a

    iget-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oThumbItem:Lcom/infraware/common/database/ThumbnailItem;

    iget-object v4, v4, Lcom/infraware/common/database/ThumbnailItem;->docLastEditor:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_a

    .line 721
    iget-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oThumbItem:Lcom/infraware/common/database/ThumbnailItem;

    iget-object v4, v4, Lcom/infraware/common/database/ThumbnailItem;->docLastEditor:Ljava/lang/String;

    iput-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strDocLastEditor:Ljava/lang/String;

    .line 725
    :goto_5
    invoke-virtual {p0}, Lcom/infraware/filemanager/FileInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 726
    .local v2, "intent":Landroid/content/Intent;
    const-string/jumbo v4, "key_current_file_doc_type"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nDocType:I

    .line 727
    iget v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nDocType:I

    if-eq v6, v4, :cond_4

    iget v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nDocType:I

    if-eq v4, v8, :cond_5

    :cond_4
    iget v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nDocType:I

    const/4 v5, 0x6

    if-ne v4, v5, :cond_b

    .line 730
    :cond_5
    const-string/jumbo v4, "key_current_file_doc_page"

    iget-object v5, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oThumbItem:Lcom/infraware/common/database/ThumbnailItem;

    iget v5, v5, Lcom/infraware/common/database/ThumbnailItem;->docPage:I

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nDocPage:I

    .line 731
    const-string/jumbo v4, "key_current_file_doc_words"

    iget-object v5, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oThumbItem:Lcom/infraware/common/database/ThumbnailItem;

    iget v5, v5, Lcom/infraware/common/database/ThumbnailItem;->docWord:I

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nDocWord:I

    .line 740
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_6
    :goto_6
    iget-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oInfoHandler:Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;

    const/4 v5, 0x4

    invoke-virtual {v4, v5, p1}, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    .line 741
    .restart local v3    # "message":Landroid/os/Message;
    iget-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oInfoHandler:Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;

    invoke-virtual {v4, v3, v9, v10}, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_1

    .line 699
    .end local v0    # "file":Ljava/io/File;
    .end local v3    # "message":Landroid/os/Message;
    :cond_7
    invoke-virtual {v1, p1}, Lcom/infraware/filemanager/file/FileListIcon;->getFileIcon(Lcom/infraware/filemanager/file/FileListItem;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_bitmapIcon:Landroid/graphics/Bitmap;

    .line 700
    iget-object v4, p1, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/infraware/filemanager/file/FileListIcon;->getTypeByFileExt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nTypeId:I

    goto/16 :goto_2

    .line 713
    .restart local v0    # "file":Ljava/io/File;
    :cond_8
    invoke-virtual {p0, v7}, Lcom/infraware/filemanager/FileInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strDocAuthor:Ljava/lang/String;

    goto/16 :goto_3

    .line 718
    :cond_9
    invoke-virtual {p0, v7}, Lcom/infraware/filemanager/FileInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strDocTitle:Ljava/lang/String;

    goto :goto_4

    .line 723
    :cond_a
    invoke-virtual {p0, v7}, Lcom/infraware/filemanager/FileInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strDocLastEditor:Ljava/lang/String;

    goto :goto_5

    .line 733
    .restart local v2    # "intent":Landroid/content/Intent;
    :cond_b
    iget v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nDocType:I

    if-ne v6, v4, :cond_6

    .line 735
    iget-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oThumbItem:Lcom/infraware/common/database/ThumbnailItem;

    iget v4, v4, Lcom/infraware/common/database/ThumbnailItem;->docPage:I

    iput v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nDocPage:I

    .line 736
    iget-object v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oThumbItem:Lcom/infraware/common/database/ThumbnailItem;

    iget v4, v4, Lcom/infraware/common/database/ThumbnailItem;->docWord:I

    iput v4, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nDocWord:I

    goto :goto_6
.end method

.method private setTextTitleSize()V
    .locals 13

    .prologue
    .line 607
    iget-object v11, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_arryTvPortTitle:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v10

    .line 609
    .local v10, "titleCount":I
    const/4 v6, 0x0

    .line 612
    .local v6, "portCurrentWidth":I
    invoke-virtual {p0}, Lcom/infraware/filemanager/FileInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v11

    iget v11, v11, Landroid/content/res/Configuration;->orientation:I

    iput v11, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nOrientation:I

    .line 614
    iget v11, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nOrientation:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_1

    .line 616
    invoke-static {p0}, Lcom/infraware/common/util/Utils;->getScreenWidthPixels(Landroid/app/Activity;)I

    move-result v11

    div-int/lit8 v4, v11, 0x2

    .line 617
    .local v4, "maxPortWidth":I
    invoke-static {p0}, Lcom/infraware/common/util/Utils;->getScreenHightPixels(Landroid/app/Activity;)I

    move-result v11

    div-int/lit8 v3, v11, 0x2

    .line 624
    .local v3, "maxLandWidth":I
    :goto_0
    const/high16 v11, 0x429d0000    # 78.5f

    invoke-static {p0, v11}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v5

    .line 627
    .local v5, "minWidth":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v10, :cond_2

    .line 629
    iget-object v11, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_arryTvPortTitle:Ljava/util/ArrayList;

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 630
    .local v9, "textView":Landroid/widget/TextView;
    invoke-virtual {v9}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    .line 632
    .local v8, "str":Ljava/lang/String;
    invoke-virtual {v9}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v11

    invoke-virtual {v11, v8}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v11

    float-to-int v7, v11

    .line 634
    .local v7, "portSize":I
    if-le v7, v6, :cond_0

    .line 635
    move v6, v7

    .line 627
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 620
    .end local v1    # "i":I
    .end local v3    # "maxLandWidth":I
    .end local v4    # "maxPortWidth":I
    .end local v5    # "minWidth":I
    .end local v7    # "portSize":I
    .end local v8    # "str":Ljava/lang/String;
    .end local v9    # "textView":Landroid/widget/TextView;
    :cond_1
    invoke-static {p0}, Lcom/infraware/common/util/Utils;->getScreenWidthPixels(Landroid/app/Activity;)I

    move-result v11

    div-int/lit8 v4, v11, 0x2

    .line 621
    .restart local v4    # "maxPortWidth":I
    invoke-static {p0}, Lcom/infraware/common/util/Utils;->getScreenHightPixels(Landroid/app/Activity;)I

    move-result v11

    div-int/lit8 v3, v11, 0x2

    .restart local v3    # "maxLandWidth":I
    goto :goto_0

    .line 639
    .restart local v1    # "i":I
    .restart local v5    # "minWidth":I
    :cond_2
    move v0, v6

    .line 641
    .local v0, "LandCurrentWidth":I
    if-le v6, v4, :cond_5

    .line 642
    move v6, v4

    .line 647
    :cond_3
    :goto_2
    if-le v0, v3, :cond_6

    .line 648
    move v0, v3

    .line 653
    :cond_4
    :goto_3
    const/high16 v11, 0x41200000    # 10.0f

    invoke-static {p0, v11}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v2

    .line 654
    .local v2, "margin":I
    add-int/2addr v6, v2

    .line 655
    add-int/2addr v0, v2

    .line 657
    const/4 v1, 0x0

    :goto_4
    if-ge v1, v10, :cond_7

    .line 658
    iget-object v11, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_arryTvPortTitle:Ljava/util/ArrayList;

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 659
    .restart local v9    # "textView":Landroid/widget/TextView;
    invoke-virtual {v9, v6}, Landroid/widget/TextView;->setWidth(I)V

    .line 661
    iget-object v11, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_arryTvLandTitle:Ljava/util/ArrayList;

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "textView":Landroid/widget/TextView;
    check-cast v9, Landroid/widget/TextView;

    .line 662
    .restart local v9    # "textView":Landroid/widget/TextView;
    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setWidth(I)V

    .line 657
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 643
    .end local v2    # "margin":I
    .end local v9    # "textView":Landroid/widget/TextView;
    :cond_5
    if-ge v6, v5, :cond_3

    .line 644
    move v6, v5

    goto :goto_2

    .line 649
    :cond_6
    if-ge v6, v5, :cond_4

    .line 650
    move v6, v5

    goto :goto_3

    .line 664
    .restart local v2    # "margin":I
    :cond_7
    return-void
.end method

.method private updateDocInfo(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 986
    const-string/jumbo v0, "key_current_file_doc_type"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nDocType:I

    .line 987
    const-string/jumbo v0, "key_current_file_doc_page"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nDocPage:I

    .line 988
    const-string/jumbo v0, "key_current_file_doc_words"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nDocWord:I

    .line 989
    return-void
.end method

.method private updateInfo()V
    .locals 8

    .prologue
    const v7, 0x7f070284

    const/4 v4, 0x1

    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 993
    const/4 v1, 0x0

    .line 994
    .local v1, "textWidth":I
    const/4 v0, 0x0

    .line 995
    .local v0, "nThumbId":I
    iget v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nTypeId:I

    sparse-switch v2, :sswitch_data_0

    .line 1029
    :goto_0
    if-lez v0, :cond_0

    .line 1031
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutPortThumb:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 1032
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutLandThumb:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 1035
    :cond_0
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_bitmapIcon:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    .line 1037
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_ivPortIcon:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_bitmapIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1038
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_ivLandIcon:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_bitmapIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1041
    :cond_1
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strFileName:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 1043
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortName:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strFileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1044
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandName:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strFileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1046
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strFileName:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/infraware/filemanager/FileInfoActivity;->getTextWidth(Ljava/lang/String;)I

    move-result v1

    .line 1047
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortName:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setWidth(I)V

    .line 1048
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandName:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setWidth(I)V

    .line 1050
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortName:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1051
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandName:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1054
    :cond_2
    iget v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nTypeId:I

    if-lez v2, :cond_3

    .line 1056
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortType:Landroid/widget/TextView;

    iget v3, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nTypeId:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1057
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandType:Landroid/widget/TextView;

    iget v3, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nTypeId:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1059
    iget v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nTypeId:I

    invoke-direct {p0, v2}, Lcom/infraware/filemanager/FileInfoActivity;->getTextWidth(I)I

    move-result v1

    .line 1060
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortType:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setWidth(I)V

    .line 1061
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandType:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setWidth(I)V

    .line 1064
    :cond_3
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strLocation:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strLocation:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_5

    .line 1065
    :cond_4
    const-string/jumbo v2, "/"

    iput-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strLocation:Ljava/lang/String;

    .line 1067
    :cond_5
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortLocation:Landroid/widget/TextView;

    new-instance v3, Lcom/infraware/filemanager/FileInfoActivity$2;

    invoke-direct {v3, p0}, Lcom/infraware/filemanager/FileInfoActivity$2;-><init>(Lcom/infraware/filemanager/FileInfoActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    .line 1079
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandLocation:Landroid/widget/TextView;

    new-instance v3, Lcom/infraware/filemanager/FileInfoActivity$3;

    invoke-direct {v3, p0}, Lcom/infraware/filemanager/FileInfoActivity$3;-><init>(Lcom/infraware/filemanager/FileInfoActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    .line 1091
    invoke-direct {p0}, Lcom/infraware/filemanager/FileInfoActivity;->onChangeSize()V

    .line 1092
    invoke-direct {p0}, Lcom/infraware/filemanager/FileInfoActivity;->onChangeCount()V

    .line 1094
    iget-boolean v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->isNewDoc:Z

    if-eqz v2, :cond_a

    .line 1096
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortModified:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(I)V

    .line 1097
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandModified:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(I)V

    .line 1099
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strLocation:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/infraware/filemanager/FileInfoActivity;->getTextWidth(Ljava/lang/String;)I

    move-result v1

    .line 1100
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortModified:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setWidth(I)V

    .line 1101
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandModified:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setWidth(I)V

    .line 1113
    :goto_1
    iget v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nAttributesId:I

    if-lez v2, :cond_b

    .line 1115
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutPortAttributes:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1116
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutLandAttributes:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1118
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortAttributes:Landroid/widget/TextView;

    iget v3, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nAttributesId:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1119
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandAttributes:Landroid/widget/TextView;

    iget v3, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nAttributesId:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1121
    iget v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nAttributesId:I

    invoke-direct {p0, v2}, Lcom/infraware/filemanager/FileInfoActivity;->getTextWidth(I)I

    move-result v1

    .line 1122
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortAttributes:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setWidth(I)V

    .line 1123
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandAttributes:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setWidth(I)V

    .line 1125
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_arryTvPortTitle:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleAttributes:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1126
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_arryTvLandTitle:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleAttributes:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1134
    :goto_2
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strDocTitle:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strDocTitle:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_6

    .line 1136
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutPortTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1137
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutLandTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1139
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortDocTitle:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strDocTitle:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1140
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandDocTitle:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strDocTitle:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1142
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strDocTitle:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/infraware/filemanager/FileInfoActivity;->getTextWidth(Ljava/lang/String;)I

    move-result v1

    .line 1143
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortDocTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setWidth(I)V

    .line 1144
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandDocTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setWidth(I)V

    .line 1146
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_arryTvLandTitle:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandTitleDocTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1147
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_arryTvPortTitle:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortTitleDocTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1151
    :cond_6
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strDocAuthor:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strDocAuthor:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_7

    .line 1167
    :cond_7
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strDocLastEditor:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_strDocLastEditor:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_8

    iget-boolean v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->isPdf:Z

    if-nez v2, :cond_8

    .line 1183
    :cond_8
    iget-boolean v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->isDoc:Z

    if-eqz v2, :cond_9

    .line 1185
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortDocPage:Landroid/widget/TextView;

    iget v3, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nDocPage:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1186
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandDocPage:Landroid/widget/TextView;

    iget v3, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nDocPage:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1189
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortDocWords:Landroid/widget/TextView;

    iget v3, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nDocWord:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1190
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandDocWords:Landroid/widget/TextView;

    iget v3, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nDocWord:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1192
    iget v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nDocPage:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/infraware/filemanager/FileInfoActivity;->getTextWidth(Ljava/lang/String;)I

    move-result v1

    .line 1193
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortDocPage:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setWidth(I)V

    .line 1194
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandDocPage:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setWidth(I)V

    .line 1197
    iget v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nDocWord:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/infraware/filemanager/FileInfoActivity;->getTextWidth(Ljava/lang/String;)I

    move-result v1

    .line 1198
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortDocWords:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setWidth(I)V

    .line 1199
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandDocWords:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setWidth(I)V

    .line 1202
    invoke-direct {p0}, Lcom/infraware/filemanager/FileInfoActivity;->isWordsNumSupport()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1204
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutLandWords:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1205
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutPortWords:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1213
    :cond_9
    :goto_3
    return-void

    .line 1025
    :sswitch_0
    const v0, 0x7f02005e

    goto/16 :goto_0

    .line 1105
    :cond_a
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortModified:Landroid/widget/TextView;

    iget-wide v3, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nLastModified:J

    invoke-static {p0, v3, v4}, Lcom/infraware/common/util/FileUtils;->getDateString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1106
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandModified:Landroid/widget/TextView;

    iget-wide v3, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nLastModified:J

    invoke-static {p0, v3, v4}, Lcom/infraware/common/util/FileUtils;->getDateString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1108
    iget-wide v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nLastModified:J

    invoke-static {p0, v2, v3}, Lcom/infraware/common/util/FileUtils;->getDateString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/infraware/filemanager/FileInfoActivity;->getTextWidth(Ljava/lang/String;)I

    move-result v1

    .line 1109
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortModified:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setWidth(I)V

    .line 1110
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandModified:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setWidth(I)V

    goto/16 :goto_1

    .line 1130
    :cond_b
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutPortAttributes:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1131
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutLandAttributes:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_2

    .line 1209
    :cond_c
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutLandWords:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1210
    iget-object v2, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_layoutPortWords:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_3

    .line 995
    :sswitch_data_0
    .sparse-switch
        0x7f070020 -> :sswitch_0
        0x7f070021 -> :sswitch_0
        0x7f070023 -> :sswitch_0
        0x7f07028d -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public isSdCardAction(Ljava/lang/String;)V
    .locals 3
    .param p1, "nowAction"    # Ljava/lang/String;

    .prologue
    .line 1287
    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oFileItem:Ljava/lang/Object;

    check-cast v1, Lcom/infraware/filemanager/file/FileListItem;

    .line 1288
    .local v1, "item":Lcom/infraware/filemanager/file/FileListItem;
    new-instance v0, Ljava/io/File;

    invoke-virtual {v1}, Lcom/infraware/filemanager/file/FileListItem;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1290
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1291
    invoke-virtual {p0}, Lcom/infraware/filemanager/FileInfoActivity;->finish()V

    .line 1292
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "_newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 331
    iget v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nOrientation:I

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v1, v2, :cond_0

    .line 332
    invoke-direct {p0}, Lcom/infraware/filemanager/FileInfoActivity;->onOrientationChanged()V

    .line 334
    :cond_0
    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v1}, Lcom/infraware/common/util/Utils;->getLocaleType(Ljava/util/Locale;)I

    move-result v0

    .line 335
    .local v0, "nLocale":I
    iget v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nLocaleType:I

    if-eq v0, v1, :cond_1

    .line 337
    invoke-direct {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->onLocaleChanged(I)V

    .line 338
    invoke-direct {p0}, Lcom/infraware/filemanager/FileInfoActivity;->updateInfo()V

    .line 341
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 342
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 253
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 254
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    invoke-static {p0}, Lcom/infraware/common/util/Utils;->fullScreenActivity(Landroid/app/Activity;)V

    .line 260
    :cond_0
    invoke-static {}, Lcom/infraware/common/util/Utils;->isL()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 261
    const v0, 0x103012c

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->setTheme(I)V

    .line 263
    :cond_1
    const v0, 0x7f030018

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->setContentView(I)V

    .line 266
    new-instance v0, Lcom/infraware/filemanager/FileInfoActivity$CloseActionReceiver;

    invoke-direct {v0, p0}, Lcom/infraware/filemanager/FileInfoActivity$CloseActionReceiver;-><init>(Lcom/infraware/filemanager/FileInfoActivity;)V

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oCloseReceiver:Lcom/infraware/filemanager/FileInfoActivity$CloseActionReceiver;

    .line 267
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    .line 268
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    const-string/jumbo v1, "com.infraware.polarisviewer5.CLOSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 269
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oCloseReceiver:Lcom/infraware/filemanager/FileInfoActivity$CloseActionReceiver;

    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/infraware/filemanager/FileInfoActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 271
    invoke-direct {p0}, Lcom/infraware/filemanager/FileInfoActivity;->initFromExtra()V

    .line 273
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oFileItem:Ljava/lang/Object;

    if-nez v0, :cond_2

    .line 275
    invoke-virtual {p0}, Lcom/infraware/filemanager/FileInfoActivity;->finish()V

    .line 292
    :goto_0
    return-void

    .line 279
    :cond_2
    new-instance v0, Lcom/infraware/common/event/SdCardEvent;

    invoke-direct {v0}, Lcom/infraware/common/event/SdCardEvent;-><init>()V

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    .line 280
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    invoke-virtual {v0, p0}, Lcom/infraware/common/event/SdCardEvent;->setSdListener(Lcom/infraware/common/event/SdCardListener;)V

    .line 281
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    invoke-direct {p0}, Lcom/infraware/filemanager/FileInfoActivity;->getSdcardIntentFilter()Landroid/content/IntentFilter;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/infraware/filemanager/FileInfoActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 283
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oFileItem:Ljava/lang/Object;

    check-cast v0, Lcom/infraware/filemanager/file/FileListItem;

    invoke-direct {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->setLocalInfo(Lcom/infraware/filemanager/file/FileListItem;)V

    .line 285
    invoke-direct {p0}, Lcom/infraware/filemanager/FileInfoActivity;->initView()V

    .line 287
    invoke-virtual {p0}, Lcom/infraware/filemanager/FileInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/infraware/common/util/Utils;->getCurrentLocaleType(Landroid/content/res/Resources;)I

    move-result v0

    iput v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nLocaleType:I

    .line 288
    invoke-direct {p0}, Lcom/infraware/filemanager/FileInfoActivity;->onOrientationChanged()V

    .line 290
    invoke-direct {p0}, Lcom/infraware/filemanager/FileInfoActivity;->updateInfo()V

    .line 291
    invoke-direct {p0}, Lcom/infraware/filemanager/FileInfoActivity;->setTextTitleSize()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 316
    :cond_0
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oCloseReceiver:Lcom/infraware/filemanager/FileInfoActivity$CloseActionReceiver;

    if-eqz v0, :cond_1

    .line 317
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_oCloseReceiver:Lcom/infraware/filemanager/FileInfoActivity$CloseActionReceiver;

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/FileInfoActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 319
    :cond_1
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 320
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 322
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_bitmapThumb:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 323
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_bitmapThumb:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 324
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_bitmapThumb:Landroid/graphics/Bitmap;

    .line 326
    :cond_2
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    const v1, 0x7f070284

    .line 296
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 298
    iget-boolean v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->isNewDoc:Z

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortModified:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 301
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandModified:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 308
    :goto_0
    return-void

    .line 305
    :cond_0
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvPortModified:Landroid/widget/TextView;

    iget-wide v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nLastModified:J

    invoke-static {p0, v1, v2}, Lcom/infraware/common/util/FileUtils;->getDateString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 306
    iget-object v0, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_tvLandModified:Landroid/widget/TextView;

    iget-wide v1, p0, Lcom/infraware/filemanager/FileInfoActivity;->m_nLastModified:J

    invoke-static {p0, v1, v2}, Lcom/infraware/common/util/FileUtils;->getDateString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
