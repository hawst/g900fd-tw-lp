.class public Lcom/infraware/filemanager/database/recent/RecentFileManager;
.super Ljava/lang/Object;
.source "RecentFileManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;
    }
.end annotation


# static fields
.field private static final DUPLICATE_CHECK_ERROR:I = -0x2

.field private static final DUPLICATE_CHECK_NOTDUP:I = -0x1

.field private static final LIVE_FOLDER_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final MAX_KEEPING_FILE_COUNT:I

.field private static final MAX_PROVIDING_FILE_COUNT:I

.field private static final RFM_LOGTAG:Ljava/lang/String; = "RecentFileMgr"

.field private static volatile mRecentFileManager:Lcom/infraware/filemanager/database/recent/RecentFileManager;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 81
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->MAX_RECENT_COUNT()I

    move-result v0

    sput v0, Lcom/infraware/filemanager/database/recent/RecentFileManager;->MAX_PROVIDING_FILE_COUNT:I

    .line 82
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->MAX_RECENT_COUNT()I

    move-result v0

    sput v0, Lcom/infraware/filemanager/database/recent/RecentFileManager;->MAX_KEEPING_FILE_COUNT:I

    .line 84
    const/4 v0, 0x0

    sput-object v0, Lcom/infraware/filemanager/database/recent/RecentFileManager;->mRecentFileManager:Lcom/infraware/filemanager/database/recent/RecentFileManager;

    .line 88
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/infraware/filemanager/database/recent/RecentFileManager;->LIVE_FOLDER_PROJECTION_MAP:Ljava/util/HashMap;

    .line 89
    sget-object v0, Lcom/infraware/filemanager/database/recent/RecentFileManager;->LIVE_FOLDER_PROJECTION_MAP:Ljava/util/HashMap;

    const-string/jumbo v1, "_id"

    const-string/jumbo v2, "_id AS _id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/infraware/filemanager/database/recent/RecentFileManager;->LIVE_FOLDER_PROJECTION_MAP:Ljava/util/HashMap;

    const-string/jumbo v1, "name"

    const-string/jumbo v2, "filename AS name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/infraware/filemanager/database/recent/RecentFileManager;->LIVE_FOLDER_PROJECTION_MAP:Ljava/util/HashMap;

    const-string/jumbo v1, "description"

    const-string/jumbo v2, "last_modified AS description"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    return-void
.end method

.method private checkDuplicateFile(Landroid/content/Context;Ljava/io/File;)I
    .locals 8
    .param p1, "aCtx"    # Landroid/content/Context;
    .param p2, "aTargetFile"    # Ljava/io/File;

    .prologue
    .line 612
    new-instance v5, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;

    invoke-direct {v5, p0, p1}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;-><init>(Lcom/infraware/filemanager/database/recent/RecentFileManager;Landroid/content/Context;)V

    .line 613
    .local v5, "recentDBHelper":Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;
    const/4 v4, 0x0

    .line 614
    .local v4, "recentDB":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, 0x0

    .line 617
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {v5}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 619
    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 620
    .local v3, "path":Ljava/lang/String;
    const/4 v6, 0x0

    const-string/jumbo v7, "/"

    invoke-virtual {v3, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v3, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 621
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "SELECT * FROM RecentFiles WHERE path=\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "   AND "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "filename"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "=\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 626
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-lez v6, :cond_2

    .line 627
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 628
    const/4 v6, 0x0

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 640
    .local v2, "pKey":I
    if-eqz v0, :cond_0

    .line 641
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v6

    if-nez v6, :cond_0

    .line 642
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 645
    :cond_0
    if-eqz v5, :cond_1

    .line 646
    invoke-virtual {v5}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    .end local v2    # "pKey":I
    .end local v3    # "path":Ljava/lang/String;
    :cond_1
    :goto_0
    return v2

    .line 632
    .restart local v3    # "path":Ljava/lang/String;
    :cond_2
    const/4 v2, -0x1

    .line 640
    if-eqz v0, :cond_3

    .line 641
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v6

    if-nez v6, :cond_3

    .line 642
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 645
    :cond_3
    if-eqz v5, :cond_1

    .line 646
    invoke-virtual {v5}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    goto :goto_0

    .line 635
    .end local v3    # "path":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 636
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 637
    const/4 v2, -0x2

    .line 640
    if-eqz v0, :cond_4

    .line 641
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v6

    if-nez v6, :cond_4

    .line 642
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 645
    :cond_4
    if-eqz v5, :cond_1

    .line 646
    invoke-virtual {v5}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    goto :goto_0

    .line 640
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    if-eqz v0, :cond_5

    .line 641
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v7

    if-nez v7, :cond_5

    .line 642
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 645
    :cond_5
    if-eqz v5, :cond_6

    .line 646
    invoke-virtual {v5}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    :cond_6
    throw v6
.end method

.method private checkDuplicateFile(Landroid/content/Context;Ljava/lang/String;)I
    .locals 10
    .param p1, "aCtx"    # Landroid/content/Context;
    .param p2, "aTargetFileFullPath"    # Ljava/lang/String;

    .prologue
    .line 652
    new-instance v6, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;

    invoke-direct {v6, p0, p1}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;-><init>(Lcom/infraware/filemanager/database/recent/RecentFileManager;Landroid/content/Context;)V

    .line 653
    .local v6, "recentDBHelper":Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;
    const/4 v5, 0x0

    .line 654
    .local v5, "recentDB":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, 0x0

    .line 657
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {v6}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 659
    const-string/jumbo v8, "/"

    invoke-virtual {p2, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    .line 661
    .local v7, "slashIdx":I
    const/4 v8, 0x0

    invoke-virtual {p2, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 662
    .local v4, "path":Ljava/lang/String;
    add-int/lit8 v8, v7, 0x1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {p2, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 663
    .local v2, "name":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "SELECT * FROM RecentFiles WHERE path=\""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "\""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "   AND "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "filename"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "=\""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "\""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v5, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 668
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v8

    if-lez v8, :cond_2

    .line 669
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 670
    const/4 v8, 0x0

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    .line 682
    .local v3, "pKey":I
    if-eqz v0, :cond_0

    .line 683
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v8

    if-nez v8, :cond_0

    .line 684
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 687
    :cond_0
    if-eqz v6, :cond_1

    .line 688
    invoke-virtual {v6}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "pKey":I
    .end local v4    # "path":Ljava/lang/String;
    .end local v7    # "slashIdx":I
    :cond_1
    :goto_0
    return v3

    .line 674
    .restart local v2    # "name":Ljava/lang/String;
    .restart local v4    # "path":Ljava/lang/String;
    .restart local v7    # "slashIdx":I
    :cond_2
    const/4 v3, -0x1

    .line 682
    if-eqz v0, :cond_3

    .line 683
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v8

    if-nez v8, :cond_3

    .line 684
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 687
    :cond_3
    if-eqz v6, :cond_1

    .line 688
    invoke-virtual {v6}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    goto :goto_0

    .line 677
    .end local v2    # "name":Ljava/lang/String;
    .end local v4    # "path":Ljava/lang/String;
    .end local v7    # "slashIdx":I
    :catch_0
    move-exception v1

    .line 678
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 679
    const/4 v3, -0x2

    .line 682
    if-eqz v0, :cond_4

    .line 683
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v8

    if-nez v8, :cond_4

    .line 684
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 687
    :cond_4
    if-eqz v6, :cond_1

    .line 688
    invoke-virtual {v6}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    goto :goto_0

    .line 682
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v8

    if-eqz v0, :cond_5

    .line 683
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v9

    if-nez v9, :cond_5

    .line 684
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 687
    :cond_5
    if-eqz v6, :cond_6

    .line 688
    invoke-virtual {v6}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    :cond_6
    throw v8
.end method

.method public static getInstance()Lcom/infraware/filemanager/database/recent/RecentFileManager;
    .locals 2

    .prologue
    .line 99
    sget-object v0, Lcom/infraware/filemanager/database/recent/RecentFileManager;->mRecentFileManager:Lcom/infraware/filemanager/database/recent/RecentFileManager;

    if-nez v0, :cond_1

    .line 100
    const-class v1, Lcom/infraware/filemanager/database/recent/RecentFileManager;

    monitor-enter v1

    .line 101
    :try_start_0
    sget-object v0, Lcom/infraware/filemanager/database/recent/RecentFileManager;->mRecentFileManager:Lcom/infraware/filemanager/database/recent/RecentFileManager;

    if-nez v0, :cond_0

    .line 102
    new-instance v0, Lcom/infraware/filemanager/database/recent/RecentFileManager;

    invoke-direct {v0}, Lcom/infraware/filemanager/database/recent/RecentFileManager;-><init>()V

    sput-object v0, Lcom/infraware/filemanager/database/recent/RecentFileManager;->mRecentFileManager:Lcom/infraware/filemanager/database/recent/RecentFileManager;

    .line 104
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    :cond_1
    sget-object v0, Lcom/infraware/filemanager/database/recent/RecentFileManager;->mRecentFileManager:Lcom/infraware/filemanager/database/recent/RecentFileManager;

    return-object v0

    .line 104
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private insertFile(Landroid/content/Context;Ljava/io/File;)V
    .locals 22
    .param p1, "aCtx"    # Landroid/content/Context;
    .param p2, "targetFile"    # Ljava/io/File;

    .prologue
    .line 446
    new-instance v15, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v15, v0, v1}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;-><init>(Lcom/infraware/filemanager/database/recent/RecentFileManager;Landroid/content/Context;)V

    .line 447
    .local v15, "recentDBHelper":Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;
    const/4 v14, 0x0

    .line 449
    .local v14, "recentDB":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-virtual {v15}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v14

    .line 451
    const/4 v4, 0x0

    .line 452
    .local v4, "drmType":I
    const-wide/16 v16, 0x0

    .local v16, "size":J
    const-wide/16 v10, 0x0

    .local v10, "lastModifiedDate":J
    const-wide/16 v8, 0x0

    .line 453
    .local v8, "lastAccessedDate":J
    const/4 v7, 0x0

    .local v7, "filename":Ljava/lang/String;
    const/4 v13, 0x0

    .line 455
    .local v13, "path":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    .line 456
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v13

    .line 457
    const/16 v20, 0x0

    const-string/jumbo v21, "/"

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 458
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->length()J

    move-result-wide v16

    .line 459
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->lastModified()J

    move-result-wide v10

    .line 460
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 462
    const-string/jumbo v19, ""

    .line 463
    .local v19, "userId":Ljava/lang/String;
    const-string/jumbo v18, ""

    .line 464
    .local v18, "storageName":Ljava/lang/String;
    const-string/jumbo v6, ""

    .line 465
    .local v6, "fileId":Ljava/lang/String;
    const-string/jumbo v12, ""

    .line 466
    .local v12, "parentFileId":Ljava/lang/String;
    const-string/jumbo v3, ""

    .line 468
    .local v3, "contentsrc":Ljava/lang/String;
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v21, "INSERT INTO RecentFiles VALUES ( null, \""

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string/jumbo v21, "\", "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string/jumbo v21, "\""

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string/jumbo v21, "\", "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string/jumbo v21, "\""

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string/jumbo v21, "\", "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string/jumbo v21, "\""

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string/jumbo v21, "\", "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string/jumbo v21, "\""

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string/jumbo v21, "\", "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string/jumbo v21, "\""

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string/jumbo v21, "\", "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string/jumbo v21, "\""

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string/jumbo v21, "\", "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string/jumbo v21, ", "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string/jumbo v21, ", "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string/jumbo v21, ", "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string/jumbo v21, ");"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 488
    if-eqz v15, :cond_0

    .line 489
    invoke-virtual {v15}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    .line 492
    .end local v3    # "contentsrc":Ljava/lang/String;
    .end local v4    # "drmType":I
    .end local v6    # "fileId":Ljava/lang/String;
    .end local v7    # "filename":Ljava/lang/String;
    .end local v8    # "lastAccessedDate":J
    .end local v10    # "lastModifiedDate":J
    .end local v12    # "parentFileId":Ljava/lang/String;
    .end local v13    # "path":Ljava/lang/String;
    .end local v16    # "size":J
    .end local v18    # "storageName":Ljava/lang/String;
    .end local v19    # "userId":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 483
    :catch_0
    move-exception v5

    .line 484
    .local v5, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 488
    if-eqz v15, :cond_0

    .line 489
    invoke-virtual {v15}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    goto :goto_0

    .line 488
    .end local v5    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v20

    if-eqz v15, :cond_1

    .line 489
    invoke-virtual {v15}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    :cond_1
    throw v20
.end method

.method private insertFile(Landroid/content/Context;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 13
    .param p1, "aCtx"    # Landroid/content/Context;
    .param p2, "absolutePath"    # Ljava/lang/String;
    .param p3, "size"    # J
    .param p5, "updateTime"    # J
    .param p7, "accountId"    # Ljava/lang/String;
    .param p8, "storageName"    # Ljava/lang/String;
    .param p9, "fileId"    # Ljava/lang/String;
    .param p10, "parentFileId"    # Ljava/lang/String;
    .param p11, "contentsrc"    # Ljava/lang/String;

    .prologue
    .line 495
    new-instance v9, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;

    invoke-direct {v9, p0, p1}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;-><init>(Lcom/infraware/filemanager/database/recent/RecentFileManager;Landroid/content/Context;)V

    .line 496
    .local v9, "recentDBHelper":Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;
    const/4 v8, 0x0

    .line 498
    .local v8, "recentDB":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-virtual {v9}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    .line 500
    const/4 v2, 0x0

    .line 501
    .local v2, "drmType":I
    const-wide/16 v5, 0x0

    .line 503
    .local v5, "lastAccessedDate":J
    const-string/jumbo v11, "/"

    invoke-virtual {p2, v11}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v10

    .line 504
    .local v10, "slashIdx":I
    const-string/jumbo v7, ""

    .line 505
    .local v7, "path":Ljava/lang/String;
    if-gez v10, :cond_1

    .line 506
    const-string/jumbo v7, ""

    .line 511
    :goto_0
    add-int/lit8 v11, v10, 0x1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v12

    invoke-virtual {p2, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 513
    .local v4, "filename":Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 515
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "INSERT INTO RecentFiles VALUES ( null, \""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p7

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p8

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p9

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p10

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-wide/from16 v0, p3

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, ", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-wide/from16 v0, p5

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, ", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, ", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, ");"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 535
    if-eqz v9, :cond_0

    .line 536
    invoke-virtual {v9}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    .line 539
    .end local v2    # "drmType":I
    .end local v4    # "filename":Ljava/lang/String;
    .end local v5    # "lastAccessedDate":J
    .end local v7    # "path":Ljava/lang/String;
    .end local v10    # "slashIdx":I
    :cond_0
    :goto_1
    return-void

    .line 507
    .restart local v2    # "drmType":I
    .restart local v5    # "lastAccessedDate":J
    .restart local v7    # "path":Ljava/lang/String;
    .restart local v10    # "slashIdx":I
    :cond_1
    if-nez v10, :cond_2

    .line 508
    :try_start_1
    const-string/jumbo v7, "/"

    goto/16 :goto_0

    .line 510
    :cond_2
    const/4 v11, 0x0

    invoke-virtual {p2, v11, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v7

    goto/16 :goto_0

    .line 530
    .end local v2    # "drmType":I
    .end local v5    # "lastAccessedDate":J
    .end local v7    # "path":Ljava/lang/String;
    .end local v10    # "slashIdx":I
    :catch_0
    move-exception v3

    .line 531
    .local v3, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 535
    if-eqz v9, :cond_0

    .line 536
    invoke-virtual {v9}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    goto :goto_1

    .line 535
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v11

    if-eqz v9, :cond_3

    .line 536
    invoke-virtual {v9}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    :cond_3
    throw v11
.end method

.method private makeFileItem(ILjava/lang/String;Ljava/lang/String;JJJI)Lcom/infraware/filemanager/file/FileListItem;
    .locals 4
    .param p1, "aPKey"    # I
    .param p2, "aFilename"    # Ljava/lang/String;
    .param p3, "aPath"    # Ljava/lang/String;
    .param p4, "aSize"    # J
    .param p6, "aLastModifiedDate"    # J
    .param p8, "aLastAccessedDate"    # J
    .param p10, "aDrmType"    # I

    .prologue
    const/4 v3, 0x0

    .line 301
    new-instance v0, Lcom/infraware/filemanager/file/FileListItem;

    invoke-direct {v0}, Lcom/infraware/filemanager/file/FileListItem;-><init>()V

    .line 302
    .local v0, "item":Lcom/infraware/filemanager/file/FileListItem;
    iput p1, v0, Lcom/infraware/filemanager/file/FileListItem;->recentPkey:I

    .line 303
    const/4 v2, 0x1

    iput v2, v0, Lcom/infraware/filemanager/file/FileListItem;->type:I

    .line 304
    iput-boolean v3, v0, Lcom/infraware/filemanager/file/FileListItem;->isFolder:Z

    .line 305
    iput-object p3, v0, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    .line 306
    const-string/jumbo v2, "."

    invoke-virtual {p2, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 307
    .local v1, "prevExtIdx":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 308
    iput-object p2, v0, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    .line 309
    const/4 v2, 0x0

    iput-object v2, v0, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    .line 315
    :goto_0
    iput-wide p4, v0, Lcom/infraware/filemanager/file/FileListItem;->size:J

    .line 316
    iput-wide p6, v0, Lcom/infraware/filemanager/file/FileListItem;->updateTime:J

    .line 317
    iput-wide p8, v0, Lcom/infraware/filemanager/file/FileListItem;->accessTime:J

    .line 319
    return-object v0

    .line 312
    :cond_0
    invoke-virtual {p2, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    .line 313
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p2, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    goto :goto_0
.end method

.method private shrinkRecentDB(Landroid/content/Context;)V
    .locals 5
    .param p1, "aCtx"    # Landroid/content/Context;

    .prologue
    .line 233
    new-instance v2, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;

    invoke-direct {v2, p0, p1}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;-><init>(Lcom/infraware/filemanager/database/recent/RecentFileManager;Landroid/content/Context;)V

    .line 234
    .local v2, "recentDBHelper":Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;
    const/4 v1, 0x0

    .line 236
    .local v1, "recentDB":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-virtual {v2}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 237
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "DELETE FROM RecentFiles  WHERE _id NOT IN (SELECT _id   FROM RecentFiles   ORDER BY last_accessed DESC LIMIT "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/infraware/filemanager/database/recent/RecentFileManager;->MAX_KEEPING_FILE_COUNT:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249
    if-eqz v2, :cond_0

    .line 250
    invoke-virtual {v2}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    .line 253
    :cond_0
    :goto_0
    return-void

    .line 245
    :catch_0
    move-exception v0

    .line 246
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 249
    if-eqz v2, :cond_0

    .line 250
    invoke-virtual {v2}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    goto :goto_0

    .line 249
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_1

    .line 250
    invoke-virtual {v2}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    :cond_1
    throw v3
.end method

.method private updateFile(Landroid/content/Context;Ljava/io/File;I)V
    .locals 12
    .param p1, "aCtx"    # Landroid/content/Context;
    .param p2, "aTargetFile"    # Ljava/io/File;
    .param p3, "aTargetIdx"    # I

    .prologue
    .line 542
    new-instance v7, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;

    invoke-direct {v7, p0, p1}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;-><init>(Lcom/infraware/filemanager/database/recent/RecentFileManager;Landroid/content/Context;)V

    .line 543
    .local v7, "recentDBHelper":Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;
    const/4 v6, 0x0

    .line 545
    .local v6, "recentDB":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-virtual {v7}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    .line 548
    const/4 v0, 0x0

    .line 549
    .local v0, "drmType":I
    const-wide/16 v8, 0x0

    .local v8, "size":J
    const-wide/16 v4, 0x0

    .local v4, "lastModifiedDate":J
    const-wide/16 v2, 0x0

    .line 551
    .local v2, "lastAccessedDate":J
    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v8

    .line 552
    invoke-virtual {p2}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    .line 553
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 555
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "UPDATE RecentFiles   SET size="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "last_modified"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "last_accessed"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "drm_type"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " WHERE "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "_id"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 569
    if-eqz v7, :cond_0

    .line 570
    invoke-virtual {v7}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    .line 573
    .end local v0    # "drmType":I
    .end local v2    # "lastAccessedDate":J
    .end local v4    # "lastModifiedDate":J
    .end local v8    # "size":J
    :cond_0
    :goto_0
    return-void

    .line 564
    :catch_0
    move-exception v1

    .line 565
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 569
    if-eqz v7, :cond_0

    .line 570
    invoke-virtual {v7}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    goto :goto_0

    .line 569
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v10

    if-eqz v7, :cond_1

    .line 570
    invoke-virtual {v7}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    :cond_1
    throw v10
.end method

.method private updateFile(Landroid/content/Context;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;I)V
    .locals 9
    .param p1, "aCtx"    # Landroid/content/Context;
    .param p2, "absolutePath"    # Ljava/lang/String;
    .param p3, "size"    # J
    .param p5, "updateTime"    # J
    .param p7, "fileId"    # Ljava/lang/String;
    .param p8, "contentSrc"    # Ljava/lang/String;
    .param p9, "aTargetIdx"    # I

    .prologue
    .line 576
    new-instance v6, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;

    invoke-direct {v6, p0, p1}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;-><init>(Lcom/infraware/filemanager/database/recent/RecentFileManager;Landroid/content/Context;)V

    .line 577
    .local v6, "recentDBHelper":Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;
    const/4 v5, 0x0

    .line 579
    .local v5, "recentDB":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-virtual {v6}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 581
    const/4 v1, 0x0

    .line 582
    .local v1, "drmType":I
    const-wide/16 v3, 0x0

    .line 584
    .local v3, "lastAccessedDate":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 586
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "UPDATE RecentFiles   SET size="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "fileid"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "contentsrc"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p8

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "last_modified"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "last_accessed"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "drm_type"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " WHERE "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "_id"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, p9

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 602
    if-eqz v6, :cond_0

    .line 603
    invoke-virtual {v6}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    .line 606
    .end local v1    # "drmType":I
    .end local v3    # "lastAccessedDate":J
    :cond_0
    :goto_0
    return-void

    .line 597
    :catch_0
    move-exception v2

    .line 598
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 602
    if-eqz v6, :cond_0

    .line 603
    invoke-virtual {v6}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    goto :goto_0

    .line 602
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    if-eqz v6, :cond_1

    .line 603
    invoke-virtual {v6}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    :cond_1
    throw v7
.end method


# virtual methods
.method public InsertFileInfoToDB(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p1, "aCtx"    # Landroid/content/Context;
    .param p2, "fileAbsolutePath"    # Ljava/lang/String;

    .prologue
    .line 323
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 325
    .local v1, "targetFile":Ljava/io/File;
    invoke-direct {p0, p1, v1}, Lcom/infraware/filemanager/database/recent/RecentFileManager;->checkDuplicateFile(Landroid/content/Context;Ljava/io/File;)I

    move-result v0

    .line 326
    .local v0, "dupIdx":I
    const/4 v2, -0x2

    if-ne v0, v2, :cond_0

    .line 327
    const-string/jumbo v2, "RecentFileMgr"

    const-string/jumbo v3, "checkDuplicateFile error"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    :goto_0
    return-void

    .line 329
    :cond_0
    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    .line 330
    invoke-direct {p0, p1, v1}, Lcom/infraware/filemanager/database/recent/RecentFileManager;->insertFile(Landroid/content/Context;Ljava/io/File;)V

    goto :goto_0

    .line 333
    :cond_1
    invoke-direct {p0, p1, v1, v0}, Lcom/infraware/filemanager/database/recent/RecentFileManager;->updateFile(Landroid/content/Context;Ljava/io/File;I)V

    goto :goto_0
.end method

.method public deleteAll(Landroid/content/Context;)V
    .locals 4
    .param p1, "aCtx"    # Landroid/content/Context;

    .prologue
    .line 427
    new-instance v2, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;

    invoke-direct {v2, p0, p1}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;-><init>(Lcom/infraware/filemanager/database/recent/RecentFileManager;Landroid/content/Context;)V

    .line 428
    .local v2, "recentDBHelper":Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;
    const/4 v1, 0x0

    .line 431
    .local v1, "recentDB":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-virtual {v2}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 432
    const-string/jumbo v3, "DELETE FROM RecentFiles"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 439
    if-eqz v2, :cond_0

    .line 440
    invoke-virtual {v2}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    .line 443
    :cond_0
    :goto_0
    return-void

    .line 435
    :catch_0
    move-exception v0

    .line 436
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 439
    if-eqz v2, :cond_0

    .line 440
    invoke-virtual {v2}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    goto :goto_0

    .line 439
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_1

    .line 440
    invoke-virtual {v2}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    :cond_1
    throw v3
.end method

.method public deleteFile(Landroid/content/Context;Ljava/lang/String;)V
    .locals 6
    .param p1, "aCtx"    # Landroid/content/Context;
    .param p2, "aTargetFileFullPath"    # Ljava/lang/String;

    .prologue
    .line 400
    invoke-direct {p0, p1, p2}, Lcom/infraware/filemanager/database/recent/RecentFileManager;->checkDuplicateFile(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 402
    .local v0, "dupRet":I
    const/4 v4, -0x1

    if-eq v0, v4, :cond_0

    const/4 v4, -0x2

    if-ne v0, v4, :cond_1

    .line 424
    :cond_0
    :goto_0
    return-void

    .line 406
    :cond_1
    new-instance v3, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;

    invoke-direct {v3, p0, p1}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;-><init>(Lcom/infraware/filemanager/database/recent/RecentFileManager;Landroid/content/Context;)V

    .line 407
    .local v3, "recentDBHelper":Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;
    const/4 v2, 0x0

    .line 410
    .local v2, "recentDB":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-virtual {v3}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 411
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "DELETE FROM RecentFiles WHERE _id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 420
    if-eqz v3, :cond_0

    .line 421
    invoke-virtual {v3}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    goto :goto_0

    .line 416
    :catch_0
    move-exception v1

    .line 417
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 420
    if-eqz v3, :cond_0

    .line 421
    invoke-virtual {v3}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    goto :goto_0

    .line 420
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    if-eqz v3, :cond_2

    .line 421
    invoke-virtual {v3}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    :cond_2
    throw v4
.end method

.method public getRecentFile(Landroid/content/Context;I)Lcom/infraware/filemanager/file/FileListItem;
    .locals 20
    .param p1, "aCtx"    # Landroid/content/Context;
    .param p2, "aIdx"    # I

    .prologue
    .line 256
    new-instance v18, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;-><init>(Lcom/infraware/filemanager/database/recent/RecentFileManager;Landroid/content/Context;)V

    .line 257
    .local v18, "recentDBHelper":Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;
    const/16 v17, 0x0

    .line 258
    .local v17, "recentDB":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v14, 0x0

    .line 260
    .local v14, "cursor":Landroid/database/Cursor;
    const/16 v16, 0x0

    .line 263
    .local v16, "item":Lcom/infraware/filemanager/file/FileListItem;
    :try_start_0
    invoke-virtual/range {v18 .. v18}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v17

    .line 264
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "SELECT * FROM RecentFiles WHERE _id="

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/16 v19, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 268
    const/4 v4, 0x0

    .local v4, "pKey":I
    const/4 v13, 0x0

    .line 269
    .local v13, "drmType":I
    const-wide/16 v7, 0x0

    .local v7, "size":J
    const-wide/16 v9, 0x0

    .local v9, "lastModifiedDate":J
    const-wide/16 v11, 0x0

    .line 270
    .local v11, "lastAccessedDate":J
    const/4 v5, 0x0

    .local v5, "filename":Ljava/lang/String;
    const/4 v6, 0x0

    .line 272
    .local v6, "path":Ljava/lang/String;
    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_0

    .line 273
    const/4 v3, 0x0

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 274
    const/4 v3, 0x1

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 275
    const/4 v3, 0x2

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 276
    const/16 v3, 0x8

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 277
    const/16 v3, 0x9

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 278
    const/16 v3, 0xa

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    .line 279
    const/16 v3, 0xb

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    move-object/from16 v3, p0

    .line 280
    invoke-direct/range {v3 .. v13}, Lcom/infraware/filemanager/database/recent/RecentFileManager;->makeFileItem(ILjava/lang/String;Ljava/lang/String;JJJI)Lcom/infraware/filemanager/file/FileListItem;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v16

    .line 287
    :cond_0
    if-eqz v14, :cond_1

    .line 288
    invoke-interface {v14}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_1

    .line 289
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 292
    :cond_1
    if-eqz v18, :cond_2

    .line 293
    invoke-virtual/range {v18 .. v18}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    .line 297
    .end local v4    # "pKey":I
    .end local v5    # "filename":Ljava/lang/String;
    .end local v6    # "path":Ljava/lang/String;
    .end local v7    # "size":J
    .end local v9    # "lastModifiedDate":J
    .end local v11    # "lastAccessedDate":J
    .end local v13    # "drmType":I
    :cond_2
    :goto_0
    return-object v16

    .line 283
    :catch_0
    move-exception v15

    .line 284
    .local v15, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v15}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 287
    if-eqz v14, :cond_3

    .line 288
    invoke-interface {v14}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_3

    .line 289
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 292
    :cond_3
    if-eqz v18, :cond_2

    .line 293
    invoke-virtual/range {v18 .. v18}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    goto :goto_0

    .line 287
    .end local v15    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v14, :cond_4

    .line 288
    invoke-interface {v14}, Landroid/database/Cursor;->isClosed()Z

    move-result v19

    if-nez v19, :cond_4

    .line 289
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 292
    :cond_4
    if-eqz v18, :cond_5

    .line 293
    invoke-virtual/range {v18 .. v18}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    :cond_5
    throw v3
.end method

.method public getRecentFiles(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 27
    .param p1, "aCtx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/filemanager/file/FileListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 161
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 163
    .local v24, "recentFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/infraware/filemanager/file/FileListItem;>;"
    new-instance v23, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;-><init>(Lcom/infraware/filemanager/database/recent/RecentFileManager;Landroid/content/Context;)V

    .line 164
    .local v23, "recentDBHelper":Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;
    const/16 v22, 0x0

    .line 165
    .local v22, "recentDB":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v16, 0x0

    .line 168
    .local v16, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-direct/range {p0 .. p1}, Lcom/infraware/filemanager/database/recent/RecentFileManager;->shrinkRecentDB(Landroid/content/Context;)V

    .line 170
    invoke-virtual/range {v23 .. v23}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v22

    .line 171
    const-string/jumbo v3, "SELECT *   FROM RecentFiles  ORDER BY last_accessed DESC"

    const/16 v26, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 176
    const/4 v4, 0x0

    .local v4, "pKey":I
    const/4 v13, 0x0

    .line 177
    .local v13, "drmType":I
    const-wide/16 v7, 0x0

    .local v7, "size":J
    const-wide/16 v9, 0x0

    .local v9, "lastModifiedDate":J
    const-wide/16 v11, 0x0

    .line 178
    .local v11, "lastAccessedDate":J
    const/4 v5, 0x0

    .local v5, "filename":Ljava/lang/String;
    const/4 v6, 0x0

    .local v6, "path":Ljava/lang/String;
    const/4 v14, 0x0

    .local v14, "accountId":Ljava/lang/String;
    const/16 v25, 0x0

    .local v25, "storageName":Ljava/lang/String;
    const/16 v18, 0x0

    .local v18, "fileId":Ljava/lang/String;
    const/16 v21, 0x0

    .local v21, "parentFileId":Ljava/lang/String;
    const/4 v15, 0x0

    .line 179
    .local v15, "contentsrc":Ljava/lang/String;
    const/16 v19, 0x0

    .line 181
    .local v19, "fileItem":Lcom/infraware/filemanager/file/FileListItem;
    const/16 v20, 0x0

    .line 182
    .local v20, "itemCount":I
    :cond_0
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 183
    const/4 v3, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 184
    const/4 v3, 0x1

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 185
    const/4 v3, 0x2

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 186
    const/4 v3, 0x3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 187
    const/4 v3, 0x4

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 188
    const/4 v3, 0x5

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 189
    const/4 v3, 0x6

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 190
    const/4 v3, 0x7

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 191
    const/16 v3, 0x8

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 192
    const/16 v3, 0x9

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 193
    const/16 v3, 0xa

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    .line 194
    const/16 v3, 0xb

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    move-object/from16 v3, p0

    .line 196
    invoke-direct/range {v3 .. v13}, Lcom/infraware/filemanager/database/recent/RecentFileManager;->makeFileItem(ILjava/lang/String;Ljava/lang/String;JJJI)Lcom/infraware/filemanager/file/FileListItem;

    move-result-object v19

    .line 198
    if-eqz v14, :cond_5

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_5

    .line 200
    add-int/lit8 v20, v20, 0x1

    .line 201
    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 211
    :cond_1
    :goto_0
    sget v3, Lcom/infraware/filemanager/database/recent/RecentFileManager;->MAX_PROVIDING_FILE_COUNT:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move/from16 v0, v20

    if-ne v0, v3, :cond_0

    .line 219
    :cond_2
    if-eqz v16, :cond_3

    .line 220
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_3

    .line 221
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 224
    :cond_3
    if-eqz v23, :cond_4

    .line 225
    invoke-virtual/range {v23 .. v23}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    .line 229
    .end local v4    # "pKey":I
    .end local v5    # "filename":Ljava/lang/String;
    .end local v6    # "path":Ljava/lang/String;
    .end local v7    # "size":J
    .end local v9    # "lastModifiedDate":J
    .end local v11    # "lastAccessedDate":J
    .end local v13    # "drmType":I
    .end local v14    # "accountId":Ljava/lang/String;
    .end local v15    # "contentsrc":Ljava/lang/String;
    .end local v18    # "fileId":Ljava/lang/String;
    .end local v19    # "fileItem":Lcom/infraware/filemanager/file/FileListItem;
    .end local v20    # "itemCount":I
    .end local v21    # "parentFileId":Ljava/lang/String;
    .end local v25    # "storageName":Ljava/lang/String;
    :cond_4
    :goto_1
    return-object v24

    .line 205
    .restart local v4    # "pKey":I
    .restart local v5    # "filename":Ljava/lang/String;
    .restart local v6    # "path":Ljava/lang/String;
    .restart local v7    # "size":J
    .restart local v9    # "lastModifiedDate":J
    .restart local v11    # "lastAccessedDate":J
    .restart local v13    # "drmType":I
    .restart local v14    # "accountId":Ljava/lang/String;
    .restart local v15    # "contentsrc":Ljava/lang/String;
    .restart local v18    # "fileId":Ljava/lang/String;
    .restart local v19    # "fileItem":Lcom/infraware/filemanager/file/FileListItem;
    .restart local v20    # "itemCount":I
    .restart local v21    # "parentFileId":Ljava/lang/String;
    .restart local v25    # "storageName":Ljava/lang/String;
    :cond_5
    :try_start_1
    new-instance v3, Ljava/io/File;

    invoke-virtual/range {v19 .. v19}, Lcom/infraware/filemanager/file/FileListItem;->getAbsolutePath()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 206
    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 207
    add-int/lit8 v20, v20, 0x1

    goto :goto_0

    .line 215
    .end local v4    # "pKey":I
    .end local v5    # "filename":Ljava/lang/String;
    .end local v6    # "path":Ljava/lang/String;
    .end local v7    # "size":J
    .end local v9    # "lastModifiedDate":J
    .end local v11    # "lastAccessedDate":J
    .end local v13    # "drmType":I
    .end local v14    # "accountId":Ljava/lang/String;
    .end local v15    # "contentsrc":Ljava/lang/String;
    .end local v18    # "fileId":Ljava/lang/String;
    .end local v19    # "fileItem":Lcom/infraware/filemanager/file/FileListItem;
    .end local v20    # "itemCount":I
    .end local v21    # "parentFileId":Ljava/lang/String;
    .end local v25    # "storageName":Ljava/lang/String;
    :catch_0
    move-exception v17

    .line 216
    .local v17, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 219
    if-eqz v16, :cond_6

    .line 220
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_6

    .line 221
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 224
    :cond_6
    if-eqz v23, :cond_4

    .line 225
    invoke-virtual/range {v23 .. v23}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    goto :goto_1

    .line 219
    .end local v17    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v16, :cond_7

    .line 220
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->isClosed()Z

    move-result v26

    if-nez v26, :cond_7

    .line 221
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 224
    :cond_7
    if-eqz v23, :cond_8

    .line 225
    invoke-virtual/range {v23 .. v23}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    :cond_8
    throw v3
.end method

.method public isEmpty(Landroid/content/Context;)Z
    .locals 9
    .param p1, "aCtx"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 111
    new-instance v4, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;

    invoke-direct {v4, p0, p1}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;-><init>(Lcom/infraware/filemanager/database/recent/RecentFileManager;Landroid/content/Context;)V

    .line 112
    .local v4, "recentDBHelper":Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;
    const/4 v3, 0x0

    .line 113
    .local v3, "recentDB":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, 0x0

    .line 116
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {v4}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 117
    const-string/jumbo v7, "SELECT *   FROM RecentFiles  ORDER BY last_accessed DESC"

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 122
    new-instance v2, Lcom/infraware/filemanager/file/FileListItem;

    invoke-direct {v2}, Lcom/infraware/filemanager/file/FileListItem;-><init>()V

    .line 123
    .local v2, "fileItem":Lcom/infraware/filemanager/file/FileListItem;
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 125
    new-instance v2, Lcom/infraware/filemanager/file/FileListItem;

    .end local v2    # "fileItem":Lcom/infraware/filemanager/file/FileListItem;
    invoke-direct {v2}, Lcom/infraware/filemanager/file/FileListItem;-><init>()V

    .line 126
    .restart local v2    # "fileItem":Lcom/infraware/filemanager/file/FileListItem;
    const/4 v7, 0x2

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    .line 127
    const/4 v7, 0x1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    .line 128
    const/4 v7, 0x3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, Lcom/infraware/filemanager/file/FileListItem;->accountId:Ljava/lang/String;

    .line 130
    iget-object v7, v2, Lcom/infraware/filemanager/file/FileListItem;->accountId:Ljava/lang/String;

    if-eqz v7, :cond_3

    iget-object v7, v2, Lcom/infraware/filemanager/file/FileListItem;->accountId:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    if-eqz v7, :cond_3

    .line 146
    if-eqz v0, :cond_1

    .line 147
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v6

    if-nez v6, :cond_1

    .line 148
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 151
    :cond_1
    if-eqz v4, :cond_2

    .line 152
    invoke-virtual {v4}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    .line 156
    .end local v2    # "fileItem":Lcom/infraware/filemanager/file/FileListItem;
    :cond_2
    :goto_0
    return v5

    .line 136
    .restart local v2    # "fileItem":Lcom/infraware/filemanager/file/FileListItem;
    :cond_3
    :try_start_1
    new-instance v7, Ljava/io/File;

    invoke-virtual {v2}, Lcom/infraware/filemanager/file/FileListItem;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->exists()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v7

    if-eqz v7, :cond_0

    .line 146
    if-eqz v0, :cond_4

    .line 147
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v6

    if-nez v6, :cond_4

    .line 148
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 151
    :cond_4
    if-eqz v4, :cond_2

    .line 152
    invoke-virtual {v4}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    goto :goto_0

    .line 146
    :cond_5
    if-eqz v0, :cond_6

    .line 147
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_6

    .line 148
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 151
    :cond_6
    if-eqz v4, :cond_7

    .line 152
    invoke-virtual {v4}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    :cond_7
    move v5, v6

    .line 156
    goto :goto_0

    .line 142
    .end local v2    # "fileItem":Lcom/infraware/filemanager/file/FileListItem;
    :catch_0
    move-exception v1

    .line 146
    .local v1, "e":Ljava/lang/Exception;
    if-eqz v0, :cond_8

    .line 147
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_8

    .line 148
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 151
    :cond_8
    if-eqz v4, :cond_9

    .line 152
    invoke-virtual {v4}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    :cond_9
    move v5, v6

    goto :goto_0

    .line 146
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    if-eqz v0, :cond_a

    .line 147
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v6

    if-nez v6, :cond_a

    .line 148
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 151
    :cond_a
    if-eqz v4, :cond_b

    .line 152
    invoke-virtual {v4}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    :cond_b
    throw v5
.end method

.method public updateDB(Landroid/content/Context;)V
    .locals 21
    .param p1, "aCtx"    # Landroid/content/Context;

    .prologue
    .line 338
    new-instance v19, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;-><init>(Lcom/infraware/filemanager/database/recent/RecentFileManager;Landroid/content/Context;)V

    .line 339
    .local v19, "recentDBHelper":Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;
    const/16 v18, 0x0

    .line 340
    .local v18, "recentDB":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v14, 0x0

    .line 343
    .local v14, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-direct/range {p0 .. p1}, Lcom/infraware/filemanager/database/recent/RecentFileManager;->shrinkRecentDB(Landroid/content/Context;)V

    .line 345
    invoke-virtual/range {v19 .. v19}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v18

    .line 346
    const-string/jumbo v3, "SELECT *   FROM RecentFiles  ORDER BY last_accessed DESC"

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 351
    const/4 v4, 0x0

    .local v4, "pKey":I
    const/4 v13, 0x0

    .line 352
    .local v13, "drmType":I
    const-wide/16 v7, 0x0

    .local v7, "size":J
    const-wide/16 v9, 0x0

    .local v9, "lastModifiedDate":J
    const-wide/16 v11, 0x0

    .line 353
    .local v11, "lastAccessedDate":J
    const/4 v5, 0x0

    .local v5, "filename":Ljava/lang/String;
    const/4 v6, 0x0

    .line 354
    .local v6, "path":Ljava/lang/String;
    const/16 v16, 0x0

    .line 356
    .local v16, "fileItem":Lcom/infraware/filemanager/file/FileListItem;
    const/16 v17, 0x0

    .line 357
    .local v17, "itemCount":I
    :cond_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 359
    const/4 v3, 0x0

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 360
    const/4 v3, 0x1

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 361
    const/4 v3, 0x2

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 362
    const/16 v3, 0x8

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 363
    const/16 v3, 0x9

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 364
    const/16 v3, 0xa

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    .line 365
    const/16 v3, 0xb

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    move-object/from16 v3, p0

    .line 367
    invoke-direct/range {v3 .. v13}, Lcom/infraware/filemanager/database/recent/RecentFileManager;->makeFileItem(ILjava/lang/String;Ljava/lang/String;JJJI)Lcom/infraware/filemanager/file/FileListItem;

    move-result-object v16

    .line 369
    new-instance v3, Ljava/io/File;

    invoke-virtual/range {v16 .. v16}, Lcom/infraware/filemanager/file/FileListItem;->getAbsolutePath()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 370
    add-int/lit8 v17, v17, 0x1

    .line 380
    :cond_1
    :goto_0
    sget v3, Lcom/infraware/filemanager/database/recent/RecentFileManager;->MAX_PROVIDING_FILE_COUNT:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move/from16 v0, v17

    if-ne v0, v3, :cond_0

    .line 388
    :cond_2
    if-eqz v14, :cond_3

    .line 389
    invoke-interface {v14}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_3

    .line 390
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 393
    :cond_3
    if-eqz v19, :cond_4

    .line 394
    invoke-virtual/range {v19 .. v19}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    .line 397
    .end local v4    # "pKey":I
    .end local v5    # "filename":Ljava/lang/String;
    .end local v6    # "path":Ljava/lang/String;
    .end local v7    # "size":J
    .end local v9    # "lastModifiedDate":J
    .end local v11    # "lastAccessedDate":J
    .end local v13    # "drmType":I
    .end local v16    # "fileItem":Lcom/infraware/filemanager/file/FileListItem;
    .end local v17    # "itemCount":I
    :cond_4
    :goto_1
    return-void

    .line 373
    .restart local v4    # "pKey":I
    .restart local v5    # "filename":Ljava/lang/String;
    .restart local v6    # "path":Ljava/lang/String;
    .restart local v7    # "size":J
    .restart local v9    # "lastModifiedDate":J
    .restart local v11    # "lastAccessedDate":J
    .restart local v13    # "drmType":I
    .restart local v16    # "fileItem":Lcom/infraware/filemanager/file/FileListItem;
    .restart local v17    # "itemCount":I
    :cond_5
    :try_start_1
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v20, "mounted"

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 375
    invoke-virtual/range {v16 .. v16}, Lcom/infraware/filemanager/file/FileListItem;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v3}, Lcom/infraware/filemanager/database/recent/RecentFileManager;->deleteFile(Landroid/content/Context;Ljava/lang/String;)V

    .line 376
    invoke-static/range {p1 .. p1}, Lcom/infraware/common/database/ThumbnailManager;->getInstance(Landroid/content/Context;)Lcom/infraware/common/database/ThumbnailManager;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Lcom/infraware/filemanager/file/FileListItem;->getAbsolutePath()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Lcom/infraware/common/database/ThumbnailManager;->deleteFile(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 384
    .end local v4    # "pKey":I
    .end local v5    # "filename":Ljava/lang/String;
    .end local v6    # "path":Ljava/lang/String;
    .end local v7    # "size":J
    .end local v9    # "lastModifiedDate":J
    .end local v11    # "lastAccessedDate":J
    .end local v13    # "drmType":I
    .end local v16    # "fileItem":Lcom/infraware/filemanager/file/FileListItem;
    .end local v17    # "itemCount":I
    :catch_0
    move-exception v15

    .line 385
    .local v15, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v15}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 388
    if-eqz v14, :cond_6

    .line 389
    invoke-interface {v14}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_6

    .line 390
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 393
    :cond_6
    if-eqz v19, :cond_4

    .line 394
    invoke-virtual/range {v19 .. v19}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    goto :goto_1

    .line 388
    .end local v15    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v14, :cond_7

    .line 389
    invoke-interface {v14}, Landroid/database/Cursor;->isClosed()Z

    move-result v20

    if-nez v20, :cond_7

    .line 390
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 393
    :cond_7
    if-eqz v19, :cond_8

    .line 394
    invoke-virtual/range {v19 .. v19}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->close()V

    :cond_8
    throw v3
.end method
