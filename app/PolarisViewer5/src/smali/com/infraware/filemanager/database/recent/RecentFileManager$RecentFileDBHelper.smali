.class public Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "RecentFileManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/filemanager/database/recent/RecentFileManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RecentFileDBHelper"
.end annotation


# static fields
.field public static final RECENT_FILE_DB_FIELDIDX_CONTENTSRC:I = 0x7

.field public static final RECENT_FILE_DB_FIELDIDX_DATE_ACCESSED:I = 0xa

.field public static final RECENT_FILE_DB_FIELDIDX_DATE_MODIFIED:I = 0x9

.field public static final RECENT_FILE_DB_FIELDIDX_DRM_TYPE:I = 0xb

.field public static final RECENT_FILE_DB_FIELDIDX_FIELID:I = 0x5

.field public static final RECENT_FILE_DB_FIELDIDX_NAME:I = 0x1

.field public static final RECENT_FILE_DB_FIELDIDX_PARENT_FIELID:I = 0x6

.field public static final RECENT_FILE_DB_FIELDIDX_PATH:I = 0x2

.field public static final RECENT_FILE_DB_FIELDIDX_PKEY:I = 0x0

.field public static final RECENT_FILE_DB_FIELDIDX_SIZE:I = 0x8

.field public static final RECENT_FILE_DB_FIELDIDX_STORAGEID:I = 0x3

.field public static final RECENT_FILE_DB_FIELDIDX_STORAGENAME:I = 0x4

.field public static final RECENT_FILE_DB_FIELD_CONTENTSRC:Ljava/lang/String; = "contentsrc"

.field public static final RECENT_FILE_DB_FIELD_DATE_ACCESSED:Ljava/lang/String; = "last_accessed"

.field public static final RECENT_FILE_DB_FIELD_DATE_MODIFIED:Ljava/lang/String; = "last_modified"

.field public static final RECENT_FILE_DB_FIELD_DRM_TYPE:Ljava/lang/String; = "drm_type"

.field public static final RECENT_FILE_DB_FIELD_FIELD:Ljava/lang/String; = "fileid"

.field public static final RECENT_FILE_DB_FIELD_NAME:Ljava/lang/String; = "filename"

.field public static final RECENT_FILE_DB_FIELD_PARENT_FIELD:Ljava/lang/String; = "parent_fileid"

.field public static final RECENT_FILE_DB_FIELD_PATH:Ljava/lang/String; = "path"

.field public static final RECENT_FILE_DB_FIELD_PKEY:Ljava/lang/String; = "_id"

.field public static final RECENT_FILE_DB_FIELD_SIZE:Ljava/lang/String; = "size"

.field public static final RECENT_FILE_DB_FIELD_STORAGEID:Ljava/lang/String; = "storageid"

.field public static final RECENT_FILE_DB_FIELD_STORAGENAME:Ljava/lang/String; = "storagename"

.field public static final RECENT_FILE_DB_NAME:Ljava/lang/String; = "InfrawareRecentFiles.db"

.field public static final RECENT_FILE_DB_VERSION:I = 0x1

.field public static final RECENT_FILE_TABLE_NAME:Ljava/lang/String; = "RecentFiles"


# instance fields
.field final synthetic this$0:Lcom/infraware/filemanager/database/recent/RecentFileManager;


# direct methods
.method public constructor <init>(Lcom/infraware/filemanager/database/recent/RecentFileManager;Landroid/content/Context;)V
    .locals 3
    .param p2, "aCtx"    # Landroid/content/Context;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->this$0:Lcom/infraware/filemanager/database/recent/RecentFileManager;

    .line 53
    const-string/jumbo v0, "InfrawareRecentFiles.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p2, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 54
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 57
    const-string/jumbo v0, "CREATE TABLE RecentFiles(_id INTEGER PRIMARY KEY AUTOINCREMENT,filename TEXT,path TEXT,storageid TEXT,storagename TEXT,fileid TEXT,parent_fileid TEXT,contentsrc TEXT,size LONG,last_modified LONG,last_accessed LONG,drm_type INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 71
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 74
    const-string/jumbo v0, "DROP TABLE IF EXISTS InfrawareRecentFiles.db"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 75
    invoke-virtual {p0, p1}, Lcom/infraware/filemanager/database/recent/RecentFileManager$RecentFileDBHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 76
    return-void
.end method
