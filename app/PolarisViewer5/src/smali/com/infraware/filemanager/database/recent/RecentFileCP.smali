.class public Lcom/infraware/filemanager/database/recent/RecentFileCP;
.super Landroid/content/ContentProvider;
.source "RecentFileCP.java"


# static fields
.field static final ALL_RECENT_FILES:I = 0x1

.field static final AUTHROTY:Ljava/lang/String; = "com.infraware.polarisviewer5"

.field static final CONTENT_URI:Landroid/net/Uri;

.field static final MIME_TYPE_MULTI_RECENT_FILES:Ljava/lang/String; = "vnd.infraware.cursor.dir/recent_files"

.field static final MIME_TYPE_SINGLE_RECENT_FILE:Ljava/lang/String; = "vnd.infraware.cursor.item/recent_file"

.field static final ONE_RECENT_FILE:I = 0x2

.field static final RECENT_SCHEME:Ljava/lang/String; = "polaris_office_recent://"

.field static final matcher:Landroid/content/UriMatcher;


# instance fields
.field mDB:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 23
    const-string/jumbo v0, "content://com.infraware.polarisviewer5/recent_files"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/infraware/filemanager/database/recent/RecentFileCP;->CONTENT_URI:Landroid/net/Uri;

    .line 46
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/infraware/filemanager/database/recent/RecentFileCP;->matcher:Landroid/content/UriMatcher;

    .line 47
    sget-object v0, Lcom/infraware/filemanager/database/recent/RecentFileCP;->matcher:Landroid/content/UriMatcher;

    const-string/jumbo v1, "com.infraware.polarisviewer5"

    const-string/jumbo v2, "/recent_files"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 48
    sget-object v0, Lcom/infraware/filemanager/database/recent/RecentFileCP;->matcher:Landroid/content/UriMatcher;

    const-string/jumbo v1, "com.infraware.polarisviewer5"

    const-string/jumbo v2, "/recent_file/*"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 49
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method public static getFileAbsPathFromScheme(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "aDocViewScheme"    # Ljava/lang/String;

    .prologue
    .line 32
    const/4 v0, 0x0

    .line 34
    .local v0, "ret":Ljava/lang/String;
    const-string/jumbo v1, "polaris_office_recent://"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 35
    const-string/jumbo v1, "polaris_office_recent://"

    const-string/jumbo v2, ""

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 41
    :goto_0
    return-object v0

    .line 38
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 116
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 58
    sget-object v0, Lcom/infraware/filemanager/database/recent/RecentFileCP;->matcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 59
    const-string/jumbo v0, "vnd.infraware.cursor.dir/recent_files"

    .line 64
    :goto_0
    return-object v0

    .line 61
    :cond_0
    sget-object v0, Lcom/infraware/filemanager/database/recent/RecentFileCP;->matcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 62
    const-string/jumbo v0, "vnd.infraware.cursor.item/recent_file"

    goto :goto_0

    .line 64
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 122
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 13
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 68
    const/4 v8, 0x6

    new-array v6, v8, [Ljava/lang/String;

    .line 69
    .local v6, "liveFolderColumns":[Ljava/lang/String;
    const/4 v8, 0x0

    const-string/jumbo v9, "_id"

    aput-object v9, v6, v8

    .line 70
    const/4 v8, 0x1

    const-string/jumbo v9, "name"

    aput-object v9, v6, v8

    .line 71
    const/4 v8, 0x2

    const-string/jumbo v9, "description"

    aput-object v9, v6, v8

    .line 72
    const/4 v8, 0x3

    const-string/jumbo v9, "icon_package"

    aput-object v9, v6, v8

    .line 73
    const/4 v8, 0x4

    const-string/jumbo v9, "icon_resource"

    aput-object v9, v6, v8

    .line 74
    const/4 v8, 0x5

    const-string/jumbo v9, "intent"

    aput-object v9, v6, v8

    .line 76
    new-instance v1, Landroid/database/MatrixCursor;

    invoke-direct {v1, v6}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 79
    .local v1, "cursor":Landroid/database/MatrixCursor;
    const/4 v8, 0x6

    :try_start_0
    new-array v0, v8, [Ljava/lang/Object;

    .line 80
    .local v0, "columnValues":[Ljava/lang/Object;
    new-instance v3, Lcom/infraware/filemanager/file/FileListIcon;

    invoke-virtual {p0}, Lcom/infraware/filemanager/database/recent/RecentFileCP;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v3, v8}, Lcom/infraware/filemanager/file/FileListIcon;-><init>(Landroid/content/Context;)V

    .line 82
    .local v3, "fIcon":Lcom/infraware/filemanager/file/FileListIcon;
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/infraware/filemanager/database/recent/RecentFileCP;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 83
    invoke-static {}, Lcom/infraware/filemanager/database/recent/RecentFileManager;->getInstance()Lcom/infraware/filemanager/database/recent/RecentFileManager;

    move-result-object v8

    invoke-virtual {p0}, Lcom/infraware/filemanager/database/recent/RecentFileCP;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/infraware/filemanager/database/recent/RecentFileManager;->getRecentFiles(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v4

    .line 85
    .local v4, "fileItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/infraware/filemanager/file/FileListItem;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/infraware/filemanager/file/FileListItem;

    .line 86
    .local v7, "tempItem":Lcom/infraware/filemanager/file/FileListItem;
    const/4 v8, 0x0

    iget v9, v7, Lcom/infraware/filemanager/file/FileListItem;->recentPkey:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v0, v8

    .line 87
    const/4 v8, 0x1

    invoke-virtual {v7}, Lcom/infraware/filemanager/file/FileListItem;->getFullFileName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v0, v8

    .line 88
    const/4 v8, 0x2

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/infraware/filemanager/database/recent/RecentFileCP;->getContext()Landroid/content/Context;

    move-result-object v10

    iget-wide v11, v7, Lcom/infraware/filemanager/file/FileListItem;->updateTime:J

    invoke-static {v10, v11, v12}, Lcom/infraware/common/util/FileUtils;->getDateString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, v7, Lcom/infraware/filemanager/file/FileListItem;->size:J

    invoke-static {v10, v11}, Lcom/infraware/common/util/FileUtils;->getSizeString(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v0, v8

    .line 90
    const/4 v8, 0x3

    invoke-virtual {p0}, Lcom/infraware/filemanager/database/recent/RecentFileCP;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v0, v8

    .line 91
    const/4 v8, 0x4

    iget-object v9, v7, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    invoke-virtual {v3, v9}, Lcom/infraware/filemanager/file/FileListIcon;->getResByFileExt(Ljava/lang/String;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v0, v8

    .line 92
    const/4 v8, 0x5

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "polaris_office_recent://"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v7}, Lcom/infraware/filemanager/file/FileListItem;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v0, v8

    .line 93
    invoke-virtual {v1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 99
    .end local v0    # "columnValues":[Ljava/lang/Object;
    .end local v3    # "fIcon":Lcom/infraware/filemanager/file/FileListIcon;
    .end local v4    # "fileItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/infraware/filemanager/file/FileListItem;>;"
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v7    # "tempItem":Lcom/infraware/filemanager/file/FileListItem;
    :catch_0
    move-exception v2

    .line 100
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 103
    if-eqz v1, :cond_0

    .line 104
    invoke-virtual {v1}, Landroid/database/MatrixCursor;->isClosed()Z

    move-result v8

    if-nez v8, :cond_0

    .line 105
    invoke-virtual {v1}, Landroid/database/MatrixCursor;->close()V

    .line 110
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_1
    return-object v1

    .line 97
    .restart local v0    # "columnValues":[Ljava/lang/Object;
    .restart local v3    # "fIcon":Lcom/infraware/filemanager/file/FileListIcon;
    :cond_1
    :try_start_2
    invoke-virtual {p0}, Lcom/infraware/filemanager/database/recent/RecentFileCP;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-virtual {v1, v8, p1}, Landroid/database/MatrixCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 103
    if-eqz v1, :cond_0

    .line 104
    invoke-virtual {v1}, Landroid/database/MatrixCursor;->isClosed()Z

    move-result v8

    if-nez v8, :cond_0

    .line 105
    invoke-virtual {v1}, Landroid/database/MatrixCursor;->close()V

    goto :goto_1

    .line 103
    .end local v0    # "columnValues":[Ljava/lang/Object;
    .end local v3    # "fIcon":Lcom/infraware/filemanager/file/FileListIcon;
    :catchall_0
    move-exception v8

    if-eqz v1, :cond_2

    .line 104
    invoke-virtual {v1}, Landroid/database/MatrixCursor;->isClosed()Z

    move-result v9

    if-nez v9, :cond_2

    .line 105
    invoke-virtual {v1}, Landroid/database/MatrixCursor;->close()V

    :cond_2
    throw v8
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 128
    const/4 v0, 0x0

    return v0
.end method
