.class public Lcom/infraware/filemanager/manager/IconManager;
.super Ljava/lang/Object;
.source "IconManager.java"


# static fields
.field public static final ICON_RES_ASC:I = 0x7f0200c5

.field public static final ICON_RES_AUDIO:I = 0x7f0200c4

.field public static final ICON_RES_CSV:I = 0x7f0200b6

.field public static final ICON_RES_DOC:I = 0x7f0200b7

.field public static final ICON_RES_DOCX:I = 0x7f0200b8

.field public static final ICON_RES_DOT:I = 0x7f0200b7

.field public static final ICON_RES_DOTX:I = 0x7f0200b8

.field public static final ICON_RES_ETC:I = 0x7f0200c6

.field public static final ICON_RES_EXE:I = 0x7f0200b4

.field public static final ICON_RES_HTML:I = 0x7f0200bb

.field public static final ICON_RES_HWP:I = 0x7f0200bc

.field public static final ICON_RES_IMG:I = 0x7f0200bd

.field public static final ICON_RES_PDF:I = 0x7f0200bf

.field public static final ICON_RES_POT:I = 0x7f0200c1

.field public static final ICON_RES_POTX:I = 0x7f0200c2

.field public static final ICON_RES_PPS:I = 0x7f0200c0

.field public static final ICON_RES_PPSX:I = 0x7f0200c0

.field public static final ICON_RES_PPT:I = 0x7f0200c1

.field public static final ICON_RES_PPTX:I = 0x7f0200c2

.field public static final ICON_RES_RTF:I = 0x7f0200c3

.field public static final ICON_RES_TXT:I = 0x7f0200c5

.field public static final ICON_RES_VCARD:I = 0x7f0200b5

.field public static final ICON_RES_VCS:I = 0x7f0200c7

.field public static final ICON_RES_VIDEO:I = 0x7f0200be

.field public static final ICON_RES_VNT:I = 0x7f0200c8

.field public static final ICON_RES_XLS:I = 0x7f0200c9

.field public static final ICON_RES_XLSX:I = 0x7f0200ca

.field public static final ICON_RES_XLT:I = 0x7f0200c9

.field public static final ICON_RES_XLTX:I = 0x7f0200ca

.field public static final ICON_RES_XML:I = 0x7f0200cb

.field public static final ICON_RES_ZIP:I = 0x7f0200cc

.field public static final TYPE_RES_ASC:I = 0x7f07028d

.field public static final TYPE_RES_AUDIO:I = 0x7f070286

.field public static final TYPE_RES_CSV:I = 0x7f070287

.field public static final TYPE_RES_DOC:I = 0x7f070020

.field public static final TYPE_RES_DOCX:I = 0x7f070023

.field public static final TYPE_RES_DOT:I = 0x7f070020

.field public static final TYPE_RES_DOTX:I = 0x7f070023

.field public static final TYPE_RES_ETC:I = 0x7f07028e

.field public static final TYPE_RES_EXE:I = 0x7f070285

.field public static final TYPE_RES_HTML:I = 0x7f070289

.field public static final TYPE_RES_HWP:I = 0x7f07028a

.field public static final TYPE_RES_IMG:I = 0x7f07028b

.field public static final TYPE_RES_PDF:I = 0x7f070021

.field public static final TYPE_RES_POT:I = 0x7f070022

.field public static final TYPE_RES_POTX:I = 0x7f070025

.field public static final TYPE_RES_PPS:I = 0x7f070026

.field public static final TYPE_RES_PPSX:I = 0x7f070027

.field public static final TYPE_RES_PPT:I = 0x7f070022

.field public static final TYPE_RES_PPTX:I = 0x7f070025

.field public static final TYPE_RES_RTF:I = 0x7f07028c

.field public static final TYPE_RES_TXT:I = 0x7f07028d

.field public static final TYPE_RES_VCARD:I = 0x7f07028f

.field public static final TYPE_RES_VCS:I = 0x7f070290

.field public static final TYPE_RES_VIDEO:I = 0x7f070291

.field public static final TYPE_RES_VNT:I = 0x7f07028e

.field public static final TYPE_RES_XLS:I = 0x7f07001f

.field public static final TYPE_RES_XLSX:I = 0x7f070024

.field public static final TYPE_RES_XLT:I = 0x7f07001f

.field public static final TYPE_RES_XLTX:I = 0x7f070024

.field public static final TYPE_RES_XML:I = 0x7f070292

.field public static final TYPE_RES_ZIP:I = 0x7f070293

.field private static mBmpIconMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile mFileIconManager:Lcom/infraware/filemanager/manager/IconManager;

.field private static mResIconMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static mResTypeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21
    sput-object v0, Lcom/infraware/filemanager/manager/IconManager;->mFileIconManager:Lcom/infraware/filemanager/manager/IconManager;

    .line 22
    sput-object v0, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    .line 23
    sput-object v0, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    .line 24
    sput-object v0, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "aCtx"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    .line 29
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    .line 32
    invoke-static {p1}, Lcom/infraware/filemanager/manager/IconManager;->loadIconResourceToMap(Landroid/content/Context;)V

    .line 33
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/infraware/filemanager/manager/IconManager;
    .locals 2
    .param p0, "aCtx"    # Landroid/content/Context;

    .prologue
    .line 36
    sget-object v0, Lcom/infraware/filemanager/manager/IconManager;->mFileIconManager:Lcom/infraware/filemanager/manager/IconManager;

    if-nez v0, :cond_1

    .line 37
    const-class v1, Lcom/infraware/filemanager/manager/IconManager;

    monitor-enter v1

    .line 38
    :try_start_0
    sget-object v0, Lcom/infraware/filemanager/manager/IconManager;->mFileIconManager:Lcom/infraware/filemanager/manager/IconManager;

    if-nez v0, :cond_0

    .line 39
    new-instance v0, Lcom/infraware/filemanager/manager/IconManager;

    invoke-direct {v0, p0}, Lcom/infraware/filemanager/manager/IconManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/infraware/filemanager/manager/IconManager;->mFileIconManager:Lcom/infraware/filemanager/manager/IconManager;

    .line 41
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 43
    :cond_1
    sget-object v0, Lcom/infraware/filemanager/manager/IconManager;->mFileIconManager:Lcom/infraware/filemanager/manager/IconManager;

    return-object v0

    .line 41
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static isExcutable(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 6
    .param p0, "aCtx"    # Landroid/content/Context;
    .param p1, "ext"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 198
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v4, "android.intent.action.VIEW"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 199
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/infraware/common/util/FileUtils;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 200
    .local v2, "type":Ljava/lang/String;
    if-nez v2, :cond_1

    .line 208
    :cond_0
    :goto_0
    return v3

    .line 203
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "file://mnt/sdcard/a."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 204
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/high16 v5, 0x10000

    invoke-virtual {v4, v1, v5}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 205
    .local v0, "Apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-eqz v4, :cond_0

    .line 208
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private static loadIconResourceToMap(Landroid/content/Context;)V
    .locals 9
    .param p0, "aCtx"    # Landroid/content/Context;

    .prologue
    const v8, 0x7f070286

    const v7, 0x7f0200c4

    const v6, 0x7f0200bd

    const v5, 0x7f070291

    const v4, 0x7f0200be

    .line 222
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 224
    .local v0, "res":Landroid/content/res/Resources;
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    const-string/jumbo v2, "doc"

    const v3, 0x7f070020

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    const-string/jumbo v2, "doc"

    const v3, 0x7f0200b7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200b7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200b7

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    const-string/jumbo v2, "docx"

    const v3, 0x7f070023

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    const-string/jumbo v2, "docx"

    const v3, 0x7f0200b8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200b8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200b8

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    const-string/jumbo v1, "dot"

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 234
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    const-string/jumbo v2, "dot"

    const v3, 0x7f070020

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    const-string/jumbo v2, "dot"

    const v3, 0x7f0200b7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    :goto_0
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200b7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200b7

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    const-string/jumbo v1, "dotx"

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 244
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    const-string/jumbo v2, "dotx"

    const v3, 0x7f070023

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    const-string/jumbo v2, "dotx"

    const v3, 0x7f0200b8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    :goto_1
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200b8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200b8

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    const-string/jumbo v2, "xls"

    const v3, 0x7f07001f

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    const-string/jumbo v2, "xls"

    const v3, 0x7f0200c9

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200c9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200c9

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    const-string/jumbo v2, "xlsx"

    const v3, 0x7f070024

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    const-string/jumbo v2, "xlsx"

    const v3, 0x7f0200ca

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200ca

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200ca

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    const-string/jumbo v1, "xlt"

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 262
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    const-string/jumbo v2, "xlt"

    const v3, 0x7f07001f

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    const-string/jumbo v2, "xlt"

    const v3, 0x7f0200c9

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    :goto_2
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200c9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200c9

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    const-string/jumbo v1, "xltx"

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 272
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    const-string/jumbo v2, "xltx"

    const v3, 0x7f070024

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    const-string/jumbo v2, "xltx"

    const v3, 0x7f0200ca

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    :goto_3
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200ca

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200ca

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    const-string/jumbo v2, "ppt"

    const v3, 0x7f070022

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    const-string/jumbo v2, "ppt"

    const v3, 0x7f0200c1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200c1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200c1

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    const-string/jumbo v2, "pptx"

    const v3, 0x7f070025

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    const-string/jumbo v2, "pptx"

    const v3, 0x7f0200c2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200c2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200c2

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    const-string/jumbo v1, "pot"

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 290
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    const-string/jumbo v2, "pot"

    const v3, 0x7f070022

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    const-string/jumbo v2, "pot"

    const v3, 0x7f0200c1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    :goto_4
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200c1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200c1

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    const-string/jumbo v1, "potx"

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 300
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    const-string/jumbo v2, "potx"

    const v3, 0x7f070025

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    const-string/jumbo v2, "potx"

    const v3, 0x7f0200c2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    :goto_5
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200c2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200c2

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    const-string/jumbo v1, "pps"

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 310
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    const-string/jumbo v2, "pps"

    const v3, 0x7f070026

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    const-string/jumbo v2, "pps"

    const v3, 0x7f0200c0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 316
    :goto_6
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200c0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200c0

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 318
    const-string/jumbo v1, "ppsx"

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 320
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    const-string/jumbo v2, "ppsx"

    const v3, 0x7f070027

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    const-string/jumbo v2, "ppsx"

    const v3, 0x7f0200c0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    :goto_7
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200c0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200c0

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    const-string/jumbo v2, "txt"

    const v3, 0x7f07028d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    const-string/jumbo v2, "txt"

    const v3, 0x7f0200c5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200c5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200c5

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    const-string/jumbo v2, "asc"

    const v3, 0x7f07028d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 333
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    const-string/jumbo v2, "asc"

    const v3, 0x7f0200c5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 334
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200c5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200c5

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    const-string/jumbo v2, "hwp"

    const v3, 0x7f07028a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 337
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    const-string/jumbo v2, "hwp"

    const v3, 0x7f0200bc

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 338
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200bc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200bc

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    const-string/jumbo v2, "pdf"

    const v3, 0x7f070021

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 341
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    const-string/jumbo v2, "pdf"

    const v3, 0x7f0200bf

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 342
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200bf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200bf

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 344
    const-string/jumbo v1, "xml"

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 346
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    const-string/jumbo v2, "xml"

    const v3, 0x7f070292

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 347
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    const-string/jumbo v2, "xml"

    const v3, 0x7f0200cb

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    :goto_8
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200cb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200cb

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    const-string/jumbo v2, "csv"

    const v3, 0x7f070287

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    const-string/jumbo v2, "csv"

    const v3, 0x7f0200b6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200b6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200b6

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 358
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    const-string/jumbo v2, "rtf"

    const v3, 0x7f07028c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 359
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    const-string/jumbo v2, "rtf"

    const v3, 0x7f0200c3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200c3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200c3

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    const-string/jumbo v2, "zip"

    const v3, 0x7f070293

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 363
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    const-string/jumbo v2, "zip"

    const v3, 0x7f0200cc

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 364
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200cc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200cc

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 367
    const-string/jumbo v1, "apk"

    const v2, 0x7f0200b4

    const v3, 0x7f070285

    invoke-static {p0, v1, v2, v3}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 368
    const-string/jumbo v1, "exe"

    const v2, 0x7f0200b4

    const v3, 0x7f070285

    invoke-static {p0, v1, v2, v3}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 369
    const-string/jumbo v1, "com"

    const v2, 0x7f0200b4

    const v3, 0x7f070285

    invoke-static {p0, v1, v2, v3}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 370
    const-string/jumbo v1, "pkg"

    const v2, 0x7f0200b4

    const v3, 0x7f070285

    invoke-static {p0, v1, v2, v3}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 371
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200b4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200b4

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 374
    const-string/jumbo v1, "ico"

    const v2, 0x7f07028b

    invoke-static {p0, v1, v6, v2}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 375
    const-string/jumbo v1, "img"

    const v2, 0x7f07028b

    invoke-static {p0, v1, v6, v2}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 376
    const-string/jumbo v1, "tga"

    const v2, 0x7f07028b

    invoke-static {p0, v1, v6, v2}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 377
    const-string/jumbo v1, "jpeg"

    const v2, 0x7f07028b

    invoke-static {p0, v1, v6, v2}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 378
    const-string/jumbo v1, "jpg"

    const v2, 0x7f07028b

    invoke-static {p0, v1, v6, v2}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 379
    const-string/jumbo v1, "bmp"

    const v2, 0x7f07028b

    invoke-static {p0, v1, v6, v2}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 380
    const-string/jumbo v1, "gif"

    const v2, 0x7f07028b

    invoke-static {p0, v1, v6, v2}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 381
    const-string/jumbo v1, "png"

    const v2, 0x7f07028b

    invoke-static {p0, v1, v6, v2}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 382
    const-string/jumbo v1, "wbmp"

    const v2, 0x7f07028b

    invoke-static {p0, v1, v6, v2}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 383
    const-string/jumbo v1, "pcx"

    const v2, 0x7f07028b

    invoke-static {p0, v1, v6, v2}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 384
    const-string/jumbo v1, "wmf"

    const v2, 0x7f07028b

    invoke-static {p0, v1, v6, v2}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 385
    const-string/jumbo v1, "emf"

    const v2, 0x7f07028b

    invoke-static {p0, v1, v6, v2}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 386
    const-string/jumbo v1, "tif"

    const v2, 0x7f07028b

    invoke-static {p0, v1, v6, v2}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 387
    const-string/jumbo v1, "tiff"

    const v2, 0x7f07028b

    invoke-static {p0, v1, v6, v2}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 388
    const-string/jumbo v1, "mpo"

    const v2, 0x7f07028b

    invoke-static {p0, v1, v6, v2}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 389
    const-string/jumbo v1, "dci"

    const v2, 0x7f07028b

    invoke-static {p0, v1, v6, v2}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 390
    const-string/jumbo v1, "jps"

    const v2, 0x7f07028b

    invoke-static {p0, v1, v6, v2}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 391
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 394
    const-string/jumbo v1, "aac"

    invoke-static {p0, v1, v7, v8}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 395
    const-string/jumbo v1, "amr"

    invoke-static {p0, v1, v7, v8}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 396
    const-string/jumbo v1, "imy"

    invoke-static {p0, v1, v7, v8}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 397
    const-string/jumbo v1, "mp3"

    invoke-static {p0, v1, v7, v8}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 398
    const-string/jumbo v1, "wma"

    invoke-static {p0, v1, v7, v8}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 399
    const-string/jumbo v1, "wav"

    invoke-static {p0, v1, v7, v8}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 400
    const-string/jumbo v1, "ogg"

    invoke-static {p0, v1, v7, v8}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 401
    const-string/jumbo v1, "mid"

    invoke-static {p0, v1, v7, v8}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 402
    const-string/jumbo v1, "midi"

    invoke-static {p0, v1, v7, v8}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 403
    const-string/jumbo v1, "m4a"

    invoke-static {p0, v1, v7, v8}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 404
    const-string/jumbo v1, "mmf"

    invoke-static {p0, v1, v7, v8}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 405
    const-string/jumbo v1, "xmf"

    invoke-static {p0, v1, v7, v8}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 406
    const-string/jumbo v1, "3ga"

    invoke-static {p0, v1, v7, v8}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 407
    const-string/jumbo v1, "flac"

    invoke-static {p0, v1, v7, v8}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 408
    const-string/jumbo v1, "qcp"

    invoke-static {p0, v1, v7, v8}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 409
    const-string/jumbo v1, "mpga"

    invoke-static {p0, v1, v7, v8}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 410
    const-string/jumbo v1, "mxmf"

    invoke-static {p0, v1, v7, v8}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 411
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v7}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 414
    const-string/jumbo v1, "avi"

    invoke-static {p0, v1, v4, v5}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 415
    const-string/jumbo v1, "mp4"

    invoke-static {p0, v1, v4, v5}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 416
    const-string/jumbo v1, "mpg"

    invoke-static {p0, v1, v4, v5}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 417
    const-string/jumbo v1, "mpeg"

    invoke-static {p0, v1, v4, v5}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 418
    const-string/jumbo v1, "mkv"

    invoke-static {p0, v1, v4, v5}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 419
    const-string/jumbo v1, "mov"

    invoke-static {p0, v1, v4, v5}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 420
    const-string/jumbo v1, "divx"

    invoke-static {p0, v1, v4, v5}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 421
    const-string/jumbo v1, "3gp"

    invoke-static {p0, v1, v4, v5}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 422
    const-string/jumbo v1, "3gpp"

    invoke-static {p0, v1, v4, v5}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 423
    const-string/jumbo v1, "3g2"

    invoke-static {p0, v1, v4, v5}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 424
    const-string/jumbo v1, "asf"

    invoke-static {p0, v1, v4, v5}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 425
    const-string/jumbo v1, "wmv"

    invoke-static {p0, v1, v4, v5}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 426
    const-string/jumbo v1, "m4v"

    invoke-static {p0, v1, v4, v5}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 427
    const-string/jumbo v1, "k3g"

    invoke-static {p0, v1, v4, v5}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 428
    const-string/jumbo v1, "skm"

    invoke-static {p0, v1, v4, v5}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 429
    const-string/jumbo v1, "ts"

    invoke-static {p0, v1, v4, v5}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 430
    const-string/jumbo v1, "flv"

    invoke-static {p0, v1, v4, v5}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 431
    const-string/jumbo v1, "dmb"

    invoke-static {p0, v1, v4, v5}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 432
    const-string/jumbo v1, "rm"

    invoke-static {p0, v1, v4, v5}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 433
    const-string/jumbo v1, "rmvb"

    invoke-static {p0, v1, v4, v5}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 435
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 438
    const-string/jumbo v1, "htm"

    const v2, 0x7f0200bb

    const v3, 0x7f070289

    invoke-static {p0, v1, v2, v3}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 439
    const-string/jumbo v1, "html"

    const v2, 0x7f0200bb

    const v3, 0x7f070289

    invoke-static {p0, v1, v2, v3}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 440
    const-string/jumbo v1, "mht"

    const v2, 0x7f0200bb

    const v3, 0x7f070289

    invoke-static {p0, v1, v2, v3}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 441
    const-string/jumbo v1, "mhtml"

    const v2, 0x7f0200bb

    const v3, 0x7f070289

    invoke-static {p0, v1, v2, v3}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 442
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200bb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200bb

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 445
    const-string/jumbo v1, "vcf"

    const v2, 0x7f0200b5

    const v3, 0x7f07028f

    invoke-static {p0, v1, v2, v3}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 446
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200b5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200b5

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 449
    const-string/jumbo v1, "vcs"

    const v2, 0x7f0200c7

    const v3, 0x7f070290

    invoke-static {p0, v1, v2, v3}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 450
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200c7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200c7

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 453
    const-string/jumbo v1, "vnt"

    const v2, 0x7f0200c8

    const v3, 0x7f07028e

    invoke-static {p0, v1, v2, v3}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    .line 454
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200c8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200c8

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 457
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200c6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200c6

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    return-void

    .line 238
    :cond_0
    const-string/jumbo v1, "dot"

    const v2, 0x7f0200b7

    const v3, 0x7f070020

    invoke-static {p0, v1, v2, v3}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    goto/16 :goto_0

    .line 248
    :cond_1
    const-string/jumbo v1, "dotx"

    const v2, 0x7f0200b8

    const v3, 0x7f070023

    invoke-static {p0, v1, v2, v3}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    goto/16 :goto_1

    .line 266
    :cond_2
    const-string/jumbo v1, "xlt"

    const v2, 0x7f0200c9

    const v3, 0x7f07001f

    invoke-static {p0, v1, v2, v3}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    goto/16 :goto_2

    .line 276
    :cond_3
    const-string/jumbo v1, "xltx"

    const v2, 0x7f0200ca

    const v3, 0x7f070024

    invoke-static {p0, v1, v2, v3}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    goto/16 :goto_3

    .line 294
    :cond_4
    const-string/jumbo v1, "pot"

    const v2, 0x7f0200c1

    const v3, 0x7f070022

    invoke-static {p0, v1, v2, v3}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    goto/16 :goto_4

    .line 304
    :cond_5
    const-string/jumbo v1, "potx"

    const v2, 0x7f0200c2

    const v3, 0x7f070025

    invoke-static {p0, v1, v2, v3}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    goto/16 :goto_5

    .line 314
    :cond_6
    const-string/jumbo v1, "pps"

    const v2, 0x7f0200c0

    const v3, 0x7f070026

    invoke-static {p0, v1, v2, v3}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    goto/16 :goto_6

    .line 324
    :cond_7
    const-string/jumbo v1, "ppsx"

    const v2, 0x7f0200c0

    const v3, 0x7f070027

    invoke-static {p0, v1, v2, v3}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    goto/16 :goto_7

    .line 350
    :cond_8
    const-string/jumbo v1, "xml"

    const v2, 0x7f0200cb

    const v3, 0x7f070292

    invoke-static {p0, v1, v2, v3}, Lcom/infraware/filemanager/manager/IconManager;->setResMap(Landroid/content/Context;Ljava/lang/String;II)V

    goto/16 :goto_8
.end method

.method private static setResMap(Landroid/content/Context;Ljava/lang/String;II)V
    .locals 2
    .param p0, "aCtx"    # Landroid/content/Context;
    .param p1, "ext"    # Ljava/lang/String;
    .param p2, "iconRes"    # I
    .param p3, "typeRes"    # I

    .prologue
    .line 213
    invoke-static {p0, p1}, Lcom/infraware/filemanager/manager/IconManager;->isExcutable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    sget-object v0, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    sget-object v0, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    :cond_0
    return-void
.end method


# virtual methods
.method public getBmpByExt(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "aExt"    # Ljava/lang/String;

    .prologue
    .line 116
    invoke-virtual {p0, p1}, Lcom/infraware/filemanager/manager/IconManager;->getResIdByExt(Ljava/lang/String;)I

    move-result v0

    .line 118
    .local v0, "resId":I
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 119
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 122
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v2, 0x7f0200c6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public getImageBmp()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 128
    sget-object v0, Lcom/infraware/filemanager/manager/IconManager;->mBmpIconMap:Ljava/util/HashMap;

    const v1, 0x7f0200bd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getResIdByExt(Ljava/lang/String;)I
    .locals 3
    .param p1, "aExt"    # Ljava/lang/String;

    .prologue
    const v0, 0x7f0200c6

    .line 47
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 54
    :cond_0
    :goto_0
    return v0

    .line 50
    :cond_1
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51
    sget-object v0, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public getResIdByFileName(Ljava/lang/String;)I
    .locals 3
    .param p1, "aFileName"    # Ljava/lang/String;

    .prologue
    .line 58
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 59
    :cond_0
    const v2, 0x7f0200c6

    .line 68
    :goto_0
    return v2

    .line 61
    :cond_1
    const/16 v2, 0x2e

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 63
    .local v1, "idx_ext":I
    if-gez v1, :cond_2

    .line 64
    const-string/jumbo v0, ""

    .line 68
    .local v0, "ext":Ljava/lang/String;
    :goto_1
    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/manager/IconManager;->getResIdByExt(Ljava/lang/String;)I

    move-result v2

    goto :goto_0

    .line 66
    .end local v0    # "ext":Ljava/lang/String;
    :cond_2
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "ext":Ljava/lang/String;
    goto :goto_1
.end method

.method public getTypeByExt(Ljava/lang/String;)I
    .locals 3
    .param p1, "aExt"    # Ljava/lang/String;

    .prologue
    const v0, 0x7f07028e

    .line 72
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 79
    :cond_0
    :goto_0
    return v0

    .line 75
    :cond_1
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 76
    sget-object v0, Lcom/infraware/filemanager/manager/IconManager;->mResTypeMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public getTypeByFileName(Ljava/lang/String;)I
    .locals 3
    .param p1, "aFileName"    # Ljava/lang/String;

    .prologue
    .line 83
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 84
    :cond_0
    const v2, 0x7f0200c6

    .line 93
    :goto_0
    return v2

    .line 86
    :cond_1
    const/16 v2, 0x2e

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 88
    .local v1, "idx_ext":I
    if-gez v1, :cond_2

    .line 89
    const-string/jumbo v0, ""

    .line 93
    .local v0, "ext":Ljava/lang/String;
    :goto_1
    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/manager/IconManager;->getTypeByExt(Ljava/lang/String;)I

    move-result v2

    goto :goto_0

    .line 91
    .end local v0    # "ext":Ljava/lang/String;
    :cond_2
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "ext":Ljava/lang/String;
    goto :goto_1
.end method

.method public isImageExt(Ljava/lang/String;)Z
    .locals 4
    .param p1, "aExt"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 97
    if-nez p1, :cond_0

    move v1, v2

    .line 112
    :goto_0
    return v1

    .line 100
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    .line 101
    goto :goto_0

    .line 103
    :cond_1
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 104
    sget-object v1, Lcom/infraware/filemanager/manager/IconManager;->mResIconMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 106
    .local v0, "resId":I
    const v1, 0x7f0200bd

    if-ne v0, v1, :cond_2

    .line 107
    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    move v1, v2

    .line 109
    goto :goto_0

    .end local v0    # "resId":I
    :cond_3
    move v1, v2

    .line 112
    goto :goto_0
.end method
