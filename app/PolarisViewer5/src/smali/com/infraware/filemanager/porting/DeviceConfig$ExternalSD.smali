.class public Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;
.super Ljava/lang/Object;
.source "DeviceConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/filemanager/porting/DeviceConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ExternalSD"
.end annotation


# static fields
.field private static final EXTERNAL_FOLDER_ICON:[I

.field private static final EXTERNAL_FOLDER_NAME:[I

.field private static final EXTERNAL_FOLDER_PATH:[Ljava/lang/String;

.field private static final EXTERNAL_FOLDER_PATH_JB:[Ljava/lang/String;

.field private static final EXTRA_STORAGE:Ljava/lang/String; = "storage"

.field private static final STORAGE_MOUNTED:Ljava/lang/String; = "mounted"

.field private static final STORAGE_TYPE_DEFAULT:Ljava/lang/String; = "fuse"

.field private static final STORAGE_TYPE_USB:Ljava/lang/String; = "usb"


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x7

    .line 167
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "/mnt/external_sd"

    aput-object v1, v0, v4

    const-string/jumbo v1, "/Removable/MicroSD"

    aput-object v1, v0, v5

    const-string/jumbo v1, "/mnt/extSdCard"

    aput-object v1, v0, v6

    const-string/jumbo v1, "/mnt/sdcard2"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string/jumbo v2, "/mnt/extstorage"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "/mnt/private"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "/storage/PersonalPage"

    aput-object v2, v0, v1

    sput-object v0, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->EXTERNAL_FOLDER_PATH:[Ljava/lang/String;

    .line 178
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "/storage/external_sd"

    aput-object v1, v0, v4

    const-string/jumbo v1, "/Removable/MicroSD"

    aput-object v1, v0, v5

    const-string/jumbo v1, "/storage/extSdCard"

    aput-object v1, v0, v6

    const-string/jumbo v1, "/storage/sdcard2"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string/jumbo v2, "/storage/extstorage"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "/storage/private"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "/storage/PersonalPage"

    aput-object v2, v0, v1

    sput-object v0, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->EXTERNAL_FOLDER_PATH_JB:[Ljava/lang/String;

    .line 189
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->EXTERNAL_FOLDER_ICON:[I

    .line 200
    new-array v0, v3, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->EXTERNAL_FOLDER_NAME:[I

    return-void

    .line 189
    :array_0
    .array-data 4
        0x7f020112
        0x7f020112
        0x7f020112
        0x7f020112
        0x7f020112
        0x7f020112
        0x7f0200cd
    .end array-data

    .line 200
    :array_1
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x7f07002e
        0x0
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCount()I
    .locals 2

    .prologue
    .line 216
    invoke-static {}, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->useExternalSD()Z

    move-result v1

    if-nez v1, :cond_1

    .line 217
    const/4 v0, 0x0

    .line 223
    .local v0, "count":I
    :cond_0
    :goto_0
    return v0

    .line 219
    .end local v0    # "count":I
    :cond_1
    sget-object v1, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->EXTERNAL_FOLDER_PATH:[Ljava/lang/String;

    array-length v0, v1

    .line 220
    .restart local v0    # "count":I
    sget-object v1, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->EXTERNAL_FOLDER_ICON:[I

    array-length v1, v1

    if-le v0, v1, :cond_0

    .line 221
    sget-object v1, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->EXTERNAL_FOLDER_ICON:[I

    array-length v0, v1

    goto :goto_0
.end method

.method public static getFolderIcon(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "index"    # I

    .prologue
    .line 252
    sget-object v0, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->EXTERNAL_FOLDER_ICON:[I

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 253
    sget-object v0, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->EXTERNAL_FOLDER_ICON:[I

    aget v0, v0, p1

    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 255
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getFolderName(Landroid/content/res/Resources;I)Ljava/lang/String;
    .locals 1
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "index"    # I

    .prologue
    .line 242
    sget-object v0, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->EXTERNAL_FOLDER_NAME:[I

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 244
    sget-object v0, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->EXTERNAL_FOLDER_NAME:[I

    aget v0, v0, p1

    if-eqz v0, :cond_0

    .line 245
    sget-object v0, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->EXTERNAL_FOLDER_NAME:[I

    aget v0, v0, p1

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 248
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getFolderPath(I)Ljava/lang/String;
    .locals 2
    .param p0, "index"    # I

    .prologue
    .line 227
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 229
    sget-object v0, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->EXTERNAL_FOLDER_PATH_JB:[Ljava/lang/String;

    array-length v0, v0

    if-ge p0, v0, :cond_1

    .line 230
    sget-object v0, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->EXTERNAL_FOLDER_PATH_JB:[Ljava/lang/String;

    aget-object v0, v0, p0

    .line 238
    :goto_0
    return-object v0

    .line 234
    :cond_0
    sget-object v0, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->EXTERNAL_FOLDER_PATH:[Ljava/lang/String;

    array-length v0, v0

    if-ge p0, v0, :cond_1

    .line 235
    sget-object v0, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->EXTERNAL_FOLDER_PATH:[Ljava/lang/String;

    aget-object v0, v0, p0

    goto :goto_0

    .line 238
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getUSBMountList(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 10
    .param p0, "paramContext"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 289
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 291
    .local v4, "oMountList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string/jumbo v9, "storage"

    invoke-virtual {p0, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/storage/StorageManager;

    .line 293
    .local v5, "oStragemanager":Landroid/os/storage/StorageManager;
    invoke-virtual {v5}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v0

    .line 294
    .local v0, "arrayOfStorageVolume":[Landroid/os/storage/StorageVolume;
    array-length v1, v0

    .line 296
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 298
    aget-object v3, v0, v2

    .line 300
    .local v3, "localStorageVolume":Landroid/os/storage/StorageVolume;
    invoke-virtual {v3}, Landroid/os/storage/StorageVolume;->getSubSystem()Ljava/lang/String;

    move-result-object v7

    .line 301
    .local v7, "szMountType":Ljava/lang/String;
    invoke-virtual {v3}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v8

    .line 302
    .local v8, "szStoragePath":Ljava/lang/String;
    invoke-virtual {v5, v8}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 304
    .local v6, "szIsMount":Ljava/lang/String;
    const-string/jumbo v9, "usb"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    const-string/jumbo v9, "mounted"

    invoke-virtual {v9, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 307
    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 296
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 311
    .end local v3    # "localStorageVolume":Landroid/os/storage/StorageVolume;
    .end local v6    # "szIsMount":Ljava/lang/String;
    .end local v7    # "szMountType":Ljava/lang/String;
    .end local v8    # "szStoragePath":Ljava/lang/String;
    :cond_1
    return-object v4
.end method

.method public static isMainStoragePath(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 9
    .param p0, "paramContext"    # Landroid/content/Context;
    .param p1, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 341
    const-string/jumbo v8, "storage"

    invoke-virtual {p0, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/storage/StorageManager;

    .line 343
    .local v4, "oStragemanager":Landroid/os/storage/StorageManager;
    invoke-virtual {v4}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v0

    .line 344
    .local v0, "arrayOfStorageVolume":[Landroid/os/storage/StorageVolume;
    array-length v1, v0

    .line 346
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 348
    aget-object v3, v0, v2

    .line 350
    .local v3, "localStorageVolume":Landroid/os/storage/StorageVolume;
    invoke-virtual {v3}, Landroid/os/storage/StorageVolume;->getSubSystem()Ljava/lang/String;

    move-result-object v5

    .line 351
    .local v5, "szMountType":Ljava/lang/String;
    invoke-virtual {v3}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v6

    .line 353
    .local v6, "szStoragePath":Ljava/lang/String;
    const-string/jumbo v8, "fuse"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 356
    if-eqz p1, :cond_0

    if-nez v6, :cond_1

    .line 365
    .end local v3    # "localStorageVolume":Landroid/os/storage/StorageVolume;
    .end local v5    # "szMountType":Ljava/lang/String;
    .end local v6    # "szStoragePath":Ljava/lang/String;
    :cond_0
    :goto_1
    return v7

    .line 358
    .restart local v3    # "localStorageVolume":Landroid/os/storage/StorageVolume;
    .restart local v5    # "szMountType":Ljava/lang/String;
    .restart local v6    # "szStoragePath":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 359
    const/4 v7, 0x1

    goto :goto_1

    .line 346
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static isMountedPath(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 8
    .param p0, "paramContext"    # Landroid/content/Context;
    .param p1, "szPath"    # Ljava/lang/String;

    .prologue
    .line 316
    const-string/jumbo v7, "storage"

    invoke-virtual {p0, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/storage/StorageManager;

    .line 318
    .local v4, "oStragemanager":Landroid/os/storage/StorageManager;
    invoke-virtual {v4}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v0

    .line 319
    .local v0, "arrayOfStorageVolume":[Landroid/os/storage/StorageVolume;
    array-length v1, v0

    .line 321
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_2

    .line 323
    aget-object v3, v0, v2

    .line 325
    .local v3, "localStorageVolume":Landroid/os/storage/StorageVolume;
    invoke-virtual {v3}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v6

    .line 326
    .local v6, "szStoragePath":Ljava/lang/String;
    invoke-virtual {v4, v6}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 329
    .local v5, "szIsMount":Ljava/lang/String;
    invoke-virtual {p1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 321
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 332
    :cond_1
    const-string/jumbo v7, "mounted"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 333
    const/4 v7, 0x1

    .line 336
    .end local v3    # "localStorageVolume":Landroid/os/storage/StorageVolume;
    .end local v5    # "szIsMount":Ljava/lang/String;
    .end local v6    # "szStoragePath":Ljava/lang/String;
    :goto_1
    return v7

    :cond_2
    const/4 v7, 0x0

    goto :goto_1
.end method

.method public static isSdCardFile(Ljava/lang/String;)Z
    .locals 4
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 260
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_2

    .line 262
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v2, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->EXTERNAL_FOLDER_PATH_JB:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 263
    sget-object v2, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->EXTERNAL_FOLDER_PATH_JB:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v1, :cond_1

    .line 276
    :cond_0
    :goto_1
    return v1

    .line 262
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 270
    .end local v0    # "i":I
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    sget-object v2, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->EXTERNAL_FOLDER_PATH:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 271
    sget-object v2, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->EXTERNAL_FOLDER_PATH:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eq v2, v1, :cond_0

    .line 270
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 276
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static useExternalSD()Z
    .locals 1

    .prologue
    .line 212
    const/4 v0, 0x1

    return v0
.end method
