.class public Lcom/infraware/filemanager/porting/DeviceConfig$SDCardList;
.super Ljava/lang/Object;
.source "DeviceConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/filemanager/porting/DeviceConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SDCardList"
.end annotation


# static fields
.field public static final MAX_SDCARD_COUNT:I = 0x1

.field public static final SDCARD_FOLDER_ICON:[I

.field public static final SDCARD_FOLDER_NAME:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 373
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "external_sd"

    aput-object v1, v0, v2

    sput-object v0, Lcom/infraware/filemanager/porting/DeviceConfig$SDCardList;->SDCARD_FOLDER_NAME:[Ljava/lang/String;

    .line 375
    new-array v0, v3, [I

    const v1, 0x7f020112

    aput v1, v0, v2

    sput-object v0, Lcom/infraware/filemanager/porting/DeviceConfig$SDCardList;->SDCARD_FOLDER_ICON:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getSDCardIcon(Landroid/content/res/Resources;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "folderName"    # Ljava/lang/String;

    .prologue
    .line 380
    const/4 v1, 0x0

    .line 381
    .local v1, "icon":Landroid/graphics/Bitmap;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x1

    if-ge v0, v2, :cond_0

    .line 383
    sget-object v2, Lcom/infraware/filemanager/porting/DeviceConfig$SDCardList;->SDCARD_FOLDER_NAME:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 384
    sget-object v2, Lcom/infraware/filemanager/porting/DeviceConfig$SDCardList;->SDCARD_FOLDER_ICON:[I

    aget v2, v2, v0

    invoke-static {p0, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 388
    :cond_0
    return-object v1

    .line 381
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
