.class Lcom/infraware/filemanager/FileInfoActivity$4;
.super Ljava/lang/Object;
.source "FileInfoActivity.java"

# interfaces
.implements Ljava/io/FileFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/filemanager/FileInfoActivity;->getLocalSubfolders(Ljava/io/File;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/filemanager/FileInfoActivity;


# direct methods
.method constructor <init>(Lcom/infraware/filemanager/FileInfoActivity;)V
    .locals 0

    .prologue
    .line 1234
    iput-object p1, p0, Lcom/infraware/filemanager/FileInfoActivity$4;->this$0:Lcom/infraware/filemanager/FileInfoActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Ljava/io/File;)Z
    .locals 4
    .param p1, "pathname"    # Ljava/io/File;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1236
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x2e

    if-ne v2, v3, :cond_0

    .line 1239
    :goto_0
    return v1

    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-ne v2, v0, :cond_1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method
