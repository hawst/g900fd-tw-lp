.class public Lcom/infraware/filemanager/LiveFolderActivity;
.super Landroid/app/Activity;
.source "LiveFolderActivity.java"


# static fields
.field private static final AUTHROTY:Ljava/lang/String; = "com.infraware.polarisviewer5"

.field private static final RECENT_FILE_CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-string/jumbo v0, "content://com.infraware.polarisviewer5/recent_files"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/infraware/filemanager/LiveFolderActivity;->RECENT_FILE_CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private static createLiveFolder(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 3
    .param p0, "aCtx"    # Landroid/content/Context;
    .param p1, "aUri"    # Landroid/net/Uri;
    .param p2, "aName"    # Ljava/lang/String;
    .param p3, "aIcon"    # I

    .prologue
    .line 39
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 40
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 41
    const-string/jumbo v1, "android.intent.extra.livefolder.NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 43
    const-string/jumbo v1, "android.intent.extra.livefolder.DISPLAY_MODE"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 44
    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 20
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    invoke-virtual {p0}, Lcom/infraware/filemanager/LiveFolderActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 23
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 24
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v3, "android.intent.action.CREATE_LIVE_FOLDER"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 26
    invoke-virtual {p0}, Lcom/infraware/filemanager/LiveFolderActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070298

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 27
    .local v2, "title":Ljava/lang/String;
    const/4 v3, -0x1

    sget-object v4, Lcom/infraware/filemanager/LiveFolderActivity;->RECENT_FILE_CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-static {p0, v4, v2, v5}, Lcom/infraware/filemanager/LiveFolderActivity;->createLiveFolder(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/infraware/filemanager/LiveFolderActivity;->setResult(ILandroid/content/Intent;)V

    .line 31
    .end local v2    # "title":Ljava/lang/String;
    :cond_0
    invoke-static {}, Lcom/infraware/common/util/Utils;->isL()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 32
    const v3, 0x103012c

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/LiveFolderActivity;->setTheme(I)V

    .line 34
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/filemanager/LiveFolderActivity;->finish()V

    .line 35
    return-void
.end method
