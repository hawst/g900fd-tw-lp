.class Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler$1;
.super Ljava/lang/Object;
.source "FileInfoActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;

.field final synthetic val$localItem:Lcom/infraware/filemanager/file/FileListItem;


# direct methods
.method constructor <init>(Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;Lcom/infraware/filemanager/file/FileListItem;)V
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler$1;->this$1:Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;

    iput-object p2, p0, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler$1;->val$localItem:Lcom/infraware/filemanager/file/FileListItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 225
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler$1;->val$localItem:Lcom/infraware/filemanager/file/FileListItem;

    invoke-virtual {v1}, Lcom/infraware/filemanager/file/FileListItem;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 226
    .local v0, "folder":Ljava/io/File;
    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler$1;->this$1:Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;

    iget-object v1, v1, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;->this$0:Lcom/infraware/filemanager/FileInfoActivity;

    # invokes: Lcom/infraware/filemanager/FileInfoActivity;->getLocalSubfolders(Ljava/io/File;)V
    invoke-static {v1, v0}, Lcom/infraware/filemanager/FileInfoActivity;->access$700(Lcom/infraware/filemanager/FileInfoActivity;Ljava/io/File;)V

    .line 227
    iget-object v1, p0, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler$1;->this$1:Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;

    iget-object v1, v1, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;->this$0:Lcom/infraware/filemanager/FileInfoActivity;

    # getter for: Lcom/infraware/filemanager/FileInfoActivity;->m_oInfoHandler:Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;
    invoke-static {v1}, Lcom/infraware/filemanager/FileInfoActivity;->access$800(Lcom/infraware/filemanager/FileInfoActivity;)Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/infraware/filemanager/FileInfoActivity$ContentsHandler;->sendEmptyMessage(I)Z

    .line 228
    return-void
.end method
