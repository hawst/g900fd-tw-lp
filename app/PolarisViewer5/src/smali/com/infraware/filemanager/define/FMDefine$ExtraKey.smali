.class public Lcom/infraware/filemanager/define/FMDefine$ExtraKey;
.super Ljava/lang/Object;
.source "FMDefine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/filemanager/define/FMDefine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ExtraKey"
.end annotation


# static fields
.field public static final CURRENT_FILE:Ljava/lang/String; = "key_current_file"

.field public static final CURRENT_PATH:Ljava/lang/String; = "key_current_folder"

.field public static final FILE_COUNT:Ljava/lang/String; = "key_file_count"

.field public static final FILE_ITEM:Ljava/lang/String; = "key_file_item"

.field public static final INTERNAL_MODE:Ljava/lang/String; = "key_interanl_mode"

.field public static final LOGIN_ID:Ljava/lang/String; = "key_signup_id"

.field public static final LOGIN_PASSWORD:Ljava/lang/String; = "key_login_password"

.field public static final NEW_FOLDER:Ljava/lang/String; = "key_new_folder"

.field public static final RENAME_FOLDER:Ljava/lang/String; = "key_rename_folder"

.field public static final SELECT_FILES:Ljava/lang/String; = "key_select_files"

.field public static final SERVICE_ID:Ljava/lang/String; = "key_service_id"

.field public static final SERVICE_TYPE:Ljava/lang/String; = "key_service_type"

.field public static final STORAGE_TYPE:Ljava/lang/String; = "key_storage_type"


# instance fields
.field final synthetic this$0:Lcom/infraware/filemanager/define/FMDefine;


# direct methods
.method public constructor <init>(Lcom/infraware/filemanager/define/FMDefine;)V
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lcom/infraware/filemanager/define/FMDefine$ExtraKey;->this$0:Lcom/infraware/filemanager/define/FMDefine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
