.class Lcom/infraware/filemanager/file/FileBaseActivity$8;
.super Ljava/lang/Object;
.source "FileBaseActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/filemanager/file/FileBaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/filemanager/file/FileBaseActivity;


# direct methods
.method constructor <init>(Lcom/infraware/filemanager/file/FileBaseActivity;)V
    .locals 0

    .prologue
    .line 1239
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$8;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1246
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    move-object v0, p1

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$8;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v1, v1, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    if-ne v0, v1, :cond_0

    .line 1248
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$8;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v0, v0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ibScrollButton:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1249
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$8;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    move-object v1, p1

    check-cast v1, Landroid/widget/ListView;

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/filemanager/file/FileBaseActivity;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 1270
    :cond_0
    return-void
.end method
