.class public Lcom/infraware/filemanager/file/FileListFilter;
.super Ljava/lang/Object;
.source "FileListFilter.java"

# interfaces
.implements Ljava/io/FilenameFilter;


# instance fields
.field private m_nFilterType:I

.field private m_oContext:Landroid/content/Context;

.field private m_strPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListFilter;->m_strPath:Ljava/lang/String;

    .line 19
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileListFilter;->m_oContext:Landroid/content/Context;

    .line 20
    iput p2, p0, Lcom/infraware/filemanager/file/FileListFilter;->m_nFilterType:I

    .line 21
    return-void
.end method


# virtual methods
.method public accept(Ljava/io/File;Ljava/lang/String;)Z
    .locals 9
    .param p1, "dir"    # Ljava/io/File;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v8, -0x1

    .line 26
    invoke-virtual {p2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v6

    const/16 v7, 0x2e

    if-ne v6, v7, :cond_0

    .line 71
    :goto_0
    return v5

    .line 29
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/infraware/filemanager/file/FileListFilter;->m_strPath:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 30
    .local v4, "strPathFileName":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 32
    .local v1, "file":Ljava/io/File;
    const/4 v3, 0x0

    .line 33
    .local v3, "result":Z
    const-string/jumbo v6, "."

    invoke-virtual {p2, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    .line 34
    .local v2, "index":I
    const/4 v0, 0x0

    .line 35
    .local v0, "ext":Ljava/lang/String;
    iget v6, p0, Lcom/infraware/filemanager/file/FileListFilter;->m_nFilterType:I

    packed-switch v6, :pswitch_data_0

    :cond_1
    :goto_1
    move v5, v3

    .line 71
    goto :goto_0

    .line 38
    :pswitch_0
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v3

    .line 39
    goto :goto_1

    .line 41
    :pswitch_1
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_2

    const/4 v3, 0x1

    .line 42
    :goto_2
    goto :goto_1

    :cond_2
    move v3, v5

    .line 41
    goto :goto_2

    .line 46
    :pswitch_2
    if-eq v2, v8, :cond_3

    .line 47
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v6, v2, 0x1

    if-eq v5, v6, :cond_3

    .line 48
    add-int/lit8 v5, v2, 0x1

    invoke-virtual {p2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 52
    :cond_3
    iget-object v5, p0, Lcom/infraware/filemanager/file/FileListFilter;->m_oContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/infraware/filemanager/manager/IconManager;->getInstance(Landroid/content/Context;)Lcom/infraware/filemanager/manager/IconManager;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/infraware/filemanager/manager/IconManager;->isImageExt(Ljava/lang/String;)Z

    move-result v3

    .line 53
    goto :goto_1

    .line 55
    :pswitch_3
    if-eq v2, v8, :cond_4

    .line 56
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v6, v2, 0x1

    if-eq v5, v6, :cond_4

    .line 57
    add-int/lit8 v5, v2, 0x1

    invoke-virtual {p2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 59
    :cond_4
    if-eqz v0, :cond_1

    .line 60
    invoke-static {v0}, Lcom/infraware/common/util/FileUtils;->isSupported(Ljava/lang/String;)Z

    move-result v3

    goto :goto_1

    .line 63
    :pswitch_4
    if-eq v2, v8, :cond_5

    .line 64
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v6, v2, 0x1

    if-eq v5, v6, :cond_5

    .line 65
    add-int/lit8 v5, v2, 0x1

    invoke-virtual {p2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 67
    :cond_5
    if-eqz v0, :cond_1

    .line 68
    invoke-static {v0}, Lcom/infraware/common/util/FileUtils;->isSupportedBroadcast(Ljava/lang/String;)Z

    move-result v3

    goto :goto_1

    .line 35
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileListFilter;->m_strPath:Ljava/lang/String;

    .line 77
    return-void
.end method
