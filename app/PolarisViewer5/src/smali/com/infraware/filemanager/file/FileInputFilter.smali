.class public Lcom/infraware/filemanager/file/FileInputFilter;
.super Ljava/lang/Object;
.source "FileInputFilter.java"


# instance fields
.field private context:Landroid/content/Context;

.field private filters:[Landroid/text/InputFilter;

.field private m_ToastMsg:Landroid/widget/Toast;

.field private m_bEditFolder:Z

.field private m_nMaxLength:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/text/InputFilter;

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileInputFilter;->filters:[Landroid/text/InputFilter;

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileInputFilter;->m_ToastMsg:Landroid/widget/Toast;

    .line 16
    iput-boolean v2, p0, Lcom/infraware/filemanager/file/FileInputFilter;->m_bEditFolder:Z

    .line 17
    sget v0, Lcom/infraware/filemanager/define/FMDefine;->MAX_FILENAME_LENGTH:I

    iput v0, p0, Lcom/infraware/filemanager/file/FileInputFilter;->m_nMaxLength:I

    .line 35
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileInputFilter;->context:Landroid/content/Context;

    .line 37
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileInputFilter;->filters:[Landroid/text/InputFilter;

    new-instance v1, Lcom/infraware/filemanager/file/FileInputFilter$1;

    invoke-direct {v1, p0}, Lcom/infraware/filemanager/file/FileInputFilter$1;-><init>(Lcom/infraware/filemanager/file/FileInputFilter;)V

    aput-object v1, v0, v2

    .line 104
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileInputFilter;->filters:[Landroid/text/InputFilter;

    const/4 v1, 0x1

    new-instance v2, Lcom/infraware/filemanager/file/FileInputFilter$2;

    invoke-direct {v2, p0}, Lcom/infraware/filemanager/file/FileInputFilter$2;-><init>(Lcom/infraware/filemanager/file/FileInputFilter;)V

    aput-object v2, v0, v1

    .line 120
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/filemanager/file/FileInputFilter;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileInputFilter;

    .prologue
    .line 12
    iget v0, p0, Lcom/infraware/filemanager/file/FileInputFilter;->m_nMaxLength:I

    return v0
.end method

.method static synthetic access$100(Lcom/infraware/filemanager/file/FileInputFilter;C)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileInputFilter;
    .param p1, "x1"    # C

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/infraware/filemanager/file/FileInputFilter;->isValidChar(C)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/infraware/filemanager/file/FileInputFilter;I)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileInputFilter;
    .param p1, "x1"    # I

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/infraware/filemanager/file/FileInputFilter;->onToastMessage(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/infraware/filemanager/file/FileInputFilter;C)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileInputFilter;
    .param p1, "x1"    # C

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/infraware/filemanager/file/FileInputFilter;->isCarriageReturn(C)Z

    move-result v0

    return v0
.end method

.method private isCarriageReturn(C)Z
    .locals 1
    .param p1, "ch"    # C

    .prologue
    .line 183
    const/16 v0, 0xa

    if-eq p1, v0, :cond_0

    const/16 v0, 0xd

    if-ne p1, v0, :cond_1

    .line 185
    :cond_0
    const/4 v0, 0x1

    .line 187
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isValidChar(C)Z
    .locals 1
    .param p1, "ch"    # C

    .prologue
    .line 150
    const/16 v0, 0x5c

    if-eq p1, v0, :cond_0

    const/16 v0, 0x2f

    if-eq p1, v0, :cond_0

    const/16 v0, 0x3a

    if-eq p1, v0, :cond_0

    const/16 v0, 0x2a

    if-eq p1, v0, :cond_0

    const/16 v0, 0x3f

    if-eq p1, v0, :cond_0

    const/16 v0, 0x3c

    if-eq p1, v0, :cond_0

    const/16 v0, 0x3e

    if-eq p1, v0, :cond_0

    const/16 v0, 0x7c

    if-eq p1, v0, :cond_0

    const/16 v0, 0x22

    if-eq p1, v0, :cond_0

    const/16 v0, 0x263a

    if-ne p1, v0, :cond_1

    .line 155
    :cond_0
    const/4 v0, 0x0

    .line 178
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private onToastMessage(I)V
    .locals 6
    .param p1, "err"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 124
    const/16 v2, -0xa

    if-ne p1, v2, :cond_1

    .line 126
    iget-boolean v2, p0, Lcom/infraware/filemanager/file/FileInputFilter;->m_bEditFolder:Z

    if-eqz v2, :cond_0

    .line 128
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileInputFilter;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07025f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 129
    .local v1, "formattedString":Ljava/lang/String;
    new-array v2, v5, [Ljava/lang/Object;

    iget v3, p0, Lcom/infraware/filemanager/file/FileInputFilter;->m_nMaxLength:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 140
    .end local v1    # "formattedString":Ljava/lang/String;
    .local v0, "errMsg":Ljava/lang/String;
    :goto_0
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileInputFilter;->m_ToastMsg:Landroid/widget/Toast;

    if-nez v2, :cond_2

    .line 141
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileInputFilter;->context:Landroid/content/Context;

    invoke-static {v2, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/filemanager/file/FileInputFilter;->m_ToastMsg:Landroid/widget/Toast;

    .line 145
    :goto_1
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileInputFilter;->m_ToastMsg:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 146
    return-void

    .line 133
    .end local v0    # "errMsg":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileInputFilter;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07025c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 134
    .restart local v1    # "formattedString":Ljava/lang/String;
    new-array v2, v5, [Ljava/lang/Object;

    iget v3, p0, Lcom/infraware/filemanager/file/FileInputFilter;->m_nMaxLength:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 135
    .restart local v0    # "errMsg":Ljava/lang/String;
    goto :goto_0

    .line 138
    .end local v0    # "errMsg":Ljava/lang/String;
    .end local v1    # "formattedString":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileInputFilter;->context:Landroid/content/Context;

    invoke-static {v2, p1}, Lcom/infraware/filemanager/porting/FileError;->getErrorMessage(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "errMsg":Ljava/lang/String;
    goto :goto_0

    .line 143
    :cond_2
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileInputFilter;->m_ToastMsg:Landroid/widget/Toast;

    invoke-virtual {v2, v0}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method public getFilters()[Landroid/text/InputFilter;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileInputFilter;->filters:[Landroid/text/InputFilter;

    return-object v0
.end method

.method public setEditFolder(Z)V
    .locals 0
    .param p1, "isFolder"    # Z

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/infraware/filemanager/file/FileInputFilter;->m_bEditFolder:Z

    .line 32
    return-void
.end method

.method public setMaxLength(I)V
    .locals 0
    .param p1, "maxLength"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/infraware/filemanager/file/FileInputFilter;->m_nMaxLength:I

    .line 27
    return-void
.end method
