.class Lcom/infraware/filemanager/file/FileBaseActivity$11;
.super Ljava/lang/Object;
.source "FileBaseActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/filemanager/file/FileBaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/filemanager/file/FileBaseActivity;


# direct methods
.method constructor <init>(Lcom/infraware/filemanager/file/FileBaseActivity;)V
    .locals 0

    .prologue
    .line 1386
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$11;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    .line 1390
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v0, v1

    .line 1392
    .local v0, "y":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1415
    :goto_0
    return v3

    .line 1395
    :pswitch_0
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/view/View;->setSelected(Z)V

    .line 1396
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$11;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # setter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_nTouchCoordY:I
    invoke-static {v1, v0}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$1302(Lcom/infraware/filemanager/file/FileBaseActivity;I)I

    goto :goto_0

    .line 1400
    :pswitch_1
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$11;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$11;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_nTouchCoordY:I
    invoke-static {v2}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$1300(Lcom/infraware/filemanager/file/FileBaseActivity;)I

    move-result v2

    sub-int v2, v0, v2

    # += operator for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_nScrollCoordY:I
    invoke-static {v1, v2}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$1412(Lcom/infraware/filemanager/file/FileBaseActivity;I)I

    .line 1401
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$11;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # invokes: Lcom/infraware/filemanager/file/FileBaseActivity;->listScroll()V
    invoke-static {v1}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$1500(Lcom/infraware/filemanager/file/FileBaseActivity;)V

    goto :goto_0

    .line 1404
    :pswitch_2
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$11;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_oIndexHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$1600(Lcom/infraware/filemanager/file/FileBaseActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1405
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$11;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # invokes: Lcom/infraware/filemanager/file/FileBaseActivity;->UpdateScrollTimer()V
    invoke-static {v1}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$1700(Lcom/infraware/filemanager/file/FileBaseActivity;)V

    .line 1406
    invoke-virtual {p1, v3}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    .line 1409
    :pswitch_3
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$11;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_oIndexHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$1600(Lcom/infraware/filemanager/file/FileBaseActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1410
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$11;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # invokes: Lcom/infraware/filemanager/file/FileBaseActivity;->UpdateScrollTimer()V
    invoke-static {v1}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$1700(Lcom/infraware/filemanager/file/FileBaseActivity;)V

    .line 1411
    invoke-virtual {p1, v3}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    .line 1392
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
