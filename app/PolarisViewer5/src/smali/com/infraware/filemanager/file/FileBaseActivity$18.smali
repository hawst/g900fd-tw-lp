.class Lcom/infraware/filemanager/file/FileBaseActivity$18;
.super Ljava/lang/Object;
.source "FileBaseActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/filemanager/file/FileBaseActivity;->isFindByPositionAction(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

.field final synthetic val$x:I

.field final synthetic val$y:I


# direct methods
.method constructor <init>(Lcom/infraware/filemanager/file/FileBaseActivity;II)V
    .locals 0

    .prologue
    .line 3639
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$18;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iput p2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$18;->val$x:I

    iput p3, p0, Lcom/infraware/filemanager/file/FileBaseActivity$18;->val$y:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 3643
    :try_start_0
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$18;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity$18;->val$x:I

    iget v4, p0, Lcom/infraware/filemanager/file/FileBaseActivity$18;->val$y:I

    invoke-virtual {v2, v3, v4}, Lcom/infraware/filemanager/file/FileBaseActivity;->getFileByPosition(II)Ljava/lang/String;

    move-result-object v0

    .line 3644
    .local v0, "fileName":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->SEND_INTENT_FIND_BY_POS()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3645
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "filepath"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3646
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$18;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    invoke-virtual {v2, v1}, Lcom/infraware/filemanager/file/FileBaseActivity;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3650
    .end local v0    # "fileName":Ljava/lang/String;
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 3648
    :catch_0
    move-exception v2

    goto :goto_0
.end method
