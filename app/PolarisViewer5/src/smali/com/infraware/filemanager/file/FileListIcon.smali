.class public Lcom/infraware/filemanager/file/FileListIcon;
.super Ljava/lang/Object;
.source "FileListIcon.java"


# static fields
.field public static final m_bUseExternalHTML:Z = true


# instance fields
.field public Folder:Landroid/graphics/Bitmap;

.field public Parent:Landroid/graphics/Bitmap;

.field context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileListIcon;->context:Landroid/content/Context;

    .line 25
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 27
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0200ba

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/filemanager/file/FileListIcon;->Parent:Landroid/graphics/Bitmap;

    .line 28
    const v1, 0x7f0200b9

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/filemanager/file/FileListIcon;->Folder:Landroid/graphics/Bitmap;

    .line 34
    return-void
.end method

.method private getIconByFileExt(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "aFileExt"    # Ljava/lang/String;

    .prologue
    .line 48
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListIcon;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/infraware/filemanager/manager/IconManager;->getInstance(Landroid/content/Context;)Lcom/infraware/filemanager/manager/IconManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/infraware/filemanager/manager/IconManager;->getBmpByExt(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 49
    .local v0, "icon":Landroid/graphics/Bitmap;
    return-object v0
.end method


# virtual methods
.method public getDRMFileIcon(Lcom/infraware/filemanager/porting/DRM;Lcom/infraware/filemanager/file/FileListItem;)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "drm"    # Lcom/infraware/filemanager/porting/DRM;
    .param p2, "item"    # Lcom/infraware/filemanager/file/FileListItem;

    .prologue
    .line 73
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p2, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 74
    .local v1, "strFileName":Ljava/lang/String;
    invoke-virtual {p1, v1}, Lcom/infraware/filemanager/porting/DRM;->getContentType(Ljava/lang/String;)I

    move-result v0

    .line 76
    .local v0, "mimeType":I
    packed-switch v0, :pswitch_data_0

    .line 86
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileListIcon;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/infraware/filemanager/manager/IconManager;->getInstance(Landroid/content/Context;)Lcom/infraware/filemanager/manager/IconManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/infraware/filemanager/manager/IconManager;->getBmpByExt(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    :goto_0
    return-object v2

    .line 79
    :pswitch_0
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileListIcon;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200bd

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_0

    .line 81
    :pswitch_1
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileListIcon;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200c4

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_0

    .line 83
    :pswitch_2
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileListIcon;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200be

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_0

    .line 76
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getDRMFileType(Lcom/infraware/filemanager/porting/DRM;Lcom/infraware/filemanager/file/FileListItem;)I
    .locals 3
    .param p1, "drm"    # Lcom/infraware/filemanager/porting/DRM;
    .param p2, "item"    # Lcom/infraware/filemanager/file/FileListItem;

    .prologue
    .line 54
    const v1, 0x7f07028e

    .line 55
    .local v1, "fileType":I
    invoke-virtual {p2}, Lcom/infraware/filemanager/file/FileListItem;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/infraware/filemanager/porting/DRM;->getContentType(Ljava/lang/String;)I

    move-result v0

    .line 56
    .local v0, "drmType":I
    packed-switch v0, :pswitch_data_0

    .line 68
    :goto_0
    return v1

    .line 58
    :pswitch_0
    const v1, 0x7f07028b

    .line 59
    goto :goto_0

    .line 61
    :pswitch_1
    const v1, 0x7f070286

    .line 62
    goto :goto_0

    .line 64
    :pswitch_2
    const v1, 0x7f070291

    goto :goto_0

    .line 56
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getFileIcon(Lcom/infraware/filemanager/file/FileListItem;)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "item"    # Lcom/infraware/filemanager/file/FileListItem;

    .prologue
    .line 106
    const/4 v1, 0x0

    .line 108
    .local v1, "icon":Landroid/graphics/Bitmap;
    iget-object v0, p1, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    .line 109
    .local v0, "ext":Ljava/lang/String;
    iget-object v2, p1, Lcom/infraware/filemanager/file/FileListItem;->webExt:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/infraware/filemanager/file/FileListItem;->webExt:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    .line 110
    iget-object v0, p1, Lcom/infraware/filemanager/file/FileListItem;->webExt:Ljava/lang/String;

    .line 112
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 113
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/infraware/filemanager/file/FileListIcon;->getIconByFileExt(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 117
    :goto_0
    return-object v1

    .line 115
    :cond_1
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileListIcon;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/infraware/filemanager/manager/IconManager;->getInstance(Landroid/content/Context;)Lcom/infraware/filemanager/manager/IconManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/infraware/filemanager/manager/IconManager;->getBmpByExt(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0
.end method

.method public getFileIcon(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 122
    const/4 v1, 0x0

    .line 123
    .local v1, "icon":Landroid/graphics/Bitmap;
    const/16 v4, 0x2e

    invoke-virtual {p1, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 124
    .local v2, "idx_ext":I
    if-gez v2, :cond_0

    .line 133
    :goto_0
    return-object v3

    .line 127
    :cond_0
    add-int/lit8 v4, v2, 0x1

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 128
    .local v0, "ext":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_1

    .line 129
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/infraware/filemanager/file/FileListIcon;->getIconByFileExt(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    :goto_1
    move-object v3, v1

    .line 133
    goto :goto_0

    .line 131
    :cond_1
    iget-object v4, p0, Lcom/infraware/filemanager/file/FileListIcon;->context:Landroid/content/Context;

    invoke-static {v4}, Lcom/infraware/filemanager/manager/IconManager;->getInstance(Landroid/content/Context;)Lcom/infraware/filemanager/manager/IconManager;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/infraware/filemanager/manager/IconManager;->getBmpByExt(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_1
.end method

.method public getFolderIcon(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "folderName"    # Ljava/lang/String;

    .prologue
    .line 138
    const/4 v0, 0x0

    .line 139
    .local v0, "icon":Landroid/graphics/Bitmap;
    const-string/jumbo v1, ".."

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 140
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileListIcon;->Parent:Landroid/graphics/Bitmap;

    .line 144
    :goto_0
    return-object v0

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileListIcon;->Folder:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public getResByFileExt(Ljava/lang/String;)I
    .locals 2
    .param p1, "aFileExt"    # Ljava/lang/String;

    .prologue
    .line 37
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListIcon;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/infraware/filemanager/manager/IconManager;->getInstance(Landroid/content/Context;)Lcom/infraware/filemanager/manager/IconManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/infraware/filemanager/manager/IconManager;->getResIdByExt(Ljava/lang/String;)I

    move-result v0

    .line 38
    .local v0, "resId":I
    return v0
.end method

.method public getTypeByFileExt(Ljava/lang/String;)I
    .locals 2
    .param p1, "aFileExt"    # Ljava/lang/String;

    .prologue
    .line 42
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListIcon;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/infraware/filemanager/manager/IconManager;->getInstance(Landroid/content/Context;)Lcom/infraware/filemanager/manager/IconManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/infraware/filemanager/manager/IconManager;->getTypeByExt(Ljava/lang/String;)I

    move-result v0

    .line 43
    .local v0, "resId":I
    return v0
.end method
