.class public Lcom/infraware/filemanager/file/FileBaseActivity;
.super Landroid/app/Activity;
.source "FileBaseActivity.java"

# interfaces
.implements Lcom/infraware/common/event/FindByPositionListener;


# static fields
.field private static final INDEX_STATE_DRAW:I = 0x1

.field private static final INDEX_STATE_HIDE:I = 0x0

.field private static final UPDATE_PROGRESS_COLOR:I = 0x1

.field private static final UPDATE_PROGRESS_END:I = 0x2

.field private static final UPDATE_PROGRESS_START:I

.field protected static m_bIsUpdated:Z


# instance fields
.field private LOCAL_ROOT_PATH:Ljava/lang/String;

.field private m_bCancelSelect:Z

.field private m_bDestroy:Z

.field private m_bShowHidden:Z

.field private m_bShowPath:Z

.field private m_bUpdatable:Z

.field protected m_btnTitle:Landroid/widget/Button;

.field protected m_btnTitleMenuNewFolder:Landroid/widget/ImageButton;

.field protected m_btnTitleMenuSave:Landroid/widget/ImageButton;

.field protected m_ibScrollButton:Landroid/widget/ImageButton;

.field protected m_ivEmptyImage:Landroid/widget/ImageView;

.field protected m_ivIndexIcon:Landroid/widget/ImageView;

.field protected m_ivTitle:Landroid/widget/ImageView;

.field protected m_layoutEmpty:Landroid/widget/LinearLayout;

.field protected m_layoutFileList:Landroid/widget/FrameLayout;

.field protected m_layoutIndex:Landroid/widget/FrameLayout;

.field protected m_layoutInfo:Landroid/widget/LinearLayout;

.field protected m_layoutProgress:Landroid/widget/FrameLayout;

.field protected m_layoutTitle:Landroid/widget/LinearLayout;

.field protected m_layoutTitleBar:Landroid/widget/LinearLayout;

.field protected m_lvFileList:Landroid/widget/ListView;

.field protected m_nButtonId:I

.field protected m_nCurDepth:I

.field protected m_nCurUpdate:J

.field protected m_nDownKey:I

.field protected m_nEmptyImageId:I

.field protected m_nEmptyTextId:I

.field private m_nFastScrollCount:I

.field protected m_nFilterType:I

.field protected m_nInfoId:I

.field protected m_nInternalMode:I

.field private m_nItemPosition:I

.field protected m_nLocaleCode:I

.field protected m_nOrientation:I

.field private m_nScrollCoordY:I

.field private m_nSelectMode:I

.field protected m_nServiceType:I

.field protected m_nStorageType:I

.field protected m_nTitleId:I

.field protected m_nTmpDepth:I

.field private m_nTouchCoordY:I

.field private m_nUpdateColor:I

.field private m_nUpdateState:I

.field protected m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

.field protected m_oFileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/filemanager/file/FileListItem;",
            ">;"
        }
    .end annotation
.end field

.field private m_oFindReceiver:Lcom/infraware/common/event/FindByPositionEvent;

.field protected m_oFolderFilter:Lcom/infraware/filemanager/file/FileListFilter;

.field protected m_oHandler:Landroid/os/Handler;

.field private m_oIndexHandler:Landroid/os/Handler;

.field protected m_oItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field protected m_oMapItemIdx:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected m_oMapItemTop:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private m_oMapSDCardTable:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private m_oProgressThread:Ljava/lang/Thread;

.field protected m_oSDCardList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/filemanager/file/FileListItem;",
            ">;"
        }
    .end annotation
.end field

.field private m_oScanProgress:Landroid/app/ProgressDialog;

.field private m_oScrollHandler:Landroid/os/Handler;

.field protected m_oScrollListener:Landroid/widget/AbsListView$OnScrollListener;

.field private m_oScrollTask:Ljava/lang/Runnable;

.field private m_oSelectProgress:Landroid/app/ProgressDialog;

.field protected m_oSelectedItem:Lcom/infraware/filemanager/file/FileListItem;

.field private m_oToastMsg:Landroid/widget/Toast;

.field private m_oTouchListener:Landroid/view/View$OnTouchListener;

.field private m_oUpdateProgress:Landroid/app/ProgressDialog;

.field private m_oUpdateThread:Ljava/lang/Thread;

.field protected m_runInitState:Ljava/lang/Runnable;

.field protected m_runPostScan:Ljava/lang/Runnable;

.field protected m_runPostSelect:Ljava/lang/Runnable;

.field protected m_runPostUpdate:Ljava/lang/Runnable;

.field protected m_runToastMsg:Ljava/lang/Runnable;

.field protected m_runUpdateList:Ljava/lang/Runnable;

.field protected m_runUpdateProgress:Ljava/lang/Runnable;

.field protected m_strCurFile:Ljava/lang/String;

.field protected m_strCurFileId:Ljava/lang/String;

.field protected m_strCurPath:Ljava/lang/String;

.field protected m_strRootPath:Ljava/lang/String;

.field protected m_strStorageId:Ljava/lang/String;

.field protected m_strStoragePath:Ljava/lang/String;

.field protected m_strStorageToken1:Ljava/lang/String;

.field protected m_strStorageToken2:Ljava/lang/String;

.field protected m_strTmpPath:Ljava/lang/String;

.field private m_strToastMsg:Ljava/lang/String;

.field protected m_tvEmptyText:Landroid/widget/TextView;

.field protected m_tvIndexFile:Landroid/widget/TextView;

.field protected m_tvIndexFolder:Landroid/widget/TextView;

.field protected m_tvIndexType:Landroid/widget/TextView;

.field protected m_tvInfo:Landroid/widget/TextView;

.field protected m_tvTitle:Landroid/widget/TextView;

.field private tag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    sput-boolean v0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_bIsUpdated:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 54
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 56
    const-string/jumbo v0, "FileBaseActivity"

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->tag:Ljava/lang/String;

    .line 65
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->LOCAL_ROOT_PATH:Ljava/lang/String;

    .line 70
    iput-boolean v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_bShowPath:Z

    .line 71
    iput-boolean v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_bShowHidden:Z

    .line 75
    iput-boolean v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_bDestroy:Z

    .line 77
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->MIN_FAST_SCROLL_COUNT()I

    move-result v0

    iput v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nFastScrollCount:I

    .line 79
    iput v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nSelectMode:I

    .line 80
    iput v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nUpdateColor:I

    .line 81
    iput v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nUpdateState:I

    .line 83
    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oUpdateProgress:Landroid/app/ProgressDialog;

    .line 84
    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSelectProgress:Landroid/app/ProgressDialog;

    .line 85
    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oScanProgress:Landroid/app/ProgressDialog;

    .line 89
    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oUpdateThread:Ljava/lang/Thread;

    .line 90
    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oProgressThread:Ljava/lang/Thread;

    .line 92
    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oToastMsg:Landroid/widget/Toast;

    .line 93
    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strToastMsg:Ljava/lang/String;

    .line 95
    iput-boolean v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_bCancelSelect:Z

    .line 96
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_bUpdatable:Z

    .line 101
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oMapSDCardTable:Ljava/util/HashMap;

    .line 106
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nCurUpdate:J

    .line 107
    iput v4, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nDownKey:I

    .line 108
    iput v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nStorageType:I

    .line 109
    iput v4, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nServiceType:I

    .line 115
    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSelectedItem:Lcom/infraware/filemanager/file/FileListItem;

    .line 129
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strCurFileId:Ljava/lang/String;

    .line 132
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oMapItemIdx:Ljava/util/HashMap;

    .line 133
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oMapItemTop:Ljava/util/HashMap;

    .line 135
    iput v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nTitleId:I

    .line 136
    iput v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nInfoId:I

    .line 137
    iput v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nButtonId:I

    .line 139
    iput v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nInternalMode:I

    .line 140
    iput v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nFilterType:I

    .line 141
    iput v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nLocaleCode:I

    .line 142
    iput v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nOrientation:I

    .line 144
    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    .line 147
    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFolderFilter:Lcom/infraware/filemanager/file/FileListFilter;

    .line 150
    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    .line 153
    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileList:Ljava/util/ArrayList;

    .line 154
    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSDCardList:Ljava/util/ArrayList;

    .line 181
    iput v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nEmptyImageId:I

    .line 182
    iput v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nEmptyTextId:I

    .line 232
    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFindReceiver:Lcom/infraware/common/event/FindByPositionEvent;

    .line 1080
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oHandler:Landroid/os/Handler;

    .line 1082
    new-instance v0, Lcom/infraware/filemanager/file/FileBaseActivity$1;

    invoke-direct {v0, p0}, Lcom/infraware/filemanager/file/FileBaseActivity$1;-><init>(Lcom/infraware/filemanager/file/FileBaseActivity;)V

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_runUpdateProgress:Ljava/lang/Runnable;

    .line 1139
    new-instance v0, Lcom/infraware/filemanager/file/FileBaseActivity$2;

    invoke-direct {v0, p0}, Lcom/infraware/filemanager/file/FileBaseActivity$2;-><init>(Lcom/infraware/filemanager/file/FileBaseActivity;)V

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_runUpdateList:Ljava/lang/Runnable;

    .line 1166
    new-instance v0, Lcom/infraware/filemanager/file/FileBaseActivity$3;

    invoke-direct {v0, p0}, Lcom/infraware/filemanager/file/FileBaseActivity$3;-><init>(Lcom/infraware/filemanager/file/FileBaseActivity;)V

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_runPostScan:Ljava/lang/Runnable;

    .line 1178
    new-instance v0, Lcom/infraware/filemanager/file/FileBaseActivity$4;

    invoke-direct {v0, p0}, Lcom/infraware/filemanager/file/FileBaseActivity$4;-><init>(Lcom/infraware/filemanager/file/FileBaseActivity;)V

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_runPostUpdate:Ljava/lang/Runnable;

    .line 1195
    new-instance v0, Lcom/infraware/filemanager/file/FileBaseActivity$5;

    invoke-direct {v0, p0}, Lcom/infraware/filemanager/file/FileBaseActivity$5;-><init>(Lcom/infraware/filemanager/file/FileBaseActivity;)V

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_runPostSelect:Ljava/lang/Runnable;

    .line 1217
    new-instance v0, Lcom/infraware/filemanager/file/FileBaseActivity$6;

    invoke-direct {v0, p0}, Lcom/infraware/filemanager/file/FileBaseActivity$6;-><init>(Lcom/infraware/filemanager/file/FileBaseActivity;)V

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_runInitState:Ljava/lang/Runnable;

    .line 1225
    new-instance v0, Lcom/infraware/filemanager/file/FileBaseActivity$7;

    invoke-direct {v0, p0}, Lcom/infraware/filemanager/file/FileBaseActivity$7;-><init>(Lcom/infraware/filemanager/file/FileBaseActivity;)V

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_runToastMsg:Ljava/lang/Runnable;

    .line 1238
    new-instance v0, Lcom/infraware/filemanager/file/FileBaseActivity$8;

    invoke-direct {v0, p0}, Lcom/infraware/filemanager/file/FileBaseActivity$8;-><init>(Lcom/infraware/filemanager/file/FileBaseActivity;)V

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1277
    new-instance v0, Lcom/infraware/filemanager/file/FileBaseActivity$9;

    invoke-direct {v0, p0}, Lcom/infraware/filemanager/file/FileBaseActivity$9;-><init>(Lcom/infraware/filemanager/file/FileBaseActivity;)V

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    .line 1311
    new-instance v0, Lcom/infraware/filemanager/file/FileBaseActivity$10;

    invoke-direct {v0, p0}, Lcom/infraware/filemanager/file/FileBaseActivity$10;-><init>(Lcom/infraware/filemanager/file/FileBaseActivity;)V

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oIndexHandler:Landroid/os/Handler;

    .line 1385
    new-instance v0, Lcom/infraware/filemanager/file/FileBaseActivity$11;

    invoke-direct {v0, p0}, Lcom/infraware/filemanager/file/FileBaseActivity$11;-><init>(Lcom/infraware/filemanager/file/FileBaseActivity;)V

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oTouchListener:Landroid/view/View$OnTouchListener;

    .line 1487
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oScrollHandler:Landroid/os/Handler;

    .line 1488
    new-instance v0, Lcom/infraware/filemanager/file/FileBaseActivity$12;

    invoke-direct {v0, p0}, Lcom/infraware/filemanager/file/FileBaseActivity$12;-><init>(Lcom/infraware/filemanager/file/FileBaseActivity;)V

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oScrollTask:Ljava/lang/Runnable;

    return-void
.end method

.method private UpdateScrollTimer()V
    .locals 4

    .prologue
    .line 1498
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oScrollHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oScrollTask:Ljava/lang/Runnable;

    if-nez v0, :cond_1

    .line 1503
    :cond_0
    :goto_0
    return-void

    .line 1501
    :cond_1
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oScrollHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oScrollTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1502
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oScrollHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oScrollTask:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/infraware/filemanager/file/FileBaseActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileBaseActivity;

    .prologue
    .line 54
    iget v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nUpdateState:I

    return v0
.end method

.method static synthetic access$100(Lcom/infraware/filemanager/file/FileBaseActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileBaseActivity;

    .prologue
    .line 54
    iget v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nUpdateColor:I

    return v0
.end method

.method static synthetic access$1000(Lcom/infraware/filemanager/file/FileBaseActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileBaseActivity;

    .prologue
    .line 54
    iget v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nFastScrollCount:I

    return v0
.end method

.method static synthetic access$102(Lcom/infraware/filemanager/file/FileBaseActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileBaseActivity;
    .param p1, "x1"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nUpdateColor:I

    return p1
.end method

.method static synthetic access$1100(Lcom/infraware/filemanager/file/FileBaseActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileBaseActivity;

    .prologue
    .line 54
    iget v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nItemPosition:I

    return v0
.end method

.method static synthetic access$112(Lcom/infraware/filemanager/file/FileBaseActivity;I)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileBaseActivity;
    .param p1, "x1"    # I

    .prologue
    .line 54
    iget v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nUpdateColor:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nUpdateColor:I

    return v0
.end method

.method static synthetic access$1200(Lcom/infraware/filemanager/file/FileBaseActivity;II)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileBaseActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/infraware/filemanager/file/FileBaseActivity;->showScrollButton(II)V

    return-void
.end method

.method static synthetic access$1300(Lcom/infraware/filemanager/file/FileBaseActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileBaseActivity;

    .prologue
    .line 54
    iget v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nTouchCoordY:I

    return v0
.end method

.method static synthetic access$1302(Lcom/infraware/filemanager/file/FileBaseActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileBaseActivity;
    .param p1, "x1"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nTouchCoordY:I

    return p1
.end method

.method static synthetic access$1412(Lcom/infraware/filemanager/file/FileBaseActivity;I)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileBaseActivity;
    .param p1, "x1"    # I

    .prologue
    .line 54
    iget v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nScrollCoordY:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nScrollCoordY:I

    return v0
.end method

.method static synthetic access$1500(Lcom/infraware/filemanager/file/FileBaseActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileBaseActivity;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->listScroll()V

    return-void
.end method

.method static synthetic access$1600(Lcom/infraware/filemanager/file/FileBaseActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileBaseActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oIndexHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/infraware/filemanager/file/FileBaseActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileBaseActivity;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->UpdateScrollTimer()V

    return-void
.end method

.method static synthetic access$1800(Lcom/infraware/filemanager/file/FileBaseActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileBaseActivity;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->hideScrollButton()V

    return-void
.end method

.method static synthetic access$200(Lcom/infraware/filemanager/file/FileBaseActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileBaseActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->tag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/filemanager/file/FileBaseActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileBaseActivity;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_bDestroy:Z

    return v0
.end method

.method static synthetic access$400(Lcom/infraware/filemanager/file/FileBaseActivity;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileBaseActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oUpdateProgress:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$402(Lcom/infraware/filemanager/file/FileBaseActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileBaseActivity;
    .param p1, "x1"    # Landroid/app/ProgressDialog;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oUpdateProgress:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$500(Lcom/infraware/filemanager/file/FileBaseActivity;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileBaseActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oScanProgress:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$502(Lcom/infraware/filemanager/file/FileBaseActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileBaseActivity;
    .param p1, "x1"    # Landroid/app/ProgressDialog;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oScanProgress:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$600(Lcom/infraware/filemanager/file/FileBaseActivity;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileBaseActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSelectProgress:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$602(Lcom/infraware/filemanager/file/FileBaseActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileBaseActivity;
    .param p1, "x1"    # Landroid/app/ProgressDialog;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSelectProgress:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$702(Lcom/infraware/filemanager/file/FileBaseActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileBaseActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_bCancelSelect:Z

    return p1
.end method

.method static synthetic access$800(Lcom/infraware/filemanager/file/FileBaseActivity;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileBaseActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oToastMsg:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$802(Lcom/infraware/filemanager/file/FileBaseActivity;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileBaseActivity;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oToastMsg:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$900(Lcom/infraware/filemanager/file/FileBaseActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileBaseActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strToastMsg:Ljava/lang/String;

    return-object v0
.end method

.method private hideScrollButton()V
    .locals 2

    .prologue
    .line 1515
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1516
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ibScrollButton:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1517
    :cond_0
    return-void
.end method

.method private listScroll()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1456
    iget v5, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nScrollCoordY:I

    iget-object v6, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    invoke-virtual {v6}, Landroid/widget/ListView;->getHeight()I

    move-result v6

    iget-object v7, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ibScrollButton:Landroid/widget/ImageButton;

    invoke-virtual {v7}, Landroid/widget/ImageButton;->getHeight()I

    move-result v7

    sub-int/2addr v6, v7

    if-le v5, v6, :cond_0

    .line 1457
    iget-object v5, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    invoke-virtual {v5}, Landroid/widget/ListView;->getHeight()I

    move-result v5

    iget-object v6, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ibScrollButton:Landroid/widget/ImageButton;

    invoke-virtual {v6}, Landroid/widget/ImageButton;->getHeight()I

    move-result v6

    sub-int/2addr v5, v6

    iput v5, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nScrollCoordY:I

    .line 1459
    :cond_0
    iget v5, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nScrollCoordY:I

    if-gez v5, :cond_1

    .line 1460
    iput v8, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nScrollCoordY:I

    .line 1462
    :cond_1
    iget-object v5, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    invoke-virtual {v5}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v5

    iget-object v6, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    invoke-virtual {v6}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v6

    sub-int/2addr v5, v6

    add-int/lit8 v4, v5, 0x1

    .line 1463
    .local v4, "nVisibleCount":I
    iget-object v5, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    invoke-virtual {v5}, Landroid/widget/ListView;->getCount()I

    move-result v5

    sub-int v2, v5, v4

    .line 1465
    .local v2, "nMaxPos":I
    iget-object v5, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    invoke-virtual {v5}, Landroid/widget/ListView;->getHeight()I

    move-result v5

    iget-object v6, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ibScrollButton:Landroid/widget/ImageButton;

    invoke-virtual {v6}, Landroid/widget/ImageButton;->getHeight()I

    move-result v6

    sub-int v3, v5, v6

    .line 1466
    .local v3, "nRange":I
    iget v5, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nScrollCoordY:I

    mul-int/2addr v5, v2

    div-int/2addr v5, v3

    iput v5, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nItemPosition:I

    .line 1468
    iget v5, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nItemPosition:I

    if-gez v5, :cond_2

    .line 1469
    iput v8, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nItemPosition:I

    .line 1471
    :cond_2
    iget v5, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nItemPosition:I

    if-le v5, v2, :cond_3

    .line 1472
    iput v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nItemPosition:I

    .line 1474
    :cond_3
    iget-object v5, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    iget v6, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nItemPosition:I

    invoke-virtual {v5, v6, v8}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 1476
    iget-object v5, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    iget v6, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nItemPosition:I

    invoke-virtual {v5, v6}, Lcom/infraware/filemanager/file/FileListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/filemanager/file/FileListItem;

    .line 1477
    .local v0, "item":Lcom/infraware/filemanager/file/FileListItem;
    iget-object v5, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oIndexHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oIndexHandler:Landroid/os/Handler;

    const/4 v7, 0x1

    invoke-virtual {v6, v7, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1479
    iget-object v5, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ibScrollButton:Landroid/widget/ImageButton;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1481
    iget-object v5, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ibScrollButton:Landroid/widget/ImageButton;

    invoke-virtual {v5}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 1482
    .local v1, "lp":Landroid/widget/FrameLayout$LayoutParams;
    iget v5, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nScrollCoordY:I

    invoke-virtual {v1, v8, v5, v8, v8}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 1484
    iget-object v5, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ibScrollButton:Landroid/widget/ImageButton;

    invoke-virtual {v5, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1485
    return-void
.end method

.method private moveScrollButton(I)V
    .locals 5
    .param p1, "nMaxPos"    # I

    .prologue
    const/4 v4, 0x0

    .line 1521
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ibScrollButton:Landroid/widget/ImageButton;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1523
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ibScrollButton:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getHeight()I

    move-result v3

    sub-int v1, v2, v3

    .line 1524
    .local v1, "nScrollRangeY":I
    iget v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nItemPosition:I

    mul-int/2addr v2, v1

    div-int/2addr v2, p1

    iput v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nScrollCoordY:I

    .line 1526
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ibScrollButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1527
    .local v0, "lp":Landroid/widget/FrameLayout$LayoutParams;
    iget v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nScrollCoordY:I

    invoke-virtual {v0, v4, v2, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 1529
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ibScrollButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1530
    return-void
.end method

.method private setCurrentPosition()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2785
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oMapItemIdx:Ljava/util/HashMap;

    iget v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nCurDepth:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2786
    .local v0, "idx":Ljava/lang/Integer;
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oMapItemTop:Ljava/util/HashMap;

    iget v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nCurDepth:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 2788
    .local v1, "top":Ljava/lang/Integer;
    if-nez v0, :cond_1

    .line 2790
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    invoke-virtual {v2, v4, v4}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 2802
    :cond_0
    :goto_0
    return-void

    .line 2794
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v3}, Lcom/infraware/filemanager/file/FileListAdapter;->getCount()I

    move-result v3

    if-lt v2, v3, :cond_2

    .line 2795
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v2}, Lcom/infraware/filemanager/file/FileListAdapter;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2797
    :cond_2
    if-nez v1, :cond_3

    .line 2798
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2800
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lez v2, :cond_0

    .line 2801
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    goto :goto_0
.end method

.method private setSDCardConfig()V
    .locals 5

    .prologue
    .line 3418
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v3, 0x1

    if-ge v0, v3, :cond_1

    .line 3420
    sget-object v3, Lcom/infraware/filemanager/porting/DeviceConfig$SDCardList;->SDCARD_FOLDER_NAME:[Ljava/lang/String;

    aget-object v2, v3, v0

    .line 3421
    .local v2, "name":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget-object v4, Lcom/infraware/filemanager/porting/DeviceConfig$SDCardList;->SDCARD_FOLDER_ICON:[I

    aget v4, v4, v0

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 3422
    .local v1, "icon":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 3423
    sget-object v3, Lcom/infraware/filemanager/porting/DeviceConfig$SDCardList;->SDCARD_FOLDER_NAME:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {p0, v3, v1}, Lcom/infraware/filemanager/file/FileBaseActivity;->addSDCardIcon(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 3418
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3425
    .end local v1    # "icon":Landroid/graphics/Bitmap;
    .end local v2    # "name":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private showScrollButton(II)V
    .locals 0
    .param p1, "nFirstItemPos"    # I
    .param p2, "nMaxItemPos"    # I

    .prologue
    .line 1507
    iput p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nItemPosition:I

    .line 1508
    invoke-direct {p0, p2}, Lcom/infraware/filemanager/file/FileBaseActivity;->moveScrollButton(I)V

    .line 1510
    invoke-direct {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->UpdateScrollTimer()V

    .line 1511
    return-void
.end method


# virtual methods
.method public addSDCardIcon(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "icon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 879
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oMapSDCardTable:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 880
    return-void
.end method

.method public clearSDCard()V
    .locals 1

    .prologue
    .line 874
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oMapSDCardTable:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 875
    return-void
.end method

.method public getCurFileId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strCurFileId:Ljava/lang/String;

    return-object v0
.end method

.method public getCurPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strCurPath:Ljava/lang/String;

    return-object v0
.end method

.method protected getFileByPosition(II)Ljava/lang/String;
    .locals 11
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 3589
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->isUpdatable()Z

    move-result v8

    if-nez v8, :cond_0

    .line 3590
    const-string/jumbo v8, ""

    .line 3620
    :goto_0
    return-object v8

    .line 3593
    :cond_0
    const/4 v1, 0x0

    .local v1, "nBottom":I
    move v3, v1

    .local v3, "nRight":I
    move v4, v1

    .local v4, "nTop":I
    move v2, v1

    .line 3595
    .local v2, "nLeft":I
    iget-object v7, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    .line 3596
    .local v7, "view":Landroid/view/View;
    invoke-virtual {v7}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v6

    .line 3597
    .local v6, "root":Landroid/view/View;
    :goto_1
    if-eq v7, v6, :cond_1

    .line 3599
    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    move-result v8

    add-int/2addr v2, v8

    .line 3600
    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v8

    add-int/2addr v4, v8

    .line 3602
    invoke-virtual {v7}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    .end local v7    # "view":Landroid/view/View;
    check-cast v7, Landroid/view/View;

    .restart local v7    # "view":Landroid/view/View;
    goto :goto_1

    .line 3605
    :cond_1
    iget-object v8, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    invoke-virtual {v8}, Landroid/widget/ListView;->getWidth()I

    move-result v8

    add-int/2addr v8, v2

    add-int/lit8 v3, v8, -0x1

    .line 3606
    iget-object v8, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    invoke-virtual {v8}, Landroid/widget/ListView;->getHeight()I

    move-result v8

    add-int/2addr v8, v4

    add-int/lit8 v1, v8, -0x1

    .line 3608
    if-lt p1, v2, :cond_2

    if-le p1, v3, :cond_3

    .line 3609
    :cond_2
    const-string/jumbo v8, ""

    goto :goto_0

    .line 3611
    :cond_3
    if-lt p2, v4, :cond_4

    if-le p2, v1, :cond_5

    .line 3612
    :cond_4
    const-string/jumbo v8, ""

    goto :goto_0

    .line 3614
    :cond_5
    iget-object v8, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    sub-int v9, p1, v2

    sub-int v10, p2, v4

    invoke-virtual {v8, v9, v10}, Landroid/widget/ListView;->pointToPosition(II)I

    move-result v5

    .line 3615
    .local v5, "position":I
    iget-object v8, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v8, v5}, Lcom/infraware/filemanager/file/FileListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/filemanager/file/FileListItem;

    .line 3617
    .local v0, "item":Lcom/infraware/filemanager/file/FileListItem;
    if-nez v0, :cond_6

    .line 3618
    const-string/jumbo v8, ""

    goto :goto_0

    .line 3620
    :cond_6
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, v0, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/infraware/filemanager/file/FileListItem;->getFullFileName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_0
.end method

.method protected getFileItem(I)Lcom/infraware/filemanager/file/FileListItem;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 3308
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    if-nez v0, :cond_0

    .line 3309
    const/4 v0, 0x0

    .line 3311
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v0, p1}, Lcom/infraware/filemanager/file/FileListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/filemanager/file/FileListItem;

    goto :goto_0
.end method

.method public getFindByPositionIntentFilter()Landroid/content/IntentFilter;
    .locals 3

    .prologue
    .line 3625
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->RECV_INTENT_FIND_BY_POS()Ljava/lang/String;

    move-result-object v0

    .line 3626
    .local v0, "filter":Ljava/lang/String;
    const-string/jumbo v2, "0"

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    .line 3627
    const/4 v1, 0x0

    .line 3630
    :goto_0
    return-object v1

    .line 3629
    :cond_0
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 3630
    .local v1, "intentFilter":Landroid/content/IntentFilter;
    goto :goto_0
.end method

.method public getRootPath(I)Ljava/lang/String;
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 846
    const-string/jumbo v0, ""

    .line 847
    .local v0, "path":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 857
    :goto_0
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strRootPath:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strRootPath:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 858
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strRootPath:Ljava/lang/String;

    .line 860
    :cond_0
    return-object v0

    .line 850
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->LOCAL_ROOT_PATH:Ljava/lang/String;

    goto :goto_0

    .line 847
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public getSDCardIcon(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 884
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oMapSDCardTable:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getSDCardName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 3234
    iget v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nStorageType:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    invoke-static {}, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->useExternalSD()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSDCardList:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 3236
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 3238
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/filemanager/file/FileListItem;

    .line 3239
    .local v1, "item":Lcom/infraware/filemanager/file/FileListItem;
    iget-object v2, v1, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3240
    iget-object v2, v1, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    .line 3244
    .end local v0    # "i":I
    .end local v1    # "item":Lcom/infraware/filemanager/file/FileListItem;
    :goto_1
    return-object v2

    .line 3236
    .restart local v0    # "i":I
    .restart local v1    # "item":Lcom/infraware/filemanager/file/FileListItem;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3244
    .end local v0    # "i":I
    .end local v1    # "item":Lcom/infraware/filemanager/file/FileListItem;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public getSelectMode()I
    .locals 1

    .prologue
    .line 842
    iget v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nSelectMode:I

    return v0
.end method

.method public getServiceType()I
    .locals 1

    .prologue
    .line 481
    iget v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nServiceType:I

    return v0
.end method

.method public getShowExt()Z
    .locals 1

    .prologue
    .line 869
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v0}, Lcom/infraware/filemanager/file/FileListAdapter;->getShowExt()Z

    move-result v0

    return v0
.end method

.method public getStorageId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strStorageId:Ljava/lang/String;

    return-object v0
.end method

.method public initDeviceStoargeState()V
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oMapItemIdx:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 263
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oMapItemTop:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 265
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nCurDepth:I

    .line 266
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strCurPath:Ljava/lang/String;

    .line 268
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->setTmpState()V

    .line 269
    return-void
.end method

.method public initState()V
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oMapItemIdx:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 252
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oMapItemTop:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 254
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nCurDepth:I

    .line 255
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strStoragePath:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strCurPath:Ljava/lang/String;

    .line 257
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->setTmpState()V

    .line 258
    return-void
.end method

.method public isFindByPositionAction(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, -0x1

    .line 3635
    const-string/jumbo v3, "pos_x"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 3636
    .local v1, "x":I
    const-string/jumbo v3, "pos_y"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 3638
    .local v2, "y":I
    new-instance v0, Ljava/lang/Thread;

    const/4 v3, 0x0

    new-instance v4, Lcom/infraware/filemanager/file/FileBaseActivity$18;

    invoke-direct {v4, p0, v1, v2}, Lcom/infraware/filemanager/file/FileBaseActivity$18;-><init>(Lcom/infraware/filemanager/file/FileBaseActivity;II)V

    invoke-direct {v0, v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;)V

    .line 3652
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 3653
    return-void
.end method

.method public isLocalStorage()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 491
    iget v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nStorageType:I

    if-ne v1, v0, :cond_0

    .line 494
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSDCardPath(Ljava/lang/String;)Z
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 3249
    iget v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nStorageType:I

    if-ne v3, v2, :cond_1

    invoke-static {}, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->useExternalSD()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSDCardList:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    .line 3251
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 3253
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/filemanager/file/FileListItem;

    .line 3254
    .local v1, "item":Lcom/infraware/filemanager/file/FileListItem;
    iget-object v3, v1, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3259
    .end local v0    # "i":I
    .end local v1    # "item":Lcom/infraware/filemanager/file/FileListItem;
    :goto_1
    return v2

    .line 3251
    .restart local v0    # "i":I
    .restart local v1    # "item":Lcom/infraware/filemanager/file/FileListItem;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3259
    .end local v0    # "i":I
    .end local v1    # "item":Lcom/infraware/filemanager/file/FileListItem;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public isUpdatable()Z
    .locals 1

    .prologue
    .line 902
    iget-boolean v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_bUpdatable:Z

    return v0
.end method

.method protected makeLocalFileList(Ljava/lang/String;)V
    .locals 11
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    .line 2569
    invoke-static {}, Lcom/infraware/common/util/FileUtils;->isSdcard()Z

    move-result v8

    if-nez v8, :cond_1

    .line 2673
    :cond_0
    return-void

    .line 2572
    :cond_1
    const/4 v6, 0x0

    .line 2573
    .local v6, "item":Lcom/infraware/filemanager/file/FileListItem;
    invoke-virtual {p0, v10}, Lcom/infraware/filemanager/file/FileBaseActivity;->getRootPath(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->getSelectMode()I

    move-result v8

    if-eq v8, v10, :cond_2

    iget v8, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nInternalMode:I

    if-eq v8, v10, :cond_2

    .line 2577
    new-instance v6, Lcom/infraware/filemanager/file/FileListItem;

    .end local v6    # "item":Lcom/infraware/filemanager/file/FileListItem;
    invoke-direct {v6}, Lcom/infraware/filemanager/file/FileListItem;-><init>()V

    .line 2578
    .restart local v6    # "item":Lcom/infraware/filemanager/file/FileListItem;
    iput-boolean v10, v6, Lcom/infraware/filemanager/file/FileListItem;->isFolder:Z

    .line 2579
    iput v10, v6, Lcom/infraware/filemanager/file/FileListItem;->type:I

    .line 2580
    iget-object v8, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strCurPath:Ljava/lang/String;

    iput-object v8, v6, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    .line 2581
    const-string/jumbo v8, ".."

    iput-object v8, v6, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    .line 2582
    iget-object v8, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileList:Ljava/util/ArrayList;

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2585
    :cond_2
    invoke-virtual {p0, v10}, Lcom/infraware/filemanager/file/FileBaseActivity;->getRootPath(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_3

    iget-object v8, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSDCardList:Ljava/util/ArrayList;

    if-eqz v8, :cond_3

    .line 2587
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget-object v8, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v4, v8, :cond_0

    .line 2588
    iget-object v9, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileList:Ljava/util/ArrayList;

    iget-object v8, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/infraware/filemanager/file/FileListItem;

    invoke-virtual {v8}, Lcom/infraware/filemanager/file/FileListItem;->cloneItem()Lcom/infraware/filemanager/file/FileListItem;

    move-result-object v8

    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2587
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2593
    .end local v4    # "i":I
    :cond_3
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2594
    .local v3, "home":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nCurUpdate:J

    .line 2595
    if-eqz v3, :cond_0

    .line 2597
    const/4 v2, 0x0

    .line 2598
    .local v2, "fileList":[Ljava/io/File;
    iget-object v8, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFolderFilter:Lcom/infraware/filemanager/file/FileListFilter;

    if-eqz v8, :cond_0

    .line 2600
    iget-object v8, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFolderFilter:Lcom/infraware/filemanager/file/FileListFilter;

    invoke-virtual {v8, p1}, Lcom/infraware/filemanager/file/FileListFilter;->setPath(Ljava/lang/String;)V

    .line 2602
    iget-object v8, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFolderFilter:Lcom/infraware/filemanager/file/FileListFilter;

    invoke-virtual {v3, v8}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v2

    .line 2603
    if-eqz v2, :cond_0

    .line 2606
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v7, v0

    .local v7, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_1
    if-ge v5, v7, :cond_0

    aget-object v1, v0, v5

    .line 2608
    .local v1, "file":Ljava/io/File;
    iget-boolean v8, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_bShowHidden:Z

    if-nez v8, :cond_4

    invoke-virtual {v1}, Ljava/io/File;->isHidden()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2606
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 2611
    :cond_4
    new-instance v6, Lcom/infraware/filemanager/file/FileListItem;

    .end local v6    # "item":Lcom/infraware/filemanager/file/FileListItem;
    invoke-direct {v6}, Lcom/infraware/filemanager/file/FileListItem;-><init>()V

    .line 2612
    .restart local v6    # "item":Lcom/infraware/filemanager/file/FileListItem;
    iput-boolean v10, v6, Lcom/infraware/filemanager/file/FileListItem;->isFolder:Z

    .line 2613
    iput v10, v6, Lcom/infraware/filemanager/file/FileListItem;->type:I

    .line 2614
    iget-object v8, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strCurPath:Ljava/lang/String;

    iput-object v8, v6, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    .line 2615
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    .line 2616
    const-wide/16 v8, 0x0

    iput-wide v8, v6, Lcom/infraware/filemanager/file/FileListItem;->size:J

    .line 2617
    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v8

    iput-wide v8, v6, Lcom/infraware/filemanager/file/FileListItem;->updateTime:J

    .line 2636
    invoke-static {v1, p1}, Lcom/infraware/filemanager/porting/DeviceConfig$SdDetectHelper;->isSdCardDirectory(Ljava/io/File;Ljava/lang/String;)Z

    move-result v8

    if-ne v8, v10, :cond_5

    .line 2638
    iget-object v8, v6, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    invoke-virtual {p0, v8}, Lcom/infraware/filemanager/file/FileBaseActivity;->getSDCardIcon(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v8

    iput-object v8, v6, Lcom/infraware/filemanager/file/FileListItem;->icon:Landroid/graphics/Bitmap;

    .line 2639
    iget-object v8, v6, Lcom/infraware/filemanager/file/FileListItem;->icon:Landroid/graphics/Bitmap;

    if-eqz v8, :cond_5

    .line 2640
    iput-boolean v10, v6, Lcom/infraware/filemanager/file/FileListItem;->isSDCard:Z

    .line 2643
    :cond_5
    iget-object v8, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileList:Ljava/util/ArrayList;

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method protected makeSDCardList()V
    .locals 13

    .prologue
    const-wide/16 v11, 0x0

    const/4 v10, 0x1

    .line 2466
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSDCardList:Ljava/util/ArrayList;

    .line 2472
    new-instance v6, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2473
    .local v6, "sdCard":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 2475
    new-instance v3, Lcom/infraware/filemanager/file/FileListItem;

    invoke-direct {v3}, Lcom/infraware/filemanager/file/FileListItem;-><init>()V

    .line 2476
    .local v3, "item":Lcom/infraware/filemanager/file/FileListItem;
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0200b3

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 2478
    .local v2, "icon":Landroid/graphics/Bitmap;
    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v3, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    .line 2479
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "FT03"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-ne v8, v10, :cond_0

    .line 2480
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f07001e

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v3, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    .line 2482
    :cond_0
    iput-boolean v10, v3, Lcom/infraware/filemanager/file/FileListItem;->isFolder:Z

    .line 2483
    iput-boolean v10, v3, Lcom/infraware/filemanager/file/FileListItem;->isSDCard:Z

    .line 2484
    iput v10, v3, Lcom/infraware/filemanager/file/FileListItem;->type:I

    .line 2485
    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v3, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    .line 2486
    iput-wide v11, v3, Lcom/infraware/filemanager/file/FileListItem;->size:J

    .line 2487
    invoke-virtual {v6}, Ljava/io/File;->lastModified()J

    move-result-wide v8

    iput-wide v8, v3, Lcom/infraware/filemanager/file/FileListItem;->updateTime:J

    .line 2488
    iput-object v2, v3, Lcom/infraware/filemanager/file/FileListItem;->icon:Landroid/graphics/Bitmap;

    .line 2490
    iget-object v8, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2493
    .end local v2    # "icon":Landroid/graphics/Bitmap;
    .end local v3    # "item":Lcom/infraware/filemanager/file/FileListItem;
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-static {}, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->getCount()I

    move-result v8

    if-ge v1, v8, :cond_2

    .line 2496
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/infraware/common/util/Utils;->isKnoxMode(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_2

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine;->isKkDisuseExternal()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2529
    :cond_2
    invoke-static {p0}, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->getUSBMountList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v5

    .line 2530
    .local v5, "oUsbMountList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v1, v8, :cond_a

    .line 2536
    new-instance v7, Ljava/io/File;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2537
    .local v7, "usbStorage":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_9

    .line 2530
    :cond_3
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2499
    .end local v5    # "oUsbMountList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7    # "usbStorage":Ljava/io/File;
    :cond_4
    new-instance v6, Ljava/io/File;

    .end local v6    # "sdCard":Ljava/io/File;
    invoke-static {v1}, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->getFolderPath(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2500
    .restart local v6    # "sdCard":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_6

    .line 2493
    :cond_5
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2503
    :cond_6
    invoke-virtual {v6}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 2504
    .local v0, "fileList":[Ljava/io/File;
    if-eqz v0, :cond_5

    .line 2507
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-static {v8, v1}, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->getFolderName(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v4

    .line 2508
    .local v4, "name":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-static {v8, v1}, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->getFolderIcon(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 2510
    .restart local v2    # "icon":Landroid/graphics/Bitmap;
    new-instance v3, Lcom/infraware/filemanager/file/FileListItem;

    invoke-direct {v3}, Lcom/infraware/filemanager/file/FileListItem;-><init>()V

    .line 2512
    .restart local v3    # "item":Lcom/infraware/filemanager/file/FileListItem;
    if-nez v4, :cond_7

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    .end local v4    # "name":Ljava/lang/String;
    :cond_7
    iput-object v4, v3, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    .line 2513
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "FT03"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-ne v8, v10, :cond_8

    iget-object v8, v3, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    const-string/jumbo v9, "extSdCard"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-ne v8, v10, :cond_8

    .line 2515
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f07002a

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v3, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    .line 2518
    :cond_8
    iput-boolean v10, v3, Lcom/infraware/filemanager/file/FileListItem;->isFolder:Z

    .line 2519
    iput-boolean v10, v3, Lcom/infraware/filemanager/file/FileListItem;->isSDCard:Z

    .line 2520
    iput v10, v3, Lcom/infraware/filemanager/file/FileListItem;->type:I

    .line 2521
    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v3, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    .line 2522
    iput-wide v11, v3, Lcom/infraware/filemanager/file/FileListItem;->size:J

    .line 2523
    invoke-virtual {v6}, Ljava/io/File;->lastModified()J

    move-result-wide v8

    iput-wide v8, v3, Lcom/infraware/filemanager/file/FileListItem;->updateTime:J

    .line 2524
    iput-object v2, v3, Lcom/infraware/filemanager/file/FileListItem;->icon:Landroid/graphics/Bitmap;

    .line 2526
    iget-object v8, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2540
    .end local v0    # "fileList":[Ljava/io/File;
    .end local v2    # "icon":Landroid/graphics/Bitmap;
    .end local v3    # "item":Lcom/infraware/filemanager/file/FileListItem;
    .restart local v5    # "oUsbMountList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v7    # "usbStorage":Ljava/io/File;
    :cond_9
    invoke-virtual {v7}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 2541
    .restart local v0    # "fileList":[Ljava/io/File;
    if-eqz v0, :cond_3

    .line 2544
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Lcom/infraware/common/util/Utils;->getFolderName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2546
    .restart local v4    # "name":Ljava/lang/String;
    if-eqz v4, :cond_3

    .line 2549
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f020116

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 2551
    .restart local v2    # "icon":Landroid/graphics/Bitmap;
    new-instance v3, Lcom/infraware/filemanager/file/FileListItem;

    invoke-direct {v3}, Lcom/infraware/filemanager/file/FileListItem;-><init>()V

    .line 2553
    .restart local v3    # "item":Lcom/infraware/filemanager/file/FileListItem;
    iput-object v4, v3, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    .line 2554
    iput-boolean v10, v3, Lcom/infraware/filemanager/file/FileListItem;->isFolder:Z

    .line 2555
    iput-boolean v10, v3, Lcom/infraware/filemanager/file/FileListItem;->isSDCard:Z

    .line 2556
    iput v10, v3, Lcom/infraware/filemanager/file/FileListItem;->type:I

    .line 2557
    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v3, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    .line 2558
    iput-wide v11, v3, Lcom/infraware/filemanager/file/FileListItem;->size:J

    .line 2559
    invoke-virtual {v7}, Ljava/io/File;->lastModified()J

    move-result-wide v8

    iput-wide v8, v3, Lcom/infraware/filemanager/file/FileListItem;->updateTime:J

    .line 2560
    iput-object v2, v3, Lcom/infraware/filemanager/file/FileListItem;->icon:Landroid/graphics/Bitmap;

    .line 2561
    iput-boolean v10, v3, Lcom/infraware/filemanager/file/FileListItem;->isFormTypeImpossibility:Z

    .line 2563
    iget-object v8, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 2565
    .end local v0    # "fileList":[Ljava/io/File;
    .end local v2    # "icon":Landroid/graphics/Bitmap;
    .end local v3    # "item":Lcom/infraware/filemanager/file/FileListItem;
    .end local v4    # "name":Ljava/lang/String;
    .end local v7    # "usbStorage":Ljava/io/File;
    :cond_a
    return-void
.end method

.method public moveFolder(Ljava/lang/String;)V
    .locals 3
    .param p1, "folder"    # Ljava/lang/String;

    .prologue
    .line 3264
    const-string/jumbo v1, ".."

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_3

    .line 3266
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oMapItemIdx:Ljava/util/HashMap;

    iget v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nCurDepth:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3267
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oMapItemTop:Ljava/util/HashMap;

    iget v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nCurDepth:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3269
    iget v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nCurDepth:I

    if-lez v1, :cond_0

    .line 3271
    iget v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nCurDepth:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nTmpDepth:I

    .line 3273
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strCurPath:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/infraware/filemanager/file/FileBaseActivity;->isSDCardPath(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3275
    const-string/jumbo v1, "/"

    iput-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strTmpPath:Ljava/lang/String;

    .line 3300
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->onUpdateList()V

    .line 3302
    iget-boolean v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_bShowPath:Z

    if-eqz v1, :cond_1

    .line 3303
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strTmpPath:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/infraware/filemanager/file/FileBaseActivity;->onToastMessage(Ljava/lang/String;)V

    .line 3304
    :cond_1
    return-void

    .line 3279
    :cond_2
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strCurPath:Ljava/lang/String;

    const/16 v2, 0x2f

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 3280
    .local v0, "nIndex":I
    if-ltz v0, :cond_0

    .line 3282
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strCurPath:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strTmpPath:Ljava/lang/String;

    .line 3283
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strTmpPath:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 3284
    const-string/jumbo v1, "/"

    iput-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strTmpPath:Ljava/lang/String;

    goto :goto_0

    .line 3292
    .end local v0    # "nIndex":I
    :cond_3
    iget v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nCurDepth:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nTmpDepth:I

    .line 3294
    invoke-virtual {p0, p1}, Lcom/infraware/filemanager/file/FileBaseActivity;->isSDCardPath(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 3295
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strTmpPath:Ljava/lang/String;

    goto :goto_0

    .line 3297
    :cond_4
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strCurPath:Ljava/lang/String;

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strCurPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    iput-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strTmpPath:Ljava/lang/String;

    goto :goto_0

    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strCurPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method protected offProgressDlg()V
    .locals 3

    .prologue
    .line 1062
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->SHOW_FILE_LIST_DIALOG()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1064
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oUpdateProgress:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oUpdateProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1066
    :try_start_0
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oUpdateProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1078
    :cond_0
    :goto_0
    return-void

    .line 1068
    :catch_0
    move-exception v0

    .line 1069
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1074
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_layoutProgress:Landroid/widget/FrameLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 2001
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2014
    return-void
.end method

.method public onButtonClick(Landroid/view/View;)V
    .locals 0
    .param p1, "target"    # Landroid/view/View;

    .prologue
    .line 2121
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "_newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1988
    iget v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nOrientation:I

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v1, v2, :cond_0

    .line 1989
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->onOrientationChanged()V

    .line 1991
    :cond_0
    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v1}, Lcom/infraware/common/util/Utils;->getLocaleType(Ljava/util/Locale;)I

    move-result v0

    .line 1992
    .local v0, "nLocale":I
    iget v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nLocaleCode:I

    if-eq v1, v0, :cond_1

    .line 1993
    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/file/FileBaseActivity;->onLocaleChanged(I)V

    .line 1995
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1996
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/16 v5, 0x8

    .line 1609
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 1611
    const v3, 0x7f03001a

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->setContentView(I)V

    .line 1614
    invoke-static {}, Lcom/infraware/common/util/Utils;->isL()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1615
    const v3, 0x103012c

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->setTheme(I)V

    .line 1619
    :cond_0
    invoke-static {}, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->useExternalSD()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1621
    const-string/jumbo v3, "/"

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->LOCAL_ROOT_PATH:Ljava/lang/String;

    .line 1622
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->makeSDCardList()V

    .line 1636
    :cond_1
    iput v6, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nStorageType:I

    .line 1637
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->LOCAL_ROOT_PATH:Ljava/lang/String;

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strStoragePath:Ljava/lang/String;

    .line 1638
    const-string/jumbo v3, ""

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strStorageId:Ljava/lang/String;

    .line 1639
    const-string/jumbo v3, ""

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strStorageToken1:Ljava/lang/String;

    .line 1640
    const-string/jumbo v3, ""

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strStorageToken2:Ljava/lang/String;

    .line 1641
    iput-object v7, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strRootPath:Ljava/lang/String;

    .line 1643
    const v3, 0x7f0b00e2

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout;

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_layoutProgress:Landroid/widget/FrameLayout;

    .line 1644
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_layoutProgress:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->bringToFront()V

    .line 1646
    const v3, 0x7f0b00e1

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout;

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_layoutFileList:Landroid/widget/FrameLayout;

    .line 1648
    const v3, 0x7f0b00de

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_layoutTitleBar:Landroid/widget/LinearLayout;

    .line 1649
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_layoutTitleBar:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1651
    const v3, 0x7f0b0031

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_layoutTitle:Landroid/widget/LinearLayout;

    .line 1652
    const v3, 0x7f0b0032

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ivTitle:Landroid/widget/ImageView;

    .line 1653
    const v3, 0x7f0b0034

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvTitle:Landroid/widget/TextView;

    .line 1654
    const v3, 0x7f0b003a

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_btnTitle:Landroid/widget/Button;

    .line 1656
    const v3, 0x7f0b00df

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_layoutInfo:Landroid/widget/LinearLayout;

    .line 1657
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_layoutInfo:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1659
    const v3, 0x7f0b00e0

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvInfo:Landroid/widget/TextView;

    .line 1667
    const v3, 0x7f0b00e8

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout;

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_layoutIndex:Landroid/widget/FrameLayout;

    .line 1668
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_layoutIndex:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1670
    const v3, 0x7f0b00e9

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvIndexFile:Landroid/widget/TextView;

    .line 1671
    const v3, 0x7f0b00ea

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvIndexFolder:Landroid/widget/TextView;

    .line 1672
    const v3, 0x7f0b00eb

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ivIndexIcon:Landroid/widget/ImageView;

    .line 1673
    const v3, 0x7f0b00ec

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvIndexType:Landroid/widget/TextView;

    .line 1731
    const v3, 0x7f0b00e5

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ibScrollButton:Landroid/widget/ImageButton;

    .line 1732
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ibScrollButton:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->bringToFront()V

    .line 1733
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ibScrollButton:Landroid/widget/ImageButton;

    iget-object v4, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1735
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3}, Lcom/infraware/common/util/Utils;->getCurrentLocaleType(Landroid/content/res/Resources;)I

    move-result v3

    iput v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nLocaleCode:I

    .line 1736
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    iput v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nOrientation:I

    .line 1738
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->initState()V

    .line 1740
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oMapItemIdx:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 1741
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oMapItemTop:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 1743
    const v3, 0x7f0b00ed

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_layoutEmpty:Landroid/widget/LinearLayout;

    .line 1744
    const v3, 0x7f0b0027

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ivEmptyImage:Landroid/widget/ImageView;

    .line 1745
    const v3, 0x7f0b0028

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvEmptyText:Landroid/widget/TextView;

    .line 1747
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_layoutEmpty:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1748
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ivEmptyImage:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1750
    const v3, 0x7f0b00e7

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    .line 1751
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1753
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    invoke-virtual {v3, v6}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 1754
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02003d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 1772
    new-instance v3, Lcom/infraware/filemanager/file/FileListFilter;

    invoke-direct {v3, p0, v6}, Lcom/infraware/filemanager/file/FileListFilter;-><init>(Landroid/content/Context;I)V

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFolderFilter:Lcom/infraware/filemanager/file/FileListFilter;

    .line 1775
    new-instance v3, Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-direct {v3, p0}, Lcom/infraware/filemanager/file/FileListAdapter;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    .line 1776
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1779
    const v3, 0x7f0b0037

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_btnTitleMenuNewFolder:Landroid/widget/ImageButton;

    .line 1786
    const v3, 0x7f0b0039

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_btnTitleMenuSave:Landroid/widget/ImageButton;

    .line 1789
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_btnTitleMenuNewFolder:Landroid/widget/ImageButton;

    new-instance v4, Lcom/infraware/filemanager/file/FileBaseActivity$13;

    invoke-direct {v4, p0}, Lcom/infraware/filemanager/file/FileBaseActivity$13;-><init>(Lcom/infraware/filemanager/file/FileBaseActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1797
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_btnTitleMenuSave:Landroid/widget/ImageButton;

    new-instance v4, Lcom/infraware/filemanager/file/FileBaseActivity$14;

    invoke-direct {v4, p0}, Lcom/infraware/filemanager/file/FileBaseActivity$14;-><init>(Lcom/infraware/filemanager/file/FileBaseActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1804
    iget v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nSelectMode:I

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->setSelectMode(I)V

    .line 1810
    const/4 v2, 0x1

    .line 1811
    .local v2, "nSortField":I
    const/4 v1, 0x1

    .line 1813
    .local v1, "isAscending":Z
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v3, v2}, Lcom/infraware/filemanager/file/FileListAdapter;->setSortField(I)V

    .line 1814
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v3, v1}, Lcom/infraware/filemanager/file/FileListAdapter;->setAscending(Z)V

    .line 1818
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->clearSDCard()V

    .line 1819
    invoke-direct {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->setSDCardConfig()V

    .line 1821
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->getFindByPositionIntentFilter()Landroid/content/IntentFilter;

    move-result-object v0

    .line 1822
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    if-eqz v0, :cond_2

    .line 1824
    new-instance v3, Lcom/infraware/common/event/FindByPositionEvent;

    invoke-direct {v3}, Lcom/infraware/common/event/FindByPositionEvent;-><init>()V

    iput-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFindReceiver:Lcom/infraware/common/event/FindByPositionEvent;

    .line 1825
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFindReceiver:Lcom/infraware/common/event/FindByPositionEvent;

    invoke-virtual {v3, p0}, Lcom/infraware/common/event/FindByPositionEvent;->setFindByPositionListener(Lcom/infraware/common/event/FindByPositionListener;)V

    .line 1826
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFindReceiver:Lcom/infraware/common/event/FindByPositionEvent;

    invoke-virtual {p0, v3, v0}, Lcom/infraware/filemanager/file/FileBaseActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1829
    :cond_2
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->showEmptyScreen(Z)V

    .line 1831
    iput-object v7, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strCurFile:Ljava/lang/String;

    .line 1832
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1853
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_bDestroy:Z

    .line 1855
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFindReceiver:Lcom/infraware/common/event/FindByPositionEvent;

    if-eqz v0, :cond_0

    .line 1856
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFindReceiver:Lcom/infraware/common/event/FindByPositionEvent;

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/file/FileBaseActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1861
    :cond_0
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 1862
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1872
    :cond_1
    iput-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    .line 1876
    const v0, 0x7f0b00dd

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/file/FileBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/infraware/common/util/Utils;->unbindDrawables(Landroid/view/View;)V

    .line 1878
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 1879
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1915
    iput p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nDownKey:I

    .line 1916
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v3, -0x1

    .line 1926
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->isUpdatable()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1928
    iput v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nDownKey:I

    .line 1977
    :cond_0
    :goto_0
    return v0

    .line 1932
    :cond_1
    sparse-switch p1, :sswitch_data_0

    .line 1976
    :cond_2
    iput v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nDownKey:I

    .line 1977
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 1935
    :sswitch_0
    iget v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nSelectMode:I

    if-eq v1, v0, :cond_0

    .line 1946
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strRootPath:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strRootPath:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_3

    .line 1948
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strCurPath:Ljava/lang/String;

    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strRootPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_3

    .line 1950
    iput v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nDownKey:I

    .line 1951
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 1955
    :cond_3
    iget v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nCurDepth:I

    if-lez v1, :cond_2

    .line 1957
    const-string/jumbo v1, ".."

    invoke-virtual {p0, v1}, Lcom/infraware/filemanager/file/FileBaseActivity;->moveFolder(Ljava/lang/String;)V

    .line 1958
    iput v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nDownKey:I

    goto :goto_0

    .line 1972
    :sswitch_1
    iput v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nDownKey:I

    goto :goto_0

    .line 1932
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x54 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 2017
    invoke-virtual {p0, p3}, Lcom/infraware/filemanager/file/FileBaseActivity;->getFileItem(I)Lcom/infraware/filemanager/file/FileListItem;

    move-result-object v1

    .line 2018
    .local v1, "item":Lcom/infraware/filemanager/file/FileListItem;
    if-nez v1, :cond_1

    .line 2092
    :cond_0
    :goto_0
    return-void

    .line 2070
    :cond_1
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oMapItemIdx:Ljava/util/HashMap;

    iget v4, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nCurDepth:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2071
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oMapItemTop:Ljava/util/HashMap;

    iget v4, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nCurDepth:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2073
    iget-boolean v3, v1, Lcom/infraware/filemanager/file/FileListItem;->isFolder:Z

    if-eqz v3, :cond_0

    .line 2081
    iget-object v2, v1, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    .line 2082
    .local v2, "movePath":Ljava/lang/String;
    iget-boolean v3, v1, Lcom/infraware/filemanager/file/FileListItem;->isSDCard:Z

    if-eqz v3, :cond_3

    .line 2084
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2085
    .local v0, "internalSDPath":Ljava/lang/String;
    iget-object v3, v1, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, v1, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2086
    :cond_2
    iget-object v2, v1, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    .line 2089
    .end local v0    # "internalSDPath":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0, v2}, Lcom/infraware/filemanager/file/FileBaseActivity;->moveFolder(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onLocaleChanged(I)V
    .locals 2
    .param p1, "nLocale"    # I

    .prologue
    .line 2152
    iput p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nLocaleCode:I

    .line 2153
    invoke-static {}, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->useExternalSD()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2154
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->makeSDCardList()V

    .line 2156
    :cond_0
    invoke-static {}, Lcom/infraware/common/util/FileUtils;->isSdcard()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->isLocalStorage()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2158
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvEmptyText:Landroid/widget/TextView;

    const v1, 0x7f07026c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2168
    :goto_0
    iget v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nTitleId:I

    if-eqz v0, :cond_1

    .line 2169
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvTitle:Landroid/widget/TextView;

    iget v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nTitleId:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2176
    :cond_1
    return-void

    .line 2162
    :cond_2
    iget v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nEmptyTextId:I

    if-lez v0, :cond_3

    .line 2163
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvEmptyText:Landroid/widget/TextView;

    iget v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nEmptyTextId:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 2165
    :cond_3
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvEmptyText:Landroid/widget/TextView;

    const v1, 0x7f07026b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public onOrientationChanged()V
    .locals 3

    .prologue
    .line 2125
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nOrientation:I

    .line 2128
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "FT03"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/infraware/common/util/Utils;->isJBP()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2129
    const-string/jumbo v1, "keyguard"

    invoke-virtual {p0, v1}, Lcom/infraware/filemanager/file/FileBaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 2130
    .local v0, "keyguardManager":Landroid/app/KeyguardManager;
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2148
    .end local v0    # "keyguardManager":Landroid/app/KeyguardManager;
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 5

    .prologue
    .line 1884
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getSelectedView()Landroid/view/View;

    move-result-object v1

    .line 1885
    .local v1, "itemView":Landroid/view/View;
    if-nez v1, :cond_0

    .line 1886
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1888
    :cond_0
    if-eqz v1, :cond_1

    .line 1890
    const/high16 v2, 0x7f080000

    invoke-virtual {v1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/filemanager/file/FileListItem;

    move-object v0, v2

    check-cast v0, Lcom/infraware/filemanager/file/FileListItem;

    .line 1892
    .local v0, "item":Lcom/infraware/filemanager/file/FileListItem;
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oMapItemIdx:Ljava/util/HashMap;

    iget v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nCurDepth:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v4, v0}, Lcom/infraware/filemanager/file/FileListAdapter;->getPosition(Lcom/infraware/filemanager/file/FileListItem;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1893
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oMapItemTop:Ljava/util/HashMap;

    iget v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nCurDepth:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1910
    .end local v0    # "item":Lcom/infraware/filemanager/file/FileListItem;
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 1911
    return-void
.end method

.method public onPostScan()V
    .locals 2

    .prologue
    .line 2205
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_runPostScan:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2206
    return-void
.end method

.method public onPostUpdate()V
    .locals 2

    .prologue
    .line 2210
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_runPostUpdate:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2211
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 1837
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->clearChoices()V

    .line 1838
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->tag:Ljava/lang/String;

    const-string/jumbo v1, "OnResume"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1840
    iget v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nStorageType:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nInternalMode:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 1842
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->onUpdateList()V

    .line 1847
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 1848
    return-void
.end method

.method public onToastMessage(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 2271
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strToastMsg:Ljava/lang/String;

    .line 2272
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strToastMsg:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strToastMsg:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2273
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_runToastMsg:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2274
    :cond_0
    return-void
.end method

.method public onUpdateList()V
    .locals 2

    .prologue
    .line 2190
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_runUpdateList:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2191
    return-void
.end method

.method public onUpdateProgress(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    .line 2184
    iput p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nUpdateState:I

    .line 2185
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_runUpdateProgress:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2186
    return-void
.end method

.method public onUpdateState(Z)V
    .locals 0
    .param p1, "isUpdatable"    # Z

    .prologue
    .line 2180
    return-void
.end method

.method public setButtonState()V
    .locals 0

    .prologue
    .line 1045
    return-void
.end method

.method public setCurFile(Ljava/lang/String;)V
    .locals 0
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 461
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strCurFile:Ljava/lang/String;

    .line 462
    return-void
.end method

.method public setCurFileId(Ljava/lang/String;)V
    .locals 0
    .param p1, "fileId"    # Ljava/lang/String;

    .prologue
    .line 466
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strCurFileId:Ljava/lang/String;

    .line 467
    return-void
.end method

.method public setCurPath(Ljava/lang/String;Z)V
    .locals 8
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "isLocal"    # Z

    .prologue
    const/16 v7, 0x2f

    .line 383
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_1

    .line 443
    :cond_0
    :goto_0
    return-void

    .line 386
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    .line 389
    .local v3, "length":I
    const/4 v0, 0x0

    .line 390
    .local v0, "curDepth":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v3, :cond_3

    .line 392
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v5, v7, :cond_2

    .line 393
    add-int/lit8 v0, v0, 0x1

    .line 390
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 396
    :cond_3
    if-eqz p2, :cond_6

    .line 398
    iget-object v5, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->LOCAL_ROOT_PATH:Ljava/lang/String;

    const-string/jumbo v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 400
    iget-object v5, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSDCardList:Ljava/util/ArrayList;

    if-eqz v5, :cond_6

    .line 402
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_2
    iget-object v5, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v2, v5, :cond_6

    .line 404
    iget-object v5, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSDCardList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/infraware/filemanager/file/FileListItem;

    iget-object v4, v5, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    .line 405
    .local v4, "sdCard":Ljava/lang/String;
    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 407
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    .line 409
    const/4 v1, 0x0

    :goto_3
    if-ge v1, v3, :cond_5

    .line 411
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v5, v7, :cond_4

    .line 412
    add-int/lit8 v0, v0, -0x1

    .line 409
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 415
    :cond_5
    add-int/lit8 v0, v0, 0x1

    .line 433
    .end local v2    # "index":I
    .end local v4    # "sdCard":Ljava/lang/String;
    :cond_6
    if-ltz v0, :cond_0

    .line 435
    iput v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nCurDepth:I

    .line 436
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strCurPath:Ljava/lang/String;

    .line 438
    iput v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nTmpDepth:I

    .line 439
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strTmpPath:Ljava/lang/String;

    .line 441
    const-wide/16 v5, 0x0

    iput-wide v5, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nCurUpdate:J

    goto :goto_0

    .line 402
    .restart local v2    # "index":I
    .restart local v4    # "sdCard":Ljava/lang/String;
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 423
    .end local v2    # "index":I
    .end local v4    # "sdCard":Ljava/lang/String;
    :cond_8
    iget-object v5, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->LOCAL_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v3

    .line 425
    const/4 v1, 0x0

    :goto_4
    if-ge v1, v3, :cond_6

    .line 427
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v5, v7, :cond_9

    .line 428
    add-int/lit8 v0, v0, -0x1

    .line 425
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_4
.end method

.method public setCurState()V
    .locals 1

    .prologue
    .line 273
    iget v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nTmpDepth:I

    iput v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nCurDepth:I

    .line 274
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strTmpPath:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strCurPath:Ljava/lang/String;

    .line 275
    return-void
.end method

.method public setEmptyScreen(II)V
    .locals 0
    .param p1, "imageId"    # I
    .param p2, "textId"    # I

    .prologue
    .line 285
    iput p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nEmptyImageId:I

    .line 286
    iput p2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nEmptyTextId:I

    .line 287
    return-void
.end method

.method public setRootPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "rootPath"    # Ljava/lang/String;

    .prologue
    .line 486
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strRootPath:Ljava/lang/String;

    .line 487
    return-void
.end method

.method public setSelectMode(I)V
    .locals 2
    .param p1, "selectMode"    # I

    .prologue
    .line 818
    iget v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nSelectMode:I

    if-eq v0, p1, :cond_0

    .line 820
    iput p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nSelectMode:I

    .line 821
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 823
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v0}, Lcom/infraware/filemanager/file/FileListAdapter;->clearSelectedList()V

    .line 836
    :cond_0
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    if-eqz v0, :cond_1

    .line 837
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v0}, Lcom/infraware/filemanager/file/FileListAdapter;->notifyDataSetChanged()V

    .line 838
    :cond_1
    return-void
.end method

.method public setShowExt(Z)V
    .locals 1
    .param p1, "isShow"    # Z

    .prologue
    .line 865
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v0, p1}, Lcom/infraware/filemanager/file/FileListAdapter;->setShowExt(Z)V

    .line 866
    return-void
.end method

.method public setStorageType(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "type"    # I
    .param p2, "loginId"    # Ljava/lang/String;
    .param p3, "authToken1"    # Ljava/lang/String;
    .param p4, "authToken2"    # Ljava/lang/String;
    .param p5, "serviceType"    # I

    .prologue
    .line 354
    packed-switch p1, :pswitch_data_0

    .line 378
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->initState()V

    .line 379
    return-void

    .line 365
    :pswitch_0
    iput p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nStorageType:I

    .line 366
    iput p5, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nServiceType:I

    goto :goto_0

    .line 354
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public setTmpState()V
    .locals 1

    .prologue
    .line 279
    iget v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nCurDepth:I

    iput v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nTmpDepth:I

    .line 280
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strCurPath:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strTmpPath:Ljava/lang/String;

    .line 281
    return-void
.end method

.method public setUpdateState(Z)V
    .locals 2
    .param p1, "isUpdate"    # Z

    .prologue
    const/4 v0, 0x0

    .line 889
    iput v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nUpdateColor:I

    .line 890
    if-eqz p1, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_bUpdatable:Z

    .line 892
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_layoutFileList:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 893
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_layoutFileList:Landroid/widget/FrameLayout;

    iget-boolean v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_bUpdatable:Z

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 895
    :cond_0
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    iget-boolean v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_bUpdatable:Z

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 897
    iget-boolean v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_bUpdatable:Z

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/file/FileBaseActivity;->onUpdateState(Z)V

    .line 898
    return-void

    .line 890
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public showEmptyScreen(Z)V
    .locals 5
    .param p1, "isEmpty"    # Z

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 291
    if-eqz p1, :cond_3

    .line 293
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ivEmptyImage:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 294
    invoke-static {}, Lcom/infraware/common/util/FileUtils;->isSdcard()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->isLocalStorage()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 296
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvEmptyText:Landroid/widget/TextView;

    const v2, 0x7f07026c

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 297
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ivEmptyImage:Landroid/widget/ImageView;

    const v2, 0x7f02011d

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 298
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ivEmptyImage:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 326
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setVisibility(I)V

    .line 334
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_layoutEmpty:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 349
    :goto_1
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->setButtonState()V

    .line 350
    return-void

    .line 302
    :cond_1
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvEmptyText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 304
    .local v0, "layoutParam":Landroid/widget/LinearLayout$LayoutParams;
    iget v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nEmptyTextId:I

    if-lez v1, :cond_2

    .line 306
    const/high16 v1, 0x41cc0000    # 25.5f

    invoke-static {p0, v1}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 307
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvEmptyText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 308
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvEmptyText:Landroid/widget/TextView;

    iget v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nEmptyTextId:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 319
    :goto_2
    iget v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nEmptyImageId:I

    if-lez v1, :cond_0

    .line 321
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ivEmptyImage:Landroid/widget/ImageView;

    iget v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nEmptyImageId:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 322
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ivEmptyImage:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 312
    :cond_2
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ivEmptyImage:Landroid/widget/ImageView;

    const v2, 0x7f02011e

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 313
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ivEmptyImage:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 314
    const/high16 v1, 0x41200000    # 10.0f

    invoke-static {p0, v1}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 315
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvEmptyText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 316
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvEmptyText:Landroid/widget/TextView;

    const v2, 0x7f07026b

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 338
    .end local v0    # "layoutParam":Landroid/widget/LinearLayout$LayoutParams;
    :cond_3
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 339
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_layoutEmpty:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method protected updateList()V
    .locals 2

    .prologue
    .line 2806
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->isUpdatable()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2808
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->tag:Ljava/lang/String;

    const-string/jumbo v1, "updatelist ---- is not updatable return "

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2809
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->offProgressDlg()V

    .line 2938
    :cond_0
    :goto_0
    return-void

    .line 2813
    :cond_1
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oScanProgress:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oScanProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2847
    :cond_2
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/infraware/filemanager/file/FileBaseActivity$15;

    invoke-direct {v1, p0}, Lcom/infraware/filemanager/file/FileBaseActivity$15;-><init>(Lcom/infraware/filemanager/file/FileBaseActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oUpdateThread:Ljava/lang/Thread;

    .line 2875
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/infraware/filemanager/file/FileBaseActivity$16;

    invoke-direct {v1, p0}, Lcom/infraware/filemanager/file/FileBaseActivity$16;-><init>(Lcom/infraware/filemanager/file/FileBaseActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oProgressThread:Ljava/lang/Thread;

    .line 2931
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->setCurState()V

    .line 2932
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/infraware/filemanager/file/FileBaseActivity;->setUpdateState(Z)V

    .line 2934
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oProgressThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 2935
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oUpdateThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 2937
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ibScrollButton:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method protected updateScreen()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 2957
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->tag:Ljava/lang/String;

    const-string/jumbo v3, "updateScreen"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2959
    iget-boolean v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_bDestroy:Z

    if-eqz v2, :cond_1

    .line 3101
    :cond_0
    return-void

    .line 2962
    :cond_1
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2963
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Lcom/infraware/filemanager/file/FileListAdapter;->setList(Ljava/util/ArrayList;)V

    .line 2968
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v2}, Lcom/infraware/filemanager/file/FileListAdapter;->getCount()I

    move-result v2

    if-lez v2, :cond_2

    .line 2970
    iget v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nStorageType:I

    if-eqz v2, :cond_2

    .line 2971
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v2}, Lcom/infraware/filemanager/file/FileListAdapter;->sortFileList()V

    .line 2974
    :cond_2
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2976
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v2}, Lcom/infraware/filemanager/file/FileListAdapter;->getCount()I

    move-result v2

    if-lez v2, :cond_9

    .line 2978
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->requestFocus()Z

    .line 2979
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2980
    iget v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nSelectMode:I

    if-eqz v2, :cond_3

    .line 2982
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v2}, Lcom/infraware/filemanager/file/FileListAdapter;->getCount()I

    move-result v2

    const/16 v3, 0x64

    if-le v2, v3, :cond_3

    .line 2984
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSelectProgress:Landroid/app/ProgressDialog;

    .line 2985
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSelectProgress:Landroid/app/ProgressDialog;

    const v3, 0x7f07026d

    invoke-virtual {p0, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 2986
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSelectProgress:Landroid/app/ProgressDialog;

    new-instance v3, Lcom/infraware/filemanager/file/FileBaseActivity$17;

    invoke-direct {v3, p0}, Lcom/infraware/filemanager/file/FileBaseActivity$17;-><init>(Lcom/infraware/filemanager/file/FileBaseActivity;)V

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 3003
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSelectProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->show()V

    .line 3005
    iput-boolean v4, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_bCancelSelect:Z

    .line 3017
    :cond_3
    invoke-virtual {p0, v4}, Lcom/infraware/filemanager/file/FileBaseActivity;->showEmptyScreen(Z)V

    .line 3019
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v2}, Lcom/infraware/filemanager/file/FileListAdapter;->notifyDataSetChanged()V

    .line 3024
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oMapItemIdx:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-eqz v2, :cond_5

    .line 3025
    invoke-direct {p0}, Lcom/infraware/filemanager/file/FileBaseActivity;->setCurrentPosition()V

    .line 3029
    :goto_0
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strCurFile:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 3031
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v2}, Lcom/infraware/filemanager/file/FileListAdapter;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_6

    .line 3033
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v2, v0}, Lcom/infraware/filemanager/file/FileListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/filemanager/file/FileListItem;

    .line 3034
    .local v1, "item":Lcom/infraware/filemanager/file/FileListItem;
    invoke-virtual {v1}, Lcom/infraware/filemanager/file/FileListItem;->getFullFileName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strCurFile:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 3035
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    invoke-virtual {v2, v0, v4}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 3031
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3027
    .end local v0    # "i":I
    .end local v1    # "item":Lcom/infraware/filemanager/file/FileListItem;
    :cond_5
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    invoke-virtual {v2, v4, v4}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    goto :goto_0

    .line 3038
    .restart local v0    # "i":I
    :cond_6
    iput-object v5, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strCurFile:Ljava/lang/String;

    .line 3041
    .end local v0    # "i":I
    :cond_7
    iget v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nSelectMode:I

    if-nez v2, :cond_8

    .line 3042
    invoke-virtual {p0, v4}, Lcom/infraware/filemanager/file/FileBaseActivity;->setUpdateState(Z)V

    .line 3089
    :cond_8
    :goto_2
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v2}, Lcom/infraware/filemanager/file/FileListAdapter;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 3091
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v2, v0}, Lcom/infraware/filemanager/file/FileListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/filemanager/file/FileListItem;

    .line 3098
    .restart local v1    # "item":Lcom/infraware/filemanager/file/FileListItem;
    iget-boolean v2, v1, Lcom/infraware/filemanager/file/FileListItem;->isFolder:Z

    if-eqz v2, :cond_0

    .line 3089
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 3046
    .end local v0    # "i":I
    .end local v1    # "item":Lcom/infraware/filemanager/file/FileListItem;
    :cond_9
    iget v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nSelectMode:I

    if-eqz v2, :cond_a

    .line 3047
    invoke-virtual {p0, v4}, Lcom/infraware/filemanager/file/FileBaseActivity;->setSelectMode(I)V

    .line 3049
    :cond_a
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v2}, Lcom/infraware/filemanager/file/FileListAdapter;->getCount()I

    move-result v2

    if-lez v2, :cond_b

    .line 3050
    invoke-virtual {p0, v4}, Lcom/infraware/filemanager/file/FileBaseActivity;->showEmptyScreen(Z)V

    .line 3054
    :goto_4
    invoke-virtual {p0, v4}, Lcom/infraware/filemanager/file/FileBaseActivity;->setUpdateState(Z)V

    goto :goto_2

    .line 3052
    :cond_b
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/infraware/filemanager/file/FileBaseActivity;->showEmptyScreen(Z)V

    goto :goto_4
.end method
