.class Lcom/infraware/filemanager/file/FileBaseActivity$10;
.super Landroid/os/Handler;
.source "FileBaseActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/filemanager/file/FileBaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/filemanager/file/FileBaseActivity;


# direct methods
.method constructor <init>(Lcom/infraware/filemanager/file/FileBaseActivity;)V
    .locals 0

    .prologue
    .line 1312
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$10;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v6, 0x1

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 1315
    if-nez p1, :cond_1

    .line 1382
    :cond_0
    :goto_0
    return-void

    .line 1318
    :cond_1
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 1321
    :pswitch_0
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$10;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileBaseActivity;->m_layoutIndex:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0

    .line 1324
    :pswitch_1
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v2, :cond_0

    .line 1327
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$10;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v2}, Lcom/infraware/filemanager/file/FileListAdapter;->getSortField()I

    move-result v1

    .line 1328
    .local v1, "sortField":I
    if-eq v1, v6, :cond_2

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    .line 1331
    :cond_2
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$10;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvIndexFile:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1332
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$10;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvIndexFolder:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1333
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$10;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ivIndexIcon:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1334
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$10;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvIndexType:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1336
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/infraware/filemanager/file/FileListItem;

    .line 1337
    .local v0, "item":Lcom/infraware/filemanager/file/FileListItem;
    iget-boolean v2, v0, Lcom/infraware/filemanager/file/FileListItem;->isFolder:Z

    if-eqz v2, :cond_4

    .line 1339
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$10;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileBaseActivity;->m_layoutIndex:Landroid/widget/FrameLayout;

    const v3, 0x7f0200cf

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 1340
    if-ne v1, v6, :cond_3

    .line 1342
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$10;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvIndexFolder:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    invoke-virtual {v3, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1343
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$10;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvIndexFolder:Landroid/widget/TextView;

    const/high16 v3, 0x42100000    # 36.0f

    invoke-virtual {v2, v6, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1344
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$10;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvIndexFolder:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1372
    :goto_1
    iget-boolean v2, v0, Lcom/infraware/filemanager/file/FileListItem;->isStorageTitle:Z

    if-nez v2, :cond_6

    .line 1373
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$10;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileBaseActivity;->m_layoutIndex:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0

    .line 1348
    :cond_3
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$10;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvIndexFolder:Landroid/widget/TextView;

    const v3, 0x7f070288

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1349
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$10;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvIndexFolder:Landroid/widget/TextView;

    const/high16 v3, 0x41600000    # 14.0f

    invoke-virtual {v2, v6, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1350
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$10;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvIndexFolder:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 1355
    :cond_4
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$10;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileBaseActivity;->m_layoutIndex:Landroid/widget/FrameLayout;

    const v3, 0x7f0200ce

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 1357
    if-ne v1, v6, :cond_5

    .line 1359
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$10;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvIndexFile:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    invoke-virtual {v3, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1360
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$10;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvIndexFile:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 1364
    :cond_5
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$10;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ivIndexIcon:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity$10;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v3, v3, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v3, v0}, Lcom/infraware/filemanager/file/FileListAdapter;->getIcon(Lcom/infraware/filemanager/file/FileListItem;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1365
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$10;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileBaseActivity;->m_ivIndexIcon:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1367
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$10;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvIndexType:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1368
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$10;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvIndexType:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 1376
    :cond_6
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$10;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileBaseActivity;->m_layoutIndex:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1377
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$10;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvIndexFile:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1378
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$10;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileBaseActivity;->m_tvIndexFolder:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1318
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
