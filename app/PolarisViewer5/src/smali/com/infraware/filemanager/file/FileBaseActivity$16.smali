.class Lcom/infraware/filemanager/file/FileBaseActivity$16;
.super Ljava/lang/Object;
.source "FileBaseActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/filemanager/file/FileBaseActivity;->updateList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/filemanager/file/FileBaseActivity;


# direct methods
.method constructor <init>(Lcom/infraware/filemanager/file/FileBaseActivity;)V
    .locals 0

    .prologue
    .line 2876
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$16;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2879
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$16;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->tag:Ljava/lang/String;
    invoke-static {v2}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$200(Lcom/infraware/filemanager/file/FileBaseActivity;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "updatelist - progress thread run"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2880
    const/16 v1, 0x3e8

    .line 2885
    .local v1, "sleepTime":I
    :try_start_0
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$16;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->tag:Ljava/lang/String;
    invoke-static {v2}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$200(Lcom/infraware/filemanager/file/FileBaseActivity;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "updatelist - progress thread sleep"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2886
    int-to-long v2, v1

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2891
    :goto_0
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$16;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_bDestroy:Z
    invoke-static {v2}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$300(Lcom/infraware/filemanager/file/FileBaseActivity;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2892
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$16;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->tag:Ljava/lang/String;
    invoke-static {v2}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$200(Lcom/infraware/filemanager/file/FileBaseActivity;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "updatelist - activity destroy!, progress thread end"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2928
    :goto_1
    return-void

    .line 2887
    :catch_0
    move-exception v0

    .line 2888
    .local v0, "e":Ljava/lang/InterruptedException;
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$16;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->tag:Ljava/lang/String;
    invoke-static {v2}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$200(Lcom/infraware/filemanager/file/FileBaseActivity;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "updatelist - Interrupted Exception"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2896
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$16;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    invoke-virtual {v2}, Lcom/infraware/filemanager/file/FileBaseActivity;->isUpdatable()Z

    move-result v2

    if-nez v2, :cond_1

    sget-boolean v2, Lcom/infraware/filemanager/file/FileBaseActivity;->m_bIsUpdated:Z

    if-nez v2, :cond_1

    .line 2898
    sput-boolean v5, Lcom/infraware/filemanager/file/FileBaseActivity;->m_bIsUpdated:Z

    .line 2899
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$16;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->tag:Ljava/lang/String;
    invoke-static {v2}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$200(Lcom/infraware/filemanager/file/FileBaseActivity;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "updatelist - progress start"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2900
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$16;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    invoke-virtual {v2, v4}, Lcom/infraware/filemanager/file/FileBaseActivity;->onUpdateProgress(I)V

    .line 2903
    :cond_1
    const/16 v1, 0xa

    .line 2904
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$16;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # setter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_nUpdateColor:I
    invoke-static {v2, v4}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$102(Lcom/infraware/filemanager/file/FileBaseActivity;I)I

    .line 2905
    :goto_2
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$16;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    invoke-virtual {v2}, Lcom/infraware/filemanager/file/FileBaseActivity;->isUpdatable()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$16;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_bDestroy:Z
    invoke-static {v2}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$300(Lcom/infraware/filemanager/file/FileBaseActivity;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2908
    int-to-long v2, v1

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2912
    :goto_3
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$16;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_nUpdateColor:I
    invoke-static {v2}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$100(Lcom/infraware/filemanager/file/FileBaseActivity;)I

    move-result v2

    const/16 v3, 0xa0

    if-ge v2, v3, :cond_2

    .line 2914
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$16;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # += operator for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_nUpdateColor:I
    invoke-static {v2, v5}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$112(Lcom/infraware/filemanager/file/FileBaseActivity;I)I

    .line 2915
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$16;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    invoke-virtual {v2, v5}, Lcom/infraware/filemanager/file/FileBaseActivity;->onUpdateProgress(I)V

    goto :goto_2

    .line 2922
    :cond_2
    const/16 v1, 0x3e8

    goto :goto_2

    .line 2925
    :cond_3
    const-string/jumbo v2, "FileBaseActivity"

    const-string/jumbo v3, "updatelist - progress thread end"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2926
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$16;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->onUpdateProgress(I)V

    .line 2927
    sput-boolean v4, Lcom/infraware/filemanager/file/FileBaseActivity;->m_bIsUpdated:Z

    goto :goto_1

    .line 2909
    :catch_1
    move-exception v2

    goto :goto_3
.end method
