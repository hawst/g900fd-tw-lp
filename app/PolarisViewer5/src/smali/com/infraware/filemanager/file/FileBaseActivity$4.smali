.class Lcom/infraware/filemanager/file/FileBaseActivity$4;
.super Ljava/lang/Object;
.source "FileBaseActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/filemanager/file/FileBaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/filemanager/file/FileBaseActivity;


# direct methods
.method constructor <init>(Lcom/infraware/filemanager/file/FileBaseActivity;)V
    .locals 0

    .prologue
    .line 1179
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$4;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 1182
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$4;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSelectProgress:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$600(Lcom/infraware/filemanager/file/FileBaseActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$4;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSelectProgress:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$600(Lcom/infraware/filemanager/file/FileBaseActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1183
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$4;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSelectProgress:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$600(Lcom/infraware/filemanager/file/FileBaseActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1185
    :cond_0
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$4;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSelectProgress:Landroid/app/ProgressDialog;
    invoke-static {v0, v1}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$602(Lcom/infraware/filemanager/file/FileBaseActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 1186
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$4;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_nUpdateColor:I
    invoke-static {v0, v1}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$102(Lcom/infraware/filemanager/file/FileBaseActivity;I)I

    .line 1188
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$4;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    invoke-virtual {v0}, Lcom/infraware/filemanager/file/FileBaseActivity;->offProgressDlg()V

    .line 1189
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$4;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->tag:Ljava/lang/String;
    invoke-static {v0}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$200(Lcom/infraware/filemanager/file/FileBaseActivity;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Call updateScreen!"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1190
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$4;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    invoke-virtual {v0}, Lcom/infraware/filemanager/file/FileBaseActivity;->updateScreen()V

    .line 1192
    return-void
.end method
