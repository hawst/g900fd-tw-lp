.class Lcom/infraware/filemanager/file/FileBaseActivity$5;
.super Ljava/lang/Object;
.source "FileBaseActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/filemanager/file/FileBaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/filemanager/file/FileBaseActivity;


# direct methods
.method constructor <init>(Lcom/infraware/filemanager/file/FileBaseActivity;)V
    .locals 0

    .prologue
    .line 1196
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$5;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1199
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$5;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->tag:Ljava/lang/String;
    invoke-static {v0}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$200(Lcom/infraware/filemanager/file/FileBaseActivity;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "runPostSelect"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1200
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$5;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSelectProgress:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$600(Lcom/infraware/filemanager/file/FileBaseActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$5;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSelectProgress:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$600(Lcom/infraware/filemanager/file/FileBaseActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1202
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$5;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->tag:Ljava/lang/String;
    invoke-static {v0}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$200(Lcom/infraware/filemanager/file/FileBaseActivity;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "runPostSelect select progress dismiss!!"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1203
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$5;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSelectProgress:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$600(Lcom/infraware/filemanager/file/FileBaseActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1205
    :cond_0
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$5;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # setter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_bCancelSelect:Z
    invoke-static {v0, v2}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$702(Lcom/infraware/filemanager/file/FileBaseActivity;Z)Z

    .line 1206
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$5;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v0, v0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$5;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v0, v0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$5;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v1, v1, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 1208
    const-string/jumbo v0, "FileBaseActivity"

    const-string/jumbo v1, "runPostSelect notify dataset changed!!"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1209
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$5;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v0, v0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileAdapter:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-virtual {v0}, Lcom/infraware/filemanager/file/FileListAdapter;->notifyDataSetChanged()V

    .line 1212
    :cond_1
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$5;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_oSelectProgress:Landroid/app/ProgressDialog;
    invoke-static {v0, v1}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$602(Lcom/infraware/filemanager/file/FileBaseActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 1213
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$5;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    invoke-virtual {v0, v2}, Lcom/infraware/filemanager/file/FileBaseActivity;->setUpdateState(Z)V

    .line 1214
    return-void
.end method
