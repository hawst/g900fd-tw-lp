.class Lcom/infraware/filemanager/file/FileBaseActivity$15;
.super Ljava/lang/Object;
.source "FileBaseActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/filemanager/file/FileBaseActivity;->updateList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/filemanager/file/FileBaseActivity;


# direct methods
.method constructor <init>(Lcom/infraware/filemanager/file/FileBaseActivity;)V
    .locals 0

    .prologue
    .line 2848
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$15;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 2851
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$15;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->tag:Ljava/lang/String;
    invoke-static {v0}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$200(Lcom/infraware/filemanager/file/FileBaseActivity;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updatelist - updateThread - start - storageType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$15;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget v2, v2, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nStorageType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2853
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$15;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v0, v0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_lvFileList:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2855
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$15;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_oFileList:Ljava/util/ArrayList;

    .line 2856
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$15;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget v0, v0, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nStorageType:I

    packed-switch v0, :pswitch_data_0

    .line 2871
    :goto_0
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$15;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    invoke-virtual {v0}, Lcom/infraware/filemanager/file/FileBaseActivity;->onPostUpdate()V

    .line 2872
    return-void

    .line 2862
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$15;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$15;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v1, v1, Lcom/infraware/filemanager/file/FileBaseActivity;->m_strCurPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/infraware/filemanager/file/FileBaseActivity;->makeLocalFileList(Ljava/lang/String;)V

    goto :goto_0

    .line 2856
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
