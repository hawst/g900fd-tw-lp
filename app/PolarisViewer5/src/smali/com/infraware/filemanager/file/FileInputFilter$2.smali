.class Lcom/infraware/filemanager/file/FileInputFilter$2;
.super Ljava/lang/Object;
.source "FileInputFilter.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/filemanager/file/FileInputFilter;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/filemanager/file/FileInputFilter;


# direct methods
.method constructor <init>(Lcom/infraware/filemanager/file/FileInputFilter;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileInputFilter$2;->this$0:Lcom/infraware/filemanager/file/FileInputFilter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 5
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    const/4 v1, 0x0

    .line 108
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileInputFilter$2;->this$0:Lcom/infraware/filemanager/file/FileInputFilter;

    # getter for: Lcom/infraware/filemanager/file/FileInputFilter;->m_nMaxLength:I
    invoke-static {v2}, Lcom/infraware/filemanager/file/FileInputFilter;->access$000(Lcom/infraware/filemanager/file/FileInputFilter;)I

    move-result v2

    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v3

    sub-int v4, p6, p5

    sub-int/2addr v3, v4

    sub-int v0, v2, v3

    .line 109
    .local v0, "nRest":I
    if-gez v0, :cond_1

    .line 117
    :cond_0
    :goto_0
    return-object v1

    .line 112
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-le v2, v0, :cond_0

    .line 114
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileInputFilter$2;->this$0:Lcom/infraware/filemanager/file/FileInputFilter;

    const/16 v2, -0xa

    # invokes: Lcom/infraware/filemanager/file/FileInputFilter;->onToastMessage(I)V
    invoke-static {v1, v2}, Lcom/infraware/filemanager/file/FileInputFilter;->access$200(Lcom/infraware/filemanager/file/FileInputFilter;I)V

    .line 115
    const/4 v1, 0x0

    invoke-interface {p1, v1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0
.end method
