.class public Lcom/infraware/filemanager/file/FileListItem;
.super Ljava/lang/Object;
.source "FileListItem.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x3fdb7adf27fda527L


# instance fields
.field public accessTime:J

.field public accountId:Ljava/lang/String;

.field public contentSrc:Ljava/lang/String;

.field public ext:Ljava/lang/String;

.field public fileId:Ljava/lang/String;

.field public transient icon:Landroid/graphics/Bitmap;

.field public isFavorite:Z

.field public isFolder:Z

.field public isFormTypeImpossibility:Z

.field public isSDCard:Z

.field public isSelect:Z

.field public isStorageTitle:Z

.field public isSyncFolder:Z

.field public name:Ljava/lang/String;

.field public parentFileId:Ljava/lang/String;

.field public path:Ljava/lang/String;

.field public recentPkey:I

.field public serviceType:I

.field public size:J

.field public storageTitleType:I

.field public syncFolderIconId:I

.field public transient thumbnail:Landroid/graphics/Bitmap;

.field public type:I

.field public updateTime:J

.field public webExt:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v0, -0x1

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput v4, p0, Lcom/infraware/filemanager/file/FileListItem;->type:I

    .line 18
    iput v0, p0, Lcom/infraware/filemanager/file/FileListItem;->serviceType:I

    .line 20
    iput-boolean v4, p0, Lcom/infraware/filemanager/file/FileListItem;->isFolder:Z

    .line 21
    iput-boolean v1, p0, Lcom/infraware/filemanager/file/FileListItem;->isSelect:Z

    .line 22
    iput-boolean v1, p0, Lcom/infraware/filemanager/file/FileListItem;->isSDCard:Z

    .line 24
    iput-boolean v1, p0, Lcom/infraware/filemanager/file/FileListItem;->isStorageTitle:Z

    .line 25
    iput v0, p0, Lcom/infraware/filemanager/file/FileListItem;->storageTitleType:I

    .line 27
    iput-boolean v1, p0, Lcom/infraware/filemanager/file/FileListItem;->isSyncFolder:Z

    .line 28
    iput v1, p0, Lcom/infraware/filemanager/file/FileListItem;->syncFolderIconId:I

    .line 30
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    .line 31
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    .line 32
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    .line 34
    iput-wide v2, p0, Lcom/infraware/filemanager/file/FileListItem;->updateTime:J

    .line 35
    iput-wide v2, p0, Lcom/infraware/filemanager/file/FileListItem;->size:J

    .line 38
    iput-wide v2, p0, Lcom/infraware/filemanager/file/FileListItem;->accessTime:J

    .line 39
    iput v1, p0, Lcom/infraware/filemanager/file/FileListItem;->recentPkey:I

    .line 41
    iput-boolean v1, p0, Lcom/infraware/filemanager/file/FileListItem;->isFavorite:Z

    .line 42
    iput-boolean v1, p0, Lcom/infraware/filemanager/file/FileListItem;->isFormTypeImpossibility:Z

    .line 44
    iput-object v5, p0, Lcom/infraware/filemanager/file/FileListItem;->icon:Landroid/graphics/Bitmap;

    .line 45
    iput-object v5, p0, Lcom/infraware/filemanager/file/FileListItem;->thumbnail:Landroid/graphics/Bitmap;

    .line 47
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListItem;->fileId:Ljava/lang/String;

    .line 48
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListItem;->parentFileId:Ljava/lang/String;

    .line 49
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListItem;->contentSrc:Ljava/lang/String;

    .line 50
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListItem;->webExt:Ljava/lang/String;

    .line 51
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListItem;->accountId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public cloneItem()Lcom/infraware/filemanager/file/FileListItem;
    .locals 3

    .prologue
    .line 55
    new-instance v0, Lcom/infraware/filemanager/file/FileListItem;

    invoke-direct {v0}, Lcom/infraware/filemanager/file/FileListItem;-><init>()V

    .line 57
    .local v0, "cloneItem":Lcom/infraware/filemanager/file/FileListItem;
    iget v1, p0, Lcom/infraware/filemanager/file/FileListItem;->type:I

    iput v1, v0, Lcom/infraware/filemanager/file/FileListItem;->type:I

    .line 58
    iget v1, p0, Lcom/infraware/filemanager/file/FileListItem;->serviceType:I

    iput v1, v0, Lcom/infraware/filemanager/file/FileListItem;->serviceType:I

    .line 60
    iget-boolean v1, p0, Lcom/infraware/filemanager/file/FileListItem;->isFolder:Z

    iput-boolean v1, v0, Lcom/infraware/filemanager/file/FileListItem;->isFolder:Z

    .line 61
    iget-boolean v1, p0, Lcom/infraware/filemanager/file/FileListItem;->isSelect:Z

    iput-boolean v1, v0, Lcom/infraware/filemanager/file/FileListItem;->isSelect:Z

    .line 62
    iget-boolean v1, p0, Lcom/infraware/filemanager/file/FileListItem;->isSDCard:Z

    iput-boolean v1, v0, Lcom/infraware/filemanager/file/FileListItem;->isSDCard:Z

    .line 63
    iget-boolean v1, p0, Lcom/infraware/filemanager/file/FileListItem;->isStorageTitle:Z

    iput-boolean v1, v0, Lcom/infraware/filemanager/file/FileListItem;->isStorageTitle:Z

    .line 65
    iget-boolean v1, p0, Lcom/infraware/filemanager/file/FileListItem;->isSyncFolder:Z

    iput-boolean v1, v0, Lcom/infraware/filemanager/file/FileListItem;->isSyncFolder:Z

    .line 66
    iget v1, p0, Lcom/infraware/filemanager/file/FileListItem;->syncFolderIconId:I

    iput v1, v0, Lcom/infraware/filemanager/file/FileListItem;->syncFolderIconId:I

    .line 68
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    iput-object v1, v0, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    .line 69
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    .line 70
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    iput-object v1, v0, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    .line 72
    iget-wide v1, p0, Lcom/infraware/filemanager/file/FileListItem;->updateTime:J

    iput-wide v1, v0, Lcom/infraware/filemanager/file/FileListItem;->updateTime:J

    .line 73
    iget-wide v1, p0, Lcom/infraware/filemanager/file/FileListItem;->size:J

    iput-wide v1, v0, Lcom/infraware/filemanager/file/FileListItem;->size:J

    .line 75
    iget-wide v1, p0, Lcom/infraware/filemanager/file/FileListItem;->accessTime:J

    iput-wide v1, v0, Lcom/infraware/filemanager/file/FileListItem;->accessTime:J

    .line 76
    iget v1, p0, Lcom/infraware/filemanager/file/FileListItem;->recentPkey:I

    iput v1, v0, Lcom/infraware/filemanager/file/FileListItem;->recentPkey:I

    .line 78
    iget-boolean v1, p0, Lcom/infraware/filemanager/file/FileListItem;->isFavorite:Z

    iput-boolean v1, v0, Lcom/infraware/filemanager/file/FileListItem;->isFavorite:Z

    .line 80
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListItem;->icon:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/infraware/filemanager/file/FileListItem;->icon:Landroid/graphics/Bitmap;

    .line 81
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListItem;->thumbnail:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/infraware/filemanager/file/FileListItem;->thumbnail:Landroid/graphics/Bitmap;

    .line 83
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListItem;->fileId:Ljava/lang/String;

    iput-object v1, v0, Lcom/infraware/filemanager/file/FileListItem;->fileId:Ljava/lang/String;

    .line 84
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListItem;->parentFileId:Ljava/lang/String;

    iput-object v1, v0, Lcom/infraware/filemanager/file/FileListItem;->parentFileId:Ljava/lang/String;

    .line 85
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListItem;->contentSrc:Ljava/lang/String;

    iput-object v1, v0, Lcom/infraware/filemanager/file/FileListItem;->contentSrc:Ljava/lang/String;

    .line 86
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListItem;->webExt:Ljava/lang/String;

    iput-object v1, v0, Lcom/infraware/filemanager/file/FileListItem;->webExt:Ljava/lang/String;

    .line 87
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListItem;->accountId:Ljava/lang/String;

    iput-object v1, v0, Lcom/infraware/filemanager/file/FileListItem;->accountId:Ljava/lang/String;

    .line 88
    iget-boolean v1, p0, Lcom/infraware/filemanager/file/FileListItem;->isFormTypeImpossibility:Z

    iput-boolean v1, v0, Lcom/infraware/filemanager/file/FileListItem;->isFormTypeImpossibility:Z

    .line 89
    return-object v0
.end method

.method public equal(ILjava/lang/String;)Z
    .locals 2
    .param p1, "type"    # I
    .param p2, "file"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 112
    iget v1, p0, Lcom/infraware/filemanager/file/FileListItem;->type:I

    if-eq v1, p1, :cond_1

    .line 118
    :cond_0
    :goto_0
    return v0

    .line 115
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileListItem;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 118
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public equal(Lcom/infraware/filemanager/file/FileListItem;)Z
    .locals 3
    .param p1, "compareItem"    # Lcom/infraware/filemanager/file/FileListItem;

    .prologue
    const/4 v0, 0x0

    .line 94
    iget v1, p0, Lcom/infraware/filemanager/file/FileListItem;->type:I

    iget v2, p1, Lcom/infraware/filemanager/file/FileListItem;->type:I

    if-eq v1, v2, :cond_1

    .line 107
    :cond_0
    :goto_0
    return v0

    .line 96
    :cond_1
    iget v1, p0, Lcom/infraware/filemanager/file/FileListItem;->serviceType:I

    iget v2, p1, Lcom/infraware/filemanager/file/FileListItem;->serviceType:I

    if-ne v1, v2, :cond_0

    .line 98
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileListItem;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/infraware/filemanager/file/FileListItem;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 101
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListItem;->fileId:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 103
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListItem;->fileId:Ljava/lang/String;

    iget-object v2, p1, Lcom/infraware/filemanager/file/FileListItem;->fileId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 107
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getAbsolutePath()Ljava/lang/String;
    .locals 6

    .prologue
    .line 149
    iget-boolean v3, p0, Lcom/infraware/filemanager/file/FileListItem;->isSDCard:Z

    if-eqz v3, :cond_2

    .line 151
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    .line 152
    .local v0, "internalSDPath":Ljava/lang/String;
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 153
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileListItem;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 172
    .end local v0    # "internalSDPath":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object v2

    .line 156
    :cond_2
    const/4 v2, 0x0

    .line 158
    .local v2, "pathArg":Ljava/lang/String;
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 159
    const-string/jumbo v2, ""

    .line 163
    :goto_1
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileListItem;->getFullFileName()Ljava/lang/String;

    move-result-object v1

    .line 164
    .local v1, "name":Ljava/lang/String;
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_5

    .line 166
    :cond_3
    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 167
    const-string/jumbo v2, "/"

    goto :goto_0

    .line 161
    .end local v1    # "name":Ljava/lang/String;
    :cond_4
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    goto :goto_1

    .line 172
    .restart local v1    # "name":Ljava/lang/String;
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public getFullFileName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileListItem;->webExt:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/filemanager/file/FileListItem;->webExt:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListItem;->webExt:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 183
    :goto_0
    return-object v0

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 181
    :cond_1
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    goto :goto_0

    .line 183
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPath()Ljava/lang/String;
    .locals 4

    .prologue
    .line 214
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    const-string/jumbo v2, "//"

    const-string/jumbo v3, "/"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 215
    .local v0, "convertPath":Ljava/lang/String;
    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 216
    const-string/jumbo v0, ""

    .line 218
    .end local v0    # "convertPath":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public getRealFileName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileListItem;->webExt:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/filemanager/file/FileListItem;->webExt:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 190
    :cond_1
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    .line 195
    :goto_0
    return-object v0

    .line 192
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListItem;->webExt:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 195
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getRealPath()Ljava/lang/String;
    .locals 3

    .prologue
    .line 200
    const/4 v0, 0x0

    .line 202
    .local v0, "pathArg":Ljava/lang/String;
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 203
    const-string/jumbo v0, ""

    .line 209
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileListItem;->getRealFileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    goto :goto_0
.end method

.method public isSupported()Z
    .locals 3

    .prologue
    .line 223
    const/4 v1, 0x0

    .line 224
    .local v1, "isSupport":Z
    iget-boolean v2, p0, Lcom/infraware/filemanager/file/FileListItem;->isFolder:Z

    if-eqz v2, :cond_0

    .line 235
    .end local v1    # "isSupport":Z
    :goto_0
    return v1

    .line 227
    .restart local v1    # "isSupport":Z
    :cond_0
    const/4 v0, 0x0

    .line 228
    .local v0, "extension":Ljava/lang/String;
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileListItem;->webExt:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/infraware/filemanager/file/FileListItem;->webExt:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    .line 229
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileListItem;->webExt:Ljava/lang/String;

    .line 235
    :goto_1
    invoke-static {v0}, Lcom/infraware/common/util/FileUtils;->isSupported(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0

    .line 230
    :cond_1
    iget-object v2, p0, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    .line 231
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    goto :goto_1

    .line 233
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setName(Ljava/lang/String;)V
    .locals 2
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 123
    const/16 v1, 0x2e

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 124
    .local v0, "nIndex":I
    if-lez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x6

    if-le v0, v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-eq v0, v1, :cond_0

    .line 126
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    .line 127
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    .line 131
    :goto_0
    return-void

    .line 130
    :cond_0
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method public setWebName(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "webExt"    # Ljava/lang/String;

    .prologue
    .line 135
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    .line 138
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    .line 139
    iput-object p2, p0, Lcom/infraware/filemanager/file/FileListItem;->webExt:Ljava/lang/String;

    .line 145
    :goto_0
    return-void

    .line 144
    :cond_0
    invoke-virtual {p0, p1}, Lcom/infraware/filemanager/file/FileListItem;->setName(Ljava/lang/String;)V

    goto :goto_0
.end method
