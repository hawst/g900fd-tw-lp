.class Lcom/infraware/filemanager/file/FileBaseActivity$9;
.super Ljava/lang/Object;
.source "FileBaseActivity.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/filemanager/file/FileBaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/filemanager/file/FileBaseActivity;


# direct methods
.method constructor <init>(Lcom/infraware/filemanager/file/FileBaseActivity;)V
    .locals 0

    .prologue
    .line 1278
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$9;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 1282
    if-lez p3, :cond_1

    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$9;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_nFastScrollCount:I
    invoke-static {v0}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$1000(Lcom/infraware/filemanager/file/FileBaseActivity;)I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$9;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_nFastScrollCount:I
    invoke-static {v0}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$1000(Lcom/infraware/filemanager/file/FileBaseActivity;)I

    move-result v0

    if-lt p4, v0, :cond_1

    if-le p4, p3, :cond_1

    .line 1284
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/AbsListView;->setVerticalScrollBarEnabled(Z)V

    .line 1285
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$9;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_nItemPosition:I
    invoke-static {v0}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$1100(Lcom/infraware/filemanager/file/FileBaseActivity;)I

    move-result v0

    if-eq v0, p2, :cond_0

    .line 1286
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileBaseActivity$9;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    sub-int v1, p4, p3

    # invokes: Lcom/infraware/filemanager/file/FileBaseActivity;->showScrollButton(II)V
    invoke-static {v0, p2, v1}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$1200(Lcom/infraware/filemanager/file/FileBaseActivity;II)V

    .line 1290
    :cond_0
    :goto_0
    return-void

    .line 1289
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/AbsListView;->setVerticalScrollBarEnabled(Z)V

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 5
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 1295
    if-eqz p2, :cond_0

    .line 1297
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v0

    .line 1298
    .local v0, "firstVisibleItem":I
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getLastVisiblePosition()I

    move-result v3

    invoke-virtual {p1}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v4

    sub-int/2addr v3, v4

    add-int/lit8 v2, v3, 0x1

    .line 1299
    .local v2, "visibleItemCount":I
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getCount()I

    move-result v1

    .line 1300
    .local v1, "totalItemCount":I
    if-lez v2, :cond_1

    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity$9;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_nFastScrollCount:I
    invoke-static {v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$1000(Lcom/infraware/filemanager/file/FileBaseActivity;)I

    move-result v3

    if-lez v3, :cond_1

    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity$9;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_nFastScrollCount:I
    invoke-static {v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$1000(Lcom/infraware/filemanager/file/FileBaseActivity;)I

    move-result v3

    if-lt v1, v3, :cond_1

    if-le v1, v2, :cond_1

    .line 1302
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/widget/AbsListView;->setVerticalScrollBarEnabled(Z)V

    .line 1303
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity$9;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    sub-int v4, v1, v2

    # invokes: Lcom/infraware/filemanager/file/FileBaseActivity;->showScrollButton(II)V
    invoke-static {v3, v0, v4}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$1200(Lcom/infraware/filemanager/file/FileBaseActivity;II)V

    .line 1308
    .end local v0    # "firstVisibleItem":I
    .end local v1    # "totalItemCount":I
    .end local v2    # "visibleItemCount":I
    :cond_0
    :goto_0
    return-void

    .line 1306
    .restart local v0    # "firstVisibleItem":I
    .restart local v1    # "totalItemCount":I
    .restart local v2    # "visibleItemCount":I
    :cond_1
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Landroid/widget/AbsListView;->setVerticalScrollBarEnabled(Z)V

    goto :goto_0
.end method
