.class Lcom/infraware/filemanager/file/FileInputFilter$1;
.super Ljava/lang/Object;
.source "FileInputFilter.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/filemanager/file/FileInputFilter;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/filemanager/file/FileInputFilter;


# direct methods
.method constructor <init>(Lcom/infraware/filemanager/file/FileInputFilter;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileInputFilter$1;->this$0:Lcom/infraware/filemanager/file/FileInputFilter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 7
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    .line 41
    iget-object v4, p0, Lcom/infraware/filemanager/file/FileInputFilter$1;->this$0:Lcom/infraware/filemanager/file/FileInputFilter;

    # getter for: Lcom/infraware/filemanager/file/FileInputFilter;->m_nMaxLength:I
    invoke-static {v4}, Lcom/infraware/filemanager/file/FileInputFilter;->access$000(Lcom/infraware/filemanager/file/FileInputFilter;)I

    move-result v4

    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v5

    sub-int v6, p6, p5

    sub-int/2addr v5, v6

    sub-int v3, v4, v5

    .line 42
    .local v3, "nRest":I
    if-gez v3, :cond_1

    .line 43
    const/4 v0, 0x0

    .line 100
    :cond_0
    :goto_0
    return-object v0

    .line 46
    :cond_1
    const/4 v1, 0x0

    .line 47
    .local v1, "isPair":Z
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    .line 48
    .local v0, "checked":Ljava/lang/String;
    sub-int v4, p3, p2

    if-lez v4, :cond_a

    .line 50
    const/4 v2, 0x0

    .line 51
    .local v2, "nIdx":I
    :goto_1
    sub-int v4, p3, p2

    if-ge v2, v4, :cond_9

    .line 53
    iget-object v4, p0, Lcom/infraware/filemanager/file/FileInputFilter$1;->this$0:Lcom/infraware/filemanager/file/FileInputFilter;

    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    # invokes: Lcom/infraware/filemanager/file/FileInputFilter;->isValidChar(C)Z
    invoke-static {v4, v5}, Lcom/infraware/filemanager/file/FileInputFilter;->access$100(Lcom/infraware/filemanager/file/FileInputFilter;C)Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_5

    .line 55
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v4, v3, :cond_3

    .line 58
    if-lez v2, :cond_2

    add-int/lit8 v4, v2, -0x1

    invoke-interface {p1, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 59
    iget-object v4, p0, Lcom/infraware/filemanager/file/FileInputFilter$1;->this$0:Lcom/infraware/filemanager/file/FileInputFilter;

    const/16 v5, -0xa

    # invokes: Lcom/infraware/filemanager/file/FileInputFilter;->onToastMessage(I)V
    invoke-static {v4, v5}, Lcom/infraware/filemanager/file/FileInputFilter;->access$200(Lcom/infraware/filemanager/file/FileInputFilter;I)V

    .line 60
    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 64
    :cond_2
    iget-object v4, p0, Lcom/infraware/filemanager/file/FileInputFilter$1;->this$0:Lcom/infraware/filemanager/file/FileInputFilter;

    const/16 v5, -0xa

    # invokes: Lcom/infraware/filemanager/file/FileInputFilter;->onToastMessage(I)V
    invoke-static {v4, v5}, Lcom/infraware/filemanager/file/FileInputFilter;->access$200(Lcom/infraware/filemanager/file/FileInputFilter;I)V

    goto :goto_0

    .line 69
    :cond_3
    add-int/lit8 v4, v2, 0x1

    sub-int v5, p3, p2

    if-ge v4, v5, :cond_8

    .line 70
    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    add-int/lit8 v5, v2, 0x1

    invoke-interface {p1, v5}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    move-result v4

    if-nez v4, :cond_7

    .line 71
    if-eqz v2, :cond_4

    add-int/lit8 v4, v2, -0x1

    invoke-interface {p1, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    move-result v4

    if-nez v4, :cond_6

    .line 72
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 73
    const/4 v1, 0x0

    .line 89
    :cond_5
    :goto_2
    iget-object v4, p0, Lcom/infraware/filemanager/file/FileInputFilter$1;->this$0:Lcom/infraware/filemanager/file/FileInputFilter;

    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    # invokes: Lcom/infraware/filemanager/file/FileInputFilter;->isCarriageReturn(C)Z
    invoke-static {v4, v5}, Lcom/infraware/filemanager/file/FileInputFilter;->access$300(Lcom/infraware/filemanager/file/FileInputFilter;C)Z

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_0

    .line 92
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 76
    :cond_6
    const/4 v1, 0x1

    goto :goto_2

    .line 79
    :cond_7
    const/4 v1, 0x1

    goto :goto_2

    .line 82
    :cond_8
    if-nez v1, :cond_5

    .line 83
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 94
    :cond_9
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_a

    .line 96
    iget-object v4, p0, Lcom/infraware/filemanager/file/FileInputFilter$1;->this$0:Lcom/infraware/filemanager/file/FileInputFilter;

    const/16 v5, -0x9

    # invokes: Lcom/infraware/filemanager/file/FileInputFilter;->onToastMessage(I)V
    invoke-static {v4, v5}, Lcom/infraware/filemanager/file/FileInputFilter;->access$200(Lcom/infraware/filemanager/file/FileInputFilter;I)V

    goto/16 :goto_0

    .line 100
    .end local v2    # "nIdx":I
    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
