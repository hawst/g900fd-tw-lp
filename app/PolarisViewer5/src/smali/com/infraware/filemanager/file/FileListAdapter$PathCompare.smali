.class Lcom/infraware/filemanager/file/FileListAdapter$PathCompare;
.super Ljava/lang/Object;
.source "FileListAdapter.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/filemanager/file/FileListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PathCompare"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/infraware/filemanager/file/FileListItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/filemanager/file/FileListAdapter;


# direct methods
.method private constructor <init>(Lcom/infraware/filemanager/file/FileListAdapter;)V
    .locals 0

    .prologue
    .line 712
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileListAdapter$PathCompare;->this$0:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/infraware/filemanager/file/FileListAdapter;Lcom/infraware/filemanager/file/FileListAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/infraware/filemanager/file/FileListAdapter;
    .param p2, "x1"    # Lcom/infraware/filemanager/file/FileListAdapter$1;

    .prologue
    .line 712
    invoke-direct {p0, p1}, Lcom/infraware/filemanager/file/FileListAdapter$PathCompare;-><init>(Lcom/infraware/filemanager/file/FileListAdapter;)V

    return-void
.end method


# virtual methods
.method public compare(Lcom/infraware/filemanager/file/FileListItem;Lcom/infraware/filemanager/file/FileListItem;)I
    .locals 3
    .param p1, "o1"    # Lcom/infraware/filemanager/file/FileListItem;
    .param p2, "o2"    # Lcom/infraware/filemanager/file/FileListItem;

    .prologue
    .line 715
    iget-object v1, p1, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p2, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 716
    .local v0, "result":I
    if-nez v0, :cond_0

    .line 717
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListAdapter$PathCompare;->this$0:Lcom/infraware/filemanager/file/FileListAdapter;

    # invokes: Lcom/infraware/filemanager/file/FileListAdapter;->nameCompare(Lcom/infraware/filemanager/file/FileListItem;Lcom/infraware/filemanager/file/FileListItem;)I
    invoke-static {v1, p1, p2}, Lcom/infraware/filemanager/file/FileListAdapter;->access$000(Lcom/infraware/filemanager/file/FileListAdapter;Lcom/infraware/filemanager/file/FileListItem;Lcom/infraware/filemanager/file/FileListItem;)I

    move-result v0

    .line 719
    :cond_0
    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 712
    check-cast p1, Lcom/infraware/filemanager/file/FileListItem;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/infraware/filemanager/file/FileListItem;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/infraware/filemanager/file/FileListAdapter$PathCompare;->compare(Lcom/infraware/filemanager/file/FileListItem;Lcom/infraware/filemanager/file/FileListItem;)I

    move-result v0

    return v0
.end method
