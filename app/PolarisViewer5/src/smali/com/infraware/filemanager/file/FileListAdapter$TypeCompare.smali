.class Lcom/infraware/filemanager/file/FileListAdapter$TypeCompare;
.super Ljava/lang/Object;
.source "FileListAdapter.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/filemanager/file/FileListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TypeCompare"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/infraware/filemanager/file/FileListItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/filemanager/file/FileListAdapter;


# direct methods
.method private constructor <init>(Lcom/infraware/filemanager/file/FileListAdapter;)V
    .locals 0

    .prologue
    .line 734
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileListAdapter$TypeCompare;->this$0:Lcom/infraware/filemanager/file/FileListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/infraware/filemanager/file/FileListAdapter;Lcom/infraware/filemanager/file/FileListAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/infraware/filemanager/file/FileListAdapter;
    .param p2, "x1"    # Lcom/infraware/filemanager/file/FileListAdapter$1;

    .prologue
    .line 734
    invoke-direct {p0, p1}, Lcom/infraware/filemanager/file/FileListAdapter$TypeCompare;-><init>(Lcom/infraware/filemanager/file/FileListAdapter;)V

    return-void
.end method


# virtual methods
.method public compare(Lcom/infraware/filemanager/file/FileListItem;Lcom/infraware/filemanager/file/FileListItem;)I
    .locals 5
    .param p1, "o1"    # Lcom/infraware/filemanager/file/FileListItem;
    .param p2, "o2"    # Lcom/infraware/filemanager/file/FileListItem;

    .prologue
    .line 737
    iget-object v3, p1, Lcom/infraware/filemanager/file/FileListItem;->webExt:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    iget-object v0, p1, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    .line 738
    .local v0, "ext1":Ljava/lang/String;
    :goto_0
    iget-object v3, p2, Lcom/infraware/filemanager/file/FileListItem;->webExt:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_2

    iget-object v1, p2, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    .line 740
    .local v1, "ext2":Ljava/lang/String;
    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    .line 741
    .local v2, "result":I
    if-nez v2, :cond_0

    .line 742
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileListAdapter$TypeCompare;->this$0:Lcom/infraware/filemanager/file/FileListAdapter;

    # invokes: Lcom/infraware/filemanager/file/FileListAdapter;->nameCompare(Lcom/infraware/filemanager/file/FileListItem;Lcom/infraware/filemanager/file/FileListItem;)I
    invoke-static {v3, p1, p2}, Lcom/infraware/filemanager/file/FileListAdapter;->access$000(Lcom/infraware/filemanager/file/FileListAdapter;Lcom/infraware/filemanager/file/FileListItem;Lcom/infraware/filemanager/file/FileListItem;)I

    move-result v2

    .line 744
    :cond_0
    return v2

    .line 737
    .end local v0    # "ext1":Ljava/lang/String;
    .end local v1    # "ext2":Ljava/lang/String;
    .end local v2    # "result":I
    :cond_1
    iget-object v0, p1, Lcom/infraware/filemanager/file/FileListItem;->webExt:Ljava/lang/String;

    goto :goto_0

    .line 738
    .restart local v0    # "ext1":Ljava/lang/String;
    :cond_2
    iget-object v1, p2, Lcom/infraware/filemanager/file/FileListItem;->webExt:Ljava/lang/String;

    goto :goto_1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 734
    check-cast p1, Lcom/infraware/filemanager/file/FileListItem;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/infraware/filemanager/file/FileListItem;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/infraware/filemanager/file/FileListAdapter$TypeCompare;->compare(Lcom/infraware/filemanager/file/FileListItem;Lcom/infraware/filemanager/file/FileListItem;)I

    move-result v0

    return v0
.end method
