.class Lcom/infraware/filemanager/file/FileBaseActivity$1;
.super Ljava/lang/Object;
.source "FileBaseActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/filemanager/file/FileBaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/filemanager/file/FileBaseActivity;


# direct methods
.method constructor <init>(Lcom/infraware/filemanager/file/FileBaseActivity;)V
    .locals 0

    .prologue
    .line 1083
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$1;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1086
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$1;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_nUpdateState:I
    invoke-static {v1}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$000(Lcom/infraware/filemanager/file/FileBaseActivity;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1136
    :cond_0
    :goto_0
    return-void

    .line 1089
    :pswitch_0
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$1;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # setter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_nUpdateColor:I
    invoke-static {v1, v4}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$102(Lcom/infraware/filemanager/file/FileBaseActivity;I)I

    .line 1091
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$1;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->tag:Ljava/lang/String;
    invoke-static {v1}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$200(Lcom/infraware/filemanager/file/FileBaseActivity;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Show Update progress - WEBProgress ( StorageType [ "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity$1;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget v3, v3, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nStorageType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " ] InteralMode [ "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity$1;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget v3, v3, Lcom/infraware/filemanager/file/FileBaseActivity;->m_nInternalMode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " ] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1092
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->SHOW_FILE_LIST_DIALOG()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1094
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$1;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_bDestroy:Z
    invoke-static {v1}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$300(Lcom/infraware/filemanager/file/FileBaseActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1096
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$1;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    new-instance v2, Landroid/app/ProgressDialog;

    iget-object v3, p0, Lcom/infraware/filemanager/file/FileBaseActivity$1;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    invoke-direct {v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_oUpdateProgress:Landroid/app/ProgressDialog;
    invoke-static {v1, v2}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$402(Lcom/infraware/filemanager/file/FileBaseActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 1097
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$1;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_oUpdateProgress:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$400(Lcom/infraware/filemanager/file/FileBaseActivity;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 1098
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$1;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_oUpdateProgress:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$400(Lcom/infraware/filemanager/file/FileBaseActivity;)Landroid/app/ProgressDialog;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$1;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    const v3, 0x7f070270

    invoke-virtual {v2, v3}, Lcom/infraware/filemanager/file/FileBaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1099
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$1;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_oUpdateProgress:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$400(Lcom/infraware/filemanager/file/FileBaseActivity;)Landroid/app/ProgressDialog;

    move-result-object v1

    new-instance v2, Lcom/infraware/filemanager/file/FileBaseActivity$1$1;

    invoke-direct {v2, p0}, Lcom/infraware/filemanager/file/FileBaseActivity$1$1;-><init>(Lcom/infraware/filemanager/file/FileBaseActivity$1;)V

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 1109
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$1;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_bDestroy:Z
    invoke-static {v1}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$300(Lcom/infraware/filemanager/file/FileBaseActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1111
    :try_start_0
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$1;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_oUpdateProgress:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$400(Lcom/infraware/filemanager/file/FileBaseActivity;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1113
    :catch_0
    move-exception v0

    .line 1114
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 1122
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$1;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v1, v1, Lcom/infraware/filemanager/file/FileBaseActivity;->m_layoutProgress:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 1129
    :pswitch_1
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->SHOW_FILE_LIST_DIALOG()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1130
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$1;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    iget-object v1, v1, Lcom/infraware/filemanager/file/FileBaseActivity;->m_layoutProgress:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/infraware/filemanager/file/FileBaseActivity$1;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    # getter for: Lcom/infraware/filemanager/file/FileBaseActivity;->m_nUpdateColor:I
    invoke-static {v2}, Lcom/infraware/filemanager/file/FileBaseActivity;->access$100(Lcom/infraware/filemanager/file/FileBaseActivity;)I

    move-result v2

    const/high16 v3, 0x1000000

    mul-int/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    goto/16 :goto_0

    .line 1133
    :pswitch_2
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileBaseActivity$1;->this$0:Lcom/infraware/filemanager/file/FileBaseActivity;

    invoke-virtual {v1}, Lcom/infraware/filemanager/file/FileBaseActivity;->offProgressDlg()V

    goto/16 :goto_0

    .line 1086
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
