.class public Lcom/infraware/filemanager/file/FileListAdapter;
.super Landroid/widget/BaseAdapter;
.source "FileListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/filemanager/file/FileListAdapter$1;,
        Lcom/infraware/filemanager/file/FileListAdapter$TypeCompare;,
        Lcom/infraware/filemanager/file/FileListAdapter$DateCompare;,
        Lcom/infraware/filemanager/file/FileListAdapter$PathCompare;,
        Lcom/infraware/filemanager/file/FileListAdapter$SizeCompare;,
        Lcom/infraware/filemanager/file/FileListAdapter$NameCompare;
    }
.end annotation


# static fields
.field static final MARGIIN_NORMAL_RIGHT:I = 0xe

.field static final MARGIIN_SELECT_RIGHT:I = 0x45


# instance fields
.field private colorFile:Landroid/content/res/ColorStateList;

.field private colorInfo:Landroid/content/res/ColorStateList;

.field private colorPrev:Landroid/content/res/ColorStateList;

.field private context:Landroid/content/Context;

.field private m_bAscend:Z

.field private m_bShowExt:Z

.field private m_bShowFavorite:Z

.field private m_listFiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/filemanager/file/FileListItem;",
            ">;"
        }
    .end annotation
.end field

.field private m_nListType:I

.field private m_nSelectMode:I

.field private m_nSort:I

.field private m_oFileIcon:Lcom/infraware/filemanager/file/FileListIcon;

.field private m_oGotoHandler:Landroid/os/Handler;

.field private m_oInflater:Landroid/view/LayoutInflater;

.field private m_oSelectHandler:Landroid/os/Handler;

.field private m_oTypesListHandler:Landroid/os/Handler;

.field private m_savedUpFolder:Lcom/infraware/filemanager/file/FileListItem;

.field private m_strSearchWord:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 75
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 47
    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->context:Landroid/content/Context;

    .line 49
    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_oFileIcon:Lcom/infraware/filemanager/file/FileListIcon;

    .line 51
    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_oSelectHandler:Landroid/os/Handler;

    .line 52
    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_oGotoHandler:Landroid/os/Handler;

    .line 53
    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_oTypesListHandler:Landroid/os/Handler;

    .line 55
    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    .line 57
    iput v2, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_nSort:I

    .line 59
    iput-boolean v2, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_bAscend:Z

    .line 60
    iput-boolean v2, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_bShowExt:Z

    .line 61
    iput-boolean v1, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_bShowFavorite:Z

    .line 63
    iput v1, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_nSelectMode:I

    .line 64
    iput v1, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_nListType:I

    .line 66
    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_savedUpFolder:Lcom/infraware/filemanager/file/FileListItem;

    .line 68
    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_strSearchWord:Ljava/lang/String;

    .line 70
    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->colorFile:Landroid/content/res/ColorStateList;

    .line 71
    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->colorInfo:Landroid/content/res/ColorStateList;

    .line 72
    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->colorPrev:Landroid/content/res/ColorStateList;

    .line 76
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_oInflater:Landroid/view/LayoutInflater;

    .line 77
    new-instance v0, Lcom/infraware/filemanager/file/FileListIcon;

    invoke-direct {v0, p1}, Lcom/infraware/filemanager/file/FileListIcon;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_oFileIcon:Lcom/infraware/filemanager/file/FileListIcon;

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    .line 80
    if-eqz p1, :cond_0

    .line 82
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0501d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->colorFile:Landroid/content/res/ColorStateList;

    .line 83
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0501d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->colorInfo:Landroid/content/res/ColorStateList;

    .line 84
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0501d8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->colorPrev:Landroid/content/res/ColorStateList;

    .line 88
    :cond_0
    iput-object p1, p0, Lcom/infraware/filemanager/file/FileListAdapter;->context:Landroid/content/Context;

    .line 89
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/filemanager/file/FileListAdapter;Lcom/infraware/filemanager/file/FileListItem;Lcom/infraware/filemanager/file/FileListItem;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/filemanager/file/FileListAdapter;
    .param p1, "x1"    # Lcom/infraware/filemanager/file/FileListItem;
    .param p2, "x2"    # Lcom/infraware/filemanager/file/FileListItem;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/infraware/filemanager/file/FileListAdapter;->nameCompare(Lcom/infraware/filemanager/file/FileListItem;Lcom/infraware/filemanager/file/FileListItem;)I

    move-result v0

    return v0
.end method

.method private getFileUpdateTime(J)Ljava/lang/String;
    .locals 1
    .param p1, "updateTime"    # J

    .prologue
    .line 1161
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->context:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/infraware/common/util/FileUtils;->getDateString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getSize(J)Ljava/lang/String;
    .locals 1
    .param p1, "size"    # J

    .prologue
    .line 1157
    invoke-static {p1, p2}, Lcom/infraware/common/util/FileUtils;->getSizeString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private nameCompare(Lcom/infraware/filemanager/file/FileListItem;Lcom/infraware/filemanager/file/FileListItem;)I
    .locals 7
    .param p1, "o1"    # Lcom/infraware/filemanager/file/FileListItem;
    .param p2, "o2"    # Lcom/infraware/filemanager/file/FileListItem;

    .prologue
    .line 673
    iget-object v4, p0, Lcom/infraware/filemanager/file/FileListAdapter;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4}, Lcom/infraware/common/util/Utils;->getCurrentLocaleType(Landroid/content/res/Resources;)I

    move-result v2

    .line 674
    .local v2, "nLocale":I
    const/4 v3, 0x0

    .line 675
    .local v3, "result":I
    const/16 v4, 0x1d

    if-eq v2, v4, :cond_0

    const/16 v4, 0x1e

    if-eq v2, v4, :cond_0

    const/16 v4, 0x1c

    if-ne v2, v4, :cond_2

    .line 678
    :cond_0
    new-instance v4, Ljava/util/Locale;

    const-string/jumbo v5, "zh"

    invoke-direct {v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v4

    iget-object v5, p1, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p2, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 681
    :goto_0
    if-nez v3, :cond_1

    .line 683
    iget-object v4, p1, Lcom/infraware/filemanager/file/FileListItem;->webExt:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_3

    iget-object v0, p1, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    .line 684
    .local v0, "ext1":Ljava/lang/String;
    :goto_1
    iget-object v4, p2, Lcom/infraware/filemanager/file/FileListItem;->webExt:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_4

    iget-object v1, p2, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    .line 686
    .local v1, "ext2":Ljava/lang/String;
    :goto_2
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    .line 689
    .end local v0    # "ext1":Ljava/lang/String;
    .end local v1    # "ext2":Ljava/lang/String;
    :cond_1
    return v3

    .line 680
    :cond_2
    iget-object v4, p1, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p2, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    goto :goto_0

    .line 683
    :cond_3
    iget-object v0, p1, Lcom/infraware/filemanager/file/FileListItem;->webExt:Ljava/lang/String;

    goto :goto_1

    .line 684
    .restart local v0    # "ext1":Ljava/lang/String;
    :cond_4
    iget-object v1, p2, Lcom/infraware/filemanager/file/FileListItem;->webExt:Ljava/lang/String;

    goto :goto_2
.end method


# virtual methods
.method public addList(Lcom/infraware/filemanager/file/FileListItem;)V
    .locals 1
    .param p1, "item"    # Lcom/infraware/filemanager/file/FileListItem;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    return-void
.end method

.method public clearList()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 93
    return-void
.end method

.method public clearSelectedList()V
    .locals 4

    .prologue
    .line 898
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 899
    .local v0, "nCount":I
    const/4 v1, 0x0

    .line 901
    .local v1, "nIndex":I
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    .line 903
    invoke-virtual {p0, v1}, Lcom/infraware/filemanager/file/FileListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/filemanager/file/FileListItem;

    .line 904
    .local v2, "tmpItem":Lcom/infraware/filemanager/file/FileListItem;
    if-eqz v2, :cond_0

    .line 905
    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/infraware/filemanager/file/FileListItem;->isSelect:Z

    .line 901
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 908
    .end local v2    # "tmpItem":Lcom/infraware/filemanager/file/FileListItem;
    :cond_1
    return-void
.end method

.method public collapse(Lcom/infraware/filemanager/file/FileListItem;)V
    .locals 1
    .param p1, "item"    # Lcom/infraware/filemanager/file/FileListItem;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 101
    return-void
.end method

.method public expand(Lcom/infraware/filemanager/file/FileListItem;)V
    .locals 1
    .param p1, "item"    # Lcom/infraware/filemanager/file/FileListItem;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    return-void
.end method

.method public findItem(Lcom/infraware/filemanager/file/FileListItem;)Lcom/infraware/filemanager/file/FileListItem;
    .locals 4
    .param p1, "findItem"    # Lcom/infraware/filemanager/file/FileListItem;

    .prologue
    const/4 v2, 0x0

    .line 170
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 172
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/filemanager/file/FileListItem;

    .line 173
    .local v1, "item":Lcom/infraware/filemanager/file/FileListItem;
    if-nez v1, :cond_1

    move-object v1, v2

    .line 180
    .end local v1    # "item":Lcom/infraware/filemanager/file/FileListItem;
    :cond_0
    :goto_1
    return-object v1

    .line 176
    .restart local v1    # "item":Lcom/infraware/filemanager/file/FileListItem;
    :cond_1
    invoke-virtual {v1, p1}, Lcom/infraware/filemanager/file/FileListItem;->equal(Lcom/infraware/filemanager/file/FileListItem;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 170
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .end local v1    # "item":Lcom/infraware/filemanager/file/FileListItem;
    :cond_2
    move-object v1, v2

    .line 180
    goto :goto_1
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getFileCount()I
    .locals 4

    .prologue
    .line 134
    const/4 v0, 0x0

    .line 136
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 138
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/filemanager/file/FileListItem;

    .line 139
    .local v2, "item":Lcom/infraware/filemanager/file/FileListItem;
    iget-boolean v3, v2, Lcom/infraware/filemanager/file/FileListItem;->isFolder:Z

    if-nez v3, :cond_0

    .line 140
    add-int/lit8 v0, v0, 0x1

    .line 136
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 142
    .end local v2    # "item":Lcom/infraware/filemanager/file/FileListItem;
    :cond_1
    return v0
.end method

.method public getIcon(Lcom/infraware/filemanager/file/FileListItem;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "item"    # Lcom/infraware/filemanager/file/FileListItem;

    .prologue
    .line 650
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_oFileIcon:Lcom/infraware/filemanager/file/FileListIcon;

    if-nez v3, :cond_0

    .line 651
    const/4 v1, 0x0

    .line 666
    :goto_0
    return-object v1

    .line 653
    :cond_0
    const/4 v1, 0x0

    .line 654
    .local v1, "icon":Landroid/graphics/Bitmap;
    iget-object v3, p1, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    invoke-static {v3}, Lcom/infraware/filemanager/porting/DRM;->isDRMExt(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 656
    invoke-static {}, Lcom/infraware/filemanager/porting/DRM;->getInstance()Lcom/infraware/filemanager/porting/DRM;

    move-result-object v0

    .line 657
    .local v0, "drm":Lcom/infraware/filemanager/porting/DRM;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p1, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/infraware/filemanager/file/FileListItem;->getFullFileName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 658
    .local v2, "strFileName":Ljava/lang/String;
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileListAdapter;->context:Landroid/content/Context;

    invoke-virtual {v0, v2, v3}, Lcom/infraware/filemanager/porting/DRM;->isDRM(Ljava/lang/String;Landroid/content/Context;)I

    move-result v3

    if-lez v3, :cond_1

    .line 659
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_oFileIcon:Lcom/infraware/filemanager/file/FileListIcon;

    invoke-virtual {v3, v0, p1}, Lcom/infraware/filemanager/file/FileListIcon;->getDRMFileIcon(Lcom/infraware/filemanager/porting/DRM;Lcom/infraware/filemanager/file/FileListItem;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0

    .line 661
    :cond_1
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_oFileIcon:Lcom/infraware/filemanager/file/FileListIcon;

    invoke-virtual {v3, p1}, Lcom/infraware/filemanager/file/FileListIcon;->getFileIcon(Lcom/infraware/filemanager/file/FileListItem;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0

    .line 664
    .end local v0    # "drm":Lcom/infraware/filemanager/porting/DRM;
    .end local v2    # "strFileName":Ljava/lang/String;
    :cond_2
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_oFileIcon:Lcom/infraware/filemanager/file/FileListIcon;

    invoke-virtual {v3, p1}, Lcom/infraware/filemanager/file/FileListIcon;->getFileIcon(Lcom/infraware/filemanager/file/FileListItem;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 158
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 159
    const/4 v0, 0x0

    .line 161
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 165
    int-to-long v0, p1

    return-wide v0
.end method

.method public getList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/filemanager/file/FileListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getManageCount()I
    .locals 5

    .prologue
    .line 146
    const/4 v0, 0x0

    .line 148
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 150
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/filemanager/file/FileListItem;

    .line 151
    .local v2, "item":Lcom/infraware/filemanager/file/FileListItem;
    iget-boolean v3, v2, Lcom/infraware/filemanager/file/FileListItem;->isStorageTitle:Z

    if-nez v3, :cond_0

    iget v3, v2, Lcom/infraware/filemanager/file/FileListItem;->type:I

    const/4 v4, 0x6

    if-eq v3, v4, :cond_0

    .line 152
    add-int/lit8 v0, v0, 0x1

    .line 148
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 154
    .end local v2    # "item":Lcom/infraware/filemanager/file/FileListItem;
    :cond_1
    return v0
.end method

.method public getPosition(Lcom/infraware/filemanager/file/FileListItem;)I
    .locals 5
    .param p1, "item"    # Lcom/infraware/filemanager/file/FileListItem;

    .prologue
    .line 184
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 185
    .local v0, "nCount":I
    const/4 v1, 0x0

    .line 187
    .local v1, "nIndex":I
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    .line 189
    invoke-virtual {p0, v1}, Lcom/infraware/filemanager/file/FileListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/filemanager/file/FileListItem;

    .line 190
    .local v2, "tmpItem":Lcom/infraware/filemanager/file/FileListItem;
    if-eqz v2, :cond_0

    iget-object v3, v2, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    iget-object v4, p1, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v2, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    iget-object v4, p1, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_0

    .line 196
    .end local v1    # "nIndex":I
    .end local v2    # "tmpItem":Lcom/infraware/filemanager/file/FileListItem;
    :goto_1
    return v1

    .line 187
    .restart local v1    # "nIndex":I
    .restart local v2    # "tmpItem":Lcom/infraware/filemanager/file/FileListItem;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 196
    .end local v2    # "tmpItem":Lcom/infraware/filemanager/file/FileListItem;
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public getShowExt()Z
    .locals 1

    .prologue
    .line 781
    iget-boolean v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_bShowExt:Z

    return v0
.end method

.method public getShowFavorite()Z
    .locals 1

    .prologue
    .line 772
    iget-boolean v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_bShowFavorite:Z

    return v0
.end method

.method public getSortField()I
    .locals 1

    .prologue
    .line 753
    iget v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_nSort:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x0

    .line 202
    if-nez p2, :cond_2

    .line 204
    iget-object v5, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_oInflater:Landroid/view/LayoutInflater;

    const v6, 0x7f030019

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 208
    new-instance v1, Lcom/infraware/filemanager/file/FileListHolder;

    invoke-direct {v1}, Lcom/infraware/filemanager/file/FileListHolder;-><init>()V

    .line 209
    .local v1, "holder":Lcom/infraware/filemanager/file/FileListHolder;
    const v5, 0x7f0b00d4

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_layoutBG:Landroid/widget/LinearLayout;

    .line 211
    const v5, 0x7f0b00d9

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_textName:Landroid/widget/TextView;

    .line 212
    const v5, 0x7f0b00da

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_textTime:Landroid/widget/TextView;

    .line 213
    const v5, 0x7f0b00db

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_textSize:Landroid/widget/TextView;

    .line 214
    const v5, 0x7f0b00dc

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_textPath:Landroid/widget/TextView;

    .line 215
    const v5, 0x7f0b00d6

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_imageIcon:Landroid/widget/ImageView;

    .line 216
    const v5, 0x7f0b00d7

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_imageSubIcon:Landroid/widget/ImageView;

    .line 232
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 237
    :goto_0
    move-object v4, p2

    .line 240
    .local v4, "localView":Landroid/view/View;
    iget-object v5, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/filemanager/file/FileListItem;

    .line 242
    .local v2, "item":Lcom/infraware/filemanager/file/FileListItem;
    const/high16 v5, 0x7f080000

    invoke-virtual {v4, v5, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 252
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_textName:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/infraware/filemanager/file/FileListAdapter;->colorFile:Landroid/content/res/ColorStateList;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 253
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_textName:Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 255
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_imageIcon:Landroid/widget/ImageView;

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 257
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_imageSubIcon:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 259
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_textTime:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 260
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_textSize:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 261
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_textPath:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 263
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_textTime:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/infraware/filemanager/file/FileListAdapter;->colorInfo:Landroid/content/res/ColorStateList;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 264
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_textSize:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/infraware/filemanager/file/FileListAdapter;->colorInfo:Landroid/content/res/ColorStateList;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 265
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_textPath:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/infraware/filemanager/file/FileListAdapter;->colorInfo:Landroid/content/res/ColorStateList;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 267
    iget-boolean v5, v2, Lcom/infraware/filemanager/file/FileListItem;->isStorageTitle:Z

    if-nez v5, :cond_3

    iget v5, v2, Lcom/infraware/filemanager/file/FileListItem;->type:I

    const/4 v6, 0x5

    if-ne v5, v6, :cond_3

    .line 269
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_layoutBG:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 270
    .local v3, "layoutParam":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v5, p0, Lcom/infraware/filemanager/file/FileListAdapter;->context:Landroid/content/Context;

    const v6, 0x423aae14    # 46.67f

    invoke-static {v5, v6}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    iput v5, v3, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 272
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_layoutBG:Landroid/widget/LinearLayout;

    const v6, 0x7f02012d

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 282
    .end local v3    # "layoutParam":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    :goto_1
    iget-boolean v5, v2, Lcom/infraware/filemanager/file/FileListItem;->isStorageTitle:Z

    if-eqz v5, :cond_4

    .line 645
    :cond_1
    :goto_2
    return-object v4

    .line 235
    .end local v1    # "holder":Lcom/infraware/filemanager/file/FileListHolder;
    .end local v2    # "item":Lcom/infraware/filemanager/file/FileListItem;
    .end local v4    # "localView":Landroid/view/View;
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/filemanager/file/FileListHolder;

    .restart local v1    # "holder":Lcom/infraware/filemanager/file/FileListHolder;
    goto :goto_0

    .line 274
    .restart local v2    # "item":Lcom/infraware/filemanager/file/FileListItem;
    .restart local v4    # "localView":Landroid/view/View;
    :cond_3
    iget-boolean v5, v2, Lcom/infraware/filemanager/file/FileListItem;->isStorageTitle:Z

    if-nez v5, :cond_0

    .line 276
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_layoutBG:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 277
    .restart local v3    # "layoutParam":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v5, -0x2

    iput v5, v3, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 279
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_layoutBG:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v8}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    goto :goto_1

    .line 424
    .end local v3    # "layoutParam":Landroid/widget/LinearLayout$LayoutParams;
    :cond_4
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_layoutBG:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 425
    iget-object v0, v2, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    .line 426
    .local v0, "fileName":Ljava/lang/String;
    iget-boolean v5, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_bShowExt:Z

    if-eqz v5, :cond_5

    iget-boolean v5, v2, Lcom/infraware/filemanager/file/FileListItem;->isFolder:Z

    if-nez v5, :cond_5

    iget-object v5, v2, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    if-eqz v5, :cond_5

    iget-object v5, v2, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_5

    .line 427
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x2e

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 429
    :cond_5
    iget-boolean v5, v2, Lcom/infraware/filemanager/file/FileListItem;->isSyncFolder:Z

    if-eqz v5, :cond_6

    .line 430
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_textName:Landroid/widget/TextView;

    const v6, 0x7f07002d

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    .line 434
    :goto_3
    iget-object v5, v2, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    const-string/jumbo v6, ".."

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_7

    .line 436
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_textName:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/infraware/filemanager/file/FileListAdapter;->colorPrev:Landroid/content/res/ColorStateList;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 437
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_imageIcon:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_oFileIcon:Lcom/infraware/filemanager/file/FileListIcon;

    iget-object v7, v2, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/infraware/filemanager/file/FileListIcon;->getFolderIcon(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_2

    .line 432
    :cond_6
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_textName:Landroid/widget/TextView;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 441
    :cond_7
    iget-boolean v5, v2, Lcom/infraware/filemanager/file/FileListItem;->isFolder:Z

    if-eqz v5, :cond_9

    .line 443
    iget-boolean v5, v2, Lcom/infraware/filemanager/file/FileListItem;->isSDCard:Z

    if-eqz v5, :cond_8

    iget-object v5, v2, Lcom/infraware/filemanager/file/FileListItem;->icon:Landroid/graphics/Bitmap;

    if-eqz v5, :cond_8

    .line 445
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_imageIcon:Landroid/widget/ImageView;

    iget-object v6, v2, Lcom/infraware/filemanager/file/FileListItem;->icon:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 446
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_textTime:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 519
    :goto_4
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->SHOW_FILE_PATH()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 521
    iget v5, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_nListType:I

    packed-switch v5, :pswitch_data_0

    goto/16 :goto_2

    .line 525
    :pswitch_0
    iget-boolean v5, v2, Lcom/infraware/filemanager/file/FileListItem;->isFolder:Z

    if-nez v5, :cond_1

    .line 527
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_textSize:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 528
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_textTime:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 529
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_textPath:Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 530
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_textPath:Landroid/widget/TextView;

    iget-object v6, v2, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 456
    :cond_8
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_imageIcon:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_oFileIcon:Lcom/infraware/filemanager/file/FileListIcon;

    iget-object v7, v2, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/infraware/filemanager/file/FileListIcon;->getFolderIcon(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 457
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_textTime:Landroid/widget/TextView;

    iget-wide v6, v2, Lcom/infraware/filemanager/file/FileListItem;->updateTime:J

    invoke-direct {p0, v6, v7}, Lcom/infraware/filemanager/file/FileListAdapter;->getFileUpdateTime(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 458
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_textTime:Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    .line 487
    :cond_9
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_imageIcon:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_oFileIcon:Lcom/infraware/filemanager/file/FileListIcon;

    invoke-virtual {v6, v2}, Lcom/infraware/filemanager/file/FileListIcon;->getFileIcon(Lcom/infraware/filemanager/file/FileListItem;)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 488
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_imageSubIcon:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 500
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_textTime:Landroid/widget/TextView;

    iget-wide v6, v2, Lcom/infraware/filemanager/file/FileListItem;->updateTime:J

    invoke-direct {p0, v6, v7}, Lcom/infraware/filemanager/file/FileListAdapter;->getFileUpdateTime(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 501
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_textTime:Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 502
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_textSize:Landroid/widget/TextView;

    iget-wide v6, v2, Lcom/infraware/filemanager/file/FileListItem;->size:J

    invoke-direct {p0, v6, v7}, Lcom/infraware/filemanager/file/FileListAdapter;->getSize(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 503
    iget-object v5, v1, Lcom/infraware/filemanager/file/FileListHolder;->m_textSize:Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    .line 521
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public isAscending()Z
    .locals 1

    .prologue
    .line 758
    iget-boolean v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_bAscend:Z

    return v0
.end method

.method public isEmpty()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1102
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 1112
    :cond_0
    :goto_0
    return v1

    .line 1105
    :cond_1
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v1, :cond_2

    .line 1107
    iget-object v3, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/filemanager/file/FileListItem;

    .line 1108
    .local v0, "item":Lcom/infraware/filemanager/file/FileListItem;
    if-eqz v0, :cond_0

    iget-boolean v3, v0, Lcom/infraware/filemanager/file/FileListItem;->isFolder:Z

    if-eqz v3, :cond_2

    iget-object v3, v0, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    const-string/jumbo v4, ".."

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_0

    .end local v0    # "item":Lcom/infraware/filemanager/file/FileListItem;
    :cond_2
    move v1, v2

    .line 1112
    goto :goto_0
.end method

.method public setAscending(Z)V
    .locals 0
    .param p1, "isAscending"    # Z

    .prologue
    .line 763
    iput-boolean p1, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_bAscend:Z

    .line 764
    return-void
.end method

.method public setList(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/filemanager/file/FileListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 109
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/infraware/filemanager/file/FileListItem;>;"
    iget-object v1, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 110
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileListAdapter;->clearList()V

    .line 114
    :goto_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 116
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 117
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/filemanager/file/FileListItem;

    invoke-virtual {v1}, Lcom/infraware/filemanager/file/FileListItem;->cloneItem()Lcom/infraware/filemanager/file/FileListItem;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/infraware/filemanager/file/FileListAdapter;->addList(Lcom/infraware/filemanager/file/FileListItem;)V

    .line 116
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 112
    .end local v0    # "i":I
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    goto :goto_0

    .line 119
    :cond_1
    return-void
.end method

.method public setShowExt(Z)V
    .locals 0
    .param p1, "isShow"    # Z

    .prologue
    .line 777
    iput-boolean p1, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_bShowExt:Z

    .line 778
    return-void
.end method

.method public setShowFavorite(Z)V
    .locals 0
    .param p1, "isShow"    # Z

    .prologue
    .line 768
    iput-boolean p1, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_bShowFavorite:Z

    .line 769
    return-void
.end method

.method public setSortField(I)V
    .locals 0
    .param p1, "sortField"    # I

    .prologue
    .line 749
    iput p1, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_nSort:I

    .line 750
    return-void
.end method

.method public sortFileList()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    .line 978
    iget v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_nSort:I

    if-eqz v11, :cond_0

    iget v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_nSort:I

    const/4 v12, 0x6

    if-ne v11, v12, :cond_1

    .line 1099
    :cond_0
    return-void

    .line 981
    :cond_1
    const/4 v9, 0x0

    .line 982
    .local v9, "selector":Lcom/infraware/filemanager/file/FileListItem;
    const/4 v10, 0x0

    .line 983
    .local v10, "upFolder":Lcom/infraware/filemanager/file/FileListItem;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 984
    .local v1, "SyncFolderList":Ljava/util/List;, "Ljava/util/List<Lcom/infraware/filemanager/file/FileListItem;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 985
    .local v0, "SDCardList":Ljava/util/List;, "Ljava/util/List<Lcom/infraware/filemanager/file/FileListItem;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 986
    .local v4, "folderList":Ljava/util/List;, "Ljava/util/List<Lcom/infraware/filemanager/file/FileListItem;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 988
    .local v3, "fileList":Ljava/util/List;, "Ljava/util/List<Lcom/infraware/filemanager/file/FileListItem;>;"
    iget-object v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 990
    .local v2, "count":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v2, :cond_7

    .line 992
    iget-object v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/infraware/filemanager/file/FileListItem;

    .line 993
    .local v7, "item":Lcom/infraware/filemanager/file/FileListItem;
    iget v11, v7, Lcom/infraware/filemanager/file/FileListItem;->type:I

    const/4 v12, 0x5

    if-ne v11, v12, :cond_2

    .line 995
    move-object v9, v7

    .line 990
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 999
    :cond_2
    iget-boolean v11, v7, Lcom/infraware/filemanager/file/FileListItem;->isFolder:Z

    if-eqz v11, :cond_3

    iget-object v11, v7, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    const-string/jumbo v12, ".."

    invoke-virtual {v11, v12}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v11

    if-nez v11, :cond_3

    .line 1001
    move-object v10, v7

    .line 1002
    goto :goto_1

    .line 1005
    :cond_3
    iget-boolean v11, v7, Lcom/infraware/filemanager/file/FileListItem;->isSDCard:Z

    if-eqz v11, :cond_4

    .line 1007
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1011
    :cond_4
    iget-boolean v11, v7, Lcom/infraware/filemanager/file/FileListItem;->isSyncFolder:Z

    if-eqz v11, :cond_5

    .line 1013
    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1017
    :cond_5
    iget-boolean v11, v7, Lcom/infraware/filemanager/file/FileListItem;->isFolder:Z

    if-eqz v11, :cond_6

    .line 1018
    iget-object v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    invoke-interface {v4, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1020
    :cond_6
    iget-object v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    invoke-interface {v3, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1023
    .end local v7    # "item":Lcom/infraware/filemanager/file/FileListItem;
    :cond_7
    iget-object v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->clear()V

    .line 1025
    new-instance v11, Lcom/infraware/filemanager/file/FileListAdapter$NameCompare;

    invoke-direct {v11, p0, v13}, Lcom/infraware/filemanager/file/FileListAdapter$NameCompare;-><init>(Lcom/infraware/filemanager/file/FileListAdapter;Lcom/infraware/filemanager/file/FileListAdapter$1;)V

    invoke-static {v1, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1026
    iget v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_nSort:I

    packed-switch v11, :pswitch_data_0

    .line 1046
    new-instance v11, Lcom/infraware/filemanager/file/FileListAdapter$NameCompare;

    invoke-direct {v11, p0, v13}, Lcom/infraware/filemanager/file/FileListAdapter$NameCompare;-><init>(Lcom/infraware/filemanager/file/FileListAdapter;Lcom/infraware/filemanager/file/FileListAdapter$1;)V

    invoke-static {v4, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1047
    new-instance v11, Lcom/infraware/filemanager/file/FileListAdapter$NameCompare;

    invoke-direct {v11, p0, v13}, Lcom/infraware/filemanager/file/FileListAdapter$NameCompare;-><init>(Lcom/infraware/filemanager/file/FileListAdapter;Lcom/infraware/filemanager/file/FileListAdapter$1;)V

    invoke-static {v3, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1051
    :goto_2
    const/4 v6, 0x0

    .line 1052
    .local v6, "index":I
    if-eqz v9, :cond_8

    .line 1053
    iget-object v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1054
    :cond_8
    if-eqz v10, :cond_9

    .line 1055
    iget-object v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1057
    :cond_9
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 1058
    .local v8, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/infraware/filemanager/file/FileListItem;>;"
    iget-object v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 1059
    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_b

    .line 1061
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/infraware/filemanager/file/FileListItem;

    .line 1062
    .restart local v7    # "item":Lcom/infraware/filemanager/file/FileListItem;
    iget-boolean v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_bAscend:Z

    if-eqz v11, :cond_a

    .line 1063
    iget-object v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v11, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1029
    .end local v6    # "index":I
    .end local v7    # "item":Lcom/infraware/filemanager/file/FileListItem;
    .end local v8    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/infraware/filemanager/file/FileListItem;>;"
    :pswitch_0
    new-instance v11, Lcom/infraware/filemanager/file/FileListAdapter$DateCompare;

    invoke-direct {v11, p0, v13}, Lcom/infraware/filemanager/file/FileListAdapter$DateCompare;-><init>(Lcom/infraware/filemanager/file/FileListAdapter;Lcom/infraware/filemanager/file/FileListAdapter$1;)V

    invoke-static {v4, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1030
    new-instance v11, Lcom/infraware/filemanager/file/FileListAdapter$DateCompare;

    invoke-direct {v11, p0, v13}, Lcom/infraware/filemanager/file/FileListAdapter$DateCompare;-><init>(Lcom/infraware/filemanager/file/FileListAdapter;Lcom/infraware/filemanager/file/FileListAdapter$1;)V

    invoke-static {v3, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_2

    .line 1033
    :pswitch_1
    new-instance v11, Lcom/infraware/filemanager/file/FileListAdapter$NameCompare;

    invoke-direct {v11, p0, v13}, Lcom/infraware/filemanager/file/FileListAdapter$NameCompare;-><init>(Lcom/infraware/filemanager/file/FileListAdapter;Lcom/infraware/filemanager/file/FileListAdapter$1;)V

    invoke-static {v4, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1034
    new-instance v11, Lcom/infraware/filemanager/file/FileListAdapter$SizeCompare;

    invoke-direct {v11, p0, v13}, Lcom/infraware/filemanager/file/FileListAdapter$SizeCompare;-><init>(Lcom/infraware/filemanager/file/FileListAdapter;Lcom/infraware/filemanager/file/FileListAdapter$1;)V

    invoke-static {v3, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_2

    .line 1037
    :pswitch_2
    new-instance v11, Lcom/infraware/filemanager/file/FileListAdapter$PathCompare;

    invoke-direct {v11, p0, v13}, Lcom/infraware/filemanager/file/FileListAdapter$PathCompare;-><init>(Lcom/infraware/filemanager/file/FileListAdapter;Lcom/infraware/filemanager/file/FileListAdapter$1;)V

    invoke-static {v4, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1038
    new-instance v11, Lcom/infraware/filemanager/file/FileListAdapter$PathCompare;

    invoke-direct {v11, p0, v13}, Lcom/infraware/filemanager/file/FileListAdapter$PathCompare;-><init>(Lcom/infraware/filemanager/file/FileListAdapter;Lcom/infraware/filemanager/file/FileListAdapter$1;)V

    invoke-static {v3, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_2

    .line 1041
    :pswitch_3
    new-instance v11, Lcom/infraware/filemanager/file/FileListAdapter$NameCompare;

    invoke-direct {v11, p0, v13}, Lcom/infraware/filemanager/file/FileListAdapter$NameCompare;-><init>(Lcom/infraware/filemanager/file/FileListAdapter;Lcom/infraware/filemanager/file/FileListAdapter$1;)V

    invoke-static {v4, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1042
    new-instance v11, Lcom/infraware/filemanager/file/FileListAdapter$TypeCompare;

    invoke-direct {v11, p0, v13}, Lcom/infraware/filemanager/file/FileListAdapter$TypeCompare;-><init>(Lcom/infraware/filemanager/file/FileListAdapter;Lcom/infraware/filemanager/file/FileListAdapter$1;)V

    invoke-static {v3, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_2

    .line 1065
    .restart local v6    # "index":I
    .restart local v7    # "item":Lcom/infraware/filemanager/file/FileListItem;
    .restart local v8    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/infraware/filemanager/file/FileListItem;>;"
    :cond_a
    iget-object v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v11, v6, v7}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_3

    .line 1068
    .end local v7    # "item":Lcom/infraware/filemanager/file/FileListItem;
    :cond_b
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 1069
    iget-object v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 1070
    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_d

    .line 1072
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/infraware/filemanager/file/FileListItem;

    .line 1073
    .restart local v7    # "item":Lcom/infraware/filemanager/file/FileListItem;
    iget-boolean v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_bAscend:Z

    if-eqz v11, :cond_c

    .line 1074
    iget-object v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v11, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1076
    :cond_c
    iget-object v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v11, v6, v7}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_4

    .line 1079
    .end local v7    # "item":Lcom/infraware/filemanager/file/FileListItem;
    :cond_d
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 1080
    iget-object v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 1081
    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_10

    .line 1083
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/infraware/filemanager/file/FileListItem;

    .line 1084
    .restart local v7    # "item":Lcom/infraware/filemanager/file/FileListItem;
    iget-boolean v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_bAscend:Z

    if-nez v11, :cond_e

    iget v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_nSort:I

    const/4 v12, 0x3

    if-ne v11, v12, :cond_f

    .line 1085
    :cond_e
    iget-object v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v11, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 1087
    :cond_f
    iget-object v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v11, v6, v7}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_5

    .line 1090
    .end local v7    # "item":Lcom/infraware/filemanager/file/FileListItem;
    :cond_10
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 1091
    iget-object v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 1092
    :goto_6
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1094
    iget-boolean v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_bAscend:Z

    if-eqz v11, :cond_11

    .line 1095
    iget-object v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 1097
    :cond_11
    iget-object v11, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v11, v6, v12}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_6

    .line 1026
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public updateList(ILcom/infraware/filemanager/file/FileListItem;)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "item"    # Lcom/infraware/filemanager/file/FileListItem;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/infraware/filemanager/file/FileListAdapter;->m_listFiles:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 105
    invoke-virtual {p0}, Lcom/infraware/filemanager/file/FileListAdapter;->notifyDataSetChanged()V

    .line 106
    return-void
.end method
