.class public final Lcom/infraware/polarisviewer5/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisviewer5/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final BookClipListView:I = 0x7f0b002d

.field public static final CheckBox01:I = 0x7f0b0109

.field public static final ColorItemText5:I = 0x7f0b010a

.field public static final EvBaseView:I = 0x7f0b0119

.field public static final EvBaseViewLayout:I = 0x7f0b0118

.field public static final EvBaseView_dummy_imgview:I = 0x7f0b011a

.field public static final LinearLayout01:I = 0x7f0b0087

.field public static final LinearLayout02:I = 0x7f0b0079

.field public static final LinearLayout1:I = 0x7f0b0162

.field public static final ListItemColor2:I = 0x7f0b010b

.field public static final ListItemIcon1:I = 0x7f0b0106

.field public static final ListItemImageView:I = 0x7f0b010c

.field public static final ListItemText1:I = 0x7f0b0104

.field public static final ListItemText2:I = 0x7f0b010d

.field public static final ListSearchOption:I = 0x7f0b01d6

.field public static final ListSearchOptionButton:I = 0x7f0b01d7

.field public static final RInputBar:I = 0x7f0b0029

.field public static final RightButton:I = 0x7f0b0105

.field public static final ScrollView01:I = 0x7f0b0078

.field public static final TitleEraserAllBtn:I = 0x7f0b0114

.field public static final TitleEraserBtn:I = 0x7f0b0113

.field public static final TitlePenBtn:I = 0x7f0b0111

.field public static final TitlelassoBtn:I = 0x7f0b0112

.field public static final actionBar:I = 0x7f0b0175

.field public static final actionList:I = 0x7f0b0222

.field public static final actionbar_draw:I = 0x7f0b0008

.field public static final actionbar_find:I = 0x7f0b0009

.field public static final actionbar_menu:I = 0x7f0b000a

.field public static final actionbar_redo:I = 0x7f0b0006

.field public static final actionbar_rightlayout:I = 0x7f0b0007

.field public static final actionbar_save:I = 0x7f0b0002

.field public static final actionbar_title:I = 0x7f0b0003

.field public static final actionbar_undo:I = 0x7f0b0005

.field public static final actionbar_undoredolayout:I = 0x7f0b0004

.field public static final actionbar_view_actionbar:I = 0x7f0b0001

.field public static final actionbar_view_root:I = 0x7f0b0000

.field public static final actiontitlebar_layout:I = 0x7f0b001d

.field public static final after_check:I = 0x7f0b01c0

.field public static final animation_hand:I = 0x7f0b0147

.field public static final annot_btn_delete:I = 0x7f0b0020

.field public static final annot_btn_delete_image:I = 0x7f0b0021

.field public static final annot_btn_delete_text:I = 0x7f0b0022

.field public static final annot_check:I = 0x7f0b001c

.field public static final annot_icon:I = 0x7f0b0018

.field public static final annot_item_indicator:I = 0x7f0b0017

.field public static final annot_item_page:I = 0x7f0b001b

.field public static final annot_item_text:I = 0x7f0b0019

.field public static final annot_item_time:I = 0x7f0b001a

.field public static final annot_list_view:I = 0x7f0b001e

.field public static final annot_ll_btn:I = 0x7f0b001f

.field public static final annot_next_btn:I = 0x7f0b0136

.field public static final annot_prev_btn:I = 0x7f0b0137

.field public static final autofilterLayout:I = 0x7f0b01db

.field public static final backward_contextmenu:I = 0x7f0b0283

.field public static final before_image:I = 0x7f0b01be

.field public static final body:I = 0x7f0b0168

.field public static final bookclip_add_button:I = 0x7f0b002a

.field public static final bookclip_edit:I = 0x7f0b002b

.field public static final bookmark_ll_empty:I = 0x7f0b0026

.field public static final bookmark_ll_empty2:I = 0x7f0b002e

.field public static final boomark_empty_image2:I = 0x7f0b002f

.field public static final boomark_empty_text2:I = 0x7f0b0030

.field public static final broadcast_mode_marker:I = 0x7f0b0140

.field public static final broadcast_mode_normal:I = 0x7f0b013e

.field public static final broadcast_mode_option:I = 0x7f0b013d

.field public static final broadcast_mode_pointer:I = 0x7f0b013f

.field public static final broadcast_pointer_color01:I = 0x7f0b0142

.field public static final broadcast_pointer_color02:I = 0x7f0b0143

.field public static final broadcast_pointer_color03:I = 0x7f0b0144

.field public static final broadcast_pointer_color04:I = 0x7f0b0145

.field public static final broadcast_pointer_color05:I = 0x7f0b0146

.field public static final broadcast_pointer_option:I = 0x7f0b0141

.field public static final btnDone:I = 0x7f0b0231

.field public static final btnFind:I = 0x7f0b024c

.field public static final btnFindDelete:I = 0x7f0b0254

.field public static final btnFindMode:I = 0x7f0b0246

.field public static final btnFindNextFind:I = 0x7f0b0251

.field public static final btnFindOption:I = 0x7f0b0250

.field public static final btnFindPrevFind:I = 0x7f0b0252

.field public static final btnMenu:I = 0x7f0b024e

.field public static final btnOk:I = 0x7f0b0059

.field public static final btnProperties:I = 0x7f0b024d

.field public static final btnRedo:I = 0x7f0b024b

.field public static final btnReplace:I = 0x7f0b025c

.field public static final btnReplaceAll:I = 0x7f0b025d

.field public static final btnReplaceDelete:I = 0x7f0b025f

.field public static final btnReplaceFindDelete:I = 0x7f0b025a

.field public static final btnReplaceMode:I = 0x7f0b022c

.field public static final btnReplaceNextFind:I = 0x7f0b0258

.field public static final btnReplaceOption:I = 0x7f0b0257

.field public static final btnUndo:I = 0x7f0b024a

.field public static final btn_cancel:I = 0x7f0b0089

.field public static final btn_chg_setting:I = 0x7f0b0177

.field public static final btn_chg_size:I = 0x7f0b0178

.field public static final btn_filter_sortby:I = 0x7f0b01da

.field public static final btn_product_close:I = 0x7f0b01b0

.field public static final btn_product_download:I = 0x7f0b01b2

.field public static final btn_save:I = 0x7f0b0088

.field public static final button_01:I = 0x7f0b0169

.field public static final button_02:I = 0x7f0b016a

.field public static final button_03:I = 0x7f0b016b

.field public static final button_04:I = 0x7f0b016c

.field public static final button_05:I = 0x7f0b016d

.field public static final button_06:I = 0x7f0b016e

.field public static final button_07:I = 0x7f0b016f

.field public static final button_08:I = 0x7f0b0170

.field public static final button_del:I = 0x7f0b0102

.field public static final button_dummey:I = 0x7f0b0171

.field public static final button_empty:I = 0x7f0b0100

.field public static final cancel_btn:I = 0x7f0b01bc

.field public static final cb_info_visibility:I = 0x7f0b01af

.field public static final check:I = 0x7f0b0278

.field public static final checkBox01:I = 0x7f0b0107

.field public static final check_box_alarm_connet_network:I = 0x7f0b003c

.field public static final check_print:I = 0x7f0b01c8

.field public static final check_print_all:I = 0x7f0b01ca

.field public static final check_print_some:I = 0x7f0b01cd

.field public static final chkMatchCase:I = 0x7f0b0228

.field public static final chkMatchWholeWord:I = 0x7f0b022b

.field public static final cm_base_web_view:I = 0x7f0b003d

.field public static final cm_menu_icon:I = 0x7f0b003e

.field public static final cm_menu_list:I = 0x7f0b0041

.field public static final cm_menu_name:I = 0x7f0b003f

.field public static final cm_menu_stat:I = 0x7f0b0040

.field public static final cm_setting_dictionary_list:I = 0x7f0b0043

.field public static final cm_setting_list:I = 0x7f0b0042

.field public static final cm_setting_title:I = 0x7f0b0044

.field public static final cm_title_bar:I = 0x7f0b0031

.field public static final cm_title_button:I = 0x7f0b003a

.field public static final cm_title_image:I = 0x7f0b0032

.field public static final cm_title_layout_text:I = 0x7f0b0033

.field public static final cm_title_menu_button:I = 0x7f0b0036

.field public static final cm_title_progress:I = 0x7f0b0035

.field public static final cm_title_text:I = 0x7f0b0034

.field public static final colorpickerLinearlayout:I = 0x7f0b004c

.field public static final colorpicker_blue_inputfield:I = 0x7f0b0052

.field public static final colorpicker_color:I = 0x7f0b004e

.field public static final colorpicker_green_inputfield:I = 0x7f0b0051

.field public static final colorpicker_hue:I = 0x7f0b0054

.field public static final colorpicker_main:I = 0x7f0b0053

.field public static final colorpicker_red_inputfield:I = 0x7f0b0050

.field public static final colorpicker_rgb:I = 0x7f0b004f

.field public static final common_actionbar_image_button:I = 0x7f0b0014

.field public static final common_actionbar_image_button2:I = 0x7f0b0013

.field public static final common_actionbar_layout:I = 0x7f0b0011

.field public static final common_actionbar_text_button:I = 0x7f0b0015

.field public static final common_actionbar_title:I = 0x7f0b0012

.field public static final common_inputfield:I = 0x7f0b005d

.field public static final current_page:I = 0x7f0b0164

.field public static final delete_contextmenu:I = 0x7f0b0281

.field public static final dic_search_panel:I = 0x7f0b013a

.field public static final docViewLayout:I = 0x7f0b002c

.field public static final docViewLayoutLeft:I = 0x7f0b0116

.field public static final docViewLayoutParent:I = 0x7f0b0115

.field public static final docViewLayoutRight:I = 0x7f0b0117

.field public static final draw_toolbar:I = 0x7f0b000b

.field public static final draw_toolbar_eraseallmode:I = 0x7f0b000f

.field public static final draw_toolbar_erasemode:I = 0x7f0b000e

.field public static final draw_toolbar_freedraw_Panning:I = 0x7f0b0010

.field public static final draw_toolbar_lassomode:I = 0x7f0b000d

.field public static final draw_toolbar_penmode:I = 0x7f0b000c

.field public static final editAnnot:I = 0x7f0b0138

.field public static final edit_name:I = 0x7f0b00f3

.field public static final edit_num:I = 0x7f0b005a

.field public static final editdegreelayout:I = 0x7f0b00f2

.field public static final edittext_contextmenu:I = 0x7f0b0284

.field public static final edtFind:I = 0x7f0b0253

.field public static final edtReplace:I = 0x7f0b025e

.field public static final edtReplaceFind:I = 0x7f0b0259

.field public static final edtTextEdit:I = 0x7f0b0261

.field public static final edt_sheet_tap_layout:I = 0x7f0b01e4

.field public static final edt_sheet_tap_rename:I = 0x7f0b01e5

.field public static final endView:I = 0x7f0b0173

.field public static final file_empty_image:I = 0x7f0b0027

.field public static final file_empty_text:I = 0x7f0b0028

.field public static final file_fl_filelist:I = 0x7f0b00e1

.field public static final file_fl_index:I = 0x7f0b00e8

.field public static final file_fl_progress:I = 0x7f0b00e2

.field public static final file_fl_progress_bar:I = 0x7f0b00e3

.field public static final file_index_file:I = 0x7f0b00e9

.field public static final file_index_folder:I = 0x7f0b00ea

.field public static final file_index_icon:I = 0x7f0b00eb

.field public static final file_index_type:I = 0x7f0b00ec

.field public static final file_info_text:I = 0x7f0b00e0

.field public static final file_item_icon:I = 0x7f0b00d6

.field public static final file_item_name:I = 0x7f0b00d9

.field public static final file_item_path:I = 0x7f0b00dc

.field public static final file_item_size:I = 0x7f0b00db

.field public static final file_item_sub_icon:I = 0x7f0b00d7

.field public static final file_item_time:I = 0x7f0b00da

.field public static final file_layout_bg:I = 0x7f0b00d4

.field public static final file_layout_content:I = 0x7f0b00d8

.field public static final file_layout_icon:I = 0x7f0b00d5

.field public static final file_list:I = 0x7f0b00e7

.field public static final file_list_layout:I = 0x7f0b00e6

.field public static final file_ll_empty:I = 0x7f0b00ed

.field public static final file_ll_info:I = 0x7f0b00df

.field public static final file_ll_list:I = 0x7f0b00e4

.field public static final file_ll_root:I = 0x7f0b00dd

.field public static final file_ll_title:I = 0x7f0b00de

.field public static final file_scroll_button:I = 0x7f0b00e5

.field public static final find_bar:I = 0x7f0b0063

.field public static final find_delete_button:I = 0x7f0b0068

.field public static final find_edit:I = 0x7f0b0067

.field public static final find_match_button:I = 0x7f0b0064

.field public static final find_next_button:I = 0x7f0b0065

.field public static final find_prev_button:I = 0x7f0b0066

.field public static final flScrollPos:I = 0x7f0b0263

.field public static final fm_dialog_edit:I = 0x7f0b01a4

.field public static final fm_dialog_text:I = 0x7f0b01a5

.field public static final fm_info_land_attributes:I = 0x7f0b00d1

.field public static final fm_info_land_attributes_text:I = 0x7f0b00d3

.field public static final fm_info_land_attributes_title:I = 0x7f0b00d2

.field public static final fm_info_land_author:I = 0x7f0b00bf

.field public static final fm_info_land_author_text:I = 0x7f0b00c1

.field public static final fm_info_land_author_title:I = 0x7f0b00c0

.field public static final fm_info_land_contents:I = 0x7f0b00c2

.field public static final fm_info_land_contents_progress:I = 0x7f0b00c5

.field public static final fm_info_land_contents_text:I = 0x7f0b00c4

.field public static final fm_info_land_contents_title:I = 0x7f0b00c3

.field public static final fm_info_land_icon:I = 0x7f0b00b2

.field public static final fm_info_land_location_text:I = 0x7f0b00b7

.field public static final fm_info_land_location_title:I = 0x7f0b00b6

.field public static final fm_info_land_modified_by:I = 0x7f0b00c8

.field public static final fm_info_land_modified_by_text:I = 0x7f0b00ca

.field public static final fm_info_land_modified_by_title:I = 0x7f0b00c9

.field public static final fm_info_land_modified_text:I = 0x7f0b00c7

.field public static final fm_info_land_modified_title:I = 0x7f0b00c6

.field public static final fm_info_land_name:I = 0x7f0b00b3

.field public static final fm_info_land_page:I = 0x7f0b00cb

.field public static final fm_info_land_page_text:I = 0x7f0b00cd

.field public static final fm_info_land_page_title:I = 0x7f0b00cc

.field public static final fm_info_land_size:I = 0x7f0b00b8

.field public static final fm_info_land_size_progress:I = 0x7f0b00bb

.field public static final fm_info_land_size_text:I = 0x7f0b00ba

.field public static final fm_info_land_size_title:I = 0x7f0b00b9

.field public static final fm_info_land_thumb:I = 0x7f0b00b0

.field public static final fm_info_land_thumb_img:I = 0x7f0b00b1

.field public static final fm_info_land_title:I = 0x7f0b00bc

.field public static final fm_info_land_title_text:I = 0x7f0b00be

.field public static final fm_info_land_title_title:I = 0x7f0b00bd

.field public static final fm_info_land_type_text:I = 0x7f0b00b5

.field public static final fm_info_land_type_title:I = 0x7f0b00b4

.field public static final fm_info_land_words:I = 0x7f0b00ce

.field public static final fm_info_land_words_text:I = 0x7f0b00d0

.field public static final fm_info_land_words_title:I = 0x7f0b00cf

.field public static final fm_info_port_attributes:I = 0x7f0b00ac

.field public static final fm_info_port_attributes_text:I = 0x7f0b00ae

.field public static final fm_info_port_attributes_title:I = 0x7f0b00ad

.field public static final fm_info_port_author:I = 0x7f0b009a

.field public static final fm_info_port_author_text:I = 0x7f0b009c

.field public static final fm_info_port_author_title:I = 0x7f0b009b

.field public static final fm_info_port_contents:I = 0x7f0b009d

.field public static final fm_info_port_contents_progress:I = 0x7f0b00a0

.field public static final fm_info_port_contents_text:I = 0x7f0b009f

.field public static final fm_info_port_contents_title:I = 0x7f0b009e

.field public static final fm_info_port_icon:I = 0x7f0b008d

.field public static final fm_info_port_location_text:I = 0x7f0b0092

.field public static final fm_info_port_location_title:I = 0x7f0b0091

.field public static final fm_info_port_modified_by:I = 0x7f0b00a3

.field public static final fm_info_port_modified_by_text:I = 0x7f0b00a5

.field public static final fm_info_port_modified_by_title:I = 0x7f0b00a4

.field public static final fm_info_port_modified_text:I = 0x7f0b00a2

.field public static final fm_info_port_modified_title:I = 0x7f0b00a1

.field public static final fm_info_port_name:I = 0x7f0b008e

.field public static final fm_info_port_page:I = 0x7f0b00a6

.field public static final fm_info_port_page_text:I = 0x7f0b00a8

.field public static final fm_info_port_page_title:I = 0x7f0b00a7

.field public static final fm_info_port_size:I = 0x7f0b0093

.field public static final fm_info_port_size_progress:I = 0x7f0b0096

.field public static final fm_info_port_size_text:I = 0x7f0b0095

.field public static final fm_info_port_size_title:I = 0x7f0b0094

.field public static final fm_info_port_thumb:I = 0x7f0b008b

.field public static final fm_info_port_thumb_img:I = 0x7f0b008c

.field public static final fm_info_port_title:I = 0x7f0b0097

.field public static final fm_info_port_title_text:I = 0x7f0b0099

.field public static final fm_info_port_title_title:I = 0x7f0b0098

.field public static final fm_info_port_type_text:I = 0x7f0b0090

.field public static final fm_info_port_type_title:I = 0x7f0b008f

.field public static final fm_info_port_words:I = 0x7f0b00a9

.field public static final fm_info_port_words_text:I = 0x7f0b00ab

.field public static final fm_info_port_words_title:I = 0x7f0b00aa

.field public static final fm_information_land:I = 0x7f0b00af

.field public static final fm_information_port:I = 0x7f0b008a

.field public static final fm_more_list:I = 0x7f0b0077

.field public static final fontwheelbtn:I = 0x7f0b0239

.field public static final forward_contextmenu:I = 0x7f0b0282

.field public static final frame_dic_search:I = 0x7f0b0174

.field public static final freedraw_marker_option:I = 0x7f0b013b

.field public static final freedraw_pendraw_option:I = 0x7f0b013c

.field public static final freedrawbase_bar:I = 0x7f0b0110

.field public static final ibScrollThumb:I = 0x7f0b0264

.field public static final img_sheet_hold:I = 0x7f0b01e2

.field public static final incorrect_text:I = 0x7f0b017f

.field public static final inputLayout:I = 0x7f0b0057

.field public static final insert_hyperlink_layer01:I = 0x7f0b017d

.field public static final ivBlackTheme:I = 0x7f0b0240

.field public static final ivBlackThemeSelected:I = 0x7f0b0241

.field public static final ivCheckIcon:I = 0x7f0b0225

.field public static final ivCopy:I = 0x7f0b0271

.field public static final ivCut:I = 0x7f0b026f

.field public static final ivFonttypeItem:I = 0x7f0b0223

.field public static final ivLightGrayTheme:I = 0x7f0b023e

.field public static final ivLightGrayThemeSelected:I = 0x7f0b023f

.field public static final ivPaste:I = 0x7f0b0273

.field public static final ivSelectAll:I = 0x7f0b026d

.field public static final ivYellowTheme:I = 0x7f0b023c

.field public static final ivYellowThemeSelected:I = 0x7f0b023d

.field public static final iv_about_infraware_logo:I = 0x7f0b01ba

.field public static final iv_about_polaris_logo:I = 0x7f0b01b5

.field public static final key0:I = 0x7f0b0101

.field public static final key1:I = 0x7f0b00f4

.field public static final key2:I = 0x7f0b00f5

.field public static final key3:I = 0x7f0b00f6

.field public static final key4:I = 0x7f0b00f8

.field public static final key5:I = 0x7f0b00f9

.field public static final key6:I = 0x7f0b00fa

.field public static final key7:I = 0x7f0b00fc

.field public static final key8:I = 0x7f0b00fd

.field public static final key9:I = 0x7f0b00fe

.field public static final keypadlayout:I = 0x7f0b005c

.field public static final last_slide_show_image:I = 0x7f0b01f8

.field public static final linearLayout1:I = 0x7f0b004d

.field public static final linearLayout2:I = 0x7f0b00f7

.field public static final linearLayout3:I = 0x7f0b00fb

.field public static final linearLayout4:I = 0x7f0b00ff

.field public static final linear_sheet_filter:I = 0x7f0b01d9

.field public static final listitem_edittext:I = 0x7f0b00f0

.field public static final listitem_icon:I = 0x7f0b00ee

.field public static final listitem_radiobutton:I = 0x7f0b00f1

.field public static final listitem_text:I = 0x7f0b00ef

.field public static final llActionBar:I = 0x7f0b0230

.field public static final llFind:I = 0x7f0b024f

.field public static final llFindOfReplace:I = 0x7f0b0256

.field public static final llMatchCase:I = 0x7f0b0226

.field public static final llMatchWholeWord:I = 0x7f0b0229

.field public static final llReplace:I = 0x7f0b0255

.field public static final llReplaceOfReplace:I = 0x7f0b025b

.field public static final llTTS:I = 0x7f0b0260

.field public static final ll_polaris_about:I = 0x7f0b01b3

.field public static final ll_polaris_logo:I = 0x7f0b01b4

.field public static final lvEncoding:I = 0x7f0b0244

.field public static final mainLayout:I = 0x7f0b0025

.field public static final main_actionbar_layout:I = 0x7f0b010f

.field public static final marker_color01:I = 0x7f0b0150

.field public static final marker_color02:I = 0x7f0b0151

.field public static final marker_color03:I = 0x7f0b0152

.field public static final marker_color04:I = 0x7f0b0153

.field public static final marker_color05:I = 0x7f0b0154

.field public static final marker_color06:I = 0x7f0b0155

.field public static final marker_color07:I = 0x7f0b0156

.field public static final marker_color08:I = 0x7f0b0157

.field public static final marker_color09:I = 0x7f0b0158

.field public static final marker_color10:I = 0x7f0b0159

.field public static final marker_color11:I = 0x7f0b015a

.field public static final marker_color12:I = 0x7f0b015b

.field public static final marker_highlighter_type:I = 0x7f0b014c

.field public static final marker_option:I = 0x7f0b0148

.field public static final marker_pen_type:I = 0x7f0b014b

.field public static final marker_preview:I = 0x7f0b0149

.field public static final marker_thickness:I = 0x7f0b014e

.field public static final marker_type_selector:I = 0x7f0b014a

.field public static final memo_delete_btn:I = 0x7f0b015d

.field public static final memo_next_btn:I = 0x7f0b015f

.field public static final memo_prev_btn:I = 0x7f0b015e

.field public static final memo_separate_line:I = 0x7f0b0160

.field public static final memo_text:I = 0x7f0b0161

.field public static final memo_view:I = 0x7f0b0132

.field public static final middle_text:I = 0x7f0b01bf

.field public static final msg:I = 0x7f0b0176

.field public static final next_btn:I = 0x7f0b006b

.field public static final no_annot_img:I = 0x7f0b0023

.field public static final no_annot_text:I = 0x7f0b0024

.field public static final no_result_frame:I = 0x7f0b017b

.field public static final no_result_text:I = 0x7f0b017c

.field public static final no_toc_img:I = 0x7f0b0187

.field public static final no_toc_text:I = 0x7f0b0188

.field public static final ok_btn:I = 0x7f0b01bd

.field public static final page_info:I = 0x7f0b0163

.field public static final pageinfo:I = 0x7f0b0123

.field public static final password_edittext:I = 0x7f0b017e

.field public static final pdf_annot_edittext:I = 0x7f0b0139

.field public static final pdf_annot_title:I = 0x7f0b0135

.field public static final pdf_annot_view:I = 0x7f0b0133

.field public static final pdf_toc_empty_layout:I = 0x7f0b0186

.field public static final pdf_toc_list_layout:I = 0x7f0b0184

.field public static final pdf_toc_rootview:I = 0x7f0b0183

.field public static final pen_drawing_next:I = 0x7f0b014f

.field public static final pen_drawing_prev:I = 0x7f0b014d

.field public static final pen_drawing_seekbar:I = 0x7f0b0197

.field public static final pen_drawing_size_button_layout:I = 0x7f0b018f

.field public static final pen_drawing_size_layout:I = 0x7f0b0196

.field public static final pendraw_color01:I = 0x7f0b0198

.field public static final pendraw_color02:I = 0x7f0b0199

.field public static final pendraw_color03:I = 0x7f0b019a

.field public static final pendraw_color04:I = 0x7f0b019b

.field public static final pendraw_color05:I = 0x7f0b019c

.field public static final pendraw_color06:I = 0x7f0b019d

.field public static final pendraw_color07:I = 0x7f0b019e

.field public static final pendraw_color08:I = 0x7f0b019f

.field public static final pendraw_color09:I = 0x7f0b01a0

.field public static final pendraw_color10:I = 0x7f0b01a1

.field public static final pendraw_color11:I = 0x7f0b01a2

.field public static final pendraw_color12:I = 0x7f0b01a3

.field public static final pendraw_highlighter_type:I = 0x7f0b018e

.field public static final pendraw_ink_type:I = 0x7f0b018c

.field public static final pendraw_option:I = 0x7f0b0189

.field public static final pendraw_pencle_type:I = 0x7f0b018b

.field public static final pendraw_preview:I = 0x7f0b018a

.field public static final pendraw_size_1:I = 0x7f0b0190

.field public static final pendraw_size_2:I = 0x7f0b0191

.field public static final pendraw_size_3:I = 0x7f0b0192

.field public static final pendraw_size_4:I = 0x7f0b0193

.field public static final pendraw_size_5:I = 0x7f0b0194

.field public static final pendraw_size_6:I = 0x7f0b0195

.field public static final pendraw_straight_type:I = 0x7f0b018d

.field public static final po_account_error:I = 0x7f0b0060

.field public static final po_account_error_image:I = 0x7f0b0061

.field public static final po_account_error_text:I = 0x7f0b0062

.field public static final po_account_id:I = 0x7f0b005e

.field public static final po_account_password:I = 0x7f0b005f

.field public static final po_broadcast_info:I = 0x7f0b01f4

.field public static final po_broadcast_info_text:I = 0x7f0b01f5

.field public static final po_progress_bar:I = 0x7f0b01a9

.field public static final po_progress_bar_sub:I = 0x7f0b01aa

.field public static final po_progress_circle:I = 0x7f0b01a6

.field public static final po_progress_count:I = 0x7f0b01a8

.field public static final po_progress_message:I = 0x7f0b01a7

.field public static final po_progress_percent:I = 0x7f0b01ab

.field public static final pointer_draw_view:I = 0x7f0b011b

.field public static final popup_menu_icon:I = 0x7f0b0076

.field public static final popup_menu_icon_item:I = 0x7f0b0075

.field public static final popup_menu_item_disable:I = 0x7f0b0072

.field public static final popup_menu_name:I = 0x7f0b0074

.field public static final popup_menu_text_item:I = 0x7f0b0073

.field public static final popup_search_list_item_tv:I = 0x7f0b01c1

.field public static final printListView:I = 0x7f0b01c5

.field public static final print_all:I = 0x7f0b01cf

.field public static final print_all_radio_button:I = 0x7f0b01c9

.field public static final print_current:I = 0x7f0b01d0

.field public static final print_edit_layout:I = 0x7f0b01d1

.field public static final print_edit_page:I = 0x7f0b01cc

.field public static final print_end_page:I = 0x7f0b01d4

.field public static final print_main_Layout:I = 0x7f0b01c2

.field public static final print_page:I = 0x7f0b01d2

.field public static final print_some_radio_button:I = 0x7f0b01cb

.field public static final print_start_page:I = 0x7f0b01d3

.field public static final printactiontitlebar_layout:I = 0x7f0b01c3

.field public static final printlistViewLayout:I = 0x7f0b01c4

.field public static final printoption_count_spin:I = 0x7f0b01ce

.field public static final printoption_layout:I = 0x7f0b01c6

.field public static final printoption_range_textview:I = 0x7f0b01c7

.field public static final relativeLayout1:I = 0x7f0b005b

.field public static final replace_bar:I = 0x7f0b0069

.field public static final replace_btn:I = 0x7f0b006e

.field public static final replace_dst_delete_button:I = 0x7f0b0071

.field public static final replace_dst_edit:I = 0x7f0b0070

.field public static final replace_match_button:I = 0x7f0b006a

.field public static final replace_src_delete_button:I = 0x7f0b006d

.field public static final replace_src_edit:I = 0x7f0b006c

.field public static final replaceall_btn:I = 0x7f0b006f

.field public static final resize_contextmenu:I = 0x7f0b0280

.field public static final rightButton:I = 0x7f0b0108

.field public static final root_view:I = 0x7f0b010e

.field public static final rulerbar_bubbleballon:I = 0x7f0b0016

.field public static final save_attention_ico:I = 0x7f0b0085

.field public static final save_attention_layout:I = 0x7f0b0084

.field public static final save_error:I = 0x7f0b0086

.field public static final save_location_icon:I = 0x7f0b0080

.field public static final save_location_name:I = 0x7f0b0081

.field public static final save_pendraw_checkbox:I = 0x7f0b0083

.field public static final saveas_btn:I = 0x7f0b007f

.field public static final saveas_editformat:I = 0x7f0b007d

.field public static final saveas_editicon:I = 0x7f0b007b

.field public static final saveas_editname:I = 0x7f0b007c

.field public static final saveas_filename:I = 0x7f0b007a

.field public static final saveas_folder:I = 0x7f0b0082

.field public static final saveas_location:I = 0x7f0b007e

.field public static final sb_gallery:I = 0x7f0b027c

.field public static final sb_gallery_layout:I = 0x7f0b027b

.field public static final sb_next:I = 0x7f0b027f

.field public static final sb_prev:I = 0x7f0b027a

.field public static final scroll_about_info:I = 0x7f0b01b7

.field public static final scrollview:I = 0x7f0b0179

.field public static final separate_line_01:I = 0x7f0b0172

.field public static final settings_item_check:I = 0x7f0b0049

.field public static final settings_item_disable:I = 0x7f0b0045

.field public static final settings_item_divider:I = 0x7f0b004b

.field public static final settings_item_selected:I = 0x7f0b004a

.field public static final settings_item_sub_title:I = 0x7f0b0048

.field public static final settings_item_text:I = 0x7f0b0046

.field public static final settings_item_title:I = 0x7f0b0047

.field public static final sheetCellEdit:I = 0x7f0b011d

.field public static final sheetFilterList:I = 0x7f0b01d8

.field public static final sheetSubCellEdit:I = 0x7f0b011e

.field public static final sheetSubTextboxEdit:I = 0x7f0b0121

.field public static final sheetTextboxEdit:I = 0x7f0b0120

.field public static final sheet_Textbox_inline_edit:I = 0x7f0b0122

.field public static final sheet_autofiler_list_container:I = 0x7f0b01dd

.field public static final sheet_autofilter_list_check_layout:I = 0x7f0b01de

.field public static final sheet_autofilter_list_checkicon:I = 0x7f0b01df

.field public static final sheet_autofilter_listitem:I = 0x7f0b01e0

.field public static final sheet_autofilter_popup_list:I = 0x7f0b01dc

.field public static final sheet_cell_inline_edit:I = 0x7f0b011f

.field public static final sheet_tab:I = 0x7f0b0128

.field public static final sheet_tab_scroll:I = 0x7f0b0127

.field public static final slide_listitem:I = 0x7f0b01e6

.field public static final slide_listitem_image:I = 0x7f0b01e7

.field public static final slide_listitem_ishidepage:I = 0x7f0b01e9

.field public static final slide_listitem_pagenum:I = 0x7f0b01e8

.field public static final slide_manage:I = 0x7f0b012d

.field public static final slide_manage_add_btn_landscape:I = 0x7f0b01ec

.field public static final slide_manage_add_btn_portrait:I = 0x7f0b01f1

.field public static final slide_manage_indicator:I = 0x7f0b01f0

.field public static final slide_manage_indicator_landscape:I = 0x7f0b01eb

.field public static final slide_manage_land_list:I = 0x7f0b01ea

.field public static final slide_manage_open_close_btn:I = 0x7f0b0125

.field public static final slide_manage_port_list:I = 0x7f0b01ef

.field public static final slide_manage_portrait_listview:I = 0x7f0b01ee

.field public static final slide_manage_slideshow_btn_landscape:I = 0x7f0b01ed

.field public static final slide_manage_slideshow_btn_portrait:I = 0x7f0b01f2

.field public static final slide_note:I = 0x7f0b012e

.field public static final slide_note_edittext:I = 0x7f0b0131

.field public static final slide_note_layout:I = 0x7f0b0130

.field public static final slide_note_text:I = 0x7f0b012f

.field public static final slide_show_close:I = 0x7f0b0207

.field public static final slide_show_controller:I = 0x7f0b0216

.field public static final slide_show_controller_first:I = 0x7f0b0217

.field public static final slide_show_controller_last:I = 0x7f0b0218

.field public static final slide_show_controller_tvout:I = 0x7f0b0219

.field public static final slide_show_eraser:I = 0x7f0b01fe

.field public static final slide_show_eraserall:I = 0x7f0b01ff

.field public static final slide_show_external_image:I = 0x7f0b0213

.field public static final slide_show_finished:I = 0x7f0b01f9

.field public static final slide_show_gl_image:I = 0x7f0b01f7

.field public static final slide_show_image:I = 0x7f0b01f6

.field public static final slide_show_main:I = 0x7f0b01f3

.field public static final slide_show_marker_option:I = 0x7f0b0212

.field public static final slide_show_mode:I = 0x7f0b01fc

.field public static final slide_show_mode_marker:I = 0x7f0b020b

.field public static final slide_show_mode_option:I = 0x7f0b0208

.field public static final slide_show_mode_pointer:I = 0x7f0b020a

.field public static final slide_show_mode_slide:I = 0x7f0b0209

.field public static final slide_show_note:I = 0x7f0b021b

.field public static final slide_show_note_close:I = 0x7f0b021a

.field public static final slide_show_note_edittext:I = 0x7f0b021c

.field public static final slide_show_option:I = 0x7f0b0215

.field public static final slide_show_page_move_layout:I = 0x7f0b01fa

.field public static final slide_show_page_next:I = 0x7f0b0205

.field public static final slide_show_page_prev:I = 0x7f0b0203

.field public static final slide_show_pointer_color01:I = 0x7f0b020d

.field public static final slide_show_pointer_color02:I = 0x7f0b020e

.field public static final slide_show_pointer_color03:I = 0x7f0b020f

.field public static final slide_show_pointer_color04:I = 0x7f0b0210

.field public static final slide_show_pointer_color05:I = 0x7f0b0211

.field public static final slide_show_pointer_draw:I = 0x7f0b01fb

.field public static final slide_show_pointer_option:I = 0x7f0b020c

.field public static final slide_show_preview_image:I = 0x7f0b0206

.field public static final slide_show_preview_seekbar:I = 0x7f0b0204

.field public static final slide_show_settings:I = 0x7f0b01fd

.field public static final slide_show_settings_listitem_image:I = 0x7f0b021d

.field public static final slide_show_settings_listitem_radiobtn:I = 0x7f0b021f

.field public static final slide_show_settings_listitem_text:I = 0x7f0b021e

.field public static final slide_show_slide:I = 0x7f0b0214

.field public static final slide_show_start_current_page:I = 0x7f0b0221

.field public static final slide_show_start_first_page:I = 0x7f0b0220

.field public static final slide_video_btn:I = 0x7f0b0201

.field public static final svEdit:I = 0x7f0b0262

.field public static final svPreferences:I = 0x7f0b0243

.field public static final tabLayout:I = 0x7f0b0126

.field public static final te_actionbar_view_root:I = 0x7f0b0248

.field public static final te_current_page:I = 0x7f0b0266

.field public static final te_page_info:I = 0x7f0b0265

.field public static final te_total_page:I = 0x7f0b0267

.field public static final te_tts_bar:I = 0x7f0b0268

.field public static final te_tts_next:I = 0x7f0b026b

.field public static final te_tts_playpause:I = 0x7f0b026a

.field public static final te_tts_prev:I = 0x7f0b0269

.field public static final text_content:I = 0x7f0b01bb

.field public static final text_sheet_name:I = 0x7f0b01e3

.field public static final text_sort_detail:I = 0x7f0b01e1

.field public static final themeLayout:I = 0x7f0b023b

.field public static final title:I = 0x7f0b0167

.field public static final titleAnnot:I = 0x7f0b0134

.field public static final titleMemo:I = 0x7f0b015c

.field public static final title_menu_new_folder:I = 0x7f0b0037

.field public static final title_menu_ok:I = 0x7f0b0038

.field public static final title_menu_save:I = 0x7f0b0039

.field public static final toc_item_icon:I = 0x7f0b0181

.field public static final toc_item_layout:I = 0x7f0b0180

.field public static final toc_item_text:I = 0x7f0b0182

.field public static final toc_list_view:I = 0x7f0b0185

.field public static final total_page:I = 0x7f0b0165

.field public static final transform_info:I = 0x7f0b0124

.field public static final transform_info_mid:I = 0x7f0b0276

.field public static final transform_info_text1:I = 0x7f0b0275

.field public static final transform_info_text2:I = 0x7f0b0277

.field public static final tts_bar:I = 0x7f0b0129

.field public static final tts_next:I = 0x7f0b012c

.field public static final tts_playpause:I = 0x7f0b012b

.field public static final tts_prev:I = 0x7f0b012a

.field public static final tvCopy:I = 0x7f0b0270

.field public static final tvCut:I = 0x7f0b026e

.field public static final tvEncodingItem:I = 0x7f0b0224

.field public static final tvFormatIcon:I = 0x7f0b0249

.field public static final tvMatchCase:I = 0x7f0b0227

.field public static final tvMatchWholeWord:I = 0x7f0b022a

.field public static final tvMore:I = 0x7f0b0274

.field public static final tvPaste:I = 0x7f0b0272

.field public static final tvRootView:I = 0x7f0b0247

.field public static final tvSearch:I = 0x7f0b022f

.field public static final tvSelectAll:I = 0x7f0b026c

.field public static final tvShare:I = 0x7f0b022e

.field public static final tvTitle:I = 0x7f0b0058

.field public static final tv_about_info:I = 0x7f0b01b8

.field public static final tv_about_info2:I = 0x7f0b01b9

.field public static final tv_about_version:I = 0x7f0b01b6

.field public static final tv_alarm_connet_network:I = 0x7f0b003b

.field public static final tv_fontsize1:I = 0x7f0b0233

.field public static final tv_fontsize2:I = 0x7f0b0234

.field public static final tv_fontsize3:I = 0x7f0b0235

.field public static final tv_fontsize4:I = 0x7f0b0236

.field public static final tv_fontsize5:I = 0x7f0b0237

.field public static final tv_fontsize6:I = 0x7f0b0238

.field public static final tv_office_info:I = 0x7f0b01ae

.field public static final tv_printing:I = 0x7f0b01d5

.field public static final tv_printing_delaytime:I = 0x7f0b0245

.field public static final tv_te_Preference_fontsize:I = 0x7f0b0232

.field public static final tv_te_preferences_Encoding:I = 0x7f0b0242

.field public static final tv_te_preferences_theme:I = 0x7f0b023a

.field public static final tv_version_text:I = 0x7f0b01b1

.field public static final tv_viewer_info1:I = 0x7f0b01ac

.field public static final tv_viewer_info2:I = 0x7f0b01ad

.field public static final tvmoreTTS:I = 0x7f0b022d

.field public static final txt:I = 0x7f0b0103

.field public static final video_framelayout:I = 0x7f0b011c

.field public static final videoframelayout:I = 0x7f0b0200

.field public static final videoview:I = 0x7f0b0202

.field public static final view4:I = 0x7f0b0055

.field public static final view5:I = 0x7f0b0056

.field public static final webview1:I = 0x7f0b017a

.field public static final wheel_btn:I = 0x7f0b0279

.field public static final wheel_line:I = 0x7f0b027d

.field public static final wheel_view:I = 0x7f0b027e

.field public static final zoom_percent:I = 0x7f0b0166


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1512
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
