.class public final Lcom/infraware/polarisviewer5/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisviewer5/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final ab_draw_btn_bg_selector:I = 0x7f020000

.field public static final about_bg:I = 0x7f020001

.field public static final about_polaris_bi:I = 0x7f020002

.field public static final actionbar_account:I = 0x7f020003

.field public static final actionbar_button:I = 0x7f020004

.field public static final actionbar_find:I = 0x7f020005

.field public static final actionbar_freewrite:I = 0x7f020006

.field public static final actionbar_ico_check:I = 0x7f020007

.field public static final actionbar_menu:I = 0x7f020008

.field public static final actionbar_print:I = 0x7f020009

.field public static final actionbar_redo:I = 0x7f02000a

.field public static final actionbar_save_pdf:I = 0x7f02000b

.field public static final actionbar_undo:I = 0x7f02000c

.field public static final all_eraser_title_selector:I = 0x7f02000d

.field public static final bg_panel_tap_pressed_selector:I = 0x7f02000e

.field public static final bg_pressed:I = 0x7f02000f

.field public static final bg_pressed_selector:I = 0x7f020010

.field public static final bg_textcolor_selector:I = 0x7f020011

.field public static final bottom_btn_bg:I = 0x7f020012

.field public static final btn_bookmark_disabled:I = 0x7f020013

.field public static final btn_bookmark_normal:I = 0x7f020014

.field public static final btn_bookmark_pressed:I = 0x7f020015

.field public static final btn_check:I = 0x7f020016

.field public static final btn_check_disabled_popover:I = 0x7f020017

.field public static final btn_check_disabled_popup:I = 0x7f020018

.field public static final btn_check_off:I = 0x7f020019

.field public static final btn_check_off_popover:I = 0x7f02001a

.field public static final btn_check_off_popup:I = 0x7f02001b

.field public static final btn_check_off_pressed:I = 0x7f02001c

.field public static final btn_check_on:I = 0x7f02001d

.field public static final btn_check_on_popover:I = 0x7f02001e

.field public static final btn_check_on_popup:I = 0x7f02001f

.field public static final btn_check_on_pressed:I = 0x7f020020

.field public static final btn_check_popover_black_selector:I = 0x7f020021

.field public static final btn_check_popover_selector:I = 0x7f020022

.field public static final btn_check_white:I = 0x7f020023

.field public static final btn_default_normal:I = 0x7f020024

.field public static final btn_marker_normal:I = 0x7f020025

.field public static final btn_play_normal:I = 0x7f020026

.field public static final btn_preview_next_land_normal:I = 0x7f020027

.field public static final btn_preview_next_land_pressed:I = 0x7f020028

.field public static final btn_preview_next_normal:I = 0x7f020029

.field public static final btn_preview_next_pressed:I = 0x7f02002a

.field public static final btn_preview_previous_land_normal:I = 0x7f02002b

.field public static final btn_preview_previous_land_pressed:I = 0x7f02002c

.field public static final btn_preview_previous_normal:I = 0x7f02002d

.field public static final btn_preview_previous_pressed:I = 0x7f02002e

.field public static final btn_radio:I = 0x7f02002f

.field public static final btn_radio2:I = 0x7f020030

.field public static final btn_radio_normal:I = 0x7f020031

.field public static final btn_radio_off:I = 0x7f020032

.field public static final btn_radio_on:I = 0x7f020033

.field public static final btn_radio_selected:I = 0x7f020034

.field public static final btn_tts_next:I = 0x7f020035

.field public static final btn_tts_pause:I = 0x7f020036

.field public static final btn_tts_play:I = 0x7f020037

.field public static final btn_tts_prev:I = 0x7f020038

.field public static final cm_bg_pressed_selector:I = 0x7f020039

.field public static final cm_check_select:I = 0x7f02003a

.field public static final cm_edit_text_bg:I = 0x7f02003b

.field public static final cm_ico_selection:I = 0x7f02003c

.field public static final cm_list_item_selector:I = 0x7f02003d

.field public static final cm_title_text_bg:I = 0x7f02003e

.field public static final color_outline:I = 0x7f02003f

.field public static final color_pressed:I = 0x7f020040

.field public static final colorpicker_hue_point_land_normal:I = 0x7f020041

.field public static final colorpicker_hue_point_land_pressed:I = 0x7f020042

.field public static final colorpicker_hue_point_normal:I = 0x7f020043

.field public static final colorpicker_hue_point_pressed:I = 0x7f020044

.field public static final colorpicker_outline:I = 0x7f020045

.field public static final colorpicker_outline_hue:I = 0x7f020046

.field public static final colorpicker_outline_main:I = 0x7f020047

.field public static final colorpicker_point_normal:I = 0x7f020048

.field public static final colorpicker_point_pressed:I = 0x7f020049

.field public static final common_default_btn:I = 0x7f02004a

.field public static final common_default_btn_bg:I = 0x7f02004b

.field public static final common_default_btn_bg_darker:I = 0x7f02004c

.field public static final common_inputbox_img:I = 0x7f02004d

.field public static final common_select_btn02:I = 0x7f02004e

.field public static final common_selected_list:I = 0x7f02004f

.field public static final download_bg:I = 0x7f020050

.field public static final downloadbtn_normal:I = 0x7f020051

.field public static final downloadbtn_normal_selector:I = 0x7f020052

.field public static final downloadbtn_pressed:I = 0x7f020053

.field public static final draw_toolbar_ico_eraseallmode:I = 0x7f020054

.field public static final draw_toolbar_ico_erasemode:I = 0x7f020055

.field public static final draw_toolbar_ico_lassomode:I = 0x7f020056

.field public static final draw_toolbar_ico_penmode:I = 0x7f020057

.field public static final dualview_ico_controlnote:I = 0x7f020058

.field public static final dualview_ico_settings_primary:I = 0x7f020059

.field public static final editbox_textcolor_selector:I = 0x7f02005a

.field public static final edittext_selector:I = 0x7f02005b

.field public static final eraser_title_selector:I = 0x7f02005c

.field public static final file_bg_page_preview_landscape:I = 0x7f02005d

.field public static final file_bg_page_preview_portrait:I = 0x7f02005e

.field public static final find_next:I = 0x7f02005f

.field public static final find_prev:I = 0x7f020060

.field public static final findbar_allchange_btn_selector:I = 0x7f020061

.field public static final findbar_change_btn_selector:I = 0x7f020062

.field public static final fm_actionbar_newfolder:I = 0x7f020063

.field public static final fm_actionbar_ok:I = 0x7f020064

.field public static final fm_actionbar_select_all:I = 0x7f020065

.field public static final fm_manage_button_delete:I = 0x7f020066

.field public static final fm_navigation_embedded:I = 0x7f020067

.field public static final fm_navigation_folder:I = 0x7f020068

.field public static final fm_navigation_sdcard:I = 0x7f020069

.field public static final fm_navigation_usbdrive:I = 0x7f02006a

.field public static final fm_quick_scroll_thumb:I = 0x7f02006b

.field public static final freedraw_ico_brush_normal:I = 0x7f02006c

.field public static final freedraw_ico_brush_selected:I = 0x7f02006d

.field public static final freedraw_ico_eraseall_normal:I = 0x7f02006e

.field public static final freedraw_ico_eraseall_pressed:I = 0x7f02006f

.field public static final freedraw_ico_eraser_normal:I = 0x7f020070

.field public static final freedraw_ico_eraser_pressed:I = 0x7f020071

.field public static final freedraw_ico_freeline_normal:I = 0x7f020072

.field public static final freedraw_ico_freeline_pressed:I = 0x7f020073

.field public static final freedraw_ico_lasso_normal:I = 0x7f020074

.field public static final freedraw_ico_lasso_pressed:I = 0x7f020075

.field public static final freedraw_ico_pen_normal:I = 0x7f020076

.field public static final freedraw_ico_pen_pressed:I = 0x7f020077

.field public static final freedraw_ico_ruler_normal:I = 0x7f020078

.field public static final freedraw_ico_ruler_selected:I = 0x7f020079

.field public static final freedraw_ico_shape_normal:I = 0x7f02007a

.field public static final freedraw_ico_shape_pressed:I = 0x7f02007b

.field public static final freedraw_ico_sticky_normal:I = 0x7f02007c

.field public static final freedraw_ico_sticky_pressed:I = 0x7f02007d

.field public static final freedraw_ico_text_normal:I = 0x7f02007e

.field public static final freedraw_ico_text_pressed:I = 0x7f02007f

.field public static final freedraw_ico_thickness_01_normal:I = 0x7f020080

.field public static final freedraw_ico_thickness_01_pressed:I = 0x7f020081

.field public static final freedraw_ico_thickness_02_normal:I = 0x7f020082

.field public static final freedraw_ico_thickness_02_pressed:I = 0x7f020083

.field public static final freedraw_ico_thickness_03_normal:I = 0x7f020084

.field public static final freedraw_ico_thickness_03_pressed:I = 0x7f020085

.field public static final freedraw_ico_thickness_04_normal:I = 0x7f020086

.field public static final freedraw_ico_thickness_04_pressed:I = 0x7f020087

.field public static final freedraw_ico_thickness_05_normal:I = 0x7f020088

.field public static final freedraw_ico_thickness_05_pressed:I = 0x7f020089

.field public static final freedraw_ico_thickness_06_normal:I = 0x7f02008a

.field public static final freedraw_ico_thickness_06_pressed:I = 0x7f02008b

.field public static final fx_textfield_selector:I = 0x7f02008c

.field public static final home_thumbnail_dummybg_sheet:I = 0x7f02008d

.field public static final home_thumbnail_dummybg_slide:I = 0x7f02008e

.field public static final home_thumbnail_dummybg_txt:I = 0x7f02008f

.field public static final home_thumbnail_dummybg_wordpdf:I = 0x7f020090

.field public static final ico_ani_sequence:I = 0x7f020091

.field public static final ico_ani_sequence_last:I = 0x7f020092

.field public static final ico_ascending_disabled:I = 0x7f020093

.field public static final ico_ascending_normal:I = 0x7f020094

.field public static final ico_attention:I = 0x7f020095

.field public static final ico_bookmark:I = 0x7f020096

.field public static final ico_browser_embedded_memory_normal:I = 0x7f020097

.field public static final ico_browser_embedded_memory_pressed:I = 0x7f020098

.field public static final ico_browser_folder_normal:I = 0x7f020099

.field public static final ico_browser_folder_pressed:I = 0x7f02009a

.field public static final ico_browser_sdcard_normal:I = 0x7f02009b

.field public static final ico_browser_sdcard_pressed:I = 0x7f02009c

.field public static final ico_browser_usbdrive_normal:I = 0x7f02009d

.field public static final ico_browser_usbdrive_pressed:I = 0x7f02009e

.field public static final ico_controller_next:I = 0x7f02009f

.field public static final ico_controller_next_disabled:I = 0x7f0200a0

.field public static final ico_controller_next_selected:I = 0x7f0200a1

.field public static final ico_controller_pause:I = 0x7f0200a2

.field public static final ico_controller_pause_disabled:I = 0x7f0200a3

.field public static final ico_controller_pause_selected:I = 0x7f0200a4

.field public static final ico_controller_play:I = 0x7f0200a5

.field public static final ico_controller_play_disabled:I = 0x7f0200a6

.field public static final ico_controller_play_selected:I = 0x7f0200a7

.field public static final ico_controller_previous:I = 0x7f0200a8

.field public static final ico_controller_previous_disabled:I = 0x7f0200a9

.field public static final ico_controller_previous_selected:I = 0x7f0200aa

.field public static final ico_delete:I = 0x7f0200ab

.field public static final ico_delete_normal:I = 0x7f0200ac

.field public static final ico_delete_pressed:I = 0x7f0200ad

.field public static final ico_delete_selected:I = 0x7f0200ae

.field public static final ico_descending_disabled:I = 0x7f0200af

.field public static final ico_descending_normal:I = 0x7f0200b0

.field public static final ico_descending_pressed:I = 0x7f0200b1

.field public static final ico_email:I = 0x7f0200b2

.field public static final ico_embedded_memory_nosub_normal:I = 0x7f0200b3

.field public static final ico_file_apkexe:I = 0x7f0200b4

.field public static final ico_file_contact:I = 0x7f0200b5

.field public static final ico_file_csv:I = 0x7f0200b6

.field public static final ico_file_doc:I = 0x7f0200b7

.field public static final ico_file_docx:I = 0x7f0200b8

.field public static final ico_file_folder_nosub:I = 0x7f0200b9

.field public static final ico_file_folder_toupper:I = 0x7f0200ba

.field public static final ico_file_html:I = 0x7f0200bb

.field public static final ico_file_hwp:I = 0x7f0200bc

.field public static final ico_file_image:I = 0x7f0200bd

.field public static final ico_file_movie:I = 0x7f0200be

.field public static final ico_file_pdf:I = 0x7f0200bf

.field public static final ico_file_pps:I = 0x7f0200c0

.field public static final ico_file_ppt:I = 0x7f0200c1

.field public static final ico_file_pptx:I = 0x7f0200c2

.field public static final ico_file_rtf:I = 0x7f0200c3

.field public static final ico_file_sound:I = 0x7f0200c4

.field public static final ico_file_txt:I = 0x7f0200c5

.field public static final ico_file_unknown:I = 0x7f0200c6

.field public static final ico_file_vcs:I = 0x7f0200c7

.field public static final ico_file_vnt:I = 0x7f0200c8

.field public static final ico_file_xls:I = 0x7f0200c9

.field public static final ico_file_xlsx:I = 0x7f0200ca

.field public static final ico_file_xml:I = 0x7f0200cb

.field public static final ico_file_zip:I = 0x7f0200cc

.field public static final ico_folder_nosub_normal:I = 0x7f0200cd

.field public static final ico_index_file:I = 0x7f0200ce

.field public static final ico_index_folder:I = 0x7f0200cf

.field public static final ico_inline_allselect_disabled:I = 0x7f0200d0

.field public static final ico_inline_allselect_normal:I = 0x7f0200d1

.field public static final ico_inline_allselect_pressed:I = 0x7f0200d2

.field public static final ico_inline_allselect_selector:I = 0x7f0200d3

.field public static final ico_inline_copy_normal:I = 0x7f0200d4

.field public static final ico_inline_copy_pressed:I = 0x7f0200d5

.field public static final ico_inline_copy_selector:I = 0x7f0200d6

.field public static final ico_inline_cut_normal:I = 0x7f0200d7

.field public static final ico_inline_cut_pressed:I = 0x7f0200d8

.field public static final ico_inline_cut_selector:I = 0x7f0200d9

.field public static final ico_inline_delete_normal:I = 0x7f0200da

.field public static final ico_inline_delete_pressed:I = 0x7f0200db

.field public static final ico_inline_delete_selector:I = 0x7f0200dc

.field public static final ico_inline_hyperlink_normal:I = 0x7f0200dd

.field public static final ico_inline_hyperlink_pressed:I = 0x7f0200de

.field public static final ico_inline_hyperlink_selector:I = 0x7f0200df

.field public static final ico_inline_insert_normal:I = 0x7f0200e0

.field public static final ico_inline_insert_pressed:I = 0x7f0200e1

.field public static final ico_inline_memo_normal:I = 0x7f0200e2

.field public static final ico_inline_memo_pressed:I = 0x7f0200e3

.field public static final ico_inline_memo_selector:I = 0x7f0200e4

.field public static final ico_inline_more_normal:I = 0x7f0200e5

.field public static final ico_inline_more_pressed:I = 0x7f0200e6

.field public static final ico_inline_more_selector:I = 0x7f0200e7

.field public static final ico_inline_paste_normal:I = 0x7f0200e8

.field public static final ico_inline_paste_pressed:I = 0x7f0200e9

.field public static final ico_inline_paste_selector:I = 0x7f0200ea

.field public static final ico_inline_sticky_normal:I = 0x7f0200eb

.field public static final ico_inline_sticky_pressed:I = 0x7f0200ec

.field public static final ico_inline_sticky_selector:I = 0x7f0200ed

.field public static final ico_list_nextmove_normal:I = 0x7f0200ee

.field public static final ico_list_nextmove_pressed:I = 0x7f0200ef

.field public static final ico_memo_delete_selector:I = 0x7f0200f0

.field public static final ico_memo_next_selector:I = 0x7f0200f1

.field public static final ico_memo_prev_selector:I = 0x7f0200f2

.field public static final ico_menubar_delete_disabled:I = 0x7f0200f3

.field public static final ico_menubar_delete_normal:I = 0x7f0200f4

.field public static final ico_multiplication:I = 0x7f0200f5

.field public static final ico_password:I = 0x7f0200f6

.field public static final ico_pdf_arrow:I = 0x7f0200f7

.field public static final ico_pdf_arrow_normal:I = 0x7f0200f8

.field public static final ico_pdf_arrow_pressed:I = 0x7f0200f9

.field public static final ico_pdf_cloud:I = 0x7f0200fa

.field public static final ico_pdf_cloud_normal:I = 0x7f0200fb

.field public static final ico_pdf_cloud_pressed:I = 0x7f0200fc

.field public static final ico_pdf_connect:I = 0x7f0200fd

.field public static final ico_pdf_connect_normal:I = 0x7f0200fe

.field public static final ico_pdf_connect_pressed:I = 0x7f0200ff

.field public static final ico_pdf_draw:I = 0x7f020100

.field public static final ico_pdf_highlight:I = 0x7f020101

.field public static final ico_pdf_line:I = 0x7f020102

.field public static final ico_pdf_line_normal:I = 0x7f020103

.field public static final ico_pdf_line_pressed:I = 0x7f020104

.field public static final ico_pdf_oval:I = 0x7f020105

.field public static final ico_pdf_oval_normal:I = 0x7f020106

.field public static final ico_pdf_oval_pressed:I = 0x7f020107

.field public static final ico_pdf_polygon:I = 0x7f020108

.field public static final ico_pdf_polygon_normal:I = 0x7f020109

.field public static final ico_pdf_polygon_pressed:I = 0x7f02010a

.field public static final ico_pdf_rectangle:I = 0x7f02010b

.field public static final ico_pdf_rectangle_normal:I = 0x7f02010c

.field public static final ico_pdf_rectangle_pressed:I = 0x7f02010d

.field public static final ico_pdf_stamp:I = 0x7f02010e

.field public static final ico_pdf_sticky:I = 0x7f02010f

.field public static final ico_pdf_strikethrough:I = 0x7f020110

.field public static final ico_pdf_underline:I = 0x7f020111

.field public static final ico_sdcard_nosub_normal:I = 0x7f020112

.field public static final ico_selection_normal:I = 0x7f020113

.field public static final ico_selection_pressed:I = 0x7f020114

.field public static final ico_selection_radio1:I = 0x7f020115

.field public static final ico_usbdrive_nosub_normal:I = 0x7f020116

.field public static final img_bubble:I = 0x7f020117

.field public static final img_color_picker:I = 0x7f020118

.field public static final img_divider:I = 0x7f020119

.field public static final img_divider_memo:I = 0x7f02011a

.field public static final img_divider_repeat:I = 0x7f02011b

.field public static final img_guide:I = 0x7f02011c

.field public static final img_no_sdcard:I = 0x7f02011d

.field public static final img_no_toc:I = 0x7f02011e

.field public static final info_bg_normal:I = 0x7f02011f

.field public static final infraware_ci:I = 0x7f020120

.field public static final infraware_ci_pressed:I = 0x7f020121

.field public static final inlinepopup_bg:I = 0x7f020122

.field public static final keypad_ico_delete_normal:I = 0x7f020123

.field public static final keypad_ico_delete_pressed:I = 0x7f020124

.field public static final keypad_ico_delete_selector:I = 0x7f020125

.field public static final keypad_ico_space_normal:I = 0x7f020126

.field public static final keypad_ico_space_pressed:I = 0x7f020127

.field public static final keypad_normal:I = 0x7f020128

.field public static final keypad_pressed:I = 0x7f020129

.field public static final keypad_tap_selector:I = 0x7f02012a

.field public static final lasso_title_selector:I = 0x7f02012b

.field public static final launcher:I = 0x7f02012c

.field public static final list_check_selector:I = 0x7f02012d

.field public static final list_pressed:I = 0x7f02012e

.field public static final list_selected:I = 0x7f02012f

.field public static final list_selector_cell_event:I = 0x7f020130

.field public static final marker_highlighter_type_selector:I = 0x7f020131

.field public static final marker_pen_type_selector:I = 0x7f020132

.field public static final marker_thickness_progress_selector:I = 0x7f020133

.field public static final marker_thickness_thumb_selector:I = 0x7f020134

.field public static final memo_bg_pressed:I = 0x7f020135

.field public static final memo_bg_pressed_selector:I = 0x7f020136

.field public static final memo_ico_delete_normal:I = 0x7f020137

.field public static final memo_ico_delete_pressed:I = 0x7f020138

.field public static final memo_ico_next_disabled:I = 0x7f020139

.field public static final memo_ico_next_normal:I = 0x7f02013a

.field public static final memo_ico_prev_normal:I = 0x7f02013b

.field public static final memo_ico_previous_disabled:I = 0x7f02013c

.field public static final menu_check_button:I = 0x7f02013d

.field public static final menu_icon_annotation:I = 0x7f02013e

.field public static final menu_icon_annotation_d:I = 0x7f02013f

.field public static final menu_icon_annotation_n:I = 0x7f020140

.field public static final menu_icon_annotation_p:I = 0x7f020141

.field public static final menu_icon_annotationlist:I = 0x7f020142

.field public static final menu_icon_annotationlist_d:I = 0x7f020143

.field public static final menu_icon_annotationlist_n:I = 0x7f020144

.field public static final menu_icon_annotationlist_p:I = 0x7f020145

.field public static final menu_icon_bookmark:I = 0x7f020146

.field public static final menu_icon_bookmark_d:I = 0x7f020147

.field public static final menu_icon_bookmark_n:I = 0x7f020148

.field public static final menu_icon_bookmark_p:I = 0x7f020149

.field public static final menu_icon_freeze:I = 0x7f02014a

.field public static final menu_icon_freeze_d:I = 0x7f02014b

.field public static final menu_icon_freeze_n:I = 0x7f02014c

.field public static final menu_icon_freeze_p:I = 0x7f02014d

.field public static final menu_icon_help_d:I = 0x7f02014e

.field public static final menu_icon_help_n:I = 0x7f02014f

.field public static final menu_icon_help_p:I = 0x7f020150

.field public static final menu_icon_info:I = 0x7f020151

.field public static final menu_icon_info_d:I = 0x7f020152

.field public static final menu_icon_info_n:I = 0x7f020153

.field public static final menu_icon_info_p:I = 0x7f020154

.field public static final menu_icon_realignment:I = 0x7f020155

.field public static final menu_icon_realignment_d:I = 0x7f020156

.field public static final menu_icon_realignment_n:I = 0x7f020157

.field public static final menu_icon_realignment_p:I = 0x7f020158

.field public static final menu_icon_setting:I = 0x7f020159

.field public static final menu_icon_setting_n:I = 0x7f02015a

.field public static final menu_icon_setting_p:I = 0x7f02015b

.field public static final menu_icon_slidenote:I = 0x7f02015c

.field public static final menu_icon_slidenote_d:I = 0x7f02015d

.field public static final menu_icon_slidenote_n:I = 0x7f02015e

.field public static final menu_icon_slidenote_p:I = 0x7f02015f

.field public static final menu_icon_toc:I = 0x7f020160

.field public static final menu_icon_toc_d:I = 0x7f020161

.field public static final menu_icon_toc_n:I = 0x7f020162

.field public static final menu_icon_toc_p:I = 0x7f020163

.field public static final menu_icon_tts:I = 0x7f020164

.field public static final menu_icon_tts_d:I = 0x7f020165

.field public static final menu_icon_tts_n:I = 0x7f020166

.field public static final menu_icon_tts_p:I = 0x7f020167

.field public static final menu_icon_viewmode_d:I = 0x7f020168

.field public static final menu_icon_viewmode_n:I = 0x7f020169

.field public static final menu_icon_viewmode_p:I = 0x7f02016a

.field public static final menu_icon_viewpage:I = 0x7f02016b

.field public static final menu_icon_viewpage_d:I = 0x7f02016c

.field public static final menu_icon_viewpage_n:I = 0x7f02016d

.field public static final menu_icon_viewpage_p:I = 0x7f02016e

.field public static final menu_icon_viewsetting:I = 0x7f02016f

.field public static final menu_icon_viewsetting_d:I = 0x7f020170

.field public static final menu_icon_viewsetting_n:I = 0x7f020171

.field public static final menu_icon_viewsetting_p:I = 0x7f020172

.field public static final menu_icon_viewslide:I = 0x7f020173

.field public static final menu_icon_viewslide_d:I = 0x7f020174

.field public static final menu_icon_viewslide_n:I = 0x7f020175

.field public static final menu_icon_viewslide_p:I = 0x7f020176

.field public static final menubar_bg:I = 0x7f020177

.field public static final mode_bg:I = 0x7f020178

.field public static final option_bg:I = 0x7f020179

.field public static final panel_btn_default_normal:I = 0x7f02017a

.field public static final panel_ico_font_arial_normal:I = 0x7f02017b

.field public static final panel_ico_font_arial_pressed:I = 0x7f02017c

.field public static final panel_ico_font_batang_eng_normal:I = 0x7f02017d

.field public static final panel_ico_font_batang_eng_pressed:I = 0x7f02017e

.field public static final panel_ico_font_batang_normal:I = 0x7f02017f

.field public static final panel_ico_font_batang_pressed:I = 0x7f020180

.field public static final panel_ico_font_courier_normal:I = 0x7f020181

.field public static final panel_ico_font_courier_pressed:I = 0x7f020182

.field public static final panel_ico_font_dotum_eng_normal:I = 0x7f020183

.field public static final panel_ico_font_dotum_eng_pressed:I = 0x7f020184

.field public static final panel_ico_font_dotum_normal:I = 0x7f020185

.field public static final panel_ico_font_dotum_pressed:I = 0x7f020186

.field public static final panel_ico_font_gulim_eng_normal:I = 0x7f020187

.field public static final panel_ico_font_gulim_eng_pressed:I = 0x7f020188

.field public static final panel_ico_font_gulim_normal:I = 0x7f020189

.field public static final panel_ico_font_gulim_pressed:I = 0x7f02018a

.field public static final panel_ico_font_tahoma_normal:I = 0x7f02018b

.field public static final panel_ico_font_tahoma_pressed:I = 0x7f02018c

.field public static final panel_ico_font_times_normal:I = 0x7f02018d

.field public static final panel_ico_font_times_pressed:I = 0x7f02018e

.field public static final panel_ico_font_verdana_normal:I = 0x7f02018f

.field public static final panel_ico_font_verdana_pressed:I = 0x7f020190

.field public static final panel_wheel_btn_left_disable:I = 0x7f020191

.field public static final panel_wheel_btn_left_normal:I = 0x7f020192

.field public static final panel_wheel_btn_right_disable:I = 0x7f020193

.field public static final panel_wheel_btn_right_normal:I = 0x7f020194

.field public static final pen_title_selector:I = 0x7f020195

.field public static final pendrawing_brush_selector:I = 0x7f020196

.field public static final pendrawing_highlighter_selector:I = 0x7f020197

.field public static final pendrawing_pen_selector:I = 0x7f020198

.field public static final pendrawing_ruler_selector:I = 0x7f020199

.field public static final pendrawing_size_1_selector:I = 0x7f02019a

.field public static final pendrawing_size_2_selector:I = 0x7f02019b

.field public static final pendrawing_size_3_selector:I = 0x7f02019c

.field public static final pendrawing_size_4_selector:I = 0x7f02019d

.field public static final pendrawing_size_5_selector:I = 0x7f02019e

.field public static final pendrawing_size_6_selector:I = 0x7f02019f

.field public static final po_product_logo_bg_selector:I = 0x7f0201a0

.field public static final pointer_blue_onetouch:I = 0x7f0201a1

.field public static final pointer_green_onetouch:I = 0x7f0201a2

.field public static final pointer_purple_onetouch:I = 0x7f0201a3

.field public static final pointer_red_onetouch:I = 0x7f0201a4

.field public static final pointer_yellow_onetouch:I = 0x7f0201a5

.field public static final popover_bg:I = 0x7f0201a6

.field public static final popover_bg_02:I = 0x7f0201a7

.field public static final popover_bg_04:I = 0x7f0201a8

.field public static final popover_btn_ascending_disabled:I = 0x7f0201a9

.field public static final popover_btn_ascending_normal:I = 0x7f0201aa

.field public static final popover_btn_ascending_pressed:I = 0x7f0201ab

.field public static final popover_btn_background_selector:I = 0x7f0201ac

.field public static final popover_btn_bg:I = 0x7f0201ad

.field public static final popover_btn_bg_selector:I = 0x7f0201ae

.field public static final popover_btn_descending_disbled:I = 0x7f0201af

.field public static final popover_btn_descending_normal:I = 0x7f0201b0

.field public static final popover_btn_descending_pressed:I = 0x7f0201b1

.field public static final popover_ico_annotation_disabled:I = 0x7f0201b2

.field public static final popover_ico_annotation_normal:I = 0x7f0201b3

.field public static final popover_ico_annotation_pressed:I = 0x7f0201b4

.field public static final popover_ico_docinfo:I = 0x7f0201b5

.field public static final popover_ico_docinfo_disabled:I = 0x7f0201b6

.field public static final popover_ico_docinfo_normal:I = 0x7f0201b7

.field public static final popover_ico_docinfo_pressed:I = 0x7f0201b8

.field public static final popover_ico_edit_normal:I = 0x7f0201b9

.field public static final popover_ico_email:I = 0x7f0201ba

.field public static final popover_ico_email_disabled:I = 0x7f0201bb

.field public static final popover_ico_email_normal:I = 0x7f0201bc

.field public static final popover_ico_email_pressed:I = 0x7f0201bd

.field public static final popover_ico_etc_normal:I = 0x7f0201be

.field public static final popover_ico_etc_pressed:I = 0x7f0201bf

.field public static final popover_ico_exportpdf:I = 0x7f0201c0

.field public static final popover_ico_exportpdf_disabled:I = 0x7f0201c1

.field public static final popover_ico_exportpdf_normal:I = 0x7f0201c2

.field public static final popover_ico_exportpdf_pressed:I = 0x7f0201c3

.field public static final popover_ico_image_normal:I = 0x7f0201c4

.field public static final popover_ico_image_pressed:I = 0x7f0201c5

.field public static final popover_ico_lasso_normal:I = 0x7f0201c6

.field public static final popover_ico_lasso_pressed:I = 0x7f0201c7

.field public static final popover_ico_notice:I = 0x7f0201c8

.field public static final popover_ico_notice_disabled:I = 0x7f0201c9

.field public static final popover_ico_notice_normal:I = 0x7f0201ca

.field public static final popover_ico_notice_pressed:I = 0x7f0201cb

.field public static final popover_ico_print:I = 0x7f0201cc

.field public static final popover_ico_print_disabled:I = 0x7f0201cd

.field public static final popover_ico_print_normal:I = 0x7f0201ce

.field public static final popover_ico_print_pressed:I = 0x7f0201cf

.field public static final print_bg_pressed_selector:I = 0x7f0201d0

.field public static final print_launcher_icon:I = 0x7f0201d1

.field public static final quick_scroll_normal:I = 0x7f0201d2

.field public static final quick_scroll_pressed:I = 0x7f0201d3

.field public static final sb_next_selector:I = 0x7f0201d4

.field public static final sb_prev_selector:I = 0x7f0201d5

.field public static final searchset_selector:I = 0x7f0201d6

.field public static final select_btn_disabled:I = 0x7f0201d7

.field public static final select_btn_normal:I = 0x7f0201d8

.field public static final select_btn_pressed:I = 0x7f0201d9

.field public static final sheet_btn_filter_more_normal:I = 0x7f0201da

.field public static final sheet_btn_filter_more_pressed:I = 0x7f0201db

.field public static final sheet_function_list_divider:I = 0x7f0201dc

.field public static final sheet_height_normal:I = 0x7f0201dd

.field public static final sheet_height_pressed:I = 0x7f0201de

.field public static final sheet_lock_img_selector:I = 0x7f0201df

.field public static final sheet_lock_normal:I = 0x7f0201e0

.field public static final sheet_lock_selected:I = 0x7f0201e1

.field public static final sheet_sort_asc_btn:I = 0x7f0201e2

.field public static final sheet_sort_asc_btn_autofilter:I = 0x7f0201e3

.field public static final sheet_sort_des_btn:I = 0x7f0201e4

.field public static final sheet_sort_des_btn_autofilter:I = 0x7f0201e5

.field public static final sheet_tab_add_normal:I = 0x7f0201e6

.field public static final sheet_tab_add_pressed:I = 0x7f0201e7

.field public static final sheet_tab_btn_background:I = 0x7f0201e8

.field public static final sheet_tab_default:I = 0x7f0201e9

.field public static final sheet_tab_normal:I = 0x7f0201ea

.field public static final sheet_tab_pressed:I = 0x7f0201eb

.field public static final sheet_tab_selected:I = 0x7f0201ec

.field public static final sheet_width_normal:I = 0x7f0201ed

.field public static final sheet_width_pressed:I = 0x7f0201ee

.field public static final slide_bg_brush_preview:I = 0x7f0201ef

.field public static final slide_bg_icon_selector:I = 0x7f0201f0

.field public static final slide_bg_number:I = 0x7f0201f1

.field public static final slide_btn_center_normal:I = 0x7f0201f2

.field public static final slide_btn_center_pressed:I = 0x7f0201f3

.field public static final slide_btn_center_selected:I = 0x7f0201f4

.field public static final slide_btn_close_normal:I = 0x7f0201f5

.field public static final slide_btn_close_pressed:I = 0x7f0201f6

.field public static final slide_btn_left_normal:I = 0x7f0201f7

.field public static final slide_btn_left_pressed:I = 0x7f0201f8

.field public static final slide_btn_left_selected:I = 0x7f0201f9

.field public static final slide_btn_open_normal:I = 0x7f0201fa

.field public static final slide_btn_open_pressed:I = 0x7f0201fb

.field public static final slide_btn_pen_normal:I = 0x7f0201fc

.field public static final slide_btn_pen_selected:I = 0x7f0201fd

.field public static final slide_btn_pointer_normal:I = 0x7f0201fe

.field public static final slide_btn_pointer_selected:I = 0x7f0201ff

.field public static final slide_btn_popup_bg:I = 0x7f020200

.field public static final slide_btn_right_normal:I = 0x7f020201

.field public static final slide_btn_right_pressed:I = 0x7f020202

.field public static final slide_btn_right_selected:I = 0x7f020203

.field public static final slide_btn_slide_normal:I = 0x7f020204

.field public static final slide_btn_slide_selected:I = 0x7f020205

.field public static final slide_close_normal:I = 0x7f020206

.field public static final slide_close_pressed:I = 0x7f020207

.field public static final slide_contents_bg:I = 0x7f020208

.field public static final slide_controlloer_bg:I = 0x7f020209

.field public static final slide_controlloer_btn_first_normal:I = 0x7f02020a

.field public static final slide_controlloer_btn_first_pressed:I = 0x7f02020b

.field public static final slide_controlloer_btn_last_normal:I = 0x7f02020c

.field public static final slide_controlloer_btn_last_pressed:I = 0x7f02020d

.field public static final slide_controlloer_btn_pause_normal:I = 0x7f02020e

.field public static final slide_controlloer_btn_pause_pressed:I = 0x7f02020f

.field public static final slide_controlloer_btn_resume_normal:I = 0x7f020210

.field public static final slide_controlloer_btn_resume_pressed:I = 0x7f020211

.field public static final slide_ico_add_disabled:I = 0x7f020212

.field public static final slide_ico_add_normal:I = 0x7f020213

.field public static final slide_ico_add_pressed:I = 0x7f020214

.field public static final slide_ico_close_normal:I = 0x7f020215

.field public static final slide_ico_close_pressed:I = 0x7f020216

.field public static final slide_ico_eraseall_disabled:I = 0x7f020217

.field public static final slide_ico_eraseall_normal:I = 0x7f020218

.field public static final slide_ico_eraseall_pressed:I = 0x7f020219

.field public static final slide_ico_eraser_disabled:I = 0x7f02021a

.field public static final slide_ico_eraser_normal:I = 0x7f02021b

.field public static final slide_ico_eraser_pressed:I = 0x7f02021c

.field public static final slide_ico_hide:I = 0x7f02021d

.field public static final slide_ico_highlighter_normal:I = 0x7f02021e

.field public static final slide_ico_highlighter_pressed:I = 0x7f02021f

.field public static final slide_ico_highlighter_selected:I = 0x7f020220

.field public static final slide_ico_open_normal:I = 0x7f020221

.field public static final slide_ico_open_pressed:I = 0x7f020222

.field public static final slide_ico_pen:I = 0x7f020223

.field public static final slide_ico_pen_normal:I = 0x7f020224

.field public static final slide_ico_pen_pressed:I = 0x7f020225

.field public static final slide_ico_pen_selected:I = 0x7f020226

.field public static final slide_ico_pointer:I = 0x7f020227

.field public static final slide_ico_pointer_blue:I = 0x7f020228

.field public static final slide_ico_pointer_blue_normal:I = 0x7f020229

.field public static final slide_ico_pointer_blue_pressed:I = 0x7f02022a

.field public static final slide_ico_pointer_green:I = 0x7f02022b

.field public static final slide_ico_pointer_green_normal:I = 0x7f02022c

.field public static final slide_ico_pointer_green_pressed:I = 0x7f02022d

.field public static final slide_ico_pointer_red_normal:I = 0x7f02022e

.field public static final slide_ico_pointer_red_pressed:I = 0x7f02022f

.field public static final slide_ico_pointer_violet:I = 0x7f020230

.field public static final slide_ico_pointer_violet_normal:I = 0x7f020231

.field public static final slide_ico_pointer_violet_pressed:I = 0x7f020232

.field public static final slide_ico_pointer_yellow:I = 0x7f020233

.field public static final slide_ico_pointer_yellow_normal:I = 0x7f020234

.field public static final slide_ico_pointer_yellow_pressed:I = 0x7f020235

.field public static final slide_ico_settings_normal:I = 0x7f020236

.field public static final slide_ico_settings_pressed:I = 0x7f020237

.field public static final slide_ico_show_disabled:I = 0x7f020238

.field public static final slide_ico_show_normal:I = 0x7f020239

.field public static final slide_ico_show_pressed:I = 0x7f02023a

.field public static final slide_ico_slide:I = 0x7f02023b

.field public static final slide_ico_slide_normal:I = 0x7f02023c

.field public static final slide_move_icon:I = 0x7f02023d

.field public static final slide_move_icon_landscape:I = 0x7f02023e

.field public static final slide_next_page:I = 0x7f02023f

.field public static final slide_next_page_normal:I = 0x7f020240

.field public static final slide_next_page_pressed:I = 0x7f020241

.field public static final slide_prev_page_normal:I = 0x7f020242

.field public static final slide_prev_page_pressed:I = 0x7f020243

.field public static final slide_previous_page:I = 0x7f020244

.field public static final slide_slider_btn_normal:I = 0x7f020245

.field public static final slide_slider_btn_pressed:I = 0x7f020246

.field public static final slide_slider_max:I = 0x7f020247

.field public static final slide_slider_min:I = 0x7f020248

.field public static final slide_slider_off:I = 0x7f020249

.field public static final slide_slider_on:I = 0x7f02024a

.field public static final slide_slider_on2:I = 0x7f02024b

.field public static final slide_thumbnail_hide_normal:I = 0x7f02024c

.field public static final slide_thumbnail_hide_selected:I = 0x7f02024d

.field public static final slide_thumbnail_normal:I = 0x7f02024e

.field public static final slide_thumbnail_selected:I = 0x7f02024f

.field public static final slidemanage_add_selector:I = 0x7f020250

.field public static final slidemanage_btn_background_selector:I = 0x7f020251

.field public static final slidemanage_close_btn_image:I = 0x7f020252

.field public static final slidemanage_open_btn_image:I = 0x7f020253

.field public static final slidemanage_slideshow_selector:I = 0x7f020254

.field public static final slideshow_close_selector:I = 0x7f020255

.field public static final slideshow_controller_first_selector:I = 0x7f020256

.field public static final slideshow_controller_last_selector:I = 0x7f020257

.field public static final slideshow_controller_pause_selector:I = 0x7f020258

.field public static final slideshow_controller_resume_selector:I = 0x7f020259

.field public static final slideshow_mode_btn_background_selector:I = 0x7f02025a

.field public static final slideshow_mode_btn_pen_selector:I = 0x7f02025b

.field public static final slideshow_mode_btn_pointer_selector:I = 0x7f02025c

.field public static final slideshow_mode_btn_slide_selector:I = 0x7f02025d

.field public static final slideshow_mode_eraser_selector:I = 0x7f02025e

.field public static final slideshow_mode_eraserall_selector:I = 0x7f02025f

.field public static final slideshow_mode_highlighter_selector:I = 0x7f020260

.field public static final slideshow_mode_pen_selector:I = 0x7f020261

.field public static final slideshow_mode_pointer_blue_selector:I = 0x7f020262

.field public static final slideshow_mode_pointer_green_selector:I = 0x7f020263

.field public static final slideshow_mode_pointer_purple_selector:I = 0x7f020264

.field public static final slideshow_mode_pointer_red_selector:I = 0x7f020265

.field public static final slideshow_mode_pointer_yellow_selector:I = 0x7f020266

.field public static final slideshow_mode_select_center_selector:I = 0x7f020267

.field public static final slideshow_mode_select_left_selector:I = 0x7f020268

.field public static final slideshow_mode_select_right_selector:I = 0x7f020269

.field public static final slideshow_note_close_selector:I = 0x7f02026a

.field public static final slideshow_note_open_selector:I = 0x7f02026b

.field public static final slideshow_page_next_selector:I = 0x7f02026c

.field public static final slideshow_page_prev_selector:I = 0x7f02026d

.field public static final slideshow_seekbar_progress_selector:I = 0x7f02026e

.field public static final slideshow_settings_selector:I = 0x7f02026f

.field public static final slideshow_thumbnail:I = 0x7f020270

.field public static final tableofcontent_close:I = 0x7f020271

.field public static final tableofcontent_open:I = 0x7f020272

.field public static final te_bg_pressed_selector2:I = 0x7f020273

.field public static final te_btn_quick_scroll:I = 0x7f020274

.field public static final te_morepopup_background:I = 0x7f020275

.field public static final te_replace_all_text:I = 0x7f020276

.field public static final text_theme_01:I = 0x7f020277

.field public static final text_theme_02:I = 0x7f020278

.field public static final text_theme_03:I = 0x7f020279

.field public static final text_theme_selected:I = 0x7f02027a

.field public static final textfield_delete:I = 0x7f02027b

.field public static final textfield_delete_longpressed:I = 0x7f02027c

.field public static final textfield_delete_normal:I = 0x7f02027d

.field public static final textfield_disabled:I = 0x7f02027e

.field public static final textfield_normal:I = 0x7f02027f

.field public static final textfield_pressed:I = 0x7f020280

.field public static final textfield_selected:I = 0x7f020281

.field public static final title_bg:I = 0x7f020282

.field public static final title_bg_etc:I = 0x7f020283

.field public static final title_btn_normal:I = 0x7f020284

.field public static final title_btn_pressed:I = 0x7f020285

.field public static final title_dropdown_normal:I = 0x7f020286

.field public static final title_dropdown_pressed:I = 0x7f020287

.field public static final title_ico_account_normal:I = 0x7f020288

.field public static final title_ico_account_pressed:I = 0x7f020289

.field public static final title_ico_allchange:I = 0x7f02028a

.field public static final title_ico_allchange_disabled:I = 0x7f02028b

.field public static final title_ico_allchange_normal:I = 0x7f02028c

.field public static final title_ico_allchange_pressed:I = 0x7f02028d

.field public static final title_ico_change:I = 0x7f02028e

.field public static final title_ico_change_disabled:I = 0x7f02028f

.field public static final title_ico_change_normal:I = 0x7f020290

.field public static final title_ico_change_pressed:I = 0x7f020291

.field public static final title_ico_check_normal:I = 0x7f020292

.field public static final title_ico_check_pressed:I = 0x7f020293

.field public static final title_ico_csv_more:I = 0x7f020294

.field public static final title_ico_doc_more:I = 0x7f020295

.field public static final title_ico_docx_more:I = 0x7f020296

.field public static final title_ico_eraser_normal:I = 0x7f020297

.field public static final title_ico_eraser_pressed:I = 0x7f020298

.field public static final title_ico_find_no_dropdown_normal:I = 0x7f020299

.field public static final title_ico_find_no_dropdown_pressed:I = 0x7f02029a

.field public static final title_ico_find_normal:I = 0x7f02029b

.field public static final title_ico_find_pressed:I = 0x7f02029c

.field public static final title_ico_freewrite_disabled:I = 0x7f02029d

.field public static final title_ico_freewrite_normal:I = 0x7f02029e

.field public static final title_ico_freewrite_pressed:I = 0x7f02029f

.field public static final title_ico_hwp_more:I = 0x7f0202a0

.field public static final title_ico_insert_disabled:I = 0x7f0202a1

.field public static final title_ico_insert_normal:I = 0x7f0202a2

.field public static final title_ico_insert_pressed:I = 0x7f0202a3

.field public static final title_ico_maximum:I = 0x7f0202a4

.field public static final title_ico_maximum_normal:I = 0x7f0202a5

.field public static final title_ico_maximum_pressed:I = 0x7f0202a6

.field public static final title_ico_minimum:I = 0x7f0202a7

.field public static final title_ico_minimum_normal:I = 0x7f0202a8

.field public static final title_ico_minimum_pressed:I = 0x7f0202a9

.field public static final title_ico_more_disabled:I = 0x7f0202aa

.field public static final title_ico_more_normal:I = 0x7f0202ab

.field public static final title_ico_more_pressed:I = 0x7f0202ac

.field public static final title_ico_newfolder_normal:I = 0x7f0202ad

.field public static final title_ico_newfolder_pressed:I = 0x7f0202ae

.field public static final title_ico_next:I = 0x7f0202af

.field public static final title_ico_next_disabled:I = 0x7f0202b0

.field public static final title_ico_next_normal:I = 0x7f0202b1

.field public static final title_ico_next_pressed:I = 0x7f0202b2

.field public static final title_ico_ok_disabled:I = 0x7f0202b3

.field public static final title_ico_ok_normal:I = 0x7f0202b4

.field public static final title_ico_ok_pressed:I = 0x7f0202b5

.field public static final title_ico_pdf_disabled:I = 0x7f0202b6

.field public static final title_ico_pdf_more:I = 0x7f0202b7

.field public static final title_ico_pdf_normal:I = 0x7f0202b8

.field public static final title_ico_pdf_pressed:I = 0x7f0202b9

.field public static final title_ico_polarisoffice:I = 0x7f0202ba

.field public static final title_ico_ppt_more:I = 0x7f0202bb

.field public static final title_ico_pptx_more:I = 0x7f0202bc

.field public static final title_ico_previous:I = 0x7f0202bd

.field public static final title_ico_previous_disabled:I = 0x7f0202be

.field public static final title_ico_previous_normal:I = 0x7f0202bf

.field public static final title_ico_previous_pressed:I = 0x7f0202c0

.field public static final title_ico_print_normal:I = 0x7f0202c1

.field public static final title_ico_print_pressed:I = 0x7f0202c2

.field public static final title_ico_properties_disabled:I = 0x7f0202c3

.field public static final title_ico_properties_normal:I = 0x7f0202c4

.field public static final title_ico_properties_pressed:I = 0x7f0202c5

.field public static final title_ico_redo_disabled:I = 0x7f0202c6

.field public static final title_ico_redo_normal:I = 0x7f0202c7

.field public static final title_ico_redo_pressed:I = 0x7f0202c8

.field public static final title_ico_rtf_more:I = 0x7f0202c9

.field public static final title_ico_save_disabled:I = 0x7f0202ca

.field public static final title_ico_save_normal:I = 0x7f0202cb

.field public static final title_ico_save_pressed:I = 0x7f0202cc

.field public static final title_ico_setting:I = 0x7f0202cd

.field public static final title_ico_setting_normal:I = 0x7f0202ce

.field public static final title_ico_setting_pressed:I = 0x7f0202cf

.field public static final title_ico_sticky_normal:I = 0x7f0202d0

.field public static final title_ico_sticky_pressed:I = 0x7f0202d1

.field public static final title_ico_txt_more:I = 0x7f0202d2

.field public static final title_ico_undo_disabled:I = 0x7f0202d3

.field public static final title_ico_undo_normal:I = 0x7f0202d4

.field public static final title_ico_undo_pressed:I = 0x7f0202d5

.field public static final title_ico_write_disabled:I = 0x7f0202d6

.field public static final title_ico_write_normal:I = 0x7f0202d7

.field public static final title_ico_write_pressed:I = 0x7f0202d8

.field public static final title_ico_xls_more:I = 0x7f0202d9

.field public static final title_ico_xlsx_more:I = 0x7f0202da

.field public static final title_tab:I = 0x7f0202db

.field public static final title_viewer_bg_document:I = 0x7f0202dc

.field public static final title_viewer_bg_etc:I = 0x7f0202dd

.field public static final title_viewer_bg_sheet:I = 0x7f0202de

.field public static final title_viewer_bg_slide:I = 0x7f0202df

.field public static final titlesub_bg:I = 0x7f0202e0

.field public static final titlesub_filemanager_bg:I = 0x7f0202e1

.field public static final titlesub_ico_select_disabled:I = 0x7f0202e2

.field public static final titlesub_ico_select_normal:I = 0x7f0202e3

.field public static final titlesub_ico_select_pressed:I = 0x7f0202e4

.field public static final titlesub_ico_select_selected:I = 0x7f0202e5

.field public static final titlesub_setting_bg:I = 0x7f0202e6

.field public static final toc_btn_closed:I = 0x7f0202e7

.field public static final toc_btn_closed_pressed:I = 0x7f0202e8

.field public static final toc_btn_opened:I = 0x7f0202e9

.field public static final toc_btn_opened_pressed:I = 0x7f0202ea

.field public static final toolbar_btn_bg_selector:I = 0x7f0202eb

.field public static final toolbar_freedraw_ico_alignment_normal:I = 0x7f0202ec

.field public static final toolbar_freedraw_ico_alignment_pressed:I = 0x7f0202ed

.field public static final toolbar_freedraw_ico_eraseall_normal:I = 0x7f0202ee

.field public static final toolbar_freedraw_ico_eraseall_selected:I = 0x7f0202ef

.field public static final toolbar_freedraw_ico_eraser_normal:I = 0x7f0202f0

.field public static final toolbar_freedraw_ico_eraser_selected:I = 0x7f0202f1

.field public static final toolbar_freedraw_ico_freeline_normal:I = 0x7f0202f2

.field public static final toolbar_freedraw_ico_freeline_selected:I = 0x7f0202f3

.field public static final toolbar_freedraw_ico_lasso_normal:I = 0x7f0202f4

.field public static final toolbar_freedraw_ico_lasso_selected:I = 0x7f0202f5

.field public static final toolbar_freedraw_ico_panning:I = 0x7f0202f6

.field public static final toolbar_freedraw_ico_panning_normal:I = 0x7f0202f7

.field public static final toolbar_freedraw_ico_panning_selected:I = 0x7f0202f8

.field public static final toolbar_freedraw_ico_pen_normal:I = 0x7f0202f9

.field public static final toolbar_freedraw_ico_pen_pressed:I = 0x7f0202fa

.field public static final toolbar_freedraw_ico_pen_selected:I = 0x7f0202fb

.field public static final toolbar_freedraw_ico_shape_normal:I = 0x7f0202fc

.field public static final toolbar_freedraw_ico_shape_pressed:I = 0x7f0202fd

.field public static final toolbar_freedraw_ico_shape_selected:I = 0x7f0202fe

.field public static final toolbar_freedraw_ico_sticky_normal:I = 0x7f0202ff

.field public static final toolbar_freedraw_ico_sticky_selected:I = 0x7f020300

.field public static final toolbar_freedraw_ico_text_normal:I = 0x7f020301

.field public static final toolbar_freedraw_ico_text_pressed:I = 0x7f020302

.field public static final toolbar_freedraw_ico_text_selected:I = 0x7f020303

.field public static final touch_fx_01_normal:I = 0x7f020304

.field public static final touch_fx_01_pressed:I = 0x7f020305

.field public static final touch_fx_02_normal:I = 0x7f020306

.field public static final touch_fx_02_pressed:I = 0x7f020307

.field public static final touch_fx_03_normal:I = 0x7f020308

.field public static final touch_fx_03_pressed:I = 0x7f020309

.field public static final touch_fx_04_normal:I = 0x7f02030a

.field public static final touch_fx_04_pressed:I = 0x7f02030b

.field public static final touch_fx_05_normal:I = 0x7f02030c

.field public static final touch_fx_05_pressed:I = 0x7f02030d

.field public static final touch_fx_06_normal:I = 0x7f02030e

.field public static final touch_fx_06_pressed:I = 0x7f02030f

.field public static final touch_fx_07_normal:I = 0x7f020310

.field public static final touch_fx_07_pressed:I = 0x7f020311

.field public static final touch_rotation_normal:I = 0x7f020312

.field public static final touch_rotation_pressed:I = 0x7f020313

.field public static final touch_scale_normal:I = 0x7f020314

.field public static final touch_scale_oneway_normal:I = 0x7f020315

.field public static final touch_scale_oneway_pressed:I = 0x7f020316

.field public static final touch_scale_pressed:I = 0x7f020317

.field public static final touch_selection_normal:I = 0x7f020318

.field public static final touch_selection_pressed:I = 0x7f020319

.field public static final touch_transform_normal:I = 0x7f02031a

.field public static final touch_transform_pressed:I = 0x7f02031b

.field public static final touch_txtselection_bar:I = 0x7f02031c

.field public static final touch_txtselection_bottom_normal:I = 0x7f02031d

.field public static final touch_txtselection_bottom_pressed:I = 0x7f02031e

.field public static final touch_txtselection_center_bottom_normal:I = 0x7f02031f

.field public static final touch_txtselection_center_bottom_pressed:I = 0x7f020320

.field public static final touch_txtselection_left_bottom_normal:I = 0x7f020321

.field public static final touch_txtselection_left_bottom_pressed:I = 0x7f020322

.field public static final touch_txtselection_left_top_normal:I = 0x7f020323

.field public static final touch_txtselection_left_top_pressed:I = 0x7f020324

.field public static final touch_txtselection_right_bottom_normal:I = 0x7f020325

.field public static final touch_txtselection_right_bottom_pressed:I = 0x7f020326

.field public static final touch_txtselection_right_top_normal:I = 0x7f020327

.field public static final touch_txtselection_right_top_pressedl:I = 0x7f020328

.field public static final touch_txtselection_top_normal:I = 0x7f020329

.field public static final touch_txtselection_top_pressed:I = 0x7f02032a

.field public static final transform_bg:I = 0x7f02032b

.field public static final translucent:I = 0x7f020333

.field public static final wheel_btn_center:I = 0x7f02032c

.field public static final wheel_btn_left_disabled:I = 0x7f02032d

.field public static final wheel_btn_left_normal:I = 0x7f02032e

.field public static final wheel_btn_left_pressed:I = 0x7f02032f

.field public static final wheel_btn_right_disabled:I = 0x7f020330

.field public static final wheel_btn_right_normal:I = 0x7f020331

.field public static final wheel_btn_right_pressed:I = 0x7f020332


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 690
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
