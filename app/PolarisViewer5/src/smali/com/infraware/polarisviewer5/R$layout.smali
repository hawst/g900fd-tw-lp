.class public final Lcom/infraware/polarisviewer5/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisviewer5/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final actionbar:I = 0x7f030000

.field public static final actionbar_done:I = 0x7f030001

.field public static final annot_list_item_view:I = 0x7f030002

.field public static final annot_listview:I = 0x7f030003

.field public static final bookclip:I = 0x7f030004

.field public static final cm_action_title_bar:I = 0x7f030005

.field public static final cm_alarm_connect_network:I = 0x7f030006

.field public static final cm_base_web_view:I = 0x7f030007

.field public static final cm_dummy_view:I = 0x7f030008

.field public static final cm_icon_menu_item:I = 0x7f030009

.field public static final cm_menu_item:I = 0x7f03000a

.field public static final cm_menu_popup:I = 0x7f03000b

.field public static final cm_settings:I = 0x7f03000c

.field public static final cm_settings_dictionary:I = 0x7f03000d

.field public static final cm_settings_item:I = 0x7f03000e

.field public static final common_colorpicker:I = 0x7f03000f

.field public static final common_input_number_popup:I = 0x7f030010

.field public static final common_input_number_popup_spinner:I = 0x7f030011

.field public static final common_inputfield:I = 0x7f030012

.field public static final dm_add_printer_account:I = 0x7f030013

.field public static final findbar:I = 0x7f030014

.field public static final fm_actionbar_listitem:I = 0x7f030015

.field public static final fm_actionbar_popup:I = 0x7f030016

.field public static final fm_export_pdf:I = 0x7f030017

.field public static final fm_file_info:I = 0x7f030018

.field public static final fm_file_item:I = 0x7f030019

.field public static final fm_file_list:I = 0x7f03001a

.field public static final icontextlist_row:I = 0x7f03001b

.field public static final input_number:I = 0x7f03001c

.field public static final keypad:I = 0x7f03001d

.field public static final keypad_horizontal:I = 0x7f03001e

.field public static final keypad_horizontal_spinner:I = 0x7f03001f

.field public static final keypad_spinner:I = 0x7f030020

.field public static final layout_dlg:I = 0x7f030021

.field public static final layout_item:I = 0x7f030022

.field public static final listitem_1_btn:I = 0x7f030023

.field public static final listitem_1_btn_checkbox:I = 0x7f030024

.field public static final listitem_1_checkbox:I = 0x7f030025

.field public static final listitem_1_color:I = 0x7f030026

.field public static final listitem_1_icon:I = 0x7f030027

.field public static final listitem_1_imageview:I = 0x7f030028

.field public static final listitem_2_btn:I = 0x7f030029

.field public static final listitem_2_icon:I = 0x7f03002a

.field public static final main:I = 0x7f03002b

.field public static final marker_options:I = 0x7f03002c

.field public static final memo_layout:I = 0x7f03002d

.field public static final page_zoom_info:I = 0x7f03002e

.field public static final panel_button_image:I = 0x7f03002f

.field public static final panel_dictionary_search:I = 0x7f030030

.field public static final panel_dictionary_search_land:I = 0x7f030031

.field public static final password:I = 0x7f030032

.field public static final pdf_tableofcontent_item:I = 0x7f030033

.field public static final pdf_tableofcontent_listview:I = 0x7f030034

.field public static final pendraw_options:I = 0x7f030035

.field public static final po_common_dialog:I = 0x7f030036

.field public static final po_common_progress_dialog:I = 0x7f030037

.field public static final po_office_download:I = 0x7f030038

.field public static final po_office_download_from_samsungapps:I = 0x7f030039

.field public static final po_product_info:I = 0x7f03003a

.field public static final polaris_print_download:I = 0x7f03003b

.field public static final popover_menu_item:I = 0x7f03003c

.field public static final popup_search_list_item:I = 0x7f03003d

.field public static final print:I = 0x7f03003e

.field public static final print_list_item:I = 0x7f03003f

.field public static final print_popup:I = 0x7f030040

.field public static final print_progress_popup:I = 0x7f030041

.field public static final search_case_window:I = 0x7f030042

.field public static final sheet_autofillter_list:I = 0x7f030043

.field public static final sheet_autofilter_popup:I = 0x7f030044

.field public static final sheet_autofilter_popup_list:I = 0x7f030045

.field public static final sheet_sort_detaillist_item:I = 0x7f030046

.field public static final sheet_tab_button:I = 0x7f030047

.field public static final sheet_tab_button_edit:I = 0x7f030048

.field public static final slide_listitem:I = 0x7f030049

.field public static final slide_manage_landscape:I = 0x7f03004a

.field public static final slide_manage_portrait:I = 0x7f03004b

.field public static final slide_show:I = 0x7f03004c

.field public static final slide_show_external_display:I = 0x7f03004d

.field public static final slide_show_settings_listitem:I = 0x7f03004e

.field public static final slide_show_start_page_popup:I = 0x7f03004f

.field public static final te_actionbar_popup:I = 0x7f030050

.field public static final te_encodinglist_row:I = 0x7f030051

.field public static final te_find_options:I = 0x7f030052

.field public static final te_morepopup:I = 0x7f030053

.field public static final te_preferences:I = 0x7f030054

.field public static final te_print_progress_popup:I = 0x7f030055

.field public static final te_replace_options:I = 0x7f030056

.field public static final te_textmain:I = 0x7f030057

.field public static final te_toastpopup:I = 0x7f030058

.field public static final transform_info_layout:I = 0x7f030059

.field public static final view_sett_dlg:I = 0x7f03005a

.field public static final view_sett_dlg_item:I = 0x7f03005b

.field public static final wheel_button:I = 0x7f03005c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
