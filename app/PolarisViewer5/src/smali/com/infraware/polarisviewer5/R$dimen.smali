.class public final Lcom/infraware/polarisviewer5/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisviewer5/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final ab_h:I = 0x7f060000

.field public static final ab_h_land:I = 0x7f060001

.field public static final ab_ico_title_width:I = 0x7f060004

.field public static final ab_ico_w44:I = 0x7f060002

.field public static final ab_ico_w48:I = 0x7f060003

.field public static final ab_toolbar_h:I = 0x7f060005

.field public static final ab_toolbar_h_land:I = 0x7f060006

.field public static final ab_toolbar_ico_w:I = 0x7f060007

.field public static final gradation_width:I = 0x7f06001b

.field public static final insert_shape_devider:I = 0x7f060040

.field public static final insert_shape_devider_margin:I = 0x7f06003f

.field public static final insert_shape_h:I = 0x7f060038

.field public static final insert_shape_height:I = 0x7f060041

.field public static final insert_shape_land_height:I = 0x7f060042

.field public static final insert_shape_margin_h:I = 0x7f06003b

.field public static final insert_shape_margin_h_land:I = 0x7f06003c

.field public static final insert_shape_margin_left_right:I = 0x7f06003d

.field public static final insert_shape_margin_left_right_land:I = 0x7f06003e

.field public static final insert_shape_margin_top_bottom:I = 0x7f060039

.field public static final insert_shape_margin_v:I = 0x7f06003a

.field public static final insert_shape_w:I = 0x7f060037

.field public static final iss_button_r_margin:I = 0x7f060036

.field public static final iss_h:I = 0x7f06002f

.field public static final iss_line_gap:I = 0x7f060033

.field public static final iss_line_gap_half:I = 0x7f060034

.field public static final iss_line_lmargin:I = 0x7f060031

.field public static final iss_line_rmargin:I = 0x7f060032

.field public static final iss_line_tmargin:I = 0x7f060030

.field public static final iss_seperator_h:I = 0x7f060035

.field public static final iss_w:I = 0x7f06002e

.field public static final keypad_celltext_size:I = 0x7f060055

.field public static final keypad_changetext_size:I = 0x7f060056

.field public static final keypad_symbol_size:I = 0x7f060054

.field public static final keypad_text_size:I = 0x7f060053

.field public static final panel_button_height:I = 0x7f06001d

.field public static final panel_button_text_size:I = 0x7f06001c

.field public static final panel_button_title_h:I = 0x7f060015

.field public static final panel_chart_style_icon_margin_left:I = 0x7f060029

.field public static final panel_chart_style_icon_margin_left_land:I = 0x7f06002a

.field public static final panel_chart_style_margin_bottom:I = 0x7f060028

.field public static final panel_chart_style_margin_left_right:I = 0x7f060027

.field public static final panel_check_box_margin_left:I = 0x7f060011

.field public static final panel_check_box_margin_right:I = 0x7f060012

.field public static final panel_check_box_width:I = 0x7f06000f

.field public static final panel_check_box_width2:I = 0x7f060010

.field public static final panel_check_text_size:I = 0x7f060013

.field public static final panel_check_text_size2:I = 0x7f060014

.field public static final panel_color_button_margin:I = 0x7f06001e

.field public static final panel_height_between_next_title:I = 0x7f060020

.field public static final panel_list_button_height:I = 0x7f060018

.field public static final panel_list_button_height_half:I = 0x7f060019

.field public static final panel_list_button_height_half_half:I = 0x7f06001a

.field public static final panel_list_button_margin_top:I = 0x7f060016

.field public static final panel_list_button_width:I = 0x7f060017

.field public static final panel_list_sheet_button_height:I = 0x7f060051

.field public static final panel_list_sheet_button_margin_left:I = 0x7f06004f

.field public static final panel_list_sheet_button_width:I = 0x7f060050

.field public static final panel_list_sheet_button_width_land:I = 0x7f060052

.field public static final panel_slide_button_width:I = 0x7f060021

.field public static final panel_subtitle_devider_h:I = 0x7f06000e

.field public static final panel_subtitle_h:I = 0x7f06000b

.field public static final panel_subtitle_margin_bottom_h:I = 0x7f06000c

.field public static final panel_subtitle_margin_left_right:I = 0x7f06000d

.field public static final panel_title_h:I = 0x7f06000a

.field public static final panel_underline_animation_height:I = 0x7f060022

.field public static final panel_wheel_height:I = 0x7f06001f

.field public static final pendraw_preview_h:I = 0x7f060008

.field public static final pendraw_preview_h_land:I = 0x7f060009

.field public static final pl_button_gap:I = 0x7f060049

.field public static final pl_button_h:I = 0x7f060044

.field public static final pl_button_right_margin:I = 0x7f060045

.field public static final pl_button_w:I = 0x7f060043

.field public static final pl_custom_text_size:I = 0x7f06004c

.field public static final pl_left_margin:I = 0x7f060046

.field public static final pl_right_margin:I = 0x7f060047

.field public static final pl_size_text_size:I = 0x7f06004d

.field public static final pl_title_left_margin:I = 0x7f06004e

.field public static final pl_title_text_bottm_margin:I = 0x7f06004b

.field public static final pl_title_text_size:I = 0x7f06004a

.field public static final pl_top_margin:I = 0x7f060048

.field public static final pop_over_check_width:I = 0x7f06005c

.field public static final popup_text_size:I = 0x7f06005a

.field public static final popup_text_size_row_gap:I = 0x7f06005b

.field public static final slide_show_transition_text:I = 0x7f060057

.field public static final slide_show_transition_title:I = 0x7f060058

.field public static final style_icon_h:I = 0x7f060024

.field public static final style_icon_margin:I = 0x7f060025

.field public static final style_icon_margin_land:I = 0x7f060026

.field public static final style_icon_w:I = 0x7f060023

.field public static final toast_text_size:I = 0x7f060059

.field public static final ts_h:I = 0x7f06002b

.field public static final ts_left_w:I = 0x7f06002c

.field public static final ts_right_w:I = 0x7f06002d


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 570
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
