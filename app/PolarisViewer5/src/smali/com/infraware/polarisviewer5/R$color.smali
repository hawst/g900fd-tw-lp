.class public final Lcom/infraware/polarisviewer5/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisviewer5/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final actionbar_button_disable:I = 0x7f050036

.field public static final actionbar_button_normal:I = 0x7f050037

.field public static final actionbar_button_pressed:I = 0x7f050035

.field public static final actionbar_menu_text_color:I = 0x7f0501ce

.field public static final actionbar_menu_text_color_disabled:I = 0x7f050132

.field public static final actionbar_menu_text_color_normal:I = 0x7f050133

.field public static final actionbar_menu_text_color_selected:I = 0x7f050131

.field public static final actionbar_shadow:I = 0x7f050034

.field public static final actionbar_text:I = 0x7f050033

.field public static final actionbar_text_color_disabled:I = 0x7f05012f

.field public static final actionbar_text_color_selected:I = 0x7f05012e

.field public static final actionbar_text_color_unselected:I = 0x7f050130

.field public static final bc_create_room_text_color:I = 0x7f0501c6

.field public static final bc_empty_text:I = 0x7f0501c0

.field public static final bc_not_connected_text:I = 0x7f0501c1

.field public static final bc_room_attendee_count_text_normal:I = 0x7f0501c4

.field public static final bc_room_attendee_count_text_pressed:I = 0x7f0501c5

.field public static final bc_room_tile_text_normal:I = 0x7f0501c2

.field public static final bc_room_tile_text_pressed:I = 0x7f0501c3

.field public static final black:I = 0x7f050000

.field public static final cm_bar_text_disabled:I = 0x7f050075

.field public static final cm_bar_text_normal:I = 0x7f050073

.field public static final cm_bar_text_pressed:I = 0x7f050074

.field public static final cm_button_text:I = 0x7f0501cf

.field public static final cm_button_text_disabled:I = 0x7f050082

.field public static final cm_button_text_normal:I = 0x7f050085

.field public static final cm_button_text_pressed:I = 0x7f050084

.field public static final cm_button_text_selected:I = 0x7f050083

.field public static final cm_button_text_shadow:I = 0x7f050086

.field public static final cm_common_background:I = 0x7f050099

.field public static final cm_common_error_text:I = 0x7f05009b

.field public static final cm_common_message_text:I = 0x7f05009a

.field public static final cm_dialog_list_item_name_normal:I = 0x7f0501cd

.field public static final cm_edit_text_highlight:I = 0x7f050087

.field public static final cm_edit_text_highlight_white:I = 0x7f0501c9

.field public static final cm_edit_text_hint:I = 0x7f05008a

.field public static final cm_edit_text_normal:I = 0x7f050088

.field public static final cm_edit_text_pressed:I = 0x7f050089

.field public static final cm_empty_bg:I = 0x7f050072

.field public static final cm_empty_text:I = 0x7f0500a2

.field public static final cm_license_agree_btn_text:I = 0x7f0501d0

.field public static final cm_license_agree_btn_text_normal:I = 0x7f05009f

.field public static final cm_license_agree_btn_text_pressed:I = 0x7f0500a0

.field public static final cm_license_agree_btn_text_selected:I = 0x7f0500a1

.field public static final cm_license_info_bg:I = 0x7f05009c

.field public static final cm_license_info_line_bg:I = 0x7f05009d

.field public static final cm_license_info_text:I = 0x7f05009e

.field public static final cm_list_bg:I = 0x7f050071

.field public static final cm_list_divider:I = 0x7f050093

.field public static final cm_list_item_info:I = 0x7f0501d1

.field public static final cm_list_item_info_disabled:I = 0x7f05008f

.field public static final cm_list_item_info_normal:I = 0x7f050092

.field public static final cm_list_item_info_pressed:I = 0x7f050091

.field public static final cm_list_item_info_selected:I = 0x7f050090

.field public static final cm_list_item_name:I = 0x7f0501d2

.field public static final cm_list_item_name_disabled:I = 0x7f05008b

.field public static final cm_list_item_name_normal:I = 0x7f05008e

.field public static final cm_list_item_name_pressed:I = 0x7f05008d

.field public static final cm_list_item_name_selected:I = 0x7f05008c

.field public static final cm_menu_divider:I = 0x7f050098

.field public static final cm_menu_item_name:I = 0x7f0501d3

.field public static final cm_menu_item_name_hovered:I = 0x7f050097

.field public static final cm_menu_item_name_normal:I = 0x7f050096

.field public static final cm_menu_item_name_pressed:I = 0x7f050095

.field public static final cm_menu_item_name_selected:I = 0x7f050094

.field public static final cm_popup_text:I = 0x7f050076

.field public static final cm_save_as_btn_normal:I = 0x7f0500a8

.field public static final cm_save_as_btn_pressed:I = 0x7f0500a9

.field public static final cm_select_button_text_disabled:I = 0x7f050081

.field public static final cm_select_button_text_normal:I = 0x7f05007e

.field public static final cm_select_button_text_pressed:I = 0x7f05007f

.field public static final cm_select_button_text_selected:I = 0x7f050080

.field public static final cm_text_color_link:I = 0x7f0500a3

.field public static final cm_title_button_text:I = 0x7f0501d4

.field public static final cm_title_button_text_disabled:I = 0x7f05007d

.field public static final cm_title_button_text_normal:I = 0x7f05007c

.field public static final cm_title_info_key_text:I = 0x7f05007a

.field public static final cm_title_info_text:I = 0x7f050079

.field public static final cm_title_sub_text:I = 0x7f05007b

.field public static final cm_title_text:I = 0x7f050077

.field public static final cm_title_text_shadow:I = 0x7f050078

.field public static final cm_user_register_edit_text:I = 0x7f0500a6

.field public static final cm_user_register_info:I = 0x7f0500a7

.field public static final cm_user_register_link:I = 0x7f0500a5

.field public static final cm_user_register_text:I = 0x7f0500a4

.field public static final cm_white:I = 0x7f0501c8

.field public static final color_black:I = 0x7f050039

.field public static final color_line:I = 0x7f050032

.field public static final color_pressed:I = 0x7f05002f

.field public static final color_selected:I = 0x7f050030

.field public static final color_white:I = 0x7f050038

.field public static final colorpalette_01:I = 0x7f05003d

.field public static final colorpalette_02:I = 0x7f05003e

.field public static final colorpalette_03:I = 0x7f05003f

.field public static final colorpalette_04:I = 0x7f050040

.field public static final colorpalette_05:I = 0x7f050041

.field public static final colorpalette_06:I = 0x7f050042

.field public static final colorpalette_07:I = 0x7f050043

.field public static final colorpalette_08:I = 0x7f050044

.field public static final colorpalette_09:I = 0x7f050045

.field public static final colorpalette_10:I = 0x7f050046

.field public static final colorpalette_11:I = 0x7f050047

.field public static final colorpalette_12:I = 0x7f050048

.field public static final colorpalette_13:I = 0x7f050049

.field public static final colorpalette_14:I = 0x7f05004a

.field public static final colorpalette_15:I = 0x7f05004b

.field public static final colorpalette_16:I = 0x7f05004c

.field public static final colorpalette_17:I = 0x7f05004d

.field public static final colorpalette_18:I = 0x7f05004e

.field public static final colorpalette_19:I = 0x7f05004f

.field public static final colorpalette_20:I = 0x7f050050

.field public static final colorpalette_21:I = 0x7f050051

.field public static final colorpalette_22:I = 0x7f050052

.field public static final colorpalette_23:I = 0x7f050053

.field public static final colorpalette_404040:I = 0x7f050055

.field public static final colorpalette_545454:I = 0x7f050059

.field public static final colorpalette_808080:I = 0x7f050057

.field public static final colorpalette_a8a8a8:I = 0x7f050058

.field public static final colorpalette_bfbfbf:I = 0x7f050056

.field public static final colorpalette_white:I = 0x7f050054

.field public static final common_background:I = 0x7f05001b

.field public static final common_base_list_divider:I = 0x7f050027

.field public static final common_base_list_item_text:I = 0x7f050026

.field public static final common_bg:I = 0x7f050003

.field public static final common_bg_eeeeee:I = 0x7f050004

.field public static final common_colorpicker_rgb_text_color:I = 0x7f05001d

.field public static final common_colorpicker_text_color:I = 0x7f05001c

.field public static final common_default_btn_text:I = 0x7f0501d5

.field public static final common_default_btn_text_default:I = 0x7f050008

.field public static final common_default_btn_text_disabled:I = 0x7f050005

.field public static final common_default_btn_text_pressed:I = 0x7f050007

.field public static final common_default_btn_text_selected:I = 0x7f050006

.field public static final common_edit_panel_divider:I = 0x7f050022

.field public static final common_edit_panel_divider2:I = 0x7f050023

.field public static final common_edit_pannel_shadow:I = 0x7f050025

.field public static final common_edit_pannel_text:I = 0x7f050024

.field public static final common_inputbox_text:I = 0x7f0501d6

.field public static final common_inputbox_text_default:I = 0x7f050011

.field public static final common_inputbox_text_disabled:I = 0x7f05000d

.field public static final common_inputbox_text_focused:I = 0x7f05000e

.field public static final common_inputbox_text_pressed:I = 0x7f050010

.field public static final common_inputbox_text_selected:I = 0x7f05000f

.field public static final common_inputfield_background:I = 0x7f05001e

.field public static final common_keypad_bg:I = 0x7f0501b8

.field public static final common_keypad_bg_darker:I = 0x7f0501b9

.field public static final common_keypad_btn_text_default:I = 0x7f05000b

.field public static final common_keypad_btn_text_disabled:I = 0x7f050009

.field public static final common_keypad_btn_text_pressed:I = 0x7f05000c

.field public static final common_keypad_btn_text_selected:I = 0x7f05000a

.field public static final common_keypad_line_bg:I = 0x7f0501ba

.field public static final common_list_text_default:I = 0x7f050015

.field public static final common_list_text_focused:I = 0x7f050012

.field public static final common_list_text_pressed:I = 0x7f050014

.field public static final common_list_text_selected:I = 0x7f050013

.field public static final common_panel_color_layout_shadow_color:I = 0x7f05001f

.field public static final common_selectbtn2_text:I = 0x7f0501d7

.field public static final common_selectbtn2_text_disabled:I = 0x7f050017

.field public static final common_selectbtn2_text_focused:I = 0x7f050018

.field public static final common_selectbtn2_text_normal:I = 0x7f050016

.field public static final common_selectbtn2_text_pressed:I = 0x7f05001a

.field public static final common_selectbtn2_text_selected:I = 0x7f050019

.field public static final common_slidebutton_inside_text_color:I = 0x7f050020

.field public static final common_slidebutton_inside_text_color2:I = 0x7f050021

.field public static final dictionary_BG:I = 0x7f050031

.field public static final disable:I = 0x7f050002

.field public static final dlg_bg_color:I = 0x7f05002d

.field public static final dlg_divideline_color:I = 0x7f05002e

.field public static final dlg_settingtitle_color:I = 0x7f05002b

.field public static final dlg_subtitle_color:I = 0x7f05002c

.field public static final doc_bg_color:I = 0x7f050028

.field public static final doc_bookmark_empty_text:I = 0x7f0501ca

.field public static final doc_edge_color:I = 0x7f050029

.field public static final doc_outline_color:I = 0x7f05002a

.field public static final fm_file_item_file_index:I = 0x7f0500ac

.field public static final fm_file_item_file_index_shadow:I = 0x7f0500ad

.field public static final fm_file_item_folder_index:I = 0x7f0500ae

.field public static final fm_file_item_name_search_normal:I = 0x7f0500aa

.field public static final fm_file_item_name_search_pressed:I = 0x7f0500ab

.field public static final fm_file_item_select_normal:I = 0x7f0500b4

.field public static final fm_file_item_select_pressed:I = 0x7f0500b3

.field public static final fm_file_item_select_selected:I = 0x7f0500b2

.field public static final fm_file_item_up:I = 0x7f0501d8

.field public static final fm_file_item_up_normal:I = 0x7f0500b1

.field public static final fm_file_item_up_pressed:I = 0x7f0500b0

.field public static final fm_file_item_up_selected:I = 0x7f0500af

.field public static final fm_file_path_normal:I = 0x7f0500b6

.field public static final fm_file_path_pressed:I = 0x7f0500b5

.field public static final fm_file_search_word_list_bg:I = 0x7f0500b9

.field public static final fm_file_search_word_list_divider:I = 0x7f0500ba

.field public static final fm_file_search_word_normal:I = 0x7f0500b7

.field public static final fm_file_search_word_pressed:I = 0x7f0500b8

.field public static final fm_folder_tree_bg:I = 0x7f0500c2

.field public static final fm_folder_tree_divider:I = 0x7f0500c3

.field public static final fm_folder_tree_name_normal:I = 0x7f0500c4

.field public static final fm_folder_tree_name_pressed:I = 0x7f0500c5

.field public static final fm_info_bg:I = 0x7f0500be

.field public static final fm_info_file_name:I = 0x7f0500bf

.field public static final fm_info_item_content:I = 0x7f0500c1

.field public static final fm_info_item_title:I = 0x7f0500c0

.field public static final fm_template_file_blank:I = 0x7f0500bb

.field public static final fm_template_file_name:I = 0x7f0500bc

.field public static final fm_template_file_name_shadow:I = 0x7f0500bd

.field public static final guide_line_color:I = 0x7f0501c7

.field public static final insert_cell_textcolor_selector_default:I = 0x7f050188

.field public static final insert_cell_textcolor_selector_focused:I = 0x7f050186

.field public static final insert_cell_textcolor_selector_pressed:I = 0x7f050187

.field public static final keypad_background:I = 0x7f050181

.field public static final keypad_shadow_color:I = 0x7f05017e

.field public static final keypad_textcolor_selector_default:I = 0x7f05018a

.field public static final keypad_textcolor_selector_pressed:I = 0x7f050189

.field public static final layout_item_text:I = 0x7f0501d9

.field public static final marker_color01:I = 0x7f05005a

.field public static final marker_color02:I = 0x7f05005b

.field public static final marker_color03:I = 0x7f05005c

.field public static final marker_color04:I = 0x7f05005d

.field public static final marker_color05:I = 0x7f05005e

.field public static final marker_color06:I = 0x7f05005f

.field public static final marker_color07:I = 0x7f050060

.field public static final marker_color08:I = 0x7f050061

.field public static final marker_color09:I = 0x7f050062

.field public static final marker_color10:I = 0x7f050063

.field public static final marker_color11:I = 0x7f050064

.field public static final marker_colorpicker_text_default:I = 0x7f050066

.field public static final marker_colorpicker_text_pressed:I = 0x7f050065

.field public static final marker_pointer_color01:I = 0x7f050067

.field public static final marker_pointer_color02:I = 0x7f050068

.field public static final marker_pointer_color03:I = 0x7f050069

.field public static final marker_pointer_color04:I = 0x7f05006a

.field public static final marker_pointer_color05:I = 0x7f05006b

.field public static final memo_bg:I = 0x7f050100

.field public static final memo_text_color:I = 0x7f050102

.field public static final memo_title_color:I = 0x7f050101

.field public static final normal_text_style_text_color:I = 0x7f050173

.field public static final pagelayout_text_color_checked:I = 0x7f050167

.field public static final pagelayout_text_color_default:I = 0x7f050168

.field public static final pagelayout_text_color_focused:I = 0x7f050164

.field public static final pagelayout_text_color_pressed:I = 0x7f050166

.field public static final pagelayout_text_color_selected:I = 0x7f050165

.field public static final panel_anchor_title_text_color:I = 0x7f050179

.field public static final panel_bg_full:I = 0x7f05003c

.field public static final panel_button_text_color:I = 0x7f0501da

.field public static final panel_button_text_color_disabled:I = 0x7f05012d

.field public static final panel_button_text_color_normal:I = 0x7f05012c

.field public static final panel_button_text_color_pressed:I = 0x7f05012b

.field public static final panel_button_text_normal:I = 0x7f05017b

.field public static final panel_button_text_pressed:I = 0x7f05017c

.field public static final panel_button_text_shadow_color:I = 0x7f05017d

.field public static final panel_button_title_text_color:I = 0x7f05017a

.field public static final panel_default_text:I = 0x7f05003a

.field public static final panel_list_black_text_color_default:I = 0x7f050135

.field public static final panel_list_black_text_color_pressed:I = 0x7f050134

.field public static final panel_list_blue_text_color_default:I = 0x7f05013d

.field public static final panel_list_font5_text_color_default:I = 0x7f050137

.field public static final panel_list_font5_text_color_pressed:I = 0x7f050136

.field public static final panel_list_font6_text_color_default:I = 0x7f050139

.field public static final panel_list_font6_text_color_pressed:I = 0x7f050138

.field public static final panel_list_red_text_color_default:I = 0x7f05013c

.field public static final panel_list_red_text_color_pressed:I = 0x7f05013a

.field public static final panel_list_red_text_color_selected:I = 0x7f05013b

.field public static final panel_list_seperate:I = 0x7f05003b

.field public static final panel_list_sheet_nega_number:I = 0x7f050140

.field public static final panel_list_text_color:I = 0x7f0501db

.field public static final panel_list_text_color_default:I = 0x7f05013f

.field public static final panel_list_text_color_pressed:I = 0x7f05013e

.field public static final panel_sheet_numbers_text_default:I = 0x7f050144

.field public static final panel_sheet_numbers_text_focused:I = 0x7f050141

.field public static final panel_sheet_numbers_text_pressed:I = 0x7f050143

.field public static final panel_sheet_numbers_text_selected:I = 0x7f050142

.field public static final panel_tab_color_selector:I = 0x7f0501dc

.field public static final panel_tab_color_selector_normal:I = 0x7f050147

.field public static final panel_tab_color_selector_pressed:I = 0x7f050146

.field public static final panel_tab_color_selector_selected:I = 0x7f050145

.field public static final panel_tab_shadow_color_selector_checked:I = 0x7f050148

.field public static final panel_tab_shadow_color_selector_default:I = 0x7f050149

.field public static final pdf_annot_list_item_info:I = 0x7f0501dd

.field public static final pdf_annot_list_item_info_normal:I = 0x7f0501cb

.field public static final pdf_annot_list_item_info_pressed:I = 0x7f0501cc

.field public static final po_add_account_exist:I = 0x7f0500d5

.field public static final po_add_account_sync_background:I = 0x7f0500d6

.field public static final po_add_account_sync_divider:I = 0x7f0500d7

.field public static final po_add_account_sync_text_color:I = 0x7f0500d8

.field public static final po_chart_data_activity_shadow_color:I = 0x7f050121

.field public static final po_cloud_menu_text:I = 0x7f0500d2

.field public static final po_cloud_sign_agree_text:I = 0x7f0500d4

.field public static final po_cloud_sign_text:I = 0x7f0500d3

.field public static final po_common_dialog_text_color:I = 0x7f0500c6

.field public static final po_common_numer_input_activity_shadow_color:I = 0x7f050122

.field public static final po_edit_panel_main_checked_shadow_color:I = 0x7f050127

.field public static final po_edit_panel_main_unchecked_shadow_color:I = 0x7f050128

.field public static final po_edit_slidebutton_left:I = 0x7f0500c7

.field public static final po_edit_slidebutton_right:I = 0x7f0500c8

.field public static final po_edit_slidebutton_shadow:I = 0x7f0500c9

.field public static final po_home_bg:I = 0x7f0500ca

.field public static final po_home_cloud_divider:I = 0x7f0500cf

.field public static final po_home_file_name:I = 0x7f0500d0

.field public static final po_home_icon_text:I = 0x7f0500cd

.field public static final po_home_icon_text_shadow:I = 0x7f0500ce

.field public static final po_home_no_recent_text:I = 0x7f0500cb

.field public static final po_home_recent_text:I = 0x7f0500cc

.field public static final po_listitem_1_btn_background:I = 0x7f0500ef

.field public static final po_listitem_1_icon_text_color:I = 0x7f0500ee

.field public static final po_listitem_2_btn_text_color:I = 0x7f0500f0

.field public static final po_listitem_2_btn_text_color2:I = 0x7f0500f1

.field public static final po_listitem_chartfontsize_text_color:I = 0x7f0500ed

.field public static final po_listitem_checkbox_text_color:I = 0x7f0501de

.field public static final po_listitem_checkbox_text_color_disabled:I = 0x7f0500eb

.field public static final po_listitem_checkbox_text_color_normal:I = 0x7f0500ec

.field public static final po_listitem_checkbox_text_color_selected:I = 0x7f0500ea

.field public static final po_main_background:I = 0x7f0500f6

.field public static final po_main_background2:I = 0x7f0500f7

.field public static final po_main_logo_bg:I = 0x7f0500d1

.field public static final po_main_shadow_color:I = 0x7f0500f4

.field public static final po_main_shadow_color2:I = 0x7f0500f5

.field public static final po_main_text_color:I = 0x7f0500f8

.field public static final po_main_text_color2:I = 0x7f0500f9

.field public static final po_main_text_color3:I = 0x7f0500fa

.field public static final po_main_text_color4:I = 0x7f0500fb

.field public static final po_main_text_color5:I = 0x7f0500fc

.field public static final po_market_color_picker_button_shdow_color:I = 0x7f050120

.field public static final po_navi_context_bg:I = 0x7f0500de

.field public static final po_navi_drag_divider:I = 0x7f0500e0

.field public static final po_navi_overlay_bg:I = 0x7f0500df

.field public static final po_navi_sub_list_bg:I = 0x7f0500e3

.field public static final po_navi_sub_list_divider:I = 0x7f0500e2

.field public static final po_navi_sub_name_normal:I = 0x7f0500e7

.field public static final po_navi_sub_name_pressed:I = 0x7f0500e8

.field public static final po_navi_sub_name_selected:I = 0x7f0500e9

.field public static final po_navi_title_list_divider:I = 0x7f0500e1

.field public static final po_navi_title_name_normal:I = 0x7f0500e4

.field public static final po_navi_title_name_pressed:I = 0x7f0500e5

.field public static final po_navi_title_name_selected:I = 0x7f0500e6

.field public static final po_page_layout_activity_checked_shadow_color:I = 0x7f050129

.field public static final po_page_layout_activity_unchecked_shadow_color:I = 0x7f05012a

.field public static final po_page_layout_radio_button_pressed_shadow_color:I = 0x7f050123

.field public static final po_page_layout_radio_button_unpressed_shadow_color:I = 0x7f050124

.field public static final po_page_zoom_info_text_color:I = 0x7f0500fd

.field public static final po_page_zoom_info_text_color2:I = 0x7f0500fe

.field public static final po_page_zoom_info_text_color3:I = 0x7f0500ff

.field public static final po_panel_dictionary_search_text_color:I = 0x7f050103

.field public static final po_panel_dictionary_search_text_color2:I = 0x7f050104

.field public static final po_panel_text_button_shadow_color:I = 0x7f050126

.field public static final po_popup_menu_window_shadow_color:I = 0x7f050125

.field public static final po_product_about_info:I = 0x7f0500db

.field public static final po_product_about_version:I = 0x7f0500da

.field public static final po_resize_image_text_color:I = 0x7f050105

.field public static final po_setting_dialog_size_text:I = 0x7f0500dc

.field public static final po_sheet_conditional_format_sub_title_text:I = 0x7f05010d

.field public static final po_sheet_conditional_format_title_text:I = 0x7f05010c

.field public static final po_sheet_function_itemdetail_text_color:I = 0x7f050109

.field public static final po_sheet_function_popup_list_divider:I = 0x7f050112

.field public static final po_sheet_numbers_list_background:I = 0x7f05010f

.field public static final po_sheet_numbers_list_text_color:I = 0x7f050110

.field public static final po_sheet_selected_function_list_child_divider:I = 0x7f05010b

.field public static final po_sheet_selected_function_list_main_text:I = 0x7f050113

.field public static final po_sheet_selected_function_list_sub_text_normal:I = 0x7f050114

.field public static final po_sheet_selected_function_list_sub_text_pressed:I = 0x7f050115

.field public static final po_sheet_selected_function_list_text_color:I = 0x7f05010a

.field public static final po_sheet_sort_list_background:I = 0x7f05010e

.field public static final po_sheet_tab_button_edit_text_color:I = 0x7f050111

.field public static final po_slide_dragndroplistview_background:I = 0x7f050116

.field public static final po_slide_page_normal:I = 0x7f050117

.field public static final po_slide_page_selected:I = 0x7f050118

.field public static final po_slide_page_shadow:I = 0x7f050119

.field public static final po_slide_show_background:I = 0x7f05011a

.field public static final po_slide_show_external_display_shadow_color:I = 0x7f05011c

.field public static final po_slide_show_external_display_text_color:I = 0x7f05011b

.field public static final po_slide_show_external_display_text_color2:I = 0x7f05011d

.field public static final po_slide_show_external_display_text_color3:I = 0x7f05011e

.field public static final po_slide_show_settings_listitem_text_color:I = 0x7f05011f

.field public static final po_sort_dialog_divider:I = 0x7f0500dd

.field public static final po_terms_text:I = 0x7f0500d9

.field public static final po_word_pagelayout_shadow_color:I = 0x7f050107

.field public static final po_word_pagelayout_shadow_color2:I = 0x7f050108

.field public static final po_word_pagelayout_text_color:I = 0x7f050106

.field public static final popuptoast_btn_text_default:I = 0x7f05016b

.field public static final popuptoast_btn_text_disabled:I = 0x7f050169

.field public static final popuptoast_btn_text_selected:I = 0x7f05016a

.field public static final sheet_autofilter_text:I = 0x7f0501df

.field public static final sheet_autofilter_text_default:I = 0x7f05014d

.field public static final sheet_autofilter_text_focused:I = 0x7f05014a

.field public static final sheet_autofilter_text_pressed:I = 0x7f05014c

.field public static final sheet_autofilter_text_selected:I = 0x7f05014b

.field public static final sheet_contextmenu_text_default:I = 0x7f05016e

.field public static final sheet_contextmenu_text_focused:I = 0x7f05016c

.field public static final sheet_contextmenu_text_selected:I = 0x7f05016d

.field public static final sheet_function_okbtn_text_default:I = 0x7f050150

.field public static final sheet_function_okbtn_text_disabled:I = 0x7f05014e

.field public static final sheet_function_okbtn_text_pressed:I = 0x7f05014f

.field public static final sheet_inline_cell_edit_bg:I = 0x7f050163

.field public static final sheet_insertcell_text:I = 0x7f0501e0

.field public static final sheet_insertcell_text_default:I = 0x7f050154

.field public static final sheet_insertcell_text_focused:I = 0x7f050151

.field public static final sheet_insertcell_text_pressed:I = 0x7f050153

.field public static final sheet_insertcell_text_selected:I = 0x7f050152

.field public static final sheet_keypad_symbol_bg:I = 0x7f050162

.field public static final sheet_numbers_currency_text_default:I = 0x7f050158

.field public static final sheet_numbers_currency_text_focused:I = 0x7f050155

.field public static final sheet_numbers_currency_text_pressed:I = 0x7f050157

.field public static final sheet_numbers_currency_text_red_default:I = 0x7f050172

.field public static final sheet_numbers_currency_text_red_focused:I = 0x7f05016f

.field public static final sheet_numbers_currency_text_red_pressed:I = 0x7f050171

.field public static final sheet_numbers_currency_text_red_selected:I = 0x7f050170

.field public static final sheet_numbers_currency_text_selected:I = 0x7f050156

.field public static final sheet_sort_text:I = 0x7f0501e1

.field public static final sheet_sort_text_default:I = 0x7f05015d

.field public static final sheet_sort_text_disabled:I = 0x7f050159

.field public static final sheet_sort_text_focused:I = 0x7f05015a

.field public static final sheet_sort_text_pressed:I = 0x7f05015c

.field public static final sheet_sort_text_selected:I = 0x7f05015b

.field public static final sheet_tab_text:I = 0x7f0501e2

.field public static final sheet_tab_text_default:I = 0x7f050161

.field public static final sheet_tab_text_pressed:I = 0x7f05015e

.field public static final sheet_tab_text_selected:I = 0x7f050160

.field public static final sheet_tab_text_selected_pressed:I = 0x7f05015f

.field public static final slide_marker_color_imageview_border:I = 0x7f0501be

.field public static final slide_marker_preview_border:I = 0x7f0501bf

.field public static final slide_note_text_color:I = 0x7f0501bb

.field public static final slide_trasnsition_text_color:I = 0x7f0501bd

.field public static final slide_trasnsition_title_text_color:I = 0x7f0501bc

.field public static final slideshow_pointer_color01:I = 0x7f05006c

.field public static final slideshow_pointer_color02:I = 0x7f05006d

.field public static final slideshow_pointer_color03:I = 0x7f05006e

.field public static final slideshow_pointer_color04:I = 0x7f05006f

.field public static final slideshow_pointer_color05:I = 0x7f050070

.field public static final strong_highlight_text_color:I = 0x7f050177

.field public static final strong_reference_text_color:I = 0x7f050178

.field public static final sut_title_text_color:I = 0x7f050176

.field public static final te_choosefolder_listbackground:I = 0x7f0501ae

.field public static final te_choosefolder_listcolorhint:I = 0x7f0501ad

.field public static final te_choosefolder_listdivider:I = 0x7f0501ac

.field public static final te_choosefolder_tvlist:I = 0x7f0501ab

.field public static final te_dropdown_disabled:I = 0x7f050192

.field public static final te_dropdown_normal:I = 0x7f050193

.field public static final te_dropdown_pressed:I = 0x7f050191

.field public static final te_dropdown_selected:I = 0x7f050190

.field public static final te_editctrl_highlight:I = 0x7f0501e3

.field public static final te_encodinglist_row_tv:I = 0x7f0501aa

.field public static final te_find_options_tv:I = 0x7f0501a4

.field public static final te_folderdate_normal:I = 0x7f050196

.field public static final te_folderdate_pressed:I = 0x7f050197

.field public static final te_foldername_normal:I = 0x7f050194

.field public static final te_foldername_pressed:I = 0x7f050195

.field public static final te_highlight_color:I = 0x7f0501b7

.field public static final te_menu_divider:I = 0x7f0501b5

.field public static final te_morepopup_divider:I = 0x7f0501b6

.field public static final te_optionmenu_normal:I = 0x7f0500f2

.field public static final te_optionmenu_pressed:I = 0x7f0500f3

.field public static final te_preference_encoding:I = 0x7f0501e4

.field public static final te_preference_encoding_normal:I = 0x7f05018d

.field public static final te_preference_encoding_pressed:I = 0x7f05018c

.field public static final te_preference_encoding_selected:I = 0x7f05018b

.field public static final te_preferences_background:I = 0x7f0501b2

.field public static final te_preferences_btntitle:I = 0x7f0501a7

.field public static final te_preferences_lvscroll:I = 0x7f0501a9

.field public static final te_preferences_tvscroll:I = 0x7f0501a8

.field public static final te_preferences_tvtitle:I = 0x7f0501a6

.field public static final te_replace_all_text_disabled:I = 0x7f050198

.field public static final te_replace_all_text_normal:I = 0x7f05019a

.field public static final te_replace_all_text_pressed:I = 0x7f050199

.field public static final te_replace_options_tv:I = 0x7f0501a5

.field public static final te_save_as_background:I = 0x7f0501b3

.field public static final te_save_as_btntitle:I = 0x7f0501a1

.field public static final te_save_as_editcolorhint:I = 0x7f0501a3

.field public static final te_save_as_tvscrollview:I = 0x7f0501a2

.field public static final te_save_as_tvtitle:I = 0x7f0501a0

.field public static final te_textcolor_black:I = 0x7f0501b1

.field public static final te_textcolor_lightgray:I = 0x7f0501af

.field public static final te_textcolor_yellow:I = 0x7f0501b0

.field public static final te_textmain_background:I = 0x7f05019e

.field public static final te_textmain_buttoncolor:I = 0x7f05019d

.field public static final te_textmain_editctrltextcolor:I = 0x7f05019f

.field public static final te_textmain_text_color:I = 0x7f0501b4

.field public static final te_textmain_textcolorhint:I = 0x7f05019c

.field public static final te_textmain_titlecolor:I = 0x7f05019b

.field public static final te_toastpopup_fontcolor_disabled:I = 0x7f05018e

.field public static final te_toastpopup_fontcolor_normal:I = 0x7f05018f

.field public static final title1_text_color:I = 0x7f050174

.field public static final title2_text_color:I = 0x7f050175

.field public static final toc_item_text:I = 0x7f0501e5

.field public static final toc_item_text_normal:I = 0x7f05017f

.field public static final toc_item_text_pressed:I = 0x7f050180

.field public static final transform_info_shadow_color:I = 0x7f050183

.field public static final transform_info_text_color:I = 0x7f050182

.field public static final transparent:I = 0x7f050001

.field public static final wheel_button_text_text_color:I = 0x7f050184

.field public static final window_title_text_color:I = 0x7f050185


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
