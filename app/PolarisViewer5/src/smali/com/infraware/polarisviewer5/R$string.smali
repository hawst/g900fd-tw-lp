.class public final Lcom/infraware/polarisviewer5/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisviewer5/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final SubUpdateURL:I = 0x7f070321

.field public static final UpdateURL:I = 0x7f070320

.field public static final app_name:I = 0x7f070000

.field public static final app_title:I = 0x7f070001

.field public static final bc_tooltip_close:I = 0x7f07004c

.field public static final bullet2_btn:I = 0x7f07004d

.field public static final cm_action_bar_draw:I = 0x7f07004e

.field public static final cm_action_bar_find:I = 0x7f07004f

.field public static final cm_action_bar_find_proper:I = 0x7f070050

.field public static final cm_action_bar_free_write:I = 0x7f070051

.field public static final cm_action_bar_more:I = 0x7f070052

.field public static final cm_action_bar_next_find:I = 0x7f070053

.field public static final cm_action_bar_panning:I = 0x7f070054

.field public static final cm_action_bar_pendraw_erase:I = 0x7f070055

.field public static final cm_action_bar_pendraw_eraseall:I = 0x7f070056

.field public static final cm_action_bar_pendraw_lasso:I = 0x7f070057

.field public static final cm_action_bar_pendraw_pen:I = 0x7f070058

.field public static final cm_action_bar_prev_find:I = 0x7f070059

.field public static final cm_alarm_connet_network_mobile_body:I = 0x7f07005b

.field public static final cm_alarm_connet_network_mobile_title:I = 0x7f07005a

.field public static final cm_alarm_connet_network_wlan_body:I = 0x7f07005d

.field public static final cm_alarm_connet_network_wlan_title:I = 0x7f07005c

.field public static final cm_alarm_no_connect_network:I = 0x7f07005e

.field public static final cm_btn_cancel:I = 0x7f07005f

.field public static final cm_btn_delete:I = 0x7f070060

.field public static final cm_btn_done:I = 0x7f070061

.field public static final cm_btn_no:I = 0x7f070062

.field public static final cm_btn_ok:I = 0x7f070063

.field public static final cm_btn_save:I = 0x7f070065

.field public static final cm_btn_select:I = 0x7f070066

.field public static final cm_btn_tts_next:I = 0x7f070067

.field public static final cm_btn_tts_pause:I = 0x7f070068

.field public static final cm_btn_tts_previous:I = 0x7f070069

.field public static final cm_btn_tts_start:I = 0x7f07006a

.field public static final cm_btn_yes:I = 0x7f07006b

.field public static final cm_color_black:I = 0x7f07006c

.field public static final cm_color_blue:I = 0x7f07006d

.field public static final cm_color_bright_blue:I = 0x7f07006e

.field public static final cm_color_bright_green:I = 0x7f07006f

.field public static final cm_color_dark_red:I = 0x7f070070

.field public static final cm_color_green:I = 0x7f070071

.field public static final cm_color_orange:I = 0x7f070072

.field public static final cm_color_others:I = 0x7f070073

.field public static final cm_color_red:I = 0x7f070074

.field public static final cm_color_violet:I = 0x7f070075

.field public static final cm_color_white:I = 0x7f070076

.field public static final cm_color_yellow:I = 0x7f070077

.field public static final cm_default_file_name_doc:I = 0x7f070007

.field public static final cm_default_file_name_hwp:I = 0x7f070008

.field public static final cm_default_file_name_ppt:I = 0x7f07000a

.field public static final cm_default_file_name_txt:I = 0x7f07000b

.field public static final cm_default_file_name_xls:I = 0x7f070009

.field public static final cm_default_folder_name:I = 0x7f070006

.field public static final cm_dictionary_chinese:I = 0x7f070078

.field public static final cm_dictionary_en_en_summary:I = 0x7f070079

.field public static final cm_dictionary_en_ja_summary:I = 0x7f07007a

.field public static final cm_dictionary_en_ko_summary:I = 0x7f07007b

.field public static final cm_dictionary_en_zh_summary:I = 0x7f07007c

.field public static final cm_dictionary_english:I = 0x7f07007d

.field public static final cm_dictionary_ja_en_summary:I = 0x7f07007e

.field public static final cm_dictionary_ja_ja_summary:I = 0x7f07007f

.field public static final cm_dictionary_ja_ko_summary:I = 0x7f070080

.field public static final cm_dictionary_ja_zh_summary:I = 0x7f070081

.field public static final cm_dictionary_japanese:I = 0x7f070082

.field public static final cm_dictionary_ko_en_summary:I = 0x7f070083

.field public static final cm_dictionary_ko_ja_summary:I = 0x7f070084

.field public static final cm_dictionary_ko_ko_summary:I = 0x7f070085

.field public static final cm_dictionary_ko_zh_summary:I = 0x7f070086

.field public static final cm_dictionary_korea:I = 0x7f070087

.field public static final cm_dictionary_no_dict_summary:I = 0x7f070088

.field public static final cm_dictionary_setting_title:I = 0x7f070089

.field public static final cm_dictionary_title:I = 0x7f07008a

.field public static final cm_dictionary_zh_en_summary:I = 0x7f07008b

.field public static final cm_dictionary_zh_ja_summary:I = 0x7f07008c

.field public static final cm_dictionary_zh_ko_summary:I = 0x7f07008d

.field public static final cm_dictionary_zh_zh_summary:I = 0x7f07008e

.field public static final cm_err_account_password_over:I = 0x7f07008f

.field public static final cm_err_file_write:I = 0x7f070090

.field public static final cm_err_invalid_account:I = 0x7f070091

.field public static final cm_err_network_connect:I = 0x7f070092

.field public static final cm_err_network_connect_eula:I = 0x7f070093

.field public static final cm_err_network_connect_notice:I = 0x7f070094

.field public static final cm_err_network_fail:I = 0x7f070095

.field public static final cm_err_network_retry:I = 0x7f070096

.field public static final cm_err_range:I = 0x7f070097

.field public static final cm_err_regist_email_over:I = 0x7f070098

.field public static final cm_err_regist_email_value:I = 0x7f070099

.field public static final cm_err_regist_server:I = 0x7f07009a

.field public static final cm_hint_email:I = 0x7f07009b

.field public static final cm_hint_filename:I = 0x7f07009c

.field public static final cm_hint_password:I = 0x7f07009d

.field public static final cm_infraware_homepage:I = 0x7f070002

.field public static final cm_msg_about_info:I = 0x7f07009e

.field public static final cm_msg_delete_file:I = 0x7f07009f

.field public static final cm_msg_delete_navi_account:I = 0x7f0700a0

.field public static final cm_msg_latest_ver:I = 0x7f0700a1

.field public static final cm_msg_multiple_close_only:I = 0x7f0700a2

.field public static final cm_msg_not_show_again:I = 0x7f0700a3

.field public static final cm_msg_regist_info1:I = 0x7f0700a4

.field public static final cm_msg_sdcard_unmonted:I = 0x7f0700a5

.field public static final cm_msg_update_progress:I = 0x7f0700a6

.field public static final cm_not_enough_memory:I = 0x7f0700a7

.field public static final cm_notification_title:I = 0x7f0700a8

.field public static final cm_progress_all_replacing:I = 0x7f0700aa

.field public static final cm_progress_replacing:I = 0x7f0700ab

.field public static final cm_progress_searching:I = 0x7f0700a9

.field public static final cm_setting_about:I = 0x7f0700ac

.field public static final cm_setting_auto_fit:I = 0x7f0700ad

.field public static final cm_setting_auto_fit_summary:I = 0x7f0700ae

.field public static final cm_setting_category_application:I = 0x7f0700af

.field public static final cm_setting_category_general:I = 0x7f0700b0

.field public static final cm_setting_clear_recent:I = 0x7f0700b1

.field public static final cm_setting_clear_recent_summary:I = 0x7f0700b2

.field public static final cm_setting_create_backup:I = 0x7f0700b3

.field public static final cm_setting_create_backup_summary:I = 0x7f0700b4

.field public static final cm_setting_default_format:I = 0x7f0700b5

.field public static final cm_setting_default_format_2003:I = 0x7f070003

.field public static final cm_setting_default_format_2007:I = 0x7f070004

.field public static final cm_setting_dictionary:I = 0x7f0700b6

.field public static final cm_setting_dictionary_summary:I = 0x7f0700b7

.field public static final cm_setting_eula:I = 0x7f070005

.field public static final cm_setting_eula_summary:I = 0x7f0700b8

.field public static final cm_setting_open_source:I = 0x7f0700b9

.field public static final cm_setting_open_source_summary:I = 0x7f0700ba

.field public static final cm_setting_show_ext:I = 0x7f0700bb

.field public static final cm_setting_show_ext_summary:I = 0x7f0700bc

.field public static final cm_setting_start_folder:I = 0x7f0700bd

.field public static final cm_setting_start_folder_summary:I = 0x7f0700be

.field public static final cm_setting_title:I = 0x7f0700bf

.field public static final cm_setting_title_about:I = 0x7f0700c0

.field public static final cm_setting_update:I = 0x7f0700c1

.field public static final cm_setting_update_available:I = 0x7f0700c2

.field public static final cm_setting_update_summary:I = 0x7f0700c3

.field public static final cm_tooltip_new_folder:I = 0x7f0700c4

.field public static final cm_tooltip_save:I = 0x7f0700c5

.field public static final copy_has_done:I = 0x7f0700c6

.field public static final cut_has_done:I = 0x7f0700c7

.field public static final dialog_video_message:I = 0x7f0700c8

.field public static final dialog_video_title:I = 0x7f0700c9

.field public static final dm_PDFAnnotList_title:I = 0x7f0700ca

.field public static final dm_PDFBookmark_title:I = 0x7f0700cb

.field public static final dm_PDFContinuousPageLayout:I = 0x7f0700cc

.field public static final dm_PDFDoublePageLayout:I = 0x7f0700cd

.field public static final dm_PDFSinglePageLayout:I = 0x7f0700ce

.field public static final dm_about:I = 0x7f0700cf

.field public static final dm_about_product:I = 0x7f070064

.field public static final dm_add_contact:I = 0x7f0700d0

.field public static final dm_add_google_account_title:I = 0x7f0700d1

.field public static final dm_align:I = 0x7f0700d2

.field public static final dm_all:I = 0x7f0700d3

.field public static final dm_annimation:I = 0x7f0700d4

.field public static final dm_annot_list_info_page:I = 0x7f0700d5

.field public static final dm_annot_list_no_annot:I = 0x7f0700d6

.field public static final dm_annotation_on:I = 0x7f0700d7

.field public static final dm_arrange:I = 0x7f0700d8

.field public static final dm_ask_register_account:I = 0x7f0700d9

.field public static final dm_autofit_columns_width:I = 0x7f0700da

.field public static final dm_autofit_rows_height:I = 0x7f0700db

.field public static final dm_backward:I = 0x7f0700dc

.field public static final dm_book_mark_exists:I = 0x7f0700dd

.field public static final dm_bookclip:I = 0x7f0700de

.field public static final dm_bookmark:I = 0x7f0700df

.field public static final dm_border_style_medium:I = 0x7f0700e0

.field public static final dm_border_style_noraml:I = 0x7f0700e1

.field public static final dm_border_style_thin:I = 0x7f0700e2

.field public static final dm_broadcast:I = 0x7f0700e3

.field public static final dm_brush:I = 0x7f0700e4

.field public static final dm_bullets_numbering:I = 0x7f0700e5

.field public static final dm_call:I = 0x7f0700e6

.field public static final dm_camera:I = 0x7f0700e7

.field public static final dm_cannot_modified_encrypt_sheet:I = 0x7f0700e8

.field public static final dm_cannot_modified_over_memory_sheet:I = 0x7f0700e9

.field public static final dm_cannot_modified_protect_sheet:I = 0x7f0700ea

.field public static final dm_cell:I = 0x7f0700eb

.field public static final dm_cells:I = 0x7f0700ec

.field public static final dm_change_editmode:I = 0x7f0700ed

.field public static final dm_change_lower:I = 0x7f0700ee

.field public static final dm_change_mode_msg:I = 0x7f0700ef

.field public static final dm_change_page_layout:I = 0x7f0700f0

.field public static final dm_change_upper:I = 0x7f0700f1

.field public static final dm_change_viewmode:I = 0x7f0700f2

.field public static final dm_chart:I = 0x7f0700f3

.field public static final dm_chart_data_range:I = 0x7f0700f4

.field public static final dm_chart_font_size1:I = 0x7f0700f5

.field public static final dm_chart_font_size2:I = 0x7f0700f6

.field public static final dm_chart_font_size3:I = 0x7f0700f7

.field public static final dm_chart_font_size4:I = 0x7f0700f8

.field public static final dm_chart_font_size5:I = 0x7f0700f9

.field public static final dm_chart_layout:I = 0x7f0700fa

.field public static final dm_chart_no_legend:I = 0x7f0700fb

.field public static final dm_chart_on_bottom:I = 0x7f0700fc

.field public static final dm_chart_on_left:I = 0x7f0700fd

.field public static final dm_chart_on_right:I = 0x7f0700fe

.field public static final dm_chart_on_top:I = 0x7f0700ff

.field public static final dm_chart_style:I = 0x7f070100

.field public static final dm_clear:I = 0x7f070101

.field public static final dm_clear_all:I = 0x7f070102

.field public static final dm_clear_contents:I = 0x7f070103

.field public static final dm_color_limit:I = 0x7f070104

.field public static final dm_color_picker:I = 0x7f070105

.field public static final dm_column_width:I = 0x7f070106

.field public static final dm_column_width_error:I = 0x7f070107

.field public static final dm_columns:I = 0x7f070108

.field public static final dm_conditional_format:I = 0x7f070109

.field public static final dm_conditional_format_greater_than_description:I = 0x7f07010a

.field public static final dm_copy_format:I = 0x7f07010b

.field public static final dm_data_sheet:I = 0x7f07010c

.field public static final dm_decimal_places:I = 0x7f07010d

.field public static final dm_decimal_places_error:I = 0x7f07010e

.field public static final dm_delete:I = 0x7f07010f

.field public static final dm_delete_cells:I = 0x7f070110

.field public static final dm_delete_columns:I = 0x7f070111

.field public static final dm_delete_entire_col:I = 0x7f070112

.field public static final dm_delete_entire_row:I = 0x7f070113

.field public static final dm_delete_rows:I = 0x7f070114

.field public static final dm_dictionary_search:I = 0x7f070115

.field public static final dm_direction_horizontal:I = 0x7f070116

.field public static final dm_direction_ltr:I = 0x7f070117

.field public static final dm_direction_rotate270:I = 0x7f070118

.field public static final dm_direction_rotate90:I = 0x7f070119

.field public static final dm_direction_rotate_asian270:I = 0x7f07011a

.field public static final dm_direction_rtl:I = 0x7f07011b

.field public static final dm_direction_vertical:I = 0x7f07011c

.field public static final dm_discard:I = 0x7f07011d

.field public static final dm_distribute_columns:I = 0x7f07011e

.field public static final dm_distribute_rows:I = 0x7f07011f

.field public static final dm_document_edit_protect:I = 0x7f070120

.field public static final dm_done:I = 0x7f070121

.field public static final dm_draw_all_earse:I = 0x7f070122

.field public static final dm_draw_earse:I = 0x7f070123

.field public static final dm_draw_lasso:I = 0x7f070124

.field public static final dm_draw_pen:I = 0x7f070125

.field public static final dm_edit:I = 0x7f070126

.field public static final dm_edit_google_account_title:I = 0x7f070127

.field public static final dm_edit_mode:I = 0x7f070128

.field public static final dm_edittext:I = 0x7f070129

.field public static final dm_edittext_maxlength:I = 0x7f07012a

.field public static final dm_empty_autofilter_field:I = 0x7f07012b

.field public static final dm_endnote:I = 0x7f07012c

.field public static final dm_err_page_value:I = 0x7f07012d

.field public static final dm_etc:I = 0x7f07012e

.field public static final dm_export_to_pdf:I = 0x7f07012f

.field public static final dm_extract_err_memory:I = 0x7f070130

.field public static final dm_file:I = 0x7f070131

.field public static final dm_file_open_err:I = 0x7f070132

.field public static final dm_file_open_err_unsupport_file:I = 0x7f070133

.field public static final dm_filter:I = 0x7f070134

.field public static final dm_find_mode:I = 0x7f070135

.field public static final dm_find_replace:I = 0x7f070136

.field public static final dm_fit_page:I = 0x7f070137

.field public static final dm_fit_slide:I = 0x7f070138

.field public static final dm_fit_width_height:I = 0x7f070139

.field public static final dm_fittowidth:I = 0x7f07013a

.field public static final dm_folder_path:I = 0x7f07013b

.field public static final dm_font:I = 0x7f07013c

.field public static final dm_font_10pt:I = 0x7f070013

.field public static final dm_font_11pt:I = 0x7f070014

.field public static final dm_font_12pt:I = 0x7f070015

.field public static final dm_font_14pt:I = 0x7f070016

.field public static final dm_font_16pt:I = 0x7f070017

.field public static final dm_font_20pt:I = 0x7f070018

.field public static final dm_font_24pt:I = 0x7f070019

.field public static final dm_font_32pt:I = 0x7f07001a

.field public static final dm_font_44pt:I = 0x7f07001b

.field public static final dm_font_8pt:I = 0x7f070011

.field public static final dm_font_9pt:I = 0x7f070012

.field public static final dm_font_normal:I = 0x7f07013d

.field public static final dm_font_size:I = 0x7f07013e

.field public static final dm_font_size_error_sheet:I = 0x7f07013f

.field public static final dm_footnote:I = 0x7f070140

.field public static final dm_format:I = 0x7f070141

.field public static final dm_format_accounting:I = 0x7f070142

.field public static final dm_format_currency:I = 0x7f070143

.field public static final dm_format_date:I = 0x7f070144

.field public static final dm_format_fraction_by_10:I = 0x7f070145

.field public static final dm_format_fraction_by_100:I = 0x7f070146

.field public static final dm_format_fraction_by_16:I = 0x7f070147

.field public static final dm_format_fraction_by_2:I = 0x7f070148

.field public static final dm_format_fraction_by_4:I = 0x7f070149

.field public static final dm_format_fraction_by_8:I = 0x7f07014a

.field public static final dm_format_fraction_upto_1_digit:I = 0x7f07014b

.field public static final dm_format_fraction_upto_2_digit:I = 0x7f07014c

.field public static final dm_format_fraction_upto_3_digit:I = 0x7f07014d

.field public static final dm_format_general:I = 0x7f07014e

.field public static final dm_format_number:I = 0x7f07014f

.field public static final dm_format_number2:I = 0x7f070150

.field public static final dm_format_percentage:I = 0x7f070151

.field public static final dm_format_scientific:I = 0x7f070152

.field public static final dm_format_text:I = 0x7f070153

.field public static final dm_format_time:I = 0x7f070154

.field public static final dm_formats:I = 0x7f070155

.field public static final dm_forward:I = 0x7f070156

.field public static final dm_fraction:I = 0x7f070157

.field public static final dm_free_draw:I = 0x7f070158

.field public static final dm_freedraw:I = 0x7f070159

.field public static final dm_freeze:I = 0x7f07015a

.field public static final dm_full_width_view_off:I = 0x7f07015b

.field public static final dm_full_width_view_on:I = 0x7f07015c

.field public static final dm_function_database:I = 0x7f07015d

.field public static final dm_function_datetime:I = 0x7f07015e

.field public static final dm_function_financial:I = 0x7f07015f

.field public static final dm_function_info:I = 0x7f070160

.field public static final dm_function_logical:I = 0x7f070161

.field public static final dm_function_lookup:I = 0x7f070162

.field public static final dm_function_math:I = 0x7f070163

.field public static final dm_function_statistical:I = 0x7f070164

.field public static final dm_function_text:I = 0x7f070165

.field public static final dm_google:I = 0x7f070166

.field public static final dm_google_email_address:I = 0x7f070167

.field public static final dm_google_search:I = 0x7f070168

.field public static final dm_group:I = 0x7f070169

.field public static final dm_hide_columns:I = 0x7f07016a

.field public static final dm_hide_rows:I = 0x7f07016b

.field public static final dm_highlight:I = 0x7f07016c

.field public static final dm_highlighter:I = 0x7f07016d

.field public static final dm_hyperlink:I = 0x7f07016e

.field public static final dm_incorrect_hyperlink:I = 0x7f07016f

.field public static final dm_init_numbering:I = 0x7f070170

.field public static final dm_insert:I = 0x7f070171

.field public static final dm_insert_above:I = 0x7f070172

.field public static final dm_insert_below:I = 0x7f070173

.field public static final dm_insert_camera:I = 0x7f070174

.field public static final dm_insert_cell:I = 0x7f070175

.field public static final dm_insert_col_left:I = 0x7f070176

.field public static final dm_insert_col_right:I = 0x7f070177

.field public static final dm_insert_column:I = 0x7f070178

.field public static final dm_insert_entire_col:I = 0x7f070179

.field public static final dm_insert_entire_row:I = 0x7f07017a

.field public static final dm_insert_gallery:I = 0x7f07017b

.field public static final dm_insert_image:I = 0x7f07017c

.field public static final dm_insert_left:I = 0x7f07017d

.field public static final dm_insert_right:I = 0x7f07017e

.field public static final dm_insert_row:I = 0x7f07017f

.field public static final dm_insert_row_above:I = 0x7f070180

.field public static final dm_insert_row_below:I = 0x7f070181

.field public static final dm_insert_textbox:I = 0x7f070182

.field public static final dm_insert_textbox_hori:I = 0x7f070183

.field public static final dm_insert_textbox_verti:I = 0x7f070184

.field public static final dm_inserthyperdlg_www:I = 0x7f07001c

.field public static final dm_invalid_input_error:I = 0x7f070185

.field public static final dm_lasso:I = 0x7f070186

.field public static final dm_line:I = 0x7f070187

.field public static final dm_lose_column_information:I = 0x7f070188

.field public static final dm_manage_sheet:I = 0x7f070189

.field public static final dm_manage_slide:I = 0x7f07018a

.field public static final dm_mask:I = 0x7f07018b

.field public static final dm_memo:I = 0x7f07018c

.field public static final dm_memo_display:I = 0x7f07018d

.field public static final dm_memo_insert:I = 0x7f07018e

.field public static final dm_memo_on:I = 0x7f07018f

.field public static final dm_merge_cells:I = 0x7f070190

.field public static final dm_merge_cells_msg:I = 0x7f070191

.field public static final dm_mm:I = 0x7f070010

.field public static final dm_msg_confirm_delete:I = 0x7f070192

.field public static final dm_msg_confirm_delete_bookmark:I = 0x7f070193

.field public static final dm_next:I = 0x7f070194

.field public static final dm_no_TOC_item:I = 0x7f070195

.field public static final dm_no_bookmark:I = 0x7f070196

.field public static final dm_no_object:I = 0x7f070197

.field public static final dm_none:I = 0x7f070198

.field public static final dm_not_pdf_export_1000pages:I = 0x7f070199

.field public static final dm_number:I = 0x7f07019a

.field public static final dm_number_1000_seperator:I = 0x7f07019b

.field public static final dm_number_negative_numbers:I = 0x7f07019c

.field public static final dm_number_symbol:I = 0x7f07019d

.field public static final dm_open_document_truncated:I = 0x7f07019e

.field public static final dm_open_password_error:I = 0x7f07019f

.field public static final dm_page_animation:I = 0x7f0701a0

.field public static final dm_page_header_footer:I = 0x7f0701a1

.field public static final dm_page_layout:I = 0x7f0701a2

.field public static final dm_page_page:I = 0x7f0701a3

.field public static final dm_paragraph:I = 0x7f0701a4

.field public static final dm_paste_format:I = 0x7f0701a5

.field public static final dm_paste_formula:I = 0x7f0701a6

.field public static final dm_paste_value:I = 0x7f0701a7

.field public static final dm_pdf_export:I = 0x7f0701a8

.field public static final dm_pdf_note_title:I = 0x7f0701a9

.field public static final dm_percent:I = 0x7f07000d

.field public static final dm_position_normal:I = 0x7f0701aa

.field public static final dm_position_subscript:I = 0x7f0701ab

.field public static final dm_position_superscript:I = 0x7f0701ac

.field public static final dm_print:I = 0x7f0701ad

.field public static final dm_print_account_cant_delete:I = 0x7f0701ae

.field public static final dm_print_account_delete:I = 0x7f0701af

.field public static final dm_print_account_exist:I = 0x7f0701b0

.field public static final dm_print_all:I = 0x7f0701b1

.field public static final dm_print_all_pages:I = 0x7f0701b2

.field public static final dm_print_all_sheets:I = 0x7f0701b3

.field public static final dm_print_all_slides:I = 0x7f0701b4

.field public static final dm_print_count:I = 0x7f0701b5

.field public static final dm_print_current:I = 0x7f0701b6

.field public static final dm_print_current_sheet:I = 0x7f0701b7

.field public static final dm_print_current_slide:I = 0x7f0701b8

.field public static final dm_print_custom_range:I = 0x7f0701b9

.field public static final dm_print_custom_range_example:I = 0x7f0701ba

.field public static final dm_print_fail:I = 0x7f0701bb

.field public static final dm_print_no_content:I = 0x7f0701bc

.field public static final dm_print_pages:I = 0x7f0701bd

.field public static final dm_print_range:I = 0x7f0701be

.field public static final dm_progress_loading:I = 0x7f0701bf

.field public static final dm_progress_pdf_exporting:I = 0x7f0701c0

.field public static final dm_progress_print_notice:I = 0x7f0701c1

.field public static final dm_progress_print_prev:I = 0x7f0701c2

.field public static final dm_progress_printing:I = 0x7f0701c3

.field public static final dm_progress_saving:I = 0x7f0701c4

.field public static final dm_progress_searching:I = 0x7f0701c5

.field public static final dm_protect_sheet:I = 0x7f0701c6

.field public static final dm_pt:I = 0x7f07000c

.field public static final dm_recalculate:I = 0x7f0701c7

.field public static final dm_recalculation_complete:I = 0x7f0701c8

.field public static final dm_recent:I = 0x7f0701c9

.field public static final dm_recording_video:I = 0x7f0701ca

.field public static final dm_redo:I = 0x7f0701cb

.field public static final dm_reflow_text:I = 0x7f0701cc

.field public static final dm_register_account_title:I = 0x7f0701cd

.field public static final dm_replace:I = 0x7f0701ce

.field public static final dm_replace_image:I = 0x7f0701cf

.field public static final dm_replace_mode:I = 0x7f0701d0

.field public static final dm_replace_shape:I = 0x7f0701d1

.field public static final dm_replaceall:I = 0x7f0701d2

.field public static final dm_replaceall_exceed_limit:I = 0x7f0701d3

.field public static final dm_replaceall_limit:I = 0x7f0701d4

.field public static final dm_replacedlg_find:I = 0x7f0701d5

.field public static final dm_replacedlg_find_text:I = 0x7f0701d6

.field public static final dm_replacedlg_replacewith:I = 0x7f0701d7

.field public static final dm_resize:I = 0x7f0701d8

.field public static final dm_result_allreplace:I = 0x7f0701d9

.field public static final dm_result_bookmarkaddd:I = 0x7f0701da

.field public static final dm_result_search_end:I = 0x7f0701db

.field public static final dm_result_search_fail:I = 0x7f0701dc

.field public static final dm_rotate:I = 0x7f0701dd

.field public static final dm_row_height:I = 0x7f0701de

.field public static final dm_row_height_error:I = 0x7f0701df

.field public static final dm_rows:I = 0x7f0701e0

.field public static final dm_ruler_on:I = 0x7f0701e1

.field public static final dm_ruller:I = 0x7f0701e2

.field public static final dm_run_hyperlink:I = 0x7f0701e3

.field public static final dm_save_changes_conform:I = 0x7f0701e4

.field public static final dm_save_folder:I = 0x7f07000e

.field public static final dm_save_officex_to_office:I = 0x7f0701e5

.field public static final dm_save_pendraw:I = 0x7f0701e6

.field public static final dm_save_pendraw_comment:I = 0x7f0701e7

.field public static final dm_save_sendfile:I = 0x7f0701e8

.field public static final dm_searchdlg_matchcase:I = 0x7f0701e9

.field public static final dm_searchdlg_wholecell:I = 0x7f0701ea

.field public static final dm_searchdlg_wholeword:I = 0x7f0701eb

.field public static final dm_select_all:I = 0x7f0701ec

.field public static final dm_select_all_cells:I = 0x7f0701ed

.field public static final dm_select_cells:I = 0x7f0701ee

.field public static final dm_select_columns:I = 0x7f0701ef

.field public static final dm_select_rows:I = 0x7f0701f0

.field public static final dm_send_email:I = 0x7f0701f1

.field public static final dm_send_email_original:I = 0x7f0701f2

.field public static final dm_send_email_pdf:I = 0x7f07000f

.field public static final dm_send_file:I = 0x7f0701f3

.field public static final dm_setting:I = 0x7f0701f4

.field public static final dm_shape:I = 0x7f0701f5

.field public static final dm_shape_action:I = 0x7f0701f6

.field public static final dm_shape_arrow:I = 0x7f0701f7

.field public static final dm_shape_banner:I = 0x7f0701f8

.field public static final dm_shape_basic:I = 0x7f0701f9

.field public static final dm_shape_explanation:I = 0x7f0701fa

.field public static final dm_shape_flow:I = 0x7f0701fb

.field public static final dm_shape_formulra:I = 0x7f0701fc

.field public static final dm_shape_line:I = 0x7f0701fd

.field public static final dm_shape_rectangle:I = 0x7f0701fe

.field public static final dm_shape_thick:I = 0x7f0701ff

.field public static final dm_share:I = 0x7f070200

.field public static final dm_sheet_chinese:I = 0x7f070201

.field public static final dm_sheet_circular_reference_warning:I = 0x7f070202

.field public static final dm_sheet_delete:I = 0x7f070203

.field public static final dm_sheet_duplicate:I = 0x7f070204

.field public static final dm_sheet_insert:I = 0x7f070205

.field public static final dm_sheet_japanese:I = 0x7f070206

.field public static final dm_sheet_keypad_currency:I = 0x7f07001d

.field public static final dm_sheet_lock_protect:I = 0x7f070207

.field public static final dm_sheet_protected:I = 0x7f070208

.field public static final dm_sheet_unprotected:I = 0x7f070209

.field public static final dm_sheet_write_protect:I = 0x7f07020a

.field public static final dm_shift_cells_down:I = 0x7f07020b

.field public static final dm_shift_cells_left:I = 0x7f07020c

.field public static final dm_shift_cells_right:I = 0x7f07020d

.field public static final dm_shift_cells_up:I = 0x7f07020e

.field public static final dm_show_smart_guides:I = 0x7f07020f

.field public static final dm_single_slide_view_on:I = 0x7f070210

.field public static final dm_slide:I = 0x7f070211

.field public static final dm_slide_layout:I = 0x7f070212

.field public static final dm_slide_mw_close_pageanimation:I = 0x7f070213

.field public static final dm_slide_mw_close_slideshow:I = 0x7f070214

.field public static final dm_slide_mw_not_pageanimation:I = 0x7f070215

.field public static final dm_slide_mw_not_slideshow:I = 0x7f070216

.field public static final dm_slide_mw_off_retry:I = 0x7f070217

.field public static final dm_slide_show:I = 0x7f070218

.field public static final dm_slide_show_draw_conform:I = 0x7f070219

.field public static final dm_slide_show_marker_annotation:I = 0x7f07021a

.field public static final dm_slide_show_start_current_page:I = 0x7f07021b

.field public static final dm_slide_show_start_first_page:I = 0x7f07021c

.field public static final dm_slideoff:I = 0x7f07021d

.field public static final dm_slideon:I = 0x7f07021e

.field public static final dm_slides:I = 0x7f07021f

.field public static final dm_slides_list:I = 0x7f070220

.field public static final dm_slideshow_controller_note:I = 0x7f070221

.field public static final dm_slideshow_finished:I = 0x7f070222

.field public static final dm_slideshow_primary_display:I = 0x7f070223

.field public static final dm_sms:I = 0x7f070224

.field public static final dm_sort:I = 0x7f070225

.field public static final dm_sort_asc:I = 0x7f070226

.field public static final dm_sort_desc:I = 0x7f070227

.field public static final dm_sort_direction:I = 0x7f070228

.field public static final dm_sort_direction_lr:I = 0x7f070229

.field public static final dm_sort_direction_rl:I = 0x7f07022a

.field public static final dm_sort_direction_tb:I = 0x7f07022b

.field public static final dm_spell_check:I = 0x7f07022c

.field public static final dm_spell_sugestions:I = 0x7f07022d

.field public static final dm_split:I = 0x7f07022e

.field public static final dm_split_cells:I = 0x7f07022f

.field public static final dm_storage_not_enough:I = 0x7f070230

.field public static final dm_symbol:I = 0x7f070231

.field public static final dm_symbol_currency:I = 0x7f070232

.field public static final dm_symbol_etc:I = 0x7f070233

.field public static final dm_symbol_number:I = 0x7f070234

.field public static final dm_symbol_shape:I = 0x7f070235

.field public static final dm_symbol_used:I = 0x7f070236

.field public static final dm_table:I = 0x7f070237

.field public static final dm_table_cancle:I = 0x7f070238

.field public static final dm_table_draw:I = 0x7f070239

.field public static final dm_table_erease:I = 0x7f07023a

.field public static final dm_table_insert:I = 0x7f07023b

.field public static final dm_toptab_Format:I = 0x7f07023c

.field public static final dm_toptab_Style:I = 0x7f07023d

.field public static final dm_toptab_style:I = 0x7f07023e

.field public static final dm_tts:I = 0x7f07023f

.field public static final dm_tts_pagenum_guide_after_scroll:I = 0x7f070240

.field public static final dm_tts_pause_pagenum_guide:I = 0x7f070241

.field public static final dm_tts_start_pagenum_guide:I = 0x7f070242

.field public static final dm_tts_start_whole_guide:I = 0x7f070243

.field public static final dm_undo:I = 0x7f070244

.field public static final dm_unfreeze:I = 0x7f070245

.field public static final dm_ungroup:I = 0x7f070246

.field public static final dm_unhide_columns:I = 0x7f070247

.field public static final dm_unhide_rows:I = 0x7f070248

.field public static final dm_unprotect_sheet:I = 0x7f070249

.field public static final dm_value_field_err:I = 0x7f07024a

.field public static final dm_value_field_err_float:I = 0x7f07024b

.field public static final dm_video:I = 0x7f07024c

.field public static final dm_view_mode:I = 0x7f07024d

.field public static final dm_view_setting:I = 0x7f07024e

.field public static final dm_view_slide_note:I = 0x7f07024f

.field public static final dm_width:I = 0x7f070250

.field public static final dm_wikipedia:I = 0x7f070251

.field public static final dm_wikipedia_search:I = 0x7f070252

.field public static final dm_word_columns:I = 0x7f070253

.field public static final dm_zoom:I = 0x7f070254

.field public static final engine_version:I = 0x7f070322

.field public static final find_match_button:I = 0x7f070255

.field public static final find_prev_button:I = 0x7f070256

.field public static final fm_SD_memory_card_name:I = 0x7f07002a

.field public static final fm_device_storage_name:I = 0x7f07001e

.field public static final fm_err_already_exist:I = 0x7f070257

.field public static final fm_err_canceled_by_user:I = 0x7f070258

.field public static final fm_err_delete_hidden_file:I = 0x7f070259

.field public static final fm_err_extract_password:I = 0x7f07025a

.field public static final fm_err_insufficient_memory:I = 0x7f07025b

.field public static final fm_err_invalid_file_length:I = 0x7f07025c

.field public static final fm_err_invalid_filelength:I = 0x7f07025d

.field public static final fm_err_invalid_filename:I = 0x7f07025e

.field public static final fm_err_invalid_folder_length:I = 0x7f07025f

.field public static final fm_err_move_hidden_file:I = 0x7f070260

.field public static final fm_err_recursive_folder:I = 0x7f070261

.field public static final fm_err_same_folder:I = 0x7f070262

.field public static final fm_err_src_not_defined:I = 0x7f070263

.field public static final fm_err_src_removed:I = 0x7f070264

.field public static final fm_err_unknown:I = 0x7f070265

.field public static final fm_err_viewmode_password:I = 0x7f070266

.field public static final fm_err_webstorage_busy:I = 0x7f070267

.field public static final fm_msg_deselect_all:I = 0x7f070268

.field public static final fm_msg_item_select_plural:I = 0x7f070269

.field public static final fm_msg_item_select_singular:I = 0x7f07026a

.field public static final fm_msg_no_items:I = 0x7f07026b

.field public static final fm_msg_no_sdcard:I = 0x7f07026c

.field public static final fm_msg_progress_arrange:I = 0x7f07026d

.field public static final fm_msg_progress_create:I = 0x7f07026e

.field public static final fm_msg_progress_media_scan:I = 0x7f07026f

.field public static final fm_msg_progress_progress:I = 0x7f070270

.field public static final fm_msg_progress_web_download:I = 0x7f070271

.field public static final fm_msg_progress_web_load:I = 0x7f070272

.field public static final fm_msg_select_all:I = 0x7f070273

.field public static final fm_name_sync_folder:I = 0x7f07002d

.field public static final fm_personal_page_name:I = 0x7f07002c

.field public static final fm_property_attr_readonly:I = 0x7f070274

.field public static final fm_property_attr_writable:I = 0x7f070275

.field public static final fm_property_contents_file:I = 0x7f070276

.field public static final fm_property_contents_folder:I = 0x7f070277

.field public static final fm_property_empty:I = 0x7f070278

.field public static final fm_property_item_attributes:I = 0x7f070279

.field public static final fm_property_item_author:I = 0x7f07027a

.field public static final fm_property_item_contents:I = 0x7f07027b

.field public static final fm_property_item_location:I = 0x7f07027c

.field public static final fm_property_item_modified:I = 0x7f07027d

.field public static final fm_property_item_modified_by:I = 0x7f07027e

.field public static final fm_property_item_page:I = 0x7f07027f

.field public static final fm_property_item_size:I = 0x7f070280

.field public static final fm_property_item_title:I = 0x7f070281

.field public static final fm_property_item_type:I = 0x7f070282

.field public static final fm_property_item_words:I = 0x7f070283

.field public static final fm_property_no_save:I = 0x7f070284

.field public static final fm_property_type_app:I = 0x7f070285

.field public static final fm_property_type_audio:I = 0x7f070286

.field public static final fm_property_type_csv:I = 0x7f070287

.field public static final fm_property_type_doc:I = 0x7f070020

.field public static final fm_property_type_docx:I = 0x7f070023

.field public static final fm_property_type_folder:I = 0x7f070288

.field public static final fm_property_type_html:I = 0x7f070289

.field public static final fm_property_type_hwp:I = 0x7f07028a

.field public static final fm_property_type_image:I = 0x7f07028b

.field public static final fm_property_type_pdf:I = 0x7f070021

.field public static final fm_property_type_pps:I = 0x7f070026

.field public static final fm_property_type_ppsx:I = 0x7f070027

.field public static final fm_property_type_ppt:I = 0x7f070022

.field public static final fm_property_type_pptx:I = 0x7f070025

.field public static final fm_property_type_rtf:I = 0x7f07028c

.field public static final fm_property_type_snb:I = 0x7f070028

.field public static final fm_property_type_text:I = 0x7f07028d

.field public static final fm_property_type_unknown:I = 0x7f07028e

.field public static final fm_property_type_vcard:I = 0x7f07028f

.field public static final fm_property_type_vcs:I = 0x7f070290

.field public static final fm_property_type_video:I = 0x7f070291

.field public static final fm_property_type_xls:I = 0x7f07001f

.field public static final fm_property_type_xlsx:I = 0x7f070024

.field public static final fm_property_type_xml:I = 0x7f070292

.field public static final fm_property_type_zip:I = 0x7f070293

.field public static final fm_root_folder_name:I = 0x7f070029

.field public static final fm_saveas_filename:I = 0x7f070294

.field public static final fm_saveas_title:I = 0x7f070295

.field public static final fm_title_add_account:I = 0x7f070296

.field public static final fm_title_information:I = 0x7f070297

.field public static final fm_title_recent:I = 0x7f070298

.field public static final fm_usb_drive_name:I = 0x7f07002b

.field public static final free_download:I = 0x7f0702b4

.field public static final freedraw_mode_pen:I = 0x7f070299

.field public static final multi_selection_limit_message:I = 0x7f07029a

.field public static final paste_has_done:I = 0x7f07029b

.field public static final po_already_open:I = 0x7f07029c

.field public static final po_dictinoary_maximize:I = 0x7f07029d

.field public static final po_dictinoary_minimize:I = 0x7f07029e

.field public static final po_dictinoary_select_title:I = 0x7f07029f

.field public static final po_dlg_title_clear_history:I = 0x7f0702a0

.field public static final po_dlg_title_default_format:I = 0x7f0702a1

.field public static final po_dlg_title_new_folder:I = 0x7f0702a2

.field public static final po_dlg_title_rename_file:I = 0x7f0702a3

.field public static final po_dlg_title_rename_folder:I = 0x7f0702a4

.field public static final po_dlg_title_sync:I = 0x7f0702a5

.field public static final po_err_file_already_exists:I = 0x7f0702a6

.field public static final po_err_folder_already_exists:I = 0x7f0702a7

.field public static final po_info_save:I = 0x7f0702a8

.field public static final po_menu_item_copy:I = 0x7f0702a9

.field public static final po_menu_item_move:I = 0x7f0702aa

.field public static final po_menu_item_new_folder:I = 0x7f0702ab

.field public static final po_menu_item_property:I = 0x7f0702ac

.field public static final po_menu_item_setting:I = 0x7f0702ad

.field public static final po_menu_title_send:I = 0x7f0702ae

.field public static final po_msg_clear_history:I = 0x7f0702af

.field public static final po_msg_delete_favorite:I = 0x7f0702b0

.field public static final po_msg_not_support_app:I = 0x7f0702b1

.field public static final po_msg_office_download_about:I = 0x7f0702b2

.field public static final po_msg_office_download_about_2:I = 0x7f0702b3

.field public static final po_msg_sync_charge:I = 0x7f0702b5

.field public static final po_msg_sync_upload_max:I = 0x7f0702b6

.field public static final po_msg_viewer_info1:I = 0x7f0702b7

.field public static final po_msg_viewer_info2:I = 0x7f0702b8

.field public static final po_new_ppt_add_subtitle:I = 0x7f0702b9

.field public static final po_new_ppt_add_text:I = 0x7f0702ba

.field public static final po_new_ppt_add_title:I = 0x7f0702bb

.field public static final po_office_download_info_visibility_text:I = 0x7f0702bc

.field public static final po_title_security_box:I = 0x7f07002e

.field public static final po_url_eula:I = 0x7f07002f

.field public static final po_url_help:I = 0x7f070031

.field public static final po_url_open_source:I = 0x7f070030

.field public static final po_web_title_eula:I = 0x7f070032

.field public static final po_web_title_help:I = 0x7f0702bd

.field public static final po_web_title_notice:I = 0x7f0702be

.field public static final po_web_title_open_source:I = 0x7f0702bf

.field public static final popup_str_delete:I = 0x7f0702c0

.field public static final popup_str_delete_columns:I = 0x7f0702c1

.field public static final popup_str_delete_rows:I = 0x7f0702c2

.field public static final popup_str_memo:I = 0x7f0702c3

.field public static final popup_str_more:I = 0x7f0702c4

.field public static final popup_str_pdf_annotation_property:I = 0x7f0702c5

.field public static final popup_str_pdf_edit_annotation:I = 0x7f0702c6

.field public static final popup_str_search:I = 0x7f0702c7

.field public static final popup_str_select_cell:I = 0x7f0702c8

.field public static final popup_str_share:I = 0x7f0702c9

.field public static final sd_card:I = 0x7f070033

.field public static final sheetname_empty_msg:I = 0x7f0702ca

.field public static final sheetname_error_msg:I = 0x7f0702cb

.field public static final sheetname_limit_msg:I = 0x7f0702cc

.field public static final sk_menu_item_send:I = 0x7f0702cd

.field public static final sk_menu_title_send:I = 0x7f0702ce

.field public static final slide_show_tooltip_Pen:I = 0x7f0702cf

.field public static final slide_show_tooltip_Pointer:I = 0x7f0702d0

.field public static final slide_show_tooltip_change_mode:I = 0x7f0702d1

.field public static final slide_show_tooltip_erase:I = 0x7f0702d2

.field public static final slide_show_tooltip_erase_all:I = 0x7f0702d3

.field public static final slide_show_tooltip_pen_setting:I = 0x7f0702d4

.field public static final slide_show_tooltip_pointer_setting:I = 0x7f0702d5

.field public static final slide_show_tooltip_slide:I = 0x7f0702d6

.field public static final str_polarisprint2:I = 0x7f070034

.field public static final string_dictionary_no_result:I = 0x7f0702d7

.field public static final string_dictionary_no_word:I = 0x7f0702d8

.field public static final string_dictionary_search_fail:I = 0x7f0702d9

.field public static final string_filemanager_copy_confirm:I = 0x7f0702da

.field public static final string_filemanager_move_confirm:I = 0x7f0702db

.field public static final string_free_drawing_pointer:I = 0x7f0702dc

.field public static final string_free_drawing_scroll_mode:I = 0x7f0702dd

.field public static final string_indicate_write_doc_password:I = 0x7f0702de

.field public static final string_panel_format_color:I = 0x7f0702df

.field public static final string_text_editor_encoding_auto_detect:I = 0x7f0702e0

.field public static final string_title_tableofcontents:I = 0x7f0702e1

.field public static final string_toast_no_installed_dictionay:I = 0x7f0702e2

.field public static final string_video_cannot_find_file:I = 0x7f0702e3

.field public static final string_video_play_error:I = 0x7f0702e4

.field public static final string_viewer_print_print_range:I = 0x7f0702e5

.field public static final stub_alert:I = 0x7f0702e6

.field public static final stub_network_is_currently_unavailable:I = 0x7f0702e7

.field public static final stub_unavailable_update:I = 0x7f0702e8

.field public static final stub_warning_for_charge:I = 0x7f0702e9

.field public static final te_bookmark_loadingdialog:I = 0x7f0702ea

.field public static final te_changeblockselectedrange:I = 0x7f0702eb

.field public static final te_dlgAccessDenied:I = 0x7f0702ec

.field public static final te_dlgAccessDeniedSDcard:I = 0x7f0702ed

.field public static final te_dlgDefaultPath:I = 0x7f070042

.field public static final te_encoding1:I = 0x7f0702ee

.field public static final te_encoding2:I = 0x7f0702ef

.field public static final te_encoding3:I = 0x7f0702f0

.field public static final te_encoding4:I = 0x7f0702f1

.field public static final te_findop_findmode:I = 0x7f0702f2

.field public static final te_findop_matchcase:I = 0x7f0702f3

.field public static final te_findop_matchwholeword:I = 0x7f0702f4

.field public static final te_findop_replacemode:I = 0x7f0702f5

.field public static final te_font_10pt:I = 0x7f070035

.field public static final te_font_12pt:I = 0x7f070036

.field public static final te_font_16pt:I = 0x7f070037

.field public static final te_font_8pt:I = 0x7f070038

.field public static final te_font_9pt:I = 0x7f070039

.field public static final te_fonttype_arial:I = 0x7f07003a

.field public static final te_fonttype_batang:I = 0x7f07003b

.field public static final te_fonttype_couriernew:I = 0x7f07003c

.field public static final te_fonttype_dotum:I = 0x7f07003d

.field public static final te_fonttype_gulim:I = 0x7f07003e

.field public static final te_fonttype_tahoma:I = 0x7f07003f

.field public static final te_fonttype_timesnewroman:I = 0x7f070040

.field public static final te_fonttype_verdana:I = 0x7f070041

.field public static final te_maxreplacement:I = 0x7f0702f6

.field public static final te_menuHelp:I = 0x7f0702f7

.field public static final te_menuInfo:I = 0x7f0702f8

.field public static final te_more_popup_search:I = 0x7f0702f9

.field public static final te_more_popup_share:I = 0x7f0702fa

.field public static final te_notsupport_editing:I = 0x7f0702fb

.field public static final te_preferences:I = 0x7f0702fc

.field public static final te_preferences_Encoding:I = 0x7f0702fd

.field public static final te_preferences_Fonttype:I = 0x7f0702fe

.field public static final te_preferences_fontsize:I = 0x7f0702ff

.field public static final te_preferences_theme:I = 0x7f070300

.field public static final te_printmsg_delaytime:I = 0x7f070301

.field public static final te_progressdialog:I = 0x7f070302

.field public static final te_progressdialog_title_findmode:I = 0x7f070303

.field public static final te_progressdialog_title_pasting:I = 0x7f070304

.field public static final te_progressdialog_title_redo:I = 0x7f070305

.field public static final te_progressdialog_title_replacemode:I = 0x7f070306

.field public static final te_progressdialog_title_undo:I = 0x7f070307

.field public static final te_replacementcompleted:I = 0x7f070308

.field public static final te_sendEmail:I = 0x7f070309

.field public static final te_tts:I = 0x7f07030a

.field public static final te_tts_afterscroll:I = 0x7f07030b

.field public static final te_tts_not_found_tts_engine:I = 0x7f07030c

.field public static final te_tts_percentage:I = 0x7f07030d

.field public static final te_tts_percentage2:I = 0x7f07030e

.field public static final te_tts_selected_done:I = 0x7f07030f

.field public static final te_tts_title_selected:I = 0x7f070310

.field public static final te_tts_title_whole:I = 0x7f070311

.field public static final te_tts_whole_done:I = 0x7f070312

.field public static final toastpopup_TTS_Talkback_error:I = 0x7f070313

.field public static final toastpopup_cannot_found:I = 0x7f070314

.field public static final toastpopup_cannot_pdf_export_for_reflow_text:I = 0x7f070315

.field public static final toastpopup_cannot_print_for_reflow_text:I = 0x7f070316

.field public static final toastpopup_cannot_read:I = 0x7f070317

.field public static final toastpopup_chart_logY_axis:I = 0x7f07031f

.field public static final toastpopup_doubletap_chart:I = 0x7f070318

.field public static final toastpopup_doubletap_shape:I = 0x7f070319

.field public static final toastpopup_fail_texttospeech:I = 0x7f07031a

.field public static final toastpopup_multi_select:I = 0x7f07031b

.field public static final toastpopup_shapedrawing_info:I = 0x7f07031c

.field public static final toastpopup_single_select:I = 0x7f07031d

.field public static final toolbar_notify_1:I = 0x7f07031e

.field public static final zoom100_submenu:I = 0x7f070046

.field public static final zoom125_submenu:I = 0x7f070047

.field public static final zoom150_submenu:I = 0x7f070048

.field public static final zoom200_submenu:I = 0x7f070049

.field public static final zoom25_submenu:I = 0x7f070043

.field public static final zoom300_submenu:I = 0x7f07004a

.field public static final zoom400_submenu:I = 0x7f07004b

.field public static final zoom50_submenu:I = 0x7f070044

.field public static final zoom75_submenu:I = 0x7f070045


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
