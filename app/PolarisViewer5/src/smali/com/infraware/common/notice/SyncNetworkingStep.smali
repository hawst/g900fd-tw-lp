.class public Lcom/infraware/common/notice/SyncNetworkingStep;
.super Ljava/lang/Object;
.source "SyncNetworkingStep.java"


# instance fields
.field public bEndFromThread:Z

.field private isNotify:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    iput-boolean v0, p0, Lcom/infraware/common/notice/SyncNetworkingStep;->bEndFromThread:Z

    .line 7
    iput-boolean v0, p0, Lcom/infraware/common/notice/SyncNetworkingStep;->isNotify:Z

    return-void
.end method


# virtual methods
.method public declared-synchronized waitTurn()V
    .locals 3

    .prologue
    .line 13
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-boolean v1, p0, Lcom/infraware/common/notice/SyncNetworkingStep;->isNotify:Z

    if-nez v1, :cond_0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 19
    :catch_0
    move-exception v0

    .line 21
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_1
    const-string/jumbo v1, "< UpdateStep >"

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 23
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :goto_1
    monitor-exit p0

    return-void

    .line 17
    :cond_0
    const/4 v1, 0x0

    :try_start_2
    iput-boolean v1, p0, Lcom/infraware/common/notice/SyncNetworkingStep;->isNotify:Z
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 13
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized waitTurn(J)V
    .locals 1
    .param p1, "timemil"    # J

    .prologue
    .line 29
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1, p2}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34
    :goto_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/infraware/common/notice/SyncNetworkingStep;->isNotify:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 35
    monitor-exit p0

    return-void

    .line 31
    :catch_0
    move-exception v0

    goto :goto_0

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized yourTurn(Z)V
    .locals 1
    .param p1, "isEndOfThread"    # Z

    .prologue
    .line 39
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/infraware/common/notice/SyncNetworkingStep;->isNotify:Z

    .line 40
    iput-boolean p1, p0, Lcom/infraware/common/notice/SyncNetworkingStep;->bEndFromThread:Z

    .line 41
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 42
    monitor-exit p0

    return-void

    .line 39
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
