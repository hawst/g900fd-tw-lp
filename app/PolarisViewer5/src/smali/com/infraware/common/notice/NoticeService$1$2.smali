.class Lcom/infraware/common/notice/NoticeService$1$2;
.super Ljava/lang/Object;
.source "NoticeService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/common/notice/NoticeService$1;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/infraware/common/notice/NoticeService$1;


# direct methods
.method constructor <init>(Lcom/infraware/common/notice/NoticeService$1;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/infraware/common/notice/NoticeService$1$2;->this$1:Lcom/infraware/common/notice/NoticeService$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x1

    .line 109
    const-string/jumbo v7, "https://usermgr1.polarisoffice.com/api/GetNoticeInfo.aspx"

    .line 110
    .local v7, "url":Ljava/lang/String;
    iget-object v8, p0, Lcom/infraware/common/notice/NoticeService$1$2;->this$1:Lcom/infraware/common/notice/NoticeService$1;

    iget-object v8, v8, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    # getter for: Lcom/infraware/common/notice/NoticeService;->m_isDoc2Server:Z
    invoke-static {v8}, Lcom/infraware/common/notice/NoticeService;->access$100(Lcom/infraware/common/notice/NoticeService;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 112
    const-string/jumbo v8, "usermgr1"

    const-string/jumbo v9, "usermgr2"

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 115
    :cond_0
    const-string/jumbo v0, "m"

    .line 116
    .local v0, "devicetype":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "/product;"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 117
    .local v2, "model":Ljava/lang/String;
    iget-object v8, p0, Lcom/infraware/common/notice/NoticeService$1$2;->this$1:Lcom/infraware/common/notice/NoticeService$1;

    iget-object v8, v8, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    iget-object v9, p0, Lcom/infraware/common/notice/NoticeService$1$2;->this$1:Lcom/infraware/common/notice/NoticeService$1;

    iget-object v9, v9, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    invoke-virtual {v9}, Lcom/infraware/common/notice/NoticeService;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget-object v9, v9, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v9}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v9

    # invokes: Lcom/infraware/common/notice/NoticeService;->getParamLanguage(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v8, v9}, Lcom/infraware/common/notice/NoticeService;->access$200(Lcom/infraware/common/notice/NoticeService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 118
    .local v1, "language":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/infraware/common/notice/NoticeService$1$2;->this$1:Lcom/infraware/common/notice/NoticeService$1;

    iget-object v9, v9, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    const v10, 0x7f070322

    invoke-virtual {v9, v10}, Lcom/infraware/common/notice/NoticeService;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0x2d

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, " "

    const-string/jumbo v11, ""

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 120
    .local v6, "productcode":Ljava/lang/String;
    iget-object v8, p0, Lcom/infraware/common/notice/NoticeService$1$2;->this$1:Lcom/infraware/common/notice/NoticeService$1;

    iget-object v8, v8, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    invoke-virtual {v8}, Lcom/infraware/common/notice/NoticeService;->makeOfficeDir()V

    .line 122
    new-instance v4, Lcom/infraware/common/notice/SyncNetworkingStep;

    invoke-direct {v4}, Lcom/infraware/common/notice/SyncNetworkingStep;-><init>()V

    .line 123
    .local v4, "netSync":Lcom/infraware/common/notice/SyncNetworkingStep;
    new-instance v5, Lcom/infraware/common/notice/NoticeNetworkConnect;

    invoke-direct {v5}, Lcom/infraware/common/notice/NoticeNetworkConnect;-><init>()V

    .line 125
    .local v5, "network":Lcom/infraware/common/notice/NoticeNetworkConnect;
    invoke-virtual {v5, v0}, Lcom/infraware/common/notice/NoticeNetworkConnect;->setDevice_Type(Ljava/lang/String;)V

    .line 126
    invoke-virtual {v5, v2}, Lcom/infraware/common/notice/NoticeNetworkConnect;->setModel(Ljava/lang/String;)V

    .line 127
    invoke-virtual {v5, v1}, Lcom/infraware/common/notice/NoticeNetworkConnect;->setLanguage(Ljava/lang/String;)V

    .line 128
    invoke-virtual {v5, v6}, Lcom/infraware/common/notice/NoticeNetworkConnect;->setProduct_code(Ljava/lang/String;)V

    .line 129
    iget-object v8, p0, Lcom/infraware/common/notice/NoticeService$1$2;->this$1:Lcom/infraware/common/notice/NoticeService$1;

    iget-object v8, v8, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    # getter for: Lcom/infraware/common/notice/NoticeService;->mTempPath:Ljava/lang/String;
    invoke-static {v8}, Lcom/infraware/common/notice/NoticeService;->access$300(Lcom/infraware/common/notice/NoticeService;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/infraware/common/notice/NoticeNetworkConnect;->setFilePath(Ljava/lang/String;)V

    .line 132
    invoke-virtual {v5, v4}, Lcom/infraware/common/notice/NoticeNetworkConnect;->setSyncher(Lcom/infraware/common/notice/SyncNetworkingStep;)V

    .line 133
    invoke-virtual {v5, v7}, Lcom/infraware/common/notice/NoticeNetworkConnect;->connectNetwork(Ljava/lang/String;)V

    .line 134
    invoke-virtual {v4}, Lcom/infraware/common/notice/SyncNetworkingStep;->waitTurn()V

    .line 136
    invoke-virtual {v5}, Lcom/infraware/common/notice/NoticeNetworkConnect;->getResult()I

    move-result v3

    .line 138
    .local v3, "nResult":I
    if-ne v3, v12, :cond_1

    .line 139
    iget-object v8, p0, Lcom/infraware/common/notice/NoticeService$1$2;->this$1:Lcom/infraware/common/notice/NoticeService$1;

    iget-object v8, v8, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    # setter for: Lcom/infraware/common/notice/NoticeService;->m_bGetNoticeSuccess:Z
    invoke-static {v8, v12}, Lcom/infraware/common/notice/NoticeService;->access$402(Lcom/infraware/common/notice/NoticeService;Z)Z

    .line 140
    iget-object v8, p0, Lcom/infraware/common/notice/NoticeService$1$2;->this$1:Lcom/infraware/common/notice/NoticeService$1;

    iget-object v8, v8, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    # getter for: Lcom/infraware/common/notice/NoticeService;->mEventHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/infraware/common/notice/NoticeService;->access$000(Lcom/infraware/common/notice/NoticeService;)Landroid/os/Handler;

    move-result-object v8

    const/16 v9, 0x44c

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 153
    :goto_0
    return-void

    .line 141
    :cond_1
    const/4 v8, 0x2

    if-ne v3, v8, :cond_2

    .line 142
    iget-object v8, p0, Lcom/infraware/common/notice/NoticeService$1$2;->this$1:Lcom/infraware/common/notice/NoticeService$1;

    iget-object v8, v8, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    # setter for: Lcom/infraware/common/notice/NoticeService;->m_bGetNoticeSuccess:Z
    invoke-static {v8, v12}, Lcom/infraware/common/notice/NoticeService;->access$402(Lcom/infraware/common/notice/NoticeService;Z)Z

    .line 143
    iget-object v8, p0, Lcom/infraware/common/notice/NoticeService$1$2;->this$1:Lcom/infraware/common/notice/NoticeService$1;

    iget-object v8, v8, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    # getter for: Lcom/infraware/common/notice/NoticeService;->mEventHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/infraware/common/notice/NoticeService;->access$000(Lcom/infraware/common/notice/NoticeService;)Landroid/os/Handler;

    move-result-object v8

    const/16 v9, 0x640

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 144
    :cond_2
    const/4 v8, 0x6

    if-ne v3, v8, :cond_3

    .line 145
    iget-object v8, p0, Lcom/infraware/common/notice/NoticeService$1$2;->this$1:Lcom/infraware/common/notice/NoticeService$1;

    iget-object v8, v8, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    iput v3, v8, Lcom/infraware/common/notice/NoticeService;->m_nFailValue:I

    .line 146
    iget-object v8, p0, Lcom/infraware/common/notice/NoticeService$1$2;->this$1:Lcom/infraware/common/notice/NoticeService$1;

    iget-object v8, v8, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    # setter for: Lcom/infraware/common/notice/NoticeService;->m_bGetNoticeSuccess:Z
    invoke-static {v8, v13}, Lcom/infraware/common/notice/NoticeService;->access$402(Lcom/infraware/common/notice/NoticeService;Z)Z

    .line 147
    iget-object v8, p0, Lcom/infraware/common/notice/NoticeService$1$2;->this$1:Lcom/infraware/common/notice/NoticeService$1;

    iget-object v8, v8, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    # getter for: Lcom/infraware/common/notice/NoticeService;->mEventHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/infraware/common/notice/NoticeService;->access$000(Lcom/infraware/common/notice/NoticeService;)Landroid/os/Handler;

    move-result-object v8

    const/16 v9, 0x578

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 149
    :cond_3
    iget-object v8, p0, Lcom/infraware/common/notice/NoticeService$1$2;->this$1:Lcom/infraware/common/notice/NoticeService$1;

    iget-object v8, v8, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    iput v3, v8, Lcom/infraware/common/notice/NoticeService;->m_nFailValue:I

    .line 150
    iget-object v8, p0, Lcom/infraware/common/notice/NoticeService$1$2;->this$1:Lcom/infraware/common/notice/NoticeService$1;

    iget-object v8, v8, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    # setter for: Lcom/infraware/common/notice/NoticeService;->m_bGetNoticeSuccess:Z
    invoke-static {v8, v13}, Lcom/infraware/common/notice/NoticeService;->access$402(Lcom/infraware/common/notice/NoticeService;Z)Z

    .line 151
    iget-object v8, p0, Lcom/infraware/common/notice/NoticeService$1$2;->this$1:Lcom/infraware/common/notice/NoticeService$1;

    iget-object v8, v8, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    # getter for: Lcom/infraware/common/notice/NoticeService;->mEventHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/infraware/common/notice/NoticeService;->access$000(Lcom/infraware/common/notice/NoticeService;)Landroid/os/Handler;

    move-result-object v8

    const/16 v9, 0x4b0

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
