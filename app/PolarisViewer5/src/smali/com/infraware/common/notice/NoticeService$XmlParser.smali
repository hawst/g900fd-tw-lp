.class Lcom/infraware/common/notice/NoticeService$XmlParser;
.super Ljava/lang/Object;
.source "NoticeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/common/notice/NoticeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "XmlParser"
.end annotation


# static fields
.field static final INDEX_OF_A:I = 0x1

.field static final INDEX_OF_B:I = 0x2

.field static final INDEX_OF_C:I = 0x3

.field static final INDEX_OF_DDD:I = 0x4

.field static final INDEX_OF_DEVICE:I = 0x0

.field static final INDEX_OF_F:I = 0x5

.field static final INDEX_OF_GG:I = 0x6

.field static final INDEX_OF_HH:I = 0x7

.field static final INDEX_OF_LANGUAGE:I = 0x8

.field static final eXML_FAQ_URL:I = 0x2

.field static final eXML_MAIN_URL:I = 0x5

.field static final eXML_NONE:I = 0x0

.field static final eXML_NOTICE_CREATE_DATE:I = 0x7

.field static final eXML_NOTICE_ID:I = 0x6

.field static final eXML_NOTICE_LIMIT_DATE:I = 0x8

.field static final eXML_NOTICE_TYPE:I = 0x1

.field static final eXML_NOTICE_URL:I = 0x4

.field static final eXML_USER_GUIDE_URL:I = 0x3


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parsingXML(Ljava/lang/String;)Lcom/infraware/common/notice/NoticeItem;
    .locals 14
    .param p0, "strFileName"    # Ljava/lang/String;

    .prologue
    .line 248
    new-instance v4, Lcom/infraware/common/notice/NoticeItem;

    invoke-direct {v4}, Lcom/infraware/common/notice/NoticeItem;-><init>()V

    .line 251
    .local v4, "item":Lcom/infraware/common/notice/NoticeItem;
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v1

    .line 252
    .local v1, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v6

    .line 254
    .local v6, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v11, 0x0

    .line 255
    .local v11, "xmlStatus":I
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 256
    .local v3, "is":Ljava/io/InputStream;
    const/4 v12, 0x0

    invoke-interface {v6, v3, v12}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 257
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v7

    .line 259
    .local v7, "parserEvent":I
    const-string/jumbo v9, ""

    .line 260
    .local v9, "strNoticeId":Ljava/lang/String;
    const-string/jumbo v8, ""

    .line 261
    .local v8, "strNoticeCreateDate":Ljava/lang/String;
    const-string/jumbo v10, ""

    .line 264
    .local v10, "strNoticeLimitDate":Ljava/lang/String;
    :goto_0
    const/4 v12, 0x1

    if-eq v7, v12, :cond_9

    .line 267
    packed-switch v7, :pswitch_data_0

    .line 341
    :cond_0
    :goto_1
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v7

    goto :goto_0

    .line 270
    :pswitch_0
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v12

    const-string/jumbo v13, "NoticeType"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 273
    const/4 v11, 0x1

    .line 274
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v5

    .line 275
    .local v5, "nSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-ge v2, v5, :cond_0

    .line 277
    invoke-interface {v6, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4, v12, v2}, Lcom/infraware/common/notice/NoticeItem;->setArrNotice_Attribute(Ljava/lang/String;I)V

    .line 275
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 281
    .end local v2    # "i":I
    .end local v5    # "nSize":I
    :cond_1
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v12

    const-string/jumbo v13, "FAQURL"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 282
    const/4 v11, 0x2

    goto :goto_1

    .line 283
    :cond_2
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v12

    const-string/jumbo v13, "UserGuideURL"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 284
    const/4 v11, 0x3

    goto :goto_1

    .line 285
    :cond_3
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v12

    const-string/jumbo v13, "NoticeURL"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 286
    const/4 v11, 0x4

    goto :goto_1

    .line 287
    :cond_4
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v12

    const-string/jumbo v13, "MainURL"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 288
    const/4 v11, 0x5

    goto :goto_1

    .line 289
    :cond_5
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v12

    const-string/jumbo v13, "NoticeID"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 290
    const/4 v11, 0x6

    goto :goto_1

    .line 291
    :cond_6
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v12

    const-string/jumbo v13, "NoticeCreateDate"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 292
    const/4 v11, 0x7

    goto :goto_1

    .line 293
    :cond_7
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v12

    const-string/jumbo v13, "NoticeLimitDate"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 294
    const/16 v11, 0x8

    goto/16 :goto_1

    .line 300
    :pswitch_1
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v12

    const-string/jumbo v13, "Notice"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 302
    invoke-virtual {v4, v9, v8, v10}, Lcom/infraware/common/notice/NoticeItem;->addNoticeList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    const-string/jumbo v9, ""

    .line 304
    const-string/jumbo v8, ""

    .line 305
    const-string/jumbo v10, ""

    .line 308
    :cond_8
    const/4 v11, 0x0

    .line 309
    goto/16 :goto_1

    .line 312
    :pswitch_2
    packed-switch v11, :pswitch_data_1

    goto/16 :goto_1

    .line 316
    :pswitch_3
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4, v12}, Lcom/infraware/common/notice/NoticeItem;->setFAQUrl(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 344
    .end local v1    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v3    # "is":Ljava/io/InputStream;
    .end local v6    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v7    # "parserEvent":I
    .end local v8    # "strNoticeCreateDate":Ljava/lang/String;
    .end local v9    # "strNoticeId":Ljava/lang/String;
    .end local v10    # "strNoticeLimitDate":Ljava/lang/String;
    .end local v11    # "xmlStatus":I
    :catch_0
    move-exception v0

    .line 346
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v12, "NOTICE_SERVICE"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    const/4 v4, 0x0

    .line 351
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_9
    return-object v4

    .line 319
    .restart local v1    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v3    # "is":Ljava/io/InputStream;
    .restart local v6    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v7    # "parserEvent":I
    .restart local v8    # "strNoticeCreateDate":Ljava/lang/String;
    .restart local v9    # "strNoticeId":Ljava/lang/String;
    .restart local v10    # "strNoticeLimitDate":Ljava/lang/String;
    .restart local v11    # "xmlStatus":I
    :pswitch_4
    :try_start_1
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4, v12}, Lcom/infraware/common/notice/NoticeItem;->setUserGuideUrl(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 322
    :pswitch_5
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4, v12}, Lcom/infraware/common/notice/NoticeItem;->setNoticeUrl(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 325
    :pswitch_6
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4, v12}, Lcom/infraware/common/notice/NoticeItem;->setMainUrl(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 328
    :pswitch_7
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v9

    .line 329
    goto/16 :goto_1

    .line 331
    :pswitch_8
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v8

    .line 332
    goto/16 :goto_1

    .line 334
    :pswitch_9
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v10

    goto/16 :goto_1

    .line 267
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 312
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
