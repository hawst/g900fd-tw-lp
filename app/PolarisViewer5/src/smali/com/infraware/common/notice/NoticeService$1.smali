.class Lcom/infraware/common/notice/NoticeService$1;
.super Landroid/os/Handler;
.source "NoticeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/common/notice/NoticeService;->SetEventHandler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/common/notice/NoticeService;


# direct methods
.method constructor <init>(Lcom/infraware/common/notice/NoticeService;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v3, 0x5dc

    .line 84
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 86
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 207
    :goto_0
    return-void

    .line 88
    :sswitch_0
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/infraware/common/notice/NoticeService$1$1;

    invoke-direct {v2, p0}, Lcom/infraware/common/notice/NoticeService$1$1;-><init>(Lcom/infraware/common/notice/NoticeService$1;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 106
    :sswitch_1
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/infraware/common/notice/NoticeService$1$2;

    invoke-direct {v2, p0}, Lcom/infraware/common/notice/NoticeService$1$2;-><init>(Lcom/infraware/common/notice/NoticeService$1;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 159
    :sswitch_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    # getter for: Lcom/infraware/common/notice/NoticeService;->mTempPath:Ljava/lang/String;
    invoke-static {v2}, Lcom/infraware/common/notice/NoticeService;->access$300(Lcom/infraware/common/notice/NoticeService;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "OfficeNotice.xml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/infraware/common/notice/NoticeService$XmlParser;->parsingXML(Ljava/lang/String;)Lcom/infraware/common/notice/NoticeItem;

    move-result-object v0

    .line 161
    .local v0, "item":Lcom/infraware/common/notice/NoticeItem;
    if-nez v0, :cond_0

    .line 163
    iget-object v1, p0, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    # getter for: Lcom/infraware/common/notice/NoticeService;->mEventHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/infraware/common/notice/NoticeService;->access$000(Lcom/infraware/common/notice/NoticeService;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x6a4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 167
    :cond_0
    iget-object v1, p0, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    # getter for: Lcom/infraware/common/notice/NoticeService;->context:Landroid/content/Context;
    invoke-static {v1}, Lcom/infraware/common/notice/NoticeService;->access$500(Lcom/infraware/common/notice/NoticeService;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/infraware/common/notice/NoticeNotifyManager;->setNoticeItem(Landroid/content/Context;Lcom/infraware/common/notice/NoticeItem;)V

    .line 168
    iget-object v1, p0, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    # getter for: Lcom/infraware/common/notice/NoticeService;->mEventHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/infraware/common/notice/NoticeService;->access$000(Lcom/infraware/common/notice/NoticeService;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 173
    .end local v0    # "item":Lcom/infraware/common/notice/NoticeItem;
    :sswitch_3
    iget-object v1, p0, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    # getter for: Lcom/infraware/common/notice/NoticeService;->mEventHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/infraware/common/notice/NoticeService;->access$000(Lcom/infraware/common/notice/NoticeService;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 177
    :sswitch_4
    iget-object v1, p0, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    # getter for: Lcom/infraware/common/notice/NoticeService;->mEventHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/infraware/common/notice/NoticeService;->access$000(Lcom/infraware/common/notice/NoticeService;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 181
    :sswitch_5
    iget-object v1, p0, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    iget v1, v1, Lcom/infraware/common/notice/NoticeService;->m_nFailValue:I

    .line 185
    iget-object v1, p0, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    # getter for: Lcom/infraware/common/notice/NoticeService;->mEventHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/infraware/common/notice/NoticeService;->access$000(Lcom/infraware/common/notice/NoticeService;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 189
    :sswitch_6
    iget-object v1, p0, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    # getter for: Lcom/infraware/common/notice/NoticeService;->mEventHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/infraware/common/notice/NoticeService;->access$000(Lcom/infraware/common/notice/NoticeService;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 193
    :sswitch_7
    iget-object v1, p0, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    # getter for: Lcom/infraware/common/notice/NoticeService;->m_isDoc2Server:Z
    invoke-static {v1}, Lcom/infraware/common/notice/NoticeService;->access$100(Lcom/infraware/common/notice/NoticeService;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 195
    iget-object v1, p0, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    const/4 v2, 0x1

    # setter for: Lcom/infraware/common/notice/NoticeService;->m_isDoc2Server:Z
    invoke-static {v1, v2}, Lcom/infraware/common/notice/NoticeService;->access$102(Lcom/infraware/common/notice/NoticeService;Z)Z

    .line 196
    iget-object v1, p0, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    # getter for: Lcom/infraware/common/notice/NoticeService;->mEventHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/infraware/common/notice/NoticeService;->access$000(Lcom/infraware/common/notice/NoticeService;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 199
    :cond_1
    iget-object v1, p0, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    # getter for: Lcom/infraware/common/notice/NoticeService;->mEventHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/infraware/common/notice/NoticeService;->access$000(Lcom/infraware/common/notice/NoticeService;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 203
    :sswitch_8
    iget-object v1, p0, Lcom/infraware/common/notice/NoticeService$1;->this$0:Lcom/infraware/common/notice/NoticeService;

    invoke-virtual {v1}, Lcom/infraware/common/notice/NoticeService;->stopSelf()V

    goto/16 :goto_0

    .line 86
    :sswitch_data_0
    .sparse-switch
        0x384 -> :sswitch_0
        0x3e8 -> :sswitch_1
        0x44c -> :sswitch_2
        0x4b0 -> :sswitch_5
        0x514 -> :sswitch_6
        0x578 -> :sswitch_7
        0x5dc -> :sswitch_8
        0x640 -> :sswitch_3
        0x6a4 -> :sswitch_4
    .end sparse-switch
.end method
