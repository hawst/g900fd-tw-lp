.class Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;
.super Ljava/lang/Thread;
.source "NoticeNetworkConnect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/common/notice/NoticeNetworkConnect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NoticeWorker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/common/notice/NoticeNetworkConnect;


# direct methods
.method public constructor <init>(Lcom/infraware/common/notice/NoticeNetworkConnect;Ljava/lang/String;)V
    .locals 0
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;->this$0:Lcom/infraware/common/notice/NoticeNetworkConnect;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 169
    # setter for: Lcom/infraware/common/notice/NoticeNetworkConnect;->mUrl:Ljava/lang/String;
    invoke-static {p1, p2}, Lcom/infraware/common/notice/NoticeNetworkConnect;->access$202(Lcom/infraware/common/notice/NoticeNetworkConnect;Ljava/lang/String;)Ljava/lang/String;

    .line 170
    return-void
.end method


# virtual methods
.method public run()V
    .locals 21

    .prologue
    .line 175
    const/4 v9, 0x0

    .line 176
    .local v9, "in":Ljava/io/BufferedReader;
    const/4 v3, 0x0

    .line 179
    .local v3, "client":Lorg/apache/http/client/HttpClient;
    :try_start_0
    const-string/jumbo v17, "NoticeNetworkConnect"

    const-string/jumbo v18, "[RegisterNetworkConnect] Start"

    invoke-static/range {v17 .. v18}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    new-instance v4, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v4}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 194
    .end local v3    # "client":Lorg/apache/http/client/HttpClient;
    .local v4, "client":Lorg/apache/http/client/HttpClient;
    :try_start_1
    invoke-interface {v4}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v17

    const-string/jumbo v18, "http.protocol.expect-continue"

    const/16 v19, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v19

    invoke-interface/range {v17 .. v19}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 195
    invoke-interface {v4}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v17

    const-string/jumbo v18, "http.connection.timeout"

    new-instance v19, Ljava/lang/Integer;

    const/16 v20, 0x1388

    invoke-direct/range {v19 .. v20}, Ljava/lang/Integer;-><init>(I)V

    invoke-interface/range {v17 .. v19}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 196
    invoke-interface {v4}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v17

    const-string/jumbo v18, "http.socket.timeout"

    new-instance v19, Ljava/lang/Integer;

    const/16 v20, 0x1388

    invoke-direct/range {v19 .. v20}, Ljava/lang/Integer;-><init>(I)V

    invoke-interface/range {v17 .. v19}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 197
    invoke-interface {v4}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v17

    const-wide/16 v18, 0x1e

    sget-object v20, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v17 .. v20}, Lorg/apache/http/conn/ClientConnectionManager;->closeIdleConnections(JLjava/util/concurrent/TimeUnit;)V

    .line 198
    new-instance v8, Lorg/apache/http/client/methods/HttpPost;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;->this$0:Lcom/infraware/common/notice/NoticeNetworkConnect;

    move-object/from16 v17, v0

    # getter for: Lcom/infraware/common/notice/NoticeNetworkConnect;->mUrl:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/infraware/common/notice/NoticeNetworkConnect;->access$200(Lcom/infraware/common/notice/NoticeNetworkConnect;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v8, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 200
    .local v8, "httppost":Lorg/apache/http/client/methods/HttpPost;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 202
    .local v11, "postParameters":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v17, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v18, "device_type"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;->this$0:Lcom/infraware/common/notice/NoticeNetworkConnect;

    move-object/from16 v19, v0

    # getter for: Lcom/infraware/common/notice/NoticeNetworkConnect;->mDevice_type:Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Lcom/infraware/common/notice/NoticeNetworkConnect;->access$300(Lcom/infraware/common/notice/NoticeNetworkConnect;)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v17 .. v19}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 203
    new-instance v17, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v18, "product_code"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;->this$0:Lcom/infraware/common/notice/NoticeNetworkConnect;

    move-object/from16 v19, v0

    # getter for: Lcom/infraware/common/notice/NoticeNetworkConnect;->mProduct_code:Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Lcom/infraware/common/notice/NoticeNetworkConnect;->access$400(Lcom/infraware/common/notice/NoticeNetworkConnect;)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v17 .. v19}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 204
    new-instance v17, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v18, "model"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;->this$0:Lcom/infraware/common/notice/NoticeNetworkConnect;

    move-object/from16 v19, v0

    # getter for: Lcom/infraware/common/notice/NoticeNetworkConnect;->mModel:Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Lcom/infraware/common/notice/NoticeNetworkConnect;->access$500(Lcom/infraware/common/notice/NoticeNetworkConnect;)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v17 .. v19}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    new-instance v17, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v18, "lang"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;->this$0:Lcom/infraware/common/notice/NoticeNetworkConnect;

    move-object/from16 v19, v0

    # getter for: Lcom/infraware/common/notice/NoticeNetworkConnect;->mLanguage:Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Lcom/infraware/common/notice/NoticeNetworkConnect;->access$600(Lcom/infraware/common/notice/NoticeNetworkConnect;)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v17 .. v19}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207
    new-instance v7, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    const-string/jumbo v17, "UTF-8"

    move-object/from16 v0, v17

    invoke-direct {v7, v11, v0}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 209
    .local v7, "formEntity":Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    invoke-virtual {v8, v7}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 210
    invoke-interface {v4, v8}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v12

    .line 212
    .local v12, "response":Lorg/apache/http/HttpResponse;
    const-string/jumbo v13, ""

    .line 213
    .local v13, "result":Ljava/lang/String;
    const-string/jumbo v2, ""
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 214
    .local v2, "buffer":Ljava/lang/String;
    const/4 v15, 0x0

    .line 217
    .local v15, "sw":Ljava/io/OutputStreamWriter;
    :try_start_2
    new-instance v10, Ljava/io/BufferedReader;

    new-instance v17, Ljava/io/InputStreamReader;

    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v17

    invoke-direct {v10, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 218
    .end local v9    # "in":Ljava/io/BufferedReader;
    .local v10, "in":Ljava/io/BufferedReader;
    :try_start_3
    new-instance v6, Ljava/io/File;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;->this$0:Lcom/infraware/common/notice/NoticeNetworkConnect;

    move-object/from16 v18, v0

    # getter for: Lcom/infraware/common/notice/NoticeNetworkConnect;->mFilePath:Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/infraware/common/notice/NoticeNetworkConnect;->access$700(Lcom/infraware/common/notice/NoticeNetworkConnect;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, "OfficeNotice.xml"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 220
    .local v6, "file":Ljava/io/File;
    new-instance v16, Ljava/io/OutputStreamWriter;

    new-instance v17, Ljava/io/FileOutputStream;

    move-object/from16 v0, v17

    invoke-direct {v0, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const-string/jumbo v18, "UTF-8"

    invoke-direct/range {v16 .. v18}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 222
    .end local v15    # "sw":Ljava/io/OutputStreamWriter;
    .local v16, "sw":Ljava/io/OutputStreamWriter;
    :goto_0
    :try_start_4
    invoke-virtual {v10}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 224
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, "\n"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto :goto_0

    .line 226
    :cond_0
    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    .line 227
    const-string/jumbo v17, "NoticeNetworkConnect"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "[RegisterNetworkConnect] Result = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_5

    .line 235
    if-eqz v10, :cond_1

    .line 236
    :try_start_5
    invoke-virtual {v10}, Ljava/io/BufferedReader;->close()V

    .line 237
    :cond_1
    if-eqz v16, :cond_b

    .line 238
    invoke-virtual/range {v16 .. v16}, Ljava/io/OutputStreamWriter;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    move-object/from16 v15, v16

    .end local v16    # "sw":Ljava/io/OutputStreamWriter;
    .restart local v15    # "sw":Ljava/io/OutputStreamWriter;
    move-object v9, v10

    .line 241
    .end local v6    # "file":Ljava/io/File;
    .end local v10    # "in":Ljava/io/BufferedReader;
    .restart local v9    # "in":Ljava/io/BufferedReader;
    :cond_2
    :goto_1
    :try_start_6
    const-string/jumbo v17, ""

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 243
    const-string/jumbo v17, "NoticeNetworkConnect"

    const-string/jumbo v18, "NO NOTICE"

    invoke-static/range {v17 .. v18}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;->this$0:Lcom/infraware/common/notice/NoticeNetworkConnect;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/infraware/common/notice/NoticeNetworkConnect;->mResult:I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 261
    :goto_2
    if-eqz v4, :cond_a

    invoke-interface {v4}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v17

    if-eqz v17, :cond_a

    .line 262
    invoke-interface {v4}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Lorg/apache/http/conn/ClientConnectionManager;->closeExpiredConnections()V

    move-object v3, v4

    .line 265
    .end local v2    # "buffer":Ljava/lang/String;
    .end local v4    # "client":Lorg/apache/http/client/HttpClient;
    .end local v7    # "formEntity":Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .end local v8    # "httppost":Lorg/apache/http/client/methods/HttpPost;
    .end local v11    # "postParameters":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .end local v12    # "response":Lorg/apache/http/HttpResponse;
    .end local v13    # "result":Ljava/lang/String;
    .end local v15    # "sw":Ljava/io/OutputStreamWriter;
    .restart local v3    # "client":Lorg/apache/http/client/HttpClient;
    :cond_3
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;->this$0:Lcom/infraware/common/notice/NoticeNetworkConnect;

    move-object/from16 v17, v0

    # getter for: Lcom/infraware/common/notice/NoticeNetworkConnect;->mNs:Lcom/infraware/common/notice/SyncNetworkingStep;
    invoke-static/range {v17 .. v17}, Lcom/infraware/common/notice/NoticeNetworkConnect;->access$100(Lcom/infraware/common/notice/NoticeNetworkConnect;)Lcom/infraware/common/notice/SyncNetworkingStep;

    move-result-object v17

    if-eqz v17, :cond_4

    .line 266
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;->this$0:Lcom/infraware/common/notice/NoticeNetworkConnect;

    move-object/from16 v17, v0

    # getter for: Lcom/infraware/common/notice/NoticeNetworkConnect;->mNs:Lcom/infraware/common/notice/SyncNetworkingStep;
    invoke-static/range {v17 .. v17}, Lcom/infraware/common/notice/NoticeNetworkConnect;->access$100(Lcom/infraware/common/notice/NoticeNetworkConnect;)Lcom/infraware/common/notice/SyncNetworkingStep;

    move-result-object v17

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/infraware/common/notice/SyncNetworkingStep;->yourTurn(Z)V

    .line 267
    :cond_4
    return-void

    .line 229
    .end local v3    # "client":Lorg/apache/http/client/HttpClient;
    .restart local v2    # "buffer":Ljava/lang/String;
    .restart local v4    # "client":Lorg/apache/http/client/HttpClient;
    .restart local v7    # "formEntity":Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v8    # "httppost":Lorg/apache/http/client/methods/HttpPost;
    .restart local v11    # "postParameters":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .restart local v12    # "response":Lorg/apache/http/HttpResponse;
    .restart local v13    # "result":Ljava/lang/String;
    .restart local v15    # "sw":Ljava/io/OutputStreamWriter;
    :catch_0
    move-exception v5

    .line 231
    .local v5, "e":Ljava/lang/Exception;
    :goto_4
    :try_start_7
    const-string/jumbo v17, "NoticeNetworkConnect"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "File Excpetion! : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 235
    if-eqz v9, :cond_5

    .line 236
    :try_start_8
    invoke-virtual {v9}, Ljava/io/BufferedReader;->close()V

    .line 237
    :cond_5
    if-eqz v15, :cond_2

    .line 238
    invoke-virtual {v15}, Ljava/io/OutputStreamWriter;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    .line 251
    .end local v2    # "buffer":Ljava/lang/String;
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v7    # "formEntity":Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .end local v8    # "httppost":Lorg/apache/http/client/methods/HttpPost;
    .end local v11    # "postParameters":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .end local v12    # "response":Lorg/apache/http/HttpResponse;
    .end local v13    # "result":Ljava/lang/String;
    .end local v15    # "sw":Ljava/io/OutputStreamWriter;
    :catch_1
    move-exception v5

    move-object v3, v4

    .line 253
    .end local v4    # "client":Lorg/apache/http/client/HttpClient;
    .restart local v3    # "client":Lorg/apache/http/client/HttpClient;
    .restart local v5    # "e":Ljava/lang/Exception;
    :goto_5
    :try_start_9
    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v14

    .line 255
    .local v14, "strErr":Ljava/lang/String;
    const-string/jumbo v17, "NoticeNetworkConnect"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "[RegisterNetworkConnect] Errer : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;->this$0:Lcom/infraware/common/notice/NoticeNetworkConnect;

    move-object/from16 v17, v0

    const/16 v18, 0x6

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/infraware/common/notice/NoticeNetworkConnect;->mResult:I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 261
    if-eqz v3, :cond_3

    invoke-interface {v3}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v17

    if-eqz v17, :cond_3

    .line 262
    invoke-interface {v3}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Lorg/apache/http/conn/ClientConnectionManager;->closeExpiredConnections()V

    goto/16 :goto_3

    .line 235
    .end local v3    # "client":Lorg/apache/http/client/HttpClient;
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v14    # "strErr":Ljava/lang/String;
    .restart local v2    # "buffer":Ljava/lang/String;
    .restart local v4    # "client":Lorg/apache/http/client/HttpClient;
    .restart local v7    # "formEntity":Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v8    # "httppost":Lorg/apache/http/client/methods/HttpPost;
    .restart local v11    # "postParameters":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .restart local v12    # "response":Lorg/apache/http/HttpResponse;
    .restart local v13    # "result":Ljava/lang/String;
    .restart local v15    # "sw":Ljava/io/OutputStreamWriter;
    :catchall_0
    move-exception v17

    :goto_6
    if-eqz v9, :cond_6

    .line 236
    :try_start_a
    invoke-virtual {v9}, Ljava/io/BufferedReader;->close()V

    .line 237
    :cond_6
    if-eqz v15, :cond_7

    .line 238
    invoke-virtual {v15}, Ljava/io/OutputStreamWriter;->close()V

    :cond_7
    throw v17
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 261
    .end local v2    # "buffer":Ljava/lang/String;
    .end local v7    # "formEntity":Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .end local v8    # "httppost":Lorg/apache/http/client/methods/HttpPost;
    .end local v11    # "postParameters":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .end local v12    # "response":Lorg/apache/http/HttpResponse;
    .end local v13    # "result":Ljava/lang/String;
    .end local v15    # "sw":Ljava/io/OutputStreamWriter;
    :catchall_1
    move-exception v17

    move-object v3, v4

    .end local v4    # "client":Lorg/apache/http/client/HttpClient;
    .restart local v3    # "client":Lorg/apache/http/client/HttpClient;
    :goto_7
    if-eqz v3, :cond_8

    invoke-interface {v3}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v18

    if-eqz v18, :cond_8

    .line 262
    invoke-interface {v3}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lorg/apache/http/conn/ClientConnectionManager;->closeExpiredConnections()V

    :cond_8
    throw v17

    .line 248
    .end local v3    # "client":Lorg/apache/http/client/HttpClient;
    .restart local v2    # "buffer":Ljava/lang/String;
    .restart local v4    # "client":Lorg/apache/http/client/HttpClient;
    .restart local v7    # "formEntity":Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v8    # "httppost":Lorg/apache/http/client/methods/HttpPost;
    .restart local v11    # "postParameters":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .restart local v12    # "response":Lorg/apache/http/HttpResponse;
    .restart local v13    # "result":Ljava/lang/String;
    .restart local v15    # "sw":Ljava/io/OutputStreamWriter;
    :cond_9
    :try_start_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;->this$0:Lcom/infraware/common/notice/NoticeNetworkConnect;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/infraware/common/notice/NoticeNetworkConnect;->mResult:I
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto/16 :goto_2

    .line 261
    .end local v2    # "buffer":Ljava/lang/String;
    .end local v4    # "client":Lorg/apache/http/client/HttpClient;
    .end local v7    # "formEntity":Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .end local v8    # "httppost":Lorg/apache/http/client/methods/HttpPost;
    .end local v11    # "postParameters":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .end local v12    # "response":Lorg/apache/http/HttpResponse;
    .end local v13    # "result":Ljava/lang/String;
    .end local v15    # "sw":Ljava/io/OutputStreamWriter;
    .restart local v3    # "client":Lorg/apache/http/client/HttpClient;
    :catchall_2
    move-exception v17

    goto :goto_7

    .end local v3    # "client":Lorg/apache/http/client/HttpClient;
    .end local v9    # "in":Ljava/io/BufferedReader;
    .restart local v2    # "buffer":Ljava/lang/String;
    .restart local v4    # "client":Lorg/apache/http/client/HttpClient;
    .restart local v6    # "file":Ljava/io/File;
    .restart local v7    # "formEntity":Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v8    # "httppost":Lorg/apache/http/client/methods/HttpPost;
    .restart local v10    # "in":Ljava/io/BufferedReader;
    .restart local v11    # "postParameters":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .restart local v12    # "response":Lorg/apache/http/HttpResponse;
    .restart local v13    # "result":Ljava/lang/String;
    .restart local v16    # "sw":Ljava/io/OutputStreamWriter;
    :catchall_3
    move-exception v17

    move-object v3, v4

    .end local v4    # "client":Lorg/apache/http/client/HttpClient;
    .restart local v3    # "client":Lorg/apache/http/client/HttpClient;
    move-object v9, v10

    .end local v10    # "in":Ljava/io/BufferedReader;
    .restart local v9    # "in":Ljava/io/BufferedReader;
    goto :goto_7

    .line 251
    .end local v2    # "buffer":Ljava/lang/String;
    .end local v6    # "file":Ljava/io/File;
    .end local v7    # "formEntity":Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .end local v8    # "httppost":Lorg/apache/http/client/methods/HttpPost;
    .end local v11    # "postParameters":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .end local v12    # "response":Lorg/apache/http/HttpResponse;
    .end local v13    # "result":Ljava/lang/String;
    .end local v16    # "sw":Ljava/io/OutputStreamWriter;
    :catch_2
    move-exception v5

    goto :goto_5

    .end local v3    # "client":Lorg/apache/http/client/HttpClient;
    .end local v9    # "in":Ljava/io/BufferedReader;
    .restart local v2    # "buffer":Ljava/lang/String;
    .restart local v4    # "client":Lorg/apache/http/client/HttpClient;
    .restart local v6    # "file":Ljava/io/File;
    .restart local v7    # "formEntity":Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v8    # "httppost":Lorg/apache/http/client/methods/HttpPost;
    .restart local v10    # "in":Ljava/io/BufferedReader;
    .restart local v11    # "postParameters":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .restart local v12    # "response":Lorg/apache/http/HttpResponse;
    .restart local v13    # "result":Ljava/lang/String;
    .restart local v16    # "sw":Ljava/io/OutputStreamWriter;
    :catch_3
    move-exception v5

    move-object v3, v4

    .end local v4    # "client":Lorg/apache/http/client/HttpClient;
    .restart local v3    # "client":Lorg/apache/http/client/HttpClient;
    move-object v9, v10

    .end local v10    # "in":Ljava/io/BufferedReader;
    .restart local v9    # "in":Ljava/io/BufferedReader;
    goto :goto_5

    .line 235
    .end local v3    # "client":Lorg/apache/http/client/HttpClient;
    .end local v6    # "file":Ljava/io/File;
    .end local v9    # "in":Ljava/io/BufferedReader;
    .end local v16    # "sw":Ljava/io/OutputStreamWriter;
    .restart local v4    # "client":Lorg/apache/http/client/HttpClient;
    .restart local v10    # "in":Ljava/io/BufferedReader;
    .restart local v15    # "sw":Ljava/io/OutputStreamWriter;
    :catchall_4
    move-exception v17

    move-object v9, v10

    .end local v10    # "in":Ljava/io/BufferedReader;
    .restart local v9    # "in":Ljava/io/BufferedReader;
    goto :goto_6

    .end local v9    # "in":Ljava/io/BufferedReader;
    .end local v15    # "sw":Ljava/io/OutputStreamWriter;
    .restart local v6    # "file":Ljava/io/File;
    .restart local v10    # "in":Ljava/io/BufferedReader;
    .restart local v16    # "sw":Ljava/io/OutputStreamWriter;
    :catchall_5
    move-exception v17

    move-object/from16 v15, v16

    .end local v16    # "sw":Ljava/io/OutputStreamWriter;
    .restart local v15    # "sw":Ljava/io/OutputStreamWriter;
    move-object v9, v10

    .end local v10    # "in":Ljava/io/BufferedReader;
    .restart local v9    # "in":Ljava/io/BufferedReader;
    goto :goto_6

    .line 229
    .end local v6    # "file":Ljava/io/File;
    .end local v9    # "in":Ljava/io/BufferedReader;
    .restart local v10    # "in":Ljava/io/BufferedReader;
    :catch_4
    move-exception v5

    move-object v9, v10

    .end local v10    # "in":Ljava/io/BufferedReader;
    .restart local v9    # "in":Ljava/io/BufferedReader;
    goto/16 :goto_4

    .end local v9    # "in":Ljava/io/BufferedReader;
    .end local v15    # "sw":Ljava/io/OutputStreamWriter;
    .restart local v6    # "file":Ljava/io/File;
    .restart local v10    # "in":Ljava/io/BufferedReader;
    .restart local v16    # "sw":Ljava/io/OutputStreamWriter;
    :catch_5
    move-exception v5

    move-object/from16 v15, v16

    .end local v16    # "sw":Ljava/io/OutputStreamWriter;
    .restart local v15    # "sw":Ljava/io/OutputStreamWriter;
    move-object v9, v10

    .end local v10    # "in":Ljava/io/BufferedReader;
    .restart local v9    # "in":Ljava/io/BufferedReader;
    goto/16 :goto_4

    .end local v6    # "file":Ljava/io/File;
    :cond_a
    move-object v3, v4

    .end local v4    # "client":Lorg/apache/http/client/HttpClient;
    .restart local v3    # "client":Lorg/apache/http/client/HttpClient;
    goto/16 :goto_3

    .end local v3    # "client":Lorg/apache/http/client/HttpClient;
    .end local v9    # "in":Ljava/io/BufferedReader;
    .end local v15    # "sw":Ljava/io/OutputStreamWriter;
    .restart local v4    # "client":Lorg/apache/http/client/HttpClient;
    .restart local v6    # "file":Ljava/io/File;
    .restart local v10    # "in":Ljava/io/BufferedReader;
    .restart local v16    # "sw":Ljava/io/OutputStreamWriter;
    :cond_b
    move-object/from16 v15, v16

    .end local v16    # "sw":Ljava/io/OutputStreamWriter;
    .restart local v15    # "sw":Ljava/io/OutputStreamWriter;
    move-object v9, v10

    .end local v10    # "in":Ljava/io/BufferedReader;
    .restart local v9    # "in":Ljava/io/BufferedReader;
    goto/16 :goto_1
.end method
