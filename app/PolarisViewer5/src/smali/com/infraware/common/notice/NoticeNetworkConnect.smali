.class public Lcom/infraware/common/notice/NoticeNetworkConnect;
.super Ljava/lang/Object;
.source "NoticeNetworkConnect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;,
        Lcom/infraware/common/notice/NoticeNetworkConnect$ConntectCheck;
    }
.end annotation


# instance fields
.field private final FILE_NAME:Ljava/lang/String;

.field private mConnect:Z

.field private mDevice_type:Ljava/lang/String;

.field private mFilePath:Ljava/lang/String;

.field private mLanguage:Ljava/lang/String;

.field private mModel:Ljava/lang/String;

.field private mNs:Lcom/infraware/common/notice/SyncNetworkingStep;

.field private mProduct_code:Ljava/lang/String;

.field public mResult:I

.field private mUrl:Ljava/lang/String;

.field private mWorker:Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mWorker:Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;

    .line 27
    iput-boolean v1, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mConnect:Z

    .line 29
    iput-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mNs:Lcom/infraware/common/notice/SyncNetworkingStep;

    .line 31
    iput-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mDevice_type:Ljava/lang/String;

    .line 32
    iput-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mProduct_code:Ljava/lang/String;

    .line 33
    iput-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mModel:Ljava/lang/String;

    .line 34
    iput-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mLanguage:Ljava/lang/String;

    .line 36
    iput-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mUrl:Ljava/lang/String;

    .line 37
    iput-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mFilePath:Ljava/lang/String;

    .line 39
    const-string/jumbo v0, "OfficeNotice.xml"

    iput-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->FILE_NAME:Ljava/lang/String;

    .line 43
    iput v1, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mResult:I

    .line 48
    invoke-direct {p0}, Lcom/infraware/common/notice/NoticeNetworkConnect;->initialize()V

    .line 49
    return-void
.end method

.method static synthetic access$002(Lcom/infraware/common/notice/NoticeNetworkConnect;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/common/notice/NoticeNetworkConnect;
    .param p1, "x1"    # Z

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mConnect:Z

    return p1
.end method

.method static synthetic access$100(Lcom/infraware/common/notice/NoticeNetworkConnect;)Lcom/infraware/common/notice/SyncNetworkingStep;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/common/notice/NoticeNetworkConnect;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mNs:Lcom/infraware/common/notice/SyncNetworkingStep;

    return-object v0
.end method

.method static synthetic access$200(Lcom/infraware/common/notice/NoticeNetworkConnect;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/common/notice/NoticeNetworkConnect;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/infraware/common/notice/NoticeNetworkConnect;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/common/notice/NoticeNetworkConnect;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mUrl:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/infraware/common/notice/NoticeNetworkConnect;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/common/notice/NoticeNetworkConnect;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mDevice_type:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/infraware/common/notice/NoticeNetworkConnect;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/common/notice/NoticeNetworkConnect;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mProduct_code:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/infraware/common/notice/NoticeNetworkConnect;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/common/notice/NoticeNetworkConnect;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mModel:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/infraware/common/notice/NoticeNetworkConnect;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/common/notice/NoticeNetworkConnect;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mLanguage:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/infraware/common/notice/NoticeNetworkConnect;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/common/notice/NoticeNetworkConnect;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mFilePath:Ljava/lang/String;

    return-object v0
.end method

.method private initialize()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 53
    iput-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mWorker:Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;

    .line 54
    iput-boolean v1, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mConnect:Z

    .line 56
    iput-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mDevice_type:Ljava/lang/String;

    .line 57
    iput-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mProduct_code:Ljava/lang/String;

    .line 58
    iput-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mModel:Ljava/lang/String;

    .line 59
    iput-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mLanguage:Ljava/lang/String;

    .line 61
    iput v1, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mResult:I

    .line 62
    return-void
.end method


# virtual methods
.method public Stop()V
    .locals 2

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/infraware/common/notice/NoticeNetworkConnect;->stopCurrentWorker()V

    .line 79
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mNs:Lcom/infraware/common/notice/SyncNetworkingStep;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/common/notice/SyncNetworkingStep;->yourTurn(Z)V

    .line 80
    return-void
.end method

.method public SyncConnectNetworkCheck(Ljava/lang/String;)V
    .locals 2
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 89
    new-instance v0, Lcom/infraware/common/notice/NoticeNetworkConnect$ConntectCheck;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/infraware/common/notice/NoticeNetworkConnect$ConntectCheck;-><init>(Lcom/infraware/common/notice/NoticeNetworkConnect;Ljava/lang/String;)V

    .line 90
    .local v0, "worker":Lcom/infraware/common/notice/NoticeNetworkConnect$ConntectCheck;
    invoke-virtual {v0}, Lcom/infraware/common/notice/NoticeNetworkConnect$ConntectCheck;->run()V

    .line 91
    return-void
.end method

.method public connectNetwork(Ljava/lang/String;)V
    .locals 2
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 158
    new-instance v0, Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;-><init>(Lcom/infraware/common/notice/NoticeNetworkConnect;Ljava/lang/String;)V

    .line 159
    .local v0, "worker":Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;
    invoke-virtual {p0, v0}, Lcom/infraware/common/notice/NoticeNetworkConnect;->setCurrentWorker(Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;)V

    .line 161
    invoke-virtual {v0}, Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;->start()V

    .line 162
    return-void
.end method

.method public getConnectNetwork()Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mConnect:Z

    return v0
.end method

.method public getResult()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mResult:I

    return v0
.end method

.method public declared-synchronized isCurrentWorker()Z
    .locals 1

    .prologue
    .line 145
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mWorker:Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 146
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 145
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isCurrentWorker(Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;)Z
    .locals 1
    .param p1, "worker"    # Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;

    .prologue
    .line 151
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mWorker:Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setCurrentWorker(Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;)V
    .locals 1
    .param p1, "worker"    # Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;

    .prologue
    .line 139
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mWorker:Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mWorker:Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;

    invoke-virtual {v0}, Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;->interrupt()V

    .line 140
    :cond_0
    iput-object p1, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mWorker:Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 141
    monitor-exit p0

    return-void

    .line 139
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setDevice_Type(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mDevice_type:Ljava/lang/String;

    return-void
.end method

.method public setFilePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mFilePath:Ljava/lang/String;

    return-void
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mLanguage:Ljava/lang/String;

    return-void
.end method

.method public setModel(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mModel:Ljava/lang/String;

    return-void
.end method

.method public setProduct_code(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mProduct_code:Ljava/lang/String;

    return-void
.end method

.method public setSyncher(Lcom/infraware/common/notice/SyncNetworkingStep;)V
    .locals 0
    .param p1, "ns"    # Lcom/infraware/common/notice/SyncNetworkingStep;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mNs:Lcom/infraware/common/notice/SyncNetworkingStep;

    .line 85
    return-void
.end method

.method public declared-synchronized stopCurrentWorker()V
    .locals 1

    .prologue
    .line 130
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mWorker:Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mWorker:Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;

    invoke-virtual {v0}, Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;->interrupt()V

    .line 133
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect;->mWorker:Lcom/infraware/common/notice/NoticeNetworkConnect$NoticeWorker;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    :cond_0
    monitor-exit p0

    return-void

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
