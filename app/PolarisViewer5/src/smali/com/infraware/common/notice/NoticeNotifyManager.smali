.class public Lcom/infraware/common/notice/NoticeNotifyManager;
.super Ljava/lang/Object;
.source "NoticeNotifyManager.java"


# static fields
.field private static final DEVICE_TYPE_PHONE:Ljava/lang/String; = "m"

.field public static final EXTRA_HELP_URL:Ljava/lang/String; = "HELP_URL"

.field public static final EXTRA_NOTICE_ID:Ljava/lang/String; = "NOTICE_ID"

.field public static final EXTRA_NOTICE_URL:Ljava/lang/String; = "NOTICE_URL"

.field public static final HELP_URL:Ljava/lang/String; = "com.infraware.polarisviewer5.HELP_URL"

.field public static final NEW_NOTICE_AVAILABLE:Ljava/lang/String; = "com.infraware.polarisviewer5.ACTION_NEW_NOTICE_AVAILABLE"

.field private static final PREFERENCE_NOTICE_FILE_NAME:Ljava/lang/String; = "polaris:preferences_notice"

.field private static final PREFERENCE_RECENT_HELP_URL_KEY:Ljava/lang/String; = "recent_help_url_id"

.field private static final PREFERENCE_RECENT_NOTICE_ID_KEY:Ljava/lang/String; = "recent_notice_id"

.field private static m_hasNewNotice:Z

.field private static m_oItem:Lcom/infraware/common/notice/NoticeItem;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    sput-boolean v0, Lcom/infraware/common/notice/NoticeNotifyManager;->m_hasNewNotice:Z

    .line 27
    const/4 v0, 0x0

    sput-object v0, Lcom/infraware/common/notice/NoticeNotifyManager;->m_oItem:Lcom/infraware/common/notice/NoticeItem;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getHelpUrl(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 38
    if-nez p0, :cond_0

    .line 41
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v1, "polaris:preferences_notice"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "recent_help_url_id"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getNoticeUrl(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_SAMSUNG()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 47
    const-string/jumbo v1, "polaris:preferences_notice"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "recent_help_url_id"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 52
    :cond_0
    :goto_0
    return-object v0

    .line 49
    :cond_1
    sget-object v1, Lcom/infraware/common/notice/NoticeNotifyManager;->m_oItem:Lcom/infraware/common/notice/NoticeItem;

    if-eqz v1, :cond_0

    .line 52
    sget-object v0, Lcom/infraware/common/notice/NoticeNotifyManager;->m_oItem:Lcom/infraware/common/notice/NoticeItem;

    invoke-virtual {v0}, Lcom/infraware/common/notice/NoticeItem;->getNoticeUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static getParamLanguage(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "lang"    # Ljava/lang/String;

    .prologue
    .line 72
    sget-object v0, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 74
    :cond_0
    const-string/jumbo v0, "kr"

    .line 89
    :goto_0
    return-object v0

    .line 76
    :cond_1
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 78
    :cond_2
    const-string/jumbo v0, "en"

    goto :goto_0

    .line 80
    :cond_3
    sget-object v0, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 82
    :cond_4
    const-string/jumbo v0, "jp"

    goto :goto_0

    .line 84
    :cond_5
    sget-object v0, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Ljava/util/Locale;->CHINESE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 86
    :cond_6
    const-string/jumbo v0, "ch"

    goto :goto_0

    .line 89
    :cond_7
    const-string/jumbo v0, "etc"

    goto :goto_0
.end method

.method public static hasNewNotice()Z
    .locals 1

    .prologue
    .line 33
    sget-boolean v0, Lcom/infraware/common/notice/NoticeNotifyManager;->m_hasNewNotice:Z

    return v0
.end method

.method public static setNoticeItem(Landroid/content/Context;Lcom/infraware/common/notice/NoticeItem;)V
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "item"    # Lcom/infraware/common/notice/NoticeItem;

    .prologue
    .line 94
    if-eqz p1, :cond_0

    if-nez p0, :cond_1

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    sput-object p1, Lcom/infraware/common/notice/NoticeNotifyManager;->m_oItem:Lcom/infraware/common/notice/NoticeItem;

    .line 100
    :try_start_0
    const-string/jumbo v9, "polaris:preferences_notice"

    const/4 v10, 0x0

    invoke-virtual {p0, v9, v10}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 101
    .local v2, "oPreference":Landroid/content/SharedPreferences;
    const-string/jumbo v9, "recent_notice_id"

    const-string/jumbo v10, "0"

    invoke-interface {v2, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 102
    .local v6, "strRecent_Notice_Id":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/infraware/common/notice/NoticeItem;->getMainUrl()Ljava/lang/String;

    move-result-object v3

    .line 103
    .local v3, "strHelp_Url":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/infraware/common/notice/NoticeItem;->getLastNoticeId()Ljava/lang/String;

    move-result-object v4

    .line 104
    .local v4, "strNew_Notice_Id":Ljava/lang/String;
    if-eqz v3, :cond_2

    .line 106
    const-string/jumbo v9, "recent_help_url_id"

    const/4 v10, 0x0

    invoke-interface {v2, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 108
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 109
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v9, "recent_help_url_id"

    invoke-interface {v1, v9, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 110
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 113
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_2
    if-eqz v4, :cond_0

    const-string/jumbo v9, ""

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 116
    invoke-virtual {p1}, Lcom/infraware/common/notice/NoticeItem;->getLastNoticeLimitDate()Ljava/lang/String;

    move-result-object v5

    .line 117
    .local v5, "strNotice_Limit_Date":Ljava/lang/String;
    invoke-virtual {v6, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v9

    if-gez v9, :cond_0

    .line 119
    new-instance v8, Ljava/util/Date;

    invoke-direct {v8}, Ljava/util/Date;-><init>()V

    .line 120
    .local v8, "today":Ljava/util/Date;
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v9, "yyyy-MM-dd"

    invoke-direct {v0, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 121
    .local v0, "date":Ljava/text/SimpleDateFormat;
    invoke-virtual {v0, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    .line 122
    .local v7, "strToday":Ljava/lang/String;
    const-string/jumbo v9, ""

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    invoke-virtual {v5, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v9

    if-lez v9, :cond_0

    .line 124
    :cond_3
    const/4 v9, 0x1

    sput-boolean v9, Lcom/infraware/common/notice/NoticeNotifyManager;->m_hasNewNotice:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 128
    .end local v0    # "date":Ljava/text/SimpleDateFormat;
    .end local v2    # "oPreference":Landroid/content/SharedPreferences;
    .end local v3    # "strHelp_Url":Ljava/lang/String;
    .end local v4    # "strNew_Notice_Id":Ljava/lang/String;
    .end local v5    # "strNotice_Limit_Date":Ljava/lang/String;
    .end local v6    # "strRecent_Notice_Id":Ljava/lang/String;
    .end local v7    # "strToday":Ljava/lang/String;
    .end local v8    # "today":Ljava/util/Date;
    :catch_0
    move-exception v9

    goto :goto_0
.end method

.method public static setNoticeRead(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 152
    if-eqz p0, :cond_0

    sget-boolean v2, Lcom/infraware/common/notice/NoticeNotifyManager;->m_hasNewNotice:Z

    if-nez v2, :cond_1

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    const-string/jumbo v2, "polaris:preferences_notice"

    invoke-virtual {p0, v2, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 156
    .local v1, "oPreference":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 157
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v2, "recent_notice_id"

    sget-object v3, Lcom/infraware/common/notice/NoticeNotifyManager;->m_oItem:Lcom/infraware/common/notice/NoticeItem;

    invoke-virtual {v3}, Lcom/infraware/common/notice/NoticeItem;->getLastNoticeId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 158
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 159
    sput-boolean v4, Lcom/infraware/common/notice/NoticeNotifyManager;->m_hasNewNotice:Z

    goto :goto_0
.end method

.method public static setNoticeUrl(Landroid/content/Context;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 57
    .local v2, "model":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget-object v6, v6, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/infraware/common/notice/NoticeNotifyManager;->getParamLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 58
    .local v1, "language":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const v7, 0x7f070322

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0x2d

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, " "

    const-string/jumbo v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 60
    .local v4, "productcode":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "https://usermgr1.polarisoffice.com/dmweb/GoNoticeUrl.aspx?device_type=m&product_code="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "&lang="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "&model="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 63
    .local v5, "strHelp_Url":Ljava/lang/String;
    const-string/jumbo v6, "polaris:preferences_notice"

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 64
    .local v3, "oPreference":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 65
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v6, "recent_help_url_id"

    invoke-interface {v0, v6, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 66
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 67
    return-void
.end method
