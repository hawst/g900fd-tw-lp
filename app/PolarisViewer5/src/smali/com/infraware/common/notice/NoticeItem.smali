.class public Lcom/infraware/common/notice/NoticeItem;
.super Ljava/lang/Object;
.source "NoticeItem.java"


# static fields
.field static final INDEX_OF_NOTICECREATEDATE:I = 0x1

.field static final INDEX_OF_NOTICEID:I = 0x0

.field static final INDEX_OF_NOTICELIMITDATE:I = 0x2

.field static final RANGE_OF_ATTRIBUTE:I = 0x8


# instance fields
.field private lstNoticeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private strArrNotice_Attribute:[Ljava/lang/String;

.field private strFAQUrl:Ljava/lang/String;

.field private strMainUrl:Ljava/lang/String;

.field private strNoticeUrl:Ljava/lang/String;

.field private strUserGuideUrl:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object v0, p0, Lcom/infraware/common/notice/NoticeItem;->strArrNotice_Attribute:[Ljava/lang/String;

    .line 14
    iput-object v0, p0, Lcom/infraware/common/notice/NoticeItem;->strFAQUrl:Ljava/lang/String;

    .line 15
    iput-object v0, p0, Lcom/infraware/common/notice/NoticeItem;->strUserGuideUrl:Ljava/lang/String;

    .line 16
    iput-object v0, p0, Lcom/infraware/common/notice/NoticeItem;->strNoticeUrl:Ljava/lang/String;

    .line 17
    iput-object v0, p0, Lcom/infraware/common/notice/NoticeItem;->strMainUrl:Ljava/lang/String;

    .line 19
    iput-object v0, p0, Lcom/infraware/common/notice/NoticeItem;->lstNoticeList:Ljava/util/ArrayList;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/common/notice/NoticeItem;->lstNoticeList:Ljava/util/ArrayList;

    .line 25
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/common/notice/NoticeItem;->strArrNotice_Attribute:[Ljava/lang/String;

    .line 26
    return-void
.end method


# virtual methods
.method public addNoticeList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "strNoticeId"    # Ljava/lang/String;
    .param p2, "strNoticeCreateDate"    # Ljava/lang/String;
    .param p3, "strNoticeLimitDate"    # Ljava/lang/String;

    .prologue
    .line 93
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/String;

    .line 94
    .local v0, "listItem":[Ljava/lang/String;
    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 95
    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 96
    const/4 v1, 0x2

    aput-object p3, v0, v1

    .line 98
    iget-object v1, p0, Lcom/infraware/common/notice/NoticeItem;->lstNoticeList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    return-void
.end method

.method public getArrNotice_Attribute(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 85
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeItem;->strArrNotice_Attribute:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getFAQUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeItem;->strFAQUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getLastNoticeCreateDate()Ljava/lang/String;
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeItem;->lstNoticeList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 71
    const/4 v0, 0x0

    .line 73
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeItem;->lstNoticeList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/infraware/common/notice/NoticeItem;->lstNoticeList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public getLastNoticeId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeItem;->lstNoticeList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 63
    const/4 v0, 0x0

    .line 65
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeItem;->lstNoticeList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/infraware/common/notice/NoticeItem;->lstNoticeList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public getLastNoticeLimitDate()Ljava/lang/String;
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeItem;->lstNoticeList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 79
    const/4 v0, 0x0

    .line 81
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeItem;->lstNoticeList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/infraware/common/notice/NoticeItem;->lstNoticeList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public getMainUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeItem;->strMainUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getNoticeUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeItem;->strNoticeUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getUserGuideUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeItem;->strUserGuideUrl:Ljava/lang/String;

    return-object v0
.end method

.method public setArrNotice_Attribute(Ljava/lang/String;I)V
    .locals 1
    .param p1, "attribute"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 88
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeItem;->strArrNotice_Attribute:[Ljava/lang/String;

    aput-object p1, v0, p2

    .line 89
    return-void
.end method

.method public setFAQUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "strFAQUrl"    # Ljava/lang/String;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/infraware/common/notice/NoticeItem;->strFAQUrl:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public setMainUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "mainUrl"    # Ljava/lang/String;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/infraware/common/notice/NoticeItem;->strMainUrl:Ljava/lang/String;

    .line 58
    return-void
.end method

.method public setNoticeUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "noticeUrl"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/infraware/common/notice/NoticeItem;->strNoticeUrl:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public setUserGuideUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "strUserGuideUrl"    # Ljava/lang/String;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/infraware/common/notice/NoticeItem;->strUserGuideUrl:Ljava/lang/String;

    .line 42
    return-void
.end method
