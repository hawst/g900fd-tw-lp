.class Lcom/infraware/common/notice/NoticeNetworkConnect$ConntectCheck;
.super Ljava/lang/Object;
.source "NoticeNetworkConnect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/common/notice/NoticeNetworkConnect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ConntectCheck"
.end annotation


# instance fields
.field testUrl:Ljava/lang/String;

.field final synthetic this$0:Lcom/infraware/common/notice/NoticeNetworkConnect;


# direct methods
.method constructor <init>(Lcom/infraware/common/notice/NoticeNetworkConnect;Ljava/lang/String;)V
    .locals 1
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/infraware/common/notice/NoticeNetworkConnect$ConntectCheck;->this$0:Lcom/infraware/common/notice/NoticeNetworkConnect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/common/notice/NoticeNetworkConnect$ConntectCheck;->testUrl:Ljava/lang/String;

    .line 98
    const/4 v0, 0x0

    # setter for: Lcom/infraware/common/notice/NoticeNetworkConnect;->mConnect:Z
    invoke-static {p1, v0}, Lcom/infraware/common/notice/NoticeNetworkConnect;->access$002(Lcom/infraware/common/notice/NoticeNetworkConnect;Z)Z

    .line 99
    iput-object p2, p0, Lcom/infraware/common/notice/NoticeNetworkConnect$ConntectCheck;->testUrl:Ljava/lang/String;

    .line 100
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 106
    :try_start_0
    const-string/jumbo v3, "NoticeNetworkConnect"

    const-string/jumbo v4, "[ConntectCheck] Start!!!!!!!!!!!!"

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    new-instance v2, Ljava/net/URL;

    iget-object v3, p0, Lcom/infraware/common/notice/NoticeNetworkConnect$ConntectCheck;->testUrl:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 108
    .local v2, "url":Ljava/net/URL;
    invoke-virtual {v2}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    .line 109
    .local v0, "connection":Ljava/net/URLConnection;
    const/16 v3, 0x2710

    invoke-virtual {v0, v3}, Ljava/net/URLConnection;->setConnectTimeout(I)V

    .line 110
    invoke-virtual {v0}, Ljava/net/URLConnection;->connect()V

    .line 111
    const-string/jumbo v3, "NoticeNetworkConnect"

    const-string/jumbo v4, "[ConntectCheck] Network Connect Success!!!!!!!!!!"

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    iget-object v3, p0, Lcom/infraware/common/notice/NoticeNetworkConnect$ConntectCheck;->this$0:Lcom/infraware/common/notice/NoticeNetworkConnect;

    const/4 v4, 0x1

    # setter for: Lcom/infraware/common/notice/NoticeNetworkConnect;->mConnect:Z
    invoke-static {v3, v4}, Lcom/infraware/common/notice/NoticeNetworkConnect;->access$002(Lcom/infraware/common/notice/NoticeNetworkConnect;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    .end local v0    # "connection":Ljava/net/URLConnection;
    .end local v2    # "url":Ljava/net/URL;
    :goto_0
    iget-object v3, p0, Lcom/infraware/common/notice/NoticeNetworkConnect$ConntectCheck;->this$0:Lcom/infraware/common/notice/NoticeNetworkConnect;

    # getter for: Lcom/infraware/common/notice/NoticeNetworkConnect;->mNs:Lcom/infraware/common/notice/SyncNetworkingStep;
    invoke-static {v3}, Lcom/infraware/common/notice/NoticeNetworkConnect;->access$100(Lcom/infraware/common/notice/NoticeNetworkConnect;)Lcom/infraware/common/notice/SyncNetworkingStep;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 122
    const-string/jumbo v3, "NoticeNetworkConnect"

    const-string/jumbo v4, "[ConntectCheck] mNs.yourTurn(true);!!!!!!!!!!!"

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v3, p0, Lcom/infraware/common/notice/NoticeNetworkConnect$ConntectCheck;->this$0:Lcom/infraware/common/notice/NoticeNetworkConnect;

    # getter for: Lcom/infraware/common/notice/NoticeNetworkConnect;->mNs:Lcom/infraware/common/notice/SyncNetworkingStep;
    invoke-static {v3}, Lcom/infraware/common/notice/NoticeNetworkConnect;->access$100(Lcom/infraware/common/notice/NoticeNetworkConnect;)Lcom/infraware/common/notice/SyncNetworkingStep;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/infraware/common/notice/SyncNetworkingStep;->yourTurn(Z)V

    .line 125
    :cond_0
    return-void

    .line 114
    :catch_0
    move-exception v1

    .line 116
    .local v1, "e":Ljava/lang/Exception;
    const-string/jumbo v3, "NoticeNetworkConnect"

    const-string/jumbo v4, "[ConntectCheck] Network Error Fail!!!!!!!!!!!"

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    iget-object v3, p0, Lcom/infraware/common/notice/NoticeNetworkConnect$ConntectCheck;->this$0:Lcom/infraware/common/notice/NoticeNetworkConnect;

    const/4 v4, 0x3

    iput v4, v3, Lcom/infraware/common/notice/NoticeNetworkConnect;->mResult:I

    .line 118
    iget-object v3, p0, Lcom/infraware/common/notice/NoticeNetworkConnect$ConntectCheck;->this$0:Lcom/infraware/common/notice/NoticeNetworkConnect;

    const/4 v4, 0x0

    # setter for: Lcom/infraware/common/notice/NoticeNetworkConnect;->mConnect:Z
    invoke-static {v3, v4}, Lcom/infraware/common/notice/NoticeNetworkConnect;->access$002(Lcom/infraware/common/notice/NoticeNetworkConnect;Z)Z

    goto :goto_0
.end method
