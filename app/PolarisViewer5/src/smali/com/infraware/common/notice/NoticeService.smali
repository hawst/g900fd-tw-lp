.class public Lcom/infraware/common/notice/NoticeService;
.super Landroid/app/Service;
.source "NoticeService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/common/notice/NoticeService$XmlParser;
    }
.end annotation


# static fields
.field private static final DEVICE_TYPE:Ljava/lang/String; = "m"

.field private static final DEVICE_TYPE_PHONE:Ljava/lang/String; = "m"

.field private static final DEVICE_TYPE_TABLET:Ljava/lang/String; = "t"

.field private static final EH_CONNECT_CHECK:I = 0x384

.field private static final EH_CONNECT_NETWORK:I = 0x3e8

.field private static final EH_FORM_CLOSE:I = 0x5dc

.field private static final EH_GET_NOTICE_FAIL:I = 0x4b0

.field private static final EH_GET_NOTICE_SUCCESS:I = 0x44c

.field private static final EH_NETWORK_ERROR:I = 0x514

.field private static final EH_NO_NOTICE:I = 0x640

.field private static final EH_NO_RESPONSE_SERVER_ERROR:I = 0x578

.field private static final EH_XML_PARSING_ERROR:I = 0x6a4

.field private static final NOTICE_INFO_URL:Ljava/lang/String; = "https://usermgr1.polarisoffice.com/api/GetNoticeInfo.aspx"


# instance fields
.field private final FILE_NAME:Ljava/lang/String;

.field private context:Landroid/content/Context;

.field private mEventHandler:Landroid/os/Handler;

.field private mTempPath:Ljava/lang/String;

.field private m_bGetNoticeSuccess:Z

.field private m_isDoc2Server:Z

.field m_nFailValue:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 25
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 29
    iput-object v0, p0, Lcom/infraware/common/notice/NoticeService;->context:Landroid/content/Context;

    .line 31
    iput-object v0, p0, Lcom/infraware/common/notice/NoticeService;->mEventHandler:Landroid/os/Handler;

    .line 48
    iput-boolean v1, p0, Lcom/infraware/common/notice/NoticeService;->m_bGetNoticeSuccess:Z

    .line 49
    iput-boolean v1, p0, Lcom/infraware/common/notice/NoticeService;->m_isDoc2Server:Z

    .line 51
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/common/notice/NoticeService;->mTempPath:Ljava/lang/String;

    .line 53
    const-string/jumbo v0, "OfficeNotice.xml"

    iput-object v0, p0, Lcom/infraware/common/notice/NoticeService;->FILE_NAME:Ljava/lang/String;

    .line 55
    iput v1, p0, Lcom/infraware/common/notice/NoticeService;->m_nFailValue:I

    .line 25
    return-void
.end method

.method private SetEventHandler()V
    .locals 1

    .prologue
    .line 82
    new-instance v0, Lcom/infraware/common/notice/NoticeService$1;

    invoke-direct {v0, p0}, Lcom/infraware/common/notice/NoticeService$1;-><init>(Lcom/infraware/common/notice/NoticeService;)V

    iput-object v0, p0, Lcom/infraware/common/notice/NoticeService;->mEventHandler:Landroid/os/Handler;

    .line 209
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/common/notice/NoticeService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/common/notice/NoticeService;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeService;->mEventHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/common/notice/NoticeService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/common/notice/NoticeService;

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/infraware/common/notice/NoticeService;->m_isDoc2Server:Z

    return v0
.end method

.method static synthetic access$102(Lcom/infraware/common/notice/NoticeService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/common/notice/NoticeService;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/infraware/common/notice/NoticeService;->m_isDoc2Server:Z

    return p1
.end method

.method static synthetic access$200(Lcom/infraware/common/notice/NoticeService;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/common/notice/NoticeService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/infraware/common/notice/NoticeService;->getParamLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/common/notice/NoticeService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/common/notice/NoticeService;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeService;->mTempPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Lcom/infraware/common/notice/NoticeService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/common/notice/NoticeService;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/infraware/common/notice/NoticeService;->m_bGetNoticeSuccess:Z

    return p1
.end method

.method static synthetic access$500(Lcom/infraware/common/notice/NoticeService;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/common/notice/NoticeService;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeService;->context:Landroid/content/Context;

    return-object v0
.end method

.method private getParamLanguage(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "lang"    # Ljava/lang/String;

    .prologue
    .line 359
    sget-object v0, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 361
    :cond_0
    const-string/jumbo v0, "kr"

    .line 376
    :goto_0
    return-object v0

    .line 363
    :cond_1
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 365
    :cond_2
    const-string/jumbo v0, "en"

    goto :goto_0

    .line 367
    :cond_3
    sget-object v0, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 369
    :cond_4
    const-string/jumbo v0, "jp"

    goto :goto_0

    .line 371
    :cond_5
    sget-object v0, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Ljava/util/Locale;->CHINESE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 373
    :cond_6
    const-string/jumbo v0, "ch"

    goto :goto_0

    .line 376
    :cond_7
    const-string/jumbo v0, "etc"

    goto :goto_0
.end method


# virtual methods
.method protected makeOfficeDir()V
    .locals 3

    .prologue
    .line 218
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/common/notice/NoticeService;->mTempPath:Ljava/lang/String;

    .line 219
    invoke-virtual {p0}, Lcom/infraware/common/notice/NoticeService;->makeRootDir()V

    .line 220
    sget-object v0, Lcom/infraware/common/define/CMModelDefine;->CUSTOM_TEMP_PATH:Ljava/lang/String;

    const-string/jumbo v1, "polarisTemp"

    sget-object v2, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->TEMP_PATH:Ljava/lang/String;

    invoke-static {v0, v1, v2, p0}, Lcom/infraware/common/util/FileUtils;->createCustomPath(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/common/notice/NoticeService;->mTempPath:Ljava/lang/String;

    .line 221
    return-void
.end method

.method protected makeRootDir()V
    .locals 2

    .prologue
    .line 213
    sget-object v0, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->OFFICE_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 214
    sget-object v0, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->OFFICE_ROOT_PATH:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {v0, v1, p0}, Lcom/infraware/common/util/FileUtils;->makeDirectory(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;

    .line 215
    :cond_0
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 78
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 67
    const-string/jumbo v0, "NoticeService"

    const-string/jumbo v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 69
    iput-object p0, p0, Lcom/infraware/common/notice/NoticeService;->context:Landroid/content/Context;

    .line 70
    invoke-direct {p0}, Lcom/infraware/common/notice/NoticeService;->SetEventHandler()V

    .line 71
    iget-object v0, p0, Lcom/infraware/common/notice/NoticeService;->mEventHandler:Landroid/os/Handler;

    const/16 v1, 0x384

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 72
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 60
    const-string/jumbo v0, "NoticeService"

    const-string/jumbo v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 62
    return-void
.end method
