.class public Lcom/infraware/common/control/custom/ClearableEditText;
.super Landroid/widget/EditText;
.source "ClearableEditText.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/infraware/common/control/custom/TextWatcherAdapter$TextWatcherListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/common/control/custom/ClearableEditText$X_STATE;,
        Lcom/infraware/common/control/custom/ClearableEditText$EDITTEXT_STATE;,
        Lcom/infraware/common/control/custom/ClearableEditText$Listener;
    }
.end annotation


# instance fields
.field private ebva:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

.field private f:Landroid/view/View$OnFocusChangeListener;

.field private k:Landroid/view/View$OnKeyListener;

.field private l:Landroid/view/View$OnTouchListener;

.field private listener:Lcom/infraware/common/control/custom/ClearableEditText$Listener;

.field mCurrentState:I

.field private mDrawableNormalId:I

.field private mDrawablePressedId:I

.field private mEditTextId:I

.field private mNextViewId:I

.field private mStateX:I

.field private nInitPaddingRight:I

.field private xD:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 62
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 40
    iput-object v1, p0, Lcom/infraware/common/control/custom/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    .line 41
    iput-object v1, p0, Lcom/infraware/common/control/custom/ClearableEditText;->listener:Lcom/infraware/common/control/custom/ClearableEditText$Listener;

    .line 42
    iput v2, p0, Lcom/infraware/common/control/custom/ClearableEditText;->nInitPaddingRight:I

    .line 44
    iput v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mEditTextId:I

    .line 45
    iput v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mNextViewId:I

    .line 47
    const v0, 0x7f02027d

    iput v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mDrawableNormalId:I

    .line 48
    const v0, 0x7f02027c

    iput v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mDrawablePressedId:I

    .line 51
    iput v2, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mStateX:I

    .line 53
    iput-object v1, p0, Lcom/infraware/common/control/custom/ClearableEditText;->ebva:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 59
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mCurrentState:I

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 66
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    iput-object v1, p0, Lcom/infraware/common/control/custom/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    .line 41
    iput-object v1, p0, Lcom/infraware/common/control/custom/ClearableEditText;->listener:Lcom/infraware/common/control/custom/ClearableEditText$Listener;

    .line 42
    iput v2, p0, Lcom/infraware/common/control/custom/ClearableEditText;->nInitPaddingRight:I

    .line 44
    iput v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mEditTextId:I

    .line 45
    iput v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mNextViewId:I

    .line 47
    const v0, 0x7f02027d

    iput v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mDrawableNormalId:I

    .line 48
    const v0, 0x7f02027c

    iput v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mDrawablePressedId:I

    .line 51
    iput v2, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mStateX:I

    .line 53
    iput-object v1, p0, Lcom/infraware/common/control/custom/ClearableEditText;->ebva:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 59
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mCurrentState:I

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 70
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    iput-object v1, p0, Lcom/infraware/common/control/custom/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    .line 41
    iput-object v1, p0, Lcom/infraware/common/control/custom/ClearableEditText;->listener:Lcom/infraware/common/control/custom/ClearableEditText$Listener;

    .line 42
    iput v2, p0, Lcom/infraware/common/control/custom/ClearableEditText;->nInitPaddingRight:I

    .line 44
    iput v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mEditTextId:I

    .line 45
    iput v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mNextViewId:I

    .line 47
    const v0, 0x7f02027d

    iput v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mDrawableNormalId:I

    .line 48
    const v0, 0x7f02027c

    iput v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mDrawablePressedId:I

    .line 51
    iput v2, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mStateX:I

    .line 53
    iput-object v1, p0, Lcom/infraware/common/control/custom/ClearableEditText;->ebva:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 59
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mCurrentState:I

    .line 71
    return-void
.end method

.method private excuteButton()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 276
    const-string/jumbo v0, ""

    invoke-virtual {p0, v0}, Lcom/infraware/common/control/custom/ClearableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 277
    invoke-virtual {p0, v1}, Lcom/infraware/common/control/custom/ClearableEditText;->setSoundEffectsEnabled(Z)V

    .line 278
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/infraware/common/control/custom/ClearableEditText;->playSoundEffect(I)V

    .line 279
    iget-object v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->listener:Lcom/infraware/common/control/custom/ClearableEditText$Listener;

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->listener:Lcom/infraware/common/control/custom/ClearableEditText$Listener;

    invoke-interface {v0}, Lcom/infraware/common/control/custom/ClearableEditText$Listener;->didClearText()V

    .line 282
    :cond_0
    invoke-virtual {p0, v1}, Lcom/infraware/common/control/custom/ClearableEditText;->setCursorVisible(Z)V

    .line 283
    return-void
.end method

.method private getStateX()I
    .locals 1

    .prologue
    .line 290
    iget v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mStateX:I

    return v0
.end method

.method private isNotEmpty(Ljava/lang/CharSequence;)Z
    .locals 1
    .param p1, "str"    # Ljava/lang/CharSequence;

    .prologue
    .line 195
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setButtonNormal()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 266
    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mDrawableNormalId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    .line 268
    iget-object v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/infraware/common/control/custom/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lcom/infraware/common/control/custom/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v5, v5, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 269
    iget-object v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    .line 270
    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v5

    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/infraware/common/control/custom/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/4 v4, 0x3

    aget-object v3, v3, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/infraware/common/control/custom/ClearableEditText;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 272
    invoke-direct {p0, v5}, Lcom/infraware/common/control/custom/ClearableEditText;->setStateX(I)V

    .line 273
    return-void
.end method

.method private setButtonPressed()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 256
    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mDrawablePressedId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    .line 258
    iget-object v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/infraware/common/control/custom/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lcom/infraware/common/control/custom/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 259
    iget-object v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    .line 260
    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v3

    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    aget-object v1, v1, v5

    iget-object v2, p0, Lcom/infraware/common/control/custom/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/4 v4, 0x3

    aget-object v3, v3, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/infraware/common/control/custom/ClearableEditText;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 262
    invoke-direct {p0, v5}, Lcom/infraware/common/control/custom/ClearableEditText;->setStateX(I)V

    .line 263
    return-void
.end method

.method private setStateX(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 286
    iput p1, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mStateX:I

    .line 287
    return-void
.end method


# virtual methods
.method public AddParent(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 0
    .param p1, "ebva"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/infraware/common/control/custom/ClearableEditText;->ebva:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 57
    return-void
.end method

.method public init()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 83
    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getId()I

    move-result v0

    iput v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mEditTextId:I

    .line 84
    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x2

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    .line 85
    iget-object v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 86
    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mDrawableNormalId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/infraware/common/control/custom/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lcom/infraware/common/control/custom/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 90
    invoke-virtual {p0, v3}, Lcom/infraware/common/control/custom/ClearableEditText;->setClearIconVisible(Z)V

    .line 91
    invoke-super {p0, p0}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 92
    invoke-super {p0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 93
    invoke-super {p0, p0}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 94
    new-instance v0, Lcom/infraware/common/control/custom/TextWatcherAdapter;

    invoke-direct {v0, p0, p0}, Lcom/infraware/common/control/custom/TextWatcherAdapter;-><init>(Landroid/widget/EditText;Lcom/infraware/common/control/custom/TextWatcherAdapter$TextWatcherListener;)V

    invoke-virtual {p0, v0}, Lcom/infraware/common/control/custom/ClearableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 96
    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getPaddingRight()I

    move-result v0

    iput v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->nInitPaddingRight:I

    .line 97
    iget v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mEditTextId:I

    invoke-virtual {p0, v0}, Lcom/infraware/common/control/custom/ClearableEditText;->setNextFocusRightId(I)V

    .line 98
    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    const/4 v2, 0x1

    .line 151
    const-string/jumbo v0, "shjeong"

    const-string/jumbo v1, "focusChange"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    iget v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mCurrentState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 153
    iput v2, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mCurrentState:I

    .line 154
    iget v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mEditTextId:I

    invoke-virtual {p0, v0}, Lcom/infraware/common/control/custom/ClearableEditText;->setNextFocusRightId(I)V

    .line 155
    invoke-virtual {p0, v2}, Lcom/infraware/common/control/custom/ClearableEditText;->setCursorVisible(Z)V

    .line 158
    :cond_0
    if-eqz p2, :cond_2

    .line 159
    invoke-virtual {p0, v2}, Lcom/infraware/common/control/custom/ClearableEditText;->setCursorVisible(Z)V

    .line 163
    :goto_0
    iget-object v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->f:Landroid/view/View$OnFocusChangeListener;

    if-eqz v0, :cond_1

    .line 164
    iget-object v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->f:Landroid/view/View$OnFocusChangeListener;

    invoke-interface {v0, p1, p2}, Landroid/view/View$OnFocusChangeListener;->onFocusChange(Landroid/view/View;Z)V

    .line 166
    :cond_1
    return-void

    .line 161
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/infraware/common/control/custom/ClearableEditText;->setCursorVisible(Z)V

    goto :goto_0
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "i"    # I
    .param p3, "keyevent"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 200
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_0

    .line 201
    sparse-switch p2, :sswitch_data_0

    .line 248
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/infraware/common/control/custom/ClearableEditText;->k:Landroid/view/View$OnKeyListener;

    if-eqz v2, :cond_1

    .line 249
    iget-object v1, p0, Lcom/infraware/common/control/custom/ClearableEditText;->k:Landroid/view/View$OnKeyListener;

    invoke-interface {v1, p1, p2, p3}, Landroid/view/View$OnKeyListener;->onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 252
    :cond_1
    return v1

    .line 203
    :sswitch_0
    const-string/jumbo v3, "shjeong"

    const-string/jumbo v4, "LEFT"

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    iget v3, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mCurrentState:I

    if-ne v3, v5, :cond_0

    .line 205
    const-string/jumbo v3, "shjeong"

    const-string/jumbo v4, "LEFT Text"

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    invoke-direct {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->setButtonNormal()V

    .line 207
    invoke-virtual {p0, v2}, Lcom/infraware/common/control/custom/ClearableEditText;->setCursorVisible(Z)V

    goto :goto_0

    .line 212
    :sswitch_1
    const-string/jumbo v3, "shjeong"

    const-string/jumbo v4, "RIGHT"

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    iget v3, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mCurrentState:I

    if-ne v3, v2, :cond_2

    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getSelectionEnd()I

    move-result v2

    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->length()I

    move-result v3

    if-ne v2, v3, :cond_2

    .line 214
    const-string/jumbo v2, "shjeong"

    const-string/jumbo v3, "RIGHT Button"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/infraware/common/control/custom/ClearableEditText;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 219
    iput v5, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mCurrentState:I

    .line 220
    invoke-virtual {p0, v1}, Lcom/infraware/common/control/custom/ClearableEditText;->setCursorVisible(Z)V

    .line 221
    invoke-direct {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->setButtonPressed()V

    goto :goto_0

    .line 223
    :cond_2
    iget v2, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mCurrentState:I

    if-ne v2, v5, :cond_0

    .line 224
    const-string/jumbo v2, "shjeong"

    const-string/jumbo v3, "RIGHT Text"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    iget v2, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mNextViewId:I

    invoke-virtual {p0, v2}, Lcom/infraware/common/control/custom/ClearableEditText;->setNextFocusRightId(I)V

    .line 226
    invoke-direct {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->setButtonNormal()V

    goto :goto_0

    .line 231
    :sswitch_2
    const-string/jumbo v3, "shjeong"

    const-string/jumbo v4, "ENTER"

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    invoke-direct {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getStateX()I

    move-result v3

    if-ne v3, v2, :cond_3

    move v0, v2

    .line 234
    .local v0, "isButtonSelected":Z
    :goto_1
    if-ne v0, v2, :cond_0

    .line 235
    invoke-direct {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->excuteButton()V

    .line 237
    iget v3, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mCurrentState:I

    if-ne v3, v5, :cond_0

    .line 238
    iput v2, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mCurrentState:I

    .line 239
    iget v2, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mEditTextId:I

    invoke-virtual {p0, v2}, Lcom/infraware/common/control/custom/ClearableEditText;->setNextFocusRightId(I)V

    .line 240
    invoke-direct {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->setButtonNormal()V

    .line 241
    invoke-virtual {p0, v1}, Lcom/infraware/common/control/custom/ClearableEditText;->setClearIconVisible(Z)V

    goto/16 :goto_0

    .end local v0    # "isButtonSelected":Z
    :cond_3
    move v0, v1

    .line 232
    goto :goto_1

    .line 201
    nop

    :sswitch_data_0
    .sparse-switch
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0x42 -> :sswitch_2
    .end sparse-switch
.end method

.method public onTextChanged(Landroid/widget/EditText;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/widget/EditText;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 170
    const-string/jumbo v0, "shjeong"

    const-string/jumbo v1, "TextChange"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    invoke-direct {p0, p2}, Lcom/infraware/common/control/custom/ClearableEditText;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/infraware/common/control/custom/ClearableEditText;->setClearIconVisible(Z)V

    .line 172
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 122
    iget-object v3, p0, Lcom/infraware/common/control/custom/ClearableEditText;->ebva:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v3, :cond_0

    .line 123
    iget-object v3, p0, Lcom/infraware/common/control/custom/ClearableEditText;->ebva:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->clearDictionaryPanel()Z

    .line 125
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/4 v4, 0x2

    aget-object v3, v3, v4

    if-eqz v3, :cond_4

    .line 126
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getPaddingRight()I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/infraware/common/control/custom/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_2

    move v0, v1

    .line 128
    .local v0, "tappedX":Z
    :goto_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-ne v3, v1, :cond_3

    .line 129
    invoke-direct {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->setButtonNormal()V

    .line 130
    invoke-virtual {p0, v1}, Lcom/infraware/common/control/custom/ClearableEditText;->setCursorVisible(Z)V

    .line 131
    if-eqz v0, :cond_4

    .line 132
    invoke-direct {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->excuteButton()V

    .line 146
    .end local v0    # "tappedX":Z
    :cond_1
    :goto_1
    return v2

    :cond_2
    move v0, v2

    .line 126
    goto :goto_0

    .line 136
    .restart local v0    # "tappedX":Z
    :cond_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_4

    .line 137
    if-eqz v0, :cond_4

    .line 138
    invoke-direct {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->setButtonPressed()V

    goto :goto_1

    .line 143
    .end local v0    # "tappedX":Z
    :cond_4
    iget-object v1, p0, Lcom/infraware/common/control/custom/ClearableEditText;->l:Landroid/view/View$OnTouchListener;

    if-eqz v1, :cond_1

    .line 144
    iget-object v1, p0, Lcom/infraware/common/control/custom/ClearableEditText;->l:Landroid/view/View$OnTouchListener;

    invoke-interface {v1, p1, p2}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v2

    goto :goto_1
.end method

.method protected setClearIconVisible(Z)V
    .locals 7
    .param p1, "visible"    # Z

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 175
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/infraware/common/control/custom/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    .line 177
    .local v0, "x":Landroid/graphics/drawable/Drawable;
    :goto_0
    if-ne p1, v6, :cond_1

    .line 178
    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p0, v1, v2, v5, v3}, Lcom/infraware/common/control/custom/ClearableEditText;->setPadding(IIII)V

    .line 190
    :goto_1
    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    aget-object v1, v1, v5

    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aget-object v2, v2, v6

    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/4 v4, 0x3

    aget-object v3, v3, v4

    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/infraware/common/control/custom/ClearableEditText;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 192
    return-void

    .line 175
    .end local v0    # "x":Landroid/graphics/drawable/Drawable;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 184
    .restart local v0    # "x":Landroid/graphics/drawable/Drawable;
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getPaddingTop()I

    move-result v2

    iget v3, p0, Lcom/infraware/common/control/custom/ClearableEditText;->nInitPaddingRight:I

    invoke-virtual {p0}, Lcom/infraware/common/control/custom/ClearableEditText;->getPaddingBottom()I

    move-result v4

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/infraware/common/control/custom/ClearableEditText;->setPadding(IIII)V

    goto :goto_1
.end method

.method public setDrawableId(II)V
    .locals 0
    .param p1, "normal"    # I
    .param p2, "pressed"    # I

    .prologue
    .line 78
    iput p1, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mDrawableNormalId:I

    .line 79
    iput p2, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mDrawablePressedId:I

    .line 80
    return-void
.end method

.method public setListener(Lcom/infraware/common/control/custom/ClearableEditText$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/infraware/common/control/custom/ClearableEditText$Listener;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/infraware/common/control/custom/ClearableEditText;->listener:Lcom/infraware/common/control/custom/ClearableEditText$Listener;

    .line 28
    return-void
.end method

.method public setNextViewId(I)V
    .locals 0
    .param p1, "nextViewId"    # I

    .prologue
    .line 74
    iput p1, p0, Lcom/infraware/common/control/custom/ClearableEditText;->mNextViewId:I

    .line 75
    return-void
.end method

.method public setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    .locals 0
    .param p1, "f"    # Landroid/view/View$OnFocusChangeListener;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/infraware/common/control/custom/ClearableEditText;->f:Landroid/view/View$OnFocusChangeListener;

    .line 108
    return-void
.end method

.method public setOnKeyListener(Landroid/view/View$OnKeyListener;)V
    .locals 0
    .param p1, "k"    # Landroid/view/View$OnKeyListener;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/infraware/common/control/custom/ClearableEditText;->k:Landroid/view/View$OnKeyListener;

    .line 113
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 0
    .param p1, "l"    # Landroid/view/View$OnTouchListener;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/infraware/common/control/custom/ClearableEditText;->l:Landroid/view/View$OnTouchListener;

    .line 103
    return-void
.end method
