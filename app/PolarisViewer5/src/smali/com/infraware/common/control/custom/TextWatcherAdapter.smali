.class public Lcom/infraware/common/control/custom/TextWatcherAdapter;
.super Ljava/lang/Object;
.source "TextWatcherAdapter.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/common/control/custom/TextWatcherAdapter$TextWatcherListener;
    }
.end annotation


# instance fields
.field private final listener:Lcom/infraware/common/control/custom/TextWatcherAdapter$TextWatcherListener;

.field private final view:Landroid/widget/EditText;


# direct methods
.method public constructor <init>(Landroid/widget/EditText;Lcom/infraware/common/control/custom/TextWatcherAdapter$TextWatcherListener;)V
    .locals 0
    .param p1, "editText"    # Landroid/widget/EditText;
    .param p2, "listener"    # Lcom/infraware/common/control/custom/TextWatcherAdapter$TextWatcherListener;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/infraware/common/control/custom/TextWatcherAdapter;->view:Landroid/widget/EditText;

    .line 20
    iput-object p2, p0, Lcom/infraware/common/control/custom/TextWatcherAdapter;->listener:Lcom/infraware/common/control/custom/TextWatcherAdapter$TextWatcherListener;

    .line 21
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 37
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 32
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 25
    iget-object v0, p0, Lcom/infraware/common/control/custom/TextWatcherAdapter;->listener:Lcom/infraware/common/control/custom/TextWatcherAdapter$TextWatcherListener;

    iget-object v1, p0, Lcom/infraware/common/control/custom/TextWatcherAdapter;->view:Landroid/widget/EditText;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/infraware/common/control/custom/TextWatcherAdapter$TextWatcherListener;->onTextChanged(Landroid/widget/EditText;Ljava/lang/String;)V

    .line 26
    return-void
.end method
