.class public Lcom/infraware/common/control/dragndroplist/DragNDropListView;
.super Landroid/widget/ListView;
.source "DragNDropListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/common/control/dragndroplist/DragNDropListView$DragNDropEventType;
    }
.end annotation


# static fields
.field private static FASTSPEED:I

.field private static SLOWSPEED:I


# instance fields
.field private final LOG_CAT:Ljava/lang/String;

.field mDragListener:Lcom/infraware/common/control/dragndroplist/DragListener;

.field private mDragMode:Z

.field private mDragPointOffset:I

.field mDragView:Landroid/widget/ImageView;

.field mDropListener:Lcom/infraware/common/control/dragndroplist/DropListener;

.field private mEndPosition:I

.field mGestureDetector:Landroid/view/GestureDetector;

.field private mLowerBound:I

.field mRemoveListener:Lcom/infraware/polarisoffice5/common/RemoveListener;

.field private mStartPosition:I

.field private mUpperBound:I

.field private prevY:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const/16 v0, 0x20

    sput v0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->FASTSPEED:I

    .line 36
    const/16 v0, 0x8

    sput v0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->SLOWSPEED:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 46
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    const-string/jumbo v0, "DragNDropListView"

    iput-object v0, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->LOG_CAT:Ljava/lang/String;

    .line 28
    iput-boolean v1, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragMode:Z

    .line 29
    iput v1, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mStartPosition:I

    .line 30
    iput v1, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mEndPosition:I

    .line 31
    iput v1, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragPointOffset:I

    .line 32
    iput v1, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mUpperBound:I

    .line 33
    iput v1, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mLowerBound:I

    .line 34
    iput v1, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->prevY:I

    .line 47
    return-void
.end method

.method private adjustScrollBounds(I)V
    .locals 2
    .param p1, "y"    # I

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->getHeight()I

    move-result v0

    .line 183
    .local v0, "nHeight":I
    div-int/lit8 v1, v0, 0x3

    if-lt p1, v1, :cond_0

    .line 184
    div-int/lit8 v1, v0, 0x5

    iput v1, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mUpperBound:I

    .line 186
    :cond_0
    mul-int/lit8 v1, v0, 0x2

    div-int/lit8 v1, v1, 0x3

    if-gt p1, v1, :cond_1

    .line 187
    mul-int/lit8 v1, v0, 0x4

    div-int/lit8 v1, v1, 0x5

    iput v1, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mLowerBound:I

    .line 190
    :cond_1
    return-void
.end method

.method private drag(II)V
    .locals 10
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v9, 0x0

    .line 137
    iget-object v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragView:Landroid/widget/ImageView;

    if-nez v7, :cond_1

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 139
    :cond_1
    iget-object v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager$LayoutParams;

    .line 140
    .local v0, "layoutParams":Landroid/view/WindowManager$LayoutParams;
    iput v9, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 141
    iget v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragPointOffset:I

    sub-int v7, p2, v7

    iput v7, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 142
    invoke-virtual {p0}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->getContext()Landroid/content/Context;

    move-result-object v7

    const-string/jumbo v8, "window"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 143
    .local v1, "mWindowManager":Landroid/view/WindowManager;
    iget-object v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragView:Landroid/widget/ImageView;

    invoke-interface {v1, v7, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 145
    iget-object v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragListener:Lcom/infraware/common/control/dragndroplist/DragListener;

    if-eqz v7, :cond_0

    .line 147
    iget-object v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragListener:Lcom/infraware/common/control/dragndroplist/DragListener;

    const/4 v8, 0x0

    invoke-interface {v7, p1, p2, v8}, Lcom/infraware/common/control/dragndroplist/DragListener;->onDrag(IILandroid/widget/ListView;)V

    .line 150
    const/4 v5, 0x0

    .line 151
    .local v5, "speed":I
    invoke-virtual {p0}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->getHeight()I

    move-result v2

    .line 152
    .local v2, "nHeight":I
    invoke-direct {p0, p2}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->adjustScrollBounds(I)V

    .line 153
    iget v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mLowerBound:I

    if-lt p2, v7, :cond_6

    iget v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->prevY:I

    if-le p2, v7, :cond_6

    .line 154
    iget v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mLowerBound:I

    add-int/2addr v7, v2

    div-int/lit8 v7, v7, 0x2

    if-le p2, v7, :cond_5

    sget v5, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->SLOWSPEED:I

    .line 160
    :cond_2
    :goto_1
    if-eqz v5, :cond_4

    .line 161
    div-int/lit8 v7, v2, 0x2

    invoke-virtual {p0, v9, v7}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->pointToPosition(II)I

    move-result v4

    .line 162
    .local v4, "ref":I
    const/4 v7, -0x1

    if-ne v4, v7, :cond_3

    .line 163
    div-int/lit8 v7, v2, 0x2

    invoke-virtual {p0}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->getDividerHeight()I

    move-result v8

    add-int/2addr v7, v8

    add-int/lit8 v7, v7, 0x40

    invoke-virtual {p0, v9, v7}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->pointToPosition(II)I

    move-result v4

    .line 166
    :cond_3
    invoke-virtual {p0}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->getFirstVisiblePosition()I

    move-result v7

    sub-int v7, v4, v7

    invoke-virtual {p0, v7}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 167
    .local v6, "v":Landroid/view/View;
    const-string/jumbo v7, "DragNDropListView"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "DRAG! - ref:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    if-eqz v6, :cond_4

    .line 170
    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v3

    .line 171
    .local v3, "pos":I
    const-string/jumbo v7, "DragNDropListView"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "DRAG! - pos:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "pos-speed"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sub-int v9, v3, v5

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    sub-int v7, v3, v5

    invoke-virtual {p0, v4, v7}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->setSelectionFromTop(II)V

    .line 173
    invoke-virtual {p0, v6}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->getPositionForView(Landroid/view/View;)I

    .line 176
    .end local v3    # "pos":I
    .end local v4    # "ref":I
    .end local v6    # "v":Landroid/view/View;
    :cond_4
    iput p2, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->prevY:I

    goto/16 :goto_0

    .line 154
    :cond_5
    sget v5, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->FASTSPEED:I

    goto :goto_1

    .line 156
    :cond_6
    iget v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mUpperBound:I

    if-gt p2, v7, :cond_2

    iget v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->prevY:I

    if-ge p2, v7, :cond_2

    .line 157
    iget v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mUpperBound:I

    div-int/lit8 v7, v7, 0x2

    if-ge p2, v7, :cond_7

    sget v7, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->FASTSPEED:I

    neg-int v5, v7

    :goto_2
    goto/16 :goto_1

    :cond_7
    sget v7, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->SLOWSPEED:I

    neg-int v5, v7

    goto :goto_2
.end method

.method private startDrag(III)V
    .locals 11
    .param p1, "nItemCurIndex"    # I
    .param p2, "itemIndex"    # I
    .param p3, "y"    # I

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, -0x2

    .line 193
    invoke-direct {p0, p2}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->stopDrag(I)V

    .line 195
    invoke-virtual {p0, p2}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 196
    .local v2, "item":Landroid/view/View;
    if-nez v2, :cond_0

    .line 225
    :goto_0
    return-void

    .line 197
    :cond_0
    invoke-virtual {v2, v10}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 198
    iget-object v6, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragListener:Lcom/infraware/common/control/dragndroplist/DragListener;

    if-eqz v6, :cond_1

    .line 199
    iget-object v6, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragListener:Lcom/infraware/common/control/dragndroplist/DragListener;

    iget-boolean v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragMode:Z

    invoke-interface {v6, v2, p1, p2, v7}, Lcom/infraware/common/control/dragndroplist/DragListener;->onStartDrag(Landroid/view/View;IIZ)V

    .line 201
    :cond_1
    invoke-virtual {v2, v10}, Landroid/view/View;->getDrawingCache(Z)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-static {v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 203
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v4, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v4}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 204
    .local v4, "mWindowParams":Landroid/view/WindowManager$LayoutParams;
    const/16 v6, 0x33

    iput v6, v4, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 205
    iput v9, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 206
    iget v6, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragPointOffset:I

    sub-int v6, p3, v6

    iput v6, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 208
    iput v8, v4, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 209
    iput v8, v4, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 210
    const/16 v6, 0x398

    iput v6, v4, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 215
    iput v8, v4, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 216
    iput v9, v4, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 218
    invoke-virtual {p0}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 219
    .local v1, "context":Landroid/content/Context;
    new-instance v5, Landroid/widget/ImageView;

    invoke-direct {v5, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 220
    .local v5, "v":Landroid/widget/ImageView;
    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 221
    const/16 v6, 0xa0

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 222
    const-string/jumbo v6, "window"

    invoke-virtual {v1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    .line 223
    .local v3, "mWindowManager":Landroid/view/WindowManager;
    invoke-interface {v3, v5, v4}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 224
    iput-object v5, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragView:Landroid/widget/ImageView;

    goto :goto_0
.end method

.method private stopDrag(I)V
    .locals 4
    .param p1, "itemIndex"    # I

    .prologue
    const/4 v3, 0x0

    .line 228
    iget-object v1, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragView:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    .line 229
    iget-object v1, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragListener:Lcom/infraware/common/control/dragndroplist/DragListener;

    if-eqz v1, :cond_0

    .line 230
    iget-object v1, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragListener:Lcom/infraware/common/control/dragndroplist/DragListener;

    invoke-virtual {p0, p1}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/infraware/common/control/dragndroplist/DragListener;->onStopDrag(Landroid/view/View;)V

    .line 231
    :cond_0
    iget-object v1, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragView:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 232
    invoke-virtual {p0}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 233
    .local v0, "wm":Landroid/view/WindowManager;
    iget-object v1, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragView:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 234
    iget-object v1, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 235
    iput-object v3, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragView:Landroid/widget/ImageView;

    .line 237
    .end local v0    # "wm":Landroid/view/WindowManager;
    :cond_1
    return-void
.end method


# virtual methods
.method public StopDrag()V
    .locals 2

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragMode:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 85
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragMode:Z

    .line 86
    iget v0, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mStartPosition:I

    invoke-virtual {p0}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->getFirstVisiblePosition()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->stopDrag(I)V

    .line 88
    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 243
    invoke-super {p0, p1}, Landroid/widget/ListView;->onDraw(Landroid/graphics/Canvas;)V

    .line 244
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 3
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "oldl"    # I
    .param p4, "oldt"    # I

    .prologue
    .line 249
    const-string/jumbo v0, "DragNDropListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "OnScrollChanged"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ListView;->onScrollChanged(IIII)V

    .line 251
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x1

    const/4 v9, -0x1

    .line 92
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 93
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-int v4, v7

    .line 94
    .local v4, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    float-to-int v5, v7

    .line 96
    .local v5, "y":I
    if-nez v0, :cond_0

    .line 97
    invoke-virtual {p0}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->getWidth()I

    move-result v7

    mul-int/lit8 v7, v7, 0x4

    div-int/lit8 v7, v7, 0x5

    if-le v4, v7, :cond_0

    .line 98
    iput-boolean v6, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragMode:Z

    .line 101
    :cond_0
    iget-boolean v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragMode:Z

    if-nez v7, :cond_2

    .line 102
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v6

    .line 131
    :cond_1
    :goto_0
    return v6

    .line 104
    :cond_2
    packed-switch v0, :pswitch_data_0

    .line 121
    :pswitch_0
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragMode:Z

    .line 122
    invoke-virtual {p0, v4, v5}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->touchUpPointToPosition(II)I

    move-result v7

    iput v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mEndPosition:I

    .line 123
    iget v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mStartPosition:I

    invoke-virtual {p0}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->getFirstVisiblePosition()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-direct {p0, v7}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->stopDrag(I)V

    .line 125
    invoke-virtual {p0, v4, v5}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->pointToRowId(II)J

    move-result-wide v2

    .line 126
    .local v2, "rowid":J
    iget-object v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDropListener:Lcom/infraware/common/control/dragndroplist/DropListener;

    if-eqz v7, :cond_1

    iget v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mStartPosition:I

    if-eq v7, v9, :cond_1

    iget v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mEndPosition:I

    if-eq v7, v9, :cond_1

    const-wide/high16 v7, -0x8000000000000000L

    cmp-long v7, v2, v7

    if-eqz v7, :cond_1

    const-wide/16 v7, -0x1

    cmp-long v7, v2, v7

    if-eqz v7, :cond_1

    .line 128
    iget-object v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDropListener:Lcom/infraware/common/control/dragndroplist/DropListener;

    iget v8, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mStartPosition:I

    iget v9, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mEndPosition:I

    invoke-interface {v7, v8, v9}, Lcom/infraware/common/control/dragndroplist/DropListener;->onDrop(II)V

    goto :goto_0

    .line 106
    .end local v2    # "rowid":J
    :pswitch_1
    invoke-virtual {p0, v4, v5}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->pointToPosition(II)I

    move-result v7

    iput v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mStartPosition:I

    .line 107
    iget v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mStartPosition:I

    if-eq v7, v9, :cond_1

    .line 108
    iget v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mStartPosition:I

    invoke-virtual {p0}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->getFirstVisiblePosition()I

    move-result v8

    sub-int v1, v7, v8

    .line 109
    .local v1, "mItemPosition":I
    invoke-virtual {p0, v1}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v7

    sub-int v7, v5, v7

    iput v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragPointOffset:I

    .line 110
    iget v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragPointOffset:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v8

    float-to-int v8, v8

    sub-int/2addr v8, v5

    sub-int/2addr v7, v8

    iput v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragPointOffset:I

    .line 111
    iget v7, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mStartPosition:I

    invoke-direct {p0, v7, v1, v5}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->startDrag(III)V

    .line 112
    invoke-direct {p0, v4, v5}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->drag(II)V

    goto :goto_0

    .line 116
    .end local v1    # "mItemPosition":I
    :pswitch_2
    invoke-direct {p0, v4, v5}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->drag(II)V

    goto :goto_0

    .line 104
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public setDragListener(Lcom/infraware/common/control/dragndroplist/DragListener;)V
    .locals 0
    .param p1, "l"    # Lcom/infraware/common/control/dragndroplist/DragListener;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDragListener:Lcom/infraware/common/control/dragndroplist/DragListener;

    .line 59
    return-void
.end method

.method public setDropListener(Lcom/infraware/common/control/dragndroplist/DropListener;)V
    .locals 0
    .param p1, "l"    # Lcom/infraware/common/control/dragndroplist/DropListener;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mDropListener:Lcom/infraware/common/control/dragndroplist/DropListener;

    .line 51
    return-void
.end method

.method public setRemoveListener(Lcom/infraware/polarisoffice5/common/RemoveListener;)V
    .locals 0
    .param p1, "l"    # Lcom/infraware/polarisoffice5/common/RemoveListener;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->mRemoveListener:Lcom/infraware/polarisoffice5/common/RemoveListener;

    .line 55
    return-void
.end method

.method public touchUpPointToPosition(II)I
    .locals 8
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 62
    invoke-virtual {p0, p1, p2}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->pointToPosition(II)I

    move-result v3

    .line 63
    .local v3, "position":I
    invoke-virtual {p0, p1, p2}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->pointToRowId(II)J

    move-result-wide v4

    .line 64
    .local v4, "rowid":J
    const/4 v6, -0x1

    if-eq v3, v6, :cond_0

    const-wide/high16 v6, -0x8000000000000000L

    cmp-long v6, v4, v6

    if-eqz v6, :cond_0

    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-nez v6, :cond_1

    .line 66
    :cond_0
    :try_start_0
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 67
    .local v1, "frame":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->getLastVisiblePosition()I

    move-result v2

    .line 68
    .local v2, "lastVisiblePos":I
    invoke-virtual {p0, v2}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 69
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0, v1}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 70
    invoke-virtual {p0}, Lcom/infraware/common/control/dragndroplist/DragNDropListView;->getBottom()I

    move-result v6

    iput v6, v1, Landroid/graphics/Rect;->bottom:I

    .line 71
    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    if-eqz v6, :cond_1

    .line 72
    move v3, v2

    .line 80
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "frame":Landroid/graphics/Rect;
    .end local v2    # "lastVisiblePos":I
    :cond_1
    :goto_0
    return v3

    .line 76
    :catchall_0
    move-exception v6

    throw v6

    .line 74
    :catch_0
    move-exception v6

    goto :goto_0
.end method
