.class public Lcom/infraware/common/control/custom/DeleteImageButton;
.super Landroid/widget/ImageButton;
.source "DeleteImageButton.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/infraware/common/control/custom/TextWatcherAdapter$TextWatcherListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/common/control/custom/DeleteImageButton$Listener;
    }
.end annotation


# instance fields
.field private listener:Lcom/infraware/common/control/custom/DeleteImageButton$Listener;

.field private mEditText:Landroid/widget/EditText;

.field private nInitPaddingRight:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 24
    iput-object v0, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->listener:Lcom/infraware/common/control/custom/DeleteImageButton$Listener;

    .line 25
    iput-object v0, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->mEditText:Landroid/widget/EditText;

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->nInitPaddingRight:I

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    iput-object v0, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->listener:Lcom/infraware/common/control/custom/DeleteImageButton$Listener;

    .line 25
    iput-object v0, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->mEditText:Landroid/widget/EditText;

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->nInitPaddingRight:I

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    iput-object v0, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->listener:Lcom/infraware/common/control/custom/DeleteImageButton$Listener;

    .line 25
    iput-object v0, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->mEditText:Landroid/widget/EditText;

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->nInitPaddingRight:I

    .line 38
    return-void
.end method

.method private excuteButton()V
    .locals 4

    .prologue
    .line 48
    iget-object v2, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->mEditText:Landroid/widget/EditText;

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 50
    iget-object v2, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->listener:Lcom/infraware/common/control/custom/DeleteImageButton$Listener;

    if-eqz v2, :cond_0

    .line 51
    iget-object v2, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->listener:Lcom/infraware/common/control/custom/DeleteImageButton$Listener;

    invoke-interface {v2}, Lcom/infraware/common/control/custom/DeleteImageButton$Listener;->didClearText()V

    .line 54
    :cond_0
    iget-object v2, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    .line 55
    invoke-virtual {p0}, Lcom/infraware/common/control/custom/DeleteImageButton;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 56
    .local v0, "context":Landroid/content/Context;
    instance-of v2, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v2, :cond_1

    move-object v1, v0

    .line 57
    check-cast v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 58
    .local v1, "oActivity":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->hideDictionaryPanel()V

    .line 59
    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ShowIme()V

    .line 61
    .end local v1    # "oActivity":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    :cond_1
    instance-of v2, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    if-eqz v2, :cond_2

    move-object v1, v0

    .line 62
    check-cast v1, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .line 63
    .local v1, "oActivity":Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->hideDictionaryPanel()V

    .line 64
    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->ShowIme()V

    .line 66
    .end local v1    # "oActivity":Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    :cond_2
    return-void
.end method

.method private isNotEmpty(Ljava/lang/CharSequence;)Z
    .locals 1
    .param p1, "str"    # Ljava/lang/CharSequence;

    .prologue
    .line 79
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public init(Landroid/widget/EditText;)V
    .locals 3
    .param p1, "editText"    # Landroid/widget/EditText;

    .prologue
    .line 41
    invoke-super {p0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 42
    iput-object p1, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->mEditText:Landroid/widget/EditText;

    .line 43
    iget-object v0, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->mEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/infraware/common/control/custom/TextWatcherAdapter;

    iget-object v2, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->mEditText:Landroid/widget/EditText;

    invoke-direct {v1, v2, p0}, Lcom/infraware/common/control/custom/TextWatcherAdapter;-><init>(Landroid/widget/EditText;Lcom/infraware/common/control/custom/TextWatcherAdapter$TextWatcherListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 44
    iget-object v0, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getPaddingRight()I

    move-result v0

    iput v0, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->nInitPaddingRight:I

    .line 45
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/infraware/common/control/custom/DeleteImageButton;->excuteButton()V

    .line 71
    return-void
.end method

.method public onTextChanged(Landroid/widget/EditText;Ljava/lang/String;)V
    .locals 1
    .param p1, "view"    # Landroid/widget/EditText;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 75
    invoke-direct {p0, p2}, Lcom/infraware/common/control/custom/DeleteImageButton;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/infraware/common/control/custom/DeleteImageButton;->setClearIconVisible(Z)V

    .line 76
    return-void
.end method

.method protected setClearIconVisible(Z)V
    .locals 5
    .param p1, "visible"    # Z

    .prologue
    const/4 v1, 0x0

    .line 83
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 84
    invoke-virtual {p0, v1}, Lcom/infraware/common/control/custom/DeleteImageButton;->setVisibility(I)V

    .line 85
    invoke-virtual {p0, v1, v1}, Lcom/infraware/common/control/custom/DeleteImageButton;->measure(II)V

    .line 86
    iget-object v0, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->mEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/infraware/common/control/custom/DeleteImageButton;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/EditText;->setPadding(IIII)V

    .line 94
    :goto_0
    return-void

    .line 90
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/infraware/common/control/custom/DeleteImageButton;->setVisibility(I)V

    .line 91
    iget-object v0, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->mEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getPaddingTop()I

    move-result v2

    iget v3, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->nInitPaddingRight:I

    iget-object v4, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/EditText;->setPadding(IIII)V

    goto :goto_0
.end method

.method public setListener(Lcom/infraware/common/control/custom/DeleteImageButton$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/infraware/common/control/custom/DeleteImageButton$Listener;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/infraware/common/control/custom/DeleteImageButton;->listener:Lcom/infraware/common/control/custom/DeleteImageButton$Listener;

    .line 22
    return-void
.end method
