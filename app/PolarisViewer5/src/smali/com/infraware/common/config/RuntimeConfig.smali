.class public Lcom/infraware/common/config/RuntimeConfig;
.super Ljava/lang/Object;
.source "RuntimeConfig.java"


# static fields
.field private static final PREFERENCE_AUTO_FIT:Ljava/lang/String; = "key_auto_fit"

.field private static final PREFERENCE_DEFAULT_FORMAT:Ljava/lang/String; = "key_default_format"

.field private static final PREFERENCE_DICTIONARY_EN:Ljava/lang/String; = "key_dictionay_en"

.field private static final PREFERENCE_DICTIONARY_JA:Ljava/lang/String; = "key_dictionay_ja"

.field private static final PREFERENCE_DICTIONARY_KO:Ljava/lang/String; = "key_dictionay_ko"

.field private static final PREFERENCE_DICTIONARY_ZH:Ljava/lang/String; = "key_dictionay_zh"

.field private static final PREFERENCE_FILE_NAME:Ljava/lang/String; = "polaris:preferences"

.field private static final PREFERENCE_HAND_ANIMATION:Ljava/lang/String; = "key_hand_animation"

.field private static final PREFERENCE_IS_BACKUP:Ljava/lang/String; = "key_is_backup"

.field private static final PREFERENCE_RECENT_SHAPE:Ljava/lang/String; = "key_recent_shape"

.field private static final PREFERENCE_RECENT_SHAPE_2003:Ljava/lang/String; = "key_recent_shape_2003"

.field private static final PREFERENCE_RECENT_SHAPE_2003_PPT:Ljava/lang/String; = "key_recent_shape_2003_ppt"

.field private static final PREFERENCE_RECENT_SHAPE_PPT:Ljava/lang/String; = "key_recent_shape_ppt"

.field private static final PREFERENCE_RECENT_SYMBOL:Ljava/lang/String; = "key_recent_symbol"

.field private static final PREFERENCE_SHOWPOPUPNETWORK_COPYRIGHTS:Ljava/lang/String; = "key_show_network_copyrights"

.field private static final PREFERENCE_SHOWPOPUPNETWORK_EULA:Ljava/lang/String; = "key_show_network_eula"

.field public static final PREFERENCE_SHOWPOPUPNETWORK_GOOLER_SERACH:Ljava/lang/String; = "key_show_network_rungoolesearch"

.field public static final PREFERENCE_SHOWPOPUPNETWORK_INFRA_HOME:Ljava/lang/String; = "key_show_network_infra_home"

.field private static final PREFERENCE_SHOWPOPUPNETWORK_MAIN_HELP:Ljava/lang/String; = "key_show_network_main_help"

.field public static final PREFERENCE_SHOWPOPUPNETWORK_OFFICE_DOWNLOAD_SAMSUNG_APPS:Ljava/lang/String; = "key_show_network_office_download_samung_apps"

.field public static final PREFERENCE_SHOWPOPUPNETWORK_RUNSHARE:Ljava/lang/String; = "key_show_network_runshare"

.field public static final PREFERENCE_SHOWPOPUPNETWORK_SAMSUNG_APPS:Ljava/lang/String; = "key_show_network_samung_apps"

.field private static final PREFERENCE_SHOWPOPUPNETWORK_SEND_EMAIL:Ljava/lang/String; = "key_show_network_send_email"

.field private static final PREFERENCE_SHOWPOPUPNETWORK_UPDATE:Ljava/lang/String; = "key_show_network_update"

.field private static final PREFERENCE_SHOW_EXT:Ljava/lang/String; = "key_show_ext"

.field private static final PREFERENCE_TEXT_ENCODING:Ljava/lang/String; = "key_text_encoding"

.field private static final PREFERENCE_TEXT_FONT_SIZE:Ljava/lang/String; = "key_text_font_size"

.field private static final PREFERENCE_TEXT_FONT_TYPE:Ljava/lang/String; = "key_text_font_type"

.field private static final PREFERENCE_TEXT_THEME:Ljava/lang/String; = "key_text_theme"

.field private static final PREFERENCE_VIEW_SPLASH:Ljava/lang/String; = "key_view_splash"

.field private static instance:Lcom/infraware/common/config/RuntimeConfig;


# instance fields
.field private m_oIntKeyMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    sput-object v0, Lcom/infraware/common/config/RuntimeConfig;->instance:Lcom/infraware/common/config/RuntimeConfig;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    .line 55
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    .line 56
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0xb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_default_format"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_is_backup"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_show_ext"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_auto_fit"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0x15

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_text_font_size"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0x16

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_text_theme"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0x17

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_text_encoding"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0x18

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_text_font_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0x5a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_dictionay_ko"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0x5b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_dictionay_en"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0x5c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_dictionay_zh"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0x5d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_dictionay_ja"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0x60

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_hand_animation"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0xc8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_recent_symbol"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0xd2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_recent_shape_ppt"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0xd3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_recent_shape_2003_ppt"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0xd4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_recent_shape"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0xd5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_recent_shape_2003"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0x61

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_view_splash"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0x12d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_show_network_update"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0x12e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_show_network_eula"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0x12f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_show_network_copyrights"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0x130

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_show_network_main_help"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0x131

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_show_network_send_email"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0x132

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_show_network_samung_apps"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0x133

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_show_network_office_download_samung_apps"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0x134

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_show_network_runshare"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0x135

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_show_network_rungoolesearch"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    iget-object v0, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    const/16 v1, 0x136

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "key_show_network_infra_home"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    return-void
.end method

.method public static getInstance()Lcom/infraware/common/config/RuntimeConfig;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/infraware/common/config/RuntimeConfig;->instance:Lcom/infraware/common/config/RuntimeConfig;

    if-nez v0, :cond_0

    .line 92
    new-instance v0, Lcom/infraware/common/config/RuntimeConfig;

    invoke-direct {v0}, Lcom/infraware/common/config/RuntimeConfig;-><init>()V

    sput-object v0, Lcom/infraware/common/config/RuntimeConfig;->instance:Lcom/infraware/common/config/RuntimeConfig;

    .line 94
    :cond_0
    sget-object v0, Lcom/infraware/common/config/RuntimeConfig;->instance:Lcom/infraware/common/config/RuntimeConfig;

    return-object v0
.end method


# virtual methods
.method public getBooleanPreference(Landroid/content/Context;IZ)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "key"    # I
    .param p3, "defaultValue"    # Z

    .prologue
    .line 152
    iget-object v2, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 153
    .local v0, "keyString":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 157
    .end local p3    # "defaultValue":Z
    :goto_0
    return p3

    .line 156
    .restart local p3    # "defaultValue":Z
    :cond_0
    const-string/jumbo v2, "polaris:preferences"

    const/4 v3, 0x4

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 157
    .local v1, "oPreference":Landroid/content/SharedPreferences;
    invoke-interface {v1, v0, p3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result p3

    goto :goto_0
.end method

.method public getIntPreference(Landroid/content/Context;II)I
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "key"    # I
    .param p3, "defaultValue"    # I

    .prologue
    .line 131
    iget-object v2, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 132
    .local v0, "keyString":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 136
    .end local p3    # "defaultValue":I
    :goto_0
    return p3

    .line 135
    .restart local p3    # "defaultValue":I
    :cond_0
    const-string/jumbo v2, "polaris:preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 136
    .local v1, "oPreference":Landroid/content/SharedPreferences;
    invoke-interface {v1, v0, p3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result p3

    goto :goto_0
.end method

.method public getStringPreference(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "key"    # I
    .param p3, "defaultValue"    # Ljava/lang/String;

    .prologue
    .line 110
    iget-object v2, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 111
    .local v0, "keyString":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 115
    .end local p3    # "defaultValue":Ljava/lang/String;
    :goto_0
    return-object p3

    .line 114
    .restart local p3    # "defaultValue":Ljava/lang/String;
    :cond_0
    const-string/jumbo v2, "polaris:preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 115
    .local v1, "oPreference":Landroid/content/SharedPreferences;
    invoke-interface {v1, v0, p3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    goto :goto_0
.end method

.method public setBooleanPreference(Landroid/content/Context;IZ)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "key"    # I
    .param p3, "value"    # Z

    .prologue
    .line 141
    iget-object v2, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 142
    .local v1, "keyString":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 148
    :goto_0
    return-void

    .line 145
    :cond_0
    const-string/jumbo v2, "polaris:preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 146
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, v1, p3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 147
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public setIntPreference(Landroid/content/Context;II)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "key"    # I
    .param p3, "value"    # I

    .prologue
    .line 120
    iget-object v2, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 121
    .local v1, "keyString":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 127
    :goto_0
    return-void

    .line 124
    :cond_0
    const-string/jumbo v2, "polaris:preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 125
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, v1, p3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 126
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public setStringPreference(Landroid/content/Context;ILjava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "key"    # I
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 99
    iget-object v2, p0, Lcom/infraware/common/config/RuntimeConfig;->m_oIntKeyMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 100
    .local v1, "keyString":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 106
    :goto_0
    return-void

    .line 103
    :cond_0
    const-string/jumbo v2, "polaris:preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 104
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, v1, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 105
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method
