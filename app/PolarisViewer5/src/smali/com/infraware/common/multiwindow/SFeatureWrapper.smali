.class public Lcom/infraware/common/multiwindow/SFeatureWrapper;
.super Ljava/lang/Object;
.source "SFeatureWrapper.java"


# static fields
.field static final LOG:Z = true

.field private static mCheckedSupporting:Z

.field private static mInstance:Lcom/infraware/common/multiwindow/SFeatureWrapper;

.field private static mMethod_EditText_setWritingBuddyEnabled:Ljava/lang/reflect/Method;

.field private static mSupported:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 11
    sput-object v1, Lcom/infraware/common/multiwindow/SFeatureWrapper;->mInstance:Lcom/infraware/common/multiwindow/SFeatureWrapper;

    .line 12
    sput-boolean v0, Lcom/infraware/common/multiwindow/SFeatureWrapper;->mCheckedSupporting:Z

    .line 13
    sput-boolean v0, Lcom/infraware/common/multiwindow/SFeatureWrapper;->mSupported:Z

    .line 16
    sput-object v1, Lcom/infraware/common/multiwindow/SFeatureWrapper;->mMethod_EditText_setWritingBuddyEnabled:Ljava/lang/reflect/Method;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private checkSupportedWritingBuddy()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 51
    sget-boolean v3, Lcom/infraware/common/multiwindow/SFeatureWrapper;->mCheckedSupporting:Z

    if-nez v3, :cond_0

    .line 52
    sput-boolean v5, Lcom/infraware/common/multiwindow/SFeatureWrapper;->mCheckedSupporting:Z

    .line 55
    new-array v1, v5, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v2

    .line 57
    .local v1, "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    :try_start_0
    const-string/jumbo v3, "get EditText.setWritingBuddyEnabled method"

    invoke-direct {p0, v3}, Lcom/infraware/common/multiwindow/SFeatureWrapper;->log(Ljava/lang/String;)V

    .line 58
    const-class v3, Landroid/widget/EditText;

    const-string/jumbo v4, "setWritingBuddyEnabled"

    invoke-virtual {v3, v4, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    sput-object v3, Lcom/infraware/common/multiwindow/SFeatureWrapper;->mMethod_EditText_setWritingBuddyEnabled:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    sput-boolean v5, Lcom/infraware/common/multiwindow/SFeatureWrapper;->mSupported:Z

    .line 66
    .end local v1    # "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    :cond_0
    sget-boolean v2, Lcom/infraware/common/multiwindow/SFeatureWrapper;->mSupported:Z

    :goto_0
    return v2

    .line 59
    .restart local v1    # "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    :catch_0
    move-exception v0

    .line 60
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string/jumbo v3, "Failed getting EditText.setWritingBuddyEnabled method"

    invoke-direct {p0, v3}, Lcom/infraware/common/multiwindow/SFeatureWrapper;->log(Ljava/lang/String;)V

    .line 61
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getInstance()Lcom/infraware/common/multiwindow/SFeatureWrapper;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/infraware/common/multiwindow/SFeatureWrapper;->mInstance:Lcom/infraware/common/multiwindow/SFeatureWrapper;

    if-nez v0, :cond_0

    .line 20
    new-instance v0, Lcom/infraware/common/multiwindow/SFeatureWrapper;

    invoke-direct {v0}, Lcom/infraware/common/multiwindow/SFeatureWrapper;-><init>()V

    sput-object v0, Lcom/infraware/common/multiwindow/SFeatureWrapper;->mInstance:Lcom/infraware/common/multiwindow/SFeatureWrapper;

    .line 22
    :cond_0
    sget-object v0, Lcom/infraware/common/multiwindow/SFeatureWrapper;->mInstance:Lcom/infraware/common/multiwindow/SFeatureWrapper;

    return-object v0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1, "a"    # Ljava/lang/String;

    .prologue
    .line 71
    const-string/jumbo v0, "SFeatureWrapper"

    invoke-static {v0, p1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    return-void
.end method


# virtual methods
.method public turnOffWritingBuddy(Landroid/widget/EditText;)V
    .locals 4
    .param p1, "aEditTextObj"    # Landroid/widget/EditText;

    .prologue
    const/4 v3, 0x0

    .line 26
    const-string/jumbo v2, "turnOffWritingBuddy"

    invoke-direct {p0, v2}, Lcom/infraware/common/multiwindow/SFeatureWrapper;->log(Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lcom/infraware/common/multiwindow/SFeatureWrapper;->checkSupportedWritingBuddy()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 30
    const/4 v2, 0x1

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v3

    .line 32
    .local v1, "parameters":[Ljava/lang/Object;
    :try_start_0
    const-string/jumbo v2, "invoke method"

    invoke-direct {p0, v2}, Lcom/infraware/common/multiwindow/SFeatureWrapper;->log(Ljava/lang/String;)V

    .line 33
    sget-object v2, Lcom/infraware/common/multiwindow/SFeatureWrapper;->mMethod_EditText_setWritingBuddyEnabled:Ljava/lang/reflect/Method;

    invoke-virtual {v2, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    .line 48
    .end local v1    # "parameters":[Ljava/lang/Object;
    :cond_0
    :goto_0
    return-void

    .line 34
    .restart local v1    # "parameters":[Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 35
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string/jumbo v2, "exception  invoke"

    invoke-direct {p0, v2}, Lcom/infraware/common/multiwindow/SFeatureWrapper;->log(Ljava/lang/String;)V

    .line 36
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 37
    sput-boolean v3, Lcom/infraware/common/multiwindow/SFeatureWrapper;->mSupported:Z

    goto :goto_0

    .line 38
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 39
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string/jumbo v2, "exception  invoke"

    invoke-direct {p0, v2}, Lcom/infraware/common/multiwindow/SFeatureWrapper;->log(Ljava/lang/String;)V

    .line 40
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 41
    sput-boolean v3, Lcom/infraware/common/multiwindow/SFeatureWrapper;->mSupported:Z

    goto :goto_0

    .line 42
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v0

    .line 43
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string/jumbo v2, "exception  invoke"

    invoke-direct {p0, v2}, Lcom/infraware/common/multiwindow/SFeatureWrapper;->log(Ljava/lang/String;)V

    .line 44
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    .line 45
    sput-boolean v3, Lcom/infraware/common/multiwindow/SFeatureWrapper;->mSupported:Z

    goto :goto_0
.end method
