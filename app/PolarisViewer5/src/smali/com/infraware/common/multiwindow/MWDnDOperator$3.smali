.class Lcom/infraware/common/multiwindow/MWDnDOperator$3;
.super Landroid/view/View$DragShadowBuilder;
.source "MWDnDOperator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/common/multiwindow/MWDnDOperator;->startDragImage(Landroid/view/View;Ljava/lang/String;Landroid/graphics/Bitmap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private height:I

.field final synthetic this$0:Lcom/infraware/common/multiwindow/MWDnDOperator;

.field private width:I


# direct methods
.method constructor <init>(Lcom/infraware/common/multiwindow/MWDnDOperator;Landroid/view/View;)V
    .locals 0
    .param p2, "x0"    # Landroid/view/View;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/infraware/common/multiwindow/MWDnDOperator$3;->this$0:Lcom/infraware/common/multiwindow/MWDnDOperator;

    invoke-direct {p0, p2}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public onDrawShadow(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x0

    .line 146
    new-instance v1, Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/infraware/common/multiwindow/MWDnDOperator$3;->this$0:Lcom/infraware/common/multiwindow/MWDnDOperator;

    # getter for: Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/infraware/common/multiwindow/MWDnDOperator;->access$000(Lcom/infraware/common/multiwindow/MWDnDOperator;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/infraware/common/multiwindow/MWDnDOperator$3;->this$0:Lcom/infraware/common/multiwindow/MWDnDOperator;

    # getter for: Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/infraware/common/multiwindow/MWDnDOperator;->access$000(Lcom/infraware/common/multiwindow/MWDnDOperator;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 147
    .local v1, "srcRect":Landroid/graphics/Rect;
    new-instance v0, Landroid/graphics/Rect;

    iget v2, p0, Lcom/infraware/common/multiwindow/MWDnDOperator$3;->width:I

    iget v3, p0, Lcom/infraware/common/multiwindow/MWDnDOperator$3;->height:I

    invoke-direct {v0, v4, v4, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 148
    .local v0, "dstRect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/infraware/common/multiwindow/MWDnDOperator$3;->this$0:Lcom/infraware/common/multiwindow/MWDnDOperator;

    # getter for: Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/infraware/common/multiwindow/MWDnDOperator;->access$000(Lcom/infraware/common/multiwindow/MWDnDOperator;)Landroid/graphics/Bitmap;

    move-result-object v2

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {p1, v2, v1, v0, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 149
    return-void
.end method

.method public onProvideShadowMetrics(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 4
    .param p1, "shadowSize"    # Landroid/graphics/Point;
    .param p2, "shadowTouchPoint"    # Landroid/graphics/Point;

    .prologue
    .line 137
    iget-object v1, p0, Lcom/infraware/common/multiwindow/MWDnDOperator$3;->this$0:Lcom/infraware/common/multiwindow/MWDnDOperator;

    iget-object v2, p0, Lcom/infraware/common/multiwindow/MWDnDOperator$3;->this$0:Lcom/infraware/common/multiwindow/MWDnDOperator;

    # getter for: Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/infraware/common/multiwindow/MWDnDOperator;->access$000(Lcom/infraware/common/multiwindow/MWDnDOperator;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/infraware/common/multiwindow/MWDnDOperator$3;->this$0:Lcom/infraware/common/multiwindow/MWDnDOperator;

    # getter for: Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/infraware/common/multiwindow/MWDnDOperator;->access$000(Lcom/infraware/common/multiwindow/MWDnDOperator;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    # invokes: Lcom/infraware/common/multiwindow/MWDnDOperator;->calcShadowSize(II)Landroid/graphics/Point;
    invoke-static {v1, v2, v3}, Lcom/infraware/common/multiwindow/MWDnDOperator;->access$100(Lcom/infraware/common/multiwindow/MWDnDOperator;II)Landroid/graphics/Point;

    move-result-object v0

    .line 139
    .local v0, "size":Landroid/graphics/Point;
    iget v1, v0, Landroid/graphics/Point;->x:I

    iput v1, p0, Lcom/infraware/common/multiwindow/MWDnDOperator$3;->width:I

    .line 140
    iget v1, v0, Landroid/graphics/Point;->y:I

    iput v1, p0, Lcom/infraware/common/multiwindow/MWDnDOperator$3;->height:I

    .line 141
    iget v1, p0, Lcom/infraware/common/multiwindow/MWDnDOperator$3;->width:I

    iget v2, p0, Lcom/infraware/common/multiwindow/MWDnDOperator$3;->height:I

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 142
    iget v1, p0, Lcom/infraware/common/multiwindow/MWDnDOperator$3;->width:I

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/infraware/common/multiwindow/MWDnDOperator$3;->height:I

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {p2, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 143
    return-void
.end method
