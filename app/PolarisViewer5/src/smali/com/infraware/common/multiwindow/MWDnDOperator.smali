.class public Lcom/infraware/common/multiwindow/MWDnDOperator;
.super Ljava/lang/Object;
.source "MWDnDOperator.java"


# static fields
.field public static LOG:Z


# instance fields
.field private mBaseRect:Landroid/graphics/Rect;

.field private mEditCopyFlag:Z

.field private mIsSending:Z

.field private mMarkedStringOnTxt:Ljava/lang/String;

.field private mMaxShadowSize:I

.field private mSendingData:Ljava/lang/String;

.field private mSendingText:Ljava/lang/String;

.field private mShadowBitmap:Landroid/graphics/Bitmap;

.field private mTextMarkRects:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x1

    sput-boolean v0, Lcom/infraware/common/multiwindow/MWDnDOperator;->LOG:Z

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 3
    .param p1, "aActivity"    # Landroid/app/Activity;

    .prologue
    const/4 v2, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-boolean v2, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mIsSending:Z

    .line 31
    const/16 v1, 0x190

    iput v1, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mMaxShadowSize:I

    .line 34
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;

    .line 35
    iput-boolean v2, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mEditCopyFlag:Z

    .line 250
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mTextMarkRects:Ljava/util/ArrayList;

    .line 284
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v2, v2, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mBaseRect:Landroid/graphics/Rect;

    .line 41
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 42
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 43
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v1, v2, :cond_0

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v1, v1, 0x2

    :goto_0
    iput v1, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mMaxShadowSize:I

    .line 44
    return-void

    .line 43
    :cond_0
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    div-int/lit8 v1, v1, 0x2

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/infraware/common/multiwindow/MWDnDOperator;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/common/multiwindow/MWDnDOperator;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/common/multiwindow/MWDnDOperator;II)Landroid/graphics/Point;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/common/multiwindow/MWDnDOperator;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/infraware/common/multiwindow/MWDnDOperator;->calcShadowSize(II)Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method private varargs arrageBitmaps([Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "aBitmaps"    # [Landroid/graphics/Bitmap;

    .prologue
    .line 402
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/infraware/common/multiwindow/MWDnDOperator$6;

    invoke-direct {v1, p0, p1}, Lcom/infraware/common/multiwindow/MWDnDOperator$6;-><init>(Lcom/infraware/common/multiwindow/MWDnDOperator;[Landroid/graphics/Bitmap;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 413
    return-void
.end method

.method private calcShadowSize(II)Landroid/graphics/Point;
    .locals 6
    .param p1, "aOrgWidth"    # I
    .param p2, "aOrgHeight"    # I

    .prologue
    .line 318
    iget v2, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mMaxShadowSize:I

    .line 319
    .local v2, "maxWidth":I
    iget v1, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mMaxShadowSize:I

    .line 320
    .local v1, "maxHeight":I
    move v4, p1

    .line 321
    .local v4, "width":I
    move v0, p2

    .line 322
    .local v0, "height":I
    if-le v4, v2, :cond_0

    .line 323
    mul-int v5, v2, v0

    div-int v0, v5, v4

    .line 324
    move v4, v2

    .line 327
    :cond_0
    if-le v0, v1, :cond_1

    .line 328
    mul-int v5, v1, v4

    div-int v4, v5, v0

    .line 329
    move v0, v1

    .line 332
    :cond_1
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3, v4, v0}, Landroid/graphics/Point;-><init>(II)V

    .line 333
    .local v3, "size":Landroid/graphics/Point;
    return-object v3
.end method

.method private createClipDataHtml(Ljava/lang/String;Ljava/lang/String;)Landroid/content/ClipData;
    .locals 4
    .param p1, "aText"    # Ljava/lang/String;
    .param p2, "aHtml"    # Ljava/lang/String;

    .prologue
    .line 228
    const/4 v2, 0x1

    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "text/html"

    aput-object v3, v1, v2

    .line 229
    .local v1, "strs":[Ljava/lang/String;
    new-instance v0, Landroid/content/ClipData$Item;

    invoke-direct {v0, p1, p2}, Landroid/content/ClipData$Item;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;)V

    .line 230
    .local v0, "item":Landroid/content/ClipData$Item;
    new-instance v2, Landroid/content/ClipData;

    const-string/jumbo v3, "MultiWindow_DragDrop "

    invoke-direct {v2, v3, v1, v0}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    return-object v2
.end method

.method private createClipDataImage(Ljava/lang/String;Ljava/lang/String;)Landroid/content/ClipData;
    .locals 6
    .param p1, "aPath"    # Ljava/lang/String;
    .param p2, "aType"    # Ljava/lang/String;

    .prologue
    .line 234
    const/4 v4, 0x1

    new-array v2, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "text/vnd.android.intent"

    aput-object v5, v2, v4

    .line 235
    .local v2, "strs":[Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    .line 236
    .local v3, "uri":Landroid/net/Uri;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 237
    .local v1, "oSendIntent":Landroid/content/Intent;
    const-string/jumbo v4, "android.intent.action.SEND"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 238
    if-nez p2, :cond_0

    .line 239
    const-string/jumbo v4, "image/*"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 244
    :goto_0
    const-string/jumbo v4, "android.intent.extra.STREAM"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 245
    new-instance v0, Landroid/content/ClipData$Item;

    invoke-direct {v0, v1}, Landroid/content/ClipData$Item;-><init>(Landroid/content/Intent;)V

    .line 246
    .local v0, "item":Landroid/content/ClipData$Item;
    new-instance v4, Landroid/content/ClipData;

    const-string/jumbo v5, "MultiWindow_DragDrop "

    invoke-direct {v4, v5, v2, v0}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    return-object v4

    .line 241
    .end local v0    # "item":Landroid/content/ClipData$Item;
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "image/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private createClipDataText(Ljava/lang/String;)Landroid/content/ClipData;
    .locals 4
    .param p1, "aText"    # Ljava/lang/String;

    .prologue
    .line 222
    const/4 v2, 0x1

    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "text/plain"

    aput-object v3, v1, v2

    .line 223
    .local v1, "strs":[Ljava/lang/String;
    new-instance v0, Landroid/content/ClipData$Item;

    invoke-direct {v0, p1}, Landroid/content/ClipData$Item;-><init>(Ljava/lang/CharSequence;)V

    .line 224
    .local v0, "item":Landroid/content/ClipData$Item;
    new-instance v2, Landroid/content/ClipData;

    const-string/jumbo v3, "MultiWindow_DragDrop "

    invoke-direct {v2, v3, v1, v0}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    return-object v2
.end method

.method private createTextBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "aText"    # Ljava/lang/String;

    .prologue
    .line 299
    move-object v4, p1

    .line 300
    .local v4, "text":Ljava/lang/String;
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 301
    .local v2, "paint":Landroid/graphics/Paint;
    const/high16 v6, 0x42480000    # 50.0f

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 303
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    const/16 v7, 0xa

    if-le v6, v7, :cond_0

    .line 304
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v7, 0x0

    const/16 v8, 0x9

    invoke-virtual {p1, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "..."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 307
    :cond_0
    invoke-virtual {v2}, Landroid/graphics/Paint;->getTextSize()F

    move-result v3

    .line 308
    .local v3, "tSize":F
    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v5

    .line 309
    .local v5, "width":F
    float-to-int v6, v5

    const v7, 0x3fa66666    # 1.3f

    mul-float/2addr v7, v3

    float-to-int v7, v7

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 310
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 312
    .local v1, "cv":Landroid/graphics/Canvas;
    const/4 v6, 0x0

    invoke-virtual {v1, v4, v6, v3, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 313
    return-object v0
.end method


# virtual methods
.method public addLastTextMarkArea(Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "aRect"    # Landroid/graphics/Rect;

    .prologue
    .line 258
    iget-object v0, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mTextMarkRects:Ljava/util/ArrayList;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 259
    return-void
.end method

.method public clearLastTextMarkArea()V
    .locals 1

    .prologue
    .line 253
    const-string/jumbo v0, ""

    invoke-virtual {p0, v0}, Lcom/infraware/common/multiwindow/MWDnDOperator;->setMarkedText(Ljava/lang/String;)V

    .line 254
    iget-object v0, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mTextMarkRects:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 255
    return-void
.end method

.method public isSelectedArea(II)Z
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 294
    iget-object v0, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mBaseRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    return v0
.end method

.method public isTextMarkArea(II)Z
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 276
    iget-object v2, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mTextMarkRects:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    .line 277
    .local v1, "rect":Landroid/graphics/Rect;
    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 278
    const/4 v2, 0x1

    .line 281
    .end local v1    # "rect":Landroid/graphics/Rect;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onDragEnded(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 337
    iget-boolean v1, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mIsSending:Z

    if-eqz v1, :cond_1

    .line 338
    iput-boolean v3, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mIsSending:Z

    .line 339
    iput-object v4, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mSendingText:Ljava/lang/String;

    .line 340
    iput-object v4, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mSendingData:Ljava/lang/String;

    .line 341
    sget-boolean v1, Lcom/infraware/common/multiwindow/MWDnDOperator;->LOG:Z

    if-eqz v1, :cond_0

    const-string/jumbo v1, "MWDnD"

    const-string/jumbo v2, "finished sending."

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    :cond_0
    iget-object v1, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 343
    iget-object v0, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;

    .line 344
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const/4 v1, 0x1

    new-array v1, v1, [Landroid/graphics/Bitmap;

    aput-object v0, v1, v3

    invoke-direct {p0, v1}, Lcom/infraware/common/multiwindow/MWDnDOperator;->arrageBitmaps([Landroid/graphics/Bitmap;)V

    .line 345
    iput-object v4, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;

    .line 351
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    return v3
.end method

.method public onDragEndedFile(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "e"    # Landroid/view/DragEvent;

    .prologue
    const/4 v3, 0x0

    .line 355
    iget-boolean v1, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mIsSending:Z

    if-eqz v1, :cond_1

    .line 356
    iput-boolean v3, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mIsSending:Z

    .line 357
    sget-boolean v1, Lcom/infraware/common/multiwindow/MWDnDOperator;->LOG:Z

    if-eqz v1, :cond_0

    const-string/jumbo v1, "MWDnD"

    const-string/jumbo v2, "finished sending."

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    :cond_0
    iget-object v1, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 359
    iget-object v0, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;

    .line 360
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const/4 v1, 0x1

    new-array v1, v1, [Landroid/graphics/Bitmap;

    aput-object v0, v1, v3

    invoke-direct {p0, v1}, Lcom/infraware/common/multiwindow/MWDnDOperator;->arrageBitmaps([Landroid/graphics/Bitmap;)V

    .line 361
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;

    .line 364
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    return v3
.end method

.method public onDragEndedOnTxt(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "e"    # Landroid/view/DragEvent;

    .prologue
    const/4 v3, 0x0

    .line 369
    iget-boolean v1, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mIsSending:Z

    if-eqz v1, :cond_1

    .line 370
    iput-boolean v3, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mIsSending:Z

    .line 371
    sget-boolean v1, Lcom/infraware/common/multiwindow/MWDnDOperator;->LOG:Z

    if-eqz v1, :cond_0

    const-string/jumbo v1, "MWDnD"

    const-string/jumbo v2, "finished sending."

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    :cond_0
    iget-object v1, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 373
    iget-object v0, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;

    .line 374
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const/4 v1, 0x1

    new-array v1, v1, [Landroid/graphics/Bitmap;

    aput-object v0, v1, v3

    invoke-direct {p0, v1}, Lcom/infraware/common/multiwindow/MWDnDOperator;->arrageBitmaps([Landroid/graphics/Bitmap;)V

    .line 375
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;

    .line 378
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    return v3
.end method

.method public onDrop(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "e"    # Landroid/view/DragEvent;

    .prologue
    const/4 v2, 0x0

    .line 393
    sget-boolean v0, Lcom/infraware/common/multiwindow/MWDnDOperator;->LOG:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "MWDnD"

    const-string/jumbo v1, "onDrop"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    :cond_0
    iget-boolean v0, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mIsSending:Z

    if-eqz v0, :cond_1

    .line 395
    sget-boolean v0, Lcom/infraware/common/multiwindow/MWDnDOperator;->LOG:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "MWDnD"

    const-string/jumbo v1, "Is Sending. No receive dropping. "

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    :cond_1
    return v2
.end method

.method public onDropOnTxt(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "e"    # Landroid/view/DragEvent;

    .prologue
    .line 384
    sget-boolean v0, Lcom/infraware/common/multiwindow/MWDnDOperator;->LOG:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "MWDnD"

    const-string/jumbo v1, "onDrop"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    :cond_0
    iget-boolean v0, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mIsSending:Z

    if-eqz v0, :cond_2

    .line 386
    sget-boolean v0, Lcom/infraware/common/multiwindow/MWDnDOperator;->LOG:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "MWDnD"

    const-string/jumbo v1, "Is Sending. No receive dropping. "

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    :cond_1
    const/4 v0, 0x1

    .line 389
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onEditCopy(IILjava/lang/String;Ljava/lang/String;I)Z
    .locals 6
    .param p1, "caller"    # I
    .param p2, "nType"    # I
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "data"    # Ljava/lang/String;
    .param p5, "nMode"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 52
    iget-boolean v2, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mEditCopyFlag:Z

    if-nez v2, :cond_0

    .line 57
    :goto_0
    return v0

    .line 53
    :cond_0
    iput-boolean v0, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mEditCopyFlag:Z

    .line 54
    iput-object p3, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mSendingText:Ljava/lang/String;

    .line 55
    iput-object p4, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mSendingData:Ljava/lang/String;

    .line 56
    sget-boolean v2, Lcom/infraware/common/multiwindow/MWDnDOperator;->LOG:Z

    if-eqz v2, :cond_1

    const-string/jumbo v2, "MWDnD"

    const-string/jumbo v3, "onEditCopy : caller[%d],nType[%d],text[%s],data[%s],mode[%d]"

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v1

    const/4 v0, 0x2

    aput-object p3, v4, v0

    const/4 v0, 0x3

    aput-object p4, v4, v0

    const/4 v0, 0x4

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move v0, v1

    .line 57
    goto :goto_0
.end method

.method public preoccupyOnEditCopy()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mEditCopyFlag:Z

    .line 49
    return-void
.end method

.method public setLastSelectedRect(IIII)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 287
    iget-object v0, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mBaseRect:Landroid/graphics/Rect;

    iput p1, v0, Landroid/graphics/Rect;->left:I

    .line 288
    iget-object v0, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mBaseRect:Landroid/graphics/Rect;

    iput p2, v0, Landroid/graphics/Rect;->top:I

    .line 289
    iget-object v0, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mBaseRect:Landroid/graphics/Rect;

    iput p3, v0, Landroid/graphics/Rect;->right:I

    .line 290
    iget-object v0, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mBaseRect:Landroid/graphics/Rect;

    iput p4, v0, Landroid/graphics/Rect;->bottom:I

    .line 291
    return-void
.end method

.method public setLastTextMarkArea(I[S)V
    .locals 5
    .param p1, "aCount"    # I
    .param p2, "rectInfo"    # [S

    .prologue
    .line 262
    iget-object v2, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mTextMarkRects:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 263
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_0

    .line 264
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 265
    .local v1, "rect":Landroid/graphics/Rect;
    mul-int/lit8 v2, v0, 0x4

    aget-short v2, p2, v2

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 266
    mul-int/lit8 v2, v0, 0x4

    add-int/lit8 v2, v2, 0x1

    aget-short v2, p2, v2

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 267
    mul-int/lit8 v2, v0, 0x4

    add-int/lit8 v2, v2, 0x2

    aget-short v2, p2, v2

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 268
    mul-int/lit8 v2, v0, 0x4

    add-int/lit8 v2, v2, 0x3

    aget-short v2, p2, v2

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 269
    iget-object v2, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mTextMarkRects:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 263
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 271
    .end local v1    # "rect":Landroid/graphics/Rect;
    :cond_0
    sget-boolean v2, Lcom/infraware/common/multiwindow/MWDnDOperator;->LOG:Z

    if-eqz v2, :cond_1

    const-string/jumbo v2, "MWDnD"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setLastTextMarkArea count : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mTextMarkRects:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    :cond_1
    return-void
.end method

.method public setMarkedText(Ljava/lang/String;)V
    .locals 0
    .param p1, "markedString"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mMarkedStringOnTxt:Ljava/lang/String;

    .line 62
    return-void
.end method

.method public startDragHtmlWithImage(Landroid/view/View;Landroid/graphics/Bitmap;)V
    .locals 8
    .param p1, "aView"    # Landroid/view/View;
    .param p2, "aShadowBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 185
    sget-boolean v6, Lcom/infraware/common/multiwindow/MWDnDOperator;->LOG:Z

    if-eqz v6, :cond_0

    const-string/jumbo v6, "MWDnD"

    const-string/jumbo v7, "startDragHtmlWithImage"

    invoke-static {v6, v7}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    :cond_0
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mIsSending:Z

    .line 187
    const-string/jumbo v6, ""

    iget-object v7, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mSendingData:Ljava/lang/String;

    invoke-direct {p0, v6, v7}, Lcom/infraware/common/multiwindow/MWDnDOperator;->createClipDataHtml(Ljava/lang/String;Ljava/lang/String;)Landroid/content/ClipData;

    move-result-object v1

    .line 188
    .local v1, "dragData":Landroid/content/ClipData;
    if-nez p2, :cond_2

    .line 189
    new-instance v0, Ljava/lang/String;

    iget-object v6, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mSendingData:Ljava/lang/String;

    invoke-direct {v0, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 190
    .local v0, "data":Ljava/lang/String;
    const-string/jumbo v6, "/data/data/[./a-zA-Z0-9]*png"

    invoke-static {v6}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v5

    .line 191
    .local v5, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v5, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 192
    .local v4, "m":Ljava/util/regex/Matcher;
    const/4 v3, 0x0

    .line 193
    .local v3, "g":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 194
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v3

    .line 196
    :cond_1
    invoke-static {v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;

    .line 201
    .end local v0    # "data":Ljava/lang/String;
    .end local v3    # "g":Ljava/lang/String;
    .end local v4    # "m":Ljava/util/regex/Matcher;
    .end local v5    # "pattern":Ljava/util/regex/Pattern;
    :goto_0
    new-instance v2, Lcom/infraware/common/multiwindow/MWDnDOperator$5;

    invoke-direct {v2, p0, p1}, Lcom/infraware/common/multiwindow/MWDnDOperator$5;-><init>(Lcom/infraware/common/multiwindow/MWDnDOperator;Landroid/view/View;)V

    .line 218
    .local v2, "dragShadow":Landroid/view/View$DragShadowBuilder;
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {p1, v1, v2, v6, v7}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    .line 219
    return-void

    .line 198
    .end local v2    # "dragShadow":Landroid/view/View$DragShadowBuilder;
    :cond_2
    iput-object p2, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public startDragImage(Landroid/view/View;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 5
    .param p1, "aView"    # Landroid/view/View;
    .param p2, "aType"    # Ljava/lang/String;
    .param p3, "aShadowBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 122
    sget-boolean v3, Lcom/infraware/common/multiwindow/MWDnDOperator;->LOG:Z

    if-eqz v3, :cond_0

    const-string/jumbo v3, "MWDnD"

    const-string/jumbo v4, "startDragImage"

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    :cond_0
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mIsSending:Z

    .line 125
    iget-object v2, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mSendingData:Ljava/lang/String;

    .line 126
    .local v2, "imgPath":Ljava/lang/String;
    invoke-direct {p0, v2, p2}, Lcom/infraware/common/multiwindow/MWDnDOperator;->createClipDataImage(Ljava/lang/String;Ljava/lang/String;)Landroid/content/ClipData;

    move-result-object v0

    .line 128
    .local v0, "dragData":Landroid/content/ClipData;
    if-nez p3, :cond_1

    .line 129
    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;

    .line 134
    :goto_0
    new-instance v1, Lcom/infraware/common/multiwindow/MWDnDOperator$3;

    invoke-direct {v1, p0, p1}, Lcom/infraware/common/multiwindow/MWDnDOperator$3;-><init>(Lcom/infraware/common/multiwindow/MWDnDOperator;Landroid/view/View;)V

    .line 152
    .local v1, "dragShadow":Landroid/view/View$DragShadowBuilder;
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p1, v0, v1, v3, v4}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    .line 153
    return-void

    .line 131
    .end local v1    # "dragShadow":Landroid/view/View$DragShadowBuilder;
    :cond_1
    iput-object p3, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public startDragText(Landroid/view/View;Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "aView"    # Landroid/view/View;
    .param p2, "aShadowBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 65
    sget-boolean v2, Lcom/infraware/common/multiwindow/MWDnDOperator;->LOG:Z

    if-eqz v2, :cond_0

    const-string/jumbo v2, "MWDnD"

    const-string/jumbo v3, "startDragText"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mIsSending:Z

    .line 67
    iget-object v2, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mMarkedStringOnTxt:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/infraware/common/multiwindow/MWDnDOperator;->createClipDataText(Ljava/lang/String;)Landroid/content/ClipData;

    move-result-object v0

    .line 68
    .local v0, "dragData":Landroid/content/ClipData;
    if-nez p2, :cond_1

    .line 69
    iget-object v2, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mMarkedStringOnTxt:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/infraware/common/multiwindow/MWDnDOperator;->createTextBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;

    .line 73
    :goto_0
    new-instance v1, Lcom/infraware/common/multiwindow/MWDnDOperator$1;

    invoke-direct {v1, p0, p1}, Lcom/infraware/common/multiwindow/MWDnDOperator$1;-><init>(Lcom/infraware/common/multiwindow/MWDnDOperator;Landroid/view/View;)V

    .line 87
    .local v1, "dragShadow":Landroid/view/View$DragShadowBuilder;
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    .line 88
    return-void

    .line 71
    .end local v1    # "dragShadow":Landroid/view/View$DragShadowBuilder;
    :cond_1
    iput-object p2, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public startDragTextMark(Landroid/view/View;Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "aView"    # Landroid/view/View;
    .param p2, "aShadowBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 156
    sget-boolean v2, Lcom/infraware/common/multiwindow/MWDnDOperator;->LOG:Z

    if-eqz v2, :cond_0

    const-string/jumbo v2, "MWDnD"

    const-string/jumbo v3, "startDragText"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mIsSending:Z

    .line 160
    iget-object v2, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mSendingText:Ljava/lang/String;

    iget-object v3, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mSendingData:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/infraware/common/multiwindow/MWDnDOperator;->createClipDataHtml(Ljava/lang/String;Ljava/lang/String;)Landroid/content/ClipData;

    move-result-object v0

    .line 161
    .local v0, "dragData":Landroid/content/ClipData;
    if-nez p2, :cond_1

    .line 162
    iget-object v2, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mSendingText:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/infraware/common/multiwindow/MWDnDOperator;->createTextBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;

    .line 167
    :goto_0
    new-instance v1, Lcom/infraware/common/multiwindow/MWDnDOperator$4;

    invoke-direct {v1, p0, p1}, Lcom/infraware/common/multiwindow/MWDnDOperator$4;-><init>(Lcom/infraware/common/multiwindow/MWDnDOperator;Landroid/view/View;)V

    .line 181
    .local v1, "dragShadow":Landroid/view/View$DragShadowBuilder;
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    .line 182
    return-void

    .line 164
    .end local v1    # "dragShadow":Landroid/view/View$DragShadowBuilder;
    :cond_1
    iput-object p2, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public startDragTextOnPDF(Landroid/view/View;Landroid/graphics/Bitmap;)Z
    .locals 6
    .param p1, "aView"    # Landroid/view/View;
    .param p2, "aShadowBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 91
    sget-boolean v4, Lcom/infraware/common/multiwindow/MWDnDOperator;->LOG:Z

    if-eqz v4, :cond_0

    const-string/jumbo v4, "MWDnD"

    const-string/jumbo v5, "startDragTextOnPDF"

    invoke-static {v4, v5}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    :cond_0
    iget-object v4, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mSendingText:Ljava/lang/String;

    if-nez v4, :cond_1

    .line 117
    :goto_0
    return v2

    .line 95
    :cond_1
    iput-boolean v3, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mIsSending:Z

    .line 96
    iget-object v4, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mSendingText:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/infraware/common/multiwindow/MWDnDOperator;->createClipDataText(Ljava/lang/String;)Landroid/content/ClipData;

    move-result-object v0

    .line 97
    .local v0, "dragData":Landroid/content/ClipData;
    if-nez p2, :cond_2

    .line 98
    iget-object v4, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mSendingText:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/infraware/common/multiwindow/MWDnDOperator;->createTextBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;

    .line 102
    :goto_1
    new-instance v1, Lcom/infraware/common/multiwindow/MWDnDOperator$2;

    invoke-direct {v1, p0, p1}, Lcom/infraware/common/multiwindow/MWDnDOperator$2;-><init>(Lcom/infraware/common/multiwindow/MWDnDOperator;Landroid/view/View;)V

    .line 116
    .local v1, "dragShadow":Landroid/view/View$DragShadowBuilder;
    const/4 v4, 0x0

    invoke-virtual {p1, v0, v1, v4, v2}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    move v2, v3

    .line 117
    goto :goto_0

    .line 100
    .end local v1    # "dragShadow":Landroid/view/View$DragShadowBuilder;
    :cond_2
    iput-object p2, p0, Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;

    goto :goto_1
.end method
