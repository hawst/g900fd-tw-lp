.class public Lcom/infraware/common/multiwindow/MultiWindowWrapper;
.super Ljava/lang/Object;
.source "MultiWindowWrapper.java"


# static fields
.field private static mCheckedSupporting:Z

.field private static mInstance:Lcom/infraware/common/multiwindow/MultiWindowWrapper;

.field private static mSupported:Z


# instance fields
.field mActivity:Landroid/app/Activity;

.field private mKlass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private mMethodGetRect:Ljava/lang/reflect/Method;

.field private mMethodIsNormalWindow:Ljava/lang/reflect/Method;

.field private mMultiWindowObj:Ljava/lang/Object;

.field private mStaticMethodCreateInstance:Ljava/lang/reflect/Method;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 12
    const/4 v0, 0x0

    sput-object v0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mInstance:Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    .line 13
    sput-boolean v1, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mCheckedSupporting:Z

    .line 14
    sput-boolean v1, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mSupported:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object v0, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mKlass:Ljava/lang/Class;

    .line 17
    iput-object v0, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mStaticMethodCreateInstance:Ljava/lang/reflect/Method;

    .line 18
    iput-object v0, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mMultiWindowObj:Ljava/lang/Object;

    .line 19
    iput-object v0, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mMethodGetRect:Ljava/lang/reflect/Method;

    .line 20
    iput-object v0, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mMethodIsNormalWindow:Ljava/lang/reflect/Method;

    .line 21
    iput-object v0, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mActivity:Landroid/app/Activity;

    return-void
.end method

.method private checkMultiWindowObj(Landroid/app/Activity;)Z
    .locals 2
    .param p1, "aActivity"    # Landroid/app/Activity;

    .prologue
    const/4 v1, 0x0

    .line 125
    iget-object v0, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mActivity:Landroid/app/Activity;

    if-eq v0, p1, :cond_1

    .line 126
    invoke-direct {p0, p1}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->createInstance(Landroid/app/Activity;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mMultiWindowObj:Ljava/lang/Object;

    .line 127
    iget-object v0, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mMultiWindowObj:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 128
    iput-object v1, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mActivity:Landroid/app/Activity;

    .line 129
    iput-object v1, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mMethodGetRect:Ljava/lang/reflect/Method;

    .line 130
    const/4 v0, 0x0

    .line 134
    :goto_0
    return v0

    .line 132
    :cond_0
    iput-object p1, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mActivity:Landroid/app/Activity;

    .line 134
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private checkSupported(Landroid/app/Activity;)Z
    .locals 6
    .param p1, "aActivity"    # Landroid/app/Activity;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 99
    sget-boolean v3, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mCheckedSupporting:Z

    if-nez v3, :cond_2

    .line 100
    sput-boolean v5, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mCheckedSupporting:Z

    .line 101
    const/4 v1, 0x0

    .line 103
    .local v1, "feature":Z
    invoke-virtual {p1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 104
    .local v2, "pm":Landroid/content/pm/PackageManager;
    if-eqz v2, :cond_0

    .line 105
    const-string/jumbo v3, "com.sec.feature.multiwindow"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    .line 107
    :cond_0
    if-nez v1, :cond_1

    .line 108
    sput-boolean v4, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mSupported:Z

    .line 109
    sget-boolean v3, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mSupported:Z

    .line 121
    .end local v1    # "feature":Z
    .end local v2    # "pm":Landroid/content/pm/PackageManager;
    :goto_0
    return v3

    .line 112
    .restart local v1    # "feature":Z
    .restart local v2    # "pm":Landroid/content/pm/PackageManager;
    :cond_1
    :try_start_0
    const-string/jumbo v3, "android.sec.multiwindow.MultiWindow"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    iput-object v3, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mKlass:Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    iget-object v3, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mKlass:Ljava/lang/Class;

    if-eqz v3, :cond_2

    .line 118
    sput-boolean v5, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mSupported:Z

    .line 121
    .end local v1    # "feature":Z
    .end local v2    # "pm":Landroid/content/pm/PackageManager;
    :cond_2
    sget-boolean v3, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mSupported:Z

    goto :goto_0

    .line 113
    .restart local v1    # "feature":Z
    .restart local v2    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v0

    .line 114
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    sput-boolean v4, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mSupported:Z

    .line 115
    sget-boolean v3, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mSupported:Z

    goto :goto_0
.end method

.method private createInstance(Landroid/app/Activity;)Ljava/lang/Object;
    .locals 8
    .param p1, "aActivity"    # Landroid/app/Activity;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 138
    iget-object v4, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mStaticMethodCreateInstance:Ljava/lang/reflect/Method;

    if-nez v4, :cond_0

    .line 139
    new-array v1, v7, [Ljava/lang/Class;

    const-class v4, Landroid/app/Activity;

    aput-object v4, v1, v6

    .line 141
    .local v1, "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    :try_start_0
    iget-object v4, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mKlass:Ljava/lang/Class;

    const-string/jumbo v5, "createInstance"

    invoke-virtual {v4, v5, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    iput-object v4, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mStaticMethodCreateInstance:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    .end local v1    # "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    :cond_0
    new-array v2, v7, [Ljava/lang/Object;

    aput-object p1, v2, v6

    .line 148
    .local v2, "parameters":[Ljava/lang/Object;
    const/4 v3, 0x0

    .line 150
    .local v3, "ret":Ljava/lang/Object;
    :try_start_1
    iget-object v4, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mStaticMethodCreateInstance:Ljava/lang/reflect/Method;

    const/4 v5, 0x0

    invoke-virtual {v4, v5, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v3

    .line 158
    .end local v2    # "parameters":[Ljava/lang/Object;
    .end local v3    # "ret":Ljava/lang/Object;
    :goto_0
    return-object v3

    .line 142
    .restart local v1    # "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    :catch_0
    move-exception v0

    .line 143
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    iput-object v3, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mStaticMethodCreateInstance:Ljava/lang/reflect/Method;

    goto :goto_0

    .line 151
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    .end local v1    # "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    .restart local v2    # "parameters":[Ljava/lang/Object;
    .restart local v3    # "ret":Ljava/lang/Object;
    :catch_1
    move-exception v0

    .line 152
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const/4 v3, 0x0

    .line 157
    goto :goto_0

    .line 153
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 154
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const/4 v3, 0x0

    .line 157
    goto :goto_0

    .line 155
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 156
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static getInstance()Lcom/infraware/common/multiwindow/MultiWindowWrapper;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mInstance:Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    if-nez v0, :cond_0

    .line 25
    new-instance v0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    invoke-direct {v0}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;-><init>()V

    sput-object v0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mInstance:Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    .line 27
    :cond_0
    sget-object v0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mInstance:Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    return-object v0
.end method


# virtual methods
.method public getAppRect(Landroid/app/Activity;)Landroid/graphics/Rect;
    .locals 5
    .param p1, "aActivity"    # Landroid/app/Activity;

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0, p1}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->checkSupported(Landroid/app/Activity;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 62
    :cond_0
    :goto_0
    return-object v1

    .line 41
    :cond_1
    invoke-direct {p0, p1}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->checkMultiWindowObj(Landroid/app/Activity;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 44
    iget-object v2, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mMethodGetRect:Ljava/lang/reflect/Method;

    if-nez v2, :cond_2

    .line 46
    :try_start_0
    iget-object v2, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mKlass:Ljava/lang/Class;

    const-string/jumbo v3, "getRect"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mMethodGetRect:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    :cond_2
    const/4 v1, 0x0

    .line 54
    .local v1, "ret":Ljava/lang/Object;
    :try_start_1
    iget-object v2, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mMethodGetRect:Ljava/lang/reflect/Method;

    iget-object v3, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mMultiWindowObj:Ljava/lang/Object;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v1

    .line 62
    .end local v1    # "ret":Ljava/lang/Object;
    :goto_1
    check-cast v1, Landroid/graphics/Rect;

    goto :goto_0

    .line 47
    :catch_0
    move-exception v0

    .line 48
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    iput-object v1, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mMethodGetRect:Ljava/lang/reflect/Method;

    goto :goto_0

    .line 55
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    .restart local v1    # "ret":Ljava/lang/Object;
    :catch_1
    move-exception v0

    .line 56
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const/4 v1, 0x0

    .line 61
    goto :goto_1

    .line 57
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 58
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const/4 v1, 0x0

    .line 61
    goto :goto_1

    .line 59
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 60
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public isMultiWindowSupported(Landroid/app/Activity;)Z
    .locals 1
    .param p1, "aActivity"    # Landroid/app/Activity;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->checkSupported(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 33
    const/4 v0, 0x0

    .line 34
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isNormalWindow(Landroid/app/Activity;)Z
    .locals 6
    .param p1, "aActivity"    # Landroid/app/Activity;

    .prologue
    const/4 v1, 0x1

    .line 67
    invoke-direct {p0, p1}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->checkSupported(Landroid/app/Activity;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 95
    :cond_0
    :goto_0
    return v1

    .line 69
    :cond_1
    invoke-direct {p0, p1}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->checkMultiWindowObj(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 72
    iget-object v3, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mMethodIsNormalWindow:Ljava/lang/reflect/Method;

    if-nez v3, :cond_2

    .line 74
    :try_start_0
    iget-object v3, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mKlass:Ljava/lang/Class;

    const-string/jumbo v4, "isNormalWindow"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    iput-object v3, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mMethodIsNormalWindow:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    :cond_2
    const/4 v1, 0x1

    .line 83
    .local v1, "ret":Z
    :try_start_1
    iget-object v3, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mMethodIsNormalWindow:Ljava/lang/reflect/Method;

    iget-object v4, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mMultiWindowObj:Ljava/lang/Object;

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 84
    .local v2, "retObj":Ljava/lang/Object;
    check-cast v2, Ljava/lang/Boolean;

    .end local v2    # "retObj":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    move-result v1

    goto :goto_0

    .line 75
    .end local v1    # "ret":Z
    :catch_0
    move-exception v0

    .line 76
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->mMethodIsNormalWindow:Ljava/lang/reflect/Method;

    goto :goto_0

    .line 85
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    .restart local v1    # "ret":Z
    :catch_1
    move-exception v0

    .line 86
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const/4 v1, 0x1

    .line 93
    goto :goto_0

    .line 87
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 88
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const/4 v1, 0x1

    .line 93
    goto :goto_0

    .line 89
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 90
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const/4 v1, 0x1

    .line 93
    goto :goto_0

    .line 91
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_4
    move-exception v0

    .line 92
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x1

    goto :goto_0
.end method
