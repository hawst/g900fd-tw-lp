.class Lcom/infraware/common/multiwindow/MWDnDOperator$1;
.super Landroid/view/View$DragShadowBuilder;
.source "MWDnDOperator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/common/multiwindow/MWDnDOperator;->startDragText(Landroid/view/View;Landroid/graphics/Bitmap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private height:I

.field final synthetic this$0:Lcom/infraware/common/multiwindow/MWDnDOperator;

.field private width:I


# direct methods
.method constructor <init>(Lcom/infraware/common/multiwindow/MWDnDOperator;Landroid/view/View;)V
    .locals 0
    .param p2, "x0"    # Landroid/view/View;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/infraware/common/multiwindow/MWDnDOperator$1;->this$0:Lcom/infraware/common/multiwindow/MWDnDOperator;

    invoke-direct {p0, p2}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public onDrawShadow(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    .line 83
    iget-object v0, p0, Lcom/infraware/common/multiwindow/MWDnDOperator$1;->this$0:Lcom/infraware/common/multiwindow/MWDnDOperator;

    # getter for: Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/infraware/common/multiwindow/MWDnDOperator;->access$000(Lcom/infraware/common/multiwindow/MWDnDOperator;)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 84
    return-void
.end method

.method public onProvideShadowMetrics(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 2
    .param p1, "shadowSize"    # Landroid/graphics/Point;
    .param p2, "shadowTouchPoint"    # Landroid/graphics/Point;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/infraware/common/multiwindow/MWDnDOperator$1;->this$0:Lcom/infraware/common/multiwindow/MWDnDOperator;

    # getter for: Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/infraware/common/multiwindow/MWDnDOperator;->access$000(Lcom/infraware/common/multiwindow/MWDnDOperator;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/infraware/common/multiwindow/MWDnDOperator$1;->width:I

    .line 77
    iget-object v0, p0, Lcom/infraware/common/multiwindow/MWDnDOperator$1;->this$0:Lcom/infraware/common/multiwindow/MWDnDOperator;

    # getter for: Lcom/infraware/common/multiwindow/MWDnDOperator;->mShadowBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/infraware/common/multiwindow/MWDnDOperator;->access$000(Lcom/infraware/common/multiwindow/MWDnDOperator;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/infraware/common/multiwindow/MWDnDOperator$1;->height:I

    .line 78
    iget v0, p0, Lcom/infraware/common/multiwindow/MWDnDOperator$1;->width:I

    iget v1, p0, Lcom/infraware/common/multiwindow/MWDnDOperator$1;->height:I

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 79
    iget v0, p0, Lcom/infraware/common/multiwindow/MWDnDOperator$1;->width:I

    div-int/lit8 v0, v0, 0x2

    iget v1, p0, Lcom/infraware/common/multiwindow/MWDnDOperator$1;->height:I

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 80
    return-void
.end method
