.class public interface abstract Lcom/infraware/common/define/CMDefine$ExtType;
.super Ljava/lang/Object;
.source "CMDefine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/common/define/CMDefine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ExtType"
.end annotation


# static fields
.field public static final ASC:Ljava/lang/String; = "asc"

.field public static final CSV:Ljava/lang/String; = "csv"

.field public static final DOC:Ljava/lang/String; = "doc"

.field public static final DOCX:Ljava/lang/String; = "docx"

.field public static final DOT:Ljava/lang/String; = "dot"

.field public static final DOTX:Ljava/lang/String; = "dotx"

.field public static final HWP:Ljava/lang/String; = "hwp"

.field public static final PDF:Ljava/lang/String; = "pdf"

.field public static final POT:Ljava/lang/String; = "pot"

.field public static final POTX:Ljava/lang/String; = "potx"

.field public static final PPS:Ljava/lang/String; = "pps"

.field public static final PPSX:Ljava/lang/String; = "ppsx"

.field public static final PPT:Ljava/lang/String; = "ppt"

.field public static final PPTX:Ljava/lang/String; = "pptx"

.field public static final RTF:Ljava/lang/String; = "rtf"

.field public static final TXT:Ljava/lang/String; = "txt"

.field public static final XLS:Ljava/lang/String; = "xls"

.field public static final XLSX:Ljava/lang/String; = "xlsx"

.field public static final XLT:Ljava/lang/String; = "xlt"

.field public static final XLTX:Ljava/lang/String; = "xltx"

.field public static final XML:Ljava/lang/String; = "xml"

.field public static final ZIP:Ljava/lang/String; = "zip"
