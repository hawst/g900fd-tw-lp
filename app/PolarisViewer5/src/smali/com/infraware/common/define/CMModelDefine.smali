.class public Lcom/infraware/common/define/CMModelDefine;
.super Ljava/lang/Object;
.source "CMModelDefine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/common/define/CMModelDefine$Diotek;,
        Lcom/infraware/common/define/CMModelDefine$SAMSUNG;,
        Lcom/infraware/common/define/CMModelDefine$HOME;,
        Lcom/infraware/common/define/CMModelDefine$S;,
        Lcom/infraware/common/define/CMModelDefine$B;,
        Lcom/infraware/common/define/CMModelDefine$I;
    }
.end annotation


# static fields
.field public static final ACER_VENDOR:Ljava/lang/String; = "FV13"

.field public static final ASUS_VENDOR:Ljava/lang/String; = "FV06"

.field public static final CUSTOM_BOOKMARK_PATH:Ljava/lang/String; = ""

.field public static final CUSTOM_PRINT_PATH:Ljava/lang/String;

.field public static final CUSTOM_TEMP_PATH:Ljava/lang/String;

.field public static final HTC_VENDOR:Ljava/lang/String; = "FV05"

.field public static final LG_VENDOR:Ljava/lang/String; = "FV02"

.field public static final PACKAGE_NAME:Ljava/lang/String; = "com.infraware.polarisviewer5"

.field public static final PACKAGE_NAME_OFFICE:Ljava/lang/String; = "com.infraware.polarisoffice5"

.field public static final PANASONIC_VENDOR:Ljava/lang/String; = "FV14"

.field public static final PANTECT_VENDOR:Ljava/lang/String; = "FV04"

.field public static final SAMSUNG_VENDOR:Ljava/lang/String; = "FT03"

.field public static final SCHEME_FAVORITE:Ljava/lang/String; = "polaris_office_favorite://"

.field public static final SCHEME_RECENT:Ljava/lang/String; = "polaris_office_recent://"

.field public static final SKTELESYS_VENDOR:Ljava/lang/String; = "FV09"

.field public static final STANDARD_VENDOR:Ljava/lang/String; = "FV01"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->TEMP_PATH:Ljava/lang/String;

    sput-object v0, Lcom/infraware/common/define/CMModelDefine;->CUSTOM_TEMP_PATH:Ljava/lang/String;

    .line 30
    sget-object v0, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->PRINT_PATH:Ljava/lang/String;

    sput-object v0, Lcom/infraware/common/define/CMModelDefine;->CUSTOM_PRINT_PATH:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 656
    return-void
.end method

.method public static getDocumentURI()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 840
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 841
    const-string/jumbo v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 843
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getExtraMimeType(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p0, "ext"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 854
    const/4 v4, 0x6

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v4, "mp2"

    aput-object v4, v0, v6

    const-string/jumbo v4, "aac"

    aput-object v4, v0, v7

    const-string/jumbo v4, "mkv"

    aput-object v4, v0, v8

    const-string/jumbo v4, "flv"

    aput-object v4, v0, v9

    const-string/jumbo v4, "snb"

    aput-object v4, v0, v10

    const/4 v4, 0x5

    const-string/jumbo v5, "3gp"

    aput-object v5, v0, v4

    .line 863
    .local v0, "extList":[Ljava/lang/String;
    const/4 v4, 0x6

    new-array v3, v4, [Ljava/lang/String;

    const-string/jumbo v4, "audio/mpeg"

    aput-object v4, v3, v6

    const-string/jumbo v4, "audio/x-aac"

    aput-object v4, v3, v7

    const-string/jumbo v4, "video/mkv"

    aput-object v4, v3, v8

    const-string/jumbo v4, "video/flv"

    aput-object v4, v3, v9

    const-string/jumbo v4, "application/snb"

    aput-object v4, v3, v10

    const/4 v4, 0x5

    const-string/jumbo v5, "video/3gp"

    aput-object v5, v3, v4

    .line 872
    .local v3, "typeList":[Ljava/lang/String;
    array-length v4, v0

    array-length v5, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 873
    .local v2, "nCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 875
    aget-object v4, v0, v1

    invoke-virtual {p0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 876
    aget-object v4, v3, v1

    .line 879
    :goto_1
    return-object v4

    .line 873
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 879
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static isATT()Z
    .locals 3

    .prologue
    .line 776
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "FT03"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 778
    const/4 v0, 0x0

    .line 780
    .local v0, "cscCode":Ljava/lang/String;
    :try_start_0
    const-string/jumbo v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 783
    if-nez v0, :cond_0

    .line 784
    const-string/jumbo v0, ""

    .line 787
    :cond_0
    :goto_0
    const-string/jumbo v1, "AIO"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "ATT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 789
    :cond_1
    const/4 v1, 0x1

    .line 793
    :goto_1
    return v1

    .line 781
    :catch_0
    move-exception v1

    .line 783
    if-nez v0, :cond_0

    .line 784
    const-string/jumbo v0, ""

    goto :goto_0

    .line 783
    :catchall_0
    move-exception v1

    if-nez v0, :cond_2

    .line 784
    const-string/jumbo v0, ""

    :cond_2
    throw v1

    .line 793
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static isChina()Z
    .locals 3

    .prologue
    .line 730
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "FT03"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 732
    const/4 v0, 0x0

    .line 735
    .local v0, "cscCode":Ljava/lang/String;
    :try_start_0
    const-string/jumbo v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 738
    if-nez v0, :cond_0

    .line 739
    const-string/jumbo v0, ""

    .line 742
    :cond_0
    :goto_0
    const-string/jumbo v1, "CHN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "CHM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "CHU"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "CTC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "CHC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 747
    :cond_1
    const/4 v1, 0x1

    .line 751
    :goto_1
    return v1

    .line 736
    :catch_0
    move-exception v1

    .line 738
    if-nez v0, :cond_0

    .line 739
    const-string/jumbo v0, ""

    goto :goto_0

    .line 738
    :catchall_0
    move-exception v1

    if-nez v0, :cond_2

    .line 739
    const-string/jumbo v0, ""

    :cond_2
    throw v1

    .line 751
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static isDocumentsFile(Ljava/lang/String;)Z
    .locals 1
    .param p0, "type"    # Ljava/lang/String;

    .prologue
    .line 847
    if-nez p0, :cond_0

    .line 848
    const/4 v0, 0x0

    .line 850
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isEpsonPrinter(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 797
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "FT03"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 799
    const-string/jumbo v0, "SC-03E SC-02E"

    .line 800
    .local v0, "epson_support_model":Ljava/lang/String;
    const-string/jumbo v1, "SC-03E SC-02E"

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 801
    const/4 v1, 0x1

    .line 803
    .end local v0    # "epson_support_model":Ljava/lang/String;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isIRAN()Z
    .locals 3

    .prologue
    .line 755
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "FT03"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 757
    const/4 v0, 0x0

    .line 763
    .local v0, "cscCode":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 764
    const-string/jumbo v0, ""

    .line 767
    :cond_0
    const-string/jumbo v1, "THR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 768
    const/4 v1, 0x1

    .line 772
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isKkDisuseExternal()Z
    .locals 2

    .prologue
    .line 910
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "FT03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/infraware/common/util/Utils;->isKKOver()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 911
    const/4 v0, 0x1

    .line 912
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isParticularFile(Ljava/lang/String;)Z
    .locals 7
    .param p0, "fileMimeType"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 884
    if-nez p0, :cond_1

    .line 905
    :cond_0
    :goto_0
    return v3

    .line 889
    :cond_1
    const/4 v5, 0x3

    :try_start_0
    new-array v2, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "audio/x-pn-realaudio"

    aput-object v6, v2, v5

    const/4 v5, 0x1

    const-string/jumbo v6, "video/flv"

    aput-object v6, v2, v5

    const/4 v5, 0x2

    const-string/jumbo v6, "video/x-msvideo"

    aput-object v6, v2, v5

    .line 895
    .local v2, "particularFileMimeTypeList":[Ljava/lang/String;
    array-length v1, v2

    .line 896
    .local v1, "nCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_0

    .line 898
    aget-object v5, v2, v0

    invoke-virtual {p0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_2

    move v3, v4

    .line 899
    goto :goto_0

    .line 896
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 902
    .end local v0    # "i":I
    .end local v1    # "nCount":I
    .end local v2    # "particularFileMimeTypeList":[Ljava/lang/String;
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method public static isShowConnetNetWorkPopup(Landroid/content/Context;I)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 221
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine;->isChina()Z

    move-result v3

    if-ne v3, v2, :cond_0

    .line 224
    const/16 v3, 0x134

    if-ne p1, v3, :cond_1

    .line 237
    :cond_0
    :goto_0
    return v1

    .line 229
    :cond_1
    const/4 v0, 0x0

    .line 230
    .local v0, "notShowPopup":Z
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v3

    invoke-virtual {v3, p0, p1, v1}, Lcom/infraware/common/config/RuntimeConfig;->getBooleanPreference(Landroid/content/Context;IZ)Z

    move-result v0

    .line 231
    if-eq v0, v2, :cond_0

    move v1, v2

    .line 234
    goto :goto_0
.end method

.method public static isSupportBroadcast(Ljava/lang/String;)Z
    .locals 4
    .param p0, "ext"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 828
    new-array v0, v2, [Ljava/lang/String;

    .line 830
    .local v0, "documentList":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_0

    .line 832
    aget-object v3, v0, v1

    invoke-virtual {p0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 833
    const/4 v2, 0x1

    .line 836
    :cond_0
    return v2

    .line 830
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static isSupportDocument(Ljava/lang/String;)Z
    .locals 7
    .param p0, "ext"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 808
    const/16 v5, 0x9

    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v5, "dot"

    aput-object v5, v0, v4

    const-string/jumbo v5, "dotx"

    aput-object v5, v0, v3

    const/4 v5, 0x2

    const-string/jumbo v6, "xlt"

    aput-object v6, v0, v5

    const/4 v5, 0x3

    const-string/jumbo v6, "xltx"

    aput-object v6, v0, v5

    const/4 v5, 0x4

    const-string/jumbo v6, "pot"

    aput-object v6, v0, v5

    const/4 v5, 0x5

    const-string/jumbo v6, "potx"

    aput-object v6, v0, v5

    const/4 v5, 0x6

    const-string/jumbo v6, "pps"

    aput-object v6, v0, v5

    const/4 v5, 0x7

    const-string/jumbo v6, "ppsx"

    aput-object v6, v0, v5

    const/16 v5, 0x8

    const-string/jumbo v6, "csv"

    aput-object v6, v0, v5

    .line 811
    .local v0, "documentList":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v5, v0

    if-ge v2, v5, :cond_2

    .line 813
    aget-object v5, v0, v2

    invoke-virtual {p0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 824
    :cond_0
    :goto_1
    return v3

    .line 811
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 817
    :cond_2
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HWP_EDIT_SUPPORT()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 819
    const-string/jumbo v1, "hwp"

    .line 820
    .local v1, "hwp":Ljava/lang/String;
    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .end local v1    # "hwp":Ljava/lang/String;
    :cond_3
    move v3, v4

    .line 824
    goto :goto_1
.end method
