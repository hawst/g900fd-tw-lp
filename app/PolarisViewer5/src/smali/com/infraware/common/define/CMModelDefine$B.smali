.class public Lcom/infraware/common/define/CMModelDefine$B;
.super Ljava/lang/Object;
.source "CMModelDefine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/common/define/CMModelDefine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "B"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final HAS_EDITOR()Z
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    return v0
.end method

.method public static final HIDE_INDICATOR_BAR()Z
    .locals 1

    .prologue
    .line 158
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_H()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    const/4 v0, 0x1

    .line 161
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final SAVE_2007()Z
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x1

    return v0
.end method

.method public static final SHOW_FILE_LIST_DIALOG()Z
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x1

    return v0
.end method

.method public static final SHOW_FILE_PATH()Z
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x1

    return v0
.end method

.method public static final SHOW_LAND_INDICATOR()Z
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x1

    return v0
.end method

.method public static final SHOW_RESUME_KEYPAD()Z
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    return v0
.end method

.method public static final SHOW_SUCCESS_RESULT()Z
    .locals 1

    .prologue
    .line 151
    const/4 v0, 0x0

    return v0
.end method

.method public static final USE_ANDROID_KEYPAD()Z
    .locals 2

    .prologue
    .line 179
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "FT03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    const/4 v0, 0x1

    .line 182
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final USE_AUDIO_ONESHOT()Z
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x1

    return v0
.end method

.method public static final USE_BUBBLE_POPUP()Z
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x1

    return v0
.end method

.method public static final USE_CHECK_MEMORYLEAK()Z
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x1

    return v0
.end method

.method public static final USE_DEFAULT_FORMAT()Z
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    return v0
.end method

.method public static final USE_DOCUMENT_CHANGE()Z
    .locals 1

    .prologue
    .line 170
    const/4 v0, 0x0

    return v0
.end method

.method public static final USE_EMAIL_DATABASE()Z
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    return v0
.end method

.method public static final USE_EMAIL_FILTER()Z
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x1

    return v0
.end method

.method public static final USE_EXIF_INFO()Z
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x1

    return v0
.end method

.method public static final USE_EXTERNAL_DISPLAY(Landroid/view/View$OnClickListener;)Z
    .locals 3
    .param p0, "onClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 109
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "FT03"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 111
    const/4 v0, 0x0

    .line 117
    .local v0, "board":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 118
    const-string/jumbo v0, ""

    .line 121
    :cond_0
    const-string/jumbo v1, "MSM8960"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 122
    const/4 v1, 0x0

    .line 126
    .end local v0    # "board":Ljava/lang/String;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static final USE_FILE_MANAGER()Z
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x0

    return v0
.end method

.method public static final USE_FINISH_BACKKEY()Z
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x1

    return v0
.end method

.method public static final USE_FUNCTION_POPUP()Z
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x1

    return v0
.end method

.method public static final USE_GOOGLE_PRINT(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 193
    invoke-static {}, Lcom/infraware/common/util/Utils;->isKKOver()Z

    move-result v2

    if-nez v2, :cond_1

    .line 203
    :cond_0
    :goto_0
    return v0

    .line 196
    :cond_1
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_S5()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_S5_MINI()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_S5_ZOOM()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_Kleos()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_Berluti()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_Arctic()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_Fortuna()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move v0, v1

    .line 198
    goto :goto_0

    .line 200
    :cond_3
    invoke-static {p0}, Lcom/infraware/office/baseframe/porting/EvPrintHelper;->isSupportMobilePrint(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 203
    goto :goto_0
.end method

.method public static final USE_HELP_MENU()Z
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x1

    return v0
.end method

.method public static final USE_HOVERING(Landroid/content/Context;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 130
    new-instance v3, Landroid/view/View;

    invoke-direct {v3, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 132
    .local v3, "v":Landroid/view/View;
    :try_start_0
    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string/jumbo v7, "pen_hovering"

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-ne v6, v4, :cond_1

    move v2, v4

    .line 133
    .local v2, "isHoveringOn":Z
    :goto_0
    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string/jumbo v7, "com.sec.feature.hovering_ui"

    invoke-virtual {v6, v7}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v6

    if-ne v6, v4, :cond_2

    move v1, v4

    .line 134
    .local v1, "hasHoveringFeature":Z
    :goto_1
    if-eqz v1, :cond_0

    if-nez v2, :cond_3

    :cond_0
    move v4, v5

    .line 146
    .end local v1    # "hasHoveringFeature":Z
    .end local v2    # "isHoveringOn":Z
    :goto_2
    return v4

    :cond_1
    move v2, v5

    .line 132
    goto :goto_0

    .restart local v2    # "isHoveringOn":Z
    :cond_2
    move v1, v5

    .line 133
    goto :goto_1

    .line 137
    .restart local v1    # "hasHoveringFeature":Z
    :cond_3
    const/4 v6, 0x2

    invoke-virtual {v3, v6}, Landroid/view/View;->setHoverPopupType(I)V

    .line 138
    invoke-virtual {v3}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    .line 139
    invoke-virtual {v3}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v6

    const/16 v7, 0x320

    invoke-virtual {v6, v7}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_2

    .line 140
    .end local v1    # "hasHoveringFeature":Z
    .end local v2    # "isHoveringOn":Z
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/NoSuchMethodError;
    move v4, v5

    .line 141
    goto :goto_2

    .line 142
    .end local v0    # "e":Ljava/lang/NoSuchMethodError;
    :catch_1
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    move v4, v5

    .line 143
    goto :goto_2
.end method

.method public static final USE_HWP_EDIT_SUPPORT()Z
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x1

    return v0
.end method

.method public static final USE_HYPERLINK()Z
    .locals 2

    .prologue
    .line 80
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "FT03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    const/4 v0, 0x0

    .line 83
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static final USE_LIVE_UPDATE()Z
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    return v0
.end method

.method public static final USE_LOW_STORAGE_CHECK()Z
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    return v0
.end method

.method public static final USE_MULTI_INSTANCE()Z
    .locals 2

    .prologue
    .line 208
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "FT03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    const/4 v0, 0x1

    .line 211
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final USE_MULTI_LINE_VIEW()Z
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    return v0
.end method

.method public static final USE_ONLINE_UPDATE()Z
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x1

    return v0
.end method

.method public static final USE_POLARIS_PRINT()Z
    .locals 1

    .prologue
    .line 186
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_H()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_H_NEO()Z

    move-result v0

    if-nez v0, :cond_0

    .line 187
    const/4 v0, 0x1

    .line 189
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final USE_PRINT()Z
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x1

    return v0
.end method

.method public static final USE_SETTING_AUTOFIT()Z
    .locals 2

    .prologue
    .line 87
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "FT03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    const/4 v0, 0x1

    .line 90
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final USE_SETTING_MENU()Z
    .locals 1

    .prologue
    .line 174
    const/4 v0, 0x1

    return v0
.end method

.method public static final USE_SHEET_TITLE_NAME()Z
    .locals 1

    .prologue
    .line 168
    const/4 v0, 0x0

    return v0
.end method

.method public static final USE_STANDARD_UI()Z
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x1

    return v0
.end method

.method public static final USE_TAB_VIEW_LIST()Z
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x1

    return v0
.end method

.method public static final USE_TEXTMARK_REVERSE()Z
    .locals 2

    .prologue
    .line 74
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "FT03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    const/4 v0, 0x1

    .line 77
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static USE_THUMBNAILOFSLIDESHOW()Z
    .locals 2

    .prologue
    .line 214
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "FT03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    const/4 v0, 0x0

    .line 216
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static final USE_TWO_LINE_EDIT()Z
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x1

    return v0
.end method

.method public static final USE_WEBDAV()Z
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x1

    return v0
.end method

.method public static final USE_WEB_LOAD()Z
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x1

    return v0
.end method

.method public static final USE_WEB_VIEW()Z
    .locals 2

    .prologue
    .line 97
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "FT03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    const/4 v0, 0x0

    .line 100
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
