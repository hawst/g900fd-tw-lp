.class public Lcom/infraware/common/define/CMDefine$ExtraKey;
.super Ljava/lang/Object;
.source "CMDefine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/common/define/CMDefine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ExtraKey"
.end annotation


# static fields
.field public static final AUTO_SAVE_TEST:Ljava/lang/String; = "key_auto_save_test"

.field public static final BROADCAST_FILE:Ljava/lang/String; = "key_broadcast_file_path"

.field public static final BROADCAST_MASTER:Ljava/lang/String; = "key_broadcast_master"

.field public static final BROADCAST_MODE:Ljava/lang/String; = "key_broadcast_mode"

.field public static final CONTENT_MODE:Ljava/lang/String; = "key_content_mode"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "key_content_type"

.field public static final CURRENT_FILE:Ljava/lang/String; = "key_current_file"

.field public static final CURRENT_FILE_DOC_PAGE:Ljava/lang/String; = "key_current_file_doc_page"

.field public static final CURRENT_FILE_DOC_TYPE:Ljava/lang/String; = "key_current_file_doc_type"

.field public static final CURRENT_FILE_DOC_WORDS:Ljava/lang/String; = "key_current_file_doc_words"

.field public static final INTERNAL_MODE:Ljava/lang/String; = "key_interanl_mode"

.field public static final NAVI_ACTION:Ljava/lang/String; = "key_navi_action"

.field public static final NEW_FILE:Ljava/lang/String; = "key_new_file"

.field public static final NEW_PPT_TEMPLATE:Ljava/lang/String; = "key_ppt_template"

.field public static final OPEN_FILE:Ljava/lang/String; = "key_filename"

.field public static final PPS_FILE:Ljava/lang/String; = "key_pps_file"

.field public static final PPS_FILE_PATH:Ljava/lang/String; = "key_pps_file_path"

.field public static final PRINT_ACCOUNT:Ljava/lang/String; = "print_account"

.field public static final SAVE_MENU_TYPE:Ljava/lang/String; = "key_save_menu"

.field public static final SAVE_PENDRAW:Ljava/lang/String; = "key_save_include_pendraw"

.field public static final SEARCH_KEY:Ljava/lang/String; = "search-key"

.field public static final SYNC_CONTENTSRC:Ljava/lang/String; = "key_contentsrc"

.field public static final SYNC_CURRENT_FILE:Ljava/lang/String; = "key_sync_current_file"

.field public static final SYNC_FILE_ID:Ljava/lang/String; = "key_sync_file_id"

.field public static final SYNC_FILE_PATH:Ljava/lang/String; = "key_sync_file_path"

.field public static final SYNC_SAVEAS:Ljava/lang/String; = "key_saveas"

.field public static final SYNC_SERVIER_TYPE:Ljava/lang/String; = "key_sync_service_type"

.field public static final SYNC_SIZE:Ljava/lang/String; = "key_sync_size"

.field public static final SYNC_STORAGE_ID:Ljava/lang/String; = "key_sync_storage_id"

.field public static final SYNC_TARGET_ID:Ljava/lang/String; = "key_sync_target_id"

.field public static final SYNC_UPDATETIME:Ljava/lang/String; = "key_sync_updatetime"

.field public static final TEMPLATE_FILE:Ljava/lang/String; = "key_template_file"

.field public static final TXT_SAVE_MODE:Ljava/lang/String; = "key_save_mode"

.field public static final VERSION_NO:Ljava/lang/String; = "key_version"


# instance fields
.field final synthetic this$0:Lcom/infraware/common/define/CMDefine;


# direct methods
.method public constructor <init>(Lcom/infraware/common/define/CMDefine;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/infraware/common/define/CMDefine$ExtraKey;->this$0:Lcom/infraware/common/define/CMDefine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
