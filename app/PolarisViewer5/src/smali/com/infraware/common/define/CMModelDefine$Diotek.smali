.class public Lcom/infraware/common/define/CMModelDefine$Diotek;
.super Ljava/lang/Object;
.source "CMModelDefine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/common/define/CMModelDefine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Diotek"
.end annotation


# static fields
.field private static final COMPATIBLE_DIOTEK_DIC_SERVICE_VERSION:I = 0x190

.field private static m_initialized:Z

.field private static m_installed:Z

.field private static m_loadComplete:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 657
    sput-boolean v0, Lcom/infraware/common/define/CMModelDefine$Diotek;->m_initialized:Z

    .line 658
    sput-boolean v0, Lcom/infraware/common/define/CMModelDefine$Diotek;->m_installed:Z

    .line 659
    sput-boolean v0, Lcom/infraware/common/define/CMModelDefine$Diotek;->m_loadComplete:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 656
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final USE_DICTIONARY_SEARCH(Landroid/content/Context;)Z
    .locals 11
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 663
    sget-boolean v8, Lcom/infraware/common/define/CMModelDefine$Diotek;->m_initialized:Z

    if-eqz v8, :cond_0

    .line 664
    sget-boolean v8, Lcom/infraware/common/define/CMModelDefine$Diotek;->m_installed:Z

    .line 692
    :goto_0
    return v8

    .line 666
    :cond_0
    sput-boolean v9, Lcom/infraware/common/define/CMModelDefine$Diotek;->m_initialized:Z

    .line 668
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 669
    .local v5, "manager":Landroid/content/pm/PackageManager;
    const/16 v8, 0x2000

    invoke-virtual {v5, v8}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v0

    .line 670
    .local v0, "appInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    const/4 v3, 0x0

    .local v3, "j":I
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    .local v4, "jend":I
    :goto_1
    if-ge v3, v4, :cond_3

    .line 671
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/PackageInfo;

    .line 673
    .local v6, "pi":Landroid/content/pm/PackageInfo;
    iget-object v8, v6, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string/jumbo v9, "com.diotek.diodict3.service"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 675
    iget-object v7, v6, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 676
    .local v7, "version":Ljava/lang/String;
    const-string/jumbo v8, "."

    const-string/jumbo v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    .line 678
    :try_start_0
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 680
    .local v1, "convertedVersionName":I
    const/16 v8, 0x190

    if-lt v1, v8, :cond_1

    .line 681
    const/4 v8, 0x1

    sput-boolean v8, Lcom/infraware/common/define/CMModelDefine$Diotek;->m_installed:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 689
    .end local v1    # "convertedVersionName":I
    :goto_2
    sget-boolean v8, Lcom/infraware/common/define/CMModelDefine$Diotek;->m_installed:Z

    goto :goto_0

    .line 683
    .restart local v1    # "convertedVersionName":I
    :cond_1
    const/4 v8, 0x0

    :try_start_1
    sput-boolean v8, Lcom/infraware/common/define/CMModelDefine$Diotek;->m_installed:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 684
    .end local v1    # "convertedVersionName":I
    :catch_0
    move-exception v2

    .line 686
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 687
    sput-boolean v10, Lcom/infraware/common/define/CMModelDefine$Diotek;->m_installed:Z

    goto :goto_2

    .line 670
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v7    # "version":Ljava/lang/String;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 692
    .end local v6    # "pi":Landroid/content/pm/PackageInfo;
    :cond_3
    sget-boolean v8, Lcom/infraware/common/define/CMModelDefine$Diotek;->m_installed:Z

    goto :goto_0
.end method

.method public static getExtraAppName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 725
    const-string/jumbo v0, "fromAppName"

    return-object v0
.end method

.method public static getExtraTextKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 721
    const-string/jumbo v0, "isSendToText"

    return-object v0
.end method

.method public static initialize()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 697
    sput-boolean v0, Lcom/infraware/common/define/CMModelDefine$Diotek;->m_initialized:Z

    .line 698
    sput-boolean v0, Lcom/infraware/common/define/CMModelDefine$Diotek;->m_installed:Z

    .line 699
    sput-boolean v0, Lcom/infraware/common/define/CMModelDefine$Diotek;->m_loadComplete:Z

    .line 700
    return-void
.end method

.method public static isLoadComplete()Z
    .locals 1

    .prologue
    .line 709
    sget-boolean v0, Lcom/infraware/common/define/CMModelDefine$Diotek;->m_loadComplete:Z

    return v0
.end method

.method public static isSendTextFileAsExtraText(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 717
    const/4 v0, 0x0

    return v0
.end method

.method public static setLoadComplete(Z)V
    .locals 0
    .param p0, "value"    # Z

    .prologue
    .line 704
    sput-boolean p0, Lcom/infraware/common/define/CMModelDefine$Diotek;->m_loadComplete:Z

    .line 705
    return-void
.end method
