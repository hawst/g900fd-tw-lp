.class public Lcom/infraware/common/define/CMModelDefine$I;
.super Ljava/lang/Object;
.source "CMModelDefine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/common/define/CMModelDefine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "I"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final DIALOG_THEME()I
    .locals 3

    .prologue
    const/4 v0, 0x4

    .line 48
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "FT03"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50
    invoke-static {}, Lcom/infraware/common/util/Utils;->isL()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51
    const/4 v0, 0x5

    .line 56
    :cond_0
    return v0
.end method

.method public static final MAX_FILE_LENGTH()I
    .locals 1

    .prologue
    .line 43
    const/16 v0, 0x3c

    return v0
.end method

.method public static final MAX_RECENT_COUNT()I
    .locals 1

    .prologue
    .line 44
    const/16 v0, 0x14

    return v0
.end method

.method public static final MIN_FAST_SCROLL_COUNT()I
    .locals 1

    .prologue
    .line 45
    const/16 v0, 0x1e

    return v0
.end method

.method public static final REGISTER_TYPE()I
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x2

    return v0
.end method
