.class public Lcom/infraware/common/define/CMDefine$Preference;
.super Ljava/lang/Object;
.source "CMDefine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/common/define/CMDefine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Preference"
.end annotation


# static fields
.field public static final AUTO_FIT:I = 0xe

.field public static final CREATE_BACKUP:I = 0xc

.field public static final DEFAULT_FORMAT:I = 0xb

.field public static final DICTIONARY_EN:I = 0x5b

.field public static final DICTIONARY_JA:I = 0x5d

.field public static final DICTIONARY_KO:I = 0x5a

.field public static final DICTIONARY_ZH:I = 0x5c

.field public static final HAND_ANIMATION:I = 0x60

.field public static final LAST_PATH:I = 0x3

.field public static final LAST_STORAGE:I = 0x4

.field public static final LAST_TAB:I = 0x1

.field public static final LAST_TYPE:I = 0x2

.field public static final LICENSE_AGREE:I = 0x55

.field public static final LICENSE_SHOWED:I = 0x54

.field public static final NAVIGATION_OVERLAY:I = 0x5f

.field public static final NOT_SHOW_CONNETPOPUP_COPYRIGHT:I = 0x12f

.field public static final NOT_SHOW_CONNETPOPUP_EULA:I = 0x12e

.field public static final NOT_SHOW_CONNETPOPUP_INFRA_HOME:I = 0x136

.field public static final NOT_SHOW_CONNETPOPUP_MAIN_HELP:I = 0x130

.field public static final NOT_SHOW_CONNETPOPUP_OFFICE_DOWNLOAD_SAMUNGAPPS:I = 0x133

.field public static final NOT_SHOW_CONNETPOPUP_RUN_GOOGLE_SEARCH:I = 0x135

.field public static final NOT_SHOW_CONNETPOPUP_RUN_SHARE:I = 0x134

.field public static final NOT_SHOW_CONNETPOPUP_SAMUNGAPPS:I = 0x132

.field public static final NOT_SHOW_CONNETPOPUP_SEND_EMAIL:I = 0x131

.field public static final NOT_SHOW_CONNETPOPUP_UPDATE:I = 0x12d

.field public static final RECENT_SHAPE:I = 0xd4

.field public static final RECENT_SHAPE_2003:I = 0xd5

.field public static final RECENT_SHAPE_2003_PPT:I = 0xd3

.field public static final RECENT_SHAPE_PPT:I = 0xd2

.field public static final RECENT_SYMBOL:I = 0xc8

.field public static final SHOW_EXT:I = 0xd

.field public static final SORT_DATE_ALLTYPES:I = 0x34

.field public static final SORT_DATE_BROWSER:I = 0x2a

.field public static final SORT_DATE_FAVORITE:I = 0x49

.field public static final SORT_DATE_TYPES:I = 0x3e

.field public static final SORT_FIELD_ALLTYPES:I = 0x20

.field public static final SORT_FIELD_BROWSER:I = 0x1f

.field public static final SORT_FIELD_FAVORITE:I = 0x22

.field public static final SORT_FIELD_TYPES:I = 0x21

.field public static final SORT_NAME_ALLTYPES:I = 0x33

.field public static final SORT_NAME_BROWSER:I = 0x29

.field public static final SORT_NAME_FAVORITE:I = 0x48

.field public static final SORT_NAME_TYPES:I = 0x3d

.field public static final SORT_PATH_ALLTYPES:I = 0x36

.field public static final SORT_PATH_FAVORITE:I = 0x4b

.field public static final SORT_PATH_TYPES:I = 0x40

.field public static final SORT_RECENT_FAVORITE:I = 0x47

.field public static final SORT_SIZE_ALLTYPES:I = 0x35

.field public static final SORT_SIZE_BROWSER:I = 0x2b

.field public static final SORT_SIZE_FAVORITE:I = 0x4a

.field public static final SORT_SIZE_TYPES:I = 0x3f

.field public static final SORT_TYPE_ALLTYPES:I = 0x37

.field public static final SORT_TYPE_BROWSER:I = 0x2c

.field public static final SORT_TYPE_FAVORITE:I = 0x4c

.field public static final SORT_TYPE_TYPES:I = 0x41

.field public static final START_FOLDER:I = 0x5e

.field public static final STORAGE_INDEX_00:I = 0x64

.field public static final STORAGE_INDEX_01:I = 0x65

.field public static final STORAGE_INDEX_02:I = 0x66

.field public static final STORAGE_INDEX_03:I = 0x67

.field public static final STORAGE_INDEX_04:I = 0x68

.field public static final STORAGE_INDEX_05:I = 0x69

.field public static final STORAGE_INDEX_06:I = 0x6a

.field public static final STORAGE_INDEX_07:I = 0x6b

.field public static final STORAGE_INDEX_08:I = 0x6c

.field public static final STORAGE_INDEX_09:I = 0x6d

.field public static final STORAGE_INDEX_10:I = 0x6e

.field public static final STORAGE_INDEX_11:I = 0x6f

.field public static final STORAGE_INDEX_12:I = 0x70

.field public static final STORAGE_INDEX_13:I = 0x71

.field public static final STORAGE_INDEX_14:I = 0x72

.field public static final STORAGE_INDEX_15:I = 0x73

.field public static final STORAGE_INDEX_16:I = 0x74

.field public static final STORAGE_INDEX_17:I = 0x75

.field public static final STORAGE_INDEX_18:I = 0x76

.field public static final STORAGE_INDEX_19:I = 0x77

.field public static final STORAGE_INDEX_20:I = 0x78

.field public static final STORAGE_INDEX_21:I = 0x79

.field public static final STORAGE_INDEX_22:I = 0x7a

.field public static final STORAGE_INDEX_23:I = 0x7b

.field public static final STORAGE_INDEX_24:I = 0x7c

.field public static final SYNC_MAX_SIZE:I = 0x53

.field public static final SYNC_PERIOD:I = 0x51

.field public static final SYNC_USE:I = 0x50

.field public static final SYNC_WIFI_ONLY:I = 0x52

.field public static final TEXT_ENCODING:I = 0x17

.field public static final TEXT_FONT_SIZE:I = 0x15

.field public static final TEXT_FONT_TYPE:I = 0x18

.field public static final TEXT_THEME:I = 0x16

.field public static final VIEW_SPLASH:I = 0x61


# instance fields
.field final synthetic this$0:Lcom/infraware/common/define/CMDefine;


# direct methods
.method public constructor <init>(Lcom/infraware/common/define/CMDefine;)V
    .locals 0

    .prologue
    .line 369
    iput-object p1, p0, Lcom/infraware/common/define/CMDefine$Preference;->this$0:Lcom/infraware/common/define/CMDefine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
