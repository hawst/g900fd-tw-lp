.class public Lcom/infraware/common/define/CMDefine$PreferenceUserPattern;
.super Ljava/lang/Object;
.source "CMDefine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/common/define/CMDefine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PreferenceUserPattern"
.end annotation


# static fields
.field public static final APK_OPEN_ALL:I = 0x42e

.field public static final AVI_OPEN_ALL:I = 0x42d

.field public static final CLOUD01_CONN:I = 0x432

.field public static final CLOUD02_CONN:I = 0x433

.field public static final CLOUD03_CONN:I = 0x434

.field public static final CLOUD04_CONN:I = 0x435

.field public static final CLOUD05_CONN:I = 0x436

.field public static final DOC03_CREATE_ALL:I = 0x3eb

.field public static final DOC03_OPEN_ALL:I = 0x3ec

.field public static final DOC03_OPEN_FAILED:I = 0x3ed

.field public static final DOC03_SAVE_ALL:I = 0x3ee

.field public static final DOC03_SAVE_FAILED:I = 0x3ef

.field public static final DOC07_CREATE_ALL:I = 0x3f0

.field public static final DOC07_OPEN_ALL:I = 0x3f1

.field public static final DOC07_OPEN_FAILED:I = 0x3f2

.field public static final DOC07_SAVE_ALL:I = 0x3f3

.field public static final DOC07_SAVE_FAILED:I = 0x3f4

.field public static final DOC10_CREATE_ALL:I = 0x3f5

.field public static final DOC10_OPEN_ALL:I = 0x3f6

.field public static final DOC10_OPEN_FAILED:I = 0x3f7

.field public static final DOC10_SAVE_ALL:I = 0x3f8

.field public static final DOC10_SAVE_FAILED:I = 0x3f9

.field public static final END_FAILED:I = 0x437

.field public static final ETC_CREATE_ALL:I = 0x427

.field public static final ETC_OPEN_ALL:I = 0x428

.field public static final ETC_OPEN_FAILED:I = 0x429

.field public static final ETC_SAVE_ALL:I = 0x42a

.field public static final ETC_SAVE_FAILED:I = 0x42b

.field public static final EXEC_DIRECTLY:I = 0x3e9

.field public static final EXEC_INDIRECTLY:I = 0x3ea

.field public static final HWP_CREATE_ALL:I = 0x422

.field public static final HWP_OPEN_ALL:I = 0x423

.field public static final HWP_OPEN_FAILED:I = 0x424

.field public static final HWP_SAVE_ALL:I = 0x425

.field public static final HWP_SAVE_FAILED:I = 0x426

.field public static final MGT_FILE:I = 0x431

.field public static final MP3_OPEN_ALL:I = 0x42c

.field public static final PDF_CREATE_ALL:I = 0x418

.field public static final PDF_OPEN_ALL:I = 0x419

.field public static final PDF_OPEN_FAILED:I = 0x41a

.field public static final PDF_SAVE_ALL:I = 0x41b

.field public static final PDF_SAVE_FAILED:I = 0x41c

.field public static final PPT03_CREATE_ALL:I = 0x3fa

.field public static final PPT03_OPEN_ALL:I = 0x3fb

.field public static final PPT03_OPEN_FAILED:I = 0x3fc

.field public static final PPT03_SAVE_ALL:I = 0x3fd

.field public static final PPT03_SAVE_FAILED:I = 0x3fe

.field public static final PPT07_CREATE_ALL:I = 0x3ff

.field public static final PPT07_OPEN_ALL:I = 0x400

.field public static final PPT07_OPEN_FAILED:I = 0x401

.field public static final PPT07_SAVE_ALL:I = 0x402

.field public static final PPT07_SAVE_FAILED:I = 0x403

.field public static final PPT10_CREATE_ALL:I = 0x404

.field public static final PPT10_OPEN_ALL:I = 0x405

.field public static final PPT10_OPEN_FAILED:I = 0x406

.field public static final PPT10_SAVE_ALL:I = 0x407

.field public static final PPT10_SAVE_FAILED:I = 0x408

.field public static final PRINT_ALL:I = 0x42f

.field public static final PRINT_FAILED:I = 0x430

.field public static final TXT_CREATE_ALL:I = 0x41d

.field public static final TXT_OPEN_ALL:I = 0x41e

.field public static final TXT_OPEN_FAILED:I = 0x41f

.field public static final TXT_SAVE_ALL:I = 0x420

.field public static final TXT_SAVE_FAILED:I = 0x421

.field public static final XLS03_CREATE_ALL:I = 0x409

.field public static final XLS03_OPEN_ALL:I = 0x40a

.field public static final XLS03_OPEN_FAILED:I = 0x40b

.field public static final XLS03_SAVE_ALL:I = 0x40c

.field public static final XLS03_SAVE_FAILED:I = 0x40d

.field public static final XLS07_CREATE_ALL:I = 0x40e

.field public static final XLS07_OPEN_ALL:I = 0x40f

.field public static final XLS07_OPEN_FAILED:I = 0x410

.field public static final XLS07_SAVE_ALL:I = 0x411

.field public static final XLS07_SAVE_FAILED:I = 0x412

.field public static final XLS10_CREATE_ALL:I = 0x413

.field public static final XLS10_OPEN_ALL:I = 0x414

.field public static final XLS10_OPEN_FAILED:I = 0x415

.field public static final XLS10_SAVE_ALL:I = 0x416

.field public static final XLS10_SAVE_FAILED:I = 0x417


# instance fields
.field final synthetic this$0:Lcom/infraware/common/define/CMDefine;


# direct methods
.method public constructor <init>(Lcom/infraware/common/define/CMDefine;)V
    .locals 0

    .prologue
    .line 490
    iput-object p1, p0, Lcom/infraware/common/define/CMDefine$PreferenceUserPattern;->this$0:Lcom/infraware/common/define/CMDefine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
