.class public Lcom/infraware/common/database/ThumbnailManager;
.super Ljava/lang/Object;
.source "ThumbnailManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;
    }
.end annotation


# static fields
.field private static final DUPLICATE_CHECK_ERROR:I = -0x2

.field private static final DUPLICATE_CHECK_NOTDUP:I = -0x1

.field private static final RFM_LOGTAG:Ljava/lang/String; = "ThumbnailMgr"

.field private static mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

.field private static volatile mThumbnailManager:Lcom/infraware/common/database/ThumbnailManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 78
    sput-object v0, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailManager:Lcom/infraware/common/database/ThumbnailManager;

    .line 79
    sput-object v0, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "aCtx"    # Landroid/content/Context;

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    new-instance v0, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-direct {v0, p0, p1}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;-><init>(Lcom/infraware/common/database/ThumbnailManager;Landroid/content/Context;)V

    sput-object v0, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    .line 87
    return-void
.end method

.method private checkDuplicateFile(Ljava/io/File;)I
    .locals 7
    .param p1, "aTargetFile"    # Ljava/io/File;

    .prologue
    .line 572
    const/4 v4, 0x0

    .line 573
    .local v4, "thumbnailDB":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, 0x0

    .line 576
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v5}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 578
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 579
    .local v3, "path":Ljava/lang/String;
    const/4 v5, 0x0

    const-string/jumbo v6, "/"

    invoke-virtual {v3, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 580
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "SELECT * FROM Thumbnails WHERE path=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "   AND "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "filename"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 585
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_2

    .line 586
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 587
    const/4 v5, 0x0

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 599
    .local v2, "pKey":I
    if-eqz v0, :cond_0

    .line 600
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_0

    .line 601
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 604
    :cond_0
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v5, :cond_1

    .line 605
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v5}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    .end local v2    # "pKey":I
    .end local v3    # "path":Ljava/lang/String;
    :cond_1
    :goto_0
    return v2

    .line 591
    .restart local v3    # "path":Ljava/lang/String;
    :cond_2
    const/4 v2, -0x1

    .line 599
    if-eqz v0, :cond_3

    .line 600
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_3

    .line 601
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 604
    :cond_3
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v5, :cond_1

    .line 605
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v5}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    goto :goto_0

    .line 594
    .end local v3    # "path":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 595
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 596
    const/4 v2, -0x2

    .line 599
    if-eqz v0, :cond_4

    .line 600
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_4

    .line 601
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 604
    :cond_4
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v5, :cond_1

    .line 605
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v5}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    goto :goto_0

    .line 599
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    if-eqz v0, :cond_5

    .line 600
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v6

    if-nez v6, :cond_5

    .line 601
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 604
    :cond_5
    sget-object v6, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v6, :cond_6

    .line 605
    sget-object v6, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v6}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    :cond_6
    throw v5
.end method

.method private checkDuplicateFile(Ljava/lang/String;)I
    .locals 9
    .param p1, "aTargetFileFullPath"    # Ljava/lang/String;

    .prologue
    .line 611
    const/4 v6, 0x0

    .line 612
    .local v6, "thumbnailDB":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, 0x0

    .line 615
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v7, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v7}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    .line 617
    const-string/jumbo v7, "/"

    invoke-virtual {p1, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    .line 619
    .local v5, "slashIdx":I
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 620
    .local v4, "path":Ljava/lang/String;
    add-int/lit8 v7, v5, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {p1, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 621
    .local v2, "name":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "SELECT * FROM Thumbnails WHERE path=\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "   AND "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "filename"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "=\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 626
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-lez v7, :cond_2

    .line 627
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 628
    const/4 v7, 0x0

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    .line 640
    .local v3, "pKey":I
    if-eqz v0, :cond_0

    .line 641
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v7

    if-nez v7, :cond_0

    .line 642
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 645
    :cond_0
    sget-object v7, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v7, :cond_1

    .line 646
    sget-object v7, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v7}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "pKey":I
    .end local v4    # "path":Ljava/lang/String;
    .end local v5    # "slashIdx":I
    :cond_1
    :goto_0
    return v3

    .line 632
    .restart local v2    # "name":Ljava/lang/String;
    .restart local v4    # "path":Ljava/lang/String;
    .restart local v5    # "slashIdx":I
    :cond_2
    const/4 v3, -0x1

    .line 640
    if-eqz v0, :cond_3

    .line 641
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v7

    if-nez v7, :cond_3

    .line 642
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 645
    :cond_3
    sget-object v7, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v7, :cond_1

    .line 646
    sget-object v7, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v7}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    goto :goto_0

    .line 635
    .end local v2    # "name":Ljava/lang/String;
    .end local v4    # "path":Ljava/lang/String;
    .end local v5    # "slashIdx":I
    :catch_0
    move-exception v1

    .line 636
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 637
    const/4 v3, -0x2

    .line 640
    if-eqz v0, :cond_4

    .line 641
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v7

    if-nez v7, :cond_4

    .line 642
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 645
    :cond_4
    sget-object v7, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v7, :cond_1

    .line 646
    sget-object v7, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v7}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    goto :goto_0

    .line 640
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    if-eqz v0, :cond_5

    .line 641
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v8

    if-nez v8, :cond_5

    .line 642
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 645
    :cond_5
    sget-object v8, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v8, :cond_6

    .line 646
    sget-object v8, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v8}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    :cond_6
    throw v7
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/infraware/common/database/ThumbnailManager;
    .locals 2
    .param p0, "aCtx"    # Landroid/content/Context;

    .prologue
    .line 90
    sget-object v0, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailManager:Lcom/infraware/common/database/ThumbnailManager;

    if-nez v0, :cond_1

    .line 91
    const-class v1, Lcom/infraware/common/database/ThumbnailManager;

    monitor-enter v1

    .line 92
    :try_start_0
    sget-object v0, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailManager:Lcom/infraware/common/database/ThumbnailManager;

    if-nez v0, :cond_0

    .line 93
    new-instance v0, Lcom/infraware/common/database/ThumbnailManager;

    invoke-direct {v0, p0}, Lcom/infraware/common/database/ThumbnailManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailManager:Lcom/infraware/common/database/ThumbnailManager;

    .line 95
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    :cond_1
    sget-object v0, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailManager:Lcom/infraware/common/database/ThumbnailManager;

    return-object v0

    .line 95
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private insertFile(Ljava/io/File;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 11
    .param p1, "aTargetFile"    # Ljava/io/File;
    .param p2, "aThumbnail"    # [B
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "writer"    # Ljava/lang/String;
    .param p5, "lastEditor"    # Ljava/lang/String;
    .param p6, "page"    # I
    .param p7, "word"    # I

    .prologue
    .line 360
    const/4 v8, 0x0

    .line 362
    .local v8, "thumbnailDB":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    sget-object v9, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v9}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    .line 364
    const/4 v3, 0x0

    .local v3, "filename":Ljava/lang/String;
    const/4 v6, 0x0

    .line 365
    .local v6, "path":Ljava/lang/String;
    const-wide/16 v4, 0x0

    .line 366
    .local v4, "lastModifiedDate":J
    const-string/jumbo v1, ""

    .line 367
    .local v1, "accountId":Ljava/lang/String;
    const-string/jumbo v7, ""

    .line 369
    .local v7, "storageName":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 371
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    .line 372
    const/4 v9, 0x0

    const-string/jumbo v10, "/"

    invoke-virtual {v6, v10}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v6, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 374
    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    .line 376
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "INSERT INTO Thumbnails VALUES ( null, \""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p5

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, p6

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, p7

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ");"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 391
    invoke-virtual {p0, p1, p2}, Lcom/infraware/common/database/ThumbnailManager;->setThumbnailData(Ljava/io/File;[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 398
    sget-object v9, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v9, :cond_0

    .line 399
    sget-object v9, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v9}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    .line 402
    .end local v1    # "accountId":Ljava/lang/String;
    .end local v3    # "filename":Ljava/lang/String;
    .end local v4    # "lastModifiedDate":J
    .end local v6    # "path":Ljava/lang/String;
    .end local v7    # "storageName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 393
    :catch_0
    move-exception v2

    .line 394
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 398
    sget-object v9, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v9, :cond_0

    .line 399
    sget-object v9, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v9}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    goto :goto_0

    .line 398
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v9

    sget-object v10, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v10, :cond_1

    .line 399
    sget-object v10, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v10}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    :cond_1
    throw v9
.end method

.method private insertFile(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 8
    .param p1, "absolutePath"    # Ljava/lang/String;
    .param p2, "updateTime"    # J
    .param p4, "accountId"    # Ljava/lang/String;
    .param p5, "storageName"    # Ljava/lang/String;
    .param p6, "aThumbnail"    # [B
    .param p7, "title"    # Ljava/lang/String;
    .param p8, "writer"    # Ljava/lang/String;
    .param p9, "lastEditor"    # Ljava/lang/String;
    .param p10, "page"    # I
    .param p11, "word"    # I

    .prologue
    .line 405
    const/4 v5, 0x0

    .line 407
    .local v5, "thumbnailDB":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    sget-object v6, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v6}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 409
    const/4 v2, 0x0

    .local v2, "filename":Ljava/lang/String;
    const/4 v3, 0x0

    .line 411
    .local v3, "path":Ljava/lang/String;
    const-string/jumbo v6, "/"

    invoke-virtual {p1, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    .line 412
    .local v4, "slashIdx":I
    if-gez v4, :cond_1

    .line 413
    const-string/jumbo v3, ""

    .line 418
    :goto_0
    add-int/lit8 v6, v4, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {p1, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 420
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "INSERT INTO Thumbnails VALUES ( null, \""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p9

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p10

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p11

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ");"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 435
    invoke-virtual {p0, p1, p6}, Lcom/infraware/common/database/ThumbnailManager;->setThumbnailData(Ljava/lang/String;[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 442
    sget-object v6, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v6, :cond_0

    .line 443
    sget-object v6, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v6}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    .line 446
    .end local v2    # "filename":Ljava/lang/String;
    .end local v3    # "path":Ljava/lang/String;
    .end local v4    # "slashIdx":I
    :cond_0
    :goto_1
    return-void

    .line 414
    .restart local v2    # "filename":Ljava/lang/String;
    .restart local v3    # "path":Ljava/lang/String;
    .restart local v4    # "slashIdx":I
    :cond_1
    if-nez v4, :cond_2

    .line 415
    :try_start_1
    const-string/jumbo v3, "/"

    goto/16 :goto_0

    .line 417
    :cond_2
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    goto/16 :goto_0

    .line 437
    .end local v2    # "filename":Ljava/lang/String;
    .end local v3    # "path":Ljava/lang/String;
    .end local v4    # "slashIdx":I
    :catch_0
    move-exception v1

    .line 438
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 442
    sget-object v6, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v6, :cond_0

    .line 443
    sget-object v6, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v6}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    goto :goto_1

    .line 442
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    sget-object v7, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v7, :cond_3

    .line 443
    sget-object v7, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v7}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    :cond_3
    throw v6
.end method

.method private makeThumbnailItem(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Lcom/infraware/common/database/ThumbnailItem;
    .locals 1
    .param p1, "aPKey"    # I
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "author"    # Ljava/lang/String;
    .param p4, "lastEditor"    # Ljava/lang/String;
    .param p5, "page"    # I
    .param p6, "word"    # I

    .prologue
    .line 289
    new-instance v0, Lcom/infraware/common/database/ThumbnailItem;

    invoke-direct {v0}, Lcom/infraware/common/database/ThumbnailItem;-><init>()V

    .line 291
    .local v0, "item":Lcom/infraware/common/database/ThumbnailItem;
    iput-object p3, v0, Lcom/infraware/common/database/ThumbnailItem;->docAuthor:Ljava/lang/String;

    .line 292
    iput-object p2, v0, Lcom/infraware/common/database/ThumbnailItem;->docTitle:Ljava/lang/String;

    .line 293
    iput-object p4, v0, Lcom/infraware/common/database/ThumbnailItem;->docLastEditor:Ljava/lang/String;

    .line 294
    iput p5, v0, Lcom/infraware/common/database/ThumbnailItem;->docPage:I

    .line 295
    iput p6, v0, Lcom/infraware/common/database/ThumbnailItem;->docWord:I

    .line 297
    return-object v0
.end method

.method private updateFile(Ljava/io/File;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V
    .locals 6
    .param p1, "aTargetFile"    # Ljava/io/File;
    .param p2, "aThumbnail"    # [B
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "writer"    # Ljava/lang/String;
    .param p5, "lastEditor"    # Ljava/lang/String;
    .param p6, "page"    # I
    .param p7, "word"    # I
    .param p8, "aTargetIdx"    # I

    .prologue
    .line 478
    const/4 v3, 0x0

    .line 480
    .local v3, "thumbnailDB":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    sget-object v4, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v4}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 482
    const-wide/16 v1, 0x0

    .line 484
    .local v1, "lastModifiedDate":J
    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v1

    .line 486
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "UPDATE Thumbnails   SET last_modified="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "title"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "writer"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "last_editor"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "page"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "word"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " WHERE "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "_id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 496
    const-string/jumbo v4, "ThumbnailManager"

    const-string/jumbo v5, "Before setThumbnailData"

    invoke-static {v4, v5}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    invoke-virtual {p0, p1, p2}, Lcom/infraware/common/database/ThumbnailManager;->setThumbnailData(Ljava/io/File;[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 504
    sget-object v4, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v4, :cond_0

    .line 505
    sget-object v4, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v4}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    .line 508
    .end local v1    # "lastModifiedDate":J
    :cond_0
    :goto_0
    return-void

    .line 499
    :catch_0
    move-exception v0

    .line 500
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 504
    sget-object v4, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v4, :cond_0

    .line 505
    sget-object v4, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v4}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    goto :goto_0

    .line 504
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v5, :cond_1

    .line 505
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v5}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    :cond_1
    throw v4
.end method

.method private updateFile(Ljava/lang/String;J[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V
    .locals 4
    .param p1, "absolutePath"    # Ljava/lang/String;
    .param p2, "updateTime"    # J
    .param p4, "aThumbnail"    # [B
    .param p5, "title"    # Ljava/lang/String;
    .param p6, "writer"    # Ljava/lang/String;
    .param p7, "lastEditor"    # Ljava/lang/String;
    .param p8, "page"    # I
    .param p9, "word"    # I
    .param p10, "aTargetIdx"    # I

    .prologue
    .line 449
    const/4 v1, 0x0

    .line 451
    .local v1, "thumbnailDB":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    sget-object v2, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v2}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 453
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "UPDATE Thumbnails   SET last_modified="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "title"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "writer"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "last_editor"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "page"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "word"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " WHERE "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "_id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 463
    const-string/jumbo v2, "ThumbnailManager"

    const-string/jumbo v3, "Before setThumbnailData"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    invoke-virtual {p0, p1, p4}, Lcom/infraware/common/database/ThumbnailManager;->setThumbnailData(Ljava/lang/String;[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 471
    sget-object v2, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v2, :cond_0

    .line 472
    sget-object v2, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v2}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    .line 475
    :cond_0
    :goto_0
    return-void

    .line 466
    :catch_0
    move-exception v0

    .line 467
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 471
    sget-object v2, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v2, :cond_0

    .line 472
    sget-object v2, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v2}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    goto :goto_0

    .line 471
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    sget-object v3, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v3, :cond_1

    .line 472
    sget-object v3, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v3}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    :cond_1
    throw v2
.end method


# virtual methods
.method public ConvertBLOB2Bitmap(Landroid/content/Context;[B)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "aCtx"    # Landroid/content/Context;
    .param p2, "aBlob"    # [B

    .prologue
    const/4 v6, 0x1

    .line 226
    if-nez p2, :cond_0

    .line 227
    const/4 v2, 0x0

    .line 238
    :goto_0
    return-object v2

    .line 229
    :cond_0
    const/4 v2, 0x0

    .line 231
    .local v2, "thumbnail":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 232
    .local v1, "option":Landroid/graphics/BitmapFactory$Options;
    const/4 v3, 0x1

    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 233
    const/4 v3, 0x0

    array-length v4, p2

    invoke-static {p2, v3, v4, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 234
    .end local v1    # "option":Landroid/graphics/BitmapFactory$Options;
    :catch_0
    move-exception v0

    .line 235
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700a7

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public InsertFileInfoToDB(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 9
    .param p1, "aFileAbsolutePath"    # Ljava/lang/String;
    .param p2, "aThumbnail"    # [B
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "writer"    # Ljava/lang/String;
    .param p5, "lastEditor"    # Ljava/lang/String;
    .param p6, "page"    # I
    .param p7, "word"    # I

    .prologue
    .line 301
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 303
    .local v1, "targetFile":Ljava/io/File;
    invoke-direct {p0, v1}, Lcom/infraware/common/database/ThumbnailManager;->checkDuplicateFile(Ljava/io/File;)I

    move-result v8

    .line 304
    .local v8, "dupIdx":I
    const/4 v0, -0x2

    if-ne v8, v0, :cond_0

    .line 305
    const-string/jumbo v0, "ThumbnailMgr"

    const-string/jumbo v2, "checkDuplicateFile error"

    invoke-static {v0, v2}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    :goto_0
    return-void

    .line 307
    :cond_0
    const/4 v0, -0x1

    if-ne v8, v0, :cond_1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    move/from16 v7, p7

    .line 308
    invoke-direct/range {v0 .. v7}, Lcom/infraware/common/database/ThumbnailManager;->insertFile(Ljava/io/File;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0

    :cond_1
    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    move/from16 v7, p7

    .line 311
    invoke-direct/range {v0 .. v8}, Lcom/infraware/common/database/ThumbnailManager;->updateFile(Ljava/io/File;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    goto :goto_0
.end method

.method public deleteAll()V
    .locals 4

    .prologue
    .line 342
    const/4 v1, 0x0

    .line 345
    .local v1, "thumbnailDB":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    sget-object v2, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v2}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 346
    const-string/jumbo v2, "DELETE FROM Thumbnails"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 353
    sget-object v2, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v2, :cond_0

    .line 354
    sget-object v2, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v2}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    .line 357
    :cond_0
    :goto_0
    return-void

    .line 349
    :catch_0
    move-exception v0

    .line 350
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 353
    sget-object v2, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v2, :cond_0

    .line 354
    sget-object v2, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v2}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    goto :goto_0

    .line 353
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    sget-object v3, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v3, :cond_1

    .line 354
    sget-object v3, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v3}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    :cond_1
    throw v2
.end method

.method public deleteFile(Ljava/lang/String;)V
    .locals 5
    .param p1, "aTargetFileFullPath"    # Ljava/lang/String;

    .prologue
    .line 316
    invoke-direct {p0, p1}, Lcom/infraware/common/database/ThumbnailManager;->checkDuplicateFile(Ljava/lang/String;)I

    move-result v0

    .line 318
    .local v0, "dupRet":I
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    const/4 v3, -0x2

    if-ne v0, v3, :cond_1

    .line 339
    :cond_0
    :goto_0
    return-void

    .line 322
    :cond_1
    const/4 v2, 0x0

    .line 325
    .local v2, "thumbnailDB":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    sget-object v3, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v3}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 326
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "DELETE FROM Thumbnails WHERE _id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 335
    sget-object v3, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v3, :cond_0

    .line 336
    sget-object v3, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v3}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    goto :goto_0

    .line 331
    :catch_0
    move-exception v1

    .line 332
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 335
    sget-object v3, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v3, :cond_0

    .line 336
    sget-object v3, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v3}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    goto :goto_0

    .line 335
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    sget-object v4, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v4, :cond_2

    .line 336
    sget-object v4, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v4}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    :cond_2
    throw v3
.end method

.method public getThumbnail(Landroid/content/Context;Ljava/io/File;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "aCtx"    # Landroid/content/Context;
    .param p2, "aTargetFile"    # Ljava/io/File;

    .prologue
    const/4 v4, 0x0

    .line 102
    const/4 v3, 0x0

    .line 103
    .local v3, "recentDB":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, 0x0

    .line 106
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v5}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 108
    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 109
    .local v2, "path":Ljava/lang/String;
    const/4 v5, 0x0

    const-string/jumbo v6, "/"

    invoke-virtual {v2, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 110
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "SELECT * FROM Thumbnails WHERE path=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "   AND "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "filename"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 115
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_2

    .line 117
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 118
    const/16 v5, 0xb

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    invoke-virtual {p0, p1, v5}, Lcom/infraware/common/database/ThumbnailManager;->ConvertBLOB2Bitmap(Landroid/content/Context;[B)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 128
    if-eqz v0, :cond_0

    .line 129
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_0

    .line 130
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 133
    :cond_0
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v5, :cond_1

    .line 134
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v5}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    .end local v2    # "path":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object v4

    .line 128
    .restart local v2    # "path":Ljava/lang/String;
    :cond_2
    if-eqz v0, :cond_3

    .line 129
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_3

    .line 130
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 133
    :cond_3
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v5, :cond_1

    .line 134
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v5}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    goto :goto_0

    .line 123
    .end local v2    # "path":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 128
    .local v1, "e":Ljava/lang/Exception;
    if-eqz v0, :cond_4

    .line 129
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_4

    .line 130
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 133
    :cond_4
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v5, :cond_1

    .line 134
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v5}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    goto :goto_0

    .line 128
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    if-eqz v0, :cond_5

    .line 129
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_5

    .line 130
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 133
    :cond_5
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v5, :cond_6

    .line 134
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v5}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    :cond_6
    throw v4
.end method

.method public getThumbnailItem(Landroid/content/Context;Ljava/io/File;)Lcom/infraware/common/database/ThumbnailItem;
    .locals 13
    .param p1, "aCtx"    # Landroid/content/Context;
    .param p2, "aTargetFile"    # Ljava/io/File;

    .prologue
    .line 242
    const/4 v11, 0x0

    .line 243
    .local v11, "recentDB":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v7, 0x0

    .line 245
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 248
    .local v9, "item":Lcom/infraware/common/database/ThumbnailItem;
    :try_start_0
    sget-object v0, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v0}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11

    .line 250
    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v10

    .line 251
    .local v10, "path":Ljava/lang/String;
    const/4 v0, 0x0

    const-string/jumbo v12, "/"

    invoke-virtual {v10, v12}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v12

    invoke-virtual {v10, v0, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    .line 253
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "SELECT * FROM Thumbnails WHERE path=\""

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v12, "\""

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v12, "   AND "

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v12, "filename"

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v12, "=\""

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v12, "\""

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v12, 0x0

    invoke-virtual {v11, v0, v12}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 258
    const/4 v1, 0x0

    .local v1, "pKey":I
    const/4 v5, 0x0

    .local v5, "page":I
    const/4 v6, 0x0

    .line 259
    .local v6, "word":I
    const/4 v2, 0x0

    .local v2, "title":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "author":Ljava/lang/String;
    const/4 v4, 0x0

    .line 261
    .local v4, "lastEditor":Ljava/lang/String;
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 262
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 263
    const/4 v0, 0x5

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 264
    const/4 v0, 0x6

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 265
    const/4 v0, 0x7

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 266
    const/16 v0, 0x8

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 267
    const/16 v0, 0x9

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    move-object v0, p0

    .line 268
    invoke-direct/range {v0 .. v6}, Lcom/infraware/common/database/ThumbnailManager;->makeThumbnailItem(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Lcom/infraware/common/database/ThumbnailItem;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 275
    :cond_0
    if-eqz v7, :cond_1

    .line 276
    invoke-interface {v7}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 277
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 280
    :cond_1
    sget-object v0, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v0, :cond_2

    .line 281
    sget-object v0, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v0}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    .line 285
    .end local v1    # "pKey":I
    .end local v2    # "title":Ljava/lang/String;
    .end local v3    # "author":Ljava/lang/String;
    .end local v4    # "lastEditor":Ljava/lang/String;
    .end local v5    # "page":I
    .end local v6    # "word":I
    .end local v10    # "path":Ljava/lang/String;
    :cond_2
    :goto_0
    return-object v9

    .line 271
    :catch_0
    move-exception v8

    .line 272
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 275
    if-eqz v7, :cond_3

    .line 276
    invoke-interface {v7}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_3

    .line 277
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 280
    :cond_3
    sget-object v0, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v0, :cond_2

    .line 281
    sget-object v0, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v0}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    goto :goto_0

    .line 275
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_4

    .line 276
    invoke-interface {v7}, Landroid/database/Cursor;->isClosed()Z

    move-result v12

    if-nez v12, :cond_4

    .line 277
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 280
    :cond_4
    sget-object v12, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v12, :cond_5

    .line 281
    sget-object v12, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v12}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    :cond_5
    throw v0
.end method

.method public isEmpty()Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 141
    const/4 v2, 0x0

    .line 142
    .local v2, "recentDB":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, 0x0

    .line 145
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v5}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 146
    const-string/jumbo v5, "SELECT *   FROM Thumbnails"

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 150
    const/4 v3, 0x0

    .line 152
    .local v3, "thumbnailItem":Lcom/infraware/common/database/ThumbnailItem;
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 154
    new-instance v3, Lcom/infraware/common/database/ThumbnailItem;

    .end local v3    # "thumbnailItem":Lcom/infraware/common/database/ThumbnailItem;
    invoke-direct {v3}, Lcom/infraware/common/database/ThumbnailItem;-><init>()V

    .line 155
    .restart local v3    # "thumbnailItem":Lcom/infraware/common/database/ThumbnailItem;
    const/4 v5, 0x2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/infraware/common/database/ThumbnailItem;->path:Ljava/lang/String;

    .line 156
    const/4 v5, 0x1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/infraware/common/database/ThumbnailItem;->name:Ljava/lang/String;

    .line 158
    new-instance v5, Ljava/io/File;

    invoke-virtual {v3}, Lcom/infraware/common/database/ThumbnailItem;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-eqz v5, :cond_0

    .line 159
    const/4 v4, 0x0

    .line 166
    if-eqz v0, :cond_1

    .line 167
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_1

    .line 168
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 171
    :cond_1
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v5, :cond_2

    .line 172
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v5}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    .line 176
    .end local v3    # "thumbnailItem":Lcom/infraware/common/database/ThumbnailItem;
    :cond_2
    :goto_0
    return v4

    .line 166
    .restart local v3    # "thumbnailItem":Lcom/infraware/common/database/ThumbnailItem;
    :cond_3
    if-eqz v0, :cond_4

    .line 167
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_4

    .line 168
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 171
    :cond_4
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v5, :cond_2

    .line 172
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v5}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    goto :goto_0

    .line 162
    .end local v3    # "thumbnailItem":Lcom/infraware/common/database/ThumbnailItem;
    :catch_0
    move-exception v1

    .line 166
    .local v1, "e":Ljava/lang/Exception;
    if-eqz v0, :cond_5

    .line 167
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_5

    .line 168
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 171
    :cond_5
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v5, :cond_2

    .line 172
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v5}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    goto :goto_0

    .line 166
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    if-eqz v0, :cond_6

    .line 167
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_6

    .line 168
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 171
    :cond_6
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v5, :cond_7

    .line 172
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v5}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    :cond_7
    throw v4
.end method

.method public isModified(Ljava/lang/String;)Z
    .locals 10
    .param p1, "aFileAbsolutePath"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 181
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 183
    .local v5, "targetFile":Ljava/io/File;
    const/4 v6, 0x0

    .line 184
    .local v6, "thumbnailDB":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, 0x0

    .line 187
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v8, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v8}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    .line 189
    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    .line 190
    .local v4, "path":Ljava/lang/String;
    const/4 v8, 0x0

    const-string/jumbo v9, "/"

    invoke-virtual {v4, v9}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v4, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 191
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "SELECT * FROM Thumbnails WHERE path=\""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "\""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "   AND "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "filename"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "=\""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "\""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v6, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 196
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v8

    if-lez v8, :cond_2

    .line 197
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 198
    const/16 v8, 0xa

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 199
    .local v2, "nLastModified":J
    invoke-virtual {v5}, Ljava/io/File;->lastModified()J

    move-result-wide v8

    cmp-long v8, v8, v2

    if-nez v8, :cond_2

    .line 200
    const-string/jumbo v8, "ThumbnailManager"

    const-string/jumbo v9, "EQUAL!!!"

    invoke-static {v8, v9}, Lcom/infraware/common/util/CMLog;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    if-eqz v0, :cond_0

    .line 211
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v8

    if-nez v8, :cond_0

    .line 212
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 215
    :cond_0
    sget-object v8, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v8, :cond_1

    .line 216
    sget-object v8, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v8}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    .line 221
    .end local v2    # "nLastModified":J
    .end local v4    # "path":Ljava/lang/String;
    :cond_1
    :goto_0
    return v7

    .line 210
    .restart local v4    # "path":Ljava/lang/String;
    :cond_2
    if-eqz v0, :cond_3

    .line 211
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v7

    if-nez v7, :cond_3

    .line 212
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 215
    :cond_3
    sget-object v7, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v7, :cond_4

    .line 216
    sget-object v7, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v7}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    .line 220
    :cond_4
    const-string/jumbo v7, "ThumbnailManager"

    const-string/jumbo v8, "MODIFIED!!!"

    invoke-static {v7, v8}, Lcom/infraware/common/util/CMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    const/4 v7, 0x1

    goto :goto_0

    .line 205
    .end local v4    # "path":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 206
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 210
    if-eqz v0, :cond_5

    .line 211
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v8

    if-nez v8, :cond_5

    .line 212
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 215
    :cond_5
    sget-object v8, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v8, :cond_1

    .line 216
    sget-object v8, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v8}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    goto :goto_0

    .line 210
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    if-eqz v0, :cond_6

    .line 211
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v8

    if-nez v8, :cond_6

    .line 212
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 215
    :cond_6
    sget-object v8, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v8, :cond_7

    .line 216
    sget-object v8, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v8}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    :cond_7
    throw v7
.end method

.method public setThumbnailData(Ljava/io/File;[B)V
    .locals 8
    .param p1, "aTargetFile"    # Ljava/io/File;
    .param p2, "aThumbnail"    # [B

    .prologue
    .line 511
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 512
    .local v2, "path":Ljava/lang/String;
    const/4 v5, 0x0

    const-string/jumbo v6, "/"

    invoke-virtual {v2, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 513
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 515
    .local v1, "filename":Ljava/lang/String;
    const/4 v3, 0x0

    .line 517
    .local v3, "thumbnailDB":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v5}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 518
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 519
    .local v4, "values":Landroid/content/ContentValues;
    const-string/jumbo v5, "thumbnail_data"

    invoke-virtual {v4, v5, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 520
    const-string/jumbo v5, "Thumbnails"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "path = \""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " AND "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "filename"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " = \""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v3, v5, v4, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 530
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v5, :cond_0

    .line 531
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v5}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    .line 534
    .end local v4    # "values":Landroid/content/ContentValues;
    :cond_0
    :goto_0
    return-void

    .line 526
    :catch_0
    move-exception v0

    .line 527
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 530
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v5, :cond_0

    .line 531
    sget-object v5, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v5}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    goto :goto_0

    .line 530
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    sget-object v6, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v6, :cond_1

    .line 531
    sget-object v6, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v6}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    :cond_1
    throw v5
.end method

.method public setThumbnailData(Ljava/lang/String;[B)V
    .locals 9
    .param p1, "absolutePath"    # Ljava/lang/String;
    .param p2, "aThumbnail"    # [B

    .prologue
    .line 537
    const-string/jumbo v6, "/"

    invoke-virtual {p1, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    .line 538
    .local v3, "slashIdx":I
    const-string/jumbo v2, ""

    .line 539
    .local v2, "path":Ljava/lang/String;
    if-gez v3, :cond_1

    .line 540
    const-string/jumbo v2, ""

    .line 545
    :goto_0
    add-int/lit8 v6, v3, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {p1, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 547
    .local v1, "filename":Ljava/lang/String;
    const/4 v4, 0x0

    .line 549
    .local v4, "thumbnailDB":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    sget-object v6, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v6}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 550
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 551
    .local v5, "values":Landroid/content/ContentValues;
    const-string/jumbo v6, "thumbnail_data"

    invoke-virtual {v5, v6, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 552
    const-string/jumbo v6, "Thumbnails"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "path = \""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " AND "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "filename"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " = \""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v4, v6, v5, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 562
    sget-object v6, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v6, :cond_0

    .line 563
    sget-object v6, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v6}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    .line 566
    .end local v5    # "values":Landroid/content/ContentValues;
    :cond_0
    :goto_1
    return-void

    .line 541
    .end local v1    # "filename":Ljava/lang/String;
    .end local v4    # "thumbnailDB":Landroid/database/sqlite/SQLiteDatabase;
    :cond_1
    if-nez v3, :cond_2

    .line 542
    const-string/jumbo v2, "/"

    goto :goto_0

    .line 544
    :cond_2
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 558
    .restart local v1    # "filename":Ljava/lang/String;
    .restart local v4    # "thumbnailDB":Landroid/database/sqlite/SQLiteDatabase;
    :catch_0
    move-exception v0

    .line 559
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 562
    sget-object v6, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v6, :cond_0

    .line 563
    sget-object v6, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v6}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    goto :goto_1

    .line 562
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    sget-object v7, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    if-eqz v7, :cond_3

    .line 563
    sget-object v7, Lcom/infraware/common/database/ThumbnailManager;->mThumbnailDBHelper:Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;

    invoke-virtual {v7}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->close()V

    :cond_3
    throw v6
.end method
