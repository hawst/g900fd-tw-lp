.class public Lcom/infraware/common/database/ThumbnailItem;
.super Ljava/lang/Object;
.source "ThumbnailItem.java"


# instance fields
.field public docAuthor:Ljava/lang/String;

.field public docLastEditor:Ljava/lang/String;

.field public docPage:I

.field public docTitle:Ljava/lang/String;

.field public docWord:I

.field public ext:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public path:Ljava/lang/String;

.field public thumbnail:Landroid/graphics/Bitmap;

.field public updateTime:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/common/database/ThumbnailItem;->path:Ljava/lang/String;

    .line 21
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/common/database/ThumbnailItem;->name:Ljava/lang/String;

    .line 22
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/common/database/ThumbnailItem;->ext:Ljava/lang/String;

    .line 23
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/infraware/common/database/ThumbnailItem;->updateTime:J

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/common/database/ThumbnailItem;->thumbnail:Landroid/graphics/Bitmap;

    .line 26
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/common/database/ThumbnailItem;->docTitle:Ljava/lang/String;

    .line 27
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/common/database/ThumbnailItem;->docAuthor:Ljava/lang/String;

    .line 28
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/common/database/ThumbnailItem;->docLastEditor:Ljava/lang/String;

    .line 29
    iput v2, p0, Lcom/infraware/common/database/ThumbnailItem;->docPage:I

    .line 30
    iput v2, p0, Lcom/infraware/common/database/ThumbnailItem;->docWord:I

    .line 31
    return-void
.end method


# virtual methods
.method public getAbsolutePath()Ljava/lang/String;
    .locals 3

    .prologue
    .line 47
    const/4 v0, 0x0

    .line 49
    .local v0, "pathArg":Ljava/lang/String;
    iget-object v1, p0, Lcom/infraware/common/database/ThumbnailItem;->path:Ljava/lang/String;

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50
    const-string/jumbo v0, ""

    .line 54
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/infraware/common/database/ThumbnailItem;->getFullFileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/infraware/common/database/ThumbnailItem;->path:Ljava/lang/String;

    goto :goto_0
.end method

.method public getFullFileName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/infraware/common/database/ThumbnailItem;->ext:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/common/database/ThumbnailItem;->ext:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/infraware/common/database/ThumbnailItem;->name:Ljava/lang/String;

    .line 62
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/infraware/common/database/ThumbnailItem;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/common/database/ThumbnailItem;->ext:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPath()Ljava/lang/String;
    .locals 4

    .prologue
    .line 67
    iget-object v1, p0, Lcom/infraware/common/database/ThumbnailItem;->path:Ljava/lang/String;

    const-string/jumbo v2, "//"

    const-string/jumbo v3, "/"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 68
    .local v0, "convertPath":Ljava/lang/String;
    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    const-string/jumbo v0, ""

    .line 71
    .end local v0    # "convertPath":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public setName(Ljava/lang/String;)V
    .locals 2
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 35
    const/16 v1, 0x2e

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 36
    .local v0, "nIndex":I
    if-lez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x6

    if-le v0, v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-eq v0, v1, :cond_0

    .line 38
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/common/database/ThumbnailItem;->name:Ljava/lang/String;

    .line 39
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/common/database/ThumbnailItem;->ext:Ljava/lang/String;

    .line 43
    :goto_0
    return-void

    .line 42
    :cond_0
    iput-object p1, p0, Lcom/infraware/common/database/ThumbnailItem;->name:Ljava/lang/String;

    goto :goto_0
.end method
