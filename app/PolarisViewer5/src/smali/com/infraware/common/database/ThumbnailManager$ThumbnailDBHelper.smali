.class public Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "ThumbnailManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/common/database/ThumbnailManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ThumbnailDBHelper"
.end annotation


# static fields
.field public static final THUMBNAIL_DB_FIELDIDX_DATE_MODIFIED:I = 0xa

.field public static final THUMBNAIL_DB_FIELDIDX_LAST_EDITOR:I = 0x7

.field public static final THUMBNAIL_DB_FIELDIDX_NAME:I = 0x1

.field public static final THUMBNAIL_DB_FIELDIDX_PAGE:I = 0x8

.field public static final THUMBNAIL_DB_FIELDIDX_PATH:I = 0x2

.field public static final THUMBNAIL_DB_FIELDIDX_PKEY:I = 0x0

.field public static final THUMBNAIL_DB_FIELDIDX_STORAGEID:I = 0x3

.field public static final THUMBNAIL_DB_FIELDIDX_STORAGENAME:I = 0x4

.field public static final THUMBNAIL_DB_FIELDIDX_THUMBNAIL_DATA:I = 0xb

.field public static final THUMBNAIL_DB_FIELDIDX_TITLE:I = 0x5

.field public static final THUMBNAIL_DB_FIELDIDX_WORD:I = 0x9

.field public static final THUMBNAIL_DB_FIELDIDX_WRITER:I = 0x6

.field public static final THUMBNAIL_DB_FIELD_DATE_MODIFIED:Ljava/lang/String; = "last_modified"

.field public static final THUMBNAIL_DB_FIELD_LAST_EDITOR:Ljava/lang/String; = "last_editor"

.field public static final THUMBNAIL_DB_FIELD_NAME:Ljava/lang/String; = "filename"

.field public static final THUMBNAIL_DB_FIELD_PAGE:Ljava/lang/String; = "page"

.field public static final THUMBNAIL_DB_FIELD_PATH:Ljava/lang/String; = "path"

.field public static final THUMBNAIL_DB_FIELD_PKEY:Ljava/lang/String; = "_id"

.field public static final THUMBNAIL_DB_FIELD_STORAGEID:Ljava/lang/String; = "storageid"

.field public static final THUMBNAIL_DB_FIELD_STORAGENAME:Ljava/lang/String; = "storagename"

.field public static final THUMBNAIL_DB_FIELD_THUMBNAIL_DATA:Ljava/lang/String; = "thumbnail_data"

.field public static final THUMBNAIL_DB_FIELD_TITLE:Ljava/lang/String; = "title"

.field public static final THUMBNAIL_DB_FIELD_WORD:Ljava/lang/String; = "word"

.field public static final THUMBNAIL_DB_FIELD_WRITER:Ljava/lang/String; = "writer"

.field public static final THUMBNAIL_DB_NAME:Ljava/lang/String; = "DocMasterThumbnails.db"

.field public static final THUMBNAIL_DB_VERSION:I = 0x1

.field public static final THUMBNAIL_TABLE_NAME:Ljava/lang/String; = "Thumbnails"


# instance fields
.field final synthetic this$0:Lcom/infraware/common/database/ThumbnailManager;


# direct methods
.method public constructor <init>(Lcom/infraware/common/database/ThumbnailManager;Landroid/content/Context;)V
    .locals 3
    .param p2, "aCtx"    # Landroid/content/Context;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->this$0:Lcom/infraware/common/database/ThumbnailManager;

    .line 50
    const-string/jumbo v0, "DocMasterThumbnails.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p2, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 51
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 54
    const-string/jumbo v0, "CREATE TABLE Thumbnails(_id INTEGER PRIMARY KEY AUTOINCREMENT,filename TEXT,path TEXT,storageid TEXT,storagename TEXT,title TEXT,writer TEXT,last_editor TEXT,page INTEGER,word INTEGER,last_modified LONG,thumbnail_data BLOB);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 68
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 71
    const-string/jumbo v0, "DROP TABLE IF EXISTS DocMasterThumbnails.db"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 72
    invoke-virtual {p0, p1}, Lcom/infraware/common/database/ThumbnailManager$ThumbnailDBHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 73
    return-void
.end method
