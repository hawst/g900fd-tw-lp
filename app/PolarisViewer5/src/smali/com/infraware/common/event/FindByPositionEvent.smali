.class public Lcom/infraware/common/event/FindByPositionEvent;
.super Landroid/content/BroadcastReceiver;
.source "FindByPositionEvent.java"


# static fields
.field public static final SELECT_FILE:Ljava/lang/String; = "filepath"


# instance fields
.field private listener:Lcom/infraware/common/event/FindByPositionListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/infraware/common/event/FindByPositionEvent;->listener:Lcom/infraware/common/event/FindByPositionListener;

    if-eqz v0, :cond_0

    .line 18
    iget-object v0, p0, Lcom/infraware/common/event/FindByPositionEvent;->listener:Lcom/infraware/common/event/FindByPositionListener;

    invoke-interface {v0, p1, p2}, Lcom/infraware/common/event/FindByPositionListener;->isFindByPositionAction(Landroid/content/Context;Landroid/content/Intent;)V

    .line 20
    :cond_0
    return-void
.end method

.method public setFindByPositionListener(Lcom/infraware/common/event/FindByPositionListener;)V
    .locals 0
    .param p1, "findByPosition"    # Lcom/infraware/common/event/FindByPositionListener;

    .prologue
    .line 13
    iput-object p1, p0, Lcom/infraware/common/event/FindByPositionEvent;->listener:Lcom/infraware/common/event/FindByPositionListener;

    .line 14
    return-void
.end method
