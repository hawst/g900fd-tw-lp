.class public Lcom/infraware/common/event/SdCardEvent;
.super Landroid/content/BroadcastReceiver;
.source "SdCardEvent.java"


# instance fields
.field private listener:Lcom/infraware/common/event/SdCardListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/infraware/common/event/SdCardEvent;->listener:Lcom/infraware/common/event/SdCardListener;

    if-eqz v0, :cond_0

    .line 19
    iget-object v0, p0, Lcom/infraware/common/event/SdCardEvent;->listener:Lcom/infraware/common/event/SdCardListener;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/infraware/common/event/SdCardListener;->isSdCardAction(Ljava/lang/String;)V

    .line 20
    :cond_0
    return-void
.end method

.method public setSdListener(Lcom/infraware/common/event/SdCardListener;)V
    .locals 0
    .param p1, "docMaster"    # Lcom/infraware/common/event/SdCardListener;

    .prologue
    .line 13
    iput-object p1, p0, Lcom/infraware/common/event/SdCardEvent;->listener:Lcom/infraware/common/event/SdCardListener;

    .line 14
    return-void
.end method
