.class Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;
.super Ljava/lang/Object;
.source "SbeamHelper.java"

# interfaces
.implements Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/common/util/SbeamHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SbeamPushCallback"
.end annotation


# static fields
.field private static final MIME_ERROR:Ljava/lang/String; = "text/DirectShareError"


# instance fields
.field final synthetic this$0:Lcom/infraware/common/util/SbeamHelper;


# direct methods
.method constructor <init>(Lcom/infraware/common/util/SbeamHelper;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getFileList([Ljava/io/File;)Lorg/json/JSONArray;
    .locals 11
    .param p1, "files"    # [Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 210
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    .line 211
    .local v7, "rootPath":Ljava/lang/String;
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6}, Lorg/json/JSONArray;-><init>()V

    .line 213
    .local v6, "retList":Lorg/json/JSONArray;
    move-object v1, p1

    .local v1, "arr$":[Ljava/io/File;
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v2, v1, v3

    .line 214
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 215
    .local v0, "absolutePath":Ljava/lang/String;
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 216
    .local v5, "obj":Lorg/json/JSONObject;
    const-string/jumbo v8, "fileName"

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 217
    const-string/jumbo v8, "fileLen"

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v9

    invoke-virtual {v5, v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 218
    const-string/jumbo v8, "filepath"

    invoke-virtual {v5, v8, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 219
    const-string/jumbo v8, "subPath"

    const-string/jumbo v9, ""

    invoke-virtual {v0, v7, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 220
    invoke-virtual {v6, v5}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 213
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 223
    .end local v0    # "absolutePath":Ljava/lang/String;
    .end local v2    # "file":Ljava/io/File;
    .end local v5    # "obj":Lorg/json/JSONObject;
    :cond_0
    return-object v6
.end method

.method private getJSONObject([Ljava/io/File;)[B
    .locals 5
    .param p1, "files"    # [Ljava/io/File;

    .prologue
    .line 197
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 199
    .local v1, "obj":Lorg/json/JSONObject;
    :try_start_0
    const-string/jumbo v2, "mac"

    invoke-direct {p0}, Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;->getMacAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 200
    const-string/jumbo v2, "mimeType"

    iget-object v3, p0, Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # getter for: Lcom/infraware/common/util/SbeamHelper;->mAppMime:Ljava/lang/String;
    invoke-static {v3}, Lcom/infraware/common/util/SbeamHelper;->access$300(Lcom/infraware/common/util/SbeamHelper;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 201
    const-string/jumbo v2, "list"

    invoke-direct {p0, p1}, Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;->getFileList([Ljava/io/File;)Lorg/json/JSONArray;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    return-object v2

    .line 202
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e":Lorg/json/JSONException;
    const-string/jumbo v2, "[SBeam]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "SbeamHelper.SbeamPushCallback.getJSONObject : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # getter for: Lcom/infraware/common/util/SbeamHelper;->mAppMime:Ljava/lang/String;
    invoke-static {v4}, Lcom/infraware/common/util/SbeamHelper;->access$300(Lcom/infraware/common/util/SbeamHelper;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getMacAddress()Ljava/lang/String;
    .locals 9

    .prologue
    const/16 v8, 0x10

    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 227
    iget-object v4, p0, Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # getter for: Lcom/infraware/common/util/SbeamHelper;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/infraware/common/util/SbeamHelper;->access$200(Lcom/infraware/common/util/SbeamHelper;)Landroid/content/Context;

    move-result-object v4

    const-string/jumbo v5, "wifi"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/WifiManager;

    .line 228
    .local v3, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    .line 229
    .local v2, "wifiInfo":Landroid/net/wifi/WifiInfo;
    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 230
    .local v0, "firstMac":Ljava/lang/String;
    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4, v7, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 232
    .local v1, "lastMac":Ljava/lang/String;
    const/4 v4, 0x1

    invoke-virtual {v0, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 233
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0, v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    or-int/lit8 v5, v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    .line 236
    :goto_0
    return-object v4

    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0, v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    or-int/lit8 v5, v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method


# virtual methods
.method public createNdefMessage(Landroid/nfc/NfcEvent;)Landroid/nfc/NdefMessage;
    .locals 6
    .param p1, "arg0"    # Landroid/nfc/NfcEvent;

    .prologue
    const/4 v5, 0x1

    .line 137
    iget-object v2, p0, Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # invokes: Lcom/infraware/common/util/SbeamHelper;->getFiles()[Ljava/io/File;
    invoke-static {v2}, Lcom/infraware/common/util/SbeamHelper;->access$000(Lcom/infraware/common/util/SbeamHelper;)[Ljava/io/File;

    move-result-object v0

    .line 138
    .local v0, "files":[Ljava/io/File;
    invoke-virtual {p0, v0}, Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;->createRecord([Ljava/io/File;)Landroid/nfc/NdefRecord;

    move-result-object v1

    .line 139
    .local v1, "record":Landroid/nfc/NdefRecord;
    if-nez v1, :cond_0

    .line 140
    const-string/jumbo v2, "[SBeam]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "SbeamPushCallback.createNdefMessage : fail to crate["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # getter for: Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I
    invoke-static {v4}, Lcom/infraware/common/util/SbeamHelper;->access$100(Lcom/infraware/common/util/SbeamHelper;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    const/4 v2, 0x0

    .line 145
    :goto_0
    return-object v2

    .line 144
    :cond_0
    iget-object v2, p0, Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # setter for: Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I
    invoke-static {v2, v5}, Lcom/infraware/common/util/SbeamHelper;->access$102(Lcom/infraware/common/util/SbeamHelper;I)I

    .line 145
    new-instance v2, Landroid/nfc/NdefMessage;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/nfc/NdefRecord;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const-string/jumbo v4, "com.sec.android.directshare"

    invoke-static {v4}, Landroid/nfc/NdefRecord;->createApplicationRecord(Ljava/lang/String;)Landroid/nfc/NdefRecord;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v2, v3}, Landroid/nfc/NdefMessage;-><init>([Landroid/nfc/NdefRecord;)V

    goto :goto_0
.end method

.method createRecord([Ljava/io/File;)Landroid/nfc/NdefRecord;
    .locals 9
    .param p1, "files"    # [Ljava/io/File;

    .prologue
    const/4 v8, 0x2

    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 149
    const/4 v3, 0x0

    .line 150
    .local v3, "payload":[B
    const/4 v2, 0x0

    .line 152
    .local v2, "mediaMime":Ljava/lang/String;
    iget-object v5, p0, Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # getter for: Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I
    invoke-static {v5}, Lcom/infraware/common/util/SbeamHelper;->access$100(Lcom/infraware/common/util/SbeamHelper;)I

    move-result v5

    if-ne v8, v5, :cond_1

    .line 153
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v5, "com.sec.android.directshare.DirectSharePopUp"

    invoke-direct {v0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 154
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v5, "POPUP_MODE"

    const-string/jumbo v6, "no_file_selected"

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 156
    invoke-static {}, Lcom/infraware/common/util/Utils;->isL()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 157
    const-string/jumbo v5, "com.sec.android.directshare"

    invoke-virtual {v0, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 158
    :cond_0
    iget-object v5, p0, Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # getter for: Lcom/infraware/common/util/SbeamHelper;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/infraware/common/util/SbeamHelper;->access$200(Lcom/infraware/common/util/SbeamHelper;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 159
    iget-object v5, p0, Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # setter for: Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I
    invoke-static {v5, v7}, Lcom/infraware/common/util/SbeamHelper;->access$102(Lcom/infraware/common/util/SbeamHelper;I)I

    .line 193
    .end local v0    # "i":Landroid/content/Intent;
    :goto_0
    return-object v4

    .line 161
    :cond_1
    const/4 v5, 0x3

    iget-object v6, p0, Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # getter for: Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I
    invoke-static {v6}, Lcom/infraware/common/util/SbeamHelper;->access$100(Lcom/infraware/common/util/SbeamHelper;)I

    move-result v6

    if-ne v5, v6, :cond_3

    .line 162
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v5, "com.sec.android.directshare.DirectSharePopUp"

    invoke-direct {v0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 163
    .restart local v0    # "i":Landroid/content/Intent;
    const-string/jumbo v5, "POPUP_MODE"

    const-string/jumbo v6, "from_cloud_file"

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 165
    invoke-static {}, Lcom/infraware/common/util/Utils;->isL()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 166
    const-string/jumbo v5, "com.sec.android.directshare"

    invoke-virtual {v0, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 167
    :cond_2
    iget-object v5, p0, Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # getter for: Lcom/infraware/common/util/SbeamHelper;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/infraware/common/util/SbeamHelper;->access$200(Lcom/infraware/common/util/SbeamHelper;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 168
    iget-object v5, p0, Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # setter for: Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I
    invoke-static {v5, v7}, Lcom/infraware/common/util/SbeamHelper;->access$102(Lcom/infraware/common/util/SbeamHelper;I)I

    goto :goto_0

    .line 170
    .end local v0    # "i":Landroid/content/Intent;
    :cond_3
    const/4 v5, 0x4

    iget-object v6, p0, Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # getter for: Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I
    invoke-static {v6}, Lcom/infraware/common/util/SbeamHelper;->access$100(Lcom/infraware/common/util/SbeamHelper;)I

    move-result v6

    if-ne v5, v6, :cond_5

    .line 171
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v5, "com.sec.android.directshare.DirectSharePopUp"

    invoke-direct {v0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 172
    .restart local v0    # "i":Landroid/content/Intent;
    const-string/jumbo v5, "POPUP_MODE"

    const-string/jumbo v6, "from_drm_file"

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 174
    invoke-static {}, Lcom/infraware/common/util/Utils;->isL()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 175
    const-string/jumbo v5, "com.sec.android.directshare"

    invoke-virtual {v0, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 176
    :cond_4
    iget-object v5, p0, Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # getter for: Lcom/infraware/common/util/SbeamHelper;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/infraware/common/util/SbeamHelper;->access$200(Lcom/infraware/common/util/SbeamHelper;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 177
    iget-object v5, p0, Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # setter for: Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I
    invoke-static {v5, v7}, Lcom/infraware/common/util/SbeamHelper;->access$102(Lcom/infraware/common/util/SbeamHelper;I)I

    goto :goto_0

    .line 179
    .end local v0    # "i":Landroid/content/Intent;
    :cond_5
    const/4 v5, 0x5

    iget-object v6, p0, Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # getter for: Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I
    invoke-static {v6}, Lcom/infraware/common/util/SbeamHelper;->access$100(Lcom/infraware/common/util/SbeamHelper;)I

    move-result v6

    if-ne v5, v6, :cond_7

    .line 180
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v5, "com.sec.android.directshare.DirectSharePopUp"

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 181
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v5, "POPUP_MODE"

    const-string/jumbo v6, "does_not_saved"

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 183
    invoke-static {}, Lcom/infraware/common/util/Utils;->isL()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 184
    const-string/jumbo v5, "com.sec.android.directshare"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 185
    :cond_6
    iget-object v5, p0, Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # getter for: Lcom/infraware/common/util/SbeamHelper;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/infraware/common/util/SbeamHelper;->access$200(Lcom/infraware/common/util/SbeamHelper;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 186
    iget-object v5, p0, Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # setter for: Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I
    invoke-static {v5, v7}, Lcom/infraware/common/util/SbeamHelper;->access$102(Lcom/infraware/common/util/SbeamHelper;I)I

    goto/16 :goto_0

    .line 189
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_7
    iget-object v4, p0, Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # getter for: Lcom/infraware/common/util/SbeamHelper;->mAppMime:Ljava/lang/String;
    invoke-static {v4}, Lcom/infraware/common/util/SbeamHelper;->access$300(Lcom/infraware/common/util/SbeamHelper;)Ljava/lang/String;

    move-result-object v2

    .line 190
    invoke-direct {p0, p1}, Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;->getJSONObject([Ljava/io/File;)[B

    move-result-object v3

    .line 193
    new-instance v4, Landroid/nfc/NdefRecord;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    new-array v6, v7, [B

    invoke-direct {v4, v8, v5, v6, v3}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    goto/16 :goto_0
.end method
