.class public Lcom/infraware/common/util/MailInputFilter;
.super Ljava/lang/Object;
.source "MailInputFilter.java"


# instance fields
.field private context:Landroid/content/Context;

.field private filters:[Landroid/text/InputFilter;

.field private m_MaxLength:I

.field private m_ToastMsg:Landroid/widget/Toast;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/text/InputFilter;

    iput-object v0, p0, Lcom/infraware/common/util/MailInputFilter;->filters:[Landroid/text/InputFilter;

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/common/util/MailInputFilter;->m_ToastMsg:Landroid/widget/Toast;

    .line 16
    const/16 v0, 0x80

    iput v0, p0, Lcom/infraware/common/util/MailInputFilter;->m_MaxLength:I

    .line 24
    iput-object p1, p0, Lcom/infraware/common/util/MailInputFilter;->context:Landroid/content/Context;

    .line 26
    iget-object v0, p0, Lcom/infraware/common/util/MailInputFilter;->filters:[Landroid/text/InputFilter;

    const/4 v1, 0x0

    new-instance v2, Lcom/infraware/common/util/MailInputFilter$1;

    invoke-direct {v2, p0}, Lcom/infraware/common/util/MailInputFilter$1;-><init>(Lcom/infraware/common/util/MailInputFilter;)V

    aput-object v2, v0, v1

    .line 59
    iget-object v0, p0, Lcom/infraware/common/util/MailInputFilter;->filters:[Landroid/text/InputFilter;

    const/4 v1, 0x1

    new-instance v2, Lcom/infraware/common/util/MailInputFilter$2;

    invoke-direct {v2, p0}, Lcom/infraware/common/util/MailInputFilter$2;-><init>(Lcom/infraware/common/util/MailInputFilter;)V

    aput-object v2, v0, v1

    .line 74
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/common/util/MailInputFilter;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/common/util/MailInputFilter;

    .prologue
    .line 12
    iget v0, p0, Lcom/infraware/common/util/MailInputFilter;->m_MaxLength:I

    return v0
.end method

.method static synthetic access$100(Lcom/infraware/common/util/MailInputFilter;C)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/common/util/MailInputFilter;
    .param p1, "x1"    # C

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/infraware/common/util/MailInputFilter;->isValidChar(C)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/infraware/common/util/MailInputFilter;I)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/common/util/MailInputFilter;
    .param p1, "x1"    # I

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/infraware/common/util/MailInputFilter;->onToastMessage(I)V

    return-void
.end method

.method private isValidChar(C)Z
    .locals 2
    .param p1, "ch"    # C

    .prologue
    const/4 v0, 0x1

    .line 86
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_EMAIL_FILTER()Z

    move-result v1

    if-nez v1, :cond_1

    .line 95
    :cond_0
    :goto_0
    return v0

    .line 90
    :cond_1
    const/16 v1, 0x30

    if-lt p1, v1, :cond_2

    const/16 v1, 0x39

    if-le p1, v1, :cond_0

    :cond_2
    const/16 v1, 0x61

    if-lt p1, v1, :cond_3

    const/16 v1, 0x7a

    if-le p1, v1, :cond_0

    :cond_3
    const/16 v1, 0x41

    if-lt p1, v1, :cond_4

    const/16 v1, 0x5a

    if-le p1, v1, :cond_0

    :cond_4
    const/16 v1, 0x2e

    if-eq p1, v1, :cond_0

    const/16 v1, 0x40

    if-eq p1, v1, :cond_0

    const/16 v1, 0x2d

    if-eq p1, v1, :cond_0

    const/16 v1, 0x5f

    if-eq p1, v1, :cond_0

    .line 95
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onToastMessage(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 78
    iget-object v0, p0, Lcom/infraware/common/util/MailInputFilter;->m_ToastMsg:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/infraware/common/util/MailInputFilter;->context:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/common/util/MailInputFilter;->m_ToastMsg:Landroid/widget/Toast;

    .line 82
    :goto_0
    iget-object v0, p0, Lcom/infraware/common/util/MailInputFilter;->m_ToastMsg:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 83
    return-void

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/infraware/common/util/MailInputFilter;->m_ToastMsg:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V

    goto :goto_0
.end method


# virtual methods
.method public getFilters()[Landroid/text/InputFilter;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/infraware/common/util/MailInputFilter;->filters:[Landroid/text/InputFilter;

    return-object v0
.end method
